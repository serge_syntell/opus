﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;
using Stalberg.TMS;
using Authority = SIL.AARTO.DAL.Entities.Authority;
using CourtJudgementType = SIL.AARTO.DAL.Entities.CourtJudgementType;

namespace SIL.AARTO.BLL.Representation
{
    public abstract partial class RepresentationBase
    {
        #region Services

        readonly AuthorityService authorityService = new AuthorityService();
        readonly RepresentationAuthorisationUserService repAutUserService = new RepresentationAuthorisationUserService();

        #endregion

        #region Bindings

        public List<SelectListItem> GetAuthoritySelectList()
        {
            var lookups = AuthorityLookupCache.GetCacheLookupValue(this.cultureName);
            var authorityList = this.authorityService.GetAll()
                .Where(a => lookups.Keys.Contains(a.AutIntNo))
                .OrderBy(a => a.AutName)
                .Select(a => new SelectListItem
                {
                    Text = lookups[a.AutIntNo],
                    Value = Convert.ToString(a.AutIntNo),
                    Selected = a.AutIntNo == AutIntNo
                }).ToList();

            return authorityList;
        }

        /// <summary>
        ///     Set model's values: RepresentationCode
        /// </summary>
        void BindRepOfficials(ref RepresentationModel model)
        {
            CheckModel(ref model);

            RepControlStatus.ddlRepOfficialEnabled = model.RepresentationCode.In(REPCODE_NONE, SUMMONS_REPCODE_NONE) || this.isNew;

            if (model.OfficialList.IsNullOrEmpty())
                model.OfficialList = this.repAutUserService.GetAll()
                    .OrderBy(r => r.RauName).ThenBy(r => r.RauForceNumber)
                    .Select(r => new SelectListItem
                    {
                        Text = r.RauName,
                        Value = r.RauName
                    }).ToList();
        }

        void BindRepCodes(ref RepresentationModel model)
        {
            CheckModel(ref model);

            var repCodes = new List<SelectListItem>();
            var unWithdrawnCount = model.Notice.UnwithdrawnChargesCount;
            var notWithdraw = unWithdrawnCount <= 1;
            if (!model.IsSummons)
            {
                repCodes.AddRange(new[]
                {
                    new SelectListItem { Value = Convert.ToString(REPCODE_NONE), Text = RepresentationResx.None },
                    new SelectListItem { Value = Convert.ToString(REPCODE_WITHDRAW), Text = RepresentationResx.NolleProseeque },
                    new SelectListItem { Value = Convert.ToString(REPCODE_REDUCTION), Text = RepresentationResx.FineChanged },
                    new SelectListItem { Value = Convert.ToString(REPCODE_NOCHANGE), Text = RepresentationResx.NoAction }
                });
            }
            else
            {
                if (unWithdrawnCount > 0)
                {
                    repCodes.AddRange(new[]
                    {
                        new SelectListItem { Value = Convert.ToString(SUMMONS_REPCODE_NONE), Text = RepresentationResx.None },
                        new SelectListItem
                        {
                            Value = Convert.ToString(SUMMONS_REPCODE_WITHDRAW),
                            Text = notWithdraw
                                ? RepresentationResx.WithdrawSummons
                                : RepresentationResx.WithdrawCharge
                        },
                        new SelectListItem { Value = Convert.ToString(SUMMONS_REPCODE_REDUCTION), Text = RepresentationResx.ReduceFine },
                        new SelectListItem { Value = Convert.ToString(SUMMONS_REPCODE_NOCHANGE), Text = RepresentationResx.NoChange }
                    });

                    if (!model.Notice.NotFilmTypeIsM)
                    {
                        repCodes.AddRange(new[]
                        {
                            new SelectListItem { Value = Convert.ToString(SUMMONS_REPCODE_REISSUE), Text = RepresentationResx.WithdrawSummonsForReIssue }
                            //new SelectListItem { Value = Convert.ToString(SUMMONS_REPCODE_ADDRESS), Text = RepresentationResx.ChangeOfAddress }
                        });
                    }
                }
            }

            model.RepCodeList = repCodes;
        }

        void BindVehicleMakes(ref RepresentationModel model)
        {
            CheckModel(ref model);
            if (!model.VehicleMakesList.IsNullOrEmpty()) return;

            using (var dr = this.vehicleMakeDB.GetVehicleMakeList(""))
            {
                if (!dr.HasRows) return;

                if (model.VehicleMakesList == null)
                    model.VehicleMakesList = new List<SelectListItem>();
                var lookups = VehicleMakeLookupCache.GetCacheLookupValue(this.cultureName);
                while (dr.Read())
                {
                    var vmIntNo = Helper.GetReaderValue<int>(dr, "VMIntNo");
                    string value;
                    if (!lookups.TryGetValue(vmIntNo, out value)) continue;

                    model.VehicleMakesList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(vmIntNo),
                        Text = value
                    });
                }
            }
            model.VehicleMakesList = model.VehicleMakesList.OrderBy(v => v.Text).ToList();
        }

        void BindVehicleTypes(ref RepresentationModel model)
        {
            CheckModel(ref model);
            if (!model.VehicleTypeList.IsNullOrEmpty()) return;

            using (var dr = this.vehicleTypeDB.GetVehicleTypeList(0, ""))
            {
                if (!dr.HasRows) return;

                if (model.VehicleTypeList == null)
                    model.VehicleTypeList = new List<SelectListItem>();
                var lookups = VehicleTypeLookupCache.GetCacheLookupValue(this.cultureName);
                while (dr.Read())
                {
                    var vtIntNo = Helper.GetReaderValue<int>(dr, "VTIntNo");
                    string value;
                    if (!lookups.TryGetValue(vtIntNo, out value)) continue;

                    model.VehicleTypeList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(vtIntNo),
                        Text = value
                    });
                }
            }
            model.VehicleTypeList = model.VehicleTypeList.OrderBy(v => v.Text).ToList();
        }

        void BindVehicleColours(ref RepresentationModel model)
        {
            CheckModel(ref model);
            if (!model.VehicleColourList.IsNullOrEmpty()) return;

            using (var dr = this.vehicleColourDB.GetVehicleColours())
            {
                if (!dr.HasRows) return;

                if (model.VehicleColourList == null)
                    model.VehicleColourList = new List<SelectListItem>();
                var lookups = VehicleColourLookupCache.GetCacheLookupValue(this.cultureName);
                while (dr.Read())
                {
                    var vcCode = Helper.GetReaderValue<string>(dr, "VCCode");
                    var vcIntNo = Helper.GetReaderValue<int>(dr, "VCIntNo");
                    string value;
                    if (!lookups.TryGetValue(vcIntNo, out value)) continue;

                    model.VehicleColourList.Add(new SelectListItem
                    {
                        Value = vcCode,
                        Text = value
                    });
                }
            }
            model.VehicleColourList = model.VehicleColourList.OrderBy(v => v.Value).ToList();
        }

        #endregion

        #region Checkings

        static bool CanChargeSelect(ChargeModel charge)
        {
            return charge != null && charge.ChgIsMain;
        }

        // Do not set "IsSuccessful = false" here!!!
        protected bool CanRepAdd(ChargeModel charge)
        {
            if (charge == null) return false;

            if (!RepAction.Equals(RepAction.O)
                && charge.IsReadOnly)
                return false;

            if (!RepAction.Equals(RepAction.O)
                && !CheckStatus(charge.ChargeStatus, 1))
            {
                if (!this.isGettingDetails)
                    charge.AddError(RepresentationResx.HasBeenFinalized, false);
                return false;
            }

            if (IsRegisterOrDecide())
            {
                if (charge.Notice.PendingNewOffender)
                {
                    if (!this.isGettingDetails)
                        charge.AddError(RepresentationResx.ThereIsAPendingNewOffenderTransactionInProgress, false);
                    return false;
                }

                // 2014-10-20, Oscar added, stagnant Summons is always allowed rep.
                if (!charge.IsStagnantSummons)
                {
                    if (Rules.AR_4650.Equals2(RepDecisionBeforeJudgement)
                        && charge.Notice.NoticeStatus >= CODE_Case_Number_Generated)
                    {
                        if (!this.isGettingDetails)
                            charge.AddError(RepresentationResx.NoRepresentationCanBeLoggedIfNoticeStatusIsGreaterThan710, false);
                        return false;
                    }
                }
            }
            //else if (charge.Notice.NoticeStatus >= Rules.AR_4538.GetValueOrDefault(CODE_Case_Number_Generated))
            //{
            //    if (!this.isGettingDetails)
            //        charge.AddError(string.Format(RepresentationResx.NoCooPodCanBeAllowedIfNoticeStatusIsGreaterThanOrEqualTo,
            //            Rules.AR_4538.GetValueOrDefault(CODE_Case_Number_Generated)), false);
            //    return false;
            //}
            else
            {
                // 2014-10-21, Oscar added, COO & POD of Summons can be done before court judgement.
                if (charge.IsSummons && GetJudgementCode(charge.CJTIntNo) > 0)
                {
                    if (!this.isGettingDetails)
                        charge.AddError(RepAction.Is(RepAction.ChangeOfOffender)
                            ? RepresentationResx.NoCOOCanBeDoneIfCourtJudgementHasBeenCaptured
                            : RepresentationResx.NoPODCanBeDoneIfCourtJudgementHasBeenCaptured, false);
                    return false;
                }
            }

            // 2014-10-20, Oscar added, stagnant Summons is always allowed rep.
            if (IsRegisterOrDecide() && !charge.IsStagnantSummons)
            {
                var courtDate = charge.SumCourtDate.GetValueOrDefault(DateTimeMax);
                //if (!Rules.AR_4536.GetValueOrDefault()
                //    && charge.IsSummons
                //    && charge.HasCaseNo
                //    && DateTimeNow.AddDays(Rules.AR_4050.GetValueOrDefault()).Date > courtDate.Date)
                if (DateTimeNow.AddDays(Rules.AR_4050.GetValueOrDefault()).Date > courtDate.Date)
                {
                    if (!this.isGettingDetails)
                        //charge.AddError(string.Format(!RepAction.Equals(RepAction.O)
                        //    ? RepresentationResx.CourtDateBeingLessThanDaysFromToday
                        //    : RepresentationResx.CourtDateBeingLessThanDaysFromToday_COO,
                        //    courtDate.ToString(DATE_FORMAT),
                        //    Rules.AR_4050.GetValueOrDefault()), false);
                        charge.AddError(string.Format(RepresentationResx.CourtDateBeingLessThanDaysFromToday, courtDate.ToString(DATE_FORMAT), Rules.AR_4050.GetValueOrDefault()));
                    return false;
                }
            }

            foreach (var rep in charge.RepresentationList)
            {
                if (rep.RepresentationCode.HasValue)
                {
                    if (RepAction.Equals(RepAction.R) && rep.RepresentationCode.Value.Equals(REPCODE_NONE))
                        return false;

                    if (Rules.AR_4595.GetValueOrDefault() && !rep.IsLocked
                        && RepAction.IsIn(RepAction.Decide, RepAction.ChangeOfOffender)
                        && rep.RepresentationCode.Value.In(REPCODE_NONE, SUMMONS_REPCODE_NONE))
                        return false;
                }
            }

            // ??? what about above checking of AR_4595 ???
            if (RepAction.Is(RepAction.Decide)
                && !Rules.AR_4595.GetValueOrDefault())
            {
                if (!this.isGettingDetails)
                    charge.AddError(RepresentationResx.OneStepRepresentationIsNotAllowed, false);
                return false;
            }

            if (!Authorise2ndRepresentation(ref charge, charge.RepresentationList.Max(r => r.RepRecommendDate), true))
                return false;

            return true;
        }

        // Do not set "IsSuccessful = false" here!!!
        protected bool CanRepSelect(RepresentationModel rep)
        {
            if (rep == null) return false;

            // allow COO to view history
            if (RepAction.Equals(RepAction.O))
                return true;

            if (!rep.RepresentationCode.HasValue || rep.IsLocked)
                return false;

            if (RepAction.Equals(RepAction.R)
                && !rep.RepresentationCode.In(REPCODE_NONE, SUMMONS_REPCODE_NONE))
                return false;

            if (IsRegisterOrDecide())
            {
                // 2014-10-20, Oscar added, stagnant Summons is always allowed rep.
                if (!rep.Charge.IsStagnantSummons)
                {
                    if (Rules.AR_4650.Equals2(RepDecisionBeforeJudgement))
                    {
                        CourtJudgementType cjt;
                        if (rep.Notice.NoticeStatus >= CODE_Case_Number_Generated
                            && GetJudgementCode(rep.Charge.CJTIntNo) > 0)
                        {
                            if (!this.isGettingDetails)
                                rep.AddError(RepresentationResx.NoRepresentationCanBeDecidedIfCourtJudgementHasBeenCaptured, false);
                            return false;
                        }
                    }
                }
            }

            //var courtDate = rep.Charge.SumCourtDate.GetValueOrDefault(DateTimeMax);
            //if (!Rules.AR_4536.GetValueOrDefault()
            //    && rep.IsSummons
            //    && rep.Charge.HasCaseNo
            //    && DateTimeNow.Date > courtDate.Date)
            //{
            //    if (!this.isGettingDetails)
            //        rep.AddError(string.Format(!RepAction.Equals(RepAction.O)
            //            ? RepresentationResx.CourtDateHasAlreadyPassed
            //            : RepresentationResx.CourtDateHasAlreadyPassed_COO,
            //            courtDate.ToString(DATE_FORMAT)), false);
            //    return false;
            //}

            var charge = rep.Charge;
            if (RepAction.Is(RepAction.Decide)
                && (rep.RepresentationCode.Equals2(REPCODE_CANCELLED)
                    // implemented from Iris's changes
                    || (!rep.IsLocked
                        && !rep.RepresentationCode.In(REPCODE_NONE, SUMMONS_REPCODE_NONE)
                        && GetLoggedRepresentationCount(ref charge) > 0)))
                return false;

            return true;
        }

        /// <summary>
        ///     type: 1=Add; 2=Decide; 3=Reverse
        /// </summary>
        protected static bool CheckStatus(int status, int type)
        {
            if (status > 900)
            {
                switch (type)
                {
                    case 1:
                        return false;

                    case 3:
                        return status.In(927, 940);
                }
            }
            return true;
        }

        static bool IsNoAOG(ICollection<ChargeModel> chargeList)
        {
            if (!chargeList.IsNullOrEmpty())
            {
                var summary = chargeList.Where(c => c.ChgIsMain)
                    .GroupBy(c => c.MainChargeID ?? c.ChgIntNo)
                    .Select(g => new { Id = g.Key, Sequence = g.Max(c => c.Sequence) });
                return chargeList.Any(c =>
                    (c.ChgNoAOG.Equals2(Y)
                        //|| c.RepresentationList.Any(r => AmountNoAOG.In(r.RepCurrentFineAmount, r.RepRevisedFineAmount))
                        )
                        && summary.Any(s => s.Sequence == c.Sequence && (s.Id == (c.MainChargeID ?? c.ChgIntNo))));
            }
            return false;
        }

        protected static bool IsChangeOfOffender(RepresentationModel rep)
        {
            return rep != null
                && (rep.ChangeOfOffender.Equals2(Y)
                    || rep.ChangeOfRegNo.Equals2(Y)
                    || rep.RepresentationCode.Equals2(SUMMONS_REPCODE_ADDRESS))
                && !rep.Notice.NotFilmTypeIsM;
        }

        bool IsPresentationOfDocument(RepresentationModel rep)
        {
            return rep != null && rep.IsPOD.GetValueOrDefault();
        }

        protected void FilterRepresentation(ChargeModel charge)
        {
            if (charge == null || charge.RepresentationList.IsNullOrEmpty()) return;
            charge.RepresentationList.RemoveAll(r =>
                r.RepIntNo > 0
                    && (RepAction.Is(RepAction.ChangeOfOffender)
                        ? !IsChangeOfOffender(r) || IsPresentationOfDocument(r)
                        : (RepAction.Is(RepAction.PresentationOfDocument)
                            ? !IsPresentationOfDocument(r)
                            : IsChangeOfOffender(r) || IsPresentationOfDocument(r))));
            if (charge.RepresentationList.Count(r => r.RepIntNo > 0) > 0)
                charge.RepresentationList.First(r => r.RepIntNo > 0).IsLastRep = true;
        }

        protected void FilterEmptyRepresentation(ChargeModel charge, Action onFiltering = null)
        {
            if (charge == null) return;
            var first = charge.RepresentationList.FirstOrDefault();
            //FilterRepresentation(charge);
            charge.Notice.ChargeList.ForEach(FilterRepresentation);

            if (onFiltering != null)
                onFiltering();

            if (!this.isNew)
                charge.RepresentationList.RemoveAll(r => r.RepIntNo <= 0);
            else if (first != null
                && charge.RepresentationList.FindIndex(r => r.RepIntNo == first.RepIntNo) < 0)
                charge.RepresentationList.Insert(0, first);
        }

        int GetLoggedRepresentationCountAll(int autIntNo, string ticketNo)
        {
            if (autIntNo <= 0 || string.IsNullOrWhiteSpace(ticketNo)) return -1;

            var paras = new[]
            {
                new SqlParameter("@AutIntNo", autIntNo),
                new SqlParameter("@TicketNo", ticketNo.Trim2())
            };

            int num;
            return int.TryParse(this.serviceDB.ExecuteScalar("GetLoggedRepresentationCountAll", paras).ToString(), out num)
                ? num : -1;
        }

        bool CheckRules(MessageResult result, bool hideWarning)
        {
            CheckModel(ref result);

            if (!Rules.AR_4650.In(NotPaymentDate,
                CDCourtRollCreatedDate,
                WithdrawSummonsRegardless,
                RepDecisionBeforeJudgement))
            {
                if (!hideWarning)
                    result.AddError(RepresentationResx.IncorrectValueOfAR_4650, false);
                return false;
            }
            return true;
        }

        protected int GetJudgementCode(int cjtIntNo)
        {
            var cjt = GetCourtJudgementType(cjtIntNo);
            return cjt.CjtIntNo > 0 ? Convert.ToInt32(cjt.CjtCode) : 0;
        }

        #region Authorise 2nd rep

        bool IsRegisterOrDecide()
        {
            return RepAction.IsIn(RepAction.Register, RepAction.Decide);
        }

        bool HasManagementOverridePermission(int autIntNo, MessageResult model = null)
        {
            if (!IsRegisterOrDecide()
                || !Rules.AR_4535.GetValueOrDefault()
                || (autIntNo > 0 && Roles.RoleList.FirstOrDefault(r => r.Equals2(ManagementOverride)) != null))
                return true;

            if (model != null && !this.isGettingDetails) model.AddError(RepresentationResx.OnlyOverrideManagementRoleCanAddANewRepresentation, false);
            return false;
        }

        // implemented from Iris's changes
        bool HasSupervisorPermission(int autIntNo)
        {
            return !IsRegisterOrDecide()
                || (autIntNo > 0 && Roles.RoleList.FirstOrDefault(r => r.Equals2(Supervisor)) != null);
        }

        // only when return 0, can add, decide or reverse
        int GetDecidedRepresentationCountAll(int autIntNo, string ticketNumber)
        {
            if (!IsRegisterOrDecide())
                return 0;

            if (autIntNo <= 0 || string.IsNullOrWhiteSpace(ticketNumber)) return -1;
            ticketNumber = ticketNumber.Trim2(true);
            var obj = this.serviceDB.ExecuteScalar("GetDecidedRepresentationCountAll",
                new[]
                {
                    new SqlParameter("@AutIntNo", autIntNo),
                    new SqlParameter("@TicketNo", ticketNumber)
                });
            return !obj.IsNullOrDBNull() ? Convert.ToInt32(obj) : -1;
        }

        int GetDecidedRepresentationCountAll(ref ChargeModel charge)
        {
            return !IsRegisterOrDecide()
                ? 0 : (charge == null
                    ? -1 : GetDecidedRepresentationCountAll(charge.AutIntNo, charge.NotTicketNo));
        }

        // implemented from Iris's changes
        int GetLoggedRepresentationCount(ref ChargeModel charge)
        {
            var count = !IsRegisterOrDecide()
                ? 0 : (charge == null
                    ? -1 : charge.Notice.ChargeList.Sum(c =>
                        c.RepresentationList.Count(r =>
                            r.RepIntNo > 0
                                && !r.IsLocked
                                && !r.Charge.IsReadOnly
                                && r.RepresentationCode.In(REPCODE_NONE, SUMMONS_REPCODE_NONE)
                                && !IsChangeOfOffender(r)
                                && !IsPresentationOfDocument(r))));

            if (count > 0 && !this.isGettingDetails) charge.AddError(RepresentationResx.ThereIsAlreadyARepresentationAwaitingDecisionForThisNotice, false);
            return count;
        }

        bool IsRecommendationDateIn24Hours(DateTime? recommendDate = null)
        {
            return !IsRegisterOrDecide()
                || !recommendDate.HasValue
                || recommendDate.Value.Date.AddDays(1) >= DateTimeNow.Date;
        }

        bool Authorise2ndRepresentation(ref ChargeModel charge, DateTime? recommendDate, bool isAdd)
        {
            return !IsRegisterOrDecide()
                || (isAdd
                    ? GetLoggedRepresentationCount(ref charge) == 0
                        && (GetDecidedRepresentationCountAll(ref charge) == 0
                            || HasManagementOverridePermission(charge.AutIntNo, charge))
                    : IsRecommendationDateIn24Hours(recommendDate)
                        || (GetDecidedRepresentationCountAll(ref charge) == 0
                            || HasSupervisorPermission(charge.AutIntNo)));
        }

        #endregion

        #endregion

        #region Searchs

        // Not finished yet!!! Do not forget binding keys when necessary
        // Do not set "IsSuccessful = false" here!!!
        protected NoticeModel GetChargeRepresentationList(int autIntNo, string ticketNumber, bool hideWarning = false)
        {
            var result = new NoticeModel();

            if (autIntNo <= 0 || string.IsNullOrWhiteSpace(ticketNumber)) return result;

            Authority authority;
            if ((authority = this.authorityService.GetByAutIntNo(autIntNo)) == null)
                return result;

            AutIntNo = autIntNo;
            ticketNumber = ticketNumber.Trim2(true);

            if (!CheckRules(result, hideWarning))
                return result;

            using (var dr = this.representationDB.NewSearchRepresentations(autIntNo, ticketNumber, 0, RepAction.ToString(), Rules.AR_4650, Rules.AR_4605.GetValueOrDefault(255)))
            {
                var index = 0;
                while (dr.Read())
                {
                    if (index == 0)
                    {
                        result.AutIntNo = autIntNo;
                        result.AutCode = authority.AutCode.Trim2();
                        result.NotTicketNo = ticketNumber; //1
                        result.IsSummons = Helper.GetReaderValue<bool>(dr, "IsSummons");
                        result.NotIntNo = Helper.GetReaderValue<int>(dr, "NotIntNo"); //1
                        result.SumIntNo = Helper.GetReaderValue<int>(dr, "SumIntNo"); //1
                        result.SummonsNo = Helper.GetReaderValue<string>(dr, "SummonsNo").Trim2();
                        result.NotLocDescr = Helper.GetReaderValue<string>(dr, "NotLocDescr").Trim2();
                        result.OrigRegNo = Helper.GetReaderValue<string>(dr, "OrigRegNo").Trim2(); //1
                        result.NotFilmType = Helper.GetReaderValue<string>(dr, "NotFilmType").Trim2(); //1
                        result.IsSection35 = Helper.GetReaderValue<bool>(dr, "IsSection35");
                        result.NoticeStatus = Helper.GetReaderValue<int>(dr, "NoticeStatus");
                        result.PendingNewOffender = Helper.GetReaderValue<bool>(dr, "PendingNewOffender");

                        result.Driver = new DriverModel
                        {
                            NotIntNo = result.NotIntNo,
                            DrvPOAdd1 = Helper.GetReaderValue<string>(dr, "DrvPoAdd1").Trim2(),
                            DrvPOAdd2 = Helper.GetReaderValue<string>(dr, "DrvPoAdd2").Trim2(),
                            DrvPOAdd3 = Helper.GetReaderValue<string>(dr, "DrvPoAdd3").Trim2(),
                            DrvPOAdd4 = Helper.GetReaderValue<string>(dr, "DrvPoAdd4").Trim2(),
                            DrvPOAdd5 = Helper.GetReaderValue<string>(dr, "DrvPoAdd5").Trim2(),
                            DrvPOCode = Helper.GetReaderValue<string>(dr, "DrvPoCode").Trim2(),
                            LastUser = LastUser
                        };
                    }

                    var chgIntNo = Helper.GetReaderValue<int>(dr, "ChgIntNo");
                    var charge = result.ChargeList.FirstOrDefault(c => c.ChgIntNo == chgIntNo);
                    if (charge == null)
                    {
                        charge = new ChargeModel
                        {
                            Notice = result,
                            ChgIntNo = chgIntNo, //1
                            Sequence = Helper.GetReaderValue<int>(dr, "Sequence"),
                            ChgOffenceDescr = Helper.GetReaderValue<string>(dr, "ChgOffenceDescr").Trim2(),
                            ChgIsMain = Helper.GetReaderValue<bool>(dr, "ChgIsMain"),
                            MainChargeID = Helper.GetReaderValue<int?>(dr, "MainChargeID"),
                            CSDescr = Helper.GetReaderValue<string>(dr, "CSDescr").Trim2(),
                            SumCaseNo = Helper.GetReaderValue<string>(dr, "SumCaseNo").Trim2(),
                            SumCourtDate = Helper.GetReaderValue<DateTime?>(dr, "SumCourtDate"),
                            ChgNoAOG = Helper.GetReaderValue<string>(dr, "ChgNoAOG").Trim2(),
                            RowVersion_Long = Helper.GetReaderValue<long>(dr, "ChargeRowVersion"),
                            ChgOffenceCode = Helper.GetReaderValue<string>(dr, "ChgOffenceCode").Trim2(),
                            HasRep = Helper.GetReaderValue<bool>(dr, "HasRep"),
                            ChargeStatus = Helper.GetReaderValue<int>(dr, "ChargeStatus"), //1
                            CJTIntNo = Helper.GetReaderValue<int>(dr, "CJTIntNo"),
                            IsReadOnly = Helper.GetReaderValue<bool>(dr, "IsReadOnly")
                        };
                        result.ChargeList.Add(charge);
                    }

                    var rep = new RepresentationModel
                    {
                        Charge = charge,
                        RepIntNo = Helper.GetReaderValue<int>(dr, "RepIntNo"),
                        RepOffenderName = Helper.GetReaderValue<string>(dr, "RepOffenderName").Trim2(), //1
                        FullName = Helper.GetReaderValue<string>(dr, "Name").Trim2(),
                        RepOfficial = Helper.GetReaderValue<string>(dr, "Repofficial").Trim2(), //1
                        RepCurrentFineAmount = Helper.GetReaderValue<decimal>(dr, "ChgFineAmount"), //1
                        RepRevisedFineAmount = Helper.GetReaderValue<decimal>(dr, "ChgRevFineAmount"), //1
                        RepRecommendDate = Helper.GetReaderValue<DateTime?>(dr, "RepRecommendDate"), //1
                        RepresentationCode = Helper.GetReaderValue<int?>(dr, "RCCode"), //1
                        ChangeOfOffender = Helper.GetReaderValue<string>(dr, "ChangeOfOffender").Trim2(), //1
                        LockedFlag = Helper.GetReaderValue<string>(dr, "LockedFlag").Trim2(),
                        RepCSDescr = Helper.GetReaderValue<string>(dr, "RepCSDescr").Trim2(), //1
                        RepDate = Helper.GetReaderValue<DateTime?>(dr, "RepDate"), //1
                        RepOffenderAddress = Helper.GetReaderValue<string>(dr, "RepOffenderAddress").Trim2(), //1
                        RepDetails = Helper.GetReaderValue<string>(dr, "RepDetails").Trim2(), //1
                        RepReverseReason = Helper.GetReaderValue<string>(dr, "RepReverseReason").Trim2(), //1
                        RepRecommend = Helper.GetReaderValue<string>(dr, "RepRecommend").Trim2(),
                        RCDescr = Helper.GetReaderValue<string>(dr, "RCDescr").Trim2(),
                        IsPOD = Helper.GetReaderValue<bool?>(dr, "IsPresentationOfDocument"),
                        RepType = Helper.GetReaderValue<string>(dr, "RepType"),
                        TargetCSCode = Helper.GetReaderValue<int?>(dr, "TargetCSCode"),
                        RWRIntNo = Helper.GetReaderValue<Int32?>(dr, "RWRIntNo")
                    };
                    rep.SetChangeAddress();
                    charge.RepresentationList.Add(rep);

                    index++;
                }
            }

            if (result.ChargeList.Count > 0)
            {
                result.ChargeList = SortChargeList(result.ChargeList);

                result.IsNoAOG = IsNoAOG(result.ChargeList);
                result.AllowNoAOG = Rules.AR_4537.GetValueOrDefault();
                result.AllowSection35 = Rules.AR_6610.GetValueOrDefault();

                var logged = GetLoggedRepresentationCountAll(result.AutIntNo, result.NotTicketNo);
                result.ChargeList.ForEach(c =>
                {
                    if (!c.IsReadOnly)
                        c.RepresentationList.ForEach(r => r.HasUndecidedRep = logged > 0);
                });

                if (IsRegisterOrDecide() && result.BlockNoAOG)
                {
                    //result.AddBlockMessage(RepresentationResx.NoAOGAlert);
                    result.ChargeList.Clear(); // if BlockNoAOG, then no blocking message, no further processing.

                    result.AddError(RepresentationResx.NoAOG_RepresentationNotAllowed, false);
                }
            }
            else if (!hideWarning)
            {
                var msg = RepresentationResx.ThisNoticeIsNotAvailableFor;
                if (RepAction != null)
                {
                    switch (RepAction.Name)
                    {
                        case RepAction.RegisterName:
                            msg += RepresentationResx.RegisteringRepresentation;
                            break;

                        case RepAction.DecideName:
                            msg += RepresentationResx.MakingDecisionOnRepresentation;
                            break;

                        case RepAction.ChangeOfOffenderName:
                            msg += RepresentationResx.ChangingOffender;
                            break;
                    }
                }
                msg += RepresentationResx.ThisNoticeHasEitherBeenPaidOrOtherStatus;
                result.AddError(msg, false);
                switch (Rules.AR_4650)
                {
                    case NotPaymentDate:
                        result.AddError(!RepAction.Equals(RepAction.O)
                            ? RepresentationResx.NoticeHavePassedPaymentDate : RepresentationResx.NoticeHavePassedPaymentDate_COO, false);
                        break;

                    case CDCourtRollCreatedDate:
                        result.AddError(!RepAction.Equals(RepAction.O)
                            ? RepresentationResx.SummonsHasBeenPrinted : RepresentationResx.SummonsHasBeenPrinted_COO, false);
                        break;
                }
            }

            return result;
        }

        static List<ChargeModel> SortChargeList(List<ChargeModel> chargeList)
        {
            if (chargeList.IsNullOrEmpty()) return chargeList;

            chargeList.ForEach(c =>
            {
                if (c.RepresentationList.IsNullOrEmpty()) return;

                c.RepresentationList = c.RepresentationList.OrderByDescending(r => r.RepIntNo).ToList();
                c.RepresentationList[0].IsLastRep = true;
            });

            return chargeList
                .OrderByDescending(c => c.IsReadOnly)
                .ThenBy(c => c.MainChargeID ?? c.ChgIntNo)
                .ThenByDescending(c => c.ChgIsMain)
                .ThenBy(c => c.Sequence).ToList();
        }

        // Do not set "IsSuccessful = false" here!!!
        public virtual NoticeModel GetChargeList(int autIntNo, string ticketNumber, bool hideWarning = false)
        {
            var result = GetChargeRepresentationList(autIntNo, ticketNumber, hideWarning);
            result.ChargeList.ForEach(c =>
            {
                c.CanSelect = CanChargeSelect(c);
                c.HasAlternate = result.ChargeList.Any(a => !a.ChgIsMain && c.ChgIntNo == a.MainChargeID.GetValueOrDefault(-1));
            });
            return result;
        }

        // Do not set "IsSuccessful = false" here!!!
        public virtual ChargeModel GetRepresentationList(int autIntNo, string ticketNumber, int chgIntNo, bool isReadOnly)
        {
            var model = GetChargeRepresentationList(autIntNo, ticketNumber);
            var result = model.ChargeList.FirstOrDefault(r => chgIntNo == r.ChgIntNo && r.IsReadOnly == isReadOnly);
            if (result == null) return new ChargeModel();

            FilterEmptyRepresentation(result, () =>
            {
                result.CanAdd = CanRepAdd(result);
                result.RepresentationList.ForEach(r => r.CanSelect = CanRepSelect(r));
            });

            return result;
        }

        protected bool GetRepresentation(ref RepresentationModel model, int autIntNo, string ticketNumber, int chgIntNo, bool isReadOnly, int repIntNo)
        {
            if (!CheckModel(ref model)) return false;

            this.isGettingDetails = true;
            var charge = GetRepresentationList(autIntNo, ticketNumber, chgIntNo, isReadOnly);
            this.isGettingDetails = false;

            RepresentationModel rep;
            if (charge.RepresentationList.IsNullOrEmpty()
                || (rep = charge.RepresentationList.FirstOrDefault(r => repIntNo <= 0 || r.RepIntNo == repIntNo)) == null)
            {
                model.AddError(!RepAction.Equals(RepAction.O)
                    ? RepresentationResx.ThereIsNoEligibleChargeOrRepresentationInformation
                    : RepresentationResx.ThereIsNoEligibleChargeOrRepresentationInformation_COO);
                return false;
            }

            if (rep.Charge.ChargeStatus >= Rules.AR_4570.GetValueOrDefault(900)
                && !rep.Charge.ChargeStatus.In(CODE_CASE_CANCELLED,
                    CODE_SUMMONS_WITHDRAWN_CHARGE))
            {
                model.AddError(RepresentationResx.ThisNoticeHasEitherBeenPaidOrOtherStatus);
                return false;
            }

            model.Combines(rep);

            if (repIntNo <= 0)
                model.RepIntNo = 0;

            return true;
        }

        /// <summary>
        ///     Set model's values: Charge.ChgIntNo, RepIntNo, IsSummons
        /// </summary>
        protected bool RetrieveRepresentation(ref RepresentationModel model, int repCode, bool isDummy = false)
        {
            if (!CheckModel(ref model)) return false;

            var paras = new[]
            {
                isDummy
                    ? new SqlParameter("@ChgIntNo", model.Charge.ChgIntNo)
                    : new SqlParameter("@RepIntNo", model.RepIntNo),
                new SqlParameter("@IsSummons", model.IsSummons)
            };

            var hasData = false;
            var rep = model;
            this.serviceDB.ExecuteReader(isDummy ? "DummyRepresentationDetails" : "RepresentationDetails", paras, dr =>
            {
                if (!dr.Read()) return;

                rep.LockedFlag = Helper.GetReaderValue<string>(dr, "LockedFlag").Trim2();
                hasData = true;

                #region cache

                rep.Driver.DrvIntNo = Helper.GetReaderValue<int>(dr, "DrvIntNo");
                rep.Charge.ChgIntNo = Helper.GetReaderValue<int>(dr, "ChgIntNo");
                rep.Charge.ChargeStatus = Helper.GetReaderValue<int>(dr, "CSCode");
                rep.Charge.RowVersion_Long = Helper.GetReaderValue<long>(dr, "ChargeRowVersion");

                #endregion

                rep.RepRecommendDate = Helper.GetReaderValue<DateTime?>(dr, "RepRecommendDate");
                if (Helper.GetReaderValue<decimal>(dr, "ChgFineAmount") == AmountNoAOG)
                    rep.Notice.IsNoAOG = true;

                if (!this.isNew)
                {
                    Helper.GetReaderValue<string>(dr, "RepDetails", v => rep.RepDetails = v.Trim2());
                    Helper.GetReaderValue<string>(dr, "RepReverseReason", v => rep.RepReverseReason = v.Trim2());
                    Helper.GetReaderValue<string>(dr, "RepRecommend", v => rep.RepRecommend = v.Trim2());
                    Helper.GetReaderValue<string>(dr, "RepOfficial", v => rep.RepOfficial = v.Trim2());
                    Helper.GetReaderValue<string>(dr, "ChangeOfOffender", v => rep.IsChangeOfOffender = v.Equals2(Y));
                }

                Helper.GetReaderValue<string>(dr, "LetterTo", v => rep.LetterTo = v.Equals2(LetterTo.A) ? LetterTo.D : v.Trim2());

                ShowChangeOfOffender(ref rep);

                Helper.GetReaderValue<string>(dr, "ChangeOfRegNo", v => rep.ChangeOfRegNo = v.Trim2());
                Helper.GetReaderValue<string>(dr, "ChangeOfRegNoType", v => rep.ChangeOfRegNoType = v.Trim2());
                Helper.GetReaderValue<string>(dr, "NewRegNoDetails", v => rep.NewRegNoDetails = v.Trim2());

                ShowChangeOfRegNo(ref rep);

                rep.Notice.OrigRegNo = Helper.GetReaderValue<string>(dr, "OrigRegNo").Trim2();
                rep.VMIntNo = Helper.GetReaderValue<int>(dr, "VMIntNo");
                rep.VTIntNo = Helper.GetReaderValue<int>(dr, "VTIntNo");
                rep.NotVehicleColour = Helper.GetReaderValue<string>(dr, "NotVehicleColour").Trim2();

                rep.Notice.NotRegNo = Helper.GetReaderValue<string>(dr, "NotRegNo").Trim2();
                rep.NewVMIntNo = Helper.GetReaderValue<int>(dr, "NewVMIntNo");
                rep.NewVTIntNo = Helper.GetReaderValue<int>(dr, "NewVTIntNo");
                rep.NewVehicleColourDescr = Helper.GetReaderValue<string>(dr, "NewVehicleColourDescr").Trim2();

                rep.Driver.DrvSurname = Helper.GetReaderValue<string>(dr, "DrvSurname").Trim2();
                rep.Driver.DrvForeNames = Helper.GetReaderValue<string>(dr, "DrvForenames").Trim2();

                var idNumber = Helper.GetReaderValue<string>(dr, "DrvIDNumber").Trim2();
                rep.Driver.DrvIDType = Helper.GetReaderValue<string>(dr, "DrvIDType").Trim2();
                if (string.IsNullOrWhiteSpace(rep.Driver.DrvIDType)
                    || rep.Driver.DrvIDType.Equals2(IDType.Type02))
                    rep.Driver.DrvIDNumber = idNumber;
                else
                    rep.Driver.DrvPassport = idNumber;

                rep.Driver.DrvInitials = Helper.GetReaderValue<string>(dr, "DrvInitials").Trim2();
                rep.Driver.DrvAge = Helper.GetReaderValue<string>(dr, "DrvAge").Trim2();
                rep.Driver.DrvNationality = Helper.GetReaderValue<string>(dr, "DrvNationality").Trim2();
                if (string.IsNullOrWhiteSpace(rep.Driver.DrvNationality))
                    rep.Driver.DrvNationality = Rules.AR_4560;

                rep.Driver.DrvPOAdd1 = Helper.GetReaderValue<string>(dr, "DrvPoAdd1").Trim2();
                rep.Driver.DrvPOAdd2 = Helper.GetReaderValue<string>(dr, "DrvPoAdd2").Trim2();
                rep.Driver.DrvPOAdd3 = Helper.GetReaderValue<string>(dr, "DrvPoAdd3").Trim2();
                rep.Driver.DrvPOAdd4 = Helper.GetReaderValue<string>(dr, "DrvPoAdd4").Trim2();
                rep.Driver.DrvPOAdd5 = Helper.GetReaderValue<string>(dr, "DrvPoAdd5").Trim2();
                rep.Driver.DrvPOCode = Helper.GetReaderValue<string>(dr, "DrvPoCode").Trim2();

                rep.Driver.DrvStAdd1 = Helper.GetReaderValue<string>(dr, "DrvStAdd1").Trim2();
                rep.Driver.DrvStAdd2 = Helper.GetReaderValue<string>(dr, "DrvStAdd2").Trim2();
                rep.Driver.DrvStAdd3 = Helper.GetReaderValue<string>(dr, "DrvStAdd3").Trim2();
                rep.Driver.DrvStAdd4 = Helper.GetReaderValue<string>(dr, "DrvStAdd4").Trim2();
                rep.Driver.DrvStCode = Helper.GetReaderValue<string>(dr, "DrvStCode").Trim2();

                rep.Driver.DrvHomeNo = Helper.GetReaderValue<string>(dr, "DrvHomeNo").Trim2();
                rep.Driver.DrvWorkNo = Helper.GetReaderValue<string>(dr, "DrvWorkNo").Trim2();
                rep.Driver.DrvCellNo = Helper.GetReaderValue<string>(dr, "DrvCellNo").Trim2();
            });

            if (hasData)
            {
                if (rep.IsLocked)
                {
                    RepControlStatus.btnSaveVisible = false;
                    RepControlStatus.btnDecideVisible = false;
                    RepControlStatus.btnReverseVisible = false;
                    RepControlStatus.chkChangeOffenderEnabled = false;
                    RepControlStatus.chkChangeAddressEnabled = false;
                    RepControlStatus.chkChangeRegistrationEnabled = false;
                }
                else
                {
                    if (!this.isNew
                        && !repCode.In(REPCODE_NONE, SUMMONS_REPCODE_NONE))
                    {
                        RepControlStatus.btnDecideVisible = false;
                        //if (!model.RepCodeList.IsNullOrEmpty()) model.RepCodeList.Clear();
                        RepControlStatus.rblCodesEnabled = false;
                    }
                    else
                        RepControlStatus.btnDecideVisible = true;

                    RepControlStatus.chkChangeOffenderEnabled = true;
                    RepControlStatus.chkChangeAddressEnabled = true;
                    RepControlStatus.chkChangeRegistrationEnabled = true;
                }
            }

            return model.IsSuccessful;
        }

        #endregion

        #region Operations

        #region Disabled

        //bool GetRepresentationDetails(ref RepresentationModel model,
        //    int autIntNo, string ticketNumber, int chgIntNo, int repIntNo)
        //{
        //    if (!CheckModel(ref model)
        //        || !GetRepresentation(ref model, autIntNo, ticketNumber, chgIntNo, repIntNo))
        //        return false;

        //    var repCode = REPCODE_NONE;
        //    if (model.RepresentationCode.HasValue)
        //        repCode = model.RepresentationCode.Value;
        //    else
        //        model.RepresentationCode = repCode;

        //    BindRepOfficials(ref model);

        //    RepControlStatus.txtRecommendVisible = false;
        //    RepControlStatus.btnReverseVisible = true;

        //    if (this.isNew)
        //    {
        //        model.RepDetails = string.Empty;
        //        model.RepReverseReason = string.Empty;
        //        model.RepOfficial = string.Empty;
        //    }

        //    if (!RepAction.Equals(RepAction.O))
        //    {
        //        SwitchRevAmount(ref model, false);
        //        BindRepCodes(ref model);
        //    }
        //        // !!! if we only need these in coo, we need to exclude these from others.
        //    else
        //    {
        //        BindVehicleMakes(ref model);
        //        BindVehicleTypes(ref model);
        //        BindVehicleColours(ref model);
        //    }

        //    if (RepControlStatus.btnSaveIsAdd)
        //    {
        //        #region

        //        RepControlStatus.btnReverseVisible = false;
        //        RepControlStatus.txtOffenceDescrEnabled = true;
        //        RepControlStatus.txtRecommendVisible = false;
        //        RepControlStatus.txtReversalReasonVisible = false;

        //        if (this.isNew)
        //            model.RepRecommend = string.Empty;

        //        if (!string.IsNullOrWhiteSpace(model.FullName))
        //            model.RepOffenderName = model.FullName;

        //        model.RepOffenderAddress = model.Driver.GetPoAddress();
        //        model.RepDate = this.dateTimeNow;

        //        #endregion
        //    }

        //    if (!RepControlStatus.btnSaveIsAdd
        //        && RepAction.Equals(RepAction.R))
        //    {
        //        #region

        //        if (model.Charge.ChargeStatus.In(CODE_REP_LOGGED_CHARGE, CODE_REP_PENDING))
        //        {
        //            RepControlStatus.btnReverseVisible = true;
        //            RepControlStatus.txtReversalReasonVisible = true;
        //        }
        //        else
        //        {
        //            RepControlStatus.btnReverseVisible = false;
        //            RepControlStatus.txtRecommendVisible = false;
        //            RepControlStatus.txtReversalReasonVisible = false;
        //            if (model.IsSummons)
        //                RepControlStatus.btnDecideVisible = false;
        //        }

        //        #endregion
        //    }

        //    if (RepAction.In(RepAction.D, RepAction.O))
        //    {
        //        #region

        //        RepControlStatus.txtRecommendVisible = true;
        //        RepControlStatus.btnDecideVisible = true;
        //        RepControlStatus.btnSaveVisible = false;

        //        if (RepAction.Equals(RepAction.O))
        //        {
        //            RepControlStatus.pnlChangeOfOffenderEnabled = true;

        //            model.IsChangeOfOffender = true;
        //        }

        //        if (repCode.In(REPCODE_CHANGEOFFENDER,
        //            SUMMONS_REPCODE_CHANGEOFFENDER))
        //        {
        //            RepControlStatus.pnlChangeOfOffenderEnabled = true;

        //            model.IsChangeOfOffender = true;

        //            if (repCode == REPCODE_CHANGEOFFENDER)
        //                repCode = REPCODE_NONE;
        //        }
        //        else if (repCode == REPCODE_CANCELLED)
        //        {
        //            RepControlStatus.pnlChangeOfOffenderEnabled = false;
        //            RepControlStatus.pnlChangeRegistrationEnabled = false;
        //            RepControlStatus.pnlRegistrationEnabled = false;

        //            RepControlStatus.chkChangeOffenderEnabled = false;
        //            RepControlStatus.chkChangeAddressEnabled = false;
        //            RepControlStatus.chkChangeRegistrationEnabled = false;
        //            // why set false here?
        //            //model.IsChangeOfOffender = false;
        //            //model.IsChangeOfRegNo = false;

        //            repCode = !model.IsSummons
        //                ? REPCODE_NONE : SUMMONS_REPCODE_NONE;
        //        }

        //        if (!this.isNew)
        //        {
        //            if (model.IsSummons
        //                && repCode.In(REPCODE_NONE,
        //                    SUMMONS_REPCODE_NONE,
        //                    SUMMONS_REPCODE_CHANGEOFFENDER))
        //                model.RepresentationCode = SUMMONS_REPCODE_NONE;
        //            else if (!model.RepCodeList.IsNullOrEmpty()
        //                && model.RepCodeList.FindIndex(i => i.Value == Convert.ToString(repCode)) >= 0)
        //                model.RepresentationCode = repCode;
        //        }
        //        else if (!model.RepCodeList.IsNullOrEmpty())
        //            model.RepresentationCode = Convert.ToInt32(model.RepCodeList.First().Value);

        //        if (model.RepIntNo == 0)
        //        {
        //            RepControlStatus.txtReversalReasonVisible = false;
        //            RepControlStatus.btnReverseVisible = false;

        //            model.RepDetails = string.Empty;
        //            model.RepDate = this.dateTimeNow;
        //            model.RepOffenderTelNo = string.Empty;
        //            model.RepOfficial = string.Empty;
        //            model.Charge.ChargeStatus = !model.IsSummons
        //                ? CODE_REP_LOGGED_CHARGE : CODE_REP_LOGGED_SUMMONS;
        //            model.IsChangeOfOffender = RepAction.Equals(RepAction.O);
        //            model.RepType = RepType.A;

        //            var rep = model;
        //            if (RepAction.Equals(RepAction.D))
        //            {
        //                var scope = TransactionScope(() =>
        //                {
        //                    if (!RepresentationUpdate(ref rep))
        //                        return false;

        //                    if (!PushQueue_RepresentationGracePeriod_Notice(rep.RepIntNo, rep.AutCode, rep)) return false;

        //                    return true;
        //                }, ex => rep.AddError(ex.ToString()));
        //                if (!scope)
        //                {
        //                    model.IsSuccessful = false;
        //                    return false;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (model.IsSummons
        //                && !model.Notice.NotFilmTypeIsM
        //                && !repCode.In(REPCODE_WITHDRAW,
        //                    SUMMONS_REPCODE_WITHDRAW,
        //                    SUMMONS_REPCODE_REDUCTION)
        //                && (repCode != SUMMONS_REPCODE_NONE
        //                    || model.Charge.ChargeStatus != CODE_REP_LOGGED_SUMMONS))
        //            {
        //                RepControlStatus.txtReversalReasonVisible = false;
        //                RepControlStatus.btnReverseVisible = false;
        //            }
        //            else
        //            {
        //                RepControlStatus.txtReversalReasonVisible = true;
        //                RepControlStatus.btnReverseVisible = true;

        //                if (model.Notice.UnwithdrawnChargesCount <= 0)
        //                {
        //                    RepControlStatus.btnDecideVisible = false;

        //                    if (!model.RepCodeList.IsNullOrEmpty())
        //                        model.RepCodeList.Clear();
        //                }
        //            }
        //        }

        //        #endregion

        //        if (!RetrieveRepresentation(ref model, repCode, this.isNew && RepAction.Equals(RepAction.O)))
        //            return false;
        //    }

        //    if ((RepControlStatus.btnDecideVisible
        //        || RepControlStatus.btnReverseVisible)
        //        && !Authorise2ndRepresentation(autIntNo, ticketNumber, chgIntNo, model.RepRecommendDate))
        //    {
        //        RepControlStatus.btnDecideVisible = false;
        //        RepControlStatus.btnReverseVisible = false;
        //    }

        //    if (RepControlStatus.btnReverseVisible)
        //        RepControlStatus.btnReverseVisible = CheckStatus(model.Charge.ChargeStatus, 3);

        //    ShowNoAOGOffence(ref model);

        //    if (RepAction.Equals(RepAction.O))
        //    {
        //        RepControlStatus.txtRecommendVisible = false;
        //        RepControlStatus.txtReversalReasonVisible = false;
        //        RepControlStatus.btnReverseVisible = false;
        //        RepControlStatus.btnSaveVisible = false;
        //    }

        //    return true;
        //}

        #endregion

        bool GetRepresentationDetails(ref RepresentationModel model,
            int autIntNo, string ticketNumber, int chgIntNo, bool isReadOnly, int repIntNo)
        {
            if (!CheckModel(ref model)
                || !GetRepresentation(ref model, autIntNo, ticketNumber, chgIntNo, isReadOnly, repIntNo))
                return false;

            var repCode = REPCODE_NONE;
            if (model.RepresentationCode.HasValue)
                repCode = model.RepresentationCode.Value;
            else
                model.RepresentationCode = repCode;

            BindRepOfficials(ref model);

            SwitchRevAmount(ref model, false);
            if (!RepAction.Equals(RepAction.O))
            {
                BindRepCodes(ref model);
            }
                // !!! if we only need these in coo, we need to exclude these from others.
            else
            {
                BindVehicleMakes(ref model);
                BindVehicleTypes(ref model);
                BindVehicleColours(ref model);
            }

            if (!BuildDetails(ref model, repCode))
            {
                model.IsSuccessful = false;
                return false;
            }

            var charge = model.Charge;
            if ((RepControlStatus.btnDecideVisible
                || RepControlStatus.btnReverseVisible)
                //&& !Authorise2ndRepresentation(ref charge, model.RepRecommendDate, false))
                // 2014-08-13, Oscar changed, if RepRecommendDate is null, we should use RepDate
                && !Authorise2ndRepresentation(ref charge, model.RepRecommendDate ?? model.RepDate, false))
            {
                RepControlStatus.btnDecideVisible = false;
                RepControlStatus.btnReverseVisible = false;
            }

            if (RepControlStatus.btnReverseVisible)
                RepControlStatus.btnReverseVisible = CheckStatus(model.Charge.ChargeStatus, 3);

            ShowNoAOGOffence(ref model);

            if (model.Charge.IsReadOnly
                && (RepControlStatus.btnSaveVisible
                    || RepControlStatus.btnDecideVisible
                    || RepControlStatus.btnReverseVisible))
            {
                RepControlStatus.btnSaveVisible = false;
                RepControlStatus.btnDecideVisible = false;
                RepControlStatus.btnReverseVisible = false;
            }

            return true;
        }

        protected void GenerateNewEmptyRep(ref RepresentationModel model)
        {
            CheckModel(ref model);
            model.RepDate = DateTimeNow;
            if (!string.IsNullOrWhiteSpace(model.FullName))
                model.RepOffenderName = model.FullName;
            model.RepOffenderAddress = model.Driver.GetPoAddress();
            model.RepOffenderTelNo = string.Empty;
            model.RepDetails = string.Empty;
            model.RepRecommend = string.Empty;
            model.RepReverseReason = string.Empty;
            model.RepOfficial = string.Empty;
            model.RepType = RepType.A;

            if (!RepAction.Equals(RepAction.O))
            {
                SwitchRevAmount(ref model, false);

                if (!model.IsSummons)
                    model.Charge.ChargeStatus = CODE_REP_LOGGED_CHARGE;
                else if (model.Charge.ChargeStatus == 0
                    || !model.Charge.ChargeStatus.Between(Code_Summons_Generated, CODE_Case_Number_Generated - 1))
                    model.Charge.ChargeStatus = CODE_REP_LOGGED_SUMMONS;

                if (!model.RepCodeList.IsNullOrEmpty())
                    model.RepresentationCode = Convert.ToInt32(model.RepCodeList.First().Value);
                model.IsChangeOfOffender = false;
            }
            else
            {
                model.RepresentationCode = !model.IsSummons
                    ? REPCODE_CHANGEOFFENDER : SUMMONS_REPCODE_CHANGEOFFENDER;
                model.IsChangeOfOffender = true;
            }
        }

        #endregion

        #region Control Status

        // S56 is not allowed to change offender.
        void CheckChangeOfOffenderForS56(ref RepresentationModel model)
        {
            CheckModel(ref model);

            if (model.Notice.NotFilmTypeIsM)
            {
                model.IsChangeOfOffender = false;
            }
        }

        void ShowChangeOfOffender(ref RepresentationModel model)
        {
            CheckModel(ref model);

            CheckChangeOfOffenderForS56(ref model);
        }

        void ShowChangeOfRegNo(ref RepresentationModel model)
        {
            CheckModel(ref model);

            RepControlStatus.pnlChangeRegistrationVisible = model.IsChangeOfRegNo;

            if (model.IsChangeOfRegNo)
            {
                model.LetterTo = LetterTo.D;

                model.IsChangeOfOffender = !model.NewRegNoDetailsChecked
                    && !model.Notice.NotFilmTypeIsM;
                ShowChangeOfOffender(ref model);
            }

            ShowRegNoDetails(ref model);

            if (!RepControlStatus.pnlChangeRegistrationVisible)
                ShowChangeOfOffender(ref model);

            CheckChangeOfOffenderForS56(ref model);
        }

        void ShowRegNoDetails(ref RepresentationModel model)
        {
            CheckModel(ref model);

            RepControlStatus.pnlRegistrationVisible = RepControlStatus.pnlChangeRegistrationVisible
                && !model.NewRegNoDetailsChecked;

            if (RepControlStatus.pnlChangeRegistrationVisible
                && model.NewRegNoDetailsChecked)
            {
                model.RepresentationCode = !model.IsSummons
                    ? REPCODE_WITHDRAW : SUMMONS_REPCODE_WITHDRAW;
            }
        }

        void ShowNoAOGOffence(ref RepresentationModel model)
        {
            CheckModel(ref model);

            RepControlStatus.txtRevAmountEnabled = !model.Notice.BlockNoAOG;
            model.RepRevisedFineAmountCache = model.RepRevisedFineAmount;

            if (!model.Notice.IsSection35
                && model.Notice.BlockNoAOG
                && !model.RepCodeList.IsNullOrEmpty())
            {
                model.RepCodeList.RemoveAll(c => c.Value.In(
                    Convert.ToString(REPCODE_REDUCTION),
                    Convert.ToString(SUMMONS_REPCODE_REDUCTION)));
            }
        }

        #endregion
    }
}
