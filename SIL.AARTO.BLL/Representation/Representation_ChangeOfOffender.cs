﻿using System;
using System.Data.SqlClient;
using System.Linq;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation
{
    public class Representation_ChangeOfOffender : RepresentationBase
    {
        protected override void OnConstructorLoading()
        {
            Title = RepresentationResx.ChangeOfOffender;
            RepAction = RepAction.ChangeOfOffender;

            ViewName.EditorPartChangeOfRegistration = "_EditorPart_ChangeOfRegistration";
            ViewName.EditorPartChangeOfOffender = "_EditorPart_ChangeOfOffender";

            //RepControlStatus.pnlAmountVisible = false;
        }

        public override void OnConstructorLoaded() {}

        #region Checkings

        bool CanAddChangeOfOffender(ChargeModel charge)
        {
            var canAdd = charge.RepresentationList.Count(r => r.RepIntNo > 0) <= 0
                || (charge.RepresentationList.Any(r => r.IsLastRep
                    // allowing adding new COO for all Insufficient Details and existing COO
                    && (r.RepresentationCode.Equals2(REPCODE_CANCELLED) || IsChangeOfOffender(r))));
            //if (isNew && !canAdd)
            //    charge.AddError(RepresentationResx.YouHaveAlreadyMadeADecisionForChangeOfOffender);
            return canAdd && !charge.Notice.NotFilmTypeIsM;
        }

        bool CanAddChangeOfOffender(NoticeModel notice)
        {
            if (notice == null || notice.ChargeList.IsNullOrEmpty()) return false;
            return notice.ChargeList.TrueForAll(c => CanAddChangeOfOffender(c) && CanRepAdd(c))
                && notice.ChargeList.Any(c => !c.IsReadOnly && CheckStatus(c.ChargeStatus, 1));
        }

        #endregion

        #region Search

        public override NoticeModel GetChargeList(int autIntNo, string ticketNumber, bool hideWarning = false)
        {
            var result = base.GetChargeList(autIntNo, ticketNumber, hideWarning);

            result.ChargeList.ForEach(c =>
            {
                FilterRepresentation(c);
                c.CanSelect = c.CanSelect && c.RepresentationList.Count(r => r.RepIntNo > 0) > 0;
            });

            result.CanAdd = CanAddChangeOfOffender(result);

            if (!hideWarning && !result.HasError && !result.CanAdd)
                result.AddError(result.NotFilmTypeIsM
                    ? RepresentationResx.S56IsNotAllowedToDoCOO
                    : RepresentationResx.CanNotDoChangeOfOffender, false);

            return result;
        }

        public override ChargeModel GetRepresentationList(int autIntNo, string ticketNumber, int chgIntNo, bool isReadOnly)
        {
            var result = GetChargeRepresentationList(autIntNo, ticketNumber).ChargeList.FirstOrDefault(c => c.ChgIntNo.Equals2(chgIntNo));
            if (result == null) return new ChargeModel();

            FilterEmptyRepresentation(result, () =>
            {
                result.CanAdd = CanAddChangeOfOffender(result.Notice);
                if (!result.HasError && !result.CanAdd)
                    result.AddError(RepresentationResx.CanNotDoChangeOfOffender, false);

                result.RepresentationList.ForEach(r => r.CanSelect = CanRepSelect(r) && !r.Notice.NotFilmTypeIsM);
            });

            return result;
        }

        #endregion

        #region Operateions

        protected override bool BuildDetails(ref RepresentationModel model, int repCode)
        {
            RepControlStatus.txtOffenceDescrEnabled = false;
            RepControlStatus.txtRecommendVisible = false;
            RepControlStatus.txtReversalReasonVisible = false;
            RepControlStatus.btnSaveVisible = false;
            RepControlStatus.btnDecideVisible = true;
            RepControlStatus.btnReverseVisible = false;
            //RepControlStatus.pnlAmountVisible = false;
            RepControlStatus.btnInsufficientDetailsVisible = true;

            RepControlStatus.chkChangeOffenderEnabled = true;
            RepControlStatus.chkChangeAddressEnabled = true;
            RepControlStatus.chkChangeRegistrationEnabled = true;

            RepControlStatus.pnlChangeOfOffenderEnabled = true;
            RepControlStatus.pnlChangeRegistrationEnabled = true;

            if (isNew)
            {
                RepControlStatus.txtOffenceDescrEnabled = true;

                GenerateNewEmptyRep(ref model);
            }

            if (!RetrieveRepresentation(ref model, repCode, isNew))
                return false;

            if (!isNew)
            {
                RepControlStatus.btnSaveVisible = false;
                RepControlStatus.btnDecideVisible = false;
                RepControlStatus.btnReverseVisible = false;
                RepControlStatus.btnInsufficientDetailsVisible = model.IsChangeOfOffender
                    && model.RepresentationCode.Equals2(REPCODE_CANCELLED)
                    && model.RepDetails.Equals2(RepresentationResx.InsufficientDetails);

                RepControlStatus.chkChangeOffenderEnabled = false;
                RepControlStatus.chkChangeAddressEnabled = false;
                RepControlStatus.chkChangeRegistrationEnabled = false;

                RepControlStatus.pnlChangeOfOffenderEnabled = false;
                RepControlStatus.pnlChangeRegistrationEnabled = false;

                // set chkNoFurtherDetails value for COR in "View"
                model.NewRegNoDetailsChecked = model.RepIntNo > 0
                    && model.IsChangeOfRegNo
                    && model.RepresentationCode.In(REPCODE_WITHDRAW, SUMMONS_REPCODE_WITHDRAW);
                RepControlStatus.pnlRegistrationVisible = !model.NewRegNoDetailsChecked;
            }

            if (!model.IsChangeOfOffender
                && !model.IsChangeAddress)
                model.IsChangeOfOffender = true;

            return true;
        }

        protected override bool RepresentationUpdate(ref RepresentationModel model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Set model's values: RepresentationCode, RepOfficial, RepRecommend, RepDate, RepOffenderName, RepOffenderAddress,
        ///     RepOffenderTelNo, RepType, ChgIntNo, Charge.ChargeStatus, Charge.RowVersion_Long, ChangeOfOffender, RepDetails,
        ///     LetterTo, IsSummons, ChangeOfRegNo, Notice.NotIntNo, Notice.NotRegNo, ChangeOfRegNoType, NewVMIntNo, NewVTIntNo,
        ///     NewVehicleColourDescr, MIPASource, Driver
        /// </summary>
        protected override bool RepresentationDecide(ref RepresentationModel model)
        {
            if (!CheckModel(ref model, true)) return false;

            if (model.Driver != null)
            {
                if (!string.IsNullOrWhiteSpace(model.Driver.DrvIDNumber))
                    model.Driver.DrvIDType = IDType.Type02;
                else
                {
                    model.Driver.DrvIDNumber = model.Driver.DrvPassport;
                    model.Driver.DrvIDType = IDType.Type01;
                }
            }

            string errorMsg;
            var result = representationDB.DecideRepresentationForCoo(out errorMsg,
                model.RepresentationCode.GetValueOrDefault(),
                model.RepOfficial ?? "",
                model.RepRecommend ?? "",
                model.RepDate.GetValueOrDefault(DateTimeNow),
                model.RepOffenderName ?? "",
                model.RepOffenderAddress ?? "",
                model.RepOffenderTelNo ?? "",
                model.RepType,
                LastUser,
                model.ChgIntNo,
                model.Charge.ChargeStatus,
                model.Charge.RowVersion_Long,
                model.ChangeOfOffender ?? "",
                model.RepDetails ?? "",
                model.LetterTo ?? "",
                model.IsSummons,
                model.ChangeOfRegNo ?? "",
                model.Notice.NotIntNo,
                model.Notice.NotRegNo ?? "",
                model.ChangeOfRegNoType ?? "",
                model.NewVMIntNo,
                model.NewVTIntNo,
                model.NewVehicleColourDescr ?? "",
                model.MIPASource ?? "",
                Rules.AR_4660.GetValueOrDefault(),
                Rules.AR_9060 ?? N,
                Rules.AR_9120 ?? N,
                Rules.AR_9130.GetValueOrDefault(12),
                RepAction.Is(RepAction.PresentationOfDocument),
                model.Driver);

            if (result == null || result.Count <= 0 || result[0] <= 0)
            {
                model.AddError(string.Format(RepresentationResx.TheRepresentationCannotBeDecidedAsThereWasAnError_COO, errorMsg));
                return false;
            }

            model.RepIntNo = result[0];
            return model.IsSuccessful;
        }

        bool InsufficientChangeOfOffender(ref RepresentationModel model)
        {
            if (!CheckModel(ref model)) return false;

            var paras = new[]
            {
                new SqlParameter("@IsSummons", model.IsSummons),
                new SqlParameter("@ChgIntNo", model.ChgIntNo),
                new SqlParameter("@RepOffenderName", model.RepOffenderName ?? ""),
                new SqlParameter("@RepOffenderAddress", model.RepOffenderAddress ?? ""),
                new SqlParameter("@RepOffenderTelNo", model.RepOffenderTelNo ?? ""),
                new SqlParameter("@RepDetails", model.RepDetails ?? ""),
                new SqlParameter("@RepReverseReason", model.RepReverseReason ?? ""),
                new SqlParameter("@RepOfficial", model.RepOfficial ?? ""),
                new SqlParameter("@LastUser", LastUser)
            };

            var result = Convert.ToInt32(serviceDB.ExecuteScalar("InsufficientChangeOfOffender", paras));
            if (result <= 0)
            {
                switch (result)
                {
                    case -1:
                    case -3:
                        model.AddError(RepresentationResx.InsufficentDetailsFailedInInsertingInformation);
                        break;

                    case -2:
                    case -4:
                        model.AddError(RepresentationResx.InsufficentDetailsFailedInInsertingEvidencePack);
                        break;
                }
                return false;
            }
            model.RepIntNo = result;

            PostRepLetter(ref model, false);
            return true;
        }

        protected override bool RepresentationReverse(ref RepresentationModel model)
        {
            return InsufficientChangeOfOffender(ref model);
        }

        #endregion
    }
}
