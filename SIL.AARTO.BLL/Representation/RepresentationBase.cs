﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;
using Stalberg.TMS;
using Stalberg.TMS.Data;
using Charge = SIL.AARTO.DAL.Entities.Charge;
using CourtJudgementType = SIL.AARTO.DAL.Entities.CourtJudgementType;
using Notice = SIL.AARTO.DAL.Entities.Notice;
using TransactionManager = System.Transactions.TransactionManager;

namespace SIL.AARTO.BLL.Representation
{
    public abstract partial class RepresentationBase : IDisposable
    {
        #region Constructor

        protected RepresentationBase()
        {
            RepControlStatus = new RepControlStatus();
            ViewName = new RepViewName
            {
                Index = "Representation",
                Message = "_Message",
                Cache = "_Cache",
                Searcher = "_Searcher",
                Search = "_Search",
                ChargeList = "_ChargeList",
                RepresentationList = "_RepresentationList",
                Editor = "_Editor",
                EditorPart = "_EditorPart"
            };
            OnConstructorLoading();
        }

        protected abstract void OnConstructorLoading();

        [EditorBrowsable(EditorBrowsableState.Never)]
        public abstract void OnConstructorLoaded();

        #endregion

        #region Consts

        protected const string Y = "Y";
        protected const string N = "N";
        const string Null = "null";
        const string ManagementOverride = "ManagementOverride";
        const string Supervisor = "Supervisor";
        const string NotPaymentDate = "NotPaymentDate";
        const string CDCourtRollCreatedDate = "CDCourtRollCreatedDate";
        protected const string MandatoryValue = "Cashier";
        const string DATE_FORMAT = "yyyy-MM-dd";
        const string DateFormat_PrintFileName = "yyyy-MM-dd_HH-mm-ss-fff";
        public const decimal AmountNoAOG = 999999;
        public const string RegNoRep = "RegNoRep";
        public const string Represent = "Represent";
        const string QueueSeparator = "|";
        const string RepDecision = "RepDecision";
        const string RepDecision_ = "RepDecision_";
        const string InsufficientDetails = "InsufficientDetails";
        const string RepInsufficentDecision_ = "RepInsufficentDecision_";
        const string Representation_LetterViewerAspx = "Representation_LetterViewer.aspx";
        const string RepresentationInsufficientDetails_LetterViewerAspx = "RepresentationInsufficientDetails_LetterViewer.aspx";
        const string RepDecisionBeforeJudgement = "RepDecisionBeforeJudgement";
        const string WithdrawSummonsRegardless = "WithdrawSummonsRegardless";

        #region Status

        public const int CODE_First_Notice_Printed = 250;
        public const int CODE_NOTICE_NEW_OFFENDER = 255;
        public const int CODE_Second_Notice_Created = 260;
        public const int CODE_Second_Notice_Printed = 270;
        public const int CODE_Second_Notice_Posted = 275;
        public const int CODE_REP_LOGGED_CHARGE = 410;
        public const int CODE_REP_PENDING = 420;
        public const int CODE_NoAOG = 500;
        public const int CODE_NoAOG_Printed = 510;
        public const int CODE_NoAOG_Posted = 520;
        public const int CODE_Officer_Error = 581;
        public const int CODE_Error_End = 599;
        public const int CODE_SUMMONS_REISSUE = 600;
        public const int CODE_No_Summons_Server_For_Area = 603;
        public const int CODE_No_Available_Court_Dates = 606;
        public const int Code_Summons_Generated = 610;
        public const int CODE_REP_LOGGED_SUMMONS = 660;
        public const int CODE_Case_Number_Generated = 710;
        public const int Code_Completion = 900;
        public const int CODE_CASE_CANCELLED = 927;
        public const int CODE_SUMMONS_WITHDRAWN_CHARGE = 940;

        #endregion

        #region RepCode

        public const int REPCODE_NONE = 0;
        public const int REPCODE_CASEWITHDRAWNORESUMMONS = 1;
        public const int REPCODE_WITHDRAW = 3;
        public const int REPCODE_REDUCTION = 4;
        public const int REPCODE_NOCHANGE = 5;
        public const int REPCODE_CHANGEOFFENDER = 9;
        public const int SUMMONS_REPCODE_NONE = 10;
        public const int SUMMONS_REPCODE_WITHDRAW = 13;
        public const int SUMMONS_REPCODE_REDUCTION = 14;
        public const int SUMMONS_REPCODE_NOCHANGE = 15;
        public const int SUMMONS_REPCODE_CHANGEOFFENDER = 19;
        public const int SUMMONS_REPCODE_REISSUE = 30;
        public const int SUMMONS_REPCODE_ADDRESS = 40;
        public const int REPCODE_CANCELLED = 99;

        #endregion

        #endregion

        #region Delegates

        public Func<string> GetTMSDomainUrl { get; set; }

        #endregion

        #region Services

        readonly ChargeService chargeService = new ChargeService();
        readonly CourtJudgementTypeService courtJudgementTypeService = new CourtJudgementTypeService();
        readonly ChargeSumChargeService cscService = new ChargeSumChargeService();
        readonly string cultureName = Thread.CurrentThread.CurrentCulture.Name;
        readonly NoticeFrameService noticeFrameService = new NoticeFrameService();
        readonly NoticeService noticeService = new NoticeService();
        readonly PrintFileNameNoticeService printFileNameNoticeService = new PrintFileNameNoticeService();
        readonly PrintFileNameService printFileNameService = new PrintFileNameService();
        readonly RepresentationService representationService = new RepresentationService();
        readonly SummonsRepresentationService summonsRepresentationService = new SummonsRepresentationService();
        readonly SummonsService summonsService = new SummonsService();
        readonly SysParamService sysParamService = new SysParamService();
        readonly WoaService woaService = new WoaService();

        #endregion

        #region DBs

        [EditorBrowsable(EditorBrowsableState.Never)] string connectionString;
        protected CourtDatesDB courtDatesDB;
        FrameDB frameDB;
        NoAOGDB noAOGDB;
        protected PunchStatistics punch;
        protected RepresentationDB representationDB;
        protected ServiceDB serviceDB;
        protected ServiceDB serviceDB_Queue = new ServiceDB(ConfigurationManager.ConnectionStrings["QueueConnectionString"].ConnectionString);
        VehicleColourDB vehicleColourDB;
        VehicleMakeDB vehicleMakeDB;
        VehicleTypeDB vehicleTypeDB;

        public string ConnectionString
        {
            get { return this.connectionString; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value)
                    && value.Equals2(this.connectionString))
                    return;
                this.connectionString = value;

                this.representationDB = null;
                this.representationDB = new RepresentationDB(value);

                this.serviceDB = null;
                this.serviceDB = new ServiceDB(value);

                this.punch = null;
                this.punch = new PunchStatistics(value);

                this.vehicleMakeDB = null;
                this.vehicleMakeDB = new VehicleMakeDB(value);

                this.vehicleTypeDB = null;
                this.vehicleTypeDB = new VehicleTypeDB(value);

                this.vehicleColourDB = null;
                this.vehicleColourDB = new VehicleColourDB(value);

                this.noAOGDB = null;
                this.noAOGDB = new NoAOGDB(value);

                this.frameDB = null;
                this.frameDB = new FrameDB(value);

                this.courtDatesDB = null;
                this.courtDatesDB = new CourtDatesDB(value);

                Roles.ConnectionString = value;
                Offences.ConnectionString = value;
            }
        }

        #endregion

        #region Elements

        #region datetimes

        public static readonly DateTime DateTimeMax = new DateTime(2079, 6, 06, 23, 59, 0);
        public static readonly DateTime DateTimeMin = new DateTime(1900, 1, 1, 0, 0, 0);
        public static DateTime DateTimeNow { get { return DateTime.Now; } }

        #endregion

        #region AutIntNo

        [EditorBrowsable(EditorBrowsableState.Never)] int autIntNo;
        public int AutIntNo
        {
            get { return GetAutIntNo(this.autIntNo); }
            set
            {
                this.autIntNo = value;
                Rules.AutIntNo = value;
            }
        }

        int GetAutIntNo(int autIntNo)
        {
            int num;
            return autIntNo <= 0
                && int.TryParse(Convert.ToString(HttpContext.Current.Session["autIntNo"]), out num)
                ? num : autIntNo;
        }

        #endregion

        #region Rules

        [EditorBrowsable(EditorBrowsableState.Never)] RepRules rules;
        public RepRules Rules
        {
            get { return this.rules ?? (this.rules = new RepRules()); }
        }

        #endregion

        #region Roles

        [EditorBrowsable(EditorBrowsableState.Never)] RepRoles roles;
        [EditorBrowsable(EditorBrowsableState.Never)] UserLoginInfo userInfo;
        public UserLoginInfo UserInfo
        {
            get { return this.userInfo; }
            set
            {
                this.userInfo = value;
                if (value != null)
                    Roles.UserIntNo = value.UserIntNo;
            }
        }

        public RepRoles Roles
        {
            get { return this.roles ?? (this.roles = new RepRoles()); }
        }

        #endregion

        #region Offences

        [EditorBrowsable(EditorBrowsableState.Never)] RepOffences offences;
        public RepOffences Offences
        {
            get { return this.offences ?? (this.offences = new RepOffences()); }
        }

        #endregion

        #endregion

        #region Variables

        protected bool isGettingDetails;
        protected bool isNew;

        #endregion

        #region Package

        public RepAction RepAction { get; protected set; }
        //public Controller Controller { get; set; }
        public RepViewName ViewName { get; private set; }
        public RepControlStatus RepControlStatus { get; private set; }
        public string LastUser
        {
            get { return UserInfo != null ? UserInfo.UserName : string.Empty; }
        }
        public string Title { get; protected set; }

        public dynamic ViewBag { get; set; }

        #endregion

        #region Initialize Environment

        #region publics

        List<SelectListItem> GetAreaCodeList(string town)
        {
            var result = new List<SelectListItem>();
            town = string.IsNullOrWhiteSpace(town) ? "" : town.Trim();
            var paras = new[]
            {
                new SqlParameter("@AreaCode", "0"),
                new SqlParameter("@AreaDescr", town.Length <= 0 ? DBNull.Value : (object)town)
            };

            this.serviceDB.ExecuteReader("GetFilteredSuburbCodes", paras, dr =>
            {
                if (!dr.HasRows) return;

                var lookups = AreaCodeLookupCache.GetCacheLookupValue(this.cultureName);
                while (dr.Read())
                {
                    var areaCode = Helper.GetReaderValue<string>(dr, "AreaCode");
                    string value;
                    if (!lookups.TryGetValue(areaCode, out value)) continue;

                    result.Add(new SelectListItem
                    {
                        Value = areaCode,
                        Text = value
                    });
                }
            });

            return result.OrderBy(c => c.Text).ToList();
        }

        public MessageResult GetAreaCodeListResult(string town)
        {
            var codes = GetAreaCodeList(town);
            var value = codes.Count == 1 ? codes[0].Value : null;
            return new MessageResult
            {
                Body = new
                {
                    Codes = codes,
                    Value = value
                }
            };
        }

        #endregion

        #region SysParam

        [EditorBrowsable(EditorBrowsableState.Never)] int? delayHours;

        int GetDelayActionDate_InHours()
        {
            SysParam sysParam;
            return !this.delayHours.HasValue
                && (sysParam = this.sysParamService.GetBySpColumnName(Convert.ToString(SysParamList.DelayActionDate_InHours))) != null
                ? (this.delayHours = sysParam.SpIntegerValue).Value
                : this.delayHours.GetValueOrDefault();
        }

        #endregion

        #region Notice

        [EditorBrowsable(EditorBrowsableState.Never)] Notice noticeCache;

        protected Notice GetNotice(int notIntNo, bool force = false)
        {
            return notIntNo <= 0
                ? new Notice()
                : (!force && this.noticeCache != null && this.noticeCache.NotIntNo == notIntNo
                    ? this.noticeCache
                    : ((this.noticeCache = this.noticeService.GetByNotIntNo(notIntNo)) ?? new Notice()));
        }

        #endregion

        #region Charge

        protected long GetChargeRowVersion(bool isSummons, int chgIntNo, bool isReadOnly)
        {
            if (chgIntNo <= 0) return 0;

            byte[] rowVersion;
            if (!isSummons || isReadOnly)
            {
                var charge = this.chargeService.GetByChgIntNo(chgIntNo);
                if (charge == null) return 0;
                rowVersion = charge.RowVersion;
            }
            else
            {
                var charges = this.cscService.DeepLoadBySchIntNo(chgIntNo, false, DeepLoadType.IncludeChildren, new[] { typeof(Charge) });
                if (charges.IsNullOrEmpty()) return 0;
                rowVersion = charges[0].ChgIntNoSource.RowVersion;
            }
            return new ChargeModel { RowVersion = rowVersion }.RowVersion_Long;
        }

        #endregion

        #region Representation

        [EditorBrowsable(EditorBrowsableState.Never)] DAL.Entities.Representation representationCache;

        [EditorBrowsable(EditorBrowsableState.Never)] SummonsRepresentation sumRepresentationCache;

        DAL.Entities.Representation GetRepresentation(int repIntNo, bool force = false)
        {
            return repIntNo <= 0
                ? new DAL.Entities.Representation()
                : (!force && this.representationCache != null && this.representationCache.RepIntNo == repIntNo
                    ? this.representationCache
                    : ((this.representationCache = this.representationService.GetByRepIntNo(repIntNo)) ?? new DAL.Entities.Representation()));
        }

        protected SummonsRepresentation GetSummonsRepresentation(int srIntNo, bool force = false)
        {
            return srIntNo <= 0
                ? new SummonsRepresentation()
                : (!force && this.sumRepresentationCache != null && this.sumRepresentationCache.SrIntNo == srIntNo
                    ? this.sumRepresentationCache
                    : ((this.sumRepresentationCache = this.summonsRepresentationService.GetBySrIntNo(srIntNo)) ?? new SummonsRepresentation()));
        }

        #endregion

        #region WOA

        [EditorBrowsable(EditorBrowsableState.Never)] Woa woaCache;

        Woa GetWOA(int sumIntNo, bool force = false)
        {
            return sumIntNo <= 0
                ? new Woa()
                : (!force && this.woaCache != null && this.woaCache.SumIntNo == sumIntNo
                    ? this.woaCache
                    : ((this.woaCache = this.woaService.GetBySumIntNo(sumIntNo).OrderByDescending(w => w.WoaIntNo).FirstOrDefault()) ?? new Woa()));
        }

        #endregion

        #region CourtJudgementType

        [EditorBrowsable(EditorBrowsableState.Never)] CourtJudgementType courtJudgementTypeCache;

        CourtJudgementType GetCourtJudgementType(int cjtIntNo, bool force = false)
        {
            return cjtIntNo <= 0
                ? new CourtJudgementType()
                : (!force && this.courtJudgementTypeCache != null && this.courtJudgementTypeCache.CjtIntNo == cjtIntNo
                    ? this.courtJudgementTypeCache
                    : ((this.courtJudgementTypeCache = this.courtJudgementTypeService.GetByCjtIntNo(cjtIntNo)) ?? new CourtJudgementType()));
        }

        #endregion

        #region NoticeFrame

        [EditorBrowsable(EditorBrowsableState.Never)] NoticeFrame noticeFrameCache;

        NoticeFrame GetNoticeFrame(int notIntNo, bool force = false)
        {
            return notIntNo <= 0
                ? new NoticeFrame()
                : (!force && this.noticeFrameCache != null && this.noticeFrameCache.NotIntNo == notIntNo
                    ? this.noticeFrameCache
                    : ((this.noticeFrameCache = this.noticeFrameService.GetByNotIntNo(notIntNo).FirstOrDefault()) ?? new NoticeFrame()));
        }

        #endregion

        #region ViolationType

        [EditorBrowsable(EditorBrowsableState.Never)] string violationTypeCache;
        [EditorBrowsable(EditorBrowsableState.Never)] int vtNotIntNo;

        string GetViolationType(int notIntNo)
        {
            if (notIntNo <= 0)
                return string.Empty;

            if (!string.IsNullOrWhiteSpace(this.violationTypeCache) && notIntNo == this.vtNotIntNo)
                return this.violationTypeCache;

            NoticeFrame noticeFrame;
            if ((noticeFrame = GetNoticeFrame(notIntNo)).FrameIntNo <= 0)
                return string.Empty;

            this.violationTypeCache = this.frameDB.GetViolationType(noticeFrame.FrameIntNo);
            this.vtNotIntNo = noticeFrame.NotIntNo;
            return this.violationTypeCache;
        }

        #endregion

        #endregion

        #region Checkings

        protected bool CheckModel<T>(ref T model, bool checkNull = false)
            where T : MessageResult, new()
        {
            if (model == null)
            {
                model = new T();
                if (checkNull)
                {
                    model.AddError(new ArgumentException2("model", model).Message);
                    return false;
                }
            }
            return model.IsSuccessful;
        }

        protected bool CheckParameters<T>(ref T model,
            int? autIntNo = null,
            string ticketNumber = Null,
            int? chgIntNo = null,
            int? repIntNo = null,
            long? chgRowVersion = null)
            where T : MessageResult, new()
        {
            CheckModel(ref model);

            if (autIntNo.HasValue && autIntNo <= 0)
                model.AddError(new ArgumentException2("autIntNo", autIntNo).Message);
            if (!ticketNumber.Equals2(Null) && string.IsNullOrWhiteSpace(ticketNumber))
                model.AddError(new ArgumentException2("ticketNumber", ticketNumber).Message);
            if (chgIntNo.HasValue && chgIntNo <= 0)
                model.AddError(new ArgumentException2("chgIntNo", chgIntNo).Message);
            if (chgRowVersion.HasValue && chgRowVersion <= 0)
                model.AddError(new ArgumentException2("chgRowVersion", chgRowVersion).Message);

            return model.IsSuccessful;
        }

        bool IsHandwrittenCorrection(int chgIntNo, bool isSummons, MessageResult model = null)
        {
            var failed = chgIntNo > 0 && this.representationDB.CheckIsHandwrittenCorrrection(chgIntNo, isSummons) == -11;
            if (failed && model != null)
                model.AddError(string.Format(!RepAction.Equals(RepAction.O)
                    ? RepresentationResx.ThisHasBeenCorrected : RepresentationResx.ThisHasBeenCorrected_COO,
                    isSummons ? "Summons" : "Notice"));
            return failed;
        }

        bool IsNoticeOrSummonsPaid(int chgIntNo, bool isSummons, MessageResult model = null)
        {
            var failed = chgIntNo > 0 && this.representationDB.CheckNoticeOrSummonsIsPaid(chgIntNo, isSummons) == -1;
            if (failed && model != null)
                model.AddError(string.Format(!RepAction.Equals(RepAction.O)
                    ? RepresentationResx.ThisHasBeenPaid : RepresentationResx.ThisHasBeenPaid_COO,
                    isSummons ? "Summons" : "Notice"));
            return failed;
        }

        protected bool IsHandwrittenCorrectionOrPaid(int chgIntNo, bool isSummons, MessageResult model = null)
        {
            return IsHandwrittenCorrection(chgIntNo, isSummons, model)
                || IsNoticeOrSummonsPaid(chgIntNo, isSummons, model);
        }

        bool CheckModelRowVersion(ref RepresentationModel model)
        {
            CheckModel(ref model);

            if (model.Charge.RowVersion_Long <= 0
                || model.Charge.RowVersion_Long != GetChargeRowVersion(model.IsSummons, model.ChgIntNo, model.Charge.IsReadOnly))
            {
                model.AddError(RepresentationResx.RowVersionChanged);
                return false;
            }
            return true;
        }

        bool CheckParameterRowVersion(long chgRowVersion, bool isSummons, int chgIntNo, bool isReadOnly, MessageResult model = null)
        {
            if (chgRowVersion <= 0
                || chgRowVersion != GetChargeRowVersion(isSummons, chgIntNo, isReadOnly))
            {
                if (model != null)
                    model.AddError(RepresentationResx.RowVersionChanged);
                return false;
            }
            return true;
        }

        protected bool CheckParameterModelRowVersion(long chgRowVersion, ref RepresentationModel model)
        {
            CheckModel(ref model);

            if (chgRowVersion <= 0
                || chgRowVersion != model.Charge.RowVersion_Long)
            {
                model.AddError(RepresentationResx.RowVersionChanged);
                return false;
            }
            return true;
        }

        #endregion

        #region Operations

        protected void SwitchRevAmount(ref RepresentationModel model, bool isPostBack)
        {
            CheckModel(ref model);

            if (!model.Charge.ChgNoAOG.Equals2(Y)
                && !model.Notice.IsNoAOG)
                return;

            if (!isPostBack && model.RepRevisedFineAmount.Equals2(0))
                model.RepRevisedFineAmount = AmountNoAOG;
            else if (isPostBack && model.RepRevisedFineAmount.Equals2(AmountNoAOG))
                model.RepRevisedFineAmount = 0;
        }

        #region TargetCSCode

        // saving target cscode before creating rep
        protected bool SaveTargetCSCode(ref RepresentationModel model, Func<RepresentationModel, bool> newUpdateRepresentation)
        {
            if (!CheckModel(ref model)) return false;

            int? targetCSCode = null;
            if (model.RepIntNo == 0)
            {
                if (model.Notice.NoticeStatus.In(
                    CODE_First_Notice_Printed,
                    CODE_NOTICE_NEW_OFFENDER,
                    CODE_Second_Notice_Created))
                    targetCSCode = CODE_NOTICE_NEW_OFFENDER;
                else if (model.Notice.NoticeStatus.In(
                    CODE_Second_Notice_Printed,
                    CODE_Second_Notice_Posted,
                    CODE_NoAOG_Printed,
                    CODE_NoAOG_Posted))
                    targetCSCode = CODE_SUMMONS_REISSUE;
                else if (model.Notice.NoticeStatus.Between(
                    CODE_Officer_Error,
                    CODE_SUMMONS_REISSUE)
                    || model.Notice.NoticeStatus.In(
                        CODE_NoAOG,
                        CODE_No_Summons_Server_For_Area,
                        CODE_No_Available_Court_Dates))
                    targetCSCode = model.Notice.NoticeStatus;
            }

            if (!newUpdateRepresentation(model))
                return false;

            if (targetCSCode.HasValue)
                model.TargetCSCode = targetCSCode;

            return true;
        }

        Notice HandleTargetCSCode(ref RepresentationModel model, out bool pushGenerateSummons)
        {
            pushGenerateSummons = false;
            Notice notice;
            if (!CheckModel(ref model)
                || (notice = GetNotice(model.Notice.NotIntNo, true)).NotIntNo <= 0)
                return new Notice();

            // only process TargetCSCode = NoticeStatus.
            if (!IsRegisterOrDecide()
                || !model.TargetCSCode.Equals2(notice.NoticeStatus))
                return notice;

            int type = 0, pfnIntNo = 0;
            switch (notice.NoticeStatus)
            {
                case CODE_NOTICE_NEW_OFFENDER:
                    ReassociationPrintFileNameNotice(model.AutIntNo, notice.NotIntNo, notice.Not2ndNoticePrintFile);
                    notice.Not2ndNoticePrintFile = null;
                    notice.NotPrint2ndNoticeDate = null;
                    notice.NotIssue2ndNoticeDate = null;
                    notice.Not2ndPaymentDate = null;
                    type = 1;
                    break;
                case CODE_NoAOG:
                    var noAogFile = string.Format("{0}_{1}_{2}",
                        !model.Notice.NotFilmTypeIsO ? "NAG" : "ASD_NAG",
                        model.AutCode, DateTimeNow.ToString(DateFormat_PrintFileName));
                    pfnIntNo = ReassociationPrintFileNameNotice(model.AutIntNo, notice.NotIntNo, notice.NotCiprusPrintReqName, noAogFile);
                    notice.NotCiprusPrintReqName = noAogFile;
                    notice.NotPrint1stNoticeDate = null;
                    notice.NotPaymentDate = null;
                    type = 2;
                    break;
                case CODE_SUMMONS_REISSUE:
                    type = 3;
                    break;
            }

            if (type.In(1, 2))
            {
                notice = this.noticeService.Save(notice);
                if (type == 1 && !PushQueue_Generate2ndNotice(notice.NotIntNo, model.AutCode, notice.NotOffenceDate, model)
                    || (type == 2 && !PushQueue_Print1stNotice(pfnIntNo, model.AutCode, notice.NotIntNo, notice.IsVideo, model)))
                    return new Notice();
            }

            pushGenerateSummons = type > 0;
            return notice;
        }

        int ReassociationPrintFileNameNotice(int autIntNo, int notIntNo, string oldPrintFileName = null, string newPrintFileName = null)
        {
            if (notIntNo <= 0) return 0;

            PrintFileName pfnEntity;
            PrintFileNameNotice pfnnEntity;

            // drop connection
            if (!string.IsNullOrWhiteSpace(oldPrintFileName))
            {
                if ((pfnEntity = this.printFileNameService.GetByPrintFileName(oldPrintFileName)) != null
                    && (pfnnEntity = this.printFileNameNoticeService.GetByNotIntNoPfnIntNo(notIntNo, pfnEntity.PfnIntNo)) != null)
                {
                    this.printFileNameNoticeService.Delete(pfnnEntity);
                }
            }

            // associate new connection
            if (!string.IsNullOrWhiteSpace(newPrintFileName))
            {
                if ((pfnEntity = this.printFileNameService.GetByPrintFileName(newPrintFileName)) == null)
                {
                    pfnEntity = new PrintFileName
                    {
                        AutIntNo = autIntNo,
                        PrintFileName = newPrintFileName,
                        DateAdded = DateTimeNow,
                        LastUser = LastUser
                    };
                    pfnEntity = this.printFileNameService.Save(pfnEntity);
                }

                if ((this.printFileNameNoticeService.GetByNotIntNoPfnIntNo(notIntNo, pfnEntity.PfnIntNo)) == null)
                {
                    pfnnEntity = new PrintFileNameNotice
                    {
                        NotIntNo = notIntNo,
                        PfnIntNo = pfnEntity.PfnIntNo,
                        LastUser = LastUser
                    };
                    this.printFileNameNoticeService.Save(pfnnEntity);
                }

                return pfnEntity.PfnIntNo;
            }

            return 0;
        }

        #endregion

        #region PrintRepLetter

        string GetPostRepLetterUrl(string ticketNo, int repIntNo, string letterType, string letterTo)
        {
            string tmsDomain;
            if (GetTMSDomainUrl == null
                || string.IsNullOrWhiteSpace(tmsDomain = GetTMSDomainUrl().Trim2()))
                return string.Empty;

            switch (letterType)
            {
                case RepDecision:
                    return string.Format("{0}/{1}?TicketNo={2}&RepIntNo={3}&LetterTo={4}",
                        tmsDomain, Representation_LetterViewerAspx, ticketNo, repIntNo, letterTo);

                case InsufficientDetails:
                    return string.Format("{0}/{1}?RepIntNo={2}&IsSummons={3}",
                        tmsDomain, RepresentationInsufficientDetails_LetterViewerAspx, repIntNo, letterTo.Equals2(LetterTo.S) ? "1" : "0");
            }

            return string.Empty;
        }

        protected MessageResult PostRepLetter(ref RepresentationModel model, bool isDecision)
        {
            CheckModel(ref model);

            var printRepLetter = new PrintRepLetter
            {
                RepIntNo = model.RepIntNo,
                LetterType = isDecision ? RepDecision : InsufficientDetails,
                LetterTo = model.IsSummons ? LetterTo.S : model.LetterTo
            };

            var isIBM = Rules.AR_6209.GetValueOrDefault();
            var fileNamePrefix = isDecision ? RepDecision_ : RepInsufficentDecision_;
            var repType = isDecision ? RepType.D : RepType.I;

            if (!isIBM && !Rules.AR_4120.GetValueOrDefault())
            {
                this.representationDB.RepresentationUpdateForPrintBatch(false, model.IsSummons, model.RepIntNo, model.AutIntNo, true, true, LastUser);

                model.Body = GetPostRepLetterUrl(model.NotTicketNo, model.RepIntNo, printRepLetter.LetterType, printRepLetter.LetterTo);
                return model;
            }

            if (!isIBM)
            {
                printRepLetter.PrintFileName = fileNamePrefix + model.AutCode + "_" + DateTimeNow.ToString(DATE_FORMAT);
                this.representationDB.RepresentationUpdateForPrintBatch(true, model.IsSummons, model.RepIntNo, model.AutIntNo, false, false, LastUser, printRepLetter.PrintFileName);
            }
            else
            {
                int result;
                string errorMsg;
                if ((result = this.representationDB.UpdateRepresentationForPrint(model.AutIntNo, model.RepIntNo, LastUser, repType, model.IsSummons, out errorMsg)) <= 0)
                {
                    model.AddError(string.Format(RepresentationResx.FailedInUpdatingRepresentationForPrint, result, errorMsg));
                    return model;
                }

                var fileName = !model.IsSummons
                    ? GetRepresentation(model.RepIntNo).RepPrintFileName
                    : GetSummonsRepresentation(model.RepIntNo).SrPrintFileName;
                if (string.IsNullOrWhiteSpace(fileName))
                {
                    model.AddError(RepresentationResx.UnableToGetPrintFileName, false);
                    return model;
                }
                printRepLetter.PrintFileName = fileName;
            }

            model.Body = printRepLetter;
            return model;
        }

        string PrintBatch(bool isYes, int autIntNo, string ticketNumber, int repIntNo, string letterType, string letterTo)
        {
            var isSummons = letterTo.Equals2(LetterTo.S);
            if (isYes)
                this.representationDB.RepresentationUpdateForPrintBatch(true, isSummons, repIntNo, autIntNo, true, true, LastUser);
            else
            {
                TransactionScope(() =>
                {
                    string printFileName = null;
                    var notIntNo = 0;
                    using (var dr = this.representationDB.GetRepPrintFileNameByRepIntNo(repIntNo, isSummons))
                    {
                        if (dr.Read())
                        {
                            printFileName = Helper.GetReaderValue<string>(dr, isSummons ? "SRPrintFileName" : "RepPrintFileName");
                            notIntNo = Helper.GetReaderValue<int>(dr, "NotIntNo");
                        }
                    }

                    if (string.IsNullOrWhiteSpace(printFileName))
                        throw new Exception(RepresentationResx.UnableToGetPrintFileName);

                    var needPush = false;
                    var pfnEntity = this.printFileNameService.GetByPrintFileName(printFileName);
                    if (pfnEntity == null)
                    {
                        pfnEntity = new PrintFileName
                        {
                            PrintFileName = printFileName,
                            AutIntNo = autIntNo,
                            DateAdded = DateTimeNow,
                            LastUser = LastUser
                        };
                        pfnEntity = this.printFileNameService.Save(pfnEntity);
                        needPush = true;
                    }

                    if (this.printFileNameNoticeService.GetByNotIntNoPfnIntNo(notIntNo, pfnEntity.PfnIntNo) == null)
                        this.printFileNameNoticeService.Save(new PrintFileNameNotice
                        {
                            PfnIntNo = pfnEntity.PfnIntNo,
                            NotIntNo = notIntNo,
                            LastUser = LastUser
                        });

                    if (needPush) PushQueue_PrintRepresentation(pfnEntity.PfnIntNo, autIntNo, letterType);

                    return true;
                }, ex => { throw ex; });
            }
            return GetPostRepLetterUrl(ticketNumber, repIntNo, letterType, letterTo);
        }

        void PrintLetter(int autIntNo, string fileName, bool onlyUpdateStatus)
        {
            if (onlyUpdateStatus)
            {
                using (var printEngine = new FreeStylePrintEngineForGenerateLatters(ConnectionString))
                {
                    using (var data = printEngine.CurrentReportDataService.LoadReportDataByPrintFiles(new List<string> { fileName }, autIntNo))
                    {
                        var userIsNull = GlobalVariates<User>.CurrentUser == null;
                        if (userIsNull)
                            GlobalVariates<User>.CurrentUser = new User { UserLoginName = LastUser };

                        printEngine.CurrentReportDataService.LastUser = LastUser;
                        printEngine.CurrentReportDataService.ModifyPrintDate(data);

                        if (userIsNull)
                            GlobalVariates<User>.CurrentUser = null;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Actions

        public MessageResult PrintRepLetter(int autIntNo, string ticketNumber,
            int repIntNo, bool isYes, string letterType, string letterTo, string printFileName)
        {
            var result = new MessageResult();
            if (!Rules.AR_6209.GetValueOrDefault())
                result.Body = PrintBatch(isYes, autIntNo, ticketNumber, repIntNo, letterType, letterTo);
            else
            {
                PrintLetter(autIntNo, printFileName, isYes);
                result.Body = GetPostRepLetterUrl(ticketNumber, repIntNo, letterType, letterTo);
            }
            return result;
        }

        #endregion

        #region Publics

        public bool TransactionScope(Func<bool> body, Action<Exception> onError = null)
        {
            if (body == null) return true;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    Timeout = TransactionManager.MaximumTimeout,
                    IsolationLevel = IsolationLevel.ReadCommitted
                }))
                {
                    if (body())
                    {
                        if (Transaction.Current != null
                            && Transaction.Current.TransactionInformation.Status == TransactionStatus.Active)
                            scope.Complete();
                        else
                        {
                            if (onError != null)
                                onError(new TransactionAbortedException());
                            return false;
                        }
                    }
                    else
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                if (onError != null)
                    onError(ex);
                ViewBag.Exception = ex;
                return false;
            }
        }

        #endregion

        #region Abstracts

        protected abstract bool BuildDetails(ref RepresentationModel model, int repCode);

        protected abstract bool RepresentationUpdate(ref RepresentationModel model);

        protected abstract bool RepresentationDecide(ref RepresentationModel model);

        protected abstract bool RepresentationReverse(ref RepresentationModel model);

        #endregion

        #region IDisposable

        bool isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~RepresentationBase()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.isDisposed) return;
            if (disposing)
            {
                GetTMSDomainUrl = null;

                Rules.Dispose();
                Roles.Dispose();
                Offences.Dispose();
            }
            this.isDisposed = true;
        }

        #endregion
    }
}
