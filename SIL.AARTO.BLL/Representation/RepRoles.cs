﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using SIL.AARTO.BLL.Extensions;
using SIL.ServiceBase;
using Stalberg.TMS.Data;

namespace SIL.AARTO.BLL.Representation
{
    public class RepRoles : ReadyForUse
    {
        const int RoleListType = 1;
        AARTOUserRoleDB userRoleDB;

        public RepRoles(string connStr = null)
        {
            if (!string.IsNullOrWhiteSpace(connStr))
                ConnectionString = connStr;
        }

        protected override object LoadValue<T>(PropertyInfo pi, int type)
        {
            return GetUserRoles(UserIntNo);
        }

        public List<string> GetUserRoles(int userIntNo)
        {
            return string.IsNullOrWhiteSpace(ConnectionString) || userIntNo <= 0
                ? new List<string>()
                : this.userRoleDB.GetAARTOUserRoleNameByUserID(userIntNo);
        }

        #region UserIntNo

        [EditorBrowsable(EditorBrowsableState.Never)] int userIntNo;
        public int UserIntNo
        {
            get { return this.userIntNo; }
            set
            {
                lock (SyncObj)
                {
                    if (value > 0 && value == this.userIntNo) return;
                    this.userIntNo = value;
                    OnTriggerChanged(RoleListType);
                }
            }
        }

        #endregion

        #region ConnectionString

        [EditorBrowsable(EditorBrowsableState.Never)] string connectionString;
        public string ConnectionString
        {
            get { return this.connectionString; }
            set
            {
                lock (SyncObj)
                {
                    if (!string.IsNullOrWhiteSpace(value)
                        && value.Equals2(this.connectionString))
                        return;
                    this.connectionString = value;

                    this.userRoleDB = null;
                    this.userRoleDB = new AARTOUserRoleDB(value);

                    ResetInconstants();
                }
            }
        }

        #endregion

        #region RoleList

        public List<string> RoleList
        {
            get { return OnPropertyGetting(() => RoleList, RoleListType); }
            set { OnPropertySetting(() => RoleList, RoleListType, value); }
        }

        #endregion
    }
}
