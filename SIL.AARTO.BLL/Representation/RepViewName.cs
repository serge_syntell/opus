﻿namespace SIL.AARTO.BLL.Representation
{
    public class RepViewName
    {
        public string Index { get; set; }
        public string Message { get; set; }
        public string Cache { get; set; }

        #region Search

        public string Searcher { get; set; }
        public string Search { get; set; }
        public string ChargeList { get; set; }
        public string RepresentationList { get; set; }

        #endregion

        #region Editor

        public string Editor { get; set; }
        public string EditorPart { get; set; }
        public string EditorPartChangeOfOffender { get; set; }
        public string EditorPartChangeOfRegistration { get; set; }

        #endregion
    }
}
