﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using SIL.AARTO.BLL.Representation.Model;

namespace SIL.AARTO.BLL.Representation
{
    public class RepControlStatus
    {
        public RepControlStatus()
        {
            Init();
        }

        #region Only type bool is allowed

        #region Editor Part

        /// <summary>
        ///     Default false
        /// </summary>
        public bool txtOffenceDescrEnabled { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool txtRecommendVisible { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool txtRevAmountEnabled { get; set; }

        /// <summary>
        ///     Default false
        /// </summary>
        public bool txtReversalReasonVisible { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool ddlRepOfficialEnabled { get; set; }

        /// <summary>
        ///     Default false
        /// </summary>
        public bool pnlAmountVisible { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool pnlRepOfficialVisible { get; set; }

        /// <summary>
        ///     Default false
        /// </summary>
        public bool btnSaveIsAdd { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool btnSaveVisible { get; set; }

        /// <summary>
        ///     Default false
        /// </summary>
        public bool btnDecideVisible { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool btnReverseVisible { get; set; }

        /// <summary>
        ///     Default false
        /// </summary>
        public bool rblCodesEnabled { get; set; }

        ///// <summary>
        /////     Default true
        ///// </summary>
        //public bool lnkShowDocumentsVisible { get; set; }

        #endregion

        #region Change Offender

        /// <summary>
        ///     Default true
        /// </summary>
        public bool chkChangeOffenderEnabled { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool chkChangeAddressEnabled { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool pnlChangeOfOffenderEnabled { get; set; }

        /// <summary>
        ///     Default false
        /// </summary>
        public bool btnInsufficientDetailsVisible { get; set; }

        #endregion

        #region Change Registration

        /// <summary>
        ///     Default true
        /// </summary>
        public bool chkChangeRegistrationEnabled { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool pnlChangeRegistrationEnabled { get; set; }

        /// <summary>
        ///     Default false
        /// </summary>
        public bool pnlChangeRegistrationVisible { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool pnlRegistrationEnabled { get; set; }

        /// <summary>
        ///     Default true
        /// </summary>
        public bool pnlRegistrationVisible { get; set; }

        #endregion

        #endregion

        void Init()
        {
            txtRecommendVisible = true;
            txtRevAmountEnabled = true;

            ddlRepOfficialEnabled = true;
            //pnlAmountVisible = true;
            pnlRepOfficialVisible = true;

            btnSaveVisible = true;
            btnReverseVisible = true;

            chkChangeOffenderEnabled = true;
            chkChangeAddressEnabled = true;
            pnlChangeOfOffenderEnabled = true;

            chkChangeRegistrationEnabled = true;
            pnlChangeRegistrationEnabled = true;
            pnlRegistrationEnabled = true;
            pnlRegistrationVisible = true;
        }

        public void Clear()
        {
            foreach (var pi in GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(pi => pi.CanWrite && pi.PropertyType == typeof(bool)))
            {
                pi.SetValue(this, false, null);
            }
            Init();
        }

        public IHtmlString HiddenFor<TModel, TKey>(HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TKey>> keySelector)
            where TModel : RepresentationModel
        {
            if (htmlHelper == null)
                throw new ArgumentNullException("htmlHelper");
            if (keySelector == null)
                throw new ArgumentNullException("keySelector");

            var source = typeof(TModel);
            var model = Expression.Parameter(source);
            var rcs = Expression.Property(model,
                source.GetProperty(((MemberExpression)keySelector.Body).Member.Name,
                    BindingFlags.Public | BindingFlags.Instance));
            var sb = new StringBuilder();
            foreach (var pi in GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(pi => pi.CanRead && pi.CanWrite && pi.PropertyType == typeof(bool)))
                sb.AppendLine(htmlHelper.HiddenFor(Expression.Lambda<Func<TModel, bool>>(Expression.Property(rcs, pi), model)).ToHtmlString());
            return htmlHelper.Raw(sb.ToString());
        }
    }
}
