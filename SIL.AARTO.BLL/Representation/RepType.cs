﻿namespace SIL.AARTO.BLL.Representation
{
    public class RepType
    {
        /// <summary>
        ///     One step rep
        /// </summary>
        public const string A = "A";
        /// <summary>
        ///     Normal rep (2 steps)
        /// </summary>
        public const string N = "N";
        /// <summary>
        ///     Reverse rep
        /// </summary>
        public const string R = "R";
        /// <summary>
        ///     Decide rep
        /// </summary>
        public const string D = "D";
        /// <summary>
        ///     Insufficient Details
        /// </summary>
        public const string I = "I";
    }

    public class LetterTo
    {
        public const string A = "A";
        public const string D = "D";
        public const string O = "O";
        public const string S = "S";
    }

    public class RegNoType
    {
        public const string D = "D";
        public const string N = "N";
        public const string I = "I";
        public const string L = "L";
    }

    public class IDType
    {
        public const string Type01 = "01";
        public const string Type02 = "02";
        public const string Type03 = "03";
        public const string Type04 = "04";
    }
}
