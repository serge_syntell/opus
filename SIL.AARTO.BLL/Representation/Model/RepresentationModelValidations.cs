﻿using System.Linq;
using System.Text.RegularExpressions;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation.Model
{
    public partial class RepresentationModel
    {
        const string RepOfficialNone = "[ None ]";

        public bool ValidateDecision(RepRules rules, out PunchStatisticsTranTypeList? pstType, out int chargeStatus)
        {
            CheckNoAOGOffenceInput(false);

            #region init status

            pstType = null;
            chargeStatus = Charge.ChargeStatus;
            var repCode = RepresentationCode.GetValueOrDefault(!IsSummons ? RepresentationBase.REPCODE_NONE : RepresentationBase.SUMMONS_REPCODE_NONE);
            var csStatus = RepresentationBase.CODE_REP_LOGGED_CHARGE;

            if (IsChangeOfOffender)
            {
                if (!IsSummons || repCode.Equals2(RepresentationBase.REPCODE_NONE))
                    repCode = RepresentationBase.REPCODE_CHANGEOFFENDER;
                else if (IsSummons || repCode.Equals2(RepresentationBase.SUMMONS_REPCODE_NONE))
                    repCode = RepresentationBase.SUMMONS_REPCODE_CHANGEOFFENDER;
                RepDetails = RepresentationResx.ChangeOffender;
            }
            else if (IsChangeAddress)
            {
                repCode = RepresentationBase.SUMMONS_REPCODE_ADDRESS;
                RepDetails = RepresentationResx.ChangeAddress;
            }

            switch (repCode)
            {
                case RepresentationBase.REPCODE_CASEWITHDRAWNORESUMMONS:
                case RepresentationBase.REPCODE_WITHDRAW:
                case RepresentationBase.SUMMONS_REPCODE_WITHDRAW:
                    csStatus = repCode.Equals2(RepresentationBase.REPCODE_WITHDRAW)
                        ? RepresentationBase.CODE_CASE_CANCELLED
                        : RepresentationBase.CODE_SUMMONS_WITHDRAWN_CHARGE;
                    pstType = PunchStatisticsTranTypeList.RepresentationResultWithdrawn;
                    break;
                case RepresentationBase.REPCODE_REDUCTION:
                case RepresentationBase.SUMMONS_REPCODE_REDUCTION:
                    pstType = PunchStatisticsTranTypeList.RepresentationResultFineReduced;
                    break;
                case RepresentationBase.REPCODE_NOCHANGE:
                case RepresentationBase.SUMMONS_REPCODE_NOCHANGE:
                    if (repCode.Equals2(RepresentationBase.SUMMONS_REPCODE_NOCHANGE))
                        csStatus = Charge.ChargeStatus;
                    pstType = PunchStatisticsTranTypeList.RepresentationResultProceed;
                    break;
                case RepresentationBase.REPCODE_CHANGEOFFENDER:
                case RepresentationBase.SUMMONS_REPCODE_CHANGEOFFENDER:
                case RepresentationBase.SUMMONS_REPCODE_ADDRESS:
                    if (repCode.Equals2(RepresentationBase.SUMMONS_REPCODE_CHANGEOFFENDER))
                        csStatus = RepresentationBase.CODE_SUMMONS_REISSUE;
                    pstType = PunchStatisticsTranTypeList.ChangeOfOffender;
                    break;
                case RepresentationBase.SUMMONS_REPCODE_REISSUE:
                    pstType = PunchStatisticsTranTypeList.RepresentationReIssue;
                    break;
            }

            if (IsChangeOfRegNo)
            {
                if (NewRegNoDetailsChecked)
                {
                    if (!IsSummons)
                    {
                        repCode = RepresentationBase.REPCODE_WITHDRAW;
                        csStatus = RepresentationBase.CODE_CASE_CANCELLED;
                    }
                    else
                    {
                        repCode = RepresentationBase.SUMMONS_REPCODE_WITHDRAW;
                        csStatus = RepresentationBase.CODE_SUMMONS_WITHDRAWN_CHARGE;
                    }
                }
                else
                {
                    if (!IsSummons)
                    {
                        repCode = RepresentationBase.REPCODE_NOCHANGE;
                        csStatus = Charge.ChargeStatus;
                    }
                    else
                    {
                        repCode = RepresentationBase.SUMMONS_REPCODE_CHANGEOFFENDER;
                        csStatus = RepresentationBase.CODE_NOTICE_NEW_OFFENDER;
                    }
                }
                RepDetails = RepresentationResx.ChangeRegistration;
            }

            #endregion

            if (repCode.In(RepresentationBase.REPCODE_NONE,
                RepresentationBase.SUMMONS_REPCODE_NONE))
                AddError(RepresentationResx.YouMustSelectARepresentationCode);

            if (!RepDate.HasValue || RepDate.Value > RepresentationBase.DateTimeNow)
                AddError(!RepAction.Equals2(Representation.RepAction.O)
                    ? RepresentationResx.PleaseEnterAValidDateForTheRepresentation
                    : RepresentationResx.PleaseEnterAValidDateForTheRepresentation_COO);

            if (!RepAction.Equals2(Representation.RepAction.O) && !rules.AR_4533.GetValueOrDefault())
            {
                if (string.IsNullOrWhiteSpace(RepDetails))
                    AddError(RepresentationResx.YouMustSupplyTheDetailsOfTheRepresentation);

                if (string.IsNullOrWhiteSpace(RepRecommend))
                    AddError(RepresentationResx.PleaseCompleteTheRecommendationForTheRepresentation);
            }

            if (!RepActionName.Equals2(Representation.RepAction.RegisterName))
            {
                if ((string.IsNullOrWhiteSpace(RepOfficial) || RepOfficial.Equals2(RepOfficialNone))
                    && (!IsChangeOfOffender && !IsChangeAddress
                        && !RepActionName.Equals2(Representation.RepAction.PresentationOfDocumentName)
                        && !rules.AR_4533.GetValueOrDefault()))
                    AddError(!RepAction.Equals2(Representation.RepAction.O)
                        ? RepresentationResx.YouMustSupplyTheNameOfTheOfficialDecidingTheRepresentation
                        : RepresentationResx.YouMustSupplyTheNameOfTheOfficialDecidingTheRepresentation_COO);
            }

            if (RepActionName.Equals2(Representation.RepAction.DecideName))
            {
                if (RepRevisedFineAmount <= 0)
                    AddError(RepresentationResx.YouCannotAmendTheFineAmountToZeroOrLess);
                else
                {
                    if (RepRevisedFineAmount > RepCurrentFineAmount)
                        AddError(RepresentationResx.TheAmountEnteredIsMoreThanTheOriginalFineAmountYouCannotIncreaseTheFine);

                    if (repCode.In(RepresentationBase.REPCODE_REDUCTION,
                        RepresentationBase.SUMMONS_REPCODE_REDUCTION)
                        && RepRevisedFineAmount.Equals2(RepCurrentFineAmount))
                        AddError(RepresentationResx.YouHaveSelectedToReduceTheFineAmountButTheRevisedAmountIsEqualToTheOriginalFineAmount);
                }
            }

            // Set the new amount to zero if it is a no re-summons
            if (repCode.In(RepresentationBase.REPCODE_CASEWITHDRAWNORESUMMONS,
                RepresentationBase.REPCODE_WITHDRAW,
                RepresentationBase.SUMMONS_REPCODE_WITHDRAW)
                || (Notice.IsNoAOG && RepRevisedFineAmount.Equals2(RepresentationBase.AmountNoAOG)))
                RepRevisedFineAmount = 0;

            #region Driver

            if (!IsChangeOfOffender
                && !repCode.Equals2(RepresentationBase.SUMMONS_REPCODE_ADDRESS))
            {
                Driver = null;
            }
            else
            {
                Driver.Trim();
                //2015-01-09 Heidi changed for read only fields should not be validated when change of address is done(bontq1785)
                if (!IsChangeAddress) 
                {
                    if (string.IsNullOrWhiteSpace(Driver.DrvSurname))
                        AddError(RepresentationResx.YouMustSupplyTheNewDriverSurname);

                    if (!rules.AR_4532.GetValueOrDefault()
                        && (string.IsNullOrWhiteSpace(Driver.DrvForeNames)
                            || Driver.DrvForeNames.Equals2(Driver.DrvInitials)
                            || Driver.DrvForeNames.Split(' ').Any(n => n.Length < 3)
                            || !new Regex(@"^[a-zA-Z- ]+$").IsMatch(Driver.DrvForeNames)))
                        AddError(RepresentationResx.YouMustEnterValidForenamesInitialsAreNotAllowed);

                    if (string.IsNullOrWhiteSpace(Driver.DrvInitials))
                        AddError(RepresentationResx.YouNeedToSupplyTheNewDriverInitials);

                    if (!rules.AR_4531.GetValueOrDefault())
                    {
                        long numLong;
                        if ((string.IsNullOrWhiteSpace(Driver.DrvIDNumber = Driver.DrvIDNumber.Trim2())
                            && string.IsNullOrWhiteSpace(Driver.DrvPassport = Driver.DrvPassport.Trim2()))
                            || (Driver.DrvIDNumber.Length2() > 0
                                && (Driver.DrvIDNumber.Length2() != 13
                                    || !long.TryParse(Driver.DrvIDNumber, out numLong))))
                            AddError(RepresentationResx.YouNeedToSupplyAValidIDNumber);

                        if (string.IsNullOrWhiteSpace(Driver.DrvAge)
                            && (!string.IsNullOrWhiteSpace(Driver.DrvIDNumber) || string.IsNullOrWhiteSpace(Driver.DrvPassport)))
                            AddError(RepresentationResx.YouMustSupplyTheNewDriverAge);
                    }

                    if (string.IsNullOrWhiteSpace(Driver.DrvNationality))
                        AddError(RepresentationResx.YouNeedToSupplyTheNewDriverNationality);
                }

                if (string.IsNullOrWhiteSpace(Driver.DrvPOAdd1))
                    AddError(RepresentationResx.YouNeedToSupplyAPostalAddressForTheNewDriver);

                if (0.AllEquals(
                    Driver.DrvPOAdd2.Length2(0),
                    Driver.DrvPOAdd3.Length2(0),
                    Driver.DrvPOAdd4.Length2(0),
                    Driver.DrvPOAdd5.Length2(0)))
                    AddError(RepresentationResx.YouNeedToSupplyTheTownCityForTheNewDriverPostalAddress);

                if (string.IsNullOrWhiteSpace(Driver.DrvPOCode))
                    AddError(RepresentationResx.YouMustSupplyTheNewDriverPostalCode);

                if (string.IsNullOrWhiteSpace(Driver.DrvStAdd1))
                    AddError(RepresentationResx.YouNeedToSupplyTheNewDriverStreetAddress);

                if (0.AllEquals(
                    Driver.DrvStAdd2.Length2(0),
                    Driver.DrvStAdd3.Length2(0),
                    Driver.DrvStAdd4.Length2(0)))
                    AddError(RepresentationResx.YouNeedToSupplyTheNewDriverStreetTownCity);

                if (string.IsNullOrWhiteSpace(Driver.DrvStCode))
                    AddError(RepresentationResx.YouMustSupplyTheNewDriverStreetAreaCode);
            }

            #endregion

            #region Registration

            if (IsChangeOfRegNo)
            {
                var start = errorList.Count;

                if (string.IsNullOrWhiteSpace(ChangeOfRegNoType)
                    || ChangeOfRegNoType.Equals2(RegNoType.N))
                    AddError(RepresentationResx.YouNeedToSelectAReasonForChangeOfRegistration);

                if (!NewRegNoDetailsChecked)
                {
                    if (string.IsNullOrWhiteSpace(Notice.NotRegNo))
                        AddError(RepresentationResx.YouNeedToEnterTheChangedRegistrationNo);

                    if (NewVMIntNo <= 0)
                        AddError(RepresentationResx.YouNeedToSelectAVehicleMake);

                    if (NewVTIntNo <= 0)
                        AddError(RepresentationResx.YouNeedToSelectAVehicleType);
                }

                if (start != errorList.Count)
                    errorList.Insert(start, RepresentationResx.YouNeedToFixTheFollowingProblemsBeforeTheNewRegistrationDetailsCanBeSaved);
            }

            #endregion

            if (repCode.In(
                RepresentationBase.REPCODE_CHANGEOFFENDER,
                RepresentationBase.SUMMONS_REPCODE_CHANGEOFFENDER,
                RepresentationBase.SUMMONS_REPCODE_ADDRESS)
                || IsChangeOfOffender)
                MIPASource = IsChangeOfRegNo ? RepresentationBase.RegNoRep : RepresentationBase.Represent;

            if (IsSuccessful)
            {
                RepresentationCode = repCode;
                Charge.ChargeStatus = csStatus;
            }

            return IsSuccessful;
        }

        public bool ValidateReversal(bool isInsufficientDetails)
        {
            if (isInsufficientDetails)
            {
                RepDetails = RepresentationResx.InsufficientDetails;
                RepReverseReason = RepresentationResx.InsufficentDetailsProvidedForNewOffender;
            }

            if (string.IsNullOrWhiteSpace(RepReverseReason))
                AddError(RepresentationResx.YouNeedToCaptureTheReasonForReversalOfTheRepresentation);

            return true;
        }

        public bool ValidateUpdate(RepRules rules)
        {
            CheckNoAOGOffenceInput(true);

            if (string.IsNullOrWhiteSpace(RepOffenderName))
                AddError(RepresentationResx.YouMustFillInTheOffenderName);

            if (!RepDate.HasValue || RepDate.Value > RepresentationBase.DateTimeNow)
                AddError(RepresentationResx.TheDateForTheRepresentationDoesNotAppearToBeValid);

            if (string.IsNullOrWhiteSpace(RepDetails) && !rules.AR_4533.GetValueOrDefault())
                AddError(RepresentationResx.YouNeedToCaptureTheDetailsOfTheRepresentation);

            if (IsSuccessful)
            {
                if (!IsSummons)
                    Charge.ChargeStatus = RepresentationBase.CODE_REP_LOGGED_CHARGE;
                else if (Charge.ChargeStatus == 0
                    || !Charge.ChargeStatus.Between(RepresentationBase.Code_Summons_Generated, RepresentationBase.CODE_Case_Number_Generated - 1))
                    Charge.ChargeStatus = RepresentationBase.CODE_REP_LOGGED_SUMMONS;
            }

            return IsSuccessful;
        }

        void CheckNoAOGOffenceInput(bool onlyCheckNoAOG)
        {
            if (!RepActionName.In(Representation.RepAction.ChangeOfOffenderName,
                Representation.RepAction.PresentationOfDocumentName)
                && Notice.BlockNoAOG)
            {
                if (onlyCheckNoAOG)
                    AddError(string.Format(RepresentationResx.NoAOG_LogRepresentationNotAllowed, NotTicketNo));
                else if (!RepRevisedFineAmount.Equals2(RepRevisedFineAmountCache)
                    || RepresentationCode.In(RepresentationBase.REPCODE_REDUCTION,
                        RepresentationBase.SUMMONS_REPCODE_REDUCTION))
                    AddError(RepresentationResx.NoAOG_RepresentationNotAllowed);
            }
        }

        void CheckChangeOfRegNo()
        {
            if (IsChangeOfRegNo) return;
            ChangeOfRegNoType = string.Empty;
            NewRegNoDetails = N;
            ChangeOfRegNo = N;
            Notice.NotRegNo = string.Empty;
            NewVMIntNo = 0;
            NewVTIntNo = 0;
            NotVehicleColour = string.Empty;
            NewVehicleColourDescr = string.Empty;
        }
    }
}
