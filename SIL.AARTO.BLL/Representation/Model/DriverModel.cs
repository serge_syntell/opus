﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using SIL.AARTO.BLL.Extensions;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Representation.Model
{
    public class DriverModel : DriverDetails
    {
        public virtual string DrvPassport { get; set; }

        public virtual string GetPoAddress(string value = "")
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(DrvPOAdd1))
                sb.AppendLine(DrvPOAdd1.Trim2());
            if (!string.IsNullOrWhiteSpace(DrvPOAdd2))
                sb.AppendLine(DrvPOAdd2.Trim2());
            if (!string.IsNullOrWhiteSpace(DrvPOAdd3))
                sb.AppendLine(DrvPOAdd3.Trim2());
            if (!string.IsNullOrWhiteSpace(DrvPOAdd4))
                sb.AppendLine(DrvPOAdd4.Trim2());
            if (!string.IsNullOrWhiteSpace(DrvPOAdd5))
                sb.AppendLine(DrvPOAdd5.Trim2());
            if (!string.IsNullOrWhiteSpace(DrvPOCode))
                sb.Append(DrvPOCode.Trim2());
            var result = sb.ToString();
            if (string.IsNullOrWhiteSpace(result))
                result = value;
            return result;
        }

        public void Trim()
        {
            foreach (var pi in GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(pi => pi.CanRead && pi.CanWrite && pi.PropertyType == typeof(string)))
            {
                var value = pi.GetValue(this, null);
                if (value != null)
                    pi.SetValue(this, Convert.ToString(value).Trim2(), null);
            }
        }
    }
}
