﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Script.Serialization;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation.Model
{
    [Serializable]
    public class MessageResult
    {
        public MessageResult()
        {
            IsSuccessful = true;
        }

        public bool IsSuccessful { get; set; }

        #region Body

        [ScriptIgnore] [EditorBrowsable(EditorBrowsableState.Never)] object body;
        public object Body
        {
            get { return this.body; }
            set
            {
                if (this.body != null && value != null)
                    throw new InvalidOperationException("Message Body has been assigned already, you can not assign it again!");
                this.body = value;
            }
        }

        #endregion

        #region Message

        [ScriptIgnore] protected List<string> messageList = new List<string>();
        public string Message
        {
            get { return GetMessage(this.messageList); }
        }
        public bool HasMessage
        {
            get { return !this.messageList.IsNullOrEmpty(); }
        }

        public void AddMessage(string message)
        {
            if (!this.messageList.Contains(message))
                this.messageList.Add(message);
        }

        public void ClearMessage()
        {
            this.messageList.Clear();
        }

        #endregion

        #region error

        [ScriptIgnore] protected List<string> errorList = new List<string>();
        public string ErrorMessage
        {
            get { return GetMessage(this.errorList); }
        }
        public bool HasError
        {
            get { return !this.errorList.IsNullOrEmpty(); }
        }

        public void AddError(string error, bool setFailed = true)
        {
            if (!this.errorList.Contains(error))
                this.errorList.Add(error);
            if (setFailed) IsSuccessful = false;
        }

        public void ClearError()
        {
            this.errorList.Clear();
        }

        #endregion

        #region block

        [ScriptIgnore] protected List<string> blockMessageList = new List<string>();
        public string BlockMessage
        {
            get { return GetMessage(this.blockMessageList); }
        }
        public bool HasBlock
        {
            get { return !this.blockMessageList.IsNullOrEmpty(); }
        }

        public void AddBlockMessage(string blockMessage)
        {
            if (!this.blockMessageList.Contains(blockMessage))
                this.blockMessageList.Add(blockMessage);
        }

        public void ClearBlockMessage()
        {
            this.blockMessageList.Clear();
        }

        #endregion

        static string GetMessage(IEnumerable<string> msgs)
        {
            var msg = string.Join("<br/>", msgs);
            int index;
            if (!string.IsNullOrWhiteSpace(msg)
                && (index = msg.IndexOf("\"", StringComparison.OrdinalIgnoreCase)) >= 0)
            {
                var msgList = msg.ToCharArray(0, index).ToList();
                for (var i = index; i < msg.Length; i++)
                {
                    var c = msg[i];
                    if (c == '"' && msg[Math.Max(i - 1, 0)] != '\\')
                        msgList.Add('\\');
                    msgList.Add(c);
                }
                msg = new string(msgList.ToArray());
            }
            return msg;
        }

        public MessageResult GetMessageResult()
        {
            return new MessageResult
            {
                Body = Body,
                IsSuccessful = IsSuccessful,
                messageList = this.messageList,
                errorList = this.errorList,
                blockMessageList = this.blockMessageList
            };
        }

        public virtual void RefCopy(MessageResult parent)
        {
            if (parent != null)
            {
                if (IsSuccessful)
                    IsSuccessful = parent.IsSuccessful;

                var source = this.messageList.FindAll(m => !parent.messageList.Contains(m));
                this.messageList = parent.messageList;
                if (!source.IsNullOrEmpty()) this.messageList.AddRange(source);

                source = this.errorList.FindAll(m => !parent.messageList.Contains(m));
                this.errorList = parent.errorList;
                if (!source.IsNullOrEmpty()) this.errorList.AddRange(source);

                source = this.blockMessageList.FindAll(m => !parent.messageList.Contains(m));
                this.blockMessageList = parent.blockMessageList;
                if (!source.IsNullOrEmpty()) this.blockMessageList.AddRange(source);
            }
        }
    }
}
