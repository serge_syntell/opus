﻿using SIL.AARTO.Web.Resource.Representation;

namespace SIL.AARTO.BLL.Representation.Model
{
    public class PrintRepLetter
    {
        public int RepIntNo { get; set; }
        public string LetterType { get; set; }
        public string LetterTo { get; set; }
        public string PrintFileName { get; set; }

        public string Confirm { get { return RepresentationResx.PostRepLetterDescription; } }
    }
}
