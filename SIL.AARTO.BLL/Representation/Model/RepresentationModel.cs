﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation.Model
{
    public partial class RepresentationModel : RepModelBase
    {
        #region Editor

        [EditorBrowsable(EditorBrowsableState.Never)] bool hasUndecidedRep;
        [EditorBrowsable(EditorBrowsableState.Never)] bool isLastRep;
        [EditorBrowsable(EditorBrowsableState.Never)] bool? isPod;
        [EditorBrowsable(EditorBrowsableState.Never)] string lockedFlag;
        [EditorBrowsable(EditorBrowsableState.Never)] string mipaSource;
        [EditorBrowsable(EditorBrowsableState.Never)] string rcDescr;
        [EditorBrowsable(EditorBrowsableState.Never)] int repIntNo;
        [EditorBrowsable(EditorBrowsableState.Never)] string repOffenderTelNo;
        [EditorBrowsable(EditorBrowsableState.Never)] string repType;
        [EditorBrowsable(EditorBrowsableState.Never)] int? targetCSCode;
        [EditorBrowsable(EditorBrowsableState.Never)]Dictionary<int, string> withdrawReasons = new Dictionary<int,string>();

        public virtual int RepIntNo
        {
            get { return this.repIntNo; }
            set { PropertyChanaged(ref this.repIntNo, value, () => RepIntNo); }
        }
        public virtual int ChgIntNo
        {
            get { return Charge.ChgIntNo; }
            set { Charge.ChgIntNo = value; }
        }
        public virtual bool IsLastRep
        {
            get { return this.isLastRep; }
            set { PropertyChanaged(ref this.isLastRep, value, () => IsLastRep); }
        }
        public virtual bool? IsPOD
        {
            get { return this.isPod; }
            set { PropertyChanaged(ref this.isPod, value, () => IsPOD); }
        }
        public virtual string MIPASource
        {
            get { return this.mipaSource.Trim2(); }
            set { PropertyChanaged(ref this.mipaSource, value, () => MIPASource); }
        }
        public virtual string RCDescr
        {
            get { return this.rcDescr.Trim2(); }
            set { PropertyChanaged(ref this.rcDescr, value, () => RCDescr); }
        }
        public virtual string RepOffenderTelNo
        {
            get { return this.repOffenderTelNo.Trim2(); }
            set { PropertyChanaged(ref this.repOffenderTelNo, value, () => RepOffenderTelNo); }
        }
        public virtual string RepType
        {
            get { return this.repType.Trim2(); }
            set { PropertyChanaged(ref this.repType, value, () => RepType); }
        }
        public virtual string LockedFlag
        {
            get { return this.lockedFlag.Trim2(); }
            set { PropertyChanaged(ref this.lockedFlag, value, () => LockedFlag); }
        }
        public virtual bool IsLocked
        {
            get { return LockedFlag.Equals2(Y); }
            set { LockedFlag = value ? Y : N; }
        }
        public virtual int? TargetCSCode
        {
            get { return this.targetCSCode; }
            set { PropertyChanaged(ref this.targetCSCode, value, () => TargetCSCode); }
        }
        public bool HasUndecidedRep
        {
            get { return this.hasUndecidedRep; }
            set { PropertyChanaged(ref this.hasUndecidedRep, value, () => HasUndecidedRep); }
        }

        public Dictionary<int, string> WithdrawalReasons
        {
            get
            {
                return withdrawReasons;
            }
            set
            {
                withdrawReasons = value;
            }
        }

        #region Editor Part

        [EditorBrowsable(EditorBrowsableState.Never)] string fullName;
        [EditorBrowsable(EditorBrowsableState.Never)] List<SelectListItem> officialList;
        [EditorBrowsable(EditorBrowsableState.Never)] string repCSDescr;
        [EditorBrowsable(EditorBrowsableState.Never)] List<SelectListItem> repCodeList;
        [EditorBrowsable(EditorBrowsableState.Never)] decimal repCurrentFineAmount;
        [EditorBrowsable(EditorBrowsableState.Never)] DateTime? repDate;
        [EditorBrowsable(EditorBrowsableState.Never)] string repDetails;
        [EditorBrowsable(EditorBrowsableState.Never)] string repOffenderAddress;
        [EditorBrowsable(EditorBrowsableState.Never)] string repOffenderName;
        [EditorBrowsable(EditorBrowsableState.Never)] string repOfficial;
        [EditorBrowsable(EditorBrowsableState.Never)] string repRecommend;
        [EditorBrowsable(EditorBrowsableState.Never)] DateTime? repRecommendDate;
        [EditorBrowsable(EditorBrowsableState.Never)] string repReverseReason;
        [EditorBrowsable(EditorBrowsableState.Never)] decimal repRevisedFineAmount;
        [EditorBrowsable(EditorBrowsableState.Never)] decimal repRevisedFineAmountCache;
        [EditorBrowsable(EditorBrowsableState.Never)] int? representationCode;
        [EditorBrowsable(EditorBrowsableState.Never)] int? withdrawalReasonCode;
        [EditorBrowsable ( EditorBrowsableState.Never )] string withdrawalReasonDescription;
        [EditorBrowsable ( EditorBrowsableState.Never )]string withdrawalReasonName;
     
       


        public virtual string RepCSDescr
        {
            get { return this.repCSDescr.Trim2(); }
            set { PropertyChanaged(ref this.repCSDescr, value, () => RepCSDescr); }
        }
        public virtual DateTime? RepDate
        {
            get { return this.repDate; }
            set { PropertyChanaged(ref this.repDate, value, () => RepDate); }
        }
        public virtual string RepDateStr
        {
            get { return RepDate.HasValue ? RepDate.Value.ToString("yyyy-MM-dd") : null; }
        }
        public virtual DateTime? RepRecommendDate
        {
            get { return this.repRecommendDate; }
            set { PropertyChanaged(ref this.repRecommendDate, value, () => RepRecommendDate); }
        }
        public virtual string RepOffenderName
        {
            get { return this.repOffenderName.Trim2(); }
            set { PropertyChanaged(ref this.repOffenderName, value, () => RepOffenderName); }
        }
        public virtual string FullName
        {
            get { return this.fullName.Trim2(); }
            set { PropertyChanaged(ref this.fullName, value, () => FullName); }
        }
        public virtual string RepOffenderAddress
        {
            get { return this.repOffenderAddress.Trim2(); }
            set { PropertyChanaged(ref this.repOffenderAddress, value, () => RepOffenderAddress); }
        }
        public virtual string RepDetails
        {
            get { return this.repDetails.Trim2(); }
            set { PropertyChanaged(ref this.repDetails, value, () => RepDetails); }
        }
        public virtual string RepRecommend
        {
            get { return this.repRecommend.Trim2(); }
            set { PropertyChanaged(ref this.repRecommend, value, () => RepRecommend); }
        }
        public virtual string RepReverseReason
        {
            get { return this.repReverseReason.Trim2(); }
            set { PropertyChanaged(ref this.repReverseReason, value, () => RepReverseReason); }
        }
        public virtual decimal RepCurrentFineAmount
        {
            get { return this.repCurrentFineAmount; }
            set { PropertyChanaged(ref this.repCurrentFineAmount, value, () => RepCurrentFineAmount); }
        }
        public virtual decimal RepRevisedFineAmount
        {
            get { return this.repRevisedFineAmount; }
            set { PropertyChanaged(ref this.repRevisedFineAmount, value, () => RepRevisedFineAmount); }
        }
        public virtual decimal RepRevisedFineAmountCache
        {
            get { return this.repRevisedFineAmountCache; }
            set { PropertyChanaged(ref this.repRevisedFineAmountCache, value, () => RepRevisedFineAmountCache); }
        }
        public virtual string RepOfficial
        {
            get { return this.repOfficial.Trim2(); }
            set { PropertyChanaged(ref this.repOfficial, value, () => RepOfficial); }
        }
        public List<SelectListItem> OfficialList
        {
            get { return this.officialList; }
            set { PropertyChanaged(ref this.officialList, value, () => OfficialList); }
        }
        public virtual int? RepresentationCode
        {
            get { return this.representationCode; }
            set { PropertyChanaged(ref this.representationCode, value, () => RepresentationCode); }
        }
        public List<SelectListItem> RepCodeList
        {
            get { return this.repCodeList; }
            set { PropertyChanaged(ref this.repCodeList, value, () => RepCodeList); }
        }

        public virtual int? RWRIntNo

        {
            get { return this.withdrawalReasonCode; }
            set { PropertyChanaged(ref this.withdrawalReasonCode, value, () => RWRIntNo); }
        }

        public virtual string RWRName
        {
            get { return this.withdrawalReasonName; }
            set { PropertyChanaged ( ref this.withdrawalReasonName, value, ( ) => RWRName ); }
        }

        public virtual string RWRDescription
        {
            get { return this.withdrawalReasonDescription; }
            set { PropertyChanaged ( ref this.withdrawalReasonDescription, value, ( ) => RWRDescription ); }
        }
        
        
        #endregion

        #region Change Registration

        [EditorBrowsable(EditorBrowsableState.Never)] string changeOfRegNo;
        [EditorBrowsable(EditorBrowsableState.Never)] string changeOfRegNoType = RegNoType.N;
        [EditorBrowsable(EditorBrowsableState.Never)] string newRegNoDetails;
        [EditorBrowsable(EditorBrowsableState.Never)] int newVMIntNo;
        [EditorBrowsable(EditorBrowsableState.Never)] int newVTIntNo;
        [EditorBrowsable(EditorBrowsableState.Never)] string newVehicleColourDescr;
        [EditorBrowsable(EditorBrowsableState.Never)] string notVehicleColour;
        [EditorBrowsable(EditorBrowsableState.Never)] List<SelectListItem> vehicleColourList;
        [EditorBrowsable(EditorBrowsableState.Never)] List<SelectListItem> vehicleMakesList;
        [EditorBrowsable(EditorBrowsableState.Never)] List<SelectListItem> vehicleTypeList;
        [EditorBrowsable(EditorBrowsableState.Never)] int vmIntNo;
        [EditorBrowsable(EditorBrowsableState.Never)] int vtIntNo;

        public virtual string ChangeOfRegNo
        {
            get { return this.changeOfRegNo.Trim2(); }
            set { PropertyChanaged(ref this.changeOfRegNo, value, () => ChangeOfRegNo); }
        }
        public virtual bool IsChangeOfRegNo
        {
            get { return ChangeOfRegNo.Equals2(Y); }
            set { ChangeOfRegNo = value ? Y : N; }
        }
        public virtual string ChangeOfRegNoType
        {
            get { return this.changeOfRegNoType.Trim2(); }
            set { PropertyChanaged(ref this.changeOfRegNoType, value, () => ChangeOfRegNoType); }
        }
        public virtual string NewRegNoDetails
        {
            get { return this.newRegNoDetails.Trim2(); }
            set { PropertyChanaged(ref this.newRegNoDetails, value, () => NewRegNoDetails); }
        }
        public virtual bool NewRegNoDetailsChecked
        {
            get { return !NewRegNoDetails.Equals2(Y); }
            set { NewRegNoDetails = value ? N : Y; }
        }
        public virtual int VMIntNo
        {
            get { return this.vmIntNo; }
            set { PropertyChanaged(ref this.vmIntNo, value, () => VMIntNo); }
        }
        public virtual int NewVMIntNo
        {
            get { return this.newVMIntNo; }
            set { PropertyChanaged(ref this.newVMIntNo, value, () => NewVMIntNo); }
        }
        public virtual int VTIntNo
        {
            get { return this.vtIntNo; }
            set { PropertyChanaged(ref this.vtIntNo, value, () => VTIntNo); }
        }
        public virtual int NewVTIntNo
        {
            get { return this.newVTIntNo; }
            set { PropertyChanaged(ref this.newVTIntNo, value, () => NewVTIntNo); }
        }
        public virtual string NotVehicleColour
        {
            get { return this.notVehicleColour.Trim2(); }
            set { PropertyChanaged(ref this.notVehicleColour, value, () => NotVehicleColour); }
        }
        public virtual string NewVehicleColourDescr
        {
            get { return this.newVehicleColourDescr.Trim2(); }
            set { PropertyChanaged(ref this.newVehicleColourDescr, value, () => NewVehicleColourDescr); }
        }
        public List<SelectListItem> VehicleMakesList
        {
            get { return this.vehicleMakesList; }
            set { PropertyChanaged(ref this.vehicleMakesList, value, () => VehicleMakesList); }
        }
        public List<SelectListItem> VehicleTypeList
        {
            get { return this.vehicleTypeList; }
            set { PropertyChanaged(ref this.vehicleTypeList, value, () => VehicleTypeList); }
        }
        public List<SelectListItem> VehicleColourList
        {
            get { return this.vehicleColourList; }
            set { PropertyChanaged(ref this.vehicleColourList, value, () => VehicleColourList); }
        }

        #endregion

        #region Change Offender

        [EditorBrowsable(EditorBrowsableState.Never)] string changeOfOffender;
        [EditorBrowsable(EditorBrowsableState.Never)] bool isChangeAddress;
        [EditorBrowsable(EditorBrowsableState.Never)] string letterTo = Representation.LetterTo.O;

        public virtual string ChangeOfOffender
        {
            get { return this.changeOfOffender.Trim2(); }
            set { PropertyChanaged(ref this.changeOfOffender, value, () => ChangeOfOffender); }
        }
        public virtual bool IsChangeOfOffender
        {
            get { return ChangeOfOffender.Equals2(Y); }
            set { ChangeOfOffender = value ? Y : N; }
        }
        public virtual bool IsChangeAddress
        {
            get { return this.isChangeAddress; }
            set { PropertyChanaged(ref this.isChangeAddress, value, () => IsChangeAddress); }
        }
        public virtual string LetterTo
        {
            get { return this.letterTo.Trim2(); }
            set { PropertyChanaged(ref this.letterTo, value, () => LetterTo); }
        }

        #endregion

        #endregion

        #region Parents

        [EditorBrowsable(EditorBrowsableState.Never)] ChargeModel charge;

        [ScriptIgnore]
        public ChargeModel Charge
        {
            get { return this.charge ?? (this.charge = new ChargeModel()); }
            set
            {
                PropertyChanaged(ref this.charge, value, () => Charge);
                RefCopy(value);
            }
        }

        public NoticeModel Notice
        {
            get { return Charge.Notice; }
        }

        public DriverModel Driver
        {
            get { return Charge.Notice.Driver; }
            set { Charge.Notice.Driver = value; }
        }

        #endregion

        public string WithdrawWarning
        {
            get
            {
                var notWithdraw = Notice.UnwithdrawnChargesCount <= 1;
                return !IsSummons
                    ? (notWithdraw
                        ? string.Format(RepresentationResx.WithdrawNoticeWarning, Notice.NotTicketNo)
                        : string.Format(RepresentationResx.WithdrawChargeWarning, Charge.ChgOffenceCode, Notice.NotTicketNo))
                    : (notWithdraw
                        ? string.Format(RepresentationResx.WithdrawSummonsWarning, Notice.NotTicketNo)
                        : string.Format(RepresentationResx.WithdrawSumChargeWarning, Charge.ChgOffenceCode, Notice.NotTicketNo));
            }
        }

        public void SetChangeAddress()
        {
            IsChangeAddress = !IsChangeOfOffender && RepresentationCode.Equals2(RepresentationBase.SUMMONS_REPCODE_ADDRESS);
        }

        public void Combines(RepresentationModel newSource, bool clearStatistics = true)
        {
            if (newSource == null) return;
            var oriMsg = GetMessageResult();

            newSource.ChangedPropertise.ForEach(name =>
            {
                var pi = GetType().GetProperty(name);
                if (pi == null || !pi.CanWrite) return;
                pi.SetValue(this, pi.GetValue(newSource, null), null);
            });

            if (clearStatistics) newSource.ClearChangedPropertise();

            RefCopy(oriMsg);
        }

        public RepresentationModel Next(Func<RepresentationModel, RepresentationModel> step)
        {
            return step == null || !IsSuccessful ? this : step(this);
        }
    }
}
