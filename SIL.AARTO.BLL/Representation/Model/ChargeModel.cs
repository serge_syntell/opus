﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Script.Serialization;
using SIL.AARTO.BLL.Extensions;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation.Model
{
    public class ChargeModel : RepModelBase
    {
        public ChargeModel()
        {
            RepresentationList = new List<RepresentationModel>();
        }

        public virtual string CSDescr { get; set; }
        public virtual int ChgIntNo { get; set; }
        public virtual bool ChgIsMain { get; set; }
        public virtual string ChgOffenceDescr { get; set; }

        [ScriptIgnore]
        public List<RepresentationModel> RepresentationList { get; set; }

        public virtual int? MainChargeID { get; set; }
        public virtual int Sequence { get; set; }
        public virtual string ChgOffenceCode { get; set; }
        public virtual DateTime? SumCourtDate { get; set; }
        public virtual string SumCaseNo { get; set; }
        public virtual int ChargeStatus { get; set; }
        public virtual int CJTIntNo { get; set; }

        public virtual bool CanAutoDoc { get; set; }

        public string IsMainCharge
        {
            get { return ChgIsMain ? "Main" : "Alternate"; }
        }
        public string SumCourtDateStr
        {
            get { return SumCourtDate.HasValue ? SumCourtDate.Value.ToString("yyyy-MM-dd") : null; }
        }
        public string ChargeNo
        {
            get { return string.Format("Charge {0}", Sequence); }
        }
        public bool HasCaseNo
        {
            get { return !string.IsNullOrWhiteSpace(SumCaseNo) && !SumCaseNo.Equals2("none"); }
        }

        public bool IsStagnantSummons
        {
            get { return IsSummons && !HasCaseNo && RepresentationBase.DateTimeNow.Date > SumCourtDate.GetValueOrDefault(RepresentationBase.DateTimeMax).Date; }
        }

        public virtual bool HasAlternate { get; set; }
        public virtual bool HasRep { get; set; }

        public virtual bool IsReadOnly { get; set; }

        #region RowVersion

        public virtual long RowVersion_Long { get; set; }
        public virtual byte[] RowVersion
        {
            get { return BitConverter.GetBytes(RowVersion_Long).Reverse().ToArray(); }
            set { RowVersion_Long = Convert.ToInt64(BitConverter.ToString(value).Replace("-", string.Empty), 16); }
        }

        #endregion

        #region Notice

        [EditorBrowsable(EditorBrowsableState.Never)] NoticeModel notice;
        [ScriptIgnore]
        public NoticeModel Notice
        {
            get { return this.notice ?? (this.notice = new NoticeModel()); }
            set
            {
                this.notice = null;
                this.notice = value;
                RefCopy(value);
            }
        }

        #endregion

        #region ChgNoAOG

        [EditorBrowsable(EditorBrowsableState.Never)] string chgNoAOG;
        public string ChgNoAOG
        {
            get { return this.chgNoAOG; }
            set { this.chgNoAOG = value.Trim2(); }
        }

        #endregion
    }
}
