﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Script.Serialization;
using SIL.AARTO.BLL.Extensions;
using SIL.ServiceBase;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Representation.Model
{
    public class NoticeModel : RepModelBase
    {
        [EditorBrowsable(EditorBrowsableState.Never)] string notRegNo;

        public NoticeModel()
        {
            ChargeList = new List<ChargeModel>();
        }

        //public virtual bool IsSummons { get; set; }
        public virtual int NotIntNo { get; set; }
        public virtual int SumIntNo { get; set; }
        public virtual string SummonsNo { get; set; }
        public virtual string NotLocDescr { get; set; }
        public virtual string OrigRegNo { get; set; }
        public virtual string NotRegNo
        {
            get { return this.notRegNo == null ? this.notRegNo : this.notRegNo.Trim2().ToUpper(); }
            set { this.notRegNo = value; }
        }
        public virtual int NoticeStatus { get; set; }
        public virtual string NotFilmType { get; set; }
        public virtual bool NotFilmTypeIsM
        {
            get { return NotFilmType.Equals2(NoticeFilmType.M.ToString()); }
            set { if (value) NotFilmType = NoticeFilmType.M.ToString(); }
        }
        public virtual bool NotFilmTypeIsH
        {
            get { return NotFilmType.Equals2(NoticeFilmType.H.ToString()); }
            set { if (value) NotFilmType = NoticeFilmType.H.ToString(); }
        }
        public virtual bool NotFilmTypeIsO
        {
            get { return NotFilmType.Equals2("O"); }
            set { if (value) NotFilmType = "O"; }
        }
        public virtual int UnwithdrawnChargesCount
        {
            get
            {
                return ChargeList.IsNullOrEmpty()
                    ? -1
                    : ChargeList.Count(c =>
                        !c.ChargeStatus.In(RepresentationBase.CODE_CASE_CANCELLED,
                            RepresentationBase.CODE_SUMMONS_WITHDRAWN_CHARGE));
            }
        }

        public virtual bool PendingNewOffender { get; set; }

        [ScriptIgnore]
        public List<ChargeModel> ChargeList { get; set; }

        public DriverModel Driver { get; set; }

        #region NoAOG

        public virtual bool IsNoAOG { get; set; }
        public virtual bool AllowNoAOG { get; set; }
        public bool BlockNoAOG
        {
            get { return IsSection35 ? !AllowSection35 : IsNoAOG && !AllowNoAOG; }
        }

        public virtual bool IsSection35 { get; set; }
        public virtual bool AllowSection35 { get; set; }

        #endregion
    }
}
