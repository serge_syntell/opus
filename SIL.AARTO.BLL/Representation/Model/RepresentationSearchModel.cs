﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace SIL.AARTO.BLL.Representation.Model
{
    public class RepresentationSearchModel : RepModelBase
    {
        public List<SelectListItem> AuthorityList { get; set; }
    }
}
