﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using SIL.AARTO.BLL.Extensions;

namespace SIL.AARTO.BLL.Representation.Model
{
    public class RepModelBase : MessageResult
    {
        protected const string Y = "Y";
        protected const string N = "N";

        [ScriptIgnore] protected readonly List<string> ChangedPropertise = new List<string>();

        #region BaseInfo

        public virtual string Title { get; set; }
        public virtual string RepAction { get; set; }
        public virtual string RepActionName { get; set; }

        #region ViewNames

        [EditorBrowsable(EditorBrowsableState.Never)] RepViewName viewName;
        public RepViewName ViewName
        {
            get { return this.viewName ?? (this.viewName = new RepViewName()); }
            set { this.viewName = value; }
        }

        #endregion

        #region ControlStatus

        [EditorBrowsable(EditorBrowsableState.Never)] RepControlStatus repControlStatus;
        public RepControlStatus RepControlStatus
        {
            get { return this.repControlStatus ?? (this.repControlStatus = new RepControlStatus()); }
            set { this.repControlStatus = value; }
        }

        #endregion

        #endregion

        public RepModelBase()
        {
            CanAdd = true;
            CanSelect = true;
        }

        #region Common

        [EditorBrowsable(EditorBrowsableState.Never)] string autCode;
        [EditorBrowsable(EditorBrowsableState.Never)] int autIntNo;
        [EditorBrowsable(EditorBrowsableState.Never)] bool canAdd;
        [EditorBrowsable(EditorBrowsableState.Never)] bool canSelect;
        [EditorBrowsable(EditorBrowsableState.Never)] bool isSummons;
        [EditorBrowsable(EditorBrowsableState.Never)] string notTicketNo;

        public bool CanAdd
        {
            get { return this.canAdd; }
            set { PropertyChanaged(ref this.canAdd, value, () => CanAdd); }
        }
        public bool CanSelect
        {
            get { return this.canSelect; }
            set { PropertyChanaged(ref this.canSelect, value, () => CanSelect); }
        }
        public virtual int AutIntNo
        {
            get { return this.autIntNo; }
            set { PropertyChanaged(ref this.autIntNo, value, () => AutIntNo); }
        }
        public virtual bool IsSummons
        {
            get { return this.isSummons; }
            set { PropertyChanaged(ref this.isSummons, value, () => IsSummons); }
        }
        public virtual string NotTicketNo
        {
            get { return this.notTicketNo.Trim2(); }
            set { PropertyChanaged(ref this.notTicketNo, value, () => NotTicketNo); }
        }
        public virtual string AutCode
        {
            get { return this.autCode.Trim2(); }
            set { PropertyChanaged(ref this.autCode, value, () => AutCode); }
        }

        #endregion

        public virtual void RefCopy(RepModelBase parent)
        {
            if (parent != null)
            {
                base.RefCopy(parent);

                AutIntNo = parent.AutIntNo;
                AutCode = parent.AutCode;
                IsSummons = parent.IsSummons;
                NotTicketNo = parent.NotTicketNo;
                Title = parent.Title;
                RepAction = parent.RepAction;
                RepActionName = parent.RepActionName;
                ViewName = parent.ViewName;
                RepControlStatus = parent.RepControlStatus;
            }
        }

        protected virtual void PropertyChanaged<T>(ref T field, T value, Expression<Func<T>> property)
        {
            field = value;
            var name = ((MemberExpression)property.Body).Member.Name;
            if (!this.ChangedPropertise.Contains(name))
                this.ChangedPropertise.Add(name);
        }

        public void ClearChangedPropertise()
        {
            this.ChangedPropertise.Clear();
            this.ChangedPropertise.TrimExcess();
        }
    }
}
