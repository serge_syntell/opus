﻿using System;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Representation;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.TMS;
using Authority = SIL.AARTO.DAL.Entities.Authority;

namespace SIL.AARTO.BLL.Representation
{
    public abstract partial class RepresentationBase
    {
        #region Services

        readonly QueueItemProcessor queueProcessor = new QueueItemProcessor();

        #endregion

        #region Push Queues

        void PushQueue(object body,
            ServiceQueueTypeList queueType,
            string group = null,
            DateTime? actionDate = null,
            int priority = 0)
        {
            this.queueProcessor.Send(new QueueItem
            {
                Body = body,
                Group = group,
                QueueType = queueType,
                ActDate = actionDate.GetValueOrDefault(DateTimeNow),
                Priority = priority,
                LastUser = LastUser
            });
        }

        protected bool PushQueue_RepresentationGracePeriod_Notice(int repIntNo, string autCode, MessageResult model = null)
        {
            const ServiceQueueTypeList queueType = ServiceQueueTypeList.RepresentationGracePeriod_Notice;
            if (repIntNo <= 0 || string.IsNullOrWhiteSpace(autCode))
            {
                if (model != null)
                    model.AddError(string.Format(RepresentationResx.PushQueueFailed, queueType));
                return false;
            }

            var actionDate = DateTimeNow.AddDays(Rules.AR_4100.GetValueOrDefault());

            var discarded = DiscardOldQueues(queueType, repIntNo, actionDate, model);
            if (discarded < 0) return false;

            if (discarded > 0)
                PushQueue(repIntNo, queueType, autCode, actionDate);
            return true;
        }

        bool PushQueue_SearchNameIDUpdate(int notIntNo, MessageResult model = null)
        {
            const ServiceQueueTypeList queueType = ServiceQueueTypeList.SearchNameIDUpdate;
            if (notIntNo <= 0)
            {
                if (model != null)
                    model.AddError(string.Format(RepresentationResx.PushQueueFailed, queueType));
                return false;
            }
            PushQueue(notIntNo, queueType);
            return true;
        }

        bool PushQueue_ProcessNewOffenderNotices(int notIntNo, string autCode, bool isNoAOG, MessageResult model = null)
        {
            const ServiceQueueTypeList queueType = ServiceQueueTypeList.ProcessNewOffenderNotices;
            if (notIntNo <= 0 || string.IsNullOrWhiteSpace(autCode))
            {
                if (model != null)
                    model.AddError(string.Format(RepresentationResx.PushQueueFailed, queueType));
                return false;
            }

            if (!NoticeDB.SetPendingNewOffender(notIntNo, LastUser))
                return false;

            //var actionDate = DateTimeNow.AddDays(isNoAOG
            //    ? Rules.DR_NotNoAOGPosted1stNoticeDate_SumIssueDate.GetValueOrDefault()
            //    : Rules.DR_NotPosted1stNoticeDate_NotIssueSummonsDate.GetValueOrDefault());
            var actionDate = DateTimeNow;

            var discarded = DiscardOldQueues(queueType, notIntNo, actionDate, model);
            if (discarded < 0) return false;

            if (discarded > 0)
                PushQueue(notIntNo, queueType, autCode, actionDate);
            return true;
        }

        string GetGenerateSummonsGroup(int notIntNo, string autCode)
        {
            Notice notice;
            return notIntNo <= 0
                || string.IsNullOrWhiteSpace(autCode)
                || (notice = GetNotice(notIntNo)).NotIntNo <= 0
                ? null
                : string.Join(QueueSeparator, new[]
                {
                    autCode.Trim2(),
                    notice.NotFilmType.Equals2(NoticeFilmType.H.ToString()) && !notice.IsVideo
                        ? NoticeFilmType.H.ToString() : "",
                    notice.NotCourtName
                });
        }

        bool PushQueue_GenerateSummons(int notIntNo, string autCode, DateTime? actionDate, MessageResult model = null)
        {
            string group;
            const ServiceQueueTypeList queueType = ServiceQueueTypeList.GenerateSummons;
            if (string.IsNullOrWhiteSpace(group = GetGenerateSummonsGroup(notIntNo, autCode)) || !actionDate.HasValue)
            {
                if (model != null)
                    model.AddError(string.Format(RepresentationResx.PushQueueFailed, queueType));
                return false;
            }

            var offenceDate = GetNotice(notIntNo).NotOffenceDate;
            var isXMonth = Rules.AR_5050_Bool.GetValueOrDefault();
            var reservedActionDate = !isXMonth ? (DateTime?)null : offenceDate.AddMonths(Rules.AR_5050_Int.GetValueOrDefault()).AddDays(-5);

            var discarded = 1;
            if (!isXMonth || reservedActionDate != actionDate)
            {
                discarded = DiscardOldQueues(queueType, notIntNo, actionDate.GetValueOrDefault(DateTimeMin), model, reservedActionDate);
                if (discarded < 0) return false;
            }

            if (discarded > 0)
                PushQueue(notIntNo,
                    queueType,
                    group,
                    actionDate,
                    offenceDate.Subtract(DateTimeNow).Days);
            return true;
        }

        protected bool PushQueue_GenerateSummons(ref RepresentationModel model, bool isReverse)
        {
            Notice notice;
            if (!CheckModel(ref model, true)
                || model.AutIntNo <= 0
                || model.RepIntNo <= 0
                || (notice = GetNotice(model.Notice.NotIntNo)).NotIntNo <= 0)
                return false;

            //update by rachel 2014/10/28 for 5332
            /* 
            DateTime? actionDate = isReverse
                ? DateTimeNow
                : (model.Notice.IsNoAOG
                    ? this.noAOGDB.GetNoAOGGenerateSummonsActionDate(model.AutIntNo, notice.NotPosted1stNoticeDate.GetValueOrDefault(DateTimeNow))
                    // here RepRecommendDate = RepDate ≈ DateTime.Now
                    : model.RepDate.GetValueOrDefault(DateTimeNow).AddDays(Rules.DR_RepRecommendDate_NotIssueSummonsDate.GetValueOrDefault()));
            */
           SIL.AARTO.BLL.Model.DateRuleInfo dateRule;
           int daysOfGenerateSummons = 0;
           dateRule = new SIL.AARTO.BLL.Model.DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssueSummonsDate");
           daysOfGenerateSummons = dateRule.ADRNoOfDays;

           dateRule = new SIL.AARTO.BLL.Model.DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotPaymentDate");
           int daysOffenceDateToPaymentDate = dateRule.ADRNoOfDays;

           dateRule = new SIL.AARTO.BLL.Model.DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotIssue2ndNoticeDate", "Not2ndPaymentDate");
           int daysIssue2ndNoticeTo2ndPaymentDate = dateRule.ADRNoOfDays;

           string aR_2500 = new SIL.AARTO.BLL.Model.AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, "2500").ARString.Trim();
           DateTime? actionDate = model.Notice.IsNoAOG ? this.noAOGDB.GetNoAOGGenerateSummonsActionDate(autIntNo, notice.NotOffenceDate) : (aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase) ? notice.NotOffenceDate.AddDays(daysOfGenerateSummons) :
                                            notice.NotOffenceDate.AddDays(daysOffenceDateToPaymentDate + daysIssue2ndNoticeTo2ndPaymentDate + 4));
            //end update by rachel 2014/10/28 for 5332

            return PushQueue_GenerateSummons(model.Notice.NotIntNo, model.AutCode, actionDate, model);
        }

        void PushQueue_PrintRepresentation(int pfnIntNo, int autIntNo, string letterType)
        {
            if (letterType.In(RepDecision, InsufficientDetails))
            {
                var queueType = letterType.Equals2(RepDecision)
                    ? ServiceQueueTypeList.PrintRepresentationResult
                    : ServiceQueueTypeList.PrintRepresentationInsufficientDetails;

                Authority authority;
                if (pfnIntNo <= 0
                    || autIntNo <= 0
                    || (authority = this.authorityService.GetByAutIntNo(autIntNo)) == null)
                    throw new Exception(string.Format(RepresentationResx.PushQueueFailed, queueType));

                PushQueue(pfnIntNo, queueType, authority.AutCode.Trim2(), DateTimeNow.AddHours(GetDelayActionDate_InHours()));
            }
        }

        bool PushQueue_Generate2ndNotice(int notIntNo, string autCode, DateTime notOffenceDate, MessageResult model = null)
        {
            string violationType;
            const ServiceQueueTypeList queueType = ServiceQueueTypeList.Generate2ndNotice;
            if (notIntNo <= 0
                || string.IsNullOrWhiteSpace(autCode)
                || notOffenceDate.In(default(DateTime), DateTimeMin)
                || string.IsNullOrWhiteSpace(violationType = GetViolationType(notIntNo)))
            {
                if (model != null)
                    model.AddError(string.Format(RepresentationResx.PushQueueFailed, queueType));
                return false;
            }
            PushQueue(notIntNo, queueType, string.Format("{0}|{1}", autCode, violationType),
                notOffenceDate.AddDays(Rules.DR_NotOffenceDate_NotPaymentDate.GetValueOrDefault()));
            return true;
        }

        bool PushQueue_Print1stNotice(int pfnIntNo, string autCode, int notIntNo, bool isVideo, MessageResult model = null)
        {
            string violationType;
            const ServiceQueueTypeList queueType = ServiceQueueTypeList.Print1stNotice;
            if (pfnIntNo <= 0 || notIntNo <= 0
                || string.IsNullOrWhiteSpace(autCode)
                || string.IsNullOrWhiteSpace(violationType = GetViolationType(notIntNo)))
            {
                if (model != null)
                    model.AddError(string.Format(RepresentationResx.PushQueueFailed, queueType));
                return false;
            }
            PushQueue(pfnIntNo, queueType, string.Format("{0}|{1}|{2}", autCode, violationType, isVideo ? "VID" : "HWO"),
                DateTimeNow.AddHours(GetDelayActionDate_InHours()));
            return true;
        }

        #endregion

        #region Discard Old Queues

        int DiscardOldQueues(ServiceQueueTypeList queueType, object key, DateTime actionDate, MessageResult model = null, DateTime? ReservedActionDate = null)
        {
            var paras = new[]
            {
                new SqlParameter("@SQTIntNo", (int)queueType),
                new SqlParameter("@SeQuKey", key.ToString()),
                new SqlParameter("@ActionDate", actionDate),
                new SqlParameter("@ReservedActionDate", ReservedActionDate ?? (object)DBNull.Value),
                new SqlParameter("@LastUser", LastUser ?? "")
            };

            int result; //2014-05-14 Heidi added Inside the HandwrittenOffencesImporter program also uses this "DiscardOlderActionDateQueues" stored procedure(5239)
            int.TryParse(Convert.ToString(this.serviceDB_Queue.ExecuteScalar("DiscardOlderActionDateQueues", paras)), out result);

            if (result < 0 && model != null)
                model.AddError(string.Format(RepresentationResx.DiscardQueueWithOlderActionDateFailded, queueType, result));

            return result;
        }

        #endregion
    }
}
