﻿using System.ComponentModel;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation
{
    public abstract partial class RepresentationBase
    {
        #region Operations

        /// <summary>
        ///     set model's values: RepIntNo, ChgIntNo, RepReverseReason, RepAction, IsSummons, Notice.NotIntNo
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected bool ReverseRepresentation(ref RepresentationModel model)
        {
            if (!CheckModel(ref model)) return false;

            string error = null;
            var result = this.representationDB.ReverseRepresentation(
                model.RepIntNo,
                model.ChgIntNo,
                model.RepReverseReason ?? "",
                LastUser,
                model.RepAction,
                model.IsSummons,
                null, 0,
                model.Notice.NotIntNo,
                "", false, ref error,
                Rules.AR_9060 ?? N,
                Rules.AR_9120 ?? N,
                Rules.AR_9130.GetValueOrDefault(12));

            if (result[0] <= 0)
            {
                model.AddError(RepresentationResx.ErrorCouldNotReverseRepresentation);
                return false;
            }

            bool pushGenerateSummons;
            var notice = HandleTargetCSCode(ref model, out pushGenerateSummons);
            if (notice.NotIntNo <= 0) return false;

            if ((pushGenerateSummons
                || (!model.IsSummons
                    && notice.NoticeStatus != CODE_REP_LOGGED_CHARGE
                    && !notice.NoticeStatus.Between(CODE_Officer_Error, CODE_Error_End))
                //push GenerateSummons for reversal the withdrawn summons
                || (RepAction.Is(RepAction.Decide)
                    && model.IsSummons
                    && model.RepresentationCode.Equals2(SUMMONS_REPCODE_WITHDRAW)
                    && model.Notice.NoticeStatus.Equals2(CODE_SUMMONS_WITHDRAWN_CHARGE)))
                && !PushQueue_GenerateSummons(ref model, true))
                return false;

            model.AddMessage(RepresentationResx.RepresentationHasBeenReversedSuccessfully);
            return true;
        }

        #endregion

        #region Actions

        public MessageResult Reverse(ref RepresentationModel model)
        {
            if (!CheckModel(ref model)
                || !CheckModelRowVersion(ref model)
                || IsHandwrittenCorrectionOrPaid(model.ChgIntNo, model.IsSummons, model)
                || !model.ValidateReversal(RepAction.Is(RepAction.ChangeOfOffender)))
                return model;

            var rep = model;
            if (!TransactionScope(() =>
            {
                if (!RepresentationReverse(ref rep))
                    return false;

                // implement Heidi's "for all Punch Statistics Transaction(5084)"
                if (this.punch.PunchStatisticsTransactionAdd(this.autIntNo, LastUser, PunchStatisticsTranTypeList.Representation, PunchAction.Change) <= 0)
                {
                    rep.AddError(this.punch.ErrorMessage);
                    return false;
                }

                return true;
            },
                ex => rep.AddError(ex.ToString())))
            {
                model.IsSuccessful = false;
                return model;
            }

            return model;
        }

        #endregion
    }
}
