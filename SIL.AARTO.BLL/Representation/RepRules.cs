﻿using System;
using System.ComponentModel;
using System.Reflection;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.BLL.Model;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation
{
    public class RepRules : ReadyForUse
    {
        const int AutIntNoType = 1;
        const int CrtIntNoType = 2;
        const string Y = "Y";

        readonly AuthorityRuleInfo authorityRuleService = new AuthorityRuleInfo();
        readonly DateRuleInfo dateRuleService = new DateRuleInfo();

        protected override object LoadValue<T>(PropertyInfo pi, int type)
        {
            var prefix = pi.Name.Substring(0, Math.Min(3, pi.Name.Length));
            var value = pi.Name.Remove(0, Math.Min(3, pi.Name.Length));

            if (prefix.Length < 3
                || string.IsNullOrWhiteSpace(value))
                return default(T);

            switch (type)
            {
                case AutIntNoType:
                    if (prefix.Equals2("AR_"))
                        return SetAuthRuleValue<T>(pi, value);
                    break;
                case CrtIntNoType:
                    if (prefix.Equals2("DR_"))
                        return SetDateRuleValue<T>(pi, value);
                    break;
            }

            return default(T);
        }

        object SetAuthRuleValue<T>(PropertyInfo pi, string code)
        {
            code = code.Trim().Split('_')[0];
            AuthorityRuleInfo ar;
            if (code.Length < 4
                || !ServiceUtility.IsNumeric(code)
                || (ar = GetAuthorityRule(AutIntNo, code)) == null)
                return default(T);

            object value = null;
            if (pi.PropertyType == typeof(bool?))
                value = ar.ARString.Equals2(Y);
            else if (pi.PropertyType == typeof(int?))
                value = ar.ARNumeric;
            else if (pi.PropertyType == typeof(string))
                value = ar.ARString.Trim2();
            return value ?? default(T);
        }

        object SetDateRuleValue<T>(PropertyInfo pi, string code)
        {
            var codes = code.Split('_');
            if (code.Length != 2)
                return default(T);

            var start = codes[0].Trim2();
            var end = codes[1].Trim2();
            DateRuleInfo dr;
            if ((dr = GetDateRule(AutIntNo, start, end)) == null)
                return default(T);

            object value = default(T);
            if (pi.PropertyType == typeof(int?))
                value = dr.ADRNoOfDays;
            return value;
        }

        public AuthorityRuleInfo GetAuthorityRule(int autIntNo, string code)
        {
            if (autIntNo <= 0 || string.IsNullOrWhiteSpace(code)) return null;
            return this.authorityRuleService.GetAuthorityRulesInfoByWFRFANameAutoIntNo(autIntNo, code);
        }

        public DateRuleInfo GetDateRule(int autIntNo, string start, string end)
        {
            if (autIntNo <= 0
                || string.IsNullOrWhiteSpace(start)
                || string.IsNullOrWhiteSpace(end))
                return null;
            return this.dateRuleService.GetDateRuleInfoByDRNameAutIntNo(autIntNo, start, end);
        }

        #region AutIntNo

        [EditorBrowsable(EditorBrowsableState.Never)] int autIntNo;
        public int AutIntNo
        {
            get { return this.autIntNo; }
            set
            {
                lock (SyncObj)
                {
                    if (value > 0 && value == this.autIntNo) return;
                    this.autIntNo = value;
                    OnTriggerChanged(AutIntNoType);
                }
            }
        }

        #endregion

        #region CrtIntNo

        [EditorBrowsable(EditorBrowsableState.Never)] int crtIntNo;
        public int CrtIntNo
        {
            get { return this.crtIntNo; }
            set
            {
                lock (SyncObj)
                {
                    if (value > 0 && value == this.crtIntNo) return;
                    this.crtIntNo = value;
                    OnTriggerChanged(CrtIntNoType);
                }
            }
        }

        #endregion

        #region Rules (Adding should obey the rules)

        #region AuthRules

        /// <summary>
        ///     Rule to indicate no. of days before court date that a representation may be logged
        /// </summary>
        public int? AR_4050
        {
            get { return OnPropertyGetting(() => AR_4050, AutIntNoType); }
            set { OnPropertySetting(() => AR_4050, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to set Representation logged grace period (no. of days)
        /// </summary>
        public int? AR_4100
        {
            get { return OnPropertyGetting(() => AR_4100, AutIntNoType); }
            set { OnPropertySetting(() => AR_4100, AutIntNoType, value); }
        }

        /// <summary>
        ///     Batch printing of representation letters
        /// </summary>
        public bool? AR_4120
        {
            get { return OnPropertyGetting(() => AR_4120, AutIntNoType); }
            set { OnPropertySetting(() => AR_4120, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to make the ID number optional
        /// </summary>
        public bool? AR_4531
        {
            get { return OnPropertyGetting(() => AR_4531, AutIntNoType); }
            set { OnPropertySetting(() => AR_4531, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to make the Forenames optional
        /// </summary>
        public bool? AR_4532
        {
            get { return OnPropertyGetting(() => AR_4532, AutIntNoType); }
            set { OnPropertySetting(() => AR_4532, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to make the Representation details and recommendation optional
        /// </summary>
        public bool? AR_4533
        {
            get { return OnPropertyGetting(() => AR_4533, AutIntNoType); }
            set { OnPropertySetting(() => AR_4533, AutIntNoType, value); }
        }

        /// <summary>
        ///     Allow automatic presentation of document representation
        /// </summary>
        public bool? AR_4534
        {
            get { return OnPropertyGetting(() => AR_4534, AutIntNoType); }
            set { OnPropertySetting(() => AR_4534, AutIntNoType, value); }
        }

        /// <summary>
        ///     Require management override role to process a second representation
        /// </summary>
        public bool? AR_4535
        {
            get { return OnPropertyGetting(() => AR_4535, AutIntNoType); }
            set { OnPropertySetting(() => AR_4535, AutIntNoType, value); }
        }

        ///// <summary>
        /////     Allow user to process representations on summons if served and after court date and case number allocation
        ///// </summary>
        //public bool? AR_4536
        //{
        //    get { return OnPropertyGetting(() => AR_4536, AutIntNoType); }
        //    set { OnPropertySetting(() => AR_4536, AutIntNoType, value); }
        //}

        /// <summary>
        ///     Representations allowed on NoAOG offences
        /// </summary>
        public bool? AR_4537
        {
            get { return OnPropertyGetting(() => AR_4537, AutIntNoType); }
            set { OnPropertySetting(() => AR_4537, AutIntNoType, value); }
        }

        ///// <summary>
        /////     Change of offender process will succeed up to case number generated (710)
        ///// </summary>
        //public int? AR_4538
        //{
        //    get { return OnPropertyGetting(() => AR_4538, AutIntNoType); }
        //    set { OnPropertySetting(() => AR_4538, AutIntNoType, value); }
        //}

        /// <summary>
        ///     Rule to set citizenship default to RSA
        /// </summary>
        public string AR_4560
        {
            get { return OnPropertyGetting(() => AR_4560, AutIntNoType); }
            set { OnPropertySetting(() => AR_4560, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to set maximum charge status at which payment/representations are allowed
        /// </summary>
        public int? AR_4570
        {
            get { return OnPropertyGetting(() => AR_4570, AutIntNoType); }
            set { OnPropertySetting(() => AR_4570, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to set one-step representations
        /// </summary>
        public bool? AR_4595
        {
            get { return OnPropertyGetting(() => AR_4595, AutIntNoType); }
            set { OnPropertySetting(() => AR_4595, AutIntNoType, value); }
        }

        /// <summary>
        ///     Status to start showing notices on enquiry screen.
        /// </summary>
        public int? AR_4605
        {
            get { return OnPropertyGetting(() => AR_4605, AutIntNoType); }
            set { OnPropertySetting(() => AR_4605, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to determine which date rule to use as the cutoff date for representations.
        /// </summary>
        public string AR_4650
        {
            get { return OnPropertyGetting(() => AR_4650, AutIntNoType); }
            set { OnPropertySetting(() => AR_4650, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to indicate no. of days to add to change of offender/change of registration date to recalculate the notice
        ///     payment date
        /// </summary>
        public int? AR_4660
        {
            get { return OnPropertyGetting(() => AR_4660, AutIntNoType); }
            set { OnPropertySetting(() => AR_4660, AutIntNoType, value); }
        }

        /// <summary>
        ///     Summons generation override - force generate summons after "x" months
        /// </summary>
        public int? AR_5050_Int
        {
            get { return OnPropertyGetting(() => AR_5050_Int, AutIntNoType); }
            set { OnPropertySetting(() => AR_5050_Int, AutIntNoType, value); }
        }

        /// <summary>
        ///     Summons generation override - force generate summons after "x" months
        /// </summary>
        public bool? AR_5050_Bool
        {
            get { return OnPropertyGetting(() => AR_5050_Bool, AutIntNoType); }
            set { OnPropertySetting(() => AR_5050_Bool, AutIntNoType, value); }
        }

        /// <summary>
        ///     Print documents on IBM printer
        /// </summary>
        public bool? AR_6209
        {
            get { return OnPropertyGetting(() => AR_6209, AutIntNoType); }
            set { OnPropertySetting(() => AR_6209, AutIntNoType, value); }
        }

        /// <summary>
        ///     Representations allowed on Section35 NoAOG offences?
        /// </summary>
        public bool? AR_6610
        {
            get { return OnPropertyGetting(() => AR_6610, AutIntNoType); }
            set { OnPropertySetting(() => AR_6610, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to set data washing is active.
        /// </summary>
        public string AR_9060
        {
            get { return OnPropertyGetting(() => AR_9060, AutIntNoType); }
            set { OnPropertySetting(() => AR_9060, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to set if data washing should use the address captured at representation
        /// </summary>
        public string AR_9120
        {
            get { return OnPropertyGetting(() => AR_9120, AutIntNoType); }
            set { OnPropertySetting(() => AR_9120, AutIntNoType, value); }
        }

        /// <summary>
        ///     Rule to set data washing use of representation address expiry period (in months)
        /// </summary>
        public int? AR_9130
        {
            get { return OnPropertyGetting(() => AR_9130, AutIntNoType); }
            set { OnPropertySetting(() => AR_9130, AutIntNoType, value); }
        }

        #endregion

        #region DateRules

        public int? DR_RepRecommendDate_NotIssueSummonsDate
        {
            get { return OnPropertyGetting(() => DR_RepRecommendDate_NotIssueSummonsDate, AutIntNoType); }
            set { OnPropertySetting(() => DR_RepRecommendDate_NotIssueSummonsDate, AutIntNoType, value); }
        }

        public int? DR_NotNoAOGPosted1stNoticeDate_SumIssueDate
        {
            get { return OnPropertyGetting(() => DR_NotNoAOGPosted1stNoticeDate_SumIssueDate, AutIntNoType); }
            set { OnPropertySetting(() => DR_NotNoAOGPosted1stNoticeDate_SumIssueDate, AutIntNoType, value); }
        }

        public int? DR_NotPosted1stNoticeDate_NotIssueSummonsDate
        {
            get { return OnPropertyGetting(() => DR_NotPosted1stNoticeDate_NotIssueSummonsDate, AutIntNoType); }
            set { OnPropertySetting(() => DR_NotPosted1stNoticeDate_NotIssueSummonsDate, AutIntNoType, value); }
        }

        public int? DR_NotOffenceDate_NotPaymentDate
        {
            get { return OnPropertyGetting(() => DR_NotOffenceDate_NotPaymentDate, AutIntNoType); }
            set { OnPropertySetting(() => DR_NotOffenceDate_NotPaymentDate, AutIntNoType, value); }
        }

        #endregion

        #endregion
    }
}
