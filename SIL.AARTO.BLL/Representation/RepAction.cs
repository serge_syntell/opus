﻿using System.Linq;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation
{
    public class RepAction
    {
        readonly string name;
        readonly string value;

        RepAction(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        #region Values

        public const string R = "R";
        public const string D = "D";
        public const string O = "O";
        public const string T = "T";

        #endregion

        #region Register

        public const string RegisterName = "Register";

        public static RepAction Register
        {
            get { return new RepAction(RegisterName, R); }
        }

        #endregion

        #region Decide

        public const string DecideName = "Decide";

        public static RepAction Decide
        {
            get { return new RepAction(DecideName, D); }
        }

        #endregion

        #region ChangeOfOffender

        public const string ChangeOfOffenderName = "ChangeOfOffender";

        public static RepAction ChangeOfOffender
        {
            get { return new RepAction(ChangeOfOffenderName, O); }
        }

        #endregion

        #region PresentationOfDocument

        public const string PresentationOfDocumentName = "PresentationOfDocument";

        public static RepAction PresentationOfDocument
        {
            get { return new RepAction(PresentationOfDocumentName, D); }
        }

        #endregion

        public string Name
        {
            get { return this.name; }
        }

        public string Value
        {
            get { return this.value; }
        }

        public override string ToString()
        {
            return this.value;
        }

        public bool Equals(string repActionValue)
        {
            return this.value.Equals(repActionValue);
        }

        public bool In(params string[] repActionValues)
        {
            return this.value.In(repActionValues);
        }

        public bool Is(RepAction repAction)
        {
            return this.name.Equals(repAction.Name);
        }

        public bool IsIn(params RepAction[] repAction)
        {
            return repAction.Any(r => r.name.Equals(this.name));
        }

        public static RepAction GetRepAction(string value)
        {
            switch (value)
            {
                case R:
                    return Register;
                case D:
                    return Decide;
                case O:
                    return ChangeOfOffender;
            }
            return null;
        }
    }
}
