﻿using System;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation
{
    public class Representation_Register : RepresentationBase
    {
        protected override void OnConstructorLoading()
        {
            Title = RepresentationResx.RepresentationRegister;
            RepAction = RepAction.Register;
        }

        public override void OnConstructorLoaded() {}

        #region Operations

        protected override bool BuildDetails(ref RepresentationModel model, int repCode)
        {
            RepControlStatus.txtOffenceDescrEnabled = false;
            RepControlStatus.txtRecommendVisible = false;
            RepControlStatus.txtReversalReasonVisible = false;
            RepControlStatus.btnSaveVisible = true;
            RepControlStatus.btnDecideVisible = false;
            RepControlStatus.btnReverseVisible = false;
            RepControlStatus.pnlRepOfficialVisible = false;

            if (isNew)
            {
                RepControlStatus.txtOffenceDescrEnabled = true;

                GenerateNewEmptyRep(ref model);
            }
            else
            {
                //dls 080611 - no longer allowed to reverse a summons rep - they need to process a new one! or create via a single summons
                if (model.Charge.ChargeStatus.In(
                    CODE_REP_LOGGED_CHARGE,
                    CODE_REP_PENDING))
                {
                    RepControlStatus.txtReversalReasonVisible = true;
                    RepControlStatus.btnReverseVisible = true;
                }
                else
                {
                    RepControlStatus.txtRecommendVisible = false;
                }
            }
            return true;
        }

        protected override bool RepresentationUpdate(ref RepresentationModel model)
        {
            return NewUpdateRepresentation(ref model);
        }

        protected override bool RepresentationDecide(ref RepresentationModel model)
        {
            throw new NotImplementedException();
        }

        protected override bool RepresentationReverse(ref RepresentationModel model)
        {
            return ReverseRepresentation(ref model);
        }

        #endregion
    }
}
