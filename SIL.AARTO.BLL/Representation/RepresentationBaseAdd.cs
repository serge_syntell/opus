﻿using System.ComponentModel;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Representation;

namespace SIL.AARTO.BLL.Representation
{
    public abstract partial class RepresentationBase
    {
        #region Operations

        /// <summary>
        ///     Only for Register or Decision
        ///     <para>
        ///         Set model's values: RepIntNo, ChgIntNo, RepDetails, RepDate, RepOffenderName, RepOffenderAddress,
        ///         RepOffenderTelNo, RepOfficial, AutIntNo, Charge.ChargeStatus, RepCurrentFineAmount, ChangeOfOffender, RepType,
        ///         IsSummons
        ///     </para>
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected bool NewUpdateRepresentation(ref RepresentationModel model)
        {
            if (!CheckModel(ref model, true)
                || !IsRegisterOrDecide())
                return false;

            string errorMsg = null;
            var repIntNo = model.RepIntNo;
            model.RepIntNo = this.representationDB.NewUpdateRepresentation(
                model.RepIntNo,
                model.ChgIntNo,
                model.RepDetails ?? "",
                model.RepDate.GetValueOrDefault(DateTimeNow),
                model.RepOffenderName ?? "",
                model.RepOffenderAddress ?? "",
                model.RepOffenderTelNo ?? "",
                model.RepOfficial ?? "",
                LastUser,
                model.AutIntNo,
                model.Charge.ChargeStatus,
                model.RepCurrentFineAmount,
                model.ChangeOfOffender ?? "",
                model.RepType ?? "",
                ref errorMsg,
                model.IsSummons);

            if (model.RepIntNo <= 0)
            {
                switch (model.RepIntNo)
                {
                    case -1:
                        model.AddError(RepresentationResx.RepresentationWithSameRepCodeAlreadyExists);
                        break;

                    case -2:
                        model.AddError(string.Format(RepresentationResx.ErrorCouldNotUpdateOrAddRepresentation, errorMsg));
                        break;

                    case -3:
                        model.AddError(RepresentationResx.FailedInUpdatingNoticeChargeStatus);
                        break;

                    case -11:
                        model.AddError(string.Format(RepresentationResx.ThisHasBeenCorrected,
                            !model.IsSummons ? RepresentationResx.Notice : RepresentationResx.Summons));
                        break;

                    default:
                        model.AddError(string.Format(RepresentationResx.ErrorCouldNotUpdateOrAddRepresentation, model.RepIntNo + " " + errorMsg));
                        break;
                }

                return false;
            }

            if (repIntNo == 0
                && !AddRepCount(ref model))
                return false;

            return true;
        }

        bool AddRepCount(ref RepresentationModel model)
        {
            if (CheckModel(ref model, true) && model.Notice.NotIntNo > 0)
            {
                if (!model.IsSummons)
                {
                    var notice = this.noticeService.GetByNotIntNo(model.Notice.NotIntNo);
                    if (notice != null)
                    {
                        notice.RepCount++;
                        this.noticeService.Save(notice);
                        return true;
                    }
                }
                else
                {
                    var summons = this.summonsService.GetBySumIntNo(model.Notice.SumIntNo);
                    if (summons != null)
                    {
                        summons.RepCount++;
                        this.summonsService.Save(summons);
                        return true;
                    }
                }
            }

            model.AddError(RepresentationResx.AddingRepCountFailed);
            return false;
        }

        //bool AddRepCount(ref RepresentationModel model)
        //{
        //    if (CheckModel(ref model, true)
        //        && ((!model.IsSummons && model.Notice.NotIntNo > 0)
        //            || (model.IsSummons && model.Notice.SumIntNo > 0)))
        //    {
        //        var paras = new[]
        //        {
        //            new SqlParameter("@PK", !model.IsSummons ? model.Notice.NotIntNo : model.Notice.SumIntNo),
        //            new SqlParameter("@IsSummons", model.IsSummons)
        //        };
        //        if (Convert.ToInt32(this.serviceDB.ExecuteScalar("RepCountAdd", paras)) > 0)
        //            return true;
        //    }

        //    model.AddError(RepresentationResx.AddingRepCountFailed);
        //    return false;
        //}

        #endregion

        #region Actions

        public virtual RepresentationModel Add(int autIntNo, string ticketNumber, bool isSummons, int chgIntNo, bool isReadOnly, long chgRowVersion)
        {
            var model = new RepresentationModel();
            if (!CheckParameters(ref model, autIntNo, ticketNumber, chgIntNo, chgRowVersion: chgRowVersion)
                || !CheckParameterRowVersion(chgRowVersion, isSummons, chgIntNo, isReadOnly, model)
                || IsHandwrittenCorrectionOrPaid(chgIntNo, isSummons, model))
                return model;

            this.isNew = true;
            RepControlStatus.btnSaveVisible = true;
            RepControlStatus.btnSaveIsAdd = true;
            RepControlStatus.txtReversalReasonVisible = false;

            GetRepresentationDetails(ref model, autIntNo, ticketNumber, chgIntNo, isReadOnly, 0);

            return model;
        }

        public MessageResult Save(ref RepresentationModel model)
        {
            if (!CheckModel(ref model)
                || !CheckModelRowVersion(ref model)
                || IsHandwrittenCorrectionOrPaid(model.ChgIntNo, model.IsSummons, model)
                || !model.ValidateUpdate(Rules))
                return model;

            model.IsChangeOfOffender = false;
            model.RepType = RepType.N;

            var rep = model;
            if (!TransactionScope(() =>
            {
                var repIntNo = rep.RepIntNo;
                if (!SaveTargetCSCode(ref rep, m => RepresentationUpdate(ref m)))
                    return false;

                if (repIntNo == 0)
                {
                    if (!PushQueue_RepresentationGracePeriod_Notice(rep.RepIntNo, rep.AutCode, rep))
                        return false;

                    int result;
                    string errorMsg;
                    if (Rules.AR_6209.GetValueOrDefault()
                        && (result = this.representationDB.UpdateRepresentationForPrint(rep.AutIntNo, rep.RepIntNo, LastUser, RepAction.Value, rep.IsSummons, out errorMsg)) <= 0)
                    {
                        rep.AddError(string.Format(RepresentationResx.FailedInUpdatingRepresentationForPrint, result, errorMsg));
                        return false;
                    }
                }

                // implement Heidi's "for all Punch Statistics Transaction(5084)"
                if (this.punch.PunchStatisticsTransactionAdd(rep.AutIntNo, LastUser,
                    repIntNo == 0 ? PunchStatisticsTranTypeList.RepresentationRegister : PunchStatisticsTranTypeList.Representation,
                    repIntNo == 0 ? PunchAction.Add : PunchAction.Change) <= 0)
                {
                    rep.AddError(this.punch.ErrorMessage);
                    return false;
                }

                rep.AddMessage(repIntNo == 0
                    ? RepresentationResx.RepresentationHasBeenAdded
                    : RepresentationResx.RepresentationHasBeenUpdated);

                return true;
            }, ex => rep.AddError(ex.Message)))
            {
                model.IsSummons = false;
                return model;
            }

            model.Body = model.RepIntNo;
            return model;
        }

        public virtual RepresentationModel Edit(int autIntNo, string ticketNumber, bool isSummons, int chgIntNo, bool isReadOnly, int repIntNo, long chgRowVersion)
        {
            var model = new RepresentationModel();
            if (!CheckParameters(ref model, autIntNo, ticketNumber, chgIntNo, repIntNo, chgRowVersion)
                || !CheckParameterRowVersion(chgRowVersion, isSummons, chgIntNo, isReadOnly, model))
                return model;

            RepControlStatus.btnSaveIsAdd = false;

            GetRepresentationDetails(ref model, autIntNo, ticketNumber, chgIntNo, isReadOnly, repIntNo);

            var a = model.RWRIntNo;

            return model;
        }

        #endregion
    }
}
