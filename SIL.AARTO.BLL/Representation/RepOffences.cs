﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Reflection;
using SIL.AARTO.BLL.Extensions;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation
{
    public class RepOffences : ReadyForUse
    {
        const int PODType = 1;
        ServiceDB serviceDB;

        public RepOffences(string connStr = null)
        {
            if (!string.IsNullOrWhiteSpace(connStr))
                ConnectionString = connStr;
        }

        #region ConnectionString

        [EditorBrowsable(EditorBrowsableState.Never)] string connectionString;
        public string ConnectionString
        {
            get { return this.connectionString; }
            set
            {
                lock (SyncObj)
                {
                    if (!string.IsNullOrWhiteSpace(value)
                        && value.Equals2(this.connectionString))
                        return;
                    this.connectionString = value;

                    this.serviceDB = null;
                    this.serviceDB = new ServiceDB(value);

                    ResetInconstants();
                }
            }
        }

        #endregion

        protected override object LoadValue<T>(PropertyInfo pi, int type)
        {
            switch (type)
            {
                case PODType:
                    return GetPODOffences();
            }
            return default(T);
        }

        public List<string> GetPODOffences()
        {
            var offenceCodes = new List<string>();
            this.serviceDB.ExecuteReader("GetOffenceCodeListByIsPresentationOfDocument",
                new[] { new SqlParameter("@IsPOD", true) },
                dr =>
                {
                    while (dr.Read())
                        offenceCodes.Add(Convert.ToString(dr["OffCode"]).Trim2());
                });
            return offenceCodes;
        }

        #region Offences

        public List<string> PODOffenceList
        {
            get { return OnPropertyGetting(() => PODOffenceList, PODType); }
            set { OnPropertySetting(() => PODOffenceList, PODType, value); }
        }

        #endregion
    }
}
