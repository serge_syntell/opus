﻿using System;
using System.ComponentModel;
using System.Linq;
using SIL.AARTO.BLL.Representation.Model;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.Representation;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Representation
{
    public class Representation_PresentationOfDocument : RepresentationBase
    {
        protected override void OnConstructorLoading()
        {
            Title = RepresentationResx.PresentationOfDocument;
            RepAction = RepAction.PresentationOfDocument;

            ViewName.ChargeList = "_ChargeList_PresentationOfDocument";
            ViewName.Editor = null;
            ViewName.EditorPart = null;
        }

        public override void OnConstructorLoaded()
        {
            Rules.SetConstantValue(() => Rules.AR_4537, false); // pod always not allow NoAOG.
            Rules.SetConstantValue(() => Rules.AR_6610, false);
        }

        #region Search

        public override NoticeModel GetChargeList(int autIntNo, string ticketNumber, bool hideWarning = false)
        {
            var result = base.GetChargeList(autIntNo, ticketNumber, hideWarning);

            var hasAutoDoc = false;
            result.ChargeList.ForEach(c =>
            {
                c.CanSelect = c.CanSelect && CanRepAdd(c);
                if (c.CanSelect)
                {
                    c.CanAutoDoc = Rules.AR_4534.GetValueOrDefault()
                        && !result.BlockNoAOG
                        && c.ChgIsMain && c.HasAlternate
                        && Offences.PODOffenceList.FirstOrDefault(o => o.Equals2(c.ChgOffenceCode)) != null
                        //&& !c.HasRep;
                        && !HasRep(ref c);
                    if (c.CanAutoDoc)
                        hasAutoDoc = true;
                }
            });

            if (!hasAutoDoc)
            {
                result.ChargeList.Clear();
                if (!hideWarning) result.AddError(Rules.AR_4534.GetValueOrDefault() ? RepresentationResx.NoAvailableChargeCanDoPOD : RepresentationResx.PODIsNotAllowed, false);
            }
            else
            {
                result.ChargeList.RemoveAll(c =>
                {
                    ChargeModel charge;
                    return c.ChgIsMain && !c.CanAutoDoc
                        || !c.ChgIsMain && ((charge = result.ChargeList.FirstOrDefault(p => p.ChgIntNo == c.MainChargeID.GetValueOrDefault(-1))) == null || !charge.CanAutoDoc);
                });
            }

            return result;
        }

        #endregion

        #region Actions

        public MessageResult MakePresentationOfDocument(int autIntNo, string ticketNumber, int chgIntNo, bool isReadOnly, long chgRowVersion)
        {
            isNew = true;

            var model = new RepresentationModel();
            if (!CheckParameters(ref model, autIntNo, ticketNumber, chgIntNo, chgRowVersion: chgRowVersion)
                || !GetRepresentation(ref model, autIntNo, ticketNumber, chgIntNo, isReadOnly, 0)
                || !CheckParameterModelRowVersion(chgRowVersion, ref model)
                || IsHandwrittenCorrectionOrPaid(chgIntNo, model.IsSummons, model))
                return model;

            var scope = TransactionScope(() =>
            {
                #region prepare

                //model.RepIntNo = 0;
                model.RepDetails = RepresentationResx.PresentationOfDocument;
                model.RepDate = DateTimeNow;
                model.RepOffenderName = GetMandatoryValue(model.FullName, MandatoryValue);
                model.RepOffenderAddress = (model.Driver ?? new DriverModel()).GetPoAddress(MandatoryValue);
                model.RepOffenderTelNo = MandatoryValue;
                model.RepOfficial = GetMandatoryValue(model.RepOfficial, MandatoryValue);
                //model.Charge.ChargeStatus = !model.IsSummons
                //    ? CODE_REP_LOGGED_CHARGE : CODE_REP_LOGGED_SUMMONS;
                model.IsChangeOfOffender = false;
                model.RepType = RepType.A;

                model.RepresentationCode = !model.IsSummons
                    ? REPCODE_WITHDRAW : SUMMONS_REPCODE_WITHDRAW;
                model.RepRecommend = RepresentationResx.PresentationOfDocument;
                //model.RepRevisedFineAmount = model.RepCurrentFineAmount;
                model.Charge.ChargeStatus = !model.IsSummons
                    ? CODE_CASE_CANCELLED : CODE_SUMMONS_WITHDRAWN_CHARGE;
                model.Charge.RowVersion_Long = GetChargeRowVersion(model.IsSummons, model.ChgIntNo, isReadOnly);
                model.LetterTo = LetterTo.D;
                model.IsChangeOfRegNo = false;
                //model.ChangeOfRegNoType = RegNoType.N;
                //model.Notice.NotRegNo = string.Empty;
                //model.NewVMIntNo = 0;
                //model.NewVTIntNo = 0;
                //model.NewVehicleColourDescr = string.Empty;
                model.Charge.Notice.Driver = null;
                //model.MIPASource = string.Empty;

                #endregion

                if (!RepresentationDecide(ref model)) return false;

                // no need to push GenerateSummons in Pod.
                //if (!model.IsSummons)
                //{
                //    var notice = GetNotice(model.Notice.NotIntNo);
                //    if (notice.NotIntNo > 0
                //        && notice.NoticeStatus != CODE_REP_LOGGED_CHARGE)
                //        PushQueue_GenerateSummons(ref model, false);
                //}

                // implement Heidi's "for all Punch Statistics Transaction(5084)"
                if (punch.PunchStatisticsTransactionAdd(autIntNo, LastUser, PunchStatisticsTranTypeList.PresentationOfDocument, PunchAction.Add) <= 0)
                {
                    model.AddError(punch.ErrorMessage);
                    return false;
                }

                return true;
            }, ex => model.AddError(ex.Message));
            if (!scope)
            {
                model.IsSuccessful = false;
                return model;
            }

            return model;
        }

        #endregion

        #region Operations

        protected override bool BuildDetails(ref RepresentationModel model, int repCode)
        {
            return false;
        }

        protected override bool RepresentationUpdate(ref RepresentationModel model)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Set model's values: RepresentationCode, RepOfficial, RepRecommend, RepDate, RepOffenderName, RepOffenderAddress,
        ///     RepOffenderTelNo, RepType, ChgIntNo, Charge.ChargeStatus, Charge.RowVersion_Long, ChangeOfOffender, RepDetails,
        ///     LetterTo, IsSummons, ChangeOfRegNo, Notice.NotIntNo
        /// </summary>
        protected override bool RepresentationDecide(ref RepresentationModel model)
        {
            if (!CheckModel(ref model, true)) return false;

            string errorMsg;
            var result = representationDB.DecideRepresentationForPod(out errorMsg,
                model.RepresentationCode.GetValueOrDefault(),
                model.RepOfficial ?? "",
                model.RepRecommend ?? "",
                model.RepDate.GetValueOrDefault(DateTimeNow),
                model.RepOffenderName ?? "",
                model.RepOffenderAddress ?? "",
                model.RepOffenderTelNo ?? "",
                model.RepType,
                LastUser,
                model.ChgIntNo,
                model.Charge.ChargeStatus,
                model.Charge.RowVersion_Long,
                model.ChangeOfOffender ?? "",
                model.RepDetails ?? "",
                model.LetterTo ?? "",
                model.IsSummons,
                model.ChangeOfRegNo ?? "",
                model.Notice.NotIntNo,
                Rules.AR_4660.GetValueOrDefault(),
                RepAction.Is(RepAction.PresentationOfDocument));

            if (result == null || result.Count <= 0 || result[0] <= 0)
            {
                model.AddError(string.Format(RepresentationResx.TheRepresentationCannotBeDecidedAsThereWasAnError_POD, errorMsg));
                return false;
            }

            model.RepIntNo = result[0];
            return model.IsSuccessful;
        }

        protected override bool RepresentationReverse(ref RepresentationModel model)
        {
            model.AddError(RepresentationResx.ItIsNotAllowedToPerformThisAction);
            return false;
        }

        static T GetMandatoryValue<T>(object value, T mandatoryValue = default(T))
        {
            if (value == null
                || value == DBNull.Value
                || (value is string && string.IsNullOrWhiteSpace((string)value))) return mandatoryValue;
            var type = typeof(T);
            if (type.IsGenericType && typeof(Nullable<>) == type.GetGenericTypeDefinition())
                type = new NullableConverter(type).UnderlyingType;
            return (T)Convert.ChangeType(value, type);
        }

        bool HasRep(ref ChargeModel charge)
        {
            if (charge.RepresentationList.Count(r => r.RepIntNo > 0) <= 0)
                return false;
            FilterRepresentation(charge);
            return charge.RepresentationList.Count(r => r.RepIntNo > 0) > 0;
        }

        #endregion
    }
}
