﻿using System;
using SIL.AARTO.Web.Resource.Representation;

namespace SIL.AARTO.BLL.Representation
{
    public class ArgumentException2 : Exception
    {
        public ArgumentException2(string name, object value, Exception innerException = null)
            : base(string.Format(RepresentationResx.InvalidParameter, value ?? "null", name), innerException) {}
    }
}
