﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Services;
using System.Collections;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class AuthorityManager
    {
        private static readonly AuthorityService service = new AuthorityService();
        public static SelectList GetSelectList()
        {
            List<AuthorityEntity> authorityList = new List<AuthorityEntity>();
            foreach (Authority aut in service.GetAll())
            {
                authorityList.Add(new AuthorityEntity(){
                    AutIntNo = aut.AutIntNo,
                    AutName = aut.AutName,
                    AutCode = aut.AutCode,
                    AutDesc = aut.AutName + " (" + aut.AutCode.Trim() + ")"
                });
            }
            return new SelectList(authorityList, "AutIntNo", "AutDesc");
        }

        public static string GetAuthCodeByAutIntNo(int autIntNo)
        {
            return service.GetByAutIntNo(autIntNo).AutCode;
        }

        public static string GetAutNameByAutIntNo(int autIntNo)
        {
            return service.GetByAutIntNo(autIntNo).AutName;
        }
    }
}
