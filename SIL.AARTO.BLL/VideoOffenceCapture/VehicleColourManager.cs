﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class VehicleColourManager
    {
        private static readonly VehicleColourService service = new VehicleColourService();
        public static TList<VehicleColour> GetAll()
        {
            return service.GetAll();
        }

        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allVehicleColour = VehicleColourLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            TList<VehicleColour> allColour = GetAll();
            int vcIntNo;
            foreach (var colour in allColour)
            {
                vcIntNo = colour.VcIntNo;
                if (allVehicleColour.Keys.Contains(vcIntNo))
                {
                    list.Add(new SelectListItem() { Value = (vcIntNo).ToString(), Text = allVehicleColour[vcIntNo] });
                }
                else
                {
                    list.Add(new SelectListItem() { Value = (vcIntNo).ToString(), Text = colour.VcDescr });
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public static VehicleColour GetByVcIntNo(int vcIntNo)
        {
            return service.GetByVcIntNo(vcIntNo);
        }
    }
}
