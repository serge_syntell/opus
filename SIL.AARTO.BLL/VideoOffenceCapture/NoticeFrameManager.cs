﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class NoticeFrameManager
    {
        private static readonly NoticeFrameService service = new NoticeFrameService();
        public static NoticeFrame GetByNotIntNo(int notIntNo)
        {
            TList<NoticeFrame> noticeFrames = service.GetByNotIntNo(notIntNo);
            if (noticeFrames.Count > 0)
            {
                return noticeFrames[0];
            }
            return null;
        }
    }
}
