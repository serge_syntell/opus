﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class VideoCaptureDetailManager
    {
        private static readonly VideoCaptureDetailService service = new VideoCaptureDetailService();
        public static VideoCaptureDetail GetByVcdIntNo(long vcdIntNo)
        {
            return service.GetByVcdIntNo(vcdIntNo);
        }

        public static TList<VideoCaptureDetail> GetByVchIntNo(long vchIntNo)
        {
            return service.GetByVchIntNo(vchIntNo);
        }

        public static int Save(VideoCaptureDetail detail)
        {
            //if (service.GetByVtNoticeTicketNo(detail.VtNoticeTicketNo) != null)
            //{
            //    return -1;
            //}
            service.Save(detail);
            return 1;
        }

        public static int Update(VideoCaptureDetail detail)
        {
            VideoCaptureDetail detailOld = service.GetByVcdIntNo(detail.VcdIntNo);
            //if (detailOld.VtNoticeTicketNo != detail.VtNoticeTicketNo 
            //    && service.GetByVtNoticeTicketNo(detail.VtNoticeTicketNo) != null)
            //{
            //    return -1;
            //}
            detail.RowVersion = detailOld.RowVersion;
            service.Update(detail);
            return 1;
        }

        public static bool Delete(long vcdIntNo, out long vchIntNo)
        {
            VideoCaptureDetail detail = service.GetByVcdIntNo(vcdIntNo);
            vchIntNo = detail.VchIntNo;
            return service.Delete(detail);
        }
    }
}
