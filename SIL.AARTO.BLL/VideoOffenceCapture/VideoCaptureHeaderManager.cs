﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using System.Web.Mvc;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class VideoCaptureHeaderManager
    {
        private static readonly VideoCaptureHeaderService service = new VideoCaptureHeaderService();
        public static SelectList GetOffencesWithCode(string lsCode)
        {
            SqlDataReader reader = service.GetOffenceSelectList(lsCode) as SqlDataReader;
            return new SelectList(reader, "SelectValue", "SelectText");
        }

        public static SelectList GetOfficers(int autIntNo)
        {
            SqlDataReader reader = service.TrafficOfficerNoes(autIntNo) as SqlDataReader;
            return new SelectList(reader, "SelectValue", "SelectText");
        }

        public static VideoCaptureHeader GetNoCompleteData(string userName, int autIntNo)
        { 
            VideoCaptureHeaderQuery query = new VideoCaptureHeaderQuery();
            query.Append(VideoCaptureHeaderColumn.VchCaptureUserName, userName);
            query.Append(VideoCaptureHeaderColumn.VchSubmitStatus, "false");
            query.Append(VideoCaptureHeaderColumn.AutIntNo, autIntNo.ToString());

            TList<VideoCaptureHeader> headers = service.Find(query);
            if (headers.Count > 0)
            {
                return headers[0];
            }
            return null;
        }

        public static VideoCaptureHeader GetNoCompleteData(string userName)
        {
            VideoCaptureHeaderQuery query = new VideoCaptureHeaderQuery();
            query.Append(VideoCaptureHeaderColumn.VchCaptureUserName, userName);
            query.Append(VideoCaptureHeaderColumn.VchSubmitStatus, "false");

            TList<VideoCaptureHeader> headers = service.Find(query);
            if (headers.Count > 0)
            {
                return headers[0];
            }
            return null;
        }

        public static int Add(VideoCaptureHeader vch, out long vchIntNo)
        {
            if (service.GetByVchTapeNo(vch.VchTapeNo) != null)
            {
                vchIntNo = 0;
                return -1;
            }
            vchIntNo = service.Save(vch).VchIntNo;
            return 1;
        }

        public static int Update(VideoCaptureHeader vch)
        {
            VideoCaptureHeader oldVch = service.GetByVchIntNo(vch.VchIntNo);
            if (oldVch.VchTapeNo != vch.VchTapeNo && service.GetByVchTapeNo(vch.VchTapeNo) != null)
            {
                return -1;
            }
            vch.RowVersion = oldVch.RowVersion;
            service.Update(vch);
            return 1;
        }

        public static VideoCaptureHeader GetByVchIntNo(long vchIntNo)
        {
            return service.GetByVchIntNo(vchIntNo);
        }

    }
}
