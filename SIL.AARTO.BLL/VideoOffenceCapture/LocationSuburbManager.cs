﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using System.Web.Mvc;
using SIL.AARTO.BLL.Culture;
using Stalberg.TMS.Data;
using System.Data;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public class LocationSuburbManager
    {
        private static readonly LocationSuburbService service = new LocationSuburbService();

        public static int GetLoSuIntNoByAutIntNo(int AutIntNo)
        {
            int loSuIntNo = 0;
            LocationSuburb entity = new LocationSuburb();
            try
            {

                LocationSuburbQuery query = new LocationSuburbQuery();
                query.Append(LocationSuburbColumn.AutIntNo, AutIntNo.ToString());
                query.Append(LocationSuburbColumn.LoSuDescr, "None");


                SIL.AARTO.DAL.Entities.TList<LocationSuburb> locationSuburbList = new LocationSuburbService().Find(query as SIL.AARTO.DAL.Data.IFilterParameterCollection);
                if (locationSuburbList != null && locationSuburbList.Count > 0)
                {
                    entity = locationSuburbList[0];
                    if (entity != null)
                    {
                        loSuIntNo = entity.LoSuIntNo;
                    }

                }
                return loSuIntNo;

            }
            catch (Exception ex)
            {
                return loSuIntNo;
            }
        }

        public static SelectList GetSelectList(int authIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            #region Jerry 2013-09-12 change it
            //VehicleTypeDB vehicleType = new VehicleTypeDB(Config.ConnectionString);
            //AuthorityService autService = new AuthorityService();
            //SIL.AARTO.DAL.Entities.Authority autEntity= autService.GetByAutIntNo(authIntNo);
            //foreach (DataRow row in vehicleType.GetValueTextListByTableNameDS("LocationSuburb", autEntity.AutCode).Tables[0].Rows)
            #endregion

            LocationSuburbDB locationSuburbDB = new LocationSuburbDB(Config.ConnectionString);
            foreach (DataRow row in locationSuburbDB.GetLocationSuburbListByAutIntNo(authIntNo).Tables[0].Rows)
            {
                list.Add(new SelectListItem() { Value = (row["DTDLDValue"]).ToString(), Text = (string)row["DTDLDLText"] });
            }
            return new SelectList(list, "Value", "Text");
        }

        public static LocationSuburb GetByLocSubIntNo(int loSuIntNo)
        {
            return service.GetByLoSuIntNo(loSuIntNo);
        }

    }
}
