﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class TrafficOfficerManager
    {
        private static readonly TrafficOfficerService service = new TrafficOfficerService();
        public static TrafficOfficer GetOfficerByTOIntNo(int toIntNo)
        {
            return service.GetByToIntNo(toIntNo);
            //return service.DeepLoadByToIntNo(toIntNo,true, DAL.Data.DeepLoadType.ExcludeChildren);
        }
    }
}
