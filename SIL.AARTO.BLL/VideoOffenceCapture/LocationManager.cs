﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using System.Web.Mvc;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using System.Data;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public class LocationManager
    {
        private static readonly LocationService service = new LocationService();

        public static SelectList GetSelectList(int authIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            LocationDB courtList = new LocationDB(Config.ConnectionString);

            foreach (DataRow row in courtList.GetValueTextListByLocationTable(authIntNo).Tables[0].Rows)
            {
                list.Add(new SelectListItem() { Value = (row["DTDLDValue"]).ToString(), Text = (string)row["DTDLDLText"] });
            }
            return new SelectList(list, "Value", "Text");
        }

        //2013-12-12 Heidi added for Get Railway Locations DDL by AutIntNo (5149)
        public static SelectList GetSelectRailwayLocationsList(int authIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            LocationDB courtList = new LocationDB(Config.ConnectionString);
            DataTable dt=courtList.GetRailwayLocationsByAutIntNo(authIntNo,0,true).Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem() { Value = (row["DTDLDValue"]).ToString(), Text = (string)row["DTDLDLText"] });
            }
            return new SelectList(list, "Value", "Text");
        }

        public static string GetCrtIntNoAndLoSuIntNoByLocIntNo(int authIntNo,int LocIntNo)
        {
            string strLoSuIntNoCrtIntNo = "";
            LocationDB courtList = new LocationDB(Config.ConnectionString);
            DataSet ds = courtList.GetRailwayLocationsByAutIntNo(authIntNo, LocIntNo, false);
            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {

                strLoSuIntNoCrtIntNo = ds.Tables[0].Rows[0]["LoSuIntNo"] .ToString()+"|"+ ds.Tables[0].Rows[0]["CrtIntNo"].ToString();
            }
            return strLoSuIntNoCrtIntNo;

        }

        //Heidi 2013-12-16 Get Railway Locations by AutIntNo and LocDesc (5149)
        public static int GetLocationByLocIntNoAndIsRailwayCrossing(int authIntNo,int locIntNo)
        {
            int locIntno = 0;
            List<SelectListItem> list = new List<SelectListItem>();

            LocationDB locationDB = new LocationDB(Config.ConnectionString);
            DataSet ds = locationDB.GetLocationByLocIntNoAndIsRailwayCrossing(authIntNo, locIntNo);
            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                locIntno = Convert.ToInt32(ds.Tables[0].Rows[0]["LocIntNo"]);
            }

            return locIntno;
        }

        public static Location GetByLocIntNo(int locIntNo)
        {
            return service.GetByLocIntNo(locIntNo);
        }
    }
}
