﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class OffenceManager
    {
        private static readonly OffenceService service = new OffenceService();
        private static readonly OffenceLookupService serviceLookup = new OffenceLookupService();
        // 2014-09-03, Oscar added
        static readonly OffenceFineService offenceFineService = new OffenceFineService();

        public static Offence GetByOffIntNo(int offIntNo)
        {
            return service.GetByOffIntNo(offIntNo);
        }

        public static Offence GetByOffIntNoAndLsCode(int offIntNo, string lsCode)
        {
            Offence offence = service.GetByOffIntNo(offIntNo);
            OffenceLookup offenceLookup = serviceLookup.GetByOffIntNoLsCode(offIntNo, lsCode);
            if (offenceLookup != null)
            {
                offence.OffDescr = offenceLookup.OffDescr;
            }
            return offence;
        }

        public static float GetFineByOffIntNoAndAutIntNo(int offIntNo,string offdate, int autIntNo)
        { 
            if (GetByOffIntNo(offIntNo) != null)
            {
                OffenceFineQuery ofQuery = new OffenceFineQuery();
                ofQuery.Append(OffenceFineColumn.AutIntNo, autIntNo.ToString());
                ofQuery.Append(OffenceFineColumn.OffIntNo, offIntNo.ToString());
                //2013-05-23 Heidi added
                DateTime now = new DateTime(1900, 1, 1);
                bool isSucceed= DateTime.TryParse(offdate, out now);
                if (isSucceed)
                {
                    ofQuery.AppendLessThanOrEqual(OffenceFineColumn.OfEffectiveDate, offdate);
                }
               
                
                //SIL.AARTO.DAL.Entities.OffenceFine offenceFine = new OffenceFineService().Find(ofQuery as IFilterParameterCollection).FirstOrDefault();
                // 2014-09-03, Oscar changed
                var offenceFine = offenceFineService.Find(ofQuery, "OFEffectiveDate DESC").FirstOrDefault();
                if (offenceFine != null)
                {
                    return offenceFine.OfFineAmount;
                }
            }
            return 0;
        }
    }
}
