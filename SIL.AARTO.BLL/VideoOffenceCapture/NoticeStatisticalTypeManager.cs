﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class NoticeStatisticalTypeManager
    {
        private static readonly NoticeStatisticalTypeService service = new NoticeStatisticalTypeService();
        private static readonly NoticeStatisticalTypeLookupService serviceLookup = new NoticeStatisticalTypeLookupService();
        public static TList<NoticeStatisticalType> GetAll()
        {
            return service.GetAll();
        }

        public static SelectList GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allNoticeStatisticalType = NoticeStatisticalTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            TList<NoticeStatisticalType> allNSTypes = GetAll();
            int nstIntNo;
            foreach (var nsType in allNSTypes)
            {
                nstIntNo = nsType.NstIntNo;
                if (allNoticeStatisticalType.Keys.Contains(nstIntNo))
                {
                    list.Add(new SelectListItem() { 
                        Value = (nstIntNo).ToString(), 
                        Text = string.Format("{0} ({1})", allNoticeStatisticalType[nstIntNo],nsType.NstCode)
                    });
                }
                else
                {
                    list.Add(new SelectListItem() { 
                        Value = (nstIntNo).ToString(), 
                        Text = string.Format("{0} ({1})", nsType.NstDescription, nsType.NstCode)
                    });
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public static SelectList GetSelectListByGroup(string ntsGroupCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allNoticeStatisticalType = NoticeStatisticalTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            TList<NoticeStatisticalType> allNSTypes = GetAll();
            int nstIntNo;
            foreach (var nsType in allNSTypes)
            {
                nstIntNo = nsType.NstIntNo;
                if (nsType.NstGroupCode == ntsGroupCode)
                {
                    if (allNoticeStatisticalType.Keys.Contains(nstIntNo))
                    {
                        list.Add(new SelectListItem()
                        {
                            Value = (nstIntNo).ToString(),
                            Text = string.Format("{0} ({1})", allNoticeStatisticalType[nstIntNo], nsType.NstCode)
                        });
                    }
                    else
                    {
                        list.Add(new SelectListItem()
                        {
                            Value = (nstIntNo).ToString(),
                            Text = string.Format("{0} ({1})", nsType.NstDescription, nsType.NstCode)
                        });
                    }
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public static NoticeStatisticalType GetByNstIntNo(int nstIntNo)
        {
            return service.GetByNstIntNo(nstIntNo);
        }

        public static NoticeStatisticalType GetByNstIntNoAndLsCode(int nstIntNo, string lsCode)
        {
            NoticeStatisticalType nsType = service.GetByNstIntNo(nstIntNo);
            INoticeStatisticalTypeLookup nsTypeLookup = serviceLookup.GetByLsCodeNstIntNo(lsCode, nstIntNo);
            if (nsTypeLookup != null)
            {
                nsType.NstDescription = nsTypeLookup.NstDescription;
            }
            return nsType;
        }
    }
}
