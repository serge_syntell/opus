﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter.Data;
using SIL.AARTO.BLL.Culture;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using SIL.AARTO.DAL.Entities;
using ceTe.DynamicPDF.Merger;
using SIL.AARTO.BLL.CameraManagement;

namespace SIL.AARTO.BLL.VideoOffenceCapture
{
    public static class OffenceReport
    {
        public static byte[] GetPdfByte(string path, int vchIntNo)
        {
            DocumentLayout reportDoc = new DocumentLayout(path);
            VideoCaptureHeader header = VideoCaptureHeaderManager.GetByVchIntNo(vchIntNo);

            SetLabelValue(reportDoc, "lblAuthority", AuthorityManager.GetAutNameByAutIntNo(header.AutIntNo));
            SetLabelValue(reportDoc, "lblTapeNo", header.VchTapeNo);
            SetLabelValue(reportDoc, "lblDate", header.VchOffenceDate.ToString("yyyy-MM-dd"));
            SetLabelValue(reportDoc, "lblLocation", header.VchLocation);
            SetLabelValue(reportDoc, "lblCourt", CourtManager.GetByCrtIntNo(header.CrtIntNo).CrtName);

            Offence off = OffenceManager.GetByOffIntNo(header.OffIntNo);
            SetLabelValue(reportDoc, "lblOffence", off.OffDescr);
            SetLabelValue(reportDoc, "lblOffenceCode", off.OffCode);
            SetLabelValue(reportDoc, "lblFine", header.VchFineAmount.ToString());

            TrafficOfficer officer = TrafficOfficerManager.GetOfficerByTOIntNo(header.ToIntNo);
            SetLabelValue(reportDoc, "lblOffName", officer.TosName);
            SetLabelValue(reportDoc, "lblStaffNo", officer.ToNo);
            SetLabelValue(reportDoc, "lblStartTime", header.VchStartTime.ToString("HH:mm"));
            SetLabelValue(reportDoc, "lblEndTime", header.VchEndTime.ToString("HH:mm"));
            SetLabelValue(reportDoc, "lblCompiled", header.VchCompiledName);
            SetLabelValue(reportDoc, "lblCompiledDate",header.VchCompiledDate.ToString("yyyy-MM-dd"));
            SetLabelValue(reportDoc, "lblChecked", header.VchCheckedName);
            SetLabelValue(reportDoc, "lblCheckedDate", header.VchCheckedDate.ToString("yyyy-MM-dd"));
            SetLabelValue(reportDoc, "lblCaptured", header.VchCaptureUserName);
            SetLabelValue(reportDoc, "lblCapturedDate", header.VchCaptureDate.ToString("yyyy-MM-dd"));

            StoredProcedureQuery query = (StoredProcedureQuery)reportDoc.GetQueryById("Query");
            query.ConnectionString = Config.ConnectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("vchIntNo", vchIntNo);

            Document document = reportDoc.Run(parameters);

            return document.Draw();
        }

        private static void SetLabelValue(DocumentLayout docLay, string labelName, string value)
        {
            ((Label)docLay.GetElementById(labelName)).Text = value;
        }
    }
}
