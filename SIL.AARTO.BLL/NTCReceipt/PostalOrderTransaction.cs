﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NTCReceipt
{
    /// <summary>
    /// Represents postal order receipt
    /// </summary>
    public class PostalOrderTransaction : ReceiptTransaction
    {
        public string PostalOrderNumber{get;set;}
    }
}
