﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.NTCReceipt;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.NTCReceipt
{
    public class NTCReceiptModel
    {
        public NTCReceiptModel() { NTCReceiptCashEntity = new NTCReceiptCasheEntity(); }
        public bool ShowQuoteHeaderAddButton { get; set; }
        public bool ShowQuoteDetailPnl { get; set; }
        public bool ShowPymtpnl { get; set; }
        public int? CurrentNTCIntNo { get; set; }
        public float? TotalInputAmount { get; set; }
        public NTCReceiptCasheEntity NTCReceiptCashEntity { get; set; }
        public List<NonTrafficCharge> NotTrafficCharges { get; set; }
        public List<UnitOfMeasure> UnitOfMeasures { get; set; }
        public List<PaymentType> PymtTypes { get; set; }

    }

    [Serializable]
    public class JsonNTCReceiptDetail
    {
        public int QuDeIntNo { get; set; }
        public int QuHeIntNo { get; set; }
        public int NTCIntNo { get; set; }
        public string NTCCode { get; set; }
        public string NTCDescr { get; set; }
        public string NTCComment { get; set; }
        public int QuDeChargeQuantity { get; set; }
        public int UOMIntNo { get; set; }
        public float NTCStandardAmount { get; set; }
        public float QuDeActualAmount { get; set; }
        public float QuDeGrossAmount { get; set; }
        public string LastUser { get; set; }

    }

    [Serializable]
    public class JsonNTCReceiptHeader
    {
        public int AutIntNo { get; set; }
        public int QuHeIntNo { get; set; }
        public string QuHeReferenceNumber { get; set; }
        public string QuHeSurName { get; set; }
        public string QuHeInitials { get; set; }
        public string QuHeIDNumber { get; set; }
        public string QuHeDate { get; set; }
        public string QuHeAddress1 { get; set; }
        public string QuHeAddress2 { get; set; }
        public string QuHeAddress3 { get; set; }
        public string QuHeAddress4 { get; set; }
        public string QuHeAddress5 { get; set; }
        public string LastUser { get; set; }
        public float? TotalAmount { get; set; }
    }

    [Serializable]
    public class JsonNTCReceiptPaymentDetail
    {
        public Guid Id { get; set; }
        public string ReceiptType { get; set; }
        public decimal Amount { get; set; }
        //Cheque
        public string ChequeBank { get; set; }
        public string ChequeBranch { get; set; }
        public DateTime ChequeDate { get; set; }
        public string ChequeDrawer { get; set; }
        public int ChequeNo { get; set; }

        //Bank
        public string Reference { get; set; }

        //Card
        public string CardIssuer { get; set; }
        public string NameOnCard { get; set; }
        public DateTime ExpiryDate { get; set; }

        //Postal Order
        public string PostalOrderNumber { get; set; }

    }

    [Serializable]
    public class JsonConfirmPaymentInfo
    {
        public JsonConfirmPaymentInfo()
        {
            PaymentDetails = new List<JsonPaymentDetail>();
        }
        public string ReferenceNumber { get; set; }
        public string ReceivedFrom { get; set; }
        public string ReceivedDate { get; set; }
        public string IDNumber { get; set; }
        public string Address { get; set; }
        public float ReceivedAmount { get; set; }
        public List<JsonPaymentDetail> PaymentDetails { get; set; }
    }

    [Serializable]
    public class JsonPaymentDetail
    {
        public string PaymentType { get; set; }
        public string Descr { get; set; }
        public float Amount { get; set; }
    }

    [Serializable]
    public class NTCReceiptCasheEntity
    {
        public NTCReceiptCasheEntity()
        {
            //QuoteHeader = new JsonNTCReceiptHeader();
            QuoteDetails = new List<JsonNTCReceiptDetail>();
            PaymentDetails = new List<JsonNTCReceiptPaymentDetail>();
        }
        public JsonNTCReceiptHeader QuoteHeader { get; set; }
        public List<JsonNTCReceiptDetail> QuoteDetails { get; set; }
        public List<JsonNTCReceiptPaymentDetail> PaymentDetails { get; set; }

    }

    public class NTCReceiptManager
    {

        private static NonTrafficChargeService chgService = new NonTrafficChargeService();
        private static QuoteHeaderService qhService = new QuoteHeaderService();
        private static QuoteDetailService qdService = new QuoteDetailService();
        private static UnitOfMeasureService unitService = new UnitOfMeasureService();
        private static PaymentTypeService pytTypeService = new PaymentTypeService();

        private static string _connStr;
        public NTCReceiptManager(string connStr)
        {
            _connStr = connStr;
        }

        public static List<NonTrafficCharge> GetNonTrafficChargesByAutIntNo(int autIntNo)
        {
            List<NonTrafficCharge> charges = chgService.GetByAutIntNo(autIntNo).ToList();
            return charges;
        }

        public static List<NonTrafficCharge> GetChargesByAutIntNoForDDL(int autIntNo)
        {
            List<NonTrafficCharge> charges = GetNonTrafficChargesByAutIntNo(autIntNo);

            foreach (NonTrafficCharge c in charges)
                c.NtcDescr = String.Format("{0} ~ {1}", c.NtcCode, c.NtcDescr);

            return charges;
        }

        public static QuoteHeader GetQuoteHeaderByRefNumnber(string refNum, bool reversed = false)
        {
            return qhService.GetByQuHeReferenceNumberReversed(refNum, reversed);
        }

        public static NonTrafficCharge GetNonTrafficCharge(int ntcIntNo)
        {
            return chgService.GetByNtcIntNo(ntcIntNo);
        }

        public static List<UnitOfMeasure> GetAllUnitOfMeasure()
        {
            return unitService.GetAll().ToList();
        }

        public static List<PaymentType> GetPaymentTypes(bool needOtherType)
        {
            List<PaymentType> types = new List<PaymentType>();
            types.Add(new PaymentType() { PtCode = "Cash", PtDescription = "Cash" });
            types.Add(new PaymentType() { PtCode = "Cheque", PtDescription = "Cheque" });
            types.Add(new PaymentType() { PtCode = "CreditCard", PtDescription = "Credit Card" });
            types.Add(new PaymentType() { PtCode = "DebitCard", PtDescription = "Debit Card" });
            types.Add(new PaymentType() { PtCode = "PostalOrder", PtDescription = "Postal Order" });
            types.Add(new PaymentType() { PtCode = "BankPayment", PtDescription = "Bank Payment" });
            if (needOtherType)
                types.Add(new PaymentType() { PtCode = "Other", PtDescription = "Other" });

            return types;
        }

        public int ProcessPayment(JsonNTCReceiptHeader header, List<JsonNTCReceiptDetail> quoteDetails, List<ReceiptTransaction> receiptTransactions, out string msg)
        {

            msg = string.Empty;
            SqlConnection conn = new SqlConnection(_connStr);
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandTimeout = 0;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "AddQuotePayment";

            cmd.Parameters.AddRange(
                new SqlParameter[]{
                    new SqlParameter("@AutIntNo",SqlDbType.Int),
                    new SqlParameter("@QuHeReferenceNumber",SqlDbType.NVarChar,50),
                    new SqlParameter("@QuHeSurName",SqlDbType.NVarChar,50),
                    new SqlParameter("@QuHeInitials",SqlDbType.NVarChar,10),
                    new SqlParameter("@QuHeIDNumber",SqlDbType.NVarChar,20),
                    new SqlParameter("@QuHeDate",SqlDbType.SmallDateTime),
                    new SqlParameter("@QuHeAddress1",SqlDbType.NVarChar,100),
                    new SqlParameter("@QuHeAddress2",SqlDbType.NVarChar,100),
                    new SqlParameter("@QuHeAddress3",SqlDbType.NVarChar,100),
                    new SqlParameter("@QuHeAddress4",SqlDbType.NVarChar,100),
                    new SqlParameter("@QuHeAddress5",SqlDbType.NVarChar,100),
                    new SqlParameter("@LastUser",SqlDbType.NVarChar,50),
                    new SqlParameter("@QuoteDetailList",SqlDbType.Structured),
                    new SqlParameter("@ReceiptList",SqlDbType.Structured)
                }
            );

            cmd.Parameters[0].Value = header.AutIntNo;
            cmd.Parameters[1].Value = header.QuHeReferenceNumber;
            cmd.Parameters[2].Value = header.QuHeSurName;
            cmd.Parameters[3].Value = header.QuHeInitials;
            cmd.Parameters[4].Value = header.QuHeIDNumber;
            cmd.Parameters[5].Value = header.QuHeDate;
            cmd.Parameters[6].Value = header.QuHeAddress1;
            cmd.Parameters[7].Value = header.QuHeAddress2;
            cmd.Parameters[8].Value = header.QuHeAddress3;
            cmd.Parameters[9].Value = header.QuHeAddress4;
            cmd.Parameters[10].Value = header.QuHeAddress5;
            cmd.Parameters[11].Value = header.LastUser;
            cmd.Parameters[12].Value = GetDataQuoteDetial(quoteDetails);
            cmd.Parameters[13].Value = GetReceiptDetail(receiptTransactions);

            try
            {
                conn.Open();
                using (IDataReader result = cmd.ExecuteReader())
                {
                    while (result.Read())
                    {
                        bool succeed = result.GetInt32(1) > 0;
                        if (!succeed)
                        {
                            msg = result.GetString(2);
                        }

                        return result.GetInt32(1);
                    }
                }
            }
            catch (SqlException ex)
            {
                msg = ex.Message;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return 0;
        }

        public int ValidateCashBoxForPaymentReversal(int rctIntNo, int cbIntNo, out string msg)
        {
            msg = string.Empty;
            SqlConnection conn = new SqlConnection(_connStr);
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandTimeout = 0;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "ValidateCashBoxForQuotePaymentReversal";

            cmd.Parameters.AddRange(
                new SqlParameter[]{
                    new SqlParameter("@RctIntNo",SqlDbType.Int),
                    new SqlParameter("@CBIntNo",SqlDbType.Int),
                     
                }
            );

            cmd.Parameters[0].Value = rctIntNo;
            cmd.Parameters[1].Value = cbIntNo;

            try
            {
                conn.Open();
                using (IDataReader result = cmd.ExecuteReader())
                {
                    while (result.Read())
                    {
                        bool succeed = result.GetInt32(1) > 0;
                        if (!succeed)
                        {
                            msg = result.GetString(2);
                        }

                        return result.GetInt32(1);
                    }
                }
            }
            catch (SqlException ex)
            {
                msg = ex.Message;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return 0;
        }

        public int ProcessReverseQuotePayment(string refNumber, int rctIntNo, int cbIntNo, string reason,string lastUser, out string msg)
        {
            msg = string.Empty;
            SqlConnection conn = new SqlConnection(_connStr);
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandTimeout = 0;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "ReverseQuotePayment";

            cmd.Parameters.AddRange(
                new SqlParameter[]{
                    new SqlParameter("@CBIntNo",SqlDbType.Int),
                    new SqlParameter("@RefNumber",SqlDbType.NVarChar,50),
                    new SqlParameter("@RctIntNo",SqlDbType.Int),
                    new SqlParameter("@Reason",SqlDbType.NVarChar,200),
                    new SqlParameter("@ReversalDate",SqlDbType.SmallDateTime),
                    new SqlParameter("@LastUser",SqlDbType.NVarChar,50),
                }
            );

            cmd.Parameters[0].Value = cbIntNo;
            cmd.Parameters[1].Value = refNumber;
            cmd.Parameters[2].Value = rctIntNo;
            cmd.Parameters[3].Value = reason;
            cmd.Parameters[4].Value = DateTime.Now;
            cmd.Parameters[5].Value = lastUser;

            try
            {
                conn.Open();
                using (IDataReader result = cmd.ExecuteReader())
                {
                    while (result.Read())
                    {
                        bool succeed = result.GetInt32(1) > 0;
                        if (!succeed)
                        {
                            msg = result.GetString(2);
                        }

                        return result.GetInt32(1);
                    }
                }
            }
            catch (SqlException ex)
            {
                msg = ex.Message;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return 0;
        }

        public int AdminProcessReverseQuotePayment(string refNumber, int rctIntNo, string reason, string lastUser, out string msg)
        {
            msg = string.Empty;
            SqlConnection conn = new SqlConnection(_connStr);
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandTimeout = 0;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "AdminReverseQuotePayment";

            cmd.Parameters.AddRange(
                new SqlParameter[]{
                    new SqlParameter("@RefNumber",SqlDbType.NVarChar,50),
                    new SqlParameter("@RctIntNo",SqlDbType.Int),
                    new SqlParameter("@Reason",SqlDbType.NVarChar,200),
                    new SqlParameter("@LastUser",SqlDbType.NVarChar,50),
                }
            );

            cmd.Parameters[0].Value = refNumber;
            cmd.Parameters[1].Value = rctIntNo;
            cmd.Parameters[2].Value = reason;
            cmd.Parameters[3].Value = lastUser;

            try
            {
                conn.Open();
                using (IDataReader result = cmd.ExecuteReader())
                {
                    while (result.Read())
                    {
                        bool succeed = result.GetInt32(1) > 0;
                        if (!succeed)
                        {
                            msg = result.GetString(2);
                        }

                        return result.GetInt32(1);
                    }
                }
            }
            catch (SqlException ex)
            {
                msg = ex.Message;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return 0;
        }
        private DataColumn[] GetReceiptDetailColumnDefiniens()
        {

            return new DataColumn[]{
                new DataColumn("UserIntNo",typeof(Int32)),
                new DataColumn("BAIntNo",typeof(Int32)),
                new DataColumn("CBIntNo",typeof(Int32)),
                new DataColumn("Address1",typeof (String)),
                new DataColumn("Address2",typeof (String)),
                new DataColumn("Address3",typeof (String)),
                new DataColumn("Address4",typeof (String)),
                new DataColumn("Address5",typeof (String)),
                new DataColumn("ReceivedDate",typeof(string)),
                new DataColumn("ReceivedFrom",typeof (String)),
                new DataColumn("CashType",typeof (Char)),
                new DataColumn("ChequeNo",typeof (String)),
                new DataColumn("ChequeBank",typeof (String)),
                new DataColumn("ChequeBranch",typeof (String)),
                new DataColumn("ChequeDrawer",typeof (String)),
                new DataColumn("ChequeDate",typeof (string)),
                new DataColumn("CardIssuer",typeof (String)),
                new DataColumn("NameOnCard",typeof (String)),
                new DataColumn("ExpiryDate",typeof (string)),
                new DataColumn("ReferenceNum",typeof (String)),
                new DataColumn("PostalOrderNum",typeof (String)),
                new DataColumn("ReceivedAmount",typeof (decimal))
            };

        }

        private DataColumn[] GetQuoteDetailColumnDefiniens()
        {
            return new DataColumn[]{
                new DataColumn("NTCIntNo",typeof(Int32)),
                new DataColumn("NTCCode",typeof(String)),
                new DataColumn("NTCDescr",typeof(String)),
                new DataColumn("NTCComment",typeof (String)),
                new DataColumn("QuDeChargeQuantity",typeof (Int32)),
                new DataColumn("UOMIntNo",typeof (Int32)),
                new DataColumn("NTCStandardAmount",typeof (decimal)),
                new DataColumn("QuDeActualAmount",typeof (decimal)),
                new DataColumn("LastUser",typeof(String)) 
            };

        }

        private DataTable GetDataQuoteDetial(List<JsonNTCReceiptDetail> quoteDetails)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(GetQuoteDetailColumnDefiniens());
            DataRow dr;
            foreach (JsonNTCReceiptDetail d in quoteDetails)
            {
                dr = dt.NewRow();
                dr["NTCIntNo"] = d.NTCIntNo;
                dr["NTCCode"] = d.NTCCode;
                dr["NTCDescr"] = d.NTCDescr;
                dr["NTCComment"] = d.NTCComment;
                dr["QuDeChargeQuantity"] = d.QuDeChargeQuantity;
                dr["UOMIntNo"] = d.UOMIntNo;
                dr["NTCStandardAmount"] = d.NTCStandardAmount;
                dr["QuDeActualAmount"] = d.QuDeActualAmount;
                dr["LastUser"] = d.LastUser;
                dt.Rows.Add(dr);
            }
            return dt;
        }

        private DataTable GetReceiptDetail(List<ReceiptTransaction> receiptTransactions)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(GetReceiptDetailColumnDefiniens());
            DataRow dr;
            foreach (ReceiptTransaction d in receiptTransactions)
            {
                dr = dt.NewRow();
                dr["UserIntNo"] = d.UserIntNo;
                dr["BAIntNo"] = d.Cashier.BAIntNo;
                dr["CBIntNo"] = d.Cashier.CBIntNo;
                dr["Address1"] = d.Address1;
                dr["Address2"] = d.Address2;
                dr["Address3"] = d.Address3;
                dr["Address4"] = d.Address4;
                dr["Address5"] = d.Address5;
                dr["ReceivedDate"] = d.ReceivedDate.ToString("yyyy-MM-dd");
                dr["ReceivedFrom"] = d.ReceivedFrom;

                char cashType = Helper.GetCashTypeChar(d.CashType);
                dr["CashType"] = d.CashType;
                switch (cashType)
                {
                    case '[':  //Cash (NTC)
                    case '*':  //Other (NTC)
                        break;
                    case '{':  //Debit card (NTC)
                    case ')':  //creadit card (NTC)
                        CardTransaction tran = (CardTransaction)d;
                        dr["CardIssuer"] = tran.CardIssuer;
                        dr["NameOnCard"] = tran.NameOnCard;
                        dr["ExpiryDate"] = tran.ExpiryDate.ToString("yyyy-MM-dd"); ;
                        break;
                    //case "E":
                    //    break;
                    case '(':  //Bank account (NTC)
                        BankPaymentTransaction bankTran = (BankPaymentTransaction)d;
                        dr["ReferenceNum"] = bankTran.Reference;
                        break;
                    //case "O":
                    //    break;
                    case '}': //Postal order (NTC)
                        PostalOrderTransaction postalTran = (PostalOrderTransaction)d;
                        dr["PostalOrderNum"] = postalTran.PostalOrderNumber;
                        break;
                    case ']':  //Cheque (NTC)
                        ChequeTransaction chequeTran = (ChequeTransaction)d;
                        dr["ChequeNo"] = chequeTran.ChequeNo;
                        dr["ChequeBank"] = chequeTran.ChequeBank;
                        dr["ChequeBranch"] = chequeTran.ChequeBranch;
                        dr["ChequeDrawer"] = chequeTran.ChequeDrawer;
                        dr["ChequeDate"] = chequeTran.ChequeDate.ToString("yyyy-MM-dd");
                        break;
                };

                dr["ReceivedAmount"] = d.Amount;
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static T Mapping<T>(object from)
        {
            Type destType = typeof(T);
            Assembly a = destType.Assembly;
            object returnObj = a.CreateInstance(destType.FullName, true);

            PropertyInfo destProInfo = null; Type type = null; object soureValue = null;
            foreach (PropertyInfo sourceProType in from.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                destProInfo = typeof(T).GetProperty(sourceProType.Name, BindingFlags.Public | BindingFlags.Instance);
                if (destProInfo == null) continue;

                soureValue = sourceProType.GetValue(from, null);
                type = sourceProType.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    if (destProInfo.PropertyType.IsGenericType
                        && destProInfo.PropertyType.GetGenericTypeDefinition() != typeof(Nullable<>))
                    {
                        switch (type.FullName)
                        {
                            case "System.DateTime":
                                soureValue = soureValue == null ? "1900-01-01" : soureValue;
                                //destProInfo.SetValue(returnObj, System.Convert.ChangeType(soureValue == null ? "1900-01-01" : soureValue, sourceProType.PropertyType), null);
                                break;
                        }
                    }

                    destProInfo.SetValue(returnObj, System.Convert.ChangeType(soureValue, destProInfo.PropertyType), null);

                }

                destProInfo.SetValue(returnObj, System.Convert.ChangeType(sourceProType.GetValue(from, null), destProInfo.PropertyType), null);
            }

            return (T)returnObj;

        }

    }
}
