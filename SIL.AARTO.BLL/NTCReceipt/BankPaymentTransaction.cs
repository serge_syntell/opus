﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NTCReceipt
{

    public class BankPaymentTransaction : ReceiptTransaction
    {
        public string Reference { get; set; }
    }
}
