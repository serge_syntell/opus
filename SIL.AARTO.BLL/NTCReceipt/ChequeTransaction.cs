﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NTCReceipt
{
    /// <summary>
    /// Represents a cash or cheque receipt
    /// </summary>
    public class ChequeTransaction : ReceiptTransaction
    {
        public string ChequeBank { get; set; }
        public string ChequeBranch { get; set; }
        public DateTime ChequeDate { get; set; }

        public string ChequeDrawer { get; set; }

        public int ChequeNo { get; set; }
    }

}
