﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NTCReceipt
{

    /// <summary>
    /// Represents a credit or debit card receipt
    /// </summary>
    public class CardTransaction : ReceiptTransaction
    {
        public string CardIssuer { get; set; }
        public string NameOnCard { get; set; }
        public DateTime ExpiryDate { get; set; }


    }
}
