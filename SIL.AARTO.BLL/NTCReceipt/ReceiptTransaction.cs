﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NTCReceipt
{
    /// <summary>
    /// Represents the transaction items details for a payment receipt
    /// </summary>
    public class ReceiptTransaction : ReceiptBase
    {
        public int UserIntNo { get; set; }
        public int AutIntNo { get; set; }
        public int AccIntNo { get; set; }
        //public int ChgIntNo;
        public string Details { get; set; }
        //public string TicketNo;
        public int BAIntNo { get; set; }
        public int CBIntNo { get; set; }
        public string ContactNumber { get; set; }
        //public List<ReceiptCharge> Charges = new List<ReceiptCharge>();
        public bool AddressRequired { get; set; }
        public int MaxStatus { get; set; }
        //public string ReceiptType;
    }
}
