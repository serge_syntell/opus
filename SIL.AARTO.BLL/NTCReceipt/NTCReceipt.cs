﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.NTCReceipt
{
    /// <summary>
    /// Represents the base class for all receipts
    /// </summary>
    public class ReceiptBase
    {
        public DateTime ReceivedDate { get; set; }
        public string ReceivedFrom { get; set; }
        public decimal Amount { get; set; }
        //public decimal ContemptAmount;
        //public decimal FineAmount;
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Address5 { get; set; }
        public string AddressAreaCode { get; set; }
        public string ReceiptNo { get; set; }
        public CashType CashType;
        public Cashier Cashier { get; set; }
        //public bool IsOneReceiptPerNotice = false;
    }
}
