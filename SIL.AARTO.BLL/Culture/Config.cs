﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Configuration;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.Culture
{
    public static class Config
    {
        public static string UserLanguageCookieName = "AARTOUserLanguage";
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;
        public static int PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);

        private static Dictionary<string, string> supportedLanguages;
        private static object lockObj = new object();

        public static Dictionary<string, string> SupportedLanguages
        { 
            get
            {
                if(supportedLanguages == null)
                {
                    lock (lockObj)
                    {
                        if (supportedLanguages == null)
                        {
                            TList<LanguageSelector> tLanguages = LanguageSelectorCache.GetAll();
                            supportedLanguages = new Dictionary<string, string>();
                            for (int i = 0; i < tLanguages.Count; i++)
                            {
                                supportedLanguages.Add(tLanguages[i].LsCode, tLanguages[i].LsDescription);
                            }
                        }
                    }
                }
                return supportedLanguages;
            }
        }

        public static string DefaultLanguage
        {
            get
            {
                TList<LanguageSelector> tLanguages = LanguageSelectorCache.GetAll();
                for (int i = 0; i < tLanguages.Count; i++)
                {
                    if (tLanguages[i].LsIsDefault)
                    {
                        return tLanguages[i].LsCode;
                    }
                }
                return "en-US";
            }
        }

        public static void ProcessMultiLanguage(HttpContext current)
        {
            string userLanguage = null;
            try
            {
                if (current.Session != null && current.Session["UserLoginInfo"] != null)
                {
                    userLanguage = ((UserLoginInfo)current.Session["UserLoginInfo"]).CurrentLanguage;
                }
                else if (current.Request.UserLanguages != null)
                {
                    foreach (var language in current.Request.UserLanguages)
                    {
                        if (Config.SupportedLanguages.ContainsKey(language))
                        {
                            userLanguage = language;
                            ((UserLoginInfo)current.Session["UserLoginInfo"]).CurrentLanguage = userLanguage;
                            break;
                        }
                    }
                }
                if (userLanguage == null)
                {
                    userLanguage = Config.DefaultLanguage;
                    ((UserLoginInfo)current.Session["UserLoginInfo"]).CurrentLanguage = userLanguage;
                }
                
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(userLanguage);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
            catch (Exception)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Config.DefaultLanguage);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
        }

        public static void ProcessMultiLanguage(HttpRequest request)
        {
            string userLanguage = null;
            if (request.Cookies[Config.UserLanguageCookieName] == null)
            {
                if (request.UserLanguages != null)
                {
                    foreach (var language in request.UserLanguages)
                    {
                        if (Config.SupportedLanguages.ContainsKey(language))
                        {
                            userLanguage = language;
                            break;
                        }
                    }
                }
                if (userLanguage == null)
                {
                    userLanguage = Config.DefaultLanguage;
                }
            }
            else
            {
                userLanguage = request.Cookies[Config.UserLanguageCookieName].Value;
            }


            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(userLanguage);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
            catch (Exception)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Config.DefaultLanguage);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }

        }

        // 2015-01-15, Oscar added (bontq 1771, ref 1770)
        public static void ResetCultureNumberFormat()
        {
            var newCulture = CultureInfo.ReadOnly(new CultureInfo(Thread.CurrentThread.CurrentCulture.Name) { NumberFormat = { NumberDecimalSeparator = ".", CurrencyDecimalSeparator = ".", PercentDecimalSeparator = "." } });

            PropertyInfo defaultCulture;
            FieldInfo userdefaultCulture;
            if ((defaultCulture = typeof(CultureInfo).GetProperty("DefaultThreadCurrentCulture",
                BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)) != null)
                defaultCulture.SetValue(null, newCulture, null);
            else if ((userdefaultCulture = typeof(CultureInfo).GetField("m_userDefaultCulture",
                BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic)) != null)
                userdefaultCulture.SetValue(null, newCulture);

            Thread.CurrentThread.CurrentCulture = newCulture;
        }
    }
}
