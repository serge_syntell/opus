﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace SIL.AARTO.BLL.SummonsManagement
{
    public class StagnantSummonsManager
    {
        readonly string _connectString = string.Empty;
        public StagnantSummonsManager(string conn)
        {
            this._connectString = conn;
        }

        public int GetStagnantSummonsCount(string notTicketNo, string summonsNo, int crtIntNo, int crtRIntNo, 
            DateTime? startCourtDate, DateTime? endCourtDate,int daysForExpirySummons)
        {
            int rowCount = 0;
            using (SqlConnection conn = new SqlConnection(this._connectString))
            {
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetStagnantSummonsCount";

                SqlParameter[] paras = new SqlParameter[] { 
                    new SqlParameter("@NotTicketNo",SqlDbType.NVarChar,50),
                    new SqlParameter("@SummonsNo",SqlDbType.NVarChar,50),
                    new SqlParameter("@CrtIntNo",SqlDbType.Int),
                    new SqlParameter("@CrtRIntNo",SqlDbType.Int),
                    new SqlParameter("@StartDate",SqlDbType.SmallDateTime),
                    new SqlParameter("@EndDate",SqlDbType.SmallDateTime),
                    new SqlParameter("@DaysForExpirySummons",SqlDbType.Int)
                };
                paras[0].Value = notTicketNo;
                paras[1].Value = summonsNo;
                paras[2].Value = crtIntNo;
                paras[3].Value = crtRIntNo;
                paras[4].Value = startCourtDate;
                paras[5].Value = endCourtDate;
                paras[6].Value = daysForExpirySummons;

                cmd.Parameters.AddRange(paras);

                try
                {
                    conn.Open();

                    IDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (result.Read())
                    {
                        rowCount = result.GetInt32(0);
                        break;
                    }
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }

            return rowCount;
        }

        public DataTable GetStagnantSummons(string notTicketNo, string summonsNo, int crtIntNo, int crtRIntNo,
            DateTime? startCourtDate, DateTime? endCourtDate, int maxNumberToProcess, int daysForExpirySummons)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[] { 
                new DataColumn("NotIntNo"),
                new DataColumn("SumIntNo"),
                new DataColumn("AutCode"),
                new DataColumn("CrtName"),
                new DataColumn("NotFilmType"),
                new DataColumn("NotOffenceDate"),
                new DataColumn("SummonsNo")
            });

            using (SqlConnection conn = new SqlConnection(this._connectString))
            {
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SearchStagnantSummons";

                SqlParameter[] paras = new SqlParameter[] { 
                    new SqlParameter("@NotTicketNo",SqlDbType.NVarChar,50),
                    new SqlParameter("@SummonsNo",SqlDbType.NVarChar,50),
                    new SqlParameter("@CrtIntNo",SqlDbType.Int),
                    new SqlParameter("@CrtRIntNo",SqlDbType.Int),
                    new SqlParameter("@StartDate",SqlDbType.SmallDateTime),
                    new SqlParameter("@EndDate",SqlDbType.SmallDateTime),
                    new SqlParameter("@MaxNumberOfProcess",SqlDbType.Int),
                    new SqlParameter("@DaysForExpirySummons",SqlDbType.Int)
                };
                paras[0].Value = notTicketNo;
                paras[1].Value = summonsNo;
                paras[2].Value = crtIntNo;
                paras[3].Value = crtRIntNo;
                paras[4].Value = startCourtDate;
                paras[5].Value = endCourtDate;
                paras[6].Value = maxNumberToProcess;
                paras[7].Value = daysForExpirySummons;
                cmd.Parameters.AddRange(paras);

                try
                {
                    conn.Open();

                    IDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (result.Read())
                    {
                        DataRow dr = dt.NewRow();
                        dr["NotIntNo"] = result.GetInt32(0);
                        dr["SumIntNo"] = result.GetInt32(1);
                        dr["AutCode"] = result["AutCode"].ToString();
                        dr["CrtName"] = result["CrtName"].ToString();
                        dr["NotFilmType"] = result["NotFilmType"];
                        dr["NotOffenceDate"] = result["NotOffenceDate"];
                        dr["SummonsNo"] = result["SummonsNo"];
                        dt.Rows.Add(dr);
                    }
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }

            return dt;
        }

        public int WithDrawForReIssueSummons(int notIntNo, int sumIntNo,int daysForExpirySummons, string lastUser)
        {
            int returnValue = 0;

            using (SqlConnection conn = new SqlConnection(this._connectString))
            {
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "StagnantSummonsWithdrawnForReIssue";

                SqlParameter[] paras = new SqlParameter[] { 
                    new SqlParameter("@NotIntNo",SqlDbType.Int),
                    new SqlParameter("@SumIntNo",SqlDbType.Int),
                    new SqlParameter("@LastUser",SqlDbType.NVarChar,50),
                    new SqlParameter("@DaysForExpirySummons",SqlDbType.Int)
                };

                paras[0].Value = notIntNo;
                paras[1].Value = sumIntNo;
                paras[2].Value = lastUser;
                paras[3].Value = daysForExpirySummons;
                cmd.Parameters.AddRange(paras);
                try
                {
                    conn.Open();
                    IDataReader result = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    while (result.Read())
                    {
                        //processedCount = result.GetInt32(0);
                        returnValue = result.GetInt32(0);
                        break;
                    }
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }

            return returnValue;
        }


    }
}
