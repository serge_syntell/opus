﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.Report
{
    public class OfficerStatisticsReport
    {
        private string connectionString;
        private HSSFWorkbook wb;
        private ISheet sheet;

        private ICell cell;
        private ICellStyle dateStyle;
        private ICellStyle doubleStyle;
        private ICellStyle stringStyle;

        public OfficerStatisticsReport(string vConstr)
        {
            connectionString = vConstr;
            wb = new HSSFWorkbook();

            this.dateStyle = wb.CreateCellStyle();
            IDataFormat format = wb.CreateDataFormat();
            this.dateStyle.DataFormat = format.GetFormat("yyyy-MM-dd");
            this.doubleStyle = wb.CreateCellStyle();
            this.doubleStyle.DataFormat = format.GetFormat("#,##0.00");
            this.stringStyle = wb.CreateCellStyle();
            this.stringStyle.Alignment = HorizontalAlignment.RIGHT;
        }

        public MemoryStream ExportToExcel(int AutIntNo, string officerNumber, DateTime before, DateTime after)
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                sheet = wb.CreateSheet();
                wb.SetSheetName(0, "Officer Statistics Report");

                DataSet ds = this.GetReportData(AutIntNo, officerNumber, before, after);

                this.createHeadings();

                IRow row;
                int rowIndex = 1;

                for(int i=0;i<ds.Tables[0].Rows.Count;i++)
                {
                    row = sheet.CreateRow(rowIndex++);

                    this.createDateCell(row, DateTime.Parse(ds.Tables[0].Rows[i]["USendDate"].ToString()));
                    this.createStringCell(row, 1, ds.Tables[0].Rows[i]["USOfficerNumber"].ToString());
                    this.createStringCell(row, 2, ds.Tables[0].Rows[i]["Name"].ToString());
                    this.createIntCell(row, 3, int.Parse(ds.Tables[0].Rows[i]["Adjudications"].ToString()));
                    this.createIntCell(row, 4, int.Parse(ds.Tables[0].Rows[i]["Minutes"].ToString()));
                    this.createDoubleCell(row, double.Parse(ds.Tables[0].Rows[i]["AverageHour"].ToString()));
                }

                sheet.SetColumnWidth(0, 11 * 256);
                for (short i = 1; i < 5; i++)
                {
                    sheet.AutoSizeColumn(i);
                }

                this.addGeneratedCell(++rowIndex);

                wb.Write(stream);
                return stream;
            }
            catch (Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, "TMS");
                throw e;
            }
        }

        private DataSet GetReportData(int AutIntNo, string officerNumber, DateTime before, DateTime after)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetUserShiftStatistics", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = AutIntNo;
            com.Parameters.Add("@USOfficerNumber", SqlDbType.VarChar, 10).Value = officerNumber;
            com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime).Value = before;
            com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime).Value = after;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        private void addGeneratedCell(int rowIndex)
        {
            String value = "Generated on: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            this.createStringCell(sheet.CreateRow(rowIndex), 0, value);

            CellRangeAddress region = new CellRangeAddress(rowIndex, rowIndex, 0, 5);
            sheet.AddMergedRegion(region);
        }

        private void createDateCell(IRow row, DateTime date)
        {
            this.cell = row.CreateCell(0);
            this.cell.SetCellValue(date);
            this.cell.CellStyle = dateStyle;
        }

        private void createDoubleCell(IRow row, double value)
        {
            this.cell = row.CreateCell(5);
            this.cell.SetCellValue(value);
            this.cell.CellStyle = doubleStyle;
        }

        private void createHeadings()
        {
            ICellStyle style = wb.CreateCellStyle();
            style.Rotation = (short)90;
            style.Alignment = HorizontalAlignment.CENTER;
            IFont font = wb.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;

            IRow row = sheet.CreateRow(0);
            cell = row.CreateCell(0);
            HSSFRichTextString rtf = new HSSFRichTextString("Day");
            rtf.ApplyFont(font);
            cell.SetCellValue(rtf);
            cell.CellStyle = style;

            cell = row.CreateCell(1);
            rtf = new HSSFRichTextString("Officer Number");
            rtf.ApplyFont(font);
            cell.SetCellValue(rtf);
            cell.CellStyle = style;

            cell = row.CreateCell(2);
            rtf = new HSSFRichTextString("Officer Name");
            rtf.ApplyFont(font);
            cell.SetCellValue(rtf);
            cell.CellStyle = style;

            cell = row.CreateCell(3);
            rtf = new HSSFRichTextString("#Adjudicated");
            rtf.ApplyFont(font);
            cell.SetCellValue(rtf);
            cell.CellStyle = style;

            cell = row.CreateCell(4);
            rtf = new HSSFRichTextString("Minutes");
            rtf.ApplyFont(font);
            cell.SetCellValue(rtf);
            cell.CellStyle = style;

            cell = row.CreateCell(5);
            rtf = new HSSFRichTextString("Average/Hour");
            rtf.ApplyFont(font);
            cell.SetCellValue(rtf);
            cell.CellStyle = style;
        }

        private void createIntCell(IRow row, int colIndex, int value)
        {
            this.cell = row.CreateCell(colIndex);
            this.cell.SetCellValue(value);
        }

        private void createStringCell(IRow row, int colIndex, String value)
        {
            this.cell = row.CreateCell(colIndex);
            this.cell.SetCellValue(new HSSFRichTextString(value));
            this.cell.CellStyle = this.stringStyle;
        }
    }
}
