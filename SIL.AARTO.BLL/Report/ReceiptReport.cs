﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;

using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;

namespace SIL.AARTO.BLL.Report
{
    public class ReceiptReport
    {
        private string connectionString;
        private HSSFWorkbook wb;

        private String[] paymentTypes = new String[]
        {
            "Unknown", 
            "Cash",
            "Cheque",
            "Card",
            "Court Payment",
            "Postal",
            "Direct",
            "DebitCard",
            "Electronic Payment",
            "Payfine",
            "EasyPay",
            "RoadBlock Cash",
            "Roadblock Cheque",
            "Roadblock Card",
            "Bank Payment",
            "Admin Receipt",
            "HRK Cash",
            "HRK BankAccount",
            "HRK Cheque",
            "HRK CreditCard",
            "HRK DebitCard",
            "HRK Other",
            "NTC Cash",//2015-02-03 Heidi added for Compatible with Ntc payment data(5384)
            "NTC Cheque",
            "NTC CreditCard",
            "NTC DebitCard",
            "NTC Postal",
            "NTC BankAccount",
            "NTC Other"
        };

        private int day = -1;
        private int month = -1;
        private int year = -1;
        private double dailyTotal = 0;
        private double monthlyTotal = 0;

        private IFont heading;
        private ICellStyle headingStyle;
        private ICellStyle dateStyle;
        private ICellStyle numberStyle;
        private ICellStyle totalNumberStyle;
        private ICellStyle lineStyle;

        public ReceiptReport(string vConstr)
        {
            connectionString = vConstr;
            wb = new HSSFWorkbook();

            this.heading = this.wb.CreateFont();
            this.heading.Boldweight = (short)FontBoldWeight.BOLD;
            this.headingStyle = this.wb.CreateCellStyle();
            this.headingStyle.Alignment = HorizontalAlignment.CENTER;
            this.headingStyle.VerticalAlignment = VerticalAlignment.CENTER;
            this.dateStyle = this.wb.CreateCellStyle();
            IDataFormat format = this.wb.CreateDataFormat();
            this.dateStyle.DataFormat = (wb.CreateDataFormat().GetFormat("yyyy-MM-dd"));
            this.numberStyle = this.wb.CreateCellStyle();
            this.numberStyle.DataFormat =(wb.CreateDataFormat().GetFormat("0.00"));
            this.totalNumberStyle = this.wb.CreateCellStyle();
            this.totalNumberStyle.DataFormat =(wb.CreateDataFormat().GetFormat("0.00"));
            this.totalNumberStyle.DataFormat = (wb.CreateDataFormat().GetFormat("0.00"));
            this.totalNumberStyle.SetFont(heading);
            this.lineStyle = this.wb.CreateCellStyle();
            this.lineStyle.BorderTop = BorderStyle.THICK;
        }

        public MemoryStream ExportToExcel(int userIntNo, string authority, int autIntNo, DateTime fromDate, DateTime toDate)
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                ISheet sheet = wb.CreateSheet("Receipt Report for " + authority);

                this.createHeadings(sheet);

                int row = 3;
                DataSet ds = this.GetReportData(userIntNo, autIntNo, fromDate, toDate);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    this.createReceiptRow(sheet, ds.Tables[0].Rows[i], row);
                    row++;
                }

                this.createRunningtotals(sheet, row, 0, -2, -2, -2);

                // Totals
                row = this.createReceiptTotals(sheet, row);

                // Summary
                row = this.createSummary(sheet, row);

                // size columns
                sheet.AutoSizeColumn((short)0);
                sheet.AutoSizeColumn((short)1);
                sheet.AutoSizeColumn((short)2);
                sheet.AutoSizeColumn((short)3);
                sheet.AutoSizeColumn((short)4);
                sheet.AutoSizeColumn((short)5);//2015-02-03 Heidi added for Compatible with Ntc payment data(5384)
                sheet.AutoSizeColumn((short)6);
                sheet.AutoSizeColumn((short)7);
                sheet.AutoSizeColumn((short)8);
                sheet.AutoSizeColumn((short)9);
                sheet.AutoSizeColumn((short)10);
                sheet.AutoSizeColumn((short)12);
                sheet.AutoSizeColumn((short)13);
                sheet.AutoSizeColumn((short)14);

                // Date stamp
                row += 2;
                sheet.CreateRow(row);
                this.createNormalCell(sheet, row, 0, "Generated : " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

                wb.Write(stream);
                return stream;
            }
            catch(Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, "TMS");
                throw e;
            }
        }

        private DataSet GetReportData(int userIntNo, int autIntNo, DateTime fromDate, DateTime toDate)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("ExtractReceiptsByDate", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = userIntNo;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime).Value = fromDate;
            com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime).Value = toDate;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        private void createHeadings(ISheet sheet)
        {
            sheet.CreateRow(1);

            this.createHeadingCell(sheet, 1, 1, "Authority");
            this.createHeadingCell(sheet, 1, 2, "Cashier");
            this.createHeadingCell(sheet, 1, 3, "Date");
            this.createHeadingCell(sheet, 1, 4, "Ticket");
            this.createHeadingCell(sheet, 1, 5, "NTC");//2015-02-03 Heidi added for Compatible with Ntc payment data(5384)
            this.createHeadingCell(sheet, 1, 6, "Receipt No.");
            this.createHeadingCell(sheet, 1, 7, "Contempt Amount");
            this.createHeadingCell(sheet, 1, 8, "Fine Amount");
            this.createHeadingCell(sheet, 1, 9, "Total Amount");
            this.createHeadingCell(sheet, 1, 10, "Type");
            this.createHeadingCell(sheet, 1, 12, "Daily");
            this.createHeadingCell(sheet, 1, 13, "Monthly");
            this.createNormalCell(sheet, 1, 14, "(* Daily and Monthly totals exclude Contempt amounts)");
        }

        private void createHeadingCell(ISheet sheet, int row, int column, string title)
        {
            ICell cell = sheet.GetRow(row).CreateCell(column);
            cell.CellStyle = this.headingStyle;
            HSSFRichTextString rts = new HSSFRichTextString(title);
            rts.ApplyFont(this.heading);
            cell.SetCellValue(rts);
        }

        private void createNormalCell(ISheet sheet, int row, int column, Object value)
        {
            ICell cell = sheet.GetRow(row).CreateCell(column);
            if (value is string)
            {
                HSSFRichTextString rts = new HSSFRichTextString((string)value);
                cell.SetCellValue(rts);
                return;
            }
            if (value is DateTime)
            {
                cell.CellStyle = this.dateStyle;
                cell.SetCellValue((DateTime)value);
                return;
            }
            if (value is double)
            {
                cell.CellStyle = this.numberStyle;
                cell.SetCellValue((double)value);
                return;
            }
        }

        private void createReceiptRow(ISheet sheet, DataRow rs, int row)
        {
            sheet.CreateRow(row);

            String authority = "";
            String cashier = "";
            DateTime date = DateTime.Now;
            String ticket = null;
            String ntc = null;//2015-02-03 Heidi added for Compatible with Ntc payment data(5384)
            String receipt = null;
            Double contempt = 0;
            Double amount = 0;
            Double total = 0;
            String cashType = null;

            authority = rs["Authority"].ToString();
            cashier = rs["Cashier"].ToString();
            date = (DateTime)rs["Date"];
            ticket = rs["Ticket"].ToString();
            ntc = rs["NTC"].ToString();//2015-02-03 Heidi added for Compatible with Ntc payment data(5384)
            receipt = rs["Receipt No."].ToString();
            contempt = double.Parse(rs["Contempt Amount"].ToString());
            amount = double.Parse(rs["Fine Amount"].ToString());
            total = double.Parse(rs["Total Amount"].ToString());
            cashType = rs["Type"].ToString();

            this.createNormalCell(sheet, row, 1, authority);
            this.createNormalCell(sheet, row, 2, cashier);
            this.createNormalCell(sheet, row, 3, date);
            this.createNormalCell(sheet, row, 4, ticket);
            this.createNormalCell(sheet, row, 5, ntc);//2015-02-03 Heidi added for Compatible with Ntc payment data(5384)
            this.createNormalCell(sheet, row, 6, receipt);
            this.createNormalCell(sheet, row, 7, contempt);
            this.createNormalCell(sheet, row, 8, amount);
            this.createNormalCell(sheet, row, 9, total);
            this.createNormalCell(sheet, row, 10, cashType);

            this.createRunningtotals(sheet, row, amount, date.Day, date.Month, date.Year);
        }

        private void createRunningtotals(ISheet sheet, int row, double amount, int day, int month, int year)
        {
            // Daily totals
            if (this.day != day || this.month != month || this.year != year)
            {
                if (this.day != -1)
                {
                    this.createNormalCell(sheet, row - 1, 12, this.dailyTotal);//2015-02-03 Heidi changed for Compatible with Ntc payment data(5384)
                    this.dailyTotal = 0;
                }
                this.day = day;
            }
            this.dailyTotal += amount;

            // Monthly totals
            if (this.month != month || this.year != year)
            {
                if (this.month != -1)
                {
                    this.createNormalCell(sheet, row - 1, 13, this.monthlyTotal);//2015-02-03 Heidi changed for Compatible with Ntc payment data(5384)
                    this.monthlyTotal = 0;
                }
                this.month = month;
            }
            this.monthlyTotal += amount;

            if (this.year != year)
            {
                this.year = year;
            }
        }

        private int createReceiptTotals(ISheet sheet, int row)
        {
            row += 1;
            sheet.CreateRow(row);

            this.createHeadingCell(sheet, row, 0, "Totals");

            this.createTotalFormula(sheet, row, 7, 4);//2015-02-03 Heidi changed for Compatible with Ntc payment data(5384)
            this.createTotalFormula(sheet, row, 8, 4);
            this.createTotalFormula(sheet, row, 9, 4);
            this.createTotalFormula(sheet, row, 12, 4);
            this.createTotalFormula(sheet, row, 13, 4);

            row += 2;
            sheet.CreateRow(row);

            ICell cell;
            for (int i = 0; i < 10; i++)
            {
                cell = sheet.GetRow(row).CreateCell(i);
                cell.CellStyle = this.lineStyle;
            }

            return row;
        }

        private int createSummary(ISheet sheet, int row)
        {
            int lastDataRow = row - 3;
            row += 1;
            sheet.CreateRow(row);

            this.createHeadingCell(sheet, row, 1, "Summary By Payment Type");
            this.createHeadingCell(sheet, row, 8, "Total");

            row += 2;
            for (int i = 0; i < this.paymentTypes.Length; i++)
            {
                row++;
                sheet.CreateRow(row);

                this.createNormalCell(sheet, row, 1, this.paymentTypes[i]);

                this.createSummaryTotalFormulaCell(sheet, row, this.paymentTypes[i], lastDataRow);
            }

            row++;
            sheet.CreateRow(row);
            this.createHeadingCell(sheet, row, 0, "Totals");
            this.createTotalFormula(sheet, row, 8, row - (this.paymentTypes.Length - 1));

            return row;
        }

        private void createSummaryTotalFormulaCell(ISheet sheet, int row, string paymentType, int lastDataRow)
        {
            ICell cell = sheet.GetRow(row).CreateCell(8);
            // NB - in POI you must use commas to separate arguments, while Excel uses semi-colons
            //string formula = string.Format("SUMIF(J4:J{1,number,####},\"{0}\",I4:I{1,number,####})", paymentType, lastDataRow);
            //2015-02-03 Heidi changed for Compatible with Ntc payment data(5384)
            string formula = "SUMIF(K4:J" + lastDataRow.ToString() + ",\"" + paymentType + "\",J4:I" + lastDataRow.ToString() + ")";
            cell.SetCellFormula(formula);
            cell.CellStyle = this.numberStyle;
        }

        private void createTotalFormula(ISheet sheet, int row, int column, int startRow)
        {
            ICell cell = sheet.GetRow(row).CreateCell(column);
            string columnLetter = this.getColumnString(column);
            //string format = string.Format("SUM({1}{2,number,####}:{1}{0,number,####})", row, columnLetter, startRow);
            string format = "SUM(" + columnLetter + startRow.ToString() + ":" + columnLetter + row.ToString() + ")";
            cell.CellStyle = this.totalNumberStyle;
            cell.SetCellFormula(format);
        }


        private string getColumnString(int column)
        {
            char[] A2Z =
            {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
            };
            StringBuilder retval = new StringBuilder();
            int tempcellnum = column;
            do
            {
                retval.Insert(0, A2Z[tempcellnum % 26]);
                tempcellnum = (tempcellnum / 26) - 1;
            }
            while (tempcellnum >= 0);

            return retval.ToString();
        }
    }
}
