﻿using System;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.IO;
using SIL.AARTO.BLL.Report.Model;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.PageElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF;
using System.Xml;
using System.Configuration;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.BLL.Report
{
    public class PDFExporter
    {
        public PDFExporter()
        {
            // Add the DynamicPDF License
            ceTe.DynamicPDF.Document.AddLicense("DPS70NPDFJJGEJFZ9ikRGHBua2M3Sl0Pwtw63QxBds5agxfAQMSTfCfEtiI2O6I13jHEsL6smMhYn63QJBLCZ3B8XtMbaTRJ+C1w");
        }

        private void setupPDFDocumentTemplate(Template template, Page page)
        {
            float x, y, w, h;

            String token = "%%CP(1)%% of %%TP(1)%%";
            x = page.Dimensions.Body.Width - 100;
            y = page.Dimensions.Body.Height - 25;
            w = 100;
            h = 25;
            PageNumberingLabel labelPN = new PageNumberingLabel(
              token, x, y, w, h, ceTe.DynamicPDF.Font.Helvetica, 12, TextAlign.Right);
            template.Elements.Add(labelPN);
        }

        private void setupPDFFooter(Page page, float offsetY)
        {
            //String strDB = "AARTO Report";
            //Label labelDB = new Label(strDB, 0, offsetY, 400, 100, ceTe.DynamicPDF.Font.Helvetica, 10, TextAlign.Left);
            //page.Elements.Add(labelDB);
        }

        private int PDF_FONT_SIZE = 12;
        public byte[] generatePDF(ReportModel model, string strFilePath)
        {
            try
            {
                Document document = new Document();

                Page page = new Page(PageSize.Letter, PageOrientation.Portrait);

                Template template = new Template();
                this.setupPDFDocumentTemplate(template, page);

                document.Template = template;


                // Header
                Label labelProgram = new Label(model.ReportName, 0, 0, page.Dimensions.Body.Width, 50, ceTe.DynamicPDF.Font.HelveticaBold, 12, TextAlign.Center);
                page.Elements.Add(labelProgram);

                Label labelReportCode = new Label("Report Code:      " + model.ReportCode, 0, 20, page.Dimensions.Body.Width / 2, 50, ceTe.DynamicPDF.Font.Helvetica, 12, TextAlign.Left);
                page.Elements.Add(labelReportCode);

                Label labelLA = new Label("LA Name and Code: " + model.LANameAndCode, page.Dimensions.Body.Width / 2, 20, page.Dimensions.Body.Width / 2, 50, ceTe.DynamicPDF.Font.Helvetica, 12, TextAlign.Left);
                page.Elements.Add(labelLA);

                Label labelDate = new Label("Date Requested:   " + DateTime.Now.ToString("yyyy/MM/dd"), 0, labelLA.Y + 30, page.Dimensions.Body.Width / 2, 50, ceTe.DynamicPDF.Font.Helvetica, 12, TextAlign.Left);
                page.Elements.Add(labelDate);

                Label labelParameters = new Label("Parameters:       " + model.Parameters.Replace("<BR/>", "\r\n"), page.Dimensions.Body.Width / 2, labelLA.Y + 30, page.Dimensions.Body.Width / 2, 60, ceTe.DynamicPDF.Font.Helvetica, 12, TextAlign.Left);
                page.Elements.Add(labelParameters);


                //Body
                Table table = new Table(0, labelParameters.Y + labelParameters.Height, page.Dimensions.Body.Width, page.Dimensions.Body.Height - labelParameters.Y - labelParameters.Height - 20, ceTe.DynamicPDF.Font.Helvetica, 12);
                table.BorderWidth = 1;
                table.RepeatColumnHeaderCount = 1;

                ColumnList colList = table.Columns;

                //Columns
                XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("HeadColumns");
                int column = nodes[0].ChildNodes.Count;
                float[] columnLength = new float[column];
                float Totallength = 0;

                for (int i = 0; i < column; i++)
                {
                    columnLength[i] = nodes[0].ChildNodes[i].ChildNodes[0].InnerText.Length * 10;
                    Totallength += columnLength[i];
                }

                using (SqlDataReader reader = SIL.AARTO.BLL.Report.ReportManager.GetDataForExport(model))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int i = 0;
                            foreach (XmlNode node in nodes[0].ChildNodes)
                            {
                                if (node.Name == "HeadColumn")
                                {
                                    string strText = reader[nodes[0].ChildNodes[i].ChildNodes[1].InnerText].ToString();
                                    if (strText.Length > columnLength[i])
                                    {
                                        columnLength[i] = strText.Length * 10;
                                    }
                                }
                                i++;
                            }
                        }
                    }
                }

                float percents = page.Dimensions.Body.Width /Totallength;

                for (int i = 0; i < column; i++)
                {
                    colList.Add(columnLength[i] * percents);
                }


                //Data
                RowList rowList = table.Rows;
                Row row1 = rowList.Add(ceTe.DynamicPDF.Font.HelveticaBold, PDF_FONT_SIZE);
                row1.VAlign = CellVAlign.Top;

                CellList cellList1 = row1.Cells;
                for (int i = 0; i < column; i++)
                {
                    string strText = nodes[0].ChildNodes[i].ChildNodes[0].InnerText;
                    Cell cell = cellList1.Add(strText);
                    //cell.RowSpan = strText.Length * 10;

                    string strColor = nodes[0].ChildNodes[i].ChildNodes[2].InnerText;
                    cell.TextColor = new RgbColor(Convert.ToByte(strColor.Substring(1, 2), 16), Convert.ToByte(strColor.Substring(3, 2), 16), Convert.ToByte(strColor.Substring(5, 2), 16));
                    cell.Align = CellAlign.Justify;
                }


                using (SqlDataReader reader = SIL.AARTO.BLL.Report.ReportManager.GetDataForExport(model))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Row row = rowList.Add(ceTe.DynamicPDF.Font.Helvetica, PDF_FONT_SIZE);
                            CellList cellList = row.Cells;

                            int i = 0;
                            foreach (XmlNode node in nodes[0].ChildNodes)
                            {
                                if (node.Name == "HeadColumn")
                                {
                                    string strText = reader[nodes[0].ChildNodes[i].ChildNodes[1].InnerText].ToString();
                                    
                                    Cell cell =  cellList.Add(strText);

                                    //cell.RowSpan = strText.Length * 10;
                                    string strColor =  node.ChildNodes[2].InnerText;
                                    cell.TextColor = new RgbColor(Convert.ToByte(strColor.Substring(1, 2), 16), Convert.ToByte(strColor.Substring(3, 2), 16), Convert.ToByte(strColor.Substring(5, 2), 16));
                                    cell.Align = CellAlign.Justify;
                                    //RgbColor foreground =  // black


                                }
                                i++;
                            }
                        }
                    }
                }

                page.Elements.Add(table);
                document.Pages.Add(page);

                Table tableOF = table.GetOverflowRows();

                while (tableOF != null)
                {
                    tableOF.X = 0;
                    tableOF.Y = 80;
                    //tableOF.Height = page.Dimensions.Body.Height - 35;
                    Page pageOF = new Page(PageSize.Letter, PageOrientation.Portrait);
                    pageOF.Elements.Add(tableOF);
                    float offsetY = tableOF.GetVisibleHeight() + 80; // last table +
                    document.Pages.Add(pageOF);
                    tableOF = tableOF.GetOverflowRows();
                    if (tableOF != null)
                    {
                        this.setupPDFFooter(pageOF, offsetY);
                    }
                }


                float offsetY1 = table.GetVisibleHeight() + 80; // last table + table
                this.setupPDFFooter(page, offsetY1);

                if (strFilePath != null)
                {
                    document.Draw(strFilePath);
                    return null;
                }
                return document.Draw();
            }
            catch (Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, null);
                throw e;
            }
        }
    }
}