﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;

using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.Report
{
    public class ManagementStats
    {
        // Fields
        private string authority;
        private int month;
        private string monthName;
        private int psIntNo;
        private int year;
        private int FirstNoticePrintedQty;
        private int FirstNoticePrintedValue;
        private int FramesRejectedQty;
        private int FramesRejectedValue;
        private int FramesExpiredQty;
        private int FramesExpiredValue;
        private int NoAOGQty;
        private int NoAOGValue;
        private int NoticesExpiredQty;
        private int NoticesExpiredValue;
        private int RepresentationsLoggedQty;
        private int RepresentationsLoggedValue;
        private int RepresentationsChargeWithdrawnQty;
        private int RepresentationsChargeWithdrawnValue;
        private int SecondNoticePrintedQty;
        private int SecondNoticePrintedValue;
        private int SummonsPrintedQty;
        private int SummonsPrintedValue;
        private int SummonsServedPersonalQty;
        private int SummonsServedPersonalValue;
        private int SummonsServedNonPersonalQty;
        private int SummonsServedNonPersonalValue;
        private int SummonsUnservedQty;
        private int SummonsUnservedValue;
        private int SummonsCaseNoQty;
        private int SummonsCaseNoValue;
        private int SummonsRepLoggedQty;
        private int SummonsRepLoggedValue;
        private int SummonsWithdrawnQty;
        private int SummonsWithdrawnValue;
        private int SummonsExpiredQty;
        private int SummonsExpiredValue;
        private int SummonsPaidQty;
        private int SummonsPaidValue;
        private int ReceiptQty;
        private int ReceiptValue;
        private int CourtJudgementQty;
        private int CourtJudgementValue;
        private int WOAIssuedQty;
        private int WOAIssuedValue;
        private int WOAPaidQty;
        private int WOAPaidValue;

        public int getCourtJudgementQty()
        {
            return CourtJudgementQty;
        }

        public void setCourtJudgementQty(int CourtJudgementQty)
        {
            this.CourtJudgementQty = CourtJudgementQty;
        }

        public int getCourtJudgementValue()
        {
            return CourtJudgementValue;
        }

        public void setCourtJudgementValue(int CourtJudgementValue)
        {
            this.CourtJudgementValue = CourtJudgementValue;
        }

        public int getFirstNoticePrintedQty()
        {
            return FirstNoticePrintedQty;
        }

        public void setFirstNoticePrintedQty(int FirstNoticePrintedQty)
        {
            this.FirstNoticePrintedQty = FirstNoticePrintedQty;
        }

        public int getFirstNoticePrintedValue()
        {
            return FirstNoticePrintedValue;
        }

        public void setFirstNoticePrintedValue(int FirstNoticePrintedValue)
        {
            this.FirstNoticePrintedValue = FirstNoticePrintedValue;
        }

        public int getFramesExpiredQty()
        {
            return FramesExpiredQty;
        }

        public void setFramesExpiredQty(int FramesExpiredQty)
        {
            this.FramesExpiredQty = FramesExpiredQty;
        }

        public int getFramesExpiredValue()
        {
            return FramesExpiredValue;
        }

        public void setFramesExpiredValue(int FramesExpiredValue)
        {
            this.FramesExpiredValue = FramesExpiredValue;
        }

        public int getFramesRejectedQty()
        {
            return FramesRejectedQty;
        }

        public void setFramesRejectedQty(int FramesRejectedQty)
        {
            this.FramesRejectedQty = FramesRejectedQty;
        }

        public int getFramesRejectedValue()
        {
            return FramesRejectedValue;
        }

        public void setFramesRejectedValue(int FramesRejectedValue)
        {
            this.FramesRejectedValue = FramesRejectedValue;
        }

        public int getNoAOGQty()
        {
            return NoAOGQty;
        }

        public void setNoAOGQty(int NoAOGQty)
        {
            this.NoAOGQty = NoAOGQty;
        }

        public int getNoAOGValue()
        {
            return NoAOGValue;
        }

        public void setNoAOGValue(int NoAOGValue)
        {
            this.NoAOGValue = NoAOGValue;
        }

        public int getNoticesExpiredQty()
        {
            return NoticesExpiredQty;
        }

        public void setNoticesExpiredQty(int NoticesExpiredQty)
        {
            this.NoticesExpiredQty = NoticesExpiredQty;
        }

        public int getNoticesExpiredValue()
        {
            return NoticesExpiredValue;
        }

        public void setNoticesExpiredValue(int NoticesExpiredValue)
        {
            this.NoticesExpiredValue = NoticesExpiredValue;
        }

        public int getReceiptQty()
        {
            return ReceiptQty;
        }

        public void setReceiptQty(int ReceiptQty)
        {
            this.ReceiptQty = ReceiptQty;
        }

        public int getReceiptValue()
        {
            return ReceiptValue;
        }

        public void setReceiptValue(int ReceiptValue)
        {
            this.ReceiptValue = ReceiptValue;
        }

        public int getRepresentationsChargeWithdrawnQty()
        {
            return RepresentationsChargeWithdrawnQty;
        }

        public void setRepresentationsChargeWithdrawnQty(int RepresentationsChargeWithdrawnQty)
        {
            this.RepresentationsChargeWithdrawnQty = RepresentationsChargeWithdrawnQty;
        }

        public int getRepresentationsChargeWithdrawnValue()
        {
            return RepresentationsChargeWithdrawnValue;
        }

        public void setRepresentationsChargeWithdrawnValue(int RepresentationsChargeWithdrawnValue)
        {
            this.RepresentationsChargeWithdrawnValue = RepresentationsChargeWithdrawnValue;
        }

        public int getRepresentationsLoggedQty()
        {
            return RepresentationsLoggedQty;
        }

        public void setRepresentationsLoggedQty(int RepresentationsLoggedQty)
        {
            this.RepresentationsLoggedQty = RepresentationsLoggedQty;
        }

        public int getRepresentationsLoggedValue()
        {
            return RepresentationsLoggedValue;
        }

        public void setRepresentationsLoggedValue(int RepresentationsLoggedValue)
        {
            this.RepresentationsLoggedValue = RepresentationsLoggedValue;
        }

        public int getSecondNoticePrintedQty()
        {
            return SecondNoticePrintedQty;
        }

        public void setSecondNoticePrintedQty(int SecondNoticePrintedQty)
        {
            this.SecondNoticePrintedQty = SecondNoticePrintedQty;
        }

        public int getSecondNoticePrintedValue()
        {
            return SecondNoticePrintedValue;
        }

        public void setSecondNoticePrintedValue(int SecondNoticePrintedValue)
        {
            this.SecondNoticePrintedValue = SecondNoticePrintedValue;
        }

        public int getSummonsCaseNoQty()
        {
            return SummonsCaseNoQty;
        }

        public void setSummonsCaseNoQty(int SummonsCaseNoQty)
        {
            this.SummonsCaseNoQty = SummonsCaseNoQty;
        }

        public int getSummonsCaseNoValue()
        {
            return SummonsCaseNoValue;
        }

        public void setSummonsCaseNoValue(int SummonsCaseNoValue)
        {
            this.SummonsCaseNoValue = SummonsCaseNoValue;
        }

        public int getSummonsExpiredQty()
        {
            return SummonsExpiredQty;
        }

        public void setSummonsExpiredQty(int SummonsExpiredQty)
        {
            this.SummonsExpiredQty = SummonsExpiredQty;
        }

        public int getSummonsExpiredValue()
        {
            return SummonsExpiredValue;
        }

        public void setSummonsExpiredValue(int SummonsExpiredValue)
        {
            this.SummonsExpiredValue = SummonsExpiredValue;
        }

        public int getSummonsPaidQty()
        {
            return SummonsPaidQty;
        }

        public void setSummonsPaidQty(int SummonsPaidQty)
        {
            this.SummonsPaidQty = SummonsPaidQty;
        }

        public int getSummonsPaidValue()
        {
            return SummonsPaidValue;
        }

        public void setSummonsPaidValue(int SummonsPaidValue)
        {
            this.SummonsPaidValue = SummonsPaidValue;
        }

        public int getSummonsPrintedQty()
        {
            return SummonsPrintedQty;
        }

        public void setSummonsPrintedQty(int SummonsPrintedQty)
        {
            this.SummonsPrintedQty = SummonsPrintedQty;
        }

        public int getSummonsPrintedValue()
        {
            return SummonsPrintedValue;
        }

        public void setSummonsPrintedValue(int SummonsPrintedValue)
        {
            this.SummonsPrintedValue = SummonsPrintedValue;
        }

        public int getSummonsRepLoggedQty()
        {
            return SummonsRepLoggedQty;
        }

        public void setSummonsRepLoggedQty(int SummonsRepLoggedQty)
        {
            this.SummonsRepLoggedQty = SummonsRepLoggedQty;
        }

        public int getSummonsRepLoggedValue()
        {
            return SummonsRepLoggedValue;
        }

        public void setSummonsRepLoggedValue(int SummonsRepLoggedValue)
        {
            this.SummonsRepLoggedValue = SummonsRepLoggedValue;
        }

        public int getSummonsServedNonPersonalQty()
        {
            return SummonsServedNonPersonalQty;
        }

        public void setSummonsServedNonPersonalQty(int SummonsServedNonPersonalQty)
        {
            this.SummonsServedNonPersonalQty = SummonsServedNonPersonalQty;
        }

        public int getSummonsServedNonPersonalValue()
        {
            return SummonsServedNonPersonalValue;
        }

        public void setSummonsServedNonPersonalValue(int SummonsServedNonPersonalValue)
        {
            this.SummonsServedNonPersonalValue = SummonsServedNonPersonalValue;
        }

        public int getSummonsServedPersonalQty()
        {
            return SummonsServedPersonalQty;
        }

        public void setSummonsServedPersonalQty(int SummonsServedPersonalQty)
        {
            this.SummonsServedPersonalQty = SummonsServedPersonalQty;
        }

        public int getSummonsServedPersonalValue()
        {
            return SummonsServedPersonalValue;
        }

        public void setSummonsServedPersonalValue(int SummonsServedPersonalValue)
        {
            this.SummonsServedPersonalValue = SummonsServedPersonalValue;
        }

        public int getSummonsUnservedQty()
        {
            return SummonsUnservedQty;
        }

        public void setSummonsUnservedQty(int SummonsUnservedQty)
        {
            this.SummonsUnservedQty = SummonsUnservedQty;
        }

        public int getSummonsUnservedValue()
        {
            return SummonsUnservedValue;
        }

        public void setSummonsUnservedValue(int SummonsUnservedValue)
        {
            this.SummonsUnservedValue = SummonsUnservedValue;
        }

        public int getSummonsWithdrawnQty()
        {
            return SummonsWithdrawnQty;
        }

        public void setSummonsWithdrawnQty(int SummonsWithdrawnQty)
        {
            this.SummonsWithdrawnQty = SummonsWithdrawnQty;
        }

        public int getSummonsWithdrawnValue()
        {
            return SummonsWithdrawnValue;
        }

        public void setSummonsWithdrawnValue(int SummonsWithdrawnValue)
        {
            this.SummonsWithdrawnValue = SummonsWithdrawnValue;
        }

        public int getWOAIssuedQty()
        {
            return WOAIssuedQty;
        }

        public void setWOAIssuedQty(int WOAIssuedQty)
        {
            this.WOAIssuedQty = WOAIssuedQty;
        }

        public int getWOAIssuedValue()
        {
            return WOAIssuedValue;
        }

        public void setWOAIssuedValue(int WOAIssuedValue)
        {
            this.WOAIssuedValue = WOAIssuedValue;
        }

        public int getWOAPaidQty()
        {
            return WOAPaidQty;
        }

        public void setWOAPaidQty(int WOAPaidQty)
        {
            this.WOAPaidQty = WOAPaidQty;
        }

        public int getWOAPaidValue()
        {
            return WOAPaidValue;
        }

        public void setWOAPaidValue(int WOAPaidValue)
        {
            this.WOAPaidValue = WOAPaidValue;
        }

        public string getAuthority()
        {
            return authority;
        }

        public void setAuthority(string authority)
        {
            this.authority = authority;
        }

        public int getMonth()
        {
            return month;
        }

        public void setMonth(int month)
        {
            this.month = month;
        }

        public string getMonthName()
        {
            return monthName;
        }

        public void setMonthName(string monthName)
        {
            this.monthName = monthName;
        }

        public int getPsIntNo()
        {
            return psIntNo;
        }

        public void setPsIntNo(int psIntNo)
        {
            this.psIntNo = psIntNo;
        }

        public int getYear()
        {
            return year;
        }

        public void setYear(int year)
        {
            this.year = year;
        }

    }

    public class ManagementStatisticsReport
    {
        private string connectionString;
        private HSSFWorkbook wb;
        private ISheet sheet;

        private ICell cell;
        private ICellStyle headingStyle;
        private IFont headingFont;
        private ICellStyle normalStyle;
        private IFont subHeadFont;
        private ICellStyle totalStyle;

        public ManagementStatisticsReport(string vConstr)
        {
            connectionString = vConstr;
            wb = new HSSFWorkbook();

            this.headingFont = wb.CreateFont();
            this.headingFont.Color = IndexedColors.BLUE.Index;
            this.headingFont.Underline = (byte)FontUnderlineType.SINGLE;
            this.headingStyle = wb.CreateCellStyle();
            this.headingStyle.Alignment = HorizontalAlignment.CENTER;
            this.headingStyle.VerticalAlignment = VerticalAlignment.CENTER;
            this.normalStyle = wb.CreateCellStyle();
            this.normalStyle.BorderBottom = BorderStyle.THIN;
            this.normalStyle.BorderTop = BorderStyle.THIN;
            this.normalStyle.BorderLeft = BorderStyle.THIN;
            this.normalStyle.BorderRight = BorderStyle.THIN;
            this.subHeadFont = wb.CreateFont();
            this.subHeadFont.Boldweight = (short)FontBoldWeight.BOLD;
            this.totalStyle = wb.CreateCellStyle();
            this.totalStyle.BorderBottom = BorderStyle.THIN;
            this.totalStyle.BorderTop = BorderStyle.THIN;
            this.totalStyle.BorderLeft = BorderStyle.THIN;
            this.totalStyle.BorderRight = BorderStyle.THIN;
            this.totalStyle.SetFont(this.subHeadFont);
        }

        public MemoryStream ExportToExcel(int autIntNo, int year, int month)
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                sheet = wb.CreateSheet();
                wb.SetSheetName(0, "Management Statistics");

                int rowIndex = 0;
                this.sheet.CreateRow(rowIndex++);
                this.sheet.CreateRow(rowIndex++);
                this.sheet.CreateRow(rowIndex++);

                List<ManagementStats> al = new List<ManagementStats>();

                IRow row;
                string authority = null;
                ManagementStats data;
                DataSet ds = this.GetData(autIntNo, year, month);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    authority = ds.Tables[0].Rows[i]["AutName"].ToString();

                    data = new ManagementStats();
                    data.setAuthority(ds.Tables[0].Rows[i]["AutName"].ToString());
                    data.setCourtJudgementQty(int.Parse(ds.Tables[0].Rows[i]["CourtJudgementQty"].ToString()));
                    data.setCourtJudgementValue(int.Parse(ds.Tables[0].Rows[i]["CourtJudgementValue"].ToString()));
                    data.setFirstNoticePrintedQty(int.Parse(ds.Tables[0].Rows[i]["FirstNoticePrintedQty"].ToString()));
                    data.setFirstNoticePrintedValue(int.Parse(ds.Tables[0].Rows[i]["FirstNoticePrintedValue"].ToString()));
                    data.setFramesExpiredQty(int.Parse(ds.Tables[0].Rows[i]["FramesExpiredQty"].ToString()));
                    data.setFramesExpiredValue(int.Parse(ds.Tables[0].Rows[i]["FramesExpiredValue"].ToString()));
                    data.setFramesRejectedQty(int.Parse(ds.Tables[0].Rows[i]["FramesRejectedQty"].ToString()));
                    data.setFramesRejectedValue(int.Parse(ds.Tables[0].Rows[i]["FramesRejectedValue"].ToString()));
                    data.setMonth(int.Parse(ds.Tables[0].Rows[i]["PSMonth"].ToString()));
                    data.setMonthName(ds.Tables[0].Rows[i]["MonthName"].ToString());
                    data.setNoAOGQty(int.Parse(ds.Tables[0].Rows[i]["NoAOGQty"].ToString()));
                    data.setNoAOGValue(int.Parse(ds.Tables[0].Rows[i]["NoAOGValue"].ToString()));
                    data.setNoticesExpiredQty(int.Parse(ds.Tables[0].Rows[i]["NoticesExpiredQty"].ToString()));
                    data.setNoticesExpiredValue(int.Parse(ds.Tables[0].Rows[i]["NoticesExpiredValue"].ToString()));
                    data.setPsIntNo(int.Parse(ds.Tables[0].Rows[i]["PsIntNo"].ToString()));
                    data.setReceiptQty(int.Parse(ds.Tables[0].Rows[i]["ReceiptQty"].ToString()));
                    data.setReceiptValue(int.Parse(ds.Tables[0].Rows[i]["ReceiptValue"].ToString()));
                    data.setRepresentationsChargeWithdrawnQty(int.Parse(ds.Tables[0].Rows[i]["RepresentationsChargeWithdrawnQty"].ToString()));
                    data.setRepresentationsChargeWithdrawnValue(int.Parse(ds.Tables[0].Rows[i]["RepresentationsChargeWithdrawnValue"].ToString()));
                    data.setRepresentationsLoggedQty(int.Parse(ds.Tables[0].Rows[i]["RepresentationsLoggedQty"].ToString()));
                    data.setRepresentationsLoggedValue(int.Parse(ds.Tables[0].Rows[i]["RepresentationsLoggedValue"].ToString()));
                    data.setSecondNoticePrintedQty(int.Parse(ds.Tables[0].Rows[i]["SecondNoticePrintedQty"].ToString()));
                    data.setSecondNoticePrintedValue(int.Parse(ds.Tables[0].Rows[i]["SecondNoticePrintedValue"].ToString()));
                    data.setSummonsCaseNoQty(int.Parse(ds.Tables[0].Rows[i]["SummonsCaseNoQty"].ToString()));
                    data.setSummonsCaseNoValue(int.Parse(ds.Tables[0].Rows[i]["SummonsCaseNoValue"].ToString()));
                    data.setSummonsExpiredQty(int.Parse(ds.Tables[0].Rows[i]["SummonsExpiredQty"].ToString()));
                    data.setSummonsExpiredValue(int.Parse(ds.Tables[0].Rows[i]["SummonsExpiredValue"].ToString()));
                    data.setSummonsPaidQty(int.Parse(ds.Tables[0].Rows[i]["SummonsPaidQty"].ToString()));
                    data.setSummonsPaidValue(int.Parse(ds.Tables[0].Rows[i]["SummonsPaidValue"].ToString()));
                    data.setSummonsPrintedQty(int.Parse(ds.Tables[0].Rows[i]["SummonsPrintedQty"].ToString()));
                    data.setSummonsPrintedValue(int.Parse(ds.Tables[0].Rows[i]["SummonsPrintedValue"].ToString()));
                    data.setSummonsRepLoggedQty(int.Parse(ds.Tables[0].Rows[i]["SummonsRepLoggedQty"].ToString()));
                    data.setSummonsRepLoggedValue(int.Parse(ds.Tables[0].Rows[i]["SummonsRepLoggedValue"].ToString()));
                    data.setSummonsServedNonPersonalQty(int.Parse(ds.Tables[0].Rows[i]["SummonsServedNonPersonalQty"].ToString()));
                    data.setSummonsServedNonPersonalValue(int.Parse(ds.Tables[0].Rows[i]["SummonsServedNonPersonalValue"].ToString()));
                    data.setSummonsServedPersonalQty(int.Parse(ds.Tables[0].Rows[i]["SummonsServedPersonalQty"].ToString()));
                    data.setSummonsServedPersonalValue(int.Parse(ds.Tables[0].Rows[i]["SummonsServedPersonalValue"].ToString()));
                    data.setSummonsUnservedQty(int.Parse(ds.Tables[0].Rows[i]["SummonsUnservedQty"].ToString()));
                    data.setSummonsUnservedValue(int.Parse(ds.Tables[0].Rows[i]["SummonsUnservedValue"].ToString()));
                    data.setSummonsWithdrawnQty(int.Parse(ds.Tables[0].Rows[i]["SummonsWithdrawnQty"].ToString()));
                    data.setSummonsWithdrawnValue(int.Parse(ds.Tables[0].Rows[i]["SummonsWithdrawnValue"].ToString()));
                    data.setWOAIssuedQty(int.Parse(ds.Tables[0].Rows[i]["WOAIssuedQty"].ToString()));
                    data.setWOAIssuedValue(int.Parse(ds.Tables[0].Rows[i]["WOAIssuedValue"].ToString()));
                    data.setWOAPaidQty(int.Parse(ds.Tables[0].Rows[i]["WOAPaidQty"].ToString()));
                    data.setWOAPaidValue(int.Parse(ds.Tables[0].Rows[i]["WOAPaidValue"].ToString()));
                    data.setYear(int.Parse(ds.Tables[0].Rows[i]["PSYear"].ToString()));

                    al.Add(data);
                }

                // Add the data-row headings
                this.addTableCell("Successful violations (Notices printed)", rowIndex++, 0);
                this.addTableCell("Unsuccessful violations (Frames rejected)", rowIndex++, 0);
                this.addTableCell("Unsuccessful violations (Frames expired)", rowIndex++, 0);
                this.addTableCell("Unsuccessful violations (Notices expired)", rowIndex++, 0);
                this.addTableCell("No Admission of Guilt charges", rowIndex++, 0);
                this.addTableCell("Representations logged", rowIndex++, 0);
                this.addTableCell("Charges withdrawn", rowIndex++, 0);
                this.addTableCell("Second notices printed", rowIndex++, 0);
                this.addTableCell("Summonses printed", rowIndex++, 0);
                this.addTableCell("Summonses served (personal)", rowIndex++, 0);
                this.addTableCell("Summonses served (impersonal)", rowIndex++, 0);
                this.addTableCell("Summonses unservable", rowIndex++, 0);
                this.addTableCell("Summonses (case no's allocated)", rowIndex++, 0);
                this.addTableCell("Summonses Representations logged", rowIndex++, 0);
                this.addTableCell("Summonses withdrawn", rowIndex++, 0);
                this.addTableCell("Summonses expired", rowIndex++, 0);
                this.addTableCell("Payments received", rowIndex++, 0);
                this.addTableCell("Summons payments received", rowIndex++, 0);
                this.addTableCell("Court judgements", rowIndex++, 0);
                this.addTableCell("WOA Issued", rowIndex++, 0);
                this.addTableCell("WOA payments received", rowIndex++, 0);

                // Loop the data and add the data to the report
                rowIndex = 3;
                int colIndex = -1;
                for (int j = 0; j < al.Count;j++ )
                {
                    // Create the headings
                    colIndex += 2;
                    row = this.addTableHeadings(colIndex, al[j].getMonthName());

                    // Add the data
                    for (int i = 0; i <= 20; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getFirstNoticePrintedQty(), al[j].getFirstNoticePrintedValue());
                                break;
                            case 1:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getFramesRejectedQty(), al[j].getFramesRejectedValue());
                                break;
                            case 2:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getFramesExpiredQty(), al[j].getFramesExpiredValue());
                                break;
                            case 3:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getNoticesExpiredQty(), al[j].getNoticesExpiredValue());
                                break;
                            case 4:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getNoAOGQty(), al[j].getNoAOGValue());
                                break;
                            case 5:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getRepresentationsLoggedQty(), al[j].getRepresentationsLoggedValue());
                                break;
                            case 6:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getRepresentationsChargeWithdrawnQty(), al[j].getRepresentationsChargeWithdrawnValue());
                                break;
                            case 7:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSecondNoticePrintedQty(), al[j].getSecondNoticePrintedValue());
                                break;
                            case 8:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsPrintedQty(), al[j].getSummonsPrintedValue());
                                break;
                            case 9:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsServedPersonalQty(), al[j].getSummonsServedPersonalValue());
                                break;
                            case 10:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsServedNonPersonalQty(), al[j].getSummonsServedNonPersonalValue());
                                break;
                            case 11:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsUnservedQty(), al[j].getSummonsUnservedValue());
                                break;
                            case 12:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsCaseNoQty(), al[j].getSummonsCaseNoValue());
                                break;
                            case 13:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsRepLoggedQty(), al[j].getSummonsRepLoggedValue());
                                break;
                            case 14:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsWithdrawnQty(), al[j].getSummonsWithdrawnValue());
                                break;
                            case 15:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsExpiredQty(), al[j].getSummonsExpiredValue());
                                break;
                            case 16:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getReceiptQty(), al[j].getReceiptValue());
                                break;
                            case 17:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getSummonsPaidQty(), al[j].getSummonsPaidValue());
                                break;
                            case 18:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getCourtJudgementQty(), al[j].getCourtJudgementValue());
                                break;
                            case 19:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getWOAIssuedQty(), al[j].getWOAIssuedValue());
                                break;
                            case 20:
                                this.createDataCell(rowIndex + i, colIndex, al[j].getWOAPaidQty(), al[j].getWOAPaidValue());
                                break;
                        }
                    }
                }

                // Add the month column headings
                this.createColumnHeadings(month);

                // Create the totals
                this.addTableHeadings((al.Count * 2) + 1, "Totals");
                this.createTotals(al.Count);

                // Auto-width the columns
                for (short i = 0; i <= (al.Count * 2) + 2; i++)
                {
                    sheet.AutoSizeColumn(i);
                }
                sheet.SetColumnWidth(0, sheet.GetColumnWidth(0) + 1000);
                // Create the report heading
                this.createHeading(authority, year, (al.Count * 2) + 2);

                wb.Write(stream);

                return stream;
            }
            catch (Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, "TMS");
                throw e;
            }
        }

        private DataSet GetData(int autIntNo, int year, int month)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetPeriodStats", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@InYear", SqlDbType.Int, 4).Value = year;
            com.Parameters.Add("@InMonth", SqlDbType.Int, 4).Value = month;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        private void addTableCell(string value, int rowIndex, int cellIndex)
        {
            IRow row = this.sheet.CreateRow(rowIndex);
            this.cell = row.CreateCell(cellIndex);
            this.cell.SetCellValue(new HSSFRichTextString(value));
            this.cell.CellStyle = this.normalStyle;
        }

        private IRow addTableHeadings(int colIndex, string monthName)
        {
            IRow row = sheet.GetRow(1);
            this.cell = row.CreateCell(colIndex);
            HSSFRichTextString rtf = new HSSFRichTextString(monthName);
            rtf.ApplyFont(this.headingFont);
            this.cell.SetCellValue(rtf);
            this.cell.CellStyle = this.headingStyle;

            this.cell = row.CreateCell(colIndex + 1);
            CellRangeAddress region = new CellRangeAddress(1, 1, colIndex, colIndex + 1);
            sheet.AddMergedRegion(region);


            row = sheet.GetRow(2);

            this.cell = row.CreateCell(colIndex++);
            rtf = new HSSFRichTextString("Qty");
            rtf.ApplyFont(this.subHeadFont);
            this.cell.SetCellValue(rtf);
            this.cell.CellStyle = this.normalStyle;

            this.cell = row.CreateCell(colIndex);
            rtf = new HSSFRichTextString("Value");
            rtf.ApplyFont(this.subHeadFont);
            this.cell.SetCellValue(rtf);
            this.cell.CellStyle = this.normalStyle;

            return row;
        }

        private void createColumnHeadings(int month)
        {
            IRow row = sheet.GetRow(1);

            this.cell = row.CreateCell(0);
            HSSFRichTextString rtf = new HSSFRichTextString("Month");
            rtf.ApplyFont(this.headingFont);
            this.cell.SetCellValue(rtf);
            this.cell.CellStyle = this.headingStyle;
        }

        private void createDataCell(int rowIndex, int colIndex, int qty, int value)
        {
            IRow row = sheet.GetRow(rowIndex);

            this.cell = row.CreateCell(colIndex++);
            this.cell.SetCellValue(qty);
            this.cell.CellStyle = this.normalStyle;

            this.cell = row.CreateCell(colIndex);
            this.cell.SetCellValue(value);
            this.cell.CellStyle = this.normalStyle;
        }

        private void createHeading(String authority, int year, int columns)
        {
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = (short)16;
            font.Boldweight = (short)FontBoldWeight.BOLD;
            this.cell = this.sheet.GetRow(0).CreateCell(0);
            HSSFRichTextString rtf = new HSSFRichTextString(authority + " Monthly Management Report -  Year: " + year);
            rtf.ApplyFont(font);
            this.cell.SetCellValue(rtf);
            ICellStyle style = wb.CreateCellStyle();
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;
            this.cell.CellStyle = style;

            CellRangeAddress region = new CellRangeAddress(0, 0, 0, columns);
            this.sheet.AddMergedRegion(region);
        }

        private void createTotals(int noMonths)
        {
            if (noMonths < 1)
            {
                return;
            }

            StringBuilder sbQty = new StringBuilder();
            StringBuilder sbValue = new StringBuilder();

            int colIndex = noMonths * 2;

            for (int i = 1; i <= colIndex; )
            {
                sbQty.Append(this.getColumnString(i++));
                sbQty.Append("{0}+");

                sbValue.Append(this.getColumnString(i++));
                sbValue.Append("{0}+");
            }
            sbQty.Length = sbQty.Length - 1;
            sbValue.Length = sbValue.Length - 1;

            for (int i = 4; i < 25; i++)
            {
                string quantityFormula = string.Format(sbQty.ToString(), i);
                this.cell = sheet.GetRow(i - 1).CreateCell(colIndex + 1);
                this.cell.SetCellFormula(quantityFormula);
                this.cell.CellStyle = this.totalStyle;

                string valueFormula = string.Format(sbValue.ToString(), i);
                this.cell = sheet.GetRow(i - 1).CreateCell(colIndex + 2);
                this.cell.SetCellFormula(valueFormula);
                this.cell.CellStyle = this.totalStyle;
            }

            // Add the generated on cell
            this.cell = sheet.CreateRow(25).CreateCell(0);
            HSSFRichTextString rtf = new HSSFRichTextString("Generated : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            this.cell.SetCellValue(rtf);
        }

        private string getColumnString(int column)
        {
            char[] A2Z =
            {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
            };
            StringBuilder retval = new StringBuilder();
            int tempcellnum = column;
            do
            {
                retval.Insert(0, A2Z[tempcellnum % 26]);
                tempcellnum = (tempcellnum / 26) - 1;
            }
            while (tempcellnum >= 0);

            return retval.ToString();
        }
    }
}
