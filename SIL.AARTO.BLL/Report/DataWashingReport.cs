﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.Report
{
    public class DataWashingReport : DynamicReportWriter
    {
        private string connectionString;
        private String generatedDate;

        public DataWashingReport(string vConstr)
        {
            connectionString = vConstr;
            this.generatedDate = "Generated :  " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public MemoryStream ExportToExcel()
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                sheet = wb.CreateSheet();

                String reportName = "DataWashing (" + DateTime.Now.ToString("yyyy-MM-dd HH-mm") + ")";
                wb.SetSheetName(0, reportName);

                DataSet ds = this.GetReportData();

                write(ds.Tables[0], 1, true);

                wb.Write(stream);
                return stream;
            }
            catch (Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, "TMS");
                throw e;
            }
        }

        private DataSet GetReportData()
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand();

            com = new SqlCommand("DataWashing", con);
            com.CommandType = CommandType.StoredProcedure;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

    }
}
