﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;

using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.Report
{
    public class PaymentHistoryData
    {
        // Fields
        private string AutName;
        private string MonthName;
        private int PtSIntNo;
        private int AutIntNo;
        private int PtSYear;
        private int PtSMonth;
        private int PaidAtFirstNoticeQty;
        private int PaidAtFirstNoticeValue;
        private int PaidAtSecondNoticeQty;
        private int PaidAtSecondNoticeValue;
        private int PaidAtSummonsQty;
        private int PaidAtSummonsValue;
        private int PaidAtCourtQty;
        private int PaidAtCourtValue;
        private int PaidAtWOAQty;
        private int PaidAtWOAValue;

        public int getAutIntNo()
        {
            return AutIntNo;
        }

        public void setAutIntNo(int AutIntNo)
        {
            this.AutIntNo = AutIntNo;
        }

        public string getAutName()
        {
            return AutName;
        }

        public void setAutName(string AutName)
        {
            this.AutName = AutName;
        }

        public string getMonthName()
        {
            return MonthName;
        }

        public void setMonthName(string MonthName)
        {
            this.MonthName = MonthName;
        }

        public int getPaidAtCourtQty()
        {
            return PaidAtCourtQty;
        }

        public void setPaidAtCourtQty(int PaidAtCourtQty)
        {
            this.PaidAtCourtQty = PaidAtCourtQty;
        }

        public int getPaidAtCourtValue()
        {
            return PaidAtCourtValue;
        }

        public void setPaidAtCourtValue(int PaidAtCourtValue)
        {
            this.PaidAtCourtValue = PaidAtCourtValue;
        }

        public int getPaidAtFirstNoticeQty()
        {
            return PaidAtFirstNoticeQty;
        }

        public void setPaidAtFirstNoticeQty(int PaidAtFirstNoticeQty)
        {
            this.PaidAtFirstNoticeQty = PaidAtFirstNoticeQty;
        }

        public int getPaidAtFirstNoticeValue()
        {
            return PaidAtFirstNoticeValue;
        }

        public void setPaidAtFirstNoticeValue(int PaidAtFirstNoticeValue)
        {
            this.PaidAtFirstNoticeValue = PaidAtFirstNoticeValue;
        }

        public int getPaidAtSecondNoticeQty()
        {
            return PaidAtSecondNoticeQty;
        }

        public void setPaidAtSecondNoticeQty(int PaidAtSecondNoticeQty)
        {
            this.PaidAtSecondNoticeQty = PaidAtSecondNoticeQty;
        }

        public int getPaidAtSecondNoticeValue()
        {
            return PaidAtSecondNoticeValue;
        }

        public void setPaidAtSecondNoticeValue(int PaidAtSecondNoticeValue)
        {
            this.PaidAtSecondNoticeValue = PaidAtSecondNoticeValue;
        }

        public int getPaidAtSummonsQty()
        {
            return PaidAtSummonsQty;
        }

        public void setPaidAtSummonsQty(int PaidAtSummonsQty)
        {
            this.PaidAtSummonsQty = PaidAtSummonsQty;
        }

        public int getPaidAtSummonsValue()
        {
            return PaidAtSummonsValue;
        }

        public void setPaidAtSummonsValue(int PaidAtSummonsValue)
        {
            this.PaidAtSummonsValue = PaidAtSummonsValue;
        }

        public int getPaidAtWOAQty()
        {
            return PaidAtWOAQty;
        }

        public void setPaidAtWOAQty(int PaidAtWOAQty)
        {
            this.PaidAtWOAQty = PaidAtWOAQty;
        }

        public int getPaidAtWOAValue()
        {
            return PaidAtWOAValue;
        }

        public void setPaidAtWOAValue(int PaidAtWOAValue)
        {
            this.PaidAtWOAValue = PaidAtWOAValue;
        }

        public int getPtSIntNo()
        {
            return PtSIntNo;
        }

        public void setPtSIntNo(int PtSIntNo)
        {
            this.PtSIntNo = PtSIntNo;
        }

        public int getPtSMonth()
        {
            return PtSMonth;
        }

        public void setPtSMonth(int PtSMonth)
        {
            this.PtSMonth = PtSMonth;
        }

        public int getPtSYear()
        {
            return PtSYear;
        }

        public void setPtSYear(int PtSYear)
        {
            this.PtSYear = PtSYear;
        }

    }

    public class PaymentHistoryReport
    {
        private string connectionString;
        private HSSFWorkbook wb;
        private ISheet sheet;

        private ICell cell;
        private ICellStyle centreStyle;
        private IFont blueFont;
        private ICellStyle borderStyle;
        private IFont boldFont;

        public PaymentHistoryReport(string vConstr)
        {
            connectionString = vConstr;
            wb = new HSSFWorkbook();

            this.centreStyle = wb.CreateCellStyle();
            this.centreStyle.Alignment = HorizontalAlignment.CENTER;
            this.centreStyle.VerticalAlignment = VerticalAlignment.CENTER;
            this.blueFont = wb.CreateFont();
            this.blueFont.Color = IndexedColors.BLUE.Index;
            this.blueFont.Underline = (byte)FontUnderlineType.SINGLE;
            this.borderStyle = wb.CreateCellStyle();
            this.borderStyle.BorderTop = BorderStyle.THIN;
            this.borderStyle.BorderRight = BorderStyle.THIN;
            this.borderStyle.BorderBottom = BorderStyle.THIN;
            this.borderStyle.BorderLeft = BorderStyle.THIN;
            this.boldFont = wb.CreateFont();
            this.boldFont.Boldweight = (short)FontBoldWeight.BOLD;
        }

        public MemoryStream ExportToExcel(int autIntNo, int year)
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                sheet = wb.CreateSheet();
                wb.SetSheetName(0, "Payment History");

                string authority = null;
                PaymentHistoryData data;
                List<PaymentHistoryData> al = new List<PaymentHistoryData>();
                DataSet ds = this.GetReportData(autIntNo,year);
                for(int i=0;i<ds.Tables[0].Rows.Count;i++)
                {
                    authority = ds.Tables[0].Rows[i]["AutName"].ToString();

                    data = new PaymentHistoryData();
                    data.setAutIntNo(int.Parse(ds.Tables[0].Rows[i]["AutIntNo"].ToString()));
                    data.setAutName(ds.Tables[0].Rows[i]["AutName"].ToString());
                    data.setMonthName(ds.Tables[0].Rows[i]["MonthName"].ToString()); ;
                    data.setPaidAtCourtQty(int.Parse(ds.Tables[0].Rows[i]["PaidAtCourtQty"].ToString()));
                    data.setPaidAtCourtValue(int.Parse(ds.Tables[0].Rows[i]["PaidAtCourtValue"].ToString()));
                    data.setPaidAtFirstNoticeQty(int.Parse(ds.Tables[0].Rows[i]["PaidAtFirstNoticeQty"].ToString()));
                    data.setPaidAtFirstNoticeValue(int.Parse(ds.Tables[0].Rows[i]["PaidAtFirstNoticeValue"].ToString()));
                    data.setPaidAtSecondNoticeQty(int.Parse(ds.Tables[0].Rows[i]["PaidAtSecondNoticeQty"].ToString()));
                    data.setPaidAtSecondNoticeValue(int.Parse(ds.Tables[0].Rows[i]["PaidAtSecondNoticeValue"].ToString()));
                    data.setPaidAtSummonsQty(int.Parse(ds.Tables[0].Rows[i]["PaidAtSummonsQty"].ToString()));
                    data.setPaidAtSummonsValue(int.Parse(ds.Tables[0].Rows[i]["PaidAtSummonsValue"].ToString()));
                    data.setPaidAtWOAQty(int.Parse(ds.Tables[0].Rows[i]["PaidAtWOAQty"].ToString()));
                    data.setPaidAtWOAValue(int.Parse(ds.Tables[0].Rows[i]["PaidAtWOAValue"].ToString()));
                    data.setPtSIntNo(int.Parse(ds.Tables[0].Rows[i]["PtSIntNo"].ToString()));
                    data.setPtSMonth(int.Parse(ds.Tables[0].Rows[i]["PtSMonth"].ToString()));
                    data.setPtSYear(int.Parse(ds.Tables[0].Rows[i]["PtSYear"].ToString()));

                    al.Add(data);
                }

                // Create the  report heading
                this.createReportHeading(authority, year);

                // Create the heading rows
                this.createHeadings();

                // Output the data row
                int rowIndex = 0;
                for (rowIndex = 3; rowIndex < 8; rowIndex++)
                {
                    IRow row = sheet.CreateRow(rowIndex);

                    switch (rowIndex)
                    {
                        case 3:
                            this.createStringCell(row, 0, "Payments: 1st Notice printed");
                            break;
                        case 4:
                            this.createStringCell(row, 0, "Payments: 2nd notice printed");
                            break;
                        case 5:
                            this.createStringCell(row, 0, "Payments: Summons");
                            break;
                        case 6:
                            this.createStringCell(row, 0, "Payments: Court");
                            break;
                        case 7:
                            this.createStringCell(row, 0, "Payments: WOA");
                            break;
                    }

                    int colIndex = 1;
                    for (int i = 0; i < al.Count;i++ )
                    {
                        switch (rowIndex)
                        {
                            case 3:
                                this.createQVDataCells(row, colIndex, al[i].getPaidAtFirstNoticeQty(), al[i].getPaidAtFirstNoticeValue());
                                break;
                            case 4:
                                this.createQVDataCells(row, colIndex, al[i].getPaidAtSecondNoticeQty(), al[i].getPaidAtSecondNoticeValue());
                                break;
                            case 5:
                                this.createQVDataCells(row, colIndex, al[i].getPaidAtSummonsQty(), al[i].getPaidAtSummonsValue());
                                break;
                            case 6:
                                this.createQVDataCells(row, colIndex, al[i].getPaidAtCourtQty(), al[i].getPaidAtCourtValue());
                                break;
                            case 7:
                                this.createQVDataCells(row, colIndex, al[i].getPaidAtWOAQty(), al[i].getPaidAtWOAValue());
                                break;
                        }
                        colIndex += 2;
                    }
                }

                // Create the totals formulas
                this.createTotalsCells();

                // Auto-size columns
                for (short i = 0; i < 27; i++)
                {
                    sheet.AutoSizeColumn(i);
                }
                sheet.SetColumnWidth(0, sheet.GetColumnWidth(0) + 5000);
                // Add the generated on cell
                this.createGeneratedMessage();

                wb.Write(stream);
                return stream;
            }
            catch (Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, "TMS");
                throw e;
            }
        }

        private DataSet GetReportData(int autIntNo, int year)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("GetPaymentStats", con);
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@InYear", SqlDbType.Int, 4).Value = year;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        private void createGeneratedMessage()
        {
            sheet.CreateRow(8);
            String message = "Generated : " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            this.cell = sheet.CreateRow(9).CreateCell(0);
            this.cell.SetCellValue(new HSSFRichTextString(message));
        }

        private void createHeadingCell(IRow row, int cellIndex, string value, bool merge)
        {
            HSSFRichTextString rtf = new HSSFRichTextString(value);
            rtf.ApplyFont(this.blueFont);

            this.cell = row.CreateCell(cellIndex);
            this.cell.SetCellValue(rtf);

            if (merge)
            {
                this.cell.CellStyle = this.centreStyle;

                CellRangeAddress region = new CellRangeAddress(1, 1, cellIndex, cellIndex + 1);
                sheet.AddMergedRegion(region);
            }
        }

        private void createHeadings()
        {
            IRow row = sheet.CreateRow(1);

            this.createHeadingCell(row, 0, "Month", false);
            this.createHeadingCell(row, 1, "January", true);
            this.createHeadingCell(row, 3, "February", true);
            this.createHeadingCell(row, 5, "March", true);
            this.createHeadingCell(row, 7, "April", true);
            this.createHeadingCell(row, 9, "May", true);
            this.createHeadingCell(row, 11, "June", true);
            this.createHeadingCell(row, 13, "July", true);
            this.createHeadingCell(row, 15, "August", true);
            this.createHeadingCell(row, 17, "September", true);
            this.createHeadingCell(row, 19, "October", true);
            this.createHeadingCell(row, 21, "November", true);
            this.createHeadingCell(row, 23, "December", true);
            this.createHeadingCell(row, 25, "Total", true);

            row = sheet.CreateRow(2);

            this.createStringCell(row, 0, "");
            this.createQVCells(row, 1);
            this.createQVCells(row, 3);
            this.createQVCells(row, 5);
            this.createQVCells(row, 7);
            this.createQVCells(row, 9);
            this.createQVCells(row, 11);
            this.createQVCells(row, 13);
            this.createQVCells(row, 15);
            this.createQVCells(row, 17);
            this.createQVCells(row, 19);
            this.createQVCells(row, 21);
            this.createQVCells(row, 23);
            this.createQVCells(row, 25);
        }

        private void createQVCells(IRow row, int colIndex)
        {
            this.cell = row.CreateCell(colIndex);
            HSSFRichTextString rtf = new HSSFRichTextString("Qty");
            rtf.ApplyFont(this.boldFont);
            this.cell.SetCellValue(rtf);
            this.cell.CellStyle = this.borderStyle;

            this.cell = row.CreateCell(colIndex + 1);
            rtf = new HSSFRichTextString("Value");
            rtf.ApplyFont(this.boldFont);
            this.cell.SetCellValue(rtf);
            this.cell.CellStyle = this.borderStyle;
        }

        private void createQVDataCells(IRow row, int colIndex, int qty, int value)
        {
            this.cell = row.CreateCell(colIndex);
            this.cell.SetCellValue(qty);
            this.cell.CellStyle = this.borderStyle;

            this.cell = row.CreateCell(colIndex + 1);
            this.cell.SetCellValue(value);
            this.cell.CellStyle = this.borderStyle;
        }

        private void createReportHeading(string authority, int year)
        {
            IFont font = wb.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;
            font.FontHeightInPoints = (short)14;

            string message = authority + " Payment History & Performance Report - Year: " + year;
            HSSFRichTextString rtf = new HSSFRichTextString(message);
            rtf.ApplyFont(font);

            this.cell = sheet.CreateRow(0).CreateCell(0);
            this.cell.SetCellValue(rtf);
            this.cell.CellStyle = this.centreStyle;

            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 26);
            sheet.AddMergedRegion(region);
        }

        private void createStringCell(IRow row, int colIndex, string value)
        {
            this.cell = row.CreateCell(colIndex);
            this.cell.SetCellValue(new HSSFRichTextString(value));
            this.cell.CellStyle = this.borderStyle;
        }

        private void createTotalsCells()
        {
            ICellStyle style = wb.CreateCellStyle();
            style.SetFont(this.boldFont);
            style.BorderTop = BorderStyle.THIN;
            style.BorderRight = BorderStyle.THIN;
            style.BorderBottom = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;

            for (int i = 3; i < 8; i++)
            {
                this.cell = sheet.GetRow(i).CreateCell(25);
                String formula = String.Format("B{0}+D{0}+F{0}+H{0}+J{0}+L{0}+N{0}+P{0}+R{0}+T{0}+V{0}+X{0}", i + 1);
                this.cell.SetCellFormula(formula);
                this.cell.CellStyle = this.borderStyle;
                this.cell.CellStyle = style;

                this.cell = sheet.GetRow(i).CreateCell(26);
                formula = String.Format("C{0}+E{0}+G{0}+I{0}+K{0}+M{0}+O{0}+Q{0}+S{0}+U{0}+W{0}+Y{0}", i + 1);
                this.cell.SetCellFormula(formula);
                this.cell.CellStyle = this.borderStyle;
                this.cell.CellStyle = style;
            }
        }
    }
}