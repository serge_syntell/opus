﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.Report
{
    public class FilmStatusData
    {
        // Fields
        private int status;
        private string description;
        private bool isFrame;
        private string filmNo;
        private DateTime firstOffenceDate;
        private int total;

        public DateTime getFirstOffenceDate()
        {
            return firstOffenceDate;
        }

        public void setFirstOffenceDate(DateTime firstOffenceDate)
        {
            this.firstOffenceDate = firstOffenceDate;
        }

        public string getFilmNo()
        {
            return filmNo;
        }

        public void setFilmNo(string filmNo)
        {
            this.filmNo = filmNo;
        }

        public int getTotal()
        {
            return total;
        }

        public void setTotal(int total)
        {
            this.total = total;
        }

        public string getDescription()
        {
            return description;
        }

        public void setDescription(string description)
        {
            this.description = description;
        }

        public int getStatus()
        {
            return status;
        }

        public void setStatus(string value)
        {
            this.status = int.Parse(value);
        }

        public bool getIsFrame()
        {
            return isFrame;
        }

        public void setIsFrame(bool value)
        {
            this.isFrame = value;
        }

        public string getStatusDisplay()
        {
            return this.description + " (" + this.status + ")";
        }
    }

    public class FilmStatusStatisticsReport
    {
        private string connectionString;
        private HSSFWorkbook wb;
        private ISheet sheet;

        // Constants
        private int COLOR_BLACK = 0;
        private int COLOR_BLUE = 1;
        private int COLOR_RED = 2;
        private int COLOR_PURPLE = 3;
        // Fields
        private string authorityName;
        private List<FilmStatusData> alData;
        private int columnCount;
        private int frameColumnCount;
        private int filmColumnCount;
        private ICell cell;
        private IFont fontHeading;
        private ICellStyle styleHeading;
        private ICellStyle blackDateStyle;
        private ICellStyle blueDateStyle;
        private ICellStyle redDateStyle;
        private ICellStyle purpleDateStyle;
        private ICellStyle greenStyle;
        private ICellStyle blueStyle;
        private ICellStyle redStyle;
        private ICellStyle purpleStyle;
        private int noDaysToColour = 30;

        public FilmStatusStatisticsReport(string vConstr)
        {
            connectionString = vConstr;
            wb = new HSSFWorkbook();

            this.alData = new List<FilmStatusData>();

            this.fontHeading = wb.CreateFont();
            this.fontHeading.Boldweight = (short)FontBoldWeight.BOLD;
            this.styleHeading = wb.CreateCellStyle();
            this.styleHeading.Rotation = (short)90;
            this.styleHeading.Alignment = HorizontalAlignment.CENTER;
            this.styleHeading.VerticalAlignment = VerticalAlignment.CENTER;
            IDataFormat format = wb.CreateDataFormat();
            this.blackDateStyle = wb.CreateCellStyle();
            this.blackDateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");
            this.greenStyle = wb.CreateCellStyle();
            IFont font = wb.CreateFont();
            font.Color = IndexedColors.GREEN.Index;
            this.greenStyle.SetFont(font);
            this.purpleStyle = wb.CreateCellStyle();
            font = wb.CreateFont();
            font.Color = IndexedColors.PLUM.Index;
            this.purpleStyle.SetFont(font);
            this.purpleDateStyle = wb.CreateCellStyle();
            this.purpleDateStyle.SetFont(font);
            this.purpleDateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");
            this.blueStyle = wb.CreateCellStyle();
            font = wb.CreateFont();
            font.Color = IndexedColors.BLUE.Index;
            this.blueStyle.SetFont(font);
            this.blueDateStyle = wb.CreateCellStyle();
            this.blueDateStyle.SetFont(font);
            this.blueDateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");
            this.redStyle = wb.CreateCellStyle();
            font = wb.CreateFont();
            font.Color = IndexedColors.RED.Index;
            this.redStyle.SetFont(font);
            this.redDateStyle = wb.CreateCellStyle();
            this.redDateStyle.SetFont(font);
            this.redDateStyle.DataFormat = format.GetFormat("yyyy-MM-dd HH:mm:ss");
        }

        public MemoryStream ExportToExcel(int autIntNo,DateTime before,DateTime after)
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                sheet = wb.CreateSheet();
                wb.SetSheetName(0, "Film Status Statistics");

                noDaysToColour = this.GetDateRule(autIntNo);

                DataSet ds = this.GetReportData(autIntNo, before, after);
                this.authorityName = ds.Tables[0].Rows[0]["AutName"].ToString();
                columnCount = 3;

                FilmStatusData data = null;
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    data = new FilmStatusData();
                    data.setStatus(ds.Tables[1].Rows[i]["Code"].ToString());
                    data.setDescription(ds.Tables[1].Rows[i]["Descr"].ToString());
                    data.setIsFrame(bool.Parse(ds.Tables[1].Rows[i]["IsFrame"].ToString()));
                    data.setFilmNo(ds.Tables[1].Rows[i]["FilmNo"].ToString());
                    data.setFirstOffenceDate(DateTime.Parse(ds.Tables[1].Rows[i]["MinOffenceDate"].ToString()));
                    data.setTotal(ds.Tables[1].Rows[i]["Count"].ToString() == "" ? 0 : int.Parse(ds.Tables[1].Rows[i]["Count"].ToString()));
                    this.alData.Add(data);
                }

                // Get the column names
                this.createHeadingsRow();

                // Write the Heading row
                this.createHeading();

                // Write the rows
                int rowIndex = this.createDataRows();

                // Write the status totals
                this.createStatusTotals(rowIndex);

                // auto-size columns
                for (int i = 1; i < this.columnCount + 3; i++)
                {
                    sheet.AutoSizeColumn((short)i);
                }

                wb.Write(stream);
                return stream;
            }
            catch (Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, "TMS");
                throw e;
            }
        }

        private int GetDateRule(int autIntNo)
        {
            NoticeProcess.DateRulesDetails ruleDetails = new NoticeProcess.DateRulesDetails();
            ruleDetails.AutIntNo = autIntNo;
            ruleDetails.DtRStartDate = "NotOffenceDate";
            ruleDetails.DtREndDate = "NotIssue1stNoticeDate";

            NoticeProcess.DefaultDateRules defaultDateRules = new NoticeProcess.DefaultDateRules(ruleDetails);

            return defaultDateRules.GetDefaultDateRule(ruleDetails).DtRNoOfDays;
        }

        private DataSet GetReportData(int autIntNo, DateTime before, DateTime after)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("FilmStatusStatisticsByDate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.CommandTimeout = 0;//Jerry 2014-10-31 add

            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            com.Parameters.Add("@StartDate", SqlDbType.SmallDateTime).Value = before;
            com.Parameters.Add("@EndDate", SqlDbType.SmallDateTime).Value = after;

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        private void createCellHeadingBorder(int cellIndex)
        {
            this.cell = sheet.GetRow(2).GetCell(cellIndex);
            ICellStyle style = wb.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.Rotation = ((short)90);
            this.cell.CellStyle = style;
        }

        private void createColumnHeading(string columnName, int columnIndex)
        {
            this.cell = sheet.GetRow(2).CreateCell(columnIndex);
            this.cell.CellStyle = this.styleHeading;

            HSSFRichTextString rtf = new HSSFRichTextString(columnName);
            rtf.ApplyFont(this.fontHeading);

            cell.SetCellValue(rtf);
        }

        private int createDataRows()
        {
            string filmNo = null;
            int rowIndex = 3;
            IRow row = null;
            bool isNewRow = true;
            int colIndex = 3;
            int rowColour = COLOR_BLACK;

            FilmStatusData data;
            for (int i = 0; i < this.alData.Count; i++)
            {
                data = this.alData[i];
                // Check if it's a new film
                if (filmNo == null || !filmNo.Equals(data.getFilmNo()))
                {
                    // Add the film total
                    if (filmNo != null)
                    {
                        this.createTotalsCell(row, colIndex++, this.frameColumnCount + 4, this.filmColumnCount - 1);
                    }

                    // Check row colour

                    // Initialise row
                    isNewRow = true;
                    row = sheet.CreateRow(rowIndex++);
                    colIndex = 3;
                    // Write film data
                    rowColour = this.writeFilmData(data, row);
                }

                if (!data.getIsFrame() && isNewRow)
                {
                    // Add the frame totals
                    this.createTotalsCell(row, colIndex++, 3, this.frameColumnCount - 1);

                    isNewRow = false;
                }

                // Add the data cell
                this.writeDataCell(row, colIndex++, data.getTotal(), rowColour);

                // Update the film variable
                filmNo = data.getFilmNo();
            }

            if(row != null)
                this.createTotalsCell(row, colIndex++, this.frameColumnCount + 4, this.filmColumnCount - 1);

            return rowIndex;
        }

        private void createFirstCellHeadingBorder(int collIndex)
        {
            this.cell = sheet.GetRow(2).CreateCell(collIndex);
            ICellStyle style = wb.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.BorderLeft = BorderStyle.THIN;
            style.Rotation = (short)90;
            this.cell.CellStyle = style;
        }

        private void createHeading()
        {
            IFont font = wb.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;
            font.FontHeightInPoints = (short)20;

            ICellStyle style = wb.CreateCellStyle();
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;

            IRow row = sheet.CreateRow(0);
            this.cell = row.CreateCell(0);
            this.cell.CellStyle = style;

            HSSFRichTextString rtf = new HSSFRichTextString(this.authorityName + " - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            rtf.ApplyFont(font);

            this.cell.SetCellValue(rtf);

            CellRangeAddress region = new CellRangeAddress(0, 0, 0, columnCount + 2);
            sheet.AddMergedRegion(region);
        }

        private void createHeadingsRow()
        {
            sheet.CreateRow(1);
            sheet.CreateRow(2);

            int columnIndex = 0;
            this.createColumnHeading("Film No", columnIndex++);
            this.createColumnHeading("1st Offence Date", columnIndex++);
            this.createColumnHeading("Days Since", columnIndex++);

            FilmStatusData data;
            string filmNo = null;

            for(int i =0;i<this.alData.Count;i++)
            {
                data = this.alData[i];

                // Check if we're still in the first film
                if (filmNo != null && !filmNo.Equals(data.getFilmNo()))
                {
                    // Add a film total heading
                    this.createColumnHeading("Total Charges", columnIndex++);
                    break;
                }

                // Count the film / frame column count
                if (data.getIsFrame())
                {
                    this.frameColumnCount++;
                }
                else
                {
                    if (filmColumnCount == 0)
                    {
                        // Add a column total heading
                        this.createColumnHeading("Total Frames", columnIndex++);
                    }
                    this.filmColumnCount++;
                }

                // Write out the statuses
                this.createColumnHeading(data.getStatusDisplay(), columnIndex++);

                // Update the loop checking data
                filmNo = data.getFilmNo();
                this.columnCount++;
            }
            this.columnCount--;

            // Add the frame / film status super-headings
            this.createSuperHeadings("Frame Status", 3, this.frameColumnCount + 3);
            this.createSuperHeadings("Charge Status", this.frameColumnCount + 4, (this.filmColumnCount + this.frameColumnCount + 4));

            // Add the frame  status borders
            int offset = 3;
            for (int i = 0; i <= this.frameColumnCount; i++)
            {
                if (i == 0)
                {
                    this.createFirstCellHeadingBorder(i + offset);
                }
                else
                {
                    if (i == this.frameColumnCount)
                    {
                        this.createLastCellHeadingBorder(this.frameColumnCount + offset);
                    }
                    else
                    {
                        this.createCellHeadingBorder(i + offset);
                    }
                }
            }

            // Add the charge borders
            offset = this.frameColumnCount + offset + 1;
            for (int i = 0; i <= this.filmColumnCount; i++)
            {
                if (i == this.filmColumnCount)
                {
                    this.createLastCellHeadingBorder(this.filmColumnCount + offset);
                }
                else
                {
                    this.createCellHeadingBorder(i + offset);
                }
            }
        }

        private void createLastCellHeadingBorder(int cellIndex)
        {
            this.cell = sheet.GetRow(2).CreateCell(cellIndex);
            ICellStyle style = wb.CreateCellStyle();
            style.BorderBottom = BorderStyle.THIN;
            style.BorderTop = BorderStyle.THIN;
            style.BorderRight =BorderStyle.THIN;
            style.Rotation = (short)90;
            this.cell.CellStyle = style;
        }

        private void createStatusTotalCell(IRow row, int columnIndex)
        {
            this.cell = row.CreateCell(columnIndex);
            string columnLetter = this.getColumnString(columnIndex);
            //String format = MessageFormat.format("SUM({0}{1,number,####}:{0}{2,number,####})", columnLetter, 4, row.RowNum);
            string format = "SUM(" + columnLetter + "4:" + columnLetter + row.RowNum.ToString() + ")";
            this.cell.SetCellFormula(format);
        }

        private void createStatusTotals(int rowIndex)
        {
            IRow row = sheet.CreateRow(rowIndex);

            for (int i = 0; i < this.columnCount; i++)
            {
                this.createStatusTotalCell(row, i + 3);
            }
        }

        private void createSuperHeadings(String value, int first, int last)
        {
            ICellStyle style = wb.CreateCellStyle();
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;

            IRow row = sheet.GetRow(1);
            this.cell = row.CreateCell(first);
            this.cell.CellStyle = style;

            HSSFRichTextString rtf = new HSSFRichTextString(value);
            rtf.ApplyFont(this.fontHeading);

            this.cell.SetCellValue(rtf);

            CellRangeAddress region = new CellRangeAddress(1, 1, first, last);
            sheet.AddMergedRegion(region);
        }

        private void createTotalsCell(IRow row, int colIndex, int startCol, int length)
        {
            this.cell = row.CreateCell(colIndex);
            string startColumnLetter = this.getColumnString(startCol);
            string endColumnLetter = this.getColumnString(length + startCol);
            //string format = MessageFormat.format("SUM({0}{1,number,####}:{2}{1,number,####})", startColumnLetter, row.getRowNum() + 1, endColumnLetter);
            string format = "SUM(" + startColumnLetter + (row.RowNum + 1).ToString() + ":" + endColumnLetter + (row.RowNum + 1).ToString() + ")";
            this.cell.CellStyle = this.greenStyle;
            this.cell.SetCellFormula(format);
        }

        private void writeDataCell(IRow row, int colIndex, int total, int rowColour)
        {
            this.cell = row.CreateCell(colIndex);
            if (total == 0)
            {
                return;
            }

            this.cell.SetCellValue(total);
            this.setCellColour(this.cell, rowColour);
        }

        private int writeFilmData(FilmStatusData data, IRow row)
        {
            int noDays = (DateTime.Now - data.getFirstOffenceDate()).Days + 1;
            int rowColour = COLOR_BLACK;
            if (noDays > this.noDaysToColour)
            {
                rowColour = COLOR_PURPLE;
            }
            else
            {
                if (noDays > this.noDaysToColour - 10)
                {
                    rowColour = COLOR_RED;
                }
                else
                {
                    if (noDays > this.noDaysToColour - 5)
                    {
                        rowColour = COLOR_BLUE;
                    }
                }
            }

            // Film number
            {
                this.cell = row.CreateCell(0);
            }
            HSSFRichTextString rtf = new HSSFRichTextString(data.getFilmNo());
            this.cell.SetCellValue(rtf);
            this.setCellColour(cell, rowColour);

            // First offence
            this.cell = row.CreateCell(1);
            this.cell.SetCellValue(data.getFirstOffenceDate().ToString("yyyy-MM-dd HH:mm:ss"));

            if (rowColour == COLOR_BLUE)
            {
                cell.CellStyle = this.blueStyle;
            }
            else if (rowColour == COLOR_PURPLE)
            {
                cell.CellStyle = this.purpleStyle;
            }
            else if (rowColour == COLOR_RED)
            {
                cell.CellStyle = this.redStyle;
            }

            // Number of days ago
            this.cell = row.CreateCell(2);
            this.cell.SetCellValue(noDays);
            this.setCellColour(cell, rowColour);

            return rowColour;
        }

        private void setCellColour(ICell cell, int color)
        {
            if (color == COLOR_BLUE)
            {
                cell.CellStyle = this.blueStyle;
            }
            else if (color == COLOR_PURPLE)
            {
                cell.CellStyle = this.purpleStyle;
            }
            else if (color == COLOR_RED)
            {
                cell.CellStyle = this.redStyle;
            }
        }

        private string getColumnString(int column)
        {
            char[] A2Z =
            {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
            };
            StringBuilder retval = new StringBuilder();
            int tempcellnum = column;
            do
            {
                retval.Insert(0, A2Z[tempcellnum % 26]);
                tempcellnum = (tempcellnum / 26) - 1;
            }
            while (tempcellnum >= 0);

            return retval.ToString();
        }
    }
}
