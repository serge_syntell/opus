﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.Report
{
    public class DynamicReportWriter
    {
        public HSSFWorkbook wb;
        public ISheet sheet;

        // Fields
        private int columnCount;
        private IFont fontHeading;
        private ICellStyle styleHeading;
        private ICellStyle dateStyle;
        private ICellStyle decimalStyle;

        public DynamicReportWriter()
        {
            wb = new HSSFWorkbook();

            this.fontHeading = wb.CreateFont();
            this.fontHeading.Boldweight = (short)FontBoldWeight.BOLD;
            this.styleHeading = wb.CreateCellStyle();
            this.styleHeading.Rotation = (short)90;
            this.styleHeading.Alignment = HorizontalAlignment.CENTER;
            this.dateStyle = wb.CreateCellStyle();
            IDataFormat format = wb.CreateDataFormat();
            this.dateStyle.DataFormat = format.GetFormat("yyyy-MM-dd");
            this.decimalStyle = this.wb.CreateCellStyle();
            this.decimalStyle.DataFormat = format.GetFormat("#,##0.00");
        }

        public int write(DataTable dt, int startRow, bool sizeColumns)
        {
            try
            {
                // write out the headings
                this.writeHeadingRows(dt, startRow++);

                // Write out the rows
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    this.writeDetailRow(dt.Rows[i], startRow++);
                }

                // Autosize the columns
                if (sizeColumns && startRow < 2000)
                {
                    for (short i = 0; i < this.columnCount; i++)
                    {
                        this.sheet.AutoSizeColumn(i);
                    }
                }
            }
            catch (Exception ex)
            {
                startRow = -1;
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, "TMS");
            }
            
            return startRow;
        }

        private void writeDetailCell(int row, int column, Object value)
        {
            ICell cell = this.sheet.GetRow(row).CreateCell(column);

            if (value == DBNull.Value)
            {
                return;
            }

            if (value is string)
            {
                HSSFRichTextString rtf = new HSSFRichTextString((String)value);
                cell.SetCellValue(rtf);
                return;
            }
            if (value is DateTime)
            {
                cell.CellStyle = this.dateStyle;
                cell.SetCellValue((DateTime)value);
                return;
            }
            if (value is Double)
            {
                cell.CellStyle = this.decimalStyle;
                cell.SetCellValue((Double)value);
                return;
            }
            if (value is int)
            {
                cell.SetCellValue((int)value);
                return;
            }
        }

        private void writeDetailRow(DataRow dr, int row)
        {
            this.sheet.CreateRow(row);

            for (int i = 0; i < this.columnCount; i++)
            {
                try
                {
                    this.writeDetailCell(row, i, dr[i]);
                }
                catch (Exception ex)
                {
                    EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, "TMS");
                }
            }
        }

        private void writeHeaderCell(String columnName, int row, int column)
        {
            ICell cell = this.sheet.GetRow(row).CreateCell(column);
            HSSFRichTextString rtf = new HSSFRichTextString(columnName);
            rtf.ApplyFont(this.fontHeading);
            cell.CellStyle = this.styleHeading;
            cell.SetCellValue(rtf);
        }

        private void writeHeadingRows(DataTable dt, int row)
        {
            try
            {
                this.sheet.CreateRow(row);

                this.columnCount = dt.Columns.Count;

                for (int i = 0; i < this.columnCount; i++)
                {
                    String columnName = dt.Columns[i].ColumnName;
                    this.writeHeaderCell(columnName, row, i);
                }
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, "TMS");
            }

        }
    }
}
