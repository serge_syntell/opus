﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.Report
{
    public class AuditTrailReport:DynamicReportWriter
    {
        private string connectionString;
        private String generatedDate;

        public AuditTrailReport(string vConstr)
        {
            connectionString = vConstr;
            this.generatedDate = "Generated :  " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public MemoryStream ExportToExcel(string auditType, string ticketNo, string regNo, int autIntNo, DateTime before, DateTime after)
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                sheet = wb.CreateSheet();

                DataSet ds = this.GetReportData(auditType, ticketNo, regNo, autIntNo, before, after);

                int lastRow = write(ds.Tables[0], 1, true);
                this.writeTimeStamp(lastRow);

                wb.Write(stream);
                return stream;
            }
            catch (Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, "TMS");
                throw e;
            }
        }

        private DataSet GetReportData(string auditType, string ticketNo, string regNo, int autIntNo, DateTime before, DateTime after)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand();

            if (auditType.IndexOf("Notice") != -1)
            {
                wb.SetSheetName(0, "Notice Audit Report");
                com = new SqlCommand("GetNoticeAuditByDate", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
                com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
                com.Parameters.Add("@RegNo", SqlDbType.NVarChar, 10).Value = regNo;
                com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime).Value = before;
                com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime).Value = after;
            }

            if (auditType.IndexOf("Charge") != -1)
            {
                wb.SetSheetName(0, "Charge Audit Report");
                com = new SqlCommand("GetChargeAuditByDate", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
                com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
                com.Parameters.Add("@RegNo", SqlDbType.NVarChar, 10).Value = regNo;
                com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime).Value = before;
                com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime).Value = after;
            }

            if (auditType.IndexOf("Driver") != -1)
            {
                wb.SetSheetName(0, "POD Audit Report");
                com = new SqlCommand("GetPODAuditByDate", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
                com.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = ticketNo;
                com.Parameters.Add("@RegNo", SqlDbType.NVarChar, 10).Value = regNo;
                com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime).Value = before;
                com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime).Value = after;
            }

            if (auditType.IndexOf("Frame") != -1)
            {
                wb.SetSheetName(0, "Frame History Report");
                com = new SqlCommand("GetFrameHistoryByDate", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
                com.Parameters.Add("@FilmNo", SqlDbType.VarChar, 25).Value = ticketNo;
                com.Parameters.Add("@FrameNo", SqlDbType.NVarChar, 10).Value = regNo;
                com.Parameters.Add("@DateFrom", SqlDbType.SmallDateTime).Value = before;
                com.Parameters.Add("@DateTo", SqlDbType.SmallDateTime).Value = after;
            }

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(com);
            da.Fill(ds);
            da.Dispose();

            return ds;
        }

        private void writeTimeStamp(int lastRow)
        {
            lastRow += 2;
            sheet.CreateRow(lastRow);

            ICell cell = sheet.GetRow(lastRow).CreateCell(0);
            cell.SetCellValue(new HSSFRichTextString(this.generatedDate));
        }

    }
}
