﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Data;
using System.Xml;
using SIL.AARTO.BLL.Report.Model;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Configuration;
using SIL.AARTO.BLL.EntLib;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary; 

namespace SIL.AARTO.BLL.Report
{
    public class ReportManager
    {
        public static TList<SIL.AARTO.DAL.Entities.Authority> GetAllAuthoritys()
        {
            return new SIL.AARTO.DAL.Services.AuthorityService().GetAll();
        }

        public static SIL.AARTO.DAL.Entities.Authority GetAuthorityById(int AutIntNo)
        {
            return new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(AutIntNo);
        }

        public static TList<SIL.AARTO.DAL.Entities.Court> GetAllCourts()
        {
            return new SIL.AARTO.DAL.Services.CourtService().GetAll();
        }

        public static TList<SIL.AARTO.DAL.Entities.ReportStoredProc> GetAllStoredProcName()
        {
            return new SIL.AARTO.DAL.Services.ReportStoredProcService().GetAll();
        }

        public static TList<SIL.AARTO.DAL.Entities.ReportConfig> GetAllReportConfig()
        {
            return new SIL.AARTO.DAL.Services.ReportConfigService().GetAll();
        }

        public static TList<SIL.AARTO.DAL.Entities.ReportConfig> GetReportConfigByIsCustomIsFreeStyle()
        {
            TList<SIL.AARTO.DAL.Entities.ReportConfig> ReportConfglistTotal = new SIL.AARTO.DAL.Services.ReportConfigService().GetAll();
            List<SIL.AARTO.DAL.Entities.ReportConfig> ReportConfigTemp = (from rc in ReportConfglistTotal
                                                                          where rc.IsCustom == true && rc.IsFreeStyle == false
                                                                          select rc).ToList<SIL.AARTO.DAL.Entities.ReportConfig>();
            TList<SIL.AARTO.DAL.Entities.ReportConfig> ReportConfgReturn = new TList<SIL.AARTO.DAL.Entities.ReportConfig>();
            foreach (SIL.AARTO.DAL.Entities.ReportConfig item in ReportConfigTemp)
            {
                ReportConfgReturn.Add(item);
            }
            return ReportConfgReturn;
        }

        public static SIL.AARTO.DAL.Entities.ReportStoredProc GetReportStoredProcById(int RspIntNo)
        {
            return new SIL.AARTO.DAL.Services.ReportStoredProcService().GetByRspIntNo(RspIntNo);
        }

        public static SIL.AARTO.DAL.Entities.ReportConfig GetReportById(int RcIntNo)
        {
            SIL.AARTO.DAL.Services.ReportConfigService rcService = new SIL.AARTO.DAL.Services.ReportConfigService();
            ReportConfig rc = rcService.GetByRcIntNo(RcIntNo);

            rcService.DeepLoad(rc);
            return rc;
        }

        public static void DeepLoadReport(ReportConfig rc)
        {
            new SIL.AARTO.DAL.Services.ReportConfigService().DeepLoad(rc);
        }

        public static void UpdateReport(ReportConfig rc)
        {
            new SIL.AARTO.DAL.Services.ReportConfigService().Update(rc);
        }

        public static bool RemoveReportById(int RcIntNo)
        {
            SIL.AARTO.DAL.Services.ReportConfigService rcService = new SIL.AARTO.DAL.Services.ReportConfigService();
            return rcService.Delete(RcIntNo);
        }

        // 2013-07-17 add parameter lastUser by Henry
        public static bool SaveReport(ReportModel model, int RspIntNo, string AutomaticGenerateIntervalType, string AutomaticGenerateFormat, string lastUser)
        {
            try
            {
               SIL.AARTO.DAL.Data.ReportConfigQuery query = new SIL.AARTO.DAL.Data.ReportConfigQuery();
               query.AppendEquals(ReportConfigColumn.ReportName, model.ReportName.Trim());
                TList<ReportConfig> rcs = new SIL.AARTO.DAL.Services.ReportConfigService().Find(query);

                ReportConfig rc = new ReportConfig();

                if (rcs.Count > 0)
                    rc = rcs[0];

                rc.ReportName = model.ReportName.Trim();
                rc.ReportCode = model.ReportCode;
                rc.RspIntNo = RspIntNo;
                rc.AutIntNo = Convert.ToInt32(model.AutIntNo);
                rc.AutomaticGenerate = model.AutomaticGenerate;
                rc.AutomaticGenerateInterval = model.AutomaticGenerateInterval;
                rc.AutomaticGenerateIntervalType = AutomaticGenerateIntervalType;
                rc.AutomaticGenerateFormat = AutomaticGenerateFormat;

               // System.Runtime.Serialization.

                using(MemoryStream memoryStream = new MemoryStream()) 
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(memoryStream, model);
                    rc.AutomaticGenerateQuery = memoryStream.GetBuffer();
                }

                rc.Template = model.xmlDoc.OuterXml;
                rc.LastUser = lastUser;

                new SIL.AARTO.DAL.Services.ReportConfigService().Save(rc);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, ex.Message);
                return false;
            }
            return true;
        }


        public static DataSet GetData(ReportModel model)
        {
            DataSet ds = new DataSet();

            string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connStr);
            SqlCommand myCommand = new SqlCommand("REPORT_ExecReportWithPages", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterPageNumber = new SqlParameter("@CurrentPage", SqlDbType.Int, 4);
            parameterPageNumber.Value = model.PageNumber;
            myCommand.Parameters.Add(parameterPageNumber);

            SqlParameter parameterPageSize = new SqlParameter("@PageSize", SqlDbType.Int, 4);
            parameterPageSize.Value = 10;
            myCommand.Parameters.Add(parameterPageSize);


            SqlParameter parameterSpName = new SqlParameter("@SpName", SqlDbType.VarChar, 100);
            parameterSpName.Value = model.StoredProcName;
            myCommand.Parameters.Add(parameterSpName);


            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.VarChar, 10);
            parameterAutIntNo.Value = model.AutIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            if(model.DateFrom == DateTime.MinValue || model.DateTo == DateTime.MinValue)
            {
                model.DateFrom = new DateTime(1900, 1, 1);
                model.DateTo = new DateTime(2079, 6, 6);
            }

            SqlParameter parameterDateFrom = new SqlParameter("@DateFrom", SqlDbType.VarChar, 10);
            parameterDateFrom.Value = model.DateFrom.ToString("yyyyMMdd");
            myCommand.Parameters.Add(parameterDateFrom);

            SqlParameter parameterDateTo = new SqlParameter("@DateTo", SqlDbType.VarChar, 10);
            //Jerry 2012-04-27 change
            parameterDateTo.Value = model.DateTo.AddDays(1).ToString("yyyyMMdd"); ;
            myCommand.Parameters.Add(parameterDateTo);

            SqlParameter parameterCourts = new SqlParameter("@Courts", SqlDbType.VarChar, 500);

            String Courts = "";
            foreach (Court court in model.SelectedCourtsList)
            {
                Courts += court.CrtIntNo.ToString() + ",";
            }
            if(Courts.Length > 0)
               Courts = Courts.Substring(0, Courts.Length - 1);

            parameterCourts.Value = Courts;
            myCommand.Parameters.Add(parameterCourts);

            if (model.NoticeType == null)
                model.NoticeType = "";
            SqlParameter parameterNoticeType = new SqlParameter("@NoticeType", SqlDbType.VarChar, 10);
            parameterNoticeType.Value = model.NoticeType;
            myCommand.Parameters.Add(parameterNoticeType);

            if (model.SortField == null)
                model.SortField = "";
            SqlParameter parameterSortField = new SqlParameter("@SortField", SqlDbType.VarChar, 50);
            parameterSortField.Value = model.SortField;
            myCommand.Parameters.Add(parameterSortField);

            if (model.Sort == null)
                model.Sort = "";
            SqlParameter parameterSort = new SqlParameter("@Sort", SqlDbType.VarChar, 10);
            parameterSort.Value = model.Sort;
            myCommand.Parameters.Add(parameterSort);


            SqlDataAdapter sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null,"SP:" + model.StoredProcName + " "+ ex.Message);
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }
            return ds;
        }


        public static SqlDataReader GetDataForExport(ReportModel model)
        {
            DataSet ds = new DataSet();

            string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connStr);
            SqlCommand myCommand = new SqlCommand("REPORT_ExportReport", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterSpName = new SqlParameter("@SpName", SqlDbType.VarChar, 100);
            parameterSpName.Value = model.StoredProcName;
            myCommand.Parameters.Add(parameterSpName);


            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.VarChar, 10);
            parameterAutIntNo.Value = model.AutIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            if (model.DateFrom == DateTime.MinValue || model.DateTo == DateTime.MinValue)
            {
                model.DateFrom = new DateTime(1900, 1, 1);
                model.DateTo = new DateTime(2079, 6, 6);
            }

            SqlParameter parameterDateFrom = new SqlParameter("@DateFrom", SqlDbType.VarChar, 10);
            parameterDateFrom.Value = model.DateFrom.ToString("yyyyMMdd");
            myCommand.Parameters.Add(parameterDateFrom);

            SqlParameter parameterDateTo = new SqlParameter("@DateTo", SqlDbType.VarChar, 10);
            parameterDateTo.Value = model.DateTo.ToString("yyyyMMdd"); ;
            myCommand.Parameters.Add(parameterDateTo);

            SqlParameter parameterCourts = new SqlParameter("@Courts", SqlDbType.VarChar, 500);

            String Courts = "";
            foreach (Court court in model.SelectedCourtsList)
            {
                Courts += court.CrtIntNo.ToString() + ",";
            }
            if (Courts.Length > 0)
                Courts = Courts.Substring(0, Courts.Length - 1);

            parameterCourts.Value = Courts;
            myCommand.Parameters.Add(parameterCourts);

            if (model.NoticeType == null)
                model.NoticeType = "";
            SqlParameter parameterNoticeType = new SqlParameter("@NoticeType", SqlDbType.VarChar, 10);
            parameterNoticeType.Value = model.NoticeType;
            myCommand.Parameters.Add(parameterNoticeType);

            if (model.SortField == null)
                model.SortField = "";
            SqlParameter parameterSortField = new SqlParameter("@SortField", SqlDbType.VarChar, 50);
            parameterSortField.Value = model.SortField;
            myCommand.Parameters.Add(parameterSortField);

            if (model.Sort == null)
                model.Sort = "";
            SqlParameter parameterSort = new SqlParameter("@Sort", SqlDbType.VarChar, 10);
            parameterSort.Value = model.Sort;
            myCommand.Parameters.Add(parameterSort);
            try
            {
                myConnection.Open();
                return myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, "SP:" + model.StoredProcName + " " + ex.Message);
                throw ex;
            }
            finally
            {
            }
            return null;
        }
    }
}
