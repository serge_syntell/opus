﻿using System;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;
using System.IO;
using SIL.AARTO.BLL.Report.Model;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.PageElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF;
using System.Xml;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.BLL.Report
{
    public class ReportExporter
    {

        private string DATAWORDPATH = @"C:\folder\doc\";
        private string EXCELPATH = @"C:\folder\excel\";
        private string TXTPATH = @"C:\folder\txt\";
        private string PDFPATH = @"C:\folder\pdf\";

        private const string WORDPOSTFIX = ".doc";
        private const string EXCELPOSTFIX = ".xls";
        private const string TXTPOSTFIX = ".txt";
        private const string PDFPOSTFIX = ".pdf";

        private const int DATADISTANCE = 5;
        private const int TABDISTANCE = 8;

        public ReportExporter(string strRootPath)
        {
            DATAWORDPATH = System.IO.Path.Combine(strRootPath, @"folder\doc\");
            EXCELPATH = System.IO.Path.Combine(strRootPath, @"folder\excel\");
            TXTPATH = System.IO.Path.Combine(strRootPath, @"folder\txt\");
            PDFPATH = System.IO.Path.Combine(strRootPath, @"folder\pdf\");
        }


        /// <summary>
        /// .xls
        /// </summary>
        /// <param name="ds"></param>
        public string ExportToExcel(ReportModel model, int CleanUpDays)
        {
            string tempFileName = "";
            using (SqlDataReader reader = SIL.AARTO.BLL.Report.ReportManager.GetDataForExport(model))
            {
                tempFileName = GetFileName(model);
                object filename = EXCELPATH + tempFileName + EXCELPOSTFIX;
                if (!Directory.Exists(EXCELPATH))
                    Directory.CreateDirectory(EXCELPATH);

                CleanUpOldFile(EXCELPATH, CleanUpDays);

                object Nothing = System.Reflection.Missing.Value;

                Excel.Application myExcel = new Excel.Application();
                myExcel.Application.Workbooks.Add(Nothing);

                try
                {
                    int i = 0;
                    XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("HeadColumns");
                    int column = nodes[0].ChildNodes.Count;

                    int[] columnLength = new int[column];

                    foreach (XmlNode node in nodes[0].ChildNodes)
                    {
                        if (node.Name == "HeadColumn")
                        {
                            string strText = node.ChildNodes[0].InnerText;
                            myExcel.Cells[7, 1 + i] = strText;
                            columnLength[i] = strText.Length;
                        }
                        i++;
                    }

                    int totalCount = 1;
                    while (reader.Read())
                    {
                        i = 0;
                        foreach (XmlNode node in nodes[0].ChildNodes)
                        {
                            if (node.Name == "HeadColumn")
                            {
                                string strText = reader[node.ChildNodes[1].InnerText].ToString();
                                myExcel.Cells[7 + totalCount, 1 + i] = strText;

                                if (strText.Length > columnLength[i])
                                {
                                    columnLength[i] = strText.Length;
                                }
                            }
                            i++;
                        }
                        totalCount++;
                    }

                    Microsoft.Office.Interop.Excel.Range rangeAll = myExcel.Cells.get_Range(myExcel.Cells[1, 1], myExcel.Cells[totalCount + 7, column]);
                    rangeAll.NumberFormat = "@";

                    Microsoft.Office.Interop.Excel.Range rangeDate = myExcel.Cells.get_Range(myExcel.Cells[4, 2], myExcel.Cells[4, 2]);
                    rangeDate.NumberFormat = "yyyy-mm-dd";

                    myExcel.Cells[1, 1] = "Report Name:";
                    myExcel.Cells[1, 2] = model.ReportName;
                    myExcel.Cells[2, 1] = "Report Code:";
                    myExcel.Cells[2, 2] = model.ReportCode;
                    myExcel.Cells[3, 1] = "LA Name and Code:";
                    myExcel.Cells[3, 2] = model.LANameAndCode;
                    myExcel.Cells[4, 1] = "Date Requested:";
                    myExcel.Cells[4, 2] = DateTime.Now.ToString("yyyy/MM/dd");
                    myExcel.Cells[5, 1] = "Parameters:";
                    myExcel.Cells[5, 2] = model.Parameters.Replace("<BR/>", " \r\n");

                    Microsoft.Office.Interop.Excel.Range rangeParameters = myExcel.Cells.get_Range(myExcel.Cells[5, 2], myExcel.Cells[5, 2]);
                    rangeParameters.WrapText = false;

                    for (i = 0; i < column; i++)
                    {
                        Microsoft.Office.Interop.Excel.Range range = myExcel.Cells.get_Range(myExcel.Cells[7, i + 1], myExcel.Cells[7 + model.ItemCount, i + 1]);
                        range.ColumnWidth = columnLength[i];
                        string strColor = nodes[0].ChildNodes[i].ChildNodes[2].InnerText;
                        range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(Convert.ToInt32(strColor.Substring(1, 2), 16), Convert.ToInt32(strColor.Substring(3, 2), 16), Convert.ToInt32(strColor.Substring(5, 2), 16)));
                    }

                    rangeAll.ColumnWidth = 20;

                    try
                    {
                        myExcel.ActiveWorkbook._SaveAs(filename, Nothing, Nothing, Nothing, Nothing, Nothing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Nothing, Nothing, Nothing, Nothing);
                    }
                    catch
                    {
                        throw new Exception("Can't find the file: " + EXCELPATH + tempFileName + EXCELPOSTFIX);
                    }
                    myExcel.Visible = false;
                }
                catch (Exception e)
                {
                    EntLibLogger.WriteErrorLog(e, LogCategory.Exception, null);
                    throw e;
                }
                finally
                {
                    myExcel.DisplayAlerts = false;
                    myExcel.ActiveWorkbook.Saved = true;
                    myExcel.ActiveWorkbook.Close();
                    myExcel.Workbooks.Close();
                    myExcel.Application.Quit();
                    myExcel.Quit();
                    int generation = System.GC.GetGeneration(myExcel);
                    while (true)
                    {
                        if (System.Runtime.InteropServices.Marshal.ReleaseComObject(myExcel) <= 0)
                            break;
                    }

                    myExcel = null;
                    System.GC.Collect(generation);
                    GC.Collect();
                }
                //}
                //else
                //{
                //    throw new Exception("No Data");
                //}
            }
            return @"/folder/excel/" + tempFileName + EXCELPOSTFIX;
        }


        /// <summary>
        /// .Doc File
        /// </summary>
        /// <param name="ds"></param>
        public string ExportToWord(ReportModel model, int CleanUpDays)
        {
            string tempFileName = "";
            using (SqlDataReader reader = SIL.AARTO.BLL.Report.ReportManager.GetDataForExport(model))
            {
                object filename = null;

                object tableBehavior = Word.WdDefaultTableBehavior.wdWord9TableBehavior;
                object autoFitBehavior = Word.WdAutoFitBehavior.wdAutoFitFixed;

                object unit = Word.WdUnits.wdStory;
                object extend = System.Reflection.Missing.Value;
                object breakType = (int)Word.WdBreakType.wdSectionBreakNextPage;

                object count = 1;
                object character = Word.WdUnits.wdCharacter;

                object Nothing = System.Reflection.Missing.Value;

                Word.Application myWord = new Word.Application();
                try
                {
                    tempFileName = GetFileName(model);
                    filename = DATAWORDPATH + tempFileName + WORDPOSTFIX;

                    if (!Directory.Exists(DATAWORDPATH))
                        Directory.CreateDirectory(DATAWORDPATH);

                    CleanUpOldFile(DATAWORDPATH, CleanUpDays);

                    Word._Document myDoc = new Word.Document();
                    myDoc = myWord.Documents.Add(ref Nothing, ref Nothing, ref Nothing, ref Nothing);
                    myDoc.Activate();

                    int i = 0;
                    XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("HeadColumns");
                    int column = nodes[0].ChildNodes.Count;

                    myWord.Application.Selection.TypeText("Report Name:      " + model.ReportName);
                    myWord.Application.Selection.TypeParagraph();
                    myWord.Application.Selection.TypeText("Report Code:      " + model.ReportCode);
                    myWord.Application.Selection.TypeParagraph();
                    myWord.Application.Selection.TypeText("LA Name and Code: " + model.LANameAndCode);
                    myWord.Application.Selection.TypeParagraph();
                    myWord.Application.Selection.TypeText("Date Requested:   " + DateTime.Now.ToString("yyyy/MM/dd"));
                    myWord.Application.Selection.TypeParagraph();
                    myWord.Application.Selection.TypeText("Parameters:       " + model.Parameters.Replace("<BR/>", "\r\n \t\t"));

                    myWord.Application.Selection.TypeParagraph();
                    Word.Range para = myWord.Application.Selection.Range;
                    myDoc.Tables.Add(para, model.ItemCount + 2, nodes[0].ChildNodes.Count, ref tableBehavior, ref autoFitBehavior);


                    Microsoft.Office.Interop.Word.WdColor[] ColumnColors = new Microsoft.Office.Interop.Word.WdColor[column];

                    foreach (XmlNode node in nodes[0].ChildNodes)
                    {
                        if (node.Name == "HeadColumn")
                        {
                            Word.Range range = myDoc.Tables[1].Cell(1, i + 1).Range;
                            string strColor = nodes[0].ChildNodes[i].ChildNodes[2].InnerText;
                            ColumnColors[i] = (Microsoft.Office.Interop.Word.WdColor)System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(Convert.ToInt32(strColor.Substring(1, 2), 16), Convert.ToInt32(strColor.Substring(3, 2), 16), Convert.ToInt32(strColor.Substring(5, 2), 16)));
                            range.InsertBefore(node.ChildNodes[0].InnerText);
                            range.Font.Color = ColumnColors[i];
                        }
                        i++;
                    }

                    int totalCount = 1;
                    while (reader.Read())
                    {
                        i = 0;
                        foreach (XmlNode node in nodes[0].ChildNodes)
                        {
                            if (node.Name == "HeadColumn")
                            {
                                Word.Range range = myDoc.Tables[1].Cell(totalCount + 2, i + 1).Range;
                                range.InsertBefore(reader[node.ChildNodes[1].InnerText].ToString());
                                range.Font.Color = ColumnColors[i];

                            }
                            i++;
                        }
                        totalCount++;
                    }

                    myWord.Application.Selection.EndKey(ref unit, ref extend);
                    myWord.Application.Selection.TypeParagraph();
                    myWord.Application.Selection.TypeParagraph();
                    myWord.Application.Selection.InsertBreak(ref breakType);

                    myWord.Application.Selection.TypeBackspace();
                    myWord.Application.Selection.Delete(ref character, ref count);
                    myWord.Application.Selection.HomeKey(ref unit, ref extend);

                    try
                    {
                        myDoc.SaveAs(ref filename, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing);
                        myWord.Visible = false;
                    }
                    catch
                    {
                        throw new Exception("Can't find the file: " + DATAWORDPATH + tempFileName + WORDPOSTFIX);
                    }
                    myWord.Visible = false;
                }
                catch (Exception ex)
                {
                    EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                    throw ex;
                }
                finally
                {
                    myWord.Quit(ref Nothing, ref Nothing, ref Nothing);
                    int generation = System.GC.GetGeneration(myWord);
                    myWord = null;
                    System.GC.Collect(generation);
                    GC.Collect();
                }
                //}
                //else
                //{
                //    throw new Exception("No Data");
                //}
            }
            return @"/folder/doc/" + tempFileName + WORDPOSTFIX;
        }

        /// <summary>
        /// .txt
        /// </summary>
        /// <param name="ds"></param>
        public string ExportToTxt(ReportModel model, int CleanUpDays)
        {
            string tempFileName = "";
            tempFileName = GetFileName(model);

            FileInfo file = new FileInfo(TXTPATH + tempFileName + TXTPOSTFIX);
            if (!Directory.Exists(TXTPATH))
                Directory.CreateDirectory(TXTPATH);

            CleanUpOldFile(TXTPATH, CleanUpDays);

            StreamWriter textFile = null;
            try
            {
                textFile = file.CreateText();
            }
            catch
            {
                throw new Exception("Can't find the file: " + TXTPATH + tempFileName + TXTPOSTFIX);
            }

            try
            {
                XmlNodeList nodes = model.xmlDoc.GetElementsByTagName("HeadColumns");
                int column = nodes[0].ChildNodes.Count;
                int totalLength = 0;

                int titleLength = 0;
                int[] columnLength = new int[column];

                for (int i = 0; i < column; i++)
                {
                    columnLength[i] = nodes[0].ChildNodes[i].ChildNodes[0].InnerText.Length;
                }

                using (SqlDataReader reader = SIL.AARTO.BLL.Report.ReportManager.GetDataForExport(model))
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int i = 0;
                            foreach (XmlNode node in nodes[0].ChildNodes)
                            {
                                if (node.Name == "HeadColumn")
                                {
                                    string strText = reader[nodes[0].ChildNodes[i].ChildNodes[1].InnerText].ToString();
                                    if (strText.Length > columnLength[i])
                                    {
                                        columnLength[i] = strText.Length;
                                    }
                                }
                                i++;
                            }
                        }
                    }
                }

                for (int i = 0; i < column; i++)
                {
                    totalLength = totalLength + columnLength[i] + DATADISTANCE;
                }
                totalLength = totalLength + 2 * TABDISTANCE - DATADISTANCE;

                string strTableName = model.ReportName;
                titleLength = strTableName.Length;

                for (int i = 0; i < (int)((totalLength - titleLength) / 2); i++)
                {
                    textFile.Write(' ');
                }
                textFile.Write(strTableName);
                textFile.WriteLine();

                //string strReportCode = "Report Code:";
                textFile.Write("Report Code:".PadRight(20));
                int CodeLength = 30;
                if (model.ReportCode.Length > 20)
                    CodeLength = model.ReportCode.Length + 10;

                textFile.Write(model.ReportCode.PadRight(CodeLength));

                textFile.Write("LA Name and Code:".PadRight(20));
                textFile.Write(model.LANameAndCode);

                textFile.WriteLine();

                textFile.Write("Date Requested:".PadRight(20));
                textFile.Write(DateTime.Now.ToString("yyyy/MM/dd").PadRight(CodeLength));

                textFile.Write("Parameters:".PadRight(20));
                string[] strParameters = model.Parameters.Split(new string[1] { "<BR/>" }, StringSplitOptions.None);


                for (int i = 0; i < strParameters.Length; i++)
                {
                    string strParameter = strParameters[i].Trim();

                    int iRow = 0;
                    int iParameterLength = totalLength - 40 - CodeLength;
                    if (iParameterLength < 40)
                        iParameterLength = 40;
                    while (strParameter.Length > iParameterLength)
                    {
                        if (iRow == 0 && i == 0)
                            textFile.Write(strParameter.Substring(0, iParameterLength));
                        else
                            textFile.Write(strParameter.Substring(0, iParameterLength).PadLeft(iParameterLength + 40 + CodeLength));

                        strParameter = strParameter.Substring(iParameterLength, strParameter.Length - iParameterLength);
                        textFile.WriteLine();
                    }

                    if (iRow == 0 && i == 0)
                        textFile.Write(strParameter);
                    else
                        textFile.Write(strParameter.PadLeft(strParameter.Length + 40 + CodeLength));
                    textFile.WriteLine();
                }
                for (int i = 0; i < totalLength; i++)
                {
                    textFile.Write('*');
                }
                textFile.WriteLine();
                textFile.Write("\t");


                for (int i = 0; i < column; i++)
                {
                    textFile.Write(nodes[0].ChildNodes[i].ChildNodes[0].InnerText);
                    for (int k = 0; k < columnLength[i] - nodes[0].ChildNodes[i].ChildNodes[0].InnerText.Length + DATADISTANCE; k++)
                    {
                        textFile.Write(' ');
                    }
                }
                textFile.WriteLine();
                for (int i = 0; i < totalLength; i++)
                {
                    textFile.Write('-');
                }
                textFile.WriteLine();
                textFile.Write("\t");

                using (SqlDataReader reader = SIL.AARTO.BLL.Report.ReportManager.GetDataForExport(model))
                {
                    if (reader.HasRows)
                    {
                        int totalCount = 1;
                        while (reader.Read())
                        {
                            int i = 0;
                            foreach (XmlNode node in nodes[0].ChildNodes)
                            {
                                if (node.Name == "HeadColumn")
                                {
                                    string strText = reader[nodes[0].ChildNodes[i].ChildNodes[1].InnerText].ToString();
                                    textFile.Write(strText);

                                    for (int k = 0; k < columnLength[i] - strText.Length + DATADISTANCE; k++)
                                    {
                                        textFile.Write(' ');
                                    }
                                }
                                i++;
                            }
                            textFile.WriteLine();
                            textFile.Write("\t");
                        }
                    }
                }

                textFile.WriteLine();
                for (int i = 0; i < totalLength; i++)
                {
                    textFile.Write('-');
                }
                textFile.WriteLine();
                textFile.WriteLine();
                textFile.WriteLine();

                textFile.Close();
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteErrorLog(ex, LogCategory.Exception, null);
                throw ex;
            }

            return @"/folder/txt/" + tempFileName + TXTPOSTFIX;
        }



        public byte[] ExportToPDF(ReportModel model, bool Generate, int CleanUpDays)
        {
            string filename = null;
            if (Generate)
            {
                string tempFileName = "";
                tempFileName = GetFileName(model);
                filename = PDFPATH + tempFileName + PDFPOSTFIX;

                if (!Directory.Exists(PDFPATH))
                    Directory.CreateDirectory(PDFPATH);

                CleanUpOldFile(PDFPATH, CleanUpDays);
            }

            PDFExporter pDFExporter = new PDFExporter();
            return pDFExporter.generatePDF(model, filename);
        }

        private string GetFileName(ReportModel model)
        {
            return model.ReportName + "-" + DateTime.Now.ToString("yyyyMMddhhmmss");
        }

        private void CleanUpOldFile(string strFolder, int CleanUpDays)
        {
            string[] files = Directory.GetFiles(strFolder);
            foreach(string file in files)
            {

                FileInfo fileInfo = new FileInfo(file);
                if (fileInfo.CreationTime < DateTime.Now.AddDays(-1 * CleanUpDays))
                {
                    fileInfo.Delete();
                }
            }
        }

    }
} 