﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;

using SIL.AARTO.BLL.EntLib;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.Report
{
    public class ChangeOfOwnerReport
    {
        private HSSFWorkbook wb;
        private ISheet sheet;
        public ChangeOfOwnerReport()
        {
            wb = new HSSFWorkbook();
        }

        public MemoryStream ExportToExcel(string companyName,string proxy,string officer, DataTable dt)
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                sheet = wb.CreateSheet();
                wb.SetSheetName(0, "Proxy Nomination Notice");

                this.createHeading(companyName, proxy, officer);
                this.createTitle();
                this.createDataRows(dt);

                for (int i = 0; i < 5; i++)
                {
                    sheet.AutoSizeColumn((short)i);
                }

                wb.Write(stream);
                return stream;
            }
            catch (Exception e)
            {
                EntLibLogger.WriteErrorLog(e, LogCategory.Exception, "COO");
                throw e;
            }
        }

        private void createHeading(string companyName, string proxy, string officer)
        {
            IFont font = wb.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;
            font.FontHeightInPoints = (short)20;

            ICellStyle style = wb.CreateCellStyle();
            style.Alignment = HorizontalAlignment.LEFT;
            style.VerticalAlignment = VerticalAlignment.CENTER;

            sheet.CreateRow(0);

            ICell cell = sheet.GetRow(0).CreateCell(0);
            cell.CellStyle = style;
            HSSFRichTextString rtf = new HSSFRichTextString("Submitted by " + companyName + " Proxy " + proxy);
            rtf.ApplyFont(font);
            cell.SetCellValue(rtf);
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, 4);
            sheet.AddMergedRegion(region);

            IFont font1 = wb.CreateFont();
            font1.Boldweight = (short)FontBoldWeight.BOLD;

            sheet.CreateRow(1);

            ICell cell1 = sheet.GetRow(1).CreateCell(0);
            cell1.CellStyle = style;
            HSSFRichTextString rtf1 = new HSSFRichTextString("Processed by Officer:" + officer);
            rtf1.ApplyFont(font1);
            cell1.SetCellValue(rtf1);
            CellRangeAddress region1 = new CellRangeAddress(1, 1, 0, 3);
            sheet.AddMergedRegion(region1);

            ICell cell2 = sheet.GetRow(1).CreateCell(4);
            cell2.CellStyle = style;
            HSSFRichTextString rtf2 = new HSSFRichTextString(DateTime.Now.ToString("yyyy-MM-dd"));
            rtf2.ApplyFont(font1);
            cell2.SetCellValue(rtf2);
        }

        private void createTitle()
        {
            sheet.CreateRow(2);
            IFont fontHeading = wb.CreateFont();
            fontHeading.Boldweight = (short)FontBoldWeight.BOLD;
            ICellStyle styleHeading = wb.CreateCellStyle();
            styleHeading.Alignment = HorizontalAlignment.CENTER;
            styleHeading.VerticalAlignment = VerticalAlignment.CENTER;

            ICell cell1 = sheet.GetRow(2).CreateCell(0);
            cell1.CellStyle = styleHeading;
            HSSFRichTextString rtf1 = new HSSFRichTextString("Notice No");
            rtf1.ApplyFont(fontHeading);
            cell1.SetCellValue(rtf1);

            ICell cell2 = sheet.GetRow(2).CreateCell(1);
            cell2.CellStyle = styleHeading;
            HSSFRichTextString rtf2 = new HSSFRichTextString("Registration");
            rtf2.ApplyFont(fontHeading);
            cell2.SetCellValue(rtf2);

            ICell cell3 = sheet.GetRow(2).CreateCell(2);
            cell3.CellStyle = styleHeading;
            HSSFRichTextString rtf3 = new HSSFRichTextString("Offence Date");
            rtf3.ApplyFont(fontHeading);
            cell3.SetCellValue(rtf3);

            ICell cell4 = sheet.GetRow(2).CreateCell(3);
            cell4.CellStyle = styleHeading;
            HSSFRichTextString rtf4 = new HSSFRichTextString("Offender");
            rtf4.ApplyFont(fontHeading);
            cell4.SetCellValue(rtf4);

            ICell cell5 = sheet.GetRow(2).CreateCell(4);
            cell5.CellStyle = styleHeading;
            HSSFRichTextString rtf5 = new HSSFRichTextString("Status");
            rtf5.ApplyFont(fontHeading);
            cell5.SetCellValue(rtf5);
        }

        private void createDataRows(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheet.CreateRow(3 + i);

                ICell cell1 = sheet.GetRow(3 + i).CreateCell(0);
                cell1.SetCellValue(dt.Rows[i]["Notice No"].ToString());

                ICell cell2 = sheet.GetRow(3 + i).CreateCell(1);
                cell2.SetCellValue(dt.Rows[i]["Registration"].ToString());

                ICell cell3 = sheet.GetRow(3 + i).CreateCell(2);
                cell3.SetCellValue(dt.Rows[i]["Offence Date"].ToString());

                ICell cell4 = sheet.GetRow(3 + i).CreateCell(3);
                cell4.SetCellValue(dt.Rows[i]["Offender"].ToString());

                ICell cell5 = sheet.GetRow(3 + i).CreateCell(4);
                cell5.SetCellValue(dt.Rows[i]["Status"].ToString());
            }
        }
    }
}
