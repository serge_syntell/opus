﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Xml;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Report.Model
{
    public enum ReortFiltersEnum { Notice_Type, Search_Branch, Date_Range, Courts, Representation_Type };
    public enum NoticeTypeEnum {S54, S56, S341}
    public enum CourtsEnum { A, B, C, D, E }

    public class Column
    {
        public int Index { get; set; }
        private string _headGroupColumnText;//added by Jacob 20120907
        public string HeadGroupColumnText
        {
            get { return _headGroupColumnText; }
            set { _headGroupColumnText = value;
                HeadText = value;//added by Jacob 20120907- copy value to HeadText.
            }
        }
        public string HeadText { get; set; }
        public string DataFiled { get; set; }
        public string ColumnColor { get; set; }
        public string FontColor { get; set; }
        public bool Sort { get; set; }

    }

    [Serializable]
    public class ReportModel
    {
        
        public XmlDocument xmlDoc;

        public TList<Court> CourtsList { get; set; }
        public TList<Court> SelectedCourtsList { get; set; }
        [NonSerialized]
        public SelectList ReportTypeList;

        public int RcIntNo { get; set; }// added by Jacob 20120830
        public string ReportName { get; set; }
        public string ReportCode { get; set; }

        [NonSerialized]
        public SelectList LocalAuthorityList;
        [NonSerialized]
        public SelectList StoredProcNameList;
        [NonSerialized]
        public SelectList DataFieldList;
        [NonSerialized]
        public SelectList ReortFiltersList;
        [NonSerialized]
        public SelectList NoticeTypeList;
        [NonSerialized]
        public SelectList AutomaticGenerateIntervalTypeList;
        [NonSerialized]
        public SelectList AutomaticGenerateFormatList;

        public string StoredProcName { get; set; }
        public string LANameAndCode { get; set; }
        public string Parameters { get; set; }

        public string HeadGroupColumnText { get; set; }
        public string HeadGroupColumnsCount { get; set; }
        public string HeadGroupColumnColour { get; set; }

        public string HeadColumn { get; set; }
        public string HeadColumnDataField { get; set; }
        public string HeadColumnColour { get; set; }

        public bool ColumnIsSortable { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public String NoticeType { get; set; }
        public String SortField { get; set; }
        public String Sort { get; set; }
        public int PageNumber { get; set; }
        public int TotalPageCount { get; set; }
        public int ItemCount { get; set; }
        public string AutIntNo { get; set; }

        public string Remark { get; set; }

        public bool AutomaticGenerate { get; set; }
        public Int32 AutomaticGenerateInterval { get; set; }
        public string AutomaticGenerateIntervalType { get; set; }
        public string AutomaticGenerateFormat { get; set; }

        //2013-04-15 add by Henry for pagination
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public int TotalPage { get { return (int)Math.Ceiling(this.TotalCount / (this.PageSize * 1.0));} }

        //20131217 added by Jacob start
        //filter when getting data
        public string FileNameForFilter { get; set; }
        public string DocumentNumberForFilter { get; set; }
        //20131217 added by Jacob end
        [NonSerialized]
        public List<ReortFiltersEnum> filters;

        public ReportModel()
        {
        }

        //public static string ToJson(this Object obj)
        //{
        //    return new JavaScriptSerializer().Serialize(obj);
        //}
    }

    [Serializable]
    public class DataField
    {
        public int DataFieldId { get; set; }
        public string DataFieldName { get; set; }
        public string DataFieldValue { get; set; }
    }

    public class Item
    {
        public Item(string value)
        {
            Text = value;
            Value = value;
        }

        public Item(string value, string text)
        {
            Text = text;
            Value = value;
        }

        public int Id { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
