﻿using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.ScannedDigitaldocument
{
    public class DigitaldocumentManager
    {
        private NoticeService noticeService = new NoticeService();
        private AartoDocumentImageService docImageService = new AartoDocumentImageService();
        private AartoDocumentImageTypeService docImgTypeService = new AartoDocumentImageTypeService();
        private OffenceTypeService offenceService = new OffenceTypeService();
        private ImageFileServerService imgFileServer = new ImageFileServerService();

        private AartoNoticeDocumentService noticeDocumentService = new AartoNoticeDocumentService();
        private AartoDocumentImageService documentImageService = new AartoDocumentImageService();
        private AartoDocumentTypeService documentTypeService = new AartoDocumentTypeService();
        private AartoDocumentStatusService documentStatusService = new AartoDocumentStatusService();

        public Notice[] GetNoticeList(Notice whereEntity, Int32 page_index, Int32 page_size, out Int32 record_count)
        {
            StringBuilder where = new StringBuilder("1=1");
            if (whereEntity != null)
            {
                if (whereEntity.NotIntNo != 0) where.AppendLine(string.Format(" AND NotIntNo={0}", whereEntity.NotIntNo));
                if (whereEntity.AutIntNo != 0) where.AppendLine(string.Format(" And AutIntNo={0}", whereEntity.AutIntNo));
                if (!string.IsNullOrEmpty(whereEntity.NotRegNo)) where.AppendLine(string.Format(" AND NotRegNo='{0}'", whereEntity.NotRegNo));
                if (!string.IsNullOrEmpty(whereEntity.NotTicketNo)) where.AppendLine(string.Format(" AND NotTicketNo LIKE '{0}%'", whereEntity.NotTicketNo));
            }
            return noticeService.GetPaged(where.ToString(), "NotIntNo", page_index, page_size, out record_count).ToArray();
        }

        public OffenceType[] GetOffenceTypeList(OffenceType whereEntity)
        {
            StringBuilder where = new StringBuilder("1=1");
            if (whereEntity != null)
            {

            }

            return offenceService.GetAll().ToArray();
        }

        public AartoDocumentImage[] GetDocumentImageByNotice(int notIntNo)
        {
            var list = docImageService.GetByNotice(notIntNo);
           return list.Where(c => c.AaDocImgTypeId == (int)AartoDocumentImageTypeList.ChangeOfOffender
                || c.AaDocImgTypeId == (int)AartoDocumentImageTypeList.PresentationOfDocument
                || c.AaDocImgTypeId == (int)AartoDocumentImageTypeList.Representation).ToArray();
        
        }

        public AartoDocumentImageType[] GetDocumentImageTypeList()
        {
            return docImgTypeService.GetAll().ToArray();
        }

        public Notice GetNotice(int notIntNo)
        {
            return noticeService.GetByNotIntNo(notIntNo);
        }

        public Notice GetNotice(string ticketNo)
        {
            return noticeService.GetByNotTicketNo(ticketNo).FirstOrDefault();
        }

        public ImageFileServer GetImageFileServer(string serverName)
        {
            return imgFileServer.Find(string.Format("IFServerName ='{0}'", serverName)).FirstOrDefault();
        }

        /// <summary>
        ///   Get External Documents Image File Server          
        /// </summary>
        /// <returns></returns>
        public ImageFileServer GetExternalDocumentsImageFileServer()
        {
            return imgFileServer.GetByAaSysFileTypeId((int)AartoSystemFileTypeList.ExternalDocuments).FirstOrDefault();
        }

        public AartoDocumentImage GetDocumentImage(Int32 imgIntNo)
        {
            return documentImageService.GetByAaDocImgId(imgIntNo);
        }

        public long AddNoticeDocumentAndRetrunId(AartoNoticeDocument entity)
        {
            if (entity == null)
            {
                return 0;
            }
            var result= noticeDocumentService.Save(entity);
            return result.AaNotDocId;
        }

        public AartoNoticeDocument GetNoticeDocumentByNotIntId(int notIntNo)
        {
           return noticeDocumentService.GetByNotIntNo(notIntNo).FirstOrDefault();
        }

        public AartoDocumentImage AddDocumentImage(AartoDocumentImage entity)
        {
            if (entity == null)
            {
                return null;
            }

            var result = documentImageService.Save(entity);
            return result;
        }

        public AartoDocumentType GetDocumentTypeByTypeName(string docTypeName)
        {
           return documentTypeService.GetByAaDocTypeName(docTypeName);
        }

        public AartoDocumentType GetDocumentTypeByTypeId(Int32 typeId)
        {
            return documentTypeService.GetByAaDocTypeId(typeId);
        }

        public AartoDocumentType GetExternalDocumentDocumentType()
        {
            return GetDocumentTypeByTypeId((int)AartoDocumentTypeList.ExternalDocument);
        }

        public AartoDocumentStatus[] GetDocumentStatusList()
        {
           return documentStatusService.GetAll().ToArray();
        }

        public bool DeleteDocumentImage(Int32 imgIntNo)
        {
          return  documentImageService.Delete(documentImageService.GetByAaDocImgId(imgIntNo));
        }

    }
}
