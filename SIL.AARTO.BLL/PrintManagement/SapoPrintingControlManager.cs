﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;


namespace SIL.AARTO.BLL.PrintManagement
{
    public class SapoPrintingControlManager
    {
        private  SapoPrintingControlService sapoPrintingControlService = null;
        private SapoReportTypeService reportService = null;

        public SapoPrintingControl[] GetNoticePrintControl(Int32 pageIndex, Int32 pageSize, SapoPrintingControl model, out int totalRecordCount)
        {
            if (sapoPrintingControlService == null)
            {
                sapoPrintingControlService = new SapoPrintingControlService();
            }
            StringBuilder where = new StringBuilder("1=1");

            if (model != null && !string.IsNullOrEmpty(model.SppcOption))
            {
                where.AppendLine(string.Format("AND Option={0}", model.SppcOption));
            }

            if (model != null && !string.IsNullOrEmpty(model.SppcTemplate))
            {
                where.AppendLine(string.Format("AND Template={0}", model.SppcTemplate));
            }

            //if (model != null && !string.IsNullOrEmpty(model.SppcPrintMethod))
            //{
            //    where.AppendLine(string.Format("AND SpcChannel={0}", model.SppcPrintMethod));
            //}

            var list = sapoPrintingControlService.GetPaged(where.ToString(), "SPPCIntNo", pageIndex, pageSize, out totalRecordCount);
            return list.ToArray();
        }

        public bool SaveNoticePrintControls(TList<SapoPrintingControl> modelList)
        {
            if (sapoPrintingControlService == null)
            {
                sapoPrintingControlService = new SapoPrintingControlService();
            }
            sapoPrintingControlService.Save(modelList);
            return true;
        }

        public SapoPrintingControl GetNoticePrintControl(int nPCIntNo)
        {
            if (sapoPrintingControlService == null)
            {
                sapoPrintingControlService = new SapoPrintingControlService();
            }

            return sapoPrintingControlService.GetBySppcIntNo(nPCIntNo);
        }

        public TList<SapoReportType> GetSAPOReportType()
        {
            if (reportService == null)
            {
                reportService = new SapoReportTypeService();
            }
            return reportService.GetAll();
        }
    }
}
