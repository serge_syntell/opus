﻿using Stalberg.TMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.PrintManagement
{
    public class PrintFileManagement
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

        public string GetPrintFilePath(int notIntNo, string notTicketNo)
        {
            using(var context = new SIL.AARTO.Web.DAL.OpusDB())
            { 
                var imageFileServer = context.ImageFileServers.Where(x => x.AaSysFileTypeID == 50).FirstOrDefault();
                var printFileName = context.spGetPrintingFileName(notIntNo).FirstOrDefault();

                var output = string.Format(@"\\{0}\{1}\{2}\{3}.pdf", imageFileServer.ImageMachineName, imageFileServer.ImageShareName, printFileName, notTicketNo.Replace("/", ""));
                return output;
            }
        }

        public string GetPrintFilePath(string notIntNo, string notTicketNo)
        {
            return GetPrintFilePath(int.Parse(notIntNo), notTicketNo);
        }

        public string GetPrintFilePath(string NoticeIntNo)
        {
            int notIntNo;

            if(int.TryParse(NoticeIntNo, out notIntNo))
            {
                GetPrintFilePath(notIntNo);
            }

            return null;
        }

        public string GetPrintFilePath(int notIntNo)
        {
            using (var context = new SIL.AARTO.Web.DAL.OpusDB())
            {
                var NotTicketNo = context.Notices.Where(x => x.NotIntNo == notIntNo).Select(y => y.NotTicketNo).FirstOrDefault();

                return GetPrintFilePath(notIntNo, NotTicketNo);
            }
        }
    }
}
