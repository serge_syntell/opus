﻿using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.PrintManagement
{
    public class SAPOEquivalenceManager
    {
        private  SapoEquivalenceService sapoEquivalenceService = null;

        public SapoEquivalence[] GetSAPOEquivalence(Int32 pageIndex, Int32 pageSize, SapoEquivalence model, out int totalRecordCount)
        {
            if (sapoEquivalenceService == null)
            {
                sapoEquivalenceService = new SapoEquivalenceService();
            }
            StringBuilder where = new StringBuilder();

            if (model != null && !string.IsNullOrEmpty(model.SpEqOpusLabel))
            {
                where.Append(where.ToString().Equals(string.Empty) ? string.Empty : "OR");
                where.AppendLine(string.Format(" SpEqOpusLabel LIKE '{0}%' ", model.SpEqOpusLabel));
            }

            if (model != null && !string.IsNullOrEmpty(model.SpEqSapoLabel))
            {
                where.Append(where.ToString().Equals(string.Empty) ?  string.Empty:"OR");
                where.AppendLine(string.Format(" SpEqSapoLabel LIKE '{0}%'", model.SpEqSapoLabel));
            }

            var list = sapoEquivalenceService.GetPaged(where.ToString(), "SPEqIntNo", pageIndex, pageSize, out totalRecordCount);
            return list.ToArray();
        }

        public bool SaveSapoEquivalence(TList<SapoEquivalence> modelList)
        {
            if (sapoEquivalenceService == null)
            {
                sapoEquivalenceService = new SapoEquivalenceService();
            }
            sapoEquivalenceService.Save(modelList);
            return true;
        }

        public SapoEquivalence GetSapoEquivalence(int nPCIntNo)
        {
            if (sapoEquivalenceService == null)
            {
                sapoEquivalenceService = new SapoEquivalenceService();
            }

            sapoEquivalenceService.Find(new DAL.Data.SqlFilterParameterCollection());

            return sapoEquivalenceService.GetBySpEqIntNo(nPCIntNo);
        }

        public TList<SapoEquivalence> GetSapoEquivalence(DAL.Data.SqlFilterParameterCollection parameterCollection)
        {
            return sapoEquivalenceService.Find(parameterCollection);
        }

    }
}
