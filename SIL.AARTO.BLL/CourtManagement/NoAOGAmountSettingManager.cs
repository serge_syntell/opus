﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;


namespace SIL.AARTO.BLL.CourtManagement
{
    public class NoAOGAmountSettingEntity
    {
        public int SumIntNo { get; set; }
        public string SummonsNo { get; set; }
        public string WoaNumber { get; set; }
        public string SumCourtDate { get; set; }
        public string NotTicketNo { get; set; }
        public string ChgIsMain { get; set; }
        public int ChgSequence { get; set; }
        public int CrtIntNo { get; set; }
        public decimal ChgRevFineAmount { get; set; }
        public string ChgNoAOG { get; set; }
    }

    public class NoAOGChargeEntity
    {
        public int ChgIntNo { get; set; }
        public int Sequence { get; set; }
        public string ChgIsMain { get; set; }
        public float FineAmount { get; set; }
        public string FormatedAmount { get; set; }
        public string ChgType { get; set; }
        public string OffCode { get; set; }
        public string OffDescr { get; set; }
    }

    public class NoAOGChageJsonEntity
    {
        public int SumIntNo { get; set; }
        public List<NoAOGChargeEntity> ChargeList { get; set; }
        public string Comment { get; set; }
        public int CoMaIntNo { get; set; }
    }

    public class NoAOGAmountSettingManager
    {
        private readonly string connStr;
        public NoAOGAmountSettingManager(string connstr)
        {
            this.connStr = connstr;
        }

        public int SetAmountForNoAOG(NoAOGChageJsonEntity entity, string lastUser)
        {
            ChargeService chargeService = new ChargeService();
            ChargeSumChargeService cscService = new ChargeSumChargeService();
            SumChargeService sumChargeService = new SumChargeService();

            NoticeSummons ns = new NoticeSummonsService().DeepLoadBySumIntNo(entity.SumIntNo, true, DAL.Data.DeepLoadType.IncludeChildren, new Type[] { typeof(Notice), typeof(Summons) }).FirstOrDefault();
            if (ns == null) return -4;

            Summons summons = ns.SumIntNoSource;
            if (summons == null) return -5;

            if (summons.PaymentOfNoAogPublicProsecutor.HasValue)
            {
                return -7;
            }

            Notice notice = ns.NotIntNoSource;
            if (notice == null) return -3;


            SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.NoAOGAmountSettingSnapshotManager snapshotManager = new SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.NoAOGAmountSettingSnapshotManager(this.connStr);

            List<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo).Where(c => c.ChgNoAog == "Y" && c.ChargeStatus < 900).ToList();

            foreach (NoAOGChargeEntity c in entity.ChargeList)
            {
                if (!charges.Exists(t => t.ChgIntNo.Equals(c.ChgIntNo)))
                {
                    return -2;
                }
            }

            //List<SumCharge> sumCharges = sumChargeService.GetBySumIntNo(summons.SumIntNo).Where(c => c.SchNoAog == "Y" && c.SumChargeStatus < 900).ToList();


            Charge tmpCharge = null;
            int mainChargeId = entity.ChargeList[0].ChgIntNo;

            bool flag = snapshotManager.SetSnapshot(summons.SumIntNo, false, lastUser);

            if (!flag)
            {
                return -6;
            }
            using (ConnectionScope.CreateTransaction())
            {
                // 2015-01-08, Oscar added NoAogAmountUpdated (5385)
                if ((notice.IsNoAog || notice.IsSection35)
                    && !notice.NoAogAmountUpdated.GetValueOrDefault())
                    new NoticeService().Save(notice);

                float fineAmount = 0;
                foreach (NoAOGChargeEntity c in entity.ChargeList)
                {

                    tmpCharge = charges.Where(t => t.ChgIntNo == c.ChgIntNo).FirstOrDefault();
                    if (tmpCharge != null)
                    {
                        tmpCharge.ChgRevFineAmount = c.FineAmount;
                        tmpCharge.LastUser = lastUser;
                    }
                    if (tmpCharge.ChgIsMain)
                    {
                        mainChargeId = tmpCharge.ChgIntNo;
                        fineAmount = tmpCharge.ChgRevFineAmount;
                    }
                    chargeService.Save(tmpCharge);

                    ChargeSumCharge csc = cscService.GetByChgIntNo(c.ChgIntNo).FirstOrDefault();

                    if (csc != null)
                    {
                        SumCharge sc = sumChargeService.GetBySchIntNo(csc.SchIntNo);
                        if (sc.SumChargeStatus < 900 && sc.SchNoAog == "Y")
                        {
                            sc.SchRevAmount = (decimal)c.FineAmount;
                            sc.LastUser = lastUser;
                            sumChargeService.Save(sc);
                        }
                    }

                }

                summons.PaymentOfNoAogPublicProsecutor = entity.CoMaIntNo;
                summons.PaymentOfNoAogComment = entity.Comment;

                EvidencePack ep = new EvidencePack()
                {
                    ChgIntNo = mainChargeId,
                    //EpItemAlternateDescr = "",
                    EpItemDate = DateTime.Now,
                    EpItemDescr = String.Format("Fine amount set to R{1} by {0}", lastUser, fineAmount.ToString("N")),
                    EpSourceId = summons.SumIntNo,
                    EpSourceTable = "Summons",
                    EpTransactionDate = DateTime.Now,
                    LastUser = lastUser

                };

                new EvidencePackService().Save(ep);

                new SummonsService().Save(summons);

                ConnectionScope.Complete();
            }

            return 1;
        }

        //Jake 2014-11-25 removed court and court room parameter and pagination
        //public List<NoAOGAmountSettingEntity> GetResultForNoAOGAmountSetting(int crtIntNo, int crtRIntNo, string summonsNo, int pageIndex, int pageSize, out int totalCount)
        public List<NoAOGAmountSettingEntity> GetResultForNoAOGAmountSetting(string summonsNo)
        {
            //totalCount = 0;
            List<NoAOGAmountSettingEntity> result = new List<NoAOGAmountSettingEntity>();
            using (SqlConnection conn = new SqlConnection(this.connStr))
            {
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SearchListForNoAOGAmountSetting";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@CrtIntNo", SqlDbType.Int));
                //cmd.Parameters.Add(new SqlParameter("@CrtRIntNo", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@Number", SqlDbType.NVarChar, 50));
                //cmd.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));
                //cmd.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int));

                //cmd.Parameters[0].Value = crtIntNo;
                //cmd.Parameters[1].Value = crtRIntNo;
                cmd.Parameters[0].Value = summonsNo;
                //cmd.Parameters[3].Value = pageSize;
                //cmd.Parameters[4].Value = pageIndex;

                conn.Open();

                IDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    result.Add(new NoAOGAmountSettingEntity()
                    {
                        SumIntNo = Convert.ToInt32(reader["SumIntNo"].ToString()),
                        NotTicketNo = reader["NotTicketNo"].ToString(),
                        SummonsNo = reader["SummonsNo"].ToString(),
                        WoaNumber = reader["WOANumber"].ToString(),
                        SumCourtDate = reader["SumCourtDate"] is DBNull ? "" : Convert.ToDateTime(reader["SumCourtDate"]).ToString("yyyy-MM-dd"),
                        CrtIntNo = Convert.ToInt32(reader["CrtIntNo"])
                        //ChgFineAmount = reader["ChgRevFineAmount"] is DBNull ? 999999 : Convert.ToDecimal(reader["ChgRevFineAmount"]),
                        //ChgIsMain = (bool)reader["ChgIsMain"] ? "Y" : "N",
                        //ChgNoAOG = reader["ChgNoAOG"].ToString(),
                        //ChgSequence = Convert.ToInt32(reader["ChgSequence"])
                    });
                }

                //if (reader.NextResult())
                //{
                //    while (reader.Read())
                //    {
                //        totalCount = Convert.ToInt32(reader.GetInt32(0));
                //        break;
                //    }
                //}

                conn.Close();
            }
            return result;
        }

        //Jake 2014-11-25 removed court and court room parameter and pagination
        //public List<NoAOGAmountSettingEntity> GetResultForNoAOGMountReversal(int crtIntNo, int crtRIntNo, string summonsNo, int pageIndex, int pageSize, out int totalCount)
        public List<NoAOGAmountSettingEntity> GetResultForNoAOGMountReversal(string summonsNo)
        {
            //totalCount = 0;
            List<NoAOGAmountSettingEntity> result = new List<NoAOGAmountSettingEntity>();
            using (SqlConnection conn = new SqlConnection(this.connStr))
            {
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SearchListForNoAOGAmountReversal";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@CrtIntNo", SqlDbType.Int));
                //cmd.Parameters.Add(new SqlParameter("@CrtRIntNo", SqlDbType.Int));
                cmd.Parameters.Add(new SqlParameter("@Number", SqlDbType.NVarChar, 50));
                //cmd.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));
                //cmd.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int));

                //cmd.Parameters[0].Value = crtIntNo;
                //cmd.Parameters[1].Value = crtRIntNo;
                cmd.Parameters[0].Value = summonsNo;
                //cmd.Parameters[3].Value = pageSize;
                //cmd.Parameters[4].Value = pageIndex;

                conn.Open();

                IDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    result.Add(new NoAOGAmountSettingEntity()
                    {
                        SumIntNo = Convert.ToInt32(reader["SumIntNo"].ToString()),
                        NotTicketNo = reader["NotTicketNo"].ToString(),
                        SummonsNo = reader["SummonsNo"].ToString(),
                        WoaNumber = reader["WOANumber"].ToString(),
                        CrtIntNo = Convert.ToInt32(reader["CrtIntNo"]),
                        SumCourtDate = reader["SumCourtDate"] is DBNull ? "" : Convert.ToDateTime(reader["SumCourtDate"]).ToString("yyyy-MM-dd")
                        //ChgRevFineAmount = reader["ChgRevFineAmount"] is DBNull ? 999999 : Convert.ToDecimal(reader["ChgRevFineAmount"]),
                        //ChgIsMain = (bool)reader["ChgIsMain"] ? "Y" : "N",
                        //ChgNoAOG = reader["ChgNoAOG"].ToString(),
                        //ChgSequence = Convert.ToInt32(reader["ChgSequence"])
                    });
                }
                //if (reader.NextResult())
                //{
                //    while (reader.Read())
                //    {
                //        totalCount = Convert.ToInt32(reader.GetInt32(0));
                //        break;
                //    }
                //}
                conn.Close();
            }
            return result;
        }

        public List<NoAOGChargeEntity> GetNoAOGCharges(int sumIntNo)
        {
            List<NoAOGChargeEntity> list = new List<NoAOGChargeEntity>();
            NoticeSummons noticeSummons = new NoticeSummonsService().GetBySumIntNo(sumIntNo).FirstOrDefault();
            if (noticeSummons == null) return list;

            List<Charge> charges = new ChargeService().GetByNotIntNo(noticeSummons.NotIntNo).Where(c => c.ChgNoAog == "Y" && c.ChargeStatus < 900).ToList();

            Notice notice = new NoticeService().GetByNotIntNo(noticeSummons.NotIntNo);

            if (charges != null)
            {
                foreach (Charge c in charges)
                {
                    list.Add(new NoAOGChargeEntity()
                    {
                        ChgIntNo = c.ChgIntNo,
                        ChgIsMain = (c.ChgIsMain == true ? "Y" : "N"),
                        Sequence = c.ChgSequence,
                        ChgType = notice.IsNoAog ? "NoAOG" : (notice.IsSection35 ? "S35" : "NoAOG"),
                        OffCode = c.ChgOffenceCode,
                        OffDescr = c.ChgOffenceDescr + " " + c.ChgOffenceDescr2 + " " + c.ChgOffenceDescr3,
                        FineAmount = c.ChgRevFineAmount,
                        FormatedAmount = String.Format("R{0}", c.ChgRevFineAmount.ToString("N"))
                    });
                }
            }

            return list;
        }

        public NoAOGChageJsonEntity GetNoAOGChargesForReverse(int sumIntNo)
        {
            NoAOGChageJsonEntity jsonObject = new NoAOGChageJsonEntity();
            List<NoAOGChargeEntity> list = new List<NoAOGChargeEntity>();
            NoticeSummons noticeSummons = new NoticeSummonsService().GetBySumIntNo(sumIntNo).FirstOrDefault();
            if (noticeSummons == null) return jsonObject;

            List<Charge> charges = new ChargeService().GetByNotIntNo(noticeSummons.NotIntNo).Where(c => c.ChgNoAog == "Y" && c.ChargeStatus < 900).ToList();

            Notice notice = new NoticeService().GetByNotIntNo(noticeSummons.NotIntNo);

            Summons summons = new SummonsService().GetBySumIntNo(sumIntNo);

            if (charges != null)
            {
                foreach (Charge c in charges)
                {
                    list.Add(new NoAOGChargeEntity()
                    {
                        ChgIntNo = c.ChgIntNo,
                        ChgIsMain = (c.ChgIsMain == true ? "Y" : "N"),
                        Sequence = c.ChgSequence,
                        ChgType = notice.IsNoAog ? "NoAOG" : (notice.IsSection35 ? "S35" : "NoAOG"),
                        OffCode = c.ChgOffenceCode,
                        OffDescr = c.ChgOffenceDescr + " " + c.ChgOffenceDescr2 + " " + c.ChgOffenceDescr3,
                        FineAmount = c.ChgRevFineAmount,
                        FormatedAmount = String.Format("R{0}", c.ChgRevFineAmount.ToString("N"))
                    });
                }

                jsonObject.ChargeList = list;
                jsonObject.SumIntNo = sumIntNo;
                jsonObject.Comment = summons.PaymentOfNoAogComment;
                jsonObject.CoMaIntNo = summons.PaymentOfNoAogPublicProsecutor ?? 0;
            }

            return jsonObject;
        }

        public int ReverseAmountForNoAOG(int sumIntNo, string lastUser)
        {
            SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.NoAOGAmountSettingSnapshotManager snapshotManager = new SIL.AARTO.BLL.PaymentOfNoAOGSnapshot.NoAOGAmountSettingSnapshotManager(this.connStr);
            return snapshotManager.RollBackFromSnapshot(sumIntNo, lastUser);
        }

        public TList<CourtMagistrate> GetMagistrate(int crtIntNo)
        {
            CourtMagistrateService cmservice = new CourtMagistrateService();
            TList<CourtMagistrate> magList = cmservice.GetByCrtIntNo(crtIntNo);
            magList.Insert(0, new CourtMagistrate() { CoMaIntNo = 0, MagistrateName = "Please select a public prosecutor" });
            return magList;
            //this.ddlMagistrate.DataSource = magList;
            //this.ddlMagistrate.DataTextField = "MagistrateName";
            //this.ddlMagistrate.DataValueField = "CoMaIntNo";
            //this.ddlMagistrate.DataBind();

            //this.ddlMagistrate.Items.Insert(0, new ListItem("Please select a magistrate", ""));
        }

    }
}
