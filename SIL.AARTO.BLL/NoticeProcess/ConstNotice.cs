﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NoticeProcess
{
    public class ConstNotice
    {
        public const string TICKET_PROCESSOR = "AARTO";
        public const string CTYPE = "CAM";    

        public const int STATUS_TICKETS = 10;
        public const int STATUS_NOTICE = 900;
        public const int STATUS_1ST_NOTICE_POSTED = 255;
        public const int STATUS_NOTICE_FIRST = 600;
        public const int STATUS_NOTICE_LAST = 800;
        public const int STATUS_AMOUNT_ADDED = 7;
        public const int STATUS_LOADED = 5;
        public const int STATUS_NO_FINE = 6;
        public const int STATUS_EXPIRED = 921;
        public const int STATUS_NO_AOG = 500;

        //public const int TRANSACTION_TYPE = 90;
        
    }
}
