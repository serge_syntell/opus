﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.NoticeProcess
{
    public class AuthorityRulesDetails
    {
        public Int32 AutIntNo;
        public string ARCode;
        public string ARDescr;
        public int ARNumeric;
        public string ARString;
        public string ARComment;
        public string LastUser;

        public AuthorityRulesDetails()
        {
        }
        public AuthorityRulesDetails(int AutIntNo, string ARCode)
        {
            this.AutIntNo = AutIntNo;
            this.ARCode = ARCode;
        }
    }

    public class DefaultAuthRules
    {
        protected AuthorityRulesDetails rule = new AuthorityRulesDetails();

        public AuthorityRulesDetails Rule
        {
            get
            {
                return rule;
            }
        }

        public DefaultAuthRules(AuthorityRulesDetails authRule)
        {
            this.rule = authRule;
        }

        #region Jerry 2012-05-28 disable
//        public KeyValuePair<int, string> SetDefaultAuthRule()
//        {
//            switch (this.rule.ARCode)
//            {
//                case "0400":
//                    this.rule.ARComment = "Y = Default, process NaTIS data after Adjudication, N = Process NaTIS data before verification and before Adjudication";
//                    this.rule.ARDescr = "Rule that indicates whether to process NaTIS data last, i.e. after Adjudication.";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "0510":
//                    this.rule.ARComment = "Y = Yes; N= No(default)";
//                    this.rule.ARDescr = "Rule to bypass verification/adjudication for specific reasons for rejection";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "0520":
//                    this.rule.ARComment = "Y = Yes; N= No(default)";
//                    this.rule.ARDescr = "Rule to bypass verification/adjudication where RegNo = 0000000000";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "0530":
//                    this.rule.ARComment = "Y = Yes; N = No(default)";
//                    this.rule.ARDescr = "Rule to bypass verification by comparing NATIS values to a lookup table of vehicle makes and types";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "0550":
//                    this.rule.ARComment = "Y = Yes, only show NaTIS discrepancy frames; N = No, shows all frames of the film.";
//                    this.rule.ARDescr = "Rule to indicate whether to just show NaTIS discrepancy frames or all frames for verification.";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "0560": //BD
//                    this.rule.ARComment = "Y = Yes, N = No";
//                    this.rule.ARDescr = "Require officer no. to be entered at adjudication";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//               case "0570": //FT
//                    this.rule.ARComment = "B = Batch; R = Real time; N = Classic or old NATIS (default)";
//                    this.rule.ARDescr = "Rule to indicate if interface with eNaTIS is in Batch mode or Real time mode or Classic (default)";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;
                    
//                //091013 FT Add new rule for reset frame status
//                case "0580":
//                    this.rule.ARComment = "Y = Yes; N= No(default)";
//                    this.rule.ARDescr = "Allow reset of frame status to adjudication";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;
//                case "0600":
//                    this.rule.ARComment = "Y - Yes; N - No(Default)";
//                    this.rule.ARDescr = "Rule to ensure that 100% of frames are Adjudicated";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                //dls 091106 - i don't think this was ever implemented
//                case "0610":
//                    this.rule.ARComment = "Y - Yes; N - No(Default)";
//                    this.rule.ARDescr = "Rule to show the digital film infoblock";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "0650":
//                    this.rule.ARComment = "N=No (Default); Y=Yes;";
//                    this.rule.ARDescr = "Display original image with cross hairs at adjudication centred on cross hair";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                //DLS 2009-11-06 ?no longer used: we use the AutTicketProcessor and the Notice.TicketProcessor values  
//                //case "0715": //BD
//                //    this.rule.ARComment = "TMS_Notice = Default; Ciprus = Civitas";
//                //    this.rule.ARDescr = "Format of External Interface";
//                //    this.rule.ARString = "TMS_Notice";
//                //    this.rule.ARNumeric = 0;
//                //    break;

//                case "1510": //BD
//                    this.rule.ARComment = "0 = unlimited; Any other numeric value = no of rows to add [default = 600 for 1.44 drives]";
//                    this.rule.ARDescr = "Rule to indicate how many NaTIS records to load into .INP files";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "2399":
//                    this.rule.ARComment = "N = No (Default); Y = Yes";
//                    this.rule.ARDescr = "Rule to generate compressed images for notices";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "2400":
//                    this.rule.ARComment = "80 (Default); ";
//                    this.rule.ARDescr = "Rule to set maximum width (in mm) of the vehicle image printed on the notice for AARTO";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 80;
//                    break;

//                case "2410":
//                    this.rule.ARComment = "65 (Default); ";
//                    this.rule.ARDescr = "Rule to set maximum height (in mm) of the vehicle image printed on the notice for AARTO";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 65;
//                    break;

//                case "2420":
//                    this.rule.ARComment = "39 (Default); ";
//                    this.rule.ARDescr = "Rule to set maximum width (in mm) of the license plate image printed on the notice for AARTO";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 39;
//                    break;

//                case "2430":
//                    this.rule.ARComment = "15 (Default); ";
//                    this.rule.ARDescr = "Rule to set maximum height (in mm) of the license plate image printed on the notice for AARTO";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 15;
//                    break;

//                case "2440":
//                    this.rule.ARComment = "7 (Default); ";
//                    this.rule.ARDescr = "Rule to set maximum kilobytes the converted image string can be for the infridgement file for AARTO";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 7;
//                    break;

//                case "2510": //BD
//                    this.rule.ARComment = "1 = print 1 image (A); 2 = print 2 images (A+B); 3 = print 2 images (A+R)";
//                    this.rule.ARDescr = "Rules for notice printing - # images to print";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 1;
//                    break;

//                case "2520": //BD
//                    this.rule.ARComment = "1 = print 1st image (default); 2 = print 2nd image";
//                    this.rule.ARDescr = "Rules for notice printing - select print image for TraffiPax (P)";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 1;
//                    break;

//                case "2530": //BD
//                    this.rule.ARComment = "1 = print 1st image (default); 2 = print 2nd image";
//                    this.rule.ARDescr = "Rules for notice printing - select print image for Digital (D)";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 1;
//                    break;

//                //dls 2010-07-29 - obsolete
//                //case "2540": //BD
//                //    this.rule.ARComment = "Y = Yes; N = No (Default)";
//                //    this.rule.ARDescr = "Rule for saving images to File System for Adjudication/Verification";
//                //    this.rule.ARString = "N";
//                //    this.rule.ARNumeric = 0;
//                //    break;

//                case "2550": //BD
//                    this.rule.ARComment = "Y = Notice will be printed based on ChargeStatus : < 260 -> 1st notice, >= 260 -> 2nd Notice (default); N = Notice will be printed based on which Notice Print viewer screen is used";
//                    this.rule.ARDescr = "Rule for re-printing a notice using the format as determined by the current status of the notice";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                //091013 FT Add new rule for use changed contrast
//                case "2560":
//                    this.rule.ARComment = "Y = Yes; N= No(default)";
//                    this.rule.ARDescr = "Use changed contrast and brightness setting";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;
//                //Add by Tod Zhang on 090707 to extract road block file
//                case "2600":
//                    this.rule.ARComment = "Y = Yes (Default); N = No";
//                    this.rule.ARDescr = "Roadblock extract – include AARTO infringement notices as well";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "2610":
//                    this.rule.ARComment = "default = 2 (2 a.m.)";
//                    this.rule.ARDescr = "Time of day to run the Roadblock extract - 24 hour clock format";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 2;
//                    break;

//                //Add by Tod Zhang on 090709 to extract road block false number plate file
//                case "2620":
//                    this.rule.ARComment = "default = 02 = 2am";
//                    this.rule.ARDescr = "Time of day to run the Roadblock extract for false number plates - 24 hour clock format";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 2;
//                    break;

//                case "2800":
//                    this.rule.ARComment = "4000000 (Default); ";
//                    this.rule.ARDescr = "Rule to set maximum kilobytes that can fit into one batch (DVD)";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 4000000;
//                    break;

//                case "2810":
//                    this.rule.ARComment = "N (Default); ";
//                    this.rule.ARDescr = "Rule to set whether the compressed images stored in the database under each notice must be resized";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "3100":
//                    this.rule.ARComment = "Y = Yes (Default); N = No";
//                    this.rule.ARDescr = "Display cross-hairs for DigiCam images";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "3150":
//                    this.rule.ARComment = "0 = Standard yellow cross (Default); 1 = Yellow Cross with missing centre; 2 = Standard inverted cross; 3 = Inverted cross with missing centre";
//                    this.rule.ARDescr = "Style of DigiCam cross-hairs";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "3200": //BD
//                    this.rule.ARComment = "N = No (Default); Y = Yes";
//                    this.rule.ARDescr = "Print cross-hairs for DigiCam images on notices";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "3300":
//                    this.rule.ARComment = "N = No (Default); Y = Yes";
//                    this.rule.ARDescr = "Rule for whether to display all notices for an ID when processing a payment.";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "3502":
//                    this.rule.ARComment = "Y = Yes; N = No (Default) [After Processing, this rule will be set back to 'N']";
//                    this.rule.ARDescr = "Rule that indicates whether to allocate EasyPay numbers retroactively and create special second notices";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4050": //BD
//                    this.rule.ARComment = "Representations may not be logged after a certain no. of days before court date";
//                    this.rule.ARDescr = "Rule to indicate no. of days before court date that a representation may be logged";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 10;
//                    break;

//                case "4100": //DLS
//                    this.rule.ARComment = "30 (Default);";
//                    this.rule.ARDescr = "Rule to set Representation logged grace period (no. of days)";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 30;
//                    break;

//                case "4110": //DLS
//                    this.rule.ARComment = "N = Hold (Default); Y = Cancel";
//                    this.rule.ARDescr = "Rule to Hold (N) / cancel (Y) the logged Representation after the grace period has expired";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                // 2011-09-29 jerry add
//                case "4120":
//                    this.rule.ARComment = "N = Print letters one at a time (Default); Y = Print as batch daily";
//                    this.rule.ARDescr = "Batch printing of representation letters";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4250": //BD
//                    this.rule.ARComment = "1 or Y = true, second notices should be printed for authority (default).";
//                    this.rule.ARDescr = "Rule for Whether to Print Second Notices";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 1;
//                    break;

//                case "4400":
//                    this.rule.ARComment = "N=No (Default); Y=Yes; default selects only those specifically searched for or previously selected";
//                    this.rule.ARDescr = "Rule to check all notices as ready for payment.";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4505":
//                    this.rule.ARComment = "The number of days that will be subtracted from the court date as the cut-off date for payment.";
//                    this.rule.ARDescr = "Rule indicating the number of days before the court date that payments will still be accepted.";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 1;
//                    break;

//                case "4510":
//                    this.rule.ARComment = "Y = Yes; N= No(default)";
//                    this.rule.ARDescr = "Rule to indicate using of postal receipt for all receipts";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4520":
//                    this.rule.ARComment = "Y = Yes; N= No(default) mandatory";
//                    this.rule.ARDescr = "Rule to make the receipt telephone number optional";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4521":
//                    this.rule.ARComment = "Y = Yes; N= No(default) mandatory";
//                    this.rule.ARDescr = "Rule to make the receipt received from field optional";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4522":
//                    this.rule.ARComment = "N=No (Default); Y=Yes";
//                    this.rule.ARDescr = "Enable processing of unauthorised Summonses & Notices at roadblocks";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4531": //BD
//                    this.rule.ARComment = "Y = Yes; N= No(default) mandatory";
//                    this.rule.ARDescr = "Rule to make the ID number optional";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4532": //BD
//                    this.rule.ARComment = "Y = Yes; N= No(default) mandatory";
//                    this.rule.ARDescr = "Rule to make the Forenames optional";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4540": //BD
//                    this.rule.ARComment = "0 - no default Dot Matrix Printer (Default); 1 - Set Default Dot Matrix Printer (Enter the Printer Name in the ARString column)";
//                    this.rule.ARDescr = "Rule to get dot matrix printer for postal receipts";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4550":
//                    this.rule.ARComment = "N = No (Default); Y = Yes";
//                    this.rule.ARDescr = "Rule to set one receipt per notice";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4560": //BD
//                    this.rule.ARComment = "NA = Foreign; RSA (default)";
//                    this.rule.ARDescr = "Rule to set citizenship default to RSA";
//                    this.rule.ARString = "RSA";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4570":
//                    this.rule.ARComment = "900 (Default); See ChargeStatus table for allowable values (status must be 255 or greater)";
//                    this.rule.ARDescr = "Rule to set maximum charge status at which payment/representations are allowed";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 900;
//                    break;

//                case "4580": //BD
//                    this.rule.ARComment = "N = False; Y = true (Default)";
//                    this.rule.ARDescr = "Display End of Day Balancing Hints";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4590": //BD
//                    this.rule.ARComment = "N = No (Default), Y = Yes";
//                    this.rule.ARDescr = "Rule to show/print receipts when processing electronic payments";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4591": //BD
//                    this.rule.ARComment = "Y = Yes; N = No (Default)";
//                    this.rule.ARDescr = "Rule that indicates whether to include electronic/manual captured payments in the EOD report";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4595": //BD
//                    this.rule.ARComment = "Y=Set ; N=Not Set (default)";
//                    this.rule.ARDescr = "Rule to set one-step representations";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4600":
//                    this.rule.ARComment = "N = on-the-fly representations are NOT allowed (Default), Y = they are!";
//                    this.rule.ARDescr = "Rule to Allow On-The-Fly Representations in cash receipting.";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4605":
//                    this.rule.ARComment = "Default = 255 (posted).";
//                    this.rule.ARDescr = "Status to start showing notices on enquiry screen.";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 255;
//                    break;

//                case "4650": //BD
//                    this.rule.ARComment = "Either: NotPaymentDate (default) = the AOG date on the Notice table;  CDCourtRollCreatedDate = the date that the court roll is created; WithdrawSummonsRegardless = do not check for case number, etc";
//                    this.rule.ARDescr = "Rule to determine which date rule to use as the cutoff date for representations.";
//                    this.rule.ARString = "NotPaymentDate";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4660": //sql
//                    this.rule.ARComment = "The no. of days that will be added to the representation date to recalculate the payment date (first/second depending on the status of the notice)";
//                    this.rule.ARDescr = "Rule to indicate no. of days to add to change of offender/change of registration date to recalculate the notice payment date";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 30;
//                    break;

//                //DLS 2009-11-06 ?no longer used
//                //case "4700": //BD
//                //    this.rule.ARComment = "";
//                //    this.rule.ARDescr = "Rule to allow Promun Interface receipt processing";
//                //    this.rule.ARString = string.Empty;
//                //    this.rule.ARNumeric = 0;
//                //    break;

//                case "4710":
//                    this.rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash once the court date has passed.";
//                    this.rule.ARDescr = "Rule to check if allowed to accept cash for notices where the court date has passed.";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4720":
//                    this.rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash after warrant issued";
//                    this.rule.ARDescr = "Rule to check if allowed to accept cash for notices where a warrant has been issued.";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4730":
//                    this.rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash once the court date has passed.";
//                    this.rule.ARDescr = "Rule to check if allowed to accept cash for notices where the court date has passed.";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4740":
//                    this.rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash after the grace period has expired.";
//                    this.rule.ARDescr = "Rule to check if allowed to accept cash for notices where the grace period has expired.";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

////dls - added 2010-09-09 book management
//                case "4810":
//                    this.rule.ARComment = "Y = Book No. format: K233; N = Book No. format: Start-End (Default)";
//                    this.rule.ARDescr = "Book Management Book Numbering Method";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4820":
//                    this.rule.ARComment = "Default = 7";
//                    this.rule.ARDescr = "Book Management maximum number of books allowed to an officer";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 7;
//                    break;

//                case "4830":
//                    this.rule.ARComment = "Y=Yes(default); N=No";
//                    this.rule.ARDescr = "An affidavit for the cancelled document must be supplied";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;
//                case "4900":
//                    this.rule.ARComment = "Y = true (Default); N = false";
//                    this.rule.ARDescr = "Rule as to whether Forenames are required for summons generation.";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "4905": //BD
//                    this.rule.ARComment = "N = No (Default), Y = Yes";
//                    this.rule.ARDescr = "Rule to hide prefix in Ticket Search";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "5010": //BD
//                    this.rule.ARComment = "200 (Default)";
//                    this.rule.ARDescr = "Rule to obtain the minimum amount for processing a summons";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 200;
//                    break;

//                case "5020": //BD
//                    this.rule.ARComment = "Default 10%";
//                    this.rule.ARDescr = "Rule to reserve % of court appearances for S56 summons";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 10;
//                    break;

//                case "5030":
//                    this.rule.ARComment = "N=No (Default); Y=Yes";
//                    this.rule.ARDescr = "Summons generate - use original fine amount";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                //2011-4-20 jerry add
//                case "5040":
//                    this.rule.ARComment = "N = Court number (Default); R = Court room number";
//                    this.rule.ARDescr = "Rule to change the display of court number to court room number.";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                //2010-1-14 ?no longer used
//                //case "5050": //BD
//                //    this.rule.ARComment = "Y = Yes (Default); N = No";
//                //    this.rule.ARDescr = "Summons must have forenames";
//                //    this.rule.ARString = "Y";
//                //    this.rule.ARNumeric = 0;
//                //    break;

//                case "5060": //BD
//                    this.rule.ARComment = "Y = true (Default); N = false;";
//                    this.rule.ARDescr = "Rule as to whether ID Numbers are required for summons generation.";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                //dls rule no longer in use - misunderstanding on usage of different stationery types. CPA6 is used for HWO = separate issue
//                //case "5070":
//                //    this.rule.ARComment = "CPA6 ; CPA5 (default)";
//                //    this.rule.ARDescr = "Rule to set summons print stationery type";
//                //    this.rule.ARString = "CPA5";
//                //    this.rule.ARNumeric = 0;
//                //    break;

//                case "5100": // jerry 2010-12-27 for batch summons
//                    this.rule.ARComment = "Default = 200";
//                    this.rule.ARDescr = "Rule for the size of Summons print file batches";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 200;
//                    break;
//                case "5600": //BD
//                    this.rule.ARComment = "No. of Days (default = 7)";
//                    this.rule.ARDescr = "Rule for grace period (no of days) for payment after judgement captured";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 7;
//                    break;

//                case "5800": //DLS - replaces 7800
//                    this.rule.ARComment = "Default = 2 days";
//                    this.rule.ARDescr = "Rule that indicates how many days must elapse before generating the Notice of WOA";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 2;
//                    break;

//                case "5801": //DLS  - replaces 7100
//                    this.rule.ARComment = "N = No (Default); Y = Yes [NB. Please check that the Date rule for Min no. of days from COURT DATE to WOA LOAD DATE is set correctly to allow for adequate no. of days for printing and posting Notice of WOA.]";
//                    this.rule.ARDescr = "Rule that indicates if Notice of WOA (letter) should be generated";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6010": //BD
//                    this.rule.ARComment = "Y or N; Y (default)";
//                    this.rule.ARDescr = "Case numbers allocated automatically";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6011": //DLS
//                    this.rule.ARComment = "N=No (Default); Y=Yes;";
//                    this.rule.ARDescr = "Allow receipts after the case number has been generated (court roll)";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6030":
//                    this.rule.ARComment = "N=No (Default); Y=Yes; S=Summons Amount";
//                    this.rule.ARDescr = "Case number generate - use original fine amount";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6100": //BD
//                    this.rule.ARComment = "No. of Days (default = 14)";
//                    this.rule.ARDescr = "Rule for period (no of days) within which the court roll must be captured (payments not allowed)";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 14;
//                    break;

//                //dls 2010-12-08 move to the correct position in numeric order!
//                case "6205"://jerry 2010-11-30 for court roll -annexure for S56 for Mossel Bay
//                    this.rule.ARComment = "Y = Yes ; N = No (Default)";
//                    this.rule.ARDescr = "Print charge sheet with court roll";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6206"://jerry 2010-11-30 for court roll -annexure for S56 for Mossel Bay
//                    this.rule.ARComment = "Y = Yes ; N = No (Default)";
//                    this.rule.ARDescr = "Print charge sheet for S56";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6207"://jerry 2010-11-30 for court roll -annexure for S56 for Mossel Bay
//                    this.rule.ARComment = "Y = Yes ; N = No (Default)";
//                    this.rule.ARDescr = "Print charge sheet for S54";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6208"://jerry 2010-11-30 for court roll -annexure for S56 for Mossel Bay
//                    this.rule.ARComment = "Y = Yes ; N = No (Default)";
//                    this.rule.ARDescr = "Print charge sheet with preliminary court roll";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6300":  //Jake 2011-06-07 for woa cancellation
//                    this.rule.ARComment = "Y = Yes (Default) ; N = No";
//                    this.rule.ARDescr = "Permit cancellation of warrant of arrest";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "6305":  //Jake 2011-06-07 for woa cancellation
//                    this.rule.ARComment = "Y = Yes; N = No (Default) ";
//                    this.rule.ARDescr = "Permit WOA print file split";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;
                    
//                //dls 100112 - replaced by 5801
//                //case "7100": //BD
//                //    this.rule.ARComment = "Y = Yes; N = No (Default)";
//                //    this.rule.ARDescr = "WOA Notice (letter) must be printed";
//                //    this.rule.ARString = "N";
//                //    this.rule.ARNumeric = 0;
//                //    break;

//                //dls 100112 - replaced by 5800
//                //case "7800":
//                //    this.rule.ARComment = "Default = 2 days";
//                //    this.rule.ARDescr = "Rule that indicates how many days must elapse before generating the Notice of WOA";
//                //    this.rule.ARString = "N";
//                //    this.rule.ARNumeric = 2;
//                //    break;

//                case "8000": //DLS
//                    this.rule.ARComment = "N=No (Default); Y=Yes; If official vehicle exclusion list is active";
//                    this.rule.ARDescr = "Rule to check if official vehicle exclusion list is active";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "8010": //DLS
//                    this.rule.ARComment = "30 km/h (Default);";
//                    this.rule.ARDescr = "Rule to check official vehicle exclusion speed allowance";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 30;
//                    break;

//                case "9000": //DLS 2010-02-12 for OpusDataArchive
//                    this.rule.ARComment = "Y = Yes ; N = No (Default)";
//                    this.rule.ARDescr = "Rule to get age to delete images for offences greater than 1095 days";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 1095;
//                    break;

//                case "9010": //DLS 2010-02-12 for OpusDataArchive
//                    this.rule.ARComment = "Y = Yes ; N = No (Default)";
//                    this.rule.ARDescr = "Rule to get age to delete images for offences that expired for other reasons";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 30;
//                    break;

//                case "9020": //DLS 2010-02-12 for OpusDataArchive
//                    this.rule.ARComment = "Y = Yes ; N = No (Default)";
//                    this.rule.ARDescr = "Rule to get age to delete images for offences that were paid in full";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 30;
//                    break;

//                case "9030": //DLS 2010-02-12 for OpusDataArchive
//                    this.rule.ARComment = "Y = Yes ; N = No (Default)";
//                    this.rule.ARDescr = "Rule to get age to delete images for offences greater than 1095 days";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 1095;
//                    break;

//                case "9050"://Jake 2011-12-22 for capture hand written offences through cashier page
//                    this.rule.ARComment = "Y = Yes ; N = No (Default) If Y the system will allow the cashier to capture a minimal HWO so that payment can be received immediately. If N the system will allow a payment to be received as a suspense and reallocated later";
//                    this.rule.ARDescr = "Allow minimal capture of HWO for immediate payment";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "9060": // Nick 2012-03-14 for data washing
//                    this.rule.ARComment = "Y=Yes(Default), N=No. Data washing is being used to verify addresses.";
//                    this.rule.ARDescr = "Rule to set data washing is active.";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "9070":
//                    this.rule.ARComment = "Default is 14. Permitted values any positive integer less than 30";
//                    this.rule.ARDescr = "Rule to set data washing number of days before printing";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 14;
//                    break;

//                case "9080":
//                    this.rule.ARComment = "Default is 6 months. Permitted values any positive integer less than 12";
//                    this.rule.ARDescr = "Rule to set data washing recycle period (in months)";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 6;
//                    break;

//                case "9090":
//                    this.rule.ARComment = "Default is 12 months. Permitted values any positive integer less than 12";
//                    this.rule.ARDescr = "Rule to set data washing paid status no longer valid (in months)";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 12;
//                    break;

//                case "9100":
//                    this.rule.ARComment = "Y=Yes, N=No(Default). Proxy ID typically has a much higher activity rate";
//                    this.rule.ARDescr = "Rule to set if data washing should exclude proxies";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "9110":
//                    this.rule.ARComment = "Y=Yes, N=No(Default)";
//                    this.rule.ARDescr = "Rule to set if data washing should use data washing address for 1st Notice instead of Natis address";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "9120":
//                    this.rule.ARComment = "Y=Yes(Default), N=No";
//                    this.rule.ARDescr = "Rule to set if data washing should use the address captured at representation";
//                    this.rule.ARString = "Y";
//                    this.rule.ARNumeric = 0;
//                    break;

//                case "9130":
//                    this.rule.ARComment = "The address details supplied through a representation decision is deemed to be of a higher quality. If the last data washing date is older than “x” months use representation data. Default 2 months, permitted values any positive integer less than 12";
//                    this.rule.ARDescr = "Rule to set data washing use of representation address expiry period (in months)";
//                    this.rule.ARString = string.Empty;
//                    this.rule.ARNumeric = 2;
//                    break;

//                case "":
//                    this.rule.ARComment = "";
//                    this.rule.ARDescr = "";
//                    this.rule.ARString = "N";
//                    this.rule.ARNumeric = 0;
//                    break;
//            }


//            this.rule = GetDefaultAuthRule(this.rule);

//            KeyValuePair<int, string> value = new KeyValuePair<int, string>(this.rule.ARNumeric, this.rule.ARString);

//            return value;
        //        }
        #endregion

        public KeyValuePair<int, string> SetDefaultAuthRule()
        {
            string ruleCode = "AR_" + this.rule.ARCode;
            this.rule = GetDefaultAuthRule(this.rule);

            if (this.rule == null)
            {
                throw new Exception(string.Format(GetResourcesInClassLibraries.GetResourcesValue("AuthorityRules.aspx", "MissingDateRule"), ruleCode));
            }

            KeyValuePair<int, string> value = new KeyValuePair<int, string>(this.rule.ARNumeric, this.rule.ARString);
            return value;
        }

        public AuthorityRulesDetails GetDefaultAuthRule(AuthorityRulesDetails rule)
        {
            IDataReader rd = new WorkFlowRuleForAuthorityService().GetByWFRFANameAutIntNo("AR_" + rule.ARCode, rule.AutIntNo);
           
            if (rd.Read())
            {
                rule.ARCode = rd["WFRFAName"].ToString();
                rule.ARComment = rd["WFRFAComment"].ToString();
                rule.ARDescr = rd["WFRFADescr"].ToString();
                rule.ARNumeric = int.Parse(rd["ARNumeric"].ToString());
                rule.ARString = rd["ARString"].ToString();
                rd.Close();
            }
            else
            {
                #region Jerry 2012-06-07 change
                //WorkFlowRuleForAuthority wfEntity = new WorkFlowRuleForAuthority();
                //wfEntity = new WorkFlowRuleForAuthorityService().GetByWfrfaName("AR_" + rule.ARCode);

                //#region Jerry 2012-05-28 change
                ////if (wfEntity == null)
                ////{
                ////    wfEntity.WfrfaName = "AR_" + rule.ARCode;
                ////    wfEntity.WfrfaComment = rule.ARComment;
                ////    wfEntity.WfrfaDescr = rule.ARDescr;
                ////    wfEntity = new WorkFlowRuleForAuthorityService().Save(wfEntity);

                ////    WorkFlowRuleForAuthorityDescrLookup wfaLookup = new WorkFlowRuleForAuthorityDescrLookup();
                ////    wfaLookup.LsCode = "en-US";
                ////    wfaLookup.Wfrfaid = wfEntity.Wfrfaid;
                ////    wfaLookup.WfrfaDescr = wfEntity.WfrfaDescr;
                ////    wfaLookup = new WorkFlowRuleForAuthorityDescrLookupService().Save(wfaLookup);

                ////    WorkFlowRuleForAuthorityCommentLookup wfaCommentLookup = new WorkFlowRuleForAuthorityCommentLookup();
                ////    wfaCommentLookup.LsCode = "en-US";
                ////    wfaCommentLookup.Wfrfaid = wfEntity.Wfrfaid;
                ////    wfaCommentLookup.WfrfaComment = wfEntity.WfrfaComment;
                ////    wfaCommentLookup = new WorkFlowRuleForAuthorityCommentLookupService().Save(wfaCommentLookup);

                ////}
                //#endregion

                //if (wfEntity != null)
                //{
                //    AuthorityRule arEntity = new AuthorityRule();
                //    arEntity.ArNumeric = rule.ARNumeric;
                //    arEntity.ArString = rule.ARString;
                //    arEntity.AutIntNo = rule.AutIntNo;
                //    arEntity.Wfrfaid = wfEntity.Wfrfaid;
                //    arEntity.LastUser = rule.LastUser;
                //    arEntity = new AuthorityRuleService().Save(arEntity);
                //}
                //else
                //{
                //    return null;
                //}

                //IDataReader reader = new WorkFlowRuleForAuthorityService().GetByWFRFANameAutIntNo("AR_" + rule.ARCode, rule.AutIntNo);
                //if (reader.Read())
                //{
                //    rule.ARCode = reader["WFRFAName"].ToString();
                //    rule.ARComment = reader["WFRFAComment"].ToString();
                //    rule.ARDescr = reader["WFRFADescr"].ToString();
                //    rule.ARNumeric = int.Parse(reader["ARNumeric"].ToString());
                //    rule.ARString = reader["ARString"].ToString();                    
                //}
                //reader.Close();
                #endregion
                return null;
            }

            return rule;
 
        }
    }
}
