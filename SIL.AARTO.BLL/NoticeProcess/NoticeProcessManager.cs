﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.NoticeProcess;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.BLL.AARTONoticeClock
{
    public class NoticeProcessManager
    {
        //private bool _natisLast = false;
        //private int _crossHairStyle = 0;
        //private bool _showCrossHairs = true;
        private List<Authority> authorityList;
        public List<Authority> AuthorityList
        {
            get { return authorityList; }
        }

        public NoticeProcessManager()
        {
            authorityList = GetAllAuthority();
        }

        /// <summary>
        /// Gets the auth list for creating notices.
        /// function to get and return the list of authorities that have frames that are ready for notice creation
        /// </summary>
        /// <returns></returns>
        public bool GetAuthListForCreatingNotices(
            int autIntNo
            , int statusProsecute)
        {
            bool success = false;

            AuthorityService authService = new AuthorityService();
            IDataReader reader =
                authService.AuthByTicketProcessorFrameStatus(autIntNo,
                ConstNotice.TICKET_PROCESSOR,
                statusProsecute,
                ConstNotice.STATUS_TICKETS,
                ConstNotice.STATUS_NOTICE);

            if (reader == null)
                success = false;
            success = ((SqlDataReader)reader).HasRows;

            return success;
        }


        public List<FrameNotice> GetBatchFrameNotice(
            int autIntNo
            , ProcessingMode proMode,string lastUser)
        {
            IDataReader reader = null;
            List<FrameNotice> notices = new List<FrameNotice>();
            switch (proMode)
            {
                case ProcessingMode.Standard:
                    int statusProsecute = 0;
                    AuthorityRulesDetails ard = GetAutRuleDetailsByAutIntNoAndCode(autIntNo, "0400", lastUser);
                    bool _natisLast = false;
                    if (ard != null && ard.ARString.Equals("Y"))
                    {
                        _natisLast = true;
                    }
                    if (_natisLast)
                        statusProsecute = ConstNotice.STATUS_NOTICE_LAST;
                    else
                        statusProsecute = ConstNotice.STATUS_NOTICE_FIRST;

                    reader = new FrameService().FrameGetListByAutIntNoFrameStatus(autIntNo, statusProsecute);
                    break;
                case ProcessingMode.CA:
                case ProcessingMode.CC:
                    reader = new FrameService().FrameGetListByAutIntNoFrameStatusCAORCC(autIntNo);
                    break;
                default:
                    throw new NotImplementedException();
            }
            if (reader != null)
            {
                while (reader.Read())
                {
                    FrameNotice item = new FrameNotice();

                    item.FrameIntNo = (int)reader["FrameIntNo"];
                    item.RegNo = reader["RegNo"] as string;
                    item.FilmNo = reader["FilmNo"] as string;
                    item.FrameNo = reader["FrameNo"] as string;
                    item.OffenceLetter = reader["OffenceLetter"] as string;
                    item.OffenderType =(byte)reader["OffenderType"];
                    item.ReferenceNo = (int)reader["ReferenceNo"];

                    notices.Add(item);
                }
            }

            return notices;

        }

        public List<Authority> GetAllAuthority()
        {
            List<Authority> autList = new List<Authority>();
            foreach (Authority aut in new AuthorityService().GetAll())
            {
                autList.Add(aut);
            }
            return autList;
        }

        public AuthorityRulesDetails GetAutRuleDetailsByAutIntNoAndCode(
            int autIntNo
            , string arCode
            , string lastUser)
        {
            AuthorityRulesDetails ard = new AuthorityRulesDetails();

            //int requiredSizeInKB - needs to also be an auth rule
            ard.AutIntNo = autIntNo;
            ard.ARCode = arCode;
            ard.LastUser = lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(ard);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            return ard;

        }
        //2014-11-24 Heidi comment out because it is not be used.(bontq1660)
        //public  int? CheckAndUpdateNoticeChargeFromFrame(int autIntNo
        //    , int statusPost
        //    , int statusNoFine
        //    , int statusExpired
        //    , int statusNotice
        //    , string lastUser
        //    , ref int noOfRecords
        //    , ref string errorMsg
        //    , int statusAmountAdded
        //    , ref int noOfFrames
        //    , string natisLast)
        //{
        //    int? success = 0;
        //    int? noOfRecord = 0;
        //    try {

        //        new NoticeService().UpdateNoticeForHandWritten(autIntNo, statusNotice, natisLast, lastUser, ref success, ref noOfRecord);
        //        return success;
        //    }
        //    catch (SqlException ex){
        //        EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.BllRethrowPolicy);
        //        return null;
        //    }
        //}


        public int? AddNoticeChargeFromFrame(
            int autIntNo
            , int statusLoaded
            , int statusNoFine
            , int statusExpired
            , int statusNotice
            , string cType
            , string lastUser
            , string printFileName
            , ref int noOfRecords
            , ref string errorMsg
            , int statusAmountAdded
            , ref int noOfFrames
            , string natisLast
            , List<DuplicateNotice> duplicates)
        {
            try
            {
                if (duplicates == null)
                {
                    throw new NullReferenceException("duplicates is null");
                }
                if (natisLast == null || natisLast.Length != 1)
                {
                    throw new ArgumentOutOfRangeException("natisLast");
                }

                int? success;

                List<DuplicateNotice> duplicateNotes
                    = Process(
                        autIntNo,
                        statusLoaded,
                        statusNoFine,
                        statusExpired,
                        statusNotice,
                        statusAmountAdded,
                        printFileName,
                        cType,
                        lastUser,
                        out success,
                        out noOfRecords,
                        out noOfFrames,
                        natisLast[0]);

                foreach (var notice in duplicateNotes)
                {
                    duplicates.Add(notice);
                }

                return success;
            }
            catch (Exception ex)
            {
                //DataLayerUtil.HandleException(ex);
                EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.BllRethrowPolicy);
                return -1;
            }
        }

        public List<DuplicateNotice> Process(
            int AutIntNo,
            int StatusLoaded,
            int StatusNoFine,
            int StatusExpired,
            int StatusNotice,
            int StatusAmountAdded,
            string PrintFileName,
            string CType,
            string LastUser,
            out int? Success,
            out int NoOfRecords,
            out int NoOFFrames,
            char NatisLast)
        {
            try
            {
                List<DuplicateNotice> dups = new List<DuplicateNotice>();
                DateTime nowTime = DateTime.Now;
                Success = 0;
                NoOfRecords = 0;
                NoOFFrames = 0;

                string partFileName = PrintFileName.Substring(3);

                string SpdPrintFile = "SPD" + partFileName;
                string RLVPrintFile = "RLV" + partFileName;
                string NoAOGPrintFile = "NAG" + partFileName;

                //CheckRules(AutIntNo, NatisLast);

                DateRulesDetails details = new DateRulesDetails(AutIntNo, LastUser, "NotOffenceDate", "NotPaymentDate");

                DefaultDateRules noticePaymentDateRule = new DefaultDateRules(details);

                noticePaymentDateRule.SetDefaultDateRule();

                details = new DateRulesDetails(AutIntNo, LastUser, "NotOffenceDate", "NotIssue1stNoticeDate");
                DefaultDateRules firstNoticeDateRule = new DefaultDateRules(details);
                firstNoticeDateRule.SetDefaultDateRule();

                List<FrameNotice> notices = GetBatchFrameNotice(AutIntNo, ProcessingMode.Standard,LastUser);

                foreach (var item in notices)
                {
                    try
                    {
                        int? dupCount = GetDupCount(item.FrameIntNo, item.RegNo);

                        if (dupCount.Value > 0)
                        {
                            bool isNatisLast = NatisLast.Equals("Y");
                            UpdateFrameStatusByNatisLast(isNatisLast, LastUser, item.FrameIntNo);
                            Success = -2;
                        }
                        else
                        {
                            if (GetNoticeCount(item.FilmNo, item.FrameNo) > 0)
                            {
                                List<DuplicateNotice> NoticeList
                                    = GetNoticeList(item.FilmNo, item.FrameNo, item.FrameIntNo);
                                foreach (var dup in NoticeList)
                                {
                                    dups.Add(dup);
                                }
                                UpdateFrameStatus(LastUser, item.FrameIntNo);
                                Success = -3;
                            }
                            else
                            {
                                using (ConnectionScope.CreateTransaction())
                                {


                                    try
                                    {
                                        new NoticeService().NoticeInsertHeaderTable(
                                             AutIntNo
                                            , item.OffenderType
                                            , item.FilmNo
                                            , item.ReferenceNo
                                            , item.OffenceLetter
                                            , firstNoticeDateRule.Rule.DtRNoOfDays
                                            , LastUser
                                            , nowTime
                                            , PrintFileName
                                            , noticePaymentDateRule.Rule.DtRNoOfDays
                                            , item.FrameIntNo
                                            , StatusNotice
                                            , CType
                                            , StatusLoaded
                                            , SpdPrintFile
                                            , RLVPrintFile
                                            , NoAOGPrintFile
                                            , ref Success);

                                        ConnectionScope.Complete();
                                    }
                                    catch (Exception ex)
                                    {
                                        //EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.BllRethrowPolicy);
                                        //DataLayerUtil.HandleException(ex);
                                         throw ex;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.BllRethrowPolicy);
                        //DataLayerUtil.HandleException(ex);
                        throw ex;
                    }
                }

                List<ChargeNoticeInfo> chargeNotices
                    = GetChargeNoticeList(StatusAmountAdded, AutIntNo, StatusLoaded, NoAOGPrintFile);

                foreach (var item in chargeNotices)
                {
                    try
                    {
                        UseChargeNotice(
                            item.ChgOffenceCode
                            , AutIntNo
                            , item.SZIntNo
                            , item.VTGIntNo
                            , item.RdTIntNo
                            , item.OffenceType
                            , item.MinSpeed
                            , item.NotOffenceDate
                            , StatusNoFine
                            , LastUser
                            , item.ChgIntNo
                            , NoAOGPrintFile
                            , StatusAmountAdded
                            , SpdPrintFile
                            , RLVPrintFile);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                        //EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.BllRethrowPolicy);
                        //DataLayerUtil.HandleException(ex);
                    }
                }
                string sql = String.Format(" SPColumnName ='{0}'", "ViolationCutOff");
                int totalCount = 0;
                bool isViolationCutOff = false;
                TList<SysParam> list = new SysParamService().GetPaged(sql, "SPColumnName", 0, 0, out totalCount);
                if (list.Count == 0)
                {
                    SysParam sysParam = new SysParam();
                    sysParam.SpColumnName = "ViolationCutOff";
                    sysParam.SpIntegerValue = 0;
                    sysParam.SpStringValue = "Y";
                    sysParam.SpDescr = "Y = Yes; N = No";
                    sysParam.LastUser = LastUser;
                }
                else
                {
                    isViolationCutOff = list[0].SpStringValue.Trim().Equals("Y");

                }
                if (isViolationCutOff)
                {
                    UpdateChargeStatus_Or_Not(
                        AutIntNo
                        , StatusExpired
                        , LastUser
                        , ref Success);
                }

                int? c = GetNoticeChargeCount(AutIntNo, StatusAmountAdded);
                NoOfRecords = c.Value;
                return dups;
            }
            catch(Exception ex)
            {
                //EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.BllRethrowPolicy);
                 
                throw ex;
            }
        }

        public List<int> GetNoticeList(int autIntNo,int status)
        {
            List<int> noticeList = new List<int>();
            using (IDataReader reader = new NoticeService().NoticeList(autIntNo, status))
            {
                while (reader.Read())
                {
                    noticeList.Add(Convert.ToInt32(reader["NotIntNo"]));
                }
            }
            return noticeList;
        }

        public TicketNo GenerateNoticeNumber(int autIntNo, string lastUser)
        {
            TicketNo ticketNo = new TicketNo();
            //string ntStartPrefix = String.Empty;
            //string ntDescr = "Notice";
            //string nPrefix = String.Empty;
            //int ntIntNo = 0;
            //int npIntNo = 0;
            ticketNo.NotTicketNo = SIL.AARTO.BLL.Utility.TicketNumber.GenerateNotNo(autIntNo, lastUser);

            //TransactionType tranType = new TransactionTypeService().GetByTtIntNo(Convert.ToInt32(ntCode));
            //if (tranType != null)
            //{
                 
            //    ntDescr = tranType.TtDescription;
            //    ntStartPrefix = tranType.TtPrefix;
            //}
            //else
            //{
            //    ticketNo.NotTicketNo = "0";
            //    return ticketNo;
            //}
            //NoticePrefix noticePre = null;
            //foreach (NoticePrefix noticePrefix in new NoticePrefixService().GetByAutIntNo(autIntNo))
            //{
            //    if (noticePrefix.NtIntNo == ntIntNo)
            //    {
            //        noticePre = noticePrefix;
            //        break;
            //    }
            //}
            //if (noticePre != null)
            //{
            //    nPrefix = noticePre.Nprefix;
            //    npIntNo = noticePre.NpIntNo;
            //}
            //else
            //{
            //    noticePre = new NoticePrefix();
            //    noticePre.Nprefix = ntStartPrefix;
            //    noticePre.NtIntNo = ntIntNo;
            //    noticePre.AutIntNo = autIntNo;
            //    noticePre.LastUser = lastUser;

            //    noticePre =new NoticePrefixService().Save(noticePre);
            //    if (noticePre != null)
            //    {
            //        nPrefix = noticePre.Nprefix;
            //        npIntNo = noticePre.NpIntNo;
            //    }
            //    else
            //    {
            //        ticketNo.NotTicketNo = "0";
            //        return ticketNo;
            //    }
            //}

            return ticketNo;
        }

        public Notice GetNotice(int notIntNo)
        {
            Notice notice = new NoticeService().GetByNotIntNo(notIntNo);
            return notice;
        }

        public int? UpdateNoticeOfTicketNo(string ticketNo,string autNo,int? notIntNo,string lastUser)
        {
            new NoticeService().NoticeUpdateTicketNo(ticketNo,lastUser,ConstNotice.STATUS_TICKETS,
                ConstNotice.STATUS_AMOUNT_ADDED, ConstNotice.STATUS_NO_AOG, string.Empty, string.Empty, autNo,ref notIntNo);
            if (notIntNo != null)
            {
                if (notIntNo != 0 && notIntNo != -1)
                {
                    Notice notice = new NoticeService().GetByNotIntNo(notIntNo.Value);
                    if (notice != null)
                    {
                        notice.NotEasyPayNumber = String.Empty;
                        // 2013-07-17 add LastUser by Henry
                        notice.LastUser = lastUser;
                        notice = new NoticeService().Save(notice);
                    }
                }
            }

            return notIntNo;
                
        }

        private int? GetNoticeChargeCount(
            int AutIntNo
            , int StatusAmountAdded)
        {
            int? success = 0;
            using (IDataReader reader =
                new NoticeService().NoticeGetNotIntNoCountByAutIntNoStatusAmountAdded(AutIntNo, StatusAmountAdded))
            {
                while (reader.Read())
                {
                    success= reader.GetInt32(0);
                }
            }

            return success;
        }

        private void UpdateChargeStatus_Or_Not(
            int AutIntNo
            , int StatusExpired
            , string LastUser
            , ref int? Success)
        {
            new NoticeService().ChargeProcessStatusCA(AutIntNo, StatusExpired, LastUser, ref Success);
        }

        private int? GetNoticeCount(
            string FilmNo
            , string FrameNo)
        {
            TList<Notice> list = new NoticeService().GetByNotFilmNoNotFrameNo(FilmNo, FrameNo);

            return list.Count;
        }

        private List<ChargeNoticeInfo> GetChargeNoticeList(
            int StatusAmountAdded
            , int AutIntNo
            , int StatusLoaded
            , string NoAOGPrintFile)
        {
            List<ChargeNoticeInfo> chargeNotices = new List<ChargeNoticeInfo>();
            using (IDataReader reader = new ChargeService().ChargeNoticeUpdateAndList(StatusAmountAdded, AutIntNo, StatusLoaded, NoAOGPrintFile))
            {
                while (reader.Read())
                {
                    ChargeNoticeInfo item = new ChargeNoticeInfo();

                    item.ChgIntNo = (int)reader["ChgIntNo"];
                    item.NotIntNo = (int)reader["NotIntNo"];
                    item.NotOffenceDate = (DateTime)reader["NotOffenceDate"];
                    item.ChgOffenceCode = reader["ChgOffenceCode"] as string;
                    item.VTGIntNo = (int)reader["VTGIntNo"];
                    item.RdTIntNo = (int)reader["RdTIntNo"];
                    item.SZIntNo = (int)reader["SZIntNo"];
                    item.OffenceType = reader["NotOffenceType"] as string;
                    item.MinSpeed = Convert.ToInt32(reader["MinSpeed"]);

                    chargeNotices.Add(item);
                }
            }

            return chargeNotices;
        }
        private void UseChargeNotice(
            string ChgOffenceCode
            , int AutIntNo
            , int SzIntNo
            , int VTGIntNo
            , int RdTIntNo
            , string OffenceType
            , int MinSpeed
            , DateTime NotOffenceDate
            , int StatusNoFine
            , string LastUser
            , int ChgIntNo
            , string NoAOGPrintFile
            , int StatusAmountAdded
            , string SpdPrintFile
            , string RLVPrintFile)
        {

            new ChargeService().ChargeNoticeUpdate(ChgOffenceCode,
                  AutIntNo, SzIntNo, VTGIntNo, RdTIntNo, OffenceType, MinSpeed,
                  NotOffenceDate, StatusNoFine, LastUser, ChgIntNo,
                  NoAOGPrintFile, StatusAmountAdded, SpdPrintFile, RLVPrintFile);



        }

        private void UpdateFrameStatusByNatisLast(
            bool NatisLast
            , string LastUser
            , int FrameIntNo)
        {
            Frame frame = new FrameService().GetByFrameIntNo(FrameIntNo);
            if (frame != null)
            {
                if (NatisLast)
                {
                    frame.FrameStatus = 754;
                }
                else
                {
                    frame.FrameStatus = 154;
                    frame.FrameAdjudicateDateTime = null;
                    frame.FrameAdjudicateUser = "";
                }
                frame.FrameVerifyDateTime = null;
                frame.FrameVerifyUser = "";
                frame.LastUser = LastUser;

                frame = new FrameService().Save(frame);
            }
        }

        private int? GetDupCount(
            int FrameIntNo
            , string RegNo)
        {
            int? returnValue = null;
            using (IDataReader reader = new FrameService().FrameGetDupCount(FrameIntNo, RegNo))
            {
                while (reader.Read())
                {
                    returnValue = reader.GetInt32(0);
                }
            }

            return returnValue;

        }

        private void UpdateFrameStatus(
            string LastUser
            , int FrameIntNo)
        {
            Frame frame = new FrameService().GetByFrameIntNo(FrameIntNo);
            if (frame != null)
            {
                frame.FrameStatus = 900;
                frame.LastUser = LastUser;
                frame = new FrameService().Save(frame);
            }
        }

        private List<DuplicateNotice> GetNoticeList(
            string FilmNo
            , string FrameNo
            , int FrameIntNo)
        {
            List<DuplicateNotice> dups = new List<DuplicateNotice>();

            foreach (Notice notice in new NoticeService().GetByNotFilmNoNotFrameNo(FilmNo, FrameNo))
            {
                DuplicateNotice dup = new DuplicateNotice();
                dup.NotIntNo = notice.NotIntNo;
                //dup.NotTicketNo = dataReader["NotTicketNo"] as string;
                dup.TicketNumber = notice.NotTicketNo;
                //dup.FilmNo = item.FilmNo;
                dup.FilmNumber = FilmNo;
                dup.FrameIntNo = FrameIntNo;
                //dup.FrameNo = item.FrameNo;
                dup.FrameNumber = FrameNo;
                dups.Add(dup);
            }

            return dups;
        }

       

 

    }
}
