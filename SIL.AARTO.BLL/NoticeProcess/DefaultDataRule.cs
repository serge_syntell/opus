﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.NoticeProcess
{
    /// <summary>
    /// Represents a DateRule from the database
    /// </summary>
    public partial class DateRulesDetails
    {
        public Int32 DtRIntNo;
        public Int32 AutIntNo;
        public string DtRStartDate;
        public string DtREndDate;
        public string DtRDescr;
        public Int32 DtRNoOfDays;
        public string LastUser;

        public DateRulesDetails()
        {
        }

        public DateRulesDetails(
            int autIntNo,
            string lastUser,
            string dtRStartDate,
            string dtREndDate)
        {
            AutIntNo = autIntNo;
            LastUser = lastUser;
            DtRStartDate = dtRStartDate;
            DtREndDate = dtREndDate;
        }
    }

    public partial class DefaultDateRules
    {
        protected DateRulesDetails rule = new DateRulesDetails();
        public DateRulesDetails Rule
        {
            get
            {
                return rule;
            }
        }

        public DefaultDateRules(DateRulesDetails dateRule)
        {
            this.rule = dateRule;

        }

        #region Jerry 2012-05-28 disable
        //public int SetDefaultDateRule()
        //{
        //    if (this.rule.DtRStartDate == "CDate" && this.rule.DtREndDate == "FinalCourtRoll")
        //    {
        //        this.rule.DtRDescr = "Amount in days that the final court roll must be printed";
        //        this.rule.DtRNoOfDays = -3;
        //    }
        //    else if (this.rule.DtRStartDate == "CDate" && this.rule.DtREndDate == "LastAOGDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days that an AOG fine can be paid from court date";
        //        this.rule.DtRNoOfDays = -7;
        //    }
        //    else if (this.rule.DtRStartDate == "CDate" && this.rule.DtREndDate == "SumServeByDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days that a summons must be served by from the court date";
        //        this.rule.DtRNoOfDays = -30;
        //    }
        //    else if (this.rule.DtRStartDate == "EasyPayDate" && this.rule.DtREndDate == "NotPaymentDate")
        //    {
        //        this.rule.DtRDescr = "No. Days Before NOTICE PAYMENT DATE that the EasyPay transaction expires";
        //        this.rule.DtRNoOfDays = 2;
        //    }
        //    else if (this.rule.DtRStartDate == "NotIssue2ndNoticeDate" && this.rule.DtREndDate == "Not2ndPaymentDate")
        //    {
        //        this.rule.DtRDescr = "Min no. of days from 2nd Notice Issued to 2nd Payment Date";
        //        this.rule.DtRNoOfDays = 30;
        //    }
        //    else if (this.rule.DtRStartDate == "NotIssueSummonsDate" && this.rule.DtREndDate == "CDate")
        //    {
        //        this.rule.DtRDescr = "Min no. of days from SUMMONS ISSUE DATE to COURT DATE";
        //        this.rule.DtRNoOfDays = 30;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "CRCourtDate")
        //    {
        //        this.rule.DtRDescr = "Max. no days before COURT DATE that payment can be received.";
        //        this.rule.DtRNoOfDays = 7;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotExpireDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days before a notice expires (Default = 548 days = 18 months)";
        //        this.rule.DtRNoOfDays = 548;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotIssue1stNoticeDate")
        //    {
        //        this.rule.DtRDescr = "Max no. of days from OFFENCE DATE to FIRST NOTICE ISSUE DATE (incl. posted date)";
        //        this.rule.DtRNoOfDays = 35;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotPaymentDate")
        //    {
        //        this.rule.DtRDescr = "Max no. of days from OFFENCE DATE to PAYMENT DATE";
        //        this.rule.DtRNoOfDays = 60;
        //    }
        //   //dls 2010-07-08 - this is needed for the system overview report
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotPosted1stNoticeDate")
        //    {
        //        this.rule.DtRDescr = "Max no. of days from OFFENCE DATE to  FIRST NOTICE POST DATE";
        //        this.rule.DtRNoOfDays = 30;
        //    }
        //    else if (this.rule.DtRStartDate == "NotOffenceDate" && this.rule.DtREndDate == "NotSummonsBeforeDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days before a notice cannot be summonsed before its expiry date (Default = 548 days = 18 months)";
        //        this.rule.DtRNoOfDays = 548;
        //    }
        //    else if (this.rule.DtRStartDate == "NotPosted1stNoticeDate" && this.rule.DtREndDate == "NotIssue2ndNoticeDate")
        //    {
        //        this.rule.DtRDescr = "Max no. of days from POSTED 1ST NOTICE DATE to ISSUE 2ND NOTICE DATE";
        //        this.rule.DtRNoOfDays = 30;
        //    }
        //    else if (this.rule.DtRStartDate == "NotPosted1stNoticeDate" && this.rule.DtREndDate == "NotIssueSummonsDate")
        //    {
        //        this.rule.DtRDescr = "Min no. of days from FIRST NOTICE POST DATE to SUMMONS ISSUE DATE";
        //        this.rule.DtRNoOfDays = 90;
        //    }
        //    else if (this.rule.DtRStartDate == "SumCourtDate" && this.rule.DtREndDate == "WOALoadDate")
        //    {
        //        this.rule.DtRDescr = "Min no. of days from COURT DATE to WOA LOAD DATE";
        //        this.rule.DtRNoOfDays = 7;
        //    }
        //    else if (this.rule.DtRStartDate == "SumIssueDate" && this.rule.DtREndDate == "SumExpiredDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days to expire a summons from issue date";
        //        this.rule.DtRNoOfDays = 548;
        //    }
        //    else if (this.rule.DtRStartDate == "RoadBlockDate" && this.rule.DtREndDate == "SumCourtDate")
        //    {
        //        this.rule.DtRDescr = "Number of days between a roadblock summons and COURT DATE";
        //        this.rule.DtRNoOfDays = 10;
        //    }
        //    else if (this.rule.DtRStartDate == "WOAIssueDate" && this.rule.DtREndDate == "WOAExpireDate")
        //    {
        //        this.rule.DtRDescr = "Amount in days before a WOA expires (Default = 1825 days = 5 years)";
        //        this.rule.DtRNoOfDays = 1825;
        //    }
        //    else if (this.rule.DtRStartDate == "WOALoadDate" && this.rule.DtREndDate == "WOAPrintDate")
        //    {
        //        rule.DtRDescr = "Amount in days before a WOA print (Default = 14 days)";
        //        rule.DtRNoOfDays = 14;
        //    }
        //    else if (this.rule.DtRStartDate == "DataWashDate" && this.rule.DtREndDate == "NotIssue2ndNoticeDate")
        //    {
        //        rule.DtRDescr = "No. of days from data wash date to 2nd notice issued date";
        //        rule.DtRNoOfDays = 14;
        //    }
        //    else if (this.rule.DtRStartDate == "DataWashDate" && this.rule.DtREndDate == "NotIssueSummonDate")
        //    {
        //        rule.DtRDescr = "No. of days from data wash date to summon issued date";
        //        rule.DtRNoOfDays = 14;
        //    }
        //    else if (this.rule.DtRStartDate == "" && this.rule.DtREndDate == "")
        //    {
        //        rule.DtRDescr = "";
        //        rule.DtRNoOfDays = 0;
        //    }

        //    rule = GetDefaultDateRule(rule);

        //    return rule.DtRNoOfDays;
        //}
        #endregion

        public int SetDefaultDateRule()
        {
            string ruleCode = "DR_" + this.rule.DtRStartDate + "_" + this.rule.DtREndDate;
            rule = GetDefaultDateRule(rule);

            if (this.rule == null)
            {
                throw new Exception(string.Format(GetResourcesInClassLibraries.GetResourcesValue("DateRules.aspx", "MissingDateRule"), ruleCode));
            }

            return rule.DtRNoOfDays;
        }

        /// <summary>
        /// Gets a Date Rule for an Authority, creating a default if it does not exist
        /// </summary>
        /// <param name="rule">The rule.</param>
        /// <returns>A <see cref="T:DateRuleDetails"/></returns>
        public DateRulesDetails GetDefaultDateRule(DateRulesDetails rule)
        {
            DateRuleInfo drInfo = null;
            drInfo = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(rule.AutIntNo, rule.DtRStartDate,rule.DtREndDate );

            if (drInfo!=null)
            {
                rule.DtRNoOfDays = drInfo.ADRNoOfDays;
            }
            else
            {
                #region Jerry 2012-06-07 change
                //DateRule drEntity = new DateRule();
                //drEntity = new DateRuleService().GetByDrName("DR_" + rule.DtRStartDate + "_" + rule.DtREndDate);

                //#region Jerry 2012-05-28 change
                ////if (drEntity != null)
                ////{
                ////    drEntity.DrName = "DR_" + rule.DtRStartDate + "_" + rule.DtREndDate;
                ////    drEntity.DtRdescr = rule.DtRDescr;
                ////    drEntity = new DateRuleService().Save(drEntity);

                ////    DateRuleLookup drLookup = new DateRuleLookup();
                ////    drLookup.Drid = drEntity.Drid;
                ////    drLookup.DtRdescr = drEntity.DtRdescr;
                ////    drLookup = new DateRuleLookupService().Save(drLookup);
                ////}
                //#endregion

                //if (drEntity != null)
                //{
                //    AuthorityDateRule adrEntity = new AuthorityDateRule();
                //    adrEntity.Drid = drEntity.Drid;
                //    adrEntity.AutIntNo = rule.AutIntNo;
                //    adrEntity.AdrNoOfDays = rule.DtRNoOfDays;
                //    adrEntity.LastUser = rule.LastUser;
                //    adrEntity = new AuthorityDateRuleService().Save(adrEntity);
                //}
                //else
                //{
                //    return null;
                //}

                //drInfo = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(rule.AutIntNo, rule.DtRStartDate, rule.DtREndDate);
                //rule.DtRNoOfDays = drInfo.ADRNoOfDays;
                #endregion
                return null;
            }
            return rule;            
        }

    }
}
