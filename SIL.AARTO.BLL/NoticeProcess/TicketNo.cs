﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NoticeProcess
{
    /// <summary>
    /// Represents a ticket number
    /// </summary>

    public class TicketNo
    {
        // Fields
        private int notIntNo;
        private string notTicketNo;
        private string nPrefix;
        private string sNumber;
        private string autNumber;

        /// <summary>
        /// Gets or sets the not int no.
        /// </summary>
        /// <value>The not int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the not ticket no.
        /// </summary>
        /// <value>The not ticket no.</value>
        public string NotTicketNo
        {
            get { return this.notTicketNo; }
            set { this.notTicketNo = value; }
        }

        /// <summary>
        /// Gets or sets the N prefix.
        /// </summary>
        /// <value>The N prefix.</value>
        public string NPrefix
        {
            get { return this.nPrefix; }
            set { this.nPrefix = value; }
        }

        /// <summary>
        /// Gets or sets the S number.
        /// </summary>
        /// <value>The S number.</value>
        public string SNumber
        {
            get { return this.sNumber; }
            set { this.sNumber = value; }
        }

        /// <summary>
        /// Gets or sets the authority number.
        /// </summary>
        /// <value>The aut number.</value>
        public string AutNumber
        {
            get { return this.autNumber; }
            set { this.autNumber = value; }
        }
    }
}
