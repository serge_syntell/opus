﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NoticeProcess
{
    public class FrameNotice
    {
        public int FrameIntNo { get; set; }
        public long FrameRowVersion { get; set; }
        public int NotIntNo { get; set; }
        public DateTime NoticeIssueDate { get; set; }
        public int Speed { get; set; }
        public bool HasProxy { get; set; }
        public string PostalCode { get; set; }
        public string StreetCode { get; set; }
        public int ChgIntNo { get; set; }
        public bool IsNoAoG { get; set; }
        public decimal FineAmount { get; set; }

        public string RegNo { get; set; }
        public string FilmNo { get; set; }
        public string FrameNo { get; set; }
        public string OffenceLetter { get; set; }
        public byte OffenderType { get; set; }
        public int ReferenceNo { get; set; }
    }
}
