﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.BLL.NoticeProcess
{
    public class CourtRulesDetails
    {
        public Int32 CRIntNo;
        public Int32 CrtIntNo;
        public string CRCode;
        public string CRDescr;
        public int CRNumeric;
        public string CRString;
        public string CRComment;
        public string LastUser;

        public CourtRulesDetails()
        {
        }

        public CourtRulesDetails(int crtIntNo, string crCode)
        {
            this.CrtIntNo = crtIntNo;
            this.CRCode = crCode;
        }
    }

    public class DefaultCourtRules
    {
        protected CourtRulesDetails rule = new CourtRulesDetails();

        public CourtRulesDetails Rule
        {
            get
            {
                return this.rule;
            }
        }

        public DefaultCourtRules(CourtRulesDetails courtRule)
        {
            this.rule = courtRule;
        }

        #region Jerry 2012-05-28 disable
        //public KeyValuePair<int, string> SetDefaultCourtRule()
        //{
        //    switch (this.rule.CRCode)
        //    {
        //        case "1010":
        //            this.rule.CRComment = "Y = All origin groups combined (Default); N = Separate court rolls for camera and handwritten offences";
        //            this.rule.CRDescr = "Generate court roll for all origin groups";
        //            this.rule.CRString = "Y";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        case "1020":
        //            this.rule.CRComment = "N = Personal and impersonal service (Default); Y = Personal service only";
        //            this.rule.CRDescr = "Generate case numbers for personal service only";
        //            this.rule.CRString = "N";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        case "3010":
        //            this.rule.CRComment = "N = All origin groups use the same court date (Default); Y = Each origin group has a specific court date";
        //            this.rule.CRDescr = "Use separate court dates for each origin group";
        //            this.rule.CRString = "N";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        case "3020":
        //            this.rule.CRComment = "N = Use same court dates for HWO-S341 and HWO-S56 (Default); Y = Use separate court dates for HWO-S341 and HWO-S56";
        //            this.rule.CRDescr = "Use separate court dates for HWO-S341 and HWO-S56";
        //            this.rule.CRString = "N";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        //jerry 2012-03-22 add
        //        case "3030":
        //            this.rule.CRComment = "Does the court restrict the number of NoAOG cases the magistrate is prepared to handle each day? Default = N; Permitted values Y or N";
        //            this.rule.CRDescr = "Restrict the number of NoAOG cases per court date";
        //            this.rule.CRString = "N";
        //            this.rule.CRNumeric = 0;
        //            break;

        //        //jerry 2011-11-16 add
        //        case "5010":
        //            this.rule.CRComment = "Y = Automatic (Default); N = Manual";
        //            this.rule.CRDescr = "WOA numbers can be generated automatically (Y) or manually (N)";
        //            this.rule.CRString = "Y";
        //            this.rule.CRNumeric = 0;
        //            break;
        //    }

        //    this.rule = GetDefaultCourtRule(this.rule);

        //    KeyValuePair<int, string> value = new KeyValuePair<int, string>(this.rule.CRNumeric, this.rule.CRString);

        //    return value;
        //}
        #endregion

        public KeyValuePair<int, string> SetDefaultCourtRule()
        {
            string ruleCode = "CR_" + this.rule.CRCode;
            this.rule = GetDefaultCourtRule(this.rule);

            if (this.rule == null)
            {
                throw new Exception(string.Format(GetResourcesInClassLibraries.GetResourcesValue("CourtRules.aspx", "MissingDateRule"), ruleCode));
            }

            KeyValuePair<int, string> value = new KeyValuePair<int, string>(this.rule.CRNumeric, this.rule.CRString);
            return value;
        }

        public CourtRulesDetails GetDefaultCourtRule(CourtRulesDetails rule)
        {
            CourtRuleInfo courtRules = null;
            courtRules = new CourtRuleInfo().GetCourtRuleInfoByWFRFCNameCrtIntNo(rule.CrtIntNo, rule.CRCode);

            if (courtRules != null)
            {
                rule.CRCode = courtRules.WFRFCName;
                rule.CRComment = courtRules.WFRFCComment;
                rule.CRDescr = courtRules.WFRFCDescr;
                rule.CRNumeric = (int)courtRules.CRNumeric;
                rule.CRString = courtRules.CRString;
            }
            else
            {
                #region Jerry 2012-06-07 change
                //WorkFlowRuleForCourt wfCourt = new WorkFlowRuleForCourt();
                //wfCourt = new WorkFlowRuleForCourtService().GetByWfrfcName("CR_"+rule.CRCode);

                //#region Jerry 2012-05-28 change
                ////if (wfCourt == null)
                ////{
                ////    wfCourt.WfrfcName = "CR_" + rule.CRCode;
                ////    wfCourt.WfrfcDescr = rule.CRDescr;
                ////    wfCourt.WfrfcComment = rule.CRComment;
                ////    wfCourt = new WorkFlowRuleForCourtService().Save(wfCourt);

                ////    WorkFlowRuleForCourtDescrLookup wfDescLookup = new WorkFlowRuleForCourtDescrLookup();
                ////    wfDescLookup.LsCode = "en-US";
                ////    wfDescLookup.Wfrfcid = wfCourt.Wfrfcid;
                ////    wfDescLookup.WfrfcDescr = wfCourt.WfrfcDescr;
                ////    wfDescLookup = new WorkFlowRuleForCourtDescrLookupService().Save(wfDescLookup);

                ////    WorkFlowRuleForCourtCommentLookup wfCommentLookup = new WorkFlowRuleForCourtCommentLookup();
                ////    wfCommentLookup.LsCode = "en-US";
                ////    wfCommentLookup.Wfrfcid = wfCourt.Wfrfcid;
                ////    wfCommentLookup.WfrfcComment = wfCourt.WfrfcComment;
                ////    wfCommentLookup = new WorkFlowRuleForCourtCommentLookupService().Save(wfCommentLookup);
                ////}
                //#endregion

                //if (wfCourt != null)
                //{
                //    CourtRule crEntity = new CourtRule();
                //    crEntity.Wfrfcid = wfCourt.Wfrfcid;
                //    crEntity.CrtIntNo = rule.CrtIntNo;
                //    crEntity.CrNumeric = rule.CRNumeric;
                //    crEntity.CrString = rule.CRString;
                //    crEntity.LastUser = rule.LastUser;
                //    crEntity = new CourtRuleService().Save(crEntity);
                //}
                //else
                //{
                //    return null;
                //}

                //courtRules = new CourtRuleInfo().GetCourtRuleInfoByWFRFCNameCrtIntNo(rule.CrtIntNo, rule.CRCode);
                //rule.CRCode = courtRules.WFRFCName;
                //rule.CRComment = courtRules.WFRFCComment;
                //rule.CRDescr = courtRules.WFRFCDescr;
                //rule.CRNumeric = (int)courtRules.CRNumeric;
                //rule.CRString = courtRules.CRString;
                #endregion
                return null;
            }

            return rule;

        }
    }
}
