﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NoticeProcess
{
    public class ChargeNoticeInfo
    {
        public int ChgIntNo { get; set; }
        public int NotIntNo { get; set; }
        public DateTime NotOffenceDate { get; set; }
        public string ChgOffenceCode { get; set; }
        public int VTGIntNo { get; set; }
        public int RdTIntNo { get; set; }
        public int SZIntNo { get; set; }
        public string OffenceType { get; set; }
        public int MinSpeed { get; set; }
    }
}
