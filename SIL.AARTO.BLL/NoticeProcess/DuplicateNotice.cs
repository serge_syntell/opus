﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.NoticeProcess
{
    /// <summary>
    /// Represents duplicate notice information
    /// </summary>
    public class DuplicateNotice
    {
        // Fields
        private string filmNo = string.Empty;
        private string frameNo = string.Empty;
        private int frameIntNo = 0;
        private int notIntNo = 0;
        private string ticketNo = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="DuplicateNotice"/> class.
        /// </summary>
        public DuplicateNotice()
        {
        }

        /// <summary>
        /// Gets or sets the frame int no.
        /// </summary>
        /// <value>The frame int no.</value>
        public int FrameIntNo
        {
            get { return this.frameIntNo; }
            set { this.frameIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the frame number.
        /// </summary>
        /// <value>The frame number.</value>
        public string FrameNumber
        {
            get { return this.frameNo; }
            set { this.frameNo = value; }
        }

        /// <summary>
        /// Gets or sets the film number.
        /// </summary>
        /// <value>The film number.</value>
        public string FilmNumber
        {
            get { return this.filmNo; }
            set { this.filmNo = value; }
        }

        /// <summary>
        /// Gets or sets the not int no.
        /// </summary>
        /// <value>The not int no.</value>
        public int NotIntNo
        {
            get { return this.notIntNo; }
            set { this.notIntNo = value; }
        }

        /// <summary>
        /// Gets or sets the ticket number.
        /// </summary>
        /// <value>The ticket number.</value>
        public string TicketNumber
        {
            get { return this.ticketNo; }
            set { this.ticketNo = value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Film: {0} - Frame: {1} ({2}) [ Notice: '{3}' ({4}) ]", this.filmNo, this.frameNo, this.frameIntNo, this.ticketNo, this.notIntNo);
        }

    }
}
