﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Transactions;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.BLL.WOAManagement.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.WOAManagement;
using SIL.ServiceBase;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.WOAManagement
{
    public class WOAReductionManager
    {
        readonly CourtMagistrateService cmService = new CourtMagistrateService();
        readonly PunchStatistics punchStatistics;
        readonly ServiceDB serviceDB;

        public WOAReductionManager(string connStr)
        {
            this.serviceDB = new ServiceDB(connStr);
            this.punchStatistics = new PunchStatistics(connStr);
        }

        public void WOAReductionSearch(ref WOAReductionModel model, int woaIntNo)
        {
            if (model == null) model = new WOAReductionModel();

            //if (string.IsNullOrWhiteSpace(woaNumber))
            //{
            //    model.Status = false;
            //    model.AddError(WOAReduction.PleaseEnterValidWOANumber);
            //    return;
            //}

            var m = model;
            var paras = new[]
            {
                new SqlParameter("@WOAIntNo", woaIntNo)
            };
            this.serviceDB.ExecuteReader("WOAReductionSearch", paras, dr =>
            {
                if (dr.Read())
                {
                    m.AutIntNo = Helper.GetReaderValue<int>(dr, "AutIntNo");
                    m.WOAIntNo = Helper.GetReaderValue<int>(dr, "WOAIntNo");
                    m.WOANumber = Helper.GetReaderValue<string>(dr, "WOANumber");
                    m.FullName = Helper.GetReaderValue<string>(dr, "AccFullName");
                    m.IdNumber = Helper.GetReaderValue<string>(dr, "AccIDNumber");
                    m.CourtDate = Helper.GetReaderValue<DateTime?>(dr, "SumCourtDate");
                    m.WOAIssueDate = Helper.GetReaderValue<DateTime?>(dr, "WOAIssueDate");
                    m.WOAStatus = Helper.GetReaderValue<string>(dr, "CSDescr");
                    m.WOAServedStatus = Helper.GetReaderValue<string>(dr, "WSSDescr");
                    m.TotalFineAmountOld = m.TotalFineAmountNew = Helper.GetReaderValue<decimal>(dr, "TotalSchFineAmount");
                    m.TotalContemptAmountOld = m.TotalContemptAmountNew = Helper.GetReaderValue<decimal>(dr, "TotalSchContemptAmount");
                    m.CrtIntNo = Helper.GetReaderValue<int>(dr, "CrtIntNo");
                    m.WOARowVersion = Helper.GetReaderValue<long>(dr, "WOARowVersion");
                    m.ChgRowVersion = Helper.GetReaderValue<long>(dr, "ChgRowVersion");
                    m.SchRowVersion = Helper.GetReaderValue<long>(dr, "SchRowVersion");
                }
                else
                {
                    m.Status = false;
                    m.AddError(WOAReduction.ThisWOADoesNotExistOrHasBeenCancelled);
                }
            });

            if (!model.Status)
                return;

            FillCourtMagistrate(ref model);
        }

        void FillCourtMagistrate(ref WOAReductionModel model)
        {
            if (model == null) model = new WOAReductionModel();
            if (model.CrtIntNo <= 0) return;

            var m = model;
            this.cmService.GetByCrtIntNo(model.CrtIntNo).ForEach(cm => m.OfficiaList.Add(new SelectListItem
            {
                Text = cm.MagistrateName,
                Value = cm.CoMaIntNo.ToString(CultureInfo.InvariantCulture)
            }));
        }

        public void WOAReductionUpdate(ref WOAReductionModel model, string lastUser)
        {
            if (model == null) model = new WOAReductionModel();

            ValidateWOAReduction(ref model);
            if (!model.Status) return;

            var paras = new[]
            {
                new SqlParameter("@WOAIntNo", model.WOAIntNo),
                new SqlParameter("@WOAROriginalFineAmount", model.TotalFineAmountOld) { SqlDbType = SqlDbType.Money },
                new SqlParameter("@WOARNewFineAmount", model.TotalFineAmountNew) { SqlDbType = SqlDbType.Money },
                new SqlParameter("@WOAROriginalContemptAmount", model.TotalContemptAmountOld) { SqlDbType = SqlDbType.Money },
                new SqlParameter("@WOARNewContemptAmount", model.TotalContemptAmountNew) { SqlDbType = SqlDbType.Money },
                new SqlParameter("@WOARowVersion", model.WOARowVersion),
                new SqlParameter("@ChgRowVersion", model.ChgRowVersion),
                new SqlParameter("@SchRowVersion", model.SchRowVersion),
                new SqlParameter("@CoMaIntNo", model.CoMaIntNo),
                new SqlParameter("@LastUser", lastUser)
            };
            using (var scope = ServiceUtility.CreateTransactionScope())
            {
                var woaIntNo = Convert.ToInt32(this.serviceDB.ExecuteScalar("WOAReductionUpdate", paras));
                if (woaIntNo < 0)
                {
                    model.Status = false;
                    if (woaIntNo == -10)
                        model.AddError(string.Format(WOAReduction.OperationFailed, WOAReduction.PleaseRefresh, woaIntNo));
                    else if (woaIntNo.In(-2, -3))
                        model.AddError(string.Format(WOAReduction.OperationFailed, WOAReduction.MinusAmount, woaIntNo));
                    else
                        model.AddError(string.Format(WOAReduction.OperationFailed, "", woaIntNo));
                    return;
                }

                if (this.punchStatistics.PunchStatisticsTransactionAdd(model.AutIntNo, lastUser, PunchStatisticsTranTypeList.WOAReduction, PunchAction.Add) <= 0)
                {
                    model.Status = false;
                    model.AddError(this.punchStatistics.ErrorMessage);
                    return;
                }

                if (ServiceUtility.TransactionCanCommit())
                    scope.Complete();
                else
                {
                    model.Status = false;
                    model.AddError(new TransactionAbortedException().Message);
                    return;
                }
            }

            model.AddMessage(WOAReduction.OperationSuccessful);
        }

        void ValidateWOAReduction(ref WOAReductionModel model)
        {
            if (model == null) model = new WOAReductionModel();

            if (model.TotalFineAmountOld < 0)
                model.TotalFineAmountOld = 0;
            if (model.TotalContemptAmountOld < 0)
                model.TotalContemptAmountOld = 0;

            if (model.TotalFineAmountNew < 0
                || model.TotalContemptAmountNew < 0)
            {
                model.Status = false;
                model.AddError(WOAReduction.PleaseEnterValidWOANumber);
            }

            if (model.TotalFineAmountOld == model.TotalFineAmountNew
                && model.TotalContemptAmountOld == model.TotalContemptAmountNew)
            {
                model.Status = false;
                model.AddError(WOAReduction.YouHaveNotChangedAnyAmount);
            }

            if (model.TotalFineAmountNew + model.TotalContemptAmountNew <= 0)
            {
                model.Status = false;
                model.AddError(WOAReduction.TheTotalOfFineAmountAndContemptAmountMustBeGreaterThanZero);
            }

            if (model.TotalFineAmountNew < 0
                || model.TotalContemptAmountNew < 0)
            {
                model.Status = false;
                model.AddError(WOAReduction.TheAmountCanNotBeLessThan0);
            }

            if (model.CoMaIntNo <= 0)
            {
                model.Status = false;
                model.AddError(WOAReduction.YouMustSelectAnOfficial);
            }
        }

        public Stream CreateReport(ref WOAReductionReportModel model)
        {
            if (model == null) model = new WOAReductionReportModel();

            ValidateWOAReductionReport(ref model);
            if (!model.Status || string.IsNullOrWhiteSpace(model.DKey)) return null;

            var data = new WOAReductionReportData
            {
                DateFrom = model.DateFrom.Value,
                DateTo = model.DateTo.Value
            };

            var paras = new[]
            {
                new SqlParameter("@AutIntNo", model.AutIntNo),
                new SqlParameter("@CrtIntNo", model.CrtIntNo),
                new SqlParameter("@DateFrom", model.DateFrom),
                new SqlParameter("@DateTo", model.DateTo)
            };
            this.serviceDB.ExecuteReader("WOAReductionReport", paras, dr =>
            {
                while (dr.Read())
                {
                    data.Rows.Add(new WOAReductionReportRow
                    {
                        AuthName = Helper.GetReaderValue<string>(dr, "AutName"),
                        CourtName = Helper.GetReaderValue<string>(dr, "CrtName"),
                        CaptureDate = Helper.GetReaderValue<string>(dr, "WOARCaptureDate"),
                        WOANumber = Helper.GetReaderValue<string>(dr, "WOANumber"),
                        OriginalFineAmount = Helper.GetReaderValue<decimal>(dr, "WOAROriginalFineAmount"),
                        NewFineAmount = Helper.GetReaderValue<decimal>(dr, "WOARNewFineAmount"),
                        OriginalContemptAmount = Helper.GetReaderValue<decimal>(dr, "WOAROriginalContemptAmount"),
                        NewContemptAmount = Helper.GetReaderValue<decimal>(dr, "WOARNewContemptAmount"),
                        Magistrate = Helper.GetReaderValue<string>(dr, "MagistrateName")
                    });
                }
            });

            if (data.Rows.IsNullOrEmpty())
            {
                model.Status = false;
                model.AddError(WOAReduction.ThereIsNoDataForGeneratingTheReport);
                return null;
            }

            var report = new WOAReductionReport(data);
            return report.Generate();
        }

        void ValidateWOAReductionReport(ref WOAReductionReportModel model)
        {
            if (model == null) model = new WOAReductionReportModel();

            if (model.AutIntNo <= 0)
            {
                model.Status = false;
                model.AddError(WOAReduction.PleaseSelectAnAuthority);
            }

            if (!model.DateFrom.HasValue
                || !model.DateTo.HasValue
                || model.DateFrom.Value > model.DateTo.Value)
            {
                model.Status = false;
                model.AddError(WOAReduction.PleaseEnterAValidDateRange);
            }
        }
    }
}
