﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.WOAManagement;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.WOAManagement
{
    public class WOASelfPlacementSnapshotManager
    {
        #region DB

        readonly Lazy2<ChargeService> chargeService = new Lazy2<ChargeService>(() => new ChargeService());
        readonly Lazy2<EvidencePackService> epService = new Lazy2<EvidencePackService>(() => new EvidencePackService());
        readonly Lazy2<NoticeService> noticeService = new Lazy2<NoticeService>(() => new NoticeService());
        readonly Lazy2<NoticeSummonsService> nsService = new Lazy2<NoticeSummonsService>(() => new NoticeSummonsService());
        readonly Lazy2<SumChargeService> scService = new Lazy2<SumChargeService>(() => new SumChargeService());
        readonly Lazy2<ServiceDB> serviceDB;
        readonly Lazy2<SummonsService> summonsService = new Lazy2<SummonsService>(() => new SummonsService());
        readonly Lazy2<WoaService> woaService = new Lazy2<WoaService>(() => new WoaService());
        readonly Lazy2<WoaSelfPlacementSnapshotService> wspsService = new Lazy2<WoaSelfPlacementSnapshotService>(() => new WoaSelfPlacementSnapshotService());

        #endregion

        public WOASelfPlacementSnapshotManager(string connStr)
        {
            this.serviceDB = new Lazy2<ServiceDB>(() => new ServiceDB(connStr));
        }

        public void Create(int woaIntNo, DateTime courtDate, string lastUser)
        {
            var wspsEntity = this.wspsService.Value.GetByWoaIntNo(woaIntNo) ?? new WoaSelfPlacementSnapshot
            {
                WoaIntNo = woaIntNo
            };
            wspsEntity.WspsDateCreated = DateTime.Now;
            wspsEntity.WspsDateReversed = null;
            wspsEntity.LastUser = lastUser;
            wspsEntity.WspsData = GenerateSnapshotXml(GenerateSnapshotData(woaIntNo, courtDate));
            this.wspsService.Value.Save(wspsEntity);
        }

        public void Reverse(int woaIntNo, string lastUser)
        {
            var wspsEntity = this.wspsService.Value.GetByWoaIntNoWspsDateReversed(woaIntNo, null);
            if (wspsEntity == null)
                throw new LogicErrorException(WOASelfPlacement.ThereIsNoAvailableSnapshotForThisWOA);

            ReverseData(DeserializeSnapshot(wspsEntity.WspsData), wspsEntity, lastUser);
        }

        public bool HasSnapshot(int woaIntNo)
        {
            return this.wspsService.Value.GetByWoaIntNoWspsDateReversed(woaIntNo, null) != null;
        }

        DataSet GenerateSnapshotData(int woaIntNo, DateTime courtDate)
        {
            var ds = this.serviceDB.Value.ExecuteDataSet("GenerateWOASelfPlacementSnapshot", new[]
            {
                new SqlParameter("@WOAIntNo", woaIntNo),
                new SqlParameter("@WOANewCourtDate", courtDate)
            });
            if (ds.Tables.Count != 5)
                throw new LogicErrorException(WOASelfPlacement.UnableToCreateTheSnapshotPleaseCheckAndRefreshYourData);

            ds.DataSetName = "WOASelfPlacementSnapshot";

            var tableNames = new[] { "WOA", "Notice", "Charge", "Summons", "SumCharge" };
            for (var i = 0; i < ds.Tables.Count; i++)
            {
                var table = ds.Tables[i];
                if (table.Rows.Count <= 0)
                    throw new LogicErrorException(WOASelfPlacement.UnableToCreateTheSnapshotPleaseCheckAndRefreshYourData);
                table.TableName = tableNames[i];
            }
            return ds;
        }

        string GenerateSnapshotXml(DataSet source)
        {
            using (source)
            {
                using (var ms = new MemoryStream())
                {
                    source.WriteXml(ms);
                    ms.Position = 0;
                    using (var sr = new StreamReader(ms))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
        }

        WOASelfPlacementSnapshot DeserializeSnapshot(string snapshot)
        {
            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms))
                {
                    sw.Write(snapshot);
                    sw.Flush();
                    ms.Position = 0;
                    return (WOASelfPlacementSnapshot)new XmlSerializer(typeof(WOASelfPlacementSnapshot)).Deserialize(ms);
                }
            }
        }

        void ReverseData(WOASelfPlacementSnapshot snapshot, WoaSelfPlacementSnapshot wspsEntity, string lastUser)
        {
            Woa woa = null;
            Summons summons = null;
            NoticeSummons noticeSummons;
            Notice notice;
            TList<Charge> charges = null;
            TList<SumCharge> sumCharges = null;

            var valid = true;
            // WOA
            if (snapshot.WOAEntity == null
                || (woa = this.woaService.Value.GetByWoaIntNo(snapshot.WOAEntity.WOAIntNo)) == null
                || woa.WoaNewCourtDate.GetValueOrDefault().Date != snapshot.WOAEntity.NewWOANewCourtDate.GetValueOrDefault().Date)
                valid = false;
                // Summons
            else if (snapshot.SummonsEntity == null
                || (summons = this.summonsService.Value.GetBySumIntNo(snapshot.SummonsEntity.SumIntNo)) == null
                || summons.SummonsStatus != snapshot.SummonsEntity.SummonsStatus
                || summons.SumCourtDate.GetValueOrDefault().Date != snapshot.SummonsEntity.NewSumCourtDate.GetValueOrDefault().Date)
                valid = false;
                // Notice_Summons
            else if ((noticeSummons = this.nsService.Value.GetBySumIntNo(snapshot.SummonsEntity.SumIntNo).FirstOrDefault()) == null)
                valid = false;
                // Notice
            else if (snapshot.NoticeEntity == null
                || noticeSummons.NotIntNo != snapshot.NoticeEntity.NotIntNo
                || (notice = this.noticeService.Value.GetByNotIntNo(snapshot.NoticeEntity.NotIntNo)) == null
                || notice.NoticeStatus != snapshot.NoticeEntity.NoticeStatus)
                valid = false;
                // Charge
            else if (snapshot.Charges == null || snapshot.Charges.Count <= 0
                || (charges = this.chargeService.Value.GetByNotIntNo(snapshot.NoticeEntity.NotIntNo)).Count <= 0
                || snapshot.Charges.Any(c =>
                    charges.Any(c2 => c2.ChgIntNo == c.ChgIntNo && c2.ChargeStatus != c.ChargeStatus)
                        || charges.All(c2 => c2.ChgIntNo != c.ChgIntNo)))
                valid = false;
                // SumCharge
            else if (snapshot.SumCharges == null || snapshot.SumCharges.Count <= 0
                || (sumCharges = this.scService.Value.GetBySumIntNo(snapshot.SummonsEntity.SumIntNo)).Count <= 0
                || snapshot.SumCharges.Any(sc =>
                    sumCharges.Any(sc2 => sc2.SchIntNo == sc.SChIntNo && (sc2.SumChargeStatus != sc.SumChargeStatus || sc2.CjtIntNo != sc.NewCJTIntNo))
                        || sumCharges.All(sc2 => sc2.SchIntNo != sc.SChIntNo)))
                valid = false;

            if (!valid)
                throw new LogicErrorException(WOASelfPlacement.UnableToReverseDataFromTheSnapshotBecauseTheSnapshotHasExpired);

            using (var scope = ServiceUtility.CreateTransactionScope())
            {
                #region WOA

                woa.WoaNewCourtDate = snapshot.WOAEntity.WOANewCourtDate;
                woa.LastUser = lastUser;
                this.woaService.Value.Save(woa);

                #endregion

                #region Summons

                summons.SumCourtDate = snapshot.SummonsEntity.SumCourtDate;
                summons.LastUser = lastUser;
                this.summonsService.Value.Save(summons);

                #endregion

                #region SumCharge

                sumCharges.ForEach(sc =>
                {
                    sc.CjtIntNo = snapshot.SumCharges.First(sc2 => sc2.SChIntNo == sc.SchIntNo).CJTIntNo;
                    sc.LastUser = lastUser;
                });
                this.scService.Value.Save(sumCharges);

                #endregion

                #region WOASelfPlacement Snapshot

                wspsEntity.WspsDateReversed = DateTime.Now;
                wspsEntity.LastUser = lastUser;
                this.wspsService.Value.Save(wspsEntity);

                #endregion

                #region EvidencePack

                var now = DateTime.Now;
                var ep = new EvidencePack
                {
                    ChgIntNo = charges.OrderBy(c => c.ChgSequence).First(c => c.ChgIsMain).ChgIntNo,
                    EpItemDate = now,
                    EpItemDescr = string.Format(WOASelfPlacement.WOASelfPlacementReversedBy, lastUser, now.ToString("yyyy-MM-dd")),
                    EpSourceId = woa.WoaIntNo,
                    EpSourceTable = "WOA",
                    EpTransactionDate = now,
                    LastUser = lastUser
                };
                this.epService.Value.Save(ep);

                #endregion

                scope.Complete();
            }
        }
    }

    [Serializable]
    public class WOASelfPlacementSnapshot
    {
        [XmlElement("WOA")]
        public WOA WOAEntity { get; set; }

        [XmlElement("Notice")]
        public Notice NoticeEntity { get; set; }

        [XmlElement("Charge")]
        public List<Charge> Charges { get; set; }

        [XmlElement("Summons")]
        public Summons SummonsEntity { get; set; }

        [XmlElement("SumCharge")]
        public List<SumCharge> SumCharges { get; set; }

        [Serializable]
        public class Charge
        {
            public int ChgIntNo { get; set; }
            public int ChargeStatus { get; set; }
        }
        [Serializable]
        public class Notice
        {
            public int NotIntNo { get; set; }
            public int NoticeStatus { get; set; }
        }
        [Serializable]
        public class SumCharge
        {
            public int SChIntNo { get; set; }
            public int SumChargeStatus { get; set; }
            public int CJTIntNo { get; set; }
            public int NewCJTIntNo { get; set; }
        }
        [Serializable]
        public class Summons
        {
            public int SumIntNo { get; set; }
            public int SummonsStatus { get; set; }
            public DateTime? SumCourtDate { get; set; }
            public DateTime? NewSumCourtDate { get; set; }
        }
        [Serializable]
        public class WOA
        {
            public int WOAIntNo { get; set; }
            public DateTime? WOANewCourtDate { get; set; }
            public DateTime? NewWOANewCourtDate { get; set; }
        }
    }

    public class LogicErrorException : Exception
    {
        public LogicErrorException(string message) : base(message) {}
    }
}
