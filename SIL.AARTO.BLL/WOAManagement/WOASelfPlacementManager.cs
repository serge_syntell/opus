﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Transactions;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.BLL.WOAManagement.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.WOAManagement;
using SIL.ServiceBase;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.WOAManagement
{
    public class WOASelfPlacementManager
    {
        readonly Lazy2<DateRuleInfo> drService = new Lazy2<DateRuleInfo>(() => new DateRuleInfo());
        readonly Lazy2<PunchStatistics> punchStatistics;
        readonly Lazy2<ServiceDB> serviceDB;
        readonly Lazy2<WOASelfPlacementSnapshotManager> snapshotManager;
        readonly Lazy2<WoaService> woaService = new Lazy2<WoaService>(() => new WoaService());
        DateTime now = DateTime.Now;

        public WOASelfPlacementManager(string connStr)
        {
            this.serviceDB = new Lazy2<ServiceDB>(() => new ServiceDB(connStr));
            this.punchStatistics = new Lazy2<PunchStatistics>(() => new PunchStatistics(connStr));
            this.snapshotManager = new Lazy2<WOASelfPlacementSnapshotManager>(() => new WOASelfPlacementSnapshotManager(connStr));
        }

        public void WOASelfPlacementSearch(ref WOASelfPlacementModel model, int woaIntNo)
        {
            if (model == null) model = new WOASelfPlacementModel();

            //if (string.IsNullOrWhiteSpace(woaNumber))
            //{
            //    model.Status = false;
            //    model.AddError(WOASelfPlacement.PleaseEnterValidWOANumber);
            //    return;
            //}

            var m = model;
            var paras = new[]
            {
                new SqlParameter("@WOAIntNo", woaIntNo)
            };
            this.serviceDB.Value.ExecuteReader("WOASelfPlacementSearch", paras, dr =>
            {
                if (dr.Read())
                {
                    m.AutIntNo = Helper.GetReaderValue<int>(dr, "AutIntNo");
                    m.WOAIntNo = Helper.GetReaderValue<int>(dr, "WOAIntNo");
                    m.WOANumber = Helper.GetReaderValue<string>(dr, "WOANumber");
                    m.FullName = Helper.GetReaderValue<string>(dr, "AccFullName");
                    m.IdNumber = Helper.GetReaderValue<string>(dr, "AccIDNumber");
                    m.WOANewCourtDate = m.OldWOACourtDate = Helper.GetReaderValue<DateTime?>(dr, "WOANewCourtDate");
                    m.CourtDate = Helper.GetReaderValue<DateTime?>(dr, "SumCourtDate");
                    m.WOAIssueDate = Helper.GetReaderValue<DateTime?>(dr, "WOAIssueDate");
                    m.WOAStatus = Helper.GetReaderValue<string>(dr, "CSDescr");
                    m.WOAServedStatus = Helper.GetReaderValue<string>(dr, "WSSDescr");
                    m.WOAFineAmount = Helper.GetReaderValue<decimal>(dr, "WOAFineAmount");
                    m.WOAAddAmount = Helper.GetReaderValue<decimal>(dr, "WOAAddAmount");
                    //m.CrtRIntNo = Helper.GetReaderValue<int>(dr, "CrtRIntNo");
                    m.WOARowVersion = Helper.GetReaderValue<long>(dr, "WOARowVersion");
                    m.OGIntNo = (int)OriginGroupList.ALL; // confirmed with Adam
                }
                else
                {
                    m.Status = false;
                    m.AddError(WOASelfPlacement.ThisWOADoesNotExistOrHasBeenCancelled);
                }
            });

            if (!model.Status)
                return;

            model.HasSnapshot = this.snapshotManager.Value.HasSnapshot(model.WOAIntNo);

            DateRuleInfo rule;
            var days = (rule = this.drService.Value.GetDateRuleInfoByDRNameAutIntNo(model.AutIntNo, "SumCourtDate", "WOAExpireDate")) != null
                ? rule.ADRNoOfDays : 0;
            model.WOAExpiredDate = model.CourtDate.GetValueOrDefault(this.now).AddDays(days);
            if (model.WOAExpiredDate < this.now.AddDays(-30))
                model.AddMessage(string.Format(WOASelfPlacement.WOAIsExpiredUnableToSetNewCourtDate, model.WOAExpiredDate.ToString("yyyy-MM-dd")));

            model.WOACourtDates = GetAvailableWOACourtDates(this.now.AddDays(-30), this.now.AddDays(30));
        }

        List<string> GetAvailableWOACourtDates(DateTime dateFrom, DateTime dateTo)
        {
            var dates = new List<string>();

            //if (autIntNo <= 0
            //    || crtRIntNo <= 0
            //    || ogIntNo <= 0)
            //    return dates;

            var paras = new[]
            {
                //new SqlParameter("@AutIntNo", autIntNo),
                //new SqlParameter("@CrtRIntNo", crtRIntNo),
                //new SqlParameter("@OGIntNo", ogIntNo),
                new SqlParameter("@DateFrom", dateFrom),
                new SqlParameter("@DateTo", dateTo)
            };
            this.serviceDB.Value.ExecuteReader("GetWorkDays", paras, dr =>
            {
                while (dr.Read())
                    dates.Add(Helper.GetReaderValue<string>(dr, "Date"));
            });
            return dates;
        }

        public void WOASelfPlacementUpdate(ref WOASelfPlacementModel model, string lastUser)
        {
            if (model == null) model = new WOASelfPlacementModel();

            ValidateWOASelfPlacement(ref model);
            if (!model.Status) return;

            var paras = new[]
            {
                new SqlParameter("@WOAIntNo", model.WOAIntNo),
                new SqlParameter("@WOANewCourtDate", model.WOANewCourtDate),
                new SqlParameter("@OGIntNo", model.OGIntNo),
                new SqlParameter("@WOARowVersion", model.WOARowVersion),
                new SqlParameter("@LastUser", lastUser)
            };
            using (var scope = ServiceUtility.CreateTransactionScope())
            {
                try
                {
                    this.snapshotManager.Value.Create(model.WOAIntNo, model.WOANewCourtDate.Value, lastUser);
                }
                catch (LogicErrorException ex)
                {
                    model.Status = false;
                    model.AddError(string.Format(WOASelfPlacement.OperationFailed, ex.Message, 0));
                    return;
                }

                var woaIntNo = Convert.ToInt32(this.serviceDB.Value.ExecuteScalar("WOASelfPlacementUpdate", paras));
                if (woaIntNo < 0)
                {
                    model.Status = false;
                    if (woaIntNo == -1)
                        model.AddError(string.Format(WOASelfPlacement.OperationFailed, WOASelfPlacement.TheNewWOACourtDateIsInvalid, woaIntNo));
                    else if (woaIntNo == -2)
                        model.AddError(string.Format(WOASelfPlacement.OperationFailed, WOASelfPlacement.PleaseRefresh, woaIntNo));
                    else
                        model.AddError(string.Format(WOASelfPlacement.OperationFailed, "", woaIntNo));
                    return;
                }

                if (this.punchStatistics.Value.PunchStatisticsTransactionAdd(model.AutIntNo, lastUser, PunchStatisticsTranTypeList.WOASelfPlacement, PunchAction.Add) <= 0)
                {
                    model.Status = false;
                    model.AddError(this.punchStatistics.Value.ErrorMessage);
                    return;
                }

                if (ServiceUtility.TransactionCanCommit())
                    scope.Complete();
                else
                {
                    model.Status = false;
                    model.AddError(new TransactionAbortedException().Message);
                    return;
                }
            }

            model.AddMessage(WOASelfPlacement.OperationSuccessful);
        }

        void ValidateWOASelfPlacement(ref WOASelfPlacementModel model)
        {
            if (model == null) model = new WOASelfPlacementModel();

            if (this.snapshotManager.Value.HasSnapshot(model.WOAIntNo))
            {
                model.Status = false;
                model.AddError(WOASelfPlacement.ReverseFirst);
                return;
            }

            if (!model.WOANewCourtDateStr.In(GetAvailableWOACourtDates(this.now.AddDays(-30), this.now.AddDays(30)).ToArray())
                || string.Compare(model.WOANewCourtDateStr, model.WOAExpiredDate.ToString("yyyy-MM-dd"), CultureInfo.InvariantCulture, CompareOptions.IgnoreCase) > 0)
            {
                model.Status = false;
                model.AddError(WOASelfPlacement.TheNewWOACourtDateIsInvalid);
            }

            if (model.OldWOACourtDateStr == model.WOANewCourtDateStr)
            {
                model.Status = false;
                model.AddError(WOASelfPlacement.YouHaveNotChangedCourtDate);
            }
        }

        public void WOASelfPlacementReverse(ref WOASelfPlacementModel model, string lastUser)
        {
            if (model == null) model = new WOASelfPlacementModel();

            ValidateWOASelfPlacementReverse(ref model);
            if (!model.Status) return;

            try
            {
                this.snapshotManager.Value.Reverse(model.WOAIntNo, lastUser);
            }
            catch (LogicErrorException ex)
            {
                model.Status = false;
                model.AddError(string.Format(WOASelfPlacement.OperationFailed, ex.Message, 0));
                return;
            }

            model.AddMessage(WOASelfPlacement.OperationSuccessful);
        }

        void ValidateWOASelfPlacementReverse(ref WOASelfPlacementModel model)
        {
            if (model == null) model = new WOASelfPlacementModel();

            var woa = this.woaService.Value.GetByWoaIntNo(model.WOAIntNo);
            if (woa == null
                || woa.RowVersionLong() != model.WOARowVersion
                || model.WOARowVersion == 0)
            {
                model.Status = false;
                model.AddError(WOASelfPlacement.PleaseRefresh);
            }
        }
    }
}
