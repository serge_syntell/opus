﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace SIL.AARTO.BLL.WOAManagement
{
    public class WOAReductionReport
    {
        const int columnCount = 8;
        readonly HSSFWorkbook book = new HSSFWorkbook();
        readonly WOAReductionReportData data;
        int dataCount, rowCount, rowIndex;
        ISheet sheet;

        #region CellStyles

        ICellStyle styleBase;
        ICellStyle styleRow1, styleRow2, styleRow3;
        ICellStyle styleSubTitle1, styleSubTitle2;
        ICellStyle styleTitle;

        #endregion

        public WOAReductionReport(WOAReductionReportData data)
        {
            this.data = data;
        }

        public Stream Generate()
        {
            this.sheet = this.book.CreateSheet("WOAReduction");
            this.dataCount = this.data.Rows.Count;
            this.rowCount = 4 + this.dataCount;
            this.rowIndex = 0;

            BuildStyle();
            CreateArea();
            AddTitle();
            AddSubTitle();
            this.rowIndex++;

            AddHeader();
            FillData();
            AutoSize();

            var ms = new MemoryStream();
            this.book.Write(ms);
            ms.Position = 0;
            return ms;
        }

        void BuildStyle()
        {
            this.styleBase = this.book.CreateCellStyle();

            this.styleTitle = this.book.CreateCellStyle();
            this.styleTitle.Alignment = HorizontalAlignment.CENTER;
            this.styleTitle.VerticalAlignment = VerticalAlignment.CENTER;

            this.styleSubTitle1 = this.book.CreateCellStyle();
            this.styleSubTitle1.Alignment = HorizontalAlignment.LEFT;
            this.styleSubTitle1.VerticalAlignment = VerticalAlignment.CENTER;

            this.styleSubTitle2 = this.book.CreateCellStyle();
            this.styleSubTitle2.Alignment = HorizontalAlignment.RIGHT;
            this.styleSubTitle2.VerticalAlignment = VerticalAlignment.CENTER;

            this.styleRow1 = this.book.CreateCellStyle();
            this.styleRow1.Alignment = HorizontalAlignment.CENTER;
            this.styleRow1.VerticalAlignment = VerticalAlignment.CENTER;
            this.styleRow1.BorderBottom = this.styleRow1.BorderLeft = this.styleRow1.BorderRight = this.styleRow1.BorderTop = BorderStyle.THIN;

            this.styleRow2 = this.book.CreateCellStyle();
            this.styleRow2.Alignment = HorizontalAlignment.RIGHT;
            this.styleRow2.VerticalAlignment = VerticalAlignment.CENTER;
            this.styleRow2.BorderBottom = this.styleRow2.BorderLeft = this.styleRow2.BorderRight = this.styleRow2.BorderTop = BorderStyle.THIN;

            this.styleRow3 = this.book.CreateCellStyle();
            this.styleRow3.Alignment = HorizontalAlignment.LEFT;
            this.styleRow3.VerticalAlignment = VerticalAlignment.CENTER;
            this.styleRow3.BorderBottom = this.styleRow3.BorderLeft = this.styleRow3.BorderRight = this.styleRow3.BorderTop = BorderStyle.THIN;
        }

        void CreateArea()
        {
            for (var i = 0; i < this.rowCount; i++)
            {
                var row = this.sheet.CreateRow(i);
                for (var j = 0; j < columnCount; j++)
                {
                    var cell = row.CreateCell(j);
                    cell.CellStyle = this.styleBase;
                }
            }
        }

        void AddTitle()
        {
            var cell = this.sheet.GetRow(this.rowIndex).GetCell(0);
            cell.CellStyle = this.styleTitle;
            cell.SetCellValue("WOA Reduction Report");

            var range = new CellRangeAddress(this.rowIndex, this.rowIndex, 0, columnCount - 1);
            this.sheet.AddMergedRegion(range);

            this.rowIndex ++;
        }

        void AddSubTitle()
        {
            var row = this.sheet.GetRow(this.rowIndex);

            var width = (int)Math.Ceiling((double)columnCount/2);
            var cell1 = row.GetCell(0);
            cell1.CellStyle = this.styleSubTitle1;
            cell1.SetCellValue(string.Format("Authority: {0}", this.data.Rows.First().AuthName));

            var range1 = new CellRangeAddress(this.rowIndex, this.rowIndex, 0, width - 1);
            this.sheet.AddMergedRegion(range1);

            var cell2 = row.GetCell(width);
            cell2.CellStyle = this.styleSubTitle2;
            cell2.SetCellValue(string.Format("{0} ~ {1}", this.data.DateFrom.ToString("yyyy-MM-dd"), this.data.DateTo.ToString("yyyy-MM-dd")));
            var range2 = new CellRangeAddress(this.rowIndex, this.rowIndex, width, columnCount - 1);
            this.sheet.AddMergedRegion(range2);

            this.rowIndex++;
        }

        void AddHeader()
        {
            var headers = new[]
            {
                "Court Name",
                "Capture Date",
                "WOA Number",
                "Original Fine Amount",
                "New Fine Amount",
                "Original Contempt Amount",
                "New Contempt Amount",
                "Magistrate Name"
            };

            var row = this.sheet.GetRow(this.rowIndex);
            for (var i = 0; i < columnCount; i++)
            {
                var cell = row.GetCell(i);
                cell.SetCellValue(headers[i]);
                cell.CellStyle = this.styleRow1;
            }

            this.rowIndex++;
        }

        void FillData()
        {
            var startIndex = this.rowIndex;
            for (var i = 0; i < this.dataCount; i++)
            {
                var row = this.sheet.GetRow(startIndex + i);
                var dataRow = this.data.Rows[i];
                for (var j = 0; j < columnCount; j++)
                {
                    var cell = row.GetCell(j);
                    switch (j)
                    {
                        case 0:
                            cell.SetCellValue(dataRow.CourtName);
                            break;
                        case 1:
                            cell.SetCellValue(dataRow.CaptureDate);
                            break;
                        case 2:
                            cell.SetCellValue(dataRow.WOANumber);
                            break;
                        case 3:
                            cell.SetCellValue(dataRow.OriginalFineAmount.ToString("f0", CultureInfo.InvariantCulture));
                            break;
                        case 4:
                            cell.SetCellValue(dataRow.NewFineAmount.ToString("f0", CultureInfo.InvariantCulture));
                            break;
                        case 5:
                            cell.SetCellValue(dataRow.OriginalContemptAmount.ToString("f0", CultureInfo.InvariantCulture));
                            break;
                        case 6:
                            cell.SetCellValue(dataRow.NewContemptAmount.ToString("f0", CultureInfo.InvariantCulture));
                            break;
                        case 7:
                            cell.SetCellValue(dataRow.Magistrate);
                            break;
                    }

                    cell.CellStyle = j >= 0 && j < 3
                        ? this.styleRow1
                        : (j >= 3 && j < 7
                            ? this.styleRow2
                            : this.styleRow3);
                }
                this.rowIndex++;
            }

            for (var i = 0; i < 3; i++)
            {
                var index = startIndex;
                foreach (var count in this.data.Rows.GroupBy(r =>
                {
                    switch (i)
                    {
                        case 0:
                            return new { r.CourtName, CaptureDate = "", WOANumber = "" };
                        case 1:
                            return new { r.CourtName, r.CaptureDate, WOANumber = "" };
                        default:
                            return new { r.CourtName, r.CaptureDate, r.WOANumber };
                    }
                }).Select(g => g.Count()))
                {
                    this.sheet.AddMergedRegion(new CellRangeAddress(index, index + count - 1, i, i));
                    index += count;
                }
            }
        }

        void AutoSize()
        {
            for (var i = 0; i < columnCount; i++)
                this.sheet.AutoSizeColumn(i, true);
        }
    }

    public class WOAReductionReportRow
    {
        public string AuthName { get; set; }

        public string CourtName { get; set; }
        public string CaptureDate { get; set; }
        public string WOANumber { get; set; }

        public decimal OriginalFineAmount { get; set; }
        public decimal NewFineAmount { get; set; }
        public decimal OriginalContemptAmount { get; set; }
        public decimal NewContemptAmount { get; set; }
        public string Magistrate { get; set; }
    }

    public class WOAReductionReportData
    {
        public List<WOAReductionReportRow> Rows = new List<WOAReductionReportRow>();
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
