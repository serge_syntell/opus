﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SIL.AARTO.BLL.Model;

namespace SIL.AARTO.BLL.WOAManagement.Model
{
    public class WOAReductionReportModel : MessageResult
    {
        public List<SelectListItem> AuthorityList = new List<SelectListItem>();

        public int AutIntNo { get; set; }
        public int CrtIntNo { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string DKey { get; set; }
        public string CrtName { get; set; }
    }
}
