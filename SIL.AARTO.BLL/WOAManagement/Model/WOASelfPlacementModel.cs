﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.Web.Resource.WOAManagement;

namespace SIL.AARTO.BLL.WOAManagement.Model
{
    public class WOASelfPlacementModel:MessageResult
    {
        public int AutIntNo { get; set; }
        public int WOAIntNo { get; set; }
        public string WOANumber { get; set; }
        public string WOAType { get; set; }
        public string WOATypeDescription
        {
            get
            {
                return string.Equals("B", WOAType, StringComparison.OrdinalIgnoreCase)
                    ? WOASelfPlacement.BenchWOA
                    : (string.Equals("D", WOAType, StringComparison.OrdinalIgnoreCase)
                        ? WOASelfPlacement.DoubleWOA
                        : WOASelfPlacement.StandardWOA);
            }
        }
        public string FullName { get; set; }
        public string IdNumber { get; set; }
        public DateTime? OldWOACourtDate { get; set; }
        public string OldWOACourtDateStr
        {
            get { return OldWOACourtDate.HasValue ? OldWOACourtDate.Value.ToString("yyyy-MM-dd") : ""; }
        }
        public DateTime? WOANewCourtDate { get; set; }
        public string WOANewCourtDateStr
        {
            get { return WOANewCourtDate.HasValue ? WOANewCourtDate.Value.ToString("yyyy-MM-dd") : ""; }
        }
        public DateTime? CourtDate { get; set; }
        public string CourtDateStr
        {
            get { return CourtDate.HasValue ? CourtDate.Value.ToString("yyyy-MM-dd") : ""; }
        }
        public DateTime? WOAIssueDate { get; set; }
        public string WOAIssueDateStr
        {
            get { return WOAIssueDate.HasValue ? WOAIssueDate.Value.ToString("yyyy-MM-dd") : ""; }
        }
        public string WOAStatus { get; set; }
        public string WOAServedStatus { get; set; }
        public decimal WOAFineAmount { get; set; }
        public decimal WOAAddAmount { get; set; }
        //public int CrtRIntNo { get; set; }
        public int OGIntNo { get; set; }
        public long WOARowVersion { get; set; }

        public bool HasSnapshot { get; set; }
        public DateTime WOAExpiredDate { get; set; }

        public List<string> WOACourtDates = new List<string>();
    }
}
