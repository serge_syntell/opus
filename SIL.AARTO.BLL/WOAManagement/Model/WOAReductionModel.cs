﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.Web.Resource.WOAManagement;

namespace SIL.AARTO.BLL.WOAManagement.Model
{
    public class WOAReductionModel : MessageResult
    {
        public List<SelectListItem> OfficiaList = new List<SelectListItem>();
        public int AutIntNo { get; set; }
        public int WOAIntNo { get; set; }
        public string WOANumber { get; set; }
        public string WOAType { get; set; }
        public string WOATypeDescription
        {
            get
            {
                return string.Equals("B", WOAType, StringComparison.OrdinalIgnoreCase)
                    ? WOAReduction.BenchWOA
                    : (string.Equals("D", WOAType, StringComparison.OrdinalIgnoreCase)
                        ? WOAReduction.DoubleWOA
                        : WOAReduction.StandardWOA);
            }
        }
        public string FullName { get; set; }
        public string IdNumber { get; set; }
        public DateTime? CourtDate { get; set; }
        public string CourtDateStr
        {
            get { return CourtDate.HasValue ? CourtDate.Value.ToString("yyyy-MM-dd") : ""; }
        }
        public DateTime? WOAIssueDate { get; set; }
        public string WOAIssueDateStr
        {
            get { return WOAIssueDate.HasValue ? WOAIssueDate.Value.ToString("yyyy-MM-dd") : ""; }
        }
        public string WOAStatus { get; set; }
        public string WOAServedStatus { get; set; }
        public decimal TotalFineAmountOld { get; set; }
        public decimal TotalContemptAmountOld { get; set; }
        public decimal TotalFineAmountNew { get; set; }
        public decimal TotalContemptAmountNew { get; set; }
        public int CrtIntNo { get; set; }
        public int CoMaIntNo { get; set; }
        public long WOARowVersion { get; set; }
        public long ChgRowVersion { get; set; }
        public long SchRowVersion { get; set; }
    }
}
