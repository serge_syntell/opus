﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Data;
using System.Data.SqlClient;
using Stalberg.TMS;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.Web.Resource;

namespace SIL.AARTO.BLL.WOAManagement
{
    [Serializable]
    public class AccusedInfo
    {
        public string AccusedName { get; set; }
        public string IDNumber { get; set; }
        public string CourtName { get; set; }
        public string CourtRoomNo { get; set; }
        public List<string> CourtDateList { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMsg { get; set; }

        public AccusedInfo()
        {
            IsSuccess = false;
            CourtDateList = new List<string>();
        }
    }
    public class WOASearchDAO : IRowVersion
    {
        public int WOAIntNo { get; set; }
        public string AccSurname { get; set; }
        public string AccInitials { get; set; }
        public string AccForenames { get; set; }
        public string AccIDNumber { get; set; }
        public string WOANumber { get; set; }
        public int WOAChargeStatus { get; set; }
        public string CSDescr { get; set; }
        public string SummonsNo { get; set; }
        public string AccFullName { get; set; }
    }

    [Serializable]
    public class KeyValue<TKey, TValue>
    {
        public TKey Key { get; set; }
        public TValue Value { get; set; }
    }

    public class WOAManager
    {
        string _ConnectionString;
        public WOAManager(string connectionString)
        {
            this._ConnectionString = connectionString;
        }

        public WOAManager() { }

        //Jerry 2014-10-28 change
        private WoaService woaService = new WoaService();
        private TrafficOfficerService trafficOfficerService = new TrafficOfficerService();
        private SummonsService summonsService = new SummonsService();
        private NoticeSummonsService noticeSummonsService = new NoticeSummonsService();
        private NoticeService noticeService = new NoticeService();
        private ChargeService chargeService = new ChargeService();
        private SumChargeService sumChargeService = new SumChargeService();
        private EvidencePackService evidencePackService = new EvidencePackService();
        private CourtDatesService courtDatesService = new CourtDatesService();
        private CourtJudgementTypeService courtJudgementTypeService = new CourtJudgementTypeService();
        private SysParamService sysParamService = new SysParamService();
        private CourtService courtService = new CourtService();
        private CourtRoomService courtRoomService = new CourtRoomService();
        private AuthorityService authorityService = new AuthorityService();
        private PrintFileNameService pfnSvc = new PrintFileNameService();
        private PrintFileNameSummonsService pfnsSvc = new PrintFileNameSummonsService();

        public List<WOASearchDAO> GetWoaByWoaNumber(int authIntNo, string IdNumber, string woaNumber, out List<KeyValue<int, long>> noticeRowVersionList)
        {
            // 2014-07-22, Oscar added rowversion.
            noticeRowVersionList = new List<KeyValue<int, long>>();

            List<WOASearchDAO> returnList = new List<WOASearchDAO>();
            using (IDataReader reader = woaService.SearchWoaByAuthorityAndWoanumber(authIntNo, IdNumber, woaNumber))
            {
                WOASearchDAO woaDAO = new WOASearchDAO();
                while (reader.Read())
                {
                    returnList.Add(new WOASearchDAO()
                    {
                        WOAIntNo = Convert.ToInt32(reader["WOAIntNo"]),
                        AccSurname = reader["AccSurname"].ToString(),
                        AccForenames = reader["AccForenames"].ToString(),
                        AccIDNumber = reader["AccIDNumber"].ToString(),
                        WOANumber = reader["WOANumber"].ToString(),
                        AccInitials = reader["AccInitials"].ToString(),
                        WOAChargeStatus = reader["WOAChargeStatus"] is DBNull ? 0 : Convert.ToInt32(reader["WOAChargeStatus"]),
                        CSDescr = reader["CSDescr"] is DBNull ? "" : Convert.ToString(reader["CSDescr"]),
                        SummonsNo = reader["SummonsNo"].ToString(),
                        AccFullName = reader["AccFullName"].ToString(),
                        RowVersion = (byte[])reader["NotRowVersion"]    // 2014-07-22, Oscar added rowversion.
                    });

                    // 2014-07-22, Oscar added rowversion.
                    noticeRowVersionList.Add(new KeyValue<int, long>
                    {
                        Key = (int)reader["NotIntNo"],
                        Value = IRowVersion.GetRowVersionLong((byte[])reader["NotRowVersion"])
                    });
                }
            }
            return returnList;
        }

        public AccusedInfo GetAccusedInfoByWoaNumber(string woaNumber)
        {
            if (!String.IsNullOrEmpty(woaNumber))
            {
                woaNumber = woaNumber.Replace("/", "").Replace("-", "").Trim();
            }
            //Jerry 2014-04-01 check WOACancel != true
            //Woa woa = woaService.GetByWoaNoEnquiry(woaNumber).FirstOrDefault();
            Woa woa = woaService.GetByWoaNoEnquiry(woaNumber).Where(w => w.WoaCancel != true).FirstOrDefault();
            Summons summons = null;
            //Accused accused = null;
            if (woa != null)
            {
                if ((woa.WoaChargeStatus != 840 && woa.WoaChargeStatus != 843 && woa.WoaChargeStatus != 847 && woa.WoaChargeStatus != 870) || woa.WoaExpireDate != null)
                {
                    return new AccusedInfo()
                    {
                        IsSuccess = false,
                        ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg1
                    };
                }
                else
                {
                    summons = summonsService.DeepLoadBySumIntNo(woa.SumIntNo, false, DeepLoadType.IncludeChildren, new Type[] { typeof(Court), typeof(CourtRoom), typeof(SIL.AARTO.DAL.Entities.TList<Accused>) });

                    return new AccusedInfo()
                    {
                        IsSuccess = true,
                        AccusedName = (summons.AccusedCollection == null || summons.AccusedCollection.Count == 0) ? "" : String.Format("{0} {1}", summons.AccusedCollection[0].AccForenames, summons.AccusedCollection[0].AccSurname),
                        IDNumber = summons.AccusedCollection[0].AccIdNumber,
                        CourtName = summons.CrtIntNoSource == null ? "" : summons.CrtIntNoSource.CrtName,
                        CourtRoomNo = summons.CrtRintNoSource == null ? "" : String.Format("{0}~{1}", summons.CrtRintNoSource.CrtRoomNo, summons.CrtRintNoSource.CrtRoomName),
                        CourtDateList = GetCourtDatesList(woaNumber)
                    };
                }
            }
            else
            {
                return new AccusedInfo();
            }
        }

        public AccusedInfo GetAccusedInfoByWoaIntNo(int woaIntNo)
        {
            Woa woa = woaService.GetByWoaIntNo(woaIntNo);
            Summons summons = null;
            //Accused accused = null;
            //Jerry 2014-04-01 check WOACancel != true
            //if (woa != null)
            if (woa != null && (woa.WoaCancel == null || !woa.WoaCancel.Value))
            {
                if ((woa.WoaChargeStatus != 840 && woa.WoaChargeStatus != 843 && woa.WoaChargeStatus != 847 && woa.WoaChargeStatus != 870) || woa.WoaExpireDate != null)
                {
                    return new AccusedInfo()
                    {
                        IsSuccess = false,
                        ErrorMsg = SIL.AARTO.Web.Resource.WOAManagement.WOABookCapture.ReturnMsg1
                    };
                }
                else
                {
                    summons = summonsService.DeepLoadBySumIntNo(woa.SumIntNo, false, DeepLoadType.IncludeChildren, new Type[] { typeof(Court), typeof(CourtRoom), typeof(SIL.AARTO.DAL.Entities.TList<Accused>) });

                    return new AccusedInfo()
                    {
                        IsSuccess = true,
                        AccusedName = (summons.AccusedCollection == null || summons.AccusedCollection.Count == 0) ? "" : summons.AccusedCollection[0].AccFullName,
                        IDNumber = summons.AccusedCollection[0].AccIdNumber,
                        CourtName = summons.CrtIntNoSource == null ? "" : summons.CrtIntNoSource.CrtName,
                        CourtRoomNo = summons.CrtRintNoSource == null ? "" : String.Format("{0}~{1}", summons.CrtRintNoSource.CrtRoomNo, summons.CrtRintNoSource.CrtRoomName),
                        CourtDateList = GetCourtDatesList(woa.WoaNumber)
                    };
                }
            }
            else
            {
                return new AccusedInfo();
            }
        }

        public Woa GetWoaById(int woaIntNo)
        {
            return woaService.GetByWoaIntNo(woaIntNo);
        }

        public bool SaveWoa(Woa woa)
        {
            return woaService.Save(woa) != null;
        }

        #region Jerry 2014-11-13 rebuild it
        // Jake 2013-08-28 modified paramater
        //public int BookOutWOAToOfficer(string[] woaIntNoArr, int toIntNo, string lastUser, List<KeyValue<int, long>> noticeRowVersionList)
        //{
        //    int returnValue = 0;
        //    using (ConnectionScope.CreateTransaction())
        //    {
        //        if (woaIntNoArr == null || woaIntNoArr.Length == 0)
        //        {
        //            return returnValue;
        //        }



        //        WoaQuery query = new WoaQuery();
        //        query.AppendIn(WoaColumn.WoaIntNo, woaIntNoArr);
        //        //Jerry 2014-11-10 add
        //        query.AppendEquals(WoaColumn.IsCurrent, "true");
        //        query.AppendNotEquals(WoaColumn.WoaCancel, "true");

        //        //Jerry 2014-04-01 check WOACancel != true and IsCurrent = 1
        //        //SIL.AARTO.DAL.Entities.TList<Woa> woas = woaService.Find(query as IFilterParameterCollection);
        //        //Jerry 2014-11-10 change the where clause
        //        //List<Woa> woas = woaService.Find(query as IFilterParameterCollection).Where(w => w.IsCurrent == true && w.WoaCancel != true).ToList();
        //        SIL.AARTO.DAL.Entities.TList<Woa> woas = woaService.Find(query as IFilterParameterCollection);
        //        foreach (Woa woa in woas)
        //        {

        //            int chgIntNo = 0;
        //            //Woa woa = woaService.GetByWoaNumber(woaNumber).Where(w => w.IsCurrent == true).FirstOrDefault();
        //            //if (woa == null) return -1;

        //            //Jerry 2014-10-28 change
        //            //TrafficOfficer trafficOfficer = new TrafficOfficerService().GetByToIntNo(toIntNo);
        //            TrafficOfficer trafficOfficer = trafficOfficerService.GetByToIntNo(toIntNo);
        //            if (trafficOfficer == null) return -2;

        //            //if (woa.WoaChargeStatus == (int)ChargeStatusList.WOANotServedUntraceable)
        //            //{
        //            //    if (woa.TimesOfGoingThroughSection72.HasValue)
        //            //    {
        //            //        woa.TimesOfGoingThroughSection72++;
        //            //    }
        //            //    else
        //            //    {
        //            //        woa.TimesOfGoingThroughSection72 = 1;
        //            //    }
        //            //}

        //            woa.WoaBookOutToOfficerDate = DateTime.Now;
        //            woa.WoaOfficerNumber = trafficOfficer.ToNo;
        //            woa.WoaChargePrevStatus = woa.WoaChargeStatus;
        //            woa.WoaChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 833;
        //            woa.LastUser = lastUser;


        //            woaService.Save(woa);

        //            //Jerry 2014-10-28 change
        //            //Summons summons = new SummonsService().GetBySumIntNo(woa.SumIntNo);
        //            Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);
        //            if (summons == null) return -3;

        //            //summons.PreviousSumCourtDate = summons.SumCourtDate;
        //            summons.SumPrevSummonsStatus = summons.SummonsStatus;
        //            summons.SummonsStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 833;\
        //            summons.LastUser = lastUser;
        //            //summons.LastUser = lastUser;
        //            //Jerry 2014-10-28 change
        //            //new SummonsService().Save(summons);
        //            summonsService.Save(summons);

        //            //Jerry 2014-10-28 change
        //            //NoticeSummons noticeSummons = new NoticeSummonsService().GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
        //            NoticeSummons noticeSummons = noticeSummonsService.GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
        //            if (noticeSummons == null) return -4;

        //            Notice notice = noticeService.GetByNotIntNo(noticeSummons.NotIntNo);
        //            if (notice == null) return -5;

        //            // 2014-07-22, Oscar added rowversion.
        //            KeyValue<int, long> noticeRowVersion;
        //            if ((noticeRowVersion = noticeRowVersionList.FirstOrDefault(n => n.Key == notice.NotIntNo)) == null
        //                || IRowVersion.GetRowVersionLong(notice.RowVersion) != noticeRowVersion.Value)
        //                return -99;

        //            notice.NotPrevNoticeStatus = notice.NoticeStatus;
        //            notice.NoticeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 833;
        //            notice.LastUser = lastUser;
        //            noticeService.Save(notice);

        //            SIL.AARTO.DAL.Entities.TList<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo);
        //            if (charges.Count() == 0) return -6;
        //            foreach (Charge chg in charges)
        //            {
        //                if (chg.ChgIsMain && chg.ChgSequence == 1)
        //                    chgIntNo = chg.ChgIntNo;

        //                chg.PreviousStatus = chg.ChargeStatus;
        //                chg.ChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 833;
        //                chg.LastUser = lastUser;
        //            }
        //            chargeService.Save(charges);

        //            SIL.AARTO.DAL.Entities.TList<SumCharge> sumCharges = sumChargeService.GetBySumIntNo(summons.SumIntNo);
        //            if (sumCharges.Count() == 0) return -7;
        //            foreach (SumCharge sc in sumCharges)
        //            {
        //                sc.SchPrevSumChargeStatus = sc.SumChargeStatus;
        //                sc.SumChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 833;
        //                sc.LastUser = lastUser;
        //            }
        //            sumChargeService.Save(sumCharges);

        //            EvidencePack ep = new EvidencePack();
        //            ep.ChgIntNo = chgIntNo;
        //            ep.EpItemDate = DateTime.Now;
        //            ep.EpItemDescr = String.Format("WOA booked out to officer {0}~{1}", trafficOfficer.TosName, trafficOfficer.ToNo);
        //            ep.EpSourceTable = "WOA";
        //            ep.EpSourceId = woa.WoaIntNo;
        //            ep.LastUser = lastUser;
        //            ep.EpTransactionDate = DateTime.Now;
        //            ep.EpItemDateUpdated = false;
        //            ep.EpatIntNo = (int)EpActionTypeList.Others;//Jerry 2013-10-15 add
        //            evidencePackService.Insert(ep);
        //        }
        //        ConnectionScope.Complete();
        //        returnValue = 1;

        //    }

        //    return returnValue;
        //}
        #endregion

        public int BookOutWOAToOfficer(string[] woaIntNoArr, int toIntNo, string lastUser, List<KeyValue<int, long>> noticeRowVersionList, ref string errorMsg)
        {
            int returnValue = 0;

            if (woaIntNoArr == null || woaIntNoArr.Length == 0)
            {
                return returnValue;
            }

            try
            {
                TrafficOfficer trafficOfficer = trafficOfficerService.GetByToIntNo(toIntNo);
                if (trafficOfficer == null) return -2;

                WoaQuery query = new WoaQuery();
                query.AppendIn(WoaColumn.WoaIntNo, woaIntNoArr);
                query.AppendEquals(WoaColumn.IsCurrent, "true");
                query.AppendNotEquals(WoaColumn.WoaCancel, "true");
                SIL.AARTO.DAL.Entities.TList<Woa> woas = woaService.Find(query as IFilterParameterCollection);

                foreach (Woa woa in woas)
                {
                    using (ConnectionScope.CreateTransaction())
                    {
                        int chgIntNo = 0;

                        woa.WoaBookOutToOfficerDate = DateTime.Now;
                        woa.WoaOfficerNumber = trafficOfficer.ToNo;
                        woa.WoaChargePrevStatus = woa.WoaChargeStatus;
                        woa.WoaChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 843;
                        woa.LastUser = lastUser;
                        woaService.Save(woa);

                        Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);
                        if (summons == null) return -3;

                        summons.SumPrevSummonsStatus = summons.SummonsStatus;
                        summons.SummonsStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 843
                        summons.LastUser = lastUser;
                        summonsService.Save(summons);

                        NoticeSummons noticeSummons = noticeSummonsService.GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                        if (noticeSummons == null) return -4;

                        Notice notice = noticeService.GetByNotIntNo(noticeSummons.NotIntNo);
                        if (notice == null) return -5;

                        // 2014-07-22, Oscar added rowversion.
                        KeyValue<int, long> noticeRowVersion;
                        if ((noticeRowVersion = noticeRowVersionList.FirstOrDefault(n => n.Key == notice.NotIntNo)) == null
                            || IRowVersion.GetRowVersionLong(notice.RowVersion) != noticeRowVersion.Value)
                            return -99;

                        notice.NotPrevNoticeStatus = notice.NoticeStatus;
                        notice.NoticeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 843;
                        notice.LastUser = lastUser;
                        noticeService.Save(notice);

                        SIL.AARTO.DAL.Entities.TList<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo);
                        if (charges.Count() == 0) return -6;
                        foreach (Charge chg in charges)
                        {
                            if (chg.ChgIsMain && chg.ChgSequence == 1)
                                chgIntNo = chg.ChgIntNo;

                            chg.PreviousStatus = chg.ChargeStatus;
                            chg.ChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 843;
                            chg.LastUser = lastUser;
                        }
                        chargeService.Save(charges);

                        SIL.AARTO.DAL.Entities.TList<SumCharge> sumCharges = sumChargeService.GetBySumIntNo(summons.SumIntNo);
                        if (sumCharges.Count() == 0) return -7;
                        foreach (SumCharge sc in sumCharges)
                        {
                            sc.SchPrevSumChargeStatus = sc.SumChargeStatus;
                            sc.SumChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// 843;
                            sc.LastUser = lastUser;
                        }
                        sumChargeService.Save(sumCharges);

                        EvidencePack ep = new EvidencePack();
                        ep.ChgIntNo = chgIntNo;
                        ep.EpItemDate = DateTime.Now;
                        ep.EpItemDescr = String.Format("WOA booked out to officer {0}~{1}", trafficOfficer.TosName, trafficOfficer.ToNo);
                        ep.EpSourceTable = "WOA";
                        ep.EpSourceId = woa.WoaIntNo;
                        ep.LastUser = lastUser;
                        ep.EpTransactionDate = DateTime.Now;
                        ep.EpItemDateUpdated = false;
                        ep.EpatIntNo = (int)EpActionTypeList.Others;
                        evidencePackService.Insert(ep);

                        ConnectionScope.Complete();
                        returnValue = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                returnValue = -100;
            }

            return returnValue;
        }


        //Jake 2014-03-05 modified , changed parameter woaNumber and use woaIntNo instead
        public int WoaBookCapture(int woaIntNo, int servedStatus, DateTime? newCourtDate, int? courtFrom, string lastUser, int courtTo, double? bailAmount, string bailReceiptNumber, DateTime? bailPaidDate, out string errorMsg)
        {
            errorMsg = "";
            //if (!String.IsNullOrEmpty(woaNumber))
            //{
            //    woaNumber = woaNumber.Replace("/", "").Replace("-", "").Trim();
            //}

            SqlConnection con = new SqlConnection(_ConnectionString);
            SqlCommand cmd = new SqlCommand("WOABookCapture", con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0; //Jerry 2014-10-31 add

            SqlParameter parameterWOANo = new SqlParameter("@WOAIntNo", SqlDbType.Int);
            parameterWOANo.Value = woaIntNo;
            cmd.Parameters.Add(parameterWOANo);

            SqlParameter parameterServedStatus = new SqlParameter("@ServedStatus", SqlDbType.Int, 4);
            parameterServedStatus.Value = servedStatus;
            cmd.Parameters.Add(parameterServedStatus);

            SqlParameter parameterNewCourtDate = new SqlParameter("@NewCourtDate", SqlDbType.SmallDateTime);
            parameterNewCourtDate.Value = newCourtDate;
            cmd.Parameters.Add(parameterNewCourtDate);

            SqlParameter parameterCourtFrom = new SqlParameter("@CourtFrom", SqlDbType.Int, 4);
            parameterCourtFrom.Value = courtFrom;
            cmd.Parameters.Add(parameterCourtFrom);

            SqlParameter parameterCourtTo = new SqlParameter("@CourtTo", SqlDbType.Int, 4);
            parameterCourtTo.Value = courtTo;
            cmd.Parameters.Add(parameterCourtTo);

            SqlParameter parameterBailAmount = new SqlParameter("@BailAmount", SqlDbType.Real);
            parameterBailAmount.Value = bailAmount;
            cmd.Parameters.Add(parameterBailAmount);

            SqlParameter parameterBailReceiptNumber = new SqlParameter("@BailReceiptNumber", SqlDbType.NVarChar, 100);
            parameterBailReceiptNumber.Value = bailReceiptNumber;
            cmd.Parameters.Add(parameterBailReceiptNumber);

            SqlParameter parameterBailPaidDate = new SqlParameter("@BailPaidDate", SqlDbType.SmallDateTime);
            parameterBailPaidDate.Value = bailPaidDate;
            cmd.Parameters.Add(parameterBailPaidDate);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            cmd.Parameters.Add(parameterLastUser);

            //2014-07-21 Heidi Comment out @ReturnValue parameter to Solve the problem of page transaction and sp transaction conflict(5321).
            //SqlParameter parameterReturnValue = new SqlParameter("@ReturnValue", SqlDbType.Int, 4);
            //parameterReturnValue.Direction = ParameterDirection.Output;
            //cmd.Parameters.Add(parameterReturnValue);

            try
            {
                con.Open();
                //cmd.ExecuteNonQuery();
                int records = Convert.ToInt32(cmd.ExecuteScalar());
                con.Dispose();

                return records;// return Convert.ToInt32(parameterReturnValue.Value);
            }
            catch (Exception e)
            {
                con.Dispose();
                errorMsg = e.Message;
                return -1;
            }
        }

        public int WoaBookCapture(string woaNumber, int servedStatus, DateTime? newCourtDate, string lastUser)
        {
            int returnValue = 0;
            if (!String.IsNullOrEmpty(woaNumber))
            {
                woaNumber = woaNumber.Replace("/", "").Replace("-", "").Trim();
            }
            if (newCourtDate.HasValue && newCourtDate <= DateTime.Now)
            {
                return -8;
            }
            using (ConnectionScope.CreateTransaction())
            {
                int chgIntNo = 0;
                // we need to push a queue to generatewoa service
                //Jerry 2014-04-01 check WOACancel != true
                //Woa woa = woaService.GetByWoaNoEnquiry(woaNumber).Where(w => w.IsCurrent == true).FirstOrDefault();
                Woa woa = woaService.GetByWoaNoEnquiry(woaNumber).Where(w => w.IsCurrent == true && w.WoaCancel != true).FirstOrDefault();
                if (woa == null) return -1;
                if (woa.WoaChargeStatus != (int)ChargeStatusList.WOABookedOutToOfficer) return -7;

                woa.LastUser = lastUser;
                int status = 0;
                switch (servedStatus)
                {
                    case 1:  //WOA service – personal  
                    case 2:  //WOA service – impersonal
                    case 4:  //WOA service - offender requests new court date 
                        status = (int)ChargeStatusList.CaseNumberGenerated;
                        break;
                    case 3:  //WOA service - not successful in tracing accused
                    case 5:  //Commit to prison  
                        status = (int)ChargeStatusList.WOAS72Served;
                        break;
                }
                //int status = newCourtDate.HasValue ? (int)ChargeStatusList.CaseNumberGenerated : (int)ChargeStatusList.WOAS72Served;
                //Jerry 2014-04-01 check WOACancel != true
                //SIL.AARTO.DAL.Entities.TList<Woa> woas = woaService.GetBySumIntNo(woa.SumIntNo);
                List<Woa> woas = woaService.GetBySumIntNo(woa.SumIntNo).Where(w => w.WoaCancel != true).ToList();
                foreach (Woa w in woas)
                {
                    if (w.WoaIntNo == woa.WoaIntNo)
                    {
                        w.WoaChargeStatus = (int)ChargeStatusList.WOAS72Served;
                        w.WoaServedStatus = servedStatus;
                        w.Woas72ReturnDate = DateTime.Now;
                        if (newCourtDate.HasValue)
                            w.WoaNewCourtDate = newCourtDate;
                        //w.IsCurrent = false;
                        if (newCourtDate.HasValue)
                            woa.WoaNewCourtDate = newCourtDate.Value;
                    }
                    else
                    {
                        if (w.IsCurrent)
                            w.IsCurrent = false;
                    }
                    w.LastUser = lastUser;

                    woaService.Save(w);
                }
                //woa.WoaChargeStatus = (int)ChargeStatusList.WOAS72Served;
                //woa.WoaServedStatus = servedStatus;
                //woa.Woas72ReturnDate = DateTime.Now;
                //woa.IsCurrent = false;
                //if (newCourtDate.HasValue)
                //    woa.WoaNewCourtDate = newCourtDate.Value;
                //woaService.Save(woas);

                Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);
                if (summons == null) return -2;
                //if (summons.SummonsStatus != (int)ChargeStatusList.WOAS72Served) return -7;

                summons.SummonsStatus = status;
                //summons.SumChargeStatus = status;
                if (newCourtDate.HasValue)
                {
                    //summons.PreviousSumCourtDate = summons.SumCourtDate;
                    summons.Section72FromCourtDate = summons.SumCourtDate;
                    summons.SumCourtDate = newCourtDate.Value;
                }
                summons.Section72Count += 1;
                summons.IsSection72 = true;
                summons.LastUser = lastUser;
                summonsService.Save(summons);

                NoticeSummons noticeSummons = noticeSummonsService.GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                if (noticeSummons == null) return -3;

                Notice notice = noticeService.GetByNotIntNo(noticeSummons.NotIntNo);
                if (notice == null) return -4;

                //woa.NotIntNo = notice.NotIntNo;
                notice.NoticeStatus = status;
                notice.LastUser = lastUser;

                noticeService.Save(notice);

                SIL.AARTO.DAL.Entities.TList<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo);
                if (charges.Count() == 0) return -5;
                foreach (Charge chg in charges)
                {
                    if (chg.ChargeStatus < 900)
                        chg.ChargeStatus = status;// 834;
                    if (chg.ChgIsMain && chg.ChgSequence == 1) chgIntNo = chg.ChgIntNo;

                    chg.LastUser = lastUser;

                }
                chargeService.Save(charges);

                SIL.AARTO.DAL.Entities.TList<SumCharge> sumCharges = sumChargeService.GetBySumIntNo(summons.SumIntNo);
                if (sumCharges.Count() == 0) return -6;

                // Jake 2013-04-08 added
                SIL.AARTO.DAL.Entities.CourtJudgementType cjt = courtJudgementTypeService.GetByCrtIntNoCjtCode(summons.CrtIntNo, 0);
                foreach (SumCharge sc in sumCharges)
                {
                    if (sc.SumChargeStatus < 900)
                        sc.SumChargeStatus = status;// 834;
                    if (cjt != null) sc.CjtIntNo = cjt.CjtIntNo;

                    sc.LastUser = lastUser;
                }
                sumChargeService.Save(sumCharges);

                EvidencePack ep = new EvidencePack();
                ep.ChgIntNo = chgIntNo;
                ep.EpItemDate = DateTime.Now;
                ep.EpItemDescr = String.Format("WOA Service of Section72 Booked in with new court date {0}", newCourtDate.HasValue ? "" : newCourtDate.Value.ToString("yyyy-MM-dd"));
                ep.EpSourceTable = "WOA";
                ep.EpSourceId = woa.WoaIntNo;
                ep.LastUser = lastUser;
                ep.EpTransactionDate = DateTime.Now;
                ep.EpItemDateUpdated = true;
                ep.EpatIntNo = (int)EpActionTypeList.Others;//Jerry 2013-10-15 add
                evidencePackService.Insert(ep);

                if (newCourtDate.HasValue && summons.SummonsStatus == (int)ChargeStatusList.CaseNumberGenerated)
                {
                    int pfnIntNo = -1;
                    DateTime actionDate; string autCode = string.Empty;
                    SetPrintFileName(summons, newCourtDate.Value, "", out pfnIntNo, out actionDate, out autCode);
                    DateTime printActionDate = DateTime.Now.AddHours(sysParamService.GetBySpColumnName(SysParamList.DelayActionDate_InHours.ToString()).SpIntegerValue);
                    new QueueItemProcessor().Send(
                        new QueueItem()
                        {
                            Body = pfnIntNo,
                            Group = autCode,
                            ActDate = actionDate > printActionDate ? actionDate : printActionDate,
                            QueueType = ServiceQueueTypeList.SecondAppearanceCCR
                        }
                    );
                }


                ConnectionScope.Complete();
                returnValue = 1;
            }

            return returnValue;
        }

        //Jake 2014-03-05 modified parameter , use woaIntNo instead of woaNumber 
        public int WoaCaptureReverse(int woaIntNo, string lastUser, bool reversalOfS72,out string errorMessage)
        {
            errorMessage = string.Empty;
            //if (String.IsNullOrEmpty(woaNumber)) return 0;

            //woaNumber = woaNumber.Replace("/", "").Replace("-", "");
            int returnValue = 0;

            Woa woa = woaService.GetByWoaIntNo(woaIntNo);
            if (woa == null) return -1;

            NoticeSummons ns = noticeSummonsService.GetBySumIntNo(woa.SumIntNo).FirstOrDefault();
            Notice notice = noticeService.GetByNotIntNo(ns.NotIntNo);

            //WHEN 5 THEN 661		--Untreatable
            //WHEN 2 THEN 847		--Released on Sec 72
            //WHEN 3 THEN 959      --Jailed 
            //WHEN 4 THEN 847      --Transferred
            if (new int[] { 661, 847, 959, 955 }.Contains(woa.WoaChargeStatus ?? 0) == false)
            {
                errorMessage = "This WOA status [" + ((ChargeStatusList)notice.NoticeStatus).ToString() + "] is not eligible for reversal";
                return -4;
            }

            Woa latestWOA = woaService.GetBySumIntNo(woa.SumIntNo).Where(w => w.WoaCancel == false).OrderByDescending(w => w.WoaEdition).FirstOrDefault();
            if (woa.WoaEdition < latestWOA.WoaEdition)
            {
                errorMessage = "Please enter the WOA number of the latest outstanding WOA on this case to reverse the WOA";
                return -5;
            }

            WOAExecutionSnapshot.WoaExecutionSnapshotDB wes = new WOAExecutionSnapshot.WoaExecutionSnapshotDB(this._ConnectionString);
            
            //Jake 2014-08-28 added a parameter to identity WOA cancellation reversal or WOA execution reversal
            //WER -> WOA execution reversal
            if (wes.TestRollBack(woa.WoaIntNo, "WER", reversalOfS72,out errorMessage))
            {
                try
                {
                    bool flag = wes.RollBackFromSnapshot(woa.WoaIntNo, lastUser);
                    if (flag)
                    {
                        returnValue = 1;
                    }
                    else
                    {
                        returnValue = -3;
                    }
                }
                catch (Exception ex)
                {
                    returnValue = -2;
                    errorMessage += "\r\n" + ex.Message;
                }
            }


            /* Jake 2013-11-20 comment out , use snapshot instead
            using (ConnectionScope.CreateTransaction())
            {
                int chgIntNo = 0;
                Woa woa = woaService.GetByWoaNoEnquiry(woaNumber).Where(w => w.IsCurrent == true).FirstOrDefault();
                if (woa == null) return -1;

                Summons summons = new SummonsService().GetBySumIntNo(woa.SumIntNo);
                if (summons == null) return -2;

                if (woa.WoaChargeStatus != (int)ChargeStatusList.WOAS72Served) return -3;
                //if (summons.SummonsStatus != (int)ChargeStatusList.WOAS72Served)
                //{
                //    return -3;
                //}
                if (summons.SumCourtDate <= DateTime.Now)
                {
                    return -4;
                }
                //Woa woa_Orig = woaService.GetBySumIntNo(summons.SumIntNo).Where(w => w.WoaNoEnquiry != woa.WoaNoEnquiry).OrderByDescending(w => w.WoaLoadDate).FirstOrDefault();
                //if (woa_Orig != null)
                //{
                //    woa_Orig.IsCurrent = true;
                //    woa_Orig.WoaChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;
                //    woa_Orig.WoaServedStatus = null;
                //    woa_Orig.Woas72ReturnDate = null;
                //    woa_Orig.WoaNewCourtDate = null;
                //    woa_Orig.NotIntNo = null;
                //    woaService.Save(woa_Orig);

                //}
                //else
                //{
                //    woa.WoaChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;
                //    woa.WoaServedStatus = null;
                //    woa.Woas72ReturnDate = null;
                //    woa.WoaNewCourtDate = null;
                //    woa.NotIntNo = null;
                //}

                woa.WoaChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;
                woa.WoaServedStatus = 0;  // Jake modified from null to default value 0
                woa.Woas72ReturnDate = null;
                woa.WoaNewCourtDate = null;
                woa.LastUser = lastUser;
                woa.NotIntNo = null;
                woa.LastUser = lastUser;

                woaService.Save(woa);

                summons.SummonsStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// summons.SumPrevSummonsStatus.Value;
                //summons.PreviousSumCourtDate = null;
                summons.Section72FromCourtDate = null;
                //summons.IsSection72 = false;
                if (summons.Section72Count > 0)
                    summons.Section72Count -= 1;
                summons.LastUser = lastUser;
                new SummonsService().Save(summons);

                NoticeSummons noticeSummons = new NoticeSummonsService().GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                if (noticeSummons == null) return -5;

                Notice notice = new NoticeService().GetByNotIntNo(noticeSummons.NotIntNo);
                if (notice == null) return -6;
                notice.LastUser = lastUser;
                notice.NoticeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// notice.NotPrevNoticeStatus.Value;
                notice.LastUser = lastUser;
                new NoticeService().Save(notice);

                SIL.AARTO.DAL.Entities.TList<Charge> charges = new ChargeService().GetByNotIntNo(notice.NotIntNo);
                if (charges.Count() == 0) return -7;

                foreach (Charge chg in charges)
                {
                    if (chg.ChargeStatus < 900)
                    {
                        chg.ChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// chg.PreviousStatus.Value;// 
                        chg.LastUser = lastUser;
                        if (chg.ChgIsMain && chg.ChgSequence == 1)
                        {
                            chgIntNo = chg.ChgIntNo;
                        }
                    }
                }
                new ChargeService().Save(charges);

                SIL.AARTO.DAL.Entities.TList<SumCharge> sumCharges = new SumChargeService().GetBySumIntNo(summons.SumIntNo);
                if (sumCharges.Count() == 0) return -8;
                foreach (SumCharge sc in sumCharges)
                {
                    if (sc.SumChargeStatus < 900)
                    {
                        sc.SumChargeStatus = (int)ChargeStatusList.WOABookedOutToOfficer;// sc.SchPrevSumChargeStatus.Value;
                        sc.LastUser = lastUser;
                    }
                }
                new SumChargeService().Save(sumCharges);

                EvidencePack ep = new EvidencePack();
                ep.ChgIntNo = chgIntNo;
                ep.EpItemDate = DateTime.Now;
                ep.EpItemDescr = String.Format("WOA section72 book-in reversed at {0}", DateTime.Now.ToString("yyyy-MM-dd"));
                ep.EpSourceTable = "Woa";
                ep.EpSourceId = woa.WoaIntNo;
                ep.LastUser = lastUser;
                ep.EpTransactionDate = DateTime.Now;
                ep.EpItemDateUpdated = false;
                ep.EpatIntNo = (int)EpActionTypeList.Others;//Jerry 2013-10-15 add
                new EvidencePackService().Insert(ep);

                ConnectionScope.Complete();
                returnValue = 1;
            }
             * 
             * */

            return returnValue;
        }

        public int WoaBookOutReverse(int woaIntNo, string lastUser, long noticeRowVersion)
        {
            //if (String.IsNullOrEmpty(woaNumber)) return 0;

            //woaNumber = woaNumber.Replace("/", "").Replace("-", "");
            int returnValue = 0;
            using (ConnectionScope.CreateTransaction())
            {
                int chgIntNo = 0;
                Woa woa = woaService.GetByWoaIntNo(woaIntNo);
                if (woa == null || woa.IsCurrent != true) return -1;

                Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);
                if (summons == null) return -2;

                if (summons.SummonsStatus != (int)ChargeStatusList.WOABookedOutToOfficer)
                {
                    return -3;
                }

                woa.LastUser = lastUser;
                woa.WoaChargeStatus = woa.WoaChargePrevStatus;// (int)ChargeStatusList.WOABookedOutToOfficer;
                woa.WoaOfficerNumber = null;
                woa.WoaBookOutToOfficerDate = null;
                woa.LastUser = lastUser;
                woaService.Save(woa);

                summons.SummonsStatus = summons.SumPrevSummonsStatus.Value;
                summons.LastUser = lastUser;
                summonsService.Save(summons);

                NoticeSummons noticeSummons = noticeSummonsService.GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                if (noticeSummons == null) return -5;

                Notice notice = noticeService.GetByNotIntNo(noticeSummons.NotIntNo);
                if (notice == null) return -6;

                // 2014-07-22, Oscar added rowversion.
                if (IRowVersion.GetRowVersionLong(notice.RowVersion) != noticeRowVersion)
                    return -99;

                notice.LastUser = lastUser;
                notice.NoticeStatus = notice.NotPrevNoticeStatus.Value;
                notice.LastUser = lastUser;
                noticeService.Save(notice);

                SIL.AARTO.DAL.Entities.TList<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo);
                if (charges.Count() == 0) return -7;
                foreach (Charge chg in charges)
                {
                    chg.ChargeStatus = chg.PreviousStatus.Value;//  
                    if (chg.ChgIsMain && chg.ChgSequence == 1)
                    {
                        chgIntNo = chg.ChgIntNo;
                        chg.LastUser = lastUser;
                    }

                }
                chargeService.Save(charges);

                SIL.AARTO.DAL.Entities.TList<SumCharge> sumCharges = sumChargeService.GetBySumIntNo(summons.SumIntNo);
                if (sumCharges.Count() == 0) return -8;
                foreach (SumCharge sc in sumCharges)
                {
                    sc.SumChargeStatus = sc.SchPrevSumChargeStatus.Value;
                    sc.LastUser = lastUser;
                }
                sumChargeService.Save(sumCharges);

                EvidencePack ep = new EvidencePack();
                ep.ChgIntNo = chgIntNo;
                ep.EpItemDate = DateTime.Now;
                ep.EpItemDescr = String.Format("WOA book-out to officer reversed at {0}", DateTime.Now.ToString("yyyy-MM-dd"));
                ep.EpSourceTable = "Woa";
                ep.EpSourceId = woa.WoaIntNo;
                ep.LastUser = lastUser;
                ep.EpTransactionDate = DateTime.Now;
                ep.EpItemDateUpdated = false;
                ep.EpatIntNo = (int)EpActionTypeList.Others;//Jerry 2013-10-15 add
                evidencePackService.Insert(ep);

                ConnectionScope.Complete();
                returnValue = 1;
            }

            return returnValue;
        }

        public List<string> GetCourtDatesList(string woaNumber)
        {
            List<string> courtDateList = new List<string>();
            courtDateList.Add("");

            if (!String.IsNullOrEmpty(woaNumber))
            {
                woaNumber = woaNumber.Replace("/", "").Replace("-", "").Trim();
            }
            //Jerry 2014-04-01 check WOACancel != true and w.IsCurrent == true
            //Woa woa = woaService.GetByWoaNoEnquiry(woaNumber).FirstOrDefault();
            Woa woa = woaService.GetByWoaNoEnquiry(woaNumber).Where(w => w.IsCurrent == true && w.WoaCancel != true).FirstOrDefault();
            if (woa != null)
            {
                Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);

                CourtDatesQuery query = new CourtDatesQuery();
                query.Append(CourtDatesColumn.AutIntNo, summons.AutIntNo.ToString());
                query.Append(CourtDatesColumn.CrtRintNo, summons.CrtRintNo.ToString());
                query.AppendGreaterThanOrEqual(CourtDatesColumn.Cdate, DateTime.Now.ToString("yyyy-MM-dd"));
                List<CourtDates> list = courtDatesService.Find(query as IFilterParameterCollection).Where(m => m.CdNoOfCases > (m.CdTotalAllocated ?? 0)).OrderBy(n => n.Cdate).ToList();

                for (int i = list.Count - 1; i >= 0; i--)
                {
                    courtDateList.Add(list[i].Cdate.ToString("yyyy-MM-dd"));
                }
            }

            return courtDateList;
        }

        public SIL.AARTO.DAL.Entities.TList<WoaServedStatus> GetWoaServedStatus()
        {
            return new WoaServedStatusCache().GetAll();
        }

        public TrafficOfficer GetTrafficOfficer(int toIntNo)
        {
            //Jerry 2014-10-28 change
            //return new TrafficOfficerService().GetByToIntNo(toIntNo);
            return trafficOfficerService.GetByToIntNo(toIntNo);
        }

        // Jake 2014-02-24 add 2ND in print file name and courtroom trim issue,
        // this function only apply to get printfilename for 2ndCCR
        public void SetPrintFileName(Summons summons, DateTime newCourtDate, string lastUser, out int pfnIntNo, out DateTime actDate, out string autCode)
        {
            string crtName = string.Empty, crtRoomNo = string.Empty;

            Court court = courtService.GetByCrtIntNo(summons.CrtIntNo);
            CourtRoom courtRoom = courtRoomService.GetByCrtRintNo(summons.CrtRintNo ?? 0);
            SIL.AARTO.DAL.Entities.Authority authority = authorityService.GetByAutIntNo(summons.AutIntNo);
            autCode = authority.AutCode.Trim();
            DateRulesDetails drRule = new DateRulesDetails()
            {
                AutIntNo = authority.AutIntNo,
                DtRStartDate = "CDate",
                DtREndDate = "FinalCourtRoll",
                LastUser = lastUser
            };
            DefaultDateRules drRuleDB = new DefaultDateRules(drRule, this._ConnectionString);
            int noOfDays = drRuleDB.SetDefaultDateRule();

            string crtDate = newCourtDate.ToString("yyyy-MM-dd");
            actDate = newCourtDate.AddDays(noOfDays);
            string printDate = actDate.ToString("yyyy-MM-dd HH-mm");
            string printFileName = string.Format("CR2ND_{0}_{1}_{2}_{3}_{4}", autCode, court == null ? "" : court.CrtName.Trim(), courtRoom == null ? "" : courtRoom.CrtRoomNo.Trim(), crtDate, printDate);

            //Jerry 2014-10-31 change
            //PrintFileNameService pfnSvc = new PrintFileNameService();
            //PrintFileNameSummonsService pfnsSvc = new PrintFileNameSummonsService();
            PrintFileName pfnEntity = pfnSvc.GetByPrintFileName(printFileName);
            if (pfnEntity == null)
            {
                pfnEntity = new PrintFileName()
                {
                    PrintFileName = printFileName,
                    AutIntNo = authority.AutIntNo,
                    LastUser = lastUser,
                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                };
                pfnEntity = pfnSvc.Save(pfnEntity);
            }
            PrintFileNameSummons pfnsEntity = pfnsSvc.GetBySumIntNoPfnIntNo(summons.SumIntNo, pfnEntity.PfnIntNo);
            if (pfnsEntity == null)
            {
                pfnsEntity = new PrintFileNameSummons()
                {
                    SumIntNo = summons.SumIntNo,
                    PfnIntNo = pfnEntity.PfnIntNo,
                    LastUser = lastUser,
                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                };
                pfnsEntity = pfnsSvc.Save(pfnsEntity);
            }
            pfnIntNo = pfnEntity.PfnIntNo;

        }

        //This function use deepload function from NetTier, Please use it carefully
        public IDataReader SearchWoaByWoaNumber(string woaNumber)
        {
            //SIL.AARTO.DAL.Entities.TList<Woa> woas = new WoaService().DeepLoadByWoaNumber(woaNumber, false, DeepLoadType.IncludeChildren, new Type[] { typeof(Summons), typeof(NoticeSummons), typeof(Notice) });
            //return woas;
            SqlConnection conn = new SqlConnection(_ConnectionString);
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SearchWOAByNumberForWOAExecutionReverse";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@WoaNumber", SqlDbType.NVarChar, 20)).Value = woaNumber;

            try
            {
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (SqlException ex)
            {
                conn.Close();
                throw ex;
            }

        }

        public SIL.AARTO.DAL.Entities.TList<Woa> DeepLoadSearchWoaByWoaNumber(string woaNumber)
        {
            if (String.IsNullOrEmpty(woaNumber)) return null;

            woaNumber = woaNumber.Replace("/", "").Replace("-", "");

            SqlConnection conn = new SqlConnection(_ConnectionString);

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "WOASearchByWOANumber";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@WOANumber", SqlDbType.NVarChar, 50)).Value = woaNumber;
            SIL.AARTO.DAL.Entities.TList<Woa> woas = new DAL.Entities.TList<Woa>();
            try
            {
                conn.Open();
                using (IDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        Woa w = new Woa()
                        {
                            WoaIntNo = Convert.ToInt32(reader["WoaIntNo"]),
                            WoaNumber = reader["WOANumber"].ToString(),
                            WoaChargeStatus = reader["WOAChargeStatus"] is DBNull ? null : (int?)reader["WOAChargeStatus"],
                            WoaExpireDate = reader["WoaExpireDate"] is DBNull ? null : (DateTime?)reader["WoaExpireDate"],
                            SumIntNoSource = new Summons()
                            {
                                SummonsNo = reader["SummonsNo"].ToString(),
                                SumNoticeNo = reader["NotTicketNo"].ToString(),
                                AccusedCollection = new DAL.Entities.TList<Accused>(){
                                    new Accused(){ AccFullName=reader["AccFullName"]==null? "": reader["AccFullName"].ToString()}
                                  }
                            },
                        };
                        woas.Add(w);
                    }
                }
            }
            catch (SqlException ex)
            {
                conn.Close();
                throw ex;
            }
            finally
            {
                conn.Close();
            }

            //WoaService woaService = new WoaService();
            //SIL.AARTO.DAL.Entities.TList<Woa> woas = woaService.DeepLoadByWoaNoEnquiry(woaNumber, false, DeepLoadType.IncludeChildren, new Type[] { typeof(Summons), typeof(SIL.AARTO.DAL.Entities.TList<Accused>) });

            return woas;
        }
    }
}
