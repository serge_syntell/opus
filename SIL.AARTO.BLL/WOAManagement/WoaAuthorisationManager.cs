﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections;
using System.Data;
using SIL.AARTO.DAL.Data;
using System.Data.SqlClient;


namespace SIL.AARTO.BLL.WOAManagement
{
    public class WoaPrintFileNameEntity
    {
        public int PFNIntNo { get; set; }
        public string PrintFileName { get; set; }
        public int NoOfWarrants { get; set; }
        public bool Checked { get; set; }
    }

    public class WoaAuthorisationEntity
    {
        public int WoaIntNo { get; set; }
        public string WoaNumber { get; set; }
        public string CaseNumber { get; set; }
        public string SendToCourt { get; set; }
        public string ReturnFromCourt { get; set; }
        // Jake 2013-08-28 added new field
        public int Status { get; set; }
        public string RejReason { get; set; }
        public string NotTicketNo { get; set; }
        public string SummonsNo { get; set; }
        public string AccFullName { get; set; }
    }

    [Serializable]
    public class VersionValidationEntity
    {
        public int WoaIntNo { get; set; }
        public int NotIntNo { get; set; }
        public int SumIntNo { get; set; }
        public int NoticeStatus { get; set; }
        public int SummonsStatus { get; set; }
        public int WoaChargeStatus { get; set; }
        public byte[] WoaVersion { get; set; }
        public byte[] NoticeVersion { get; set; }
        public byte[] SummonsVersion { get; set; }

        public Dictionary<int, byte[]> ChargeVersions { get; set; }
        public Dictionary<int, byte[]> SumChargeVersions { get; set; }
    }


    public class AuthorisationTypeEntity
    {
        public int TypeId { get; set; }
        public string Description { get; set; }
    }

    public class WoaAuthorisationJsonEntity
    {
        public int WoaIntNo { get; set; }
        public int Status { get; set; }
        public string SentToCourt { get; set; }
        public string ReturnFromCourt { get; set; }
        public string RejectDate { get; set; }
        public string RejectReason { get; set; }
        public int CrtIntNo { get; set; }
        public int CrtRIntNo { get; set; }
        public int CDIntNo { get; set; }
    }

    public class WoaAuthorisationPkg
    {
        public int CrtIntNo { get; set; }

        public List<WoaAuthorisationJsonEntity> WoaRequest { get; set; }

    }

    public class SendToCourtJsonEntity
    {
        public int CrtIntNo { get; set; }
        public string CourtDate { get; set; }
        public List<string> WoaIntNoList { get; set; }
    }

    // Jake 2013-08-28 modified GetPagedPrintFileName function
    // added new parameter 
    public class WoaAuthorisationManager
    {
        private string connectionString;
        public WoaAuthorisationManager() { }
        public WoaAuthorisationManager(string connectStr)
        {
            connectionString = connectStr;
        }
        //Jerry 2014-10-24 add
        private WoaService woaService = new WoaService();
        private PrintFileNameWoaService printFileNameWoaService = new PrintFileNameWoaService();
        private SummonsService summonsService = new SummonsService();
        private NoticeSummonsService noticeSummonsService = new NoticeSummonsService();
        private NoticeService noticeService = new NoticeService();
        private SumChargeService sumChargeService = new SumChargeService();
        private ChargeService chargeService = new ChargeService();
        private EvidencePackService evidencePackService = new EvidencePackService();
        private CourtDatesForWarrantService cdService = new CourtDatesForWarrantService();
        private OriginGroupService ogservice = new OriginGroupService();

        public List<WoaAuthorisationEntity> GetPagedPrintFileName(string woaNumber,
            int crtIntNo, int crtRIntNo, int cdIntNo, int? autIntNo, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            try
            {
                DateTime? courtDate = null;
                CourtDates cd = new CourtDatesService().GetByCdIntNo(cdIntNo);
                if (cd != null)
                {
                    courtDate = cd.Cdate;
                }

                List<WoaAuthorisationEntity> returnList = new List<WoaAuthorisationEntity>();
                //List<WoaPrintFileNameEntity> returnList = new List<WoaPrintFileNameEntity>();
                //Jerry 2014-10-24 change
                //using (IDataReader reader = new WoaService().GetPagedWoaForAuthorisation(autIntNo, crtIntNo, crtRIntNo, courtDate, woaNumber, pageSize, pageIndex))
                using (IDataReader reader = woaService.GetPagedWoaForAuthorisation(autIntNo, crtIntNo, crtRIntNo, courtDate, woaNumber, pageSize, pageIndex))
                {
                    while (reader.Read())
                    {

                        //returnList.Add(new WoaPrintFileNameEntity()
                        //{
                        //    PFNIntNo = Convert.ToInt32(reader["PFNIntNo"]),
                        //    PrintFileName = reader["PrintFileName"].ToString(),
                        //    NoOfWarrants = Convert.ToInt32(reader["NoOfWarrants"])
                        //});
                        returnList.Add(new WoaAuthorisationEntity()
                        {
                            CaseNumber = reader["SumCaseNo"] is DBNull ? "" : reader["SumCaseNo"].ToString(),
                            WoaIntNo = Convert.ToInt32(reader["WoaIntNo"].ToString()),
                            WoaNumber = reader["WoaNumber"].ToString(),
                            Status = Convert.ToInt32(reader["NoticeStatus"]),
                            SendToCourt = reader["WOASentToCourtDate"] is DBNull ? "" : Convert.ToString(reader["WOASentToCourtDate"]),
                            ReturnFromCourt = reader["WOAReturnedFromCourtDate"] is DBNull ? "" : Convert.ToString(reader["WOAReturnedFromCourtDate"]),
                            RejReason = reader["WoaRejectedReason"] is DBNull ? "" : Convert.ToString(reader["WoaRejectedReason"]),
                            NotTicketNo = reader["NotTicketNo"].ToString(),
                            SummonsNo = reader["SummonsNo"].ToString(),
                            AccFullName = reader["AccFullName"] is DBNull ? "" : reader["AccFullName"].ToString()
                        });
                    }
                    if (reader.NextResult())
                    {
                        while (reader.Read()) { totalCount = Convert.ToInt32(reader["TotalRowCount"]); break; }
                    }
                }

                return returnList;
            }
            catch (Exception ex) { throw ex; }


        }

        public List<WoaPrintFileNameEntity> GetWoaSendToCourt(string woaNumber, int? autIntNo, int crtIntNo, int crtRIntNo, DateTime? date, int pageIndex, int pageSize, out int totalCount)
        {
            List<WoaPrintFileNameEntity> list = new List<WoaPrintFileNameEntity>();
            totalCount = 0;
            //using (IDataReader reader = new WoaService().GetPagedWoaSendToCourt(autIntNo, crtIntNo, crtRIntNo, date, woaNumber, pageSize, pageIndex))
            using (IDataReader reader = woaService.GetPagedWoaSendToCourt(autIntNo, crtIntNo, crtRIntNo, date, woaNumber, pageSize, pageIndex))
            {
                while (reader.Read())
                {
                    list.Add(new WoaPrintFileNameEntity()
                    {
                        PFNIntNo = Convert.ToInt32(reader["PFNIntNo"]),
                        PrintFileName = reader["PrintFileName"].ToString(),
                        NoOfWarrants = Convert.ToInt32(reader["NoOfWarrants"])
                    });
                }
                if (reader.NextResult())
                {
                    while (reader.Read()) { totalCount = Convert.ToInt32(reader["TotalRowCount"]); break; }
                }
            }

            return list;
        }

        public List<VersionValidationEntity> GetVersionValidateEntities(List<WoaAuthorisationEntity> walist)
        {
            List<string> woaIds = new List<string>();
            foreach (WoaAuthorisationEntity e in walist)
            {
                woaIds.Add(e.WoaIntNo.ToString());
            }

            string ids = string.Join("~", woaIds);

            List<VersionValidationEntity> list = new List<VersionValidationEntity>();

            //Jerry 2014-10-24 change
            //using (IDataReader reader = new WoaService().GetRowVersion(ids))
            using (IDataReader reader = woaService.GetRowVersion(ids))
            {
                while (reader.Read())
                {
                    list.Add(new VersionValidationEntity()
                    {
                        WoaIntNo = Convert.ToInt32(reader["WoaIntNo"]),
                        NotIntNo = Convert.ToInt32(reader["NotIntNo"]),
                        SumIntNo = Convert.ToInt32(reader["SumIntNo"]),
                        WoaVersion = (byte[])reader["WoaVersion"],
                        NoticeVersion = (byte[])reader["NoticeVersion"],
                        SummonsVersion = (byte[])reader["SummonsVersion"],
                        WoaChargeStatus = reader["WoaChargeStatus"] is DBNull ? 0 : Convert.ToInt32(reader["WoaChargeStatus"]),
                        NoticeStatus = Convert.ToInt32(reader["NoticeStatus"]),
                        SummonsStatus = Convert.ToInt32(reader["SummonsStatus"])
                    });
                }

                if (reader.NextResult())
                {
                    int woaIntNo = 0;
                    while (reader.Read())
                    {
                        woaIntNo = Convert.ToInt32(reader["WoaIntNo"]);

                        VersionValidationEntity v = list.Where(s => s.WoaIntNo.Equals(woaIntNo)).FirstOrDefault();
                        if (v != null)
                        {
                            if (v.ChargeVersions == null) v.ChargeVersions = new Dictionary<int, byte[]>();
                            if (v.SumChargeVersions == null) v.SumChargeVersions = new Dictionary<int, byte[]>();
                            // Jake 2013-09-09 added check key not exists in dictionary
                            if (!v.ChargeVersions.ContainsKey(Convert.ToInt32(reader["ChgIntNo"])))
                                v.ChargeVersions.Add(Convert.ToInt32(reader["ChgIntNo"]), (byte[])reader["ChargeVersion"]);
                            if (!v.SumChargeVersions.ContainsKey(Convert.ToInt32(reader["SchIntNo"])))
                                v.SumChargeVersions.Add(Convert.ToInt32(reader["SchIntNo"]), (byte[])reader["SumChargeVersion"]);
                        }


                    }
                }
            }

            return list;
        }

        public List<WoaAuthorisationEntity> GetByPrintFileID(string woaNumber, int? pfnIntNo, int authorisizeType)
        {
            List<WoaAuthorisationEntity> returnList = new List<WoaAuthorisationEntity>();

            TList<PrintFileNameWoa> pwList = printFileNameWoaService.GetByPfnIntNo(pfnIntNo.Value);

            var q = from r in pwList select r.WoaIntNo.ToString();

            WoaQuery wq = new WoaQuery();
            wq.AppendIn(WoaColumn.WoaIntNo, q.ToArray());
            if (!string.IsNullOrEmpty(woaNumber)) wq.Append(WoaColumn.WoaNumber, woaNumber);
            wq.AppendIsNull(WoaColumn.WoaRejectedReason);
            if (authorisizeType == 1) // sent to court
            {
                wq.AppendIsNull(WoaColumn.WoaSentToCourtDate);
            }
            else
            {
                wq.AppendIsNotNull(WoaColumn.WoaSentToCourtDate);
                wq.AppendIsNull(WoaColumn.WoaReturnedFromCourtDate);
            }
            //Jerry 2014-10-24 change
            //TList<Woa> woaList = new WoaService().Find(wq as IFilterParameterCollection);
            TList<Woa> woaList = woaService.Find(wq as IFilterParameterCollection);

            //new WoaService().DeepLoad(woaList, false, DeepLoadType.IncludeChildren, new Type[] { typeof(Summons) });
            woaService.DeepLoad(woaList, false, DeepLoadType.IncludeChildren, new Type[] { typeof(Summons) });
            //if (woaList != null && woaList.Count > 0)
            //{
            //    new WoaService().DeepLoad(woaList as TList<Woa>, true);
            //}
            foreach (Woa w in woaList)
            {
                if (w.SumIntNoSource != null)
                {
                    returnList.Add(new WoaAuthorisationEntity()
                    {
                        CaseNumber = w.SumIntNoSource.SumCaseNo,
                        WoaIntNo = w.WoaIntNo,
                        WoaNumber = w.WoaNumber,
                        ReturnFromCourt = w.WoaReturnedFromCourtDate.HasValue ? w.WoaReturnedFromCourtDate.Value.ToString("yyyy-MM-dd") : "",
                        SendToCourt = w.WoaSentToCourtDate.HasValue ? w.WoaSentToCourtDate.Value.ToString("yyyy-MM-dd") : ""
                    });
                }
            }

            return returnList;
        }

        public int WoaAuthorize(string[] woaIntNo, int crtIntNo, DateTime date, int authorizeType, string lastUser, ref string errorMsg)
        {
            try
            {

                WoaQuery woaQuery = new WoaQuery();
                woaQuery.AppendIn(WoaColumn.WoaIntNo, woaIntNo);
                //Woa woa = new WoaService().GetByWoaIntNo(woaIntNo);
                //Jerry 2014-04-01 check WOACancel != true
                //TList<Woa> woas = new WoaService().Find(woaQuery as IFilterParameterCollection);
                //Allow  IsCurrent = 0, so here doesn't need the check for IsCurrent.

                //Jerry 2014-10-24 change
                //List<Woa> woas = new WoaService().Find(woaQuery as IFilterParameterCollection).Where(w => w.WoaCancel != true).ToList();
                woaQuery.AppendNotEquals(WoaColumn.WoaCancel, "true");
                List<Woa> woas = woaService.Find(woaQuery as IFilterParameterCollection).ToList();
                foreach (Woa woa in woas)
                {
                    using (ConnectionScope.CreateTransaction())
                    {
                        int status = 0;
                        Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);
                        NoticeSummons ns = noticeSummonsService.GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                        Notice notice = null;
                        if (ns != null)
                        {
                            notice = noticeService.GetByNotIntNo(ns.NotIntNo);
                            //Jake 2013-11-12 added 
                            if (authorizeType == 1)
                            {
                                if (notice.NoticeStatus != (int)ChargeStatusList.WarrantPrinted)
                                {
                                    return -4;
                                }
                            }
                            else
                            {
                                if (notice.NoticeStatus != (int)ChargeStatusList.WarrantDeliveredToCourt)
                                {
                                    return -4;
                                }
                            }
                        }
                        else
                        {
                            return -3;
                        }
                        if (authorizeType == 1)
                        {
                            if (summons.CrtIntNo != crtIntNo)
                            {
                                return -1;
                            }
                            woa.WoaSentToCourtDate = date;
                            status = (int)ChargeStatusList.WarrantDeliveredToCourt;
                            woa.WoaChargePrevStatus = woa.WoaChargeStatus;
                        }
                        else
                        {
                            woa.WoaReturnedFromCourtDate = date;
                            status = (int)ChargeStatusList.ReturnedFromCourtAuthorised;
                            woa.WoaAuthorised = "Y";
                        }
                        woa.WoaChargePrevStatus = woa.WoaChargeStatus;
                        woa.WoaChargeStatus = status;
                        woa.LastUser = lastUser;
                        //Jerry 2014-10-24 change
                        //new WoaService().Save(woa);
                        woaService.Save(woa);

                        if ((authorizeType == 1 && summons.SummonsStatus != (int)ChargeStatusList.WarrantDeliveredToCourt)
                                || (authorizeType == 2 && summons.SummonsStatus != (int)ChargeStatusList.ReturnedFromCourtAuthorised))
                        {
                            summons.SumPrevSummonsStatus = summons.SummonsStatus;
                            summons.SummonsStatus = status;
                            summons.LastUser = lastUser;
                            summonsService.Save(summons);
                        }
                        TList<SumCharge> sumCharges = sumChargeService.GetBySumIntNo(summons.SumIntNo);

                        foreach (SumCharge s in sumCharges)
                        {
                            if ((authorizeType == 1 && s.SumChargeStatus != (int)ChargeStatusList.WarrantDeliveredToCourt)
                                || (authorizeType == 2 && s.SumChargeStatus != (int)ChargeStatusList.ReturnedFromCourtAuthorised))
                            {
                                s.SchPrevSumChargeStatus = s.SumChargeStatus;
                                s.SumChargeStatus = status;
                                s.LastUser = lastUser;
                            }
                        }
                        sumChargeService.Save(sumCharges);

                        if (ns != null)
                        {
                            int chgIntNo = 0;
                            if (notice != null)
                            {

                                notice.NotPrevNoticeStatus = notice.NoticeStatus;
                                notice.NoticeStatus = status;
                                notice.LastUser = lastUser;

                                noticeService.Save(notice);

                                TList<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo);
                                foreach (Charge c in charges.OrderBy(c => c.ChgSequence))
                                {
                                    if (c.ChargeStatus < 900)
                                    {
                                        c.PreviousStatus = c.ChargeStatus;
                                        c.ChargeStatus = status;
                                        c.LastUser = lastUser;
                                        if (c.ChgIsMain && chgIntNo == 0)
                                            chgIntNo = c.ChgIntNo;
                                    }
                                }

                                chargeService.Save(charges);

                                //Jake 2014-08-14 modified .set EpitemDate to getdate(), ref->bontq client ref #1359 
                                // Jake 2013-10-28 modified,  set EpitemDate to return from court date .
                                EvidencePack ep = new EvidencePack();
                                ep.ChgIntNo = chgIntNo;
                                ep.EpItemDate = DateTime.Now;// date;
                                ep.EpItemDescr = authorizeType == 1 ? String.Format("WOA sent to court at {0}", date.ToString("yyyy-MM-dd")) : String.Format("WOA returned from court at {0}", date.ToString("yyyy-MM-dd"));
                                ep.EpSourceTable = "WOA";
                                ep.EpSourceId = woa.WoaIntNo;
                                ep.LastUser = lastUser;
                                ep.EpTransactionDate = DateTime.Now;
                                ep.EpItemDateUpdated = false;
                                ep.EpatIntNo = (int)EpActionTypeList.Others; //Jerry 2013-10-15 add
                                evidencePackService.Insert(ep);

                                ConnectionScope.Complete();

                            }
                        }
                        else
                        {
                            return -3;
                        }
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return -2;
            }
            //return 0;
        }

        public bool RejectWoa(int woaIntNo, DateTime rejDate, string rejreason, string lastUser, ref string errorMessage)
        {
            try
            {
                using (ConnectionScope.CreateTransaction())
                {
                    //Jerry 2014-10-24 change
                    //Woa woa = new WoaService().GetByWoaIntNo(woaIntNo);
                    Woa woa = woaService.GetByWoaIntNo(woaIntNo);

                    //Jerry 2014-04-01 check WOACancel != true
                    //if (woa != null)
                    if (woa != null && (woa.WoaCancel == null || !woa.WoaCancel.Value))
                    {
                        int status = 0;
                        Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);


                        status = (int)ChargeStatusList.ReturnedFromCourtRejected;


                        woa.LastUser = lastUser;
                        woa.WoaRejectedReason = rejreason;
                        woa.WoaReturnedFromCourtDate = rejDate;
                        woa.WoaChargeStatus = status;

                        //Jerry 2014-10-24 change
                        //new WoaService().Save(woa);
                       woaService.Save(woa);

                        summons.SumPrevSummonsStatus = summons.SummonsStatus;
                        summons.SummonsStatus = status;
                        summons.LastUser = lastUser;
                        summonsService.Save(summons);

                        TList<SumCharge> sumCharges = sumChargeService.GetBySumIntNo(summons.SumIntNo);

                        foreach (SumCharge s in sumCharges)
                        {
                            if (s.SumChargeStatus < 900)
                            {
                                s.SchPrevSumChargeStatus = s.SumChargeStatus;
                                s.SumChargeStatus = status;
                                s.LastUser = lastUser;
                            }


                        }
                        sumChargeService.Save(sumCharges);

                        NoticeSummons ns = noticeSummonsService.GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                        if (ns != null)
                        {
                            Notice notice = noticeService.GetByNotIntNo(ns.NotIntNo);
                            if (notice != null)
                            {
                                notice.NotPrevNoticeStatus = notice.NoticeStatus;
                                notice.NoticeStatus = status;
                                notice.LastUser = lastUser;

                                noticeService.Save(notice);

                                TList<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo);

                                TList<EvidencePack> epList = new TList<EvidencePack>();
                                bool evdencePackAdded = false;
                                foreach (Charge c in charges.OrderBy(c => c.ChgSequence))
                                {
                                    if (c.ChargeStatus < 900)
                                    {
                                        c.PreviousStatus = c.ChargeStatus;
                                        c.ChargeStatus = status;
                                        c.LastUser = lastUser;
                                    }
                                    if (c.ChgIsMain && evdencePackAdded == false)
                                    {
                                        epList.Add(new EvidencePack()
                                        {
                                            ChgIntNo = c.ChgIntNo,
                                            EpItemDescr = "WOA rejected: " + rejreason,
                                            EpItemDate = DateTime.Now,
                                            EpSourceId = woaIntNo,
                                            EpSourceTable = "WOA",
                                            EpTransactionDate = DateTime.Now,
                                            EpItemDateUpdated = true,
                                            EpatIntNo = (int)EpActionTypeList.Others,//Jerry 2013-10-15 add
                                            LastUser = lastUser
                                        });
                                        evdencePackAdded = true;
                                    }
                                }
                                chargeService.Save(charges);
                                evidencePackService.Insert(epList);

                                ConnectionScope.Complete();

                                return true;
                            }
                        }


                    }

                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.ToString();
                return false;
            }
            return false;
        }

        public int GetCourtByWoaIntNo(int woaIntNo)
        {
            //Jerry 2014-10-24 change
            //Woa woa = new WoaService().GetByWoaIntNo(woaIntNo);
            Woa woa = woaService.GetByWoaIntNo(woaIntNo);

            if (woa != null)
            {
                Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);
                if (summons != null)
                    return summons.CrtIntNo;
            }
            return 0;
        }

        public bool CheckRowVersion(VersionValidationEntity e, out string errorMessage)
        {
            //Jerry 2014-10-24 change
            //Woa woa = new WoaService().GetByWoaIntNo(e.WoaIntNo);
            Woa woa = woaService.GetByWoaIntNo(e.WoaIntNo);

            if (woa.WoaChargeStatus != e.WoaChargeStatus && e.WoaChargeStatus > 0)
            {
                errorMessage = String.Format("Woa status has been changed and correct status should be {0}, WoaNumber: {1}", e.WoaChargeStatus, woa.WoaNumber);
                return false;
            }
            else if (GetRowVersionValue(e.WoaVersion) != GetRowVersionValue(woa.RowVersion))
            {
                errorMessage = String.Format("Unable to update WOA as the data has been updated by another user. Please refresh the screen and try again. Woa Number: {0}", woa.WoaNumber);
                return false;
            }

            Summons summons = summonsService.GetBySumIntNo(e.SumIntNo);
            if (summons.SummonsStatus != e.SummonsStatus && e.SummonsStatus > 0)
            {
                errorMessage = String.Format("Summons status has been changed and correct status should be {0}, SummonsNo: {1}", e.SummonsStatus, summons.SummonsNo);
                return false;
            }
            else if (GetRowVersionValue(e.SummonsVersion) != GetRowVersionValue(summons.RowVersion))
            {
                errorMessage = String.Format("Unable to update Summons as the data has been updated by another user. Please refresh the screen and try again. Summons Number: {0}", summons.SummonsNo);
                return false;
            }

            Notice notice = noticeService.GetByNotIntNo(e.NotIntNo);
            if (notice.NoticeStatus != e.NoticeStatus && e.NoticeStatus > 0)
            {
                errorMessage = String.Format("Notice status has been changed and correct status should be {0}, Notice Number: {1}", e.NoticeStatus, notice.NotTicketNo);
                return false;
            }
            else if (GetRowVersionValue(e.NoticeVersion) != GetRowVersionValue(notice.RowVersion))
            {
                errorMessage = String.Format("Unable to update Notice as the data has been updated by another user. Please refresh the screen and try again. Notice Number: {0}", notice.NotTicketNo);
                return false;
            }

            if (e.ChargeVersions != null)
            {
                List<string> chgList = new List<string>();
                foreach (int chgIntNo in e.ChargeVersions.Keys)
                {
                    chgList.Add(chgIntNo.ToString());
                }

                ChargeQuery cq = new ChargeQuery();
                cq.AppendIn(ChargeColumn.ChgIntNo, chgList.ToArray());
                TList<Charge> charges = chargeService.Find(cq as IFilterParameterCollection);

                foreach (Charge chg in charges)
                {
                    if (GetRowVersionValue(e.ChargeVersions[chg.ChgIntNo]) != GetRowVersionValue(chg.RowVersion))
                    {
                        errorMessage = String.Format("Unable to update Charge as the data has been updated by another user. Please refresh the screen and try again");
                        return false;
                    }
                }
            }

            if (e.SumChargeVersions != null)
            {
                List<string> schList = new List<string>();
                foreach (int schIntNo in e.SumChargeVersions.Keys)
                {
                    schList.Add(schIntNo.ToString());
                }

                SumChargeQuery scq = new SumChargeQuery();
                scq.AppendIn(SumChargeColumn.SchIntNo, schList.ToArray());
                TList<SumCharge> sumCharges = sumChargeService.Find(scq as IFilterParameterCollection);

                foreach (SumCharge chg in sumCharges)
                {
                    if (GetRowVersionValue(e.SumChargeVersions[chg.SchIntNo]) != GetRowVersionValue(chg.RowVersion))
                    {
                        errorMessage = String.Format("Unable to update SumCharge as the data has been updated by another user. Please refresh the screen and try again");
                        return false;
                    }
                }
            }

            errorMessage = string.Empty;

            return true;
        }

        public int ReverseWoa(int woaIntNo, int authorizeType, string lastUser, ref string errorMsg)
        {
            try
            {
                //Jerry 2014-10-24 change
                //Woa woa = new WoaService().GetByWoaIntNo(woaIntNo);
                Woa woa = woaService.GetByWoaIntNo(woaIntNo);

                using (ConnectionScope.CreateTransaction())
                {
                    int status = 0;
                    Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);
                    if (authorizeType == 1)
                    {
                        woa.WoaSentToCourtDate = null;
                        //status = (int)ChargeStatusList.WarrantDeliveredToCourt;
                    }
                    else if (authorizeType == 2)
                    {
                        woa.WoaReturnedFromCourtDate = null;
                        //status = (int)ChargeStatusList.ReturnedFromCourtAuthorised;
                        woa.WoaAuthorised = "N";
                    }
                    else if (authorizeType == 3)
                    {
                        woa.WoaRejectedReason = null;
                        woa.WoaReturnedFromCourtDate = null;
                    }
                    woa.LastUser = lastUser;
                    woa.WoaChargeStatus = woa.WoaChargePrevStatus;
                    //Jerry 2014-10-24 change
                    //new WoaService().Save(woa);
                    woaService.Save(woa);

                    summons.SummonsStatus = summons.SumPrevSummonsStatus.Value;
                    summons.LastUser = lastUser;
                    summonsService.Save(summons);

                    TList<SumCharge> sumCharges = sumChargeService.GetBySumIntNo(summons.SumIntNo);

                    foreach (SumCharge s in sumCharges)
                    {
                        if (s.SumChargeStatus < 900)
                        {
                            s.SumChargeStatus = s.SchPrevSumChargeStatus.Value;
                            s.LastUser = lastUser;
                        }
                    }
                    sumChargeService.Save(sumCharges);


                    NoticeSummons ns = noticeSummonsService.GetBySumIntNo(summons.SumIntNo).FirstOrDefault();
                    if (ns != null)
                    {
                        int chgIntNo = 0;
                        Notice notice = noticeService.GetByNotIntNo(ns.NotIntNo);
                        if (notice != null)
                        {
                            notice.NoticeStatus = notice.NotPrevNoticeStatus.Value;
                            notice.LastUser = lastUser;

                            noticeService.Save(notice);

                            TList<Charge> charges = chargeService.GetByNotIntNo(notice.NotIntNo);
                            foreach (Charge c in charges.OrderBy(c => c.ChgSequence))
                            {
                                if (c.ChargeStatus < 900)
                                {
                                    c.ChargeStatus = c.PreviousStatus.Value;
                                    c.LastUser = lastUser;
                                    if (c.ChgIsMain && chgIntNo == 0)
                                        chgIntNo = c.ChgIntNo;
                                }
                            }

                            chargeService.Save(charges);

                            EvidencePack ep = new EvidencePack();
                            ep.ChgIntNo = chgIntNo;
                            ep.EpItemDate = DateTime.Now;
                            if (authorizeType == 1)
                            {
                                ep.EpItemDescr = String.Format("WOA sent to court reversed at {0}", DateTime.Now.ToString("yyyy-MM-dd"));
                            }
                            else if (authorizeType == 2)
                            {
                                ep.EpItemDescr = String.Format("WOA return from court reversed at {0}", DateTime.Now.ToString("yyyy-MM-dd"));
                            }
                            else if (authorizeType == 3)
                            {
                                ep.EpItemDescr = String.Format("WOA rejected reversed at {0}", DateTime.Now.ToString("yyyy-MM-dd"));
                            }

                            ep.EpSourceTable = "WOA";
                            ep.EpSourceId = woa.WoaIntNo;
                            ep.LastUser = lastUser;
                            ep.EpTransactionDate = DateTime.Now;
                            ep.EpItemDateUpdated = false;
                            ep.EpatIntNo = (int)EpActionTypeList.Others;//Jerry 2013-10-15 add
                            evidencePackService.Insert(ep);

                            ConnectionScope.Complete();

                        }
                    }
                    else
                    {
                        return -3;
                    }
                }

                return 1;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return -2;
            }
        }

        long GetRowVersionValue(byte[] rowVersion)
        {
            return Convert.ToInt64(BitConverter.ToString(rowVersion).Replace("-", string.Empty), 16);
        }

        //Jake 2013-11-15 modified this function to use custom sql interface to access database instead of nettier interface
        public int ProcessSendToCourt(int pfnIntNo, DateTime date, string lastUser, out string errMsg)
        {
            int returnValue = 0;
            errMsg = "";
            SqlConnection conn = new SqlConnection(this.connectionString);
            SqlParameter[] paras = new SqlParameter[] { 
                new SqlParameter("@PFNIntNO",SqlDbType.Int),
                new SqlParameter("@SendToCourtDate",SqlDbType.SmallDateTime),
                new SqlParameter("@LastUser",SqlDbType.NVarChar,50)

            };
            paras[0].Value = pfnIntNo;
            paras[1].Value = date;
            paras[2].Value = lastUser;

            SqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddRange(paras);
            cmd.CommandText = "SILCustom_WOA_SendToCourt";
            cmd.CommandTimeout = (int)new TimeSpan(1, 0, 0).TotalMilliseconds;
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                returnValue = (int)cmd.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                returnValue = -6;
                errMsg = ex.Message;
            }
            finally
            {
                conn.Close();
            }
            //using (IDataReader reader = new WoaService().SendToCourt(pfnIntNo, date, lastUser))
            //{
            //    while (reader.Read())
            //    {
            //        returnValue = reader.GetInt32(0);
            //        break;
            //    }
            //}
            return returnValue;
        }

        public List<WoaAuthorisationEntity> GetWOAByPfnIntNo(int pfnIntNo, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            List<WoaAuthorisationEntity> list = new List<WoaAuthorisationEntity>();
            //Jerry 2014-10-24 change
            //using (IDataReader reader = new WoaService().GetPagedWoaByPFNIntNo(pfnIntNo, pageSize, pageIndex))
            using (IDataReader reader = woaService.GetPagedWoaByPFNIntNo(pfnIntNo, pageSize, pageIndex))
            {
                while (reader.Read())
                {
                    list.Add(new WoaAuthorisationEntity()
                    {
                        WoaIntNo = Convert.ToInt32(reader["WoaIntNo"]),
                        WoaNumber = reader["WoaNumber"].ToString(),
                        SummonsNo = reader["SummonsNo"].ToString(),
                        AccFullName = reader["AccFullName"] == null ? "" : reader["AccFullName"].ToString()
                    });
                }

                if (reader.NextResult())
                {
                    while (reader.Read()) { totalCount = Convert.ToInt32(reader["TotalRowCount"]); break; }
                }
            }

            return list;
        }
        //2014-07-18 Heidi changed for fixing the problem is hint information error(5321).
        public int CheckCourtDate(string newCourtDate, string woaNumber, string lastUser, out string errormsg)
        {
            //Jerry 2014-04-01 check WOACancel != true and IsCurrent == true
            //Woa woa = new WoaService().GetByWoaNumber(woaNumber).FirstOrDefault();
            errormsg = "";
            try
            {
                //Jerry 2014-10-24 change
               // Woa woa = new WoaService().GetByWoaNumber(woaNumber).Where(w => w.IsCurrent == true && w.WoaCancel != true).FirstOrDefault();
                Woa woa = woaService.GetByWoaNumber(woaNumber).Where(w => w.IsCurrent == true && w.WoaCancel != true).FirstOrDefault();

                if (woa != null)//woa != null && !String.IsNullOrEmpty(newCourtDate)
                {
                    if (!String.IsNullOrEmpty(newCourtDate))
                    {
                        //Jake 2014-08-12 added to check new court date is a validate date (not weekend and public holiday)

                        DateTime cDate = Convert.ToDateTime(newCourtDate);

                        Stalberg.TMS.PublicHolidaysDB publicHolidayDb = new Stalberg.TMS.PublicHolidaysDB(this.connectionString);
                        string returnValue = publicHolidayDb.CheckWeekDayOrPublicHoliday(cDate);
                        if (returnValue == "Y")
                        {
                            return -5;
                        }
                        else if (returnValue == "P")
                        {
                            return -6;
                        }

                        Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);

                        //CourtDatesForWarrantService cdService = new CourtDatesForWarrantService();
                        SIL.AARTO.DAL.Entities.CourtDatesForWarrant courtDate = cdService.GetByCdateWarCrtRintNo(Convert.ToDateTime(newCourtDate), summons.CrtRintNo.Value);
                        if (courtDate == null)
                        {
                            //OriginGroupService ogservice = new OriginGroupService();
                            OriginGroup og = ogservice.GetByOgCode("ALL");
                            int count = 0;
                            if (og == null) og = ogservice.GetPaged(0, 1, out count).FirstOrDefault();

                            courtDate = new DAL.Entities.CourtDatesForWarrant();
                            courtDate.AutIntNo = summons.AutIntNo;
                            courtDate.CdateLocked = true;
                            courtDate.CdateWar = Convert.ToDateTime(newCourtDate);
                            courtDate.CdNoOfCases = 0;
                            courtDate.CrtRintNo = summons.CrtRintNo.Value;
                            courtDate.OgIntNo = og.OgIntNo;
                            courtDate.LastUser = lastUser;
                            return cdService.Insert(courtDate) ? 1 : -1; //2014-07-18 Heidi changed for fixing the problem is hint information error(5321).
                        }
                        else
                        {
                            return 1;
                        }
                        //if (courtDate == null)
                        //{
                        //    return false;
                        //}
                        //if (courtDate.CdNoOfCases < courtDate.CdAllocated && courtDate.CdAllocated > 0)
                        //{
                        //    return false;
                        //}
                        //return false;
                    }
                    else
                    {
                        return -3;
                    }
                }
                else
                {
                    return -2;
                }
            }
            catch (Exception ex)
            {
                errormsg = ex.Message;
                return -4;
            }

            //return -1;
        }

        public bool CheckNoticeForWOAExecution(int woaIntNo)
        {
            try
            {
                //Jerry 2014-10-24 change
                //Woa woa = new WoaService().GetByWoaIntNo(woaIntNo);
                Woa woa = woaService.GetByWoaIntNo(woaIntNo);

                //Jerry 2014-04-01 check WOACancel != true
                //if (woa != null)
                if (woa != null && (woa.WoaCancel == null || !woa.WoaCancel.Value))
                {
                    Summons summons = summonsService.GetBySumIntNo(woa.SumIntNo);

                    NoticeSummons ns = noticeSummonsService.GetBySumIntNo(summons.SumIntNo).FirstOrDefault();

                    Notice notice = noticeService.GetByNotIntNo(ns.NotIntNo);

                    if (notice.NoticeStatus == 720)
                    {
                        return true;
                    }

                }
            }
            catch
            {
                throw;
            }

            return false;
        }
    }
}
