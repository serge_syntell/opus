﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.BLL.Utility.Cache;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.BLL.StreetCoding
{
    public class PlaceAtIntersectionManager
    {
        PlaceAtIntersectionService paiServ = new PlaceAtIntersectionService();
        StreetService stServ = new StreetService();
        LocationSuburbService lsServ = new LocationSuburbService();
        LocationSuburbStreetConjoinService lssSrv = new LocationSuburbStreetConjoinService();

        public List<PlaceAtIntersectionModel> GetAllPAI(int autIntNo, int loSuIntNo, int Str1IntNo, int Str2IntNo, int pageIndex, int pageSize, out int totalCount)
        {
            string where = String.Format("1=1");
            if (autIntNo > 0)
            {
                where = String.Format(" a.AutIntNo = {0}", autIntNo);
            }
            if (loSuIntNo > 0)
            {
                where += String.Format(" AND ls.LoSuIntNo = {0}", loSuIntNo);
            }
            if (Str1IntNo > 0)
            {
                where += String.Format(" AND s1.StrIntNo = {0}", Str1IntNo);
            }
            if (Str2IntNo > 0)
            {
                where += String.Format(" AND s2.StrIntNo = {0}", Str2IntNo);
            }

            return GetPagedPais(where, "PAIIntNo", pageIndex, pageSize, out totalCount);
        }

        public PlaceAtIntersectionModel GetPaiById(int paiId)
        {
            PlaceAtIntersection pai = paiServ.GetByPaiIntNo(paiId);
            LocationSuburbStreetConjoin lss1 = lssSrv.GetByLsscIntNo(pai.LsscIntNo1);
            LocationSuburbStreetConjoin lss2 = lssSrv.GetByLsscIntNo(pai.LsscIntNo2);
            LocationSuburb losu = lsServ.GetByLoSuIntNo(lss1.LoSuIntNo);
            PlaceAtIntersectionModel paiModel = new PlaceAtIntersectionModel();

            paiModel.PAIIntNo = pai.PaiIntNo;
            paiModel.AutIntNo = losu.AutIntNo;
            paiModel.LoSuIntNo = losu.LoSuIntNo;
            paiModel.CrtIntNo = pai.CrtIntNo;
            paiModel.PAIStreet1 = lss1.StrIntNo;
            paiModel.PAIStreet2 = lss2.StrIntNo;
            paiModel.PAIGPSXCoord = pai.PaigpsxCoord;
            paiModel.PAIGPSYCoord = pai.PaigpsyCoord;
            return paiModel;
        }

        private List<PlaceAtIntersectionModel> GetPagedPais(string strWhere, string strOrder, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
            SqlCommand command = con.CreateCommand();
            command.CommandText = "SILCustom_PlaceAtIntersection_GetByAutSubSt";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@WhereClause", SqlDbType.VarChar, 2000)).Value = strWhere;
            command.Parameters.Add(new SqlParameter("@OrderBy", SqlDbType.VarChar, 2000)).Value = strOrder;
            command.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int)).Value = pageIndex;
            command.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int)).Value = pageSize;

            DataSet dsReturn = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                da.Fill(dsReturn);
                List<PlaceAtIntersectionModel> pais = new List<PlaceAtIntersectionModel>();

                if (dsReturn != null && dsReturn.Tables.Count > 0)
                {
                    DataTable dt = dsReturn.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        PlaceAtIntersectionModel intersect = new PlaceAtIntersectionModel();
                        intersect.PAIIntNo = Convert.ToInt32(dt.Rows[i]["PAIIntNo"]);
                        intersect.AutIntNo = Convert.ToInt32(dt.Rows[i]["AutIntNo"]);
                        intersect.AutName = (dt.Rows[i]["AutName"] + "").ToString();
                        intersect.LoSuIntNo = Convert.ToInt32(dt.Rows[i]["LoSuIntNo"]);
                        intersect.LoSuDescr = (dt.Rows[i]["LoSuDescr"] + "").ToString();
                        intersect.CrtIntNo = Convert.ToInt32(dt.Rows[i]["CrtIntNo"]);
                        intersect.PAIStreet1 = Convert.ToInt32(dt.Rows[i]["Str1IntNo"]);
                        intersect.PAIStreet1Name = (dt.Rows[i]["Str1Name"] + "").ToString();
                        intersect.PAIStreet2 = Convert.ToInt32(dt.Rows[i]["Str2IntNo"]);
                        intersect.PAIStreet2Name = (dt.Rows[i]["Str2Name"] + "").ToString();
                        pais.Add(intersect);
                    }
                    totalCount = Convert.ToInt32(dsReturn.Tables[1].Rows[0]["TotalRowCount"]);
                }
                return pais;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int SavePAI(PlaceAtIntersectionModel paiModel, int st1Before, int st2Before)
        {
            int actResult = 0;

            try
            {
                LocationSuburbStreetConjoin lss1 = lssSrv.GetByLoSuIntNoStrIntNo(paiModel.LoSuIntNo, paiModel.PAIStreet1);
                LocationSuburbStreetConjoin lss2 = lssSrv.GetByLoSuIntNoStrIntNo(paiModel.LoSuIntNo, paiModel.PAIStreet2);
                if (st1Before == 0)
                {
                    PlaceAtIntersection paiExist = paiServ.GetByLsscIntNo1LsscIntNo2(lss1.LsscIntNo, lss2.LsscIntNo);
                    if (paiExist == null)
                    {
                        PlaceAtIntersection pai = new PlaceAtIntersection();
                        pai.LsscIntNo1 = lss1.LsscIntNo;
                        pai.LsscIntNo2 = lss2.LsscIntNo;
                        pai.PaigpsxCoord = paiModel.PAIGPSXCoord;
                        pai.PaigpsyCoord = paiModel.PAIGPSYCoord;
                        pai.CrtIntNo = paiModel.CrtIntNo;
                        pai.LastUser = paiModel.LastUser;
                        pai = paiServ.Save(pai);
                        actResult = 1;
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(paiModel.AutIntNo, paiModel.LastUser, PunchStatisticsTranTypeList.PlaceAtIntersectionMaintenance, PunchAction.Add);
                    }
                    else
                    {
                        actResult = 2;
                        return actResult;
                    }
                }
                else
                {
                    PlaceAtIntersection paiNew = paiServ.GetByPaiIntNo(paiModel.PAIIntNo);
                    if (st1Before != paiModel.PAIStreet1 || st2Before != paiModel.PAIStreet2)
                    {
                        PlaceAtIntersection paiExist = paiServ.GetByLsscIntNo1LsscIntNo2(lss1.LsscIntNo, lss2.LsscIntNo);
                        if (paiExist != null)
                        {
                            actResult = 2;
                            return actResult;
                        }
                    }
                    paiNew.LsscIntNo1 = lss1.LsscIntNo;
                    paiNew.LsscIntNo2 = lss2.LsscIntNo;
                    paiNew.PaigpsxCoord = paiModel.PAIGPSXCoord;
                    paiNew.PaigpsyCoord = paiModel.PAIGPSYCoord;
                    paiNew.CrtIntNo = paiModel.CrtIntNo;
                    paiNew.LastUser = paiModel.LastUser;
                    paiNew = paiServ.Save(paiNew);
                    actResult = 1;
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(paiModel.AutIntNo, paiModel.LastUser, PunchStatisticsTranTypeList.PlaceAtIntersectionMaintenance, PunchAction.Change);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actResult;
        }

        public int DeletePAIByID(int paiId)
        {
            int actResult = 0;
            try
            {
                PlaceAtIntersection pai = paiServ.GetByPaiIntNo(paiId);
                if (paiServ.Delete(pai)) actResult = 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actResult;
        }

    }
}
