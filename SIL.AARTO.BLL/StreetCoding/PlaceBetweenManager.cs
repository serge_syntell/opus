﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.BLL.Utility.Cache;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.BLL.StreetCoding
{
    public class PlaceBetweenManager
    {
        PlaceBetweenService pbServ = new PlaceBetweenService();
        StreetService stServ = new StreetService();
        LocationSuburbService lsServ = new LocationSuburbService();
        LocationSuburbStreetConjoinService lssSrv = new LocationSuburbStreetConjoinService();

        public List<PlaceBetweenModel> GetAllPB(int autIntNo, int loSuIntNo, int Str1IntNo, int Str2IntNo, int Str3IntNo, int pageIndex, int pageSize, out int totalCount)
        {
            string where = String.Format("1=1");
            if (autIntNo > 0)
            {
                where = String.Format(" a.AutIntNo = {0}", autIntNo);
            }
            if (loSuIntNo > 0)
            {
                where += String.Format(" AND ls.LoSuIntNo = {0}", loSuIntNo);
            }
            if (Str1IntNo > 0)
            {
                where += String.Format(" AND s1.StrIntNo = {0}", Str1IntNo);
            }
            if (Str2IntNo > 0)
            {
                where += String.Format(" AND s2.StrIntNo = {0}", Str2IntNo);
            }
            if (Str3IntNo > 0)
            {
                where += String.Format(" AND s3.StrIntNo = {0}", Str3IntNo);
            }

            return GetPagedPbs(where, "PBIntNo", pageIndex, pageSize, out totalCount);
        }

        private List<PlaceBetweenModel> GetPagedPbs(string strWhere, string strOrder, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
            SqlCommand command = con.CreateCommand();
            command.CommandText = "SILCustom_PlaceBetween_GetByAutSubSt";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@WhereClause", SqlDbType.VarChar, 2000)).Value = strWhere;
            command.Parameters.Add(new SqlParameter("@OrderBy", SqlDbType.VarChar, 2000)).Value = strOrder;
            command.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int)).Value = pageIndex;
            command.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int)).Value = pageSize;

            DataSet dsReturn = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                da.Fill(dsReturn);
                List<PlaceBetweenModel> pbs = new List<PlaceBetweenModel>();

                if (dsReturn != null && dsReturn.Tables.Count > 0)
                {
                    DataTable dt = dsReturn.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        PlaceBetweenModel between = new PlaceBetweenModel();
                        between.PBIntNo = Convert.ToInt32(dt.Rows[i]["PBIntNo"]);
                        between.AutIntNo = Convert.ToInt32(dt.Rows[i]["AutIntNo"]);
                        between.AutName = (dt.Rows[i]["AutName"] + "").ToString();
                        between.LoSuIntNo = Convert.ToInt32(dt.Rows[i]["LoSuIntNo"]);
                        between.LoSuDescr = (dt.Rows[i]["LoSuDescr"] + "").ToString();
                        between.CrtIntNo = Convert.ToInt32(dt.Rows[i]["CrtIntNo"]);
                        between.PBStreet1 = Convert.ToInt32(dt.Rows[i]["Str1IntNo"]);
                        between.PBStreet1Name = (dt.Rows[i]["Str1Name"] + "").ToString();
                        between.PBStreet2 = Convert.ToInt32(dt.Rows[i]["Str2IntNo"]);
                        between.PBStreet2Name = (dt.Rows[i]["Str2Name"] + "").ToString();
                        between.PBStreet3 = Convert.ToInt32(dt.Rows[i]["Str3IntNo"]);
                        between.PBStreet3Name = (dt.Rows[i]["Str3Name"] + "").ToString();
                        pbs.Add(between);
                    }
                    totalCount = Convert.ToInt32(dsReturn.Tables[1].Rows[0]["TotalRowCount"]);
                }
                return pbs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public PlaceBetweenModel GetPbById(int pbId)
        {
            PlaceBetween pb = pbServ.GetByPbIntNo(pbId);
            LocationSuburbStreetConjoin lss1 = lssSrv.GetByLsscIntNo(pb.LsscIntNo1);
            LocationSuburbStreetConjoin lss2 = lssSrv.GetByLsscIntNo(pb.LsscIntNo2);
            LocationSuburbStreetConjoin lss3 = lssSrv.GetByLsscIntNo(pb.LsscIntNo3);
            LocationSuburb losu = lsServ.GetByLoSuIntNo(lss1.LoSuIntNo);
            PlaceBetweenModel pbModel = new PlaceBetweenModel();

            pbModel.PBIntNo = pb.PbIntNo;
            pbModel.AutIntNo = losu.AutIntNo;
            pbModel.LoSuIntNo = losu.LoSuIntNo;
            pbModel.CrtIntNo = pb.CrtIntNo;
            pbModel.PBStreet1 = lss1.StrIntNo;
            pbModel.PBStreet2 = lss2.StrIntNo;
            pbModel.PBStreet3 = lss3.StrIntNo;
            pbModel.PBGPSXCoord = pb.PbgpsxCoord;
            pbModel.PBGPSYCoord = pb.PbgpsyCoord;
            return pbModel;
        }

        public int SavePB(PlaceBetweenModel pbModel, int st1Before, int st2Before, int st3Before)
        {
            int actResult = 0;

            try
            {
                LocationSuburbStreetConjoin lss1 = lssSrv.GetByLoSuIntNoStrIntNo(pbModel.LoSuIntNo, pbModel.PBStreet1);
                LocationSuburbStreetConjoin lss2 = lssSrv.GetByLoSuIntNoStrIntNo(pbModel.LoSuIntNo, pbModel.PBStreet2);
                LocationSuburbStreetConjoin lss3 = lssSrv.GetByLoSuIntNoStrIntNo(pbModel.LoSuIntNo, pbModel.PBStreet3);
                if (st1Before == 0)
                {
                    PlaceBetween pbExist = pbServ.GetByLsscIntNo1LsscIntNo2LsscIntNo3(lss1.LsscIntNo, lss2.LsscIntNo, lss3.LsscIntNo);
                    if (pbExist == null)
                    {
                        PlaceBetween pb = new PlaceBetween();
                        pb.LsscIntNo1 = lss1.LsscIntNo;
                        pb.LsscIntNo2 = lss2.LsscIntNo;
                        pb.LsscIntNo3 = lss3.LsscIntNo;
                        pb.PbgpsxCoord = pbModel.PBGPSXCoord;
                        pb.PbgpsyCoord = pbModel.PBGPSYCoord;
                        pb.CrtIntNo = pbModel.CrtIntNo;
                        pb.LastUser = pbModel.LastUser;
                        pb = pbServ.Save(pb);
                        actResult = 1;
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(pbModel.AutIntNo, pbModel.LastUser, PunchStatisticsTranTypeList.PlaceBetweenMaintenance, PunchAction.Add);
                    }
                    else
                    {
                        actResult = 2;
                        return actResult;
                    }
                }
                else
                {
                    PlaceBetween pbNew = pbServ.GetByPbIntNo(pbModel.PBIntNo);
                    if (st1Before != pbModel.PBStreet1 || st2Before != pbModel.PBStreet2 || st3Before != pbModel.PBStreet3)
                    {
                        PlaceBetween pbExist = pbServ.GetByLsscIntNo1LsscIntNo2LsscIntNo3(lss1.LsscIntNo, lss2.LsscIntNo, lss3.LsscIntNo);
                        if (pbExist != null)
                        {
                            actResult = 2;
                            return actResult;
                        }
                    }
                    pbNew.LsscIntNo1 = lss1.LsscIntNo;
                    pbNew.LsscIntNo2 = lss2.LsscIntNo;
                    pbNew.LsscIntNo3 = lss3.LsscIntNo;
                    pbNew.PbgpsxCoord = pbModel.PBGPSXCoord;
                    pbNew.PbgpsyCoord = pbModel.PBGPSYCoord;
                    pbNew.CrtIntNo = pbModel.CrtIntNo;
                    pbNew.LastUser = pbModel.LastUser;
                    pbNew = pbServ.Save(pbNew);
                    actResult = 1;
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(pbModel.AutIntNo, pbModel.LastUser, PunchStatisticsTranTypeList.PlaceBetweenMaintenance, PunchAction.Change);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actResult;
        }

        public int DeletePBByID(int pbId)
        {
            int actResult = 0;
            try
            {
                PlaceBetween pb = pbServ.GetByPbIntNo(pbId);
                if (pbServ.Delete(pb)) actResult = 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actResult;
        }
    }
}
