﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.BLL.Utility.Cache;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.BLL.StreetCoding
{
    public class LocationAreaCodeManager
    {
        LocationAreaCodeService lacServ = new LocationAreaCodeService();
        LocationSuburbLocationAreaCodeConjoinService lslacServ = new LocationSuburbLocationAreaCodeConjoinService();

        public TList<LocationAreaCode> GetAllAreaCode()
        {
            return lacServ.GetAll();
        }

        public TList<LocationAreaCode> GetAllAreaCode(string lacCode, int pageIndex, int pageSize, out int totalCount)
        {
            string where = String.Empty;
            if (!String.IsNullOrEmpty(lacCode))
            {
                where = String.Format(" LACCode like '%{0}%' Or LACDescr like '%{0}%'", lacCode);
            }
            else
            {
                where = String.Format("1=1");
            }
            return lacServ.GetPaged(where, "LACCode", pageIndex, pageSize, out totalCount);
        }

        public LocationAreaCode GetLocAreaCodeByLacCode(string lacCode)
        {
            return lacServ.GetByLacCode(lacCode);
        }

        public int DeleteLocAreaCodeByLacCode(string lacCode)
        {
            int actResult = 0;
            LocationAreaCode lac = lacServ.GetByLacCode(lacCode);
            LocationSuburbLocationAreaCodeConjoinService lslaServ = new LocationSuburbLocationAreaCodeConjoinService();
            TList<LocationSuburbLocationAreaCodeConjoin> lslacList = lslaServ.GetByLacCode(lacCode);
            if (lslacList.Count > 0)
            {
                actResult = 2;
            }
            else
            {
                if (lacServ.Delete(lac)) actResult = 1;
            }
            return actResult;
        }

        public int SaveLAC(LocationAreaCodeModel lacModel, string lacCodeBefore)
        {
            int actResult = 0;
            bool isNew = false;
            try
            {
                LocationAreaCode lac = lacServ.GetByLacCode(lacModel.LACCode);

                if (lacModel.LACCode != lacCodeBefore)
                {
                    if (string.IsNullOrEmpty(lacCodeBefore) && lac == null)
                    {
                        lac = new LocationAreaCode();
                        isNew = true;
                    }
                    else if (!string.IsNullOrEmpty(lacCodeBefore) && lac == null)
                    {
                        TList<LocationSuburbLocationAreaCodeConjoin> lslacs = lslacServ.GetByLacCode(lacCodeBefore);
                        if (lslacs.Count > 0)
                        {
                            actResult = 3;
                            return actResult;
                        }
                        lac = lacServ.GetByLacCode(lacCodeBefore);
                    }
                    else
                    {
                        actResult = 2;
                        return actResult;
                    }
                }

                lac.LacCode = lacModel.LACCode;
                lac.LacDescr = lacModel.LACDescr;
                lac.LastUser = lacModel.LastUser;
                lac = lacServ.Save(lac);
                if (lac != null) actResult = 1;
                if (actResult == 1)
                {
                    if (isNew)
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(lacModel.AutIntNo, lacModel.LastUser, PunchStatisticsTranTypeList.AreaCodeMaintenance, PunchAction.Add);  

                    }
                    else
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(lacModel.AutIntNo, lacModel.LastUser, PunchStatisticsTranTypeList.AreaCodeMaintenance, PunchAction.Change);  

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actResult;
        }

    }
}
