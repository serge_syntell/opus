﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using System.Collections;

namespace SIL.AARTO.BLL.StreetCoding.Model
{
    public class PlaceBetweenModel
    {
        public int PBIntNo { get; set; }

        [UIHint("StreetDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        [ValueNotEqual("PBStreet3", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        public int PBStreet1 { get; set; }
        public string PBStreet1Name { get; set; }

        [UIHint("StreetDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        [ValueNotEqual("PBStreet1", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        public int PBStreet2 { get; set; }
        public string PBStreet2Name { get; set; }

        [UIHint("StreetDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        [ValueNotEqual("PBStreet2", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        public int PBStreet3 { get; set; }
        public string PBStreet3Name { get; set; }

        [StringLength(20, ErrorMessageResourceName = "XCoord_MaxLen_20",
            ErrorMessageResourceType = typeof(PlaceBetweenManage_cshtml))]
        public string PBGPSXCoord { get; set; }
        [StringLength(20, ErrorMessageResourceName = "YCoord_MaxLen_20",
            ErrorMessageResourceType = typeof(PlaceBetweenManage_cshtml))]
        public string PBGPSYCoord { get; set; }

        [UIHint("AuthorityDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int AutIntNo { get; set; }
        public string AutName { get; set; }

        [UIHint("LocationSuburbDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int LoSuIntNo { get; set; }
        public string LoSuDescr { get; set; }

        [UIHint("CourtDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int CrtIntNo { get; set; }
        public string CrtName { get; set; }

        [UIHint("AuthorityDropdown")]
        public int SearchAutIntNo { get; set; }
        [UIHint("LocationSuburbDropdown")]
        public int SearchLoSuIntNo { get; set; }

        [UIHint("StreetDropdown")]
        public int SearchStreet1 { get; set; }
        [UIHint("StreetDropdown")]
        public int SearchStreet2 { get; set; }
        [UIHint("StreetDropdown")]
        public int SearchStreet3 { get; set; }

        public string LastUser { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        public List<PlaceBetweenModel> Betweens { get; set; }

    }
}
