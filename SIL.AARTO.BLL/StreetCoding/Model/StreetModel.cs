﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;

namespace SIL.AARTO.BLL.StreetCoding.Model
{
    public class StreetModel
    { 
        public int StrIntNo { get; set; }

        [Required(ErrorMessageResourceName = "StreetName_Is_Required",
            ErrorMessageResourceType = typeof(StreetManage_cshtml))]
        [StringLength(255, ErrorMessageResourceName = "StreetName_Is_Required",
            ErrorMessageResourceType = typeof(StreetManage_cshtml))]
        public string StrName { get; set; }
        public string LastUser { get; set; }

        [UIHint("LocationSuburbDropdown")]
        [Required(ErrorMessageResourceName = "Suburb_Is_Required",
            ErrorMessageResourceType = typeof(Global))]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessageResourceName = "Suburb_Is_Required",
    ErrorMessageResourceType = typeof(Global))]
        public int LoSuIntNo { get; set; }
        public string LoSuDescr { get; set; }

        [UIHint("AuthorityDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessageResourceName = "Aut_Is_Required",
    ErrorMessageResourceType = typeof(Global))]
        public int AutIntNo { get; set; }
        public string AutName { get; set; }

        [UIHint("AuthorityDropdown")]
        public int SearchAutIntNo { get; set; }
        [UIHint("LocationSuburbDropdown")]
        public int SearchSubIntNo { get; set; }
        public string SearchKey { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        public List<StreetModel> Streets { get; set; }
    }
}
