﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.StreetCoding;

namespace SIL.AARTO.BLL.StreetCoding.Model
{
    public class LocationSuburbModel
    {
        public int LoSuIntNo { get; set; }

        [Required(ErrorMessageResourceName = "Sub_Is_Required",
    ErrorMessageResourceType = typeof(LocationSuburbManage_cshtml))]
        [StringLength(100, ErrorMessageResourceName = "Sub_Is_Required",
            ErrorMessageResourceType = typeof(LocationSuburbManage_cshtml))]
        public string LoSuDescr { get; set; }

        [UIHint("AuthorityDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessageResourceName = "Aut_Is_Required",
    ErrorMessageResourceType = typeof(LocationSuburbManage_cshtml))]
        public int AutIntNo { get; set; }
        public string AutName { get; set; }
        public string LACCode { get; set; }

        [UIHint("AuthorityDropdown")]
        public int SearchAutIntNo { get; set; }
        public string SearchKey { get; set; }
        public string LastUser { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        public SelectList locAreaCodes { get; set; }
        public List<LocationSuburbModel> locSuburbs { get; set; }

    }
}
