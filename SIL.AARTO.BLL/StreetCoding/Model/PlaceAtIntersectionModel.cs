﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using System.Collections;

namespace SIL.AARTO.BLL.StreetCoding.Model
{
    public class PlaceAtIntersectionModel
    {
        public int PAIIntNo { get; set; }

        [UIHint("StreetDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        [ValueNotEqual("PAIStreet2", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        public int PAIStreet1 { get; set; }
        public string PAIStreet1Name { get; set; }

        [UIHint("StreetDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        [ValueNotEqual("PAIStreet1", ErrorMessageResourceName = "lblStCantSameErr_Text",
            ErrorMessageResourceType = typeof(Global))]
        public int PAIStreet2 { get; set; }
        public string PAIStreet2Name { get; set; }

        [StringLength(20, ErrorMessageResourceName = "XCoord_MaxLen_20",
            ErrorMessageResourceType = typeof(PlaceAtIntersectionManage_cshtml))]
        public string PAIGPSXCoord { get; set; }
        [StringLength(20, ErrorMessageResourceName = "YCoord_MaxLen_20",
            ErrorMessageResourceType = typeof(PlaceAtIntersectionManage_cshtml))]
        public string PAIGPSYCoord { get; set; }

        [UIHint("AuthorityDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int AutIntNo { get; set; }
        public string AutName { get; set; }

        [UIHint("LocationSuburbDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int LoSuIntNo { get; set; }
        public string LoSuDescr { get; set; }

        [UIHint("CourtDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int CrtIntNo { get; set; }
        public string CrtName { get; set; }
        
        [UIHint("AuthorityDropdown")]
        public int SearchAutIntNo { get; set; }
        [UIHint("LocationSuburbDropdown")]
        public int SearchLoSuIntNo { get; set; }

        [UIHint("StreetDropdown")]
        public int SearchStreet1 { get; set; }
        [UIHint("StreetDropdown")]
        public int SearchStreet2 { get; set; }

        public string LastUser { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        public List<PlaceAtIntersectionModel> Intersections { get; set; }

    }

    public class ValueNotEqual : ValidationAttribute, IClientValidatable
    {
        private readonly string testedPropertyName;
        public ValueNotEqual(string testedPropertyName, bool allowEqualDates = false)
        {
            this.testedPropertyName = testedPropertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var propertyTestedInfo = validationContext.ObjectType.GetProperty(this.testedPropertyName);
            if (propertyTestedInfo == null)
            {
                return new ValidationResult(string.Format("unknown property {0}", this.testedPropertyName));
            }

            var propertyTestedValue = propertyTestedInfo.GetValue(validationContext.ObjectInstance, null);

            if (value == null)
            {
                return ValidationResult.Success;
            }
            if (propertyTestedValue == null || !(propertyTestedValue is Int32))
            {
                return ValidationResult.Success;
            }

            // Compare values
            if ((Int32)value != (Int32)propertyTestedValue)
            {
                return ValidationResult.Success;
            }
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessageString,
                ValidationType = "valuenotequal"
            };
            rule.ValidationParameters["propertytested"] = this.testedPropertyName;
            yield return rule;
        }
    }
}