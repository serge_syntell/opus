﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.StreetCoding;
using System.Collections;

namespace SIL.AARTO.BLL.StreetCoding.Model
{
    public class PlaceAtFixedLocationModel
    {
        public int PAFLIntNo { get; set; }

        [UIHint("StreetDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        [ValueNotEqual("PBStreet3", ErrorMessage = "*")]
        public int PAFLStreet1 { get; set; }
        public string PAFLStreet1Name { get; set; }

        [Required(ErrorMessageResourceName = "Loc_MaxLen_255",
            ErrorMessageResourceType = typeof(PlaceAtFixedLocationManage_cshtml))]
        [StringLength(255, ErrorMessageResourceName = "Loc_MaxLen_255",
            ErrorMessageResourceType = typeof(PlaceAtFixedLocationManage_cshtml))]
        public string PAFLocation { get; set; }

        [StringLength(20, ErrorMessageResourceName = "XCoord_MaxLen_20",
            ErrorMessageResourceType = typeof(PlaceAtFixedLocationManage_cshtml))]
        public string PAFLGPSXCoord { get; set; }
        [StringLength(20, ErrorMessageResourceName = "YCoord_MaxLen_20",
            ErrorMessageResourceType = typeof(PlaceAtFixedLocationManage_cshtml))]
        public string PAFLGPSYCoord { get; set; }

        [UIHint("AuthorityDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int AutIntNo { get; set; }
        public string AutName { get; set; }

        [UIHint("LocationSuburbDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int LoSuIntNo { get; set; }
        public string LoSuDescr { get; set; }

        [UIHint("CourtDropdown")]
        [RegularExpression(@"^[1-9]\d*$", ErrorMessage = "*")]
        public int CrtIntNo { get; set; }
        public string CrtName { get; set; }

        [UIHint("AuthorityDropdown")]
        public int SearchAutIntNo { get; set; }
        [UIHint("LocationSuburbDropdown")]
        public int SearchLoSuIntNo { get; set; }

        [UIHint("StreetDropdown")]
        public int SearchStreet { get; set; }
        public string SearchKey { get; set; }

        public string LastUser { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        public List<PlaceAtFixedLocationModel> FixedLocations { get; set; }
    }
}
