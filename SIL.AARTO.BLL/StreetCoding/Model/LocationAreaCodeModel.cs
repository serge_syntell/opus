﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.Web.Resource.StreetCoding;

namespace SIL.AARTO.BLL.StreetCoding.Model
{
    public class LocationAreaCodeModel
    {
        [Required(ErrorMessageResourceName = "AreaCode_Is_Required",
            ErrorMessageResourceType = typeof(LocationAreaCodeManage_cshtml))]
        [StringLength(8, ErrorMessageResourceName = "AreaCode_Is_Required",
            ErrorMessageResourceType = typeof(LocationAreaCodeManage_cshtml))]
        [RegularExpression(@"^\d*$", ErrorMessageResourceName = "AreaCode_Is_Required",
            ErrorMessageResourceType = typeof(LocationAreaCodeManage_cshtml))]
        public string LACCode { get; set; }

        [Required(ErrorMessageResourceName = "AreaCodeDescr_Is_Required",
            ErrorMessageResourceType = typeof(LocationAreaCodeManage_cshtml))]
        [StringLength(100, ErrorMessageResourceName = "AreaCodeDescr_Is_Required",
            ErrorMessageResourceType = typeof(LocationAreaCodeManage_cshtml))]
        public string LACDescr { get; set; }

        public string LastUser { get; set; }
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        public int AutIntNo { get; set; }

        public string SearchKey { get; set; }
        public int TotalCount { get; set; }
        public string URLPara { get; set; }

        public TList<LocationAreaCode> locAreaCodes { get; set; }

        public LocationAreaCodeModel()
        {
            locAreaCodes = new TList<LocationAreaCode>();
        }
    }
}
