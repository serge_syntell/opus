﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.StreetCoding.Model
{
    public class SuburbAreaCode
    {
        public int SACIntNo { get; set; }
        public string AreaCode { get; set; }
        public int SubIntNo { get; set; }
        public string LastUser { get; set; }
    }
}
