﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.BLL.Utility.Cache;
using System.Data.SqlClient;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.BLL.StreetCoding
{
    public class StreetManager
    {
        StreetService stServ = new StreetService();
        LocationSuburbService lsServ = new LocationSuburbService();
        LocationSuburbStreetConjoinService lssServ = new LocationSuburbStreetConjoinService();

        public List<StreetModel> GetAllStreet(int autIntNo, int loSuIntNo, string stName, int pageIndex, int pageSize, out int totalCount)
        {
            string where = String.Format("1=1");
            if (autIntNo > 0)
            {
                where = String.Format(" ls.AutIntNo = {0}", autIntNo);
            }
            if (loSuIntNo > 0)
            {
                where += String.Format(" AND ls.LoSuIntNo = {0}", loSuIntNo);
            }
            if (!String.IsNullOrEmpty(stName))
            {
                where += String.Format(" AND s.StrName like '%{0}%'", stName);
            }

            return GetPagedStreets(where, "StrName", pageIndex, pageSize, out totalCount);
        }

        public StreetModel GetStreetByStId(int subId, int stId)
        {
            Street st = stServ.GetByStrIntNo(stId);
            LocationSuburb ls = lsServ.GetByLoSuIntNo(subId);

            StreetModel stModel = new StreetModel();
            stModel.AutIntNo = ls.AutIntNo;
            stModel.LoSuIntNo = ls.LoSuIntNo;
            stModel.StrName = st.StrName;

            return stModel;
        }

        public TList<Street> GetStreetsByAutIntNoLoSuIntNo(int autIntNo, int loSuIntNo)
        {
            TList<Street> streets = new TList<Street>();
            Street stDefault = new Street();
            stDefault.StrIntNo = 0;
            stDefault.StrName = Global.SelectSt;
            streets.Add(stDefault);
            if (autIntNo > 0 || loSuIntNo > 0)
            {
                DataSet dsReturn = new DataSet();
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                SqlCommand command = con.CreateCommand();
                command.CommandText = "SILCustom_Street_GetByAutIntNoLoSuIntNo";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int, 4)).Value = autIntNo;
                command.Parameters.Add(new SqlParameter("@LoSuIntNo", SqlDbType.Int, 4)).Value = loSuIntNo;

                SqlDataAdapter da = new SqlDataAdapter(command);
                try
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }
                    da.Fill(dsReturn);
                    if (dsReturn != null && dsReturn.Tables.Count > 0)
                    {
                        DataTable dt = dsReturn.Tables[0];
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Street st = new Street();
                            st.StrIntNo = Convert.ToInt32(dt.Rows[i]["StrIntNo"]);
                            st.StrName = (dt.Rows[i]["StrName"] + "").ToString();
                            streets.Add(st);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }

            return streets;
        }

        private List<StreetModel> GetPagedStreets(string strWhere, string strOrder, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
            SqlCommand command = con.CreateCommand();
            command.CommandText = "SILCustom_Street_GetBySuburbAndStreet";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@WhereClause", SqlDbType.VarChar, 2000)).Value = strWhere;
            command.Parameters.Add(new SqlParameter("@OrderBy", SqlDbType.VarChar, 2000)).Value = strOrder;
            command.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int)).Value = pageIndex;
            command.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int)).Value = pageSize;

            DataSet dsReturn = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                da.Fill(dsReturn);

                List<StreetModel> streets = new List<StreetModel>();
                
                if (dsReturn != null && dsReturn.Tables.Count > 0)
                {
                    DataTable dt = dsReturn.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        StreetModel st = new StreetModel();
                        st.StrIntNo = Convert.ToInt32(dt.Rows[i]["StrIntNo"]);
                        st.StrName = (dt.Rows[i]["StrName"]+"").ToString();
                        st.LoSuIntNo = Convert.ToInt32(dt.Rows[i]["LoSuIntNo"]);
                        st.LoSuDescr = (dt.Rows[i]["LoSuDescr"] + "").ToString();
                        st.AutName = (dt.Rows[i]["AutName"] + "").ToString();
                        streets.Add(st);
                    }
                    totalCount = Convert.ToInt32(dsReturn.Tables[1].Rows[0]["TotalRowCount"]);
                }

                return streets;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int SaveStreet(StreetModel stModel, int autIntNoBefore, int subIntNoBefore)
        {
            int actResult = 0;
            try
            {
                int stCount = 0;
                LocationSuburbStreetConjoin lss = lssServ.GetByLoSuIntNoStrIntNo(stModel.LoSuIntNo, stModel.StrIntNo);

                if (autIntNoBefore == 0)
                {
                    TList<Street> streets = stServ.GetPaged(" LOWER(StrName) = '" + stModel.StrName.ToLower() + "'", "StrName", 0, 50, out stCount);
                    if (stCount > 0) lss = lssServ.GetByLoSuIntNoStrIntNo(stModel.LoSuIntNo, streets[0].StrIntNo);
                    if (lss == null)
                    {
                        Street st = new Street();
                        if (stCount == 0)
                        {
                            st.StrName = stModel.StrName;
                        }
                        else
                        {
                            st = streets[0];
                        }
                        st.LastUser = stModel.LastUser;
                        st = stServ.Save(st);
                        if (st.StrIntNo > 0)
                        {
                            lss = new LocationSuburbStreetConjoin();
                            lss.LoSuIntNo = stModel.LoSuIntNo;
                            lss.StrIntNo = st.StrIntNo;
                            lss.LastUser = stModel.LastUser;
                            lss = lssServ.Save(lss);
                            if (lss.LsscIntNo > 0) actResult = 1;
                        }
                    }
                    else
                    {
                        actResult = 2;
                        return actResult;
                    }
                    if (actResult == 1)
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(stModel.AutIntNo, stModel.LastUser, PunchStatisticsTranTypeList.StreetMaintenance, PunchAction.Add);
                    }
                }
                else
                {
                    TList<Street> streets = stServ.GetPaged(" LOWER(StrName) = '" + stModel.StrName.ToLower() + "' AND StrIntNo <> " + stModel.StrIntNo, "StrName", 0, 50, out stCount);
                    if (stCount > 0) return 2;

                    if (stModel.AutIntNo != autIntNoBefore || stModel.LoSuIntNo != subIntNoBefore)
                    {
                        int isExistInPlaces = 1;
                        using (IDataReader reader = stServ.CheckExistInPlaces(stModel.StrIntNo))
                        {
                            if (reader.Read()) isExistInPlaces = Convert.ToInt32(reader[0]);
                        }
                        if (isExistInPlaces == 1) return 3;
                    }
                    Street st = stServ.GetByStrIntNo(stModel.StrIntNo);
                    st.StrName = stModel.StrName;
                    st.LastUser = stModel.LastUser;
                    if (stModel.LoSuIntNo != subIntNoBefore)
                    {
                        if (lss == null)
                        {
                            if (stServ.Update(st))
                            {
                                lss = lssServ.GetByLoSuIntNoStrIntNo(subIntNoBefore, stModel.StrIntNo);
                                lss.LoSuIntNo = stModel.LoSuIntNo;
                                lss.LastUser = stModel.LastUser;
                                if (lssServ.Update(lss)) actResult = 1;
                            }
                        }
                        else
                        {
                            actResult = 2;
                            return actResult;
                        }
                    }
                    else
                    {
                        if (stServ.Update(st)) actResult = 1;
                    }
                    if (actResult == 1)
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(stModel.AutIntNo, stModel.LastUser, PunchStatisticsTranTypeList.StreetMaintenance, PunchAction.Change);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actResult;
        }

        public int DeleteStreetByStrIntNo(int loSuIntNo, int strIntNo)
        {
            int actResult = 0;
            int isExistInPlaces = 1;
            using (IDataReader reader = stServ.CheckExistInPlaces(strIntNo))
            {
                if (reader.Read()) isExistInPlaces = Convert.ToInt32(reader[0]);
            }
            if (isExistInPlaces == 1) return 3;

            Street st = stServ.GetByStrIntNo(strIntNo);
            LocationSuburbStreetConjoin lss = lssServ.GetByLoSuIntNoStrIntNo(loSuIntNo, strIntNo);
            if (lssServ.Delete(lss)) 
            {
                TList<LocationSuburbStreetConjoin> lssList = lssServ.GetByStrIntNo(strIntNo);
                if (lssList.Count == 0)
                {
                    if (!stServ.Delete(st)) return 0;
                }
                actResult = 1;
            }
            return actResult;
        }
    }
}
