﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.BLL.Utility.Cache;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.BLL.StreetCoding
{
    public class PlaceAtFixedLocationManager
    {
        PlaceAtFixedLocationService fixlocServ = new PlaceAtFixedLocationService();
        StreetService stServ = new StreetService();
        LocationSuburbService lsServ = new LocationSuburbService();
        LocationSuburbStreetConjoinService lssSrv = new LocationSuburbStreetConjoinService();

        public List<PlaceAtFixedLocationModel> GetAllFixLoc(int autIntNo, int loSuIntNo, int strIntNo, string locName, int pageIndex, int pageSize, out int totalCount)
        {
            string where = String.Format("1=1");
            if (autIntNo > 0)
            {
                where = String.Format(" a.AutIntNo = {0}", autIntNo);
            }
            if (loSuIntNo > 0)
            {
                where += String.Format(" AND ls.LoSuIntNo = {0}", loSuIntNo);
            }
            if (strIntNo > 0)
            {
                where += String.Format(" AND s.StrIntNo = {0}", strIntNo);
            }
            if (!string.IsNullOrEmpty(locName))
            {
                where += String.Format(" AND p.PAFLocation like '%{0}%'", locName);
            }

            return GetPagedFixLoc(where, "PAFLIntNo", pageIndex, pageSize, out totalCount);
        }

        private List<PlaceAtFixedLocationModel> GetPagedFixLoc(string strWhere, string strOrder, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
            SqlCommand command = con.CreateCommand();
            command.CommandText = "SILCustom_PlaceAtFixedLocation_GetByAutSubSt";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@WhereClause", SqlDbType.VarChar, 2000)).Value = strWhere;
            command.Parameters.Add(new SqlParameter("@OrderBy", SqlDbType.VarChar, 2000)).Value = strOrder;
            command.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int)).Value = pageIndex;
            command.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int)).Value = pageSize;

            DataSet dsReturn = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                da.Fill(dsReturn);
                List<PlaceAtFixedLocationModel> fixlocs = new List<PlaceAtFixedLocationModel>();

                if (dsReturn != null && dsReturn.Tables.Count > 0)
                {
                    DataTable dt = dsReturn.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        PlaceAtFixedLocationModel fixloc = new PlaceAtFixedLocationModel();
                        fixloc.PAFLIntNo = Convert.ToInt32(dt.Rows[i]["PAFLIntNo"]);
                        fixloc.AutName = (dt.Rows[i]["AutName"] + "").ToString();
                        fixloc.LoSuIntNo = Convert.ToInt32(dt.Rows[i]["LoSuIntNo"]);
                        fixloc.LoSuDescr = (dt.Rows[i]["LoSuDescr"] + "").ToString();
                        fixloc.PAFLStreet1Name = (dt.Rows[i]["StrName"] + "").ToString();
                        fixloc.PAFLocation = (dt.Rows[i]["PAFLocation"] + "").ToString();
                        fixlocs.Add(fixloc);
                    }
                    totalCount = Convert.ToInt32(dsReturn.Tables[1].Rows[0]["TotalRowCount"]);
                }
                return fixlocs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public PlaceAtFixedLocationModel GetFixLocById(int flId)
        {
            PlaceAtFixedLocation fixloc = fixlocServ.GetByPaflIntNo(flId);
            LocationSuburbStreetConjoin lss = lssSrv.GetByLsscIntNo(fixloc.LsscIntNo1);
            LocationSuburb losu = lsServ.GetByLoSuIntNo(lss.LoSuIntNo);
            PlaceAtFixedLocationModel flModel = new PlaceAtFixedLocationModel();

            flModel.PAFLIntNo = fixloc.PaflIntNo;
            flModel.AutIntNo = losu.AutIntNo;
            flModel.LoSuIntNo = losu.LoSuIntNo;
            flModel.CrtIntNo = fixloc.CrtIntNo;
            flModel.PAFLStreet1 = lss.StrIntNo;
            flModel.PAFLocation = fixloc.PafLocation;
            flModel.PAFLGPSXCoord = fixloc.PaflgpsxCoord;
            flModel.PAFLGPSYCoord = fixloc.PaflgpsyCoord;
            return flModel;
        }

        public int SaveFixLoc(PlaceAtFixedLocationModel fixlocModel, int subBefore, int stBefore, string locBefore)
        {
            int actResult = 0;
            PlaceAtFixedLocation fixlocExist = null;
            LocationSuburbStreetConjoin lssExist = null;
            LocationSuburbStreetConjoinService lssServ = new LocationSuburbStreetConjoinService();

            try
            {
                if (stBefore == 0)
                {
                    lssExist = lssServ.GetByLoSuIntNoStrIntNo(fixlocModel.LoSuIntNo, fixlocModel.PAFLStreet1);
                    if (lssExist == null)
                    {
                        actResult = 3;
                        return actResult;
                    }
                    fixlocExist = fixlocServ.GetByLsscIntNo1PafLocation(lssExist.LsscIntNo, fixlocModel.PAFLocation);
                    if (fixlocExist == null)
                    {
                        PlaceAtFixedLocation fixloc = new PlaceAtFixedLocation();
                        fixloc.LsscIntNo1 = lssExist.LsscIntNo;
                        fixloc.PafLocation = fixlocModel.PAFLocation;
                        fixloc.PaflgpsxCoord = fixlocModel.PAFLGPSXCoord;
                        fixloc.PaflgpsyCoord = fixlocModel.PAFLGPSYCoord;
                        fixloc.CrtIntNo = fixlocModel.CrtIntNo;
                        fixloc.LastUser = fixlocModel.LastUser;
                        fixloc = fixlocServ.Save(fixloc);
                        actResult = 1;
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(fixlocModel.AutIntNo, fixlocModel.LastUser, PunchStatisticsTranTypeList.PlaceAtFixedLocationMaintenance, PunchAction.Add);
                    }
                    else
                    {
                        actResult = 2;
                        return actResult;
                    }
                }
                else
                {
                    PlaceAtFixedLocation fixlocNew = fixlocServ.GetByPaflIntNo(fixlocModel.PAFLIntNo);
                    lssExist = lssServ.GetByLoSuIntNoStrIntNo(fixlocModel.LoSuIntNo, fixlocModel.PAFLStreet1);
                    if (subBefore != fixlocModel.LoSuIntNo || stBefore != fixlocModel.PAFLStreet1 || locBefore != fixlocModel.PAFLocation)
                    {
                        if (lssExist == null)
                        {
                            actResult = 3;
                            return actResult;
                        }
                        fixlocExist = fixlocServ.GetByLsscIntNo1PafLocation(lssExist.LsscIntNo, fixlocModel.PAFLocation);
                        if (fixlocExist != null)
                        {
                            actResult = 2;
                            return actResult;
                        }
                    }
                    fixlocNew.LsscIntNo1 = lssExist.LsscIntNo;
                    fixlocNew.PafLocation = fixlocModel.PAFLocation;
                    fixlocNew.PaflgpsxCoord = fixlocModel.PAFLGPSXCoord;
                    fixlocNew.PaflgpsyCoord = fixlocModel.PAFLGPSYCoord;
                    fixlocNew.CrtIntNo = fixlocModel.CrtIntNo;
                    fixlocNew.LastUser = fixlocModel.LastUser;
                    fixlocNew = fixlocServ.Save(fixlocNew);
                    if (fixlocNew.PaflIntNo > 0)
                    {
                        actResult = 1;
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(fixlocModel.AutIntNo, fixlocModel.LastUser, PunchStatisticsTranTypeList.PlaceAtFixedLocationMaintenance, PunchAction.Change);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actResult;
        }

        public int DeleFixLocByID(int flId)
        {
            int actResult = 0;
            try
            {
                PlaceAtFixedLocation fixloc = fixlocServ.GetByPaflIntNo(flId);
                if (fixlocServ.Delete(fixloc)) actResult = 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actResult;
        }
         
    }
}
