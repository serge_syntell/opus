﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.StreetCoding.Model;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.Web.Resource;
using SIL.AARTO.Web.Resource.StreetCoding;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.BLL.StreetCoding
{
    public class LocationSuburbManager
    {
        LocationSuburbService locSubServ = new LocationSuburbService();
        LocationSuburbLocationAreaCodeConjoinService lsLACServ = new LocationSuburbLocationAreaCodeConjoinService();
        LocationSuburbStreetConjoinService lssServ = new LocationSuburbStreetConjoinService();
        PlaceAtFixedLocationService paflServ = new PlaceAtFixedLocationService();
        PlaceAtIntersectionService paiServ = new PlaceAtIntersectionService();
        PlaceBetweenService pbServ = new PlaceBetweenService();

        public List<LocationSuburbModel> GetAllLocSub(int autIntNo, string searchKey, int pageIndex, int pageSize, out int totalCount)
        {
            string where = String.Format("1=1");
            if (autIntNo > 0)
            {
                where = String.Format(" a.AutIntNo={0}", autIntNo);
            }
            if (!String.IsNullOrEmpty(searchKey))
            {
                where += String.Format(" AND (ls.LoSuDescr LIKE '%{0}%' OR lac.LACCode LIKE '%{0}%')", searchKey);
            }

            return GetPagedLoSu(where, "LoSuDescr", pageIndex, pageSize, out totalCount);
        }

        public TList<Authority> GetAllAuth()
        {
            return new AuthorityService().GetAll();
        }

        public List<LocationSuburb> GetSuburbsByAutIntNo(int autIntNo)
        {
            List<LocationSuburb> lsList = new List<LocationSuburb>();
            if (autIntNo > 0)
            {
                lsList = locSubServ.GetByAutIntNo(autIntNo).OrderBy(r => r.LoSuDescr).ToList();
            }

            LocationSuburb suburb = new LocationSuburb();
            suburb.LoSuIntNo = 0;
            suburb.LoSuDescr = Global.SelectSuburb;
            lsList.Insert(0, suburb);
            return lsList;
        }

        public TList<LocationAreaCode> GetAllLocAreaCodes()
        {
            return new LocationAreaCodeService().GetAll();
        }

        public LocationSuburb GetSubBySubId(int subId)
        {
            return locSubServ.GetByLoSuIntNo(subId);
        }

        private List<LocationSuburbModel> GetPagedLoSu(string strWhere, string strOrder, int pageIndex, int pageSize, out int totalCount)
        {
            totalCount = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
            SqlCommand command = con.CreateCommand();
            command.CommandText = "SILCustom_LocationSuburb_GetByAutLoSuLocAreaCode";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add(new SqlParameter("@WhereClause", SqlDbType.VarChar, 2000)).Value = strWhere;
            command.Parameters.Add(new SqlParameter("@OrderBy", SqlDbType.VarChar, 2000)).Value = strOrder;
            command.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int)).Value = pageIndex;
            command.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int)).Value = pageSize;

            DataSet dsReturn = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(command);

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                da.Fill(dsReturn);
                List<LocationSuburbModel> subs = new List<LocationSuburbModel>();

                if (dsReturn != null && dsReturn.Tables.Count > 0)
                {
                    DataTable dt = dsReturn.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        LocationSuburbModel sub = new LocationSuburbModel();
                        sub.LoSuIntNo = Convert.ToInt32(dt.Rows[i]["LoSuIntNo"]);
                        sub.AutName = (dt.Rows[i]["AutName"] + "").ToString();
                        sub.LACCode = (dt.Rows[i]["LACCode"] + "").ToString();
                        sub.LoSuDescr = (dt.Rows[i]["LoSuDescr"] + "").ToString();
                        subs.Add(sub);
                    }
                    totalCount = Convert.ToInt32(dsReturn.Tables[1].Rows[0]["TotalRowCount"]);
                }
                return subs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int SaveLocSub(LocationSuburbModel sub, int autIntNoBefore, string areaCodeBefore)
        {
            int actResult = 0;
            LocationSuburbLocationAreaCodeConjoin lsLac = lsLACServ.GetByLoSuIntNoLacCode(sub.LoSuIntNo, sub.LACCode);
            LocationSuburb subNew = null;

            int subCount = 0;
            // add new
            if (autIntNoBefore == 0)
            {
                TList<LocationSuburb> subs = locSubServ.GetPaged(" LOWER(LoSuDescr) = '" + sub.LoSuDescr.ToLower() + "'", "LoSuIntNo", 0, 50, out subCount);
                if (subCount > 0) lsLac = lsLACServ.GetByLoSuIntNoLacCode(subs[0].LoSuIntNo, sub.LACCode);
                if (lsLac == null)
                {
                    subNew = new LocationSuburb();
                    if (subCount == 0)
                    {
                        subNew.LoSuDescr = sub.LoSuDescr;
                        subNew.AutIntNo = sub.AutIntNo;
                    }
                    else
                    {
                        subNew = subs[0];
                    }
                    subNew.LastUser = sub.LastUser;
                    subNew = locSubServ.Save(subNew);
                    if (subNew.LoSuIntNo > 0)
                    {
                        lsLac = new LocationSuburbLocationAreaCodeConjoin();
                        lsLac.LoSuIntNo = subNew.LoSuIntNo;
                        lsLac.LacCode = sub.LACCode;
                        lsLac.LastUser = sub.LastUser;
                        lsLac = lsLACServ.Save(lsLac);
                        if (lsLac.LslacIntNo > 0) actResult = 1;
                    }
                }
                else
                {
                    actResult = 2;
                    return actResult;
                }
                if (actResult == 1)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(sub.AutIntNo, sub.LastUser, PunchStatisticsTranTypeList.SuburbMaintenance, PunchAction.Add);
                }
            }
            else
            {
                TList<LocationSuburb> subs = locSubServ.GetPaged(" LOWER(LoSuDescr) = '" + sub.LoSuDescr.ToLower() + "' AND LoSuIntNo <> " + sub.LoSuIntNo, "LoSuIntNo", 0, 50, out subCount);
                if (subCount > 0) return 2;

                if (sub.AutIntNo != autIntNoBefore)
                {
                    TList<LocationSuburbStreetConjoin> lssList = lssServ.GetByLoSuIntNo(sub.LoSuIntNo);
                    if (lssList.Count > 0) return 3;

                    int isExistInPlaces = 1;
                    using (IDataReader reader = locSubServ.CheckExistInPlaces(sub.LoSuIntNo))
                    {
                        if (reader.Read()) isExistInPlaces = Convert.ToInt32(reader[0]);
                    }
                    if (isExistInPlaces == 1) return 4;  
                }
                subNew = locSubServ.GetByLoSuIntNo(sub.LoSuIntNo);
                subNew.AutIntNo = sub.AutIntNo;
                subNew.LoSuDescr = sub.LoSuDescr;
                subNew.LastUser = sub.LastUser;
                
                if (sub.LACCode != areaCodeBefore)
                {
                    if (lsLac == null)
                    {
                        bool subUpdate = locSubServ.Update(subNew);
                        bool lsLacUpdate;
                        if (subUpdate)
                        {
                            lsLac = lsLACServ.GetByLoSuIntNoLacCode(sub.LoSuIntNo, areaCodeBefore);
                            lsLac.LacCode = sub.LACCode;
                            lsLac.LastUser = sub.LastUser;
                            lsLacUpdate = lsLACServ.Update(lsLac);
                            if (lsLacUpdate) actResult = 1;
                        } 
                    }
                    else
                    {
                        actResult = 2;
                        return actResult;
                    }
                }
                else
                {
                    if (locSubServ.Update(subNew)) actResult = 1;
                }

                if (actResult == 1)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(sub.AutIntNo, sub.LastUser, PunchStatisticsTranTypeList.SuburbMaintenance, PunchAction.Change);
                }
            }

            return actResult;
        }

        public int DeleteLocSubByLoSuIntNo(int loSuIntNo, string lacCode)
        {
            int actResult = 0;
            LocationSuburb locSub = locSubServ.GetByLoSuIntNo(loSuIntNo);
            TList<LocationSuburbStreetConjoin> lssList = lssServ.GetByLoSuIntNo(loSuIntNo);
            TList<LocationSuburbLocationAreaCodeConjoin> lslacList = lsLACServ.GetByLoSuIntNo(loSuIntNo);

            int isExistInPlaces = 1;
            using (IDataReader reader = locSubServ.CheckExistInPlaces(loSuIntNo))
            {
                if (reader.Read()) isExistInPlaces = Convert.ToInt32(reader[0]);
            }
            if (isExistInPlaces == 1) return 3;

            if (lssList.Count > 0)
            {
                actResult = 2;
            }
            else
            {
                LocationSuburbLocationAreaCodeConjoin lsLac = lsLACServ.GetByLoSuIntNoLacCode(locSub.LoSuIntNo, lacCode);
                if (lsLACServ.Delete(lsLac))
                {
                    lslacList = lsLACServ.GetByLoSuIntNo(loSuIntNo);
                    if (lslacList.Count == 0)
                    {
                        if (!locSubServ.Delete(locSub)) return 0;
                    }
                    actResult = 1;
                }
            }
            return actResult;
        }
    }
}
