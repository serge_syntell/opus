﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.ReportManagement
{
    public static class TmpNoticeStatusReportManager
    {
        private static readonly TmpNoticeStatusReportService Service = new TmpNoticeStatusReportService();
        public static TList<TmpNoticeStatusReport> GetAllNoticeStatusReport()
        {
            return Service.GetAll();
        }
    }
}
