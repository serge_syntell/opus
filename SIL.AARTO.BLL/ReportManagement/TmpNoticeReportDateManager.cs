﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.ReportManagement
{
    public static class TmpNoticeReportDateManager
    {
        private static readonly TmpNoticeReportDateService Service = new TmpNoticeReportDateService();
        public static TmpNoticeReportDate SetNoticeReportDate(DateTime startDate, DateTime endDate, string lastUser)
        {
            TList<TmpNoticeReportDate> dates = Service.GetAll();
            TmpNoticeReportDate date = new TmpNoticeReportDate();
            if (dates.Count == 1)
            {
                date = dates[0];
            }
            else
            {
                foreach (TmpNoticeReportDate item in dates)
                {
                    Service.Delete(item);
                }
            }
            date.StartDate = startDate;
            date.EndDate = endDate;
            date.CreateDate = null;
            date.GenerateReportDate = DateTime.Now;
            date.LastUser = lastUser;
            return Service.Save(date);
        }

        public static TList<TmpNoticeReportDate> GetNoticeReportDate()
        {
            return Service.GetAll();
        }
    }
}
