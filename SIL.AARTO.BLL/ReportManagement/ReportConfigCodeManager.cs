﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using System.Web.Mvc;

namespace SIL.AARTO.BLL.ReportManagement
{
    public class ReportConfigCodeManager
    {
        private static readonly ReportConfigCodeService service = new ReportConfigCodeService();
        public static TList<ReportConfigCode> GetAllCodes()
        {
            return service.GetAll();
        }

        public static SelectList GetSelectList()
        {
            Dictionary<int,string> listOr = new Dictionary<int,string>();
            foreach (var item in GetAllCodes())
            {
                listOr.Add(item.ReportCode,string.Format("{0}({1})",item.ReportName, item.ReportCode));
            }
            SelectList list = new SelectList(listOr,"Key", "Value");
            return list;
        }
    }
}
