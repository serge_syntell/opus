﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;

namespace SIL.AARTO.BLL.ReportManagement
{
    public class ReportEmailAddressManager
    {
        private static readonly ReportEmailAddressService service = new ReportEmailAddressService();
        public static TList<ReportEmailAddress> GetByReportCode(int reportCode)
        {
            return service.GetByReportCode(reportCode);
        }

        public static ReportEmailAddress GetByReaIntNo(long reaIntNo)
        {
            return service.GetByReaIntNo(reaIntNo);
        }

        public static TList<ReportEmailAddress> GetPageByReportCode(int reportCode, int pageIndex, int pageSize, out int totalCount)
        {
            pageIndex = (pageIndex * pageSize);
            return service.GetByReportCode(reportCode, pageIndex, pageSize, out totalCount);
        }

        public static int AddEmail(ReportEmailAddress emailAddressEntity)
        {
            ReportEmailAddress dubEmailAddress = service.GetByReportCodeReaEmailAddress(emailAddressEntity.ReportCode, emailAddressEntity.ReaEmailAddress);
            if (dubEmailAddress != null)
            {
                return -1;  //There email is duplicate
            }
            service.Save(emailAddressEntity);
            return 1;
        }

        public static int DeleteEmail(long reaIntNo)
        {
            ReportEmailAddress reportEmail = service.GetByReaIntNo(reaIntNo);
            service.Delete(reportEmail);
            return 1;
        }

        public static int UpdateEmail(ReportEmailAddress emailAddressEntity)
        {
            ReportEmailAddress emailAddressOld = service.GetByReaIntNo(emailAddressEntity.ReaIntNo);
            ReportEmailAddress dubEmailAddress = service.GetByReportCodeReaEmailAddress(emailAddressEntity.ReportCode, emailAddressEntity.ReaEmailAddress);

            if (emailAddressOld.ReaEmailAddress != emailAddressEntity.ReaEmailAddress
                && emailAddressOld.ReportCode == emailAddressEntity.ReportCode
                && dubEmailAddress != null)
            {
                return -1; //There email is duplicate
            }
            emailAddressEntity.RowVersion = emailAddressOld.RowVersion;
            emailAddressEntity.ReaDateCreated = emailAddressOld.ReaDateCreated;
            service.Update(emailAddressEntity);
            return 1;
        }
    }
}
