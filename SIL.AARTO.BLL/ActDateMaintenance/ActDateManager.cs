﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.Web.Resource.ActDateMaintenance;
using SIL.ServiceQueueLibrary.DAL.Data;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.ServiceQueueLibrary.DAL.Services;

namespace SIL.AARTO.BLL.ActDateMaintenance
{
    public class PrintFileEntity
    {
        public string PrintFileName { get; set; }
        public int PFNIntNo { get; set; }
        public string ActDate { get; set; }
        public bool NoQueueItemFound { get; set; }
    }

    public class ActDateManager
    {
        public List<PrintFileEntity> GetByCourtAndCourtDate(int sqtIntNo, int crtIntNo, DateTime? courtDateFrom, DateTime? courtDateto, int pageIndex, int pageSize, out int totalCount)
        {

            totalCount = 0;
            Dictionary<string, PrintFileEntity> dictPrintFiles = new Dictionary<string, PrintFileEntity>();
            List<PrintFileEntity> returnList = new List<PrintFileEntity>();
            using (IDataReader result = new PrintFileNameService().GetPagedPrintFileNameByCourtAndCourtDate(crtIntNo, courtDateFrom, courtDateto, pageIndex, pageSize))
            {
                while (result.Read())
                {
                    dictPrintFiles.Add(result["PFNIntNo"].ToString(), new PrintFileEntity()
                    {
                        PFNIntNo = Convert.ToInt32(result["PFNIntNo"]),
                        PrintFileName = result["PrintFileName"].ToString()
                    });
                }
                if (result.NextResult())
                {
                    while (result.Read())
                    {
                        totalCount = Convert.ToInt32(result["TotalCount"]);
                        break;
                    }
                }
            }

            if (dictPrintFiles.Keys.Count > 0)
            {
                SIL.ServiceQueueLibrary.DAL.Data.ServiceQueueQuery query = new ServiceQueueLibrary.DAL.Data.ServiceQueueQuery();

                query.AppendIn(ServiceQueueColumn.SeQuKey, dictPrintFiles.Keys.ToArray());
                query.AppendEquals(ServiceQueueColumn.SqtIntNo, sqtIntNo.ToString());

                List<ServiceQueue> queues = new ServiceQueueService().Find(query).ToList();

                ServiceQueue q = null;
                foreach (string key in dictPrintFiles.Keys)
                {
                    q = queues.Where(s => s.SeQuKey.Equals(key)).FirstOrDefault();
                    if (q != null)
                    {
                        dictPrintFiles[key].ActDate = q.SeQuActionDate.HasValue ? q.SeQuActionDate.Value.ToString("yyyy/MM/dd") : "";
                        dictPrintFiles[key].NoQueueItemFound = false;
                    }
                    else
                    {
                        dictPrintFiles[key].ActDate = PrintSummons.msgNotAvailable;
                        dictPrintFiles[key].NoQueueItemFound = true;
                    }
                    returnList.Add(dictPrintFiles[key]);
                }
            }

            return returnList;
        }


        public int SetActionDate(int sqtIntNo, string sequkey, DateTime actDate, string lastUser)
        {
            ServiceQueueService service = new ServiceQueueService();
            ServiceQueueQuery query = new ServiceQueueQuery();
            query.Append(ServiceQueueColumn.SqtIntNo, sqtIntNo.ToString());
            query.Append(ServiceQueueColumn.SeQuKey, sequkey);

            SIL.ServiceQueueLibrary.DAL.Entities.TList<ServiceQueue> queues = service.Find(query);
            if (queues != null && queues.Count > 0)
            {
                ServiceQueue firstQueue = null;
                if (queues.Count > 1)
                {
                    firstQueue = queues.OrderBy(q => q.SeQuActionDate).FirstOrDefault();
                }
                else
                {
                    firstQueue = queues[0];
                }

                if (actDate < DateTime.Now.Date)
                {
                    return -2;
                }
                else if (actDate >= firstQueue.SeQuActionDate)
                {
                    return -3;
                }

                firstQueue.SeQuActionDate = actDate;
                firstQueue.LastUser = lastUser;

                foreach (ServiceQueue q in queues)
                {
                    if (!q.SeQuId.Equals(firstQueue.SeQuId))
                        q.MarkToDelete();
                }
                try
                {
                    service.Save(queues);
                    return 0;
                }
                catch
                {
                    throw;
                }
            }

            return -1;
        }
    }
}
