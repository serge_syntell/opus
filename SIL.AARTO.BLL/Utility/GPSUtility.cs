﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility
{
    public class GPSUtility
    {
        public static void GetLatitudeAndLongitude(string gps, out string latitude, out string longitude)
        {
            string gpsValue = gps;
            string gPSLatitude = string.Empty;
            string gPSLongitude = string.Empty;
            if (string.IsNullOrEmpty(gpsValue))
            {
                gPSLatitude = "";
                gPSLongitude = "";
            }
            else
            {
                string[] tempGPS = gpsValue.Split(',');
                if (tempGPS.Length == 2)
                {
                    gPSLatitude = tempGPS[0];
                    gPSLongitude = tempGPS[1];
                }
                else
                {
                    gPSLatitude = tempGPS[0];
                    gPSLongitude = "";
                }
            }

            latitude = gPSLatitude;
            longitude = gPSLongitude;
        }
    }
}
