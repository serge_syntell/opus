﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.Indaba;
using System.Configuration;
using System.IO;

namespace SIL.AARTO.BLL.Utility
{
    public class IndabaClientUtil: IDisposable
    {
        public List<string> GetReceivedFiles()
        {
            List<string> files = ReceiveDoc("pebs");
            files.AddRange(ReceiveDoc("pdf"));
            return files;
        }
        private ICommunicate indaba;
        public IndabaClientUtil()
        {
            Client.ApplicationName = "Aarto";
            Client.ConnectionString = ConfigurationManager.AppSettings["IndabaConnectionString"];
            Client.Initialise();
            indaba = Client.Create();
            indaba.Destination = ConfigurationManager.AppSettings["IndabaDestination"];
        }
        public void SendADoc(string fileName)
        {
            indaba.Enqueue(fileName);
        }
        public List<string> ReceiveDoc(string ext)
        {
            IndabaFileInfo[] fileInfoList;
            fileInfoList = indaba.GetFileInformation(ext);
            List<string> files = new List<string>();
            foreach (IndabaFileInfo indabaFile in fileInfoList)
            {
                string fullFileName = GetFileFromIndabaDB(indabaFile, indaba);
                if (string.IsNullOrEmpty(fullFileName))
                {
                    continue;
                }
                files.Add(fullFileName);
            }
            return files;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;
        private void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    indaba.Dispose();
                }
                Client.Dispose();
                disposed = true;

            }
        }
        ~IndabaClientUtil()
        {
            Dispose(false);
        }

        private string GetFileFromIndabaDB(IndabaFileInfo indabaFile, ICommunicate indaba)
        {
            string printDirectory = ConfigurationManager.AppSettings["PrintDirectoryIn"];
            if (!Directory.Exists(printDirectory))
            {
                Directory.CreateDirectory(printDirectory);
            }
            string fullFileName = Path.Combine(printDirectory, indabaFile.Name);
            try
            {
                using (FileStream file = File.Create(fullFileName))
                {
                    MemoryStream stream = (MemoryStream)indaba.Dequeue(indabaFile);
                    stream.WriteTo(file);
                }
            }
            catch
            {
                return null;
            }
            return fullFileName;
        }
    }
}
