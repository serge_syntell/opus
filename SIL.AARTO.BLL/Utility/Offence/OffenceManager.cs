﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Offence
{
    public class OffenceManager
    {
        public static SelectList GetOffenceSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allLangOffence = OffenceLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            TList<SIL.AARTO.DAL.Entities.Offence> allOffence = new SIL.AARTO.DAL.Services.OffenceService().GetAll();
            int offIntNo;
            foreach (var offence in allOffence)
            {
                offIntNo = offence.OffIntNo;
                if (allLangOffence.Keys.Contains(offIntNo))
                {
                    list.Add(new SelectListItem() { Value = (offIntNo).ToString(), Text = allLangOffence[offIntNo] });
                }
                else
                {
                    list.Add(new SelectListItem() { Value = (offIntNo).ToString(), Text = offence.OffDescr });
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public static SelectList GetOffenceSelectListByOGIntNo(int ogIntNo)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allLangOffence = OffenceLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            TList<SIL.AARTO.DAL.Entities.Offence> allOffence = new SIL.AARTO.DAL.Services.OffenceService().GetByOgIntNo(ogIntNo);
            int offIntNo;

            list.Add(new SelectListItem() { Value = "0", Text = SIL.AARTO.Web.Resource.SecondaryOffence.SecondaryOffenceList.PleaseSelectOffence });
            foreach (var offence in allOffence)
            {
                offIntNo = offence.OffIntNo;
                if (allLangOffence.Keys.Contains(offIntNo))
                {
                    list.Add(new SelectListItem() { Value = (offIntNo).ToString(), Text = offence.OffCode + " ~ " + allLangOffence[offIntNo] });
                }
                else
                {
                    list.Add(new SelectListItem() { Value = (offIntNo).ToString(), Text = offence.OffCode + " ~ " + offence.OffDescr });
                }
            }
            return new SelectList(list, "Value", "Text");
        }

        public static List<SelectListItem> GetOffenceListByOGIntNo(int ogIntNo = 0)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allLangOffence = OffenceLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            TList<SIL.AARTO.DAL.Entities.Offence> allOffence = new SIL.AARTO.DAL.Services.OffenceService().GetByOgIntNo(ogIntNo);

            int offIntNo;
            foreach (var offence in allOffence)
            {
                offIntNo = offence.OffIntNo;
                if (allLangOffence.Keys.Contains(offIntNo))
                {
                    list.Add(new SelectListItem() { Value = (offIntNo).ToString(), Text = offence.OffCode + " ~ " + allLangOffence[offIntNo] });
                }
                else
                {
                    list.Add(new SelectListItem() { Value = (offIntNo).ToString(), Text = offence.OffCode + " ~ " + offence.OffDescr });
                }
            }
            return list;
        }

    }
}
