﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;

namespace SIL.AARTO.Util
{
    public sealed class MessageQueueUtil
    {
        private MessageQueueUtil()
        {
        }
        public static void SendMsgToQueue(object msgBody, string qName)
        {
            Message msg = new Message(msgBody, new BinaryMessageFormatter());
            msg.Recoverable = true;
            if (!MessageQueue.Exists(qName))
            {
                MessageQueue.Create(qName);
            }
            MessageQueue mq = new MessageQueue(qName);
            //mq.Formatter = new BinaryMessageFormatter();
            //mq.SetPermissions(Environment.UserName, MessageQueueAccessRights.FullControl, AccessControlEntryType.Set);                        
            mq.Send(msg);
        }
        public static List<T> GetMsgListFromQueue<T>(string qName, int timeoutSeconds)
        {
            if (!MessageQueue.Exists(qName))
            {
                MessageQueue.Create(qName);
            }
            MessageQueue mq = new MessageQueue(qName);
            mq.Formatter = new BinaryMessageFormatter();
            //mq.SetPermissions(Environment.UserName, MessageQueueAccessRights.FullControl, AccessControlEntryType.Set);

          
            List<T> objList = new List<T>();
            try
            {
                Message msg = mq.Receive(TimeSpan.FromSeconds(timeoutSeconds));
                while (msg != null)
                {
                    objList.Add((T)msg.Body);
                    msg = mq.Receive(TimeSpan.FromSeconds(timeoutSeconds));
                }
            }
            catch (MessageQueueException ex)
            {
                // ignore ex;
            }

            if (objList.Count > 0)
            {
                return objList;
            }
            
            //Message[] msgs = mq.GetAllMessages();
            //if (msgs.Length > 0)
            //{
                
            //    foreach (var msg in msgs)
            //    {
            //        msg = mq.Receive(TimeSpan.FromSeconds(1));
            //        objList.Add((T)msg.Body);
            //    }
            //    return objList;                
            //}
            return null;
        }
        //public static void LogErrorToQ(Exception ex, string qName)
        //{
        //    string ErrMessage = ex.ToString();
        //    string msgBody = "<span style='background-color: #ccf0f0;'>时间：" + DateTime.Now.ToString() +
        //        "</span><br />Url：" + HttpContext.Current.Request.Path + "<br />Error：" + HttpContext.Current.Server.HtmlEncode(ErrMessage);
        //    SendMsgToQueue(msgBody, qName);
        //}
        //public static string ReadErrorFromQ(string qName)
        //{
        //    string msg = GetMsgFromQueue(qName) as string;
        //    return msg;
        //}
    }
}
