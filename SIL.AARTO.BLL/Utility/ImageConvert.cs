﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

using SIL.AARTO.DAL.Services;
using System.Data;

namespace SIL.AARTO.BLL.Utility
{
    public class ImageConvert
    {
        public static string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String                
                return Convert.ToBase64String(imageBytes);
            }
        }

        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        /// <summary>
        /// Get images full path from file server by notice
        /// </summary>
        /// <param name="noticeNumber">notice number</param>
        /// <param name="imageType">
        /// 1:Main image
        /// 2:Alternate image
        /// R:RegNo image
        /// D:Driver image
        /// '': string.empty for all images related to this notice number</param>
        /// <returns></returns>
        public static List<string> GetFullImagePathByNotice(string noticeNumber, string imageType)
        {
            List<string> imgPaths = new List<string>();
            IDataReader reader = new ScanImageService().GetImageInfoByNotice(noticeNumber, imageType);
            while (reader.Read())
            {
                StringBuilder sbPath = new StringBuilder();
                sbPath.AppendFormat(@"\\{0}\{1}\{2}\{3}",
                    reader["ImageMachineName"].ToString(),
                    reader["ImageShareName"].ToString(),
                    reader["FrameImagePath"].ToString(),
                    reader["JPegName"].ToString());
                imgPaths.Add(sbPath.ToString());
            }
            reader.Close();
            return imgPaths;
        }
    }    
}
