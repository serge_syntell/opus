﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Culture;
using Stalberg.TMS;


namespace SIL.AARTO.BLL.Utility
{
    public class AuthorityRule
    {
        //2014-01-08 Added by Nancy start
        //Set the value of current conn for being called by Servive
        private static string _connnectionString;
        public static string ConnnectionString
        {
            get { return _connnectionString; }
            set { _connnectionString = value; }
        }
        //2014-01-08 Added by Nancy end

        public static bool GetAuthorityRulesBool(string arCode, int authIntNo)
        {
            return GetAuthorityRulesKeyValue(arCode, authIntNo).Value.Equals("Y");
        }

        public static KeyValuePair<int, string> GetAuthorityRulesKeyValue(string arCode, int authIntNo)
        {
            //2014-01-08 Modified by Nancy for being called by Servive(add if...else)
            if (!string.IsNullOrEmpty(_connnectionString))
            {
                return (new DefaultAuthRules(GetAuthorityRules(arCode, authIntNo),                    _connnectionString)).SetDefaultAuthRule();
            }
            else
                return (new DefaultAuthRules(GetAuthorityRules(arCode, authIntNo),
                    Config.ConnectionString)).SetDefaultAuthRule();
        }

        private static AuthorityRulesDetails GetAuthorityRules(string arCode, int authIntNo)
        {
            return new AuthorityRulesDetails()
            {
                AutIntNo = authIntNo,
                ARCode = arCode
            };
        }        
    }
}
