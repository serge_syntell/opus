﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Authority
{
    public class AuthorityManager
    {
        public static string GetAutNameByAutIntNo(int autIntNo)
        {
            return new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(autIntNo).AutName;
        }
    }
}
