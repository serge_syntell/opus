﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Web;


namespace SIL.AARTO.BLL.Utility
{
    public class Language
    {
        #region data
        public static string[] LanguageCode = new string[] {
            "",
            "en-US"
                                              
        };

        public static string[] LanguageKey = new string[] {
            "selectLanguage",
            "enUS"
            
        };

        private const string SessionRegistrationInfoKey = "RegistrationInfo";
        private const string CultureDictCacheKey = "CultureDict";
        public static string[] SupportedLanguages = new string[] { "en-US" };
        public const string DefaultLanguage = "en-US";
        #endregion

        #region property
        public static string BrowserLanguage
        {
            get
            {
                var BrowserLanguages = HttpContext.Current.Request.UserLanguages;
                return (BrowserLanguages != null && BrowserLanguages.Length > 0) ?
                    BrowserLanguages[0] : DefaultLanguage;
            }
        }
        public static CultureInfo DefaultCulture
        {
            get
            {
                return CultureDict[DefaultLanguage];
            }
        }
        public static CultureInfo BrowserCulture
        {
            get
            {
                if (!CultureDict.ContainsKey(BrowserLanguage))
                {
                    CultureDict[BrowserLanguage] = CultureInfo.CreateSpecificCulture(BrowserLanguage);
                }
                return CultureDict[BrowserLanguage];
            }
        }
       
        private static Dictionary<string,CultureInfo> CultureDict
        {
            get
            {
                if (HttpContext.Current.Cache[CultureDictCacheKey] == null)
                {
                    var dict = new Dictionary<string, CultureInfo>();
                    foreach (var supportedLang in SupportedLanguages)
                    {
                        if (string.IsNullOrEmpty(supportedLang))
                        {
                            dict.Add(supportedLang, CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            dict.Add(supportedLang, CultureInfo.CreateSpecificCulture(supportedLang));
                        }
                    }
                    HttpContext.Current.Cache[CultureDictCacheKey] = dict;
                }
                return HttpContext.Current.Cache[CultureDictCacheKey] as Dictionary<string, CultureInfo>;
            }
        }
        public static string EnvironmentLanguage
        {
            get
            {
                //if (HttpContext.Current.Request.IsAuthenticated &&
                //    LoginedUserInfo != null)
                //{
                //    return LoginedUserInfo.Language;
                //}
                //else
                //{
                    //var sep = new char[] { ';' };
                    var sep = new char[] { '-' };
                    if (HttpContext.Current.Request.UserLanguages != null &&
                        HttpContext.Current.Request.UserLanguages.Length > 0)
                    {
                        foreach (string userLang in HttpContext.Current.Request.UserLanguages)
                        {
                            var ul = userLang.Split(sep)[0];
                            //if (SupportedLanguages.Contains(ul))
                            //    return ul;
                            foreach (string supportedLang in SupportedLanguages)
                            {
                                if (string.Compare(ul, supportedLang.Split(sep)[0], true) == 0)
                                {
                                    return supportedLang;
                                }
                            }
                        }
                    }
                    return DefaultLanguage;
                //}
            }
        }
        public static CultureInfo EnvironmentCulture
        {
            get
            {
                return CultureDict[EnvironmentLanguage];
            }
        }
        #endregion

        #region method
        /// <summary>
        /// Get current language format string according default browser language
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentLanguageFormat()
        {
            return BrowserCulture.DateTimeFormat.ShortDatePattern;
        }
        #endregion
    }
}
