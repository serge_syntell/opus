﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SIL.AARTO.BLL.Utility
{
    public class WebUtility
    {
        public static string GetServerIp()
        {
            string serverIP = string.Empty;
            try
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                {
                    serverIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    if (HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] != null)
                    {
                        serverIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                //string msg = ex.Message;
                throw ex;
            }
            return serverIP;
        }

        public static string GetTMSDomainName()
        {
            string HostName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            string domainUrl = string.Empty;
            string ip = GetServerIp();
            if (String.IsNullOrEmpty(ip))
            {
                domainUrl = System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"];
            }
            else if (System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"].IndexOf(ip) >= 0
                || System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"].IndexOf(HostName) >= 0)
            {
                domainUrl = System.Configuration.ConfigurationManager.AppSettings["TMSDomainURL"];
            }
            else if (System.Configuration.ConfigurationManager.AppSettings["TMSDomainExternalURL"].IndexOf(ip) >= 0
                || System.Configuration.ConfigurationManager.AppSettings["TMSDomainExternalURL"].IndexOf(HostName) >= 0)
            {
                domainUrl = System.Configuration.ConfigurationManager.AppSettings["TMSDomainExternalURL"];
            }

            return domainUrl;
        }

        public static string GetAartoDomainName() {
            string HostName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            string domainUrl = System.Configuration.ConfigurationManager.AppSettings["AARTODomainURLDefault"] ?? String.Empty;
            string ip = GetServerIp();
            if (String.IsNullOrEmpty(ip))
            {
                domainUrl = System.Configuration.ConfigurationManager.AppSettings["AARTODomainURL"];
            }
            else if (System.Configuration.ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(ip) >= 0
                || System.Configuration.ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(HostName) >= 0)
            {
                domainUrl = System.Configuration.ConfigurationManager.AppSettings["AARTODomainURL"];
            }
            else if (System.Configuration.ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(ip) >= 0
                || System.Configuration.ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(HostName) >= 0)
            {
                domainUrl = System.Configuration.ConfigurationManager.AppSettings["AARTODomainExternalURL"];
            }

            return domainUrl;
        }
    }
}
