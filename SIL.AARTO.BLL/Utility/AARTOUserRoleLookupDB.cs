﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace SIL.AARTO.BLL.Utility
{
    public class AARTOUserRoleLookupDB
    {
        public static DataSet GetListByAaUserRoleId(int AaUserRoleId)
        {
            DataSet ds = new DataSet();

            string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();

            SqlConnection myConnection = new SqlConnection(connStr);
            SqlCommand myCommand = new SqlCommand("AARTOUserRoleLookupGetListByAaMeID", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAaUserRoleId = new SqlParameter("@AaUserRoleId", SqlDbType.Int, 4);
            parameterAaUserRoleId.Value = AaUserRoleId;
            myCommand.Parameters.Add(parameterAaUserRoleId);

            SqlDataAdapter sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }
            return ds;
        }

        public static bool DeleteByAaUserRoleLid(int AaUserRoleId)
        {
            bool result=false;
            string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();

            SqlConnection myConnection = new SqlConnection(connStr);
            SqlCommand myCommand = new SqlCommand("AARTOUserRoleLookupDeleteByAaMeID", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAaUserRoleId = new SqlParameter("@AaUserRoleId", SqlDbType.Int, 4);
            parameterAaUserRoleId.Value = AaUserRoleId;
            myCommand.Parameters.Add(parameterAaUserRoleId);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
            }
            return result;
        }
    }
}
