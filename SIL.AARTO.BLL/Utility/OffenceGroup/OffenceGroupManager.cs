﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.OffenceGroup
{
    public class OffenceGroupManager
    {
        public static SelectList GetOffenceGroupSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            Dictionary<int, string> allLangOffenceGroup = OffenceGroupLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            TList<SIL.AARTO.DAL.Entities.OffenceGroup> allOffenceGroup = new SIL.AARTO.DAL.Services.OffenceGroupService().GetAll();
            int ogIntNo;

            list.Add(new SelectListItem() { Value = "0", Text = SIL.AARTO.Web.Resource.SecondaryOffence.SecondaryOffenceList.PleaseSelectOffenceGroup });
            foreach (var offenceGroup in allOffenceGroup)
            {
                ogIntNo = offenceGroup.OgIntNo;
                if (allLangOffenceGroup.Keys.Contains(ogIntNo))
                {
                    list.Add(new SelectListItem() { Value = (ogIntNo).ToString(), Text = offenceGroup.OgCode + " ~ " + allLangOffenceGroup[ogIntNo] });
                }
                else
                {
                    list.Add(new SelectListItem() { Value = (ogIntNo).ToString(), Text = offenceGroup.OgCode + " ~ " + offenceGroup.OgDescr });
                }
            }

            return new SelectList(list, "Value", "Text");
        }
    }
}
