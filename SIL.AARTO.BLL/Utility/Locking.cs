﻿using System.Data;
using System.Data.SqlClient;

namespace SIL.AARTO.BLL.Utility
{
    public class Locking
    {
        public static void LockNoticeChargeSummonsSumCharge(string connStr, int notIntNo, bool lockAll = false)
        {
            if (string.IsNullOrWhiteSpace(connStr)
                || notIntNo <= 0)
                return;

            var paras = new[]
            {
                new SqlParameter("@NotIntNo", notIntNo),
                new SqlParameter("@LockAll", lockAll)
            };

            using (var conn = new SqlConnection(connStr))
            {
                using (var cmd = new SqlCommand("Lock_Notice_Charge_Summons_SumCharge", conn)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = 0
                })
                {
                    cmd.Parameters.AddRange(paras);
                    conn.Open();
                    cmd.ExecuteScalar();
                }
            }
        }
    }
}
