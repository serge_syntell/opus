﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility
{
    public class RuleViolation
    {
        public string ModelName { get; set; }
        public string ResourceName { get; set; }
        public object[] Args { get; set; }

        public RuleViolation(string modelName, string resourceName, params object[] args)
        {
            this.ModelName = modelName;
            this.ResourceName = resourceName;
            this.Args = args;
        }
    }
}
