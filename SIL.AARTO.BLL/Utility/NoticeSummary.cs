using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;

/// <summary>
/// Summary description for NoticeSummary
/// </summary>

namespace Stalberg.TMS
{
    public class NoticeSummary
    {
        string mConstr = string.Empty;
        string printFile = string.Empty;
        int autIntNo = 0;
        string format = string.Empty;
        string showAll = string.Empty;
        int status = 0;
        int option = 0;
        string reportPage = string.Empty;
        string path = string.Empty;
        int noOfNotices = 0;

        public NoticeSummary(string vConstr, int vAutIntNo, string vPrintFile, string vFormat, string vShowAll, 
            int vStatus, int vOption, string vPath, string vReportPage, int vNoOfNotices)
        {
            mConstr = vConstr;
            printFile = vPrintFile;
            autIntNo = vAutIntNo;
            format = vFormat;
            showAll = vShowAll;
            status = vStatus;
            option = vOption;
            reportPage = vReportPage;
            path = vPath;
            noOfNotices = vNoOfNotices;
        }

        public byte[] CreateSummary()
        {
            DocumentLayout doc = new DocumentLayout(path);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = mConstr;
            ParameterDictionary parameters = new ParameterDictionary();

            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSummaryRptName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblSummaryRptName");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblNotTicketNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblNotTicketNo");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblForAttention = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblForAttention");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenderAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenderAddress");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblNotLocDescr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblNotLocDescr");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCount");

            lblNotLocDescr.LayingOut += new LayingOutEventHandler(lblNotLocDescr_LayingOut);
            lblNotTicketNo.LayingOut += new LayingOutEventHandler(lblNotTicketNo_LayingOut);
            lblForAttention.LayingOut += new LayingOutEventHandler(lblForAttention_LayingOut);
            lblOffenderAddress.LayingOut += new LayingOutEventHandler(lblOffenderAddress_LayingOut);
            lblSummaryRptName.LayingOut += new LayingOutEventHandler(lblSummaryRptName_LayingOut);
            lblCount.LayingOut += new LayingOutEventHandler(lblCount_LayingOut);

            parameters.Add("AutIntNo", autIntNo);
            parameters.Add("PrintFile", printFile);
            parameters.Add("Status", status);
            parameters.Add("ShowAll", showAll);
            parameters.Add("Format", format);
            parameters.Add("Option", option);

            Document reportA = doc.Run(parameters);
            byte[] bufferA = reportA.Draw();

            lblNotLocDescr.LayingOut -= new LayingOutEventHandler(lblNotLocDescr_LayingOut);
            lblNotTicketNo.LayingOut -= new LayingOutEventHandler(lblNotTicketNo_LayingOut);
            lblForAttention.LayingOut -= new LayingOutEventHandler(lblForAttention_LayingOut);
            lblOffenderAddress.LayingOut -= new LayingOutEventHandler(lblOffenderAddress_LayingOut);
            lblSummaryRptName.LayingOut -= new LayingOutEventHandler(lblSummaryRptName_LayingOut);
            lblCount.LayingOut -= new LayingOutEventHandler(lblCount_LayingOut);

            return bufferA;
        }

        private void lblCount_LayingOut(object sender, LayingOutEventArgs e)
        {
            Label lblCount = (Label)sender;
            lblCount.Text = noOfNotices.ToString();
        }

        private void lblNotLocDescr_LayingOut(object sender, LayingOutEventArgs e)
        {
            //Oscar 20120330 add for empty record check;
            if (!e.LayoutWriter.RecordSets.Current.HasData) return;

            Label lblNotLocDescr = (Label)sender;
            lblNotLocDescr.Text = e.LayoutWriter.RecordSets.Current["NotLocDescr"].ToString().Trim();
        }

        private void lblNotTicketNo_LayingOut(object sender, LayingOutEventArgs e)
        {
            //Oscar 20120330 add for empty record check;
            if (!e.LayoutWriter.RecordSets.Current.HasData) return;

            Label lblNotTicketNo = (Label)sender;
            lblNotTicketNo.Text = e.LayoutWriter.RecordSets.Current["NotTicketNo"].ToString().Trim();
        }

        private void lblForAttention_LayingOut(object sender, LayingOutEventArgs e)
        {
            //Oscar 20120330 add for empty record check;
            if (!e.LayoutWriter.RecordSets.Current.HasData) return;

            Label lblForAttention = (Label)sender;
            string notSendTo = e.LayoutWriter.RecordSets.Current["NotSendTo"].ToString().Trim();

            if (notSendTo.Equals("P"))
                lblForAttention.Text = e.LayoutWriter.RecordSets.Current["PrxInitials"].ToString() 
                    + " " + e.LayoutWriter.RecordSets.Current["PrxSurname"].ToString() 
                    + "\nas Representative of\n" 
                    + e.LayoutWriter.RecordSets.Current["DrvSurname"].ToString();
            else
                lblForAttention.Text = e.LayoutWriter.RecordSets.Current["DrvInitials"].ToString() 
                    + " " + e.LayoutWriter.RecordSets.Current["DrvSurname"].ToString();           
        }

        private void lblOffenderAddress_LayingOut(object sender, LayingOutEventArgs e)
        {
            //Oscar 20120330 add for empty record check;
            if (!e.LayoutWriter.RecordSets.Current.HasData) return;

            Label lblOffenderAddress = (Label)sender;

            lblOffenderAddress.Text = (e.LayoutWriter.RecordSets.Current["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (e.LayoutWriter.RecordSets.Current["DrvPOAdd1"].ToString().Trim()) + "\n")
                 + (e.LayoutWriter.RecordSets.Current["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (e.LayoutWriter.RecordSets.Current["DrvPOAdd2"].ToString().Trim()) + "\n")
                 + (e.LayoutWriter.RecordSets.Current["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (e.LayoutWriter.RecordSets.Current["DrvPOAdd3"].ToString().Trim()) + "\n")
                 + (e.LayoutWriter.RecordSets.Current["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (e.LayoutWriter.RecordSets.Current["DrvPOAdd4"].ToString().Trim()) + "\n")
                 + (e.LayoutWriter.RecordSets.Current["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (e.LayoutWriter.RecordSets.Current["DrvPOAdd5"].ToString().Trim()) + "\n")
                 + e.LayoutWriter.RecordSets.Current["DrvPOCode"].ToString().Trim();
        }

        private void lblSummaryRptName_LayingOut(object sender, LayingOutEventArgs e)
        {
            Label lblSummaryRptName = (Label)sender;
            lblSummaryRptName.Text = "Notice Summary report for: " + printFile;

        }
    }
}
