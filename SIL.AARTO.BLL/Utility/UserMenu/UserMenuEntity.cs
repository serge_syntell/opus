﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.UserMenu
{
    [Serializable]
    public class UserMenuEntity
    {
        public decimal AaMeID {  get;  set;   }

        public decimal ParentAaMeID { get; set; }

        public UserMenuEntity ParentMenuEntity { get; set; }

        public string AMLMenuItemName { get; set; }
        public string AMLMenuItemDesc { get; set; }

        public string AaMePageURL { get; set; }
        public string PageID { get; set; }
        public bool? IsAarto { get; set; }

        public bool? IsNewWindow { get; set; }

        public int? UserRole { get; set; }

        public int  AaMeOrderNo { get; set; }

        public bool IsVisable { get; set; }

        public int Level { get; set; }

        public UserMenuEntityCollection UserMenuList { get; set; }

        public UserMenuEntity()
        {
            AaMeID = 0;
            ParentAaMeID = 0;
            AMLMenuItemName = string.Empty;
            PageID = string.Empty;
            AaMeOrderNo = 0;
            IsVisable = false;
            UserMenuList = new UserMenuEntityCollection();
        }
    }

    [Serializable]
    public class UserMenuEntityCollection : List<UserMenuEntity>
    { }
}
