﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Web.UI.WebControls;
using SIL.AARTO.BLL.Utility.UserMenu;
using SIL.AARTO.BLL.Utility.Cache;

namespace SIL.AARTO.BLL.Utility
{
    public class UserMenuManager
    {
        private static AartoMenuService menuService = new AartoMenuService();
        private static AartoMenuLookupService menuLookupService = new AartoMenuLookupService();
        private static AARTOMenuCache cacheMenu = new AARTOMenuCache();
        public static void GetNavigationMenu(UserMenuEntityCollection collection, UserMenuEntity menu, ref ArrayList returnValue)
        {
            ArrayList list = new ArrayList();

            GetUserMenuCollectionFromTreecollection(collection, ref list);

            if (!returnValue.Contains(menu))
            {
                returnValue.Add(menu);
            }
            foreach (UserMenuEntity userMenu in list)
            {
                if (userMenu.AaMeID == menu.ParentAaMeID)
                {
                    returnValue.Add(userMenu);

                    GetNavigationMenu(collection, userMenu, ref returnValue);
                }
            }
        }

        public UserMenuEntityCollection GetUserMenuCollectionUserRoles(List<int> userRoles, string language, int level)
        {
            UserMenuEntityCollection childNodes = new UserMenuEntityCollection();

            UserMenuEntityCollection userMenuList = cacheMenu.GetUserMenuCollection(language, level);

            RecursionBuildTree(userMenuList, childNodes, userRoles);

            //Up find a node if need to show for the user
            foreach (UserMenuEntity entity in childNodes)
            {
                if (entity.ParentAaMeID != 0 && entity.UserMenuList.Count == 0)
                {
                    UpFindUserMenu(entity.ParentMenuEntity);
                }
            }

            return userMenuList;

        }

        public UserMenuEntityCollection GetUserMenuCollectionUserRolesFromDB(List<int> userRoles, string language, int level)
        {
            UserMenuEntityCollection childNodes = new UserMenuEntityCollection();

            UserMenuEntityCollection userMenuList = cacheMenu.GetUserMenuFromDB(language, level);
            if(userMenuList.Count==0)
                userMenuList = cacheMenu.GetUserMenuFromDB("en-US", level);

            RecursionBuildTree(userMenuList, childNodes, userRoles);

            //Up find a node if need to show for the user
            foreach (UserMenuEntity entity in childNodes)
            {
                if (entity.ParentAaMeID != 0 && entity.UserMenuList.Count == 0)
                {
                    UpFindUserMenu(entity.ParentMenuEntity);
                }
            }

            return userMenuList;

        }

        public decimal AddMenu(decimal parentMenuID, string menuName, string menuDesc, byte orderNO, string language, string lastUser)
        {
            decimal menuID = 0;

            using (ConnectionScope.CreateTransaction())
            {
                AartoMenu menu = new AartoMenu();

                if (parentMenuID == 0)
                {
                    menu.AaParentMeId = null;

                }
                else
                {
                    menu.AaParentMeId = parentMenuID;
                }
                menu.AaMeOrderNo = orderNO;
                menu.LastUser = lastUser;
                menu = menuService.Save(menu);
                if (menu != null)
                {
                    AartoMenuLookup menuLookup = new AartoMenuLookup();
                    menuLookup.AaMeId = menu.AaMeId;
                    menuLookup.AaMlLanguage = language;
                    menuLookup.AaMlMenuItemDesc = menuDesc;
                    menuLookup.AaMlMenuItemName = menuName;
                    menuLookup.LastUser = lastUser;

                    menuLookup = new AartoMenuLookupService().Save(menuLookup);

                    if (menuLookup != null)
                    {
                        menuID = menu.AaMeId;
                    }
                }

                ConnectionScope.Complete();
            }
            return menuID;
        }

        public bool EditMenu(decimal menuID, string menuName, string menuDesc, byte orderNo, string language, string lastUser)
        {
            bool success = false;
            AartoMenuLookup menuLookup = menuLookupService.GetByAaMeIdAaMlLanguage(menuID, language);
            if (menuLookup == null)
            {
                return success;
            }
            using (ConnectionScope.CreateTransaction())
            {
                if (menuLookup.AaMlLanguage.ToLower().Trim() == language.ToLower().Trim())
                {
                    menuLookup.AaMlMenuItemName = menuName;
                    menuLookup.AaMlMenuItemDesc = menuDesc;
                    menuLookup.LastUser = lastUser;

                    menuLookupService.Save(menuLookup);

                    AartoMenu menu = menuService.GetByAaMeId(menuID);
                    if (menu != null)
                    {
                        menu.AaMeOrderNo = orderNo;
                        menu.LastUser = lastUser;
                        menuService.Save(menu);
                        success = true;
                    }

                }
                ConnectionScope.Complete();
            }

            return success;

        }

        public bool DeleteMenu(decimal menuID)
        {
            bool status = false;
            using (ConnectionScope.CreateTransaction())
            {
                try
                {
                    TList<AartoMenuLookup> menuLiikupList = GetMenulookupContainMenuID(menuID);
                    //AartoAction action = new AartoActionService().GetByAaMeId(menuID);
                    //if (action != null)
                    //{
                    //    action.AaMeId = null;
                    //    action = new AartoActionService().Save(action);
                    //}
                   
                    menuLiikupList = new AartoMenuLookupService().Delete(menuLiikupList);
                    //new AartoMenuLookupService().DeepLoad(menuLiikupList,true);
                    if (menuLiikupList != null)
                    {
                        AartoMenu menu = GetMenuByMenuID(menuID);
                        if (menu != null)
                        {
                            menu.MarkToDelete();
                            status = menuService.Delete(menu);
                        }
                    }
                }
                catch { status = false; }
                ConnectionScope.Complete();
            }
            return status;
        }

        public TList<AartoMenuLookup> GetMenulookupContainMenuID(decimal menuID)
        {
            int totalCount = 0;
            string where = String.Format("AaMeID IN (SELECT AaMeID FROM dbo.AARTOMenu WHERE " +
                " AaParentMeID={0} OR AaMeID={0})", menuID);
            return new AartoMenuLookupService().GetPaged(where, "AaMeID", 0, 0, out totalCount);
        }

        public AartoMenu GetMenuByMenuID(decimal menuID)
        {
            Type[] param = new Type[] { typeof(AartoMenu) };
            //return menuService.DeepLoadByAaMeId(menuID, true, SIL.AARTO.DAL.Data.DeepLoadType.IncludeChildren, param);
            return menuService.GetByAaMeId(menuID);
        }

        public AartoMenuLookup GetMenuLookUpByMenuIDAndLanguage(decimal menuID,string language)
        {
            return menuLookupService.GetByAaMeIdAaMlLanguage(menuID,language);
        }

        public byte GetMenuOrderNo(decimal? parentMenuID, string parentMenuName)
        {
            byte maxOrderNo = 0;
            int totalCount = 0;
            string where = String.Format("AaParentMeID IN ( SELECT AaMeID FROM dbo.AARTOMenuLookup " +
                " WHERE AaMeID = {0} AND AaMLMenuItemName='{1}')", parentMenuID, parentMenuName);
            TList<AartoMenu> menuList = menuService.GetPaged(where, "AaMeID", 0, 0, out totalCount);
            foreach (AartoMenu aartoMenu in menuList)
            {
                if (maxOrderNo < aartoMenu.AaMeOrderNo)
                {
                    maxOrderNo = aartoMenu.AaMeOrderNo;
                }
            }
            return maxOrderNo;
        }



        public TList<AartoUserRole> GetAllUserRole()
        {
            return new AartoUserRoleService().GetAll();
        }

        public TList<User> GetUserByRoleID(int roleID)
        {
            return new TList<User>();
        }

        private void UpFindUserMenu(UserMenuEntity parent)
        {
            bool visable = false;
            int index = 0;
            foreach (UserMenuEntity entity in parent.UserMenuList)
            {
                if (visable == entity.IsVisable)
                {
                    index++;
                }
            }
            if (parent.UserMenuList.Count == index && index != 0)
            {
                parent.IsVisable = false;
                if (parent.ParentMenuEntity != null)
                {
                    UpFindUserMenu(parent.ParentMenuEntity);
                }
            }

        }

        /// <summary>
        /// Using Recursion to build a tree menu for user
        /// </summary>
        /// <param name="nodes">The tree of nodes which is generated</param>
        /// <param name="childNodes">All child nodes in the tree</param>
        /// <param name="userRoles">The user roles </param>
        private void RecursionBuildTree(UserMenuEntityCollection nodes, UserMenuEntityCollection childNodes, List<int> userRoles)
        {
            foreach (UserMenuEntity userMenu in nodes)
            {
                if (userMenu.UserMenuList.Count > 0)
                {
                    userMenu.IsVisable = true;
                    RecursionBuildTree(userMenu.UserMenuList, childNodes, userRoles);
                }
                else
                {
                    if (userMenu.ParentMenuEntity == null)
                    {
                        userMenu.IsVisable = false;
                    }
                    else
                    {
                        childNodes.Add(userMenu);
                        //if (!CheckUserInRole(userRoles, userMenu.UserRole.Value))
                        if (!CheckUserInRole(userRoles, userMenu))
                        {
                            userMenu.IsVisable = false;
                            //nodes2.Remove(userMenu);
                        }
                        else
                        {
                            userMenu.IsVisable = true;
                        }
                    }
                }
            }

        }


        private bool CheckUserInRole(List<int> userRoles, int userRole)
        {
            bool flag = false;
            for (int index = 0; index < userRoles.Count; index++)
            {
                if (userRoles[index] == userRole)
                {
                    flag = true;
                    break;
                }
            }

            return flag;
        }

        private bool CheckUserInRole(List<int> userRoles, UserMenuEntity userMenu)
        {
            if (userMenu.IsAarto.Value)
            {
                //if (!userRoles.Contains(userMenu.UserRole.Value))
                //{
                //    return false;
                //}
                TList<AartoActionRoleConjoin> aarcList = new AartoActionRoleConjoinService().GetAll();
                AartoAction aartoAction = new AartoActionService().GetByAaPageId(Convert.ToInt32(userMenu.PageID));
                List<int> roles = new List<int>();
                if (aartoAction != null)
                {
                    foreach (AartoActionRoleConjoin cc in aarcList)
                    {
                        if (cc.AaActId == aartoAction.AaActId)
                        {
                            roles.Add(cc.AaUserRoleId);
                        }
                    }
                }
                foreach (int role in roles)
                {
                    if (userRoles.Contains(role))
                    {
                        return true;
                    }
                }

            }
            else
            {
                return userRoles.Contains(userMenu.UserRole.Value);
            }

            return false;
        }

        private UserMenuEntityCollection GetUserMenuCollection(string language, int level)
        {
            UserMenuEntityCollection returnCollection = new UserMenuEntityCollection();
            UserMenuEntityCollection containerCollection = new UserMenuEntityCollection();
            UserMenuEntityCollection currentCollection = new UserMenuEntityCollection();
            //int currentLevel = 0;
            int oldlevel = 2;

            using (IDataReader reader = menuService.GenerateUserMenuByLanguageAndLevel(language, level))
            {
                while (reader.Read())
                {
                    UserMenuEntity menu = GetUserMenuEntityFromDataReader(reader);

                    if (menu.Level == 1)
                    {
                        returnCollection.Add(menu);
                        containerCollection.Add(menu);
                    }
                    else
                    {
                        if (oldlevel != menu.Level)
                        {
                            containerCollection.Clear();

                            foreach (UserMenuEntity menuEntity in currentCollection)
                            {
                                containerCollection.Add(menuEntity);
                            }
                            currentCollection.Clear();
                            oldlevel = menu.Level;
                        }

                        currentCollection.Add(menu);

                        CreateUserMenu(containerCollection, menu);
                    }
                }
            }

            return returnCollection;
        }

        private void CreateUserMenu(UserMenuEntityCollection container, UserMenuEntity menu)
        {
            foreach (UserMenuEntity menuEntity in container)
            {
                if (menuEntity.AaMeID == menu.ParentAaMeID)
                {
                    menu.ParentMenuEntity = menuEntity;
                    menuEntity.UserMenuList.Add(menu);
                    break;
                }
            }
        }

        private UserMenuEntity GetUserMenuEntityFromDataReader(IDataReader reader)
        {
            UserMenuEntity userMenu = new UserMenuEntity();
            userMenu.AaMeID = Convert.ToDecimal(reader["AaMeID"]);
            userMenu.ParentAaMeID = reader["AaParentMeIDMeID"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["AaParentMeIDMeID"]);
            userMenu.AMLMenuItemName = reader["AaMLMenuItemName"].ToString();
            userMenu.PageID = reader["AaPageID"].ToString();
            userMenu.UserRole = reader["AaUserRoleID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AaUserRoleID"]);
            userMenu.Level = Convert.ToInt32(reader["Level"]);
            userMenu.AaMeOrderNo = Convert.ToInt32(reader["AaMeOrderNo"]);
            userMenu.IsVisable = true;
            userMenu.IsAarto = reader["IsAARTO"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsAARTO"]);
            userMenu.AaMePageURL = reader["AaPageURL"].ToString();
            userMenu.IsNewWindow = reader["IsNewWindow"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsNewWindow"]);
            return userMenu;
        }

        private static void GetUserMenuCollectionFromTreecollection(UserMenuEntityCollection treeCollection, ref ArrayList list)
        {
            foreach (UserMenuEntity userMenu in treeCollection)
            {
                list.Add(userMenu);

                if (userMenu.UserMenuList.Count > 0)
                {
                    GetUserMenuCollectionFromTreecollection(userMenu.UserMenuList, ref list);
                }
            }
        }



    }
}
