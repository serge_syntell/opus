﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace SIL.AARTO.BLL.Utility
{
    public class AARTOMenulistLookupDB
    {
        public static DataSet GetListByAaMeID(decimal AaMeID)
        {
            DataSet ds = new DataSet();

            string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();

            SqlConnection myConnection = new SqlConnection(connStr);
            SqlCommand myCommand = new SqlCommand("AARTOMenuListLookupGetListByAaMeID", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAaMenuID = new SqlParameter("@AaMeID", SqlDbType.Int, 4);
            parameterAaMenuID.Value = AaMeID;
            myCommand.Parameters.Add(parameterAaMenuID);

            SqlDataAdapter sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }
            return ds;
        }
    }
}
