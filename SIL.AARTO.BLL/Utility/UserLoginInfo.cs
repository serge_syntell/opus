﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility
{
    [Serializable]
    public class UserLoginInfo
    {
        public int UserIntNo { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public int UserAccessLevel { get; set; }
        public int AuthIntNo { get; set; }
        public List<int> UserRoles { get; set; }
        //Jerry 2012-06-12 add
        public bool UserPasswordReset { get; set; }
        public int UserPasswordGraceLoginCount { get; set; }
        public DateTime UserPasswordExpiryDate { get; set; }
        //Henry 2013-01-24
        public string CurrentLanguage { get; set; }
    }
}
