﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility
{
    public class Message
    {
        public string Text { get; set; }
        public bool Status { get; set; }
        public string User { get; set; }

        public Message()
        {
            Status = false;
        }
    }
}
