﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Services;
namespace SIL.AARTO.BLL.Utility
{
    public class AARTODocumentSourceTable
    {
         public static AartoDocumentSourceTableList IsForNotice(int docTypeID)
         {
             AartoDocumentType docType = new AartoDocumentTypeService().GetByAaDocTypeId(docTypeID);
             return (AartoDocumentSourceTableList)docType.AaDocSourceTableId;    
         }
    }
}
