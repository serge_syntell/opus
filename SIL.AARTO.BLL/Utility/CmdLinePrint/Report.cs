﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public abstract class Report
    {
        protected LPT LPT;
        protected FormatFactory FormatFactory;
        protected string LPTName;

        public Report(string lptName)
        {
            LPT = new LPT(lptName);
            FormatFactory = new FormatFactory();
            this.LPTName = lptName;
        }

        protected T GetValue<T>(DataRow dr, string columnName)
        {
            var value = dr[columnName];
            if (value == DBNull.Value)
                return default(T);
            else
            {
                var type = typeof(T);
                if (type.IsGenericType && typeof(Nullable<>) == type.GetGenericTypeDefinition())
                    type = new NullableConverter(type).UnderlyingType;
                return (T)Convert.ChangeType(value, type);
            }
        }

        public abstract void SendToPrint(DataTable dtReport);

        //protected abstract void BuildReport(DataRow dr);2013-08-30 Removed by Nancy

        protected abstract void BuildReport(DataRow dr, int RowIndex);//2013-08-30 Added by Nancy(Add the current row number)
    }
}
