﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class CPA5Report : Report
    {
        public CPA5Report(string lptName)
            : base(lptName)
        {
        }

        public override void SendToPrint(DataTable dtReport)
        {
            if (dtReport != null && dtReport.Rows.Count > 0)
            {
                //foreach (DataRow dr in dtReport.Rows)2013-08-30 Removed by Nancy
                for (int i = 0; i < dtReport.Rows.Count; i++)//2013-08-30 added by Nancy
                {
                    DataRow dr = dtReport.Rows[i];//2013-08-30 added by Nancy
                    BuildReport(dr,i);

                    Thread.Sleep(400);
                }
            }
        }

        //2013-08-30 Modified by Nancy(Add parameter RowIndex for the number of current row)
        protected override void BuildReport(System.Data.DataRow dr,int RowIndex)
        {
            if (dr != null)
            {
                Print(dr,RowIndex);

            }
        }
        //2013-08-30 Modified by Nancy(Add parameter RowIndex for the number of current row)
        void Print(DataRow dr, int RowIndex)
        {
            string commandLine = "";
            //lpt.Send(commandLine);

            //PurposeSets the current paper position as the top-of-form.
            //commandLine += "\x1B\x34";
            ////  set form length to 12 inches
            int formLength = Convert.ToInt32(12);
            commandLine += "\x1B\x43\x00" + (char)(formLength);
            //commandLine += (char)(27)+(char)(67) + (char)(0) + (char)(12);
            //27 67

            //Character Pitch 12 cpi
            commandLine += "\x1B\x3a";
            //lpt.Send(commandLine);

            //FilmNo
            //commandLine += FormatDescr(GetValue<string>(dr, "SumFilmNo"), 63, 10) + "\x0d\x0a ";
            commandLine += FormatFactory.FormatDescr(
                new FormatContent((RowIndex + 1).ToString(), new FormatOption() { Retract = 14, WordsNum = 9, Rows = 1 })
                ,new FormatContent("F: " + GetValue<string>(dr, "SumFilmNo"), new FormatOption() { Retract = 50, WordsNum = 10, Rows = 1 }));//2013-08-28 Modified by Nancy(Add 'F: ')
            //SurName
            //commandLine += FormatDescr(GetValue<string>(dr, "AccSurname"), 14, 20) + "\x0d\x0a";
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "AccSurname"), new FormatOption() { Retract = 16, WordsNum = 20, Rows = 1 }));
            //commandLine += "\x0d\x0a";

            //commandLine += FormatDescr(GetValue<string>(dr, "AccForenames"), 14, 10)
            //    + FormatDescr(GetValue<string>(dr, "SumNoticeNo"), 32, 20) + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<string>(dr, "AccForenames"), new FormatOption() { Retract = 16, WordsNum = 10, Rows = 1 })
                , new FormatContent(GetValue<string>(dr, "SumNoticeNo"), new FormatOption() { Retract = 46, WordsNum = 20, Rows = 1 })
                );

            //lpt.Send(commandLine);
            //commandLine += "\x0d\x0a";
            //commandLine += FormatFactory.WriteCR(1);

            //commandLine += FormatDescr(GetValue<string>(dr, "AccIDNumber"), 12, 15) + WriteSpace(10)
            //    + FormatDescr(GetValue<string>(dr, "AccAge"), 10, 3) + WriteSpace(6)
            //    + FormatDescr(GetValue<string>(dr, "AccSex"), 3, 2)
            //    + FormatDescr(GetValue<string>(dr, "AccOver18"), 2, 12) + "\x0d\x0a";

            commandLine += FormatFactory.WriteCR(1);//2013-08-28 added by Nancy
            commandLine += FormatFactory.FormatDescr(
                FormatFactory.MergeFormatContent(
                    new FormatContent(GetValue<string>(dr, "AccIDNumber"),
                        new FormatOption() { Retract = 14, WordsNum = 15, Rows = 1 }),
                    new FormatContent(GetValue<string>(dr, "AccAge"),
                        new FormatOption() { Retract = 10, WordsNum = 3, Rows = 1 })),
               FormatFactory.MergeFormatContent(
                   new FormatContent(GetValue<string>(dr, "AccSex"),
                       new FormatOption() { Retract = 3, WordsNum = 2, Rows = 1 }),
                   new FormatContent(GetValue<string>(dr, "AccOver18"),
                       new FormatOption() { Retract = 2, WordsNum = 12, Rows = 1 })));


            //commandLine += "\x0d\x0a";
            //commandLine += FormatFactory.WriteCR(1);2013-08-28 Removed by Nancy

            //commandLine += FormatDescr(GetValue<string>(dr, "StreetAddress"), 24, 30)
            //    + FormatDescr(GetValue<string>(dr, "NotVehicleMake"), 8, 12) + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "StreetAddress"), new FormatOption() { Retract = 18, WordsNum = 20, Rows = 3 })
               , new FormatContent(GetValue<string>(dr, "NotVehicleMake"), new FormatOption() { Retract = 8, WordsNum = 12, Rows = 2 })
               );



            //commandLine += FormatDescr(GetValue<string>(dr, "StreetCode"), 62, 5) + "\x0d\x0a";
            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<string>(dr, "StreetCode"), new FormatOption() { Retract = 60, WordsNum = 5, Rows = 1 }));

            //commandLine += FormatDescr(GetValue<string>(dr, "PostalAddress"), 24, 20)
            //    + FormatDescr(GetValue<string>(dr, "NotVehicleType"), 8, 12)
            //    + FormatDescr(GetValue<string>(dr, "SummonsNo"), 8, 20) + "\x0d\x0a";


            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<string>(dr, "PostalAddress"),
                    new FormatOption() { Retract = 18, WordsNum = 20, Rows = 3 }),
                new FormatContent(GetValue<string>(dr, "NotVehicleType"),
                    new FormatOption() { Retract = 8, WordsNum = 10, Rows = 2 }),
                new FormatContent(GetValue<string>(dr, "SummonsNo"),
                    new FormatOption() { Retract = 19, WordsNum = 20, Rows = 1 }));//2013-08-28 Modified by Nancy from 'Retract = 16' to 'Retract = 19' 

            //commandLine += FormatDescr(GetValue<string>(dr, "PostCode"), 62, 5) + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "PostCode"), new FormatOption() { Retract = 60, WordsNum = 5, Rows = 1 }));


            //commandLine += "\x1b \x5c 100 50";
            //commandLine += "ABC";
            //commandLine += "\x0a \x0a \x0a \x0a \x0a\x0a\x0a\x0d";
            commandLine += FormatFactory.WriteCR(3);//2013-08-28 Modified by Nancy from '2' to '3'

            //commandLine += WriteSpace(6) + FormatDescr(GetValue<string>(dr, "StatutoryRef"), 6, 60) + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "StatutoryRef"), new FormatOption() { Retract = 12, WordsNum = 70, Rows = 3 })); //2013-08-28 Modified by Nancy from 'Rows = 4' to 'Rows = 3'

            //commandLine += WriteSpace(18) + GetValue<DateTime>(dr, "SumOffenceDate").ToString("yyyy/MM/dd HH:mm:ss") + WriteSpace(21) + GetValue<string>(dr, "SumLocDescr1_1") + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<DateTime>(dr, "SumOffenceDate").ToString("dd MMM yyyy HH:mm"), new FormatOption() { Retract = 28, WordsNum = 21, Rows = 1 })
              , new FormatContent(GetValue<string>(dr, "SumLocDescr1_1"), new FormatOption() { Retract = 21, WordsNum = 20, Rows = 2 }));

            //commandLine += FormatFactory.WriteCR(1); 2013-08-28 Removed by Nancy
            //commandLine += WriteSpace(6) + FormatDescr(GetValue<string>(dr, "SumLocDescr1_2"), 6, 60) + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "SumLocDescr1_2"), new FormatOption() { Retract = 16, WordsNum = 60, Rows = 2 }));


            //commandLine += WriteSpace(6) + FormatDescr(GetValue<string>(dr, "OffenceDescrEng"), 6, 60) + "\x0d\x0a";

            commandLine += FormatFactory.WriteCR(1);//2013-08-28 added by Nancy 
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "OffenceDescrEng"), new FormatOption() { Retract = 12, WordsNum = 70, Rows = 4 }));

            //commandLine += WriteSpace(58) + GetValue<string>(dr, "SChCode") + "\x0d\x0a";
            commandLine += FormatFactory.WriteCR(1);//2013-08-28 Added by Nancy
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SChCode"), new FormatOption() { Retract = 86, WordsNum = 6, Rows = 1 }));

            //commandLine += WriteSpace(6) + FormatDescr(GetValue<string>(dr, "OffenceDescrAfr"), 6, 60) + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "OffenceDescrAfr"), new FormatOption() { Retract = 12, WordsNum = 70, Rows = 4 }));

            //commandLine += WriteSpace(58) + GetValue<string>(dr, "SumOfficerNo") + "\x0d\x0a";

            commandLine += FormatFactory.WriteCR(1);//2013-08-28 Modified by Nancy from '2' to '1'

            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SumOfficerNo"), new FormatOption() { Retract = 83, WordsNum = 10, Rows = 1 }));//2013-08-28 Modified by Nancy from 'Retract = 86' to 'Retract = 83'

            //commandLine += WriteSpace(24) + GetValue<DateTime>(dr, "SumCourtDate").ToString("yyyy/MM/dd") + WriteSpace(10) + GetValue<string>(dr, "CourtName") + WriteSpace(10) + GetValue<string>(dr, "CrtNo") + "\x0d\x0a";

            commandLine += FormatFactory.WriteCR(5);//2013-08-28 Modified by Nancy from '7' to '5'

            commandLine += FormatFactory.FormatDescr(
                FormatFactory.MergeFormatContent(
                     new FormatContent(GetValue<DateTime>(dr, "SumCourtDate").ToString("dd MMMMM yyyy"), new FormatOption() { Retract = 30, WordsNum = 10, Rows = 1 })
                     , new FormatContent(GetValue<string>(dr, "CourtName"), new FormatOption() { Retract = 25, WordsNum = 20, Rows = 1 })),
                new FormatContent(GetValue<string>(dr, "CrtNo"), new FormatOption() { Retract = 8, WordsNum = 2, Rows = 1 }));

            //commandLine += "\x0a \x0a \x0d";

            commandLine += FormatFactory.WriteCR(3);//2013-08-28 Modified by Nancy from '2' to '3'

            //commandLine += WriteSpace(30) + GetValue<int>(dr, "FineAmount") + WriteSpace(20) + GetValue<DateTime>(dr, "SumLastAOGDate").ToString("yyyy/MM/dd") + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
             new FormatContent('*'+GetValue<string>(dr, "FineAmount"), new FormatOption() { Retract = 30, WordsNum = 5, Rows = 1 })//2013-08-28 Modified by Nancy(Add '*')
             , new FormatContent(GetValue<DateTime>(dr, "SumLastAOGDate").ToString("dd MMM yyyy"), new FormatOption() { Retract = 32, WordsNum = 15, Rows = 1 }));//2013-08-28 Modified by Nancy from '10' to '15'


            //commandLine += WriteSpace(6) + FormatDescr(GetValue<string>(dr, "AuthorityAddress"), 6, 20) + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
             new FormatContent(GetValue<string>(dr, "AuthorityAddress"), new FormatOption() { Retract = 16, WordsNum = 30, Rows = 3 }));


            //commandLine += "\x0a \x0a \x0d";
            commandLine += FormatFactory.WriteCR(1);//2013-08-28 Modified by Nancy from '2' to '1'


            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<string>(dr, "CrtName"), new FormatOption() { Retract = 18, WordsNum = 40, Rows = 1 })
                , new FormatContent(GetValue<DateTime>(dr, "SumPrintSummonsDate").ToString("dd MMM yyyy"), new FormatOption() { Retract = 2, WordsNum = 15, Rows = 1 })
                );//2013-08-28 Modified by Nancy from 'WordsNum = 60' to 'WordsNum = 40'&'WordsNum = 10' to 'WordsNum = 15'


            //commandLine += "\x0a \x0a \x0a \x0a \x0a \x0a \x0a \x0a \x0a \x0a \x0a \x0a \x0d";
            commandLine += FormatFactory.WriteCR(19);//2013-08-28 Modified by Nancy from '15' to '19'

            // commandLine += WriteSpace(11) + GetValue<string>(dr, "AutName") + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "AutName"), new FormatOption() { Retract = 20, WordsNum = 20, Rows = 1 }));


            //commandLine += WriteSpace(11) + GetValue<DateTime>(dr, "SumServeByDate").ToString("yyyy/MM/dd") + "\x0d\x0a";

            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<DateTime>(dr, "SumServeByDate").ToString("dd MMM yyyy"), new FormatOption() { Retract = 20, WordsNum = 15, Rows = 1 }));//2013-08-28 Modified by Nancy from '10' to '15'


            //commandLine += "\x1B\x4A 54\x0d";
            //Form feed
            commandLine += "\x0c";
            //Jacob, call extracted print method
            PrintToLPT(commandLine);
            
        }

        //jacob, extract print method for out calling.
        public void PrintToLPT(string commandLine)
        {
            try
            {
                LPT.Send(commandLine);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Unable to find port {0}, Error: {1}", this.LPTName, ex.Message));
            }
        }
    }
}
