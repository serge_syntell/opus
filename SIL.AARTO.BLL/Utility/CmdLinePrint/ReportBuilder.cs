﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportBuilder
    {

        string _LPTName;
        int _RepType;
        public ReportBuilder(string lptName, int repType)
        {
            this._RepType = repType;

            this._LPTName = lptName;
        }

        public void Print(DataTable dtReport)
        {
            Report report = null;
            switch (_RepType)
            {
                case (int)ReportConfigCodeList.CPA5:
                    report = new CPA5Report(this._LPTName);
                    break;
                case (int)ReportConfigCodeList.CPA6:
                    report = new CPA6Report(this._LPTName);
                    break;
                default:
                    break;
            }

            if (report == null)
            {
                throw new NotImplementedException();
            }
            report.SendToPrint(dtReport);
        }
    }
}
