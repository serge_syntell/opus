﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public struct FormatOption
    {
        public int Retract;
        public int WordsNum;
        public int Rows;
    }

    public class FormatContent
    {
        private string input;
        public string Input
        {
            get
            {
                return String.IsNullOrEmpty(input) ? "" : input;
            }
            set
            {
                input = value;
            }
        }
        public FormatOption Option { get; set; }
        public FormatContent(string input, FormatOption option)
        {
            this.Input = input;
            this.Option = option;
        }
    }

    public class FormatFactory
    {

        const char pi = (char)960;
        const string space = "\x20";

        public static string WriteSpace(int num)
        {
            string returnValue = "";
            for (int i = 1; i <= num; i++)
                returnValue += space;

            return returnValue;
        }

        public static string WriteCR(int rows)
        {
            string returnValue = "";
            for (int i = 1; i <= rows; i++)
                returnValue += "\x0d\x0a ";

            return returnValue;
        }

        /// <summary>
        /// Format message with control code for IMB printer
        /// </summary>
        /// <param name="content1">message1</param>
        /// <param name="content2">message1 (option)</param>
        /// <param name="content3">message1 (option)</param>
        /// <returns></returns>
        public static StringBuilder FormatDescr(FormatContent content1, FormatContent content2 = null, FormatContent content3 = null)
        {
            StringBuilder sb = new StringBuilder();

            if (content1 == null) return new StringBuilder("");

            FormatOption option1, option2, option3;
            option1 = option2 = option3 = new FormatOption();

            option1 = content1.Option;

            string[] contentArr1 = FillWithSpace(SplitContent(content1).Input.Split(pi), option1);
            string[] contentArr2 = new string[] { };
            string[] contentArr3 = new string[] { };
            if (content2 != null)
            {
                option2 = content2.Option;
                contentArr2 = FillWithSpace(SplitContent(content2).Input.Split(pi), option2);
                if (content3 != null)
                {
                    option3 = content3.Option;
                    contentArr3 = FillWithSpace(SplitContent(content3).Input.Split(pi), option3);
                }
            }


            if (contentArr1.Length >= contentArr2.Length && contentArr1.Length >= contentArr3.Length && contentArr1.Length > 1)
            {

                for (int i = 0; i < contentArr1.Length; i++)
                {
                    sb.Append(WriteSpace(option1.Retract)).Append(contentArr1[i].TrimStart());

                    if (contentArr2.Length >= i + 1)
                    {
                        sb.Append(WriteSpace(option2.Retract)).Append(contentArr2[i].TrimStart());
                    }
                    else
                    {
                        if (content2 != null)
                            sb.Append(WriteSpace(content2.Option.Retract + content2.Option.WordsNum));
                    }
                    if (contentArr3.Length >= i + 1)
                    {
                        sb.Append(WriteSpace(option3.Retract)).Append(contentArr3[i].TrimStart());
                    }
                    else
                    {
                        if (content3 != null)
                            sb.Append(WriteSpace(content3.Option.Retract + content3.Option.WordsNum));
                    }
                    sb.Append(WriteCR(1));
                }
                if (contentArr1.Length < content1.Option.Rows)
                {
                    sb.Append(WriteCR(content1.Option.Rows - contentArr1.Length));
                }

            }
            else if (contentArr2.Length > contentArr1.Length && contentArr2.Length >= contentArr3.Length && contentArr2.Length > 1)
            {
                for (int i = 0; i < contentArr2.Length; i++)
                {
                    if (contentArr1.Length >= i + 1)
                    {
                        sb.Append(WriteSpace(option1.Retract)).Append(contentArr1[i].TrimStart());
                    }
                    else
                    {
                        if (content1 != null)
                            sb.Append(WriteSpace(content1.Option.Retract + content1.Option.WordsNum));
                    }

                    sb.Append(WriteSpace(option2.Retract)).Append(contentArr2[i].TrimStart());

                    if (contentArr3.Length >= i + 1)
                    {
                        sb.Append(WriteSpace(option3.Retract)).Append(contentArr3[i].TrimStart());
                    }
                    else
                    {
                        if (content3 != null)
                            sb.Append(WriteSpace(content3.Option.Retract + content3.Option.WordsNum));
                    }
                    sb.Append(WriteCR(1));
                }
                if (contentArr2.Length < content2.Option.Rows)
                {
                    sb.Append(WriteCR(content2.Option.Rows - contentArr2.Length));
                }
            }
            else if (contentArr3.Length >= contentArr1.Length && contentArr3.Length >= contentArr2.Length && contentArr3.Length > 1)
            {
                for (int i = 0; i < contentArr3.Length; i++)
                {

                    if (contentArr1.Length >= i + 1)
                    {
                        sb.Append(WriteSpace(option1.Retract)).Append(contentArr1[i].TrimStart());
                    }
                    else
                    {
                        if (content1 != null)
                            sb.Append(WriteSpace(content1.Option.Retract + content1.Option.WordsNum));
                    }
                    if (contentArr2.Length >= i + 1)
                    {
                        sb.Append(WriteSpace(option2.Retract)).Append(contentArr2[i].TrimStart());
                    }
                    else
                    {
                        if (content2 != null)
                            sb.Append(WriteSpace(content2.Option.Retract + content2.Option.WordsNum));
                    }

                    sb.Append(WriteSpace(option3.Retract)).Append(contentArr3[i].TrimStart());

                    sb.Append(WriteCR(1));

                    if (contentArr3.Length < content3.Option.Rows)
                    {
                        sb.Append(WriteCR(content3.Option.Rows - contentArr3.Length));
                    }
                }
            }
            else
            {
                sb.Append(WriteSpace(content1.Option.Retract)).Append(GetDescrFromArray(contentArr1, content1.Option, true));
                if (content2 != null)
                {
                    sb.Append(WriteSpace(content2.Option.Retract)).Append(GetDescrFromArray(contentArr2, content2.Option, true));
                }
                if (content3 != null)
                {
                    sb.Append(WriteSpace(content3.Option.Retract)).Append(GetDescrFromArray(contentArr3, content3.Option, true));
                }

                if (contentArr1.Length < content1.Option.Rows)
                {
                  sb.Append(WriteCR(content1.Option.Rows - contentArr1.Length));
                }
                sb.Append(WriteCR(1));
            }

            return sb;
        }

        /// <summary>
        /// Merge two messages into one description
        /// </summary>
        /// <param name="content1">message 1</param>
        /// <param name="content2">message 2</param>
        /// <returns></returns>
        public static string MergeDescr(FormatContent content1, FormatContent content2)
        {
            string content = "";
            if (content1 != null && content2 != null)
            {
                content = WriteSpace(content1.Option.Retract)
                    + FillWithSpace(content1)
                    + FillWithSpace(content2);

            }

            return content;
        }

        /// <summary>
        /// Merge two messages into one description
        /// </summary>
        /// <param name="content1">message 1</param>
        /// <param name="content2">message 2</param>
        /// <returns>This function return a object FormatContent</returns>
        public static FormatContent MergeFormatContent(FormatContent content1, FormatContent content2)
        {
            string content = "";
            if (content1 != null && content2 != null)
            {
                content = FillWithSpace(content1)
                    + FillWithSpace(content2);

            }

            return new FormatContent(content,
                new FormatOption() { Retract = 0, WordsNum = content.Length, Rows = 1 });
        }

        /// <summary>
        /// Splite message(use character pi)
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        static FormatContent SplitContent(FormatContent content)
        {
            if (content == null) return content;

            content.Input = content.Input.Replace("\r", " ").Replace("\n", "");
            if (content.Input.Length > content.Option.WordsNum)
            {
                int inputLength = content.Input.Length;
                int piAddedCount = 0;

                for (int i = content.Option.WordsNum; i < inputLength; i += content.Option.WordsNum + piAddedCount)
                {
                    if (i >= content.Input.Length) break;
                    if (content.Input[i] != '\x20')
                    {
                        i = content.Input.Substring(0, i).LastIndexOf("\x20");
                    }
                    //EntLibLogger.WriteLog("General", null, "Execute insert function : Value= " + content.Input + "  content.Input.Insert(" + i + ", String.Format(\"{0}\"," + pi + "))");
                    if (i < 0) break;
                    content.Input = content.Input.Insert(i, String.Format("{0}", pi));
                    piAddedCount += 1;

                }

            }

            return content;
        }

        static string GetDescrFromArray(string[] arr, FormatOption option, bool crFlag = false)
        {
            string returnValue = "";
            if (arr == null || arr.Length == 0) return returnValue;
            else
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    returnValue += (crFlag == true && arr.Length > 1 ? WriteSpace(option.Retract) : "")
                        + arr[i] + (crFlag == true && arr.Length > 1 ? "\x0d\x0a" : "");
                }
            }

            return returnValue;
        }

        public static string[] FillWithSpace(string[] arr, FormatOption option)
        {
            string[] returnArra;
            if (arr == null || arr.Length == 0) return new string[] { };

            returnArra = new string[arr.Length];

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].Length < option.WordsNum)
                {
                    arr[i] = arr[i] + WriteSpace(option.WordsNum - arr[i].Length);
                }

                returnArra[i] = arr[i];

            }

            return returnArra;
        }

        public static string FillWithSpace(FormatContent content)
        {
            if (content == null) return "";
            string descr = string.Empty;
            if (!String.IsNullOrEmpty(content.Input) && content.Input.Length < content.Option.WordsNum)
            {
                descr = WriteSpace(content.Option.Retract)
                    + content.Input
                    + WriteSpace(content.Option.WordsNum - content.Input.Length);
            }
            else
            {
                descr = WriteSpace(content.Option.Retract)
                    + content.Input;
            }

            return descr;
        }

    }
}
