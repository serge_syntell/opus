﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading;


namespace SIL.AARTO.BLL.Utility.Printing
{
    public class CPA6Report : Report
    {
        public CPA6Report(string lptName)
            : base(lptName)
        {
        }

        public override void SendToPrint(DataTable dtReport)
        {
            if (dtReport != null && dtReport.Rows.Count > 0)
            {
                //foreach (DataRow dr in dtReport.Rows)2013-08-30 Removed by Nancy
                for (int i = 0; i < dtReport.Rows.Count; i++)//2013-08-30 added by Nancy
                {
                    DataRow dr = dtReport.Rows[i];//2013-08-30 added by Nancy
                    BuildReport(dr,i);

                    Thread.Sleep(400);
                }
            }
        }
        //2013-08-30 Modified by Nancy(Add parameter RowIndex for the number of current row)
        protected override void BuildReport(System.Data.DataRow dr, int RowIndex)
        {
            if (dr != null)
            {
                Print(dr, RowIndex);

            }
        }
        /// <summary>
        /// Print the report CPA6
        /// </summary>
        /// <param name="dr">the data source</param>
        void Print(DataRow dr, int RowIndex)
        {
            string commandLine = "";
            //  set form length to 12 inches
            int formLength = Convert.ToInt32(12);

            #region Start the first page
            commandLine += "\x1B\x43\x00" + (char)(formLength);
            //Character Pitch 12 cpi
            commandLine += "\x1B\x3a";
            //Set place holder 'AccName&SumFilmNo'
            commandLine += FormatFactory.FormatDescr(
                new FormatContent((RowIndex + 1).ToString(), new FormatOption() { Retract = 12, WordsNum = 3, Rows = 1 })//2013-08-30 Added by Nancy
                ,new FormatContent(GetValue<string>(dr, "AccName"), new FormatOption() { Retract = 1, WordsNum = 30, Rows = 1 })//'13'->'1'
                , new FormatContent("F: " + GetValue<string>(dr, "SumFilmNo"), new FormatOption() { Retract = 27, WordsNum = 10, Rows = 1 })//'30'->'27'
                );
            //Set blank row
            commandLine += FormatFactory.WriteCR(2);
            //Set place holder 'SumNoticeNo'
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "SumNoticeNo"), new FormatOption() { Retract = 72, WordsNum = 20, Rows = 1 })
                );
            commandLine += FormatFactory.WriteCR(1);            
            commandLine += FormatFactory.FormatDescr(
                FormatFactory.MergeFormatContent(
                    new FormatContent(GetValue<string>(dr, "AccIDNumber"),
                        new FormatOption() { Retract = 10, WordsNum = 15, Rows = 1 }),
                    new FormatContent(GetValue<string>(dr, "AccAge"),
                        new FormatOption() { Retract = 12, WordsNum = 3, Rows = 1 })),
               FormatFactory.MergeFormatContent(
                   new FormatContent(GetValue<string>(dr, "AccSex"),
                       new FormatOption() { Retract = 5, WordsNum = 2, Rows = 1 }),
                   new FormatContent(GetValue<string>(dr, "AccOver18"),
                       new FormatOption() { Retract = 9, WordsNum = 12, Rows = 1 })));

            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "StreetAddress"), new FormatOption() { Retract = 17, WordsNum = 20, Rows = 3 })
               , new FormatContent(GetValue<string>(dr, "NotVehicleMake"), new FormatOption() { Retract = 13, WordsNum = 12, Rows = 2 })
               );

            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<string>(dr, "StreetCode"), new FormatOption() { Retract = 58, WordsNum = 5, Rows = 1 }));

            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<string>(dr, "PostalAddress"),
                    new FormatOption() { Retract = 17, WordsNum = 20, Rows = 3 }),
                new FormatContent(GetValue<string>(dr, "NotVehicleType"),
                    new FormatOption() { Retract = 13, WordsNum = 10, Rows = 2 }),
                new FormatContent(GetValue<string>(dr, "SummonsNo"),
                    new FormatOption() { Retract = 14, WordsNum = 20, Rows = 1 }));

            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "PostCode"), new FormatOption() { Retract = 58, WordsNum = 5, Rows = 1 }));

            commandLine += FormatFactory.WriteCR(4);//'3' -> '4'
            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "Description"), new FormatOption() { Retract = 12, WordsNum = 82, Rows = 7 }));

            commandLine += FormatFactory.WriteCR(7);//'8' -> '7'
            commandLine += FormatFactory.FormatDescr(
                FormatFactory.MergeFormatContent(
                     new FormatContent(GetValue<DateTime>(dr, "SumCourtDate").ToString("dd MMM yyyy"), new FormatOption() { Retract = 23, WordsNum = 15, Rows = 1 })
                     , new FormatContent(GetValue<string>(dr, "CourtName"), new FormatOption() { Retract = 18, WordsNum = 20, Rows = 1 })),
                new FormatContent(GetValue<string>(dr, "CrtRoomNo"), new FormatOption() { Retract = 6, WordsNum = 2, Rows = 1 }));

            commandLine += FormatFactory.WriteCR(3);
            commandLine += FormatFactory.FormatDescr(
             new FormatContent(GetValue<string>(dr, "MainPageChargeNo1"), new FormatOption() { Retract = 30, WordsNum = 5, Rows = 1 })
             , new FormatContent(GetValue<string>(dr, "MainPageChargeNo2"), new FormatOption() { Retract = 2, WordsNum = 5, Rows = 1 })
             , new FormatContent(GetValue<string>(dr, "MainPageChargeNo3"), new FormatOption() { Retract = 2, WordsNum = 5, Rows = 1 })
             );
            commandLine += FormatFactory.WriteCR(1);
            commandLine += FormatFactory.FormatDescr(
             new FormatContent(GetValue<string>(dr, "FineAmount1"), new FormatOption() { Retract = 30, WordsNum = 5, Rows = 1 })
             , new FormatContent(GetValue<string>(dr, "FineAmount2"), new FormatOption() { Retract = 2, WordsNum = 5, Rows = 1 })
             , new FormatContent(GetValue<string>(dr, "FineAmount3"), new FormatOption() { Retract = 2, WordsNum = 5, Rows = 1 })
             );

            commandLine += FormatFactory.WriteCR(1);
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<DateTime>(dr, "SumLastAOGDate").ToString("dd MMM yyyy"), new FormatOption() { Retract = 40, WordsNum = 15, Rows = 1 }));

            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "AuthorityAddress"), new FormatOption() { Retract = 16, WordsNum = 60, Rows = 3 }));

            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "AutTel"), new FormatOption() { Retract = 76, WordsNum = 15, Rows = 1 }));

            commandLine += FormatFactory.WriteCR(2);
            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<string>(dr, "CrtName"), new FormatOption() { Retract = 18, WordsNum = 40, Rows = 1 })
                , new FormatContent(GetValue<DateTime>(dr, "SumPrintSummonsDate").ToString("dd MMM yyyy"), new FormatOption() { Retract = 2, WordsNum = 15, Rows = 1 })
                );

            commandLine += FormatFactory.WriteCR(20);
            commandLine += FormatFactory.FormatDescr(
               new FormatContent(GetValue<string>(dr, "AutName"), new FormatOption() { Retract = 17, WordsNum = 30, Rows = 1 }));
            commandLine += FormatFactory.FormatDescr(
               new FormatContent("Serve by: " + GetValue<DateTime>(dr, "SumServeByDate").ToString("dd MMM yyyy"), new FormatOption() { Retract = 17, WordsNum = 30, Rows = 1 }));            
            #endregion

            //Set paging
            commandLine += "\x0c";

            #region Start the second page
            commandLine += "\x1B\x43\x00" + (char)(formLength);
            //Character Pitch 12 cpi
            commandLine += "\x1B\x3a";

            commandLine += FormatFactory.WriteCR(2);
            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<string>(dr, "SummonsNo"), new FormatOption() { Retract = 14, WordsNum = 20, Rows = 1 })
                , new FormatContent(GetValue<string>(dr, "PageSequence"), new FormatOption() { Retract = 54, WordsNum = 5, Rows = 1 })
                );

            commandLine += FormatFactory.WriteCR(2);
            //the first charge start
            commandLine += FormatFactory.FormatDescr(
                FormatFactory.MergeFormatContent(
                    new FormatContent(GetValue<string>(dr, "ChargeNo1"),
                        new FormatOption() { Retract = 21, WordsNum = 2, Rows = 1 }),
                    new FormatContent(GetValue<string>(dr, "AlterChargeNo1"),
                        new FormatOption() { Retract = 3, WordsNum = 15, Rows = 1 })),
               FormatFactory.MergeFormatContent(
                   new FormatContent(GetValue<string>(dr, "FineAmount1"),
                       new FormatOption() { Retract = 8, WordsNum = 8, Rows = 1 }),
                   new FormatContent(GetValue<string>(dr, "SumNoticeNo1"),
                       new FormatOption() { Retract = 14, WordsNum = 20, Rows = 1 })));

            commandLine += FormatFactory.WriteCR(2);
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "StatutoryRef1"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 3 })
                );

            //commandLine += FormatFactory.WriteCR(1);
            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<DateTime>(dr, "SumOffenceDate1").ToString("dd MMM yyyy HH:mm"), new FormatOption() { Retract = 28, WordsNum = 20, Rows = 1 })
                , new FormatContent(GetValue<string>(dr, "SumLocDescr1_1"), new FormatOption() { Retract = 15, WordsNum = 20, Rows = 1 })
                );

            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "SumLocDescr1_2"), new FormatOption() { Retract = 8, WordsNum = 20, Rows = 1 })
                );

            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "OffenceDescrEng1"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 5 })
                );
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SchCode1"), new FormatOption() { Retract = 85, WordsNum = 6, Rows = 1 }));
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "OffenceDescrAfr1"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 4 })
                );
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SumOfficerNo1"), new FormatOption() { Retract = 82, WordsNum = 10, Rows = 1 }));
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SumOfficerName1"), new FormatOption() { Retract = 82, WordsNum = 10, Rows = 1 }));
            //the first charge end
            commandLine += FormatFactory.WriteCR(1);
            //the second charge start
            commandLine += FormatFactory.FormatDescr(
                FormatFactory.MergeFormatContent(
                    new FormatContent(GetValue<string>(dr, "ChargeNo2"),
                        new FormatOption() { Retract = 21, WordsNum = 2, Rows = 1 }),
                    new FormatContent(GetValue<string>(dr, "AlterChargeNo2"),
                        new FormatOption() { Retract = 3, WordsNum = 15, Rows = 1 })),
               FormatFactory.MergeFormatContent(
                   new FormatContent(GetValue<string>(dr, "FineAmount2"),
                       new FormatOption() { Retract = 8, WordsNum = 8, Rows = 1 }),
                   new FormatContent(GetValue<string>(dr, "SumNoticeNo2"),
                       new FormatOption() { Retract = 14, WordsNum = 20, Rows = 1 })));

            commandLine += FormatFactory.WriteCR(2);
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "StatutoryRef2"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 3 })
                );

            //commandLine += FormatFactory.WriteCR(1);
            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<DateTime>(dr, "SumOffenceDate2").ToString("dd MMM yyyy HH:mm"), new FormatOption() { Retract = 28, WordsNum = 20, Rows = 1 })
                , new FormatContent(GetValue<string>(dr, "SumLocDescr2_1"), new FormatOption() { Retract = 15, WordsNum = 20, Rows = 1 })
                );

            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "SumLocDescr2_2"), new FormatOption() { Retract = 8, WordsNum = 20, Rows = 1 })
                );

            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "OffenceDescrEng2"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 5 })
                );
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SchCode2"), new FormatOption() { Retract = 85, WordsNum = 6, Rows = 1 }));
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "OffenceDescrAfr2"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 4 })
                );
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SumOfficerNo2"), new FormatOption() { Retract = 82, WordsNum = 10, Rows = 1 }));
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SumOfficerName2"), new FormatOption() { Retract = 82, WordsNum = 10, Rows = 1 }));
            //the second charge end
            commandLine += FormatFactory.WriteCR(1);
            //the third charge start
            commandLine += FormatFactory.FormatDescr(
                FormatFactory.MergeFormatContent(
                    new FormatContent(GetValue<string>(dr, "ChargeNo3"),
                        new FormatOption() { Retract = 21, WordsNum = 2, Rows = 1 }),
                    new FormatContent(GetValue<string>(dr, "AlterChargeNo3"),
                        new FormatOption() { Retract = 3, WordsNum = 15, Rows = 1 })),
               FormatFactory.MergeFormatContent(
                   new FormatContent(GetValue<string>(dr, "FineAmount3"),
                       new FormatOption() { Retract = 8, WordsNum = 8, Rows = 1 }),
                   new FormatContent(GetValue<string>(dr, "SumNoticeNo3"),
                       new FormatOption() { Retract = 14, WordsNum = 20, Rows = 1 })));

            commandLine += FormatFactory.WriteCR(2);
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "StatutoryRef3"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 3 })
                );

            //commandLine += FormatFactory.WriteCR(1);
            commandLine += FormatFactory.FormatDescr(
                new FormatContent(GetValue<DateTime>(dr, "SumOffenceDate3").ToString("dd MMM yyyy HH:mm"), new FormatOption() { Retract = 28, WordsNum = 20, Rows = 1 })
                , new FormatContent(GetValue<string>(dr, "SumLocDescr3_1"), new FormatOption() { Retract = 15, WordsNum = 20, Rows = 1 })
                );

            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "SumLocDescr3_2"), new FormatOption() { Retract = 8, WordsNum = 20, Rows = 1 })
                );

            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "OffenceDescrEng3"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 5 })
                );
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SchCode3"), new FormatOption() { Retract = 85, WordsNum = 6, Rows = 1 }));
            commandLine += FormatFactory.FormatDescr(new FormatContent(GetValue<string>(dr, "OffenceDescrAfr3"), new FormatOption() { Retract = 8, WordsNum = 80, Rows = 4 })
                );
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SumOfficerNo3"), new FormatOption() { Retract = 82, WordsNum = 10, Rows = 1 }));
            commandLine += FormatFactory.FormatDescr(
              new FormatContent(GetValue<string>(dr, "SumOfficerName3"), new FormatOption() { Retract = 82, WordsNum = 10, Rows = 1 }));
            //the third charge end
            #endregion

            //Form end
            commandLine += "\x0c";

            try
            {
                LPT.Send(commandLine);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Unable to find port {0}, Error: {1}", this.LPTName, ex.Message));
            }
        }
    }
}
