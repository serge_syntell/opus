﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class LPT
    {
        public LPT(string lptName)
        {
            this._lptName = lptName;
        }

        private string _lptName;
        public const short FILE_ATTRIBUTE_NORMAL = 0x80;
        public const short INVALID_HANDLE_VALUE = -1;
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;
        public const uint CREATE_NEW = 1;
        public const uint CREATE_ALWAYS = 2;
        public const uint OPEN_EXISTING = 3;

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr CreateFile(string lpFileName, uint dwDesiredAccess,
            uint dwShareMode, IntPtr lpSecurityAttributes, uint dwCreationDisposition,
            uint dwFlagsAndAttributes, IntPtr hTemplateFile);


        public void Send(String receiptText)
        {
            IntPtr ptr = CreateFile(_lptName, GENERIC_WRITE, 0, IntPtr.Zero, CREATE_ALWAYS, 0, IntPtr.Zero);

            if (ptr.ToInt32() == -1)
            {
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            }
            else
            {
                using (FileStream lpt = new FileStream(ptr, FileAccess.ReadWrite))
                {
                    Byte[] buffer = System.Text.Encoding.Default.GetBytes(receiptText);
                    lpt.Write(buffer, 0, buffer.Length);
                    lpt.Close();
                }
            }
        }
    }
}
