﻿using System;
using System.Collections;
using System.Text;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Configuration;


namespace SIL.AARTO.BLL.Utility
{
    class API
    {
    }
    
    public class IdentityAnalogue
    {
        //Simulation using the specified user defined constants
        /**/
        /// <summary>
        /// 
        /// </summary>
        public const int LOGON32_LOGON_INTERACTIVE = 2;
        /**/
        /// <summary>
        /// 
        /// </summary>
        public const int LOGON32_PROVIDER_DEFAULT = 0;

        /**/
        /// <summary>
        /// 
        /// </summary>
        WindowsImpersonationContext impersonationContext;

        //win32api reference
        /**/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lpszUserName"></param>
        /// <param name="lpszDomain"></param>
        /// <param name="lpszPassword"></param>
        /// <param name="dwLogonType"></param>
        /// <param name="dwLogonProvider"></param>
        /// <param name="phToken"></param>
        /// <returns></returns>
        [DllImport("advapi32.dll")]
        public static extern int LogonUserA(string lpszUserName,
            string lpszDomain,
            string lpszPassword,
            int dwLogonType,
            int dwLogonProvider,
            ref IntPtr phToken);
        /**/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hToken"></param>
        /// <param name="impersonationLevel"></param>
        /// <param name="hNewToken"></param>
        /// <returns></returns>
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(IntPtr hToken,
            int impersonationLevel,
            ref IntPtr hNewToken);
        /**/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RevertToSelf();
        /**/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr handle);
        /**/
        /// <summary>
        /// 
        /// </summary>
        public IdentityAnalogue()
        {
        }

        /// <summary>
        /// for login in add  by andy
        /// </summary>
        /// <returns></returns>
        public bool impersonateValidUser()
        {
            bool isPass = false;

            string domain = null;
            string userName = null;
            string password = null;
            try
            {
                domain = ConfigurationSettings.AppSettings["domain"];
                userName = ConfigurationSettings.AppSettings["userName"];
                password = ConfigurationSettings.AppSettings["password"];

                isPass = impersonateValidUser(userName, domain, password);
            }
            catch (Exception e)
            {
              
                return isPass;
            }

            return isPass;

        }



        //Simulate the specified user identity
        /**/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="domain"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool impersonateValidUser(string userName, string domain, string password)
        {
            WindowsIdentity tempWindowsIdentity;
            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;
            if (RevertToSelf())
            {
                if (LogonUserA(userName, domain, password, 2, 0, ref token) != 0)
                {
                    if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                    {
                        tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                        impersonationContext = tempWindowsIdentity.Impersonate();
                        if (impersonationContext != null)
                        {
                            CloseHandle(token);
                            CloseHandle(tokenDuplicate);
                            return true;
                        }
                    }
                }
            }
            if (token != IntPtr.Zero)
                CloseHandle(token);
            if (tokenDuplicate != IntPtr.Zero)
                CloseHandle(tokenDuplicate);
            return false;
        }

        //Cancel simulation
        /**/
        /// <summary>
        /// 
        /// </summary>
        public void undoImpersonation()
        {
            impersonationContext.Undo();
        }
    }


}
