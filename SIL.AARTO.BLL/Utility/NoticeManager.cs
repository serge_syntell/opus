﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
namespace SIL.AARTO.BLL.Utility
{
    public class NoticeManager
    {
        public static Charge GetChargeByNotIntNo(int notIntNo)
        {
            Charge cg = null;
            try
            {
                cg = new ChargeService().GetByNotIntNo(notIntNo)[0];
            }
            catch
            {
                cg = null;
            }
            return cg;
        }
        public static int? GetNotIntNoByRepID(int repID)
        {
            int? notIntNo;
            int? chgIntNo;
            try
            {
                chgIntNo = new AartoRepresentationService().GetByAaRepId(repID).ChgIntNo;
                if (chgIntNo == null) return null;                
                notIntNo = new ChargeService().GetByChgIntNo((int)chgIntNo).NotIntNo;
            }
            catch
            {
                notIntNo = null;
            }
            return notIntNo;
        }
        public static SIL.AARTO.DAL.Entities.Authority GetAuthorityByNotIntNo(int notIntNo)
        {
            Notice notice = new NoticeService().GetByNotIntNo(notIntNo);
            if (notice != null)
            {
                return new AuthorityService().GetByAutIntNo(notice.AutIntNo);
            }
            else return null;
        }
        public static SIL.AARTO.DAL.Entities.Authority GetAuthorityByAutCode(string autCode)
        {
            return new AuthorityService().GetByAutCode(autCode);
        }
        public static int? GetMetroIDByAuthorityID(int autIntNo)
        {
            SIL.AARTO.DAL.Entities.Authority authority = new AuthorityService().GetByAutIntNo(autIntNo);
            if (authority != null)
                return authority.MtrIntNo;
            else return null;
        }
        public static Notice GetNoticeByNotTicketNo(string ticketNo)
        {
            int notIntNo = -1;
            IDataReader idr = new NoticeService().GetNoticeByNotTicketNoWithNoSplit(ticketNo);
            while (idr.Read())
            {
                notIntNo = Convert.ToInt32(idr["NotIntNo"]);
                break;
            }
            idr.Close();
            if (notIntNo != -1) return new NoticeService().GetByNotIntNo(notIntNo);
            else return null;
        }
        public static void AddSearchIDAndName(int notIntNo, string idNo, string surname, string initials)
        {
            
            TList<SearchId> list = new TList<SearchId>();
            SearchId sid = null;
            list = new SearchIdService().GetByNotIntNo(notIntNo);
            if (list.Count > 0)
            {
                sid = list[0];
            }
            else {
                sid = new SearchId();
            }
            sid.NotIntNo = notIntNo;
            sid.IdNumber = idNo;
            new SearchIdService().Save(sid);

            SearchSurName sname =null;
                
            TList<SearchSurName> listSurName = new SearchSurNameService().GetByNotIntNo(notIntNo);
            if (listSurName.Count == 0)
            {
                sname = new SearchSurName();
            }
            else
            {
                sname = listSurName[0];
            }

            sname.NotIntNo = notIntNo;
            sname.Initials = initials;
            sname.SurName = surname;
            new SearchSurNameService().Save(sname);
        }
        public static void DeleteSearchIDAndNameByDriverInf(int notIntNo)
        {
            TList<Driver> driverList=new DriverService().GetByNotIntNo(notIntNo);
            if (driverList.Count > 0)
            {
                Driver oldDriver = driverList[0];
                SearchId sid = new SearchIdService().GetByIdNumberNotIntNo(oldDriver.DrvIdNumber, notIntNo);
                if (sid != null)
                    new SearchIdService().Delete(sid);
                SearchSurName sname = new SearchSurNameService().GetByInitialsSurNameNotIntNo(oldDriver.DrvInitials, oldDriver.DrvSurname, notIntNo);
                if (sname != null)
                    new SearchSurNameService().Delete(sname);
            }
        }
        // 2013-07-17 add parameter lastUser by Henry
        public static void UpdateChargeStatus(ChargeStatusList csCode, int notIntNo, string lastUser)
        {
            Charge charge = GetChargeByNotIntNo(notIntNo);
            if (charge != null)
            {
                charge.ChargeStatus = (int)csCode;
                charge.LastUser = lastUser;
                new ChargeService().Save(charge);
            }
        }
    }
}
