﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Collections;
using System.Web;
using System.IO;

namespace SIL.AARTO.BLL.Utility
{
    public class GetResourcesInClassLibraries
    {
        /// <summary>
        /// Get Resources from SIL.AARTO.TMS\App_LocalResources
        /// </summary>
        /// <param name="resourcesName"></param>
        /// <param name="resourcesKey"></param>
        /// <returns></returns>
        public static string GetResourcesValue(string resourcesName, string resourcesKey)
        {
            string value = "";
            string fileName = System.Web.HttpContext.Current.Server.MapPath(@"App_LocalResources\" + resourcesName + ".resx");

            string language = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            if (language != "en-US")
                fileName = System.Web.HttpContext.Current.Server.MapPath(@"App_LocalResources\" + resourcesName + "." + language + ".resx");
            if (!File.Exists(fileName))
            {
                fileName = System.Web.HttpContext.Current.Server.MapPath(@"App_LocalResources\" + resourcesName + ".resx");
            }
            ResXResourceReader rsxr = new ResXResourceReader(fileName);
            foreach (DictionaryEntry d in rsxr)
            {
                if (d.Key.ToString() == resourcesKey)
                    value = d.Value.ToString();
            }
            rsxr.Close();

            return value;
        }

        /// <summary>
        /// Get Resources from custom path
        /// </summary>
        /// <param name="resourcesName"></param>
        /// <param name="resourcesKey"></param>
        /// <returns></returns>
        public static string GetResourcesValueByCustomPath(string resourcesName, string resourcesKey)
        {
            string value = "";
            string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath;
            string fileName = path + resourcesName + ".resx";

            string language = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            if (language != "en-US")
                fileName = path + resourcesName + "." + language + ".resx";
            if (!File.Exists(fileName))
            {
                fileName = path + resourcesName + ".resx";
            }
            ResXResourceReader rsxr = new ResXResourceReader(fileName);
            foreach (DictionaryEntry d in rsxr)
            {
                if (d.Key.ToString() == resourcesKey)
                    value = d.Value.ToString();
            }
            rsxr.Close();
            return value;
        }
    }
}
