﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.BLL.Culture;

namespace SIL.AARTO.BLL.Utility
{
    public static class SqlComm
    {
        public static SqlParameter GetParam(string paramName, SqlDbType type, int size)
        {
            return new SqlParameter(paramName, type, size);
        }

        public static SqlParameter GetVarCharParam(string paramName, int size, string value)
        {
            SqlParameter param = GetParam(paramName, SqlDbType.VarChar, size);
            param.Value = value;
            return param;
        }

        public static SqlParameter GetNVarCharParam(string paramName, int size, string value)
        {
            SqlParameter param = GetParam(paramName, SqlDbType.NVarChar, size);
            param.Value = value;
            return param;
        }

        public static SqlParameter GetIntParam(string paramName, int value)
        {
            SqlParameter param = GetParam(paramName, SqlDbType.Int, 4);
            param.Value = value;
            return param;
        }

        public static SqlParameter GetTimeParam(string paramName, DateTime value)
        {
            SqlParameter param = new SqlParameter(paramName, SqlDbType.SmallDateTime);
            param.Value = value;
            return param;
        }

        public static int ExecuteNoneQuery(string procName, params SqlParameter[] parameters)
        {
            using (SqlConnection con = new SqlConnection(Config.ConnectionString))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand(procName, con))
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddRange(parameters);
                    return com.ExecuteNonQuery();
                }
            }
        }

        public static SqlDataReader ExecuteQuery(string procName, params SqlParameter[] parameters)
        {
            SqlConnection con = new SqlConnection(Config.ConnectionString);
            con.Open();
            SqlCommand com = new SqlCommand(procName, con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddRange(parameters);
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public static string GetDBNullAbleString(object readerObj)
        {
            return (string)(readerObj == DBNull.Value ? string.Empty : readerObj);
        }
    }
}
