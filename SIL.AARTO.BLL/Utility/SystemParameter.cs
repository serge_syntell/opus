﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
namespace SIL.AARTO.BLL.Utility
{
    public class SystemParameter
    {
        private static SystemParameter _sp;
        public static SystemParameter Get()
        {
            if (_sp == null)
            {
                _sp = new SystemParameter();
                TList<SysParam> pList = new SysParamService().GetAll();
                _sp.PostDeliveryDays = pList.Find(sp => sp.SpColumnName.ToLower() == SysParamList.PostDeliveryDays.ToString().ToLower()).SpIntegerValue;
                _sp.DiscountPercentage = pList.Find(sp => sp.SpColumnName.ToLower() == SysParamList.DiscountPercentage.ToString().ToLower()).SpIntegerValue;
                _sp.DiscountPercentage = (decimal)(_sp.DiscountPercentage / 100);
                _sp.InstallmentDays = pList.Find(sp => sp.SpColumnName.ToLower() == SysParamList.InstallmentDays.ToString().ToLower()).SpIntegerValue;
                _sp.NoticeNoUsesMetroIA = pList.Find(sp => sp.SpColumnName.ToLower() == SysParamList.NoticeNoUsesMetroIA.ToString().ToLower()).SpIntegerValue;
            }
            return _sp;
        }

        public static string GetSPByEnvironmentType(string type)
        {
            string environment = string.Empty;
            foreach (SysParam sp in new SysParamService().GetAll())
            {
                if (sp.SpColumnName.Equals("EnvironmentType"))
                {
                    environment = sp.SpStringValue;
                    break;
                }
            }
            return environment;
        }
        public int PostDeliveryDays;
        public decimal DiscountPercentage;
        public decimal InstallmentDays;
        public int NoticeNoUsesMetroIA;
        public string SpStringValue;
        
    }
}
