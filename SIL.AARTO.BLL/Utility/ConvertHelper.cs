﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace SIL.AARTO.BLL.Utility
{
    public class ConvertHelper
    {
        public static TValue ConvertClass<TSource, TValue>(TSource source)
            where TSource : class, new()
            where TValue : class, new()
        {
            TValue value = null;
            if (source != null)
            {
                value = new TValue();
                var mis = value.GetType().GetMembers();
                var mis0 = source.GetType().GetMembers();
                foreach (var mi in mis)
                {
                    if (mi.MemberType == MemberTypes.Property || mi.MemberType == MemberTypes.Field)
                    {
                        var mi0 = mis0.FirstOrDefault(p => p.Name.Equals(mi.Name, StringComparison.Ordinal));
                        if (mi0 != null && mi.MemberType == mi0.MemberType)
                        {
                            PropertyInfo pi, pi0;
                            FieldInfo fi, fi0;
                            if (mi.MemberType == MemberTypes.Property
                                && (pi = (PropertyInfo) mi).PropertyType == (pi0 = (PropertyInfo) mi0).PropertyType)
                                pi.SetValue(value, pi0.GetValue(source, null), null);
                            else if (mi.MemberType == MemberTypes.Field
                                     && (fi = (FieldInfo) mi).FieldType == (fi0 = (FieldInfo) mi0).FieldType)
                                fi.SetValue(value, fi0.GetValue(source));
                        }
                    }
                }
            }
            return value;
        }

        public static TValueList ConvertClassList<TSource, TValue, TValueList>(IList<TSource> sourceList)
            where TSource : class, new()
            where TValue : class, new()
            where TValueList : class, IList<TValue>, new()
        {
            TValueList valueList = null;
            if (sourceList != null)
            {
                valueList = new TValueList();
                foreach (var source in sourceList)
                {
                    var value = ConvertClass<TSource, TValue>(source);
                    valueList.Add(value);
                }
            }
            return valueList;
        }

    }
}
