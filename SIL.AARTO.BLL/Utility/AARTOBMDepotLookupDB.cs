﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace SIL.AARTO.BLL.Utility
{
    public class AARTOBMDepotLookupDB
    {
        public static DataSet GetListByAaBMDepotID(int AaBMDepotID)
        {
            DataSet ds = new DataSet();

            string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();

            SqlConnection myConnection = new SqlConnection(connStr);
            SqlCommand myCommand = new SqlCommand("AARTOBMDepotLookupGetListByAaBMDepotID", myConnection);

            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAaUserRoleId = new SqlParameter("@AaBMDepotID", SqlDbType.Int, 4);
            parameterAaUserRoleId.Value = AaBMDepotID;
            myCommand.Parameters.Add(parameterAaUserRoleId);

            SqlDataAdapter sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }
            return ds;
        }
    }
}
