﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using System.Data.SqlClient;
using System.Data;

namespace SIL.AARTO.BLL.Utility
{
    public class ViolationLoaderDB
    {
        string mConstr = "";

        public ViolationLoaderDB(string vConstr)
        {
            mConstr = vConstr;
        }

        public int UpdateRejection(int rejIntNo, string rejReason, string rejSkip, string lastUser, int rejCIntNo, string natisBSkip)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("RejectionUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterRejReason = new SqlParameter("@RejReason", SqlDbType.VarChar, 100);
            parameterRejReason.Value = rejReason;
            myCommand.Parameters.Add(parameterRejReason);

            SqlParameter parameterRejSkip = new SqlParameter("@RejSkip", SqlDbType.Char, 1);
            parameterRejSkip.Value = rejSkip;
            myCommand.Parameters.Add(parameterRejSkip);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterRejCIntNo = new SqlParameter("@RejCIntNo", SqlDbType.Int, 4);
            parameterRejCIntNo.Value = rejCIntNo;
            myCommand.Parameters.Add(parameterRejCIntNo);

            SqlParameter parameterNatisBskip = new SqlParameter("@NatisBasketSkip", SqlDbType.Char, 1);
            parameterNatisBskip.Value = natisBSkip;
            myCommand.Parameters.Add(parameterNatisBskip);

            SqlParameter parameterRejIntNo = new SqlParameter("@RejIntNo", SqlDbType.Int);
            parameterRejIntNo.Direction = ParameterDirection.InputOutput;
            parameterRejIntNo.Value = rejIntNo;
            myCommand.Parameters.Add(parameterRejIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int RejIntNo = (int)myCommand.Parameters["@RejIntNo"].Value;
                //int menuId = (int)parameterRejIntNo.Value;

                return RejIntNo;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                myConnection.Dispose();
                return 0;
            }
        }

        public int DeleteFilm(int filmIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("FilmDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFilmIntNo = new SqlParameter("@FilmIntNo", SqlDbType.Int, 4);
            parameterFilmIntNo.Value = filmIntNo;
            parameterFilmIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterFilmIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int delFilmIntNo = (int)parameterFilmIntNo.Value;

                return delFilmIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }
    }
}
