﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Net;
using System.IO;

namespace SIL.AARTO.BLL.Utility
{
    public sealed class WebServiceAccessor
    {
        private WebServiceAccessor()
        {
        }
        public static T GetFromWebServiceBySoap<T>(string serviceUrl, string funName, string inputParaName, object input)
        {
            // Prepare Input
            string inputStr = ObjectSerializer.Serialize(input);
            XmlDocument docInput = new XmlDocument();
            docInput.LoadXml(inputStr);
            string inputXml = docInput.DocumentElement.InnerXml;
            string argString = string.Format("op={0}&{1}={2}", funName, inputParaName, inputXml);

            // Call & Get Result XmlDocument
            XmlDocument docResult;
            docResult = CallWebServiceBySoap(serviceUrl, argString);

            // DeSerialize to Result Object
            Type resultType = typeof(T);
            string objStr = docResult.DocumentElement.ChildNodes[0].ChildNodes[0].ChildNodes[0].OuterXml;
            string tagName = docResult.DocumentElement.ChildNodes[0].ChildNodes[0].ChildNodes[0].Name;
            objStr = objStr.Replace("<" + tagName, "<" + resultType.Name);
            objStr = objStr.Replace("</" + tagName, "</" + resultType.Name);
            docResult.LoadXml(objStr);
            docResult.LoadXml(docResult.OuterXml.Replace(docResult.DocumentElement.NamespaceURI, ""));
            docResult.DocumentElement.RemoveAllAttributes();
            objStr = docResult.OuterXml;
            T result
                = ObjectSerializer.DeSerializerString2Object<T>(objStr);

            // Return Result
            return result;
        }
        private const string soapFmt =
@"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
   xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" 
   xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
  <soap:Body>
    <{0} xmlns=""http://tempuri.org/"">
      {1}
    </{0}>
  </soap:Body>
</soap:Envelope>";
        private static XmlDocument CallWebServiceBySoap(string serviceUrl, string argString)
        {
            // For example: serviceUrl = "http://localhost:2258/ComplexService.asmx?"
            // argString = "op=DoSomething&input=<a>1</a><b>2</b>"
            // op is function name

            try
            {
                char[] sepKey = "&".ToCharArray();
                char[] sepValue = "=".ToCharArray();
                string[] pas = argString.Split(sepKey);
                StringBuilder sb = new StringBuilder();
                string funName = null;
                foreach (string pa in pas)
                {
                    string[] ps = pa.Split(sepValue);
                    if (ps[0] == "op")
                    {
                        funName = ps[1];
                    }
                    else
                    {
                        sb.Append("<" + ps[0] + ">");
                        sb.Append(ps[1]);
                        sb.Append("</" + ps[0] + ">");
                    }
                }

                string soap = string.Format(soapFmt, funName, sb.ToString());

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(serviceUrl);
                req.Headers.Add("SOAPAction", string.Format("\"http://tempuri.org/{0}\"", funName));
                req.ContentType = "text/xml;charset=\"utf-8\"";
                req.Accept = "text/xml";
                req.Method = "POST";

                using (Stream stm = req.GetRequestStream())
                {
                    using (StreamWriter stmw = new StreamWriter(stm))
                    {
                        stmw.Write(soap);
                    }
                }

                WebResponse response = req.GetResponse();

                XmlDocument doc = ReadXmlResponse(response);
                return doc;
            }
            catch
            {
                throw;
            }
        }
        private static XmlDocument ReadXmlResponse(WebResponse response)
        {
            StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            String retXml = sr.ReadToEnd();
            sr.Close();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(retXml);
            return doc;
        }
    }

    public class WebserviceResult
    {
        public string Result { get; set; }
    }
}
