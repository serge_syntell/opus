﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SIL.AARTO.BLL.Utility
{
    public class Validate
    {
       
        #region Regex
        /// <summary>
        /// Check email address (Regex)
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool CheckEmail(string email)
        {
            string pattern = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(email);
        }

        public static bool CheckURL(string URL)
        {
            string pattern = @"[a-zA-z]+://[^\s]*";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(URL);
        }
        public static bool IsInt(string str)
        {
            return Regex.IsMatch(str, @"^-?\d+$");
        }
        public static bool IsNull(object str)
        {
            if (str == DBNull.Value) return true;
            if (str == null) return true;
            else
            {
                if (str.ToString().Trim().Equals(string.Empty)) return true;
                else return false;
            }
        }
        public static bool IsEmail(string email)
        {
            return Regex.IsMatch(email, @"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$");
        }
        public static bool IsNumber(string num)
        {
            return Regex.IsMatch(num, @"^\d+$");
        }
        public static bool IsSAPostCode(string code)
        {
            //only for south africa
            return IsNumber(code) && code.Length == 4 && code != "0000";
        }
        #endregion

        /// <summary>
        /// Check phone number
        /// Jerry 2014-03-04 add by Teresa
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static bool CheckPhoneNumber(string phoneNumber)
        {
            string strMatch = "0123456789()-+.";
            char[] arrCell = phoneNumber.ToCharArray();
            foreach (char cell in arrCell)
            {
                if (cell.ToString().Trim() != "" && strMatch.IndexOf(cell.ToString()) < 0)
                {
                    return false;
                }
            }

            return true;
        }

    }
}
