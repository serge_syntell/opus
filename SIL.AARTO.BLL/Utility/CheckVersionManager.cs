using System;
using System.Reflection;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility
{
    public static class ProjectLastUpdated
    {
        public static DateTime AARTOWebApplication
        {
            get { return DateTime.Parse("2015/07/27"); }
        }
        public static DateTime AARTOImporterConsole
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOBatchConsole
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
      
        public static DateTime AARTOClockManagerConsole
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime HandwrittenOffencesImporterForDMS
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime HandwrittenOffencesImporter
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOPrintEngine
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOPrintEnginePrintFactoryBackService
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOPrintEnginePrintFactoryConfiguration
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOViolation_Loader
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOTPExInt
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOThaboExporter
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTORoadblockExtractor
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOPayfineExtractor
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOThaboAggregator
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime TMS
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOReportConsole
        {
            get { return DateTime.Parse("2015/04/01"); }
        }

        public static DateTime AARTOIMXImageLoader
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
        public static DateTime AARTOXMLLoader
        {
            get { return DateTime.Parse("2015/04/01"); }
        }
    }

    public class CheckVersionManager
    {
        public static bool CheckVersion(AartoProjectList appName, out string errorMessage)
        {
            DateTime lastUpdated = (DateTime)GetLastUpdatedDatetime(appName);

            DateTime? dbLastupdated = DateTime.Parse("1900/01/01");

            string dbName = "AARTO";

            // 2010/10/13 jerry changed it
            if (appName == AartoProjectList.AARTOThaboAggregator)
            {
                SIL.Thabo.DAL.Services.VersionControlSilService x = new SIL.Thabo.DAL.Services.VersionControlSilService();
                x.VerifySync(appName.ToString(), ref dbLastupdated);
                dbName = "THABO";
            }
            else
            {
                SIL.AARTO.DAL.Services.VersionControlSilService x = new SIL.AARTO.DAL.Services.VersionControlSilService();
                x.VerifySync(appName.ToString(), ref dbLastupdated);
            }

            errorMessage = string.Format("Application {0} version {1:yyyy/MM/dd} is unable to access Database {2} version {3:yyyy/MM/dd}",
                appName.ToString(), lastUpdated, dbName, dbLastupdated.Value);
            return lastUpdated == dbLastupdated.Value;
        }


        public static object GetLastUpdatedDatetime(AartoProjectList appName)
        {

            //Type type = Type.GetType("SIL.AARTO.BLL.Utility.ProjectLastUpdated");
            //MemberInfo[] members = type.GetMember(appName.ToString());
            //return members[0].
            PropertyInfo propertyInfo = Type.GetType("SIL.AARTO.BLL.Utility.ProjectLastUpdated").GetProperty(appName.ToString());
            //BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            return propertyInfo.GetValue(null, null);
        }
        public static object GetLastUpdatedDatetime(string appName)
        {

            //Type type = Type.GetType("SIL.AARTO.BLL.Utility.ProjectLastUpdated");
            //MemberInfo[] members = type.GetMember(appName.ToString());
            //return members[0].
            PropertyInfo propertyInfo = Type.GetType("SIL.AARTO.BLL.Utility.ProjectLastUpdated").GetProperty(appName.ToString());
            //BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            return propertyInfo.GetValue(null, null);
        }
    }
}
