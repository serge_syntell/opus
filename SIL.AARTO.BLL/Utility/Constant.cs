﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility
{
    public class Constant
    {
        public const string NoticeCancelled = "notice is cancelled";
        public const string NoticeAdded = "add a new notice";
        public const string AlreadyAdjudicated = "Already adjudicated";
    }
}
