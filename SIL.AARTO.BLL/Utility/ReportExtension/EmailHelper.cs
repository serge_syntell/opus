﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Net.Mail;

namespace SIL.AARTO.BLL.Utility.ReportExtension
{
    public delegate void OnErrorHandle(Exception ex);

    public class EmailSettings : ConfigurationSection
    {
        [ConfigurationProperty("host")]
        public string Host
        {
            get { return this["host"] as string; }
            set { this["host"] = value; }
        }

        [ConfigurationProperty("userName")]
        public string UserName
        {
            get { return this["userName"] as string; }
            set { this["userName"] = value; }
        }

        [ConfigurationProperty("password")]
        public string Password
        {
            get { return this["password"] as string; }
            set { this["password"] = value; }
        }

        [ConfigurationProperty("sender")]
        public string Sender
        {
            get { return this["sender"] as string; }
            set { this["sender"] = value; }
        }

        [ConfigurationProperty("port")]
        public int Port
        {
            get
            {
                int value;
                int.TryParse(Convert.ToString(this["port"]), out value);
                return value;
            }
            set { this["port"] = value; }
        }
    }

    public class EmailHelper
    {
        public EmailHelper(MailMessage mailBox = null, EmailSettings settings = null, string sectionName = "emailSettings")
        {
            Settings = settings ?? ConfigurationManager.GetSection(sectionName) as EmailSettings;
            MailBox = mailBox;
        }

        public MailMessage MailBox { get; set; }
        public EmailSettings Settings { get; set; }
        public OnErrorHandle OnError { get; set; }

        public void Send()
        {
            try
            {
                var smtp = new SmtpClient();

                if (MailBox == null) return;

                if (MailBox.From == null || string.IsNullOrWhiteSpace(MailBox.From.Address))
                    MailBox.From = new MailAddress(Settings.Sender);


                if  (Settings != null)
                { 
                    smtp.Host = Settings.Host;
                    smtp.Port = Settings.Port > 0 ? Settings.Port : 25;
                    smtp.Credentials = new NetworkCredential(Settings.UserName, Settings.Password);
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                }

                smtp.Send(MailBox);
            }
            catch (Exception ex)
            {
                if (OnError != null)
                    OnError(ex);
            }
        }
    }
}