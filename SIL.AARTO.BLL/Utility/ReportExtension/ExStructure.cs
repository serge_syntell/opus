﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace SIL.AARTO.BLL.Utility.ReportExtension
{
    public class ExSheet
    {
        public ExSheet()
        {
            Rows = new List<ExRow>();
        }

        public int TotalWidth { get; set; }
        public int TotalHeight { get; set; }
        List<ExRow> Rows { get; set; }
    }

    public class ExRow
    {
        public ExRow()
        {
            Cells = new List<ExCell>();
        }

        List<ExCell> Cells { get; set; }
    }

    public class ExCell
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Text { get; set; }
        public int Index { get; set; }
        public ICellStyle Style { get; set; }

    }

}
