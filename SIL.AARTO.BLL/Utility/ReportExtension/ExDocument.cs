﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.BLL.Utility.ReportExtension
{
    public class ExDocument
    {
        public ExDocument()
        {
            Sheets = new List<ExSheet>();
        }

        List<ExSheet> Sheets { get; set; }

        public void Save(string fileName) {}

        public void Clear()
        {
            Sheets.Clear();
        }

        protected virtual void CalculateStructure() {}

        public virtual void ReadStructure(object structrue) {}

        public virtual void AddCell(ExCell cell) {}
        

        public virtual void AddCell(string text, int x, int y, int width, int height, System.Drawing.Font font) {}

    }

}
