﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using SIL.AARTO.BLL.Utility.Printing;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Utility.ReportExtension
{
    public delegate void OnCreatedEvent(string attachment);

    public class ReportToExcel : ExDocument
    {
        private readonly List<ReportTextPrintArg> reportTextList = new List<ReportTextPrintArg>();
        public bool IsFirstStart = true;
        private string TempFileName = "";
        private IFont blueFont;
        private IFont boldFont;
        private ICellStyle borderStyle;
        private ICell cell;
        private ICellStyle centreStyle;
        private int lastColIndex;
        private int lastRowIndex;
        private ISheet sheet;
        private HSSFWorkbook wb;

        public OnCreatedEvent onCreated;


        public override void ReadStructure(object structrue)
        {
        }

        public void Start()
        {
        }

        private string RefreshTempFileName()
        {
            TempFileName = string.Format("PunchStatistic-{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"));

            return TempFileName;
        }

        public void End()
        {
            lastRowIndex = 0;
            lastColIndex = 0;

            wb = new HSSFWorkbook();
            sheet = wb.CreateSheet();
            wb.SetSheetName(0, "Punch statistic");
            float lastY = 0F;
            IRow lastRow = sheet.CreateRow(1);


            foreach (ReportTextPrintArg arg in reportTextList)
            {
                if (arg.textArea.Y != lastY)
                {
                    lastY = arg.textArea.Y;
                    lastRowIndex += 1;
                    lastRow = sheet.CreateRow(lastRowIndex);
                    lastColIndex = 0;
                }
                createStringCell(lastRow, lastColIndex, arg.text);
                if (arg.Portion == PrintPortionDef.Header)
                {
                    ICellStyle sty = wb.CreateCellStyle();
                    sty.WrapText = true;
                    //sty.BorderBottom= BorderStyle.MEDIUM;
                    //sty.SetFont(new HSSFFont(0,FontBoldWeight));
                    lastRow.Cells[lastColIndex].CellStyle = sty;
                    lastRow.HeightInPoints = 2*sheet.DefaultRowHeight/20;
                    sheet.AddMergedRegion(new CellRangeAddress(lastRowIndex, lastRowIndex, 0, 5));
                }

                lastColIndex += 1;
            }


            sheet.SetColumnWidth(0, 6000);
            sheet.SetColumnWidth(1, 10000);
            //sheet.SetColumnWidth(2, 12000);
            //sheet.AutoSizeColumn(0); sheet.AutoSizeColumn(1);
            using (FileStream fs = File.Create(RefreshTempFileName()))
            {
                wb.Write(fs);
            }

            if (onCreated != null)
                onCreated(TempFileName);
            try
            {
                File.Delete(TempFileName);
            }
            catch{}

            //responseFile_SendEmail(this,
            //                       new EmailEventArgs(string.Format("PunchStatistic:", DateTime.Now), "test punch",
            //                                          new[] {TempFileName}));
        }

        private void responseFile_SendEmail(object sender, EmailEventArgs e)
        {
            //using (var mailBox = new MailMessage { Subject = TempFileName })
            //{
            //    mailBox.To.Add("csknife@163.com");
            //    mailBox.Attachments.Add(new Attachment(TempFileName));
            //    var emailSender = new EmailHelper(mailBox);
            //    emailSender.Send();               
            //}
            //File.Delete(TempFileName);
            //return;


            //var from = new MailAddress("oscarkoo1982@163.com");
            //var to = new MailAddress("csknife@163.com");

            //var mail = new MailMessage(from, to);
            //mail.Subject = string.Format("PunchStatistic{0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            //mail.Body = mail.Subject;
            //mail.BodyEncoding = Encoding.UTF8;

            //// this will release the resources used by Attachment. 

            //try
            //{
            //    var mailClient = new SmtpClient();
            //    mailClient.Host = "smtp.163.com";
            //    mailClient.PickupDirectoryLocation = @"D:\Tmp";
            //    mailClient.Credentials = new NetworkCredential("oscarkoo1982", "19216811");
            //    mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            //        //.Network;//.SpecifiedPickupDirectory;//.PickupDirectoryFromIis;

            //    var attObj = new Attachment(TempFileName);
            //    mail.Attachments.Add(attObj);
            //    mailClient.Send(mail);
            //    attObj.Dispose();
            //    File.Delete(TempFileName);
            //}
            //catch (Exception smtpEx)
            //{
            //    Debugger.Break();
            //    throw;
            //}
        }
        private void createStringCell(IRow row, int colIndex, string value)
        {
            cell = row.CreateCell(colIndex);
            cell.SetCellValue(new HSSFRichTextString(value));
            //this.cell.CellStyle = this.borderStyle;
        }
        public void AddCellForReport(ReportTextPrintArg arg)
        {
            reportTextList.Add(arg);
        }
    }
}