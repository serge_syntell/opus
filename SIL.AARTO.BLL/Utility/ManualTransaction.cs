﻿using System;
using System.Collections.Generic;
using System.IO;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Utility
{
    public class ManualTransaction<THistory>
        where THistory : History
    {
        readonly ManualTransaction<THistory> outerTransaction;
        bool isRolledBack;

        public ManualTransaction(Action<THistory> onRollingBackDatabaseData = null,
            Action<Exception> onException = null,
            ManualTransaction<THistory> outerTransaction = null)
        {
            OnRollingBackDatabaseData = onRollingBackDatabaseData;
            OnException = onException;
            this.outerTransaction = outerTransaction;
            Histories = new List<THistory>();
        }

        public List<THistory> Histories { get; protected set; }
        public Action<THistory> OnRollingBackDatabaseData { get; protected set; }
        public Action<Exception> OnException { get; protected set; }

        /// <summary>
        ///     return true = Commit transaction; return false = RollBack transaction.
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool BeginTransaction(Func<bool> tran)
        {
            this.isRolledBack = false;
            try
            {
                if (tran())
                {
                    Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(ex);
            }

            RollBack();
            return false;
        }


        public virtual void AddHistory(THistory history)
        {
            Histories.Add(history);

            if (this.outerTransaction != null)
                this.outerTransaction.AddHistory(history);
        }

        protected virtual void Complete()
        {
            Histories.Clear();
        }

        protected virtual void RollBack()
        {
            if (!this.isRolledBack)
            {
                for (var i = 0; i < Histories.Count;)
                {
                    try
                    {
                        var history = Histories[i];

                        if (OnRollingBackDatabaseData != null)
                            OnRollingBackDatabaseData(history);

                        if (!history.TransactionalFiles.IsNullOrEmpty())
                            history.TransactionalFiles.ForEach(t =>
                            {
                                DeleteFile(t.Path);
                                DeleteFile(t.OriginalPath);
                            });

                        Histories.RemoveAt(i);

                        if (this.outerTransaction != null)
                        {
                            var index = this.outerTransaction.Histories.FindIndex(h => h.Guid == history.Guid);
                            if (index >= 0)
                                this.outerTransaction.Histories.RemoveAt(index);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (OnException != null)
                            OnException(ex);
                        i++;
                    }
                }
            }
            this.isRolledBack = true;
        }

        public void DeleteFile(string file)
        {
            try
            {
                if (File.Exists(file))
                    File.Delete(file);
            }
            catch {}
        }
    }

    public abstract class History
    {
        protected History()
        {
            Guid = Guid.NewGuid();
            TransactionalFiles = new List<TransactionalFile>();
        }

        public Guid Guid { get; set; }
        public List<TransactionalFile> TransactionalFiles { get; protected set; }
    }

    public class TransactionalFile
    {
        public TransactionalFile(string path)
        {
            Path = path;
            OriginalPath = path;
        }

        public string Path { get; set; }
        public string OriginalPath { get; private set; }
    }
}
