﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public enum PunchAction
    {
        Add = 1,
        Change = 2,
        Delete = 3,
        Enquiry = 4,//2013-11-6 Heidi added for add all Punch Statistics Transaction(5084)
        Other = 5 //2013-11-6 Heidi added for add all Punch Statistics Transaction(5084)
    }

    public class PunchStatistics
    {
        public PunchStatistics(string connStr)
        {
            ConnectionString = connStr;
        }

        string lastUser;

        string connStr;
        public string ConnectionString
        {
            get { return this.connStr; }
            set
            {
                this.connStr = value;
                db = new ServiceDB(value);
            }
        }

        string errorMsg;
        public string ErrorMessage
        {
            get { return this.errorMsg; }
            set
            {
                this.errorMsg = value;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    SaveErrorLog(value);
                    if (WriteErrorMessage != null)
                        WriteErrorMessage(value);
                }
            }
        }

        public WriteErrorMessageHandler WriteErrorMessage { get; set; }

        ServiceDB db;

        //2013-11-6 Heidi changed default value for pstAction
        //public int PunchStatisticsTransactionAdd(int autIntNo, string userLoginName, PunchStatisticsTranTypeList psttName, PunchAction pstAction = PunchAction.Add, int pstActionCount = 1)
        public int PunchStatisticsTransactionAdd(int autIntNo, string userLoginName, PunchStatisticsTranTypeList psttName, PunchAction pstAction = PunchAction.Other, int pstActionCount = 1)
        {
            //2013-11-6 Heidi changed for add all Punch Statistics Transaction(5084)
            var result = 0;
            ErrorMessage = string.Empty;
            this.lastUser = userLoginName;
            var msgFormat = "Add punch action has error: AutIntNo: '{0}'; User: '{1}'; Transaction: '{2}'; Action: '{3}'; Message: '{4}'";

            var paras = new[]
            {
                new SqlParameter("@AutIntNo", autIntNo),
                new SqlParameter("@UserLoginName", userLoginName),
                new SqlParameter("@PSTTName", psttName.ToString()),
                new SqlParameter("@PSTAction", (short) pstAction),
                new SqlParameter("@PSTActionCount", pstActionCount)
            };

            try
            {
                var obj = db.ExecuteScalar("PunchStatisticsTransactionAdd", paras);
                if (obj != null)
                    int.TryParse(obj.ToString(), out result);
            }
            catch (Exception ex)
            {
                result = -99;
                ErrorMessage = string.Format(msgFormat, autIntNo, userLoginName, psttName, pstAction, ex.Message + ex.StackTrace);
            }

            switch (result)
            {
                case -1:
                    ErrorMessage = string.Format(msgFormat, autIntNo, userLoginName, psttName, pstAction, "Authority does not exist.");
                    break;
                case -2:
                    ErrorMessage = string.Format(msgFormat, autIntNo, userLoginName, psttName, pstAction, "User login name does not exist.");
                    break;
                case -3:
                    ErrorMessage = string.Format(msgFormat, autIntNo, userLoginName, psttName, pstAction, "Punch statistics transaction type does not exist.");
                    break;
                case -4:
                    ErrorMessage = string.Format(msgFormat, autIntNo, userLoginName, psttName, pstAction, "Insert Punch statistics transaction failed.");
                    break;
            }
            return result;
        }

        void SaveErrorLog(string errorMsg)
        {
            try
            {
                var logName = HttpContext.Current != null
                                  ? HttpContext.Current.Request.Path : "PunchStatisticsTransactionAdd";
                var errorlog = new AartoErrorLog
                {
                    AaErLogDate = DateTime.Now,
                    AaErLogName = logName.Substring(0, Math.Min(logName.Length, 50)),
                    AaErLogDescription = errorMsg,
                    AaErLogPriority = 5,
                    LastUser = this.lastUser,
                    AaProjectId = (int) AartoProjectList.AARTOWebApplication
                };

                new AartoErrorLogService().Save(errorlog);
            }
            catch {}
        }

    }
}
