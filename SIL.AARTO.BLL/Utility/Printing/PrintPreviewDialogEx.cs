using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SIL.AARTO.BLL.Utility.ReportExtension;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class PrintPreviewDialogEx : PrintPreviewDialog
    {
        public ListPrintEngine commonListReprint = null;
        public bool ShowExport = false;
        public PrintPreviewDialogEx():this(false,null)
        {
             
        }
        public PrintPreviewDialogEx(bool showExport, ListPrintEngine engine)
        {
            commonListReprint = engine;
            foreach (Control ctrl in base.Controls)
            {
                if (ctrl.GetType() == typeof(ToolStrip))
                {
                    ToolStrip tools = ctrl as ToolStrip;
                    tools.Items.Insert(0, CreatePrintsetButton());
                    tools.Items.Insert(0, CreatePapersetButton());
                    
                    #region removed by Jacob 20121129
                    // delete send mail function from preview dialog.
                    //if(showExport) tools.Items.Insert(0, CreateExportButton());
                    // if ((this.commonListReprint!=null))//&&Convert.ToInt32(this.commonListReprint.PrintModel.ReportCode) == Convert.ToInt32(ReportConfigCodeList.PunchStatics))
                    //{
                    //    excel.IsFirstStart = true;
                    //    commonListReprint.OnDrawString -= new DrawStringHandler(commonListReprint_OnDrawString);
                    //    commonListReprint.OnStartPrint -= new StartPrintHandler(commonListReprint_OnStartPrint);
                    //    commonListReprint.OnEndPrint -= new EndPrintHandler(commonListReprint_OnEndPrint);

                    //    commonListReprint.OnDrawString += new DrawStringHandler(commonListReprint_OnDrawString);
                    //    commonListReprint.OnStartPrint += new StartPrintHandler(commonListReprint_OnStartPrint);
                    //    commonListReprint.OnEndPrint += new EndPrintHandler(commonListReprint_OnEndPrint);

                    //}
                    #endregion

                }
            }
        }
        ToolStripButton CreatePrintsetButton()
        {
            ToolStripButton Stripbutton = new ToolStripButton();
            Stripbutton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            Stripbutton.Image = Resource1.Printer.ToBitmap();//.CtrlResource.property;
            Stripbutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            Stripbutton.Name = "printsetStripButton";
            Stripbutton.Size = new System.Drawing.Size(23, 22);
            Stripbutton.Text = "Printer settings..";
            Stripbutton.Click += new System.EventHandler(this.Stripbutton_Click);
            return Stripbutton;
        }

        ToolStripButton CreateExportButton()
        {
            ToolStripButton Stripbutton = new ToolStripButton();
            Stripbutton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            Stripbutton.Image = Resource1.Printer.ToBitmap();//.CtrlResource.property;
            Stripbutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            Stripbutton.Name = "exportButton";
            Stripbutton.Size = new System.Drawing.Size(23, 22);
            Stripbutton.Text = "Send mail...";
            Stripbutton.Click += new System.EventHandler(this.exportButton_Click);
            return Stripbutton;
        }

        ToolStripButton CreatePapersetButton()
        {
            ToolStripButton Stripbutton = new ToolStripButton();
            Stripbutton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            // Stripbutton.Image = global::UserCtrl.CtrlResource.property;
            Stripbutton.Image = Resource1.Paper.ToBitmap();
            Stripbutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            Stripbutton.Name = "papersetStripButton";
            Stripbutton.Size = new System.Drawing.Size(23, 22);
            Stripbutton.Text = "Paper settings..";
            Stripbutton.Click += new System.EventHandler(this.PaperSetbutton_Click);
            return Stripbutton;
        }
        private void Stripbutton_Click(object sender, EventArgs e)
        {
            using (PrintDialog diag = new PrintDialog())
            {

                diag.Document = base.Document;
                diag.ShowDialog();
            }
        }
        private void PaperSetbutton_Click(object sender, EventArgs e)
        {
            using (PageSetupDialog diag = new PageSetupDialog())
            {

                diag.Document = base.Document;
                diag.ShowDialog();
            }
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Start to send email with report data, Yes or No?", 
                    "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    excel.End();
                    MessageBox.Show("Current report's data has been mailed.", "Mail sending succeed");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show( ex.Message,"error happened.");
                
            }
        }

        void commonListReprint_OnEndPrint(ReportTextPrintArg arg)
        {
            //excel.End();
            commonListReprint.OnDrawString -= new DrawStringHandler(commonListReprint_OnDrawString);
            commonListReprint.OnStartPrint -= new StartPrintHandler(commonListReprint_OnStartPrint);
            commonListReprint.OnEndPrint -= new EndPrintHandler(commonListReprint_OnEndPrint);

            // excel.IsFirstStart = true;
        }

        void commonListReprint_OnStartPrint(ReportTextPrintArg arg)
        {
            excel.Start();
        }
        ReportToExcel excel = new ReportToExcel();
        void commonListReprint_OnDrawString(ReportTextPrintArg arg)
        {
            string infor = string.Format(
                "{0}:{1}-{2}-{3}", DateTime.Now.ToShortTimeString(), arg.text, arg.printFont.Size, arg.textArea.ToString());
            excel.AddCellForReport(arg);

        }
        
    }



    public class DrawStringMonitor
    {
        public ListPrintEngine commonListReprint = null;
        ReportToExcel excel = new ReportToExcel();
        public bool ShowExport = false;
        public DrawStringMonitor()
            : this(null)
        {

        }
        public DrawStringMonitor(ListPrintEngine engine)
        {
            commonListReprint = engine;
           if ((this.commonListReprint != null)&&Convert.ToInt32(this.commonListReprint.PrintModel.ReportCode) == Convert.ToInt32(ReportConfigCodeList.PunchStatics))
           {
               excel.IsFirstStart = true;
               commonListReprint.OnDrawString -= new DrawStringHandler(commonListReprint_OnDrawString);
               commonListReprint.OnStartPrint -= new StartPrintHandler(commonListReprint_OnStartPrint);
               commonListReprint.OnEndPrint -= new EndPrintHandler(commonListReprint_OnEndPrint);

               commonListReprint.OnDrawString += new DrawStringHandler(commonListReprint_OnDrawString);
               commonListReprint.OnStartPrint += new StartPrintHandler(commonListReprint_OnStartPrint);
               commonListReprint.OnEndPrint += new EndPrintHandler(commonListReprint_OnEndPrint);
           }


        }


        public void AfterPrint()
        {
            if ((this.commonListReprint != null) && Convert.ToInt32(this.commonListReprint.PrintModel.ReportCode) == Convert.ToInt32(ReportConfigCodeList.PunchStatics))
            {
                excel.End();
            }
        }
       

        void commonListReprint_OnEndPrint(ReportTextPrintArg arg)
        {
            //excel.End();
            commonListReprint.OnDrawString -= new DrawStringHandler(commonListReprint_OnDrawString);
            commonListReprint.OnStartPrint -= new StartPrintHandler(commonListReprint_OnStartPrint);
            commonListReprint.OnEndPrint -= new EndPrintHandler(commonListReprint_OnEndPrint);

            excel.onCreated = e =>
            {
                try
                {
                    var reaList = new ReportEmailAddressService().GetByReportCode((int) ReportConfigCodeList.PunchStatics);
                    if (reaList != null && reaList.Count > 0)
                    {
                        var receiverList = string.Join(";", reaList.Select(p => p.ReaEmailAddress));
                        var rfe = new ReadyForEmail
                        {
                            ReceiverList = receiverList,
                            Subject = e,
                            AttachmentList = new List<string> { e }
                        };
                        if (commonListReprint.onCreated != null)
                            commonListReprint.onCreated(rfe);
                    }
                }
                catch(Exception ex)
                {
                    if (commonListReprint.WriteErrorMessage != null)
                        commonListReprint.WriteErrorMessage(ex.Message);
                }
            };

            AfterPrint();
            // excel.IsFirstStart = true;
        }

        void commonListReprint_OnStartPrint(ReportTextPrintArg arg)
        {
            excel.Start();
        }
       
        void commonListReprint_OnDrawString(ReportTextPrintArg arg)
        {
            string infor = string.Format(
                "{0}:{1}-{2}-{3}", DateTime.Now.ToShortTimeString(), arg.text, arg.printFont.Size, arg.textArea.ToString());
            excel.AddCellForReport(arg);

        }

    }
}