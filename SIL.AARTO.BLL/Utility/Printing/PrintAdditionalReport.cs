﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;

namespace SIL.AARTO.BLL.Utility.Printing
{
   
    public class PrintAdditionalReport
    {
        SIL.AARTO.DAL.Entities.ReportAdditionalRequest addtionalReportEntity = new SIL.AARTO.DAL.Entities.ReportAdditionalRequest();

        public void PrintAditionalReport(SIL.AARTO.DAL.Entities.ReportAdditionalRequest ReportEntity)
        {
            this.addtionalReportEntity = ReportEntity;
            //PrintPreviewDialog printPreviewDialog1 = new PrintPreviewDialog();
            PrintDocument printDocument1 = new PrintDocument();

            printDocument1.DefaultPageSettings.PaperSize = new PaperSize("US Std Fanfold", 950, 1200);
            printDocument1.PrintPage += new PrintPageEventHandler(this.MyPrintDocument_PrintPage);

            //printPreviewDialog1.Document = printDocument1;
            //DialogResult result = printPreviewDialog1.ShowDialog();
            printDocument1.Print();
        }
        private void MyPrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //e.Graphics.DrawString(addtionalReportEntity.RcIntNoSource.ReportName, new Font("Courier New", 10), System.Drawing.Brushes.Black, 10, 12);
            //e.Graphics.DrawString("Requested by " + addtionalReportEntity.RequestedUser + " on " + addtionalReportEntity.RequestedDate.ToShortDateString(), new Font("Courier New", 10), System.Drawing.Brushes.Black, 10, 28);
            //e.Graphics.DrawString("DateRange " + addtionalReportEntity.DateFrom.ToShortDateString() + " to " + addtionalReportEntity.DateTo.ToShortDateString(), new Font("Courier New", 10), System.Drawing.Brushes.Black, 10, 44);
            //e.Graphics.DrawString(DateTime.Now.Date.ToShortDateString(), new Font("Courier New", 10), System.Drawing.Brushes.Black, 10, 76);
            // Oscar 2013-03-26 changed
            var dateFormat = "yyyy/MM/dd";//2013-05-16 updated by Nancy from 'dd MMM yyyy' to 'yyyy/MM/dd'
            var sb = new StringBuilder();
            sb.AppendFormat("{0} {1}", addtionalReportEntity.RcIntNoSource.ReportCode, addtionalReportEntity.RcIntNoSource.ReportName)
                .AppendLine()
                .AppendFormat("Requested by {0} on {1}", addtionalReportEntity.RequestedUser, addtionalReportEntity.RequestedDate.ToString(dateFormat))
                .AppendLine()
                .AppendFormat("Date Range {0} to {1}", addtionalReportEntity.DateFrom.ToString(dateFormat), addtionalReportEntity.DateTo.ToString(dateFormat))
                .AppendLine()
                .Append(DateTime.Now.ToString(dateFormat));
            e.Graphics.DrawString(sb.ToString().ToUpper(), new Font("Courier New", 10), Brushes.Black, 10, 12);
        }

    }
}
