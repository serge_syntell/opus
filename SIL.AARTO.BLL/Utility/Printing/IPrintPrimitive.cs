﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Text.RegularExpressions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data.SqlClient;
using System.Collections.Specialized;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.Report.Model;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.Xml;
using System.Data;

namespace SIL.AARTO.BLL.Utility.Printing
{
    //added by Jacob 20121113 start
    //raise print text info.
    public delegate void DrawStringHandler(ReportTextPrintArg arg);
    public delegate void StartPrintHandler(ReportTextPrintArg arg);
    public delegate void EndPrintHandler(ReportTextPrintArg arg);
    public delegate void StartPageHandler();
    public delegate void EndPageHandler();
    public delegate void OnCreatedEvent(ReadyForEmail e);

    public class ReadyForEmail
    {
        public string ReceiverList { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public List<string> AttachmentList { get; set; }
    }
    /// <summary>
    /// print text info.
    /// </summary>
    public class ReportTextPrintArg
    {
        public ReportTextPrintArg(string t, Font p, RectangleF ta)
        {
            this.text = t;
            this.printFont = p;
            this.textArea = ta;
        }

        public PrintPortionDef Portion = PrintPortionDef.None;
        public string text;
        public Font printFont;
        public RectangleF textArea;
    }

    public enum PrintPortionDef
    {
        None=0, Header=1, HeaderGroup,DataTableHeader, DataTableRow
    }

    //added by Jacob 20121113 end

    public interface  IPrintPrimitive
    {
        //added by Jacob 20121113  
        //raise print text info.
            event DrawStringHandler OnDrawString; 
            float CalculateHeight(PrintEngine engine, Graphics graphics);
            void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds);
    }

    //added by jacob 20120821
    public class PrintPrimitiveLine : IPrintPrimitive
    {
        public Pen linePen = new Pen(Color.Red);
        public PrintPrimitiveLine(CellLine lineDef)
        {
            this.LineDef = lineDef;
            this.linePen = lineDef.CellPen ?? new Pen(Color.Red);

        }
        //added by Jacob 20121113 
        //raise print text info. not used for now.
        public event DrawStringHandler OnDrawString;

        public   float CalculateHeight(PrintEngine engine, Graphics graphics)
        {
            return 1;

        }

        public   void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds)
        {
            int pageWidth = elementBounds.Right - elementBounds.Left;

            graphics.DrawLine(linePen,
                        LineDef.rx + xPos,
                        LineDef.ry + yPos,
                        LineDef.tx + xPos,
                        LineDef.ty + yPos);
        }

        public CellLine LineDef { get; set; }
    }

    public class PrintPrimitiveEmptyRow : IPrintPrimitive
    {
        //added by Jacob 20121113  
        //raise print text info. not used for now.
        public event DrawStringHandler OnDrawString;

        public   float CalculateHeight(PrintEngine engine, Graphics graphics)
        {
            return engine.PrintFont.Size == 8 ? 12 : 15;
        }

        public   void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds)
        {
            Pen pen = new Pen(engine.PrintBrush, 0);
            graphics.DrawString(null, engine.PrintFont,
                engine.PrintBrush, xPos, yPos, new StringFormat());
        }
    }
    /// <summary>
    /// change
    /// added by Jacob 20120904
    /// </summary>
    public class PrintPrimitiveRow : IPrintPrimitive
    {
        public int height = 0;
        public List<PrintPrimitiveLine> lines = new List<PrintPrimitiveLine>();
        public Pen BorderBottom = null;
        public bool TopLineVisible = false;
        public bool BottomLineVisible = false;

        public List<PrintPrimitiveColumn> texts = new List<PrintPrimitiveColumn>();
        public CellDefination rowDef = null;
        public PrintPrimitiveRow(int RowHeight)
        {
            this.height = RowHeight;
        }
        public PrintPrimitiveRow( )
        {
            
        }

        public void AddTopLine(Pen pen)
        {
            lines.Add(new PrintPrimitiveLine(new CellLine
            {

                CellPen = pen,
                rx = 0,
                ry = 0,
                tx = rowDef.PageWidth,
                ty = 0
            }));


        }
        
        public void AddBottomLine(Pen pen)
        {
            lines.Add(new PrintPrimitiveLine(new CellLine
            {

                CellPen = pen,
                rx = 0,
                ry = (int) CalculateHeight(null, null) ,
                tx =  rowDef.PageWidth,
                ty = (int)CalculateHeight(null, null)
            }));


        }

        public void AddTopLinePad(Pen pen, int yPad)
        {
            lines.Add(new PrintPrimitiveLine(new CellLine
            {

                CellPen = pen,
                rx = 0,
                ry = 0 + yPad,
                tx = rowDef.PageWidth,
                ty = 0 + yPad
            }));


        }
        public void AddBottomLinePad(Pen pen, int yPad)
        {
            lines.Add(new PrintPrimitiveLine(new CellLine
            {

                CellPen = pen,
                rx = 0,
                ry = (int)CalculateHeight(null, null) + yPad,
                tx = rowDef.PageWidth,
                ty = (int)CalculateHeight(null, null)+yPad
            }));


        }
        //added by Jacob 20121113 
        //raise print text info. not used for now.
        public event DrawStringHandler OnDrawString;

        public   float CalculateHeight(PrintEngine engine, Graphics graphics)
        {
            //return engine.PrintFont.Size == 8 ? 12 : 15;
            //return 30;
            //return 30;
            if(height>0)
            {
                return height;

            }
            else
            { float maxHeight = 0;
                foreach(var text in texts)
                {
                   float tempHeight= PrintEngine.GetCellContentSize(text.Text.Replace("\\r\\n", "\n")).Height;
                   if (tempHeight > maxHeight)
                       maxHeight = tempHeight;
                    
                    if(!(text is PrintPrimitiveHeaderText) && text.CurrentCellDef.Height>   maxHeight) 
                        maxHeight = text.CurrentCellDef.Height;
                }
                return maxHeight;
            }
           

            return height > 0
                       ? height
                       : PrintEngine.GetCellContentSize(texts[0].Text.Replace("\\r\\n", "\n")).Height;
                      //;//   : GetCellContentSize(null, texts[0].CurrentCellDef.HeadText.Replace("\\r\\n", "\n")).Height;
            return 33;
            //return GetCellContentSize(null, texts[0].CurrentCellDef.HeadText.Replace("\\r\\n", "\n")).Height;
        }
     
        Pen DefaultPen = new Pen(Color.RoyalBlue);
        public   void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds)
        {
            if(BottomLineVisible)
                AddBottomLine(DefaultPen);
            if(TopLineVisible)
                AddTopLine(DefaultPen);
            float oldXpos = xPos;
            float oldYpos = yPos;
            foreach (var tb in texts)
             { 
                 tb.Draw(engine,xPos,yPos,graphics,elementBounds);
                 xPos += tb.CurrentCellDef.Width * ReportDefaultSetting.BodyWidth/ 100;
             }
            foreach (var line in lines)
            {
                line.Draw(engine, oldXpos, oldYpos, graphics, elementBounds);
            }
           
        }
    }

    /// <summary>
    /// change
    /// added by Jacob 20120904
    /// </summary>
    public class PrintPrimitiveRowBlank : PrintPrimitiveRow
    {

    }

    public class PrintPrimitiveHeaderText :PrintPrimitiveText// PrintPrimitiveColumn
    {
        protected override float CalculateHeightForText(PrintEngine engine, Graphics graphics)
        {
            return CurrentCellDef.HeadHeight != 0 ? CurrentCellDef.HeadHeight :
                 PrintEngine.GetCellContentSize( Text.Replace("\\r\\n", "\n")).Height;
                //engine.PrintFont.GetHeight(graphics);
        }

        public PrintPrimitiveHeaderText(CellDefination cellDef)
            :base(cellDef)
        {
            
        }

        public override string Text
        {
            get
            {
                return  CurrentCellDef.HeadText;
            }
        }
        protected override void DrawBorder(PrintEngine engine, float xPos, float yPos, Graphics graphics)
        {
            var DashPen = new Pen(Color.Black);
            DashPen.DashStyle = DashStyle.Dash;
            int height = CurrentCellDef.Height;
            int colWidth = Convert.ToInt32(CurrentCellDef.Width * ReportDefaultSetting.BodyWidth / 100);
            if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos, yPos + height);
            if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos + colWidth, yPos);
            //if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos + height, xPos + colWidth, yPos + height);
            xPos += colWidth;
            if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos, yPos + height);
        }
       
    }
    public class PrintPrimitiveHeaderTextWithoutBorder : PrintPrimitiveText// PrintPrimitiveColumn
    {
        public PrintPrimitiveHeaderTextWithoutBorder(CellDefination cellDef)
            : base(cellDef)
        {

        }

        public override string Text
        {
            get
            {
                return CurrentCellDef.HeadText;
            }
        }
        protected override void DrawBorder(PrintEngine engine, float xPos, float yPos, Graphics graphics)
        {
            
        }
      
    }
    public class PrintPrimitiveSpanColumnText : PrintPrimitiveColumn 
    {
        public override string Text
        {
            get
            {
                return CurrentCellDef.HeadGroupColumnText;
            }
        }
      
        public PrintPrimitiveSpanColumnText(CellDefination cellDef)
        {
            CurrentCellDef = cellDef;
        }

        public  override float CalculateHeight(PrintEngine engine, Graphics graphics)
        {  string printText =Text;
           
            //return engine.PrintFont.GetHeight(graphics);
        return CurrentCellDef.Height > 0 ? CurrentCellDef.Height : PrintEngine.GetCellContentSize(printText).Height;
        }

        public override void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds)
        {
            Pen pen = new Pen(engine.PrintBrush, 1);
            string printText = engine.ReplaceTokens(CurrentCellDef.HeadGroupColumnText)
                .Replace("\\r\\n", "\r\n").ToUpper();
            RectangleF textArea = new RectangleF(xPos, yPos,
                                                 (float) CurrentCellDef.Width*ReportDefaultSetting.BodyWidth/100,
                                                 CurrentCellDef.Height);

            graphics.DrawString(printText, engine.PrintFont,
                engine.PrintBrush, textArea, new StringFormat());
            //added by Jacob 20121113
            //raise print text info.
            RaiseDrawString(printText, engine.PrintFont, textArea);
        }
    }

    public class PrintPrimitiveText : PrintPrimitiveColumn
    {   
       
        public PrintPrimitiveLine BottomLine;
        public PrintPrimitiveText(  )
        {
           
        }

        public PrintPrimitiveText(CellDefination cellDef)
        {
            CurrentCellDef = cellDef;
        }
        /// <summary>
        /// added by jacob 20120821
        /// </summary>
        /// <param name="cell"></param>
        public PrintPrimitiveText(TableCell cell)
        {
            CurrentCellDef = cell.cellDef;
            foreach (var line in cell.lines)
            {
                this.Lines.Add(new PrintPrimitiveLine(line));
            }

        }
        public    override  float CalculateHeight(PrintEngine engine, Graphics graphics)
        {
            return CalculateHeightForText(engine, graphics);
            //return engine.PrintFont.GetHeight(graphics);
        }

        protected virtual float CalculateHeightForText(PrintEngine engine, Graphics graphics)
        {
            return CurrentCellDef.Height != 0
                       ? CurrentCellDef.Height
                       : PrintEngine.GetCellContentSize(Text).Height;
             //engine.PrintFont.GetHeight(graphics);
        }
       

        public override void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds)
        {
            
            string data = Text;
            //if(string.IsNullOrEmpty(CurrentCellDef.DataFiled))

            string printText = engine.ReplaceTokens(data).Replace("\\r\\n", "\r\n");
            
            StringFormat style = null;
            if (CurrentCellDef.style != null)//Jacob
                style = CurrentCellDef.style;
            else
            {
                style = new StringFormat();
                style.LineAlignment = StringAlignment.Center;
                //style.Alignment = StringAlignment.Center;
            }
            RectangleF textArea = new RectangleF(xPos,
                                                 yPos,
                                                 (float) CurrentCellDef.Width*ReportDefaultSetting.BodyWidth/100,
                                                 CalculateHeightForText(engine, graphics));

            if (CurrentCellDef.CellFont == null) CurrentCellDef.CellFont = engine.PrintFont;
            graphics.DrawString(printText, CurrentCellDef.CellFont,
                engine.PrintBrush, textArea, style);
            //added by Jacob 20121113
            //raise print text info.
            RaiseDrawString(printText, engine.PrintFont, textArea);

            //added by jacob 20120821
            foreach (var line in Lines)
            {

                line.Draw(engine, xPos, yPos, graphics, elementBounds);
            }
            
            DrawBorder(engine, xPos, yPos, graphics);
        }

        protected virtual void DrawBorder(PrintEngine engine, float xPos, float yPos, Graphics graphics)
        {
            var DashPen = new Pen(Color.Black);
            DashPen.DashStyle = DashStyle.Dash;
            int height = CurrentCellDef.Height;
            int colWidth = Convert.ToInt32(CurrentCellDef.Width * ReportDefaultSetting.BodyWidth / 100);
            if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos, yPos + height);
            if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos + colWidth, yPos);
            if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos + height, xPos + colWidth, yPos + height);
            xPos += colWidth;
            if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos, yPos + height);
        }
    }

    public class PrintPrimitiveBlockText : PrintPrimitiveColumn
    {
        public string Text;
        public CellDefination CurrentCellDef = new DefaultCellDefination(); 

        public PrintPrimitiveBlockText(string buf)
        {
            Text = buf;
        }

        public override float CalculateHeight(PrintEngine engine, Graphics graphics)
        {
            return CurrentCellDef.Height != 0
                       ? CurrentCellDef.Height
                       : PrintEngine.GetCellContentSize(Text).Height;
        }

        public override void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds)
        {
            Pen pen = new Pen(engine.PrintBrush, 1);
            string printText = engine.ReplaceTokens(Text);

            RectangleF textArea = new RectangleF(CurrentCellDef.Left, CurrentCellDef.Top, CurrentCellDef.Width, CurrentCellDef.Height);
            graphics.DrawString(printText, engine.PrintFont,
                engine.PrintBrush, textArea, new StringFormat());
        }
    }

    public class PrintPrimitiveColumn : IPrintPrimitive
    { //added by jacob 20120821 start
        public List<PrintPrimitiveLine> Lines = new List<PrintPrimitiveLine>();
        public CellDefination CurrentCellDef = new DefaultCellDefination();
        public virtual string Text
        {
            get {//edited by Jacob 20131105
                var originText= CurrentCellDef.DataFiled ;
                var cellStyle = CurrentCellDef;
                DateTime dt = DateTime.MinValue;
                if (!string.IsNullOrEmpty(cellStyle.DataFormat))
                {
                    //currently only datetime format 
                    if (DateTime.TryParse(originText, out dt))
                    {
                        originText = string.Format("{0:" + cellStyle.DataFormat + "}", dt);
                    }
                    else//support decimal format.20130411 Jacob.
                    {
                        Decimal decTarget = Decimal.MinValue;
                        if (Decimal.TryParse(originText, out decTarget))
                        {
                            originText = string.Format("{0:" + cellStyle.DataFormat + "}", decTarget);
                        }
                    }

                }
                return cellStyle.IsUpperCase ? originText.ToUpper() : originText;
             }
        }

        //added by jacob 20121113 start
        //raise print text info.
        public event DrawStringHandler OnDrawString;
        protected void RaiseDrawString(string text, Font printFont, RectangleF textArea)
        {
            if (OnDrawString != null)
            {
                OnDrawString(new ReportTextPrintArg(text, printFont, textArea));
            }
        }
        //added by jacob 20121113 end

        public  virtual float CalculateHeight(PrintEngine engine, Graphics graphics)
        {
            throw new NotImplementedException();
        }

        public virtual void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds)
        {
            throw new NotImplementedException();
        }
    }
}
