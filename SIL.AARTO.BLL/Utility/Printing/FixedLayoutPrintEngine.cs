using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Xml;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using System.Collections.Generic;
using System.Collections;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FixedLayoutPrintEngine : PrintEngine
    {
        public override void CreateEngine(int rcIntNo)
        {
            ReportModel printModel = GetPrintTemplate(rcIntNo);
            //edited by jacob 20120606 start
            //DataSet dsData = GetPrintData(printModel);
            //if (dsData == null || dsData.Tables.Count < 1)
            //{
            //    throw new Exception("Print data is null");
            //}
            DataTable dtData = GetPrintData(printModel);
            //edited by jacob 20120606 end
            //DataSet dsData = GetPrintData(printModel);
            //if (dsData == null || dsData.Tables.Count < 1)
            //{
            //    throw new Exception("Print data is null");
            //}
            
            printTemplate = printModel.xmlDoc;
            if (printTemplate == null)
            {
                throw new Exception("Print template is null");
            }

            // Add data
            _printObjects = new ArrayList();
            DataColunms = new List<CellDefination>();
            int pageIndex = 0;
            XmlNodeList pageNodes = printTemplate.GetElementsByTagName("PageMain");
            for (int i = 0; i < pageNodes.Count; i++)
            {
                XmlNode pageXml = pageNodes[i];
                foreach (XmlNode node in pageXml.ChildNodes)
                {
                    int xPos = Convert.ToInt32(node.ChildNodes[1].InnerText);
                    int yPos = Convert.ToInt32(node.ChildNodes[2].InnerText);
                    int width = Convert.ToInt32(node.ChildNodes[3].InnerText);
                    int height = Convert.ToInt32(node.ChildNodes[4].InnerText);

                    CellDefination col = new CellDefination();
                    col.DataFiled = node.ChildNodes[0].InnerText;
                    col.Top = yPos;
                    col.Left = xPos;
                    col.Width = width;
                    col.Height = height;
                    col.Page = pageIndex;
                    DataColunms.Add(col);
                }
                pageIndex++;
            }

            for (int rwIdx = 0; rwIdx < dtData.Rows.Count; rwIdx++)
            {
                DataRow dr = dtData.Rows[rwIdx];
                _columnNum = 1;

                int pageIdx = 0;
                FreePage pageBlocks = new FreePage();
                foreach (CellDefination column in DataColunms)
                {
                    if (pageIdx != column.Page)
                    {
                        pageIdx = column.Page;
                        AddPrintObject(pageBlocks);
                        pageBlocks = new FreePage();
                    }
                    FreeCell cell = new FreeCell();
                    cell.cellStyle = column;
                    cell.Text = dr[column.DataFiled].ToString();
                    pageBlocks.AddToList(cell);
                }
                AddPrintObject(pageBlocks);
            }

            PageSettings settings = new PageSettings();
            settings.PaperSize = new PaperSize("US Std Fanfold", 950, 1200);
            settings.Margins = new Margins(0, 0, 0, 10);
            settings.Landscape = false;

            DefaultPageSettings = settings;
            //ShowPageSettings();
            ShowPreview();
            //_engine.Print();
        }

        protected  override ReportModel GetPrintTemplate(int rcIntNo)
        {
            ReportConfig Rc = SIL.AARTO.BLL.Report.ReportManager.GetReportById(rcIntNo);
            ReportModel model =  new ReportModel();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Rc.Template);

            model.xmlDoc = xmlDoc;
            model.StoredProcName = Rc.RspIntNoSource.ReportStoredProcName;
            model.DateFrom = Convert.ToDateTime("1996-01-01");
            model.DateTo = Convert.ToDateTime("2012-05-22");
            model.AutIntNo = Rc.AutIntNo.ToString();
            model.ReportName = Rc.ReportName;
            model.ReportCode = Rc.ReportCode;
            //model.StoredProcName = Rc.RspIntNoSource.ReportStoredProcName;
            return model;
        }

        protected override void MyBeginPrint(PrintEventArgs e)
        {
            // Go through the objects in the list and create print elements for each one
            foreach (IPrintable printObject in _printObjects)
            {
                PrintFixedAreaElement element = new PrintFixedAreaElement(printObject);
                _printElements.Add(element);

                printObject.Print(element);
            }
        }

        protected override void MyPrintPage(PrintPageEventArgs e)
        {
            _pageNum++;

            Rectangle printBounds = GetPageBounds();

            _columnWidth = Convert.ToInt32(printBounds.Width / _columnNum);

            float headerHeight = Header.CalculateHeight(this, e.Graphics);
            Header.Draw(this, printBounds.Top, e.Graphics, printBounds);

            float tblHeaderHeight = DataTableHeader.CalculateTblRowHeight(this, e.Graphics);
            DataTableHeader.Draw(this, printBounds.Top + headerHeight, e.Graphics, printBounds);

            Rectangle pageBounds = new Rectangle(printBounds.Left,
                                                 (int)(printBounds.Top + headerHeight + tblHeaderHeight), printBounds.Width,
                                                 (int)(printBounds.Height - headerHeight - tblHeaderHeight));

            float yPos = pageBounds.Top + 0;
            bool morePages = false;

            PrintFixedAreaElement element = (PrintFixedAreaElement)_printElements[_printIndex];

            element.Draw(this, yPos, e.Graphics, pageBounds);

            // next
            _printIndex++;

            if (_printIndex < _printElements.Count)
            {
                morePages = true;
            }

            e.HasMorePages = morePages;
        }

        protected override Rectangle GetPageBounds()
        {
            return new Rectangle(10, 0, 950, 1200);
        }
    }
}