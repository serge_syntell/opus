﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Text.RegularExpressions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data.SqlClient;
using System.Collections.Specialized;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.Report.Model;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.Xml;
using System.Data;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public interface IPrintable
    {
        void Print(PrintElement element);
    }

    public class TableCell : IPrintable
    { //added by jacob 20120821 start
        public List<CellLine> lines = new List<CellLine>();
        //added by jacob 20120821 end
        public CellDefination cellDef;

        public void Print(PrintElement element)
        {
        }
    }

    public class TableCellHeader : TableCell
    {
       
    }
    public class PageHeaderCell : TableCell
    {

    }
    public class TableRow : IPrintable
    {
        public CellDefination rowDef;
        public List<TableCell> cells = new List<TableCell>();
        //added by jacob 20120821 start
        public List<CellLine> lines = new List<CellLine>();
        //added by jacob 20120821 end
        public void Print(PrintElement element)
        {
            int rowHeight = rowDef == null ? 60 : rowDef.Height;
            var r = new PrintPrimitiveRow(rowHeight);
            r.rowDef = rowDef;
            int maxCellHeight = 0;
            foreach (TableCell cell in cells)
            {
                if(cell is TableCellHeader)
                {
                    r.texts.Add(new PrintPrimitiveHeaderText(cell.cellDef));
                }
                else if(cell is PageHeaderCell)
                {
                    r.texts.Add(new PrintPrimitiveHeaderTextWithoutBorder(cell.cellDef));
                }
                else
                if (cell.cellDef.ColumnSpan > 0)
                {   
                    r.texts.Add(new PrintPrimitiveSpanColumnText(cell.cellDef));
                }
                else
                //added by jacob 20120821
                {
                    r.texts.Add(new PrintPrimitiveText(cell));
                    //element.AddPrimitive(new PrintPrimitiveText(cell.cellDef));
                } 
                
                  
            }
            //Lines in tableRow should copy to PrintPrimitiveRow
            foreach(var line in lines)
            {
                r.lines.Add(new PrintPrimitiveLine(line)); 
            }

            element.AddPrimitive(r);// AddEmptyRow();
        }

        public void AddTopLine(Pen pen)
        {
            lines.Add( new CellLine
            {

                CellPen = pen,
                rx = 0,
                ry = 0,
                tx = rowDef.PageWidth,
                ty = 0
            }) ;


        }
        public void AddBottomLine(Pen pen)
        {
            lines.Add( new CellLine
            {

                CellPen = pen,
                rx = 0,
                ry = rowDef.Height,
                tx = rowDef.PageWidth,
                ty = rowDef.Height
            }) ;


        }
    }
    //Jacob added
    public class GroupSumRow : TableRow
    {
    }

    public class NewGroup : IPrintable
    {
        public List<PrintPrimitiveSpanColumnText> groupCols;
        public void Print(PrintElement element)
        {
        }
    }

    public class FixedArea : IPrintable
    {
        public string text;
        public Rectangle areaRectangle;

        public void Print(PrintElement element)
        {
            //element.AddFixedAreaText(text, areaRectangle);
        }
    }

    //added by jacob 20120821
    public class CellLine : IPrintable
    {
        private Pen pen = null;
        public CellLine(Pen inpen)
        {
            this.CellPen = inpen;
        }
        public CellLine()
        {
            this.CellPen = new Pen(Color.Red);
        }

        public int rx, ry, tx, ty;
        public virtual void Print(PrintElement element)
        {
            element.AddPrimitive(new PrintPrimitiveLine(this));
        }

        public int Width
        {
            get { return tx - rx; }
        }

        public Pen CellPen
        {
            get { return pen; }
            set { pen = value; }
        }
    }


}
