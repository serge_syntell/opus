﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Text.RegularExpressions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data.SqlClient;
using System.Collections.Specialized;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.Report.Model;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.Xml;
using System.Data;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class PrintElement
    {
        //added by jacob  20120821 begin
        public bool FirstRowOfPage = false;
        public bool LastRowOfPage = false;
        //added by jacob 20120821 end
       
        public ArrayList _printPrimitives = new ArrayList();
        public IPrintable _printObject;

        public PrintElement(IPrintable printObject)
        {
            _printObject = printObject;
        }

        public void AddEmptyRow()
        {
            AddPrimitive(new PrintPrimitiveEmptyRow());
        }

        public void AddSpanColumnText(CellDefination cellDef)
        {
            AddPrimitive(new PrintPrimitiveSpanColumnText(cellDef));
        }

        public void AddHeaderText(CellDefination cellDef)
        {
            AddPrimitive(new PrintPrimitiveHeaderText(cellDef));
        }

        public void AddText(CellDefination cellDef)
        {
            AddPrimitive(new PrintPrimitiveText(cellDef));
        }

        public void AddFixedAreaText(String buf)
        {
            AddPrimitive(new PrintPrimitiveBlockText(buf));
        }

        public void AddPrimitive(IPrintPrimitive primitive)
        {
            _printPrimitives.Add(primitive); 
        }

        public float CalculateHeight(PrintEngine engine, Graphics graphics)
        {
            return CalculateTblRowHeight(engine, graphics);
            float height = 0;
            foreach (IPrintPrimitive primitive in _printPrimitives)
            {
                height += primitive.CalculateHeight(engine, graphics);
            }
            return (float) Math.Ceiling(height);
        }

        public float CalculateTblRowHeight(PrintEngine engine, Graphics graphics)
        {
            float height = 0;
            string typeName = null;
            bool spanColumnAdded = false;
            foreach (IPrintPrimitive primitive in _printPrimitives)
            {
                
                if(primitive is PrintPrimitiveSpanColumnText)
                {
                    if(height==0 ||!spanColumnAdded)
                    {
                        height += primitive.CalculateHeight(engine, graphics);
                        spanColumnAdded = true;
                    }
                }
                if(primitive is PrintPrimitiveRow)
                {
                    height += ((PrintPrimitiveRow)primitive).CalculateHeight(engine, graphics);
                }
                if(primitive is PrintPrimitiveEmptyRow)
                {
                    height += ((PrintPrimitiveEmptyRow)primitive).CalculateHeight(engine, graphics);
                }
            }
            return height;
        }
        public Rectangle CurrentPageBounds;//added by jacob 20120822
        public virtual void Draw(PrintEngine engine, float yPos, Graphics graphics, Rectangle pageBounds)
        {
            CurrentPageBounds = pageBounds;
            float height = CalculateTblRowHeight(engine, graphics);// CalculateHeight(engine, graphics);
            int pageWidth = pageBounds.Right - pageBounds.Left;

            float xPos = pageBounds.Left;
            float oldXPos = pageBounds.Left;
            float oldYPos = yPos;
            float drawHeight = 0;
            Rectangle elementBounds = new Rectangle(pageBounds.Left, (int)yPos, pageWidth, (int)height);
            float preRowHeight = 0;
            foreach (IPrintPrimitive primitive in _printPrimitives)
            {
                
                string typeName = primitive.GetType().Name;
                
                if (typeName == "PrintPrimitiveRow")
                {
                    float colHeight = ((PrintPrimitiveRow)primitive).CalculateHeight(engine,graphics);// Convert.ToInt32(pageWidth * Convert.ToSingle(((PrintPrimitiveLine)primitive).LineDef.Width) / 100); 
                     //Row must start from page left.
                    if(xPos!=pageBounds.Left)
                    {
                        xPos = pageBounds.Left;
                        yPos += preRowHeight;
                        drawHeight += colHeight;

                    } preRowHeight = colHeight; 
                    //    colHeight; }

                    
                }
                else if (typeName == "PrintPrimitiveEmptyRow")
                {
                    float cHeight = primitive.CalculateHeight(engine, graphics);
                    drawHeight += cHeight;
                    //yPos += preRowHeight;
                    xPos = pageBounds.Left;
                  
                    //yPos += preRowHeight; 

                }
                primitive.Draw(engine, xPos, yPos, graphics, elementBounds);
                Pen DashPen=new Pen(Color.Black);
                switch (engine.LineStyle )
                {
                    case "Custom":
                        DashPen.DashStyle = DashStyle.Custom;
                        break;
                    case "Dash":
                        DashPen.DashStyle = DashStyle.Dash;
                        break;
                    case "DashDot":
                        DashPen.DashStyle = DashStyle.DashDot;
                        break;
                    case "DashDotDot":
                        DashPen.DashStyle = DashStyle.DashDotDot;
                        break;
                    case "Dot":
                        DashPen.DashStyle = DashStyle.Dot;
                        break;
                    case "Solid":
                        DashPen.DashStyle = DashStyle.Solid;
                        break;
                    default:
                        DashPen.DashStyle = DashStyle.Dash;
                        break;
                }
                 
                if (typeName == "PrintPrimitiveHeaderText")
                {
                    int colWidth = Convert.ToInt32(pageWidth * Convert.ToSingle(((PrintPrimitiveHeaderText)primitive).CurrentCellDef.Width) / 100);
                    if(engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos, yPos + height);
                    if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos + colWidth, yPos);
                    if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos + height, xPos + colWidth, yPos + height);
                    xPos += colWidth;
                    if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos, yPos + height);
                }
                else if (typeName == "PrintPrimitiveSpanColumnText")
                {
                    int colWidth = Convert.ToInt32(pageWidth * Convert.ToSingle(((PrintPrimitiveSpanColumnText)primitive).CurrentCellDef.Width) / 100);
                    xPos += colWidth;
                }
                else if (typeName == "PrintPrimitiveText")
                {
                    int colWidth =Convert.ToInt32(pageWidth * Convert.ToSingle(((PrintPrimitiveText)primitive).CurrentCellDef.Width) / 100);
                    if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos, yPos + height);
                    if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos + colWidth, yPos);
                    if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos + height, xPos + colWidth, yPos + height);
                    xPos += colWidth;
                    if (engine.ShowGridLine) graphics.DrawLine(DashPen, xPos, yPos, xPos, yPos + height);
                }
                else if (typeName == "PrintPrimitiveLine")
                {
                    int colWidth = ((PrintPrimitiveLine) primitive).LineDef.Width;// Convert.ToInt32(pageWidth * Convert.ToSingle(((PrintPrimitiveLine)primitive).LineDef.Width) / 100); 
                    xPos += colWidth;

                }
                else if (typeName == "PrintPrimitiveEmptyRow" || typeName == "PrintPrimitiveRow")
                {
                    xPos = pageBounds.Left;
                    yPos += preRowHeight;

                }
               else
                {
                    //xPos = pageBounds.Left;
                    //yPos += preRowHeight;// primitive.CalculateHeight(engine, graphics);
                } 
                
                
                preRowHeight = primitive.CalculateHeight(engine, graphics);
            }

            //bool drawOuterLine = true;
            //if (drawOuterLine && _printPrimitives.Count > 0 && 
            //    (_printPrimitives[0] is PrintPrimitiveText ))
            //{
            //    Pen OuterDashPen = new Pen(Color.Red, 1);
            //    OuterDashPen.DashStyle = DashStyle.Solid;
            //    graphics.DrawLine(OuterDashPen, oldXPos, oldYPos, oldXPos + pageWidth, oldYPos);
            //    graphics.DrawLine(OuterDashPen, oldXPos, oldYPos+10, oldXPos + pageWidth, oldYPos+10);

            //    graphics.DrawLine(OuterDashPen, oldXPos, oldYPos + height, oldXPos + pageWidth, oldYPos + height);
            //    graphics.DrawLine(OuterDashPen, oldXPos, yPos, oldXPos, yPos + height);
            //    graphics.DrawLine(OuterDashPen, oldXPos + pageWidth, oldYPos, oldXPos + pageWidth, oldYPos + height);
            //}
        }  

    }

    public class PrintFixedAreaElement : PrintElement
    {
        public PrintFixedAreaElement(IPrintable printObject) : base(printObject)
        {
            _printObject = printObject;
        }

        public override void Draw(PrintEngine engine, float yPos, Graphics graphics, Rectangle pageBounds)
        {
            foreach (PrintPrimitiveForBlockText primitive in _printPrimitives)
            {
                primitive.Draw(engine, 0, 0, graphics, pageBounds);
            }
        }
    }

}
