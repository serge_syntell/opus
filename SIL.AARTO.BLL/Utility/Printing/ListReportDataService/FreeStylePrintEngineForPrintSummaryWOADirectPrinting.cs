﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForPrintSummaryWOADirectPrinting : FreeStylePrintEngineForListReportDirectPrinting
    {
        /// <summary>
        /// Override for 2nd Notice direct printing, change data service when user changing mode 
        /// don't forget initializing data service.
        /// //2013-12-23 Heidi added for 2nd Notice Direct Printing (5101)
        /// </summary>
        public override ReportFormMode CurrentMode
        {
            get
            { return base.CurrentMode; }
            set
            {
                base.CurrentMode = value;
                if (value is ReportNewMode || value is ReportHistoryMode)
                {
                    CurrentReportDataService = new ReportDataServiceForSummaryWOA(DBConnectionString);
                    CurrentReportDataService.InitializeReportConfig(this.PrintModel.RcIntNo);
                }
                else if (value is NewFilesMode)
                {
                    CurrentReportDataService = new ReportDataServiceForSummaryWOANewFileDirectPrinting();
                    CurrentReportDataService.InitializeReportConfig(this.PrintModel.RcIntNo);
                }
                else if (value is CompletedFilesMode)
                {
                    CurrentReportDataService = new ReportDataServiceForSummaryWOACompletedFileDirectPrinting();
                    CurrentReportDataService.InitializeReportConfig(this.PrintModel.RcIntNo);
                    //                    CurrentReportDataService.RCIntNo = this.PrintModel.RcIntNo; //for getting LPT port
                }
            }
        }

        public FreeStylePrintEngineForPrintSummaryWOADirectPrinting(string conns)
        {
            CurrentReportCode = ReportConfigCodeList.WOARegisterDirectPrinting;
            this.DBConnectionString = conns;

        }

    }

    public class ReportDataServiceForSummaryWOANewFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\WOA Register"; }
        }

        public override string FieldForFileName
        {
            get { return "WOAPrintFileName"; }
        }


    }

    public class ReportDataServiceForSummaryWOACompletedFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\Completed\WOA Register"; }
        }

        public override string FieldForFileName
        {
            get { return "WOAPrintFileName"; }
        }

        //must override and make it empty to avoid moving files, as completed files can't move.
        public override void SaveReportDataToHistory(DataTable dataTable)
        {

        }
    }

}
