﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Utility.Printing
{
    /// <summary>
    /// Load db data, save as prn file.
    /// </summary> 
    public class FreeStylePrintEngineForListReportToFileService : FreeStylePrintEngineForService
    {
        protected ReportConfigCodeList CurrentReportCode { get; set; }

        protected override decimal charWidth
        {
            get
            {
                return 8.6M;
            }
        }

        public FreeStylePrintEngineForListReportToFileService()
        {


        }
        public FreeStylePrintEngineForListReportToFileService(ReportConfigCodeList code, string conns, string folder, string file)
        {
            this.CurrentReportCode = code;
            this.DBConnectionString = conns;
            this.ReportFilesFolder = folder; this.PrintFileName = file;
            // CurrentReportDataService = new ReportDataServiceForSummaryWOA(conns);

        }


        public override void PrintTestPage()
        {
            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)CurrentReportCode).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            listPrint.CreateEngineWithTestData(rc.RcIntNo);
        }
        public override void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)CurrentReportCode).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }
            PrintReportWithDataForListReport(rc, dtData, addHistory, onlyHistory);

        }

        public virtual void PrintReportWithDataForListReport(ReportConfig rc, DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {

            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = DBConnectionString;

            ReportModel printModel = GetPrintTemplate(rc.RcIntNo);
            listPrint.PrinterSettings.PrintToFile = true;
            listPrint.PrinterSettings.PrintFileName = "sample.prn";
            ListMonitor monitor = new ListMonitor(listPrint, this); //printing happens when monitor gets the 'end printing' message.

            listPrint.CreateMyEngine(printModel, dtData);
            // listPrint.CreateMyEngine(printModel, dtData);
            //will drive monitor to print.
            monitor.Unregister();
            rc.AutomaticGenerateLastDatetime = DateTime.Now;
            // 2013-07-16 add by Henry for LastUser
            rc.LastUser = CurrentReportDataService.LastUser;
            ReportManager.UpdateReport(rc);
        }
    }

}
