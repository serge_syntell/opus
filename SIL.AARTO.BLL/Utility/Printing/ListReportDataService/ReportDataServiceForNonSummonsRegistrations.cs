﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using SIL.AARTO.BLL.Report.Model;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForNonSummonsRegistrations : ListPrintEngine
    {
        //protected override void AddLineToDataTableHeader(PrintElement DataTableHeader) {}

        //protected override void AddLinesInMyPrintPage(PrintPageEventArgs e, float yPos, Rectangle pageBounds, PrintElement element) {}
        PrintEngine pe = new PrintEngine();
        public override void CreateEngine(int rcIntNo)
        {
            var printModel = CreateReportModel(rcIntNo);
            var dtData = CreatePrintData(printModel);
            
            foreach (DataRow dr in dtData.Rows)
            {
                dr.SetAdded();
            }

            var rdService = new ReportDataService();
            rdService.InitializeReportConfig(rcIntNo, this.connStr);
            rdService.SaveReportDataToHistory(dtData);

            CreateMyEngine(printModel, dtData);

            ModifyNonSummonsReportDate(dtData, printModel.ReportCode);
        }

        void ModifyNonSummonsReportDate(DataTable dtList, string reportCode)
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("NotIntNo");
            for (int i = 0; i < dtList.Rows.Count; i++)
            {
                dtInput.Rows.Add(dtList.Rows[i]["NotIntNo"].ToString());
            }

            SqlConnection myConnection = new SqlConnection(this.connStr);
            SqlCommand myCommand = new SqlCommand("Report_UpdateNonSummonsReportDate", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterPageNumber = new SqlParameter("@NotIntNoList", SqlDbType.Structured) { Value = dtInput };
            myCommand.Parameters.Add(parameterPageNumber);

            // 2013-07-23 add by Henry
            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar) { Value = base.LastUser };
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (this.WriteErrorMessage != null) this.WriteErrorMessage(string.Format(pe.GetResource("ReportEngineService_Error_ModNonSumRptDate"), ex.Message, reportCode));
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
            }
        }

        public override DataTable CreateHistoryPrintData(ReportModel printModel)
        {
            var paras = new[]
            {
                new SqlParameter("@RCIntNo", printModel.RcIntNo),
                new SqlParameter("@DateFrom", printModel.DateFrom),
                new SqlParameter("@DateTo", printModel.DateTo)
            };
            ServiceDB db = new ServiceDB(connStr);
            var ds = db.ExecuteDataSet("GetReportHistoryForNonSummonsRegistrations", paras);
            return ds.Tables[0];
        }

    }
}