﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ListPrintEngineGroupSummary : ListPrintEngine
    {
        protected List<string> GroupingFields;
        protected int LastRowIndex;
        protected int TotalRows
        {
            get { return DataSource == null ? -1 : DataSource.Rows.Count; }
        }
        protected int? RowHeight { get; set; }
        protected bool IsEndOfReport { get; set; }

        protected override void OnReadingRow(int rowIndex)
        {
            if (TotalRows <= 0) return;

            DataRow row = DataSource.Rows[rowIndex], nextRow = null;
            if (rowIndex < TotalRows - 1)
                nextRow = DataSource.Rows[rowIndex + 1];
            IsEndOfReport = nextRow == null;

            if (PagingFields != null && this.GroupingFields == null)
                this.GroupingFields = PagingFields.Cast<string>().ToList();
            OnInitParameters();

            string group = null, nextGroup = null;
            if (this.GroupingFields != null && this.GroupingFields.Count > 0)
            {
                this.GroupingFields.ForEach(f =>
                {
                    group += row[f];
                    if (nextRow != null)
                        nextGroup += nextRow[f];
                });
            }

            if (IsGroupEnd(row, group, nextRow, nextGroup))
            {
                BuildPrintRows(GetSection(rowIndex, RemoveCurrentSectionRow));
                if (!IsEndOfReport)
                    this.LastRowIndex = rowIndex + 1;
            }
        }

        List<DataRow> GetSection(int rowIndex, Func<DataRow, int, bool> removed = null)
        {
            var rowList = new List<DataRow>();

            if (TotalRows > 0
                && this.LastRowIndex <= rowIndex
                && rowIndex <= TotalRows - 1)
            {
                for (var i = this.LastRowIndex; i <= rowIndex; i++)
                {
                    var row = DataSource.Rows[i];

                    if (removed != null
                        && removed(row, i))
                        continue;

                    rowList.Add(row);
                }
            }

            return rowList;
        }

        protected TableCell BuildCell(int cellIndex, int? width = null, string text = "", StringAlignment? hAlign = null, StringAlignment? vAlign = null)
        {
            var cellDef = DataColunms[cellIndex];
            var style = cellDef.style;
            if (hAlign.HasValue)
                style.Alignment = hAlign.Value;
            if (vAlign.HasValue)
                style.LineAlignment = vAlign.Value;

            var cell = new CellDefination
            {
                DataFiled = text,
                Width = width ?? cellDef.Width,
                Height = cellDef.Height,
                CellFont = cellDef.CellFont,
                PageWidth = cellDef.PageWidth,
                style = style,
            };

            var tbCell = new TableCell
            {
                cellDef = cell,
                lines = cellDef.Lines
            };
            return tbCell;
        }

        protected TableRow BuildRow(params TableCell[] cells)
        {
            if (cells.Length <= 0)
            {
                return BuildRow(BuildCell(0, 100));
            }

            var row = new TableRow();
            for (var i = 0; i < cells.Length; i++)
            {
                var cell = cells[i];
                if (i == 0)
                {
                    row.rowDef = new CellDefination
                    {
                        Height = RowHeight ?? cell.cellDef.Height,
                        PageWidth = cell.cellDef.PageWidth,
                        Width = 100
                    };
                }
                row.cells.Add(cell);
            }

            return row;
        }

        #region needs override

        protected virtual void OnInitParameters() {}

        protected virtual bool IsGroupEnd(DataRow row, string group, DataRow nextRow, string nextGroup)
        {
            return false;
        }

        protected virtual bool RemoveCurrentSectionRow(DataRow row, int relativeIndex)
        {
            return false;
        }

        protected virtual void BuildPrintRows(List<DataRow> section) {}

        #endregion
    }
}
