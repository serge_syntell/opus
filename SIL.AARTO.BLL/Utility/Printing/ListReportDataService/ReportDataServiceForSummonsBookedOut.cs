using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Transactions;
using SIL.AARTO.BLL.Report.Model;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForSummonsBookedOut : ListPrintEngine
    {
        PrintEngine pe = new PrintEngine();
        /// <summary>
        /// Jacob add lines
        /// </summary>
        /// <param name="DataTableHeader"></param>
        protected override void AddLineToDataTableHeader(PrintElement DataTableHeader) {}

        protected override void AddLinesInMyPrintPage(PrintPageEventArgs e, float yPos, Rectangle pageBounds, PrintElement element) {}

        public override void CreateEngine(int rcIntNo)
        {
            var printModel = CreateReportModel(rcIntNo);
            var dtData = CreatePrintData(printModel);

            var rdService = new ReportDataService();
            rdService.InitializeReportConfig(rcIntNo, this.connStr);
            
            foreach (DataRow dr in dtData.Rows)
            {
                dr.SetAdded();
            }

            //2014-01-08 Heidi added CreateTransactionScope and changed code structure to prevent Transaction timeout.(bontq1767)
           
           DataTable dtOld = dtData.Copy();
           DataTable dtNew = dtData.Clone();

            int i = 0;
            int rows = 300;
            DataRow dr_tmp;

            for (int index = 0; index < dtOld.Rows.Count; index++) {

                i++;
                dr_tmp = dtNew.NewRow();

                foreach (DataColumn colum in dtOld.Columns)
                {
                    dr_tmp[colum.ColumnName] = dtOld.Rows[index][colum.ColumnName];
                }
                dtNew.Rows.Add(dr_tmp);
                dtOld.Rows.Remove(dtOld.Rows[index]);
                 
                if (i >=rows)
                {
                    break;
                }
            }

            while (dtNew.Rows.Count > 0)
            {
                using (var scope = ServiceUtility.CreateTransactionScope())
                {
                    rdService.SaveReportDataToHistory(dtNew);
                    ModifySummonsBookedOutStatus(dtNew, printModel.ReportCode);

                    if (ServiceUtility.TransactionCanCommit())
                    {
                        scope.Complete();
                    }
                    else
                    {
                        throw new TransactionAbortedException();
                    }

                }

                dtNew.Rows.Clear();
                if (dtOld.Rows.Count > 0)
                {
                    i= 0;
                    for (int index = 0; index < dtOld.Rows.Count; index++)
                    {

                        i++;
                        dr_tmp = dtNew.NewRow();

                        foreach (DataColumn colum in dtOld.Columns)
                        {
                            dr_tmp[colum.ColumnName] = dtOld.Rows[index][colum.ColumnName];
                        }
                        dtNew.Rows.Add(dr_tmp);
                        dtOld.Rows.Remove(dtOld.Rows[index]);

                        if (i >= rows)
                        {
                            break;
                        }
                    }
                }

            }


            //var rdService = new ReportDataService();
            //rdService.InitializeReportConfig(rcIntNo, this.connStr);
            //using (var scope = ServiceUtility.CreateTransactionScope())
            //{
            //    rdService.SaveReportDataToHistory(dtData);
            //    ModifySummonsBookedOutStatus(dtData, printModel.ReportCode);

            //    string ss = "";

            //    //if (ServiceUtility.TransactionCanCommit())
            //    //{
            //    //    scope.Complete();
            //    //}
            //    //else
            //    //{
            //    //    throw new TransactionAbortedException();
            //    //}
                
            //}

            CreateMyEngine(printModel, dtData);
           
            
        }

        void ModifySummonsBookedOutStatus(DataTable dtSumList, string reportCode)
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("SumIntNo");
            for (int i = 0; i < dtSumList.Rows.Count; i++)
            {
                dtInput.Rows.Add(dtSumList.Rows[i]["SumIntNo"].ToString());
            }

            SqlConnection myConnection = new SqlConnection(this.connStr);//2014-01-08 Heidi added CommandTimeout =0 (bontq1767)
            SqlCommand myCommand = new SqlCommand("Report_UpdateSummonsBookedOut", myConnection) { CommandTimeout =0 };
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterPageNumber = new SqlParameter("@SumIntNoList", SqlDbType.Structured) { Value = dtInput };
            myCommand.Parameters.Add(parameterPageNumber);

            // 2013-07-23 add by Henry
            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar) { Value = base.LastUser };
            myCommand.Parameters.Add(parameterLastUser);

            try
            {
                myConnection.Open();
                myCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (this.WriteErrorMessage != null) this.WriteErrorMessage(string.Format(pe.GetResource("ReportEngineService_Error_ModSumBookedOutStat"), ex.Message, reportCode));
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
            }
        }

        public override DataTable CreateHistoryPrintData(ReportModel printModel)
        {
            var paras = new[]
            {
                new SqlParameter("@RCIntNo", printModel.RcIntNo),
                new SqlParameter("@DateFrom", printModel.DateFrom),
                new SqlParameter("@DateTo", printModel.DateTo)
            };
            ServiceDB db = new ServiceDB(connStr);
            var ds = db.ExecuteDataSet("GetReportHistoryForSummonsBookedOut", paras);
            return ds.Tables[0];
        }
    }
}