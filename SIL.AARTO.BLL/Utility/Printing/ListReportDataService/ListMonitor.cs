﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ListMonitor
    {
        public ListMonitor(ListPrintEngine inlistEngine)
            : this(inlistEngine, new FreeStylePrintEngineForService(@"C:\sample\WOA", "ff_WOA_Reg.prn"))
        {


        }

        public ListMonitor(ListPrintEngine inlistEngine, FreeStylePrintEngine serviceEngine)
        {
            this.Register(inlistEngine);
            fileService = serviceEngine;
        }
        ListPrintEngine listEngine = null;
        public void Register(ListPrintEngine inlistEngine)
        {
            listEngine = inlistEngine;
            inlistEngine.OnDrawString += listEngine_OnDrawString;
            inlistEngine.OnStartPage += listEngine_OnStartPage;
            inlistEngine.OnEndPage += listEngine_OnEndPage;
            inlistEngine.OnEndPrint += inlistEngine_OnEndPrint;
        }


        FreeStylePrintEngine fileService = new FreeStylePrintEngineForService(@"C:\sample\WOA", "ff_WOA_Reg.prn");
        public void Unregister()
        {
            if (listEngine != null)
            {
                listEngine.OnDrawString -= listEngine_OnDrawString;
                listEngine.OnStartPage -= listEngine_OnStartPage;
                listEngine.OnEndPage -= listEngine_OnEndPage;
                listEngine.OnEndPrint -= inlistEngine_OnEndPrint;
            }
        }
        private List<FreePage> Pages = new List<FreePage>();
        private FreePage CurPage { get; set; }

        void listEngine_OnEndPage()
        {
            //print curpage
            Pages.Add(CurPage);

        }

        void listEngine_OnStartPage()
        {
            CurPage = new FreePage();
        }
        void inlistEngine_OnEndPrint(ReportTextPrintArg arg)
        {
            fileService.PrintPages(Pages, listEngine);

        }
        void listEngine_OnDrawString(ReportTextPrintArg arg)
        {
            CurPage.cells.Add(new FreeCell
            {
                PrintPortionDef = arg.Portion,
                cellStyle = new CellDefination
                {
                    Top = (int)arg.textArea.Top,
                    Left = (int)arg.textArea.Left,
                    Width = (int)arg.textArea.Width,
                    Height = (int)arg.textArea.Height
                },
                Text = arg.text
            });

        }
    }



}
