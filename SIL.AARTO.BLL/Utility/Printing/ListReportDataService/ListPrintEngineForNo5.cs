﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing.ListReportDataService
{
    //20130515 Jacob, Changed indexes for counting total.
    public class ListPrintEngineForNo5 : ListPrintEngine
    {
        private int s1, s2, s3;
        private int t1, t2, t3;
        protected override void BeforePrintBodyTail(PrintElement BodyTail)
        {
            int leftPad = 0;
            if (_printIndex > 0)
            {
                PrintElement preElement = (PrintElement)_printElements[_printIndex - 1];
                PrintPrimitiveRow preRow = preElement._printPrimitives[0] as PrintPrimitiveRow;
                if (preRow != null)
                {
                    leftPad =
                         preRow.texts[0].CurrentCellDef.Width +
                         preRow.texts[1].CurrentCellDef.Width +
                         preRow.texts[2].CurrentCellDef.Width +
                         preRow.texts[3].CurrentCellDef.Width;
                }
                BodyTail._printPrimitives.Clear();

                #region add PAGE TOTAL

                TableRow tailRow = new TableRow
                {
                    rowDef = new CellDefination
                    {
                        Height = 0
                    }
                };
                tailRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        style = new StringFormat(StringFormatFlags.DirectionRightToLeft),
                        Width = preRow.texts[0].CurrentCellDef.Width +
                            preRow.texts[1].CurrentCellDef.Width +
                            preRow.texts[2].CurrentCellDef.Width +
                            preRow.texts[3].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = "PAGE TOTAL"
                    }
                });
                tailRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {

                        Width = preRow.texts[4].CurrentCellDef.Width + preRow.texts[5].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = " "
                    }
                });

                tailRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        style = new StringFormat(StringFormatFlags.DirectionRightToLeft),
                        Width = preRow.texts[6].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = s1.ToString()
                    }
                });
                tailRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        style = new StringFormat(StringFormatFlags.DirectionRightToLeft),
                        Width = preRow.texts[7].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = s2.ToString()
                    }
                });

                tailRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        Width = preRow.texts[8].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = ""
                    }
                });

                tailRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        Width = preRow.texts[9].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = s3.ToString()
                    }
                });

                tailRow.Print(BodyTail);
                #endregion

                #region add TOTAL

                TableRow totalRow = new TableRow
                {
                    rowDef = new CellDefination
                    {
                        Height = 0
                    }
                };
                totalRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        style = new StringFormat(StringFormatFlags.DirectionRightToLeft),
                        Width = preRow.texts[0].CurrentCellDef.Width +
                            preRow.texts[1].CurrentCellDef.Width +
                            preRow.texts[2].CurrentCellDef.Width +
                            preRow.texts[3].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = "TOTAL"
                    }
                });
                totalRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {

                        Width = preRow.texts[4].CurrentCellDef.Width + preRow.texts[5].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = " "
                    }
                });

                totalRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        style = new StringFormat(StringFormatFlags.DirectionRightToLeft),
                        Width = preRow.texts[6].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = t1.ToString()
                    }
                });
                totalRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        style = new StringFormat(StringFormatFlags.DirectionRightToLeft),
                        Width = preRow.texts[7].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = t2.ToString()
                    }
                });

                totalRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        Width = preRow.texts[8].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = ""
                    }
                });

                totalRow.cells.Add(new TableCell
                {
                    cellDef = new CellDefination
                    {
                        Width = preRow.texts[9].CurrentCellDef.Width,
                        PageWidth = preRow.rowDef.PageWidth,
                        DataFiled = t3.ToString()
                    }
                });

                if (_printIndex == _printElements.Count)
                {
                    totalRow.Print(BodyTail);
                }
                #endregion

                BodyTail._printPrimitives.Add(new PrintPrimitiveEmptyRow());
                BodyTail._printPrimitives.AddRange(BodyTailTemplate._printPrimitives);
            }



        }

        protected override void AfterPrintBodyTail(PrintElement BodyTail)
        {
            s1 = 0; s2 = 0; s3 = 0;
        }

        protected override void AfterPrintElement(PrintElement element)
        {
            //Jaocb, no need to handle exception here.
            PrintPrimitiveRow row = element._printPrimitives[0] as PrintPrimitiveRow;
            try
            {

                int irow1 = string.IsNullOrEmpty(row.texts[6].Text) ? 0 : Convert.ToInt32(row.texts[6].Text);
                s1 += irow1;
                t1 += irow1;
            }

            catch { }

            try
            {

                int irow2 = string.IsNullOrEmpty(row.texts[7].Text) ? 0 : Convert.ToInt32(row.texts[7].Text);
                s2 += irow2;
                t2 += irow2;
            }

            catch { }

            try
            {

                int irow3 = string.IsNullOrEmpty(row.texts[9].Text) ? 0 : Convert.ToInt32(row.texts[9].Text);
                s3 += irow3;
                t3 += irow3;
            }

            catch { }
        }

    }
}
