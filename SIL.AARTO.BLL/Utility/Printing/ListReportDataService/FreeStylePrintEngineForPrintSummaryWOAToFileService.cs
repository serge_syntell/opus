﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForPrintSummaryWOAToFileService : FreeStylePrintEngineForListReportToFileService
    {
        public FreeStylePrintEngineForPrintSummaryWOAToFileService(string conns)
        {
            CurrentReportCode = ReportConfigCodeList.WOARegisterDirectPrinting;
            this.DBConnectionString = conns;

            CurrentReportDataService = new ReportDataServiceForSummaryWOA(conns);
            RowCount = 60;// Jake 2014-08-28 added to set WOA register direct printing row count
            FormLength = 11;//Jake 2014-08-28 set WOA register direct printing page length to 11 inch

        }
        public FreeStylePrintEngineForPrintSummaryWOAToFileService(string conns, string folder, string file)
        {
            CurrentReportCode = ReportConfigCodeList.WOARegisterDirectPrinting;
            this.DBConnectionString = conns;
            this.ReportFilesFolder = folder;
            this.PrintFileName = file;
            CurrentReportDataService = new ReportDataServiceForSummaryWOA(conns);
            RowCount = 60;// Jake 2014-08-28 added to set WOA register direct printing row count
            FormLength = 11;//Jake 2014-08-28 set WOA register direct printing page length to 11 inch
        }

    }


}
