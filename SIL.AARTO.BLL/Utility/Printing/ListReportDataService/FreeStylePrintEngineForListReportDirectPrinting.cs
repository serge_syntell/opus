﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Utility.Printing
{

    /// <summary>
    /// 1. Load prn file, send to LPT port.
    /// 2. Load history data from db, send to LPT port.
    /// </summary>
    public class FreeStylePrintEngineForListReportDirectPrinting : FreeStylePrintEngineForFileSystem
    {
        protected override decimal charWidth
        {
            get
            {
                return 8.6M;
            }
        }
        protected ReportConfigCodeList CurrentReportCode { get; set; }
        public FreeStylePrintEngineForListReportDirectPrinting() { }
        public override void PrintTestPage()
        {
            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)CurrentReportCode).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            listPrint.CreateEngineWithTestData(rc.RcIntNo);
        }

        public override void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {

            if (onlyHistory)
            {
                return;
            }
            if (CurrentMode is NewFilesMode || CurrentMode is CompletedFilesMode)
            {
                var report = new CPA5Report(this.RegisterLPTPort());
                foreach (DataRow dr in dtData.Rows)
                {
                    string file = dr[this.FieldForFileName].ToString();
                    using (TextReader rt = new StreamReader(file))
                    {
                        string s = rt.ReadToEnd();
                        //send to LPT;
                        report.PrintToLPT(s);
                        Thread.Sleep(200);//Heidi 2013-10-08 changed for fixed print error
                    }
                    // Process.Start(file);

                }
                //2013-12-26 added by Nancy start
                var dsFileService = CurrentReportDataService as ReportDataForFileService;
                //2014-01-15 changed for Repeat build Completed folder problem
                if (dsFileService != null && CurrentMode is NewFilesMode) { dsFileService.MoveFilesRepotToComplete(dtData); }
                //2013-12-26 added by Nancy end
            }
            else if (CurrentMode is ReportHistoryMode)
            {
                ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
                listPrint.connStr = DBConnectionString;

                ReportConfigService rcServ = new ReportConfigService();
                TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)CurrentReportCode).ToString() + "'");
                ReportConfig rc;
                if (rcList.Count > 0)
                {
                    rc = rcList[0];
                }
                else
                {
                    throw new Exception("Can't find control sheet config data");
                }
                PrintReportWithDataForListReport(rc, dtData, addHistory, onlyHistory);
            }
            else if (CurrentMode is ReportNewMode)
            {
                // 2013-10-15, Oscar removed - adding transaction for saving history and modifying print date and status
                //if (addHistory)
                //{
                //    CurrentReportDataService.SaveReportDataToHistory(dtData);
                //}
                if (onlyHistory)
                {
                    return;
                }

                ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
                listPrint.connStr = connStr;

                ReportConfigService rcServ = new ReportConfigService();
                TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)ReportConfigCodeList.WOASheet).ToString() + "'");
                ReportConfig rc;
                if (rcList.Count > 0)
                {
                    rc = rcList[0];
                }
                else
                {
                    throw new Exception("Can't find control sheet config data");
                }

                ReportModel printModel = GetPrintTemplate(rc.RcIntNo);


                listPrint.CreatePreviewEngine(printModel, dtData);
                //listPrint.CreateMyEngine(printModel, dtData);


                rc.AutomaticGenerateLastDatetime = DateTime.Now;
                // 2013-07-16 add by Henry for LastUser
                rc.LastUser = CurrentReportDataService.LastUser;
                ReportManager.UpdateReport(rc);
            }
        }

        public virtual void PrintReportWithDataForListReport(ReportConfig rc, DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {

            if (onlyHistory)
            {
                return;
            }

            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = DBConnectionString;

            ReportModel printModel = GetPrintTemplate(rc.RcIntNo);
            listPrint.PrinterSettings.PrintToFile = true;
            listPrint.PrinterSettings.PrintFileName = "sample.prn";
            ListMonitor monitor = new ListMonitor(listPrint, this); //printing happens when monitor gets the 'end printing' message.

            listPrint.CreateMyEngine(printModel, dtData);
            // listPrint.CreateMyEngine(printModel, dtData);

            monitor.Unregister();
            //rc.AutomaticGenerateLastDatetime = DateTime.Now;
            //// 2013-07-16 add by Henry for LastUser
            //rc.LastUser = CurrentReportDataService.LastUser;
            //ReportManager.UpdateReport(rc);
        }
    }

}
