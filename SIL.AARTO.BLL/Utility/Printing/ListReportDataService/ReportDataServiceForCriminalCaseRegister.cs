﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using SIL.ServiceBase;
using SIL.ServiceQueueLibrary.DAL.Services;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.BLL.Report.Model;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForCriminalCaseRegister : ListPrintEngine
    {
        DataTable history;
        ArrayList arrPageIndex = new ArrayList();
        PrintEngine pe = new PrintEngine();
        string colstr_S54Impersonal = "** SECTION 54 SUMMONSES (NON-PERSONAL SERVICES) **";
        string colstr_S54Personal = "** SECTION 54 SUMMONSES (PERSONAL SERVICES) **";
        string colstr_S56 = "** SECTION 56 SUMMONSES **";
        string colstr = "** SECTION ** SUMMONSES **";

        public ReportDataServiceForCriminalCaseRegister()
        {
            this.history = new DataTable();
            this.history.Columns.Add("PrintFileName", typeof(string));
            this.DirectPrintAfterLoadHistory = false; //added by Jacob 20131114
            ReprintSetting = new CCRReprintSetting();
        }

        // Oscar 20121107 added for print setting
        public CCRReprintSetting ReprintSetting { get; set; }

        public override void CreateEngine(int rcIntNo)
        {
            var printModel = CreateReportModel(rcIntNo);
            CreateMyEngineWithQueues(printModel);
        }

        /// <summary>
        /// Jacob added 20140124,  check if file is already in history table.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private bool IsFileInHistory(string fileName)
        {
            ServiceDB db = new ServiceDB(connStr);
            //Jacob fix code bug.
            //SqlParameter[] paras = new SqlParameter[9]; 
            //paras[1] = new SqlParameter("@FileName", fileName);
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@FileName", fileName);
            var dt = db.ExecuteDataSet("GetCCRFileCountInHistory", paras, true, 0).Tables[0];
            
            return int.Parse(dt.Rows[0][0].ToString()) > 0 ? true : false; 
        }

        public void CreateMyEngineWithQueues(ReportModel printModel)
        {
            // Read the first queue
            QueueItemProcessor qProcessor = new QueueItemProcessor();
            QueueItem que;
            PrintCourtRollFileName entity = null;
            while ((que = qProcessor.Receive(ServiceQueueTypeList.PrintCourtRoll, "", DateTime.Now)) != null)
            {
                this.history.Clear();

                entity = GetPrintParam(printModel, que);
                if (entity == null) continue;

                var newHistory = this.history.NewRow();
                var fileName = entity.CombineName();
                if (fileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
                    fileName = fileName.Remove(fileName.Length - 4, 4);
                newHistory[0] = fileName;

                //Jacob added 20140124, check if fileName exists in history table.
                if (IsFileInHistory(fileName))
                {
                    Logger(2, false, "Print court roll CCR has error: duplicate file- {0} ", fileName);
                    que.IsSuccessful = false;
                    que.Status = QueueItemStatus.Discard;
                    qProcessor.DeQueue(que);
                }
                else
                {
                    this.history.Rows.Add(newHistory);

                    // Oscar 2013-04-02 added try-catch
                    try
                    {
                        PrintReport(entity, printModel, false);
                    }
                    catch (Exception ex)
                    {
                        Logger(2, false, "Print court roll CCR has error: {0}", ex.ToString());
                    }

                    var historyDB = new ReportDataService();
                    historyDB.InitializeReportConfig(printModel.RcIntNo, this.connStr);
                    historyDB.SaveReportDataToHistory(this.history);

                    que.IsSuccessful = true;
                    que.Status = QueueItemStatus.Discard;
                    qProcessor.DeQueue(que);
                }
            }
        }

        public void BuildEngineByTypes(PrintCourtRollFileName entity, string sumType, string sumServedStatus, int courtRollType, ReportModel printModel, bool isPreview)
        {
            bool isDataEmpty = false;
            arrPageIndex = new ArrayList();
            DataTable dtData = GetPrintData(entity, sumType, sumServedStatus, courtRollType);
            DataTable dtDataForIndex = GetPrintDataForIndex(entity, sumType, sumServedStatus, courtRollType);
            // 2012.11.15 Nick changed, When the report data row is 0, then print report CCR one page with empty rows
            //if ((dtData == null || dtData.Rows.Count == 0)
                //&& !(sumType == "S54" && sumServedStatus == "1" && courtRollType <= 1))
            if (dtData == null) return;

            if (!isPreview || ReprintSetting.Print1st)
            {
                // Print CCR
                ClearEngineData();
                if (dtData.Rows.Count == 0)
                {
                    AuthorityService autServ = new AuthorityService();
                    DAL.Entities.Authority aut = autServ.GetByAutIntNo(entity.AutIntNo);
                    string[] courtDate = entity.CourtDate.Split('-', ' ', ':');
                    string groupColStr_Aut = aut.AutNo + " " + aut.AutName;
                    string groupColStr_Court = entity.CourtName;
                    string groupColStr_CourtRoom = entity.CourtRoomNo;
                    //2013-03-18 updated by Nancy(exchage the location of 'courtDate[2]' and 'courtDate[0]') 
                    // string groupColStr_TrailDate = string.Format("{0}/{1}/{2}", courtDate[2], courtDate[1], courtDate[0]);
                    string groupColStr_TrailDate = string.Format("{0}/{1}/{2}", courtDate[0], courtDate[1], courtDate[2]);

                    DataRow row = dtData.NewRow();
                    row["AutName"] = groupColStr_Aut;
                    row["CrtName"] = groupColStr_Court;
                    row["CrtRoomNo"] = groupColStr_CourtRoom;
                    row["SumCourtDate"] = groupColStr_TrailDate;
                    dtData.Rows.Add(row);
                    dtData.Rows.Add(row.ItemArray);
                    dtData.Rows.Add(row.ItemArray);

                    //row = dtData.NewRow();
                    //dtData.Rows.Add(row);
                    //row = dtData.NewRow();
                    //dtData.Rows.Add(row);
                    isDataEmpty = true;
                }

                if (isPreview)
                    base.CreatePreviewEngine(printModel, dtData);
                else
                    base.CreateMyEngine(printModel, dtData);
                //return;
            }

            if (isDataEmpty) 
                return;

            ReportConfigService rcServ = new ReportConfigService();

            if (!isPreview || ReprintSetting.Print2nd)
            {
                // Print Report Surname Index for CCR
                ClearEngineData();
                printModel = CreateReportModel(rcServ.Find("ReportCode='" + ((int) ReportConfigCodeList.IndexSurnameForCriminalCaseRegister).ToString().PadLeft(6, '0') + "'").FirstOrDefault().RcIntNo);
                dtDataForIndex = AddPageIndex(dtDataForIndex, arrPageIndex);
                dtDataForIndex = SortTable(dtDataForIndex, "AccSurname");
                if (dtDataForIndex == null || dtDataForIndex.Rows.Count == 0)
                    return;
                if (isPreview)
                    base.CreatePreviewEngine(printModel, dtDataForIndex);
                else
                    base.CreateMyEngine(printModel, dtDataForIndex);
            }

            if (!isPreview || ReprintSetting.Print3rd)
            {
                // Print Report Notice Number Index for CCR
                ClearEngineData();
                printModel = CreateReportModel(rcServ.Find("ReportCode='" + ((int) ReportConfigCodeList.IndexNoticeNumberForCriminalCaseRegister).ToString().PadLeft(6, '0') + "'").FirstOrDefault().RcIntNo);
                dtDataForIndex = SortTable(dtDataForIndex, "SumNoticeNo");
                if (dtDataForIndex == null || dtDataForIndex.Rows.Count == 0)
                    return;
                if (isPreview)
                    base.CreatePreviewEngine(printModel, dtDataForIndex);
                else
                    base.CreateMyEngine(printModel, dtDataForIndex);
            }

            if (isPreview && ReprintSetting.Print4th)
            {
                // Print Report Charge sheet annexures for CCR
                if (sumType == "S54" && sumServedStatus == "1")
                    return;
                ClearEngineData();
                printModel = CreateReportModel(rcServ.Find("ReportCode='" + ((int) ReportConfigCodeList.ChargeSheetAnnexuresForCriminalCaseRegister).ToString().PadLeft(6, '0') + "'").FirstOrDefault().RcIntNo);
                dtData = GetPrintDataForAnnexure(entity, sumType, courtRollType);
                if (dtData == null || dtData.Rows.Count == 0)
                    return;
                if (isPreview)
                    base.CreatePreviewEngine(printModel, dtData);
                else
                    base.CreateMyEngine(printModel, dtData);
            }
        }

        DataTable SortTable(DataTable dt, string sortColumn)
        {
            DataView sortView = dt.DefaultView;
            sortView.Sort = sortColumn;
            return sortView.ToTable();
        }

        // Nick 2012.09.19 add for add the page index for next index report
        DataTable AddPageIndex(DataTable dt, ArrayList arrPageNum)
        {
            dt.Columns.Add("PageIndex", Type.GetType("System.Int32"));
            for (int r=0; r<dt.Rows.Count; r++)
            {
                dt.Rows[r]["PageIndex"] = arrPageNum[r];
            }
            return dt;
        }

        public PrintCourtRollFileName GetPrintParam(ReportModel model, QueueItem que)
        {
            AuthorityService autServ = new AuthorityService();
            CourtRulesDB crtRulesDB = new CourtRulesDB(connStr);

            int pfnIntNo = 0;
            int noOfDays = 0;
            string autCode = "";
            int autIntNo = 0;
            AARTORules rules = AARTORules.GetSingleton();

            pfnIntNo = Convert.ToInt32(que.Body);
            autCode = que.Group;
            autIntNo = autServ.GetByAutCode(autCode).AutIntNo;
            if (autIntNo != Convert.ToInt32(model.AutIntNo))
            {
                if (WriteErrorMessage != null) WriteErrorMessage(string.Format(pe.GetResource("ReportEngineService_Error_CCRInvalidAuth"), que.ID, model.ReportCode));
                return null;
            }

            rules.Initialize(this.connStr, autIntNo);
            noOfDays = rules.GetDateRule("CDate", "FinalCourtRoll").DtRNoOfDays;

            PrintCourtRollFileName fnEntity = new PrintCourtRollFileName(this.connStr);
            fnEntity = fnEntity.DepartName(pfnIntNo);
            //Edited by Jacob 20130114 start
            // add an error log before return null.
            //if (fnEntity == null) return null;
            if (fnEntity == null)
            {
                WriteErrorMessage(string.Format("Report Engine Service: Error happened. Report code:{0}." +
                                                " There's no valid print file name with id : {1}," +
                                                " or can not parse the print file name. Please check.",model.ReportCode, pfnIntNo));
                return null;
            
            }
            
            //Edited by Jacob 20130114 end
            return FormatPrintCourtRollFileName(fnEntity, autIntNo, noOfDays, que.ID, model.ReportCode);
        }

        PrintCourtRollFileName FormatPrintCourtRollFileName(PrintCourtRollFileName fnEntity, int autIntNo, int noOfDays, object key, string ReportCode)
        {
            if (fnEntity == null) return new PrintCourtRollFileName(connStr);

            fnEntity.AutIntNo = autIntNo;

            DateTime crtDate = Convert.ToDateTime(fnEntity.CourtDate);
            //DateTime prtDate = Convert.ToDateTime(fnEntity.PrintDate);

            CourtDB courtDB = new CourtDB(this.connStr);
            DateTime dt = courtDB.CourtDateByCourt(fnEntity.CrtIntNo, crtDate);
            if (dt.CompareTo(new DateTime(2000, 1, 1)) <= 0)
            {
                decimal queID;
                if (WriteErrorMessage != null)
                    WriteErrorMessage(string.Format(pe.GetResource("ReportEngineService_Error_CCRInvalidCrtDate"), fnEntity.CourtDate, decimal.TryParse(Convert.ToString(key), out queID) ? queID.ToString() : Convert.ToString(key), ReportCode));
                return null;
            }

            string reportType = "P";
            dt = dt.AddDays(noOfDays);
            if (dt.CompareTo(DateTime.Now) <= 0)
                reportType = "F";

            fnEntity.ReportType = reportType;
            CourtRulesDB crtRuleDB = new CourtRulesDB(connStr);
            CourtRulesDetails crDet = new CourtRulesDetails();
            crDet.CrtIntNo = fnEntity.CrtIntNo;
            crDet.CRCode = "1010";
            crDet = crtRuleDB.GetDefaultCourtRule(crDet);
            fnEntity.CourtRollType = crDet.CRString.Trim().ToUpper() == "Y" ? 0 : 1;

            return fnEntity;
        }

        void PrintReport(PrintCourtRollFileName entity, ReportModel printModel, bool isPreview)
        {
            if (entity.CourtRollType == 0)
            {
                // CourtRollType is 0 , mix the CAM/HWO
                // for S54 and Personal service type reports
                colstr = colstr_S54Personal;
                BuildEngineByTypes(entity, "S54", "1", 0, printModel, isPreview);
                //return;
                // for S54 and Impersonal service type reports
                colstr = colstr_S54Impersonal;
                BuildEngineByTypes(entity, "S54", "2", 0, printModel, isPreview);

                // for S56 and Personal service type reports
                colstr = colstr_S56;
                BuildEngineByTypes(entity, "S56", "1", 0, printModel, isPreview);
            }
            else
            {
                // CourtRollType is 1 , separate the CAM/HWO
                colstr = colstr_S54Personal;
                BuildEngineByTypes(entity, "S54", "1", 1, printModel, isPreview);
                colstr = colstr_S54Impersonal;
                BuildEngineByTypes(entity, "S54", "2", 1, printModel, isPreview);
                colstr = colstr_S56;
                BuildEngineByTypes(entity, "S56", "1", 1, printModel, isPreview);

                colstr = colstr_S54Personal;
                BuildEngineByTypes(entity, "S54", "1", 2, printModel, isPreview);
                colstr = colstr_S54Impersonal;
                BuildEngineByTypes(entity, "S54", "2", 2, printModel, isPreview);
                colstr = colstr_S56;
                BuildEngineByTypes(entity, "S56", "1", 2, printModel, isPreview);
            }
        }

        public DataTable GetPrintData(PrintCourtRollFileName entity, string sumType, string sumServedStatus, int courtRollType)
        {            
            ServiceDB db = new ServiceDB(connStr);
            SqlParameter[] paras = new SqlParameter[9];
            paras[0] = new SqlParameter("@AutIntNo", entity.AutIntNo);
            paras[1] = new SqlParameter("@CrtIntNo", entity.CrtIntNo);
            paras[2] = new SqlParameter("@CrtRIntNo", entity.CrtRIntNo);
            paras[3] = new SqlParameter("@Type", sumType);
            paras[4] = new SqlParameter("@SumServedStatus", sumServedStatus);
            paras[5] = new SqlParameter("@SumCourtDate", entity.CourtDate);
            paras[6] = new SqlParameter("@FinalPrintDate", entity.PrintDate);
            paras[7] = new SqlParameter("@FinalFlag", entity.ReportType);
            paras[8] = new SqlParameter("@CourtRollType", courtRollType);

            //return db.ExecuteDataSet("Report_CourtRoll", paras).Tables[0];
            // 2013-10-17, Oscar added timeout
            return db.ExecuteDataSet("Report_CourtRoll", paras, true, 0).Tables[0];
        }

        public DataTable GetPrintDataForIndex(PrintCourtRollFileName entity, string sumType, string sumServedStatus, int courtRollType)
        {
            ServiceDB db = new ServiceDB(connStr);
            SqlParameter[] paras = new SqlParameter[9];
            paras[0] = new SqlParameter("@AutIntNo", entity.AutIntNo);
            paras[1] = new SqlParameter("@CrtIntNo", entity.CrtIntNo);
            paras[2] = new SqlParameter("@CrtRIntNo", entity.CrtRIntNo);
            paras[3] = new SqlParameter("@Type", sumType);
            paras[4] = new SqlParameter("@SumServedStatus", sumServedStatus);
            paras[5] = new SqlParameter("@SumCourtDate", entity.CourtDate);
            paras[6] = new SqlParameter("@FinalPrintDate", entity.PrintDate);
            paras[7] = new SqlParameter("@FinalFlag", entity.ReportType);
            paras[8] = new SqlParameter("@CourtRollType", courtRollType);

            //return db.ExecuteDataSet("Report_CourtRollForIndex", paras).Tables[0];
            // 2013-10-17, Oscar added timeout
            return db.ExecuteDataSet("Report_CourtRollForIndex", paras, true, 0).Tables[0];
        }
        public DataTable GetPrintDataForAnnexure(PrintCourtRollFileName entity, string sumType, int courtRollType)
        {
            ServiceDB db = new ServiceDB(connStr);
            SqlParameter[] paras = new SqlParameter[8];
            paras[0] = new SqlParameter("@AutIntNo", entity.AutIntNo);
            paras[1] = new SqlParameter("@CrtIntNo", entity.CrtIntNo);
            paras[2] = new SqlParameter("@CrtRIntNo", entity.CrtRIntNo);
            paras[3] = new SqlParameter("@Type", sumType);
            paras[4] = new SqlParameter("@SumCourtDate", entity.CourtDate);
            paras[5] = new SqlParameter("@FinalPrintDate", entity.PrintDate);
            paras[6] = new SqlParameter("@FinalFlag", entity.ReportType);
            paras[7] = new SqlParameter("@CourtRollType", courtRollType);

            //return db.ExecuteDataSet("Report_CourtRollChargeSheet", paras).Tables[0];
            // 2013-10-17, Oscar added timeout
            return db.ExecuteDataSet("Report_CourtRollChargeSheet", paras, true, 0).Tables[0];
        }

        #region For getting history data
        public override DataTable CreateHistoryPrintData(ReportModel printModel)
        {
            var paras = new[]
            {
                new SqlParameter("@RCIntNo", printModel.RcIntNo),
                new SqlParameter("@DateFrom", printModel.DateFrom),
                new SqlParameter("@DateTo", printModel.DateTo)
            };
            ServiceDB db = new ServiceDB(connStr);
            var ds = db.ExecuteDataSet("GetReportHistoryForCriminalCaseRegister", paras);
            return ds.Tables[0];
        }

        // 20121011 Nick changed CreateMyEngine to CreatePreviewEngine for winform app preview
        /*
        public override void CreateMyEngine(ReportModel printModel, DataTable dtData)
        {
            int autIntNo;
            int.TryParse(printModel.AutIntNo, out autIntNo);

            AARTORules rules = AARTORules.GetSingleton();
            rules.Initialize(this.connStr, autIntNo);
            int noOfDays = rules.GetDateRule("CDate", "FinalCourtRoll").DtRNoOfDays;

            var fnEntity = new PrintCourtRollFileName(connStr);
            
            foreach (DataRow dr in dtData.Rows)
            {
                var fileName = dr["PrintFileName"].ToString();
                var entity = fnEntity.DepartName(fileName);
                entity = FormatPrintCourtRollFileName(entity, autIntNo, noOfDays, fileName);
                PrintReport(entity, printModel, false);
            }
        }
        */
        public override void CreatePreviewEngine(ReportModel printModel, DataTable dtData)
        {
            int autIntNo;
            int.TryParse(printModel.AutIntNo, out autIntNo);

            AARTORules rules = AARTORules.GetSingleton();
            rules.Initialize(this.connStr, autIntNo);
            int noOfDays = rules.GetDateRule("CDate", "FinalCourtRoll").DtRNoOfDays;

            var fnEntity = new PrintCourtRollFileName(connStr);

            foreach (DataRow dr in dtData.Rows)
            {
                var fileName = dr["PrintFileName"].ToString();
                var entity = fnEntity.DepartName(fileName);
                entity = FormatPrintCourtRollFileName(entity, autIntNo, noOfDays, fileName, printModel.ReportCode);
                if (entity == null) continue;
                PrintReport(entity, printModel, true);
            }
        }

        protected override DataTable GetPrintData(ReportModel model)
        {
            var fnEntity = new PrintCourtRollFileName("")
                               {
                                   AutIntNo = 0,
                                   CrtIntNo = 0,
                                   CrtRIntNo = 0,
                                   CourtDate = DateTime.Now.ToString("yyyy-MM-dd"),
                                   PrintDate = DateTime.Now.ToString("yyyy-MM-dd"),
                                   ReportType = ""
                               };
            return GetPrintData(fnEntity, "S54", "1", 0);
        }
        #endregion

        // Nick 2012.09.19 add for store the page index for next index report
        protected override void StorePageIndex(int pageNum)
        {
            arrPageIndex.Add(pageNum);
            return;
        }

        protected override void AddLinesInMyPrintPage(PrintPageEventArgs e, float yPos, System.Drawing.Rectangle pageBounds, PrintElement element)
        {
            //return;
            if(Convert.ToInt32(PrintModel.ReportCode) != (int)ReportConfigCodeList.CriminalCaseRegister)
            {
                 return;
            }
            //return;
            var height = element.CalculateTblRowHeight(this, e.Graphics);
            PrintElement preElement = _printIndex > 0 ? (PrintElement)_printElements[_printIndex - 1] : null;
           
            PrintElement next = _printIndex < _printElements.Count - 1 ? (PrintElement)_printElements[_printIndex + 1] : null;

            PrintPrimitiveRow row = element._printPrimitives[0] as PrintPrimitiveRow;
            //last row in one page, or last row in whole data, or last row before new group object.
            //add the bottom line.
            if (yPos + height * 2 > pageBounds.Bottom || next == null || next._printObject.GetType().Name == "NewGroup"
                || next._printObject is GroupSumRow)     //yPos + height * 2 > pageBounds.Bottom ||   next== null
            {
             

                if (row != null)
                {
                    row.AddBottomLinePad(new Pen(Color.Black), 0 );
                    row.AddBottomLinePad(new Pen(Color.Black), -2);
                    

                }
            }
            // Nick 20121009 Moved below
            //if (preElement._printObject is NewGroup || preElement._printObject is GroupSumRow)
            //{ 
            //    if (row != null)
            //    {
            //        row.AddTopLinePad(solidPen, 0);
            //        row.AddTopLinePad(solidPen, 2);
            //       // row.TopLineVisible = true;
            //        //row.AddTopLine(solidPen);

            //    }
            //}
           if(preElement!=null)
           {
               if (preElement._printObject is NewGroup || preElement._printObject is GroupSumRow)
               {
                   if (row != null)
                   {
                       row.AddTopLinePad(solidPen, 0);
                       row.AddTopLinePad(solidPen, 2);
                       // row.TopLineVisible = true;
                       //row.AddTopLine(solidPen);

                   }
               }
               PrintPrimitiveRow prerow =   preElement._printPrimitives.Count>0?( preElement._printPrimitives[0] as PrintPrimitiveRow):null;
               if (prerow != null && row!=null)
               {
                   if (element.FirstRowOfPage)
                   {
                       row.AddTopLinePad(solidPen, 0);
                       row.AddTopLinePad(solidPen, 2);
                   }
                   else
                   {
                       //Edited by Jacob 20121213 start
                       // Change the compare rule for choosing line styles between rows 
                       //       from equal to partial contains, as the data source has changed in sp.
                       //if (prerow.texts[0].Text.Equals(row.texts[0].Text))
                       if (!String.IsNullOrEmpty(prerow.texts[0].Text.Trim()) &&
                              prerow.texts[0].Text.Replace("\r\n", "").Contains(row.texts[0].Text.Replace("\r\n", "")))
                       //Edited by Jacob 20121213 end
                       {
                           row.AddTopLinePad(dashPen, 0);
                       }
                       else
                       {
                           row.AddTopLinePad(solidPen, 0);
                           row.AddTopLinePad(solidPen, 2);
                       }
                   } 

               }
           }
            if(row !=null)
            {//add right border
                foreach(var c in row.texts)
                {
                    if(row.texts.IndexOf(c)<row.texts.Count-1)
                    c.Lines.Add(new PrintPrimitiveLine(new CellLine
                                                           {
                                                               
                                                               CellPen = solidPen,
                                                              
                                                               rx= c.CurrentCellDef.Width*c.CurrentCellDef.PageWidth/100,
                                                               ry=5,

                                                               tx = c.CurrentCellDef.Width * c.CurrentCellDef.PageWidth / 100,
                                                               ty = (int)c.CalculateHeight(this,e.Graphics)-3,
                                                           }));
                }
            }



        }

        protected override PrintElement GetHeaderCols2Data(PrintElement headerCols)
        {
            ((PrintPrimitiveRow)headerCols._printPrimitives[0]).AddTopLinePad(dashPen, -3);
            ((PrintPrimitiveRow)headerCols._printPrimitives[0]).AddTopLinePad(dashPen, -1);
            ((PrintPrimitiveText)((PrintPrimitiveRow)headerCols._printPrimitives[0]).texts[0]).CurrentCellDef.DataFiled = colstr;
            return headerCols;
        }


    }

    public class PrintCourtRollFileName
    {
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string AutCode { get; set; }
        public string CourtName { get; set; }
        public int CrtIntNo { get; set; }
        public string CourtRoomNo { get; set; }
        public int CrtRIntNo { get; set; }
        public string CourtDate { get; set; }
        public string PrintDate { get; set; }
        // 20120903 Nick added
        public int AutIntNo { get; set; }
        public string ReportType { get; set; }
        public int CourtRollType { get; set; }

        ServiceDB db;
        string connStr;

        public PrintCourtRollFileName(string connStr)
        {
            this.connStr = connStr;
            db = new ServiceDB(connStr);
        }

        public PrintCourtRollFileName DepartName(int pfnIntNo)
        {
            PrintCourtRollFileName entity = null;

            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@PFNIntNo", pfnIntNo);

            object obj = db.ExecuteScalar("GetPrintFileName_WS", paras);
            if (obj == null) return entity;
            string printFileName = Convert.ToString(obj);

            entity = DepartName(printFileName);
            return entity;
        }
        public PrintCourtRollFileName DepartName(string printFileName)
        {
            PrintCourtRollFileName entity = null;
            if (!string.IsNullOrWhiteSpace(printFileName))
            {
                printFileName = printFileName.Trim();
                if (printFileName.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase))
                    printFileName = printFileName.Remove(printFileName.Length - 4, 4);
                if (printFileName.EndsWith("_", StringComparison.OrdinalIgnoreCase))
                    printFileName = printFileName.Remove(printFileName.Length - 1, 1);
                string[] info = printFileName.Split('_');
                if (info != null && (info.Length == 6 || info.Length == 7))
                {
                    entity = new PrintCourtRollFileName(this.connStr);
                    entity.Prefix = info[0];
                    entity.AutCode = info[1];
                    entity.CourtName = info[2];
                    entity.CourtRoomNo = info[3];
                    entity.CourtDate = info[4];
                    entity.PrintDate = info[5];
                    string[] printDate = info[5].Split('-', ' ', ':');
                    if (printDate != null && printDate.Length == 5)
                        entity.PrintDate = string.Format("{0}-{1}-{2} {3}:{4}:00", printDate[0], printDate[1], printDate[2], printDate[3], printDate[4]);

                    if (printDate != null && printDate.Length == 6)
                        entity.PrintDate = string.Format("{0}-{1}-{2} {3}:{4}:{5}", printDate[0], printDate[1], printDate[2], printDate[3], printDate[4], printDate[5]);
                   
                    if (info.Length == 7)
                        entity.Suffix = info[6];

                    SqlParameter[] paras = new SqlParameter[2];
                    paras[0] = new SqlParameter("@CrtName", entity.CourtName);
                    paras[1] = new SqlParameter("@CrtRoomNo", entity.CourtRoomNo);
                    DataSet ds = db.ExecuteDataSet("GetCrtIntNoCrtRIntNoByCrtNameCrtRoomNo_WS", paras);

                    if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0) return null;
                    DataRow dr = ds.Tables[0].Rows[0];
                    entity.CrtIntNo = ServiceDB.GetDataRowValue<int>(dr, "CrtIntNo");
                    entity.CrtRIntNo = ServiceDB.GetDataRowValue<int>(dr, "CrtRIntNo");
                }
            }
            return entity;
        }

        public string CombineName()
        {
            string[] info = new string[7];
            info[0] = this.Prefix;
            info[1] = this.AutCode;
            info[2] = this.CourtName;
            info[3] = this.CourtRoomNo;
            info[4] = this.CourtDate;
            info[5] = this.PrintDate.Replace(":", "-");
            info[6] = this.Suffix;
            string printFileName = string.Join("_", info);
            if (printFileName.EndsWith("_"))
                printFileName = printFileName.Remove(printFileName.Length - 1, 1);
            printFileName += ".pdf";
            return printFileName;
        }
    }

    
}
