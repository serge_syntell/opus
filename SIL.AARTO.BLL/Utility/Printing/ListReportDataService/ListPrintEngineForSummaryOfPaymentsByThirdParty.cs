﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ListPrintEngineForSummaryOfPaymentsByThirdParty : ListPrintEngineGroupSummary
    {
        int contempt;
        int contemptCancelled;
        int contemptCancelledTotal;
        int contemptCancelledValue;
        int contemptCancelledValueTotal;
        int contemptTotal;
        int contemptValue;
        int contemptValueTotal;
        int paid;
        int paidCancelled;
        int paidCancelledTotal;
        int paidCancelledValue;
        int paidCancelledValueTotal;
        int paidTotal;
        int paidValue;
        int paidValueTotal;

        protected override void OnInitParameters()
        {
            if (!RowHeight.HasValue)
                RowHeight = 20;
        }

        protected override bool IsGroupEnd(DataRow row, string group, DataRow nextRow, string nextGroup)
        {
            return group != nextGroup;
        }

        protected override bool RemoveCurrentSectionRow(DataRow row, int relativeIndex)
        {
            int num;
            //2013-10-22 updated by Nancy
            //row[DataColunms[7].DataFiled].ToString()-> row["AMOUNTPAID"].ToString()
            if (int.TryParse(row["AMOUNTPAID"].ToString(), out num) && num != 0)
            {
                this.paid++;
                this.paidValue += num;

                this.paidTotal++;
                this.paidValueTotal += num;
            }
            //2013-10-22 updated by Nancy
            //row[DataColunms[8].DataFiled].ToString()-> row["AMOUNTREFUNDED"].ToString()
            if (int.TryParse(row["AMOUNTREFUNDED"].ToString(), out num) && num != 0)
            {
                this.paidCancelled++;
                this.paidCancelledValue += num;

                this.paidCancelledTotal++;
                this.paidCancelledValueTotal += num;
            }
            //2013-10-22 updated by Nancy
            //row[DataColunms[7].DataFiled].ToString()-> row["AMOUNTPAID"].ToString()
            //2013-11-18 updated by Nancy 
            //row["CONTEMPT"].ToString() -> row["SUMCONTEMPT"].ToString()
            if (int.TryParse(row["SUMCONTEMPT"].ToString(), out num) && num != 0)
            {
                this.contempt++;
                this.contemptValue += num;

                this.contemptTotal++;
                this.contemptValueTotal += num;
            }

            if (int.TryParse(row["CancelledContempts"].ToString(), out num) && num != 0)
            {
                this.contemptCancelled++;
                this.contemptCancelledValue += num;

                this.contemptCancelledTotal++;
                this.contemptCancelledValueTotal += num;
            }

            return false;
        }

        protected override void BuildPrintRows(List<DataRow> section)
        {
            BuildSummary();

            if (IsEndOfReport)
            {
                BuildSummary(false);

                contemptCancelledTotal = contemptCancelledValueTotal = contemptTotal = contemptValueTotal
                    = paidCancelledTotal = paidCancelledValueTotal = paidTotal = paidValueTotal
                        = 0;

                LastRowIndex = 0;
            }
        }

        void BuildSummary(bool isGroupSummary = true)
        {
            int width0, width1, width2;
            width0 = width1 = width2 = 20;

            #region build end report summary

            if (!isGroupSummary)
                AddPrintObject(BuildRow());

            #endregion

            #region header

            AddPrintObject(BuildRow(
                BuildCell(0, width0, string.Format("        SUMMARY OF {0}", isGroupSummary ? "GROUP" : "REPORT")),
                BuildCell(1, width1),
                BuildCell(2, width2)));

            AddPrintObject(BuildRow(
                BuildCell(0, width0),
                BuildCell(1, width1, "TOTAL DOCUMENTS"),
                BuildCell(2, width2, "TOTAL RAND VALUE")));

            #endregion

            #region body

            AddPrintObject(BuildRow(
                BuildCell(0, width0, "PAID"),
                BuildCell(1, width1, (isGroupSummary ? this.paid : this.paidTotal).ToString(), StringAlignment.Far),
                BuildCell(2, width2, (isGroupSummary ? this.paidValue : this.paidValueTotal).ToString(), StringAlignment.Far)));

            AddPrintObject(BuildRow(
                BuildCell(0, width0, "CANCELLED"),
                BuildCell(1, width1, (isGroupSummary ? this.paidCancelled : this.paidCancelledTotal).ToString(), StringAlignment.Far),
                BuildCell(2, width2, (isGroupSummary ? this.paidCancelledValue : this.paidCancelledValueTotal).ToString(), StringAlignment.Far)));

            AddPrintObject(BuildRow(
                BuildCell(0, width0, "NET PAYMENTS"),
                BuildCell(1, width1, (isGroupSummary ? (this.paid - this.paidCancelled) : (this.paidTotal - this.paidCancelledTotal)).ToString(), StringAlignment.Far),
                BuildCell(2, width2, (isGroupSummary ? (this.paidValue + this.paidCancelledValue) : (this.paidValueTotal + this.paidCancelledValueTotal)).ToString(), StringAlignment.Far)));

            AddPrintObject(BuildRow());

            AddPrintObject(BuildRow(
                BuildCell(0, width0, "CONTEMPT"),
                BuildCell(1, width1, (isGroupSummary ? this.contempt : this.contemptTotal).ToString(), StringAlignment.Far),
                BuildCell(2, width2, (isGroupSummary ? this.contemptValue : this.contemptValueTotal).ToString(), StringAlignment.Far)));

            AddPrintObject(BuildRow(
                BuildCell(0, width0, "CANCELLED CONTEMPTS"),
                BuildCell(1, width1, (isGroupSummary ? this.contemptCancelled : this.contemptCancelledTotal).ToString(), StringAlignment.Far),
                BuildCell(2, width2, (isGroupSummary ? this.contemptCancelledValue : this.contemptCancelledValueTotal).ToString(), StringAlignment.Far)));

            AddPrintObject(BuildRow(
                BuildCell(0, width0, "NET CONTEMPT PAYMENTS"),
                BuildCell(1, width1, (isGroupSummary ? (this.contempt - this.contemptCancelled) : (this.contemptTotal - this.contemptCancelledTotal)).ToString(), StringAlignment.Far),
                BuildCell(2, width2, (isGroupSummary ? (this.contemptValue + this.contemptCancelledValue) : (this.contemptValueTotal + this.contemptCancelledValueTotal)).ToString(), StringAlignment.Far)));

            #endregion

            this.paid = 0;
            this.paidValue = 0;
            this.paidCancelled = 0;
            this.paidCancelledValue = 0;
            this.contempt = 0;
            this.contemptValue = 0;
            this.contemptCancelled = 0;
            this.contemptCancelledValue = 0;
        }
    }
}
