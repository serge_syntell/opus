﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing.ListReportDataService
{
    public class ListPrintEngineForReportOfficeErrors : ListPrintEngine
    {
        int lastIndex;
        string lastGroup;
        List<DataRow> section;

        protected override void OnReadingRow(int rowIndex)
        {
            if (DataSource == null
                || DataSource.Rows.Count <= 0
                )
                return;

            var row = DataSource.Rows[rowIndex];

            string group = null;
            if (PagingFields != null)
            {
                PagingFields.Cast<object>().ToList().ForEach(f => group += row[f.ToString()]);
            }

            if (rowIndex < DataSource.Rows.Count - 1)
            {//2013-07-04 add by Nancy(Add if.. conditions)
                var row2 = DataSource.Rows[rowIndex + 1];
                string group2 = null;
                if (PagingFields != null)
                {
                    PagingFields.Cast<object>().ToList().ForEach(f => group2 += row2[f.ToString()]);
                }

                if ((group != group2 || row2["AaOfErDescription"].ToString() == @"Report Summary:-\r\n")
                    && string.IsNullOrEmpty(row["NotTicketNo"].ToString())
                    && string.IsNullOrEmpty(row["Date"].ToString())
                    && string.IsNullOrEmpty(row["AaOfErDescription"].ToString())
                    )
                {
                    section = GetSection(rowIndex);
                    BuildRows();
                    lastIndex = rowIndex + 1;
                    lastGroup = group;

                    // Oscar 2013-04-11 added to reset parameter
                    if (row2["AaOfErDescription"].ToString() == @"Report Summary:-\r\n")
                        lastIndex = 0;
                }
            }
        }

        List<DataRow> GetSection(int rowIndex)
        {
            var rowList = new List<DataRow>();

            if (this.lastIndex < rowIndex
                && rowIndex < DataSource.Rows.Count - 1)
            {
                for (var i = this.lastIndex + 1; i < rowIndex; i++)
                {
                    rowList.Add(DataSource.Rows[i]);
                }
            }

            return rowList.Where(r => r["NotTicketNo"].ToString() != "Officer:"
                && !string.IsNullOrEmpty(r["NotTicketNo"].ToString())
                && !string.IsNullOrEmpty(r["Date"].ToString())
                && !string.IsNullOrEmpty(r["AaOfErDescription"].ToString())
                ).ToList();
        }

        void BuildRows()
        {
            #region header

            var tbRow = new TableRow();
            tbRow.cells.Add(BuildCell(0));
            tbRow.cells.Add(BuildCell(1));
            tbRow.cells.Add(BuildCell(2, "Group summary:-"));
            tbRow = BuildRow(tbRow);
            AddPrintObject(tbRow);

            #endregion

            #region body

            var grouping = section.GroupBy(r => r["AaOfErDescription"]);
            var sum = 0;
            foreach (var group in grouping)
            {
                var count = group.Count();
                sum += count;

                tbRow = new TableRow();
                tbRow.cells.Add(BuildCell(0));
                tbRow.cells.Add(BuildCell(1));
                tbRow.cells.Add(BuildCell(2, string.Format(" {0} {1}", count, group.Key)));

                tbRow = BuildRow(tbRow);
                AddPrintObject(tbRow);
            }

            #endregion

            #region footer

            tbRow = new TableRow();
            tbRow.cells.Add(BuildCell(0));
            tbRow.cells.Add(BuildCell(1));
            tbRow.cells.Add(BuildCell(2, string.Format(" {0} {1}", sum, "Total")));

            tbRow = BuildRow(tbRow);
            AddPrintObject(tbRow);

            #endregion
        }

        TableCell BuildCell(int cellIndex, string text = "")
        {
            var cellDef = DataColunms[cellIndex];
            var cell = new CellDefination
            {
                DataFiled = text,
                Width = cellDef.Width,
                Height = cellDef.Height,
                CellFont = cellDef.CellFont,
                PageWidth = cellDef.PageWidth,
                style = cellDef.style,
            };

            var tbCell = new TableCell
            {
                cellDef = cell,
                lines = cellDef.Lines
            };
            return tbCell;
        }

        TableRow BuildRow(TableRow row)
        {
            if (row == null) return null;
            var cellDef = row.cells[0];
            row.rowDef = new CellDefination
            {
                Height = cellDef.cellDef.Height
                ,
                PageWidth = cellDef.cellDef.PageWidth,
                Width = 100
            };
            return row;
        }
    }
}
