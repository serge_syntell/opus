using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Xml;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.BLL.Utility.ReportExtension;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Text.RegularExpressions;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public delegate void WriteErrorMessageHandler(string msg);
    public delegate void WriteLogHandler(int logType, object message, bool flush);

    public class ListPrintEngine : PrintEngine
    {
        public string _groupField;
        public string _groupName;
        public int _cellRows;
        public ArrayList PagingFields = new ArrayList();
        public List<CellDefination> GroupColunms = new List<CellDefination>();
        public CellDefination countFoot = new CellDefination();
        public CellDefination countFootPage = new CellDefination();
        public CellDefination FootPage = new CellDefination();//2013-02-25 added by Nancy for 4862
        public CellDefination Summary = new CellDefination();
        public string ConnectionString { get; set; }
        PrintEngine peServ = new PrintEngine();

        public string LastUser { get; set; } // 2013-07-23 add by Henry
        public string ReportCode { get; set; }//2015-01-20 Heidi added for generated prn file less a row data(woa register direct printing) (bontq1798)

        #region // Oscar 2013-04-01 added
        protected DataTable DataSource { get; set; }
        #endregion

        //public WriteErrorMessageHandler WriteErrorMessage { get; set; }

        //Added by Nancy 2013-11-19 start
        private string queryButtonText = "Get report data";
        /// <summary>
        /// Change the text of Button7 for direct printing report
        /// </summary>
        public string QueryButtonText
        {
            get { return queryButtonText; }
            set { queryButtonText = value; }
        }
        //Added by Nancy 2013-11-19 end
        //Added by Jacob 20131114 start
        private bool directPrintAfterLoadHistory = true;        
        /// <summary>
        /// change list report history printing behavior,
        /// if true, directly start printing preview after loading history data.
        /// </summary>
        public bool DirectPrintAfterLoadHistory
        {
            get { return directPrintAfterLoadHistory; }
            set { directPrintAfterLoadHistory = value; }
        }
        //Added by Jacob 20131114 end

        //added by Jacob 20121113 start
        //raise print text info.
        public event StartPrintHandler OnStartPrint;
        public event EndPrintHandler OnEndPrint;


        public event StartPageHandler OnStartPage;
        public event EndPageHandler OnEndPage;
        public event DrawStringHandler OnDrawString;
        public OnCreatedEvent onCreated;

        // Oscar 2013-02-26 added additional requests
        public ReportAdditionalRequest AdditionalRequest { get; set; }

        public bool IsPreview = false;//true:raise print info.
        public void RaiseOnDrawString(ReportTextPrintArg arg)//string text, Font printFont, RectangleF textArea)
        {
         
            if (OnDrawString != null)
                OnDrawString(arg);

        }
        public void RaiseOnDrawStringHeader(ReportTextPrintArg arg)//string text, Font printFont, RectangleF textArea)
        {
            if(arg!=null) arg.Portion=PrintPortionDef.Header;
            if (OnDrawString != null)
                OnDrawString(arg);

        }
        public void RaiseOnDrawStringHeaderGroup(ReportTextPrintArg arg)//string text, Font printFont, RectangleF textArea)
        {
            if (arg != null) arg.Portion = PrintPortionDef.HeaderGroup;
            if (OnDrawString != null)
                OnDrawString(arg);

        }
        public void RaiseOnDrawStringTableHeader(ReportTextPrintArg arg)//string text, Font printFont, RectangleF textArea)
        {
            if (arg != null) arg.Portion = PrintPortionDef.DataTableHeader;
            if (OnDrawString != null)
                OnDrawString(arg);

        }

        public void RaiseOnDrawStringTableRow(ReportTextPrintArg arg)//string text, Font printFont, RectangleF textArea)
        {
            if (arg != null) arg.Portion = PrintPortionDef.DataTableRow;
            if (OnDrawString != null)
                OnDrawString(arg);

        }
        public void RaiseOnStartPage()//string text, Font printFont, RectangleF textArea)
        {

            if (OnStartPage != null)
                OnStartPage();

        }
        public void RaiseOnEndPage( )//string text, Font printFont, RectangleF textArea)
        {
           
            if (OnEndPage != null)
                OnEndPage();

        }
        //added by Jacob 20121113 end
        public ListPrintEngine()
            : base()
        {
            PrintFont = ReportDefaultSetting.font;
        }

        public override String ReplaceTokens(String buf)
        {
            if (!string.IsNullOrEmpty(buf))
            {
                buf = buf.Replace("[pagenum]", _pageNum.ToString());
            }
            return buf;
        }

        public override void CreateEngine(int rcIntNo)
        {
            // Oscar 2013-04-02 added
            Logger(0, true, "Start creating template.");

            var printModel = CreateReportModel(rcIntNo);

            // Oscar 2013-04-02 added
            Logger(0, true, "Start creating data.");

            var dtData = CreatePrintData(printModel);

            // Oscar 2013-04-02 added
            Logger(0, true, "Start creating engine.");

            CreateMyEngine(printModel, dtData);
        }

        #region 20120830 disabled for backup
        //public override void CreateEngine(int rcIntNo)
        //{
        //    _groupField = "";
        //    _groupName = "";
        //    _cellRows = 0;
        //    GroupColunms = new List<CellDefination>();
        //    _printObjects = new ArrayList();
        //    Header = new PrintElement(null);
        //    DataTableHeader = new PrintElement(null);

        //    DataTable dtData;
        //    ReportModel printModel = GetPrintTemplate(rcIntNo);

        //    if (printModel.ReportCode == ((int)ReportConfigCodeList.CriminalCaseRegister).ToString())
        //        dtData = new ReportDataServiceForCriminalCaseRegister(connStr).GetPrintData();
        //    else
        //        dtData = GetPrintData(printModel);

        //    //Oscar 20120829 added save history
        //    var rdService = new ReportDataService();
        //    rdService.InitializeReportConfig(rcIntNo, ConnectionString);
        //    rdService.SaveReportDataToHistory(dtData);

        //    CreateMyEngine(printModel, dtData);

        //    //add modify summons.summonsBookedOut='P'
        //    if (printModel.ReportCode==(Convert.ToInt32(ReportConfigCodeList.SummonsBookedOutList)).ToString())
        //    {
        //        ModifySummonsBookedOutStatue(dtData);
        //    }
        //}
        #endregion

        #region CreateEngineProcess
        public virtual ReportModel CreateReportModel(int rcIntNo, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            ClearEngineData();

            var printModel = GetPrintTemplate(rcIntNo);
            if (dateFrom.HasValue && dateTo.HasValue)
            {
                printModel.DateFrom = dateFrom.Value;
                printModel.DateTo = dateTo.Value;
            }
            printModel.RcIntNo = rcIntNo;//added by Jacob 20120904--
            return printModel;
        }
        public void ClearEngineData()
        {
            _groupField = "";
            _groupName = "";
            _cellRows = 0;
            PagingFields = new ArrayList();
            GroupColunms = new List<CellDefination>();
            _printObjects = new ArrayList();
            Header = new PrintElement(null);
            DataTableHeader = new PrintElement(null);
            countFoot = new CellDefination();
            Summary = new CellDefination();//2013-03-07 added by Nancy for 4862
            HeaderGroup = new PrintElement(null);//Jacob
            FirstPageHeaderGroup2 = new PrintElement(null);
            FirstPageHeaderCols2 = new PrintElement(null);  
            BodyTail = new PrintElement(null);
            BodyTailTemplate = new PrintElement(null);
        }


        public virtual DataTable CreatePrintData(ReportModel printModel)
        {
            DataTable dtData = GetPrintData(printModel);
            return dtData;
        }


        public virtual DataTable CreateHistoryPrintData(ReportModel printModel)
        {
            return CreatePrintData(printModel);
        }

        #endregion
       public bool isFreeStyle;
        // Create test data preview for windows application 
        public void CreateEngineWithTestData(int rcIntNo)
        {
            //Jacob Replace with method.
            ClearEngineData();
            //_groupField = "";
            //_groupName = "";
            //_cellRows = 0;
            //GroupColunms = new List<CellDefination>();
            //_printObjects = new ArrayList();
            //Header = new PrintElement(null);
            //DataTableHeader = new PrintElement(null);
            //HeaderGroup = new PrintElement(null);//Jacob


            ReportModel printModel = GetPrintTemplate(rcIntNo);
            //DataTable dtData = GetTestPrintData(printModel);

          
            //try
            //{
            //    isFreeStyle = (new StackTrace()).GetFrame(1).GetMethod().ReflectedType.BaseType.BaseType.Name == (typeof(FreeStylePrintEngine)).Name;
            //}
            //catch
            //{
            //    isFreeStyle = false;
            //}

            DataTable dtData, dtNew;
            if (isFreeStyle)
            {
                dtData = GetTestPrintData(printModel);
                dtNew = dtData;
            }
            else
            {

                  //printModel = this.CreateReportModel
                  //  (printModel.RcIntNo, printModel.DateFrom, printModel.DateTo);
                dtData = GetPrintData(printModel);

                #region fill in test data

                dtNew = dtData.Clone();
                for (int i = 0; i < dtNew.Columns.Count; i++)
                {
                    if (dtNew.Columns[i].DataType != typeof(string))
                        dtNew.Columns[i].DataType = typeof(string);
                }
                for (int i = 0; i < 8; i++)
                {
                    var dr = dtNew.NewRow();
                    for (int j = 0; j < dtNew.Columns.Count; j++)
                    {
                        dr[j] = "**********";
                    }
                    dtNew.Rows.Add(dr);
                }

                #endregion

            }

            CreateTestEngine(printModel, dtNew);
        }

        public virtual void CreateMyEngine(ReportModel printModel, DataTable dtData)
        {
            printTemplate = printModel.xmlDoc;
            if (printTemplate == null)
            {
                if (WriteErrorMessage != null) WriteErrorMessage(string.Format(peServ.GetResource("ReportEngineService_Error_PrintTemplateNull"), printModel.ReportCode));
                return;
            }
            ClearEngineData();
            // Read template data
            ReadPrintTemplate(printModel);
            // Get Data
            ReadPrintData(dtData);

            var settings = GetDefaultPageSettings();

            DefaultPageSettings = settings; ExportPageSettings(DefaultPageSettings, PrintModel);//only test
            this.ReportCode = printModel.ReportCode;//2015-01-20 Heidi added for generated prn file less a row data(woa register direct printing) (bontq1798)
            //Edited by Jacob 20121116 start
            //add self defined print method,
            // 
            //Print();
            CustomPrint();
            //Edited by Jacob 20121116 end
            
        }
        /// <summary>
        /// add monitor object, send mail after print.
        /// </summary>
        protected virtual  void CustomPrint()
        {
            DrawStringMonitor m=new DrawStringMonitor(this);
            Print();
          

        }

        private PageSettings GetDefaultPageSettings()
        {
            PageSettings settings = new PageSettings();
            settings.PaperSize = new PaperSize(ReportDefaultSetting.PaperName,
                                               ReportDefaultSetting.PageWidth,
                                               ReportDefaultSetting.PageHeight);
            settings.Margins = new Margins(ReportDefaultSetting.BorderWidth,
                                           ReportDefaultSetting.BorderWidth,
                                           0,
                                           ReportDefaultSetting.BorderWidth);
            settings.Landscape = false;
            //settings.PrinterResolution=new PrinterResolution();
            return settings;
        }

        public void CreateTestEngine(ReportModel printModel, DataTable dtData)
        {
            printTemplate = printModel.xmlDoc;
            if (printTemplate == null)
            {
                if (WriteErrorMessage != null) WriteErrorMessage(string.Format(peServ.GetResource("ReportEngineService_Error_PrintTemplateNull"), printModel.ReportCode));
                return;
            }

            ReadPrintTemplate(printModel);
            ReadPrintData(dtData);

            var settings = GetDefaultPageSettings();

            DefaultPageSettings = settings;
            ExportPageSettings(DefaultPageSettings, printModel);//only test
            ShowPreview();
        }

        public virtual void CreatePreviewEngine(ReportModel printModel, DataTable dtData)
        {
            IsPreview = true;//added by Jacob 20121113
            printTemplate = printModel.xmlDoc;
            if (printTemplate == null)
            {
                if (WriteErrorMessage != null) WriteErrorMessage(string.Format(peServ.GetResource("ReportEngineService_Error_PrintTemplateNull"), printModel.ReportCode));
                return;
            }

            //Jacob Replace with method.
            ClearEngineData();
            //_groupField = "";
            //_groupName = "";
            //_cellRows = 0;
            //GroupColunms = new List<CellDefination>();
            //_printObjects = new ArrayList();
            //Header = new PrintElement(null);
            //DataTableHeader = new PrintElement(null);
            //HeaderGroup = new PrintElement(null);//Jacob
            // Read template data
            ReadPrintTemplate(printModel);
            // Get Data
            ReadPrintData(dtData);

            var settings = GetDefaultPageSettings();

            DefaultPageSettings = settings; ExportPageSettings(DefaultPageSettings, printModel);//only test
            ShowPreview();
           
        }

        protected void ReadPrintTemplate(ReportModel printModel)
        {
            this.PrintModel = printModel;
            string reportPageName = null;
            SIL.AARTO.DAL.Entities.Authority aut = new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(Convert.ToInt32(printModel.AutIntNo));

            // Get the format template
            if (printTemplate != null)
            {
                XmlNodeList pageHeadNodes = printTemplate.GetElementsByTagName("PageHeads");
                if (pageHeadNodes != null && pageHeadNodes.Count > 0)
                {
                    reportPageName = pageHeadNodes[0].ChildNodes[0].ChildNodes[0].InnerText;
                    this.ShowGridLine = false;
                    if (pageHeadNodes[0].ChildNodes[0].ChildNodes[1] != null)
                    {
                        this.ShowGridLine = Convert.ToBoolean(pageHeadNodes[0].ChildNodes[0].ChildNodes[1].InnerText);
                    }
                    if (pageHeadNodes[0].ChildNodes[0].ChildNodes[2] != null)
                    {
                        this.LineStyle = pageHeadNodes[0].ChildNodes[0].ChildNodes[2].InnerText.ToString();
                    }
                }

                // Read the paging section
                XmlNodeList PagingNodes = printTemplate.GetElementsByTagName("PagingBy");
                if (PagingNodes != null && PagingNodes[0] != null && PagingNodes[0].ChildNodes.Count > 0)
                {
                    foreach (XmlNode node in PagingNodes[0].ChildNodes)
                    {
                        PagingFields.Add(node.InnerText);
                    }
                }

                XmlNodeList headGroupNodes = printTemplate.GetElementsByTagName("HeadGroupColumns");
                if (headGroupNodes != null && headGroupNodes[0] != null && headGroupNodes[0].ChildNodes.Count > 0)
                {
                    string groupColumnName = null;
                    int colSpan = 0;
                    int width = 0;
                    int height = 0;
                    foreach (XmlNode node in headGroupNodes[0].ChildNodes)
                    {
                        groupColumnName = "";
                        _groupField = "";
                        colSpan = 0;
                        width = 0;
                        height = 0;
                        if (node.SelectSingleNode("ColumnText[1]") != null)
                        {
                            groupColumnName = node.SelectSingleNode("ColumnText[1]").InnerText;
                        }
                        if (node.SelectSingleNode("ColumnValue[1]") != null)
                        {
                            _groupField = node.SelectSingleNode("ColumnValue[1]").InnerText;
                            //groupColumnName = "[groupname]";
                        }
                        if (node.SelectSingleNode("ColumnCount[1]") != null)
                        {
                            colSpan = Convert.ToInt32(node.SelectSingleNode("ColumnCount[1]").InnerText);
                        }
                        if (node.SelectSingleNode("Width[1]") != null)
                        {
                            width = Convert.ToInt32(node.SelectSingleNode("Width[1]").InnerText);
                        }
                        if (node.SelectSingleNode("Height[1]") != null)
                        {
                            height = Convert.ToInt32(node.SelectSingleNode("Height[1]").InnerText);
                        }

                        CellDefination group = new CellDefination();
                        group.HeadGroupColumnText = groupColumnName;
                        group.DataFiled = _groupField;
                        group.ColumnSpan = colSpan;
                        group.Width = width;
                        group.Height = height;
                        GroupColunms.Add(group);
                    }
                }

                // 2012.11.14 Nick add
                XmlNodeList firstPageHeadGroupNodes2 = printTemplate.GetElementsByTagName("FirstPageHeadGroupColumns2");
                if (firstPageHeadGroupNodes2 != null && firstPageHeadGroupNodes2[0] != null && firstPageHeadGroupNodes2[0].ChildNodes.Count > 0)
                {
                    string groupColumnName = null;
                    string groupColumnField = null;
                    int width = 0;
                    int height = 0;
                    foreach (XmlNode node in firstPageHeadGroupNodes2[0].ChildNodes)
                    {
                        groupColumnName = "";
                        groupColumnField = "";
                        width = 0;
                        height = 0;
                        if (node.SelectSingleNode("ColumnText[1]") != null)
                        {
                            groupColumnName = node.SelectSingleNode("ColumnText[1]").InnerText;
                        }
                        if (node.SelectSingleNode("ColumnValue[1]") != null)
                        {
                            groupColumnField = node.SelectSingleNode("ColumnValue[1]").InnerText;
                        }
                        if (node.SelectSingleNode("Width[1]") != null)
                        {
                            width = Convert.ToInt32(node.SelectSingleNode("Width[1]").InnerText);
                        }
                        if (node.SelectSingleNode("Height[1]") != null)
                        {
                            height = Convert.ToInt32(node.SelectSingleNode("Height[1]").InnerText);
                        }

                        CellDefination colDef = new CellDefination();
                        colDef.HeadGroupColumnText = groupColumnName;
                        colDef.DataFiled = groupColumnName;
                        colDef.Width = width;
                        colDef.Height = height;

                        FirstPageHeaderGroup2._printPrimitives.Add(new PrintPrimitiveSpanColumnText(colDef));
                    }                    
                }

                // 2012.11.14 Nick add
                XmlNodeList firstPageHeadColNodes2 = printTemplate.GetElementsByTagName("FirstPageHeadColumns2");
                if (firstPageHeadColNodes2 != null && firstPageHeadColNodes2[0] != null && firstPageHeadColNodes2[0].ChildNodes.Count > 0)
                {
                    string columnName = null;
                    string columnField = null;
                    int width = 0;
                    int height = 0;
                    string valign = "";
                    string halign = "";
                    PrintPrimitiveRow row = new PrintPrimitiveRow();
                    List<PrintPrimitiveColumn> listCol = new List<PrintPrimitiveColumn>();
                    foreach (XmlNode node in firstPageHeadColNodes2[0].ChildNodes)
                    {
                        columnName = "";
                        columnField = "";
                        width = 0;
                        height = 0;
                        if (node.SelectSingleNode("ColumnText[1]") != null)
                        {
                            columnName = node.SelectSingleNode("ColumnText[1]").InnerText;
                        }
                        if (node.SelectSingleNode("DataField[1]") != null)
                        {
                            columnField = node.SelectSingleNode("DataField[1]").InnerText;
                        }
                        if (node.SelectSingleNode("Width[1]") != null)
                        {
                            width = Convert.ToInt32(node.SelectSingleNode("Width[1]").InnerText);
                        }
                        if (node.SelectSingleNode("Height[1]") != null)
                        {
                            height = Convert.ToInt32(node.SelectSingleNode("Height[1]").InnerText);
                        }
                        if (node.SelectSingleNode("Halign[1]") != null)
                        {
                            halign = node.SelectSingleNode("Halign[1]").InnerText.ToLower();
                        }
                        if (node.SelectSingleNode("Valign[1]") != null)
                        {
                            valign = node.SelectSingleNode("Valign[1]").InnerText.ToLower();
                        }
                        
                        CellDefination colDef = new CellDefination();
                        colDef.HeadGroupColumnText = columnName;
                        colDef.DataFiled = columnField;
                        colDef.Width = width;
                        colDef.Height = height;
                        colDef.PageWidth = ReportDefaultSetting.BodyWidth;
                        colDef.style = new StringFormat();
                        switch (valign)
                        {
                            case "bottom":
                                colDef.style.LineAlignment = StringAlignment.Far;
                                break;
                            case "middle":
                                colDef.style.LineAlignment = StringAlignment.Center;
                                break;
                            default:
                                colDef.style.LineAlignment = StringAlignment.Near;
                                break;
                        }
                        switch (halign)
                        {
                            case "right":
                                colDef.style.Alignment = StringAlignment.Far;
                                break;
                            case "center":
                                colDef.style.Alignment = StringAlignment.Center;
                                break;
                            default:
                                colDef.style.Alignment = StringAlignment.Near;
                                break;
                        }
                        PrintPrimitiveText col = new PrintPrimitiveText(colDef);
                        listCol.Add((PrintPrimitiveColumn)col);
                    }
                    row.rowDef = new CellDefination
                    {
                        Height = listCol[0].CurrentCellDef.Height,
                        Width = 100,
                        PageWidth = listCol[0].CurrentCellDef.PageWidth                        
                    };
                    row.texts = listCol;
                    FirstPageHeaderCols2._printPrimitives.Add(row);
                }

                XmlNodeList headNodes = printTemplate.GetElementsByTagName("HeadColumns");
                DataColunms = new List<CellDefination>();
                int groupColIdx = 0;
                int groupColWidth = 0;
                int groupSubColNum = 0;

                if (headNodes != null)
                {
                    _cellRows = 0;
                    foreach (XmlNode node in headNodes[0].ChildNodes)
                    {
                        int width = 0;
                        int height = 0;
                        int HeadHeight = 0;
                        int bold = 0;
                        int uppercase = 0;
                        float fontSize = PrintFont.Size;
                        string dataFormat = "";
                        string valign = "";
                        string halign = "";
                        List<CellLine> innerLines = new List<CellLine>();
                        foreach (XmlNode cnode in node.ChildNodes)
                        {
                            switch (cnode.Name.ToLower())
                            {
                                case "width":
                                    width = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "height":
                                    height = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "headheight":
                                    HeadHeight = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "fontsize":
                                    fontSize = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "fontbold":
                                    bold = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "uppercase":
                                    uppercase = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "dataformat":
                                    dataFormat = cnode.InnerText;
                                    break;



                            }
                        }

                        CellDefination col = new CellDefination();
                        col.HeadText = node.ChildNodes[0].InnerText.Replace("[Authority]", aut.AutNo + " " + aut.AutName);
                        col.DataFiled = node.ChildNodes[1].InnerText;
                        col.CellFont = new Font(PrintFont.FontFamily, fontSize, FontStyle.Regular);
                        col.Width = width;
                        col .IsUpperCase = uppercase == 1; // Jacob 20131105
                        col.Height = height;
                        col.HeadHeight = HeadHeight;
                        col.DataFormat = dataFormat;
                        col.PageWidth = ReportDefaultSetting.BodyWidth;
                        col.style = new StringFormat();//Jacob

                        //Jacob 20120918
                        //support column's v-align and h-align.
                        //
                        //added by Jacob 20120905 start for test
                        //please change to parsing from xml string. 
                        foreach (XmlNode cnode in node.ChildNodes)
                        {
                            //if (cnode.Name.ToLower()== "celllines")//added by jacob 20120905, lines in cell.
                            //{     innerLines = ParseCellLines(col, cnode.InnerText);//override to add special lines.
                            //       break;
                            //}
                            //Jacob
                            switch (cnode.Name.ToLower())
                            {
                                case "celllines":
                                    innerLines = ParseCellLines(col, cnode.InnerText);//override to add special lines.
                                    break;
                                case "valign"://Jacob
                                    valign = cnode.InnerText;
                                    break;
                                case "halign"://Jacob
                                    halign = cnode.InnerText;
                                    break;
                            }
                        }
                        switch (valign)//Jacob
                        {
                            case "bottom":
                                col.style.LineAlignment = StringAlignment.Far;
                                break;
                            case "top":
                                col.style.LineAlignment = StringAlignment.Near;
                                break;
                            default:
                                col.style.LineAlignment = StringAlignment.Center;
                                break;

                        }
                        switch (halign)//Jacob
                        {
                            case "right":
                                col.style.Alignment = StringAlignment.Far;
                                break;
                            case "center":
                                col.style.Alignment = StringAlignment.Center;
                                break;
                            default:
                                col.style.Alignment = StringAlignment.Near;
                                break;

                        }
                        col.Lines = innerLines;
                        //added by Jacob 20120905 end for test

                        DataColunms.Add(col);

                        string[] cellTexts = Regex.Split(node.ChildNodes[0].InnerText, "\\\\r\\\\n", RegexOptions.IgnoreCase);
                        if (cellTexts.Length > _cellRows)
                            _cellRows = cellTexts.Length;

                        groupSubColNum++;
                        groupColWidth += width;

                        // Add group header
                        if (GroupColunms.Count > 0 && groupColIdx < GroupColunms.Count && GroupColunms[groupColIdx].ColumnSpan > 0 && groupSubColNum == GroupColunms[groupColIdx].ColumnSpan)
                        {
                            groupSubColNum = 0;
                            GroupColunms[groupColIdx].Width = groupColWidth;
                            //DataTableHeader.AddSpanColumnText(GroupColunms[groupColIdx]);
                            groupColIdx++;
                            groupColWidth = 0;
                        }
                    }

                    if (GroupColunms.Count > 0)
                    {
                        //DataTableHeader.AddEmptyRow(); 
                        //DataTableHeader.AddEmptyRow();
                        // DataTableHeader.AddEmptyRow();
                    }

                    //edited by Jacob start
                    TableRow headRow = new TableRow();

                    headRow.cells = new List<TableCell>();
                    // Add column header
                    for (int n = 0; n < headNodes[0].ChildNodes.Count; n++)
                    {
                        TableCellHeader c = new TableCellHeader { cellDef = DataColunms[n] };

                        // c.cellDef.DataFiled = c.cellDef.HeadText;
                        headRow.cells.Add(c);
                        //DataTableHeader.AddHeaderText(DataColunms[n]);
                    }

                    headRow.rowDef = new CellDefination();
                    headRow.rowDef.Height = headRow.cells[0].cellDef.HeadHeight;    //2012.11.15 Nick changed 0 to headRow.cells[0].cellDef.HeadHeight   //0: cause to calculate height about content.
                    headRow.rowDef.PageWidth = headRow.cells[0].cellDef.PageWidth;


                    headRow.Print(DataTableHeader);
                    //edited by Jacob end
                    //DataTableHeader.AddPrimitive(new PrintPrimitiveRow(headRow));
                }
                // DataTableHeader.AddEmptyRow();

                XmlNodeList footNodes = printTemplate.GetElementsByTagName("Footer");
                if (footNodes != null && footNodes[0] != null && footNodes[0].ChildNodes.Count > 0 && footNodes[0].SelectSingleNode("Calculate[1]") != null)
                {
                    XmlNode TotalRow = footNodes[0].SelectSingleNode("Calculate[1]");
                    if (TotalRow.SelectSingleNode("Text[1]") != null)
                        countFoot.HeadText = TotalRow.SelectSingleNode("Text[1]").InnerText;
                    if (TotalRow.SelectSingleNode("DataField[1]") != null)
                        countFoot.DataFiled = TotalRow.SelectSingleNode("DataField[1]").InnerText;
                }

                //Jacob added 20120918
                //support bodytail element in xml config.
                //BodyTail
                //the inner elementes structure is the same as Footer.
                XmlNodeList bodyTailNodes = printTemplate.GetElementsByTagName("BodyTail");
                if (bodyTailNodes != null && bodyTailNodes[0] != null && bodyTailNodes[0].ChildNodes.Count > 0)
                {
                    XmlNode TotalRow = bodyTailNodes[0].SelectSingleNode("Calculate[1]");
                    if (TotalRow.SelectSingleNode("Text[1]") != null)
                    {
                        string tailText = TotalRow.SelectSingleNode("Text[1]").InnerText;
                        TableRow tailRow = new TableRow();
                        tailRow.cells.Add(new TableCell
                        {
                            cellDef = new CellDefination
                            {
                                DataFiled = tailText,
                                PageWidth = GetPageBounds().Width,
                                Width = GetPageBounds().Width
                            }

                        });
                        //  tailRow.Print(BodyTailTemplate);
                        BodyTailTemplate._printPrimitives.Add(new PrintPrimitiveSpanColumnText(
                                                                  new CellDefination
                                                                  {
                                                                      HeadGroupColumnText = tailText,
                                                                      PageWidth = GetPageBounds().Width,
                                                                      Width = GetPageBounds().Width

                                                                  }));
                        BodyTailHeight = PrintEngine.GetCellContentSizeWithReplace(tailText).Height;

                    }
                    if (TotalRow.SelectSingleNode("Height[1]") != null)
                    {
                        string tailText = TotalRow.SelectSingleNode("Height[1]").InnerText;
                        TableRow tailRow = new TableRow();

                        int height = 0;
                        if (int.TryParse(tailText, out height))
                            BodyTailHeight = height;


                    }

                    //countFoot.HeadText = TotalRow.SelectSingleNode("Text[1]").InnerText;

                }

                //2012-9-14 linda add for list report19
                if (footNodes != null && footNodes[0] != null && footNodes[0].ChildNodes.Count > 0 && footNodes[0].SelectSingleNode("Calculate[2]") != null)
                {
                    XmlNode TotalRow = footNodes[0].SelectSingleNode("Calculate[2]");
                    if (TotalRow.SelectSingleNode("Text[1]") != null)
                        countFootPage.HeadText = TotalRow.SelectSingleNode("Text[1]").InnerText;
                }
                //2013-03-07 added by Nancy for 4862 --start
                XmlNodeList summaries = printTemplate.GetElementsByTagName("Summary");
                if (summaries != null && summaries[0] != null && summaries[0].ChildNodes.Count > 0 && summaries[0].SelectSingleNode("Calculate[1]") != null)
                {
                    XmlNode TotalRow = summaries[0].SelectSingleNode("Calculate[1]");
                    if (TotalRow.SelectSingleNode("Text[1]") != null)
                        Summary.HeadText = TotalRow.SelectSingleNode("Text[1]").InnerText;
                    if (TotalRow.SelectSingleNode("DataField[1]") != null)
                        Summary.DataFiled = TotalRow.SelectSingleNode("DataField[1]").InnerText;
                }
                //2013-03-07 added by Nancy for 4862 --end
            }
            if (reportPageName != null)
            {
                CellDefination reportName = new CellDefination();
                //string replaceDate = reportPageName.Replace("[datenow]", DateTime.Now.ToString("dd/MM/yyyy"));  2013-03-26 removed by Nancy for date format 'yyyy/MM/dd'
                string replaceDate = reportPageName.Replace("[datenow]", DateTime.Now.ToString("yyyy/MM/dd")); //2013-03-26 added by Nancy for date format 'yyyy/MM/dd'

                SIL.AARTO.DAL.Entities.SysParam sysParam = new SIL.AARTO.DAL.Services.SysParamService().GetBySpColumnName("StartDate");
                System.TimeSpan ts = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")) - DateTime.Parse(sysParam.SpStringValue);
                string replaceRunNo = replaceDate.Replace("[RunNo]", ts.Days.ToString().PadLeft(4, '0'));


                reportName.HeadText = replaceRunNo.Replace("[Authority]", aut.AutNo + " " + aut.AutName);
                reportName.PageWidth = ReportDefaultSetting.PageWidth;
                reportName.Width = 100;// Jacob 20140527 change to percentage number// ReportDefaultSetting.BodyWidth;
                reportName.Height = (int)GetCellContentSize(reportName.HeadText.Replace("\\r\\n", "\n")).Height;
                TableRow headRow = new TableRow();

                headRow.cells = new List<TableCell>();

                PageHeaderCell c = new PageHeaderCell { cellDef = reportName };
                headRow.cells.Add(c);
                headRow.rowDef = new CellDefination();
                headRow.rowDef.Height = 0;
                headRow.rowDef.PageWidth = ReportDefaultSetting.BodyWidth;

                //headRow.AddTopLine(new Pen(Color.Green)); headRow.AddBottomLine(new Pen(Color.Green));
                headRow.Print(Header);
                PrintPrimitiveRow h = Header._printPrimitives[0] as PrintPrimitiveRow;
                //if(h!=null)
                //{
                //    h.AddBottomLine(new Pen(Color.Gold));
                //    h.AddTopLine(new Pen(Color.Green)); 
                //}
                //Header.AddHeaderText(reportName);
                // Header.AddEmptyRow();
            }
        }

        protected virtual List<CellLine> ParseCellLines(CellDefination col, string p)
        {
            List<CellLine> lines = new List<CellLine>();
            CellLine innerLineM1 = new CellLine
            {
                rx = 0,
                ry = col.Height / 4,
                tx = col.Width * col.PageWidth / 100,
                ty = col.Height / 4,
                CellPen = dashPen
            };
            CellLine innerLineM2 = new CellLine
            {
                rx = 0,
                ry = col.Height / 2,
                tx = col.Width * col.PageWidth / 100,
                ty = col.Height / 2,
                CellPen = dashPen
            }; CellLine innerLineM3 = new CellLine
            {
                rx = 0,
                ry = col.Height * 3 / 4,
                tx = col.Width * col.PageWidth / 100,
                ty = col.Height * 3 / 4,
                CellPen = dashPen
            };

            lines.Add(innerLineM1);
            lines.Add(innerLineM2);
            lines.Add(innerLineM3);
            return lines;
        }


        //added by Jacob 20120905 start
        protected Pen dashPen = new Pen(Color.Black);
        protected Pen solidPen = new Pen(Color.Black);
        //added by Jacob 20120905 end
        protected void ReadPrintData(DataTable dtData)
        {
            // Oscar 2013-04-01 added
            DataSource = dtData;

            _columnNum = DataColunms.Count;
            if (dtData == null) return;

            int sumColumnData = 0;
            bool isValidTotal = false;
            List<TableCell> rowCells = new List<TableCell>();
            string lastGroupName = null;
            string groupName = null;
            string strSummary = null;//2013-03-07 added by Nancy for 4862
            //added by jacob 20120821 start
            Rectangle pagesize = GetPageBounds();

            dashPen.DashStyle = DashStyle.Dash;
            solidPen.DashStyle = DashStyle.Solid;

            //added by jacob 20120821 end
            for (int n = 0; n < dtData.Rows.Count; n++)
            {
                groupName = null;
                DataRow dr = dtData.Rows[n];
                DataRow drPre = n > 0 ? dtData.Rows[n - 1] : null;
                rowCells = new List<TableCell>(); // added by jacob 20120821
                List<CellLine> lines = new List<CellLine>(); // added by jacob 20120821
                //if (!string.IsNullOrEmpty(_groupField))
                if (PagingFields.Count > 0)
                {
                    foreach (object pfiled in PagingFields)
                    {
                        groupName += dr[pfiled.ToString()].ToString();
                    }
                    if (lastGroupName != groupName)
                    {
                        if (lastGroupName != null && !string.IsNullOrEmpty(countFoot.HeadText))
                        {
                            List<TableCell> totalCells = new List<TableCell>();
                            CellDefination sumCellDef = new CellDefination();
                            sumCellDef.HeadGroupColumnText = countFoot.HeadText + (isValidTotal ? sumColumnData.ToString() : "");
                            //2012-9-14 linda add footer page
                            if (!string.IsNullOrEmpty(countFootPage.HeadText))
                            {
                                sumCellDef.HeadGroupColumnText += "   ";
                                sumCellDef.HeadGroupColumnText += countFootPage.HeadText;
                            }
                            
                            //if (!string.IsNullOrEmpty(FootPage.HeadText))
                            //{
                            //    sumCellDef.HeadGroupColumnText = countFoot.HeadText + "\r\n" + strFootPage;
                            //}
                            sumCellDef.ColumnSpan = _columnNum;

                            TableCell sumCell = new TableCell();
                            sumCell.cellDef = sumCellDef;

                            totalCells.Add(sumCell);
                            sumColumnData = 0;

                            GroupSumRow sumRow = new GroupSumRow();
                            sumRow.cells = totalCells; sumRow.rowDef = sumCellDef;
                            AddPrintObject(sumRow);
                        }
                        //2013-03-07 added by Nancy for 4862 --start
                        if (lastGroupName != null && !string.IsNullOrEmpty(Summary.HeadText))
                        {
                            List<TableCell> totalCells = new List<TableCell>();
                            CellDefination sumCellDef = new CellDefination();
                            sumCellDef.HeadGroupColumnText = Summary.HeadText + "\r\n" + strSummary;
                            sumCellDef.ColumnSpan = _columnNum;

                            TableCell sumCell = new TableCell();
                            sumCell.cellDef = sumCellDef;

                            totalCells.Add(sumCell);
                            sumColumnData = 0;

                            GroupSumRow sumRow = new GroupSumRow();
                            sumRow.cells = totalCells; sumRow.rowDef = sumCellDef;
                            AddPrintObject(sumRow);
                        }
                        //2013-03-07 added by Nancy for 4862 --end

                        lastGroupName = groupName;
                        AddNewGroupTag(dr);
                    }
                }
                else if (n == 0 && GroupColunms.Count > 0)
                {
                    AddNewGroupTag(dr);
                }
                int colIdx = 0;//added by jacob 20120821.
                foreach (CellDefination cellDef in DataColunms)
                {
                    colIdx += 1;//added by jacob 20120821.
                    TableCell dataCell = new TableCell();
                    CellDefination dataCellDef = new CellDefination();

                    string[] cellCols = Regex.Split(cellDef.DataFiled, "\\\\r\\\\n", RegexOptions.IgnoreCase);
                    string[] cellFormats = Regex.Split(cellDef.DataFormat, "\\\\r\\\\n", RegexOptions.IgnoreCase);
                    string cellText = "";
                    string cellData = "";
                    for (int c = 0; c < cellCols.Length; c++)
                    {
                        //edited by Nick 20120830 start---jacob.
                        //cellData = dr[cellCols[c]].ToString();
                        try
                        {
                            cellData = string.IsNullOrEmpty(cellCols[c]) ? null : dr[cellCols[c]].ToString();
                        }
                        catch
                        {
                            cellData = cellCols[c].ToString();
                        }
                        //edited by Nick 20120830 end---jacob.
                        if (!string.IsNullOrEmpty(cellData) && !string.IsNullOrEmpty(cellDef.DataFormat) && !string.IsNullOrEmpty(cellFormats[c]))
                        {
                            //DateTime dateData;
                            double doubleData;
                            //if (cellFormats[c].ToLower().Contains("dd") && DateTime.TryParse(cellData, out dateData))
                            //    cellText += Convert.ToDateTime(cellData).ToString(cellFormats[c]);
                            if (Double.TryParse(cellData, out doubleData))
                                cellText += doubleData.ToString(cellFormats[c]);
                            else
                                cellText += cellData;
                        }
                        else
                        {
                            cellText += cellData;
                        }
                        cellText += cellCols.Length > 1 ? "\r\n" : "";
                    }
                    dataCellDef.DataFiled = cellText;
                    dataCellDef.Width = cellDef.Width;
                    dataCellDef.Height = cellDef.Height;
                    dataCellDef.CellFont = cellDef.CellFont;
                    dataCellDef.PageWidth = cellDef.PageWidth;
                    dataCellDef.style = cellDef.style;//Jacob 20131105
                   
                       dataCellDef.IsUpperCase = cellDef.IsUpperCase;//Jacob. 20131105
                    dataCellDef.DataFormat = cellDef.DataFormat;//Jacob. 20131105
                    dataCell.lines = cellDef.Lines;//added by Jacob 20120905
                    dataCell.cellDef = dataCellDef;
                    rowCells.Add(dataCell);

                    //added by Jacob 20120905 start
                    AddLineToCell(DataColunms, colIdx, dataCell, cellDef, pagesize, n, dashPen, solidPen, drPre, dr);
                    //added by Jacob 20120905 end

                }

                //Jacob 20120905 start

                TableRow dataRow = new TableRow();


                dataRow.rowDef = new CellDefination
                {
                    Height = rowCells[0].cellDef.Height
                    ,
                    PageWidth = rowCells[0].cellDef.PageWidth,
                    Width = 100
                };

                dataRow.cells = rowCells;

                //AddRowLines(dr, drPre, dataRow);

                //Jacob 20120905 end


                AddPrintObject(dataRow);

                // Oscar 2013-04-01 added
                OnReadingRow(n);

                if (!string.IsNullOrEmpty(countFoot.HeadText) && !string.IsNullOrEmpty(countFoot.DataFiled))
                {
                    int countNumber = 0;
                    bool isNumber = int.TryParse(countFoot.DataFiled == "*" ? "1" : dr[countFoot.DataFiled].ToString(), out countNumber);
                    if (isNumber)
                    {
                        sumColumnData += countNumber;
                        isValidTotal = true;
                    }
                    else
                    {
                        isValidTotal = false;
                    }
                }
                //2013-03-07 added by Nancy for 4862 --start
                if (!string.IsNullOrEmpty(Summary.HeadText) && !string.IsNullOrEmpty(Summary.HeadText))
                {
                    strSummary = dr[Summary.DataFiled].ToString();
                }
                //2013-03-07 added by Nancy for 4862 --end
            }
            
            if (!string.IsNullOrEmpty(countFoot.HeadText))
            {
                rowCells = new List<TableCell>();
                CellDefination sumCellDef = new CellDefination();
                sumCellDef.HeadGroupColumnText = countFoot.HeadText + (isValidTotal ? sumColumnData.ToString() : "");
                //2012-9-14 linda add footer page
                if (!string.IsNullOrEmpty(countFootPage.HeadText))
                {
                    sumCellDef.HeadGroupColumnText += "   ";
                    sumCellDef.HeadGroupColumnText += countFootPage.HeadText;
                }
                sumCellDef.ColumnSpan = _columnNum;

                TableCell sumCell = new TableCell();
                sumCell.cellDef = sumCellDef;

                rowCells.Add(sumCell);

                GroupSumRow sumRow = new GroupSumRow();
                sumRow.cells = rowCells;
                sumRow.rowDef = sumCellDef;
                AddPrintObject(sumRow);
            }
            //2013-03-07 added by Nancy for 4862 --start
            if (!string.IsNullOrEmpty(Summary.HeadText))
            {
                rowCells = new List<TableCell>();
                CellDefination sumCellDef = new CellDefination();
                sumCellDef.HeadGroupColumnText = Summary.HeadText + "\r\n" + strSummary;
                sumCellDef.ColumnSpan = _columnNum;

                TableCell sumCell = new TableCell();
                sumCell.cellDef = sumCellDef;

                rowCells.Add(sumCell);

                GroupSumRow sumRow = new GroupSumRow();
                sumRow.cells = rowCells;
                sumRow.rowDef = sumCellDef;
                AddPrintObject(sumRow);
            }
            //2013-03-07 added by Nancy for 4862 --end
        }
        /// <summary>
        /// sample No.40.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="drPre"></param>
        /// <param name="dataRow"></param>
        /// <param name="NeedBottomLine"></param>
        public virtual void AddRowLines(DataRow dr, DataRow drPre, TableRow dataRow, bool NeedBottomLine = false)
        {
            //return;
            NeedBottomLine = true;

            //foreach(var cell in dataRow.cells)
            //{
            //    CellLine innerLineCell = new CellLine
            //    {
            //        rx = cell.cellDef.Width*cell.cellDef.PageWidth/100,
            //        ry = 0,
            //        tx = cell.cellDef.Width * cell.cellDef.PageWidth / 100,
            //        ty = cell.cellDef.Height,
            //        CellPen = dashPen
            //    };
            //    if (dataRow.cells.IndexOf(cell) == dataRow.cells.Count - 1) continue;

            //    //add cell lines
            //    cell.lines.Add(innerLineCell);
            //}
            //Jacob 20120905 start
            ////add lines according to data.
            if (drPre == null)
            {
                CellLine innerLine = new CellLine
                {
                    rx = 0,
                    ry = 0,
                    tx = dataRow.rowDef.PageWidth,
                    ty = 0,
                    CellPen = dashPen
                };
                dataRow.lines.Add(innerLine);
            }

            //if (drPre != null && drPre[0].ToString() == dr[0].ToString())
            //{
            //    innerLine.CellPen = dashPen;
            //    dataRow.lines.Add(innerLine);
            //}
            //else
            //{
            //    innerLine.CellPen = solidPen;
            //    CellLine innerLine2 = new CellLine
            //                              {
            //                                  rx = 0,
            //                                  ry = 2,
            //                                  tx = dataRow.rowDef.PageWidth,
            //                                  ty = 2
            //                              };
            //    dataRow.lines.Add(innerLine);
            //    dataRow.lines.Add(innerLine2);
            //}

            if (NeedBottomLine)
            {

                CellLine innerLineB2 = new CellLine
                {
                    rx = 0,
                    ry = dataRow.rowDef.Height,
                    tx = dataRow.rowDef.PageWidth,
                    ty = dataRow.rowDef.Height,
                    CellPen = dashPen
                };
                CellLine innerLineB1 = new CellLine
                {
                    rx = 0,
                    ry = dataRow.rowDef.Height - 2,
                    tx = dataRow.rowDef.PageWidth,
                    ty = dataRow.rowDef.Height - 2,
                    CellPen = dashPen
                };
                dataRow.lines.Add(innerLineB2);// dataRow.lines.Add(innerLineB2);
            }
        }

        private void AddLineToCell(List<CellDefination> dataColunms, int colIdx,
            TableCell dataCell, CellDefination cellDef, Rectangle pagesize,
            int n, Pen dashPen, Pen solidPen, DataRow drPre, DataRow dr)
        {

            //var cell = dataCell;
            //{
            //    CellLine innerLineCell = new CellLine
            //    {
            //        rx = cell.cellDef.Width * cell.cellDef.PageWidth / 100,
            //        ry = 0,
            //        tx = cell.cellDef.Width * cell.cellDef.PageWidth / 100,
            //        ty = cell.cellDef.Height,
            //        CellPen = dashPen
            //    };
            //    if (dataColunms.IndexOf(cellDef) < dataColunms.Count - 1)
            //    {
            //        //add right border cell lines
            //        cell.lines.Add(innerLineCell);
            //    }
            //}
            //return;
            //added by Jacob 20120904 start
            //top line
            //if (n == 0)
            //{
            //    CellLine innerLine = new CellLine
            //                             {
            //                                 rx = 0,
            //                                 ry = 0
            //                                 ,
            //                                 tx = cellDef.Width*pagesize.Width/100
            //                                 ,
            //                                 ty = 0
            //                                 ,
            //                                 CellPen =
            //                                     drPre != null && drPre[0].ToString() == dr[0].ToString() ? dashPen : solidPen
            //                             };
            //    dataCell.lines.Add(innerLine);
            //}
            //else
            //{
            //    CellLine innerLine = new CellLine
            //                             {
            //                                 rx = 0,
            //                                 ry = 0,
            //                                 tx = cellDef.Width*pagesize.Width/100,
            //                                 ty = 0
            //                             };

            //    if (drPre != null && drPre[0].ToString() == dr[0].ToString())
            //    {
            //        innerLine.CellPen = dashPen;
            //        dataCell.lines.Add(innerLine);
            //    }
            //    else
            //    {
            //        innerLine.CellPen = solidPen;
            //        CellLine innerLine2 = new CellLine
            //                                  {
            //                                      rx = 0,
            //                                      ry = 2,
            //                                      tx = cellDef.Width*pagesize.Width/100,
            //                                      ty = 2
            //                                  };
            //        dataCell.lines.Add(innerLine);
            //        dataCell.lines.Add(innerLine2);
            //    }
            //}


            //add right line , except the last column.
            //if (colIdx < DataColunms.Count) //
            //{
            //    // CellLine innerLineM4 = new CellLine { rx = 0, ry = 0, tx = 0, ty = cellDef.Height };
            //    //dataCell.lines.Add(innerLineM4);
            //    CellLine innerLineM5 = new CellLine
            //                               {
            //                                   CellPen=dashPen,
            //                                   rx = cellDef.Width*pagesize.Width/100,
            //                                   ry = 0,
            //                                   tx = cellDef.Width*pagesize.Width/100,
            //                                   ty = cellDef.Height
            //                                   //,   CellPen = drPre != null && drPre[0].ToString() == dr[0].ToString() ? dashPen : solidPen
            //                               };
            //    dataCell.lines.Add(innerLineM5);
            //}


        }


        // Oscar 2013-04-01 added
        protected virtual void OnReadingRow(int rowIndex)
        {

        }



        private void AddNewGroupTag(DataRow dr)
        {
            List<PrintPrimitiveSpanColumnText> cols = new List<PrintPrimitiveSpanColumnText>();
            CellDefination newCol = new CellDefination();
            foreach (CellDefination col in GroupColunms)
            {
                newCol = new CellDefination();
                newCol.HeadGroupColumnText = col.HeadGroupColumnText;
                newCol.Width = col.Width;
                newCol.Height = col.Height;
                if (!string.IsNullOrEmpty(col.DataFiled))
                {
                    newCol.HeadGroupColumnText = col.HeadGroupColumnText + dr[col.DataFiled].ToString();
                }
                cols.Add(new PrintPrimitiveSpanColumnText(newCol));
            }
            NewGroup newPage = new NewGroup();
            newPage.groupCols = cols;
            AddPrintObject(newPage);
        }


        protected DataTable GetTestPrintData(ReportModel model)
        {
            XmlNodeList headGroupNodes = model.xmlDoc.GetElementsByTagName("HeadGroupColumns");
            List<CellDefination> testGroupColunms = new List<CellDefination>();
            if (headGroupNodes != null && headGroupNodes[0] != null && headGroupNodes[0].ChildNodes.Count > 0)
            {
                //foreach (XmlNode node in headGroupNodes[0].ChildNodes)
                //{
                //    if (node.ChildNodes[0].Name.ToLower() == "columnvalue")
                //    {
                //        _groupField = node.ChildNodes[0].InnerText;
                //    }
                //}
                // 2012.11.26 Nick changed
                string groupColumnName = null;
                int colSpan = 0;
                int width = 0;
                int height = 0; 
                foreach (XmlNode node in headGroupNodes[0].ChildNodes)
                {
                    groupColumnName = "";
                    _groupField = "";
                    colSpan = 0;
                    width = 0;
                    height = 0;
                    if (node.SelectSingleNode("ColumnText[1]") != null)
                    {
                        groupColumnName = node.SelectSingleNode("ColumnText[1]").InnerText;
                    }
                    if (node.SelectSingleNode("ColumnValue[1]") != null)
                    {
                        _groupField = node.SelectSingleNode("ColumnValue[1]").InnerText;
                    }
                    if (node.SelectSingleNode("ColumnCount[1]") != null)
                    {
                        colSpan = Convert.ToInt32(node.SelectSingleNode("ColumnCount[1]").InnerText);
                    }
                    if (node.SelectSingleNode("Width[1]") != null)
                    {
                        width = Convert.ToInt32(node.SelectSingleNode("Width[1]").InnerText);
                    }
                    if (node.SelectSingleNode("Height[1]") != null)
                    {
                        height = Convert.ToInt32(node.SelectSingleNode("Height[1]").InnerText);
                    }

                    CellDefination group = new CellDefination();
                    group.HeadGroupColumnText = groupColumnName;
                    group.DataFiled = _groupField;
                    group.ColumnSpan = colSpan;
                    group.Width = width;
                    group.Height = height;
                    testGroupColunms.Add(group);
                }
                _groupField = "";
            }

            XmlNodeList headNodes = model.xmlDoc.GetElementsByTagName("HeadColumns");
            List<CellDefination> DataFields = new List<CellDefination>();
            if (headNodes != null)
            {
                foreach (XmlNode node in headNodes[0].ChildNodes)
                {
                    int width = 0;
                    int height = 0;
                    int bold = 0;
                    int uppercase = 0;
                    float fontSize = PrintFont.Size;
                    string dataFormat = "";
                    foreach (XmlNode cnode in node.ChildNodes)
                    {
                        switch (cnode.Name.ToLower())
                        {
                            case "width":
                                width = Convert.ToInt32(cnode.InnerText);
                                break;
                            case "height":
                                height = Convert.ToInt32(cnode.InnerText);
                                break;
                            case "fontsize":
                                fontSize = Convert.ToInt32(cnode.InnerText);
                                break;
                            case "fontbold":
                                bold = Convert.ToInt32(cnode.InnerText);
                                break;
                            case "uppercase":
                                uppercase = Convert.ToInt32(cnode.InnerText);
                                break;
                            case "dataformat":
                                dataFormat = cnode.InnerText;
                                break;
                        }
                    }
                    string[] cellCols = Regex.Split(node.ChildNodes[1].InnerText, "\\\\r\\\\n", RegexOptions.IgnoreCase);
                    for (int c = 0; c < cellCols.Length; c++)
                    {
                        CellDefination col = new CellDefination();
                        col.DataFiled = cellCols[c];
                        col.Width = width;
                        DataFields.Add(col);
                    }
                }
            }

            DataTable dtTest = new DataTable("TestData"); ;
            DataRow dr = null;

            foreach (CellDefination cellDef in DataFields)
            {
                dtTest.Columns.Add(cellDef.DataFiled, System.Type.GetType("System.String"));
            }
            if (testGroupColunms.Count > 0)
            {
                foreach (CellDefination cellDef in testGroupColunms)
                {
                    if (string.IsNullOrEmpty(cellDef.DataFiled)) 
                        continue;
                    if (DataFields.Find(delegate(CellDefination c) { return c.DataFiled == cellDef.DataFiled; }) == null)
                        dtTest.Columns.Add(cellDef.DataFiled, System.Type.GetType("System.String"));
                }
            }

            for (int n = 0; n < 8; n++)
            {
                dr = dtTest.NewRow();
                foreach (CellDefination cellDef in testGroupColunms)
                {
                    if (!string.IsNullOrEmpty(cellDef.DataFiled))
                        dr[cellDef.DataFiled] = "**";
                }
                foreach (CellDefination cellDef in DataFields)
                {
                    string[] cellCols = Regex.Split(cellDef.DataFiled, "\\\\r\\\\n", RegexOptions.IgnoreCase);
                    string cellText = "";
                    for (int c = 0; c < cellCols.Length; c++)
                    {
                        cellText += "".PadRight(Convert.ToInt32(cellDef.Width * 0.8), '*') + (cellCols.Length > 1 ? "\r\n" : "");
                    }
                    dr[cellDef.DataFiled] = cellText;
                }
                dtTest.Rows.Add(dr);
            }

            return dtTest;
        }


        protected override Rectangle GetPageBounds()
        {
           
            int fitPageWidth = ReportDefaultSetting.BodyWidth;
            //2015-01-20 Heidi added for generated prn file less a row data(woa register direct printing) (bontq1798)
            if (this.ReportCode == ((int)ReportConfigCodeList.WOARegisterDirectPrinting).ToString())
            { 
                return new Rectangle(ReportDefaultSetting.BorderLeft,
                    ReportDefaultSetting.BorderTop, fitPageWidth, 1000);
            }
            else
            {
                return new Rectangle(ReportDefaultSetting.BorderLeft,
                    ReportDefaultSetting.BorderTop, fitPageWidth, ReportDefaultSetting.BodyHeight);
            }
        }


        protected override ReportModel GetPrintTemplate(int rcIntNo)
        {
            ReportConfig Rc = SIL.AARTO.BLL.Report.ReportManager.GetReportById(rcIntNo);
            ReportModel model = null;

            if (Rc.AutomaticGenerateQuery != null && Rc.AutomaticGenerateQuery.Length > 0)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    memoryStream.Write(Rc.AutomaticGenerateQuery, 0, Rc.AutomaticGenerateQuery.Length);
                    memoryStream.Position = 0;
                    model = formatter.Deserialize(memoryStream) as ReportModel;
                }
            }
            else
            {
                model = new ReportModel();
                model.StoredProcName = Rc.RspIntNoSource.ReportStoredProcName;
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Rc.Template);

            model.RcIntNo = rcIntNo;
            model.xmlDoc = xmlDoc;
            model.AutIntNo = Rc.AutIntNo.ToString();
            model.ReportName = Rc.ReportName;
            model.ReportCode = Rc.ReportCode;

            if (Rc.AutomaticGenerate && !Rc.IsCustom  && !Rc.IsFreeStyle)
            {
                switch (Rc.AutomaticGenerateIntervalType.Trim().ToLower())
                {
                    //2013-11-15 Updated by Nancy start
                    //update the value of 'DateTo' 
                    case "hours":
                        model.DateFrom = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + DateTime.Now.AddHours(-Convert.ToDouble(Rc.AutomaticGenerateInterval)).Hour + ":00");
                        model.DateTo = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + DateTime.Now.Hour + ":00");
                        break;
                    case "days":
                        model.DateFrom = Convert.ToDateTime(DateTime.Now.AddDays(-Convert.ToDouble(Rc.AutomaticGenerateInterval)).ToShortDateString() + " 00:00");
                        model.DateTo = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00");
                        break;
                    case "months":
                        model.DateFrom = Convert.ToDateTime(DateTime.Now.AddDays(1 - DateTime.Now.Day).AddMonths(-Convert.ToInt32(Rc.AutomaticGenerateInterval)).ToShortDateString() + " 00:00");
                        model.DateTo = Convert.ToDateTime(DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToShortDateString() + " 00:00");
                        break;
                    case "daily":
                        model.DateFrom = Convert.ToDateTime(DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00");
                        model.DateTo = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 00:00");
                        break;
                    case "weekly":
                        model.DateFrom = Convert.ToDateTime(DateTime.Now.AddDays(1 - Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d")) - 7).ToShortDateString() + " 00:00");
                        model.DateTo = Convert.ToDateTime(model.DateFrom.AddDays(7).ToShortDateString() + " 00:00");
                        break;
                    case "monthly":
                        model.DateFrom = Convert.ToDateTime(DateTime.Now.AddDays(1 - DateTime.Now.Day).AddMonths(-1).ToShortDateString() + " 00:00");
                        model.DateTo = Convert.ToDateTime(DateTime.Now.AddDays(1 - DateTime.Now.Day).ToShortDateString() + " 00:00");
                        break;
                    //2013-11-15 Updated by Nancy end
                }
            }
            else
            {
                model.DateFrom = Convert.ToDateTime(DateTime.Now.AddDays(-1).ToShortDateString() + " 00:00");
                model.DateTo = Convert.ToDateTime(DateTime.Now.AddDays(-1).ToShortDateString() + " 23:59");
            }

            return model;
        }

        protected override DataTable GetPrintData(ReportModel model)
        {
            DataSet ds = new DataSet();

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(connStr);
            SqlCommand myCommand = new SqlCommand("REPORT_ExportReport", myConnection);
            myCommand.CommandTimeout = 0;//2013-11-14 added by Nancy

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterSpName = new SqlParameter("@SpName", SqlDbType.VarChar, 100);
            parameterSpName.Value = model.StoredProcName;
            myCommand.Parameters.Add(parameterSpName);

            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.VarChar, 10);
            parameterAutIntNo.Value = model.AutIntNo;
            myCommand.Parameters.Add(parameterAutIntNo);

            // Oscar 2013-02-26 added additional requests
            if (AdditionalRequest != null)
            {
                model.DateFrom = AdditionalRequest.DateFrom;
                model.DateTo = AdditionalRequest.DateTo;
            }

            if (model.DateFrom == DateTime.MinValue || model.DateTo == DateTime.MinValue)
            {
                model.DateFrom = new DateTime(1900, 1, 1);
                model.DateTo = new DateTime(2079, 6, 6);
            }

            SqlParameter parameterDateFrom = new SqlParameter("@DateFrom", SqlDbType.VarChar, 10);
            parameterDateFrom.Value = model.DateFrom.ToString("yyyyMMdd");
            myCommand.Parameters.Add(parameterDateFrom);

            SqlParameter parameterDateTo = new SqlParameter("@DateTo", SqlDbType.VarChar, 10);
            parameterDateTo.Value = model.DateTo.ToString("yyyyMMdd");//2013-11-15 updated by Nancy '.AddDays(1)'
            myCommand.Parameters.Add(parameterDateTo);

            SqlParameter parameterCourts = new SqlParameter("@Courts", SqlDbType.VarChar, 500);

            String Courts = "";
            if (model.SelectedCourtsList != null)
            {
                foreach (Court court in model.SelectedCourtsList)
                {
                    Courts += court.CrtIntNo.ToString() + ",";
                }
                if (Courts.Length > 0)
                    Courts = Courts.Substring(0, Courts.Length - 1);
            }

            parameterCourts.Value = Courts;
            myCommand.Parameters.Add(parameterCourts);

            if (model.NoticeType == null)
                model.NoticeType = "";
            SqlParameter parameterNoticeType = new SqlParameter("@NoticeType", SqlDbType.VarChar, 10);
            parameterNoticeType.Value = model.NoticeType;
            myCommand.Parameters.Add(parameterNoticeType);

            if (model.SortField == null)
                model.SortField = "";
            SqlParameter parameterSortField = new SqlParameter("@SortField", SqlDbType.VarChar, 50);
            parameterSortField.Value = model.SortField;
            myCommand.Parameters.Add(parameterSortField);

            if (model.Sort == null)
                model.Sort = "";
            SqlParameter parameterSort = new SqlParameter("@Sort", SqlDbType.VarChar, 10);
            parameterSort.Value = model.Sort;
            myCommand.Parameters.Add(parameterSort);

            SqlDataAdapter sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                if (WriteErrorMessage != null) WriteErrorMessage(string.Format(peServ.GetResource("ReportEngineService_Error_LoadPrintData"), ex.Message, model.ReportCode));
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            if (ds == null || ds.Tables.Count == 0)
            {
                if (WriteErrorMessage != null) WriteErrorMessage(string.Format(peServ.GetResource("ReportEngineService_Error_PrintDataNull"), model.ReportCode));
                return null;
            }
            return ds.Tables[0];
        }

        PrintElement HeaderGroup = new PrintElement(null);//Jacob 
        PrintElement FirstPageHeaderGroup2 = new PrintElement(null);  // 2012.11.13 Nick add
        PrintElement FirstPageHeaderCols2 = new PrintElement(null);
        PrintElement BodyTail = new PrintElement(null);//Jacob
        protected PrintElement BodyTailTemplate = new PrintElement(null);//Jacob
        PrintElement PageFooter = new PrintElement(null);//Jacob
        //Jacob added 20120918
        //indicate bodyTail's height.
        /// <summary>
        /// Indicate bodyTail part's height.
        /// </summary>
        protected virtual float BodyTailHeight { get; set; }

        //Jacob
        private float PageFooterHeight
        {
            get { return PageFooter.CalculateHeight(null, null); }//remove parameter later.

        }
        protected override void MyPrintPage(PrintPageEventArgs e)
        {
            //added by Jacob 20121113
            //new page, raise start print event.
            //
            if(OnStartPrint!=null) OnStartPrint(null);

            if (OnStartPage != null) OnStartPage();
            _pageNum++;

            Rectangle printBounds = GetPageBounds();
            
            printBounds.Y = 10;
            _columnWidth = Convert.ToInt32(printBounds.Width / _columnNum);
            PrintElement newGroupPage = null;


            string objectType_NewGroupPage = null;

            if (_printElements.Count > 0)
            {
                List<PrintPrimitiveSpanColumnText> headerGroupCols = new List<PrintPrimitiveSpanColumnText>();
                newGroupPage = (PrintElement)_printElements[_printIndex];
                objectType_NewGroupPage = newGroupPage._printObject.GetType().Name;
                if (objectType_NewGroupPage == "NewGroup")
                {

                    _pageNum = 1;
                    //_groupName = ((NewGroup)elementNewGroupPage._printObject).textGroup;
                    headerGroupCols = ((NewGroup)newGroupPage._printObject).groupCols;
                    if ((_printIndex == 0 || PagingFields.Count > 0) && headerGroupCols.Count > 0)
                    {
                        // if (_printIndex > 0)
                        //  DataTableHeader._printPrimitives.RemoveRange(0, headerGroupCols.Count);
                        // DataTableHeader._printPrimitives.InsertRange(0, headerGroupCols);
                        //if (_printIndex > 0)
                        //Oscar 20121107 changed for head group overflow
                        if (HeaderGroup._printPrimitives.Count >= headerGroupCols.Count)
                            HeaderGroup._printPrimitives.RemoveRange(0, headerGroupCols.Count);
                        HeaderGroup._printPrimitives.InsertRange(0, headerGroupCols);


                    }

                    _printIndex++;
                }
            }

            float headerHeight = Header.CalculateHeight(this, e.Graphics);
            //added by Jacob 20121113 start
            // raise print text info for Header.
            foreach (var p in Header._printPrimitives)
            {
                PrintPrimitiveRow t = p as PrintPrimitiveRow;
                if (t != null)
                {
                    foreach (var ht in t.texts)
                    {
                        var sct = ht as PrintPrimitiveHeaderTextWithoutBorder;
                        if (sct != null)
                        {
                            sct.OnDrawString -= new Printing.DrawStringHandler(this.RaiseOnDrawStringHeader);
                            sct.OnDrawString += new Printing.DrawStringHandler(this.RaiseOnDrawStringHeader);
                        }
                    }
                }
            }
            //added by Jacob 20121113 end
            Header.Draw(this, printBounds.Top, e.Graphics, printBounds);

            int Ytemp = 100;
            
            float EmptyRowHeight = (new PrintPrimitiveEmptyRow()).CalculateHeight(this, e.Graphics);
            float tblHeaderHeight = DataTableHeader.CalculateTblRowHeight(this, e.Graphics);// +EmptyRowHeight * (_cellRows - 1);
            float headerCols2Height = 0;
            float groupHeight = HeaderGroup.CalculateTblRowHeight(this, e.Graphics);   //Jacob
            float group2Height = _pageNum == 1 ? FirstPageHeaderGroup2.CalculateHeight(this, e.Graphics) : 0; // Nick
            //Jaocb removed templately.
            //float tblHeaderHeight = DataTableHeader.CalculateTblRowHeight(this, e.Graphics);
            // Nick 20120911 deleted
            //float EmptyRowHeight = (new PrintPrimitiveEmptyRow()).CalculateHeight(this, e.Graphics);
            //float tblHeaderHeight = DataTableHeader.CalculateTblRowHeight(this, e.Graphics) + EmptyRowHeight * (_cellRows - 1);
            if (_pageNum == 1)
            {
                AddLineToDataTableHeader(DataTableHeader);
            }

            //added by Jacob 20121113 start
            // raise print text info.

            //rasie event from headergroup
            foreach (var p in HeaderGroup._printPrimitives)
            {
                PrintPrimitiveSpanColumnText t = p as PrintPrimitiveSpanColumnText;
                if (t != null)
                {
                    t.OnDrawString -= new Printing.DrawStringHandler(this.RaiseOnDrawStringHeaderGroup);
                    t.OnDrawString += new Printing.DrawStringHandler(this.RaiseOnDrawStringHeaderGroup);
                }
            }
            foreach (var p in DataTableHeader._printPrimitives)
            {
                PrintPrimitiveRow t = p as PrintPrimitiveRow;
                if (t != null)
                {
                    foreach (var ht in t.texts)
                    {
                        var sct = ht as PrintPrimitiveHeaderText;
                        if (sct != null)
                        {
                            sct.OnDrawString -= new Printing.DrawStringHandler(this.RaiseOnDrawStringTableHeader);
                            sct.OnDrawString += new Printing.DrawStringHandler(this.RaiseOnDrawStringTableHeader);
                        }
                    }
                }
            }

            foreach (var pe in this._printElements)
            {
                PrintElement pee = pe as PrintElement;
                if (pee != null)
                {
                    foreach (var p in pee._printPrimitives)
                    {
                        PrintPrimitiveRow t = p as PrintPrimitiveRow;
                        if (t != null)
                        {
                            foreach (var ht in t.texts)
                            {
                                var sct = ht as PrintPrimitiveColumn;
                                if (sct != null)
                                {
                                    sct.OnDrawString -= new Printing.DrawStringHandler(this.RaiseOnDrawStringTableRow);
                                    sct.OnDrawString += new Printing.DrawStringHandler(this.RaiseOnDrawStringTableRow);
                                }
                            }
                        }
                    }
                }



            }
            //added by Jacob 20121113 end.


            HeaderGroup.Draw(this, printBounds.Top + headerHeight, e.Graphics, printBounds);
            float padding = 10;
            if (_pageNum == 1 && group2Height > 0)
            {
                FirstPageHeaderGroup2.Draw(this, printBounds.Top + headerHeight + groupHeight, e.Graphics, printBounds);
            }

            DataTableHeader.Draw(this, printBounds.Top + headerHeight + groupHeight + group2Height + padding, e.Graphics, printBounds);

            if (_pageNum == 1 && FirstPageHeaderCols2 != null && FirstPageHeaderCols2._printPrimitives.Count > 0)
            {
                FirstPageHeaderCols2 = GetHeaderCols2Data(FirstPageHeaderCols2);
                headerCols2Height = _pageNum == 1 ? FirstPageHeaderCols2.CalculateHeight(this, e.Graphics) : 0;
                FirstPageHeaderCols2.Draw(this, printBounds.Top + headerHeight + groupHeight + group2Height + tblHeaderHeight + padding, e.Graphics, printBounds);
            }
            Rectangle pageBounds = new Rectangle(printBounds.Left,
                                                 (int)(printBounds.Top + headerHeight + groupHeight + group2Height + tblHeaderHeight + headerCols2Height), printBounds.Width,
                //Jacob add BodyTail height.
                //(int)(printBounds.Height - headerHeight - tblHeaderHeight - groupHeight ));
                                                 (int)(printBounds.Height - headerHeight - groupHeight - group2Height - tblHeaderHeight - headerCols2Height - BodyTailHeight));

            float yPos = printBounds.Top + headerHeight + groupHeight + group2Height + tblHeaderHeight + headerCols2Height + padding;
            Ytemp = (int)(printBounds.Top + headerHeight + tblHeaderHeight + groupHeight);
            //e.Graphics.DrawLine(solidPen, printBounds.Left, (int)(printBounds.Top + headerHeight + tblHeaderHeight),
            //    printBounds.Width, (int)(printBounds.Top + headerHeight + tblHeaderHeight));

            bool morePages = false;
            int elementsOnPage = 0;
            while (_printIndex < _printElements.Count && _printElements.Count > 0)
            {
                PrintElement element = (PrintElement)_printElements[_printIndex];
                objectType_NewGroupPage = element._printObject.GetType().Name;
                if (objectType_NewGroupPage == "NewGroup")
                {
                    _pageNum = 0;
                    //_groupName = ((NewGroup)element._printObject).textGroup;
                    //if (((NewGroup)element._printObject).groupCols.Count > 0)
                    //DataTableHeader._printPrimitives.InsertRange(0, ((NewGroup)element._printObject).groupCols);
                    //_printIndex++;
                    PrintBodyTail(BodyTail, e, yPos, pageBounds);
                    RaiseOnEndPage();
                    e.HasMorePages = true;
                    return;
                }

                float height = element.CalculateTblRowHeight(this, e.Graphics);
                //float height = element.CalculateTblRowHeight(this, e.Graphics) * _cellRows;

                // will it fit on the page? 
                //Jacob edited, add body tail.
                //if (yPos + height > pageBounds.Bottom)
                if (yPos + height + BodyTailHeight > pageBounds.Bottom)
                {
                    // we don't want to do this if we're the first thing on the page
                    if (elementsOnPage != 0)
                    {

                        element.FirstRowOfPage = true;//Jacob
                        morePages = true;
                        break;
                    }
                }

                AddLinesInMyPrintPage(e, yPos, pageBounds, element);
                element.Draw(this, yPos, e.Graphics, pageBounds);
                AfterPrintElement(element);

                // Nick 2012.09.19 add for store the page index for next index report
                StorePageIndex(_pageNum);

                // move the ypos
                yPos += height;

                // next
                _printIndex++;
                elementsOnPage++;
            }


            PrintBodyTail(BodyTail, e, yPos, pageBounds);
            RaiseOnEndPage();
            e.HasMorePages = morePages;
            //added by Jacob 20121113
            //new page, raise start print event.
            //
            if (!morePages && OnEndPrint != null) OnEndPrint(null);
            if (!morePages) IsPreview = false;//added by Jacob 20121113

        }

        protected virtual PrintElement GetHeaderCols2Data(PrintElement headerCols)
        {
            return headerCols;
        }

        /// <summary>
        /// called before bodyTail printing.
        /// </summary>
        /// <param name="BodyTail"></param>
        protected virtual void BeforePrintBodyTail(PrintElement BodyTail)
        {
            BodyTail._printPrimitives.Clear();

            BodyTail._printPrimitives.AddRange(BodyTailTemplate._printPrimitives);
        }

        protected virtual void AfterPrintBodyTail(PrintElement BodyTail)
        {
        }
        /// <summary>
        /// called after row element printed.
        /// </summary>
        /// <param name="element"></param>
        protected virtual void AfterPrintElement(PrintElement element)
        {

        }

        protected virtual void PrintBodyTail(PrintElement BodyTail, PrintPageEventArgs e, float yPos, Rectangle pageBounds)
        {
            BeforePrintBodyTail(BodyTail);

            BodyTail.Draw(this, yPos, e.Graphics, pageBounds);

            AfterPrintBodyTail(BodyTail);

            RaiseOnPageEnd();
        }

        private void RaiseOnPageEnd()
        {
             
        }

        // Nick 2012.09.19 add for store the page index for next index report
        protected virtual void StorePageIndex(int pageNum)
        {
            return;
        }

        /// <summary>
        /// Jacob add lines
        /// </summary>
        /// <param name="DataTableHeader"></param>
        protected virtual void AddLineToDataTableHeader(PrintElement DataTableHeader)
        {
            return;
        }

        protected virtual void AddLinesInMyPrintPage(PrintPageEventArgs e, float yPos, Rectangle pageBounds, PrintElement element)
        {
            return;
        }
        /// <summary>
        /// add special export button to export data and send emal.
        /// </summary>
        /// <remarks>
        /// added by Jacob 20121114.
        /// </remarks>
        public override void ShowPreview()
        {
            PrintPreviewDialogEx dialog = null;
             if (Convert.ToInt32(this.PrintModel.ReportCode) == Convert.ToInt32(ReportConfigCodeList.PunchStatics))
            {
               
                dialog = new PrintPreviewDialogEx(true, this);
            }
             else
             {
                 dialog = new PrintPreviewDialogEx();
             }
           
            dialog.Document = this;
            dialog.commonListReprint = this;
            
            if (dialog.ShowDialog() == DialogResult.OK)
                dialog.Close();
         
            return;
        }

       
    }
}