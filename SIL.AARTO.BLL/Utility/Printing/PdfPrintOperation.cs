﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using NDde;
using NDde.Client;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class PdfPrintOperation : IRetryable
    {
        private DdeClient _client;
        private string _filePath;

        public PdfPrintOperation(string filePath)
        {
            _filePath = filePath;
            _client = new DdeClient("acroview", "control");
        }

        public bool Attemp()
        {
            try
            {
                _client.Connect();
                return true;
            }
            catch (DdeException)
            {
                // ignore exception
            }

            return false;
        }

        public void Recover()
        {
            // try running Adobe Reader
            Process p = new Process();
            p.StartInfo.FileName = "AcroRd32.exe";
            //p.StartInfo.CreateNoWindow = true;
            //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.Start();
            p.WaitForInputIdle();
            //p.CloseMainWindow();
        }

        public void Print()
        {
            _client.Execute("[DocOpen(\"" + _filePath + "\")]", 60000);
            _client.Execute("[FilePrintSilent(\"" + _filePath + "\")]", 60000);
            _client.Execute("[DocClose(\"" + _filePath + "\")]", 60000);
            _client.Execute("[AppExit]", 60000);
        }
    }
}
