﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class Retryer
    {
        private IRetryable _operation;

        public Retryer(IRetryable operation)
        {
            _operation = operation;
        }

        public void Perform(int maximumAttempts, int attemptInterval)
        {
            bool succeeded = false;

            for (int i = 1; (i <= maximumAttempts || maximumAttempts < 0) && !succeeded; ++i)
            {
                if (_operation.Attemp())
                {
                    succeeded = true;
                }
                else
                {
                    _operation.Recover();
                    if (attemptInterval > 0)
                    {
                        System.Threading.Thread.Sleep(attemptInterval);
                    }
                }
            }

            if (!succeeded)
            {
                throw new RetryFailedException("Maximun failed attempts was reached.");
            }

        }
    }

    class RetryFailedException : Exception
    {
        public RetryFailedException(string message)
            : base(message)
        {
        }

        public RetryFailedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

    }
}
