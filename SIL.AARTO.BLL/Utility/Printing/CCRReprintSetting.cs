﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class CCRReprintSetting
    {
        public bool Print1st { get; set; }
        public bool Print2nd { get; set; }
        public bool Print3rd { get; set; }
        public bool Print4th { get; set; }
        public bool ApplyToAll { get; set; }
    }
}
