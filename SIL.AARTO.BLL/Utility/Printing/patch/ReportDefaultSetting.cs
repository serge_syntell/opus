using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDefaultSettingBase
    {
        private static string SettingFileName = @"ReportSettings.xml";
        public static ReportSettingXML settingXML;

        public static Font font = new Font("Courier New", 10);
        public static int PageWidth = 1390;
        public static int PageHeight = 1100;
        public static int BorderWidth = 10;
        public static int BorderBottom = 10;
        public static int BorderLeft = 10;
        public static int BorderRight = 10;
        public static int BorderTop = 10;
        public static string PaperName = "US Std Fanfold";
        public static bool OriginAtMargins = false;
        public static int BodyWidth
        {
            get { return PageWidth - BorderLeft - BorderRight; }
        }

        public static int BodyHeight
        {
            get { return PageHeight - BorderBottom - BorderTop; }
        }
        static ReportDefaultSettingBase()
        {
            try
            {
                var serializer =
                    new XmlSerializer(typeof(ReportSettingXML));
                using (var fs = new FileStream(SettingFileName, FileMode.Open))
                {
                    TextReader reader = new StreamReader(fs, new UTF8Encoding());
                    settingXML = serializer.Deserialize(reader) as ReportSettingXML;
                }
                if (settingXML != null)
                {
                    font = new Font(settingXML.FontName, settingXML.FontSize, settingXML.GraphicsUnit);
                    PageWidth = settingXML.PageWidth;
                    PageHeight = settingXML.PageHeight;

                    BorderWidth = settingXML.BorderWidth;
                    BorderBottom = settingXML.BorderBottom;
                    BorderLeft = settingXML.BorderLeft;
                    BorderRight = settingXML.BorderRight;
                    BorderTop = settingXML.BorderTop;

                    PaperName = settingXML.PaperName;
                    OriginAtMargins = settingXML.OriginAtMargins;

                }
            }
            catch (Exception ex)
            {
                //will use default settings value.
            }
        }



    }

    /// <summary>
    /// added by Jacob
    /// static values.
    /// </summary>
    public class ReportDefaultSetting : ReportDefaultSettingBase
    {
    }

    public class ReportSettingXML
    {

        public int BorderBottom;
        public int BorderLeft, BorderRight;
        public int BorderTop;
        public int BorderWidth;

        public string FontName = "Courier New";
        public float FontSize = 10;
        public GraphicsUnit GraphicsUnit = GraphicsUnit.Point;

        public bool OriginAtMargins;

        public int PageHeight;
        public int PageWidth;
        public string PaperName;
    }
}