using System.Collections.Generic;
using System.Data;
using SIL.AARTO.BLL.Report.Model;
using System;

namespace SIL.AARTO.BLL.Utility.Printing
{
    /// <summary>
    /// special page creator for cpa6 report, create two page for one data row.
    /// </summary>
    public class PageGeneratorForTwoPages:PageGenerator
    {
        public override List<FreePage> CreatePages(List<CellDefination> cellDefinations
                                                   ,  DataTable dtData)
        {
            var pages = new List<FreePage>();
            foreach (DataRow dataRow in dtData.Rows)
            {
                var firstPage = new FreePage();
                var secondPage = new FreePage();
                foreach (CellDefination cellDefination in cellDefinations)
                {
                    //seperate column to two pages, base on the PageNum field of cell defination  
                    var cell = GetCellByDef(dataRow, cellDefination);
                    switch (cellDefination.PageNum)
                    {
                        case 1:
                            firstPage.AddToList(cell);
                            break;
                        case 2:
                            secondPage.AddToList(cell);
                            break;
                    }
                }
                firstPage.MainDataRow = dataRow;
                secondPage.MainDataRow = dataRow;
                pages.Add(firstPage);
                pages.Add(secondPage);
            }
            for (int i = 0; i < pages.Count; i += 2)
            {
                pages[i].PageNum = Convert.ToInt32(Math.Floor(Convert.ToDouble(i / 2))) + 1;
            }
            return pages;
        }

     
    }
}