﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForServiceCpa6 : FreeStylePrintEngineForService
    {
        public FreeStylePrintEngineForServiceCpa6(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForTwoPages(); //2014-02-26 Heidi changed for fixed Set the page number problem(bontq997)
            CurrentReportDataService = new ReportDataServiceForCPA6(conns);          
            //connection string need to be set before call.
        }
    }
}
