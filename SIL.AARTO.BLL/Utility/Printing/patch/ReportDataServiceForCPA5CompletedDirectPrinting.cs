using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Report.Model;
using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;
using System.IO;
using System.Linq;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForCPA5CompletedFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\Completed\CPA5"; }
        } 

        public override string FieldForFileName
        {
            get { return "SumPrintFileName"; }
        }
      
        //must override and make it empty to avoid moving files, as completed files can't move.
        public override void SaveReportDataToHistory(DataTable dataTable)
        {

        }
    }
}