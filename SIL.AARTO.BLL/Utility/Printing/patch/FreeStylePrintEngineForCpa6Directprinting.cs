using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using System.Collections.Generic;
using System.Threading;

namespace SIL.AARTO.BLL.Utility.Printing
{
                  
    public class FreeStylePrintEngineForCpa6DirectPrinting : FreeStylePrintEngineForFileSystem
    {
        /// <summary>
        /// Override for CPA5 direct printing, change data service when user changing mode 
        /// don't forget initializing data service.
        /// </summary>
        public override ReportFormMode CurrentMode
        {
            get
            { return base.CurrentMode; }
            set
            {
                base.CurrentMode = value;
                if (value is ReportNewMode || value is ReportHistoryMode)
                {
                    CurrentReportDataService = new ReportDataServiceForCPA6(DBConnectionString);
                    CurrentReportDataService.InitializeReportConfig(this.PrintModel.RcIntNo);
                }
                else if (value is NewFilesMode)
                {
                    CurrentReportDataService = new ReportDataServiceForCPA6NewFileDirectPrinting();
                    CurrentReportDataService.RCIntNo = this.PrintModel.RcIntNo; //for getting LPT port
                }
                else if (value is CompletedFilesMode)
                {
                    CurrentReportDataService = new ReportDataServiceForCPA6CompletedFileDirectPrinting();
                    CurrentReportDataService.RCIntNo = this.PrintModel.RcIntNo; //for getting LPT port
                }
            }
        }

        //public override string FieldForFileName
        //{
        //    get { return "SumPrintFileName"; }
        //}

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "SummonsNo";
        //    }
        //}

        public FreeStylePrintEngineForCpa6DirectPrinting(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForTwoPages();
            //Edited by Jacob 20131217 start
            // to use new data service.
            //CurrentReportDataService = new ReportDataServiceForCPA5(conns); 
            CurrentReportDataService = new ReportDataServiceForCPA6NewFileDirectPrinting();
            //Edited by Jacob 20131217 end
            //connection string need to be set before call.
        }

    }
}