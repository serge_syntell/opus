using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForCPA6 : ReportDataService
    {
        //public override string rptType
        //{
        //    get { return "CPA6"; }
        //}

        public ReportDataServiceForCPA6(string conn)
        {
            //CurrentLogTable = "ReportHistoryForCPA6";
            CurrentConnnectionString = conn;
        }

        // 2013-10-15, Oscar added for field name of Number of document
        public override string FieldForNoOfDocument { get { return "NoOfNotices"; } }

        public override string FieldForFileName
        {
            get { return "SumPrintFileName"; }
        }

        //2014-03-10 Heidi added for fixed search by document number not working
        public override string FieldForDocumentNumber { get { return "SummonsNo"; } }

        public override DataTable LoadPrintFiles(ReportModel model)
        {
            /*
             EXEC	@return_value = [dbo].[NoticePrintrun]
                @AutIntNo = 20,
                @Type = N'SUM',
                @StatusLoad = 3,
                @ShowAll = N'N',
                @ShowControlRegister = N'Y',
                @DateFrom = '2000/2/2',
                @DateTo = '2020/2/2'
             */
            string StrConnetion = CurrentConnnectionString;

            DataTable SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();
                //2013-08-27 added by Henry for Filter by AR_5110 and AR_5120
                int autIntNo = Convert.ToInt32(model.AutIntNo);
                SqlCommand selCmd = new SqlCommand("SummonsPrint_Files", SQLConn) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-09-18 Updated by Nancy for time out
                selCmd.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
                selCmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar, 6) { Value = "CPA6" });
                selCmd.Parameters.Add(new SqlParameter("@StatusLoad", SqlDbType.Int) { Value = 610 });
                selCmd.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@ShowControlRegister", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@DateFrom", SqlDbType.SmallDateTime) { Value = model.DateFrom });
                selCmd.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.SmallDateTime) { Value = model.DateTo });
                
                //2013-08-27 added by Henry for Filter by AR_5110 and AR_5120
                selCmd.Parameters.Add(new SqlParameter("@AR_5110", SqlDbType.NVarChar, 1) 
                { 
                    Value = AuthorityRule.GetAuthorityRulesKeyValue("5110", autIntNo).Value.Trim()
                });
                selCmd.Parameters.Add(new SqlParameter("@AR_5120", SqlDbType.NVarChar, 1)
                {
                    Value = AuthorityRule.GetAuthorityRulesKeyValue("5120", autIntNo).Value.Trim()
                });

                // 2013-04-16 add by Henry for pagination
                selCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = model.PageSize;
                selCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = model.PageIndex;

                SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                paraTotalCount.Direction = ParameterDirection.Output;
                selCmd.Parameters.Add(paraTotalCount);

                SqlDataAdapter adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);

                model.TotalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return SQLTable;
            }

            //                //Remember to change this to stored procedure later. jacob
            //                string SQLStr =
            //                    @"SELECT SumPrintFileName AS 'FileName', 
            //                                Count(Summons.SumIntNo) As 'SummonsCount'
            //			                    FROM Summons
            //			                    WHERE SummonsStatus = 620
            //			                    GROUP BY SumPrintFileName";

            //                var SQLAdapter2 = new SqlDataAdapter();
            //                SQLAdapter2.SelectCommand = new SqlCommand(SQLStr, SQLConn);
            //                SQLAdapter2.Fill(SQLTable);


        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {
            //Need to load print file list from summons table,
            //caller can show the table in winform, wait for select.
            //then call with the file names selected
            
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("fileName");
            foreach (string f in files)
            {
                dtInput.Rows.Add(new object[] { f });
            }

            var ds = new DataSet();
            DataTable dtData = null;
            // Create Instance of Connection and Command Object

            var myConnection = new SqlConnection(CurrentConnnectionString);
            var myCommand = new SqlCommand("SummonsHWOPrint_Details", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout()};

            myCommand.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
            myCommand.Parameters.Add(new SqlParameter("@fileNames", SqlDbType.Structured) { Value = dtInput });
            myCommand.Parameters.Add(new SqlParameter("@PrintFileName", SqlDbType.VarChar) { Value = "" });
            myCommand.Parameters.Add(new SqlParameter("@Mode", SqlDbType.VarChar) { Value = "N" });
            myCommand.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.VarChar) { Value = "ReportEngine" });
            myCommand.Parameters.Add(new SqlParameter("@IsCourtNumber", SqlDbType.VarChar) { Value = 0 });

            //2013-08-27 added by Henry for Filter by AR_5110 and AR_5120
            AuthorityRule.ConnnectionString = CurrentConnnectionString;//2013-12-12 added by Nancy
            myCommand.Parameters.Add(new SqlParameter("@AR_5110", SqlDbType.NVarChar, 1)
            {
                Value = AuthorityRule.GetAuthorityRulesKeyValue("5110", autIntNo).Value.Trim()
            });
            myCommand.Parameters.Add(new SqlParameter("@AR_5120", SqlDbType.NVarChar, 1)
            {
                Value = AuthorityRule.GetAuthorityRulesKeyValue("5120", autIntNo).Value.Trim()
            });

            // Mark the Command as a SPROC  
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                if (ds.Tables.Count < 1)
                {
                    throw new Exception("Print data is null");
                }
                dtData = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //EntLibLogger.WriteLog(LogCategory.Exception, null, "SP:" + "SummonsCPA5_PrintTest" + " " + ex.Message);
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dtData;

        }

        public override void ModifyPrintDate(DataTable dataTable)
        {
            using (var myConnection = new SqlConnection(CurrentConnnectionString))//2013-08-08 updated by Nancy
            {
                myConnection.Open();
                string strPrintFileName = "";//2013-07-01 added by Nancy(Avoid repeat modifing date for the same one PrintFileName) 
                DateTime PrePrintDate = DateTime.MinValue;//2013-07-11 added by Nancy
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    //2013-06-03 added by Nancy for getting authority rule start
                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = this.IAutIntNo;
                    arDetails.ARCode = "2570";
                    arDetails.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
                    DefaultAuthRules authRule = new DefaultAuthRules(arDetails, CurrentConnnectionString);
                    KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();
                    //2013-06-03 added by Nancy for getting authority rule end

                    if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["SumPrintFileName"].ToString())
                    {
                        strPrintFileName = dataTable.Rows[i]["SumPrintFileName"].ToString();

                        var myCommand = new SqlCommand("NoticePrintSummonsHWO_Update_WS", myConnection)
                        {
                            CommandType = CommandType.StoredProcedure,
                            CommandTimeout = GetSqlCmdTimeout()
                        };

                        myCommand.Parameters.Add(new SqlParameter("@PrintFileName", dataTable.Rows[i]["SumPrintFileName"].ToString()));
                        myCommand.Parameters.Add(new SqlParameter("@Mode", ""));
                        myCommand.Parameters.Add(new SqlParameter("@LastUser", GlobalVariates<User>.CurrentUser.UserLoginName));                        
                        myCommand.Parameters.Add(new SqlParameter("@IsCourtNumber", 1));
                        myCommand.Parameters.Add("@PrintDate", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;
                        myCommand.Parameters.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = valueArDet.Value }); //2013-06-03 added by Nancy for 4973

                        //2013-09-03 added by Henry for Filter by AR_5110 and AR_5120
                        //2013-12-04 updated by Nancy
                        //from 'base.AutIntNo' to 'this.IAutIntNo'
                        AuthorityRule.ConnnectionString = CurrentConnnectionString;//2013-12-12 added by Nancy
                        myCommand.Parameters.Add(new SqlParameter("@AR_5110", SqlDbType.NVarChar, 1)
                        {
                            Value = AuthorityRule.GetAuthorityRulesKeyValue("5110", this.IAutIntNo).Value.Trim()
                        });
                        myCommand.Parameters.Add(new SqlParameter("@AR_5120", SqlDbType.NVarChar, 1)
                        {
                            Value = AuthorityRule.GetAuthorityRulesKeyValue("5120", this.IAutIntNo).Value.Trim()
                        });

                        try
                        {
                            myCommand.ExecuteScalar();
                            DateTime ModifiedPrintDate = Convert.ToDateTime(myCommand.Parameters["@PrintDate"].Value);
                            dataTable.Rows[i]["SumPrintSummonsDate"] = ModifiedPrintDate;
                            PrePrintDate = ModifiedPrintDate;//2013-07-11 added by Nancy
                        }
                        catch (Exception ex)
                        {
                            //throw ex;
                            // 2013-10-16, Oscar added print file name
                            throw new Exception(string.Format("PrintFileName: {0}; Error: {1}", strPrintFileName, ex.Message), ex);
                        }
                        finally
                        {
                            myCommand.Dispose();
                        }
                    }
                    else
                    {//2013-07-11 added by Nancy
                        dataTable.Rows[i]["SumPrintSummonsDate"] = PrePrintDate;
                    }
                }
            }
        }

    }


    public class ReportDataServiceForCPA6CompletedFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\Completed\CPA6"; }
        }

        public override string FieldForFileName
        {
            get { return "SumPrintFileName"; }
        }

        //must override and make it empty to avoid moving files, as completed files can't move.
        public override void SaveReportDataToHistory(DataTable dataTable)
        {

        }
    }

    public class ReportDataServiceForCPA6NewFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\CPA6"; }
        }

        public override string FieldForFileName
        {
            get { return "SumPrintFileName"; }
        }

       
    }
}