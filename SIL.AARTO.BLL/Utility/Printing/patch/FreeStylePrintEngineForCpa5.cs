using System;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForCpa5 : FreeStylePrintEngine
    {
        public FreeStylePrintEngineForCpa5(PageGenerator gen) : base(gen)
        {
        }
        //public override string FieldForFileName
        //{
        //    get { return "SumPrintFileName"; }
        //}

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "SummonsNo";
        //    }
        //}

        public FreeStylePrintEngineForCpa5()
            :this("")
        {
            
        }
        public FreeStylePrintEngineForCpa5(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            CurrentReportDataService = new ReportDataServiceForCPA5(conns);          
            //connection string need to be set before call.

        }
       
    }
}