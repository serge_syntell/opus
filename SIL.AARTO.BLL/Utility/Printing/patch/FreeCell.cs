using System;
using System.Diagnostics;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeCell : IPrintable
    {
        public virtual CellContainer Container { get; set; }
        public virtual string Text { get; set; }
        public virtual PrintPortionDef PrintPortionDef { get; set; }
    
        public CellDefination cellStyle;
        public FreeCell( CellDefination s)
        {
            this.cellStyle = s;
        }
        public FreeCell( )
        {
            this.cellStyle = new CellDefination();
        }
        public virtual void Print(PrintElement element)
        {
            var block = new PrintPrimitiveForBlockText(Text)
                            {
                                CurrentCellDef=cellStyle
                             
                            }; 
            element.AddPrimitive(block);
        }
    } 

    public class PlainTextCell : FreeCell
    {
         

        public PlainTextCell(CellDefination cellDefination)
        {

            this.cellStyle = cellDefination;
        }

        public override string Text
        {
            get
            {
                var def = this.cellStyle as PlainTextCellDef;
                if (def != null) return def.PlainText2;
                else
                    return "null";
            }
        }

    } 
    public class PageNumCell : FreeCell
    {
        
        public PageNumCell(CellDefination cellDefination)
        {
            
            this.cellStyle = cellDefination;
        }
         
        public override string Text
        {
            get
            {
                FreePage page = Container as FreePage;
                string text = "";
                if (page != null) 
                    text=page.PageNum.ToString();
                else
                {
                    text = "PageNumCell must set in Page.";
                }
                return text;
            }
        }

    }

    public class DataFieldCell : FreeCell
    {
        private string originText = "";
        public override string Text
        {
            get
            {
                DateTime dt = DateTime.MinValue;
                if (!string.IsNullOrEmpty(cellStyle.DataFormat))
                {
                    //currently only datetime format 
                    if (DateTime.TryParse(originText, out dt))
                    {
                        originText = string.Format("{0:" + cellStyle.DataFormat + "}", dt);
                    }
                    else//support decimal format.20130411 Jacob.
                    {
                        Decimal decTarget = Decimal.MinValue;
                        if(Decimal.TryParse(originText,out decTarget))
                        {
                            originText = string.Format("{0:" + cellStyle.DataFormat + "}", decTarget);
                        }
                    }
                   
                }
                else
                {
                    if (DateTime.TryParse(originText, out dt))
                    {
                        originText = string.Format("{0:yyyy/MM/dd}", dt);
                    }
                }
                return cellStyle.IsUpperCase ? originText.ToUpper() : originText;
            }
            set { this.originText = value.Replace("\r", "\r\n");   }
        }
    }
    
    /// <summary>
    /// not used, now  create test page in the page generator.
    /// </summary>
    public class DummyFreeCell : FreeCell
    {
        public override string Text
        {
            get
            {
                string rtn = "";
               rtn= rtn.PadRight(cellStyle.Width, '*');
                return rtn;
            }
            
        }
    }

}