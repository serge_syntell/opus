﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForService : FreeStylePrintEngine
    {
        public FreeStylePrintEngineForService(string folder, string file)
        {
            ReportFilesFolder = folder; PrintFileName = file;
        }

        public FreeStylePrintEngineForService()
        {
            // TODO: Complete member initialization
        }
        private string _reportFilesFolder;
        private string _printFileName;
        //Jake 2014-08-28 added, set default form length 12 inch
        private int _formLength = 12;

        public virtual string ReportFilesFolder
        {
            set { _reportFilesFolder = value; }
            get { return _reportFilesFolder; }
        }

        public virtual string PrintFileName
        {
            set { _printFileName = value; }
            get { return _printFileName; }
        }

        protected int FormLength
        {
            get { return _formLength; }
            set { _formLength = value; }
        }

        // 2014-02-14, Oscar added
        public virtual string ExportedFile { get; set; }

        public override void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {
            //special page generator decides how to create pages for print.
            List<FreePage> pages = CurrentPageGenerator.CreatePages(DataColunms, dtData);
            PrintPages(pages);
        }

        public override void PrintPages(List<FreePage> pages)
        {

            string inputReportFilesFolder = _reportFilesFolder, inputPrintFileName = _printFileName;

            _printObjects.Clear();
            var commandLine = "";

            foreach (FreePage page in pages)
            {
                //int formLength = Convert.ToInt32(12);
                commandLine += "\x1B\x43\x00" + (char)(_formLength);

                //Character Pitch 12 cpi
                commandLine += "\x1B\x3a";
                commandLine += "\x1B\x41" + (char)12; //Heidi 2013-10-08 changed for fixed Print the offset
                var pieces = CombineToLines(page);
                pieces = ((string)pieces).TrimEnd(new[] { '\r', '\n' });
                commandLine += pieces;

                commandLine += "\x0c";
            }

            if (!Directory.Exists(inputReportFilesFolder))
                Directory.CreateDirectory(inputReportFilesFolder);

            inputPrintFileName = Helper.ReplaceSpecialCharacters(inputPrintFileName);//2014-01-16 Heidi added for use "/" not create file(5101)
            string exportFullPath = inputReportFilesFolder + "\\" + inputPrintFileName + ".prn";

            // 2014-02-14, Oscar added
            ExportedFile = exportFullPath;

            //write the exported files
            using (FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                {
                    sw.Write(commandLine);
                }
            }
        }
        public override void PrintPages(List<FreePage> pages, ListPrintEngine listEngine)
        {
            this.ShowGridLine = listEngine.ShowGridLine;

            PrintPages(pages);

            /* Jake comment out to re-use PringPages function
             * 
            string inputReportFilesFolder = _reportFilesFolder, inputPrintFileName = _printFileName;

            _printObjects.Clear();
            var commandLine = "";
            this.ShowGridLine = listEngine.ShowGridLine;
            foreach (FreePage page in pages)
            {
                int formLength = Convert.ToInt32(12);
                commandLine += "\x1B\x43\x00" + (char)(formLength);

                //Character Pitch 12 cpi
                commandLine += "\x1B\x3a";
                commandLine += "\x1B\x41" + (char)12; //Heidi 2013-10-08 changed for fixed Print the offset
                var pieces = CombineToLines(page);
                pieces = ((string)pieces).TrimEnd(new[] { '\r', '\n' });
                commandLine += pieces;

                commandLine += "\x0c";
            }

            if (!Directory.Exists(inputReportFilesFolder))
                Directory.CreateDirectory(inputReportFilesFolder);

            inputPrintFileName = Helper.ReplaceSpecialCharacters(inputPrintFileName);//2014-01-16 Heidi added for use "/" not create file(5101)
            string exportFullPath = inputReportFilesFolder + "\\" + inputPrintFileName + ".prn";

            // 2014-02-14, Oscar added
            ExportedFile = exportFullPath;

            //write the exported files
            using (FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
                {
                    sw.Write(commandLine);
                }
            }
             * */
        }

    }
}
