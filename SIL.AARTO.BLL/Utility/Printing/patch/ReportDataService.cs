using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.ServiceBase;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class  ReportDataService
    {
       
        /// <summary>
        /// some specific report types in some specific modes, should access file system,
        /// This indicates where to get files.
        /// </summary>
        /// <remarks>
        /// 20131217 added by Jacob.
        /// </remarks>
        /// 
        public virtual string RootFolder
        {
            get { return "";}

        }
        protected string CurrentLogTable { get; set; }
        public string CurrentConnnectionString { get; set; }
        public int RCIntNo { get; set; }
        public string ReportType { get; set; }
        public int AutIntNo { get; set; }   // 2013-09-03 add by Henry for AR_5110 and AR_5120 filter
        public int IAutIntNo { get; set; } //2013-06-17 added by Nancy  

        public string LastUser { get; set; } // 2013-07-23 add by Henry
        // 2013-10-15, Oscar added for field name of Number of document
        public virtual string FieldForNoOfDocument { get { return string.Empty; } }

        // 2013-10-16, Oscar moved from FreeStylePrintEngine to here
        public virtual string FieldForFileName { get; set; }

        //Add by Brian 2013-10-11
        //Removed by Jacob  20131217, should use variables defined by Oscar
        //public string FileNameFieldName { get; set; }
        //public string DocumentNumberName { get; set; }

        //2014-03-10 Heidi added for fixed search by document number not working
        public virtual string FieldForDocumentNumber { get { return string.Empty; } }
      

        //Add by Brian 2013-10-18
        public string CurrentPCMACAddress { get; set; }

        /// <summary>
        /// save normal report data to history
        /// </summary>
        /// <param name="dataTable"></param>
        public virtual void SaveReportDataToHistory(DataTable dataTable)
        {
            if (string.IsNullOrWhiteSpace(CurrentLogTable)) return;

            //insert to table report history
            //then insert into ReportHistoryFor CPA5
            var myConnection = new SqlConnection(CurrentConnnectionString);
            string StrConnetion = CurrentConnnectionString;
            //Remember to change this to stored procedure later. jacob
            string SQLStr = @"SELECT * FROM " + CurrentLogTable + " ";
            DataTable SQLTable = dataTable;
            foreach (DataRow row in SQLTable.Rows)
            {
                // row.SetAdded();
            }
            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();

                string SQLStr2 = @"SELECT * FROM  ReportHistory where 1=2 ";
                SqlTransaction tran = SQLConn.BeginTransaction();
                try
                {
                    var id = InsertMainTable(tran, SQLStr2, SQLConn);
                    InsertDetailTable(SQLTable, id, tran, SQLStr, SQLConn);
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;//Jacob added 20130314, should not hide the exception.
                }
            }
        }

        private void InsertDetailTable(DataTable SQLTable, object id, SqlTransaction tran, string SQLStr,
                                       SqlConnection SQLConn)
        {
            var SQLAdapter2 = new SqlDataAdapter();
            SQLAdapter2.SelectCommand = new SqlCommand(SQLStr, SQLConn) { CommandTimeout = GetSqlCmdTimeout() };
            SQLAdapter2.SelectCommand.Transaction = tran;
            SqlCommandBuilder thisBuilder2 = new SqlCommandBuilder(SQLAdapter2);

            var cmd11 = thisBuilder2.GetInsertCommand();
            cmd11.Transaction = tran;
            var cmd21 = thisBuilder2.GetDeleteCommand();
            cmd21.Transaction = tran;
            var cmd31 = thisBuilder2.GetUpdateCommand();
            cmd31.Transaction = tran;
            SQLAdapter2.InsertCommand = cmd11;
            SQLAdapter2.UpdateCommand = cmd31;
            SQLAdapter2.DeleteCommand = cmd21;

            //added by jacob 20120727 to prevent adding existing column.
            if (!SQLTable.Columns.Contains("HistoryId"))
            {
                SQLTable.Columns.Add(new DataColumn("HistoryId", typeof(Int64)));
            }
            if (SQLTable.Columns.Contains("Id"))
            {
                SQLTable.Columns.Remove("Id");//
            }
            foreach (DataRow row in SQLTable.Rows)
            {
                //2013-12-03 added by Nancy start
                row.AcceptChanges();
                row.SetAdded(); 
                //2013-12-03 added by Nancy end
                row["HistoryId"] = id;                
            }

            SQLAdapter2.Update(SQLTable);
        }

        private object InsertMainTable(SqlTransaction tran, string SQLStr2, SqlConnection SQLConn)
        {

            //Remember to change this to stored procedure later. jacob
            var SQLAdapter = new SqlDataAdapter();
            SQLAdapter.SelectCommand = new SqlCommand(SQLStr2, SQLConn);
            SQLAdapter.SelectCommand.Transaction = tran;
            SqlCommandBuilder thisBuilder = new SqlCommandBuilder(SQLAdapter);
            DataTable dtMain = new DataTable();


            var cmd1 = thisBuilder.GetInsertCommand();

            cmd1.Transaction = tran;
            var cmd2 = thisBuilder.GetDeleteCommand();
            cmd2.Transaction = tran;
            var cmd3 = thisBuilder.GetUpdateCommand();
            cmd3.Transaction = tran;
            SQLAdapter.InsertCommand = cmd1;
            SQLAdapter.UpdateCommand = cmd3;
            SQLAdapter.DeleteCommand = cmd2;

            SQLAdapter.Fill(dtMain);
            DataRow dr = dtMain.NewRow();
            dr["PrintTime"] = DateTime.Now;
            dr["LastUser"] = "Report Engine";
            //dr["ReportType"] = rptType;
            dr["RCIntNo"] = RCIntNo;
            dtMain.Rows.Add(dr);
            DataSet ds = new DataSet();
            ds.Tables.Add(dtMain);
            ds.Tables[0].TableName = "aa";

            var ss = SQLAdapter.Update(ds, "aa");
            SqlCommand command = new SqlCommand("SELECT @@IDENTITY AS Id");
            command.Connection = SQLConn;
            command.Transaction = tran;
            var id = command.ExecuteScalar();
            return id;
        }

        //public virtual string rptType { get; set; }

        /// <summary>
        /// print history, user, date
        /// </summary>
        /// <returns></returns>
        public virtual DataTable LoadPrintHistory(string fileName, string documentNumber, int pageSize, int pageIndex, out int totalCount)
        {
            string StrConnetion = CurrentConnnectionString;
            DataTable SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();
                SqlCommand selCmd = new SqlCommand("GetPrintHistory", SQLConn) { CommandType = CommandType.StoredProcedure };
                //selCmd.Parameters.Add(new SqlParameter("@ReportType", SqlDbType.NVarChar, 20)).Value = this.rptType;
                selCmd.Parameters.Add(new SqlParameter("@RCIntNo", this.RCIntNo));

                // 2013-04-17 add by Henry for pagination
                selCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;
                selCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pageIndex;

                //Add by Brian 2013-10-11
                selCmd.Parameters.Add("@CurrentLogTable", SqlDbType.NVarChar, 200).Value = this.CurrentLogTable;
                //Edited by Jacob  20131217 start, should use variables defined by Oscar
                //selCmd.Parameters.Add("@FileNameFieldName", SqlDbType.VarChar, 50).Value = this.FileNameFieldName;
                //selCmd.Parameters.Add("@DocumentNumberFieldName", SqlDbType.VarChar, 50).Value = this.DocumentNumberName;
                selCmd.Parameters.Add("@FileNameFieldName", SqlDbType.VarChar, 50).Value = this.FieldForFileName;
                //2014-03-10 Heidi changed for fixed search by document number not working
                selCmd.Parameters.Add("@DocumentNumberFieldName", SqlDbType.VarChar, 50).Value = this.FieldForDocumentNumber;//this.FieldForNoOfDocument;
                //Edited by Jacob  20131217 start
                selCmd.Parameters.Add("@FileName", SqlDbType.VarChar, 200).Value = fileName;
                selCmd.Parameters.Add("@DocumentNumber", SqlDbType.VarChar, 100).Value = documentNumber;

                SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                paraTotalCount.Direction = ParameterDirection.Output;
                selCmd.Parameters.Add(paraTotalCount);

                SqlDataAdapter adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);

                totalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
            }
            return SQLTable;
        }

        /// <summary>
        /// get report data from different report history data table
        /// then convert to normal report data structure.
        /// </summary>
        /// <returns></returns>
        public virtual DataTable LoadReportDataHistory(int hisId, string fileName, string documentNumber)
        {
            string StrConnetion = CurrentConnnectionString;
            DataTable SQLTable = new DataTable();

            if (!string.IsNullOrWhiteSpace(CurrentLogTable))
            {
                using (var SQLConn = new SqlConnection(StrConnetion))
                {
                    SQLConn.Open();
                    SqlCommand selCmd = new SqlCommand("GetReportDataHistory", SQLConn) { CommandType = CommandType.StoredProcedure };
                    selCmd.Parameters.Add(new SqlParameter("@CurrentLogTable", SqlDbType.NVarChar, 200)).Value = CurrentLogTable;
                    selCmd.Parameters.Add(new SqlParameter("@HistoryID", SqlDbType.BigInt)).Value = hisId;

                    //Add by Brian 2013-10-11

                    //Edited by Jacob  20131217 start, should use variables defined by Oscar
                    //selCmd.Parameters.Add("@FileNameFieldName", SqlDbType.VarChar, 50).Value = this.FileNameFieldName;
                    //selCmd.Parameters.Add("@DocumentNumberFieldName", SqlDbType.VarChar, 50).Value = this.DocumentNumberName;
                    selCmd.Parameters.Add("@FileNameFieldName", SqlDbType.VarChar, 50).Value = this.FieldForFileName;
                    //2014-03-10 Heidi changed for fixed search by document number not working
                    selCmd.Parameters.Add("@DocumentNumberFieldName", SqlDbType.VarChar, 50).Value = this.FieldForDocumentNumber;// this.FieldForNoOfDocument;
                    //Edited by Jacob  20131217 end
                    selCmd.Parameters.Add("@FileName", SqlDbType.VarChar, 200).Value = fileName;
                    selCmd.Parameters.Add("@DocumentNumber", SqlDbType.VarChar, 100).Value = documentNumber;

                    SqlDataAdapter adapter = new SqlDataAdapter(selCmd);
                    adapter.Fill(SQLTable);
                }

                //added by jacob 20120727 start
                //  GetReportDataHistory=> ReportData
                //     remove the first two columns.
                if (SQLTable.Columns.Contains("HistoryId"))
                {
                    SQLTable.Columns.Remove("HistoryId");
                }
                if (SQLTable.Columns.Contains("Id"))
                {
                    SQLTable.Columns.Remove("Id");
                }
            }
            else
            {
                throw new Exception("The history table name is not defined.");
            }
            //added by jacob 20120727 end
            return SQLTable;
        }

        /// <summary>
        /// Load print file from database.
        /// </summary>
        /// <returns></returns>
        public virtual DataTable LoadPrintFiles(ReportModel model)
        {
            return null;
        }

        public virtual  DataTable LoadReportDataByPrintFiles(List<string> files,int autIntNo)
        {
            return null;
        }

        public virtual void ModifyPrintDate(DataTable dataTable)
        {
        }

        public virtual void ModifyNoticeStatus(DataTable dataTable)
        {
        }

        public void InitializeReportConfig(int rcIntNo, string connStr)
        {
            InitializeReportConfig(rcIntNo);
            InitializeReportConfig(connStr);
        }
        public void InitializeReportConfig(int rcIntNo)
        {

            if (RCIntNo != 0) return;
            RCIntNo = rcIntNo;
            CurrentLogTable = null;
            var rcEntity = new ReportConfigService().GetByRcIntNo(rcIntNo);
            if (rcEntity != null)
            {
                ReportType = rcEntity.ReportType;
                CurrentLogTable = rcEntity.HistoryTableName;
                IAutIntNo = rcEntity.AutIntNo; //2013-06-17 added by Nancy
            }
        }
        public void InitializeReportConfig(string connStr)
        {
            CurrentConnnectionString = connStr;
        }

        // Oscar 2013-06-09 added for temporary solution.
        public int GetSqlCmdTimeout(string key = "SqlCmdTimeout")
        {
            string value;
            int num;
            if (string.IsNullOrWhiteSpace(key)
                || string.IsNullOrWhiteSpace(value = ConfigurationManager.AppSettings[key])
                || !int.TryParse(value, out num))
                return 30;

            return num;
        }

        // 2013-10-16, Oscar added
        public int GetDocumentsAmountLimit(string key = "DocumentsAmountLimit")
        {
            string value;
            int num;
            if (string.IsNullOrWhiteSpace(key)
                || string.IsNullOrWhiteSpace(value = ConfigurationManager.AppSettings[key])
                || !int.TryParse(value, out num))
                return 500;

            return num;
    }

        // 2013-10-15, Oscar added transaction for saving history and modifying print date and status
    
        public void SaveHistory_ModifyPrintDateAndStatus(DataTable source, bool addHistory, bool modifyDateAndStatus)
        {
            using (var scope = ServiceUtility.CreateTransactionScope())
            {
                if (modifyDateAndStatus)
                    ModifyPrintDate(source);

                if (addHistory)
                    SaveReportDataToHistory(source);

                if (modifyDateAndStatus )
                    ModifyNoticeStatus(source);

                if (ServiceUtility.TransactionCanCommit())
                    scope.Complete();
                else
                    throw new TransactionAbortedException();
            }
        }

        //Add by Brian 2013-10-18
        public Dictionary<string, string> GetReportLPTPortAndPrinterPath()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            LocalPcSettingsQuery lpsQuery = new LocalPcSettingsQuery();
            int psIntNo = 0;
            lpsQuery.Append(LocalPcSettingsColumn.AutIntNo, this.AutIntNo.ToString());
            lpsQuery.Append(LocalPcSettingsColumn.PcmacAddress, this.CurrentPCMACAddress);
            lpsQuery.Append(LocalPcSettingsColumn.RcIntNo, this.RCIntNo.ToString());
            LocalPcSettings lps = new LocalPcSettingsService().Find(lpsQuery).FirstOrDefault();
            if (lps != null)
            {
                dict.Add("LPTPort", lps.LptPort);
                psIntNo = lps.PsIntNo;
    }
            else
            {
                ReportSettingsQuery rsQuery = new ReportSettingsQuery();
                rsQuery.Append(ReportSettingsColumn.AutIntNo, this.AutIntNo.ToString());
                rsQuery.Append(ReportSettingsColumn.IsDefault, "true");
                rsQuery.Append(ReportSettingsColumn.RcIntNo, this.RCIntNo.ToString());
                ReportSettings rs = new ReportSettingsService().Find(rsQuery).FirstOrDefault();
                if (rs != null)
                {
                    dict.Add("LPTPort", rs.LptPort);
                    psIntNo = rs.PsIntNo;
                }
            }
            if (psIntNo > 0)
            {
                PrinterSettings ps = new PrinterSettingsService().GetByPsIntNo(psIntNo);
                if (ps != null)
                {
                    dict.Add("PrinterPath", ps.PsPath);
                }
            }
            return dict;
        }
    }


    public class ListPrintEngineForHistoryReprint : FreeStylePrintEngine
    {
        public ListPrintEngineForHistoryReprint(PageGenerator gen)
            : base(gen)
        {
        }
       
        public ListPrintEngineForHistoryReprint()
        {
            
            CurrentReportDataService = new ListReportHistoryDataService();

        }
        public ListPrintEngineForHistoryReprint(int rcIntNo)
        {
             
            CurrentReportDataService = new ListReportHistoryDataService();
            CurrentReportDataService.InitializeReportConfig(rcIntNo);

        }
        public override void PrintTestPage()
        {
            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("rcintno=" + CurrentReportDataService.RCIntNo.ToString() + "");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            listPrint.CreateEngineWithTestData(rc.RcIntNo);
        }

        public override void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {
            // 2013-10-15, Oscar removed - adding transaction for saving history and modifying print date and status
            //if (addHistory)
            //{
            //    CurrentReportDataService.SaveReportDataToHistory(dtData);
            //}
            if (onlyHistory)
            {
                return;
            }

            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("rcintno=" + CurrentReportDataService.RCIntNo.ToString() + "");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            ReportModel printModel = GetPrintTemplate(rc.RcIntNo);
            listPrint.CreatePreviewEngine(printModel, dtData);
            rc.AutomaticGenerateLastDatetime = DateTime.Now;
            // 2013-07-16 add by Henry for LastUser
            rc.LastUser = CurrentReportDataService.LastUser;
            ReportManager.UpdateReport(rc);
        }

    }
    public class ListReportHistoryDataService : ReportDataService
    {

    }  
}