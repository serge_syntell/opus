﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForWOADirectPrinting : FreeStylePrintEngineForFileSystem
    {
       /// <summary>
        /// Override for WOA direct printing, change data service when user changing mode 
        /// don't forget initializing data service.
        /// //2013-12-25 Heidi added for WOA Direct Printing (5101)
        /// </summary>
        public override ReportFormMode CurrentMode
        {
            get
            { return base.CurrentMode; }
            set
            {
                base.CurrentMode = value;
                if (value is ReportNewMode || value is ReportHistoryMode)
                {
                    CurrentReportDataService = new ReportDataServiceForWOA(DBConnectionString);
                    CurrentReportDataService.InitializeReportConfig(this.PrintModel.RcIntNo);
                }
                else if (value is NewFilesMode)
                {
                    CurrentReportDataService = new ReportDataServiceForWOANewFileDirectPrinting();
                    CurrentReportDataService.RCIntNo = this.PrintModel.RcIntNo; //for getting LPT port
                }
                else if (value is CompletedFilesMode)
                {
                    CurrentReportDataService = new ReportDataServiceForWOACompletedFileDirectPrinting();
                    CurrentReportDataService.RCIntNo = this.PrintModel.RcIntNo; //for getting LPT port
                }
            }
        }

       //2013-12-25 Heidi added for WOA Direct Printing (5101)
       //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "WOANumber";
        //    }
        //}

        public FreeStylePrintEngineForWOADirectPrinting(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            //Edited by Jacob 20131217 start
            // to use new data service.
            //CurrentReportDataService = new ReportDataServiceForCPA5(conns); 
            CurrentReportDataService = new ReportDataServiceForWOANewFileDirectPrinting();
            //Edited by Jacob 20131217 end
            //connection string need to be set before call.
        }
    }
}
