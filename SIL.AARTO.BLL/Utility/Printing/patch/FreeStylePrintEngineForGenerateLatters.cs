using System;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForGenerateLatters : FreeStylePrintEngine
    {
        public FreeStylePrintEngineForGenerateLatters(PageGenerator gen) : base(gen)
        {
        }
        //public override string FieldForFileName
        //{
        //    get { return "PrintFileName"; }
        //}

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "NotTicketNo";
        //    }
        //}

        public FreeStylePrintEngineForGenerateLatters()
        {
            CurrentPageGenerator = new PageGeneratorForOnePage();
            CurrentReportDataService=new ReportDataServiceForGenerateLetters("");
            //connection string need to be set before call.
            
        }


        public FreeStylePrintEngineForGenerateLatters(string conns)
        {
            DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            CurrentReportDataService = new ReportDataServiceForGenerateLetters(conns);
            //connection string need to be set before call.

        }
       
    }
}