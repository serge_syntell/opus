﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineFor2ndNoticeOfBus : FreeStylePrintEngine
    {
        public FreeStylePrintEngineFor2ndNoticeOfBus(PageGenerator gen)
            : base(gen)
        {
        }
        //public override string FieldForFileName
        //{
        //    get { return "Not2ndNoticePrintFile"; }
        //}

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "NotTicketNo";
        //    }
        //}

        public FreeStylePrintEngineFor2ndNoticeOfBus()
            :this("")
        {
           
        }

        public FreeStylePrintEngineFor2ndNoticeOfBus(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            CurrentReportDataService = new ReportDataServiceFor2ndNoticeOfBus(conns);
        }
    }
}
