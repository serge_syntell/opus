using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForGenerateLetters : ReportDataService
    {
        //public override string rptType
        //{
        //    get { return "Generate Letters"; }
        //}

        public ReportDataServiceForGenerateLetters(string conn)
        {
            //CurrentLogTable = "ReportHistoryForGenerateLetters";
            CurrentConnnectionString = conn;
        }

        // 2013-10-15, Oscar added for field name of Number of document
        public override string FieldForNoOfDocument { get { return "NoOfNotices"; } }

        public override string FieldForFileName
        {
            get { return "PrintFileName"; }
        }

        //2014-03-10 Heidi added for fixed search by document number not working
        public override string FieldForDocumentNumber { get { return "NotTicketNo"; } }

        public override DataTable LoadPrintFiles(ReportModel model)
        {
            string StrConnetion = CurrentConnnectionString;

            DataTable SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();//Heidi 2014-08-21 added CommandTimeout for fixing time out issue.(bontq1408)
                SqlCommand selCmd = new SqlCommand("GenerateLettersDetail_GetPrintBatchFile", SQLConn) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };
                selCmd.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = Convert.ToInt32(model.AutIntNo) });

                // 2013-04-17 add by Henry for pagination
                selCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = model.PageSize;
                selCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = model.PageIndex;

                SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                paraTotalCount.Direction = ParameterDirection.Output;
                selCmd.Parameters.Add(paraTotalCount);

                SqlDataAdapter adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);

                model.TotalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return SQLTable;
            }
        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("fileName");
            foreach (string f in files)
            {
                dtInput.Rows.Add(new object[] { f });
            }

            var ds = new DataSet();
            DataTable dtData = null;
            // Create Instance of Connection and Command Object

            var myConnection = new SqlConnection(CurrentConnnectionString);
            var myCommand = new SqlCommand("GenerateLetters_GetByPrintFile", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'

            myCommand.Parameters.Add(new SqlParameter("@autIntNo", SqlDbType.Int) { Value = autIntNo });
            myCommand.Parameters.Add(new SqlParameter("@PrintedFlag", SqlDbType.Bit) { Value = false });
            myCommand.Parameters.Add(new SqlParameter("@WFRFAName", SqlDbType.NVarChar, 200) { Value = "AR_6209" });
            myCommand.Parameters.Add(new SqlParameter("@fileNames", SqlDbType.Structured) { Value = dtInput });

            // Mark the Command as a SPROC  
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                if (ds.Tables.Count < 1)
                {
                    throw new Exception("Print data is null");
                }
                dtData = ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dtData;
        }

        public override void ModifyPrintDate(DataTable dataTable)
        {
            using (var myConnection = new SqlConnection(CurrentConnnectionString))//2013-08-08 updated by Nancy
            {
                myConnection.Open();//2013-07-23 updated by Nancy
                string strPrintFileName = "";//2013-07-23 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["PrintFileName"].ToString())
                    {//2013-07-22 added by Nancy('if...')
                        strPrintFileName = dataTable.Rows[i]["PrintFileName"].ToString();//2013-07-22 added by Nancy
                        var myCommand = new SqlCommand("GenerateLetterDetail_UpdateStatues", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'
                        myCommand.Parameters.Add("@RepIntNo", SqlDbType.Int, 4).Value = Convert.ToInt32(dataTable.Rows[i]["RepIntNo"].ToString());
                        //2013-07-04 added by Nancy start
                        if (dataTable.Rows[i]["RepIntNo2"] != null && !string.IsNullOrEmpty(dataTable.Rows[i]["RepIntNo2"].ToString()))
                        {
                            myCommand.Parameters.Add("@RepIntNo2", SqlDbType.Int, 4).Value = Convert.ToInt32(dataTable.Rows[i]["RepIntNo2"].ToString());
                        }
                        if (dataTable.Rows[i]["RepIntNo3"] != null && !string.IsNullOrEmpty(dataTable.Rows[i]["RepIntNo3"].ToString()))
                        {
                            myCommand.Parameters.Add("@RepIntNo3", SqlDbType.Int, 4).Value = Convert.ToInt32(dataTable.Rows[i]["RepIntNo3"].ToString());
                        }
                        //2013-07-04 added by Nancy end
                        myCommand.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = GlobalVariates<SIL.AARTO.DAL.Entities.User>.CurrentUser.UserLoginName;
                        //myCommand.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = dataTable.Rows[i]["RepPrintFileName"].ToString();

                        try
                        {
                            myCommand.ExecuteScalar();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            myCommand.Dispose();
                        }
                    }
                }
            }
        }


    }
}