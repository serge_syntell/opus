﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForServiceWOA : FreeStylePrintEngineForService
    {
        public FreeStylePrintEngineForServiceWOA(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            CurrentReportDataService = new ReportDataServiceForWOA(conns);         
            //connection string need to be set before call.
        }
    }
}
