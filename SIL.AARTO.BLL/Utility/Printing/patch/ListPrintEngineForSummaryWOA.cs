using System;
using System.Data;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Threading;

namespace SIL.AARTO.BLL.Utility.Printing
{
    

    public class ListPrintEngineForSummaryWOA : FreeStylePrintEngine
    {
        public ListPrintEngineForSummaryWOA(PageGenerator gen)
            : base(gen)
        {
        }
         

        public ListPrintEngineForSummaryWOA()
        {
            // CurrentPageGenerator = new PageGeneratorForCpa5();
            CurrentReportDataService = new ReportDataServiceForSummaryWOA("");
        }
        public ListPrintEngineForSummaryWOA(string conns)
        {
            DBConnectionString = conns;
            // CurrentPageGenerator = new PageGeneratorForCpa5();
            CurrentReportDataService = new ReportDataServiceForSummaryWOA(conns);
        }
        public override void PrintTestPage()
        {
            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)ReportConfigCodeList.WOASheet).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            listPrint.CreateEngineWithTestData(rc.RcIntNo);
        }

        public override void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {
            // 2013-10-15, Oscar removed - adding transaction for saving history and modifying print date and status
            //if (addHistory)
            //{
            //    CurrentReportDataService.SaveReportDataToHistory(dtData);
            //}
            if (onlyHistory)
            {
                return;
            }

            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)ReportConfigCodeList.WOASheet).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            ReportModel printModel = GetPrintTemplate(rc.RcIntNo);
            //listPrint.PrinterSettings.PrintToFile = true;
            //listPrint.PrinterSettings.PrintFileName = "c:\\sample.prn";
           
            listPrint.CreatePreviewEngine(printModel, dtData);
           // listPrint.CreateMyEngine(printModel, dtData);

         
            rc.AutomaticGenerateLastDatetime = DateTime.Now;
            // 2013-07-16 add by Henry for LastUser
            rc.LastUser = CurrentReportDataService.LastUser;
            ReportManager.UpdateReport(rc);
        }

    }
 
}