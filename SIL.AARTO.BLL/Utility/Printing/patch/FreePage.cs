using System.Collections.Generic;
using System.Data;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class CellContainer : IPrintable
    {
        public List<FreeCell> cells = new List<FreeCell>(); //2013-09-16 Update by Nancy for direct printing
        public DataRow MainDataRow { get; set; }
        public void Print(PrintElement element)
        {
            foreach (var c in cells)
            {
                c.Print(element);
            }

        }
    }

    public class FreePage : CellContainer
    { 
       
        public int PageNum { get; set; }
        public void AddToList(FreeCell cell )
        {
            this.cells.Add(cell);
            cell.Container = this;
        }
    }

}