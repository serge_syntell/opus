using System;
using System.Drawing;
using System.Linq;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    /// <summary>
    /// freedom report, just draw all printPrimitive object in the element.
    /// added by jacob 20120605
    /// </summary>
    public class PrintPrimitiveForBlockText : IPrintPrimitive
    {
        public string Text;

        public PrintPrimitiveForBlockText(string buf)
        {
            Text = buf;
        }

        public CellDefination CurrentCellDef = new DefaultCellDefination();

        public event DrawStringHandler OnDrawString;

        public float CalculateHeight(PrintEngine engine, Graphics graphics)
        {
            return PrintEngine.GetCellContentSize(Text).Height;
            //return engine.PrintFont.GetHeight(graphics) + 0;
        }

        public void Draw(PrintEngine engine, float xPos, float yPos, Graphics graphics, Rectangle elementBounds)
        {
            //use CurrentCell defination to draw the string. 
            Pen pen = new Pen(engine.PrintBrush, 1);
            string printText = engine.ReplaceTokens(Text);

            //20130724 Jacob for long description
            if (CurrentCellDef.LineCharacters != null)
            {
                printText = ProcessText(printText, CurrentCellDef.LineCharacters);

            }
            //Jacob edited 20130314 
            //if there's a style defined in cell defination then use it.
            StringFormat stringFormat=CurrentCellDef.style??new StringFormat();
            //stringFormat.Alignment= StringAlignment.Center;
            if (CurrentCellDef.CellFont == null) CurrentCellDef.CellFont = engine.PrintFont;
                graphics.DrawString(printText,
                                    CurrentCellDef.CellFont,// engine.PrintFont,
                                    engine.PrintBrush,
                                    CurrentCellDef.Rect,
                                    stringFormat);
        }


        string ProcessText(string text, int[] lines = null, int lineIndex = 0)
        {
            if (string.IsNullOrEmpty(text)
                || lines == null
                || lines.Length <= 0
                || lineIndex >= lines.Length
                || text.Length <= lines[lineIndex]
                )
                return text;

            int index;
            var i = lines[lineIndex] - 1;

            var current = text[0];
            if (current == 13 || current == 10)
                return ProcessText(text.Remove(0, 1), lines, lineIndex);

            var temp = text.Substring(0, i + 1);
            var temp2 = text.Substring(i + 1, text.Length - i - 1);

            current = text[i];
            if (IsReserved(current))
            {
                index = text.ToList().FindLastIndex(i, c => !IsReserved(c));
                if (index >= 0)
                {
                    temp = text.Substring(0, index + 1);
                    temp2 = text.Substring(index + 1, text.Length - index - 1);
                }
            }

            index = temp.IndexOf('\r');
            var index2 = index;
            if (index >= 0)
            {
                if (text[index + 1] == 10)
                {
                    index2 = index + 1;
                }

                temp = text.Substring(0, index);
                temp2 = text.Substring(index2 + 1, text.Length - index2 - 1);

            }

            temp += Environment.NewLine;
            return temp + ProcessText(temp2, lines, ++lineIndex);
        }

        bool IsReserved(char current)
        {
            return (current >= 48 && current <= 57)
                || (current >= 65 && current <= 90)
                || (current >= 97 && current <= 122);
        }

    }
}