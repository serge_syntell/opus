﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineFor2ndNoticeVIDEO : FreeStylePrintEngine
    {
        public FreeStylePrintEngineFor2ndNoticeVIDEO(PageGenerator gen)
            : base(gen)
        {
        }
        //public override string FieldForFileName
        //{
        //    get { return "Not2ndNoticePrintFile"; }
        //}

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "NotTicketNo";
        //    }
        //}

        public FreeStylePrintEngineFor2ndNoticeVIDEO()
            :this("")
        {
           
        }
        public FreeStylePrintEngineFor2ndNoticeVIDEO(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            CurrentReportDataService = new ReportDataServiceFor2ndNoticeVIDEO(conns);
        }
    }
}
