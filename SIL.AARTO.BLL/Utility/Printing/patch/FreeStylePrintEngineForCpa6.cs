using System;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForCpa6 : FreeStylePrintEngine
    {
        public FreeStylePrintEngineForCpa6(PageGenerator gen)
            : base(gen)
        {
        }
        //public override string FieldForFileName
        //{
        //    get { return "SumPrintFileName"; }
        //}
        public FreeStylePrintEngineForCpa6()
            :this("")
        {
            
        }

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "SummonsNo";
        //    }
        //}

        public FreeStylePrintEngineForCpa6(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForTwoPages();
            CurrentReportDataService = new ReportDataServiceForCPA6(conns);
        }

    }
}