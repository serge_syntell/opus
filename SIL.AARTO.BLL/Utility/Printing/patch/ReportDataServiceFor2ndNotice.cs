using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.PostalManagement;
using System.Diagnostics;
using System.Transactions;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceFor2ndNotice : ReportDataService
    {
        //public override string rptType
        //{
        //    get { return "2nd Notice"; }
        //}

        public ReportDataServiceFor2ndNotice(string conn)
        {
            //CurrentLogTable = "ReportHistoryForSecondNotice";
            CurrentConnnectionString = conn;
        }

        // 2013-10-15, Oscar added for field name of Number of document
        public override string FieldForNoOfDocument { get { return "NoOFNotices"; } }

        public override string FieldForFileName
        {
            get { return "Not2ndNoticePrintFile"; }
        }

        //2014-03-10 Heidi added for fixed search by document number not working
        public override string FieldForDocumentNumber { get { return "NotTicketNo"; } }

        public override DataTable LoadPrintFiles(ReportModel model)
        {
            string StrConnetion = CurrentConnnectionString;

            DataTable SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();
                SqlCommand selCmd = new SqlCommand("SecondNotice_GetPrintFileNames", SQLConn) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() }; //2013-12-09 Updated by Nancy for time out
                selCmd.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = Convert.ToInt32(model.AutIntNo) });
                selCmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar, 6) { Value = "2ND" });
                selCmd.Parameters.Add(new SqlParameter("@StatusLoad", SqlDbType.Int) { Value = 260 });
                selCmd.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@ShowControlRegister", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@DateFrom", SqlDbType.SmallDateTime) { Value = model.DateFrom });
                selCmd.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.SmallDateTime) { Value = model.DateTo });

                //2013-04-16 add by Henry for pagination
                selCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = model.PageSize;
                selCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = model.PageIndex;

                SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                paraTotalCount.Direction = ParameterDirection.Output;
                selCmd.Parameters.Add(paraTotalCount);

                SqlDataAdapter adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);

                model.TotalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return SQLTable;
            }
        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("fileName");
            foreach (string f in files)
            {
                dtInput.Rows.Add(new object[] { f });
            }

            var ds = new DataSet();
            DataTable dtData = null;
            // Create Instance of Connection and Command Object

            var myConnection = new SqlConnection(CurrentConnnectionString);
            var myCommand = new SqlCommand("NoticePrint_SecondNotice", myConnection) { CommandType = CommandType.StoredProcedure ,CommandTimeout = GetSqlCmdTimeout()};//2013-07-26 Nancy add 'Timeout'

            myCommand.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
            myCommand.Parameters.Add(new SqlParameter("@PrintFile", SqlDbType.VarChar, 50) { Value = "" });
            myCommand.Parameters.Add(new SqlParameter("@NotIntNo", SqlDbType.Int) { Value = 0 });//
            myCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int){Value = 260 });
            myCommand.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Char, 1) {Value = "N" });
            myCommand.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int){Value = 2});
            myCommand.Parameters.Add(new SqlParameter("@fileNames", SqlDbType.Structured) { Value = dtInput });

            // Mark the Command as a SPROC  
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                if (ds.Tables.Count < 1)
                {
                    throw new Exception("Print data is null");
                }
                dtData = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //EntLibLogger.WriteLog(LogCategory.Exception, null, "SP:" + "SummonsCPA5_PrintTest" + " " + ex.Message);
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dtData;
        }

        public override void ModifyPrintDate(DataTable dataTable)
        {
            
			using (var myConnection = new SqlConnection(CurrentConnnectionString))//2013-08-08 updated by Nancy
            {
                myConnection.Open();//2013-07-22 updated by Nancy
                string strPrintFileName = "";//2013-07-22 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
                DateTime PrePrintDate = DateTime.MinValue;//2013-07-22 added by Nancy
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    //2013-06-13 added by Nancy for getting authority rule start
                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = Convert.ToInt16(dataTable.Rows[i]["autIntNo"].ToString());
                    arDetails.ARCode = "2570";
                    string lastUser = "";
                    //arDetails.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
                    //2013-12-25 Heidi updated for Service PrintToFileService(5101)
                    //add if..else for Service PrintToFileService
                    if (GlobalVariates<User>.CurrentUser != null)
                    {
                        lastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
                    }
                    else
                    {
                        lastUser = this.LastUser;
                    }
                    arDetails.LastUser = lastUser;
                    DefaultAuthRules authRule = new DefaultAuthRules(arDetails, CurrentConnnectionString);
                    KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();
                    //2013-06-13 added by Nancy for getting authority rule end

                    if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString())
                    {//2013-07-22 added by Nancy('if...')
                        strPrintFileName = dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString();//2013-07-22 added by Nancy
                        var myCommand = new SqlCommand("NoticePrint_Update2nd_WS", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'

                        myCommand.Parameters.Add(new SqlParameter("@AutIntNo", dataTable.Rows[i]["autIntNo"].ToString()));
                        myCommand.Parameters.Add(new SqlParameter("@PrintFile", dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString()));
                        myCommand.Parameters.Add(new SqlParameter("@Status", 260));
                        myCommand.Parameters.Add(new SqlParameter("@ShowAll", "N"));
                        //2013-12-25 Heidi updated for Service PrintToFileService(5101)
                        //myCommand.Parameters.Add(new SqlParameter("@LastUser", GlobalVariates<User>.CurrentUser.UserLoginName));
                        myCommand.Parameters.Add(new SqlParameter("@LastUser", lastUser));
                        myCommand.Parameters.Add(new SqlParameter("@ViolationCutOff", new char()));
                        myCommand.Parameters.Add(new SqlParameter("@NoOfDaysIssue", new int()));
                        myCommand.Parameters.Add("@ModifiedPrintDate", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;
                        myCommand.Parameters.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = valueArDet.Value }); //2013-06-13 added by Nancy for 4973

                        try
                        {
                            myCommand.ExecuteScalar();
                            DateTime ModifiedPrintDate = Convert.ToDateTime(myCommand.Parameters["@ModifiedPrintDate"].Value);
                            dataTable.Rows[i]["NotPrint2ndNoticeDate"] = ModifiedPrintDate;
                            PrePrintDate = ModifiedPrintDate;//2013-07-22 added by Nancy
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            myCommand.Dispose();
                        }
                    }
                    else
                    {//2013-07-22 added by Nancy
                        dataTable.Rows[i]["NotPrint2ndNoticeDate"] = PrePrintDate;
                    }
                }
            }
        }

        public override void ModifyNoticeStatus(DataTable dataTable) {        
            string errMessage = string.Empty;
            int status2ndLoaded = 260;
            int status2ndPrinted = 270;
            NoticeDB noticeDB = new NoticeDB(CurrentConnnectionString);
            for (int i = 0; i < dataTable.Rows.Count;i++ )
            {
                string strPrintFileName = "";//2013-07-30 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
                //using (TransactionScope scope = new TransactionScope())
                // 2013-09-30, Oscar changed
                using (var scope = ServiceBase.ServiceUtility.CreateTransactionScope())
                {
                    if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString())
                    {//2013-07-30 added by Nancy('if...')
                        strPrintFileName = dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString();//2013-07-30 added by Nancy
                        string lastUser = Process.GetCurrentProcess().ProcessName.Replace(".vshost", string.Empty).Replace("SIL.AARTO.", string.Empty);
                        int row = noticeDB.UpdateNoticeChargeStatus(
                            Convert.ToInt32(dataTable.Rows[i]["autIntNo"].ToString()),
                            dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString(),
                            status2ndLoaded, status2ndPrinted, "SecondNotice", lastUser, ref errMessage);

                        string printFileName = dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString();
                        int autIntNo = Convert.ToInt32(dataTable.Rows[i]["autIntNo"].ToString());

                        // 2013-09-30, Oscar added "posted" to separate "row"
                        var posted = 1;

                        if (row > 0)
                        {
                            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                            arDetails.AutIntNo = autIntNo;
                            arDetails.ARCode = "4200";
                            arDetails.LastUser = lastUser;
                            DefaultAuthRules ar = new DefaultAuthRules(arDetails, CurrentConnnectionString);
                            bool isPostDate = ar.SetDefaultAuthRule().Value.Equals("Y");

                            if (isPostDate)
                            {
                                PostDateNotice postDate = new PostDateNotice(CurrentConnnectionString);
                                //row = postDate.SetNoticePostedDate(printFileName, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), lastUser, ref errMessage);
                                // 2013-09-30, Oscar changed
                                posted = postDate.SetNoticePostedDate(printFileName, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), lastUser, ref errMessage);
                            }
                        }
                        else
                        {
                            errMessage = string.Format("Error {0} - unable to set print status. {1}", row.ToString(), errMessage);  // 2013-10-21, added {1}-errMessage
                        }

                        //if (row > 0)
                        // 2013-09-30, Oscar changed, we have to commit when "row = 0"
                        if (row >= 0 && posted > 0)
                        {
                            scope.Complete();
                        }
                        else
                        {
                            throw new Exception(errMessage);
                        }
                    }
                }
            }
        }
    }

    //2013-12-23 Heidi added for 2nd Notice Direct Printing (5101)
    public class ReportDataServiceFor2ndNoticeCompletedFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\Completed\Second Notice"; }
        }

        public override string FieldForFileName
        {
            get { return "SecondNoticePrintFileName"; }
        }

        //must override and make it empty to avoid moving files, as completed files can't move.
        public override void SaveReportDataToHistory(DataTable dataTable)
        {

        }
    }

    //2013-12-23 Heidi added for 2nd Notice Direct Printing (5101)
    public class ReportDataServiceFor2ndNoticeNewFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\Second Notice"; }
        }

        public override string FieldForFileName
        {
            get { return "SecondNoticePrintFileName"; }
        }


    }
}