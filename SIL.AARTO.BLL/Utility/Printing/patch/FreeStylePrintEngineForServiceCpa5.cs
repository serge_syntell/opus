﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineForServiceCpa5 : FreeStylePrintEngineForService
    {
        public FreeStylePrintEngineForServiceCpa5(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            CurrentReportDataService = new ReportDataServiceForCPA5(conns);          
            //connection string need to be set before call.
        }
    }
}
