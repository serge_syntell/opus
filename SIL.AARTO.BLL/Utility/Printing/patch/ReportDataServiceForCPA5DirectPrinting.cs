using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Report.Model;
using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.IO;
using System.Linq;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataForFileService : ReportDataService {
         //read from queue parameter.
        protected string RootFolderFromQueueParameter
        {
            get
            {
                SysParam param = new SysParamService().GetBySpColumnName("ExportedReportFilesFolder");
                if (param != null)
                    return @param.SpStringValue;
                else
                    return "";
            } //@"D:\tmp3\PrintReports"
        }
        protected virtual string Subfolder 
        {
            get{return "";}
        }
        public override string RootFolder
        {
            get//get from service parameter.
            { return RootFolderFromQueueParameter + Subfolder; }
        }
        /// <summary>
        /// Load print file from database.
        /// </summary>
        /// <returns></returns>
        public override DataTable LoadPrintFiles(ReportModel model)
        {


            //List all files in the sub folder.
            DirectoryInfo info = new DirectoryInfo(RootFolder);
            if (!info.Exists) { Directory.CreateDirectory(info.FullName); }
            FileInfo[] files = info.GetFiles().Where(s => model.FileNameForFilter == null || s.Name.Contains(model.FileNameForFilter)).OrderByDescending(p => p.CreationTime)
                .Skip((model.PageIndex - 1) * model.PageSize).Where(s => model.FileNameForFilter == null || s.Name.Contains(model.FileNameForFilter)).Take(model.PageSize).ToArray();

            model.TotalCount = info.GetFiles().Where(s => model.FileNameForFilter == null || s.Name.Contains(model.FileNameForFilter)).Count();


            DataTable SQLTable = new DataTable();
            SQLTable.Columns.Add(FieldForFileName);
            SQLTable.Columns.Add("FileCreateDate", typeof(DateTime));
            foreach (FileInfo file in files)
            {
                var row = SQLTable.NewRow();
                row[0] = file.Name;//only filename, not fullpath.
                row[1] = file.CreationTime;
                SQLTable.Rows.Add(row);
            }
            return SQLTable;
        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {

            DataTable SQLTable = new DataTable();
            SQLTable.Columns.Add(FieldForFileName);
            foreach (var file in files)
            {
                var row = SQLTable.NewRow();
                row[0] = RootFolder + @"\" + file;//add full path.
                SQLTable.Rows.Add(row);
            }


            return SQLTable;

        }

        public void MoveFilesRepotToComplete(DataTable dataTable)
        {
            #region 2013-12-26 Moved by Nancy
            //file will be moved to new place at this step, 
            //should update the new file location back to data table, 
            //so that the next printing step can get the file correctly.
            //move file to completed folder
            DirectoryInfo di = new DirectoryInfo(RootFolderFromQueueParameter
                    + @"\Completed" + Subfolder);
            if (!di.Exists) { Directory.CreateDirectory(di.FullName); }
            foreach (DataRow row in dataTable.Rows)
            {

                string filename = row[FieldForFileName].ToString();
                string tempfile = (filename.Replace(RootFolder, "")).Replace(@"\", "");
                string targetFilename = RootFolderFromQueueParameter
                    + @"\Completed" + Subfolder + @"\" + tempfile;
                row[FieldForFileName] = targetFilename;
                Directory.Move(filename, targetFilename);

            }
            #endregion

        }
        public override void SaveReportDataToHistory(DataTable dataTable)
        {

        }
    }

    public class ReportDataServiceForCPA5NewFileDirectPrinting : ReportDataForFileService
    { 
        //part of the RootFolder
        protected override string Subfolder 
        {
            get { return @"\CPA5"; }
        }
        
       

        public override string FieldForFileName
        {
            get { return "SumPrintFileName"; }
        }
     
    }
}