using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.XPath;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using System.Text;
using System.Collections;
using System.IO;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngine : PrintEngine
    {
        private PageGenerator _currentPageGenerator;
        
        public string DBConnectionString
        {
            set
            {
                connStr = value;
                if(CurrentReportDataService!=null) CurrentReportDataService.CurrentConnnectionString = value;
            }
            get { return connStr; }

        }

        public FreeStylePrintEngine(PageGenerator gen)
        {
            _currentPageGenerator = gen;
        }

        public FreeStylePrintEngine()
        {
            _currentPageGenerator = new PageGenerator();
        }

        /// <summary>
        /// report code.  can get xml setting from db by this code.
        /// </summary>
        public virtual string ReportCode { get; set; }
        //public string FieldForFileName { get; set; }
        // 2013-10-16, Oscar changed
        public string FieldForFileName { get { return CurrentReportDataService != null ? CurrentReportDataService.FieldForFileName : string.Empty; } }
        //Added by Nancy 2013-11-19
        /// <summary>
        /// Change the text of Button7 for free style report
        /// </summary>
        public string Button7_Text
        {
            get { return "Show history"; }
        }

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public virtual string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "DocumentNumber";
        //    }
        //}

        //2014-03-10 Heidi changed for fixed search by document number not working
        public virtual string FieldForDocumentNumber
        {
            get { return CurrentReportDataService != null ? CurrentReportDataService.FieldForDocumentNumber : string.Empty; }
        }

        /// <summary>
        /// Page Generator for current report.
        /// Will reate cells and pages for print by column definations and data.
        /// </summary>
        public PageGenerator CurrentPageGenerator
        {
            get { return _currentPageGenerator ?? (_currentPageGenerator = new PageGenerator()); }
            set { _currentPageGenerator = value; }
        }

        /// <summary>
        /// Prepare print engine info.
        /// </summary>
        /// <param name="rcIntNo"></param>
        public override void CreateEngine(int rcIntNo)
        {
            //Prepare engine info.
            //mainly set the columns' defination
            
            //Get Report config info from database by id,
            //and convert it into ReportModel object.
            PrintModel = GetPrintTemplate(rcIntNo);
            
            printTemplate = PrintModel.xmlDoc;
            
            //Parse columns' definations base on the xml string in reportmodel object,
            //and set to class field DataColumns.
            DataColunms = GetColDefsByXML(printTemplate);

           // //useless code.
           //_columnNum = DataColunms.Count;
           //DataTableHeader = new PrintElement(null);
        }

        /// <summary>
        /// Get Report config info from database by config no,
        ///  and convert the config into ReportModel object.
        /// </summary>
        /// <param name="rcIntNo"></param>
        /// <returns></returns>
        protected override ReportModel GetPrintTemplate(int rcIntNo)
        {
            ReportConfig Rc = ReportManager.GetReportById(rcIntNo);
            var model = new ReportModel();


            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Rc.Template);

            model.xmlDoc = xmlDoc;
            model.DateFrom = Convert.ToDateTime("1996-01-01");
            model.DateTo = Convert.ToDateTime("2012-05-22");
            model.AutIntNo = Rc.AutIntNo.ToString();
            model.ReportName = Rc.ReportName;
            model.ReportCode = Rc.ReportCode;
            model.RcIntNo = Rc.RcIntNo;//20131217 added by Jacob, to parse report int no into data service class.
            return model;
        }

        protected override DataTable GetPrintData(ReportModel model)
        {
            return GeneratePrintDataTable(model);
        }

        protected override void MyPrintPage(PrintPageEventArgs e)
        { 
            Rectangle printBounds = GetPageBounds();
            float headerHeight = 0;
            float tblHeaderHeight = 0;

            var pageBounds = new Rectangle(printBounds.Left,
                                           (int) (printBounds.Top + headerHeight + tblHeaderHeight),
                                           printBounds.Width,
                                           (int) (printBounds.Height - headerHeight - tblHeaderHeight));

            float eleTopOffset = pageBounds.Top + 0;
            bool morePages = false;
            int elementsOnPage = 0;
            //freestyle report.
            int maxCount = _printElements.Count;
            if (_printIndex < _printElements.Count)
            {
                var element = (PrintElement) _printElements[_printIndex];
                element.Draw(this, eleTopOffset, e.Graphics, pageBounds);
                
                _printIndex++;
                elementsOnPage++;
                //jacob each element is a new page. Only the last page has no more page.
                morePages = _printIndex < maxCount;
            }

            e.HasMorePages = morePages;
        }



        /// <summary>
        /// Parse xml document to get cell definations(inherited from Column class),
        ///  This can be a common function, if all report config abide same rules.
        /// </summary>
        /// <param name="aXmlDocument">report config xml</param>
        /// <returns></returns>
        public virtual List<CellDefination> GetColDefsByXML(XmlDocument aXmlDocument)
        {
            var cellDefinations = new List<CellDefination>();
            if (aXmlDocument != null)
            {
                // Add data
                XmlNodeList pageNodes = aXmlDocument.GetElementsByTagName("PageMain");
                int pageNum = 0;
                foreach (XmlNode pageNode in pageNodes)
                {
                    pageNum++;
                    foreach (XmlNode node in pageNode.ChildNodes)
                    {
                        XPathNavigator nav = node.CreateNavigator();
                        CellDefination colDef = null;

                        //special/first node decide the coldef type.
                        //the col defination will decide to create different cell.
                        foreach (XmlNode cnode in node.ChildNodes)
                        {
                            if (cnode.Name.ToLower().Equals("datafield"))
                            {
                                colDef = new DataFieldCellDef { DataFiled = node.ChildNodes[0].InnerText };
                                break;
                            }
                            //For now, calculation field will only be PageNumCell type,
                            //but it can be extended to fit for some more calculation types
                            //We can set different formula string into the calculation content,
                            //and then parse the formula, to set the print data, in the page 
                            //generator class.
                            if (cnode.Name.ToLower().Equals("calc"))
                            {
                                colDef = new PageNumCellDef { Prefix = "" };
                                break;
                            }
                            if (cnode.Name.ToLower().Equals("plaintext"))
                            {
                                colDef = new PlainTextCellDef { PlainText2 = cnode.InnerText };
                                break;
                            }
                        }
                        int xPos = 0;
                        int yPos = 0;
                        int width = 0;
                        int height = 0;
                        int bold = 0;
                        int uppercase = 0;
                        float fontSize = PrintFont.Size;
                        string dataFormat = "";
                        string valign = "";//Jacob added 20130314  support text alignment in free style report
                        string halign = "";//Jacob added 20130314  support text alignment in free style report
                        int[] lineCharacters = null; //20130724 Jacob for long description
                        foreach (XmlNode cnode in node.ChildNodes)
                        {
                            switch (cnode.Name.ToLower())
                            {
                                case "positionx":
                                    xPos = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "positiony":
                                    yPos = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "width":
                                    width = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "height":
                                    height = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "fontsize":
                                    fontSize = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "fontbold":
                                    bold = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "uppercase":
                                    uppercase = Convert.ToInt32(cnode.InnerText);
                                    break;
                                case "dataformat":
                                    dataFormat = cnode.InnerText;
                                    break;
                                //Jacob added 20130314  support text alignment in free style report start
                                case "valign":
                                    valign = cnode.InnerText;
                                    break;
                                case "halign":
                                    halign = cnode.InnerText;
                                    break;
                                //Jacob added 20130314  support text alignment in free style report end
                                case "linecharacters"://20130724 Jacob for long description
                                    if (!string.IsNullOrWhiteSpace(cnode.InnerText))
                                    {
                                        lineCharacters = cnode.InnerText.Split(',').Select(s => Convert.ToInt32(s.Trim())).ToArray();
                                    }
                                    break;
                            }
                        }

                        //20130724 Jacob for long description
                        colDef.LineCharacters = lineCharacters;

                        //Jacob added 20130314  support text alignment in free style report start
                         colDef.style  =new StringFormat();
                        
                         switch (valign.ToLower())
                        {
                            case "bottom":
                                colDef.style.LineAlignment = StringAlignment.Far;
                                break;
                            case "middle":
                                colDef.style.LineAlignment = StringAlignment.Center;
                                break;
                            default:
                                colDef.style.LineAlignment = StringAlignment.Near;
                                break;
                        }
                        switch (halign.ToLower())
                        {
                            case "right":
                                colDef.style.Alignment = StringAlignment.Far;
                                break;
                            case "center":
                                colDef.style.Alignment = StringAlignment.Center;
                                break;
                            default:
                                colDef.style.Alignment = StringAlignment.Near;
                                break;
                        }
                        //Jacob added 20130314  support text alignment in free style report end

                        Font cellFont = PrintFont;

                        FontStyle fontStyle = bold == 1 ? FontStyle.Bold : FontStyle.Regular;
                        cellFont = new Font(PrintFont.FontFamily, fontSize, fontStyle);
                        colDef.Top = yPos;
                        colDef.Left = xPos;
                        colDef.Width = width;
                        colDef.Height = height;
                        colDef.IsUpperCase = uppercase == 1;
                        colDef.CellFont = cellFont;
                        colDef.DataFormat = dataFormat;
                        colDef.PageNum = pageNum;
                        cellDefinations.Add(colDef);
                    }
                }
            }
            return cellDefinations;
        }

        /// <summary>
        /// Print test page.
        /// </summary>
        public virtual void PrintTestPage()
        {

            //1. Clear existing print objects.
            _printObjects.Clear();

            //2. Call Generator to create test page/pages by the columns' definations
            //  in current engine, parsed before, in the CreateEngine method.
            List<FreePage> pages = _currentPageGenerator.CreatePagesForTest(DataColunms);

            //3. Add test pages into print object array in current engine.
            foreach (FreePage page in pages)
            {
                AddPrintObject(page);
            }

            //4. Set the page setting(PaperSize, Margins, Landscape flag...)
            SetPageSettings();

            //5. Preview current print document
            //   the execution sequence is:
            //      5.1 Current print document's OnBeginPrint method will be called.
            //          clear  _printElements list,then call each printobject's print
            //          method to add corresponding print element to _printElements list.
            //          the print method can be override to support variable element.
            //          Here the print objects are all FreePage type.
            //          Each print object is a print page.
            //      5.2 Afterwards, when current engine start to print, the "OnPrintPage"
            //          method of current engine will be called, each print element's
            //          method named "Draw"  will be called, to draw itself on the print 
            //          page's Graphics.
            //          Both the print object and the print element contain cell defination. 
            ShowPreview();

        }

        public void PrintReport()
        {
            DataTable dtData = GetPrintData(PrintModel);
            PrintReportWithData(dtData);
        }

        /// <summary>
        /// Get all data can be printed. 
        /// Call PrintReportWithData method to print data.
        /// </summary>
        /// <returns></returns>
        public DataTable GetPrintData()
        {
            return GetPrintData(PrintModel);
        }

        /// <summary>
        /// dtData structure should agree with the DataColunms defination from XML.
        /// DB data collection -> PrintDocument printing.
        /// </summary>
        /// <param name="dtData"></param>
        public virtual void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {
            //history function
            //first, save to history

            // 2013-10-15, Oscar removed - adding transaction for saving history and modifying print date and status
            //if (addHistory)
            //{
            //    CurrentReportDataService.SaveReportDataToHistory(dtData);//
            //}
            //if no need to print, exit.
            if (onlyHistory)
            {
                return;
            }
            //special page generator decides how to create pages for print.
            List<FreePage> pages = _currentPageGenerator.CreatePages(DataColunms, dtData);
            _printObjects.Clear();
            foreach (FreePage page in pages)
            {
                AddPrintObject(page);
            }

            SetPageSettings();
            if (HttpContext.Current == null)
                ShowPreview();
            else
                Print();
        }        

        private void SetPageSettings()
        {
            var settings = new PageSettings
            {
                PaperSize = new PaperSize("US Std Fanfold", 950, 1200),
                Margins = new Margins(0, 0, 0, 0),
                Landscape = false
            };

            DefaultPageSettings = settings;
        }


        protected virtual DataTable GeneratePrintDataTable(ReportModel model)
        {

            return null;
        }



        //Add by Brian 2013-10-18
        public string RegisterLPTPort()
        {
            Dictionary<string, string> dict = this.CurrentReportDataService.GetReportLPTPortAndPrinterPath();
            if (dict.ContainsKey("LPTPort") && !string.IsNullOrEmpty(dict["LPTPort"]) && dict.ContainsKey("PrinterPath") && !string.IsNullOrEmpty(dict["PrinterPath"]))
            {
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                try
                {
                    proc.StartInfo.FileName = "cmd.exe";
                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.RedirectStandardInput = true;
                    proc.StartInfo.RedirectStandardOutput = true;
                    proc.StartInfo.RedirectStandardError = true;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.Start();
                    proc.StandardInput.WriteLine(string.Format("net use {0} /delete", dict["LPTPort"]));
                    proc.StandardInput.WriteLine("exit");
                    proc.Start();
                    proc.StandardInput.WriteLine(string.Format("net use {0}: {1} /persistent:yes", dict["LPTPort"], dict["PrinterPath"]));
                    proc.StandardInput.WriteLine("exit");
                    while (!proc.HasExited)
                    {
                        proc.WaitForExit(1000);
                    }
                    string errormsg = proc.StandardError.ReadToEnd();
                    proc.StandardError.Close();
                    if (!string.IsNullOrEmpty(errormsg))
                    {
                        throw new Exception(string.Format("Sorry, unable to register the port:{0} for the printer:{1}. Please check the printer to make sure it has got connected correctly.", dict["LPTPort"], dict["PrinterPath"]));
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    proc.Close();
                    proc.Dispose();
                }
                return dict["LPTPort"];
            }
            throw new Exception("Sorry, please configure the report print settings!");
        }

        public virtual void PrintPages(List<FreePage> pages)
        {
             
        }


        public virtual void PrintPages(List<FreePage> Pages, ListPrintEngine listEngine)
        {
            
        }
    }
    /// <summary>
    /// one line for a text block
    /// </summary>
    /// <remarks>
    ///  Jacob added 20130904
    /// </remarks>
    public class LinePiece
    {
        public int yRow, xCharStart, xCharLength;
        public string text;
    }
}