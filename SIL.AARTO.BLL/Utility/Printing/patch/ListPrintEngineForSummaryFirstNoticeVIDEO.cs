﻿using System;
using System.Data;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public partial class ListPrintEngineForSummaryFirstNoticeVIDEO : FreeStylePrintEngine
    {
         public ListPrintEngineForSummaryFirstNoticeVIDEO(PageGenerator gen)
            : base(gen)
        {
        }

        //public override string FieldForFileName
        //{
        //    get { return "NotCiprusPrintReqName"; }
        //}

        //Add by Brian 2013-10-11
         //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "NotTicketNo";
        //    }
        //}

        public ListPrintEngineForSummaryFirstNoticeVIDEO()
        {
            CurrentReportDataService = new ReportDataServiceForSummary1stNoticeVIDEO("");
        }
        public ListPrintEngineForSummaryFirstNoticeVIDEO(string conns)
        {
            DBConnectionString = conns;
            CurrentReportDataService = new ReportDataServiceForSummary1stNoticeVIDEO(conns);
        }
        public override void PrintTestPage()
        {
            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)ReportConfigCodeList.FirstNoticeVIDEOSummary).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            listPrint.CreateEngineWithTestData(rc.RcIntNo);
        }

        public override void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {
            // 2013-10-15, Oscar removed - adding transaction for saving history and modifying print date and status
            //if (addHistory)
            //{
            //    CurrentReportDataService.SaveReportDataToHistory(dtData);
            //}
            if (onlyHistory)
            {
                return;
            }

            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)ReportConfigCodeList.FirstNoticeVIDEOSummary).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            ReportModel printModel = GetPrintTemplate(rc.RcIntNo);
            listPrint.CreatePreviewEngine(printModel, dtData);
            rc.AutomaticGenerateLastDatetime = DateTime.Now;
            // 2013-07-16 add by Henry for LastUser
            rc.LastUser = CurrentReportDataService.LastUser;
            ReportManager.UpdateReport(rc);
        }

    }
}
