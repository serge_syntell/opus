﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceFor2ndNoticeVIDEO : ReportDataService
    {
        public ReportDataServiceFor2ndNoticeVIDEO(string conn)
        {
            CurrentConnnectionString = conn;
        }

        // 2013-10-15, Oscar added for field name of Number of document
        public override string FieldForNoOfDocument { get { return "NoOFNotices"; } }

        public override string FieldForFileName
        {
            get { return "Not2ndNoticePrintFile"; }
        }

        //2014-03-10 Heidi added for fixed search by document number not working
        public override string FieldForDocumentNumber { get { return "NotTicketNo"; } }

        public override DataTable LoadPrintFiles(ReportModel model)
        {
            string StrConnetion = CurrentConnnectionString;

            DataTable SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();//Heidi 2014-08-21 added CommandTimeout for fixing time out issue.(bontq1408)
                SqlCommand selCmd = new SqlCommand("SecondNotice_FileNameList_VIDEO", SQLConn) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };
                selCmd.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = Convert.ToInt32(model.AutIntNo) });
                selCmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar, 6) { Value = "2ND" });
                selCmd.Parameters.Add(new SqlParameter("@StatusLoad", SqlDbType.Int) { Value = 260 });
                selCmd.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@ShowControlRegister", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@DateFrom", SqlDbType.SmallDateTime) { Value = model.DateFrom });
                selCmd.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.SmallDateTime) { Value = model.DateTo });
                selCmd.Parameters.Add(new SqlParameter("@IsVideo", SqlDbType.Bit) { Value = 1 });

                // 2013-09-24 add by Henry for pagination
                selCmd.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int) { Value = model.PageSize });
                selCmd.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.Int) { Value = model.PageIndex });
                SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                paraTotalCount.Direction = ParameterDirection.Output;
                selCmd.Parameters.Add(paraTotalCount);

                SqlDataAdapter adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);
                // 2013-09-24 add by Henry for pagination
                model.TotalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return SQLTable;
            }
        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("fileName");
            foreach (string f in files)
            {
                dtInput.Rows.Add(new object[] { f });
            }

            var ds = new DataSet();
            DataTable dtData = null;
            // Create Instance of Connection and Command Object

            var myConnection = new SqlConnection(CurrentConnnectionString);
            var myCommand = new SqlCommand("NoticePrint_SecondNotice_VIDEO", myConnection) { CommandType = CommandType.StoredProcedure ,CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'

            myCommand.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
            myCommand.Parameters.Add(new SqlParameter("@PrintFile", SqlDbType.VarChar, 50) { Value = "" });
            myCommand.Parameters.Add(new SqlParameter("@NotIntNo", SqlDbType.Int) { Value = 0 });//
            myCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int) { Value = 0 });
            myCommand.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Char, 1) { Value = "N" });
            myCommand.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int) { Value = 2 });
            myCommand.Parameters.Add(new SqlParameter("@fileNames", SqlDbType.Structured) { Value = dtInput });
            myCommand.Parameters.Add(new SqlParameter("@IsVideo", SqlDbType.Bit) { Value = 1 });

            // Mark the Command as a SPROC  
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                if (ds.Tables.Count < 1)
                {
                    throw new Exception("Print data is null");
                }
                dtData = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //EntLibLogger.WriteLog(LogCategory.Exception, null, "SP:" + "SummonsCPA5_PrintTest" + " " + ex.Message);
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dtData;
        }

        public override void ModifyPrintDate(DataTable dataTable)
        {
				using (var myConnection = new SqlConnection(CurrentConnnectionString))//2013-07-22 updated by Nancy
            	{
                myConnection.Open();
                string strPrintFileName = "";//2013-07-22 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
                DateTime PrePrintDate = DateTime.MinValue;//2013-07-22 added by Nancy
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    //2013-06-13 added by Nancy for getting authority rule start
                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = Convert.ToInt16(dataTable.Rows[i]["autIntNo"].ToString());
                    arDetails.ARCode = "2570";
                    arDetails.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
                    DefaultAuthRules authRule = new DefaultAuthRules(arDetails, CurrentConnnectionString);
                    KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();
                    //2013-06-13 added by Nancy for getting authority rule end
                    
                    if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString())
                    {//2013-07-22 added by Nancy('if...')
                        strPrintFileName = dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString();//2013-07-22 added by Nancy
                        var myCommand = new SqlCommand("NoticePrint_Update2nd_WS", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'

                        myCommand.Parameters.Add(new SqlParameter("@AutIntNo", dataTable.Rows[i]["autIntNo"].ToString()));
                        myCommand.Parameters.Add(new SqlParameter("@PrintFile", dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString()));
                        myCommand.Parameters.Add(new SqlParameter("@Status", 260));
                        myCommand.Parameters.Add(new SqlParameter("@ShowAll", "N"));
                        myCommand.Parameters.Add(new SqlParameter("@LastUser", GlobalVariates<User>.CurrentUser.UserLoginName));
                        myCommand.Parameters.Add(new SqlParameter("@ViolationCutOff", new char()));
                        myCommand.Parameters.Add(new SqlParameter("@NoOfDaysIssue", new int()));
                        myCommand.Parameters.Add("@ModifiedPrintDate", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;
                        myCommand.Parameters.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = valueArDet.Value }); //2013-06-13 added by Nancy for 4973


                        try
                        {
                            myCommand.ExecuteScalar();
                            DateTime ModifiedPrintDate = Convert.ToDateTime(myCommand.Parameters["@ModifiedPrintDate"].Value);
                            dataTable.Rows[i]["NotPrint2ndNoticeDate"] = ModifiedPrintDate;
                            PrePrintDate = ModifiedPrintDate;//2013-07-22 added by Nancy
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            myCommand.Dispose();
                        }
                    }
                    else
                    {//2013-07-22 added by Nancy
                        dataTable.Rows[i]["NotPrint2ndNoticeDate"] = PrePrintDate;
                    }
                }
            }
        }

        public override void ModifyNoticeStatus(DataTable dataTable)
        {
            string errMessage = string.Empty;
            int status2ndLoaded = 260;
            int status2ndPrinted = 270;
            NoticeDB noticeDB = new NoticeDB(CurrentConnnectionString);
            string lastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            string strPrintFileName = "";//2013-07-30 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString())
                {//2013-07-30 added by Nancy('if...')
                    strPrintFileName = dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString();//2013-07-22 added by Nancy
                    noticeDB.UpdateNoticeChargeStatus(
                        Convert.ToInt32(dataTable.Rows[i]["autIntNo"].ToString()),
                        dataTable.Rows[i]["Not2ndNoticePrintFile"].ToString(),
                        status2ndLoaded, status2ndPrinted, "SecondNotice", lastUser, ref errMessage);
                }
            }
        }

    }
}
