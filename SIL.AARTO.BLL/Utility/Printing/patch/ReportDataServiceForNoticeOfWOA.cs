﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForNoticeOfWOA : ReportDataService
    {
        public ReportDataServiceForNoticeOfWOA(string conn)
        {
            CurrentConnnectionString = conn;
        }

        // 2013-10-15, Oscar added for field name of Number of document
        public override string FieldForNoOfDocument { get { return "Count"; } }

        public override string FieldForFileName
        {
            get { return "SumNoticeOfWOAPrintFile"; }
        }

        //2014-03-10 Heidi added for fixed search by document number not working
        public override string FieldForDocumentNumber { get { return "NotTicketNo"; } }

        public override DataTable LoadPrintFiles(ReportModel model)
        {
            SqlDataAdapter sqlPrintrun = new SqlDataAdapter();
            DataTable dtPrintrun = new DataTable();

            // Create Instance of Connection and Command Object
            //Heidi 2014-08-21 added CommandTimeout for fixing time out issue.(bontq1408)
            sqlPrintrun.SelectCommand = new SqlCommand() { CommandTimeout = GetSqlCmdTimeout() };
            sqlPrintrun.SelectCommand.Connection = new SqlConnection(CurrentConnnectionString);
            sqlPrintrun.SelectCommand.CommandText = "NoticeOfWOAPrint_Files";

            // Mark the Command as a SPROC
            sqlPrintrun.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = Convert.ToInt32(model.AutIntNo);
            sqlPrintrun.SelectCommand.Parameters.Add(parameterAutIntNo);

            // 2013-04-17 add by Henry for pagination
            sqlPrintrun.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = model.PageSize;
            sqlPrintrun.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = model.PageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            sqlPrintrun.SelectCommand.Parameters.Add(paraTotalCount);

            // Execute the command and close the connection
            sqlPrintrun.Fill(dtPrintrun);
            sqlPrintrun.SelectCommand.Connection.Dispose();

            model.TotalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);

            // Return the dataset result
            return dtPrintrun;
        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("fileName");
            foreach (string f in files)
            {
                dtInput.Rows.Add(new object[] { f });
            }

            var ds = new DataSet();
            DataTable dtData = null;
            // Create Instance of Connection and Command Object

            var myConnection = new SqlConnection(CurrentConnnectionString);
            var myCommand = new SqlCommand("Report_GetNoticeOfWOA", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'

            myCommand.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
            myCommand.Parameters.Add(new SqlParameter("@fileNames", SqlDbType.Structured) { Value = dtInput });

            // Mark the Command as a SPROC  
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                if (ds.Tables.Count < 1)
                {
                    throw new Exception("Print data is null");
                }
                dtData = ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dtData;
        }

        public override void ModifyPrintDate(DataTable dataTable)
        {
            using (var myConnection = new SqlConnection(CurrentConnnectionString))//2013-08-08 updated by Nancy
            {
                myConnection.Open();
                string strPrintFileName = "";//2013-07-22 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["SumNoticeOfWOAPrintFile"].ToString())
                    {//2013-07-22 added by Nancy('if...')
                        strPrintFileName = dataTable.Rows[i]["SumNoticeOfWOAPrintFile"].ToString();//2013-07-22 added by Nancy
                        var myCommand = new SqlCommand("SetPrintedStatusOfNoticeOfWoa", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'

                        myCommand.Parameters.Add(new SqlParameter("@NoticeOfWoaPrintFileName", dataTable.Rows[i]["SumNoticeOfWOAPrintFile"].ToString()));
                        myCommand.Parameters.Add(new SqlParameter("@OnOf", true));
                        myCommand.Parameters.Add(new SqlParameter("@LastUser", GlobalVariates<User>.CurrentUser.UserLoginName));
                        myCommand.Parameters.Add(new SqlParameter("@Success", 0));

                        try
                        {
                            myCommand.ExecuteScalar();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            myCommand.Dispose();
                        }
                    }
                }
            }
        }

    }
}