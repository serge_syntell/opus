using System.Collections.Generic;
using System.Data;
using System.Drawing;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    /// <summary>
    /// Default page creator, one row print on one page.
    /// </summary>
    public class PageGenerator
    {
        /// <summary>
        /// common page creation for one page report. 
        /// </summary>
        /// <param name="cellDefinations">column position, color, and so on.</param>
        /// <param name="dtData"> datatable contains all data that need to be printed</param>
        /// <returns></returns>
        public virtual List<FreePage> CreatePages(List<CellDefination> cellDefinations
                                                  , DataTable dtData)
        {
            return null;
             
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cellDefinations"></param>
        /// <returns></returns>
        public virtual List<FreePage> CreatePagesForTest(List<CellDefination> cellDefinations
                         )
        { 
            //build datatable for test.
            //cellDefinations.
            DataTable dt = new DataTable();
            int colAmount = cellDefinations.Count;
             List<string> itemArray=new List<string>();
            foreach (CellDefination cellDefination in cellDefinations)
            {
               
                if (cellDefination is DataFieldCellDef)
                {
                    if (dt.Columns.Contains(cellDefination.DataFiled)) continue;
                    
                    dt.Columns.Add(cellDefination.DataFiled);
                    //itemArray.Add(cellDefination.DataFiled+"Location".PadRight(cellDefination.Width, '*'));  
                    itemArray.Add(cellDefination.DataFiled+"".PadRight(cellDefination.Width, '*'));  
                }   

               
               
                   
            }
            dt.Rows.Add(itemArray.ToArray());
            //send test datatable to fomal method, that'll create a test page.
            return this.CreatePages(cellDefinations, dt);
        }

        /// <summary>
        /// common function , parse cell defination to cell.
        /// 1.datafield;2.plain text; 3.page number.
        ///  jacob.
        /// </summary>
        /// <param name="dataRow"></param>
        /// <param name="cellDefination"></param>
        /// <returns></returns>
        protected virtual FreeCell GetCellByDef(DataRow dataRow, CellDefination cellDefination)
        {
            FreeCell cell = null;
            //previous step is GetColDefByXML method in FreeStylePrintEngine class.
           if(cellDefination is PlainTextCellDef)
           {
               cell = new PlainTextCell(cellDefination);
               
           }
           if (cellDefination is PageNumCellDef)
           {
               cell = new PageNumCell(cellDefination);
              
           }
           if (cellDefination is DataFieldCellDef)
           {  
               cell = new DataFieldCell
               {
                   cellStyle = cellDefination,
                   Text = dataRow[cellDefination.DataFiled].ToString()
               }; 
           }   
            
            return cell;
        }

        public SizeF GetCellContentSize(CellDefination defination)
        {
            string Str = "testcode testcode".PadRight(defination.Width,'*');
            Image image=new Bitmap(defination.Width,defination.Height);

            Graphics tmpG = Graphics.FromImage(image);
            SizeF size = tmpG.MeasureString(Str, ReportDefaultSetting.font);
            return size;
        }
    }
}