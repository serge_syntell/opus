using System.Collections.Generic;
using System.Drawing;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.BLL.Report.Model
{
    public class CellDefination : Column
    {
        public CellDefination()
        {
        }
        public StringFormat style;
        public int HeadHeight { get; set; }
        public List<CellLine> Lines=new List<CellLine>();
        //Added by Jacob 20120605 start
        //add location feature.
        public int Top { get; set; }
        public int Left { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Page { get; set; }
        public int PageWidth { get; set; }
        /// <summary>
        ///Indicate which page the cell is on
        /// Some reports, like CPA6, have more than one page.
        /// </summary>
        public int PageNum { get; set; }
        public Font CellFont { get; set; }
        public bool IsUpperCase { get; set; }
        public string DataFormat { get; set; }
        public int ColumnSpan { get; set; }
        public int[] LineCharacters { get; set; }//20130724
        public RectangleF Rect
        {
            get { return new RectangleF(Left, Top, Width, Height); }
        }

       
        //cell belong to which page
        //if cpa5 it's always 1,when cpa6 it's 1 or 2.
        //Added by Jacob 20120605 end
    }

    public class DefaultCellDefination : CellDefination
    {
        
    }

    

    public class DataFieldCellDef : CellDefination
    {
        //data field name. type. format.
    }

    public class PlainTextCellDef : CellDefination
    {
        public string PlainText2 { get; set; }
 
    }

    public class PageNumCellDef : CellDefination
    {
        public string Prefix { get; set; }
    }



}