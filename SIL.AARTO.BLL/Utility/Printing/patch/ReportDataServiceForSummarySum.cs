using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForSummarySum : ReportDataService
    {
        //public override string rptType
        //{
        //    get { return "Summary Summons"; }
        //}

        public ReportDataServiceForSummarySum(string conn)
        {
            //CurrentLogTable = "ReportHistoryForSummarySheetSummons";
            CurrentConnnectionString = conn;
        }

        // 2013-10-15, Oscar added for field name of Number of document
        public override string FieldForNoOfDocument { get { return "NoOfNotices"; } }

        public override string FieldForFileName
        {
            get { return "SumPrintFileName"; }
        }

        //2014-03-10 Heidi added for fixed search by document number not working
        public override string FieldForDocumentNumber { get { return "SummonsNo"; } }

        public override DataTable LoadPrintFiles(ReportModel model)
        {
            string StrConnetion = CurrentConnnectionString;

            DataTable SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();
                //2013-08-29 added by Henry for Filter by AR_5110 and AR_5120
                int autIntNo = Convert.ToInt32(model.AutIntNo);//Heidi 2014-08-21 added CommandTimeout for fixing time out issue.(bontq1408)
                SqlCommand selCmd = new SqlCommand("SummonsPrint_Files", SQLConn) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };
                selCmd.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
                selCmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar, 6) { Value = "SUM" });
                selCmd.Parameters.Add(new SqlParameter("@StatusLoad", SqlDbType.Int) { Value = 620 }); //2013-05-24 update by Nancy from '610' to '620'
                selCmd.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@ShowControlRegister", SqlDbType.Char, 1) { Value = "Y" }); //2013-05-24 update by Nancy from 'N' to 'Y'
                selCmd.Parameters.Add(new SqlParameter("@DateFrom", SqlDbType.SmallDateTime) { Value = model.DateFrom });
                selCmd.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.SmallDateTime) { Value = model.DateTo });

                //2013-08-29 added by Henry for Filter by AR_5110 and AR_5120
                selCmd.Parameters.Add(new SqlParameter("@AR_5110", SqlDbType.NVarChar, 1)
                {
                    Value = AuthorityRule.GetAuthorityRulesKeyValue("5110", autIntNo).Value.Trim()
                });
                selCmd.Parameters.Add(new SqlParameter("@AR_5120", SqlDbType.NVarChar, 1)
                {
                    Value = AuthorityRule.GetAuthorityRulesKeyValue("5120", autIntNo).Value.Trim()
                });

                // 2013-04-17 add by Henry for pagination
                selCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = model.PageSize;
                selCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = model.PageIndex;

                SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                paraTotalCount.Direction = ParameterDirection.Output;
                selCmd.Parameters.Add(paraTotalCount);

                SqlDataAdapter adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);

                model.TotalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return SQLTable;
            }
        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("fileName");
            foreach (string f in files)
            {
                dtInput.Rows.Add(new object[] { f });
            }

            var ds = new DataSet();
            DataTable dtData = null;
            // Create Instance of Connection and Command Object

            var myConnection = new SqlConnection(CurrentConnnectionString);
            var myCommand = new SqlCommand("SummonsPrint_Details", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-08-22 Nancy add 'Timeout'

            myCommand.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
            myCommand.Parameters.Add(new SqlParameter("@fileNames", SqlDbType.Structured) { Value = dtInput });
            myCommand.Parameters.Add(new SqlParameter("@PrintFileName", SqlDbType.VarChar) { Value = "" });
            myCommand.Parameters.Add(new SqlParameter("@Mode", SqlDbType.VarChar) { Value = "S" }); //2013-05-24 update by Jacob from 'N' to 'S'
            myCommand.Parameters.Add(new SqlParameter("@LastUser", SqlDbType.VarChar) { Value = "Report Engine" });
            myCommand.Parameters.Add(new SqlParameter("@IsCourtNumber", SqlDbType.VarChar) { Value = 0 });

            //2013-08-29 added by Henry for Filter by AR_5110 and AR_5120
            myCommand.Parameters.Add(new SqlParameter("@AR_5110", SqlDbType.NVarChar, 1)
            {
                Value = AuthorityRule.GetAuthorityRulesKeyValue("5110", autIntNo).Value.Trim()
            });
            myCommand.Parameters.Add(new SqlParameter("@AR_5120", SqlDbType.NVarChar, 1)
            {
                Value = AuthorityRule.GetAuthorityRulesKeyValue("5120", autIntNo).Value.Trim()
            });

            // Mark the Command as a SPROC  
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                if (ds.Tables.Count < 1)
                {
                    throw new Exception("Print data is null");
                }
                dtData = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //EntLibLogger.WriteLog(LogCategory.Exception, null, "SP:" + "SummonsCPA5_PrintTest" + " " + ex.Message);
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dtData;
        }

        public override void ModifyPrintDate(DataTable dataTable)
        {
            using (var myConnection = new SqlConnection(CurrentConnnectionString))//2013-08-22 updated by Nancy
            {
                myConnection.Open();//2013-08-22 updated by Nancy
                string strPrintFileName = "";//2013-08-22 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    //2013-06-03 added by Nancy for getting authority rule start
                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = this.IAutIntNo;
                    arDetails.ARCode = "2570";
                    arDetails.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
                    DefaultAuthRules authRule = new DefaultAuthRules(arDetails, CurrentConnnectionString);
                    KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();
                    //2013-06-03 added by Nancy for getting authority rule end

                    if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["SumPrintFileName"].ToString())
                    {//2013-08-22 added by Nancy('if...')
                        strPrintFileName = dataTable.Rows[i]["SumPrintFileName"].ToString();//2013-08-22 added by Nancy
                        var myCommand = new SqlCommand("NoticePrintSummonsControl_Update_WS", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-08-22 Nancy add 'Timeout'

                        myCommand.Parameters.Add(new SqlParameter("@PrintFileName", dataTable.Rows[i]["SumPrintFileName"].ToString()));
                        myCommand.Parameters.Add(new SqlParameter("@Mode", ""));
                        myCommand.Parameters.Add(new SqlParameter("@LastUser", GlobalVariates<User>.CurrentUser.UserLoginName));
                        myCommand.Parameters.Add(new SqlParameter("@IsCourtNumber", 1));
                        myCommand.Parameters.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = valueArDet.Value }); //2013-06-17 added by Nancy for 4973

                        //2013-00-03 added by Henry for Filter by AR_5110 and AR_5120
                        myCommand.Parameters.Add(new SqlParameter("@AR_5110", SqlDbType.NVarChar, 1)
                        {
                            Value = AuthorityRule.GetAuthorityRulesKeyValue("5110", base.AutIntNo).Value.Trim()
                        });
                        myCommand.Parameters.Add(new SqlParameter("@AR_5120", SqlDbType.NVarChar, 1)
                        {
                            Value = AuthorityRule.GetAuthorityRulesKeyValue("5120", base.AutIntNo).Value.Trim()
                        });
                        myCommand.Parameters.Add(new SqlParameter("@AR_5130", SqlDbType.NVarChar, 1)
                        {
                            Value = AuthorityRule.GetAuthorityRulesKeyValue("5130", base.AutIntNo).Value.Trim()
                        });

                        try
                        {
                            myCommand.ExecuteScalar();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            myCommand.Dispose();
                        }
                    }
                }
            }
        }
    }
}