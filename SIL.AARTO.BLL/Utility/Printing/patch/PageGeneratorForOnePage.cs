using System.Collections.Generic;
using System.Data;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    /// <summary>
    /// Create one page for one data row.
    /// CPA5 and WOA.
    /// </summary>
    public class PageGeneratorForOnePage : PageGenerator
    {
        public override List<FreePage> CreatePages(List<CellDefination> cellDefinations
                                                   , DataTable dtData)
        {
            var pages = new List<FreePage>();

            foreach (DataRow dataRow in dtData.Rows)
            {
                var page = new FreePage();
                foreach (CellDefination cellDefination in cellDefinations)
                {
                    var cell = GetCellByDef(dataRow, cellDefination);

                    page.AddToList(cell);
                }
                page.MainDataRow = dataRow;
                pages.Add(page);
            }
            foreach (var freePage in pages)
            {
                freePage.PageNum = pages.IndexOf(freePage)+1;
            }
            return pages;
        }
    }
}