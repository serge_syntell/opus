using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;
using System.Diagnostics;
using SIL.AARTO.BLL.PostalManagement;
using System.Transactions;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceFor1stNoticeHWO : ReportDataService
    {
        //public override string rptType
        //{
        //    get { return "1st Notice HWO"; }
        //}

        // 2013-10-15, Oscar added for field name of Number of document
        public override string FieldForNoOfDocument { get { return "NoOfNotices"; } }

        public override string FieldForFileName
        {
            get { return "NotCiprusPrintReqName"; }
        }

        //2014-03-10 Heidi added for fixed search by document number not working
        public override string FieldForDocumentNumber { get { return "NotTicketNo"; } }

        public ReportDataServiceFor1stNoticeHWO(string conn)
        {
            //CurrentLogTable = "ReportHistoryForFirstNoticeHWO";
            CurrentConnnectionString = conn;
        }

        public override DataTable LoadPrintFiles(ReportModel model)
        {
            string StrConnetion = CurrentConnnectionString;

            DataTable SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();//Heidi 2014-08-21 added CommandTimeout for fixing time out issue.(bontq1408)
                SqlCommand selCmd = new SqlCommand("FirstNotice_FileNameList", SQLConn) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };
                selCmd.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = Convert.ToInt32(model.AutIntNo) });
                selCmd.Parameters.Add(new SqlParameter("@Type", SqlDbType.VarChar, 6) { Value = "CAM" });
                selCmd.Parameters.Add(new SqlParameter("@StatusLoad", SqlDbType.Int) { Value = 10 });
                selCmd.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@ShowControlRegister", SqlDbType.Char, 1) { Value = "N" });
                selCmd.Parameters.Add(new SqlParameter("@DateFrom", SqlDbType.SmallDateTime) { Value = model.DateFrom });
                selCmd.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.SmallDateTime) { Value = model.DateTo });
                selCmd.Parameters.Add(new SqlParameter("@IsVideo", SqlDbType.Bit) { Value = 0 });

                // 2013-04-16 add by Henry for pagination
                selCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = model.PageSize;
                selCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = model.PageIndex;

                SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
                paraTotalCount.Direction = ParameterDirection.Output;
                selCmd.Parameters.Add(paraTotalCount);

                SqlDataAdapter adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);

                model.TotalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
                return SQLTable;
            }
        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {
            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("fileName");
            foreach (string f in files)
            {
                dtInput.Rows.Add(new object[] { f });
            }

            var ds = new DataSet();
            DataTable dtData = null;
            // Create Instance of Connection and Command Object

            var myConnection = new SqlConnection(CurrentConnnectionString);
            //Heidi 2014-08-21 added CommandTimeout for fixing time out issue.(bontq1408)
            var myCommand = new SqlCommand("NoticePrint_FirstNotice", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };

            myCommand.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
            myCommand.Parameters.Add(new SqlParameter("@PrintFile", SqlDbType.VarChar, 50) { Value = "" });
            myCommand.Parameters.Add(new SqlParameter("@NotIntNo", SqlDbType.Int) { Value = 0 });//
            myCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int){Value = 0 });
            myCommand.Parameters.Add(new SqlParameter("@ShowAll", SqlDbType.Char, 1) {Value = "N" });
            myCommand.Parameters.Add(new SqlParameter("@Option", SqlDbType.Int){Value = 2});
            myCommand.Parameters.Add(new SqlParameter("@fileNames", SqlDbType.Structured) { Value = dtInput });
            myCommand.Parameters.Add(new SqlParameter("@IsVideo", SqlDbType.Bit) { Value = 0 });

            // Mark the Command as a SPROC  
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                if (ds.Tables.Count < 1)
                {
                    throw new Exception("Print data is null");
                }
                dtData = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //EntLibLogger.WriteLog(LogCategory.Exception, null, "SP:" + "SummonsCPA5_PrintTest" + " " + ex.Message);
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dtData;
        }

        public override void ModifyPrintDate(DataTable dataTable)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                var myConnection = new SqlConnection(CurrentConnnectionString);
                //Heidi 2014-08-21 added CommandTimeout for fixing time out issue.(bontq1408)
                var myCommand = new SqlCommand("NoticePrint_Update1st_WS", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };

                int ExpiredRows = 0;
                myCommand.Parameters.Add(new SqlParameter("@AutIntNo", dataTable.Rows[i]["autIntNo"].ToString()));
                myCommand.Parameters.Add(new SqlParameter("@PrintFile", SqlDbType.VarChar, 50) { Value = dataTable.Rows[i]["NotCiprusPrintReqName"].ToString() });
                myCommand.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int) { Value = Convert.ToInt32(dataTable.Rows[i]["NoticeStatus"].ToString()) });
                myCommand.Parameters.Add(new SqlParameter("@ShowAll", "N"));
                myCommand.Parameters.Add(new SqlParameter("@LastUser", GlobalVariates<User>.CurrentUser.UserLoginName));
                myCommand.Parameters.Add(new SqlParameter("@ViolationCutOff", "N"));
                myCommand.Parameters.Add(new SqlParameter("@NoOfDaysIssue", SqlDbType.Int) { Value = 0 });
                myCommand.Parameters.Add(new SqlParameter("@ExpiredRows", ExpiredRows) { Direction = ParameterDirection.InputOutput });
                myCommand.Parameters.Add("@ModifiedPrintDate", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                try
                {
                    myConnection.Open();
                    myCommand.ExecuteScalar();
                    DateTime ModifiedPrintDate =Convert.ToDateTime( myCommand.Parameters["@ModifiedPrintDate"].Value);
                    dataTable.Rows[i]["NotPrint1stNoticeDate"] = ModifiedPrintDate;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    myConnection.Close();
                    myCommand.Dispose();
                }
            }
        }

        public override void ModifyNoticeStatus(DataTable dataTable)
        {
            string errMessage = string.Empty;
            NoticeDB noticeDB = new NoticeDB(CurrentConnnectionString);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    string lastUser = Process.GetCurrentProcess().ProcessName.Replace(".vshost", string.Empty).Replace("SIL.AARTO.", string.Empty);
                    int row = noticeDB.UpdateNoticeChargeStatus(
                        Convert.ToInt32(dataTable.Rows[i]["autIntNo"].ToString()),
                        dataTable.Rows[i]["NotCiprusPrintReqName"].ToString(),
                        Convert.ToInt32(dataTable.Rows[i]["NoticeStatus"].ToString()), 250, "FirstNotice", lastUser, ref errMessage);

                    string printFileName = dataTable.Rows[i]["NotCiprusPrintReqName"].ToString();
                    int autIntNo = Convert.ToInt32(dataTable.Rows[i]["autIntNo"].ToString());

                    // 2013-10-21, Oscar added "posted" to separate "row"
                    var posted = 1;

                    if (row > 0)
                    {
                        AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                        arDetails.AutIntNo = autIntNo;
                        arDetails.ARCode = "4200";
                        arDetails.LastUser = lastUser;
                        DefaultAuthRules ar = new DefaultAuthRules(arDetails, CurrentConnnectionString);
                        bool isPostDate = ar.SetDefaultAuthRule().Value.Equals("Y");

                        if (isPostDate)
                        {
                            PostDateNotice postDate = new PostDateNotice(CurrentConnnectionString);
                            //row = postDate.SetNoticePostedDate(printFileName, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), lastUser, ref errMessage);
                            // 2013-10-21, Oscar changed
                            posted = postDate.SetNoticePostedDate(printFileName, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), lastUser, ref errMessage);
                        }
                    }
                    else
                    {
                        errMessage = string.Format("Error {0} - unable to set print status. {1}", row.ToString(), errMessage);  // 2013-10-21, added {1}-errMessage
                    }

                    //if (row > 0)
                    // 2013-10-21, Oscar changed, we have to commit when "row = 0"
                    // 2013-10-21, Oscar added -999 checking for type = 'FirstNotice' Or 'NoAOG'
                    if ((row >= 0 || row == -999) && posted > 0)
                    {
                        scope.Complete();
                    }
                    else
                    {
                        throw new Exception(errMessage);
                    }
                }
            }
        }
    }
}