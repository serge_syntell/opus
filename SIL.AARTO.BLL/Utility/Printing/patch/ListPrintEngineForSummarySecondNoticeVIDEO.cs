﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using System.Data;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.BLL.Report;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ListPrintEngineForSummarySecondNoticeVIDEO : FreeStylePrintEngine
    {
        public ListPrintEngineForSummarySecondNoticeVIDEO(PageGenerator gen)
            : base(gen)
        {
        }

        //public override string FieldForFileName
        //{
        //    get { return "Not2ndNoticePrintFile"; }
        //}

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "NotTicketNo";
        //    }
        //}

        public ListPrintEngineForSummarySecondNoticeVIDEO()
        {
            CurrentReportDataService = new ReportDataServiceForSummary2ndNoticeVIDEO("");
        }
        public ListPrintEngineForSummarySecondNoticeVIDEO(string conns)
        {
            DBConnectionString = conns;
            CurrentReportDataService = new ReportDataServiceForSummary2ndNoticeVIDEO(conns);
        }
        public override void PrintTestPage()
        {
            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)ReportConfigCodeList.SecondNoticeVIDEOSummary).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            listPrint.CreateEngineWithTestData(rc.RcIntNo);
        }

        public override void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {
            // 2013-10-15, Oscar removed - adding transaction for saving history and modifying print date and status
            //if (addHistory)
            //{
            //    CurrentReportDataService.SaveReportDataToHistory(dtData);
            //}
            if (onlyHistory)
            {
                return;
            }

            ListPrintEngine listPrint = new ListPrintEngine(); listPrint.isFreeStyle = true; //Jacob
            listPrint.connStr = connStr;

            ReportConfigService rcServ = new ReportConfigService();
            TList<ReportConfig> rcList = rcServ.Find("ReportCode='" + ((int)ReportConfigCodeList.SecondNoticeVIDEOSummary).ToString() + "'");
            ReportConfig rc;
            if (rcList.Count > 0)
            {
                rc = rcList[0];
            }
            else
            {
                throw new Exception("Can't find control sheet config data");
            }

            ReportModel printModel = GetPrintTemplate(rc.RcIntNo);
            listPrint.CreatePreviewEngine(printModel, dtData);
            rc.AutomaticGenerateLastDatetime = DateTime.Now;
            // 2013-07-16 add by Henry for LastUser
            rc.LastUser = CurrentReportDataService.LastUser;
            ReportManager.UpdateReport(rc);
        }

    }
}
