using System;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.DAL.Entities;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace SIL.AARTO.BLL.Utility.Printing
{
    /// <summary>
    /// For specific reports, supports to
    /// 1. list and print new files and completed files, 
    /// 2. list and reprint db history data.
    /// </summary>
    public class FreeStylePrintEngineForFileSystem : FreeStylePrintEngine
    {
        public FreeStylePrintEngineForFileSystem(PageGenerator gen)
            : base(gen)
        {
        }
        public FreeStylePrintEngineForFileSystem()
            : base()
        {
        }

        /// <summary>
        /// override to use new print method from Jake's spike, directly send data to LPT port.
        /// </summary>
        /// <param name="dtData">each row is for one page</param>
        ///  Added by Jacob 20130828.
        public override void PrintReportWithData(DataTable dtData, bool addHistory = true, bool onlyHistory = true)
        {
            //history function
            //first, save to history
            //if (addHistory)
            //{
            //    CurrentReportDataService.SaveReportDataToHistory(dtData);//
            //}
            //if no need to print, exit.
            if (onlyHistory)
            {
                return;
            }

            /* Jacob comment out 20130904.
             * Jake's api to build string and print to LPT port.
             * you can uncomment this portion to switch back to that way.
            ReportBuilder rb = new ReportBuilder(ConfigurationManager.AppSettings["LPTName"].ToString(), (int)ReportConfigCodeList.CPA5);
            rb.Print(dtData);
            */

            //Jacob added 20130904 start 
            //Reuse configuration to build string 

            //special page generator decides how to create pages for print.
            // different form mode will have different printing behavior.
            // 
            //when file mode, load file and send to LPT port.
            if (CurrentMode is NewFilesMode || CurrentMode is CompletedFilesMode)
            {
                var report = new CPA5Report(this.RegisterLPTPort());
                foreach (DataRow dr in dtData.Rows)
                {
                    string file = dr[this.FieldForFileName].ToString();
                    using (TextReader rt = new StreamReader(file))
                    {
                        string s = rt.ReadToEnd();
                        //send to LPT;
                        report.PrintToLPT(s); 
                        Thread.Sleep(200);//Heidi 2013-10-08 changed for fixed print error
                    }
                    // Process.Start(file);

                }
                //2013-12-26 added by Nancy start
                var dsFileService = CurrentReportDataService as ReportDataForFileService;
                //2014-01-15 changed for Repeat build Completed folder problem
                if (dsFileService != null && CurrentMode is NewFilesMode) { dsFileService.MoveFilesRepotToComplete(dtData); }
                //2013-12-26 added by Nancy end
            }
            // when history data mode, load db data and build string then send to LPT port.
            else if (CurrentMode is ReportHistoryMode)
            {
                List<FreePage> pages = CurrentPageGenerator.CreatePages(DataColunms, dtData);
                PrintPages(pages);
            }

        }

        //
        //Jacob added 20130904 end 
        //ReportBuilder rb = new ReportBuilder(ConfigurationManager.AppSettings["LPTName"].ToString(), (int)ReportConfigCodeList.CPA5);
        //rb.Print(dtData);
        public override void PrintPages(List<FreePage> pages)
        {

            _printObjects.Clear();

            //Modified by Brian 2013-10-18
            //var report = new CPA5Report(ConfigurationManager.AppSettings["LPTName"].ToString());
            var report = new CPA5Report(this.RegisterLPTPort());
            StringBuilder sb = new StringBuilder();
            int formLength = Convert.ToInt32(12);
            //var commandLine = "\x1B\x43\x00" + (char)(formLength);
            sb.Append("\x1B\x43\x00" + (char)(formLength));
            //Character Pitch 12 cpi
            // commandLine += "\x1B\x3a";
            sb.Append("\x1B\x3a");
            //commandLine += "\x1B\x41" + (char)12; //Heidi 2013-10-08 changed for fixed Print the offset
            sb.Append("\x1B\x41" + (char)12);

            foreach (FreePage page in pages)
            {
                var pieces = CombineToLines(page);
                pieces = ((string)pieces).TrimEnd(new[] { '\r', '\n' });
                //commandLine += pieces;
                sb.Append(pieces);
                //commandLine += "\x0c";
                sb.Append("\x0c");

            }

            //string inputReportFilesFolder = @"\\192.168.1.250\Company Shared Folders\WOA Register";
            //string inputPrintFileName = "mockprinter"+string.Format("{0:yyyyMMddHHmmss}", DateTime.Now);
            //if (!Directory.Exists(inputReportFilesFolder))
            //    Directory.CreateDirectory(inputReportFilesFolder);


            //string exportFullPath = inputReportFilesFolder + "\\" + inputPrintFileName + ".prn";


            ////write the exported files
            //using (FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write))
            //{
            //    using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
            //    {
            //        sw.Write(sb.ToString());
            //    }
            //}
            // report.PrintToLPT(commandLine);//2014-01-16 Heidi fixed for if CurrentMode is ReportHistoryMode,can't print (5101)
            report.PrintToLPT(sb.ToString());//2014-01-16 Heidi fixed for if CurrentMode is ReportHistoryMode,can't print (5101)
            Thread.Sleep(500);//Heidi 2013-10-08 changed for fixed print error

        }

        public override void PrintPages(List<FreePage> pages, ListPrintEngine listEngine)
        {

            _printObjects.Clear();
            this.ShowGridLine = listEngine.ShowGridLine;
            //Modified by Brian 2013-10-18
            //var report = new CPA5Report(ConfigurationManager.AppSettings["LPTName"].ToString());
            var report = new CPA5Report(this.RegisterLPTPort());
            StringBuilder sb = new StringBuilder();
            int formLength = Convert.ToInt32(11);//2015-01-20 Heidi changed for list report should be 11 (bontq1798)
            //var commandLine = "\x1B\x43\x00" + (char)(formLength);
            sb.Append("\x1B\x43\x00" + (char)(formLength));
            //Character Pitch 12 cpi
            // commandLine += "\x1B\x3a";
            sb.Append("\x1B\x3a");
            //commandLine += "\x1B\x41" + (char)12; //Heidi 2013-10-08 changed for fixed Print the offset
            sb.Append("\x1B\x41" + (char)12);

            foreach (FreePage page in pages)
            {
                var pieces = CombineToLines(page);
                pieces = ((string)pieces).TrimEnd(new[] { '\r', '\n' });
                //commandLine += pieces;
                sb.Append(pieces);
                //commandLine += "\x0c";
                sb.Append("\x0c");

            }

            //string inputReportFilesFolder = @"\\192.168.1.250\Company Shared Folders\WOA Register";
            //string inputPrintFileName = "mockprinter" + string.Format("{0:yyyyMMddHHmmss}", DateTime.Now);
            //if (!Directory.Exists(inputReportFilesFolder))
            //    Directory.CreateDirectory(inputReportFilesFolder);


            //string exportFullPath = inputReportFilesFolder + "\\" + inputPrintFileName + ".prn";


            ////write the exported files
            //using (FileStream fs = new FileStream(exportFullPath, FileMode.Create, FileAccess.Write))
            //{
            //    using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.ASCII))
            //    {
            //        sw.Write(sb.ToString());
            //    }
            //}
            // report.PrintToLPT(commandLine);//2014-01-16 Heidi fixed for if CurrentMode is ReportHistoryMode,can't print (5101)
            report.PrintToLPT(sb.ToString());//2014-01-16 Heidi fixed for if CurrentMode is ReportHistoryMode,can't print (5101)
            Thread.Sleep(500);//Heidi 2013-10-08 changed for fixed print error

        }



    }
    public class FreeStylePrintEngineForCpa5DirectPrinting : FreeStylePrintEngineForFileSystem
    {
        /// <summary>
        /// Override for CPA5 direct printing, change data service when user changing mode 
        /// don't forget initializing data service.
        /// </summary>
        public override ReportFormMode CurrentMode
        {
            get
            { return base.CurrentMode; }
            set
            {
                base.CurrentMode = value;
                if (value is ReportNewMode || value is ReportHistoryMode)
                {
                    CurrentReportDataService = new ReportDataServiceForCPA5(DBConnectionString);
                    CurrentReportDataService.InitializeReportConfig(this.PrintModel.RcIntNo);
                }
                else if (value is NewFilesMode)
                {
                    CurrentReportDataService = new ReportDataServiceForCPA5NewFileDirectPrinting();
                    CurrentReportDataService.RCIntNo = this.PrintModel.RcIntNo; //for getting LPT port
                    //CurrentReportDataService.InitializeReportConfig(this.PrintModel.RcIntNo);
                }
                else if (value is CompletedFilesMode)
                {
                    CurrentReportDataService = new ReportDataServiceForCPA5CompletedFileDirectPrinting();
                    CurrentReportDataService.RCIntNo = this.PrintModel.RcIntNo;//for getting LPT port
                    //CurrentReportDataService.InitializeReportConfig(this.PrintModel.RcIntNo);
                }
            }
        }

        //public override string FieldForFileName
        //{
        //    get { return "SumPrintFileName"; }
        //}

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "SummonsNo";
        //    }
        //}

        public FreeStylePrintEngineForCpa5DirectPrinting(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            //Edited by Jacob 20131217 start
            // to use new data service.
            //CurrentReportDataService = new ReportDataServiceForCPA5(conns); 
            CurrentReportDataService = new ReportDataServiceForCPA5NewFileDirectPrinting();
            //Edited by Jacob 20131217 end
            //connection string need to be set before call.
        }

    }
}