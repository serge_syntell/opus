using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;
using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class ReportDataServiceForWOA : ReportDataService
    {
        //public override string rptType
        //{
        //    get { return "WOA"; }
        //}

        public ReportDataServiceForWOA(string conn)
        {
            //CurrentLogTable = "ReportHistoryForWOA";
            CurrentConnnectionString = conn;
        }

        // 2013-10-15, Oscar added for field name of Number of document
        public override string FieldForNoOfDocument { get { return "NoOfWarrants"; } }

        public override string FieldForFileName
        {
            get { return "WOAPrintFileName"; }
        }

        //2014-03-10 Heidi added for fixed search by document number not working
        public override string FieldForDocumentNumber { get { return "WOANumber"; } }

        public override DataTable LoadPrintFiles(ReportModel model)
        {
            SqlDataAdapter sqlPrintrun = new SqlDataAdapter();
            DataTable dtPrintrun = new DataTable();

            // Create Instance of Connection and Command Object
            //Heidi 2014-08-21 added CommandTimeout for fixing time out issue.(bontq1408)
            sqlPrintrun.SelectCommand = new SqlCommand() { CommandTimeout = GetSqlCmdTimeout() };
            sqlPrintrun.SelectCommand.Connection = new SqlConnection(CurrentConnnectionString);
            sqlPrintrun.SelectCommand.CommandText = "WOAPrint_Files";

            // Mark the Command as a SPROC
            sqlPrintrun.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterAutIntNo = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            parameterAutIntNo.Value = Convert.ToInt32(model.AutIntNo);
            sqlPrintrun.SelectCommand.Parameters.Add(parameterAutIntNo);

            SqlParameter parameterShowAll = new SqlParameter("@ShowAll", SqlDbType.Char, 1);
            parameterShowAll.Value = "N";
            sqlPrintrun.SelectCommand.Parameters.Add(parameterShowAll);

            DateRulesDetails rule = new DateRulesDetails();
            rule.DtRStartDate = "WOALoadDate";
            rule.DtREndDate = "WOAPrintDate";
            rule.LastUser = "Report Engine";
            rule.AutIntNo = Convert.ToInt32(model.AutIntNo);
            DefaultDateRules dateRule = new DefaultDateRules(rule, CurrentConnnectionString);
            int noDaysForNoticeExpiry = dateRule.SetDefaultDateRule();

            //Insert the 14 days rule
            SqlParameter parameterNoOfDaysBeforeWOA = new SqlParameter("@NoOfDaysBeforeWOA", SqlDbType.Int, 4);
            parameterNoOfDaysBeforeWOA.Value = noDaysForNoticeExpiry;
            sqlPrintrun.SelectCommand.Parameters.Add(parameterNoOfDaysBeforeWOA);

            // 2013-04-17 add by Henry for pagination
            sqlPrintrun.SelectCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = model.PageSize;
            sqlPrintrun.SelectCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = model.PageIndex;

            SqlParameter paraTotalCount = new SqlParameter("@TotalCount", SqlDbType.Int);
            paraTotalCount.Direction = ParameterDirection.Output;
            sqlPrintrun.SelectCommand.Parameters.Add(paraTotalCount);

            // Execute the command and close the connection
            sqlPrintrun.Fill(dtPrintrun);
            sqlPrintrun.SelectCommand.Connection.Dispose();

            model.TotalCount = (int)(paraTotalCount.Value == DBNull.Value ? 0 : paraTotalCount.Value);
            // Return the dataset result
            return dtPrintrun;
        }

        public override DataTable LoadReportDataByPrintFiles(List<string> files, int autIntNo)
        {
            //Jerry 2014-05-13 get 6210 authority rule
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = this.IAutIntNo;
            arDetails.ARCode = "6210";
            string lastUser = "";
            if (GlobalVariates<User>.CurrentUser != null)
            {
                lastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            }
            else
            {
                lastUser = this.LastUser;
            }
            arDetails.LastUser = lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(arDetails, CurrentConnnectionString);
            KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();

            DataTable dtInput = new DataTable();
            dtInput.Columns.Add("fileName");
            foreach (string f in files)
            {
                dtInput.Rows.Add(new object[] { f });
            }

            var ds = new DataSet();
            DataTable dtData = null;
            // Create Instance of Connection and Command Object

            var myConnection = new SqlConnection(CurrentConnnectionString);
            var myCommand = new SqlCommand("WOAPrint_Details", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'

            myCommand.Parameters.Add(new SqlParameter("@AutIntNo", SqlDbType.Int) { Value = autIntNo });
            myCommand.Parameters.Add(new SqlParameter("@PrintFileName", SqlDbType.VarChar) { Value = "" });
            myCommand.Parameters.Add(new SqlParameter("@fileNames", SqlDbType.Structured) { Value = dtInput });
            myCommand.Parameters.Add(new SqlParameter("@AR6210", SqlDbType.Char) { Value = valueArDet.Value });

            // Mark the Command as a SPROC  
            var sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
                if (ds.Tables.Count < 1)
                {
                    throw new Exception("Print data is null");
                }
                dtData = ds.Tables[0];
            }
            //catch (Exception ex)
            //{
            //    //EntLibLogger.WriteLog(LogCategory.Exception, null, "SP:" + "SummonsCPA5_PrintTest" + " " + ex.Message);
            //    throw ex;
            //}
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }

            return dtData;
        }

        public override void ModifyPrintDate(DataTable dataTable)
        {
            //2013-06-17 added by Heidi for getting authority rule start
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo =this.IAutIntNo;
            arDetails.ARCode = "2570";
            //arDetails.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            string lastUser = "";
            //2013-12-27 Heidi updated for Service PrintToFileService(5101)
            //add if..else for Service PrintToFileService
            if (GlobalVariates<User>.CurrentUser != null)
            {
                lastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            }
            else
            {
                lastUser = this.LastUser;
            }
            arDetails.LastUser = lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(arDetails, CurrentConnnectionString);
            KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();
            //2013-06-17 added by Heidi for getting authority rule end
            
            using (var myConnection = new SqlConnection(CurrentConnnectionString))//2013-08-08 updated by Nancy
            {
                myConnection.Open();//2013-07-22 updated by Nancy
                string strPrintFileName = "";//2013-07-22 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
                DateTime PrePrintDate = DateTime.MinValue;//2013-07-22 added by Nancy
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["WOAPrintFileName"].ToString())
                    {//2013-07-22 added by Nancy('if...')
                        strPrintFileName = dataTable.Rows[i]["WOAPrintFileName"].ToString();//2013-07-22 added by Nancy
                        var myCommand = new SqlCommand("WOAPrintUpdate_WS", myConnection) { CommandType = CommandType.StoredProcedure, CommandTimeout = GetSqlCmdTimeout() };//2013-07-26 Nancy add 'Timeout'

                        myCommand.Parameters.Add(new SqlParameter("@PrintFileName", dataTable.Rows[i]["WOAPrintFileName"].ToString()));
                myCommand.Parameters.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = valueArDet.Value }); //2013-06-17 added by Heidi for 4973
                        myCommand.Parameters.Add("@PrintDate", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;
                myCommand.Parameters.Add("@LastUser", base.LastUser);   // 2013-07-23 add by Henry

                        try
                        {
                            myCommand.ExecuteScalar();
                            DateTime ModifiedPrintDate = Convert.ToDateTime(myCommand.Parameters["@PrintDate"].Value);
                            dataTable.Rows[i]["WOAIssueDate"] = ModifiedPrintDate;
                            PrePrintDate = ModifiedPrintDate;//2013-07-22 added by Nancy
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            myCommand.Dispose();
                        }
                    }
                    else
                    {//2013-07-22 added by Nancy
                        dataTable.Rows[i]["WOAIssueDate"] = PrePrintDate;
                    }
                }
            }
        }

        public override void ModifyNoticeStatus(DataTable dataTable)
        {
            //2013-06-17 added by Heidi for getting authority rule start
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = this.IAutIntNo;
            arDetails.ARCode = "2570";
            //arDetails.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            string lastUser = "";
            //2013-12-27 Heidi updated for Service PrintToFileService(5101)
            //add if..else for Service PrintToFileService
            if (GlobalVariates<User>.CurrentUser != null)
            {
                lastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            }
            else
            {
                lastUser = this.LastUser;
            }
            arDetails.LastUser = lastUser;
            DefaultAuthRules authRule = new DefaultAuthRules(arDetails, CurrentConnnectionString);
            KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();
            //2013-06-17 added by Heidi for getting authority rule end
            
            //Jake 2015-03-09 move it out from loop function
            string strPrintFileName = "";//2013-07-30 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)

            Stalberg.TMS.WOADB printStatus = new Stalberg.TMS.WOADB(CurrentConnnectionString);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                //string strPrintFileName = "";//2013-07-30 added by Nancy(Avoid repeat modifing date for the same one PrintFileName)
                if (string.IsNullOrEmpty(strPrintFileName) || strPrintFileName != dataTable.Rows[i]["WOAPrintFileName"].ToString())
                {//2013-07-30 added by Nancy('if...')
                    strPrintFileName = dataTable.Rows[i]["WOAPrintFileName"].ToString();//2013-07-30 added by Nancy
                    printStatus.UpdateWOAPrintStatus(dataTable.Rows[i]["WOAPrintFileName"].ToString(), base.LastUser, valueArDet.Value);
                }
            }            
        }

    }

    //2013-12-25 Heidi added for WOA Direct Printing (5101)
    public class ReportDataServiceForWOACompletedFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\Completed\WOA"; }
        }

        public override string FieldForFileName
        {
            get { return "WOAPrintFileName"; }
        }

        //must override and make it empty to avoid moving files, as completed files can't move.
        public override void SaveReportDataToHistory(DataTable dataTable)
        {

        }
    }

    //2013-12-25 Heidi added for WOA Direct Printing (5101)
    public class ReportDataServiceForWOANewFileDirectPrinting : ReportDataForFileService
    {  //part of the RootFolder
        protected override string Subfolder
        {
            get { return @"\WOA"; }
        }

        public override string FieldForFileName
        {
            get { return "WOAPrintFileName"; }
        }


    }
}