using System;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Report.Model;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class FreeStylePrintEngineFor1stNoticeHWO : FreeStylePrintEngine
    {
        public FreeStylePrintEngineFor1stNoticeHWO(PageGenerator gen)
            : base(gen)
        {
        }
        //public override string FieldForFileName
        //{
        //    get { return "NotCiprusPrintReqName"; }
        //}

        //Add by Brian 2013-10-11
        //2014-03-10 Heidi changed for fixed search by document number not working
        //public override string DocumentNumberFileName
        //{
        //    get
        //    {
        //        return "NotTicketNo";
        //    }
        //}

        public FreeStylePrintEngineFor1stNoticeHWO()
            :this("")
        {
           
        }

        public FreeStylePrintEngineFor1stNoticeHWO(string conns)
        {
            this.DBConnectionString = conns;
            CurrentPageGenerator = new PageGeneratorForOnePage();
            CurrentReportDataService = new ReportDataServiceFor1stNoticeHWO(conns);
        }
    }
}