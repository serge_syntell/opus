﻿using System;
using SIL.AARTO.BLL.Utility.Printing.ListReportDataService;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class EngineFactory
    {
        public static ListPrintEngine GetByReportConfig(ReportConfig rc)
        {
            return GetByReportConfig(Convert.ToInt32(rc.ReportCode));
        }

        public static ListPrintEngine GetByReportConfig(int reportCode)
        {
            if (reportCode == (int)ReportConfigCodeList.SummonsBookedOutOnCasesNowFinalised)//2014-10-11 Heidi changed for changing report name(5322)
                return new ReportDataServiceForSummonsBookedOut();
            else if (reportCode == (int)ReportConfigCodeList.CriminalCaseRegister)
                return new ReportDataServiceForCriminalCaseRegister();
            else if (reportCode == (int)ReportConfigCodeList.NonSummonsRegistrations)
                return new ReportDataServiceForNonSummonsRegistrations();
            else if (reportCode == (int)ReportConfigCodeList.AdmissionOfGuiltRegister)//2014-08-25 Heidi remove 'report' from reportName field(5364)
                return new ListPrintEngineForNo5();
            //2013-03-14 added by Nancy for 4574 start
            else if (reportCode == (int)ReportConfigCodeList.SecondAppearanceCCR)
                return new ReportDataServiceForSecondCriminalCaseRegister();
            //2013-03-14 added by Nancy for 4574 end
            // Oscar 2013-04-01 added
            else if (reportCode == (int)ReportConfigCodeList.OfficerErrors)//2014-08-25 Heidi remove 'report' from reportName field(5364)
                return new ListPrintEngineForReportOfficeErrors();

            // Oscar 2013-04-07 added
            else if (reportCode==(int)ReportConfigCodeList.SummaryOfPaymentsByThirdParty)
                return new ListPrintEngineForSummaryOfPaymentsByThirdParty();

            else
                return new ListPrintEngine();
        }
    }
}