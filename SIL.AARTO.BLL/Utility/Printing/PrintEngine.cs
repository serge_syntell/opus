﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.QueueLibrary;
using System.Drawing;
using System.Drawing.Printing;
using System.Text.RegularExpressions;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data.SqlClient;
using System.Collections.Specialized;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.Report.Model;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.Xml;
using System.Reflection;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public class PrintEngine : PrintDocument
    {
        //20131217 added by Jacob
        /// <summary>
        /// Need overriding.
        /// Switch data service for one specific engine, based on different operation mode
        /// e.g.  getting new data/ getting history data/ getting new files/ getting completed files.
        /// for a specific report each mode has its own data service class.
        /// </summary>
        public virtual ReportFormMode CurrentMode
        { get; set; }//new, data history, list history/file history.
        protected bool IsExportSetting = false;
        /// <summary>
        /// append pagesettings to file "PageSettings.log", for test only.
        /// </summary>
        /// <param name="settings"></param>
        protected void ExportPageSettings(PageSettings settings, ReportModel printModel = null)
        {
            if (!IsExportSetting) return;
            using (TextWriter fs = new StreamWriter("PageSettings.log", true))
            {
                StringBuilder sb = new StringBuilder();
                TextWriter tw = new StringWriter(sb);
                var x = new System.Xml.Serialization.XmlSerializer(settings.GetType());
                x.Serialize(tw, settings);

                sb.AppendLine("Print time:" + DateTime.Now.ToString());
                sb.AppendLine("Printable Area:" + settings.PrintableArea.ToString());
                if (printModel != null)
                {
                    sb.AppendLine(string.Format("ReportCode:{0}, ReportName:{1}", printModel.ReportCode,
                                                printModel.ReportName));
                }
                fs.WriteLine(sb.ToString());
            }
        }
        public bool ShowGridLine = false; //jacob added 20120821.
        public string LineStyle = "Dash";

        protected ArrayList _printObjects = new ArrayList();
        public Font PrintFont = ReportDefaultSetting.font;
        public Brush PrintBrush = Brushes.Black;
        public XmlDocument printTemplate;
        public PrintElement Header = new PrintElement(null);
        public PrintElement DataTableHeader = new PrintElement(null);
        protected ArrayList _printElements;
        public List<CellDefination> DataColunms = new List<CellDefination>();
        protected int _printIndex = 0;
        protected int _pageNum = 0;
        public int _columnNum = 0;
        public int _columnWidth = 0;
        public string connStr;

        public WriteErrorMessageHandler WriteErrorMessage { get; set; }

        // Oscar 2013-04-02 added
        public WriteLogHandler WriteLog { get; set; }
        // level 0=info; 1=warning; 2=error
        public void Logger(int level, bool flush, string message, params object[] formatArgs)
        {
            if (WriteLog != null)
                WriteLog(level, string.Format(message, formatArgs), flush);
        }

        public virtual String ReplaceTokens(String buf)
        {
            if (!string.IsNullOrEmpty(buf))
                buf = buf.Replace("[pagenum]", _pageNum.ToString());
            return buf;
        }

        public PrintEngine()
        {
            this.IsExportSetting = false;
            string s = ConfigurationManager.AppSettings["ExportPageSetting"];
            if (!bool.TryParse(s, out IsExportSetting))
            {
                IsExportSetting = false;
            }
        }

        public void AddPrintObject(IPrintable printObject)
        {
            _printObjects.Add(printObject);
        }

        public virtual void CreateEngine(int rcIntNo)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static SizeF GetCellContentSize(string str)
        {
            return GetCellContentSize(str, ReportDefaultSetting.font);

        }
        public static SizeF GetCellContentSizeWithReplace(string str)
        {
            return GetCellContentSize(str.Replace("\\r\\n", "\r\n"), ReportDefaultSetting.font);

        }
        public static SizeF GetCellContentSize(string str, Font font)
        {
            return GetCellContentSize(str,
                ReportDefaultSetting.font,
                ReportDefaultSetting.BodyWidth,
                ReportDefaultSetting.BodyHeight);

        }

        public static SizeF GetCellContentSize(string str, Font font, int width, int height)
        {
            SizeF size = SizeF.Empty;
            try
            {

                string Str = str;
                using (Image image = new Bitmap(width, height))
                {

                    using (Graphics tmpG = Graphics.FromImage(image))
                    {
                        size = tmpG.MeasureString(Str, font);
                    }
                }

                return size;
            }
            catch (Exception ex)
            {
                Debugger.Break();
                return size;
            }
        }
        //load report template.
        protected virtual ReportModel GetPrintTemplate(int rcIntNo)
        {
            ReportConfig Rc = SIL.AARTO.BLL.Report.ReportManager.GetReportById(rcIntNo);
            ReportModel model = null;

            if (Rc.AutomaticGenerateQuery != null && Rc.AutomaticGenerateQuery.Length > 0)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    memoryStream.Write(Rc.AutomaticGenerateQuery, 0, Rc.AutomaticGenerateQuery.Length);
                    memoryStream.Position = 0;
                    model = formatter.Deserialize(memoryStream) as ReportModel;
                }
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Rc.Template);

            model.xmlDoc = xmlDoc;
            model.DateFrom = Convert.ToDateTime("1996-01-01");
            model.DateTo = Convert.ToDateTime("2012-05-22");
            model.AutIntNo = Rc.AutIntNo.ToString();
            model.ReportName = Rc.ReportName;
            model.ReportCode = Rc.ReportCode;
            //model.StoredProcName = Rc.RspIntNoSource.ReportStoredProcName;
            return model;
        }

        //load data
        protected virtual DataTable GetPrintData(ReportModel model)
        {
            return null;
        }
        protected override void OnBeginPrint(PrintEventArgs e)
        {
            _printElements = new ArrayList();
            _pageNum = 0;
            _printIndex = 0;
            this.OriginAtMargins = ReportDefaultSetting.OriginAtMargins;
            MyBeginPrint(e);

        }

        protected virtual void MyBeginPrint(PrintEventArgs e)
        {
            // Go through the objects in the list and create print elements for each one
            foreach (IPrintable printObject in _printObjects)
            {
                PrintElement element = new PrintElement(printObject);
                _printElements.Add(element);

                printObject.Print(element);
            }
        }

        protected override void OnPrintPage(PrintPageEventArgs e)
        {
            MyPrintPage(e);
        }

        protected virtual void MyPrintPage(PrintPageEventArgs e)
        {
            _pageNum++;

            Rectangle printBounds = GetPageBounds();

            _columnWidth = Convert.ToInt32(printBounds.Width / _columnNum);

            float headerHeight = Header.CalculateHeight(this, e.Graphics);
            Header.Draw(this, printBounds.Top, e.Graphics, printBounds);

            float tblHeaderHeight = DataTableHeader.CalculateTblRowHeight(this, e.Graphics);
            DataTableHeader.Draw(this, printBounds.Top + headerHeight, e.Graphics, printBounds);

            Rectangle pageBounds = new Rectangle(printBounds.Left,
                                                 (int)(printBounds.Top + headerHeight + tblHeaderHeight), printBounds.Width,
                                                 (int)(printBounds.Height - headerHeight - tblHeaderHeight));

            float yPos = pageBounds.Top + 0;
            bool morePages = false;
            int elementsOnPage = 0;
            while (_printIndex < _printElements.Count)
            {
                PrintElement element = (PrintElement)_printElements[_printIndex];
                float height = element.CalculateTblRowHeight(this, e.Graphics);

                // will it fit on the page? 
                if (yPos + height > pageBounds.Bottom)
                {
                    // we don't want to do this if we're the first thing on the page
                    if (elementsOnPage != 0)
                    {
                        morePages = true;
                        break;
                    }
                }
                element.Draw(this, yPos, e.Graphics, pageBounds);
                // move the ypos
                yPos += height;

                // next
                _printIndex++;
                elementsOnPage++;
            }

            e.HasMorePages = morePages;
        }

        protected virtual Rectangle GetPageBounds()
        {
            return new Rectangle(0, 0, 0, 0);
        }

        public virtual void ShowPreview()
        {
            PrintPreviewDialogEx dialog = new PrintPreviewDialogEx();
            dialog.Document = this;

            //dialog.ShowDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
                dialog.Close();
            //ShowPageSettings();
            return;
        }

        public void ShowPageSettings()
        {
            PageSetupDialog setup = new PageSetupDialog();
            PageSettings settings = DefaultPageSettings;
            setup.PageSettings = settings;

            if (setup.ShowDialog() == DialogResult.OK)
            {
                DefaultPageSettings = setup.PageSettings;
                Print();
            }
        }


        #region Common parts
        public ReportModel PrintModel { get; set; }

        /// <summary>
        /// control how to handle history printing.
        /// </summary>
        public ReportDataService CurrentReportDataService { get; set; }

        #endregion

        // 2012.10.12 Nick added for get resource
        MethodInfo getResource;
        public string GetResource(string key, string defaultVale = null)
        {
            string value = string.Empty;
            try
            {
                if (getResource == null)
                {
                    var method = Assembly.Load("SIL.AARTOService.Resource")
                        .GetType("SIL.AARTOService.Resource.ResourceHelper")
                        .GetMethod("GetResource", new[] { typeof(string) });
                    if (method != null)
                        getResource = method;
                }
                if (getResource != null)
                    value = Convert.ToString(getResource.Invoke(null, new object[] { key }));
            }
            catch { }
            return value;
        }

        public new void Print()
        {
            try
            {
                // Oscar 2013-04-02 added
                Logger(0, true, "Start printing report. Printer: {0}", PrinterSettings.PrinterName);

                base.Print();
            }
            catch (Exception ex)
            {
                if (WriteErrorMessage != null)
                    WriteErrorMessage(string.Format("Printer is not accessible, please check the printer / settings. Error: {0}", ex.ToString()));
                throw;
            }
        }

        #region  Build report string for LPT printing
        //Jake 2014-08-28 added a property to set RowCount 
        protected int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; }
        }
        int rowCount = 71, charCount = 96;//Heidi 2013-10-08 changed rowCount = 70 to 71 for Adjust the last place
        decimal rowHeight = 16.39M;
        protected virtual decimal charWidth { get { return 8.33M; } }

        //Jacob added 20130904 start 
        /// <summary>
        /// break down print objects into lines and combine them to lines.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        protected string CombineToLines(FreePage page)
        {

            List<LinePiece> newLinePieceList = new List<LinePiece>();
            foreach (var cell in page.cells)
            {
                newLinePieceList.AddRange(BreakDownCells(cell));
            }

            // have got all pieces for one page, next we need to combine them to lines


            StringBuilder sb = new StringBuilder();
            int prow = 0, crow = 0;

            for (int i = 0; i < rowCount; i++)
            {
                var cc = from c in newLinePieceList
                         orderby c.yRow, c.xCharStart
                         where c.yRow == i
                         select c;
                if (cc.Count() > 0)
                {
                    sb.AppendLine(CombineOneLine(cc));
                }
                else

                    sb.AppendLine();
            }

            return sb.ToString();
        }


        protected string CombineOneLine(IEnumerable<LinePiece> pieces)
        {
            StringBuilder isb = new StringBuilder();
            bool first = true;
            foreach (var p in pieces)
            {


                if (isb.Length > p.xCharStart)
                {
                    isb.Remove(p.xCharStart, isb.Length - p.xCharStart);
                }

                //first item
                if (first)
                {

                    isb.Append((p.text.PadRight(p.xCharLength)).PadLeft(p.xCharStart + p.xCharLength));
                    first = false;
                }
                else
                    isb.Append(p.text.PadRight(p.xCharLength).PadLeft(p.xCharStart - isb.Length + p.xCharLength));
            }
            return isb.ToString();


        }

        /// <summary>
        /// seperate text block into lines.
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        /// <remarks>
        /// Jacob 2013/10/16 edited to support cell height control.
        /// </remarks>
        protected List<LinePiece> BreakDownCells(FreeCell cell)
        {
            List<LinePiece> newLinePieceList = new List<LinePiece>();

            int cellRows = (int)(cell.cellStyle.Height / rowHeight);
            int startRow = (int)(cell.cellStyle.Top / rowHeight);
            int defCharCount = (int)(cell.cellStyle.Width / charWidth);
            int cellCharCount = ShowGridLine ? defCharCount - 1 : defCharCount;

            int startChar = (int)(cell.cellStyle.Left / charWidth);
            int rowIndex = startRow;//Jacob
            string s = cell.Text;
            int lineIndex = 0;
            string[] sa = s.Split("\r\n".ToCharArray());
            foreach (string st in sa)
            {
                string text = st;
                while (!string.IsNullOrEmpty(text) && rowIndex <= startRow + cellRows)//Jacob
                {
                    //if line characters is set, use it instead of the default cellCharCount.
                    if (cell.cellStyle.LineCharacters != null)
                    {
                        cellCharCount = cell.cellStyle.LineCharacters.Length > lineIndex
                            ? cell.cellStyle.LineCharacters[lineIndex] : cellCharCount;
                        lineIndex += 1;
                    }
                    int leftLength = text.Length < cellCharCount ? text.Length : cellCharCount;

                    // string cellText = text.Substring(0, leftLength);
                    int realposition;
                    string cellText = SplitBySpace(text, leftLength, out realposition);

                    if (realposition >= 0)
                        text = text.Substring(realposition);
                    else
                        text = "";
                    newLinePieceList.Add(new LinePiece
                    {
                        text = (cell.PrintPortionDef == PrintPortionDef.DataTableRow && ShowGridLine ? "|" : "") + cellText,
                        xCharStart = startChar,
                        xCharLength = cellCharCount,
                        yRow = rowIndex
                    });

                    rowIndex += 1;
                }
            }

            if (cell.PrintPortionDef == PrintPortionDef.DataTableRow && ShowGridLine)
            {
                while (rowIndex < startRow + cellRows - 1)
                {
                    newLinePieceList.Add(new LinePiece
                    {
                        text = "|",
                        xCharStart = startChar,
                        xCharLength = cellCharCount,
                        yRow = rowIndex++
                    });
                }

                if (cell.PrintPortionDef == PrintPortionDef.DataTableRow)
                {
                    newLinePieceList.Add(new LinePiece
                    {
                        text = "".PadRight(cellCharCount, '-'),
                        xCharStart = startChar,
                        xCharLength = cellCharCount,
                        yRow = rowIndex
                    });
                }
            }
            return newLinePieceList;


        }

        /// <summary>
        /// get the left indicated length of text, space.
        /// 
        /// </summary>
        /// <remarks>
        ///  Jacob added 20130904
        /// </remarks>
        /// <param name="oriText"></param>
        /// <param name="length"></param>
        /// <param name="outPosition"></param>
        /// <returns></returns>
        protected string SplitBySpace(string oriText, int length, out int outPosition)
        {
            outPosition = -1;
            if (length >= oriText.Length)
            {
                outPosition = oriText.Length;
                return oriText;
            }

            for (int index = length - 1; index >= 0; index--)
            {
                if (oriText[index] == ' ')
                {
                    outPosition = index; break;
                }
            }

            string rtnStr = string.Empty;
            if (outPosition <= 0)
            {
                //no space, or space is at start, abnormal text, have to avoid endless loop.
                outPosition = length;

            }
            if (outPosition > 0 && outPosition < oriText.Length)
                rtnStr = oriText.Substring(0, outPosition);
            outPosition += 1;
            return rtnStr;

        }
        //Jacob added 20130904 end 
        #endregion

    }


    #region  all kinds of ReportFormMode

    public class ReportFormMode
    {
        public virtual string ModeDisplayName { get; set; }

        public virtual void WhenSelectFile()
        {
        }

        public virtual void WhenPrint()
        {
        }
    }



    public class ReportHistoryMode : ReportFormMode
    {
        private readonly string HISTORYMODE_ADDITION_TITLE = " History";

        public override string ModeDisplayName
        {
            get { return HISTORYMODE_ADDITION_TITLE; }
        }
    }



    public class ReportNewMode : ReportFormMode
    {
        private readonly string NEWMODE_ADDITION_TITLE = " Not printed yet.";

        public override string ModeDisplayName
        {
            get { return NEWMODE_ADDITION_TITLE; }
        }
    }



    public class ListReportHistoryMode : ReportFormMode
    {
        private readonly string HISTORYMODE_ADDITION_TITLE = " List History";

        public override string ModeDisplayName
        {
            get { return HISTORYMODE_ADDITION_TITLE; }
        }
    }
    public class CompletedFilesMode : ReportFormMode
    {
        private readonly string HISTORYMODE_ADDITION_TITLE = " Completed Files";

        public override string ModeDisplayName
        {
            get { return HISTORYMODE_ADDITION_TITLE; }
        }
    }

    public class NewFilesMode : ReportFormMode
    {
        private readonly string HISTORYMODE_ADDITION_TITLE = " New Files";

        public override string ModeDisplayName
        {
            get { return HISTORYMODE_ADDITION_TITLE; }
        }
    }

    #endregion
}
