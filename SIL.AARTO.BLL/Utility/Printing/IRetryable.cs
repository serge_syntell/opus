﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.Printing
{
    public interface IRetryable
    {
        bool Attemp();
        void Recover();
    }
}
