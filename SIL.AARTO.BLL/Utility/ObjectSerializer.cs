﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Collections;
using System.Reflection;

namespace SIL.AARTO.BLL.Utility
{
    public static class ObjectSerializer
    {
        public static string Serialize(object objToXml)
        {
            if (objToXml == null)
                throw new ArgumentNullException("objToXml");

            XmlSerializer serializer;
            XmlSerializerNamespaces xsn;
            StringWriter writer;

            serializer = new XmlSerializer(objToXml.GetType());
            xsn = new XmlSerializerNamespaces();
            xsn.Add(String.Empty, String.Empty);
            writer = new StringWriter();

            serializer.Serialize(writer, objToXml, xsn);

            return writer.GetStringBuilder().ToString();
        }
        public static T DeSerializerString2Object<T>(string sXml)
        {
            if (sXml == null)
                throw new ArgumentNullException("sXml");

            XmlReader reader = XmlReader.Create(new StringReader(sXml));
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            object obj = serializer.Deserialize(reader);
            return (T)obj;
        }
        public static void SaveObjectToXmlFile(Object obj, string xmlFile)
        {
            XmlSerializer serializer;
            XmlSerializerNamespaces xsn;
            serializer = new XmlSerializer(obj.GetType());
            xsn = new XmlSerializerNamespaces();
            xsn.Add(String.Empty, String.Empty);

            if (MutexHashtable[xmlFile] == null)
            {
                MutexHashtable[xmlFile] = xmlFile;
            }

            lock (MutexHashtable[xmlFile])
            {
                DirectoryInfo folder = new FileInfo(xmlFile).Directory;
                if (!folder.Exists)
                    folder.Create();

                using (Stream writer = new FileStream(xmlFile, FileMode.Create))
                {
                    serializer.Serialize(writer, obj, xsn);
                }
            }
        }
        public static T ReadXmlFileToObject<T>(string xmlFile)
        {
            Type t = typeof(T);
            XmlSerializer serializer = new XmlSerializer(t);
            T obj;
            if (MutexHashtable[xmlFile] == null)
            {
                MutexHashtable[xmlFile] = xmlFile;
            }
            lock (MutexHashtable[xmlFile])
            {
                using (FileStream fs = new FileStream(xmlFile, FileMode.Open))
                {
                    obj = (T)serializer.Deserialize(fs);
                }
            }
            return obj;
        }
        private static readonly Hashtable MutexHashtable = new Hashtable();



        public static void Serialize(XmlDocument doc, object obj)
        {
            if (obj == null)
                return;

            XmlNode pNode = doc.CreateNode(XmlNodeType.Element, obj.GetType().Name, string.Empty);

            foreach (PropertyInfo pInfo in obj.GetType().GetProperties())
            {
                XmlNode node = doc.CreateNode(XmlNodeType.Element, pInfo.Name, string.Empty);
                if (pInfo.GetValue(obj, null) != null)
                {
                    node.InnerText = pInfo.GetValue(obj, null) == null ? "" : pInfo.GetValue(obj, null).ToString();
                    pNode.AppendChild(node);
                }
            }

            doc.FirstChild.AppendChild(pNode);

        }

        public static void Serialize<T>(XmlDocument doc, List<T> list)
        {
            if (list != null)
            {
                foreach (T obj in list)
                {
                    XmlNode pNode = doc.CreateNode(XmlNodeType.Element, obj.GetType().Name, string.Empty);

                    foreach (PropertyInfo pInfo in obj.GetType().GetProperties())
                    {
                        XmlNode node = doc.CreateNode(XmlNodeType.Element, pInfo.Name, string.Empty);
                        if (pInfo.GetValue(obj, null) != null)
                        {
                            node.InnerText = pInfo.GetValue(obj, null) == null ? "" : pInfo.GetValue(obj, null).ToString();
                            pNode.AppendChild(node);
                        }
                    }

                    doc.FirstChild.AppendChild(pNode);
                }
            }
        }
    }
}
