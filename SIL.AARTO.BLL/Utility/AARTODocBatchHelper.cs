﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Services;
using System.IO;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility
{
    public abstract class AARTODocBatchHelper
    {
        public static string GetDocxFilePath(int ifsIntNo, string backPath)
        {
            ImageFileServerService iService = new ImageFileServerService();
            ImageFileServer iItem = iService.GetByIfsIntNo(ifsIntNo);
            string prefix = string.Format(@"\\{0}\{1}", iItem.ImageMachineName, iItem.ImageShareName);
            //string batchDocx = Path.Combine(prefix, backPath);
            return prefix + backPath;
        }
    }
}
