﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.EntLib;
using System.Configuration;
using System.IO;
using Stalberg.Indaba;

namespace SIL.AARTO.IMX
{
    public class CommonFunction
    {
        public static Stalberg.Indaba.ICommunicate indaba;

        public static void InitIndaba()
        {
            Stalberg.Indaba.Client.ApplicationName = "IMX.Importer";
            Stalberg.Indaba.Client.ConnectionString = ConfigurationManager.ConnectionStrings["IndabaConnectionString"].ConnectionString;
            Stalberg.Indaba.Client.Initialise();
            indaba = Stalberg.Indaba.Client.Create();
        }

        public static void DisposeIndaba()
        {
            indaba.Dispose();
            Stalberg.Indaba.Client.Dispose();
        }

        public static string[] GetServerAndDatabaseName(string connectionStr)
        {
            System.Data.SqlClient.SqlConnection tempConn = new System.Data.SqlClient.SqlConnection();
            tempConn.ConnectionString = connectionStr;

            return new string[] { tempConn.DataSource, tempConn.Database };

        }

        public static void ConsoleWriteLine(string Msg)
        {
            EntLibLogger.WriteLog(LogCategory.General, null, Msg);
            Console.WriteLine(Msg);
        }

        public static void ConsoleWriteLine(string Msg, string category)
        {
            EntLibLogger.WriteLog(category, null, Msg);
            Console.WriteLine(Msg);
        }

        public static void ConsoleWriteLine(string Msg, string category, Exception ex)
        {
            EntLibLogger.WriteLog(category, null, Msg + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
            Console.WriteLine(Msg);
        }

        public static void ConsoleWriteLine(string format, params object[] arg)
        {
            string Msg = string.Format(format, arg);
            ConsoleWriteLine(Msg);
        }


    }
}
