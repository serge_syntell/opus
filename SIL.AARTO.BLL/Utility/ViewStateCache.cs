﻿using System;
using System.Web.UI;

namespace SIL.AARTO.BLL.Utility
{
    [Serializable]
    public class ViewStateCache<T>
        where T : class, new()
    {
        readonly string cacheName;
        readonly StateBag viewState;
        T cache;

        public ViewStateCache(StateBag viewState, string cacheName = "ViewStateCache")
        {
            this.viewState = viewState;
            this.cacheName = cacheName;
            this.cache = Current;
        }

        public T Current
        {
            get
            {
                if (this.cache == null)
                {
                    if (this.viewState[this.cacheName] != null)
                        this.cache = this.viewState[this.cacheName] as T;
                    else
                    {
                        this.cache = new T();
                        this.viewState[this.cacheName] = this.cache;
                    }
                }
                return this.cache;
            }
        }
    }
}
