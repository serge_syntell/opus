﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;


namespace SIL.AARTO.BLL.Utility.AartoDocType
{
    public class AartoDocumentTypeManager
    {
        public static List<AartoDocTypeEntity> GetAatoHandWrittenDocTypeList()
        {
            List<AartoDocTypeEntity> aartoHandWrittenDocTypeList = new List<AartoDocTypeEntity>();
            AartoDocTypeEntity aartoDocTypeEntity;

            TList<SIL.AARTO.DAL.Entities.AartoDocumentType> aartoDocTypeList = new AartoDocumentTypeService().GetAll();

            foreach (SIL.AARTO.DAL.Entities.AartoDocumentType aartoDocType in aartoDocTypeList)
            {
                if (aartoDocType.AaIsHandwrittenNotice.Value)
                {
                    aartoDocTypeEntity = new AartoDocTypeEntity();
                    aartoDocTypeEntity.AaDocTypeID = aartoDocType.AaDocTypeId;
                    aartoDocTypeEntity.AaDocTypeName = aartoDocType.AaDocTypeName;

                    aartoHandWrittenDocTypeList.Add(aartoDocTypeEntity);
                }
            }

            return aartoHandWrittenDocTypeList;
        }

        public static string GetAartoDocTypeName(int aartoDocTypeID)
        {
            return new AartoDocumentTypeService().GetByAaDocTypeId(aartoDocTypeID).AaDocTypeName;
        }
    }
}
