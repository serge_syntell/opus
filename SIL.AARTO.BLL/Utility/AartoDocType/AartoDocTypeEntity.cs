﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.Utility.AartoDocType
{
    public class AartoDocTypeEntity
    {
        public int AaDocTypeID { get; set; }
        public string AaDocTypeName { get; set; }
    }
}
