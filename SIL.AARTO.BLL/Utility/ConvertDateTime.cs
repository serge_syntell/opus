﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;


namespace SIL.AARTO.BLL.Utility
{
    public class ConvertDateTime
    {
        #region private member
        private const string convertNull = "Cannot convert null or empty to datetime";
        #endregion

        #region browser <-> default
        public static string BrowserStr2DefaultStr(string dtStr)
        {
            return Date2DefaultStr(BrowserStr2NullableDate(dtStr));
        }
        public static string DefaultStr2BrowserStr(string dtStr)
        {
            return Date2BrowserStr(DefaultStr2NullableDate(dtStr));
        }
        #endregion

        #region browser <-> DateTime
        public static DateTime? BrowserStr2NullableDate(string dtStr)
        {
            if (!string.IsNullOrEmpty(dtStr))
                return DateTime.Parse(dtStr, Language.BrowserCulture.DateTimeFormat);
            return null;
        }
        public static DateTime BrowserStr2Date(string dtStr)
        {
            if (!string.IsNullOrEmpty(dtStr))
                return DateTime.Parse(dtStr, Language.BrowserCulture.DateTimeFormat);
            throw new ArgumentOutOfRangeException(convertNull);
        }
        public static string Date2BrowserStr(DateTime? dt)
        {
            return dt.HasValue
                ? dt.Value.ToString("d", Language.BrowserCulture.DateTimeFormat)
                : "";
        }
        #endregion

        #region default <-> DateTime
        public static DateTime? DefaultStr2NullableDate(string dtStr)
        {
            if (!string.IsNullOrEmpty(dtStr))
                return DateTime.Parse(dtStr, Language.DefaultCulture.DateTimeFormat);
            return null;
        }
        public static DateTime DefaultStr2Date(string dtStr)
        {
            if (!string.IsNullOrEmpty(dtStr))
                return DateTime.Parse(dtStr, Language.DefaultCulture.DateTimeFormat);
            throw new ArgumentOutOfRangeException(convertNull);
        }
        public static string Date2DefaultStr(DateTime? dt)
        {
            if (dt.HasValue)
                return dt.Value.ToString("d", Language.DefaultCulture);
            return null;
        }
        #endregion

        #region date format for JQuery Date Picker
        public static string JQueryDateFormat
        {
            get
            {
                return Language.BrowserCulture.DateTimeFormat.ShortDatePattern.ToLower().Replace("yyyy", "yy");
            }
        }
        #endregion

        #region Whether it is a valid format for the browser
        public static bool IsValidBrowserFormat(string dtStr)
        {
            DateTime dt;
            return DateTime.TryParse(dtStr, Language.BrowserCulture, DateTimeStyles.None, out dt);
        }
        #endregion

        public static long MilliTimeStamp(DateTime TheDate)
        {
            DateTime d1 = new DateTime(1970, 1, 1);
            DateTime d2 = TheDate.ToUniversalTime();
            TimeSpan ts = new TimeSpan(d2.Ticks - d1.Ticks);
            return (long)ts.TotalMilliseconds;
        }

        public static DateTime GetShortDateTime(DateTime date)
        {
            return Convert.ToDateTime(date.ToShortDateString());
        }

        public static string GetDateTimeNowString()
        {
            DateTime date = DateTime.Now;
            StringBuilder dateStr=new StringBuilder();
            dateStr.Append(date.Year.ToString());
            dateStr.Append("-");
            dateStr.Append(date.Month.ToString().PadLeft(2,'0'));
            dateStr.Append("-");
            dateStr.Append(date.Day.ToString().PadLeft(2,'0'));
            dateStr.Append("-");
            dateStr.Append(date.Hour.ToString().PadLeft(2,'0'));
            dateStr.Append("-");
            dateStr.Append(date.Minute.ToString().PadLeft(2,'0'));
            dateStr.Append("-");
            dateStr.Append(date.Second.ToString().PadLeft(2,'0'));
            return dateStr.ToString();
        }

        public static DateTime GetNowDate()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("http://192.168.1.219/Test/now.xml");
            XmlElement rootElement = xmlDoc.DocumentElement;
            XmlNode datas = rootElement.SelectSingleNode(@"//Datas");
            string now = datas.Attributes["Now"].Value.Trim();
            return GetShortDateTime(Convert.ToDateTime(now));
        }
    }
}
