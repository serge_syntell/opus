﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
namespace SIL.AARTO.BLL.Utility
{
    /// <summary>
    /// try to connect all the file server,if success you can access all the images in the file servers.
    /// </summary>
    public class FileServer
    {
        public static bool ConnectAll()
        {
            ConnectService service = new ConnectService();
            bool flag = true;
            TList<ImageFileServer> ifsList = new ImageFileServerService().GetByAaSysFileTypeId((int)AartoSystemFileTypeList.DMSScanImage);
            foreach (ImageFileServer ifs in ifsList)
            {
                if (!service.Connect(ifs.ImageMachineName, ifs.ImageShareName,"",""))// ifs.RemoteUserName, ifs.RemotePassword))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
        public static bool Connect(string host,string folder,string userName,string password)
        {
            ConnectService service = new ConnectService();
            return service.Connect(host, folder, userName, password);
        }
    }
}
