﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
namespace SIL.AARTO.BLL.Utility
{
    public class Session
    {
        public static UserLoginInfo GetUserLoginInfo()
        {
            return (UserLoginInfo)HttpContext.Current.Session["UserLoginInfo"];
        }
        public static string UserName
        {
            get
            {
                return GetUserLoginInfo().UserName;
            }
        }
        public static int UserID
        {
            get
            {
                return GetUserLoginInfo().UserIntNo;
            }
        }
    }
}
