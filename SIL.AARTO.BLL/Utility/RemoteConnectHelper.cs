﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using SIL.AARTO.DAL.Entities;
using System.IO;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Utility
{
    public abstract class RemoteConnectHelper
    {
        //public static bool RemoteConnect(string remoteHost, string shareName, string userName, string passWord)
        //{
        //    string StrConnectCommand = @"net use \\" + remoteHost + @"\" + shareName + " /User:" + userName + " " + passWord + " /PERSISTENT:YES";
        //    bool connectResult = RemoteCommand(StrConnectCommand);
        //    if (connectResult == false)
        //    {
        //        // disconnect and reconnect again
        //        bool disConnectResult = RemoteCommand(@"net use \\" + remoteHost + @"\" + shareName + " /Delete");
        //        if (disConnectResult)
        //        {
        //            connectResult = RemoteCommand(StrConnectCommand);
        //        }
        //    }
        //    return connectResult;
        //}

        //public static bool RemoteCommand(string command)
        //{
        //    bool Flag = false;
        //    Process proc = new Process();

        //    try
        //    {
        //        proc.StartInfo.FileName = "cmd.exe";
        //        proc.StartInfo.UseShellExecute = false;
        //        proc.StartInfo.RedirectStandardInput = true;
        //        proc.StartInfo.RedirectStandardOutput = true;
        //        proc.StartInfo.RedirectStandardError = true;
        //        proc.StartInfo.CreateNoWindow = true;
        //        proc.Start();

        //        proc.StandardInput.WriteLine(command);
        //        proc.StandardInput.WriteLine("exit");
        //        while (!proc.HasExited)
        //        {
        //            proc.WaitForExit(1000);
        //        }
        //        string errormsg = proc.StandardError.ReadToEnd();
        //        proc.StandardError.Close();
        //        if (String.IsNullOrEmpty(errormsg))
        //        {
        //            Flag = true;
        //        }
        //        else
        //        {
        //            throw new Exception(errormsg);
        //            //MessageBox.Show("Command error: command = " + command + ", error = " + errormsg);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //        //ErrorLog.AddErrorLog(ex, BatchManager.LastUser);
        //        //MessageBox.Show("Command error: command = " + command + ", error = " + ex.Message);
        //    }
        //    finally
        //    {
        //        proc.Close();
        //        proc.Dispose();
        //    }
        //    return Flag;
        //}

        //public static bool RemoteConnect(ImageFileServer fileServer)
        //{
        //    return RemoteConnectHelper.RemoteConnect(
        //               fileServer.ImageMachineName,
        //               fileServer.ImageShareName,
        //               fileServer.RemoteUserName,
        //               fileServer.RemotePassword);
        //}

        public static string GetRemoteFileServerTestImagePath(ImageFileServer fileServer)
        {
            return string.Format(@"\\{0}\{1}\test.jpg", fileServer.ImageMachineName, fileServer.ImageShareName); 
        }

        public static bool CheckFileServerAvaliable(ImageFileServer fileServer)
        {
            return File.Exists(GetRemoteFileServerTestImagePath(fileServer));
        }
    }

    public class ImageHelper
    {
        public static byte[] GetImagesFromRemoteFileServer(ScanImageDetails image)
        {
            byte[] data = null;
            if (image.FileServer != null)
            {
                //WebService service = new WebService();
                //if (service.RemoteConnect(image.FileServer.ImageMachineName,
                //    image.FileServer.ImageShareName,
                //    image.FileServer.RemoteUserName,
                //    image.FileServer.RemotePassword))
                //{
                bool fileExists = File.Exists(image.ImageFullPath);
                if (fileExists)
                    using (FileStream fs = File.OpenRead(image.ImageFullPath))
                    {
                        data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                    }
                //}
            }
            return data;
        }
    }
}
