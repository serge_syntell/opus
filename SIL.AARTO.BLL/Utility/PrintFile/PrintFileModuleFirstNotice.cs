﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.Data;
using ceTe.DynamicPDF.Merger;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF;
using System.IO;
using ceTe.DynamicPDF.Imaging;
using Stalberg.TMS.Data.Util;
using System.Text.RegularExpressions;
using System.Web;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Globalization;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleFirstNotice : PrintFileBase
    {
        public PrintFileModuleFirstNotice(int nrIntNo = 0)
        {
            this.NRIntNo = nrIntNo;
            //this.FileType = PrintFileNameType.DPLX;
            this.ErrorPage = GetResource("FirstNoticeViewer.aspx", "thisPage");
            this.ErrorPageUrl = "FirstNoticeViewer.aspx";

        }

        public System.Web.UI.Page page { get; set; }

        public override void InitialWork()
        {
            string path;
            //this.SubProcess.IsSuccessful = false;
            string printFileName = this.PrintFileName;

            this.imgDB = new ScanImageDB(this.SubProcess.ConnectionString);
            AuthReportNameDB arn = new AuthReportNameDB(this.SubProcess.ConnectionString);
            ReportFile = arn.GetAuthReportName(this.AutIntNo, "FirstNotice");
            string sTemplate = arn.GetAuthReportNameTemplate(this.AutIntNo, "FirstNotice");
            if (ReportFile.Equals(string.Empty))
            {
                ReportFile = "FirstNotice_PL.dplx";
                arn.AddAuthReportName(this.AutIntNo, ReportFile, "FirstNotice", "System", "FirstNoticeTemplate_PL.pdf");
            }

            int noticeStage = FIRST_NOTICE;
            string prefix = string.Empty;
            string longPrefix = string.Empty;
            bool singleNotice = false;

            //AuthorityRulesDB authRules = new AuthorityRulesDB(connectionString);   
            NoticeDB notice = new NoticeDB(this.SubProcess.ConnectionString);
            DataSet ds = null;

            if (PrintFileName != "-1" && PrintFileName.Length > 7)
            {
                //string prefix = this.printFile.Substring(0, 3).ToUpper();
                prefix = this.PrintFileName.Substring(0, 3).ToUpper();
                if (prefix == "NON")
                {
                    this.SubProcess.IsSuccessful = false;
                    return;
                }
                //string longPrefix = "NONE";
                if (PrintFileName.Length >= 7)
                    longPrefix = PrintFileName.Substring(0, 7).ToUpper();

                // See if its a single notice
                string pattern = @"^\w{2,}/\d{2,}/\d{2,}/\d{3,}$";
                //string patternAARTO = @"^\w{2,}-\d{4,}-\d{9,}-\d{1,}$";
                Regex regex = new Regex(pattern, RegexOptions.Singleline);
                //Regex regex2 = new Regex(patternAARTO, RegexOptions.Singleline);

                if (regex.IsMatch(this.PrintFileName))
                {
                    if (!this.PrintFileName[0].Equals('*'))
                        this.PrintFileName = "*" + this.PrintFileName;

                    singleNotice = true;
                    ds = notice.GetNoticeCheckRLVDS(this.PrintFileName.Substring(1, this.PrintFileName.Length - 1));
                    //Jake 2014-04-25 added code to handle longprefix is this notice is a NAG notice
                    Notice not = new NoticeService().GetByNotTicketNo(this.PrintFileName.Substring(1, this.PrintFileName.Length - 1)).FirstOrDefault();
                    if (not != null && !String.IsNullOrEmpty(not.NotCiprusPrintReqName))
                    {
                        longPrefix = not.NotCiprusPrintReqName.Length > 7 ? not.NotCiprusPrintReqName.Substring(0, 7) : "";
                    }

                }
            }
            else if (this.NRIntNo > 0)
            {
                ds = notice.GetNoticeCheckRLVDS(this.NRIntNo);
            }

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    prefix = dr["PrintFileName"].ToString();
                    //add the check for the status of the notice here
                    noticeStage = Convert.ToInt16(dr["NoticeStage"]);

                    //dls 090321 - need to use the Ticket Processor on the Notice, not the AutTicketProcessor on the Authority
                    format = dr["NotTicketProcessor"].ToString();

                    //dls 090625 - need to add the new ticket processor for CofCT here
                    //if (format.Equals("CiprusPI"))

                    if (format.Equals("CiprusPI") || format.Equals("Cip_CofCT"))
                        this._status = 210;
                    else if (format.Equals("Cip_Aarto"))
                        this._status = 500;
                }

                ds.Dispose();
            }
            //Commented by linda 2012-12-20 ( change AutTicketProcessor to NotTicketProcessor=TMS)
            //else
            //{
            //    //dls 090723 - have to check what status we need to use here!
            //    AuthorityDB authDB = new AuthorityDB(this.SubProcess.ConnectionString);
            //    AuthorityDetails authDetails = authDB.GetAuthorityDetails(this.AutIntNo);
            //    if (authDetails.AutTicketProcessor == "CiprusPI" || authDetails.AutTicketProcessor == "Cip_CofCT")
            //    {
            //        format = authDetails.AutTicketProcessor;
            //        this._status = 210;
            //    }
            //    else if (authDetails.AutTicketProcessor == "Cip_Aarto")
            //    {
            //        format = authDetails.AutTicketProcessor;
            //        this._status = 500;
            //    }
            //}

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails arDet1 = new AuthorityRulesDetails();
            arDet1.AutIntNo = this.AutIntNo;
            arDet1.ARCode = "2550";
            arDet1.LastUser = this.SubProcess.LastUser;

            DefaultAuthRules authRule = new DefaultAuthRules(arDet1, this.SubProcess.ConnectionString);
            KeyValuePair<int, string> valueArDet1 = authRule.SetDefaultAuthRule();

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails arDet2 = new AuthorityRulesDetails();
            arDet2.AutIntNo = this.AutIntNo;
            arDet2.ARCode = "4250";
            arDet2.LastUser = this.SubProcess.LastUser;

            DefaultAuthRules authRule1 = new DefaultAuthRules(arDet2, this.SubProcess.ConnectionString);
            KeyValuePair<int, string> valueArDet2 = authRule1.SetDefaultAuthRule();


            //if the rule for printing 2nd notice is turned off or they want see the notice based on the selected viewer, regardless of status

            if (PrintFileName != "-1" && valueArDet1.Value.Equals("N") || valueArDet2.Value.Equals("N") || valueArDet2.Key == 0)
                noticeStage = FIRST_NOTICE;

            PrintFileProcess process = new PrintFileProcess(this.SubProcess.ConnectionString, this.SubProcess.LastUser);
            if (noticeStage == SECOND_NOTICE)
            //have to place this here to cater for printing 2nd notices for new offenders/changed regno's
            {
                //if (page != null)
                //    page.Response.Redirect("SecondNoticeViewer.aspx?printfile=" + PrintFileName);                    
                //else
                //    return;
                process.ErrorProcessing = this.SubProcess.ErrorProcessing;
                process.BuildPrintFile(
                    new PrintFileModule2ndNotice(),
                    this.AutIntNo, printFileName, this.PrintFileOutputFolder);
                this.SubProcess.IsSuccessful = process.IsSuccessful;
                this.SubProcess.SkipNextProcesses = true;
                return;
            }

            //dls 090710 - need to make all the string checks ToUpper - user doesn't necessarily use the same case
            // Check the print file prefix
            #region switch
            this.StoredProcedureName_Update = "NoticePrint_Update1st_WS";
            //Jerry 2012-06-14 add arnFunction
            string arnFunction = "FirstNotice";
            switch (prefix.ToUpper())
            {
                case "ASD": // Average Speed Over Distance
                    if (longPrefix == "ASD_NAG")
                    {
                        ReportFile = arn.GetAuthReportName(this.AutIntNo, "ASDNoAOG");

                        if (ReportFile.Equals(string.Empty))
                        {
                            ReportFile = "NoAOG_CofCT_ASD.dplx";
                            arn.AddAuthReportName(AutIntNo, ReportFile, "ASDNoAOG", "System", "NoAOGTemplate_CofCT_ASD.pdf");
                        }

                        sTemplate = arn.GetAuthReportNameTemplate(this.AutIntNo, "ASDNoAOG");
                        arnFunction = "ASDNoAOG";

                        //dls 2012-10-23 - need to use this status for ASD No AOG's ==> doesn't make a big difference, as the proc checks for all status values
                        this._status = NO_AOG_STATUS;
                    }
                    else
                    {
                        ReportFile = arn.GetAuthReportName(this.AutIntNo, "ASDFirstNotice");

                        if (ReportFile.Equals(string.Empty))
                        {
                            ReportFile = "FirstNotice_CofCT_ASD.dplx";
                            arn.AddAuthReportName(this.AutIntNo, ReportFile, "ASDFirstNotice", "System", "FirstNoticeTemplate_CofCT_ASD.pdf");
                        }

                        sTemplate = arn.GetAuthReportNameTemplate(this.AutIntNo, "ASDFirstNotice");
                        arnFunction = "ASDFirstNotice";
                    }
                    break;
                case "BUS":
                    ReportFile = arn.GetAuthReportName(this.AutIntNo, "BUSFirstNotice");

                    if (ReportFile.Equals(string.Empty))
                    {
                        ReportFile = "FirstNotice_CofCT_BUS.dplx";
                        arn.AddAuthReportName(this.AutIntNo, ReportFile, "BUSFirstNotice", "System", "FirstNoticeTemplate_CofCT_BUS.pdf");
                    }

                    sTemplate = arn.GetAuthReportNameTemplate(this.AutIntNo, "BUSFirstNotice");
                    arnFunction = "BUSFirstNotice";
                    break;
                case "RLV": // red light violation
                    ReportFile = arn.GetAuthReportName(this.AutIntNo, "FirstNotice RLV");

                    if (ReportFile.Equals(string.Empty))
                    {
                        ReportFile = "FirstNotice_RLV.dplx";
                        arn.AddAuthReportName(this.AutIntNo, ReportFile, "FirstNotice RLV", "System", "");
                    }

                    sTemplate = arn.GetAuthReportNameTemplate(this.AutIntNo, "FirstNotice RLV");
                    arnFunction = "FirstNotice RLV";
                    break;

                case "NAG": // No Admission of guilt
                    // Jake 2013-12-10 add to hand section 45 NoAOG
                    if (longPrefix != null && longPrefix.Equals("NAG_S35", StringComparison.OrdinalIgnoreCase))
                    {
                        ReportFile = arn.GetAuthReportName(this.AutIntNo, "NoAog_S35");

                        if (ReportFile.Equals(string.Empty))
                        {
                            ReportFile = "NoAog_S35.dplx";
                            arn.AddAuthReportName(this.AutIntNo, ReportFile, "NoAog_S35", "System", "NoAogS35Template.pdf");
                        }

                        sTemplate = arn.GetAuthReportNameTemplate(this.AutIntNo, "NoAog_S35");

                        this._status = NO_AOG_STATUS;
                        arnFunction = "NoAog_S35";
                    }
                    else
                    {
                        ReportFile = arn.GetAuthReportName(this.AutIntNo, "NoAog");

                        if (ReportFile.Equals(string.Empty))
                        {
                            ReportFile = "NoAog.dplx";
                            arn.AddAuthReportName(this.AutIntNo, ReportFile, "NoAog", "System", "NoAOGTemplate_PL.pdf");
                        }

                        sTemplate = arn.GetAuthReportNameTemplate(this.AutIntNo, "NoAog");

                        this._status = NO_AOG_STATUS;
                        arnFunction = "NoAog";
                    }
                    break;

                case "RNO": // red light violation
                    switch (longPrefix)
                    {
                        //at the moment everyone except JMPD uses the same report for RLV as for SPD, e.g. FirstNotice_ST.rpt
                        case "RNO_RLV":
                        case "RNO_SPD":
                        default:
                            ReportFile = arn.GetAuthReportName(this.AutIntNo, "FirstNotice");

                            if (ReportFile.Equals(string.Empty))
                            {
                                ReportFile = "FirstNotice.dplx";
                                arn.AddAuthReportName(this.AutIntNo, ReportFile, "FirstNotice", "System", "FirstNoticeTemplate_PL.pdf");
                            }
                            sTemplate = arn.GetAuthReportNameTemplate(this.AutIntNo, "FirstNotice");

                            //this.StoredProcedureName_Update = "NoticePrint_UpdateRNO_WS";
                            this.StoredProcedureName_Update = "";
                            break;
                    }
                    break;

                case "REG": // Change of registration
                    //page.Response.Redirect("ChangeRegNo_Viewer.aspx?printfile=" + PrintFileName);
                    process.ErrorProcessing = this.SubProcess.ErrorProcessing;
                    process.BuildPrintFile(
                        new PrintFileModuleChangeRegNo(),
                        this.AutIntNo, printFileName, this.PrintFileOutputFolder);
                    this.SubProcess.IsSuccessful = process.IsSuccessful;
                    this.SubProcess.SkipNextProcesses = true;
                    return;
            }
            #endregion

            //if (page != null)
            //{
            //    path = page.Server.MapPath("reports/" + ReportFile);
            //    string templatePath = string.Empty;

            //    //dls 081117 - check that report actually exists
            //    if (!File.Exists(path))
            //    {
            //        string error = "Report " + ReportFile + " does not exist";

            //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, page.thisPage, thisPageURL);
            //        page.Response.Redirect(errorURL);
            //        return;
            //    }

            //    if (!sTemplate.Equals(""))
            //    {
            //        //dls 081117 - we can only check that the template path exists if there is actually a template!
            //        templatePath = page.Server.MapPath("Templates/" + sTemplate);

            //        if (!File.Exists(templatePath))
            //        {
            //            string error = "Report template " + sTemplate + " does not exist";
            //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

            //            page.Response.Redirect(errorURL);
            //            return;
            //        }
            //    }
            //}

            //Jerry 2012-06-14 change 
            if (!CheckReportTemplateExists(arnFunction, null, out path)) return;

            //this.SubProcess.IsSuccessful = true;
        }

        private const int NO_AOG_STATUS = 500;
        private const int FIRST_NOTICE = 1;
        private const int SECOND_NOTICE = 2;
        private string specificLA_COFCT = "_COFCT";

        private int _status = 10;
        private string _showAll = "N";
        private int NRIntNo = 0;
        protected int _option = 1;              //1st Notice
        protected string format = "TMS"; //BD 20090128 - chnaged from TMS_Notice to TMS

        // BarcodeNETImage barcode;

        protected ScanImageDB imgDB = null;
        PlaceHolder phBarCode;
        byte[] mainImage;
        byte[] regNoImage;
        byte[] imageA = null;
        byte[] imageB = null;
        System.Drawing.Image barCodeImage = null;


        public override void MainWork()
        {
            #region Init doc
            string sTempEng = string.Empty;
            string sTempAfr = string.Empty;

            // Declare some SQL Objects
            //SqlConnection con = new SqlConnection(this.SubProcess.ConnectionString);
            SqlCommand com = null;

            DocumentLayout doc = new DocumentLayout(this.ReportFilePath);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.SubProcess.ConnectionString;
            ParameterDictionary parameters = new ParameterDictionary();

            Label lblId = (Label)doc.GetElementById("lblId");
            Label lblReference = (Label)doc.GetElementById("lblReference");
            Label lblReferenceB = (Label)doc.GetElementById("lblReferenceB");
            Label lblFormattedNotice = (Label)doc.GetElementById("lblFormattedNotice");
            Label lblForAttention = (Label)doc.GetElementById("lblForAttention");
            Label lblAddress = (Label)doc.GetElementById("lblAddress");
            Label lblNoticeNumber = (Label)doc.GetElementById("lblNoticeNumber");
            Label lblPaymentInfo = (Label)doc.GetElementById("lblPaymentInfo");
            Label lblStatRef = (Label)doc.GetElementById("lblStatRef");
            Label lblDate = (Label)doc.GetElementById("lblDate");
            Label lblTime = (Label)doc.GetElementById("lblTime");
            Label lblLocDescr = (Label)doc.GetElementById("lblLocDescr");
            Label lblOffenceDescrEng = (Label)doc.GetElementById("lblOffenceDescrEng");
            Label lblOffenceDescrAfr = (Label)doc.GetElementById("lblOffenceDescrAfr");
            Label lblCode = (Label)doc.GetElementById("lblCode");
            Label lblCameraNo = (Label)doc.GetElementById("lblCamera");
            Label lblOfficerNo = (Label)doc.GetElementById("lblOfficerNo");
            Label lblFineAmount = (Label)doc.GetElementById("lblAmount");
            Label lblPrintDate = (Label)doc.GetElementById("lblPrintDate");
            Label lblOffenceDate = (Label)doc.GetElementById("lblOffenceDate");
            Label lblOffenceTime = (Label)doc.GetElementById("lblOffenceTime");
            Label lblAut = (Label)doc.GetElementById("lblText");
            Label lblLocation = (Label)doc.GetElementById("lblLocation");
            Label lblSpeedLimit = (Label)doc.GetElementById("lblSpeedLimit");
            Label lblSpeed = (Label)doc.GetElementById("lblSpeed");
            Label lblOfficer = (Label)doc.GetElementById("lblOfficer");
            Label lblRegNo = (Label)doc.GetElementById("lblRegNo");
            Label lblIssuedBy = (Label)doc.GetElementById("lblIssuedBy");
            Label lblPaymentDate = (Label)doc.GetElementById("lblPaymentDate");
            Label lblCourtName = (Label)doc.GetElementById("lblCourtName");
            Label lblEasyPay = (Label)doc.GetElementById("lblEasyPay");
            Label lblFilmNo = (Label)doc.GetElementById("lblFilmNo");
            Label lblFrameNo = (Label)doc.GetElementById("lblFrameNo");
            Label lblVehicle = (Label)doc.GetElementById("lblVehicle");
            Label lblAuthorityAddress = (Label)doc.GetElementById("lblAuthorityAddress");
            Label lblAutTel = (Label)doc.GetElementById("lblAutTel");
            Label lblAutFax = (Label)doc.GetElementById("lblAutFax");
            Label lblDisclaimer = (Label)doc.GetElementById("lblDisclaimer");
            Label lblReprintDate = (Label)doc.GetElementById("lblReprintDate");
            Label lblReprintDateText = (Label)doc.GetElementById("lblReprintDateText");


            //dls 090720 -added file name for CoCT
            //Jerry 2012-06-14 change it
            //Label lblPrintFileNameName = (Label)doc.GetElementById("lblPrintFileNameName");
            Label lblPrintFileNameName = (Label)doc.GetElementById("lblPrintFileName");
            PageNumberingLabel PageNumberingLabel1 = (PageNumberingLabel)doc.GetElementById("PageNumberingLabel1");


            Label lblIssue1stNoticeDate = (Label)doc.GetElementById("lblIssue1stNoticeDate");

            Label lbl_AutPostAddr = (Label)doc.GetElementById("lbl_AutPostAddr");
            Label lblpaypoint = (Label)doc.GetElementById("lbl_paypoint");
            Label lblMtrName = (Label)doc.GetElementById("lbl_MtrName");
            Label lblMtrDepart = (Label)doc.GetElementById("lbl_MtrDepart");
            Label lblMtrPostAddr = (Label)doc.GetElementById("lbl_MtrPostAddr");
            Label lbl_AuthName = (Label)doc.GetElementById("lbl_AuthName");
            Label lblAutDepartName = (Label)doc.GetElementById("lbl_AutDepartName");
            Label lblAutPhyAddr = (Label)doc.GetElementById("lblAutPhyAddr");
            Label lblCourt = (Label)doc.GetElementById("lblCourt");
            Label lblCameraA = (Label)doc.GetElementById("lblCameraA");
            Label lblCameraB = (Label)doc.GetElementById("lblCameraB");
            Label lblMeasurement = (Label)doc.GetElementById("lblMeasurement");
            Label lblTimeDifference = (Label)doc.GetElementById("lblTimeDifference");
            Label lblGpsTimeA = (Label)doc.GetElementById("lblGpsTimeA");
            Label lblGpsTimeB = (Label)doc.GetElementById("lblGpsTimeB");
            Label lbl_AuthName2 = (Label)doc.GetElementById("lbl_AuthName2");
            Label lbl_Court = (Label)doc.GetElementById("lbl_Court");
            //2011-9-7 jerr add _RLV_BV
            Label lblAutNameEng = (Label)doc.GetElementById("lblAutNameEng");
            Label lblAutNameAfr = (Label)doc.GetElementById("lblAutNameAfr");
            //Seawen 2013-05-07
            Label lblLSSiteCode1 = (Label)doc.GetElementById("lblLSSiteCode1");
            Label lblLSSiteCode2 = (Label)doc.GetElementById("lblLSSiteCode2");

            //Issue1stNoticeDate

            if (this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0)
            {
                phBarCode = (PlaceHolder)doc.GetElementById("phBarCode");
                phBarCode.LaidOut += ph_BarCode;
            }

            PlaceHolder phImage = null;
            PlaceHolder phImageA = null;
            PlaceHolder phImageB = null;
            #endregion
            bool ignore = false;

            // dl 100805 - add the second image clipping change the layout for 'ASD'
            // jerry 2011-09-26 add FIRSTNOTICE_RLV_ST
            if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0 || this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE_RLV_ST") >= 0)
            {
                phImageA = (PlaceHolder)doc.GetElementById("phImageA");
                phImageA.LaidOut += new PlaceHolderLaidOutEventHandler(ph_ImageA);
                phImageB = (PlaceHolder)doc.GetElementById("phImageB");
                phImageB.LaidOut += new PlaceHolderLaidOutEventHandler(ph_ImageB);
            }
            else
            {
                // only do this if it is a noaog or first notice
                if (this.ReportFile.ToUpper().IndexOf("_RLV") < 0 && this.ReportFile.ToUpper().IndexOf("SECONDNOTICE") < 0 && this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0)
                {
                    phImage = (PlaceHolder)doc.GetElementById("phImage");
                    phImage.LaidOut += new PlaceHolderLaidOutEventHandler(ph_Image);
                }
            }

            //dls 080107 - add the regno clipping to the SW layout  091014 FT add  regno image to cofct.
            // dl 2010/08/19 add regno show for NoAog_CofCT_ASD
            // jerry 2011-09-26 add FIRSTNOTICE_RLV_ST
            PlaceHolder phRegNoImage = null;
            if ((this.ReportFile.ToUpper().IndexOf("SW") > 0 && this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                || (this.ReportFile.ToUpper().IndexOf(specificLA_COFCT) > 0 && this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE") >= 0 && this.ReportFile.ToUpper().IndexOf("RLV") < 0)
                || ((this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0) && (this.ReportFile.ToUpper().IndexOf("COFCT") > 0)) || this.ReportFile.ToUpper().IndexOf("_ST") >= 0)
            {
                phRegNoImage = (PlaceHolder)doc.GetElementById("phRegNoImage");
                phRegNoImage.LaidOut += ph_RegNoImage;
            }

            //if (Session["showAllNotices"] != null)
            //    if (Session["showAllNotices"].ToString().Equals("Y")) _showAll = "Y";

            //get additional parameters for date rules and sysparam setting
            SysParamDB sp = new SysParamDB(this.SubProcess.ConnectionString);

            string violationCutOff = "N";
            int spValue = 0;

            bool found = sp.CheckSysParam("ViolationCutOff", ref spValue, ref violationCutOff);

            int noOfDaysIssue = GetDateRule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays;
            bool showCrossHairs = GetAuthorityRule("3200").ARString.Trim().ToUpper() == "Y";
            int crossHairStyle = (int)GetAuthorityRule("3150").ARNumeric;
            bool allowImageSettings = GetAuthorityRule("2560").ARString == "Y" ? true : false;

            if (this.IsWebMode
                && HttpContext.Current.Session["showAllNotices"] != null
                && HttpContext.Current.Session["showAllNotices"].ToString().Equals("Y"))
                this._showAll = "Y";

            if (PrintFileName != null)
            {
                DataSet ds = new DataSet();

                string strSetGeneratedDate = GetAuthorityRule("2570").ARString.Trim().ToUpper(); //2013-06-18 added by Nancy for 4973 

                // Fill the DataSet
                //need to check if PrintFileName = -1 then use reprint stored proc
                if (PrintFileName == "-1")
                {
                    //com = new SqlCommand("NoticeReprintReport", con);
                    //com.CommandType = CommandType.StoredProcedure;
                    //com.CommandTimeout = 0;
                    //com.Parameters.Add("@NRIntNo", SqlDbType.Int, 4).Value = NRIntNo;
                    //com.Parameters.Add("@NoOfDaysIssue", SqlDbType.Int, 4).Value = noOfDaysIssue;

                    //con.Open();
                    //dr = com.ExecuteReader(CommandBehavior.CloseConnection);

                    // Oscar 20120413 put it back
                    List<SqlParameter> paraList = new List<SqlParameter>();
                    paraList.Add(new SqlParameter("@NRIntNo", this.NRIntNo));
                    paraList.Add(new SqlParameter("@NoOfDaysIssue", noOfDaysIssue));
                    ds = ExecuteDataSet("NoticeReprintReport", paraList);
                }
                else
                {
                    int expiredRows = 0;
                    bool isUpdate1stNotice = this.StoredProcedureName_Update.Equals("NoticePrint_Update1st_WS");
                    List<SqlParameter> paraList = new List<SqlParameter>();
                    paraList.Add(new SqlParameter("@AutIntNo", this.AutIntNo));
                    paraList.Add(new SqlParameter("@PrintFile", this.PrintFileName));
                    paraList.Add(new SqlParameter("@Status", this._status));
                    paraList.Add(new SqlParameter("@ShowAll", this._showAll));
                    paraList.Add(new SqlParameter("@LastUser", this.SubProcess.LastUser));
                    paraList.Add(new SqlParameter("@ViolationCutOff", violationCutOff));
                    paraList.Add(new SqlParameter("@NoOfDaysIssue", noOfDaysIssue));
                    if (isUpdate1stNotice)
                        paraList.Add(new SqlParameter("@ExpiredRows", expiredRows) { Direction = ParameterDirection.InputOutput });

                    if (!string.IsNullOrWhiteSpace(this.StoredProcedureName_Update))
                    {
                        paraList.Add(new SqlParameter("@AuthRule", strSetGeneratedDate));//2013-06-17 added by Nancy for 4973
                        object objValue = ExecuteScalar(this.StoredProcedureName_Update, paraList, false);
                        int iValue = -1;
                        if (objValue == null || !int.TryParse(objValue.ToString(), out iValue) || iValue < 0)
                        {
                            CloseConnection();
                            ErrorProcessing(string.Format("Error on executing {0}", this.StoredProcedureName_Update));
                            return;
                        }

                        //warning message only for service.
                        if (!IsWebMode
                            && isUpdate1stNotice
                            && int.TryParse(Convert.ToString(paraList[7].Value), out expiredRows)
                            && expiredRows > 0)
                            this.SubProcess.Message = string.Format(GetResource(null, "NoticesMovedToPrintFile"), expiredRows, "Expired_" + this.PrintFileName);
                    }

                    paraList.RemoveRange(4, isUpdate1stNotice ? 5 : 3); //2013-06-17 updated by Nancy from 'isUpdate1stNotice ? 4 : 3' to 'isUpdate1stNotice ? 5 : 3'
                    paraList.Insert(2, new SqlParameter("@NotIntNo", 0));
                    paraList.Add(new SqlParameter("@Option", 1));

                    //DataSet ds = ExecuteDataSet("NoticePrint", paraList);
                    ds = ExecuteDataSet("NoticePrint_WS", paraList, "@NotIntNo");

                }

                //Jake 2014-08-11 add
                CsvPrintModule = new CSVPrintFistNotice(SapoReportTypeList.stNotice, PrintFileName, this.SubProcess.ConnectionString);

                CsvPrintModule.SetDocumentLayout(doc);

                CsvPrintModule.ProcessMessage += CsvProcessModule_ProcessMessage;


                //AuthorityRulesDetails details = authRules.GetAuthorityRulesDetailsByCode(autIntNo, "3200", "Print cross-hairs for DigiCam images on notices", 0, "N", "N = No (Default); Y = Yes", _login);
                //bool showCrossHairs = details.ARString.Equals("Y") ? true : false;

                //details = authRules.GetAuthorityRulesDetailsByCode(autIntNo, "3150", "Style of DigiCam cross-hairs", 0, "", "0 = Standard yellow cross (Default); 1 = Yellow Cross with missing centre; 2 = Standard inverted cross; 3 = Inverted cross with missing centre", _login);
                //int crossHairStyle = details.ARNumeric;

                //get data and populate dr set          

                MergeDocument merge = new MergeDocument();
                Document report;

                int noOfNotices = 0;
                char pad0Char = Convert.ToChar("0");

                //dls 080325 - need to check for additional drset because of nested call to newly added Notice Reprint
                foreach (DataTable dt in ds.Tables)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (ignore) break;

                        try
                        {
                            int nValue;
                            if (int.TryParse(dr[0].ToString(), out nValue))
                            {
                                //ignore this dr - it is returned from one of the nested stored procs
                                ignore = true;
                            }
                            else if (dr[0] == System.DBNull.Value)
                            {
                                //ignore this dr - it is returned from one of the nested stored procs
                                ignore = true;
                            }
                            #region else
                            else
                            {
                                ignore = false;

                                //dls 090710 - need to make all the string checks ToUpper - user doesn't necessarily use the same case
                                #region _CPI
                                if (this.ReportFile.ToUpper().IndexOf("_CPI") >= 0)
                                {
                                    lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    //lblReference.Text = "VERWYSING/REFERENCE: " + dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    //lblReferenceB.Text = "VERWYSING/REFERENCE: " + dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    lblReference.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblReference.Text"), dr["NotTicketNo"].ToString().Trim().Replace("/", " / "));
                                    lblReferenceB.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblReference.Text"), dr["NotTicketNo"].ToString().Trim().Replace("/", " / "));
                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim();
                                    lblPaymentInfo.Text = dr["AutNoticePaymentInfo"].ToString().Trim();
                                    lblOfficer.Text = dr["NotOfficerSName"].ToString();
                                    //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Convert.ToDecimal(dr["ChgRevFineAmount"]));
                                    //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));

                                    //update by Rachel 20140811 for 5337 
                                    //lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString())));
                                    lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]));
                                    //end update by Rachel 20140811 for 5337
                                }
                                #endregion

                                #region _PL
                                if (this.ReportFile.ToUpper().IndexOf("_PL") >= 0)
                                {
                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    //lblFineAmount.Text = string.Format("{0:F}", (dr["NoticeOption"].ToString().Equals("1") ? dr["ChgFineAmount"].ToString() : dr["ChgRevFineAmount"].ToString()));
                                    //update by Rachel 20140811 for 5337 
                                    //lblFineAmount.Text = string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                                    lblFineAmount.Text = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]);
                                    //end update by Rachel 20140811 for 5337
                                    lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    // mrs 20080627 added easypaynumber
                                    //mrs 20081216 removed - there is a function in SQL
                                    //lblEasyPay.Text = ">>>>> " + dr["NotEasyPayNumber"].ToString().Trim();
                                    //mrs 20090108 still need to set the label
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") == -1)
                                        lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                                }
                                #endregion

                                if (this.ReportFile.ToUpper().IndexOf("AOG") >= 0 && this.ReportFile.ToUpper().IndexOf("ST") < 0)
                                {
                                    //currently for PL & SW NoAOG's (and COFCT)
                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");

                                    //Tod Zhang 090804-no authority label when the report is NoAOG_CofCT.  tf Show OfficeNo
                                    if (this.ReportFile.ToUpper().IndexOf(specificLA_COFCT) < 0)
                                    {
                                        if (this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0
                                            && this.ReportFile.ToUpper().IndexOf("GE") < 0
                                            && this.ReportFile.ToUpper().IndexOf("MB") < 0
                                            && this.ReportFile.ToUpper().IndexOf("BV") < 0
                                            && this.ReportFile.ToUpper().IndexOf("TW") < 0 //Henry add 2013-03-25 for TW
                                            )
                                        {
                                            lblAut.Text = dr["AutNoticeIssuedByInfo"].ToString();
                                            lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        lblOfficer.Text = dr["NotOfficerNo"].ToString();
                                    }
                                }

                                #region _ST
                                if (this.ReportFile.ToUpper().IndexOf("_ST") >= 0)
                                {
                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") == -1)
                                        lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                                    lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                    lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                                    lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();
                                    if (dr["NotSendTo"].ToString().Equals("P"))
                                        lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                    else
                                        lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                                    //update by Rachel 20140811 for 5337 
                                    //if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                    //    lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString())));

                                    if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                        lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]));
                                    //end update by Rachel 20140811 for 5337

                                    //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));

                                    lblOfficer.Text = dr["NotOfficerNo"].ToString(); //dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                }
                                #endregion

                                #region _FS
                                if (this.ReportFile.ToUpper().IndexOf("_FS") >= 0)
                                {
                                    lblMtrName.Text = dr["MtrName"].ToString().Trim();
                                    lblMtrDepart.Text = dr["MtrDepartName"].ToString().Trim();

                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") == -1)
                                        lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                                    lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                    lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                                    lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();
                                    if (dr["NotSendTo"].ToString().Equals("P"))
                                        lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                    else
                                        lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                                    if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                    {
                                        //update by Rachel 20140811 for 5337
                                        //lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString())));
                                        lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]));
                                        //end update by Rachel 20140811 for 5337

                                        lblMtrPostAddr.Text = (dr["MtrPostAddr1"].ToString().Trim().Equals("") ? "" : dr["MtrPostAddr1"].ToString().Trim() + "\n")
                                        + (dr["MtrPostAddr2"].ToString().Trim().Equals("") ? "" : dr["MtrPostAddr2"].ToString().Trim() + "\n")
                                        + (dr["MtrPostAddr3"].ToString().Trim().Equals("") ? "" : dr["MtrPostAddr3"].ToString().Trim() + "\n")
                                        + (dr["MtrPostCode"].ToString().Trim().Equals("") ? "" : dr["MtrPostCode"].ToString().Trim());
                                    }
                                    lblOfficer.Text = dr["NotOfficerNo"].ToString();
                                }
                                #endregion

                                #region _RM
                                if (this.ReportFile.ToUpper().IndexOf("_RM") >= 0)
                                {
                                    lblAutDepartName.Text = dr["AutDepartName"].ToString();
                                    lbl_AuthName.Text = dr["AutName"].ToString();
                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") == -1)
                                        lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                                    lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                    lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                                    lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();
                                    if (dr["NotSendTo"].ToString().Equals("P"))
                                        lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                    else
                                        lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                                    //update by Rachel 20140811 for 5337
                                    //if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                    //    lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString())));
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                        lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]));
                                    //end update by Rachel 20140811 for 5337
                                    
                                    lblOfficer.Text = dr["NotOfficerNo"].ToString();
                                    lblCourtName.Text = dr["NotCourtName"].ToString();


                                }
                                #endregion

                                #region _SW
                                if (this.ReportFile.ToUpper().IndexOf("_SW") >= 0)
                                {
                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                    //lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                                    //lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();
                                    lblPaymentInfo.Text = dr["AutNoticePaymentInfo"].ToString().Trim();
                                    //if (dr["NotSendTo"].ToString().Equals("P"))
                                    //    lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                    //else
                                    //    lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                                    //update by Rachel 20140811 for 5337
                                    //if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                    //    lblFineAmount.Text = string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                        lblFineAmount.Text = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]);
                                    //end update by Rachel 20140811 for 5337

                                    lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    //lblOfficer.Text = dr["NotOfficerNo"].ToString(); //dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    lblReferenceB.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblReferenceB.Text"), dr["NotTicketNo"].ToString());
                                    ////mrs 20090108 still need to set the label - no easypay receiver ID at the moment
                                    //if (this.ReportFile.IndexOf("Aog") == -1)
                                    //    lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                                }
                                #endregion
                                //Modify by Tod zhang on 090724 to make the the distinction between _COFCT and _RLV_COFCT
                                if (this.ReportFile.ToUpper().IndexOf(specificLA_COFCT) >= 0 && this.ReportFile.ToUpper().IndexOf("_RLV") < 0)           //&& this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                {
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                    {
                                        lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                        //lblReference.Text = "VERWYSING/REFERENCE: " + dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                        lblOfficer.Text = dr["NotOfficerNo"].ToString();
                                        lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]));

                                        lblpaypoint.Text = (dr["MtrPayPoint1Add1"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add1"].ToString().Trim() + "\n")
                                            + (dr["MtrPayPoint1Add2"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add2"].ToString().Trim() + "\n")
                                            + (dr["MtrPayPoint1Add3"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add3"].ToString().Trim() + "\n")
                                            + (dr["MtrPayPoint1Add4"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add4"].ToString().Trim() + "\n")
                                            + dr["MtrPayPoint1Code"].ToString();


                                    }

                                    if (dr["NotSendTo"].ToString().Equals("P"))
                                        lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                    else
                                        lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                                    //dls 090720 - added print file name for CoCT
                                    lblPrintFileNameName.Text = "Batch Number: " + dr["NotCiprusPrintReqName"].ToString();

                                    int page = noOfNotices + 1;
                                    PageNumberingLabel1.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "PageNumberingLabel1.Text"), page.ToString().PadLeft(4, pad0Char));
                                }

                                if (this.ReportFile.ToUpper().IndexOf(specificLA_COFCT) >= 0)
                                {
                                    if (dr["Reprint"].ToString().Equals("1") && lblReprintDate != null && lblReprintDateText != null)
                                    {
                                        lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);
                                        //lblReprintDateText.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                        lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");
                                    }
                                }

                                // FBJ (2010-09-20): New section for the CofCT RLV notice
                                // Henry 2013-03-26 modify yyyy/MM/dd to dd/MMM/yyyy
                                #region _RLV_COFCT
                                if (this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") >= 0)
                                {
                                    //barcode = new BarcodeNETImage();
                                    //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                                    //barcode.ShowBarcodeText = false;

                                    string NotOffenceDate, NotPrint1stNoticeDate;
                                    if (!string.IsNullOrEmpty(dr["NotOffenceDate"].ToString()))
                                    {
                                        NotOffenceDate = Convert.ToDateTime(dr["NotOffenceDate"]).ToString("dd MMM yyyy");
                                    }
                                    else
                                    {
                                        NotOffenceDate = "";
                                    }

                                    if (!string.IsNullOrEmpty(dr["NotPrint1stNoticeDate"].ToString()))
                                    {
                                        NotPrint1stNoticeDate = Convert.ToDateTime(dr["NotPrint1stNoticeDate"]).ToString("dd MMM yyyy");
                                    }
                                    else
                                    {
                                        NotPrint1stNoticeDate = "";
                                    }

                                    barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);



                                    lblCode.Text = dr["ChgOffenceCode"].ToString();
                                    lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");

                                    //update by Rachel 20140811 for 5337
                                    //lblFineAmount.Text = "R " + string.Format("{0:0}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                                    lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0}",dr["ChgRevFineAmount"]);
                                    //end update by Rachel 20140811 for 5337

                                    sTempEng = string.Format(GetResource("FirstNoticeViewer.aspx", "sTempEng"),
                                            dr["NotVehicleMake"].ToString().Trim(),
                                            dr["NotRegNo"].ToString().Trim(),
                                            NotOffenceDate,
                                            string.Format("{0:HH:mm}", dr["NotOffenceDate"]),
                                            dr["NotLocDescr"],
                                            dr["AutName"]
                                        );
                                    // lblOffenceDescrEng.Text 
                                    sTempAfr = string.Format(GetResource("FirstNoticeViewer.aspx", "sTempAfr"),
                                        dr["NotVehicleMake"].ToString().Trim(),
                                        dr["NotRegNo"].ToString().Trim(),
                                        NotOffenceDate,
                                        string.Format("{0:HH:mm}", dr["NotOffenceDate"]),
                                        dr["NotLocDescr"],
                                        dr["AutName"]
                                        );


                                    lblPrintDate.Text = NotPrint1stNoticeDate;

                                    if (Convert.ToInt16(dr["NoticeStatus"]) >= 250)
                                    {
                                        if (lblReprintDateText != null)
                                            lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                        if (lblReprintDate != null)
                                            lblReprintDate.Text = DateTime.Today.ToString("dd MMM yyyy");
                                    }
                                }
                                #endregion

                                #region _RLV_JMPD
                                if (this.ReportFile.ToUpper().IndexOf("_RLV_JMPD") >= 0)
                                {
                                    //barcode = new BarcodeNETImage();
                                    //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                                    //barcode.ShowBarcodeText = false;

                                    barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);


                                    if (this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") >= 0)
                                    {
                                        lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                            + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                            + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                            + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                            + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                            + dr["DrvPOCode"].ToString().Trim();
                                        if (dr["NotSendTo"].ToString().Equals("P"))
                                            lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                        else
                                            lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();
                                    }

                                    lblCode.Text = dr["ChgOffenceCode"].ToString();
                                    lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    //Add by Tod Zhang on 090723 - no these three fields lblAuthorityAddress,lblAutTel and lblAutFax in the First Notice RLV COFCT report.
                                    if (this.ReportFile.ToUpper().IndexOf("_RLV_JMPD") >= 0)
                                    {
                                        lblAuthorityAddress.Text = (dr["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : (dr["AutPhysAddr1"].ToString().Trim() + "\n"))
                                            + (dr["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : (dr["AutPhysAddr2"].ToString().Trim() + "\n"))
                                            + (dr["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : (dr["AutPhysAddr3"].ToString().Trim() + "\n"))
                                            + dr["AutPhysCode"].ToString().Trim();
                                        lblAutTel.Text = dr["AutTel"].ToString();
                                        lblAutFax.Text = dr["AutFax"].ToString();
                                    }

                                    lblOffenceDescrAfr.Text = "DEUR DAT die bestuurder van voertuig " + dr["NotVehicleMake"].ToString().Trim()
                                        + " met registrasienommer " + dr["NotRegNo"].ToString().Trim() + " op "
                                        + string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"])
                                        + " om " + string.Format("{0:HH:mm}", dr["NotOffenceDate"]) + " en te \n"
                                        + dr["NotLocDescr"].ToString() + " n openbare pad in die distrik van "
                                        + dr["AutName"].ToString() + ", n rooi verkeerslig verontagsaam";
                                    lblOffenceDescrEng.Text = "IN THAT the driver of motor vehicle " + dr["NotVehicleMake"].ToString().Trim()
                                        + " with registration number " + dr["NotRegNo"].ToString().Trim()
                                        + " on " + string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"])
                                        + " at " + string.Format("{0:HH:mm}", dr["NotOffenceDate"]) + " and at \n"
                                        + dr["NotLocDescr"].ToString() + " a public road in the district of "
                                        + dr["AutName"].ToString() + ", failed to obey a red light";

                                    lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);
                                }
                                #endregion

                                // 2011-9-7 jerry add FirstNotice_RLV_BV
                                #region _RLV_BV
                                if (this.ReportFile.ToUpper().IndexOf("_RLV_BV") >= 0)
                                {
                                    string NotOffenceDate, NotPrint1stNoticeDate;
                                    if (!string.IsNullOrEmpty(dr["NotOffenceDate"].ToString()))
                                    {
                                        NotOffenceDate = Convert.ToDateTime(dr["NotOffenceDate"]).ToString("yyyy/MM/dd");
                                    }
                                    else
                                    {
                                        NotOffenceDate = "";
                                    }

                                    if (!string.IsNullOrEmpty(dr["NotPrint1stNoticeDate"].ToString()))
                                    {
                                        NotPrint1stNoticeDate = Convert.ToDateTime(dr["NotPrint1stNoticeDate"]).ToString("yyyy/MM/dd");
                                    }
                                    else
                                    {
                                        NotPrint1stNoticeDate = "";
                                    }

                                    barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);

                                    lblCode.Text = dr["ChgOffenceCode"].ToString();
                                    lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");

                                    //update By Rachel 20140811 for 5337
                                    //lblFineAmount.Text = "R " + string.Format("{0:0}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                                    lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0}",dr["ChgRevFineAmount"]);
                                    //end update By Rachel 20140811 for 5337

                                    //lblOffenceDescrAfr.Text
                                    sTempEng = string.Format(GetResource("FirstNoticeViewer.aspx", "sTempEng"),
                                            dr["NotVehicleMake"].ToString().Trim(),
                                            dr["NotRegNo"].ToString().Trim(),
                                            NotOffenceDate,
                                            string.Format("{0:HH:mm}", dr["NotOffenceDate"]),
                                            dr["NotLocDescr"],
                                            dr["AutName"]
                                        );
                                    // lblOffenceDescrEng.Text 
                                    sTempAfr = string.Format(GetResource("FirstNoticeViewer.aspx", "sTempAfr"),
                                        dr["NotVehicleMake"].ToString().Trim(),
                                        dr["NotRegNo"].ToString().Trim(),
                                        NotOffenceDate,
                                        string.Format("{0:HH:mm}", dr["NotOffenceDate"]),
                                        dr["NotLocDescr"],
                                        dr["AutName"]
                                        );

                                    lblPrintDate.Text = NotPrint1stNoticeDate;
                                    lblAutNameEng.Text = dr["AutName"].ToString();
                                    lblAutNameAfr.Text = dr["AutNameAfrikaans"].ToString();

                                    if (Convert.ToInt16(dr["NoticeStatus"]) >= 250)
                                    {
                                        if (lblReprintDateText != null)
                                            lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                        if (lblReprintDate != null)
                                            lblReprintDate.Text = DateTime.Today.ToString("yyyy/MM/dd");
                                    }
                                }
                                #endregion

                                #region _GE and _BV
                                if (this.ReportFile.ToUpper().IndexOf("_GE") >= 0 || this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE_BV") >= 0 || this.ReportFile.ToUpper().IndexOf("NOAOG_BV") >= 0)
                                {
                                    //barcode = new BarcodeNETImage();
                                    //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                                    //barcode.ShowBarcodeText = false;

                                    barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);
                                    if (this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE_GE") >= 0)
                                    {
                                        lbl_AutPostAddr.Text = (dr["AutPostAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPostAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPostAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr3"].ToString().Trim() + "\n")
                                        + dr["AutPostCode"].ToString();
                                        lblpaypoint.Text = (dr["MtrPayPoint1Add1"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add1"].ToString().Trim() + "\n")
                                            + (dr["MtrPayPoint1Add2"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add2"].ToString().Trim() + "\n")
                                            + (dr["MtrPayPoint1Add3"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add3"].ToString().Trim() + "\n")
                                            + (dr["MtrPayPoint1Add4"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add4"].ToString().Trim() + "\n")
                                            + dr["MtrPayPoint1Code"].ToString();

                                        //Jerry 2012-05-03 add
                                        if (this.ReportFile.ToUpper().IndexOf("AOG") == -1)
                                            lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                                    }
                                    if (dr["NotSendTo"].ToString().Equals("P"))
                                    {
                                        lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                        lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                    }
                                    else
                                    {
                                        lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();
                                        lblId.Text = dr["DrvIDNUmber"].ToString().Trim();
                                    }
                                    // add by richard 2011-05-27
                                    lbl_AuthName.Text = dr["AutName"].ToString();

                                    lblAutPhyAddr.Text = (dr["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr3"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr4"].ToString().Trim() + "\n")
                                        + dr["AutPhysCode"].ToString();

                                    //client has changed there mind about the Court Name on the BV notice
                                    if (lblCourt != null)
                                        lblCourt.Text = dr["NotCourtName"].ToString();

                                    if (lbl_AuthName2 != null)
                                        lbl_AuthName2.Text = dr["AutName"].ToString();

                                    //end


                                    lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                        + dr["DrvPOCode"].ToString().Trim();

                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    lblStatRef.Text = (dr["ChgStatRefLine1"] == DBNull.Value ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                            + (dr["ChgStatRefLine2"] == DBNull.Value ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                            + (dr["ChgStatRefLine3"] == DBNull.Value ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                            + (dr["ChgStatRefLine4"] == DBNull.Value ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                                    sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                    sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());

                                    lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                    lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);

                                    lblLocDescr.Text = dr["NotLocDescr"].ToString();

                                    if (dr["NotNewOffender"].ToString().ToUpper().Equals("Y"))
                                    {
                                        lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                        lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                                    }
                                    else
                                    {
                                        lblOffenceDescrEng.Text = sTempEng;
                                        lblOffenceDescrAfr.Text = sTempAfr;
                                    }

                                    lblCode.Text = dr["ChgOffenceCode"].ToString();

                                    if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                    {
                                        lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();

                                        lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPaymentDate"]);

                                        //update by Rachel 20140811 for 5337
                                        //lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString())));
                                        lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]));
                                        //end update by Rachel 20140811 for 5337

                                        lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                        lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                                    }

                                    if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0)
                                    {
                                        lblCameraA.Text = dr["CameraNoA"].ToString();
                                        lblCameraB.Text = dr["CameraNoB"].ToString();
                                        if (lblMeasurement != null)
                                            lblMeasurement.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblMeasurement.Text"), dr["MeasurementDistance"].ToString());
                                        if (lblTimeDifference != null)
                                            lblTimeDifference.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblTimeDifference.Text"), dr["TimeDifference"].ToString());

                                        //if (dr["GpsXA"] != DBNull.Value && dr["GpsYA"] != DBNull.Value)
                                        //{
                                        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblGpsA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblGpsA");
                                        //    lblGpsA.Text = string.Format("{0}, {1}", dr["GpsXA"].ToString(), dr["GpsYA"].ToString());
                                        //}                                                

                                        if (dr["ASDGPSDateTime1"] != DBNull.Value)
                                        {
                                            lblGpsTimeA.Text = string.Format("(Time{0: HH:mm})", (DateTime)dr["ASDGPSDateTime1"]);
                                        }

                                        //if (dr["GpsXB"] != DBNull.Value && dr["GpsYB"] != DBNull.Value)
                                        //{
                                        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblGpsB = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblGpsB");
                                        //    lblGpsB.Text = string.Format("{0}, {1}", dr["GpsXB"].ToString(), dr["GpsYB"].ToString());
                                        //}

                                        if (dr["ASDGPSDateTime2"] != DBNull.Value)
                                        {
                                            lblGpsTimeB.Text = string.Format("(Time{0: HH:mm})", (DateTime)dr["ASDGPSDateTime2"]);
                                        }
                                    }
                                    else
                                    {
                                        lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                    }

                                    lblOfficerNo.Text = dr["NotOfficerNo"].ToString();
                                    lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);

                                    if (PrintFileName.Length > 3 && !PrintFileName.Substring(0, 3).Equals("NAG"))
                                    {
                                        if (lblDisclaimer != null)
                                            lblDisclaimer.Text = dr["Disclaimer"].ToString();
                                    }
                                    lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                    //By seawen 2013-04-24
                                    if (this.ReportFile.ToUpper().IndexOf("BUS") >= 0)
                                    {
                                        string time1 = string.Format("{0:HH:mm}", dr["ASDGPSDateTime1"]);
                                        string time2 = string.Format("{0:HH:mm}", dr["ASDGPSDateTime2"]);
                                        lblOffenceTime.Text = string.Format("Between {0} and {1}", time1, time2);
                                    }
                                    else
                                    {
                                        lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                    }
                                    lblLocation.Text = dr["NotLocDescr"].ToString();
                                    if (lblSpeedLimit != null)
                                        lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                                    if (lblSpeed != null)
                                        lblSpeed.Text = dr["NotSpeed1"].ToString(); ;
                                    lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    lblRegNo.Text = dr["NotRegNo"].ToString();

                                    //Jerry 2012-06-14 add
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") > 0)
                                    {
                                        if (dr["NotOriginalPrintDate"] != System.DBNull.Value)
                                        {
                                            if (lblReprintDateText != null)
                                                lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                            if (lblReprintDate != null)
                                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                        }
                                    }
                                    else
                                    {
                                        //dls 071221 - added reprint date
                                        //if (Convert.ToInt16(dr["ChargeStatus"]) >= 250)
                                        // Oscar 2010-12 changed for 4416
                                        if (Convert.ToInt16(dr["NoticeStatus"]) >= 250)
                                        {
                                            //dls 081111 - FristNotice.dplx does not have these fields
                                            if (lblReprintDateText != null)
                                                lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                            if (lblReprintDate != null)
                                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                        }
                                    }
                                }
                                #endregion

                                //2013-03-25 add by Henry for _TW
                                #region _TW
                                if (this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE_TW") >= 0 || this.ReportFile.ToUpper().IndexOf("NOAOG_TW") >= 0)
                                {
                                    //barcode = new BarcodeNETImage();
                                    //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                                    //barcode.ShowBarcodeText = false;

                                    barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);
                                    if (dr["NotSendTo"].ToString().Equals("P"))
                                    {
                                        lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                        lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                    }
                                    else
                                    {
                                        lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();
                                        lblId.Text = dr["DrvIDNUmber"].ToString().Trim();
                                    }
                                    // add by richard 2011-05-27
                                    lbl_AuthName.Text = dr["AutName"].ToString();

                                    lblAutPhyAddr.Text = (dr["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr3"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr4"].ToString().Trim() + "\n")
                                        + dr["AutPhysCode"].ToString();

                                    //client has changed there mind about the Court Name on the BV notice
                                    if (lblCourt != null)
                                        lblCourt.Text = dr["NotCourtName"].ToString();

                                    if (lbl_AuthName2 != null)
                                        lbl_AuthName2.Text = dr["AutName"].ToString();

                                    //end


                                    lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                        + dr["DrvPOCode"].ToString().Trim();

                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    lblStatRef.Text = (dr["ChgStatRefLine1"] == DBNull.Value ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                            + (dr["ChgStatRefLine2"] == DBNull.Value ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                            + (dr["ChgStatRefLine3"] == DBNull.Value ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                            + (dr["ChgStatRefLine4"] == DBNull.Value ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                                    sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                    sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());

                                    lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                    lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);

                                    lblLocDescr.Text = dr["NotLocDescr"].ToString();

                                    if (dr["NotNewOffender"].ToString().ToUpper().Equals("Y"))
                                    {
                                        lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                        lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                                    }
                                    else
                                    {
                                        lblOffenceDescrEng.Text = sTempEng;
                                        lblOffenceDescrAfr.Text = sTempAfr;
                                    }

                                    lblCode.Text = dr["ChgOffenceCode"].ToString();

                                    if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                    {
                                        lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();

                                        lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPaymentDate"]);

                                        //update by Rachel 20140811  for 5337
                                        //lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString())));
                                        lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]));
                                        //end update by Rachel 20140811  for 5337

                                        lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                        lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                                    }

                                    if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0)
                                    {
                                        lblCameraA.Text = dr["CameraNoA"].ToString();
                                        lblCameraB.Text = dr["CameraNoB"].ToString();
                                        lblMeasurement.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblMeasurement.Text"), dr["MeasurementDistance"].ToString());
                                        lblTimeDifference.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblTimeDifference.Text"), dr["TimeDifference"].ToString());

                                        //if (dr["GpsXA"] != DBNull.Value && dr["GpsYA"] != DBNull.Value)
                                        //{
                                        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblGpsA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblGpsA");
                                        //    lblGpsA.Text = string.Format("{0}, {1}", dr["GpsXA"].ToString(), dr["GpsYA"].ToString());
                                        //}                                                

                                        if (dr["ASDGPSDateTime1"] != DBNull.Value)
                                        {
                                            lblGpsTimeA.Text = string.Format("(Time{0: HH:mm})", (DateTime)dr["ASDGPSDateTime1"]);
                                        }

                                        //if (dr["GpsXB"] != DBNull.Value && dr["GpsYB"] != DBNull.Value)
                                        //{
                                        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblGpsB = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblGpsB");
                                        //    lblGpsB.Text = string.Format("{0}, {1}", dr["GpsXB"].ToString(), dr["GpsYB"].ToString());
                                        //}

                                        if (dr["ASDGPSDateTime2"] != DBNull.Value)
                                        {
                                            lblGpsTimeB.Text = string.Format("(Time{0: HH:mm})", (DateTime)dr["ASDGPSDateTime2"]);
                                        }
                                    }
                                    else
                                    {
                                        lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                    }

                                    lblOfficerNo.Text = dr["NotOfficerNo"].ToString();
                                    lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);

                                    if (PrintFileName.Length > 3 && !PrintFileName.Substring(0, 3).Equals("NAG"))
                                    {
                                        if (lblDisclaimer != null)
                                            lblDisclaimer.Text = dr["Disclaimer"].ToString();
                                    }
                                    lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                    lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                    lblLocation.Text = dr["NotLocDescr"].ToString();
                                    lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                                    lblSpeed.Text = dr["NotSpeed1"].ToString();
                                    lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    lblRegNo.Text = dr["NotRegNo"].ToString();

                                    //Jerry 2012-06-14 add
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") > 0)
                                    {
                                        if (dr["NotOriginalPrintDate"] != System.DBNull.Value)
                                        {
                                            if (lblReprintDateText != null)
                                                lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                            if (lblReprintDate != null)
                                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                        }
                                    }
                                    else
                                    {
                                        //dls 071221 - added reprint date
                                        //if (Convert.ToInt16(dr["ChargeStatus"]) >= 250)
                                        // Oscar 2010-12 changed for 4416
                                        if (Convert.ToInt16(dr["NoticeStatus"]) >= 250)
                                        {
                                            //dls 081111 - FristNotice.dplx does not have these fields
                                            if (lblReprintDateText != null)
                                                lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                            if (lblReprintDate != null)
                                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                        }
                                    }
                                }
                                #endregion

                                //2010/9/14 jerry changed for MB
                                #region _MB
                                else if (this.ReportFile.ToUpper().IndexOf("_MB") >= 0)
                                {
                                    //barcode = new BarcodeNETImage();
                                    //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                                    //barcode.ShowBarcodeText = false;

                                    barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);
                                    if (this.ReportFile.ToUpper().IndexOf("NOAOG_MB") < 0)
                                    {

                                        lbl_Court.Text = dr["NotCourtName"].ToString();
                                        lbl_AutPostAddr.Text = (dr["AutPostAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPostAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPostAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr3"].ToString().Trim() + "\n")
                                        + dr["AutPostCode"].ToString();

                                        lbl_AuthName2.Text = dr["AutName"].ToString();
                                    }

                                    //add by richard 
                                    lbl_AuthName.Text = dr["AutName"].ToString();

                                    lblAutPhyAddr.Text = (dr["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr3"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr4"].ToString().Trim() + "\n")
                                        + dr["AutPhysCode"].ToString();
                                    lblCourt.Text = dr["NotCourtName"].ToString();



                                    if (dr["NotSendTo"].ToString().Equals("P"))
                                    {
                                        lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                        lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                    }
                                    else
                                    {
                                        lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();
                                        lblId.Text = dr["DrvIDNUmber"].ToString().Trim();
                                    }

                                    lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                        + dr["DrvPOCode"].ToString().Trim();

                                    lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                    lblStatRef.Text = (dr["ChgStatRefLine1"] == DBNull.Value ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                            + (dr["ChgStatRefLine2"] == DBNull.Value ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                            + (dr["ChgStatRefLine3"] == DBNull.Value ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                            + (dr["ChgStatRefLine4"] == DBNull.Value ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                                    sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                    sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());

                                    lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                    lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);

                                    lblLocDescr.Text = dr["NotLocDescr"].ToString();

                                    if (dr["NotNewOffender"].ToString().ToUpper().Equals("Y"))
                                    {
                                        lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                        lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                                    }
                                    else
                                    {
                                        lblOffenceDescrEng.Text = sTempEng;
                                        lblOffenceDescrAfr.Text = sTempAfr;
                                    }

                                    lblCode.Text = dr["ChgOffenceCode"].ToString();

                                    if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                    {
                                        //lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();

                                        lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPaymentDate"]);

                                        // update by Rachel 20140811 for 5337
                                        //lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString())));
                                        lblFineAmount.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]));
                                        //end update by Rachel 20140811 for 5337

                                        lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                        lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                                    }

                                    if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0)
                                    {
                                        lblCameraA.Text = dr["CameraNoA"].ToString();
                                        lblCameraB.Text = dr["CameraNoB"].ToString();
                                        if (lblMeasurement != null)
                                            lblMeasurement.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblMeasurement.Text"), dr["MeasurementDistance"].ToString());
                                        if (lblTimeDifference != null)
                                            lblTimeDifference.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblTimeDifference.Text"), dr["TimeDifference"].ToString());

                                        //if (dr["GpsXA"] != DBNull.Value && dr["GpsYA"] != DBNull.Value)
                                        //{
                                        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblGpsA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblGpsA");
                                        //    lblGpsA.Text = string.Format("{0}, {1}", dr["GpsXA"].ToString(), dr["GpsYA"].ToString());
                                        //}                                                

                                        if (dr["ASDGPSDateTime1"] != DBNull.Value)
                                        {
                                            lblGpsTimeA.Text = string.Format("(Time{0: HH:mm})", (DateTime)dr["ASDGPSDateTime1"]);
                                        }

                                        //if (dr["GpsXB"] != DBNull.Value && dr["GpsYB"] != DBNull.Value)
                                        //{
                                        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblGpsB = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblGpsB");
                                        //    lblGpsB.Text = string.Format("{0}, {1}", dr["GpsXB"].ToString(), dr["GpsYB"].ToString());
                                        //}

                                        if (dr["ASDGPSDateTime2"] != DBNull.Value)
                                        {
                                            lblGpsTimeB.Text = string.Format("(Time{0: HH:mm})", (DateTime)dr["ASDGPSDateTime2"]);
                                        }
                                    }
                                    else
                                    {
                                        lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                    }
                                    lblOfficerNo.Text = dr["NotOfficerNo"].ToString();
                                    lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);

                                    if (PrintFileName.Length > 3 && !PrintFileName.Substring(0, 3).Equals("NAG"))
                                    {
                                        if (lblDisclaimer != null)
                                            lblDisclaimer.Text = dr["Disclaimer"].ToString();
                                    }
                                    lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                    //By seawen 2013-04-24
                                    if (this.ReportFile.ToUpper().IndexOf("BUS") >= 0)
                                    {
                                        string time1 = string.Format("{0:HH:mm}", dr["ASDGPSDateTime1"]);
                                        string time2 = string.Format("{0:HH:mm}", dr["ASDGPSDateTime2"]);
                                        lblOffenceTime.Text = string.Format("Between {0} and {1}", time1, time2);
                                    }
                                    else
                                    {
                                        lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                    }
                                    lblLocation.Text = dr["NotLocDescr"].ToString();
                                    if (lblSpeedLimit != null)
                                        lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                                    if (lblSpeed != null)
                                        lblSpeed.Text = dr["NotSpeed1"].ToString();
                                    lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                    lblRegNo.Text = dr["NotRegNo"].ToString();

                                    //Jerry 2012-06-14 add
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") > 0)
                                    {
                                        if (dr["NotOriginalPrintDate"] != System.DBNull.Value)
                                        {
                                            if (lblReprintDateText != null)
                                                lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                            if (lblReprintDate != null)
                                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                        }
                                    }
                                    else
                                    {
                                        //dls 071221 - added reprint date
                                        //if (Convert.ToInt16(dr["ChargeStatus"]) >= 250)
                                        // Oscar 2010-12 changed for 4416
                                        if (Convert.ToInt16(dr["NoticeStatus"]) >= 250)
                                        {
                                            //dls 081111 - FristNotice.dplx does not have these fields
                                            if (lblReprintDateText != null)
                                                lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                            if (lblReprintDate != null)
                                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                        }
                                    }
                                }
                                #endregion
                                #region else
                                else
                                {
                                    if (dr["NotSendTo"].ToString().Equals("P"))
                                        lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                    else
                                        lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();

                                    lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                        + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                        + dr["DrvPOCode"].ToString().Trim();

                                    if (this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0)
                                    {
                                        //barcode = new BarcodeNETImage();
                                        //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                                        //barcode.ShowBarcodeText = false;

                                        barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);
                                    }

                                    if (this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") < 0 && this.ReportFile.ToUpper().IndexOf("_RLV_BV") < 0)
                                    {
                                        if (this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0)
                                        {
                                            lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                            lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                        }
                                        else
                                        {
                                            lblDate.Text =
                                                string.Format("{0:yyyy/MM/dd}", dr["NotOffenceDate"]) + "  " +
                                                string.Format("{0:HH:mm:ss}", dr["NotOffenceDate"]);
                                        }

                                        lblLocDescr.Text = dr["NotLocDescr"].ToString();

                                        if (this.ReportFile.ToUpper().IndexOf("_CPI") >= 0)
                                        {
                                            lblStatRef.Text = dr["ChgStatutoryRef"].ToString().Trim();
                                            sTempEng = dr["OcTDescr1"].ToString().Replace("*001      ", dr["NotRegNo"].ToString()).Replace("*002", dr["MinSpeed"].ToString());
                                            sTempAfr = dr["OcTDescr"].ToString().Replace("*001      ", dr["NotRegNo"].ToString()).Replace("*002", dr["MinSpeed"].ToString());
                                        }
                                        else
                                        {
                                            lblStatRef.Text = (dr["ChgStatRefLine1"] == DBNull.Value ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                                + (dr["ChgStatRefLine2"] == DBNull.Value ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                                + (dr["ChgStatRefLine3"] == DBNull.Value ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                                + (dr["ChgStatRefLine4"] == DBNull.Value ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                                            sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                            sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                        }
                                    }

                                    if (dr["NotNewOffender"].ToString().ToUpper().Equals("Y"))
                                    {
                                        lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                        if (this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0)
                                        {
                                            lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                                        }
                                    }
                                    else
                                    {
                                        lblOffenceDescrEng.Text = sTempEng;
                                        if (this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0)
                                        {
                                            lblOffenceDescrAfr.Text = sTempAfr;
                                        }
                                    }

                                    if (this.ReportFile.ToUpper().IndexOf("AOG") == -1 && this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") < 0 && this.ReportFile.ToUpper().IndexOf("_RLV_BV") < 0)
                                    {
                                        lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPaymentDate"]);
                                        //2014-12-22 Heidi fixed issue that it be not find the object in the GE template.(5379)
                                        if (this.ReportFile.ToUpper().IndexOf("_GE") <= 0)
                                        {
                                            lblAut.Text = dr["AutNoticeIssuedByInfo"].ToString();
                                        }

                                        //dls 071221 - added reprint date
                                        //if (Convert.ToInt16(dr["ChargeStatus"]) >= 250)
                                        // Oscar 2010-12 changed for 4416
                                        if (Convert.ToInt16(dr["NoticeStatus"]) >= 250)
                                        {
                                            //dls 081111 - FristNotice.dplx does not have these fields
                                            if (lblReprintDateText != null)
                                            {
                                                lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");
                                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                            }
                                        }
                                    }

                                    //Jerry 2012-06-14 add
                                    if (this.ReportFile.ToUpper().IndexOf("AOG") > 0 && this.ReportFile.ToUpper().IndexOf(specificLA_COFCT) < 0)
                                    {
                                        if (dr["NotOriginalPrintDate"] != System.DBNull.Value)
                                        {
                                            if (lblReprintDateText != null)
                                                lblReprintDateText.Text = GetResource("FirstNoticeViewer.aspx", "lblReprintDateText.Text");

                                            if (lblReprintDate != null)
                                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                                        }
                                    }


                                    lblCode.Text = dr["ChgOffenceCode"].ToString();

                                    if (this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") < 0 && this.ReportFile.ToUpper().IndexOf("_RLV_BV") < 0)
                                    {
                                        if (this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0)
                                        {

                                            lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();

                                            //lblCode.Text = dr["ChgOffenceCode"].ToString();
                                            if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0)
                                            {
                                                lblCameraA.Text = dr["CameraNoA"].ToString();
                                                lblCameraB.Text = dr["CameraNoB"].ToString();
                                                if (lblMeasurement != null)
                                                    lblMeasurement.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblMeasurement.Text"), dr["MeasurementDistance"].ToString());
                                                if (lblTimeDifference != null)
                                                    lblTimeDifference.Text = string.Format(GetResource("FirstNoticeViewer.aspx", "lblTimeDifference.Text"), dr["TimeDifference"].ToString());

                                                //if (dr["GpsXA"] != DBNull.Value && dr["GpsYA"] != DBNull.Value)
                                                //{
                                                //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblGpsA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblGpsA");
                                                //    lblGpsA.Text = string.Format("{0}, {1}", dr["GpsXA"].ToString(), dr["GpsYA"].ToString());
                                                //}                                                

                                                if (dr["ASDGPSDateTime1"] != DBNull.Value)
                                                {
                                                    lblGpsTimeA.Text = string.Format("Time{0: HH:mm}", (DateTime)dr["ASDGPSDateTime1"]);
                                                }

                                                //if (dr["GpsXB"] != DBNull.Value && dr["GpsYB"] != DBNull.Value)
                                                //{
                                                //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblGpsB = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblGpsB");
                                                //    lblGpsB.Text = string.Format("{0}, {1}", dr["GpsXB"].ToString(), dr["GpsYB"].ToString());
                                                //}

                                                if (dr["ASDGPSDateTime2"] != DBNull.Value)
                                                {
                                                    lblGpsTimeB.Text = string.Format("Time{0: HH:mm}", (DateTime)dr["ASDGPSDateTime2"]);
                                                }
                                            }
                                            else
                                            {
                                                lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                            }

                                            lblOfficerNo.Text = dr["NotOfficerNo"].ToString();
                                            //2014-12-22 Heidi fixed issue that it be not find the object in the GE template.(5379)
                                            if (this.ReportFile.ToUpper().IndexOf("_GE")<=0)
                                            {
                                                lblIssuedBy.Text = dr["AutNoticeIssuedByInfo"].ToString();
                                            }

                                            //not used by NoAOG --> Moved into section above
                                            //lblAut.Text = dr["AutNoticeIssuedByInfo"].ToString();
                                            lblPrintDate.Text = string.Format("{0:d MMMM yyyy}",
                                                                                dr["NotPrint1stNoticeDate"]);

                                            if (this.ReportFile.ToUpper().IndexOf(specificLA_COFCT) >= 0)
                                            {
                                                if (dr["Reprint"].ToString().Equals("1") &&
                                                    dr["NotOriginalPrintDate"] != System.DBNull.Value)
                                                {
                                                    lblPrintDate.Text = string.Format("{0:d MMMM yyyy}",
                                                                                        dr["NotOriginalPrintDate"]);
                                                }
                                            }

                                            lblOffenceDate.Text = string.Format(
                                                this.ReportFile.ToUpper().IndexOf("_COFCT") >= 0 ? "{0:dd MMM yyyy}" : "{0:yyyy-MM-dd}",
                                                                                dr["NotOffenceDate"]);
                                            //By seawen 2013-04-24
                                            if (this.ReportFile.ToUpper().IndexOf("BUS") >= 0)
                                            {
                                                string time1 = string.Format("{0:HH:mm}", dr["ASDGPSDateTime1"]);
                                                string time2 = string.Format("{0:HH:mm}", dr["ASDGPSDateTime2"]);
                                                lblOffenceTime.Text = string.Format("Between {0} and {1}", time1, time2);
                                            }
                                            else
                                            {
                                                lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                            }
                                            lblLocation.Text = dr["NotLocDescr"].ToString();
                                            // jerry 2011-09-26 add FIRSTNOTICE_RLV_ST
                                            if (this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE_RLV_ST") < 0)
                                            {
                                                if (lblSpeedLimit != null)
                                                    lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                                            }
                                            if (lblSpeed != null)
                                                lblSpeed.Text = dr["NotSpeed1"].ToString();
                                            //lblOfficer.Text = dr["NotOfficerNo"].ToString(); //dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                            lblRegNo.Text = dr["NotRegNo"].ToString();
                                        }
                                        else
                                        {
                                            //dls 2010-06-24 need to use the print date evne tho the label says otherwise
                                            //lblIssue1stNoticeDate.Text = string.Format("{0:yyyy/MM/dd}", dr["NotIssue1stNoticeDate"]);
                                            lblIssue1stNoticeDate.Text = string.Format("{0:yyyy/MM/dd}",
                                                                                        dr[
                                                                                            "NotPrint1stNoticeDate"]);
                                        }
                                    }
                                }
                                #endregion

                                #region bus lane
                                if (this.ReportFile.ToUpper().IndexOf("BUS") >= 0)
                                {
                                    //by Seawen 2013-05-07
                                    string siteCode1 = (dr["LSSiteCode1"] == null ? "" : dr["LSSiteCode1"].ToString());
                                    string siteCode2 = (dr["LSSiteCode2"] == null ? "" : dr["LSSiteCode2"].ToString());
                                    string siteDesc1 = (dr["LSSiteDescription1"] == null ? "" : dr["LSSiteDescription1"].ToString());
                                    string siteDesc2 = (dr["LSSiteDescription2"] == null ? "" : dr["LSSiteDescription2"].ToString());

                                    if (!string.IsNullOrEmpty(siteCode1) && !string.IsNullOrEmpty(siteDesc1))
                                        lblLSSiteCode1.Text = string.Format("Camera {0} {1}", siteCode1, siteDesc1);
                                    if (!string.IsNullOrEmpty(siteCode2) && !string.IsNullOrEmpty(siteDesc2))
                                        lblLSSiteCode2.Text = string.Format("Camera {0} {1}", siteCode2, siteDesc2);
                                }
                                #endregion

                                //Jake added check, no need to set this field if print single section 35 notice
                                //mrs 20080212 added disclaimer
                                if (PrintFileName.Length > 3 && !PrintFileName.Contains("NAG") && this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") < 0 && this.ReportFile.ToUpper().IndexOf("_RLV_BV") < 0)
                                {
                                    if (lblDisclaimer != null)
                                    {
                                        lblDisclaimer.Text = dr["Disclaimer"].ToString();
                                    }
                                }
                                //ScanImageDB imgDB = new ScanImageDB(this.SubProcess.ConnectionString);
                                #region (this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") < 0 && this.ReportFile.ToUpper().IndexOf("_RLV_BV") < 0)
                                if (this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") < 0 && this.ReportFile.ToUpper().IndexOf("_RLV_BV") < 0)
                                {
                                    try
                                    {
                                        // David Lin 20100810 Add ImageA and ImageB for SAD
                                        // david lin 20100323 Remove images from database
                                        //mainImage = dr["ScanImage1"] == System.DBNull.Value ? null : (byte[])dr["ScanImage1"];
                                        if (dr["ScanImage1"] != System.DBNull.Value)
                                        {
                                            ScanImageDetails image1 =
                                                this.imgDB.GetImageFullPath(Convert.ToInt32(dr["ScanImage1"]));
                                            // jerry 2011-09-26 add FIRSTNOTICE_RLV_ST
                                            if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0 || this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE_RLV_ST") >= 0)
                                            {
                                                imageA = ImageHelper.GetImagesFromRemoteFileServer(image1);
                                                // update notice and charge status if missing image
                                                if (imageA == null || imageA.Length == 0)
                                                {
                                                    string strError = string.Empty;
                                                    int val = imgDB.UpdateNoticeChargeMissImage(Convert.ToInt32(dr["ScanImage1"]), out strError, this.SubProcess.LastUser);
                                                    if (val < 0 || strError.Length > 0)
                                                    {
                                                        this.ErrorProcessing(string.Format(GetResource("FirstNoticeViewer.aspx", "strWriteMsg1"), dr["NotTicketNo"].ToString().Trim(), image1.ImageFullPath));
                                                        return;
                                                    }
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                mainImage = ImageHelper.GetImagesFromRemoteFileServer(image1);

                                                // update notice and charge status if missing image
                                                if (mainImage == null || mainImage.Length == 0)
                                                {
                                                    string strError = string.Empty;
                                                    int val =
                                                        imgDB.UpdateNoticeChargeMissImage(
                                                            Convert.ToInt32(dr["ScanImage1"]), out strError,
                                                            this.SubProcess.LastUser);
                                                    if (val < 0 || strError.Length > 0)
                                                    {
                                                        this.ErrorProcessing(string.Format(
                                                                GetResource("FirstNoticeViewer.aspx", "strWriteMsg1"),
                                                                dr["NotTicketNo"].ToString().Trim(),
                                                                image1.ImageFullPath));
                                                        return;
                                                    }
                                                    continue;
                                                }
                                            }
                                        }

                                        if (dr["ScanImage2"] != System.DBNull.Value)
                                        {
                                            ScanImageDetails image2 = this.imgDB.GetImageFullPath(Convert.ToInt32(dr["ScanImage2"]));
                                            // jerry 2011-09-26 add FIRSTNOTICE_RLV_ST
                                            if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0 || this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE_RLV_ST") >= 0)
                                            {
                                                imageB = ImageHelper.GetImagesFromRemoteFileServer(image2);
                                            }
                                        }

                                        //091014 FT apply image contrast and brightness setting.
                                        if (mainImage != null && allowImageSettings && (dr["Contrast"] != System.DBNull.Value || dr["Brightness"] != System.DBNull.Value))
                                        {
                                            System.Drawing.Bitmap Bitimage = null;

                                            using (MemoryStream ms = new MemoryStream(mainImage))
                                            {
                                                Bitimage = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms);
                                                if (dr["Contrast"] != System.DBNull.Value)
                                                {
                                                    ImageProcesses.Contrast(Bitimage, float.Parse(dr["Contrast"].ToString()));
                                                }

                                                if (dr["Brightness"] != System.DBNull.Value)
                                                {
                                                    ImageProcesses.BrightNess(Bitimage, float.Parse(dr["Brightness"].ToString()));
                                                }

                                                MemoryStream newStream = new MemoryStream();
                                                Bitimage.Save(newStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                                mainImage = newStream.GetBuffer();
                                                newStream.Dispose();
                                            }

                                            Bitimage.Dispose();
                                        }

                                        // add cross-hair processing
                                        int X = Convert.ToInt32(dr["XValue1"]);
                                        int Y = Convert.ToInt32(dr["YValue1"]);

                                        if (mainImage != null && showCrossHairs && X > 0 && Y > 0)
                                        {
                                            Drawing draw = new Drawing();
                                            System.Drawing.Image image = draw.CrossHairs(mainImage,
                                                                                            crossHairStyle.ToString
                                                                                                (), X, Y);
                                            MemoryStream ms = new MemoryStream();
                                            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                                            mainImage = ms.ToArray();
                                            ms.Dispose();
                                        }

                                        //091014 add regno image for COFCT
                                        // jerry 2011-09-26 add FIRSTNOTICE_RLV_ST
                                        if ((this.ReportFile.ToUpper().IndexOf("SW") > 0 && this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                                            || (this.ReportFile.ToUpper().IndexOf(specificLA_COFCT) > 0 && this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE") >= 0 && this.ReportFile.ToUpper().IndexOf("RLV") < 0)
                                            || ((this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0) && (this.ReportFile.ToUpper().IndexOf("COFCT") > 0)) || this.ReportFile.ToUpper().IndexOf("_ST") >= 0
                                            )
                                        {
                                            if (dr["ScanImage3"] != System.DBNull.Value)
                                            {
                                                ScanImageDetails image3 =
                                                    imgDB.GetImageFullPath(Convert.ToInt32(dr["ScanImage3"]));
                                                regNoImage = ImageHelper.GetImagesFromRemoteFileServer(image3);
                                            }
                                            else
                                            {
                                                regNoImage = null;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //System.Diagnostics.Debug.WriteLine("ERROR: " + ex);
                                        ErrorProcessing(ex.Message);
                                        return;
                                    }
                                }
                                #endregion

                                report = doc.Run(parameters);
                                ImportedPageArea importedPage;
                                byte[] bufferTemplate;
                                if (!TemplateFile.Equals(""))
                                {
                                    if (TemplateFile.ToLower().IndexOf(".dplx") > 0)
                                    {
                                        DocumentLayout template = new DocumentLayout(TemplateFilePath);
                                        StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                        queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                                        ParameterDictionary parameterTemplateFile = new ParameterDictionary();
                                        Document reportTemplate = template.Run(parameterTemplateFile);
                                        bufferTemplate = reportTemplate.Draw();
                                        PdfDocument pdf = new PdfDocument(bufferTemplate);
                                        PdfPage page = pdf.Pages[0];
                                        importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                                    }
                                    else
                                    {

                                        //importedPage = new ImportedPageArea(Server.MapPath("reports/" + TemplateFile), 1, 0.0F, 0.0F, 1.0F);
                                        importedPage = new ImportedPageArea(TemplateFilePath, 1, 0.0F, 0.0F);
                                    }
                                    //ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                                    //rptPage.Elements.Insert(0, importedPage);
                                    report.Template = new Template();
                                    report.Template.Elements.Add(importedPage);
                                }
                                byte[] buffer = report.Draw();
                                merge.Append(new PdfDocument(buffer));

                                buffer = null;
                                bufferTemplate = null;
                                noOfNotices++;

                                //Jake 2014-08-11 append to csv
                                if (!this.IsWebMode && CsvPrintModule.IsSAPOPrint)
                                {
                                    CsvPrintModule.AppendDataRow(dr);
                                }

                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            //String sError = ex.Message;
                            //System.Diagnostics.Debug.WriteLine("ERROR: " + ex.ToString());
                            ErrorProcessing(ex.ToString());
                            return;
                        }
                    }
                }

                //Jake  2014-08-07 add test code to verify DocumentLayout.GetElementById()
                if (!this.IsWebMode && CsvPrintModule.IsSAPOPrint)
                {
                    if (this.SubProcess.ExportPrintFilePath.EndsWith(".pdf"))
                    {
                        this.SubProcess.ExportPrintFilePath = this.SubProcess.ExportPrintFilePath.Replace(".pdf", ".csv");
                    }

                    CsvPrintModule.BuildCsvFile(this.SubProcess.ExportPrintFilePath);
                    CsvPrintModule.ProcessMessage -= CsvProcessModule_ProcessMessage;
                    ReportCreated = File.Exists(this.SubProcess.ExportPrintFilePath);
                    return;
                }

                //dls 090808 - why no summary report for No Aog's???
                //if (!PrintFileName.Substring(0, 1).Equals("*") && !PrintFileName.Substring(0, 2).Equals("-1") && !PrintFileName.Substring(0, 3).Equals("NAG"))
                if (!PrintFileName.Substring(0, 1).Equals("*") && !PrintFileName.Substring(0, 2).Equals("-1"))
                {
                    //dls 071228 - add summary report page and append to end of PDF document
                    string summaryReportPage = "NoticeSummary.dplx";
                    //string path = Path.Combine(this.ReportFilePath, summaryReportPage);
                    string path = GetFilePath(summaryReportPage);

                    NoticeSummary summary = new NoticeSummary(this.SubProcess.ConnectionString, this.AutIntNo, PrintFileName, format, _showAll, _status, _option, path, summaryReportPage, noOfNotices);

                    byte[] sumReport = summary.CreateSummary();

                    merge.Append(new PdfDocument(sumReport));

                    sumReport = null;
                }

                //this.OutputBuffer = merge.Draw();
                CreateTempFile(merge);

                if (this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0)
                {
                    phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);
                }

                // DL 20100811 ImageA and B add to ASD notice
                // jerry 2011-09-26 add FIRSTNOTICE_RLV_ST
                if (this.ReportFile.ToUpper().IndexOf("ASD") < 0 && this.ReportFile.ToUpper().IndexOf("BUS") < 0 && this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE_RLV_ST") < 0)
                {
                    if (this.ReportFile.ToUpper().IndexOf("_RLV_JMPD") < 0 && this.ReportFile.ToUpper().IndexOf("SECONDNOTICE") < 0 && this.ReportFile.ToUpper().IndexOf("_RLV_COFCT") < 0 && this.ReportFile.ToUpper().IndexOf("AOG_JMPD") < 0 && this.ReportFile.ToUpper().IndexOf("_RLV_BV") < 0)
                    {
                        phImage.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_Image);
                    }
                }
                else
                {
                    phImageA.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_ImageA);
                    phImageB.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_ImageB);
                }

                //dls 080107 - regno image added to SW notice
                // jerry 2011-09-26 add _ST
                if ((this.ReportFile.ToUpper().IndexOf("SW") > 0 && this.ReportFile.ToUpper().IndexOf("AOG") < 0) || this.ReportFile.ToUpper().IndexOf("_ST") > 0)
                {
                    phRegNoImage.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_RegNoImage);
                }
            }
        }

        private void CsvProcessModule_ProcessMessage(string msg)
        {
            ErrorProcessing(msg);
        }

        //Jake 2011-02-25 Removed BarCodeNetImage
        public void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (barCodeImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 25.0F;
                    e.ContentArea.Add(img);
                }

                int generation = System.GC.GetGeneration(barCodeImage);
                barCodeImage = null;
                System.GC.Collect(generation);
            }
        }

        void ph_Image(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (mainImage != null)
            {
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(mainImage), 0, 0);
                img.Height = 160.0F;            //12
                img.Width = 210.0F;             //15
                e.ContentArea.Add(img);

                int generation = System.GC.GetGeneration(mainImage);
                mainImage = null;
                System.GC.Collect(generation);
            }
        }

        // 100806 DL add for SAD
        void ph_ImageA(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (imageA != null)
            {
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(imageA), 0, 0);
                img.Height = 73.0F;            //12
                img.Width = 135.0F;             //15
                e.ContentArea.Add(img);

                int generation = System.GC.GetGeneration(imageA);
                imageA = null;
                System.GC.Collect(generation);
            }
        }

        void ph_ImageB(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (imageB != null)
            {
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(imageB), 0, 0);
                img.Height = 73.0F;//160.0F;            //12
                img.Width = 135.0F;//210.0F;             //15
                e.ContentArea.Add(img);

                int generation = System.GC.GetGeneration(imageB);
                imageB = null;
                System.GC.Collect(generation);
            }
        }

        void ph_RegNoImage(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (regNoImage != null)
            {
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(regNoImage), 0, 0);
                //100819 DL add for ASD
                //091014 modify image size for COFCT
                // jerry 2011-09-26 add _ST
                if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0 || this.ReportFile.ToUpper().IndexOf("BUS") >= 0 || this.ReportFile.ToUpper().IndexOf("_ST") >= 0)
                {
                    img.Height = 24.0F;
                    img.Width = 106.0F;
                }
                else if (this.ReportFile.ToUpper().IndexOf(specificLA_COFCT) > 0 && this.ReportFile.ToUpper().IndexOf("FIRSTNOTICE") >= 0 && this.ReportFile.ToUpper().IndexOf("RLV") < 0)
                {
                    img.Height = 24.0F;
                    img.Width = 95.0F;
                }
                else
                {
                    img.Height = 40.0F;
                    img.Width = 150.0F;
                }
                e.ContentArea.Add(img);

                int generation = System.GC.GetGeneration(regNoImage);
                regNoImage = null;
                System.GC.Collect(generation);
            }
        }

        private enum ViolationType
        {
            Speed = 0,
            RLV = 1,
            NoAOG = 2
        }

    }
}
