﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.ServiceBase;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Transactions;
using System.Web;
using ceTe.DynamicPDF;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.ServiceLibrary;
using IsolationLevel = System.Transactions.IsolationLevel;
//Jacob 2013/08/23 add  CommandTimeout = 0 to allow long running db operations.
namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileProcess
    {
        PrintFileBase SubModule { get; set; }
        internal string ConnectionString { get; set; }
        internal string LastUser { get; set; }
        public bool IsSuccessful { get; set; }
        internal bool SkipNextProcesses { get; set; }
        public string Message { get; set; }
        public string ExportPrintFileName { get; set; }
        public string ExportPrintFilePath { get; set; }
        public string PrintFileName { get; private set; }
        public bool IsMvcPath { get; set; }
        public bool UseTransaction { get; set; }

        public delegate void ErrorProcessingHandler(string errorMsg = null);
        public ErrorProcessingHandler ErrorProcessing { get; set; }

        // 2014-09-15, Oscar added
        public delegate void LogProcessingHandler(string message, LogType type = LogType.Info);
        public LogProcessingHandler LogProcessing { get; set; }

        public PrintFileProcess(string connStr, string lastUser = null, bool isMvcPath = false)
        {
            this.ConnectionString = connStr;
            this.LastUser = lastUser;
            // Add the DynamicPDF License
            ceTe.DynamicPDF.Document.AddLicense("DPS70NPDFJJGEJFZ9ikRGHBua2M3Sl0Pwtw63QxBds5agxfAQMSTfCfEtiI2O6I13jHEsL6smMhYn63QJBLCZ3B8XtMbaTRJ+C1w");
            this.IsMvcPath = isMvcPath;
            UseTransaction = true;
        }

        /// <summary>
        /// Run and create print pdf file
        /// </summary>
        /// <param name="printFileNameOrID"></param>
        /// <param name="autIntNo"></param>
        /// <param name="lastUser"></param>
        /// <param name="intNoList"></param>
        /// <param name="printFileOutputFolder"></param>
        public void BuildPrintFile(PrintFileBase subModule, int autIntNo, object printFileNameOrID = null, string printFileOutputFolder = null, string customFileName = null, bool forMvc = false)
        {
            if (subModule == null) return;
            this.SubModule = subModule;
            this.SubModule.SubProcess = this;

            Initialization();
            if (string.IsNullOrWhiteSpace(this.ConnectionString) || autIntNo == 0)
            {
                this.SubModule.ErrorProcessing("Connection string is null or AuthIntNo is null");
                return;
            }

            // set value to sub module            
            this.SubModule.PrintFileOutputFolder = printFileOutputFolder;
            this.SubModule.AutIntNo = autIntNo;

            // get print file name by pfnIntNo only in WindowService Mode.            
            int pfnIntNo;
            if (printFileNameOrID != null)
            {
                if (!string.IsNullOrWhiteSpace(printFileOutputFolder))
                {
                    if (int.TryParse(printFileNameOrID.ToString().Trim(), out pfnIntNo))
                    {
                        if (pfnIntNo > 0)
                        {
                            List<SqlParameter> paraList = new List<SqlParameter>();
                            paraList.Add(new SqlParameter("@PFNIntNo", pfnIntNo));
                            object result = this.SubModule.ExecuteScalar("GetPrintFileName_WS", paraList);
                            if (result != null)
                            {
                                this.SubModule.PFNIntNo = pfnIntNo;
                                this.SubModule.PrintFileName = result.ToString().Trim();
                            }
                        }

                        if (string.IsNullOrWhiteSpace(this.SubModule.PrintFileName))
                        {
                            //this.SubModule.ErrorProcessing("Print file name is invalid");
                            this.SubModule.ErrorProcessing(string.Format("Unable to get print file name for PFNIntNo {0}", printFileNameOrID));
                            return;
                        }
                    }
                    else
                        this.SubModule.PrintFileName = Convert.ToString(printFileNameOrID).Trim();

                    this.PrintFileName = this.SubModule.PrintFileName;

                    this.ExportPrintFileName = (string.IsNullOrWhiteSpace(customFileName) ? this.SubModule.PrintFileName : customFileName).Trim();

                    string extension = ".pdf";

                    if (!this.ExportPrintFileName.EndsWith(extension, StringComparison.OrdinalIgnoreCase))
                        this.ExportPrintFileName += extension;

                    //2014-01-16 Heidi changed for use "/" not create file(5101)
                    this.ExportPrintFilePath = Path.Combine(printFileOutputFolder, Helper.ReplaceSpecialCharacters(this.ExportPrintFileName.Replace(":", "-")));
                }
                else
                    this.SubModule.PrintFileName = Convert.ToString(printFileNameOrID).Trim();

            }

            SubModule.OnLoad();

            // start work            
            try
            {
                using (var scope = UseTransaction ? new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = TransactionManager.MaximumTimeout
                }) : null)
                {
                    if (!this.IsSuccessful) return;
                    if (!this.SkipNextProcesses)
                        this.SubModule.PrepareWork();

                    if (!this.IsSuccessful) return;
                    if (!this.SkipNextProcesses)
                        this.SubModule.InitialWork();

                    if (!this.IsSuccessful) return;
                    if (!this.SkipNextProcesses)
                        this.SubModule.MainWork();

                    if (!this.IsSuccessful) return;
                    if (!this.SkipNextProcesses)
                    {
                        if (forMvc == false)
                        {
                            this.SubModule.FinaliseWork();
                        }
                    }

                    if (!this.IsSuccessful) return;
                    if (scope != null
                        && Transaction.Current != null
                        && Transaction.Current.TransactionInformation.Status == TransactionStatus.Active)
                        scope.Complete(); //disable this for testing many times, do not forget to enable it when release.
                }
            }
            catch (Exception ex)
            {
                this.SubModule.ErrorProcessing(ex.ToString());
                return;
            }
            finally
            {
                GC.Collect();
            }
        }

        void Initialization()
        {
            this.IsSuccessful = true;
            this.SkipNextProcesses = false;
            this.Message = string.Empty;

            this.SubModule.AutIntNo = 0;
            this.SubModule.PFNIntNo = 0;
            this.SubModule.PrintFileName = null;
            this.SubModule.PrintFileOutputFolder = null;
            this.ExportPrintFileName = null;
            this.ExportPrintFilePath = null;
            this.PrintFileName = null;
        }
    }

    public abstract class PrintFileBase
    {
        internal PrintFileProcess SubProcess { get; set; }

        /// <summary>
        /// print file name, equals queue item key
        /// </summary>
        internal string PrintFileName { get; set; }

        public int PFNIntNo { get; set; }

        /// <summary>
        /// only for windows service, no need to set value when in web mode.
        /// </summary>
        internal string PrintFileOutputFolder { get; set; }

        internal int AutIntNo { get; set; }

        internal string ErrorPage { get; set; }
        internal string ErrorPageUrl { get; set; }

        /// <summary>
        /// running in web or windows services
        /// </summary>
        internal bool IsWebMode { get { return HttpContext.Current != null; } }

        /// <summary>
        /// report template file that in folder Report
        /// </summary>
        protected string ReportFile { get; set; }

        /// <summary>
        /// the path of folder Report
        /// </summary>
        protected string ReportFilePath { get; set; }

        /// <summary>
        /// report template file that in folder Template
        /// </summary>
        protected string TemplateFile { get; set; }

        /// <summary>
        /// the path of folder Template
        /// </summary>
        protected string TemplateFilePath { get; set; }

        protected string StoredProcedureName_Update { get; set; }
        protected string StoredProcedureName_Select { get; set; }
        protected byte[] OutputBuffer { get; set; }
        //protected Stream OutputMS { get; set; }
        protected AuthReportNameDB ReportDB { get; set; }
        protected bool ReportCreated { get; set; }
        private const string TMP_SUFFIX = ".tmp";

        //Jake 2014-08-19 added for 5306
        protected CSVGeneratorForPrintModule CsvPrintModule { get; set; }

        public byte[] GetBuffer()
        {
            byte[] b = new byte[this.OutputBuffer.Length];
            b = this.OutputBuffer;
            this.OutputBuffer = null;
            return b;
        }

        AARTORules rules = AARTORules.GetSingleton();

        SqlConnection conn;

        #region processing methods

        internal void PrepareWork()
        {
            //this.DPLXDocument = null;
            //if (this.RPTDocument != null)
            //{
            //    this.RPTDocument.Dispose();
            //    this.RPTDocument = null;
            //}
            //this.OutputBuffer = null;

            this.ReportDB = new AuthReportNameDB(this.SubProcess.ConnectionString);

            //switch (this.FileType)
            //{
            //    case PrintFileType.DPLX:
            //        this.DPLXDocument = new Document();
            //        //this.DPLXDocumentLayout = new DocumentLayout(this.SubProcess.ReportTemplateFullPath);
            //        //this.DPLXMergeDocument = new MergeDocument();
            //        break;

            //    case PrintFileType.RPT:
            //        this.RPTDocument = new ReportDocument();
            //        break;
            //}
        }
        //Jake 2014-07-02 modified
        //use byte[] for web printing and use temp file for windows service printing
        internal void FinaliseWork()
        {
            //if (this.OutputBuffer != null)
            //{
            try
            {
                if (this.ReportCreated == false)
                {
                    Document doc = new Document();
                    Page page = new Page();
                    string text = "No data rows found!";
                    float textSize = 20;
                    ceTe.DynamicPDF.PageElements.Label lbl = new ceTe.DynamicPDF.PageElements.Label(text, 10, 10, text.Length * 20, textSize * 2, Font.CourierBold, textSize);
                    page.Elements.Add(lbl);
                    doc.Pages.Add(page);
                    if (IsWebMode) this.OutputBuffer = doc.Draw();
                    else
                        CreateTempFile(doc);
                }

                if (this.IsWebMode)
                {
                    // web output
                    HttpResponse response = HttpContext.Current.Response;
                    response.ClearContent();
                    response.ClearHeaders();
                    response.ContentType = "application/pdf";
                    //response.AddHeader("Content-Disposition ", string.Format("attachment;Filename= {0}.pdf", this.PrintFileName));
                    response.BinaryWrite(this.OutputBuffer);
                    //response.End();
                }
                else
                {
                    //Jake 2014-08-19 added 
                    if (CsvPrintModule != null && CsvPrintModule.IsSAPOPrint)
                    {
                        return;
                    }
                    // file system output                        
                    if (!Directory.Exists(this.PrintFileOutputFolder))
                        Directory.CreateDirectory(this.PrintFileOutputFolder);

                    string tempFile = this.SubProcess.ExportPrintFilePath + TMP_SUFFIX;

                    if (File.Exists(this.SubProcess.ExportPrintFilePath))
                        //File.Delete(this.SubProcess.ExportPrintFilePath);
                        this.SubProcess.ExportPrintFilePath = this.SubProcess.ExportPrintFilePath.Insert(this.SubProcess.ExportPrintFilePath.Length - 4, string.Format("({0})", DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")));

                    if (File.Exists(tempFile))
                    {
                        //this.OutputMS.Close();
                        // this.OutputMS.Dispose();
                        File.Copy(tempFile, this.SubProcess.ExportPrintFilePath);
                        File.Delete(tempFile);
                    }
                }

                this.OutputBuffer = null;
                this.ReportCreated = false;
                //this.OutputMS = null;
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrWhiteSpace(this.SubProcess.ExportPrintFilePath) && File.Exists(this.SubProcess.ExportPrintFilePath))
                    File.Delete(this.SubProcess.ExportPrintFilePath);
                ErrorProcessing(ex.Message);
            }
            //}
            //else
            //{
            //    ErrorProcessing("There is no content to create printing file.");
            //}
        }

        public virtual void InitialWork() { }
        public virtual void MainWork() { }

        internal virtual void OnLoad() { }

        #endregion
        protected void CreateTempFile(Document document)
        {
            if (IsWebMode)
            {
                this.OutputBuffer = document.Draw();
                ReportCreated = document.Pages.Count > 0;
                return;
            }

            if (!Directory.Exists(this.PrintFileOutputFolder))
                Directory.CreateDirectory(this.PrintFileOutputFolder);

            string tmpFile = this.SubProcess.ExportPrintFilePath + TMP_SUFFIX;

            //this.OutputBuffer = merge.Draw();
            if (File.Exists(tmpFile))
                File.Delete(tmpFile);

            if (document != null && document.Pages.Count > 0)
                document.Draw(tmpFile);

            ReportCreated = File.Exists(tmpFile);
        }

        protected void CreateTempFile(Stream stream)
        {
            if (IsWebMode)
            {
                if (this.OutputBuffer == null) this.OutputBuffer = new byte[(int)stream.Length];
                stream.Read(this.OutputBuffer, 0, (int)stream.Length);
                ReportCreated = this.OutputBuffer.Length > 0;
                return;
            }

            if (!Directory.Exists(this.PrintFileOutputFolder))
                Directory.CreateDirectory(this.PrintFileOutputFolder);

            string tmpFile = this.SubProcess.ExportPrintFilePath + TMP_SUFFIX;

            //this.OutputBuffer = merge.Draw();
            if (File.Exists(tmpFile))
                File.Delete(tmpFile);

            if (stream != null)
            {
                using (FileStream fs = new FileStream(tmpFile, FileMode.Create))
                {
                    int length = 4096;
                    byte[] buffer = new byte[length];
                    int offset = 0;
                    while ((offset = stream.Read(buffer, 0, length)) > 0)
                    {
                        fs.Write(buffer, 0, offset);
                    }
                    fs.Close();
                    fs.Dispose();
                }
            }

            ReportCreated = File.Exists(tmpFile);
        }

        protected DateRulesDetails GetDateRule(string dtStart, string dtEnd)
        {
            rules.Initialize(this.SubProcess.ConnectionString, this.AutIntNo);
            return rules.GetDateRule(dtStart, dtEnd);
        }

        protected AuthorityRulesDetails GetAuthorityRule(string arCode)
        {
            rules.Initialize(this.SubProcess.ConnectionString, this.AutIntNo);
            return rules.GetAuthRule(arCode);
        }

        // check report template file is existed
        protected bool CheckReportTemplateExists(string function, string reportFile, out string reportFilePath)
        {
            bool result = true;
            string file = !string.IsNullOrWhiteSpace(reportFile) ? reportFile : this.ReportFile;
            reportFilePath = GetFilePath(file);
            if (string.IsNullOrWhiteSpace(this.ReportFilePath))
                this.ReportFilePath = reportFilePath;
            //this.ReportFilePath = GetFilePath(this.ReportFile);
            if (!File.Exists(reportFilePath))
            {
                result = false;
                //Jerry 2012-04-16 change
                ErrorProcessing(string.Format("Report {0} does not exist", reportFilePath));
                return result;
            }

            if (!string.IsNullOrWhiteSpace(function))
                this.TemplateFile = this.ReportDB.GetAuthReportNameTemplate(this.AutIntNo, function);

            if (!string.IsNullOrWhiteSpace(this.TemplateFile))
            {
                this.TemplateFilePath = GetFilePath(this.TemplateFile, false);

                if (!File.Exists(this.TemplateFilePath))
                {
                    result = false;
                    //Jerry 2012-04-16 change
                    ErrorProcessing(string.Format("Report template {0} does not exist", this.TemplateFilePath));
                    return result;
                }
            }
            return result;
        }

        // 2014-09-15, Oscar added
        protected const string None = "None";
        protected readonly AuthReportNamesService authReportNamesService = new AuthReportNamesService();
        protected bool CheckReportTemplateExists(string function, bool useDBValues = true)
        {
            if (useDBValues)
            {
                const string noReportOrTemplate = "Unable to get the name of report or template from database. (Function: \"{0}\")";

                if (string.IsNullOrWhiteSpace(function))
                {
                    LogProcessing(string.Format(GetResource(null, "UnableToGetTheNameOfReportOrTemplateFromDatabase", noReportOrTemplate), function), LogType.Error, false);
                    return false;
                }

                var arn = this.authReportNamesService.GetByAutIntNoArnFunction(AutIntNo, function);
                if (arn == null)
                {
                    if (!string.IsNullOrWhiteSpace(ReportFile))
                    {
                        arn = new AuthReportNames
                        {
                            AutIntNo = AutIntNo,
                            ArnFunction = function,
                            ArnReportPage = ReportFile,
                            ArnTemplate = TemplateFile,
                            LastUser = SubProcess.LastUser
                        };
                        this.authReportNamesService.Save(arn);
                    }
                    else
                    {
                        LogProcessing(string.Format(GetResource(null, "UnableToGetTheNameOfReportOrTemplateFromDatabase", noReportOrTemplate), function), LogType.Error, false);
                        return false;
                    }
                }

                if (!string.IsNullOrWhiteSpace(arn.ArnReportPage)
                    && !arn.ArnReportPage.Equals2(None))
                    ReportFile = arn.ArnReportPage;

                if (!string.IsNullOrWhiteSpace(arn.ArnTemplate)
                    && !arn.ArnTemplate.Equals2(None))
                    TemplateFile = arn.ArnTemplate;
            }

            ReportFilePath = GetFilePath(ReportFile);
            if (!File.Exists(ReportFilePath))
            {
                LogProcessing(string.Format("Report {0} does not exist", ReportFilePath), LogType.Error, false);
                return false;
            }

            if (!string.IsNullOrWhiteSpace(TemplateFile))
            {
                TemplateFilePath = GetFilePath(TemplateFile, false);
                if (!File.Exists(TemplateFilePath))
                {
                    LogProcessing(string.Format("Report template {0} does not exist", TemplateFilePath), LogType.Error, false);
                    return false;
                }
            }

            return true;
        }

        protected string GetFilePath(string fileName, bool inReportsFolder = true)
        {
            string folder = inReportsFolder ? "Reports" : "Templates";
            //return this.isWebMode ? HttpContext.Current.Server.MapPath(folder + "/" + fileName) : folder + "\\" + fileName;//HttpContext.Current.Server.MapPath(folder + "/" + fileName) :
            //Oscar 20120406 change to unc

            if (this.SubProcess.IsMvcPath)
            {
                return this.IsWebMode ? Path.Combine(AppDomain.CurrentDomain.BaseDirectory, folder + "\\" + fileName) :
                    Path.Combine(GetConnectionString((inReportsFolder ? ServiceConnectionNameList.ReportFolder : ServiceConnectionNameList.TemplateFolder), ServiceConnectionTypeList.UNC), fileName);
            }
            else
            {
                return this.IsWebMode ? HttpContext.Current.Server.MapPath(folder + "/" + fileName) :
                   Path.Combine(GetConnectionString((inReportsFolder ? ServiceConnectionNameList.ReportFolder : ServiceConnectionNameList.TemplateFolder), ServiceConnectionTypeList.UNC), fileName);
            }
        }

        // Oscar 20120405 add for unc connection string.
        Dictionary<string, string> connectionList;

        protected string GetConnectionString(ServiceConnectionNameList name, ServiceConnectionTypeList type)
        {
            if (connectionList == null)
                connectionList = new Dictionary<string, string>();

            string listName = name.ToString() + "_" + type.ToString();

            if (!connectionList.ContainsKey(listName) || string.IsNullOrEmpty(connectionList[listName]))
            {
                //ServiceConnectionParameter para = scpService.GetBySctIntNoScnIntNo((int)type, (int)name);
                //connectionList.Add(listName, para.ScpConnStr);
                string connStr = string.Empty;
                ConnectionBase connBase = SerializeConnection(type, name);
                if (connBase != null)
                {
                    switch (type)
                    {
                        case ServiceConnectionTypeList.DB:
                            ConnectionDB connDB = connBase as ConnectionDB;
                            connStr = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", connDB.Server, connDB.DataBase, connDB.UserId, connDB.Password);
                            break;
                        case ServiceConnectionTypeList.FTP:
                            ConnectionFtp connFtp = connBase as ConnectionFtp;
                            break;
                        case ServiceConnectionTypeList.UNC:
                            ConnectionUnc connUnc = connBase as ConnectionUnc;
                            connStr = string.Format("{0}", connUnc.UNCPath);
                            break;
                    }
                }
                connectionList.Add(listName, connStr);
            }
            return connectionList[listName];
        }

        ConnectionBase SerializeConnection(ServiceConnectionTypeList connType, ServiceConnectionNameList connName)
        {
            ConnectionBase connDB = null;
            try
            {
                ServiceConnectionParameterManager connParaManager = new ServiceConnectionParameterManager();

                SIL.ServiceQueueLibrary.DAL.Entities.ServiceConnectionParameter connPara = new ServiceQueueLibrary.DAL.Services.ServiceConnectionParameterService().GetBySctIntNoScnIntNo((int)connType, (int)connName);
                if (connPara != null)
                {
                    connDB = connParaManager.ConvertXmltoStructure(connPara.ScpConnStr, connType);
                }

                return connDB;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        void OpenConnection()
        {
            if (conn == null)
                conn = new SqlConnection(this.SubProcess.ConnectionString);
            if (conn.State != ConnectionState.Open)
                conn.Open();
        }

        protected void CloseConnection()
        {
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
                conn = null;
            }
        }

        protected int ExecuteNonQuery(string spName, List<SqlParameter> paraList = null, bool closeConn = true)
        {
            int result = -1;
            if (string.IsNullOrWhiteSpace(spName)) return result;
            try
            {
                OpenConnection();
                //Edited by Jacob 2013/08/23 start,  to allow long running db operations.
                //using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure})
                using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                //Edited by Jacob 2013/08/23 end
                {
                    if (paraList != null && paraList.Count > 0)
                        cmd.Parameters.AddRange(paraList.ToArray());
                    result = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception ex)
            {
                ErrorProcessing(ex.Message);
            }
            finally
            {
                CloseConnection();
                GC.Collect();
            }

            return result;
        }

        internal object ExecuteScalar(string spName, List<SqlParameter> paraList = null, bool closeConn = true)
        {
            object result = null;
            if (string.IsNullOrWhiteSpace(spName)) return result;
            try
            {
                OpenConnection();
                //Edited by Jacob 2013/08/23 start,  to allow long running db operations.
                //using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure})
                using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                //Edited by Jacob 2013/08/23 end
                {
                    if (paraList != null && paraList.Count > 0)
                        cmd.Parameters.AddRange(paraList.ToArray());
                    result = cmd.ExecuteScalar();
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception ex)
            {
                ErrorProcessing(ex.Message);
            }
            finally
            {
                CloseConnection();
                GC.Collect();
            }

            return result;
        }

        // get dataset
        protected DataSet ExecuteDataSet(string spName, List<SqlParameter> paraList = null, string intNoParaName = null, bool closeConn = true)
        {
            return ExecuteDataSet<DataSet>(spName, paraList, intNoParaName, closeConn);
        }

        protected TDataSet ExecuteDataSet<TDataSet>(string spName, List<SqlParameter> paraList = null, string intNoParaName = null, bool closeConn = true)
            where TDataSet : DataSet, new()
        {
            TDataSet result = default(TDataSet);
            if (string.IsNullOrWhiteSpace(spName)) return result;

            //OpenConnection();
            try
            {
                List<int> intNoList = new List<int>();
                bool needMerge = !string.IsNullOrWhiteSpace(intNoParaName);
                if (needMerge)
                {
                    intNoParaName = intNoParaName.Trim();
                    if (!intNoParaName.StartsWith("@"))
                        intNoParaName = "@" + intNoParaName;
                    intNoList = GetIntNoList(intNoParaName);
                }
                if (intNoList.Count <= 0)
                    intNoList.Add(0);

                OpenConnection();
                int tableIndex = -1;
                for (int i = 0; i < intNoList.Count; i++)
                {
                    //Edited by Jacob 2013/08/23 start,  to allow long running db operations.
                    //using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure})
                    using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 })
                    //Edited by Jacob 2013/08/23 end
                    {
                        if (paraList != null && paraList.Count > 0)
                        {
                            if (needMerge)
                            {
                                for (int j = 0; j < paraList.Count; j++)
                                {
                                    if (paraList[j].ParameterName.Equals(intNoParaName, StringComparison.OrdinalIgnoreCase))
                                    {
                                        paraList[j].Value = intNoList[i];
                                        break;
                                    }
                                }
                            }
                            cmd.Parameters.AddRange(paraList.ToArray());
                        }

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (i == 0)
                            {
                                result = new TDataSet();
                                da.Fill(result);
                                if (needMerge)
                                {
                                    for (int j = 0; j < result.Tables.Count; j++)
                                    {
                                        //table filter condition
                                        if (result.Tables[j].Columns.Count > 1)
                                        {
                                            tableIndex = j;
                                            break;
                                        }
                                    }
                                    if (tableIndex < 0)
                                        return result;
                                }
                            }
                            else
                            {
                                DataRow drSource;
                                using (TDataSet ds = new TDataSet())
                                {
                                    da.Fill(ds);
                                    using (DataTable dt = ds.Tables[tableIndex])
                                    {
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            drSource = result.Tables[tableIndex].NewRow();
                                            drSource.ItemArray = dr.ItemArray;
                                            result.Tables[tableIndex].Rows.Add(drSource);
                                        }
                                    }
                                }
                            }
                        }
                        cmd.Parameters.Clear();
                    }
                }

                //if (!string.IsNullOrWhiteSpace(orderBy) && needMerge && tableIndex >= 0)
                //{
                //    TDataSet sortedResult = new TDataSet();
                //    for (int i = 0; i < result.Tables.Count; i++)
                //    {
                //        if (i == tableIndex)
                //        {
                //            using (DataTable dt = result.Tables[tableIndex])
                //            {
                //                if (dt.Rows.Count > 0)
                //                {
                //                    using (DataView dv = new DataView(dt))
                //                    {
                //                        dv.Sort = orderBy;
                //                        sortedResult.Tables.Add(dv.ToTable());
                //                    }
                //                }
                //            }
                //        }
                //        else
                //            sortedResult.Tables.Add(result.Tables[i]);
                //    }
                //    result = sortedResult;
                //}

                //using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure })
                //{
                //    if (paraList != null && paraList.Count > 0)
                //    {
                //        cmd.Parameters.AddRange(paraList.ToArray());
                //    }

                //    if (conn.State != ConnectionState.Open)
                //        conn.Open();

                //    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                //    {
                //        result = new TDataSet();
                //        da.Fill(result);
                //    }

                //}
            }
            catch (Exception ex)
            {
                ErrorProcessing(ex.Message);
            }
            finally
            {
                CloseConnection();
                GC.Collect();
            }

            return result;
        }

        List<int> GetIntNoList(string intNoParaName = null)
        {
            List<int> intNoList = new List<int>();
            if (string.IsNullOrWhiteSpace(intNoParaName))
                return intNoList;
            else
                intNoParaName = intNoParaName.Trim();

            if (this.PFNIntNo <= 0)
            {
                List<SqlParameter> paraList = new List<SqlParameter>();
                paraList.Add(new SqlParameter("@AutIntNo", this.AutIntNo));
                paraList.Add(new SqlParameter("@PrintFileName", this.PrintFileName));
                object result = ExecuteScalar("GetPrintFileName_WS", paraList, false);
                if (result != null)
                {
                    this.PFNIntNo = Convert.ToInt32(result);
                }
            }

            //if (this.PFNIntNo > 0)
            if (this.PFNIntNo > 0 || this.IsWebMode)
            {
                List<SqlParameter> paraList = new List<SqlParameter>();
                paraList.Add(new SqlParameter("@PFNIntNo", this.PFNIntNo));
                //Jerry 2012-04-24 change
                if (this.IsWebMode)
                    paraList.Add(new SqlParameter("@PrintFileName", this.PrintFileName));
                else
                    paraList.Add(new SqlParameter("@PrintFileName", ""));
                paraList.Add(new SqlParameter("@ConjoinType", intNoParaName.Substring(1, 1).ToUpper()));
                DataSet ds = ExecuteDataSet("GetPrintFileNameConjoinList_WS", paraList, null);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        intNoList.Add(Convert.ToInt32(dr[0]));
                    }
                }
            }
            return intNoList;
        }

        string AddErrorMsg(string errorMsg = null)
        {
            SubProcess.IsSuccessful = false;
            CloseConnection();
            //filter the error msg.
            if (!string.IsNullOrWhiteSpace(errorMsg)
                && !errorMsg.Trim().Equals("Thread was being aborted.", StringComparison.OrdinalIgnoreCase))
            {
                errorMsg = errorMsg.Replace("\r", "").Replace("\n", "");
                SubProcess.Message += " " + errorMsg;
            }
            return errorMsg;
        }

        // error processing
        public void ErrorProcessing(string errorMsg = null)
        {
            errorMsg = AddErrorMsg(errorMsg);

            if (this.IsWebMode)
                HttpContext.Current.Response.Redirect(string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", this.SubProcess.Message, this.ErrorPage, this.ErrorPageUrl));

            if (this.SubProcess.ErrorProcessing != null)
                this.SubProcess.ErrorProcessing(errorMsg);
        }

        // 2014-09-15, Oscar created
        /// <summary>
        /// make sure you have set correct links before using this method!!!
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <param name="isSuccessful"></param>
        public void LogProcessing(string message, LogType type = LogType.Info, bool isSuccessful = true)
        {
            if (!isSuccessful)
            {
                if (SubProcess.LogProcessing == null
                    && SubProcess.ErrorProcessing != null)
                    ErrorProcessing(message);
                else
                    AddErrorMsg(message);
            }

            if (SubProcess.LogProcessing != null)
                SubProcess.LogProcessing(message, type);
        }

        MethodInfo getResource;

        public string GetResource(string pageName, string key, string defaultVale = null)
        {
            string value = string.Empty;
            if (IsWebMode)
                value = GetResourcesInClassLibraries.GetResourcesValue(pageName, key);
            else
            {
                string newKey = (!string.IsNullOrWhiteSpace(pageName) ? (pageName + "_") : string.Empty) + key;
                try
                {
                    if (getResource == null)
                    {
                        var method = Assembly.Load("SIL.AARTOService.Resource")
                            .GetType("SIL.AARTOService.Resource.ResourceHelper")
                            .GetMethod("GetResource", new[] { typeof(string) });
                        if (method != null)
                            getResource = method;
                    }
                    if (getResource != null)
                        value = Convert.ToString(getResource.Invoke(null, new object[] { newKey }));
                }
                catch { }
            }
            if ((string.IsNullOrWhiteSpace(value) || value == key) && !string.IsNullOrWhiteSpace(defaultVale))
                value = defaultVale;
            return value;
        }

        #region 2014-09-15, Oscar added
        public string GetExportedFilePath(params string[] paths)
        {
            var pathList = paths.Where(p => !string.IsNullOrWhiteSpace(p))
                .Select(p =>
                {
                    if (p.StartsWith(@"\")
                        && !p.StartsWith(@"\\"))
                        p = p.Remove(0, 1);
                    if (p.EndsWith(@"\"))
                        p = p.Remove(p.Length - 1, 1);
                    return p.Replace(":", "-").Replace("/", "~");
                }).ToArray();
            var path = Path.Combine(pathList);

            if (File.Exists(path))
                path = path.Insert(path.Length - 4, string.Format("({0})", DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss")));
            return path;
        }

        protected string GetRelativePath(string oldAbsolutePath, string oldRelativePath, string newAbsolutePath)
        {
            var index = oldAbsolutePath.IndexOf(oldRelativePath, StringComparison.OrdinalIgnoreCase);
            var prefix = oldAbsolutePath.Remove(index);
            var index2 = newAbsolutePath.IndexOf(prefix, StringComparison.OrdinalIgnoreCase);
            return newAbsolutePath.Substring(index2 + prefix.Length);
        }

        protected bool BindQuery(DocumentLayout layout, string name, OpeningRecordSetEventHandler openingRecordSet, bool isUnbinding = false)
        {
            Query query;
            if (layout == null
                || string.IsNullOrWhiteSpace(name)
                || openingRecordSet == null
                || (query = layout.GetQueryById(name)) == null)
                return false;

            if (!isUnbinding)
                query.OpeningRecordSet += openingRecordSet;
            else
                query.OpeningRecordSet -= openingRecordSet;
            return true;
        }

        protected bool BindTemplate(DocumentLayout layout, string name, PdfPage page)
        {
            ceTe.DynamicPDF.ReportWriter.Report report;

            if (layout == null
                || string.IsNullOrWhiteSpace(name)
                || page == null
                || (report = (ceTe.DynamicPDF.ReportWriter.Report)layout.GetElementById(name)) == null)
                return false;

            report.Template = page;
            return true;
        }

        protected bool BindLabel(DocumentLayout layout, string name, string value)
        {
            Label lbl;
            if (layout == null
                || string.IsNullOrWhiteSpace(name)
                || string.IsNullOrEmpty(value)
                || (lbl = (Label)layout.GetElementById(name)) == null)
                return false;

            lbl.Text = value;
            return true;
        }

        protected bool BindPlaceHolder(DocumentLayout layout, string name, PlaceHolderLaidOutEventHandler laidOut, bool isUnbinding = false)
        {
            PlaceHolder ph;
            if (layout == null
                || string.IsNullOrWhiteSpace(name)
                || laidOut == null
                || (ph = (PlaceHolder)layout.GetElementById(name)) == null)
                return false;

            if (!isUnbinding)
                ph.LaidOut += laidOut;
            else
                ph.LaidOut -= laidOut;
            return true;
        }

        #endregion
    }

}
