﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using Stalberg.TMS;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using System.IO;
using System.Web;
using System.Data.SqlClient;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using System.Globalization;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModule2ndNotice : PrintFileBase
    {
        public PrintFileModule2ndNotice()
        {
            this.ErrorPage = GetResource("SecondNoticeViewer.aspx", "thisPage");
            this.ErrorPageUrl = "SecondNoticeViewer.aspx";
        }

        const int FIRST_NOTICE = 1;
        const int SECOND_NOTICE = 2;
        int status = 260;
        int option = 2;
        PlaceHolder phBarCode;
        string showAll = "N";
        string format = "TMS";
        System.Drawing.Image barCodeImage = null;

        public override void InitialWork()
        {
            if (string.IsNullOrWhiteSpace(this.PrintFileName))
            {
                ErrorProcessing("There was no print file supplied!");
                return;
            }

            string printFileName = this.PrintFileName;

            string prefix = string.Empty;
            string longPrefix = string.Empty;
            string extendedPrefix = string.Empty;
            int noticeStage = SECOND_NOTICE;

            DataSet ds = null;
            NoticeDB notice = new NoticeDB(this.SubProcess.ConnectionString);

            if (this.PrintFileName != "-1")
            {
                prefix = this.PrintFileName.Substring(0, 3).ToUpper();
                longPrefix = this.PrintFileName.Substring(0, 7).ToUpper();
                extendedPrefix = this.PrintFileName.Substring(0, 11).ToUpper();

                // See if its a single notice
                string pattern = @"^\w{2,}/\d{2,}/\d{2,}/\d{3,}$";
                Regex regex = new Regex(pattern, RegexOptions.Singleline);
                if (regex.IsMatch(this.PrintFileName))
                {
                    if (!this.PrintFileName[0].Equals('*'))
                        this.PrintFileName = "*" + this.PrintFileName;

                    ds = notice.GetNoticeCheckRLVDS(this.PrintFileName.Substring(1, this.PrintFileName.Length - 1));
                }
            }

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    prefix = dr["PrintFileName"].ToString();
                    //add the check for the status of the notice here
                    noticeStage = Convert.ToInt16(dr["NoticeStage"]);
                }

                ds.Dispose();
            }

            AuthorityRulesDetails arDet1, arDet2;
            arDet1 = GetAuthorityRule("2550");
            arDet2 = GetAuthorityRule("4250");

            if (arDet1.ARString.Equals("N") || arDet2.ARString.Equals("N") || arDet2.ARNumeric == 0)
                noticeStage = SECOND_NOTICE;

            PrintFileProcess process = new PrintFileProcess(this.SubProcess.ConnectionString, this.SubProcess.LastUser);
            if (noticeStage == FIRST_NOTICE)
            {
                process.ErrorProcessing = this.SubProcess.ErrorProcessing;
                process.BuildPrintFile(
                    new PrintFileModuleFirstNotice(),
                    this.AutIntNo, printFileName, this.PrintFileOutputFolder);
                this.SubProcess.IsSuccessful = process.IsSuccessful;
                this.SubProcess.SkipNextProcesses = true;
                return;
            }

            //commented out by linda 2012-12-20 ( change AutTicketProcessor to NotTicketProcessor=TMS)
            //BD 20090128 - removing of auth rule to control what ticket processing being used, now using AutTicketProcessor
            //AuthorityDB authDB = new AuthorityDB(this.SubProcess.ConnectionString);
            //AuthorityDetails authDetails = authDB.GetAuthorityDetails(this.AutIntNo);

            //if (authDetails.AutTicketProcessor == "CiprusPI" || authDetails.AutTicketProcessor == "Cip_CofCT")
            //    format = authDetails.AutTicketProcessor;

            this.ReportFile = ReportDB.GetAuthReportName(this.AutIntNo, "SecondNotice");
            this.TemplateFile = ReportDB.GetAuthReportNameTemplate(this.AutIntNo, "SecondNotice");

            if (this.ReportFile.Equals(string.Empty))
            {
                this.ReportFile = "SecondNotice_PL.dplx";
                //ReportDB.AddAuthReportName(this.AutIntNo, "SecondNotice.rpt", "SecondNotice", "system", "SecondNoticeTemplate_PL.pdf");
                ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, "SecondNotice", "system", "SecondNoticeTemplate_PL.pdf");
            }

            this.StoredProcedureName_Update = "NoticePrint_Update2nd_WS";
            switch (prefix.ToUpper())
            {
                case "ASD": // Average Speed Over Distance
                    this.ReportFile = ReportDB.GetAuthReportName(this.AutIntNo, "ASDSecondNotice");

                    if (this.ReportFile.Equals(string.Empty))
                    {
                        this.ReportFile = "SecondNotice_CofCT_ASD.dplx";
                        ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, "ASDSecondNotice", "System", "SecondNoticeTemplate_CofCT_ASD.pdf");
                    }

                    this.TemplateFile = ReportDB.GetAuthReportNameTemplate(this.AutIntNo, "ASDSecondNotice");
                    break;

                case "REG": // Change of registration
                    switch (longPrefix)
                    {
                        //at the moment everyone except JMPD uses the same report for RLV as for SPD, e.g. FirstNotice_ST.rpt
                        case "REG_RLV":
                        case "REG_SPD":
                        default:
                            process.ErrorProcessing = this.SubProcess.ErrorProcessing;
                            process.BuildPrintFile(
                                new PrintFileModuleChangeRegNo(),
                                this.AutIntNo, printFileName, this.PrintFileOutputFolder);
                            this.SubProcess.IsSuccessful = process.IsSuccessful;
                            this.SubProcess.SkipNextProcesses = true;
                            return;
                            break;
                    }
                    break;
                case "HWO": //jerry 2011-3-2 add
                    //Response.Redirect("SecondNotice_HWO_Viewer.aspx?printfile=" + printFile);
                    process.ErrorProcessing = this.SubProcess.ErrorProcessing;
                    process.BuildPrintFile(
                        new PrintFileModule2ndNoticeHWO(),
                        this.AutIntNo, printFileName, this.PrintFileOutputFolder
                    );
                    this.SubProcess.IsSuccessful = process.IsSuccessful;
                    this.SubProcess.SkipNextProcesses = true;
                    return;
                    break;
                case "RNO":
                    this.StoredProcedureName_Update = "NoticePrint_UpdateRNO_WS";
                    break;
                // 20120228 Oscar removed, we don't need Option=3 any longer.
                //default:
                //    if (extendedPrefix.IndexOf("EPR") > 0)
                //    {
                //        //dls 080118 - add extra option for printing these EasyPay Retrofitted notices - their status values will be all over the place
                //        this.option = 3;
                //    }
                //    break;
            }

            // check report template file exists
            string path;
            if (!CheckReportTemplateExists(null, null, out path)) return;
        }

        public override void MainWork()
        {
            string strSetGeneratedDate = GetAuthorityRule("2570").ARString.Trim().ToUpper(); //2013-06-13 added by Nancy for 4973 

            DocumentLayout doc = new DocumentLayout(this.ReportFilePath);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.SubProcess.ConnectionString;
            ParameterDictionary parameters = new ParameterDictionary();

            Label lblId = (Label)doc.GetElementById("lblId");
            Label lblReference = (Label)doc.GetElementById("lblReference");
            Label lblReferenceB = (Label)doc.GetElementById("lblReferenceB");
            Label lblFormattedNotice = (Label)doc.GetElementById("lblFormattedNotice");
            Label lblForAttention = (Label)doc.GetElementById("lblForAttention");
            Label lblAddress = (Label)doc.GetElementById("lblAddress");
            Label lblNoticeNumber = (Label)doc.GetElementById("lblNoticeNumber");
            Label lblPaymentInfo = (Label)doc.GetElementById("lblPaymentInfo");
            Label lblStatRef = (Label)doc.GetElementById("lblStatRef");
            Label lblDate = (Label)doc.GetElementById("lblDate");
            Label lblTime = (Label)doc.GetElementById("lblTime");
            Label lblLocDescr = (Label)doc.GetElementById("lblLocDescr");
            Label lblOffenceDescrEng = (Label)doc.GetElementById("lblOffenceDescrEng");
            Label lblOffenceDescrAfr = (Label)doc.GetElementById("lblOffenceDescrAfr");
            Label lblCode = (Label)doc.GetElementById("lblCode");
            Label lblCameraNo = (Label)doc.GetElementById("lblCamera");
            Label lblOfficerNo = (Label)doc.GetElementById("lblOfficerNo");
            Label lblFineAmount = (Label)doc.GetElementById("lblAmount");
            Label lblPrintDate = (Label)doc.GetElementById("lblPrintDate");
            Label lblOffenceDate = (Label)doc.GetElementById("lblOffenceDate");
            Label lblOffenceTime = (Label)doc.GetElementById("lblOffenceTime");
            Label lblAut = (Label)doc.GetElementById("lblText");
            Label lblLocation = (Label)doc.GetElementById("lblLocation");
            Label lblSpeedLimit = (Label)doc.GetElementById("lblSpeedLimit");
            Label lblSpeed = (Label)doc.GetElementById("lblSpeed");
            Label lblOfficer = (Label)doc.GetElementById("lblOfficer");
            Label lblRegNo = (Label)doc.GetElementById("lblRegNo");
            Label lblIssuedBy = (Label)doc.GetElementById("lblIssuedBy");
            Label lblPaymentDate = (Label)doc.GetElementById("lblPaymentDate");
            Label lblCourtName = (Label)doc.GetElementById("lblCourtName");
            Label lblEasyPay = (Label)doc.GetElementById("lblEasyPay");
            Label lblFilmNo = (Label)doc.GetElementById("lblFilmNo");
            Label lblFrameNo = (Label)doc.GetElementById("lblFrameNo");
            Label lblVehicle = (Label)doc.GetElementById("lblVehicle");
            Label lblAuthorityAddress = (Label)doc.GetElementById("lblAuthorityAddress");
            Label lblAutTel = (Label)doc.GetElementById("lblAutTel");
            Label lblAutFax = (Label)doc.GetElementById("lblAutFax");
            Label lblDisclaimer = (Label)doc.GetElementById("lblDisclaimer");
            Label lblReprintDate = (Label)doc.GetElementById("lblReprintDate");
            Label lblReprintDateText = (Label)doc.GetElementById("lblReprintDateText");
            Label lblMtrName = (Label)doc.GetElementById("lbl_MtrName");
            Label lblMtrDepart = (Label)doc.GetElementById("lbl_MtrDepart");
            Label lblMtrPostAddr = (Label)doc.GetElementById("lbl_MtrPostAddr");
            Label lblAuthName = (Label)doc.GetElementById("lbl_AuthName");
            Label lblAutDepart = (Label)doc.GetElementById("lbl_AutDepart");
            Label lblAutPhysAddr = (Label)doc.GetElementById("lbl_AutPhysAddr");
            Label lblAutPostAddr = (Label)doc.GetElementById("lbl_AutPostAddr");
            Label lblCourt = (Label)doc.GetElementById("lblCourt");
            Label lblpaypoint = (Label)doc.GetElementById("lbl_paypoint");
            Label lblCameraA = (Label)doc.GetElementById("lblCameraA");
            Label lblCameraB = (Label)doc.GetElementById("lblCameraB");
            Label lblMeasurement = (Label)doc.GetElementById("lblMeasurement");
            Label lblTimeDifference = (Label)doc.GetElementById("lblTimeDifference");
            Label lbl_AuthName2 = (Label)doc.GetElementById("lbl_AuthName2");
            Label lbl_CourtName = (Label)doc.GetElementById("lblCourtName");

            phBarCode = (PlaceHolder)doc.GetElementById("phBarCode");
            phBarCode.LaidOut += new PlaceHolderLaidOutEventHandler(ph_BarCode);

            Label lblPrintFileName = (Label)doc.GetElementById("lblPrintFileName");
            PageNumberingLabel PageNumberingLabel1 = (PageNumberingLabel)doc.GetElementById("PageNumberingLabel1");

            if (this.IsWebMode
                && HttpContext.Current.Session["showAllNotices"] != null
                && HttpContext.Current.Session["showAllNotices"].ToString().Equals("Y"))
                this.showAll = "Y";

            ////get additional parameters for date rules and sysparam setting
            //SysParamDB sp = new SysParamDB(this.SubProcess.ConnectionString);

            //string violationCutOff = "N";
            //int spValue = 0;

            //bool found = sp.CheckSysParam("ViolationCutOff", ref spValue, ref violationCutOff);

            //int noOfDaysIssue = GetDateRule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays;

            List<SqlParameter> paraList = new List<SqlParameter>();
            paraList.Add(new SqlParameter("@AutIntNo", this.AutIntNo));
            paraList.Add(new SqlParameter("@PrintFile", this.PrintFileName));
            paraList.Add(new SqlParameter("@Status", this.status));
            paraList.Add(new SqlParameter("@ShowAll", this.showAll));
            paraList.Add(new SqlParameter("@LastUser", this.SubProcess.LastUser));
            paraList.Add(new SqlParameter("@ViolationCutOff", new char()));
            paraList.Add(new SqlParameter("@NoOfDaysIssue", new int()));
            paraList.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = strSetGeneratedDate }); //2013-06-13 added by Nancy for 4973

            object objValue = ExecuteScalar(this.StoredProcedureName_Update, paraList, false);
            int iValue = -1;
            if (objValue == null || !int.TryParse(objValue.ToString(), out iValue) || iValue < 0)
            {
                CloseConnection();
                ErrorProcessing(string.Format("Error on executing {0}", this.StoredProcedureName_Update));
                return;
            }

            paraList.RemoveRange(4, 4);//2013-06-13 updated by Nancy from '(4, 3)' to '(4, 4)' for 4973
            paraList.Insert(2, new SqlParameter("@NotIntNo", 0));
            paraList.Add(new SqlParameter("@Option", this.option));

            DataSet ds = ExecuteDataSet("NoticePrint_WS", paraList, "@NotIntNo");

            string sTempEng = string.Empty;
            string sTempAfr = string.Empty;
            MergeDocument merge = new MergeDocument();
            byte[] buffer;
            Document report = null;
            int noOfNotices = 0;
            char pad0Char = Convert.ToChar("0");

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    #region
                    try
                    {
                        //dls 090710 - need to make all the string checks ToUpper - user doesn't necessarily use the same case
                        if (this.ReportFile.ToUpper().IndexOf("_CPI") >= 0)
                        {
                            lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblReference.Text = "VERWYSING/REFERENCE: " + dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblReferenceB.Text = "VERWYSING/REFERENCE: " + dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim();
                            lblPaymentInfo.Text = dr["AutNoticePaymentInfo"].ToString().Trim();
                        }

                        if (this.ReportFile.ToUpper().IndexOf("_PL") >= 0)
                        {
                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                            //lblFineAmount.Text = (dr["NoticeOption"].ToString().Equals("1") ? dr["ChgFineAmount"].ToString() : dr["ChgRevFineAmount"].ToString());

                            //update by Rachel 20140811 for 5337
                            //lblFineAmount.Text = string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                            lblFineAmount.Text = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]);
                            //end update by Rachel 20140811 for 5337

                            // mrs 20080627 added easypaynumber
                            //mrs 20081217 easypay number formatted in stored proc
                            //lblEasyPay.Text = ">>>>>" + dr["NotEasyPayNumber"].ToString().Trim();
                            //mrs 20090109 still need to fill the label
                            lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                        }

                        if (this.ReportFile.ToUpper().IndexOf("_ST") >= 0)
                        {
                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                            lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                            lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                            lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();
                            if (dr["NotSendTo"].ToString().Equals("P"))
                                lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                            else
                                lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                            lblOfficer.Text = dr["NotOfficerNo"].ToString();
                            //lblFineAmount.Text = "R " + (dr["NoticeOption"].ToString().Equals("1") ? dr["ChgFineAmount"].ToString() : dr["ChgRevFineAmount"].ToString());

                            //update by Rachel 20140811 for 5337
                            //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                            lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture,"{0:0.00}",dr["ChgRevFineAmount"]);
                            //end update by Rachel 20140811 for 5337
                        }

                        if (this.ReportFile.ToUpper().IndexOf("_FS") >= 0)
                        {
                            lblMtrName.Text = dr["MtrName"].ToString().Trim();
                            lblMtrDepart.Text = dr["MtrDepartName"].ToString().Trim();
                            lblMtrPostAddr.Text = (dr["MtrPostAddr1"].ToString().Trim().Equals("") ? "" : dr["MtrPostAddr1"].ToString().Trim() + "\n")
                                + (dr["MtrPostAddr2"].ToString().Trim().Equals("") ? "" : dr["MtrPostAddr2"].ToString().Trim() + "\n")
                                + (dr["MtrPostAddr3"].ToString().Trim().Equals("") ? "" : dr["MtrPostAddr3"].ToString().Trim() + "\n")
                                + (dr["MtrPostCode"].ToString().Trim().Equals("") ? "" : dr["MtrPostCode"].ToString().Trim());

                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                            lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                            lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                            lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();
                            if (dr["NotSendTo"].ToString().Equals("P"))
                                lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                            else
                                lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                            lblOfficer.Text = dr["NotOfficerNo"].ToString();

                            //update by Rachel 20140811 for 5337
                            //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                            lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]);
                            //end update by Rachel 20140811 for 5337
                        }

                        if (this.ReportFile.ToUpper().IndexOf("_RM") >= 0)
                        {
                            lblAuthName.Text = dr["AutName"].ToString().Trim();
                            lblAutDepart.Text = dr["AutDepartName"].ToString().Trim();

                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                            lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                            lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                            lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();
                            if (dr["NotSendTo"].ToString().Equals("P"))
                                lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                            else
                                lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                            lblOfficer.Text = dr["NotOfficerNo"].ToString();
                            //update by Rachel 20140811 for 5337
                            //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                            lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]);
                            //end update by Rachel 20140811 for 5337
                        }

                        if (this.ReportFile.ToUpper().IndexOf("_SW") >= 0)
                        {
                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                            lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                            lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();
                            lblPaymentInfo.Text = dr["AutNoticePaymentInfo"].ToString().Trim();
                            if (dr["NotSendTo"].ToString().Equals("P"))
                                lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                            else
                                lblId.Text = dr["DrvIDNUmber"].ToString().Trim();
                        }

                        //Modify by Tod Zhang on 090927 
                        if (this.ReportFile.ToUpper().IndexOf("_COFCT") >= 0)
                        {
                            if (this.ReportFile.ToUpper().IndexOf("AOG") < 0)
                            {
                                lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                lblOfficer.Text = dr["NotOfficerNo"].ToString();

                                //update by Rachel 20140811 for 5337
                                //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                                lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["ChgRevFineAmount"]);
                                //end update by Rachel 20140811 for 5337

                                lblpaypoint.Text = (dr["MtrPayPoint1Add1"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add1"].ToString().Trim() + "\n")
                                           + (dr["MtrPayPoint1Add2"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add2"].ToString().Trim() + "\n")
                                           + (dr["MtrPayPoint1Add3"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add3"].ToString().Trim() + "\n")
                                           + (dr["MtrPayPoint1Add4"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add4"].ToString().Trim() + "\n")
                                           + dr["MtrPayPoint1Code"].ToString();

                            }

                            if (dr["NotSendTo"].ToString().Equals("P"))
                                lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                            else
                                lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                            lblPrintFileName.Text = "Batch Number: " + dr["Not2ndNoticePrintFile"].ToString();

                            int page = noOfNotices + 1;
                            PageNumberingLabel1.Text = "Sequential Number: " + page.ToString().PadLeft(4, pad0Char);
                        }

                        if (this.ReportFile.ToUpper().IndexOf("_RLV_JMPD") >= 0)
                        {
                            //Jake 2011-02-25 Removed BarcodeNETImage 
                            //barcode = new BarcodeNETImage();
                            //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                            //barcode.ShowBarcodeText = false;
                            barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);

                            lblAddress.Text = dr["DrvPOAdd1"].ToString().Trim() + "\n" + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : dr["DrvPOAdd2"].ToString().Trim()) + "\n"
                                + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "\n" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "\n" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "\n" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                + dr["DrvPOCode"].ToString().Trim();

                            if (dr["NotSendTo"].ToString().Equals("P"))
                                lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                            else
                                lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();

                            lblCode.Text = dr["ChgOffenceCode"].ToString();
                            lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblAuthorityAddress.Text = (dr["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr3"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr4"].ToString().Trim() + "\n")
                                        + dr["AutPhysCode"].ToString();
                            lblAutTel.Text = dr["AutTel"].ToString();
                            lblAutFax.Text = dr["AutFax"].ToString();
                            lblOffenceDescrAfr.Text = "DEURDAT die bestuurder van voertuig " + dr["NotVehicleMake"].ToString().Trim() +
                                " met registrasienommer " + dr["NotRegNo"].ToString().Trim() + " op " + string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]) +
                                " om " + string.Format("{0:HH:mm}", dr["NotOffenceDate"]) + " en te \n" +
                            dr["NotLocDescr"].ToString() + " n openbare pad in die distrik van " + dr["AutName"].ToString() + " , n rooi verkeerslig verontagsaam";
                            lblOffenceDescrEng.Text = "IN THAT the driver of motor vehicle " + dr["NotVehicleMake"].ToString().Trim() +
                                " with registration number " + dr["NotRegNo"].ToString().Trim() + " on " + string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]) +
                                " at " + string.Format("{0:HH:mm}", dr["NotOffenceDate"]) + " and at \n" +
                            dr["NotLocDescr"].ToString() + " a public road in the district of " + dr["AutName"].ToString() + " , failed to obey a red light";
                            lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);
                            //dls 071221 - added reprint date
                            lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint2ndNoticeDate"]);
                        }

                        if (this.ReportFile.ToUpper().IndexOf("_GE") >= 0 
                            || this.ReportFile.ToUpper().IndexOf("_BV") >= 0
                            || this.ReportFile.ToUpper().IndexOf("_TW") >= 0 //2013-03-25 added by Henry for TW
                            )
                        {
                            //barcode = new BarcodeNETImage();
                            //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                            //barcode.ShowBarcodeText = false;
                            // add by richard 2011-06-03

                            lblAuthName.Text = dr["AutName"].ToString().Trim();

                            //client has changed there mind about the Court Name on the BV notice
                            if (lblCourt != null)
                                lblCourt.Text = dr["NotCourtName"].ToString();

                            if (lbl_AuthName2 != null)
                                lbl_AuthName2.Text = dr["AutName"].ToString();

                            lblAutPhysAddr.Text = (dr["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr1"].ToString().Trim() + "\n")
                                + (dr["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr2"].ToString().Trim() + "\n")
                                + (dr["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr3"].ToString().Trim() + "\n")
                                + (dr["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr4"].ToString().Trim() + "\n")
                                + dr["AutPhysCode"].ToString();

                            // 2011-7-21 add
                            if (this.ReportFile.ToUpper().IndexOf("_GE") >= 0)
                            {
                                lblAutPostAddr.Text = (dr["AutPostAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr1"].ToString().Trim() + "\n")
                                    + (dr["AutPostAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr2"].ToString().Trim() + "\n")
                                    + (dr["AutPostAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr3"].ToString().Trim() + "\n")
                                    + dr["AutPostCode"].ToString().Trim();


                                lblpaypoint.Text = (dr["MtrPayPoint1Add1"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add1"].ToString().Trim() + "\n")
                                                + (dr["MtrPayPoint1Add2"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add2"].ToString().Trim() + "\n")
                                                + (dr["MtrPayPoint1Add3"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add3"].ToString().Trim() + "\n")
                                                + (dr["MtrPayPoint1Add4"].ToString().Trim().Equals("") ? "" : dr["MtrPayPoint1Add4"].ToString().Trim() + "\n")
                                                + dr["MtrPayPoint1Code"].ToString();

                                //Jerry 2012-05-03 add
                                if (this.ReportFile.ToUpper().IndexOf("AOG") == -1)
                                    lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                            }



                            barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);

                            if (dr["NotSendTo"].ToString().Equals("P"))
                            {
                                lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                            }
                            else
                            {
                                lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();
                                lblId.Text = dr["DrvIDNUmber"].ToString().Trim();
                            }

                            lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                 + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                 + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                 + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                 + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                 + dr["DrvPOCode"].ToString().Trim();

                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");

                            lblStatRef.Text = (dr["ChgStatRefLine1"] == DBNull.Value ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                + (dr["ChgStatRefLine2"] == DBNull.Value ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                + (dr["ChgStatRefLine3"] == DBNull.Value ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                + (dr["ChgStatRefLine4"] == DBNull.Value ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                            lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                            lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                            lblLocDescr.Text = dr["NotLocDescr"].ToString();
                            lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                            lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();

                            sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                            sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());

                            if (dr["NotNewOffender"].ToString().Equals("Y"))
                            {
                                lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                            }
                            else
                            {
                                lblOffenceDescrEng.Text = sTempEng;
                                lblOffenceDescrAfr.Text = sTempAfr;
                            }

                            lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["Not2ndPaymentDate"]);
                            lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();
                            lblCode.Text = dr["ChgOffenceCode"].ToString();

                            //lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                            // David Lin 20100812 change layout for ASD 2nd Notice
                            if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0)
                            {
                                lblCameraA.Text = dr["CameraNoA"].ToString();
                                lblCameraB.Text = dr["CameraNoB"].ToString();
                                lblMeasurement.Text = dr["MeasurementDistance"].ToString() + " metres";
                                lblTimeDifference.Text = dr["TimeDifference"].ToString() + " seconds";
                            }
                            else
                            {
                                lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                            }

                            lblOfficerNo.Text = dr["NotOfficerNo"].ToString();

                            //update by Rachel 20140811 for 5337
                            //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                            lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]);
                            //end update by Rachel 20140811 for 5337

                            lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);
                            lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint2ndNoticeDate"]);

                            lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                            lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                            lblLocation.Text = dr["NotLocDescr"].ToString();
                            lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                            lblSpeed.Text = dr["NotSpeed1"].ToString();
                            //lblOfficer.Text = dr["NotOfficerNo"].ToString();  //dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                            lblRegNo.Text = dr["NotRegNo"].ToString();
                            lblDisclaimer.Text = dr["Disclaimer"].ToString();

                            lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                        }

                        else
                        {
                            //2010/9/14 jerry changed for MB
                            if (this.ReportFile.ToUpper().IndexOf("_MB") >= 0)
                            {
                                //barcode = new BarcodeNETImage();
                                //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                                //barcode.ShowBarcodeText = false;
                                // add by richard 2011-06-03

                                lblAuthName.Text = dr["AutName"].ToString().Trim();
                                lbl_AuthName2.Text = dr["AutName"].ToString();
                                lblCourt.Text = dr["NotCourtName"].ToString().Trim();
                                lbl_CourtName.Text = dr["NotCourtName"].ToString().Trim();
                                lblAutPhysAddr.Text = (dr["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr3"].ToString().Trim() + "\n")
                                        + (dr["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr4"].ToString().Trim() + "\n")
                                        + dr["AutPhysCode"].ToString();
                                lblAutPostAddr.Text = (dr["AutPostAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPostAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPostAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr3"].ToString().Trim() + "\n")
                                        + dr["AutPostCode"].ToString();


                                barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);


                                if (dr["NotSendTo"].ToString().Equals("P"))
                                {
                                    lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                    lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                                }
                                else
                                {
                                    lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();
                                    lblId.Text = dr["DrvIDNUmber"].ToString().Trim();
                                }

                                lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                     + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                     + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                     + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                     + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                     + dr["DrvPOCode"].ToString().Trim();

                                lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");

                                lblStatRef.Text = (dr["ChgStatRefLine1"] == DBNull.Value ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                    + (dr["ChgStatRefLine2"] == DBNull.Value ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                    + (dr["ChgStatRefLine3"] == DBNull.Value ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                    + (dr["ChgStatRefLine4"] == DBNull.Value ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                                lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                lblLocDescr.Text = dr["NotLocDescr"].ToString();
                                lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();

                                sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());

                                if (dr["NotNewOffender"].ToString().Equals("Y"))
                                {
                                    lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                    lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                                }
                                else
                                {
                                    lblOffenceDescrEng.Text = sTempEng;
                                    lblOffenceDescrAfr.Text = sTempAfr;
                                }

                                lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["Not2ndPaymentDate"]);
                                //lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();
                                lblCode.Text = dr["ChgOffenceCode"].ToString();

                                //lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                // David Lin 20100812 change layout for ASD 2nd Notice
                                if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0)
                                {
                                    lblCameraA.Text = dr["CameraNoA"].ToString();
                                    lblCameraB.Text = dr["CameraNoB"].ToString();
                                    lblMeasurement.Text = dr["MeasurementDistance"].ToString() + " metres";
                                    lblTimeDifference.Text = dr["TimeDifference"].ToString() + " seconds";
                                }
                                else
                                {
                                    lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                }

                                lblOfficerNo.Text = dr["NotOfficerNo"].ToString();

                                //update by Rachel 20140811 for 5337
                                //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                                lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]);
                                //end update by Rachel 20140811 for 5337

                                lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);
                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint2ndNoticeDate"]);

                                lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                lblLocation.Text = dr["NotLocDescr"].ToString();
                                lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                                lblSpeed.Text = dr["NotSpeed1"].ToString();
                                //lblOfficer.Text = dr["NotOfficerNo"].ToString();  //dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                lblRegNo.Text = dr["NotRegNo"].ToString();
                                lblDisclaimer.Text = dr["Disclaimer"].ToString();

                                lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();

                            }
                            else
                            {

                                if (dr["NotSendTo"].ToString().Equals("P"))
                                    lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                else
                                    lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();

                                lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                     + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                     + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                     + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                     + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                     + dr["DrvPOCode"].ToString().Trim();

                                lblStatRef.Text = (dr["ChgStatRefLine1"] == DBNull.Value ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                    + (dr["ChgStatRefLine2"] == DBNull.Value ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                    + (dr["ChgStatRefLine3"] == DBNull.Value ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                    + (dr["ChgStatRefLine4"] == DBNull.Value ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                                //barcode = new BarcodeNETImage();
                                //barcode.BarcodeText = dr["NotTicketNo"].ToString();
                                //barcode.ShowBarcodeText = false;
                                barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);


                                lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                lblLocDescr.Text = dr["NotLocDescr"].ToString();
                                sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());

                                if (dr["NotNewOffender"].ToString().Equals("Y"))
                                {
                                    lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                    lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                                }
                                else
                                {
                                    lblOffenceDescrEng.Text = sTempEng;
                                    lblOffenceDescrAfr.Text = sTempAfr;
                                }

                                //lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPaymentDate"]);
                                lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["Not2ndPaymentDate"]);
                                lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();
                                lblCode.Text = dr["ChgOffenceCode"].ToString();

                                //lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                // David Lin 20100812 change layout for ASD 2nd Notice
                                if (this.ReportFile.ToUpper().IndexOf("ASD") >= 0)
                                {
                                    lblCameraA.Text = dr["CameraNoA"].ToString();
                                    lblCameraB.Text = dr["CameraNoB"].ToString();
                                    lblMeasurement.Text = dr["MeasurementDistance"].ToString() + " metres";
                                    lblTimeDifference.Text = dr["TimeDifference"].ToString() + " seconds";
                                }
                                else
                                {
                                    lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                }

                                lblOfficerNo.Text = dr["NotOfficerNo"].ToString();
                                //lblFineAmount.Text = "R " + (dr["NoticeOption"].ToString().Equals("1") ? dr["ChgFineAmount"].ToString() : dr["ChgRevFineAmount"].ToString());
                                lblIssuedBy.Text = dr["AutNoticeIssuedByInfo"].ToString();

                                if (this.ReportFile.ToUpper().IndexOf("_ST") < 0)
                                {
                                    lblAut.Text = (dr["AutName"].ToString().ToUpper().Trim().Equals("") ? "" : dr["AutName"].ToString().ToUpper().Trim() + "\n") +
                                    (dr["AutPostAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr1"].ToString().Trim() + "\n")
                                        + (dr["AutPostAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr2"].ToString().Trim() + "\n")
                                        + (dr["AutPostAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr3"].ToString().Trim() + "\n")
                                        + dr["AutPostCode"].ToString();
                                }

                                lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);
                                //dls 071221 - added reprint date
                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint2ndNoticeDate"]);
                                lblReprintDateText.Text = "Reprint date:";
                                lblOffenceDate.Text = string.Format(
                                    this.ReportFile.ToUpper().IndexOf("_COFCT") >= 0 ? "{0:dd MMM yyyy}" : "{0:yyyy-MM-dd}", //Henry 2013-03-26
                                    dr["NotOffenceDate"]);
                                lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                lblLocation.Text = dr["NotLocDescr"].ToString();
                                lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                                lblSpeed.Text = dr["NotSpeed1"].ToString();
                                //lblOfficer.Text = dr["NotOfficerNo"].ToString();  //dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                                lblRegNo.Text = dr["NotRegNo"].ToString();
                                //mrs 20080212 added disclaimer
                                lblDisclaimer.Text = dr["Disclaimer"].ToString();

                                //b = (byte[])dr["ScanImage1"];
                            }
                        }

                        report = doc.Run(parameters);

                        ImportedPageArea importedPage;
                        byte[] bufferTemplate;
                        if (!this.TemplateFile.Equals(""))
                        {
                            if (this.TemplateFile.ToLower().IndexOf(".dplx") > 0)
                            {
                                DocumentLayout template = new DocumentLayout(this.TemplateFilePath);
                                StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                                ParameterDictionary parametersTemplate = new ParameterDictionary();
                                Document reportTemplate = template.Run(parametersTemplate);
                                bufferTemplate = reportTemplate.Draw();
                                PdfDocument pdf = new PdfDocument(bufferTemplate);
                                PdfPage page = pdf.Pages[0];
                                importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                            }
                            else
                            {
                                //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                importedPage = new ImportedPageArea(this.TemplateFilePath, 1, 0.0F, 0.0F, 1.0F);
                            }

                            //Jerry 2013-06-19 change it
                            //ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                            //rptPage.Elements.Insert(0, importedPage);
                            report.Template = new Template();
                            report.Template.Elements.Add(importedPage);
                        }
                        buffer = report.Draw();
                        merge.Append(new PdfDocument(buffer));

                        buffer = null;
                        bufferTemplate = null;
                    }
                    catch (Exception ex)
                    {
                        ErrorProcessing(ex.Message);
                        return;
                    }

                    noOfNotices++;
                    #endregion
                }

                if (!this.PrintFileName.Substring(0, 1).Equals("*") && !this.PrintFileName.Equals("-1"))
                {
                    //dls 071228 - add summary report page and append to end of PDF document
                    string reportFile = "NoticeSummary.dplx";
                    string path;
                    CheckReportTemplateExists(null, reportFile, out path);

                    NoticeSummary summary = new NoticeSummary(this.SubProcess.ConnectionString, this.AutIntNo, this.PrintFileName, format, this.showAll, this.status, this.option, path, reportFile, noOfNotices);

                    byte[] sumReport = summary.CreateSummary();

                    merge.Append(new PdfDocument(sumReport));

                    sumReport = null;
                }

            }

            if (merge.Pages.Count > 0)
            {
                //this.OutputBuffer = merge.Draw();
                CreateTempFile(merge);
            }

            phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);
        }

        void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (barCodeImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 25.0F;
                    e.ContentArea.Add(img);
                }

                int generation = System.GC.GetGeneration(barCodeImage);
                barCodeImage = null;
                System.GC.Collect(generation);
            }
        }
    }
}
