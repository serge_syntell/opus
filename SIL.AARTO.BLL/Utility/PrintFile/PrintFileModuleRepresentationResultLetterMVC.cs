﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Merger;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.Web.Resource.Enquiry;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleRepresentationResultLetterMVC : PrintFileBase
      {

        bool printFlag = false;
        int repIntNo = -1;
        string sLetterTo = string.Empty;
        string sTicketNo = string.Empty;

        public PrintFileModuleRepresentationResultLetterMVC(bool printFlag, int repIntNo, string sLetterTo = null, string sTicketNo = null)
        {
           // this.ErrorPage = GetResource("Representation_LetterViewer.aspx", "thisPage");
           
            //this.ErrorPage = "Representation Result Letter";
            this.ErrorPageUrl = "RepresentationLetterViewer";

            this.printFlag = printFlag;
            this.repIntNo = repIntNo;
            this.sLetterTo = sLetterTo;
            this.sTicketNo = sTicketNo;
        }

        public override void InitialWork()
        {
            string function = string.Empty;
            string defaultReportPage = string.Empty;
            
            function = "RepresentationLetter";
            defaultReportPage = "RepresentationLetter_PL.dplx";

            //set ReportFile
            this.ReportFile = this.ReportDB.GetAuthReportName(this.AutIntNo, function);
            if (string.IsNullOrWhiteSpace(this.ReportFile))
            {
                this.ReportFile = defaultReportPage;
                this.ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, function, "system", string.Empty);
            }

            // check report template file exists
            string path;
            if (!CheckReportTemplateExists(function, null, out path)) return;
        }

        public override void MainWork()
        {
            DocumentLayout doc = new DocumentLayout(this.ReportFilePath);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.SubProcess.ConnectionString;

            byte[] buffer = null;
            byte[] bufferTemplate = null;

            RecordArea recordChanged = (RecordArea)doc.GetElementById("recordChanged");
            RecordArea recordUnsuccessful = (RecordArea)doc.GetElementById("recordUnsuccessful");
            RecordArea recordWithdrawn = (RecordArea)doc.GetElementById("recordWithdrawn");
            RecordArea recordName = (RecordArea)doc.GetElementById("recordName");
            RecordArea recordEnglishOffence = (RecordArea)doc.GetElementById("recordEnglishOffence");
            RecordArea recordAfrikaansOffence = (RecordArea)doc.GetElementById("recordAfrikaansOffence");
            RecordArea recordOriginalAmount = (RecordArea)doc.GetElementById("recordOriginalAmount");
            RecordArea recordSumCourtDate = (RecordArea)doc.GetElementById("recordSumCourtDate");

            recordChanged.LaidOut += new RecordAreaLaidOutEventHandler(ph_LaidOut);
            recordUnsuccessful.LaidOut += new RecordAreaLaidOutEventHandler(recordUnsuccessful_LaidOut);
            recordWithdrawn.LaidOut += new RecordAreaLaidOutEventHandler(recordWithdrawn_LaidOut);
            recordName.LayingOut += new LayingOutEventHandler(recordName_LayingOut);
            recordEnglishOffence.LayingOut += new LayingOutEventHandler(recordEnglishOffence_LayingOut);
            recordAfrikaansOffence.LayingOut += new LayingOutEventHandler(recordAfrikaansOffence_LayingOut);
            recordOriginalAmount.LayingOut += new LayingOutEventHandler(recordOriginalAmount_LayingOut);

            if (recordSumCourtDate != null)
                recordSumCourtDate.LayingOut += new LayingOutEventHandler(recordSumCourtDate_LayingOut);

            //2011-10-18 jerry change
            if (string.IsNullOrEmpty(this.PrintFileName))
            {
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("RepIntNo", repIntNo);
                parameters.Add("NotTicketNo", sTicketNo);
                parameters.Add("AutIntNo", AutIntNo);

                if (!this.TemplateFile.Equals(""))
                {
                    DocumentLayout template = new DocumentLayout(TemplateFilePath);
                    StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                    queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                    ParameterDictionary parametersTemplate = new ParameterDictionary();
                    Document reportTemplate = template.Run(parametersTemplate);
                    bufferTemplate = reportTemplate.Draw();
                }

                Document report = doc.Run(parameters);
                ImportedPageArea importedPage;
                if (!this.TemplateFile.Equals(""))
                {
                    if (this.TemplateFile.ToLower().IndexOf(".dplx") > 0)
                    {
                        PdfDocument pdf = new PdfDocument(bufferTemplate);
                        PdfPage page = pdf.Pages[0];
                        importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                    }
                    else
                    {
                        //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                        importedPage = new ImportedPageArea(this.TemplateFilePath, 1, 0.0F, 0.0F, 1.0F);
                    }

                    //ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                    //rptPage.Elements.Insert(0, importedPage);

                    report.Template = new Template();
                    report.Template.Elements.Add(importedPage);
                }

                this.OutputBuffer = report.Draw();
                
            }
            else
            {
                RepresentationDB represent = new RepresentationDB(this.SubProcess.ConnectionString);

                List<SqlParameter> paraList = new List<SqlParameter>();
                paraList.Add(new SqlParameter("@PrintFileName", this.PrintFileName));
                paraList.Add(new SqlParameter("@PrintedFlag", this.printFlag));

                DataSet ds;
                if (this.IsWebMode)
                {
                    ds = ExecuteDataSet("RepresentationGetRepInfoByPrintFile", paraList);
                }
                else
                {
                    ds = ExecuteDataSet("RepresentationGetRepInfoByPrintFile_WS", paraList);
                    //Jerry 2012-06-20 change the status when windows service is coming
                    represent.UpdateRepresentationLetterPrinted(this.AutIntNo, this.PrintFileName);
                }

                //SqlDataReader reader = represent.GetRepInfoByPrintFile(this.PrintFileName, this.printFlag);

                MergeDocument merge = new MergeDocument();
                Document report;
                
                //Jerry 2012-06-28 handle no data
                bool hasData = false;

                foreach (DataTable dt in ds.Tables)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ParameterDictionary parameters = new ParameterDictionary();
                            parameters.Add("RepIntNo", Convert.ToInt32(dr["RepIntNo"]));
                            parameters.Add("NotTicketNo", dr["NotTicketNo"]);
                            parameters.Add("AutIntNo", AutIntNo);
                            //dls 2012-03-01 - need this for printing the offender details
                            this.sLetterTo = dr["LetterTo"].ToString();

                            if (!this.TemplateFile.Equals(""))
                            {
                                DocumentLayout template = new DocumentLayout(this.TemplateFilePath);
                                StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                                ParameterDictionary parametersTemplate = new ParameterDictionary();
                                Document reportTemplate = template.Run(parametersTemplate);
                                bufferTemplate = reportTemplate.Draw();
                            }

                            report = doc.Run(parameters);
                            ImportedPageArea importedPage;
                            if (!this.TemplateFile.Equals(""))
                            {
                                if (this.TemplateFile.ToLower().IndexOf(".dplx") > 0)
                                {
                                    PdfDocument pdf = new PdfDocument(bufferTemplate);
                                    PdfPage page = pdf.Pages[0];
                                    importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                                }
                                else
                                {
                                    //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                    importedPage = new ImportedPageArea(this.TemplateFilePath, 1, 0.0F, 0.0F, 1.0F);
                                }

                                //ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                                //rptPage.Elements.Insert(0, importedPage);

                                report.Template = new Template();
                                report.Template.Elements.Add(importedPage);
                            }
                            buffer = report.Draw();
                            merge.Append(new PdfDocument(buffer));

                            buffer = null;
                            bufferTemplate = null;

                            hasData = true;
                        }
                    }
                }

                ds.Dispose();

                //Jerry 2012-06-28 handle no data
                if (!hasData)
                {
                    this.SubProcess.SkipNextProcesses = true;
                    this.SubProcess.Message = "NoData";
                }
                else
                {
                    this.OutputBuffer = merge.Draw();
                }
            }
        }

        #region use for Representation_LetterViewer.aspx page

        private void recordAfrikaansOffence_LayingOut(object sender, LayingOutEventArgs e)
        {
            RecordArea lbl = (RecordArea)sender;

            //check to proc for which reg no is getting used here. 
            //Use OrigRegNo if ChangeOfRegNo=Y and LetterTo=O
            string regNo = e.LayoutWriter.RecordSets.Current["NotRegNo"].ToString().Trim();
            string speedZone = e.LayoutWriter.RecordSets.Current["NotSpeedLimit"].ToString().Trim();
            string speed = e.LayoutWriter.RecordSets.Current["MinSpeed"].ToString().Trim();

            string description = e.LayoutWriter.RecordSets.Current["OcTDescr1"].ToString().Trim();
            // Remove the first sentence
            int pos = description.IndexOf('.');
            if (pos >= 0 && pos + 1 < description.Length)
                description = description.Substring(pos + 1);

            lbl.Text = description.Replace("(X~1)", regNo).Replace("(X~2)", speedZone).Replace("(X~3)", speed);
        }

        private void recordEnglishOffence_LayingOut(object sender, LayingOutEventArgs e)
        {
            RecordArea lbl = (RecordArea)sender;

            //check to proc for which reg no is getting used here. Use OrigRegNo if ChangeOfRegNo=Y and LetterTo=O
            string regNo = e.LayoutWriter.RecordSets.Current["NotRegNo"].ToString().Trim();
            string speedZone = e.LayoutWriter.RecordSets.Current["NotSpeedLimit"].ToString().Trim();
            string speed = e.LayoutWriter.RecordSets.Current["MinSpeed"].ToString().Trim();

            string description = e.LayoutWriter.RecordSets.Current["OcTDescr"].ToString().Trim();
            // Remove the first sentence
            int pos = description.IndexOf('.');
            if (pos >= 0 && pos + 1 < description.Length)
                description = description.Substring(pos + 1);

            lbl.Text = description.Replace("(X~1)", regNo).Replace("(X~2)", speedZone).Replace("(X~3)", speed);
        }

        private void recordOriginalAmount_LayingOut(object sender, LayingOutEventArgs e)
        {
            RecordArea lbl = (RecordArea)sender;

            decimal originalAmount = Convert.ToDecimal(e.LayoutWriter.RecordSets.Current["ChgFineAmount"]);
            if (originalAmount == 999999)
            {
                lbl.Text = RepresentationLetterViewerRes.lblText;//GetResource("Representation_LetterViewer.aspx", "lbl.Text");
                //lbl.Text = "[ No AoG ]";
            }
            else
                lbl.Text = e.LayoutWriter.RecordSets.Current["OriginalAmount"].ToString();
        }

        private void recordSumCourtDate_LayingOut(object sender, LayingOutEventArgs e)
        {
            RecordArea lbl = (RecordArea)sender;

            DateTime sumCourtDate;
            string crtDate = e.LayoutWriter.RecordSets.Current["SumCourtDate"].ToString();

            if (!DateTime.TryParse(crtDate, out sumCourtDate))
                lbl.Text = string.Empty;
            else
                lbl.Text = string.Format("{0:yyyy/MM/dd}", sumCourtDate);
        }

        void recordName_LayingOut(object sender, LayingOutEventArgs e)
        {
            RecordArea lbl = (RecordArea)sender;
            StringBuilder sb = new StringBuilder();
            string temp = string.Empty;

            object o = e.LayoutWriter.RecordSets.Current["NotProxyFlag"];
            if (o == null)
                return;

            string proxyFlag = o.ToString().Trim();
            bool isProxy = proxyFlag.Equals("Y", StringComparison.InvariantCultureIgnoreCase);

            //dls 070730 - first need to check whether to send letter to owner/driver, and only check for Proxy if its going to the owner\
            // LMZ 2007-07-27 change to allow switch to effect owner driver printing
            if (this.sLetterTo == "O")  // Owner (using O and D instead of 0 & 1)
            {
                if (isProxy) //Proxy
                {
                    temp = e.LayoutWriter.RecordSets.Current["PrxInitials"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append(' ');
                    }
                    sb.Append(e.LayoutWriter.RecordSets.Current["PrxSurname"].ToString().Trim());
                    sb.Append("\n");
                    temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd1"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd2"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd3"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd4"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd5"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["PrxPOCode"].ToString().Trim();
                    if (temp.Length > 0)
                        sb.Append(temp);
                }
                else  // owner
                {
                    temp = e.LayoutWriter.RecordSets.Current["OwnInitials"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append(' ');
                    }
                    sb.Append(e.LayoutWriter.RecordSets.Current["OwnSurname"].ToString().Trim());
                    sb.Append("\n");
                    temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd1"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd2"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd3"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd4"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd5"].ToString().Trim();
                    if (temp.Length > 0)
                    {
                        sb.Append(temp);
                        sb.Append("\n");
                    }
                    temp = e.LayoutWriter.RecordSets.Current["OwnPOCode"].ToString().Trim();
                    if (temp.Length > 0)
                        sb.Append(temp);
                }
            }
            else if (sLetterTo == "D") // driver
            {
                temp = e.LayoutWriter.RecordSets.Current["DrvInitials"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append(' ');
                }
                sb.Append(e.LayoutWriter.RecordSets.Current["DrvSurname"].ToString().Trim());
                sb.Append("\n");
                temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd1"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd2"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd3"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd4"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd5"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["DrvPOCode"].ToString().Trim();
                if (temp.Length > 0)
                    sb.Append(temp);
            }
            else if (sLetterTo == "S") // Summons - Accused
            {
                temp = e.LayoutWriter.RecordSets.Current["AccInitials"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append(" ");
                }
                temp = e.LayoutWriter.RecordSets.Current["AccSurname"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["AccPOAdd1"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["AccPOAdd2"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["AccPOAdd3"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["AccPOAdd4"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["AccPOAdd5"].ToString().Trim();
                if (temp.Length > 0)
                {
                    sb.Append(temp);
                    sb.Append("\n");
                }
                temp = e.LayoutWriter.RecordSets.Current["AccPOCode"].ToString().Trim();
                if (temp.Length > 0)
                    sb.Append(temp);
            }

            lbl.Text = sb.ToString();
        }

        void recordWithdrawn_LaidOut(object sender, RecordAreaLaidOutEventArgs e)
        {
            object o = e.LayoutWriter.RecordSets.Current["RepresentationCode"];
            if (o == null)
                return;

            string repCode = o.ToString().Trim();

            switch (repCode)
            {
                //case "200": // Summons
                case "13":      // Summons
                case "1":
                case "3":
                    e.ReportTextArea.TextColor = CmykColor.Black;
                    return;
            }
            e.ReportTextArea.TextColor = CmykColor.White;
        }

        void recordUnsuccessful_LaidOut(object sender, RecordAreaLaidOutEventArgs e)
        {
            object o = e.LayoutWriter.RecordSets.Current["RepresentationCode"];
            if (o == null)
                return;

            string repCode = o.ToString().Trim();
            switch (repCode)
            {
                //case "220": // Summons
                case "15": // Summons
                case "5":
                    e.ReportTextArea.TextColor = CmykColor.Black;
                    return;
            }
            e.ReportTextArea.TextColor = CmykColor.White;
        }

        void ph_LaidOut(object sender, RecordAreaLaidOutEventArgs e)
        {
            // Reduced Fine
            object o = e.LayoutWriter.RecordSets.Current["RepresentationCode"];
            if (o == null)
                return;

            string repCode = o.ToString().Trim();
            switch (repCode)
            {
                //case "210": // Summons
                case "14": // Summons      
                case "4":
                    e.ReportTextArea.TextColor = CmykColor.Black;
                    return;
            }
            e.ReportTextArea.TextColor = CmykColor.White;
        }

        #endregion
    }
}