﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrystalDecisions.Shared;
using Stalberg.TMS.Data.Datasets;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleSummons : PrintFileBase
    {
        public PrintFileModuleSummons(string summary = null, string mode = null)
        {
            //this.FileType = PrintFileType.RPT;
            this.ErrorPage = GetResource("SummonsViewer.aspx", "thisPage");
            this.ErrorPageUrl = "SummonsViewer.aspx";

            this.summary = summary;
            //this.isSummary = !string.IsNullOrWhiteSpace(summary);
            this.isSummary = summary != null;
            //this.mode = string.IsNullOrWhiteSpace(mode) ? "N" : mode.Trim().ToUpper();
            this.mode = mode == null ? "N" : mode.Trim().ToUpper();
        }

        string summary, mode;
        bool isSummary;

        string spNoticePrintSummonsUpdate = "NoticePrintSummons_Update_WS";
        string spNoticePrintSummonsControlUpdate = "NoticePrintSummonsControl_Update_WS";
        string spNoticePrintSummonsHWOUpdate = "NoticePrintSummonsHWO_Update_WS";
        string spNoticePrintSummons = "NoticePrintSummons_WS";
        string spNoticePrintSummonsControl = "NoticePrintSummonsControl_WS";
        string spNoticePrintSummonsHWO = "NoticePrintSummonsHWO_WS";
        string spNoticePrintSummons_COCT = "NoticePrintSummons_COCT";
        string spNoticePrintSummonsHWO_COCT = "NoticePrintSummonsHWO_COCT";

        public override void InitialWork()
        {
            #region set notice film type
            string noticeFilmType = string.Empty;
            bool isVideo =false;
            if (this.PrintFileName.IndexOf("*") >= 0)
            {
                NoticeDB _noticeDB = new NoticeDB(this.SubProcess.ConnectionString);
                if (this.mode == "N")
                {
                    string noticeTicketNo = this.PrintFileName.Substring(1, this.PrintFileName.Length - 1);
                    NoticeForPaymentDB noticeDB = new NoticeForPaymentDB(this.SubProcess.ConnectionString);
                    noticeFilmType = noticeDB.GetFilmType(noticeTicketNo.Replace("-", string.Empty).Replace("/", string.Empty));
                    //Heidi 2014-06-06 added if isVideo=1 should be cpa5 printing
                    isVideo = _noticeDB.GetIsVideo(noticeTicketNo,"",this.mode);
                }
                else
                {
                    string summonsNo = this.PrintFileName.Substring(1, this.PrintFileName.Length - 1);
                    SummonsDB summonsDB = new SummonsDB(this.SubProcess.ConnectionString);
                    noticeFilmType = summonsDB.GetNotFilmTypeBySummonsNo(summonsNo);
                    //Heidi 2014-06-06 added if isVideo=1 should be cpa5 printing
                    isVideo = _noticeDB.GetIsVideo("", summonsNo,this.mode);
                }
            }
            #endregion

            #region set report template file and stored procedure
            //this.StoredProcedureName_Select = string.IsNullOrWhiteSpace(this.summary) ? this.spNoticePrintSummons : this.spNoticePrintSummonsControl;
            this.StoredProcedureName_Select = this.summary == null ? this.spNoticePrintSummons : this.spNoticePrintSummonsControl;

            if (this.PrintFileName.IndexOf("CPA6", StringComparison.OrdinalIgnoreCase) >= 0 || (noticeFilmType == "H" && !isVideo)) //Heidi 2014-06-06 added if isVideo=1 should be cpa5 printing
                this.ReportFile = this.ReportDB.GetAuthReportName(this.AutIntNo, "Summons_HWO");
            else
                this.ReportFile = this.ReportDB.GetAuthReportName(this.AutIntNo, "Summons");

            if (!this.isSummary)
            {
                if (this.ReportFile.Equals(string.Empty))
                {
                    this.ReportFile = "Summons_CPA5.rpt";
                    this.ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, "Summons", "System", "");
                }
            }
            else
                this.ReportFile = "Summons_Personal_Summary.rpt";

            if (this.ReportFile.Equals("Summons_HWO_CPA6.rpt")) 
                this.StoredProcedureName_Select = this.spNoticePrintSummonsHWO;

            //2013-09-04 added by Nancy start(For the new CPA5&CPA6 report in Road Lock)
            //2014-07-11 Heidi changed for HS CPA5 Summons(5318)
            if (this.ReportFile.Equals("Summons_COCT_CPA5.rpt") || this.ReportFile.Equals("Summons_HS_CPA5.rpt"))
                this.StoredProcedureName_Select = this.spNoticePrintSummons_COCT;
            if (this.ReportFile.Equals("Summons_COCT_CPA6.rpt")) 
                this.StoredProcedureName_Select = this.spNoticePrintSummonsHWO_COCT;
            //2013-09-04 added by Nancy end
            #endregion

            // check report template file exists
            string path;
            if (!CheckReportTemplateExists("Summons", null, out path)) return;

        }

        public override void MainWork()
         {
            //int noOfDays = GetDateRules("NotOffenceDate", "CRCourtDate").DtRnoOfDays;
            bool isCourtNumber = GetAuthorityRule("5040").ARString.Trim().ToUpper() == "N";
            string strSetGeneratedDate = GetAuthorityRule("2570").ARString.Trim().ToUpper(); //2013-06-17 added by Nancy for 4973 

            #region load and initialize rpt file
            ReportDocument report = new ReportDocument();
            report.Load(this.ReportFilePath);

            // Get the PageMargins structure and set the margins for the report.
            PageMargins margins = report.PrintOptions.PageMargins;
            margins.leftMargin = 0;
            margins.topMargin = 0;
            margins.rightMargin = 0;
            margins.bottomMargin = 0;

            // Apply the page margins.
            report.PrintOptions.ApplyPageMargins(margins);

            if (this.StoredProcedureName_Select == this.spNoticePrintSummons || this.StoredProcedureName_Select == this.spNoticePrintSummonsHWO || this.StoredProcedureName_Select == this.spNoticePrintSummons_COCT || this.StoredProcedureName_Select == this.spNoticePrintSummonsHWO_COCT)//2013-09-04 updated by Nancy for the new CPA6 report in Road Lock
                report.PrintOptions.PaperSize = PaperSize.PaperFanfoldStdGerman;
            else
                report.PrintOptions.PaperSize = PaperSize.DefaultPaperSize;
            #endregion

            #region get the data source

            if (this.StoredProcedureName_Select == this.spNoticePrintSummons || this.StoredProcedureName_Select == this.spNoticePrintSummons_COCT)//2013-09-04 updated by Nancy for the new CPA6 report in Road Lock
                this.StoredProcedureName_Update = this.spNoticePrintSummonsUpdate;
            else if (this.StoredProcedureName_Select == this.spNoticePrintSummonsControl)
                this.StoredProcedureName_Update = this.spNoticePrintSummonsControlUpdate;
            else if (this.StoredProcedureName_Select == this.spNoticePrintSummonsHWO || this.StoredProcedureName_Select == this.spNoticePrintSummonsHWO_COCT) //2013-09-04 updated by Nancy for the new CPA6 report in Road Lock
                this.StoredProcedureName_Update = this.spNoticePrintSummonsHWOUpdate;
            //Jaocb 20140109
            //********
            //paraList is for both print and update sp, be careful when adding new paramenters here.
            //********
            List<SqlParameter> paraList = new List<SqlParameter>();
            paraList.Add(new SqlParameter("@PrintFileName", this.PrintFileName));
            paraList.Add(new SqlParameter("@Mode", this.mode));
            paraList.Add(new SqlParameter("@LastUser", this.SubProcess.LastUser));
            paraList.Add(new SqlParameter("@IsCourtNumber", isCourtNumber));
            paraList.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = strSetGeneratedDate }); //2013-06-17 added by Nancy for 4973
          
            // 2013-08-27 added by Henry for filter summons printing
            paraList.Add(new SqlParameter("@AR_5110", GetAuthorityRule("5110").ARString.Trim().ToUpper()));
            paraList.Add(new SqlParameter("@AR_5120", GetAuthorityRule("5120").ARString.Trim().ToUpper()));

            //2013-12-5 add by Teresa for work package 5132
            if (this.StoredProcedureName_Update == this.spNoticePrintSummonsControlUpdate)
            {
                paraList.Add(new SqlParameter("@AR_5130", GetAuthorityRule("5130").ARString.Trim().ToUpper()));
            }

            object objValue = ExecuteScalar(this.StoredProcedureName_Update, paraList, false);
            int iValue = -1;
            if (objValue == null || !int.TryParse(objValue.ToString(), out iValue) || iValue < 0)
            {
                CloseConnection();
                ErrorProcessing();
                return;
            }

            //Jacob 20140109  fixed a bug for too many param
            if (this.StoredProcedureName_Update == this.spNoticePrintSummonsControlUpdate)
            {
                paraList.RemoveAt(7);
            }
           

            //2013-06-27 Heidi fixed a bug for too many param
            paraList.RemoveAt(4);

            dsPrintSummons ds = ExecuteDataSet<dsPrintSummons>(this.StoredProcedureName_Select, paraList);
            if (ds == null || !this.SubProcess.IsSuccessful || (ds.Tables[1].Rows.Count == 0 && this.IsWebMode))
            {
                ds.Dispose();
                ErrorProcessing(GetResource("SummonsViewer.aspx", "tipText"));
                return;
            }
            #endregion

            // Bind the report to the data
            report.SetDataSource(ds.Tables[1]);
            ds.Dispose();

            // Perform the export
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat);
            //this.OutputBuffer = ms.ToArray(); //push data to byte array.
            CreateTempFile(ms);
            report.Dispose();

        }
    }
}
