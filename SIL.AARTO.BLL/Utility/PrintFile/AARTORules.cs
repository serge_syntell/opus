﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Model;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class AARTORules
    {
        static AARTORules aartoRules;
        private AARTORules() { }
        public static AARTORules GetSingleton()
        {
            if (aartoRules == null)
                aartoRules = new AARTORules();
            return aartoRules;
        }

        string connStr;
        int autIntNo;
        Dictionary<int, List<AuthorityRulesDetails>> authRules = new Dictionary<int, List<AuthorityRulesDetails>>();
        Dictionary<int, List<DateRulesDetails>> dateRules = new Dictionary<int, List<DateRulesDetails>>();

        public void Initialize(string connStr, int autIntNo)
        {
            this.connStr = connStr;
            this.autIntNo = autIntNo;
        }

        public AuthorityRulesDetails GetAuthRule(string arCode)
        {
            return GetAuthRules(arCode);
        }

        public DateRulesDetails GetDateRule(string dtStart, string dtEnd)
        {
            return GetDateRules(dtStart,dtEnd);
        }

        private AuthorityRulesDetails GetAuthRules(string arCode)
        {
            AuthorityRuleInfo autInfo = new AuthorityRuleInfo().GetAuthorityRulesInfoByWFRFANameAutoIntNo(this.autIntNo,arCode);
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.ARCode = autInfo.WFRFAName;
            rule.ARComment = autInfo.WFRFAComment;
            rule.ARDescr = autInfo.WFRFADescr;
            rule.ARNumeric = autInfo.ARNumeric;
            rule.ARString = autInfo.ARString;
            return rule;
        }

        private DateRulesDetails GetDateRules(string dtStart, string dtEnd)
        {
            DateRuleInfo dateInfo = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(this.autIntNo,dtStart,dtEnd);
            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = dateInfo.AutIntNo;
            rule.DtRNoOfDays = dateInfo.ADRNoOfDays;
            rule.DtRDescr = dateInfo.DtRDescr;
            return rule;
        }

        private DataSet ExecuteDataSet(string spName, List<SqlParameter> paraList)
        {
            DataSet ds = null;
            using (SqlConnection conn = new SqlConnection(this.connStr))
            {
                using (SqlCommand cmd = new SqlCommand(spName, conn) { CommandType = CommandType.StoredProcedure })
                {
                    if (paraList != null && paraList.Count > 0)
                        cmd.Parameters.AddRange(paraList.ToArray());

                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    using (SqlDataAdapter dr = new SqlDataAdapter(cmd))
                    {
                        ds = new DataSet();
                        dr.Fill(ds);
                    }

                    conn.Close();
                    cmd.Parameters.Clear();
                }
            }
            return ds;
        }

        private T GetDataRowValue<T>(DataRow row, string columnName, object alternateValue = null)
        {
            if (row[columnName] == null || row[columnName] == DBNull.Value)
            {
                if (alternateValue == null)
                    return default(T);
                else
                    return (T)Convert.ChangeType(alternateValue, typeof(T));
            }
            else
                return (T)Convert.ChangeType(row[columnName], typeof(T));
        }
    }
}
