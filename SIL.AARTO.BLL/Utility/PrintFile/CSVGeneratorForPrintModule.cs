﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SADE = SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using ceTe.DynamicPDF.ReportWriter;
using System.IO;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using Stalberg.TMS;
using System.Text.RegularExpressions;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public delegate void LogMessageHandler(string msg);


    public abstract class CSVGeneratorForPrintModule
    {
        private SapoReportTypeList enumSapoReportType;

        private DocumentLayout docLayout = null;
        public event LogMessageHandler ProcessMessage;
        protected SADE.TList<SapoEquivalence> SapoEquivalelces;
        private SapoPrintingControl sapoPrintingControl;
        private bool? isPrintBySAPO;

        protected DataTable CsvTable;
        private string printFileName;
        protected string ImagePath;
        protected Dictionary<string, string> DicLabels = new Dictionary<string, string>();
        private SapoEquivalenceService equivalenceService = new SapoEquivalenceService();
        private SapoPrintingControlService printingControlService = new SapoPrintingControlService();

        public CSVGeneratorForPrintModule(SapoReportTypeList enumSapoReportType, string printFileName)
        {
            //this.docLayout = docLayout;
            this.enumSapoReportType = enumSapoReportType;
            this.printFileName = printFileName;
        }

        public void AppendDataRow(DataRow rowInDB = null)
        {
            if (docLayout == null)
                return;

            if (this.CsvTable == null)
                this.CsvTable = DefineTable();

            MappingAndCreateRow(rowInDB);


        }

        public bool BuildCsvFile(string filePath)
        {

            if (this.CsvTable == null || this.CsvTable.Rows.Count == 0)
                return false;
            StringBuilder sb = new StringBuilder();
            this.ImagePath = filePath;
            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                else
                {
                    DirectoryInfo d = new DirectoryInfo(Path.GetDirectoryName(filePath));
                    if (!d.Exists)
                    {
                        d.Create();
                    }
                }
            }
            catch (IOException ex)
            {
                ProcessMessage(ex.ToString());
                //throw new IOException();
            }


            FileStream fs = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write);

            StreamWriter sw = new StreamWriter(fs, Encoding.Default);
            try
            {
                int index = 0;
                string formatedValue = string.Empty;
                while (index < this.CsvTable.Columns.Count - 1)
                {
                    sb.Append(this.CsvTable.Columns[index++].ColumnName).Append("%");
                }
                sb.Append(this.CsvTable.Columns[index].ColumnName);

                sw.WriteLine(sb.ToString());

                sb.Clear();

                index = 0;

                foreach (DataRow dr in this.CsvTable.Rows)
                {
                    ProcessImages(dr);

                    while (index < this.CsvTable.Columns.Count - 1)
                    {
                        sb.Append(dr[index++].ToString()).Append("%");
                    }
                    sb.Append(dr[index].ToString());
                    index = 0;

                    formatedValue = Regex.Replace(sb.ToString(), @"[\n\r]", "");
                    
                    sb.Clear();
   
                    sw.WriteLine(formatedValue);

                }
                if (this.CsvTable.Rows.Count > 0)
                {
                    sw.WriteLine("Notice Count:" + this.CsvTable.Rows.Count);
                }
            }
            catch (Exception ex)
            {
                ProcessMessage(ex.ToString());
            }
            finally
            {
                sw.Close();
                fs.Close();
                sw.Dispose();
                fs.Dispose();

            }
            return true;
        }

        public bool PrintSAPO()
        {
            string option = string.Empty;
            string template = string.Empty;
            if (this.printFileName.IndexOf("BUS") >= 0)
            {
                option = "BUS";
            }
            else if (this.printFileName.IndexOf("RNO") >= 0)
            {
                option = "RNO";
            }
            else if (this.printFileName.IndexOf("REG") >= 0)
            {
                option = "REG";
            }
            else if (this.printFileName.IndexOf("RLV") >= 0)
            {
                option = "RLV";
            }
            else if (this.printFileName.IndexOf("VID") >= 0)
            {
                option = "VID";
            }
            else if (this.printFileName.IndexOf("SPD") >= 0)
            {
                option = "SPD";
            }


            if (this.printFileName.IndexOf("ASDNAG") >= 0)
            {
                template = "ASDNAG";
            }
            else if (this.printFileName.IndexOf("ASDS35NAG") >= 0)
            {
                template = "ASDS35NAG";
            }
            else if (this.printFileName.IndexOf("NAG") >= 0)
            {
                template = "NAG";
            }
            else
            {
                template = "Standard";
            }
            sapoPrintingControl = printingControlService.GetBySprtIntNoSppcOptionSppcTemplate((int)enumSapoReportType, option, template);

            if (sapoPrintingControl == null)
            {
                //ProcessMessage("No data found in SapoPrintControl table, option: " + option + " template: " + template + "Print file name: " + this.PrintFileName);
                return false;
            }

            return sapoPrintingControl.SppcPrintMethod;
        }

        public void SetDocumentLayout(DocumentLayout doc)
        {
            this.docLayout = doc;
        }

        public bool IsSAPOPrint
        {
            get
            {
                if (!isPrintBySAPO.HasValue)
                {
                    isPrintBySAPO = PrintSAPO();
                }
                return isPrintBySAPO.Value;
            }
        }

        protected virtual void CreateMappingDictionary() { }

        protected virtual void ProcessImages(DataRow dr) { }

        private void MappingAndCreateRow(DataRow rowInDB)
        {
            if (SapoEquivalelces == null)
                SapoEquivalelces = equivalenceService.GetBySprtIntNo((int)enumSapoReportType);

            DataRow dr = this.CsvTable.NewRow();

            foreach (SapoEquivalence equ in SapoEquivalelces)
            {
                if (String.IsNullOrEmpty(equ.SpEqSapoLabel) || String.IsNullOrEmpty(equ.SpEqSapoLabel.Trim())) continue;

                if (DicLabels.ContainsKey(equ.SpEqSapoLabel)
                    && rowInDB.Table.Columns.Contains(DicLabels[equ.SpEqSapoLabel]))
                {
                    dr[equ.SpEqSapoLabel] = rowInDB[DicLabels[equ.SpEqSapoLabel]];
                }
                else
                {
                    dr[equ.SpEqSapoLabel] = GetValueFromDocument(equ);
                }
            }

            this.CsvTable.Rows.Add(dr);

        }

        private DataTable DefineTable()
        {
            if (SapoEquivalelces == null)
                SapoEquivalelces = equivalenceService.GetBySprtIntNo((int)enumSapoReportType);
            DataTable csvTable = new DataTable();

            foreach (SapoEquivalence equ in SapoEquivalelces)
                csvTable.Columns.Add(new DataColumn(equ.SpEqSapoLabel, typeof(String)));

            CreateMappingDictionary();

            return csvTable;
        }


        private string GetValueFromDocument(SapoEquivalence equ)
        {
            string value = "";
            Label lbl = null;
            if (String.IsNullOrEmpty(equ.SpEqSapoLabel))
                return value;

            if (this.docLayout == null)
            {
                if (ProcessMessage != null)
                {
                    ProcessMessage("DocumentLayout is null, please initialize DocumentLayout before use it");
                }
            }

            object objLbl = docLayout.GetElementById(equ.SpEqOpusLabel);
            if (objLbl != null)
            {
                if (objLbl is ceTe.DynamicPDF.ReportWriter.ReportElements.Label)
                {
                    lbl = (Label)objLbl;
                }
            }
            //lbl = (Label)docLayout.GetElementById(equ.SpEqOpusLabel);

            if (lbl != null)
            {
                value = lbl.Text;

                if (value.Length > equ.SpEqSapoDataLength)
                    value = value.Substring(0, equ.SpEqSapoDataLength);
            }

            return value;
        }

    }

    public class CSVPrintFistNotice : CSVGeneratorForPrintModule
    {
        private readonly string ConnectString;
        private ScanImageDB scanImageDB;
        public event LogMessageHandler LogMessage;
        public CSVPrintFistNotice(SapoReportTypeList enumSapoReportType, string printFileName, string connectString)
            : base(enumSapoReportType, printFileName)
        {
            this.ConnectString = connectString;
            scanImageDB = new ScanImageDB(this.ConnectString);
        }

        protected override void CreateMappingDictionary()
        {
            this.DicLabels.Add("POAddress1", "DrvPOAdd1");
            this.DicLabels.Add("POAddress2", "DrvPOAdd2");
            this.DicLabels.Add("POAddress3", "DrvPOAdd3");
            this.DicLabels.Add("POAddress4", "DrvPOAdd4");
            this.DicLabels.Add("POAddrCode", "DrvPOCode");

            this.DicLabels.Add("PaypointLine1", "MtrPayPoint1Add1");
            this.DicLabels.Add("PaypointLine2", "MtrPayPoint1Add2");
            this.DicLabels.Add("PaypointLine3", "MtrPayPoint1Add3");
            this.DicLabels.Add("PaypointLine4", "MtrPayPoint1Add4");
            this.DicLabels.Add("PaypointCode", "MtrPayPoint1Code");

            this.DicLabels.Add("MainImageName", "ScanImage1");
            this.DicLabels.Add("RegNoImage Name", "ScanImage2");
        }

        protected override void ProcessImages(DataRow dr)
        {
            string destFileName = string.Empty;
            ScanImageDetails image1 = null;
            if (dr["MainImageName"] != null && dr["MainImageName"] != DBNull.Value)
            {

                image1 = scanImageDB.GetImageFullPath(Convert.ToInt32(dr["MainImageName"]));
                if (image1 != null)
                {
                    dr["MainImageName"] = string.Format(@"{0}_{1}", image1.FrameIntNo, image1.JPegName);

                    destFileName = Path.Combine(Path.GetDirectoryName(this.ImagePath), dr["MainImageName"].ToString());

                    CopyScanImage(image1.ImageFullPath, Path.Combine(destFileName));
                }

            }
            if (dr["RegNoImage Name"] != null && dr["RegNoImage Name"] != DBNull.Value)
            {
                image1 = scanImageDB.GetImageFullPath(Convert.ToInt32(dr["RegNoImage Name"]));
                if (image1 != null)
                {
                    dr["RegNoImage Name"] = string.Format(@"{0}_{1}", image1.FrameIntNo, image1.JPegName);

                    destFileName = Path.Combine(Path.GetDirectoryName(this.ImagePath), dr["RegNoImage Name"].ToString());

                    CopyScanImage(image1.ImageFullPath, Path.Combine(destFileName));
                }
            }
        }

        private void CopyScanImage(string sourceFileName, string destFileName)
        {
            if (!File.Exists(sourceFileName))
            {
                if (LogMessage != null)
                    LogMessage("Source image does not exists, Source image path: " + sourceFileName);
                //throw new Exception("Source image does not exists, Source image path: " + sourceFileName);
            }
            try
            {
                string path = Path.GetDirectoryName(destFileName);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                File.Copy(sourceFileName, destFileName, true);
            }
            catch (IOException e)
            {
                if (LogMessage != null)
                    LogMessage(e.ToString());
            }
        }
    }
}
