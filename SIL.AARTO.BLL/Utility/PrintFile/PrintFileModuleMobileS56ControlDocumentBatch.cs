﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using ceTe.DynamicPDF.Merger;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using Stalberg.TMS;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleMobileS56ControlDocumentBatch : PrintFileBase
    {
        protected const string ThereIsNoDataForReport = "There is no data for report. \"{0}\"";
        protected const string FileDoesNotExistOrIsInvalid = "File does not exist or is invalid. \"{0}\"";
        readonly AartoDocumentImageService docImgService = new AartoDocumentImageService();
        readonly PrintFileNameService pfnService = new PrintFileNameService();
        readonly SummonsService summonsService = new SummonsService();
        bool isOfficerError;
        ServiceDB serviceDB;

        public PrintFileModuleMobileS56ControlDocumentBatch(bool isOfficerError = false)
        {
            this.isOfficerError = isOfficerError;
        }

        public Action onLoad { get; set; }

        internal override void OnLoad()
        {
            if (onLoad != null)
                onLoad();
        }

        public override void InitialWork()
        {
            if (this.serviceDB == null) this.serviceDB = new ServiceDB(SubProcess.ConnectionString);

            if (string.IsNullOrWhiteSpace(SubProcess.ExportPrintFilePath)
                || !ServiceUtility.CheckFolder(new FileInfo(SubProcess.ExportPrintFilePath).DirectoryName))
            {
                LogProcessing(string.Format(GetResource(null, "FileDoesNotExistOrIsInvalid", FileDoesNotExistOrIsInvalid), SubProcess.ExportPrintFilePath), LogType.Error, false);
            }
        }

        public override void MainWork()
        {
            var subProcess = new PrintFileProcess(SubProcess.ConnectionString, SubProcess.LastUser)
            {
                LogProcessing = SubProcess.LogProcessing
            };

            // Batch Transaction
            var mainTran = new ManualTransaction<MobileS56History>(h =>
            {
                Summons summons;
                if (h.Data != null
                    && h.Data.SumIntNo > 0
                    && (summons = this.summonsService.GetBySumIntNo(h.Data.SumIntNo)) != null
                    && summons.MobileControlDocumentGeneratedDate.HasValue)
                {
                    summons.MobileControlDocumentGeneratedDate = null;
                    this.summonsService.Save(summons);
                }

                AartoDocumentImage docImg;
                if (h.TransactionalFiles.Count > 0
                    && !h.TransactionalFiles[0].OriginalPath.Equals2(h.TransactionalFiles[0].Path)
                    && h.Data != null
                    && !string.IsNullOrWhiteSpace(h.Data.ConDocPath)
                    && h.Data.ConDocId > 0
                    && (docImg = this.docImgService.GetByAaDocImgId(h.Data.ConDocId)) != null
                    && h.Data.ConDocPath.Equals2(docImg.AaDocImgPath))
                {
                    docImg.AaDocImgPath = h.Data.ConDocPath;
                    this.docImgService.Save(docImg);
                }
            }, ex => LogProcessing(ex.Message, LogType.Error, false));

            // Batch Transaction Start
            var result = mainTran.BeginTransaction(() =>
            {
                var mainHistory = new MobileS56History();
                mainHistory.TransactionalFiles.Add(new TransactionalFile(SubProcess.ExportPrintFilePath));
                mainTran.AddHistory(mainHistory);

                var sources = GetMobileS56ControlDocumentBatchSource();
                if (sources.IsNullOrEmpty())
                {
                    LogProcessing(string.Format(GetResource(null, "ThereIsNoDataForReport", ThereIsNoDataForReport), SubProcess.ExportPrintFilePath), LogType.Error, false);
                    return false;
                }

                var merge = new MergeDocument();
                foreach (var s in sources)
                {
                    // Inner Transaction
                    var subTran = new ManualTransaction<MobileS56History>(mainTran.OnRollingBackDatabaseData, mainTran.OnException, mainTran);

                    var d = s;

                    // Inner Transaction Start
                    var subResult = subTran.BeginTransaction(() =>
                    {
                        var subHistory = new MobileS56History
                        {
                            Data = d
                        };
                        subHistory.TransactionalFiles.Add(new TransactionalFile(d.ConDocFullPath));
                        subTran.AddHistory(subHistory);

                        using (var subModule = new PrintFileModuleMobileS56ControlDocument
                        {
                            PageData = d,
                            onLoad = () => { subProcess.ExportPrintFilePath = d.ConDocFullPath; },
                            OnSavingFile = p => { subHistory.TransactionalFiles[0].Path = p; },
                        })
                        {
                            subProcess.BuildPrintFile(subModule, AutIntNo, null, "", "", true);

                            if (subProcess.SkipNextProcesses
                                || (!subProcess.IsSuccessful || subModule.OutputDocument == null) && !subModule.BuildFailedReport()
                                || subModule.OutputDocument == null)
                                return false;

                            merge.Append(new PdfDocument(subModule.OutputDocument.Draw()));
                            return true;
                        }
                    });
                    GC.Collect();

                    if (!subResult)
                        return false;
                }

                SubProcess.ExportPrintFilePath = GetExportedFilePath(SubProcess.ExportPrintFilePath);
                mainHistory.TransactionalFiles[0].Path = SubProcess.ExportPrintFilePath;
                merge.Draw(SubProcess.ExportPrintFilePath);

                // if path has been changed, update old one.
                if (!mainHistory.TransactionalFiles[0].OriginalPath.Equals2(mainHistory.TransactionalFiles[0].Path))
                {
                    PrintFileName pfn;
                    if (PFNIntNo <= 0
                        || (pfn = this.pfnService.GetByPfnIntNo(PFNIntNo)) == null)
                        return false;

                    //var prefix = mainHistory.TransactionalFiles[0].OriginalPath.Replace(pfn.MobileControlDocumentPath, string.Empty);
                    //var suffix = SubProcess.ExportPrintFilePath.Replace(prefix, string.Empty);
                    pfn.MobileControlDocumentPath = GetRelativePath(mainHistory.TransactionalFiles[0].OriginalPath, pfn.MobileControlDocumentPath, SubProcess.ExportPrintFilePath);
                    this.pfnService.Save(pfn);
                }

                return true;
            });

            if (!result)
                SubProcess.IsSuccessful = false;
        }

        List<MobileS56ControlDocumentPageData> GetMobileS56ControlDocumentBatchSource()
        {
            var result = new List<MobileS56ControlDocumentPageData>();
            var paras = new[]
            {
                new SqlParameter("@PFNIntNo", PFNIntNo)
            };
            this.serviceDB.ExecuteReader("GetMobileS56ControlDocumentBatchSource", paras, dr =>
            {
                while (dr.Read())
                {
                    var sumIntNo = Helper.GetReaderValue<int>(dr, "SumIntNo");
                    var data = result.FirstOrDefault(s => s.SumIntNo == sumIntNo);
                    if (data == null)
                    {
                        data = new MobileS56ControlDocumentPageData
                        {
                            SumIntNo = sumIntNo,
                            SummonsNo = Helper.GetReaderValue<string>(dr, "SummonsNo").Trim2(),

                            #region Page1

                            AccSurname = Helper.GetReaderValue<string>(dr, "AccSurname").Trim2(),
                            AccForenames = Helper.GetReaderValue<string>(dr, "AccForenames").Trim2(),
                            AccInitials = Helper.GetReaderValue<string>(dr, "AccInitials").Trim2(),
                            AccIDNumber = Helper.GetReaderValue<string>(dr, "AccIDNumber").Trim2(),
                            AccSex = Helper.GetReaderValue<string>(dr, "AccSex").Trim2(),
                            AccAge = Helper.GetReaderValue<string>(dr, "AccAge").Trim2(),
                            AccOccupation = Helper.GetReaderValue<string>(dr, "AccOccupation").Trim2(),
                            AccEmail = Helper.GetReaderValue<string>(dr, "AccEmail").Trim2(),
                            NotOffenceDate = Helper.GetReaderValue<DateTime>(dr, "NotOffenceDate"),
                            NotRegNo = Helper.GetReaderValue<string>(dr, "NotRegNo").Trim2(),
                            NotLocDescr = Helper.GetReaderValue<string>(dr, "NotLocDescr").Trim2(),
                            GPS1 = Helper.GetReaderValue<string>(dr, "GPS1").Trim2(),
                            GPS2 = Helper.GetReaderValue<string>(dr, "GPS2").Trim2(),
                            NotVehicleMake = Helper.GetReaderValue<string>(dr, "NotVehicleMake").Trim2(),
                            NotVehicleType = Helper.GetReaderValue<string>(dr, "NotVehicleType").Trim2(),
                            IsTaxi = Helper.GetReaderValue<string>(dr, "IsTaxi").Trim2(),
                            NotSpeedLimit = Helper.GetReaderValue<string>(dr, "NotSpeedLimit").Trim2(),
                            NotSpeed1 = Helper.GetReaderValue<string>(dr, "NotSpeed1").Trim2(),
                            SumCourtDate = Helper.GetReaderValue<DateTime?>(dr, "SumCourtDate"),
                            NotCourtName = Helper.GetReaderValue<string>(dr, "NotCourtName").Trim2(),
                            NotCourtNo = Helper.GetReaderValue<string>(dr, "NotCourtNo").Trim2(),
                            NotPaymentDate = Helper.GetReaderValue<DateTime?>(dr, "NotPaymentDate"),
                            NotOfficerInit = Helper.GetReaderValue<string>(dr, "NotOfficerInit").Trim2(),
                            NotOfficerSName = Helper.GetReaderValue<string>(dr, "NotOfficerSName").Trim2(),
                            LoSuDescr = Helper.GetReaderValue<string>(dr, "LoSuDescr").Trim2(),
                            AccLicenceNumber = Helper.GetReaderValue<string>(dr, "AccLicenceNumber").Trim2(),
                            NotOfficerNo = Helper.GetReaderValue<string>(dr, "NotOfficerNo").Trim2(),

                            #region Address

                            AccPoAdd1 = Helper.GetReaderValue<string>(dr, "AccPoAdd1").Trim2(),
                            AccPoAdd2 = Helper.GetReaderValue<string>(dr, "AccPoAdd2").Trim2(),
                            AccPoAdd3 = Helper.GetReaderValue<string>(dr, "AccPoAdd3").Trim2(),
                            AccPoAdd4 = Helper.GetReaderValue<string>(dr, "AccPoAdd4").Trim2(),
                            AccPoAdd5 = Helper.GetReaderValue<string>(dr, "AccPoAdd5").Trim2(),
                            AccTelHome = Helper.GetReaderValue<string>(dr, "AccTelHome").Trim2(),
                            AccPoCode = Helper.GetReaderValue<string>(dr, "AccPoCode").Trim2(),

                            #endregion

                            #region Business

                            AccEmployerAdd1 = Helper.GetReaderValue<string>(dr, "AccEmployerAdd1").Trim2(),
                            AccEmployerAdd2 = Helper.GetReaderValue<string>(dr, "AccEmployerAdd2").Trim2(),
                            AccEmployerAdd3 = Helper.GetReaderValue<string>(dr, "AccEmployerAdd3").Trim2(),
                            AccEmployerAdd4 = Helper.GetReaderValue<string>(dr, "AccEmployerAdd4").Trim2(),
                            AccTelWork = Helper.GetReaderValue<string>(dr, "AccTelWork").Trim2(),
                            AccEmployerPoCode = Helper.GetReaderValue<string>(dr, "AccEmployerPoCode").Trim2(),

                            #endregion

                            #region Images

                            ImageMachineName = Helper.GetReaderValue<string>(dr, "ImageMachineName").Trim2(),
                            ImageShareName = Helper.GetReaderValue<string>(dr, "ImageShareName").Trim2(),
                            OffSigPath = Helper.GetReaderValue<string>(dr, "OffSigPath").Trim2(),
                            TOSigPath = Helper.GetReaderValue<string>(dr, "TOSigPath").Trim2(),
                            ConDocPath = Helper.GetReaderValue<string>(dr, "ConDocPath").Trim2(),

                            #endregion

                            #endregion
                        };

                        string[] tmp;
                        if (!string.IsNullOrWhiteSpace(data.ConDocPath)
                            && (tmp = data.ConDocPath.Split('|')).Length == 2)
                        {
                            data.ConDocId = Convert.ToInt32(tmp[0], CultureInfo.InvariantCulture);
                            data.ConDocPath = tmp[1];
                        }

                        result.Add(data);
                    }

                    var charge = new MobileS56Charge
                    {
                        ChgIntNo = Helper.GetReaderValue<int>(dr, "ChgIntNo"),
                        ChgIsMain = Helper.GetReaderValue<bool>(dr, "ChgIsMain"),
                        MainChargeID = Helper.GetReaderValue<int?>(dr, "MainChargeID"),
                        ChgSequence = Helper.GetReaderValue<int>(dr, "ChgSequence"),
                        ChgStatutoryRef = Helper.GetReaderValue<string>(dr, "ChgStatutoryRef").Trim2(),
                        ChgOffenceCode = Helper.GetReaderValue<string>(dr, "ChgOffenceCode").Trim2(),
                        ChgOffenceDescr = Helper.GetReaderValue<string>(dr, "ChgOffenceDescr").Trim2(),
                        ChgFineAmount = Helper.GetReaderValue<string>(dr, "ChgFineAmount").Trim2(),
                    };
                    data.Charges.Add(charge);
                }
            }, 600);
            return result;
        }

        public class MobileS56History : History
        {
            public MobileS56ControlDocumentPageData Data { get; set; }
        }
    }
}
