﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF;
using Stalberg.TMS;


namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleSecondAppearanceCCR : PrintFileBase
    {
        public PrintFileModuleSecondAppearanceCCR(CourtRollType crType, string reportType, string crtIntNo, string crtRIntNo, string courtDate, string finalPrintDate, string courtRollType)
        {
            this.crType = crType;
            switch (crType)
            {
                case CourtRollType.Summary:
                    this.ErrorPage = GetResource("CourtRollViewer.aspx", "thisPage");
                    this.ErrorPageUrl = "CourtRollViewer.aspx";
                    break;

                case CourtRollType.Label:
                    this.ErrorPage = GetResource("CourtRollLabelsViewer.aspx", "thisPage");
                    this.ErrorPageUrl = "CourtRollLabelsViewer.aspx";
                    break;

                case CourtRollType.ChargeSheet:
                    this.ErrorPage = GetResource("CourtRollChargeSheetViewer.aspx", "thisPage");
                    //this.ErrorPage = "Court Roll Charge Sheet Viewer";
                    this.ErrorPageUrl = "CourtRollChargeSheetViewer.aspx";
                    break;
            }

            try
            {
                this.reportType = string.IsNullOrWhiteSpace(reportType) ? "P" : reportType.Trim();
                this.crtIntNo = string.IsNullOrWhiteSpace(crtIntNo) ? 0 : Convert.ToInt32(crtIntNo);
                this.crtRIntNo = string.IsNullOrWhiteSpace(crtRIntNo) ? 0 : Convert.ToInt32(crtRIntNo);
                if (!DateTime.TryParse(courtDate, out this.courtDate))
                {
                    ErrorProcessing(GetResource("CourtRollChargeSheetViewer.aspx", "error2"));
                    return;
                }
                if (crType != CourtRollType.Label)
                {
                    if (!DateTime.TryParse(finalPrintDate, out this.finalPrintDate))
                    {
                        ErrorProcessing(GetResource("CourtRollChargeSheetViewer.aspx", "error3"));
                        return;
                    }
                }
                this.courtRollType = Convert.ToInt32(courtRollType);
            }
            catch (Exception ex)
            {
                ErrorProcessing(ex.Message);
                return;
            }
            finally
            {
                GC.Collect();
            }
        }

        string reportType;
        int crtIntNo, crtRIntNo, courtRollType;
        DateTime courtDate, finalPrintDate;
        RecordBox rb;
        CourtRollType crType;
        int noOfDays;

        public override void InitialWork()
        {
            string function = string.Empty;
            if (this.crType == CourtRollType.Summary)
                function = "2ndCourtRoll";
            else if (this.crType == CourtRollType.Label)
            {
                function = "2ndCourtRollLabels";
                this.noOfDays = GetDateRule("CDate", "FinalCourtRoll").DtRNoOfDays;
            }
            else if (this.crType == CourtRollType.ChargeSheet)
                function = "2ndCourtRollChargeSheet";

            this.ReportFile = this.ReportDB.GetAuthReportName(this.AutIntNo, function);
            if (string.IsNullOrWhiteSpace(this.ReportFile))
            {
                this.ReportFile = function + ".dplx";
                this.ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, function, "system", string.Empty);
            }
            string path;
            if (!CheckReportTemplateExists(function, null, out path)) return;
        }

        public override void MainWork()
        {
            DocumentLayout doc = new DocumentLayout(this.ReportFilePath);

            if (this.crType == CourtRollType.Summary)
            {
                this.rb = (RecordBox)doc.GetElementById("rbCaseNo");
                Label lbl = (Label)doc.GetElementById("lblCaseNo");
                Label lblLAName = (Label)doc.GetElementById("lblLAName");
                this.rb.LayingOut += new LayingOutEventHandler(rb_LayingOut);

                // We run 4 types of reports and then merge them into one using the ceTe Merge functionality

                // run the query for S54 and Personal service type reports
                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = this.SubProcess.ConnectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("AutIntNo", this.AutIntNo);
                parameters.Add("CrtIntNo", this.crtIntNo);
                parameters.Add("CrtRIntNo", this.crtRIntNo);
                parameters.Add("Type", "S54");
                parameters.Add("SumServedStatus", 1);
                parameters.Add("SumCourtDate", this.courtDate);
                parameters.Add("FinalPrintDate", this.finalPrintDate);
                parameters.Add("FinalFlag", this.reportType);
                parameters.Add("CourtRollType", this.courtRollType);
                Document reportA = doc.Run(parameters);

                // run the query for S54 and Impersonal service type reports
                query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = this.SubProcess.ConnectionString;
                parameters = new ParameterDictionary();
                parameters.Add("AutIntNo", this.AutIntNo);
                parameters.Add("CrtIntNo", this.crtIntNo);
                parameters.Add("CrtRIntNo", this.crtRIntNo);
                parameters.Add("Type", "S54");
                parameters.Add("SumServedStatus", 2);
                parameters.Add("SumCourtDate", this.courtDate);
                parameters.Add("FinalPrintDate", this.finalPrintDate);
                parameters.Add("FinalFlag", this.reportType);
                parameters.Add("CourtRollType", this.courtRollType);
                Document reportB = doc.Run(parameters);

                // run the query for S56 and Personal service type reports
                query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = this.SubProcess.ConnectionString;
                parameters = new ParameterDictionary();
                parameters.Add("AutIntNo", this.AutIntNo);
                parameters.Add("CrtIntNo", this.crtIntNo);
                parameters.Add("CrtRIntNo", this.crtRIntNo);
                parameters.Add("Type", "S56");
                parameters.Add("SumServedStatus", 1);
                parameters.Add("SumCourtDate", this.courtDate);
                parameters.Add("FinalPrintDate", this.finalPrintDate);
                parameters.Add("FinalFlag", this.reportType);
                parameters.Add("CourtRollType", this.courtRollType);
                Document reportC = doc.Run(parameters);

                byte[] bufferA = reportA.Draw();
                byte[] bufferB = reportB.Draw();
                byte[] bufferC = reportC.Draw();

                // 2013-09-18 add by Henry for fix no data hard code 1488
                query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = this.SubProcess.ConnectionString;
                parameters = new ParameterDictionary();
                parameters.Add("AutIntNo", 0);
                parameters.Add("CrtIntNo", 0);
                parameters.Add("CrtRIntNo", 0);
                parameters.Add("Type", "");
                parameters.Add("SumServedStatus", 0);
                parameters.Add("SumCourtDate", this.courtDate);
                parameters.Add("FinalPrintDate", this.finalPrintDate);
                parameters.Add("FinalFlag", this.reportType);
                parameters.Add("CourtRollType", this.courtRollType);
                Document emptyPdf = doc.Run(parameters);
                int emptyPdfLength = emptyPdf.Draw().Length;

                if (bufferA.Length > emptyPdfLength)
                {
                    if (bufferB.Length > emptyPdfLength)
                        foreach (Page p in reportB.Pages)
                            reportA.Pages.Add(p);
                    //reportA.Pages.Append(reportB.Pages);
                    if (bufferC.Length > emptyPdfLength)
                        foreach (Page p in reportC.Pages)
                            reportA.Pages.Add(p);
                    //reportA.Pages.Append(reportC.Pages);

                    //this.OutputBuffer = reportA.Draw();
                    CreateTempFile(reportA);
                }
                else if (bufferB.Length > emptyPdfLength)
                {
                    if (bufferC.Length > emptyPdfLength)
                        //reportB.Pages.Append(reportC.Pages);
                        foreach (Page p in reportC.Pages) reportB.Pages.Add(p);

                    //this.OutputBuffer = reportB.Draw();
                    CreateTempFile(reportB);
                }
                else
                {
                    //this.OutputBuffer = reportC.Draw();
                    CreateTempFile(reportC);
                }

                this.rb.LayingOut -= new LayingOutEventHandler(rb_LayingOut);
            }
            else if (this.crType == CourtRollType.Label)
            {
                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = this.SubProcess.ConnectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("AutIntNo", this.AutIntNo);
                if (this.crtIntNo > 0)
                    parameters.Add("CrtIntNo", this.crtIntNo);
                if (this.crtRIntNo > 0)
                    parameters.Add("CrtRIntNo", this.crtRIntNo);
                parameters.Add("SumCourtDate", this.courtDate);
                parameters.Add("FinalPrintDate", this.courtDate.AddDays(this.noOfDays));
                //jerry 2011-11-07 add
                parameters.Add("CourtRollType", this.courtRollType);

                Document report = doc.Run(parameters);
                //this.OutputBuffer = report.Draw();
                CreateTempFile(report);
            }
            else if (this.crType == CourtRollType.ChargeSheet)
            {
                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = this.SubProcess.ConnectionString;

                if (this.reportType.Equals("F"))
                {
                    // print charge sheet with S56  court roll
                    AuthorityRulesDetails arChargeSheetWithS56 = GetAuthorityRule("6206");
                    // print charge sheet with S54  court roll
                    AuthorityRulesDetails arChargeSheetWithS54 = GetAuthorityRule("6207");

                    if (arChargeSheetWithS56.ARString == "N" && arChargeSheetWithS54.ARString == "N")
                    {
                        ErrorProcessing();
                        return;
                    }
                    else
                    {
                        Document reportA = null;
                        byte[] bufferA = null;
                        if (arChargeSheetWithS56.ARString == "Y" || this.reportType == "P")
                        {
                            ParameterDictionary parameters = new ParameterDictionary();
                            parameters.Add("AutIntNo", this.AutIntNo);
                            parameters.Add("CrtIntNo", this.crtIntNo);
                            parameters.Add("CrtRIntNo", this.crtRIntNo);
                            parameters.Add("SumCourtDate", this.courtDate);
                            parameters.Add("FinalPrintDate", this.finalPrintDate);
                            parameters.Add("Type", "S56");
                            parameters.Add("FinalFlag", this.reportType);
                            //jerry 2011-11-07 add
                            parameters.Add("CourtRollType", this.courtRollType);

                            reportA = doc.Run(parameters);
                            bufferA = reportA.Draw();
                        }

                        Document reportB = null;
                        byte[] bufferB = null;
                        if (arChargeSheetWithS54.ARString == "Y" || this.reportType == "P")
                        {
                            ParameterDictionary parameters = new ParameterDictionary();
                            parameters.Add("AutIntNo", this.AutIntNo);
                            parameters.Add("CrtIntNo", this.crtIntNo);
                            parameters.Add("CrtRIntNo", this.crtRIntNo);
                            parameters.Add("SumCourtDate", this.courtDate);
                            parameters.Add("FinalPrintDate", this.finalPrintDate);
                            parameters.Add("Type", "S54");
                            parameters.Add("FinalFlag", this.reportType);
                            //jerry 2011-11-07 add
                            parameters.Add("CourtRollType", this.courtRollType);

                            reportB = doc.Run(parameters);
                            bufferB = reportB.Draw();
                        }

                        // 2013-09-18 add by Henry for fix no data hard code 1488
                        ParameterDictionary param0 = new ParameterDictionary();
                        param0.Add("AutIntNo", 0);
                        param0.Add("CrtIntNo", 0);
                        param0.Add("CrtRIntNo", 0);
                        param0.Add("SumCourtDate", this.courtDate);
                        param0.Add("FinalPrintDate", this.finalPrintDate);
                        param0.Add("Type", "");
                        param0.Add("FinalFlag", this.reportType);
                        param0.Add("CourtRollType", this.courtRollType);
                        Document emptyPdf = doc.Run(param0);
                        int emptyPdfLength = emptyPdf.Draw().Length;

                        if (bufferA != null && bufferA.Length > emptyPdfLength)
                        {
                            if (bufferB != null && bufferB.Length > emptyPdfLength)
                                //reportA.Pages.Append(reportB.Pages);
                                foreach (Page p in reportB.Pages) reportA.Pages.Add(p);

                            //this.OutputBuffer = reportA.Draw();
                            CreateTempFile(reportA);
                        }
                        else if (bufferB != null)
                        {
                           // this.OutputBuffer = reportB.Draw();
                            CreateTempFile(reportB);
                        }
                    }
                }
                else
                {
                    Document reportA = null;
                    byte[] bufferA = null;

                    ParameterDictionary parameters = new ParameterDictionary();
                    parameters.Add("AutIntNo", this.AutIntNo);
                    parameters.Add("CrtIntNo", this.crtIntNo);
                    parameters.Add("CrtRIntNo", this.crtRIntNo);
                    parameters.Add("SumCourtDate", this.courtDate);
                    parameters.Add("FinalPrintDate", this.finalPrintDate);
                    parameters.Add("Type", "S56");
                    parameters.Add("FinalFlag", this.reportType);
                    //jerry 2011-11-07 add
                    parameters.Add("CourtRollType", this.courtRollType);

                    reportA = doc.Run(parameters);
                    bufferA = reportA.Draw();

                    Document reportB = null;
                    byte[] bufferB = null;

                    parameters = new ParameterDictionary();
                    parameters.Add("AutIntNo", this.AutIntNo);
                    parameters.Add("CrtIntNo", this.crtIntNo);
                    parameters.Add("CrtRIntNo", this.crtRIntNo);
                    parameters.Add("SumCourtDate", this.courtDate);
                    parameters.Add("FinalPrintDate", this.finalPrintDate);
                    parameters.Add("Type", "S54");
                    parameters.Add("FinalFlag", this.reportType);
                    //jerry 2011-11-07 add
                    parameters.Add("CourtRollType", this.courtRollType);

                    reportB = doc.Run(parameters);
                    bufferB = reportB.Draw();

                    // 2013-09-18 add by Henry for fix no data hard code 1488
                    ParameterDictionary param0 = new ParameterDictionary();
                    param0.Add("AutIntNo", 0);
                    param0.Add("CrtIntNo", 0);
                    param0.Add("CrtRIntNo", 0);
                    param0.Add("SumCourtDate", this.courtDate);
                    param0.Add("FinalPrintDate", this.finalPrintDate);
                    param0.Add("Type", "");
                    param0.Add("FinalFlag", this.reportType);
                    param0.Add("CourtRollType", this.courtRollType);
                    Document emptyPdf = doc.Run(param0);
                    int emptyPdfLength = emptyPdf.Draw().Length;

                    if (bufferA != null && bufferA.Length > emptyPdfLength)
                    {
                        if (bufferB != null && bufferB.Length > emptyPdfLength)
                            //reportA.Pages.Append(reportB.Pages);
                            foreach (Page p in reportB.Pages) reportA.Pages.Add(p);

                        //this.OutputBuffer = reportA.Draw();
                        CreateTempFile(reportA);
                    }
                    else if (bufferB != null)
                    {
                        //this.OutputBuffer = reportB.Draw();
                        CreateTempFile(reportB);
                    }
                }
            }
        }

        private void rb_LayingOut(object sender, LayingOutEventArgs e)
        {
            if (this.reportType == "P")
                this.rb.TextColor = CmykColor.White;
        }
    }
}
