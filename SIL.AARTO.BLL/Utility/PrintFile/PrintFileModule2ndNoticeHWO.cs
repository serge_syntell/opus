﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Stalberg.TMS;
using System.Text.RegularExpressions;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using System.IO;
using ceTe.DynamicPDF.Imaging;
using System.Web;
using System.Data.SqlClient;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF;
using SIL.AARTO.BLL.BarCode;
using System.Globalization;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModule2ndNoticeHWO : PrintFileBase
    {
        public PrintFileModule2ndNoticeHWO()
        {
            this.ErrorPage = GetResource("SecondNotice_HWO_Viewer.aspx", "thisPage");
            //this.ErrorPage = "View HWO Second Notices";
            this.ErrorPageUrl = "SecondNotice_HWO_Viewer.aspx";
        }

        const int FIRST_NOTICE = 1;
        const int SECOND_NOTICE = 2;
        string format = "TMS";
        PlaceHolder phBarCode;
        System.Drawing.Image barCodeImage = null;

        public override void InitialWork()
        {
            if (string.IsNullOrWhiteSpace(this.PrintFileName))
            {
                ErrorProcessing(GetResource("SecondNotice_HWO_Viewer.aspx", "strWriteMsg"));
                return;
            }

            string printFileName = this.PrintFileName;
            string prefix = string.Empty;
            string longPrefix = string.Empty;
            string extendedPrefix = string.Empty;
            int noticeStage = SECOND_NOTICE;

            DataSet ds = null;
            NoticeDB notice = new NoticeDB(this.SubProcess.ConnectionString);

            if (this.PrintFileName != "-1")
            {
                prefix = this.PrintFileName.Substring(0, 3).ToUpper();
                longPrefix = this.PrintFileName.Substring(0, 7).ToUpper();
                extendedPrefix = this.PrintFileName.Substring(0, 11).ToUpper();

                // See if its a single notice
                string pattern = @"^\w{2,}/\d{2,}/\d{2,}/\d{3,}$";
                Regex regex = new Regex(pattern, RegexOptions.Singleline);
                if (regex.IsMatch(this.PrintFileName))
                {
                    if (!this.PrintFileName[0].Equals('*'))
                        this.PrintFileName = "*" + this.PrintFileName;

                    ds = notice.GetNoticeCheckRLVDS(this.PrintFileName.Substring(1, this.PrintFileName.Length - 1));
                }
            }

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    prefix = dr["PrintFileName"].ToString();
                    //add the check for the status of the notice here
                    noticeStage = Convert.ToInt16(dr["NoticeStage"]);
                }

                ds.Dispose();
            }

            AuthorityRulesDetails arDet1, arDet2;
            arDet1 = GetAuthorityRule("2550");
            arDet2 = GetAuthorityRule("4250");

            if (arDet1.ARString.Equals("N") || arDet2.ARString.Equals("N") || arDet2.ARNumeric == 0)
                noticeStage = SECOND_NOTICE;

            PrintFileProcess process = new PrintFileProcess(this.SubProcess.ConnectionString, this.SubProcess.LastUser);
            if (noticeStage == FIRST_NOTICE)
            {
                process.ErrorProcessing = this.SubProcess.ErrorProcessing;
                process.BuildPrintFile(
                    new PrintFileModuleFirstNotice(),
                    this.AutIntNo, printFileName, this.PrintFileOutputFolder);
                this.SubProcess.IsSuccessful = process.IsSuccessful;
                this.SubProcess.SkipNextProcesses = true;
                return;
            }

            //BD 20090128 - removing of auth rule to control what ticket processing being used, now using AutTicketProcessor
            //AuthorityDB authDB = new AuthorityDB(this.SubProcess.ConnectionString);
            //AuthorityDetails authDetails = authDB.GetAuthorityDetails(this.AutIntNo);

            //if (authDetails.AutTicketProcessor == "CiprusPI" || authDetails.AutTicketProcessor == "Cip_CofCT")
            //    format = authDetails.AutTicketProcessor;

            if (prefix.ToUpper() == "HWO")
            {
                this.ReportFile = ReportDB.GetAuthReportName(this.AutIntNo, "SecondNoticeHWO");

                if (this.ReportFile.Equals(string.Empty))
                {
                    this.ReportFile = "SecondNotice_MB_HWO.dplx";
                    ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, "SecondNoticeHWO", "System", "SecondNoticeTemplate_MB_HWO.pdf");
                }

                this.TemplateFile = ReportDB.GetAuthReportNameTemplate(this.AutIntNo, "SecondNoticeHWO");
            }

            // check report template file exists
            string path;
            if (!CheckReportTemplateExists(null, null, out path)) return;

        }

        public override void MainWork()
        {
            string strSetGeneratedDate = GetAuthorityRule("2570").ARString.Trim().ToUpper(); //2013-06-26 added by Nancy for 4973 

            string showAll = "N";
            int status = 260;
            int option = 2;

            DocumentLayout doc = new DocumentLayout(this.ReportFilePath);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.SubProcess.ConnectionString;
            ParameterDictionary parameters = new ParameterDictionary();

            Label lblId = (Label)doc.GetElementById("lblId");
            Label lblReference = (Label)doc.GetElementById("lblReference");
            Label lblReferenceB = (Label)doc.GetElementById("lblReferenceB");
            Label lblFormattedNotice = (Label)doc.GetElementById("lblFormattedNotice");
            Label lblForAttention = (Label)doc.GetElementById("lblForAttention");
            Label lblAddress = (Label)doc.GetElementById("lblAddress");
            Label lblNoticeNumber = (Label)doc.GetElementById("lblNoticeNumber");
            Label lblPaymentInfo = (Label)doc.GetElementById("lblPaymentInfo");
            Label lblStatRef = (Label)doc.GetElementById("lblStatRef");
            Label lblDate = (Label)doc.GetElementById("lblDate");
            Label lblTime = (Label)doc.GetElementById("lblTime");
            Label lblLocDescr = (Label)doc.GetElementById("lblLocDescr");
            Label lblOffenceDescrEng = (Label)doc.GetElementById("lblOffenceDescrEng");
            Label lblOffenceDescrAfr = (Label)doc.GetElementById("lblOffenceDescrAfr");
            Label lblCode = (Label)doc.GetElementById("lblCode");
            //Label lblCameraNo = (Label)doc.GetElementById("lblCamera");
            Label lblOfficerNo = (Label)doc.GetElementById("lblOfficerNo");
            Label lblFineAmount = (Label)doc.GetElementById("lblAmount");
            Label lblPrintDate = (Label)doc.GetElementById("lblPrintDate");
            Label lblOffenceDate = (Label)doc.GetElementById("lblOffenceDate");
            Label lblOffenceTime = (Label)doc.GetElementById("lblOffenceTime");
            Label lblAut = (Label)doc.GetElementById("lblText");
            Label lblLocation = (Label)doc.GetElementById("lblLocation");
            //Label lblSpeedLimit = (Label)doc.GetElementById("lblSpeedLimit");
            //Label lblSpeed = (Label)doc.GetElementById("lblSpeed");
            Label lblOfficer = (Label)doc.GetElementById("lblOfficer");
            Label lblRegNo = (Label)doc.GetElementById("lblRegNo");
            Label lblIssuedBy = (Label)doc.GetElementById("lblIssuedBy");
            Label lblPaymentDate = (Label)doc.GetElementById("lblPaymentDate");
            Label lblCourtName = (Label)doc.GetElementById("lblCourtName");
            Label lblEasyPay = (Label)doc.GetElementById("lblEasyPay");
            //Label lblFilmNo = (Label)doc.GetElementById("lblFilmNo");
            //Label lblFrameNo = (Label)doc.GetElementById("lblFrameNo");
            Label lblVehicle = (Label)doc.GetElementById("lblVehicle");
            Label lblAuthorityAddress = (Label)doc.GetElementById("lblAuthorityAddress");
            Label lblAutTel = (Label)doc.GetElementById("lblAutTel");
            Label lblAutFax = (Label)doc.GetElementById("lblAutFax");
            Label lblDisclaimer = (Label)doc.GetElementById("lblDisclaimer");
            Label lblReprintDate = (Label)doc.GetElementById("lblReprintDate");
            Label lblReprintDateText = (Label)doc.GetElementById("lblReprintDateText");

            Label lblAuthName = (Label)doc.GetElementById("lbl_AuthName");
            Label lblAutDepart = (Label)doc.GetElementById("lbl_AutDepart");
            Label lblAutPhysAddr = (Label)doc.GetElementById("lbl_AutPhysAddr");
            Label lblAutPostAddr = (Label)doc.GetElementById("lbl_AutPostAddr");
            Label lblCourt = (Label)doc.GetElementById("lblCourt");
            Label lbl_AuthName2 = (Label)doc.GetElementById("lbl_AuthName2");
            Label lbl_Court = (Label)doc.GetElementById("lbl_Court");

            phBarCode = (PlaceHolder)doc.GetElementById("phBarCode");
            phBarCode.LaidOut += new PlaceHolderLaidOutEventHandler(ph_BarCode);

            Label lblPrintFileName = (Label)doc.GetElementById("lblPrintFileName");
            PageNumberingLabel PageNumberingLabel1 = (PageNumberingLabel)doc.GetElementById("PageNumberingLabel1");

            if (this.IsWebMode
                && HttpContext.Current.Session["showAllNotices"] != null
                && HttpContext.Current.Session["showAllNotices"].ToString().Equals("Y"))
                showAll = "Y";

            List<SqlParameter> paraList = new List<SqlParameter>();
            paraList.Add(new SqlParameter("@AutIntNo", this.AutIntNo));
            paraList.Add(new SqlParameter("@PrintFile", this.PrintFileName));
            paraList.Add(new SqlParameter("@Status", status));
            paraList.Add(new SqlParameter("@ShowAll", showAll));
            paraList.Add(new SqlParameter("@LastUser", this.SubProcess.LastUser));
            paraList.Add(new SqlParameter("@ViolationCutOff", new char()));
            paraList.Add(new SqlParameter("@NoOfDaysIssue", new int()));
            paraList.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = strSetGeneratedDate }); //2013-06-26 added by Nancy for 4973

            object objValue = ExecuteScalar("NoticePrint_Update2nd_WS", paraList, false);
            int iValue = -1;
            if (objValue == null || !int.TryParse(objValue.ToString(), out iValue) || iValue < 0)
            {
                CloseConnection();
                ErrorProcessing("Error on executing NoticePrint_Update2nd_WS");
                return;
            }

            paraList.RemoveRange(4, 4);//2013-06-26 updated by Nancy from '(4, 3)' to '(4, 4)' for 4973
            paraList.Insert(2, new SqlParameter("@NotIntNo", 0));
            paraList.Add(new SqlParameter("@Option", option));

            DataSet ds = ExecuteDataSet("NoticePrint_WS", paraList, "@NotIntNo");

            string sTempEng = string.Empty;
            string sTempAfr = string.Empty;
            MergeDocument merge = new MergeDocument();
            byte[] buffer;
            Document report = null;
            int noOfNotices = 0;
            char pad0Char = Convert.ToChar("0");

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    #region
                    try
                    {
                        //2014-04-22 Heidi added this.ReportFile.ToUpper().IndexOf("_TW") >= 0 for TWK upgrade(5168)
                        if (this.ReportFile.ToUpper().IndexOf("_MB") >= 0 || this.ReportFile.ToUpper().IndexOf("_TW") >= 0)
                        {
                            barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);

                            lblAuthName.Text = dr["AutName"].ToString().Trim();
                            lbl_AuthName2.Text = dr["AutName"].ToString();
                            lblCourt.Text = dr["NotCourtName"].ToString().Trim();
                            lbl_Court.Text = dr["NotCourtName"].ToString().Trim();
                            lblAutPhysAddr.Text = (dr["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr1"].ToString().Trim() + "\n")
                                    + (dr["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr2"].ToString().Trim() + "\n")
                                    + (dr["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr3"].ToString().Trim() + "\n")
                                    + (dr["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : dr["AutPhysAddr4"].ToString().Trim() + "\n")
                                    + dr["AutPhysCode"].ToString();
                            lblAutPostAddr.Text = (dr["AutPostAddr1"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr1"].ToString().Trim() + "\n")
                                    + (dr["AutPostAddr2"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr2"].ToString().Trim() + "\n")
                                    + (dr["AutPostAddr3"].ToString().Trim().Equals("") ? "" : dr["AutPostAddr3"].ToString().Trim() + "\n")
                                    + dr["AutPostCode"].ToString();

                            if (dr["NotSendTo"].ToString().Equals("P"))
                            {
                                lblForAttention.Text = dr["PrxInitials"].ToString() + " " + dr["PrxSurname"].ToString() + " as Representative of " + dr["DrvSurname"].ToString();
                                lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                            }
                            else
                            {
                                lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();
                                lblId.Text = dr["DrvIDNUmber"].ToString().Trim();
                            }

                            lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                    + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                    + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                    + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                    + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                    + dr["DrvPOCode"].ToString().Trim();

                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");

                            lblStatRef.Text = (dr["ChgStatRefLine1"] == DBNull.Value ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                + (dr["ChgStatRefLine2"] == DBNull.Value ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                + (dr["ChgStatRefLine3"] == DBNull.Value ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                + (dr["ChgStatRefLine4"] == DBNull.Value ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                            lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                            lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                            lblLocDescr.Text = dr["NotLocDescr"].ToString();
                            //lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                            //lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();

                            sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                            sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());

                            if (dr["NotNewOffender"].ToString().Equals("Y"))
                            {
                                lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                            }
                            else
                            {
                                lblOffenceDescrEng.Text = sTempEng;
                                lblOffenceDescrAfr.Text = sTempAfr;
                            }

                            lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["Not2ndPaymentDate"]);
                            //lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();
                            lblCode.Text = dr["ChgOffenceCode"].ToString();

                            //dls no need for camera if this is an HWO
                            //lblCameraNo.Text = dr["NotCamSerialNo"].ToString();

                            lblOfficerNo.Text = dr["NotOfficerNo"].ToString();
                            
                            //update by Rachel 20140811 for 5337
                            //lblFineAmount.Text = string.Format(GetResource("SecondNotice_HWO_Viewer.aspx", "lblFineAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString())));
                            //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["ChgRevFineAmount"].ToString()));
                            lblFineAmount.Text = string.Format(GetResource("SecondNotice_HWO_Viewer.aspx", "lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]));
                            lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]);
                            //end update by Rachel 20140811 for 5337

                            lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);
                            lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint2ndNoticeDate"]);

                            lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                            lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                            lblLocation.Text = dr["NotLocDescr"].ToString();
                            //lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                            //lblSpeed.Text = dr["NotSpeed1"].ToString();
                            //lblOfficer.Text = dr["NotOfficerNo"].ToString();  //dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();
                            lblRegNo.Text = dr["NotRegNo"].ToString();
                            lblDisclaimer.Text = dr["Disclaimer"].ToString();

                            lblOfficer.Text = dr["NotOfficerInit"].ToString() + " " + dr["NotOfficerSName"].ToString();

                        }

                        report = doc.Run(parameters);

                        ImportedPageArea importedPage;
                        byte[] bufferTemplate;
                        if (!this.TemplateFile.Equals(""))
                        {
                            if (this.TemplateFile.ToLower().IndexOf(".dplx") > 0)
                            {
                                DocumentLayout template = new DocumentLayout(this.TemplateFilePath);
                                StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                                ParameterDictionary parametersTemplate = new ParameterDictionary();
                                Document reportTemplate = template.Run(parametersTemplate);
                                bufferTemplate = reportTemplate.Draw();
                                PdfDocument pdf = new PdfDocument(bufferTemplate);
                                PdfPage page = pdf.Pages[0];
                                importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                            }
                            else
                            {
                                //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                importedPage = new ImportedPageArea(this.TemplateFilePath, 1, 0.0F, 0.0F, 1.0F);
                            }

                            //ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                            //rptPage.Elements.Insert(0, importedPage);
                            report.Template = new Template();
                            report.Template.Elements.Add(importedPage);
                        }
                        buffer = report.Draw();
                        merge.Append(new PdfDocument(buffer));
                    }
                    catch (Exception ex)
                    {
                        ErrorProcessing(ex.Message);
                        return;
                    }

                    noOfNotices++;
                    #endregion
                }

                if (!this.PrintFileName.Substring(0, 1).Equals("*") && !this.PrintFileName.Equals("-1"))
                {
                    //dls 071228 - add summary report page and append to end of PDF document
                    string reportFile = "NoticeSummary.dplx";
                    string path;
                    CheckReportTemplateExists(null, reportFile, out path);

                    NoticeSummary summary = new NoticeSummary(this.SubProcess.ConnectionString, this.AutIntNo, this.PrintFileName, format, showAll, status, option, path, reportFile, noOfNotices);

                    byte[] sumReport = summary.CreateSummary();

                    merge.Append(new PdfDocument(sumReport));

                    sumReport = null;
                }

            }

            if (merge.Pages.Count > 0)
            {
                //this.OutputBuffer = merge.Draw();
                CreateTempFile(merge);
            }

            phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);

        }

        void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (barCodeImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 25.0F;
                    e.ContentArea.Add(img);
                }
            }
        }

    }
}
