﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Datasets;
using System.IO;
using Stalberg.TMS;
using System.Data;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleWOA : PrintFileBase
    {
        public PrintFileModuleWOA(string lastUser)  // 2013-07-23 add parameter lastUser by Henry
        {
            this.ErrorPage = GetResource("WOAViewer.aspx", "thisPage");
            this.ErrorPageUrl = "WOAViewer.aspx";
            this.lastUser = lastUser;
        }

        string lastUser { get; set; }
        string spWOAPrintUpdate = "WOAPrintUpdate_WS";
        string spWOAPrintWOA = "WOAPrintWOA_WS";
        string spWOAPrintWOA_MM = "WOAPrintWOA_MM_WS";
        string spWOAPrintWOA_UL = "WOAPrintWOA_UL_WS";//Heidi 2013-08-19 added for task 5067

        public override void InitialWork()
        {
            //set ReportFile
            this.ReportFile = this.ReportDB.GetAuthReportName(this.AutIntNo, "WOAPrint");
            if (string.IsNullOrWhiteSpace(this.ReportFile))
            {
                this.ReportFile = "WOA_Personal_DotMatrix.rpt";
                this.ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, "WOAPrint", "System", "");
            }

            //set stored procedure name
            this.StoredProcedureName_Update = spWOAPrintUpdate;
            if (this.ReportFile.ToUpper().IndexOf("MB") > 1)
            {
                this.StoredProcedureName_Select = this.spWOAPrintWOA_MM;
            }
            else if (this.ReportFile.ToUpper().IndexOf("UL") > 1)
            {
                this.StoredProcedureName_Select = this.spWOAPrintWOA_UL;//Heidi 2013-08-19 added for task 5067
            }
            else
            {
                this.StoredProcedureName_Select = this.spWOAPrintWOA;
            }

            // check report template file exists
            string path;
            if (!CheckReportTemplateExists("WOAPrint", null, out path)) return;
        }

        public override void MainWork()
        {
            string strSetGeneratedDate = GetAuthorityRule("2570").ARString.Trim().ToUpper(); //2013-06-27 added by Heidi for 4973 
            #region load and initialize rpt file
            ReportDocument reportDoc = new ReportDocument();
            reportDoc.Load(this.ReportFilePath);

            // Get the PageMargins structure and set the margins for the report.
            PageMargins margins = reportDoc.PrintOptions.PageMargins;
            margins.leftMargin = 0;
            margins.topMargin = 0;
            margins.rightMargin = 0;
            margins.bottomMargin = 0;

            // Apply the page margins.
            reportDoc.PrintOptions.ApplyPageMargins(margins);

            reportDoc.PrintOptions.PaperSize = PaperSize.PaperFanfoldStdGerman; // 8.5" x 12"
            #endregion

            List<SqlParameter> paraList;

            #region update WOAGeneratedDate and WOAIssueDate when WOAPrintDate is null
            paraList = new List<SqlParameter>();
            paraList.Add(new SqlParameter("@PrintFileName", this.PrintFileName));
            paraList.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = strSetGeneratedDate }); //2013-06-27 added by Heidi for 4973
		    paraList.Add(new SqlParameter("@LastUser", lastUser));
            object objValue = ExecuteScalar(this.StoredProcedureName_Update, paraList, false);
            int iValue = -1;
            if (objValue == null || !int.TryParse(objValue.ToString(), out iValue) || iValue < 0)
            {
                CloseConnection();
                ErrorProcessing();
                return;
            }
            #endregion

            #region get the data source            

            paraList = new List<SqlParameter>();
            paraList.Add(new SqlParameter("@PrintFileName", this.PrintFileName));
            //Jerry 2014-05-13 add 6210 authority rule value param, S56 name and address can not be changed
            paraList.Add(new SqlParameter("@AR6210", GetAuthorityRule("6210").ARString.Trim().ToUpper()));
            dsPrintWOA ds = ExecuteDataSet<dsPrintWOA>(this.StoredProcedureName_Select, paraList);
            if (ds == null || !this.SubProcess.IsSuccessful || (ds.Tables[1].Rows.Count == 0 && this.IsWebMode))
            {
                ds.Dispose();
                ErrorProcessing(GetResource("WOAViewer.aspx", "strWriteMsg"));
                return;
            }
            #endregion

            // Bind the report to the data
            reportDoc.SetDataSource(ds.Tables[1]);
            ds.Dispose();

            // Perform the export
            var stream  = reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            //this.OutputBuffer = ms.ToArray(); //push data to byte array.
            CreateTempFile(stream);
            reportDoc.Dispose();
        }
    }
}
