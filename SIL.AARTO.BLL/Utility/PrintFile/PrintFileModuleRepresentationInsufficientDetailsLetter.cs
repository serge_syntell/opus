﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Merger;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.Data;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleRepresentationInsufficientDetailsLetter : PrintFileBase
    {
        bool printFlag = false;
        int repIntNo = -1;
        bool isSummons = false;

        public PrintFileModuleRepresentationInsufficientDetailsLetter(bool printFlag, int repIntNo, bool isSummons = false)
        {
            this.ErrorPage = GetResource("RepresentationInsufficientDetails_LetterViewer.aspx", "thisPage"); 
            this.ErrorPageUrl = "RepresentationInsufficientDetails_LetterViewer.aspx";

            this.printFlag = printFlag;
            this.repIntNo = repIntNo;
            this.isSummons = isSummons;
        }

        public override void InitialWork()
        {
            string function = string.Empty;
            string defaultReportPage = string.Empty;


            function = "InsufficientDetails";
            defaultReportPage = "RepresentationInsufficientDetails_PL.dplx";


            //set ReportFile
            this.ReportFile = this.ReportDB.GetAuthReportName(this.AutIntNo, function);
            if (string.IsNullOrWhiteSpace(this.ReportFile))
            {
                this.ReportFile = defaultReportPage;
                this.ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, function, "system", string.Empty);
            }

            // check report template file exists
            string path;
            if (!CheckReportTemplateExists(function, null, out path)) return;
        }

        public override void MainWork()
        {
            DocumentLayout doc = new DocumentLayout(this.ReportFilePath);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.SubProcess.ConnectionString;

            byte[] buffer = null;
            byte[] bufferTemplate = null;


            //2011-10-18 jerry change
            if (string.IsNullOrEmpty(this.PrintFileName))
            {
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("RepIntNo", this.repIntNo.ToString());
                parameters.Add("IsSummons", this.isSummons);

                if (!this.TemplateFile.Equals("") && this.TemplateFile.ToLower().IndexOf(".pdf") == -1)
                {
                    DocumentLayout template = new DocumentLayout(this.TemplateFilePath);
                    StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                    queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                    ParameterDictionary parametersTemplate = new ParameterDictionary();

                    Document reportTemplate = template.Run(parametersTemplate);
                    bufferTemplate = reportTemplate.Draw();
                }

                Document report = doc.Run(parameters);
                ImportedPageArea importedPage;

                if (!this.TemplateFile.Equals(""))
                {
                    if (this.TemplateFile.ToLower().IndexOf(".dplx") > 0)
                    {
                        PdfDocument pdf = new PdfDocument(bufferTemplate);
                        PdfPage page = pdf.Pages[0];
                        importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                    }
                    else
                    {
                        //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1 , 0.0F, 0.0F, 1.0F);
                        importedPage = new ImportedPageArea(this.TemplateFilePath, 1, 0.0F, 0.0F, 1.0F);
                    }

                    //ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                    //rptPage.Elements.Insert(0, importedPage);

                    report.Template = new Template();
                    report.Template.Elements.Add(importedPage);
                }

                //this.OutputBuffer = report.Draw();
                CreateTempFile(report);
            }
            else
            {
                RepresentationDB represent = new RepresentationDB(this.SubProcess.ConnectionString);

                List<SqlParameter> paraList = new List<SqlParameter>();
                paraList.Add(new SqlParameter("@PrintFileName", this.PrintFileName));
                paraList.Add(new SqlParameter("@PrintedFlag", this.printFlag));

                DataSet ds;
                if (this.IsWebMode)
                {
                    ds = ExecuteDataSet("RepresentationGetRepInfoByPrintFile", paraList);
                }
                else
                {
                    ds = ExecuteDataSet("RepresentationGetRepInfoByPrintFile_WS", paraList);
                    //Jerry 2012-06-20 change the status when windows service is coming
                    represent.UpdateRepresentationLetterPrinted(this.AutIntNo, this.PrintFileName);
                }

                //SqlDataReader reader = represent.GetRepInfoByPrintFile(this.PrintFileName, this.printFlag);

                MergeDocument merge = new MergeDocument();
                Document report;

                //Jerry 2012-06-28 handle no data
                bool hasData = false;

                foreach (DataTable dt in ds.Tables)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ParameterDictionary parameters = new ParameterDictionary();
                            parameters.Add("RepIntNo", Convert.ToInt32(dr["RepIntNo"]));
                            parameters.Add("IsSummons", Convert.ToBoolean(dr["IsSummons"]));

                            if (!this.TemplateFile.Equals("") && this.TemplateFile.ToLower().IndexOf(".pdf") == -1)
                            {
                                DocumentLayout template = new DocumentLayout(this.TemplateFilePath);
                                StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                                ParameterDictionary parametersTemplate = new ParameterDictionary();

                                Document reportTemplate = template.Run(parametersTemplate);
                                bufferTemplate = reportTemplate.Draw();
                            }

                            report = doc.Run(parameters);
                            ImportedPageArea importedPage;

                            if (!this.TemplateFile.Equals(""))
                            {
                                if (this.TemplateFile.ToLower().IndexOf(".dplx") > 0)
                                {
                                    PdfDocument pdf = new PdfDocument(bufferTemplate);
                                    PdfPage page = pdf.Pages[0];
                                    importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                                }
                                else
                                {
                                    //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1 , 0.0F, 0.0F, 1.0F);
                                    importedPage = new ImportedPageArea(this.TemplateFilePath, 1, 0.0F, 0.0F, 1.0F);
                                }

                                //ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                                //rptPage.Elements.Insert(0, importedPage);

                                report.Template = new Template();
                                report.Template.Elements.Add(importedPage);
                            }
                            buffer = report.Draw();
                            merge.Append(new PdfDocument(buffer));

                            buffer = null;
                            bufferTemplate = null;

                            hasData = true;
                        }
                    }
                }
                ds.Dispose();

                //Jerry 2012-06-28 handle no data
                if (!hasData)
                {
                    this.SubProcess.SkipNextProcesses = true;
                    this.SubProcess.Message = "NoData";
                }
                else
                {
                    //this.OutputBuffer = merge.Draw();
                    CreateTempFile(merge);
                }
            }

        }

    }
}
