﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF;
using SIL.AARTO.BLL.BarCode;
using Stalberg.TMS.Data.Util;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Globalization;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleChangeRegNo : PrintFileBase
    {
        public PrintFileModuleChangeRegNo()
        {
            //this.FileType = PrintFileType.DPLX;
            this.ErrorPage = GetResource("ChangeRegNo_Viewer.aspx", "thisPage"); 
            this.ErrorPageUrl = "ChangeRegNo_Viewer.aspx";
        }

        System.Drawing.Image barCodeImage;
        PlaceHolder phBarCode;
        byte[] b, bA;

        public override void InitialWork()
        {
            string longPrefix = this.PrintFileName.Substring(0, 7).ToUpper();
            string function;
            switch (longPrefix)
            {
                //at the moment everyone except JMPD uses the same report for RLV as for SPD, e.g. FirstNotice_ST.rpt
                case "REG_RLV":
                case "REG_SPD":
                default:
                    string defaultTemplate;

                    if (this.PrintFileName.IndexOf("2ND") > 0)
                    {
                        function = "ChangeRegNo2ndNotice";
                        defaultTemplate = "SecondNoticeTemplate_ST.pdf";
                    }
                    else
                    {
                        function = "ChangeRegNoNotice";
                        defaultTemplate = "FirstNoticeTemplate_ST.pdf";
                    }

                    this.ReportFile = this.ReportDB.GetAuthReportName(this.AutIntNo, function);
                    if (string.IsNullOrWhiteSpace(this.ReportFile))
                    {
                        this.ReportFile = function + ".dplx";
                        this.ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, function, "System", defaultTemplate);
                    }
                    else
                    {
                        if (!this.ReportFile.TrimEnd().ToLower().EndsWith(".dplx"))
                            this.ReportFile += ".dplx";
                    }

                    break;
            }

            string path;
            if (!CheckReportTemplateExists(function, null, out path)) return;
        }

        public override void MainWork()
        {
            bool showCrossHairs = GetAuthorityRule("3200").ARString.Trim().ToUpper() == "Y";
            int crossHairStyle = (int)GetAuthorityRule("3150").ARNumeric;
            int noOfDaysIssue = GetDateRule("NotOffenceDate", "NotIssue1stNoticeDate").DtRNoOfDays;
            string strSetGeneratedDate = GetAuthorityRule("2570").ARString.Trim().ToUpper(); //2013-06-13 added by Nancy for 4973 

            DocumentLayout doc = new DocumentLayout(this.ReportFilePath);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.SubProcess.ConnectionString;
            ParameterDictionary parameters = new ParameterDictionary();

            Label lblId = (Label)doc.GetElementById("lblId");
            Label lblReference = (Label)doc.GetElementById("lblReference");
            Label lblReferenceB = (Label)doc.GetElementById("lblReferenceB");
            Label lblFormattedNotice = (Label)doc.GetElementById("lblFormattedNotice");
            Label lblForAttention = (Label)doc.GetElementById("lblForAttention");
            Label lblAddress = (Label)doc.GetElementById("lblAddress");
            Label lblNoticeNumber = (Label)doc.GetElementById("lblNoticeNumber");
            Label lblPaymentInfo = (Label)doc.GetElementById("lblPaymentInfo");
            Label lblStatRef = (Label)doc.GetElementById("lblStatRef");
            Label lblDate = (Label)doc.GetElementById("lblDate");
            Label lblTime = (Label)doc.GetElementById("lblTime");
            Label lblLocDescr = (Label)doc.GetElementById("lblLocDescr");
            Label lblOffenceDescrEng = (Label)doc.GetElementById("lblOffenceDescrEng");
            Label lblOffenceDescrAfr = (Label)doc.GetElementById("lblOffenceDescrAfr");
            Label lblCode = (Label)doc.GetElementById("lblCode");
            Label lblCameraNo = (Label)doc.GetElementById("lblCamera");
            Label lblOfficerNo = (Label)doc.GetElementById("lblOfficerNo");
            Label lblFineAmount = (Label)doc.GetElementById("lblAmount");
            Label lblPrintDate = (Label)doc.GetElementById("lblPrintDate");
            Label lblOffenceDate = (Label)doc.GetElementById("lblOffenceDate");
            Label lblOffenceTime = (Label)doc.GetElementById("lblOffenceTime");
            Label lblAut = (Label)doc.GetElementById("lblText");
            Label lblLocation = (Label)doc.GetElementById("lblLocation");
            Label lblSpeedLimit = (Label)doc.GetElementById("lblSpeedLimit");
            Label lblSpeed = (Label)doc.GetElementById("lblSpeed");
            Label lblOfficer = (Label)doc.GetElementById("lblOfficer");
            Label lblRegNo = (Label)doc.GetElementById("lblRegNo");
            Label lblIssuedBy = (Label)doc.GetElementById("lblIssuedBy");
            Label lblPaymentDate = (Label)doc.GetElementById("lblPaymentDate");
            Label lblCourtName = (Label)doc.GetElementById("lblCourtName");
            Label lblEasyPay = (Label)doc.GetElementById("lblEasyPay");
            Label lblFilmNo = (Label)doc.GetElementById("lblFilmNo");
            Label lblFrameNo = (Label)doc.GetElementById("lblFrameNo");
            Label lblVehicle = (Label)doc.GetElementById("lblVehicle");
            Label lblAuthorityAddress = (Label)doc.GetElementById("lblAuthorityAddress");
            Label lblAutTel = (Label)doc.GetElementById("lblAutTel");
            Label lblAutFax = (Label)doc.GetElementById("lblAutFax");
            Label lblDisclaimer = (Label)doc.GetElementById("lblDisclaimer");
            Label lblReprintDate = (Label)doc.GetElementById("lblReprintDate");
            Label lblReprintDateText = (Label)doc.GetElementById("lblReprintDateText");

            phBarCode = (PlaceHolder)doc.GetElementById("phBarCode");
            phBarCode.LaidOut += new PlaceHolderLaidOutEventHandler(phBarCode_LaidOut);
            PlaceHolder phImage = null;
            PlaceHolder phImageA = null;

            // only do this is its a noaog or first notice
            if (this.ReportFile.IndexOf("ChangeRegNo2nd") < 0)
            {
                phImage = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phImage");
                phImage.LaidOut += new PlaceHolderLaidOutEventHandler(phImage_LaidOut);
            }
            if (this.ReportFile.IndexOf("_RLV") >= 0)
            {
                phImageA = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phImageA");
                phImageA.LaidOut += new PlaceHolderLaidOutEventHandler(phImageA_LaidOut);
            }

            //get additional parameters for date rules and sysparam setting
            SysParamDB sp = new SysParamDB(this.SubProcess.ConnectionString);
            string violationCutOff = "N";
            int spValue = 0;
            sp.CheckSysParam("ViolationCutOff", ref spValue, ref violationCutOff);

            List<SqlParameter> paraList = new List<SqlParameter>();
            paraList.Add(new SqlParameter("@AutIntNo", this.AutIntNo));
            paraList.Add(new SqlParameter("@PrintFile", this.PrintFileName));
            //paraList.Add(new SqlParameter("@NotIntNo", 0));
            paraList.Add(new SqlParameter("@Status", 10));
            paraList.Add(new SqlParameter("@ShowAll", "N"));
            //paraList.Add(new SqlParameter("@Format", "TMS"));
            //paraList.Add(new SqlParameter("@Option", 1));
            paraList.Add(new SqlParameter("@LastUser", this.SubProcess.LastUser));
            paraList.Add(new SqlParameter("@ViolationCutOff", violationCutOff));
            paraList.Add(new SqlParameter("@NoOfDaysIssue", noOfDaysIssue));
            paraList.Add(new SqlParameter("@AuthRule", SqlDbType.VarChar, 3) { Value = strSetGeneratedDate }); //2013-06-13 added by Nancy for 4973
            
            //object objValue = ExecuteScalar("NoticePrint_Update1st_WS", paraList, false);
            object objValue = ExecuteScalar("NoticePrint_UpdateREG_WS", paraList, false);
            int iValue = -1;
            if (objValue == null || !int.TryParse(objValue.ToString(), out iValue) || iValue < 0)
            {
                CloseConnection();
                ErrorProcessing("Error on executing NoticePrint_UpdateREG_WS");
                return;
            }

            paraList.RemoveRange(4, 4);//2013-06-13 updated by Nancy from '(4, 3)' to '(4, 4)' for 4973
            paraList.Insert(2, new SqlParameter("@NotIntNo", 0));
            paraList.Add(new SqlParameter("@Option", 1));

            //DataSet ds = ExecuteDataSet("NoticePrint", paraList);
            DataSet ds = ExecuteDataSet("NoticePrint_WS", paraList, "@NotIntNo");

            string sTempEng, sTempAfr;
            MergeDocument merge = new MergeDocument();
            if (ds != null)
            {
                bool ignore = false;
                int nValue = 0;
                byte[] bufferReport, bufferTemplate;
                foreach (DataTable dt in ds.Tables)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dt.Columns.Count <= 1 || Convert.IsDBNull(dr[0]) || int.TryParse(dr[0].ToString(), out nValue))
                                break;  // try next DataTable
                            else
                                ignore = true;  // get the correct DataTable

                            lblNoticeNumber.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                            lblStatRef.Text = (Convert.IsDBNull(dr["ChgStatRefLine1"]) ? " " : dr["ChgStatRefLine1"].ToString().Trim())
                                + (Convert.IsDBNull(dr["ChgStatRefLine2"]) ? " " : dr["ChgStatRefLine2"].ToString().Trim())
                                + (Convert.IsDBNull(dr["ChgStatRefLine3"]) ? " " : dr["ChgStatRefLine3"].ToString().Trim())
                                + (Convert.IsDBNull(dr["ChgStatRefLine4"]) ? " " : dr["ChgStatRefLine4"].ToString().Trim());

                            //dls 071221 - added reprint date
                            //if (Convert.ToInt32(dr["ChargeStatus"]) >= 250)   //ChargeStatus has been changed to NoticeStatus since 4416
                            if (Convert.ToInt32(dr["NoticeStatus"]) >= 250)
                            {
                                lblReprintDateText.Text = GetResource("ChangeRegNo_Viewer.aspx", "lblReprintDateText.Text");
                                lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
                            }

                            if (this.ReportFile.IndexOf("_RLV") < 0)
                            {
                                lblEasyPay.Text = dr["NotEasyPayNumber"].ToString().Trim();
                                lblFilmNo.Text = dr["NotFilmNo"].ToString().Trim();
                                lblFrameNo.Text = dr["NotFrameNo"].ToString().Trim();
                                lblVehicle.Text = dr["NotVehicleMake"].ToString().Trim();

                                lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPaymentDate"]);
                                lblCourtName.Text = dr["NotCourtName"].ToString().ToUpper();
                                lblCameraNo.Text = dr["NotCamSerialNo"].ToString();
                                lblOfficerNo.Text = dr["NotOfficerNo"].ToString();

                                //update by Rachel 20140811 for 5337
                                //lblFineAmount.Text = "R " + string.Format("{0:0.00}", Convert.ToDecimal(dr["ChgRevFineAmount"]));
                                lblFineAmount.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0.00}",dr["ChgRevFineAmount"]);
                                //end update by Rachel 20140811 for 5337

                                lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);
                                lblOffenceDate.Text = string.Format("{0:yyyy-MM-dd}", dr["NotOffenceDate"]);
                                lblOffenceTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                lblLocation.Text = dr["NotLocDescr"].ToString();
                                lblSpeedLimit.Text = dr["NotSpeedLimit"].ToString();
                                lblSpeed.Text = dr["NotSpeed1"].ToString();
                                lblOfficer.Text = dr["NotOfficerNo"].ToString();
                                lblRegNo.Text = dr["NotRegNo"].ToString();
                                lblDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]);
                                lblTime.Text = string.Format("{0:HH:mm}", dr["NotOffenceDate"]);
                                lblLocDescr.Text = dr["NotLocDescr"].ToString();
                                sTempEng = dr["OcTDescr"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                sTempAfr = dr["OcTDescr1"].ToString().Replace("(X~1)", dr["NotRegNo"].ToString()).Replace("(X~2)", dr["NotSpeedLimit"].ToString()).Replace("(X~3)", dr["MinSpeed"].ToString());
                                if (dr["NotNewOffender"].ToString().Equals("Y"))
                                {
                                    lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
                                    lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
                                }
                                else
                                {
                                    lblOffenceDescrEng.Text = sTempEng;
                                    lblOffenceDescrAfr.Text = sTempAfr;
                                }

                            }
                            else
                            {
                                lblAutTel.Text = dr["AutTel"].ToString();
                                lblAutFax.Text = dr["AutFax"].ToString();
                                sTempEng = dr["OcTDescr"].ToString().Replace("*001   ", dr["OrigRegNo"].ToString());
                                sTempAfr = dr["OcTDescr1"].ToString().Replace("*001   ", dr["OrigRegNo"].ToString());

                                lblOffenceDescrAfr.Text = string.Format(GetResource("ChangeRegNo_Viewer.aspx", "lblOffenceDescrAfr.Text"),
                                    dr["NotVehicleMake"].ToString().Trim(),
                                    dr["NotRegNo"].ToString().Trim(),
                                    string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]),
                                    string.Format("{0:HH:mm}", dr["NotOffenceDate"]),
                                    dr["NotLocDescr"].ToString(),
                                    sTempAfr
                                    );
                                lblOffenceDescrEng.Text = string.Format(GetResource("ChangeRegNo_Viewer.aspx", "lblOffenceDescrEng.Text"),
                                dr["NotVehicleMake"].ToString().Trim(),
                                dr["NotRegNo"].ToString().Trim(),
                                string.Format("{0:d MMMM yyyy}", dr["NotOffenceDate"]),
                                string.Format("{0:HH:mm}", dr["NotOffenceDate"]),
                                dr["NotLocDescr"].ToString(),
                                sTempAfr
                                );

                                lblFormattedNotice.Text = dr["NotTicketNo"].ToString().Trim().Replace("/", " / ");
                                lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", dr["NotPrint1stNoticeDate"]);

                            }

                            if (this.ReportFile.ToLower().IndexOf("2nd") < 0)
                            {
                                lblAut.Text = dr["AutNoticeIssuedByInfo"].ToString();
                            }

                            if (dr["NotSendTo"].ToString().Equals("P"))
                                lblId.Text = dr["PrxIDNUmber"].ToString().Trim();
                            else
                                lblId.Text = dr["DrvIDNUmber"].ToString().Trim();

                            lblDisclaimer.Text = dr["Disclaimer"].ToString();

                            barCodeImage = Code128Rendering.MakeBarcodeImage(dr["NotTicketNo"].ToString(), 1, 25, true);

                            lblCode.Text = dr["ChgOffenceCode"].ToString();

                            if (dr["NotSendTo"].ToString().Equals("P"))
                                lblForAttention.Text = string.Format(GetResource("ChangeRegNo_Viewer.aspx", "lblForAttention.Text"),
                                        dr["PrxInitials"].ToString(), dr["PrxSurname"].ToString(), dr["DrvSurname"].ToString()
                                        );
                            else
                                lblForAttention.Text = dr["DrvInitials"].ToString() + " " + dr["DrvSurname"].ToString();

                            lblAddress.Text = (dr["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd1"].ToString().Trim()) + "\n")
                                + (dr["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd2"].ToString().Trim()) + "\n")
                                + (dr["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd3"].ToString().Trim()) + "\n")
                                + (dr["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd4"].ToString().Trim()) + "\n")
                                + (dr["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (dr["DrvPOAdd5"].ToString().Trim()) + "\n")
                                + dr["DrvPOCode"].ToString().Trim();

                            lblIssuedBy.Text = dr["AutNoticeIssuedByInfo"].ToString();

                            ScanImageDB imgDB = new ScanImageDB(this.SubProcess.ConnectionString);

                            if (!Convert.IsDBNull(dr["ScanImage1"]))
                            {
                                ScanImageDetails image1 = imgDB.GetImageFullPath(Convert.ToInt32(dr["ScanImage1"]));
                                b = GetImagesFromRemoteFileServer(image1);
                            }

                            // add cross-hair processing
                            int X = Convert.ToInt32(dr["XValue1"]);
                            int Y = Convert.ToInt32(dr["YValue1"]);

                            if (b != null && showCrossHairs && X > 0 && Y > 0)
                            {
                                Drawing draw = new Drawing();
                                System.Drawing.Image image = draw.CrossHairs(b, crossHairStyle.ToString(), X, Y);
                                MemoryStream ms = new MemoryStream();
                                image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                                b = ms.ToArray();
                            }

                            if (!Convert.IsDBNull(dr["ScanImage2"]))
                            {
                                //ScanImageDetails image2 = imgDB.GetImageFullPath(Convert.ToInt32(dr["ScanImage3"]));
                                // Oscar 20121019 changed
                                var image2 = imgDB.GetImageFullPath(Convert.ToInt32(dr["ScanImage2"]));
                                bA = GetImagesFromRemoteFileServer(image2);
                            }

                            Document report = doc.Run(parameters);

                            ImportedPageArea importedPage;
                            if (!this.TemplateFile.Equals(""))
                            {
                                if (this.TemplateFile.ToLower().IndexOf(".dplx") > 0)
                                {
                                    DocumentLayout template = new DocumentLayout(this.TemplateFilePath);
                                    StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                    queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                                    ParameterDictionary parametersTemplate = new ParameterDictionary();
                                    Document reportTemplate = template.Run(parametersTemplate);
                                    bufferTemplate = reportTemplate.Draw();
                                    PdfDocument pdf = new PdfDocument(bufferTemplate);
                                    PdfPage page = pdf.Pages[0];
                                    importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                                }
                                else
                                {
                                    importedPage = new ImportedPageArea(this.TemplateFilePath, 1, 0.0F, 0.0F, 1.0F);
                                }

                                //report.Pages[0].Elements.Insert(0, importedPage);
                                report.Template = new Template();
                                report.Template.Elements.Add(importedPage);
                            }
                            bufferReport = report.Draw();
                            merge.Append(new PdfDocument(bufferReport));
                            bufferReport = null;
                            bufferTemplate = null;

                        }

                        if (ignore)
                            break;
                    }
                }
                if (merge.Pages.Count > 0)
                {
                    //this.OutputBuffer = merge.Draw();
                    CreateTempFile(merge);
                }
            }

            phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(phBarCode_LaidOut);
            if (this.ReportFile.IndexOf("ChangeRegNo2nd") < 0)
            {
                phImage.LaidOut -= new PlaceHolderLaidOutEventHandler(phImage_LaidOut);
            }
            if (this.ReportFile.IndexOf("_RLV") >= 0)
            {
                phImageA.LaidOut -= new PlaceHolderLaidOutEventHandler(phImageA_LaidOut);
            }

        }


        private void phBarCode_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (barCodeImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 25.0F;
                    e.ContentArea.Add(img);
                }

                int generation = System.GC.GetGeneration(barCodeImage);
                barCodeImage = null;
                System.GC.Collect(generation);
            }
        }

        private void phImage_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            ImageLaidOut(this.b, e);
        }

        private void phImageA_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            ImageLaidOut(this.bA, e);
        }

        private byte[] GetImagesFromRemoteFileServer(ScanImageDetails image)
        {
            byte[] data = null;
            if (image.FileServer != null)
            {
                if (File.Exists(image.ImageFullPath))
                {
                    using (FileStream fs = File.OpenRead(image.ImageFullPath))
                    {
                        data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                    }
                }
            }
            return data;
        }


        private void ImageLaidOut(byte[] buff, PlaceHolderLaidOutEventArgs e)
        {
            if (buff != null)
            {
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buff), 0, 0);
                img.Height = 160.0F;
                img.Width = 210.0F;
                e.ContentArea.Add(img);

                int generation = GC.GetGeneration(buff);
                buff = null;
                GC.Collect(generation);
            }
        }

    }
}
