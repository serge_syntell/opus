﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using System.Data.SqlClient;
using System.Data;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF;
using System.Globalization;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleNoticeOfWOA : PrintFileBase
    {
        public PrintFileModuleNoticeOfWOA()
        {
            //this.ErrorPage = "NoticeOfWOA Viewer";
            this.ErrorPage = GetResource("NoticeOfWOAViewer.aspx", "thisPage");
            this.ErrorPageUrl = "NoticeOfWOAViewer.aspx";
        }

        public override void InitialWork()
        {
            //set ReportFile
            this.ReportFile = this.ReportDB.GetAuthReportName(this.AutIntNo, "NoticeOfWOA");
            if (string.IsNullOrWhiteSpace(this.ReportFile))
            {
                this.ReportFile = "NoticeOfWOA.dplx";
                this.ReportDB.AddAuthReportName(this.AutIntNo, this.ReportFile, "NoticeOfWOA", "System", "");
            }

            // check report template file exists
            string path;
            if (!CheckReportTemplateExists("NoticeOfWOA", null, out path)) return;
        }

        public override void MainWork()
        {
            DocumentLayout reportDoc = new DocumentLayout(this.ReportFilePath);
            StoredProcedureQuery query = (StoredProcedureQuery)reportDoc.GetQueryById("Query");
            query.ConnectionString = this.SubProcess.ConnectionString;

            // Create parameters for report's stored proc.
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("AutIntNo", this.AutIntNo);
            //mrs 20081020 must send printfile name parameter as we are only interested in the contents of the printfile
            parameters.Add("PrintFileName", this.PrintFileName);

            #region LAOUT OBJECT EXTRACTION
            Label lblNoticeTxt = (Label)reportDoc.GetElementById("lblTestNoticeText");
            Label lblPrintDate = (Label)reportDoc.GetElementById("lblPrintDate");
            Label lblCrtName = (Label)reportDoc.GetElementById("lblCrtName");
            Label lblSumCaseNo = (Label)reportDoc.GetElementById("lblSumCaseNo");
            Label lblSumCourtDate = (Label)reportDoc.GetElementById("lblSumCourtDate");
            Label lblSummonsNo = (Label)reportDoc.GetElementById("lblSummonsNo");
            Label lblJdgAmount = (Label)reportDoc.GetElementById("lblJdgAmount");
            Label lblAccFullName = (Label)reportDoc.GetElementById("lblAccFullName");
            Label fraPostalAddress = (Label)reportDoc.GetElementById("fraPostalAddress");
            Label lblTestNoticeText = (Label)reportDoc.GetElementById("lblTestNoticeText");
            Label recordEnglishOffence = (Label)reportDoc.GetElementById("recordEnglishOffence");
            Label fraCrtPaymentAddr = (Label)reportDoc.GetElementById("fraCrtPaymentAddr");
            Label fraPhysicalAddress = (Label)reportDoc.GetElementById("fraPhysicalAddress");
            Label recordAfrikaansOffence = (Label)reportDoc.GetElementById("recordAfrikaansOffence");
            Label lblmtrAfr = (Label)reportDoc.GetElementById("lblmtrAfr");
            Label lblMtrEng = (Label)reportDoc.GetElementById("lblMtrEng");
            #endregion

            List<SqlParameter> paraList = new List<SqlParameter>();
            paraList.Add(new SqlParameter("@AutIntNo", this.AutIntNo));
            paraList.Add(new SqlParameter("@PrintFileName", this.PrintFileName));

            DataSet ds = ExecuteDataSet("GetNoticeOfWOA_WS", paraList);

            MergeDocument merge = new MergeDocument();
            byte[] buffer;
            Document report = null;
            int noOfNotices = 0;

            char pad0Char = Convert.ToChar("0");

            if (ds != null)
            {
                bool ignore = false;
                int nValue = 0;

                foreach (DataTable dt in ds.Tables)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dt.Columns.Count <= 1 || Convert.IsDBNull(dr[0]) || int.TryParse(dr[0].ToString(), out nValue))
                                break;  // try next DataTable
                            else
                                ignore = true;  // get the correct DataTable

                            try
                            {
                                lblCrtName.Text = dr["CrtName"].ToString().Trim();
                                lblSumCaseNo.Text = dr["SumCaseNo"].ToString().Trim();
                                lblSumCourtDate.Text = string.Format("{0:d MMMM yyyy}", dr["SumCourtDate"]);
                                lblSummonsNo.Text = dr["SummonsNo"].ToString().Trim();
                                //lblJdgAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(dr["JdgAmount"].ToString()));

                                //update by Rachel 20140819 for 5337
                                //lblJdgAmount.Text = string.Format(GetResource("NoticeOfWOAViewer.aspx", "lblJdgAmount.Text"), string.Format("{0:0.00}", Decimal.Parse(dr["JdgAmount"].ToString()))); 
                                lblJdgAmount.Text = string.Format(GetResource("NoticeOfWOAViewer.aspx", "lblJdgAmount.Text"), string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["JdgAmount"])); 
                                //end update by Rachel 20140819 for 5337

                                lblAccFullName.Text = dr["AccFullName"].ToString().Trim();
                                lblmtrAfr.Text = dr["AutDocumentFromAfri"].ToString().Trim();
                                lblMtrEng.Text = dr["AutDocumentFromEnglish"].ToString().Trim();
                                //check to proc for which reg no is getting used here. 
                                //Use OrigRegNo if ChangeOfRegNo=Y and LetterTo=O
                                string regNo = dr["SumRegNo"].ToString().Trim();
                                string speedZone = dr["SumSpeedLimit"].ToString().Trim();
                                string speed = dr["MinSpeed"].ToString().Trim();

                                //string description = "Dat die beskuldigde " + dr["SChOcTDescr1"].ToString().Trim();
                                string description = string.Format(GetResource("NoticeOfWOAViewer.aspx", "description"), dr["SChOcTDescr1"].ToString().Trim());
                                // Remove the first sentence
                                int pos = description.IndexOf('.');
                                if (pos >= 0 && pos + 1 < description.Length)
                                    description = description.Substring(pos + 1);

                                recordAfrikaansOffence.Text = description.Replace("(X~1)", regNo).Replace("(X~2)", speedZone).Replace("(X~3)", speed);

                                //description = "That the accused " + dr["SChOcTDescr"].ToString().Trim();
                                description = string.Format(GetResource("NoticeOfWOAViewer.aspx", "description1"), dr["SChOcTDescr"].ToString().Trim());
                                // Remove the first sentence
                                pos = description.IndexOf('.');
                                if (pos >= 0 && pos + 1 < description.Length)
                                    description = description.Substring(pos + 1);

                                recordEnglishOffence.Text = description.Replace("(X~1)", regNo).Replace("(X~2)", speedZone).Replace("(X~3)", speed);

                                string strCrtPaymentAddr1 = dr["CrtPaymentAddr1"].ToString().Trim();
                                string strCrtPaymentAddr2 = dr["CrtPaymentAddr2"].ToString().Trim();
                                string strCrtPaymentAddr3 = dr["CrtPaymentAddr3"].ToString().Trim();
                                string strCrtPaymentAddr4 = dr["CrtPaymentAddr4"].ToString().Trim();

                                string[] courtPaymentAddrStrArray = { lblCrtName.Text, strCrtPaymentAddr1, strCrtPaymentAddr2, strCrtPaymentAddr3, strCrtPaymentAddr4 };

                                fraCrtPaymentAddr.Text = AppendAndRemoveEmptyStrs(courtPaymentAddrStrArray);

                                lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);     //DateTime.Now.ToString("D");

                                string newNoticeText = lblTestNoticeText.Text;
                                newNoticeText = newNoticeText.Replace("_SumCourtDate_", lblSumCourtDate.Text);
                                newNoticeText = newNoticeText.Replace("_SummonsNo_", lblSummonsNo.Text);
                                newNoticeText = newNoticeText.Replace("_CrtName_", lblCrtName.Text);

                                lblTestNoticeText.Text = newNoticeText;

                                string strAccStAdd1 = dr["AccStAdd1"].ToString().Trim();
                                string strAccStAdd2 = dr["AccStAdd2"].ToString().Trim();
                                string strAccStAdd3 = dr["AccStAdd3"].ToString().Trim();
                                string strAccStAdd4 = dr["AccStAdd4"].ToString().Trim();
                                string strAccStAdd5 = dr["AccStAdd5"].ToString().Trim();
                                string strAccStCode = dr["AccStCode"].ToString().Trim();

                                string[] physAddresStrArray = { strAccStAdd1, strAccStAdd2, strAccStAdd3, strAccStAdd4, strAccStAdd5, strAccStCode };

                                fraPhysicalAddress.Text = AppendAndRemoveEmptyStrs(physAddresStrArray);

                                string strPostalAddr1 = dr["AccPoAdd1"].ToString().Trim();
                                string strPostalAddr2 = dr["AccPoAdd2"].ToString().Trim();
                                string strPostalAddr3 = dr["AccPoAdd3"].ToString().Trim();
                                string strPostalAddr4 = dr["AccPoAdd4"].ToString().Trim();
                                string strPostalAddr5 = dr["AccPoAdd5"].ToString().Trim();
                                string strAccPoCode = dr["AccPoCode"].ToString().Trim();

                                string[] postalAddresStrArray = { strPostalAddr1, strPostalAddr2, strPostalAddr3, strPostalAddr4, strPostalAddr5, strAccPoCode };

                                fraPostalAddress.Text = AppendAndRemoveEmptyStrs(postalAddresStrArray);

                                report = reportDoc.Run(parameters);

                                ImportedPageArea importedPage;
                                byte[] bufferTemplate;
                                if (!this.TemplateFile.Equals(""))
                                {
                                    if (this.TemplateFile.ToLower().IndexOf(".dplx") > 0)
                                    {
                                        DocumentLayout template = new DocumentLayout(this.TemplateFilePath);
                                        StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                        queryTemplate.ConnectionString = this.SubProcess.ConnectionString;
                                        ParameterDictionary parametersTemplate = new ParameterDictionary();
                                        Document reportTemplate = template.Run(parametersTemplate);
                                        bufferTemplate = reportTemplate.Draw();
                                        PdfDocument pdf = new PdfDocument(bufferTemplate);
                                        PdfPage page = pdf.Pages[0];
                                        importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                                    }
                                    else
                                    {
                                        //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                        importedPage = new ImportedPageArea(this.TemplateFilePath, 1, 0.0F, 0.0F, 1.0F);
                                    }

                                    //ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                                    //rptPage.Elements.Insert(0, importedPage);

                                    report.Template = new Template();
                                    report.Template.Elements.Add(importedPage);

                                   
                                }
                                buffer = report.Draw();
                                merge.Append(new PdfDocument(buffer));

                                buffer = null;
                                bufferTemplate = null;
                            }
                            catch (Exception ex)
                            {
                                String sError = ex.Message;
                            }

                            noOfNotices++;
                        }
                        if (ignore)
                            break;
                    }
                }

                if (merge.Pages.Count > 0)
                {
                    //this.OutputBuffer = merge.Draw();
                    CreateTempFile(merge);
                }
            }
            ds.Dispose();
        }

        private string AppendAndRemoveEmptyStrs(params string[] pStringsToAppend)
        {
            string strComposite = "";
            if (pStringsToAppend.Length < 1)
                return "";

            for (int i = 0; i < pStringsToAppend.Length; i++)
            {
                string str = pStringsToAppend[i];

                if (str.Trim() != "" && i >= (pStringsToAppend.Length - 1))
                    strComposite += str;
                else if (str.Trim() != "")
                {
                    strComposite += str + "\n";
                }
            }
            return strComposite;
        }

    }
}
