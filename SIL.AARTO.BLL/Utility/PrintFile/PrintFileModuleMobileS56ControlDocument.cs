﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.PageElements;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using SIL.AARTO.BLL.Extensions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.ServiceBase;
using SIL.ServiceLibrary;
using Action = System.Action;
using Color = System.Drawing.Color;
using Image = ceTe.DynamicPDF.PageElements.Image;

namespace SIL.AARTO.BLL.Utility.PrintFile
{
    public class PrintFileModuleMobileS56ControlDocument : PrintFileBase, IDisposable
    {
        protected const string ThereIsNoDataForReport = "There is no data for report. \"{0}\"";
        protected const string FileDoesNotExistOrIsInvalid = "File does not exist or is invalid. \"{0}\"";
        const string DateFormat = "ddMMyy";
        const string MobileFailedDocumentLine1 = "SummonsNo: {0}";
        const string MobileFailedDocumentLine2 = "Offender: {0}";
        const string MobileFailedDocumentLine3 = "Address: {0}";
        const string MobileFailedDocumentLine4 = "There was a data error or similar problem with the electronic information and we were not able to generate the control document.";
        const string MobileFailedDocumentLine5 = "Please refer to the physical document printed by the mobile device.";
        readonly AartoDocumentImageService docImgService = new AartoDocumentImageService();
        readonly SummonsService summonsService = new SummonsService();

        public MobileS56ControlDocumentPageData PageData { get; set; }
        public Document OutputDocument { get; protected set; }
        public Action<string> OnSavingFile { get; set; }
        public Action onLoad { get; set; }

        internal override void OnLoad()
        {
            if (onLoad != null)
                onLoad();
        }

        public override void InitialWork()
        {
            OutputDocument = null;

            if (PageData == null)
            {
                LogProcessing(string.Format(GetResource(null, "ThereIsNoDataForReport", ThereIsNoDataForReport), SubProcess.ExportPrintFilePath), LogType.Error, false);
                return;
            }

            if (string.IsNullOrWhiteSpace(SubProcess.ExportPrintFilePath)
                || !ServiceUtility.CheckFolder(new FileInfo(SubProcess.ExportPrintFilePath).DirectoryName))
            {
                LogProcessing(string.Format(GetResource(null, "FileDoesNotExistOrIsInvalid", FileDoesNotExistOrIsInvalid), SubProcess.ExportPrintFilePath), LogType.Error, false);
                return;
            }

            if (string.IsNullOrWhiteSpace(PageData.OffSigFullPath)
                || !File.Exists(PageData.OffSigFullPath))
            {
                LogProcessing(string.Format(GetResource(null, "FileDoesNotExistOrIsInvalid", FileDoesNotExistOrIsInvalid), PageData.OffSigFullPath), LogType.Error, false);
                return;
            }

            if (string.IsNullOrWhiteSpace(PageData.TOSigFullPath)
                || !File.Exists(PageData.TOSigFullPath))
            {
                LogProcessing(string.Format(GetResource(null, "FileDoesNotExistOrIsInvalid", FileDoesNotExistOrIsInvalid), PageData.TOSigFullPath), LogType.Error, false);
                return;
            }

            ReportFile = "MobileS56ControlDocument.dplx";
            TemplateFile = "MobileS56ControlDocumentTemplate.pdf";
            if (!CheckReportTemplateExists("MobileS56ControlDocument"))
            {
                SubProcess.IsSuccessful = false;
                SubProcess.SkipNextProcesses = true;
            }
        }

        public override void MainWork()
        {
            var layout = new DocumentLayout(ReportFilePath);

            BuildQueries(layout);
            BuildTemplates(layout);
            BuildFields(layout);
            BuildImages(layout);

            OutputDocument = layout.Run(new ParameterDictionary());

            BuildImages(layout, true);
            BuildQueries(layout, true);

            SubProcess.ExportPrintFilePath = GetExportedFilePath(SubProcess.ExportPrintFilePath);
            if (OnSavingFile != null) OnSavingFile(SubProcess.ExportPrintFilePath);
            OutputDocument.Draw(SubProcess.ExportPrintFilePath);

            if (SubProcess.IsSuccessful)
                SubProcess.IsSuccessful = SetGenerated();
        }

        public bool BuildFailedReport()
        {
            OutputDocument = null;

            if (PageData == null)
                return false;

            var sb = new StringBuilder();
            sb.AppendFormat(GetResource(null, "MobileFailedDocumentLine1", MobileFailedDocumentLine1), PageData.SummonsNo).AppendLine();
            sb.AppendFormat(GetResource(null, "MobileFailedDocumentLine2", MobileFailedDocumentLine2), string.Format("{0} {1}", PageData.AccSurname, PageData.AccForenamesStr)).AppendLine();
            sb.AppendFormat(GetResource(null, "MobileFailedDocumentLine3", MobileFailedDocumentLine3), string.Format("{0} {1} {2}", PageData.Address1, PageData.Address2, PageData.Address3)).AppendLine().AppendLine();
            sb.AppendLine(GetResource(null, "MobileFailedDocumentLine4", MobileFailedDocumentLine4));
            sb.AppendLine(GetResource(null, "MobileFailedDocumentLine5", MobileFailedDocumentLine5));
            var text = sb.ToString();

            var lbl = new Label(text, 25, 25, 545, 792);

            var page = new Page(595, 842, 0);
            page.Elements.Add(lbl);

            OutputDocument = new Document();
            OutputDocument.Pages.Add(page);

            SubProcess.ExportPrintFilePath = GetExportedFilePath(SubProcess.ExportPrintFilePath);
            if (OnSavingFile != null) OnSavingFile(SubProcess.ExportPrintFilePath);
            OutputDocument.Draw(SubProcess.ExportPrintFilePath);

            return SetGenerated();
        }

        bool SetGenerated()
        {
            Summons summons;
            if (PageData == null
                || PageData.SumIntNo <= 0
                || (summons = this.summonsService.GetBySumIntNo(PageData.SumIntNo)) == null)
                return false;

            // if path has been changed, update old one.
            if (!PageData.ConDocFullPath.Equals2(SubProcess.ExportPrintFilePath))
            {
                AartoDocumentImage docImg;
                if (PageData.ConDocId <= 0
                    || (docImg = this.docImgService.GetByAaDocImgId(PageData.ConDocId)) == null)
                    return false;

                //var prefix = PageData.ConDocFullPath.Replace(docImg.AaDocImgPath, string.Empty);
                //var suffix = SubProcess.ExportPrintFilePath.Replace(prefix, string.Empty);
                docImg.AaDocImgPath = GetRelativePath(PageData.ConDocFullPath, docImg.AaDocImgPath, SubProcess.ExportPrintFilePath);
                this.docImgService.Save(docImg);
            }

            summons.MobileControlDocumentGeneratedDate = DateTime.Now;
            this.summonsService.Save(summons);
            return true;
        }

        #region BuildReport

        #region Queries

        void BuildQueries(DocumentLayout layout, bool isDetaching = false)
        {
            BindQuery(layout, "Query1", OpeningRecordSet, isDetaching);
            BindQuery(layout, "Query2", OpeningRecordSet, isDetaching);
        }

        void OpeningRecordSet(object sender, OpeningRecordSetEventArgs e)
        {
            using (var dt = new DataTable())
                e.RecordSet = new DataTableRecordSet(dt);
        }

        #endregion

        #region Templates

        void BuildTemplates(DocumentLayout layout)
        {
            var pdfPages = new PdfDocument(TemplateFilePath).Pages;
            BindTemplate(layout, "Report1", pdfPages[0]);
            BindTemplate(layout, "Report2", pdfPages[2]);
        }

        #endregion

        #region Fields

        void BuildFields(DocumentLayout layout)
        {
            #region Page1

            //BindLabel(layout, "lblNotTicketNo", PageData.SummonsNo);
            var ticketNoParts = PageData.SummonsNoParts;
            if (ticketNoParts.Length >= 4)
            {
                BindLabel(layout, "lblNotTicketNo1", ticketNoParts[0]);
                BindLabel(layout, "lblNotTicketNo2", ticketNoParts[1]);
                BindLabel(layout, "lblNotTicketNo3", ticketNoParts[3]);
            }
            //BindLabel(layout, "lblSurname", PageData.AccSurname);
            BindSeparatedLabels(layout, "lblSurname", 20, PageData.AccSurname, false);
            //BindLabel(layout, "lblForenames", PageData.AccForenamesStr);
            BindSeparatedLabels(layout, "lblForenames", 20, PageData.AccForenamesStr, false);
            BindSeparatedLabels(layout, "lblIDNumber", 13, PageData.AccIDNumber, true);
            BindLabel(layout, "lblAddress1", PageData.Address1);
            BindLabel(layout, "lblAddress2", PageData.Address2);
            BindLabel(layout, "lblAddress3", PageData.Address3);
            BindSeparatedLabels(layout, "lblTelHome", 10, PageData.AccTelHome, true);
            BindSeparatedLabels(layout, "lblPoCode", 4, PageData.AccPoCode, true);
            BindLabel(layout, "lblBusiness1", PageData.Business1);
            BindLabel(layout, "lblBusiness2", PageData.Business2);
            BindSeparatedLabels(layout, "lblTelWork", 10, PageData.AccTelWork, true);
            BindSeparatedLabels(layout, "lblEmployerPoCode", 4, PageData.AccEmployerPoCode, true);
            BindLabel(layout, "lblAge", PageData.AccAge);
            BindLabel(layout, "lblOccupation", PageData.AccOccupation);
            BindLabel(layout, "lblEmail", PageData.AccEmail);
            BindSeparatedLabels(layout, "lblOffenceDate", 6, PageData.NotOffenceDate.ToString(DateFormat), true);
            BindLabel(layout, "lblOffenceDate7", string.Format("{0}H{1}", PageData.NotOffenceDate.ToString("HH"), PageData.NotOffenceDate.ToString("mm")));
            BindSeparatedLabels(layout, "lblRegNo", 10, PageData.NotRegNo, false);
            BindLabel(layout, "lblLocDescr", PageData.NotLocDescr);
            BindLabel(layout, "lblGPS", string.Format("{0} {1}", PageData.GPS1, PageData.GPS2));
            BindLabel(layout, "lblVehicleMake", PageData.NotVehicleMake);
            BindLabel(layout, "lblVehicleType", PageData.NotVehicleType);
            BindLabel(layout, "lblIsTaxi", PageData.IsTaxi);
            //BindSeparatedLabels(layout, "lblSpeedLimit", 5, PageData.NotSpeedLimit, true);
            BindLabel(layout, "lblSpeedLimit", PageData.NotSpeedLimit);
            BindLabel(layout, "lblSpeed1", PageData.NotSpeed1);
            BindSeparatedLabels(layout, "lblCourtDate", 6, PageData.SumCourtDate.HasValue ? PageData.SumCourtDate.Value.ToString(DateFormat) : string.Empty, true);
            BindLabel(layout, "lblCourtName", PageData.NotCourtName);
            BindLabel(layout, "lblCourtNo", PageData.NotCourtNo);
            BindSeparatedLabels(layout, "lblPaymentDate", 6, PageData.NotPaymentDate.HasValue ? PageData.NotPaymentDate.Value.ToString(DateFormat) : string.Empty, true);
            BindLabel(layout, "lblOfficerName", PageData.NotOfficerName);
            BindLabel(layout, "lblLoSuDescr", PageData.LoSuDescr);
            BindLabel(layout, "lblLicenceNumber", PageData.AccLicenceNumber);
            BindSeparatedLabels(layout, "lblOfficerNo", 8, PageData.NotOfficerNo, false);

            #region Charges

            // find valid alternate charges
            PageData.FixedCharges.InsertRange(0, PageData.Charges.Where(c => !c.ChgIsMain
                && c.MainChargeID.HasValue
                && PageData.Charges.FirstOrDefault(m => m.ChgIsMain && m.ChgIntNo == c.MainChargeID.Value) != null));

            // find invalid alternate charges
            var exclude = PageData.FixedCharges.Select(a => a.ChgIntNo).ToArray();
            PageData.FixedCharges.InsertRange(0, PageData.Charges.Where(c => !c.ChgIsMain
                && !c.ChgIntNo.In(exclude)));

            // find main charges
            exclude = PageData.FixedCharges.Select(a => a.ChgIntNo).ToArray();
            PageData.FixedCharges.InsertRange(0, PageData.Charges.Where(c => c.ChgIsMain
                && !c.ChgIntNo.In(exclude)));

            // make up charges
            var chargeCount = PageData.FixedCharges.Count;
            if (chargeCount < 3)
            {
                var index = PageData.FixedCharges.FindLastIndex(c => c.ChgIsMain);
                if (index <= 0)
                {
                    if (chargeCount - index > 1)
                    {
                        for (var i = 0; i < 3 - chargeCount; i++)
                        {
                            PageData.FixedCharges.Insert(index + 1, new MobileS56Charge
                            {
                                ChgIsMain = true
                            });
                        }
                    }
                }
            }

            for (var i = 0; i < PageData.FixedCharges.Count; i++)
            {
                var charge = PageData.FixedCharges[i];
                if (charge.ChgIntNo == 0) continue;

                BindLabel(layout, "lblChgStatutoryRef" + (i + 1), charge.ChgStatutoryRef);
                // offcode will > 6 digits and has characters
                BindSeparatedLabels(layout, "lblChgOffenceCode" + (i + 1) + "_", charge.ChgOffenceCode.Length2() > 5 ? 6 : 5, charge.ChgOffenceCode, true);
                BindLabel(layout, "lblChgOffenceDescr" + (i + 1), charge.ChgOffenceDescr);

                BindSeparatedLabels(layout, "lblChgFineAmount" + (i + 1) + "_", 4, charge.ChgFineAmount, true);

                if (i == 2 && !charge.ChgIsMain && charge.MainChargeID.HasValue)
                {
                    var index = PageData.FixedCharges.FindIndex(c => c.ChgIsMain && c.ChgIntNo == charge.MainChargeID.Value);
                    if (index >= 0)
                        BindSeparatedLabels(layout, "lblMainChargeSequence", 2, (index + 1).ToString(CultureInfo.InvariantCulture), true);
                }
            }

            //var mainCharges = PageData.Charges.Where(c => c.ChgIsMain).ToArray();
            //var length = Math.Min(2, mainCharges.Length);
            //for (var i = 0; i < length; i++)
            //    BindSeparatedLabels(layout, "lblChgFineAmount" + (i + 1) + "_", 4, mainCharges[i].ChgFineAmount, true);

            //MobileS56Charge mainCharge = null;
            //var altCharge = PageData.Charges
            //    .Where(c => !c.ChgIsMain
            //        && c.MainChargeID.HasValue
            //        && (mainCharge = PageData.Charges.FirstOrDefault(m => m.ChgIsMain && m.ChgIntNo == c.MainChargeID.Value)) != null)
            //    .OrderBy(a => a.ChgSequence).FirstOrDefault();
            //if (altCharge != null)
            //{
            //    mainCharge = PageData.Charges.First(c => c.ChgIntNo == altCharge.MainChargeID.Value);
            //    BindSeparatedLabels(layout, "lblMainChargeSequence", 2, mainCharge.ChgSequence.ToString(CultureInfo.InvariantCulture), true);
            //    BindSeparatedLabels(layout, "lblChgFineAmount3_", 4, altCharge.ChgFineAmount, true);
            //}

            #endregion

            #endregion
        }

        void BindSeparatedLabels(DocumentLayout layout, string lblPrefix, int count, string value, bool isNumeric)
        {
            if (layout == null
                || string.IsNullOrWhiteSpace(lblPrefix)
                || string.IsNullOrWhiteSpace(value))
                return;

            int i, j;
            if (isNumeric)
            {
                if (value.Length < count)
                    value = value.PadLeft(count, '0');

                for (i = count - 1, j = value.Length - 1; i >= 0; i--,j--)
                    if (!BindLabel(layout, lblPrefix + (i + 1), value[j].ToString(CultureInfo.InvariantCulture)))
                        j++;
            }
            else
            {
                var length = Math.Min(count, value.Length);
                for (i = j = 0; i < length; i++,j++)
                    if (!BindLabel(layout, lblPrefix + (i + 1), value[j].ToString(CultureInfo.InvariantCulture)))
                        j--;
            }
        }

        #endregion

        #region Images

        void BuildImages(DocumentLayout layout, bool isDetaching = false)
        {
            if (PageData.AccSex.In("M", "F"))
                BindPlaceHolder(layout, PageData.AccSex.Equals2("F") ? "imgFemale" : "imgMale", (s, e) => DrawCross(e.ContentArea), isDetaching);
            BindPlaceHolder(layout, "imgOffSig", (s, e) => AddImage(PageData.OffSigFullPath, e.ContentArea), isDetaching);
            BindPlaceHolder(layout, "imgTOSig", (s, e) => AddImage(PageData.TOSigFullPath, e.ContentArea), isDetaching);
        }

        void DrawCross(ContentArea area)
        {
            var width = (int)area.Width;
            var height = (int)area.Height;
            using (var bmp = new Bitmap(width, height))
            {
                using (var grah = Graphics.FromImage(bmp))
                {
                    grah.Clear(Color.Transparent);
                    var pen = new Pen(Color.Gray, 1);
                    grah.DrawLine(pen, 0, 0, width, height);
                    grah.DrawLine(pen, width, 0, 0, height);
                    using (var ms = new MemoryStream())
                    {
                        bmp.Save(ms, ImageFormat.Png);
                        var image = new Image(ImageData.GetImage(ms), 0, 0)
                        {
                            Width = area.Width,
                            Height = area.Height
                        };
                        area.Add(image);
                    }
                }
            }
        }

        void AddImage(string imgPath, ContentArea area)
        {
            var imgData = ImageData.GetImage(imgPath);
            var fixedHeight = area.Height;
            var fixedWidth = fixedHeight*imgData.Width/imgData.Height;

            var image = new Image(imgData, 0, 0)
            {
                Width = fixedWidth,
                Height = fixedHeight
            };
            area.Add(image);
        }

        #endregion

        #endregion

        #region IDisposable

        bool isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~PrintFileModuleMobileS56ControlDocument()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.isDisposed) return;
            if (disposing)
            {
                var genPage = GC.GetGeneration(PageData);
                PageData.Charges.Clear();
                PageData.FixedCharges.Clear();
                PageData = null;

                var genDoc = GC.GetGeneration(OutputDocument);
                OutputDocument = null;

                GC.Collect(genPage);
                GC.Collect(genDoc);
            }
            this.isDisposed = true;
        }

        #endregion
    }

    public class MobileS56ControlDocumentPageData
    {
        public MobileS56ControlDocumentPageData()
        {
            Charges = new List<MobileS56Charge>();
            FixedCharges = new List<MobileS56Charge>();
        }

        public int SumIntNo { get; set; }
        public string SummonsNo { get; set; }
        public string SummonsNoStr
        {
            get { return !string.IsNullOrWhiteSpace(SummonsNo) ? SummonsNo.Replace("/", string.Empty) : string.Empty; }
        }

        public string[] SummonsNoParts
        {
            get { return !string.IsNullOrWhiteSpace(SummonsNo) ? SummonsNo.Split('/') : new string[0]; }
        }

        #region Page1

        public string AccSurname { get; set; }
        public string AccForenames { get; set; }
        public string AccInitials { get; set; }
        public string AccForenamesStr
        {
            get { return !string.IsNullOrWhiteSpace(AccForenames) ? AccForenames : AccInitials; }
        }
        public string AccIDNumber { get; set; }
        public string AccSex { get; set; }
        public string AccAge { get; set; }
        public string AccOccupation { get; set; }
        public string AccEmail { get; set; }
        public DateTime NotOffenceDate { get; set; }
        public string NotRegNo { get; set; }
        public string NotLocDescr { get; set; }
        public string GPS1 { get; set; }
        public string GPS2 { get; set; }
        public string NotVehicleMake { get; set; }
        public string NotVehicleType { get; set; }
        public string IsTaxi { get; set; }
        public string NotSpeedLimit { get; set; }
        public string NotSpeed1 { get; set; }
        public DateTime? SumCourtDate { get; set; }
        public string NotCourtName { get; set; }
        public string NotCourtNo { get; set; }
        public DateTime? NotPaymentDate { get; set; }
        public string NotOfficerInit { get; set; }
        public string NotOfficerSName { get; set; }
        public string NotOfficerName
        {
            get { return StringJoin(" ", NotOfficerInit, NotOfficerSName); }
        }
        public string LoSuDescr { get; set; }
        public string AccLicenceNumber { get; set; }
        public string NotOfficerNo { get; set; }

        #region Address

        public string AccPoAdd1 { get; set; }
        public string AccPoAdd2 { get; set; }
        public string AccPoAdd3 { get; set; }
        public string AccPoAdd4 { get; set; }
        public string AccPoAdd5 { get; set; }
        public string AccTelHome { get; set; }
        public string AccPoCode { get; set; }

        public string Address1
        {
            get { return AccPoAdd1 ?? string.Empty; }
        }

        public string Address2
        {
            get { return AccPoAdd2 ?? string.Empty; }
        }

        public string Address3
        {
            get { return StringJoin(" ", AccPoAdd3, AccPoAdd4, AccPoAdd5); }
        }

        #endregion

        #region Business

        public string AccEmployerAdd1 { get; set; }
        public string AccEmployerAdd2 { get; set; }
        public string AccEmployerAdd3 { get; set; }
        public string AccEmployerAdd4 { get; set; }
        public string AccTelWork { get; set; }
        public string AccEmployerPoCode { get; set; }

        public string Business1
        {
            get { return AccEmployerAdd1 ?? string.Empty; }
        }

        public string Business2
        {
            get { return StringJoin(" ", AccEmployerAdd2, AccEmployerAdd3, AccEmployerAdd4); }
        }

        #endregion

        #region Charges

        public List<MobileS56Charge> Charges { get; private set; }
        public List<MobileS56Charge> FixedCharges { get; private set; }

        #endregion

        #region Images

        public string ImageMachineName { get; set; }
        public string ImageShareName { get; set; }

        public string OffSigPath { get; set; }
        public string TOSigPath { get; set; }
        public int ConDocId { get; set; }
        public string ConDocPath { get; set; }

        public string OffSigFullPath
        {
            get { return GetFullPath(ImageMachineName, ImageShareName, OffSigPath); }
        }

        public string TOSigFullPath
        {
            get { return GetFullPath(ImageMachineName, ImageShareName, TOSigPath); }
        }

        public string ConDocFullPath
        {
            get { return GetFullPath(ImageMachineName, ImageShareName, ConDocPath); }
        }

        string GetFullPath(string path1, string path2, string path3)
        {
            return !string.IsNullOrWhiteSpace(path1)
                && !string.IsNullOrWhiteSpace(path2)
                && !string.IsNullOrWhiteSpace(path3)
                ? string.Format(@"\\{0}\{1}\{2}", path1, path2, path3) : string.Empty;
        }

        #endregion

        #endregion

        string StringJoin(string separator, params string[] values)
        {
            return string.Join(separator, values.Where(v => !string.IsNullOrWhiteSpace(v)));
        }
    }

    public class MobileS56Charge
    {
        public int ChgIntNo { get; set; }
        public bool ChgIsMain { get; set; }
        public int? MainChargeID { get; set; }
        public int ChgSequence { get; set; }

        public string ChgStatutoryRef { get; set; }
        public string ChgOffenceCode { get; set; }
        public string ChgOffenceDescr { get; set; }
        public string ChgFineAmount { get; set; }
    }
}
