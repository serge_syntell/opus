﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;


using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using SIL.AARTO.BLL.EntLib;
namespace SIL.AARTO.BLL.Utility
{
    public class AARTOErrorLog : ActionFilterAttribute
    {
        private AartoErrorLogService errorLogService = new AartoErrorLogService();
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // 2014-10-15, Oscar added ViewBag.Exception
            var viewBag = filterContext.Controller.ViewBag;
            var hasViewBagException = viewBag != null && viewBag.Exception != null && viewBag.Exception is Exception;

            if (filterContext.Exception != null || hasViewBagException)
            {
                Exception ex = !hasViewBagException ? filterContext.Exception : viewBag.Exception;
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
                filterContext.HttpContext.Session["ErrorMessage"] = ex.Message;
                try
                {
                    EntLibLogger.WriteErrorLog(ex,LogCategory.Error, AartoProjectList.AARTOWebApplication.ToString());

                    AartoErrorLog errorlog = new AartoErrorLog();
                    errorlog.AaErLogDate = DateTime.Now;
                    string logName = filterContext.Controller.ToString() + "/" + filterContext.ActionDescriptor.ActionName;
                    errorlog.AaErLogName = logName.Substring(0, logName.Length > 50 ? 50 : logName.Length);
                    errorlog.AaErLogDescription = ex.Message + ex.StackTrace;
                    errorlog.AaErLogPriority = 5;
                    errorlog.LastUser = Session.UserName;
                    errorlog.AaProjectId = (int)AartoProjectList.AARTOWebApplication;
                    errorLogService.Save(errorlog);

                }
                catch (Exception e)
                {
                    EntLibLogger.WriteErrorLog(e, LogCategory.Error, AartoProjectList.AARTOWebApplication.ToString());
                }

            }
        }
    }
}
