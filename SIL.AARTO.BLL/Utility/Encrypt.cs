﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Security.Cryptography;



namespace SIL.AARTO.BLL.Utility
{
    public class Encrypt
    {
        #region Password Util
        /// <summary>
        /// Encrypts a string using the SHA256 algorithm.
        /// </summary>
        public static string HashPassword(string plainMessage)
        {
            byte[] data = Encoding.UTF8.GetBytes(plainMessage);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                byte[] encryptedBytes = sha.TransformFinalBlock(data, 0, data.Length);
                return Convert.ToBase64String(sha.Hash);
            }
        }
        #endregion

        #region Encrypt MD5
        /// <summary>
        /// Encrypt using the MD5 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string MD5(string value)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(value, "MD5");
        }

        #endregion

        #region Encrypt RC4

        /// <summary>
        /// Encrypt using RC4
        /// </summary>
        /// <param name="value">input value</param>
        /// <returns></returns>
        //public static string RC4Encrypt(string value)
        //{
        //    Rc4Encrypt.clsRc4Encrypt clsRc4Encrypt = new clsRc4Encrypt();
        //    clsRc4Encrypt.Password = "sacompconsult";
        //    clsRc4Encrypt.PlainText = value;

        //    return clsRc4Encrypt.EnDeCrypt();
        //}

        #endregion
    }
}
