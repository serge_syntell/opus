﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.DMS.DAL.Entities;
using SIL.DMS.DAL.Services;
namespace SIL.AARTO.BLL.Utility.DMS
{
    public class BatchDocumentEntity
    {
        /// <summary>
        /// BatchDocument infomation
        /// </summary>
        public long BaDoId { get; set; }
        public bool BaDoIsLocked { get; set; }
        public int BaDoStId { get; set; }
        public int BaDoOrder { get; set; }

        public List<BatchDocumentImage> BatchDocumentImageList { get; set; }

        public long BatchId { get; set; }

        /// <summary>
        /// Document template infomation
        /// </summary>            
        public int DoTeId { get; set; }
        public string DoTeName { get; set; }
        public bool DoTeIsFixedPageNumber { get; set; }
        public bool DoTeIsDuplex { get; set; }
        public string DoTeDescription { get; set; }

        public List<DocumentTemplateDetail> DocumentTemplateDetailList { get; set; }


    }
}
