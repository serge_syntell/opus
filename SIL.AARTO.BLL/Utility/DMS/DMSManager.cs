﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.DMS.DAL.Entities;
using SIL.DMS.DAL.Services;
using System.Data;
namespace SIL.AARTO.BLL.Utility.DMS
{
    /// <summary>
    /// Author:Lucas Li
    /// </summary>
    public class DMSManager
    {
        public static BatchDocumentEntity GetTopOneBatchDocumentInfomationByTemplateTypeAndStatus(int documentTemplateID,int userID)
        {
            //IDataReader idr = null;
            //BatchDocumentEntity bde = new BatchDocumentEntity();
            //bde.DocumentTemplateDetailList = new List<DocumentTemplateDetail>();
            //DocumentTemplateDetail dtd = null;
            //bde.BatchDocumentImageList = new List<BatchDocumentImage>();
            //BatchDocumentImage bdi = null;
            //idr null;//= new BatchDocumentService().GetTopOneBatchDocumentByTypeAndStatus(documentTemplateID, (int)BatchDocumentStatusList.Scanned, (int)BatchDocumentStatusList.Processing, userID, 24);

            //int retStatus = -1;
            //while (idr.Read())
            //{
            //    retStatus = Convert.ToInt32(idr[0]);
            //    break;
            //}
            //if (retStatus == 1)
            //{
            //    idr.NextResult();
            //    while (idr.Read())
            //    {
            //        bde.BaDoId = idr["BaDoId"] == DBNull.Value ? 0 : Convert.ToInt64(idr["BaDoId"]);
            //        //bde.BaDoIsLocked = idr["BaDoIsLocked"] == DBNull.Value ? true : idr["BaDoIsLocked"].ToString() == "True";
            //        bde.BaDoOrder = idr["BaDoOrder"] == DBNull.Value ? 0 : Convert.ToInt32(idr["BaDoOrder"]);
            //        bde.BaDoStId = idr["BaDoStId"] == DBNull.Value ? 0 : Convert.ToInt32(idr["BaDoStId"]);
            //        bde.BatchId = idr["BatchId"] == DBNull.Value ? 0 : Convert.ToInt64(idr["BatchId"]);
            //        bde.DoTeDescription = idr["DoTeDescription"].ToString();
            //        bde.DoTeId = idr["DoTeId"] == DBNull.Value ? 0 : Convert.ToInt32(idr["DoTeId"]);
            //        bde.DoTeIsDuplex = idr["DoTeIsDuplex"] == DBNull.Value ? true : idr["DoTeIsDuplex"].ToString() == "True";
            //        bde.DoTeIsFixedPageNumber = idr["DoTeIsFixedPageNumber"] == DBNull.Value ? true : idr["DoTeIsFixedPageNumber"].ToString() == "True";
            //        bde.DoTeName = idr["DoTeName"].ToString();
            //        break;
            //    }                
            //    idr.NextResult();
            //    while (idr.Read())
            //    {
            //        bdi = new BatchDocumentImage();
            //        bdi.BaDoImFilePath =idr["BaDoImFilePath"].ToString();
            //        //bdi.BaDoImId = idr["BaDoImId"] == DBNull.Value ? 0 : Convert.ToInt64(idr["BaDoImId"]);
            //        //bdi.BaDoId = idr["BaDoId"] == DBNull.Value ? 0 : Convert.ToInt64(idr["BaDoId"]);
            //        bdi.BaDoImOrder = idr["BaDoImOrder"] == DBNull.Value ? 0 : Convert.ToInt32(idr["BaDoImOrder"]);
            //        bde.BatchDocumentImageList.Add(bdi);
            //    }
            //    idr.NextResult();
            //    while (idr.Read())
            //    {
            //        dtd = new DocumentTemplateDetail();
            //        //dtd.DoTeDeId = idr["DoTeDeId"] == DBNull.Value ? 0 : Convert.ToInt32(idr["DoTeDeId"]);
            //        //dtd.DoTeId = idr["DoTeId"] == DBNull.Value ? 0 : Convert.ToInt32(idr["DoTeId"]);
            //        dtd.DoTeDeFieldName = idr["DoTeDeFieldName"].ToString();
            //        dtd.DoTeDeIsDisplay = idr["DoTeDeIsDisplay"] == DBNull.Value ? true : idr["DoTeDeIsDisplay"].ToString() == "True";
            //        dtd.DoTeDeIsOcr = idr["DoTeDeIsOcr"] == DBNull.Value ? true : idr["DoTeDeIsOcr"].ToString() == "True";
            //        dtd.DoTeDeIsBarcode = idr["DoTeDeIsBarcode"] == DBNull.Value ? true : idr["DoTeDeIsBarcode"].ToString() == "True";
            //        dtd.BaTyId = idr["BaTyId"] == DBNull.Value ? 0 : Convert.ToInt32(idr["BaTyId"]);
            //        //dtd.DoTeDeIsBarcodeInHead = idr["DoTeDeIsBarcodeInHead"] == DBNull.Value ? true : idr["DoTeDeIsBarcodeInHead"].ToString() == "True";
            //        dtd.DoTeDeRegionFromx = idr["DoTeDeRegionFromx"] == DBNull.Value ? 0 : Convert.ToInt32(idr["DoTeDeRegionFromx"]);
            //        dtd.DoTeDeRegionFromy = idr["DoTeDeRegionFromy"] == DBNull.Value ? 0 : Convert.ToInt32(idr["DoTeDeRegionFromy"]);
            //        dtd.DoTeDeRegionWidth = idr["DoTeDeRegionWidth"] == DBNull.Value ? 0 : Convert.ToInt32(idr["DoTeDeRegionWidth"]);
            //        dtd.DoTeDeRegionHeight = idr["DoTeDeRegionHeight"] == DBNull.Value ? 0 : Convert.ToInt32(idr["DoTeDeRegionHeight"]);
            //        dtd.DoTeDePageNumber = idr["DoTeDePageNumber"] == DBNull.Value ? 0 : Convert.ToInt32(idr["DoTeDePageNumber"]);
            //        //dtd.DoTeDeRegExpress = idr["DoTeDeRegExpress"].ToString();
            //        bde.DocumentTemplateDetailList.Add(dtd);
            //    }
            //}
            //else
            //{
            //    idr.Close();
            //}
            //return bde;

            return new BatchDocumentEntity();
        }
    }
}
