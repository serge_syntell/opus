﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Culture;
namespace SIL.AARTO.BLL.Utility
{
    public class TicketNumber
    {
        public static string ConnectionString = "";//2014-11-06 Heidi added for generating correct TicketNumber
        public static AlphabetNumericMappingDB anmDB =null;
        //TicketNumber tn = new TicketNumber("Data Source=Thomas;Initial Catalog=TMS;;user id=Aarto_User;password=aarto2010");
        //string ttt = tn.GenerateNotNo(22, 90, 3, "Lucas");
        //bool ss = Verhoeff.Check(ttt.Replace("-", ""));
        /// <summary>
        /// Generate notice ticket number
        /// </summary>
        /// <param name="autIntNo">authority ID</param>
        /// <param name="tTIntNo">transaction type ID</param>
        /// <param name="metroIntNo">metro ID</param>
        /// <param name="lastUser">login user name</param>
        /// <returns></returns>
        public static string GenerateNotNo(int autIntNo, string lastUser)
        {
            int tTIntNo = (int)TransactionTypeList.AARTOInfringement;
                int? metroIntNo = NoticeManager.GetMetroIDByAuthorityID(autIntNo);
                StringBuilder noticeTicketNumber = new StringBuilder();
                noticeTicketNumber.Append(GetPrefixByTransactionTypeID(tTIntNo));
                noticeTicketNumber.Append("-");
                noticeTicketNumber.Append(GetENatisAuthorityNumberByAutIntNo(autIntNo));
                noticeTicketNumber.Append("-");
                noticeTicketNumber.Append(GetSequenctialNo((int)metroIntNo,autIntNo, tTIntNo, lastUser));
                noticeTicketNumber.Append("-");
                string tmpStr = noticeTicketNumber.ToString().Replace("-", "");
                noticeTicketNumber.Append(Verhoeff.CalculateCheckDigit(tmpStr));
                return noticeTicketNumber.ToString();
            //string x = GetRandomNum(2) + "-";
            //string y = DateTime.Now.Ticks.ToString();
            //string z = y.Substring(y.Length - 14);
            //string m = z.Insert(4, "-").Insert(z.Length, "-");
            //return x + m;
        }

        private static string GetRandomNum(int ix)
        {
            string x = "";
            Random ran = new Random();
            for (int i = 0; i < ix; i++)
            {
                x = x + ran.Next(0, 9).ToString();
            }
            return x;
        }

        private static string GetENatisAuthorityNumberByAutIntNo(int autIntNo)
        {
            int autNo = -1;
            SIL.AARTO.DAL.Entities.Authority authority = new AuthorityService().GetByAutIntNo(autIntNo);
            if (authority == null) throw new Exception();
            if (SystemParameter.Get().NoticeNoUsesMetroIA == 1)
            {
                Metro metro = new MetroService().GetByMtrIntNo(authority.MtrIntNo);
                if (metro == null || metro.MtrIssuingAuthorityCode == null || metro.MtrIssuingAuthorityCode == 0)
                {
                    throw new Exception("No fundamental data.");
                }
                else autNo = (int)metro.MtrIssuingAuthorityCode;
            }
            else
            {
                if (authority.EnatisAuthorityNumber == null || authority.EnatisAuthorityNumber == 0)
                {
                    throw new Exception("No fundamental data.");
                }
                else autNo = authority.EnatisAuthorityNumber;
            }
            //SqlConnection conn = new SqlConnection(this.connectionString);
            //SqlCommand cmd = new SqlCommand("AuthorityDetail", conn);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = autIntNo;
            //conn.Open();
            //SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //while (dr.Read())
            //{
            //    if (dr["eNatisAuthorityNumber"] != DBNull.Value)
            //        autNo = Convert.ToInt32(dr["eNatisAuthorityNumber"]);
            //    break;
            //}
            //conn.Dispose();
            if (autNo != -1) return PadLeft(autNo.ToString(), 4);
            else return "";
        }
        private static string GetPrefixByTransactionTypeID(int tTIntNo)
        {
            string prefix = "";
            TransactionType tt = new TransactionTypeService().GetByTtIntNo(tTIntNo);
            if (tt == null || tt.TtPrefix == null)
            {
                prefix = "";
            }
            else prefix = tt.TtPrefix;
            //SqlConnection conn = new SqlConnection(this.connectionString);
            //SqlCommand cmd = new SqlCommand("TransactionTypeDetail", conn);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@TTIntNo", SqlDbType.Int).Value = tTIntNo;
            //conn.Open();
            //SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //while (dr.Read())
            //{
            //    prefix = dr["TTPrefix"].ToString();
            //    break;
            //}
            //conn.Dispose();
            if (prefix != "") return PadLeft(prefix, 2);
            else return "";
        }
        private static string GetSequenctialNo(int metroIntNo,int autIntNo, int tTIntNo, string lastUser)
        {
            string number = "";
            if (SystemParameter.Get().NoticeNoUsesMetroIA == 1)
            {
                System.Data.IDataReader idr = new AartoAuthorityTransactionService().GetNextNumberByType(autIntNo, tTIntNo, lastUser);
                while (idr.Read())
                {
                    number = idr[0].ToString();
                    break;
                }
                idr.Close();
            }
            else
            {
                System.Data.IDataReader idr = new MetroTransactionService().GetNextNumberByType(metroIntNo, tTIntNo, lastUser);
                while (idr.Read())
                {
                    number = idr[0].ToString();
                    break;
                }
                idr.Close();
            }
            //SqlConnection conn = new SqlConnection(this.connectionString);
            //SqlCommand cmd = new SqlCommand("MetroTransactionNextNumberByType", conn);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@MetroIntNo", SqlDbType.Int).Value = metroIntNo;
            //cmd.Parameters.Add("@TTIntNo", SqlDbType.Int).Value = tTIntNo;
            //cmd.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = tTIntNo;
            //cmd.Parameters.Add("@MTNumber", SqlDbType.Int).Value = 0;
            //cmd.Parameters[3].Direction = ParameterDirection.Output;
            //try
            //{
            //    conn.Open();
            //    cmd.ExecuteNonQuery();
            //    number = cmd.Parameters[3].Value.ToString();
            //}
            //catch (Exception ex)
            //{

            //}
            //finally
            //{
            //    conn.Dispose();
            //}
            if (number != "") return PadLeft(number, 9);
            else return "";
        }
        private static string PadLeft(string str, int len)
        {
            return str.PadLeft(len, '0');
        }

        /// <summary>
        /// jerry 2011-1-13 create for Cdv of Ticket number, the processor is TMS
        /// </summary>
        /// <param name="nSeq"></param>
        /// <param name="notPrefix"></param>
        /// <param name="autNo"></param>
        /// <returns></returns>
        public static int GetCdvOfTicketNumber(int nSeq, string notPrefix, string autNo)
        {
            int a1 = 0;
            int a2 = 0;
            int aa;
            autNo = autNo.Replace(" ", "").Replace(" ", "");

            if (ConnectionString == "")//compatible with "ConnectionString" params from windowservice
            {
                ConnectionString = Config.ConnectionString;
            }

            //2014-11-06 Heidi added for generating correct TicketNumber
            if (anmDB == null)
            {
                anmDB = new AlphabetNumericMappingDB(ConnectionString);
            }

            Dictionary<string, int> dict = anmDB.GetAllAlphabetNumericMapping();

            if (int.TryParse(notPrefix, out aa))
            {
                a1 = Convert.ToInt32(notPrefix);
            }
            else
            {
                if (!int.TryParse(notPrefix.Substring(1, 1), out aa))
                {
                    //this.lblError.Text = "The 2nd character of the prefix portion of the ticket number must be numeric";
                    return -1;
                }
                a2 = Convert.ToInt32(notPrefix.Substring(1, 1));

                //2014-11-06 Heidi comment out for generating correct TicketNumber
                //switch (notPrefix.Substring(0, 1).ToUpper())
                //{
                //    case "A":
                //        a1 = 100;
                //        break;
                //    case "B":
                //        a1 = 110;
                //        break;
                //    case "C":
                //        a1 = 120;
                //        break;
                //    case "D":
                //        a1 = 130;
                //        break;
                //    case "E":
                //        a1 = 140;
                //        break;
                //    case "F":
                //        a1 = 150;
                //        break;
                //    case "G":
                //        a1 = 160;
                //        break;
                //    case "H":
                //        a1 = 170;
                //        break;
                //}

                if (dict.ContainsKey(notPrefix.Substring(0, 1).ToUpper()))
                {
                    a1 = dict[notPrefix.Substring(0, 1).ToUpper()];
                }
            }

            //nCdv = Convert.ToInt32(txtNoticePrefix.Text.Trim()) + (2 * nSeq) + Convert.ToInt32(txtAuthCode.Text.Trim());
            //A + (2 x B) + C = D
            return a1 + a2 + (2 * nSeq) + Convert.ToInt32(autNo);
        }
    }
}
