﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using System.Transactions;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;

namespace SIL.AARTO.BLL.Utility.SecondaryOffence
{
    public class SecondaryOffenceManager
    {
        private static readonly SecondaryOffenceService service = new SecondaryOffenceService();
        private static readonly SecondaryOffenceLookupService lookupService = new SecondaryOffenceLookupService();

        public static TList<SIL.AARTO.DAL.Entities.SecondaryOffence> GetPage(int start, int pageLength, out int totalCount)
        {
            return service.GetPaged(start, pageLength, out totalCount);
        }

        public static TList<SIL.AARTO.DAL.Entities.SecondaryOffence> GetPageByOffIntNo(int offIntNo, int start, int pageLength, out int totalCount)
        {
            SecondaryOffenceQuery query = new SecondaryOffenceQuery();
            query.AppendEquals(SecondaryOffenceColumn.OffIntNo, offIntNo.ToString());
            return service.Find(query, "", start, pageLength, out totalCount);

            //return service.GetPaged(start, pageLength, out totalCount);
        }

        /// <summary>
        /// get secondary offence by Offence Group
        /// </summary>
        /// <param name="ogIntNo"></param>
        /// <param name="start"></param>
        /// <param name="pageLength"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public static TList<SIL.AARTO.DAL.Entities.SecondaryOffence> GetPageByOGIntNo(int ogIntNo, int start, int pageLength, out int totalCount)
        {
            //totalCount = 0;
            TList<SIL.AARTO.DAL.Entities.SecondaryOffence> secondaryOffenceList = new TList<DAL.Entities.SecondaryOffence>();
            TList<SIL.AARTO.DAL.Entities.Offence> offenceList = new SIL.AARTO.DAL.Services.OffenceService().GetByOgIntNo(ogIntNo);
            foreach(SIL.AARTO.DAL.Entities.Offence offence in offenceList){
                SecondaryOffenceQuery query = new SecondaryOffenceQuery();
                query.AppendEquals(SecondaryOffenceColumn.OffIntNo, offence.OffIntNo.ToString());
                secondaryOffenceList.AddRange(service.Find(query));
            }
            totalCount = secondaryOffenceList.Count;

            return secondaryOffenceList.GetRange(start * pageLength, pageLength);
        }

        public static int Add(SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffence, TList<SecondaryOffenceLookup> secondaryOffenceLookupList)
        {
            SecondaryOffenceQuery query = new SecondaryOffenceQuery();
            query.Append(SecondaryOffenceColumn.SeOfCode, secondaryOffence.SeOfCode);
            if (service.Find(query).Count > 0)
            {
                return -1;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                SetLanguageFK(secondaryOffenceLookupList, service.Save(secondaryOffence).SeOfIntNo);
                lookupService.Save(secondaryOffenceLookupList);
                scope.Complete();
                return 1;
            }
        }

        public static int Update(SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffence, TList<SecondaryOffenceLookup> secondaryOffenceLookupList, int seOfIntNo)
        {
            SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffenceOld = service.GetBySeOfIntNo(seOfIntNo);
            SecondaryOffenceQuery query = new SecondaryOffenceQuery();
            query.Append(SecondaryOffenceColumn.SeOfCode, secondaryOffence.SeOfCode);

            if (secondaryOffenceOld.SeOfCode != secondaryOffence.SeOfCode && service.Find(query).Count > 0)
            {
                return -1;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                secondaryOffence.RowVersion = secondaryOffenceOld.RowVersion;
                service.Update(secondaryOffence);
                lookupService.Save(UpdateLookupBeforProcess(secondaryOffenceLookupList, seOfIntNo));
                scope.Complete();
                return 1;
            }
        }

        public static int Delete(int seOfIntNo)
        {
            SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffence = service.GetBySeOfIntNo(seOfIntNo);
            using (TransactionScope scope = new TransactionScope())
            {
                lookupService.Delete(lookupService.GetBySeOfIntNo(seOfIntNo));
                service.Delete(secondaryOffence);
                scope.Complete();
                return 1;
            }
        }

        public static SIL.AARTO.DAL.Entities.SecondaryOffence GetByseOfIntNo(int seOfIntNo)
        {
            SIL.AARTO.DAL.Entities.SecondaryOffence secondaryOffence = service.GetBySeOfIntNo(seOfIntNo);
            secondaryOffence.SecondaryOffenceLookupCollection = lookupService.GetBySeOfIntNo(seOfIntNo);
            return secondaryOffence;
        }

        private static void SetLanguageFK(TList<SecondaryOffenceLookup> secondaryOffenceLookupList, int SeOfIntNo)
        {
            foreach (var lookup in secondaryOffenceLookupList)
            {
                lookup.SeOfIntNo = SeOfIntNo;
            }
        }

        private static TList<SecondaryOffenceLookup> UpdateLookupBeforProcess(TList<SecondaryOffenceLookup> secondaryOffenceLookupList, int seOfIntNo)
        {
            TList<SecondaryOffenceLookup> lookups = lookupService.GetBySeOfIntNo(seOfIntNo);
            foreach (var lookup in secondaryOffenceLookupList)
            {
                if (lookups.Exists(item => item.LsCode == lookup.LsCode))
                {
                    lookups.Find(item => item.LsCode == lookup.LsCode).SeOfDescription = lookup.SeOfDescription;
                }
                else if (!string.IsNullOrEmpty(lookup.SeOfDescription))
                {
                    lookups.Add(lookup);
                }
            }
            return lookups;
        }

        public static List<SecondaryOffenceLookup> GetAllByLanguage()
        {
            List<SecondaryOffenceLookup> secondaryOffenceLookupList = new List<SecondaryOffenceLookup>();
            Dictionary<int, string> allLangSecondaryOffence = SecondaryOffenceLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            TList<SIL.AARTO.DAL.Entities.SecondaryOffence> allSecondaryOffence = service.GetAll();
            int seOfIntNo;

            foreach (var secondaryOffence in allSecondaryOffence)
            {
                seOfIntNo = secondaryOffence.SeOfIntNo;
                if (allLangSecondaryOffence.Keys.Contains(seOfIntNo))
                {
                    secondaryOffenceLookupList.Add(lookupService.GetBySeOfIntNoLsCode(seOfIntNo, Thread.CurrentThread.CurrentCulture.Name));
                }
                //else
                //{
                //    secondaryOffenceLookupList.Add(lookupService.GetBySeOfIntNoLsCode(seOfIntNo, Thread.CurrentThread.CurrentCulture.Name));
                //}
            }

            return secondaryOffenceLookupList;
        }
    }
}
