﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace SIL.AARTO.BLL.Utility
{
    public class AARTOPagelistLookupDB
    {
        public static DataSet GetListByAaPageID(int AaPageID)
        {
            DataSet ds = new DataSet();

            string connStr = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString();

            SqlConnection myConnection = new SqlConnection(connStr);
            SqlCommand myCommand = new SqlCommand("AARTOPageListLookupGetListByAaPageID", myConnection);
            
            myCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterAaPageID = new SqlParameter("@AaPageID", SqlDbType.Int, 4);
            parameterAaPageID.Value = AaPageID;
            myCommand.Parameters.Add(parameterAaPageID);
            
            SqlDataAdapter sda = new SqlDataAdapter(myCommand);
            try
            {
                myConnection.Open();
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                myConnection.Close();
                myCommand.Dispose();
                sda.Dispose();
            }
            return ds;
        }

    }
}
