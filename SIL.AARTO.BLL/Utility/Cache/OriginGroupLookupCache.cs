﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class OriginGroupLookupCache : AARTOCache
    {
        public const string CacheKey = "OriginGroupCacheKey";
        public static TList<OriginGroupLookup> GetAll()
        {
            return AARTOCache.GetCacheData("OriginGroupLookup", "OriginGroupLookup") as TList<OriginGroupLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<OriginGroupLookup> lookups = GetAll();
                foreach (OriginGroupLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.OgIntNo, item.OgDescr);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.OgIntNo, item.OgDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

