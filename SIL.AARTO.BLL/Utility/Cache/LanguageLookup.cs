﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.BLL.Admin;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public static class LanguageLookup
    {
        public static Dictionary<string, string> GetCacheLookupValue(string lsCode, string cacheKey, List<LanguageLookupEntity> lookups)
        {
            Dictionary<string, string> cacheLanguage = AARTOCache.Cache[cacheKey + lsCode] as Dictionary<string, string>;
            Dictionary<string, string> enLanguage = new Dictionary<string, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<string, string>();
                foreach (LanguageLookupEntity item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.LookUpId, item.LookupValue);
                    }
                    if (item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.LookUpId, item.LookupValue);
                    }
                }

                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}
