﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOPageListCache : AARTOCache
    {
        public TList<AartoPageList> GetAll()
        {
            className = "AartoPageList";
            tableName = "AARTOPageList";

            return AARTOCache.GetCacheData(className, tableName) as TList<AartoPageList>;
        }
    }
}
