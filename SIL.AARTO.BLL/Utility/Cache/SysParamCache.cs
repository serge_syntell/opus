﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using System.Data.SqlClient;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class SysParamCache
    {
        public static List<SysParamDetails> GetAllSysParamDetails()
        {
            if (AARTOCache.Cache["SysParamDetails"] == null)
            {
                List<SysParamDetails> sysParamList = new List<SysParamDetails>();
                SysParamDB sysParam = new SysParamDB(Config.ConnectionString);
                using (SqlDataReader reader = sysParam.GetSysParamList())
                {
                    while (reader.Read())
                    {
                        SysParamDetails spDetails = new SysParamDetails();
                        spDetails.SPIntNo = Convert.ToInt32(reader["SPIntNo"]);
                        spDetails.SPColumnName = reader["SPColumnName"].ToString();
                        spDetails.SPIntegerValue = Convert.ToInt32(reader["SPIntegerValue"]);
                        spDetails.SPStringValue = reader["SPStringValue"].ToString();
                        sysParamList.Add(spDetails);
                    }
                }
                return sysParamList;
            }
            else
            {
                return (List<SysParamDetails>)AARTOCache.Cache["SysParamDetails"];
            }
        }


    }
}
