﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class RejectCategoryLookupCache : AARTOCache
    {
        public const string CacheKey = "RejectCategoryCacheKey";
        public static TList<RejectCategoryLookup> GetAll()
        {
            return AARTOCache.GetCacheData("RejectCategoryLookup", "RejectCategoryLookup") as TList<RejectCategoryLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<RejectCategoryLookup> lookups = GetAll();
                foreach (RejectCategoryLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.RejCintNo, item.RejCcategory);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.RejCintNo, item.RejCcategory);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

