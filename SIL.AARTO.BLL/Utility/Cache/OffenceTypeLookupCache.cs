﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class OffenceTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "OffenceTypeCacheKey";
        public static TList<OffenceTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("OffenceTypeLookup", "OffenceTypeLookup") as TList<OffenceTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<OffenceTypeLookup> lookups = GetAll();
                foreach (OffenceTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.OcTintNo, item.OcTdescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.OcTintNo, item.OcTdescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

