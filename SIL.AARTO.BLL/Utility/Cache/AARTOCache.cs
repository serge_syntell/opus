﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Collections;
using System.Reflection;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public abstract class AARTOCache
    {
        public static System.Web.Caching.Cache Cache = HttpContext.Current.Cache;
        protected string className;
        protected string tableName;

        protected static object GetCacheData(string className, string tableName)
        {

            Type typeEntity = Type.GetType("SIL.AARTO.DAL.Entities." + className + ",SIL.AARTO.DAL.Entities");
            Type typeService = Type.GetType("SIL.AARTO.DAL.Services." + className + "Service,SIL.AARTO.DAL.Services");
            object objEntity = Activator.CreateInstance(typeEntity);
            object objService = Activator.CreateInstance(typeService);

            object result = Cache[tableName];

            if (result == null)
            {
                string dataBaseName = "";
                ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["AARTOConnectionString"];
                string[] connectParameterArray = connectionStringSettings.ConnectionString.Split(';');
                foreach (string connectParameter in connectParameterArray)
                {
                    if (connectParameter.Substring(0, connectParameter.IndexOf("=")) == "Initial Catalog")
                    {
                        //dataBaseName = connectParameter.Substring(connectParameter.IndexOf("="));
                        dataBaseName = connectParameter.Substring(connectParameter.IndexOf("=") + 1);
                        break;
                    }
                }

                foreach (MethodInfo info in typeService.GetMethods())
                {
                    if (info.Name == "GetAll")
                    {
                        result = info.Invoke(objService, null);
                        //if (result != null)
                        //{
                        //    SqlCacheDependencyAdmin.EnableNotifications(connectionStringSettings.ConnectionString);
                        //    if (!SqlCacheDependencyAdmin.GetTablesEnabledForNotifications(connectionStringSettings.ConnectionString).Contains(tableName))
                        //    {
                        //        SqlCacheDependencyAdmin.EnableTableForNotifications(connectionStringSettings.ConnectionString, tableName);
                        //    }
                        //    SqlCacheDependency sqlCacheDependency = new SqlCacheDependency(dataBaseName, tableName);
                        //    HttpContext.Current.Cache.Insert(tableName, result, sqlCacheDependency);
                        //}
                        break;
                    }
                }

            }

            return result;

        }

        private static void OnRemoveQuotesCollection(string key, object val,
              CacheItemRemovedReason r)
        {
            // Do something about the dependency Change
            if (r == CacheItemRemovedReason.DependencyChanged)
            {
                HttpRuntime.Cache.Remove(key);
            }
        }
    }
}
