﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class CourtJudgementTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "CourtJudgementTypeCacheKey";
        public static TList<CourtJudgementTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("CourtJudgementTypeLookup", "CourtJudgementTypeLookup") as TList<CourtJudgementTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<CourtJudgementTypeLookup> lookups = GetAll();
                foreach (CourtJudgementTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.CjtIntNo, item.CjtDescription);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.CjtIntNo, item.CjtDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

