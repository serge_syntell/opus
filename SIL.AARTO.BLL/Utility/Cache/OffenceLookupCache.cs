﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class OffenceLookupCache : AARTOCache
    {
        public const string CacheKey = "OffenceCacheKey";
        public static TList<OffenceLookup> GetAll()
        {
            return AARTOCache.GetCacheData("OffenceLookup", "OffenceLookup") as TList<OffenceLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<OffenceLookup> lookups = GetAll();
                foreach (OffenceLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.OffIntNo, item.OffDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.OffIntNo, item.OffDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

