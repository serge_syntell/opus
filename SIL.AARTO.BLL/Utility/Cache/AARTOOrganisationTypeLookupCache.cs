﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOOrganisationTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "AARTOOrganisationTypeCacheKey";
        public static TList<AartoOrganisationTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("AartoOrganisationTypeLookup", "AARTOOrganisationTypeLookup") as TList<AartoOrganisationTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<AartoOrganisationTypeLookup> lookups = GetAll();
                foreach (AartoOrganisationTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.AaOrTypeId, item.AaOrTypeDescription);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.AaOrTypeId, item.AaOrTypeDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

