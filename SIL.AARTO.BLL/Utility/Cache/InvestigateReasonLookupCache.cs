﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using SIL.AES.BLL.CameraManagement.DAL;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class InvestigateReasonLookupCache : AARTOCache
    {
        public static readonly string CacheKey = "InvestigateReasonLookupCacheKey";
        public static TList<InvestigateReasonLookup> GetAll()
        {
            return AARTOCache.GetCacheData("InvestigateReasonLookup", "InvestigateReasonLookup") as TList<InvestigateReasonLookup>;
        }

        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<InvestigateReasonLookup> lookups = GetAll();
                foreach (InvestigateReasonLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.InReIntNo, item.InReason);
                    }
                    if (item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.InReIntNo, item.InReason);
                    }
                }

                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}
