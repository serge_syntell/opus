﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOPrDPCodeLookupCache : AARTOCache
    {
        public const string CacheKey = "AARTOPrDPCodeCacheKey";
        public static TList<AartoPrDpCodeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("AartoPrDpCodeLookup", "AARTOPrDPCodeLookup") as TList<AartoPrDpCodeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<AartoPrDpCodeLookup> lookups = GetAll();
                foreach (AartoPrDpCodeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.AaPrDpCodeId, item.AaPrDpCodeDescription);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.AaPrDpCodeId, item.AaPrDpCodeDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

