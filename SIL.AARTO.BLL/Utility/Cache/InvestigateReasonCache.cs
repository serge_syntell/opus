﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using SIL.AES.BLL.CameraManagement.DAL;
using SIL.AARTO.BLL.CameraManagement;
using SIL.AARTO.BLL.CameraManagement.Model;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class InvestigateReasonCache : AARTOCache
    {
        public static readonly string CacheKey = "InvestigateReasonCacheKey";
        public static TList<InvestigateReason> GetAll()
        {
            return AARTOCache.GetCacheData("InvestigateReason", "InvestigateReason") as TList<InvestigateReason>;
        }
    }
}
