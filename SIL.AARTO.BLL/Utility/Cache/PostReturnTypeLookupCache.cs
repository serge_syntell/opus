﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class PostReturnTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "PostReturnTypeCacheKey";
        public static TList<PostReturnTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("PostReturnTypeLookup", "PostReturnTypeLookup") as TList<PostReturnTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<PostReturnTypeLookup> lookups = GetAll();
                foreach (PostReturnTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.PrtIntNo, item.PrtDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.PrtIntNo, item.PrtDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

