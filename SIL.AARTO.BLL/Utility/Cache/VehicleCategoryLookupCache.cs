﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class VehicleCategoryLookupCache : AARTOCache
    {
        public const string CacheKey = "VehicleCategoryCacheKey";
        public static TList<VehicleCategoryLookup> GetAll()
        {
            return AARTOCache.GetCacheData("VehicleCategoryLookup", "VehicleCategoryLookup") as TList<VehicleCategoryLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<VehicleCategoryLookup> lookups = GetAll();
                foreach (VehicleCategoryLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.VcatIntNo, item.VcatDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.VcatIntNo, item.VcatDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

