﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class RegionLookupCache : AARTOCache
    {
        public const string CacheKey = "RegionCacheKey";
        public static TList<RegionLookup> GetAll()
        {
            return AARTOCache.GetCacheData("RegionLookup", "RegionLookup") as TList<RegionLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<RegionLookup> lookups = GetAll();
                foreach (RegionLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.RegIntNo, item.RegName);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.RegIntNo, item.RegName);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

