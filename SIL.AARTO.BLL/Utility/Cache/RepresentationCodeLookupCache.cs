﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class RepresentationCodeLookupCache : AARTOCache
    {
        public const string CacheKey = "RepresentationCodeCacheKey";
        public static TList<RepresentationCodeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("RepresentationCodeLookup", "RepresentationCodeLookup") as TList<RepresentationCodeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<RepresentationCodeLookup> lookups = GetAll();
                foreach (RepresentationCodeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.RcCode, item.RcDescr);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.RcCode, item.RcDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

