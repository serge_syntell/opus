﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOActionCache : AARTOCache
    {
        public TList<AartoAction> GetAll()
        {
            className = "AartoAction";
            tableName = "AARTOAction";

            return AARTOCache.GetCacheData(className, tableName) as TList<AartoAction>;
        }
    }
}
