﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class FilmLoadStatusLookupCache : AARTOCache
    {
        public const string CacheKey = "FilmLoadStatusCacheKey";
        public static TList<FilmLoadStatusLookup> GetAll()
        {
            return AARTOCache.GetCacheData("FilmLoadStatusLookup", "FilmLoadStatusLookup") as TList<FilmLoadStatusLookup>;
        }
        
        public static Dictionary<short, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<short, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<short, string>;
            Dictionary<short, string> enLanguage = new Dictionary<short, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<short, string>();
                TList<FilmLoadStatusLookup> lookups = GetAll();
                foreach (FilmLoadStatusLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.FlsStatus, item.FlsDescription);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.FlsStatus, item.FlsDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

