﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class DocumentTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "DocumentTypeCacheKey";
        public static TList<DocumentTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("DocumentTypeLookup", "DocumentTypeLookup") as TList<DocumentTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<DocumentTypeLookup> lookups = GetAll();
                foreach (DocumentTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.DocTypeId, item.DocDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.DocTypeId, item.DocDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

