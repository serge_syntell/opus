﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class MetroLookupCache : AARTOCache
    {
        public const string CacheKey = "MetroCacheKey";
        public static TList<MetroLookup> GetAll()
        {
            return AARTOCache.GetCacheData("MetroLookup", "MetroLookup") as TList<MetroLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<MetroLookup> lookups = GetAll();
                foreach (MetroLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.MtrIntNo, item.MtrName);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.MtrIntNo, item.MtrName);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

