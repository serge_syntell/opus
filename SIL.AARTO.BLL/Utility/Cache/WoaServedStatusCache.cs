﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class WoaServedStatusCache : AARTOCache
    {
        public TList<WoaServedStatus> GetAll()
        {
            className = "WoaServedStatus";
            tableName = "WOAServedStatus";

            return GetCacheData(className, tableName) as TList<WoaServedStatus>;

        }
    }
}
