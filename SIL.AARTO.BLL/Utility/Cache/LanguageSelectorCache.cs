﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class LanguageSelectorCache : AARTOCache
    {
        private static LanguageSelectorService lanSelService = new LanguageSelectorService();
        public static TList<LanguageSelector> GetAll()
        {
            return AARTOCache.GetCacheData("LanguageSelector", "LanguageSelector") as TList<LanguageSelector>;
        }

        public static LanguageSelector GetDefaultLanguageSelector()
        {
            if (AARTOCache.Cache["default_language"] == null)
	        {
                AARTOCache.Cache["default_language"] = lanSelService.GetByLsIsDefault(true)[0];
	        }
            return (LanguageSelector)AARTOCache.Cache["default_language"];
        }

        public static LanguageSelector GetLanguageSelectorByLSCode(string lsCode)
        {
            LanguageSelector selector = new LanguageSelector();
            foreach (var item in GetAll())
            {
                if (item.LsCode == lsCode)
                {
                    selector = item;
                }
            }
            return selector;
        }
    }
}
