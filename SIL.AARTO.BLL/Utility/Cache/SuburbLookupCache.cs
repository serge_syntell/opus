﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class SuburbLookupCache : AARTOCache
    {
        public const string CacheKey = "SuburbCacheKey";
        public static TList<SuburbLookup> GetAll()
        {
            return AARTOCache.GetCacheData("SuburbLookup", "SuburbLookup") as TList<SuburbLookup>;
        }
        
        public static Dictionary<string, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<string, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<string, string>;
            Dictionary<string, string> enLanguage = new Dictionary<string, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<string, string>();
                TList<SuburbLookup> lookups = GetAll();
                foreach (SuburbLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.SubIntNo.ToString(), item.SubDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.SubIntNo.ToString(), item.SubDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

