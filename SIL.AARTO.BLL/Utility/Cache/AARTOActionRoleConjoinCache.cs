﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOActionRoleConjoinCache:AARTOCache
    {
        public TList<AartoActionRoleConjoin> GetAll()
        {
            className = "AartoActionRoleConjoin";
            tableName = "AARTOActionRoleConjoin";

            return AARTOCache.GetCacheData(className, tableName) as TList<AartoActionRoleConjoin>;
        }
    }
}
