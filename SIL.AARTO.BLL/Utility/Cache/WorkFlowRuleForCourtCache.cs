﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class WorkFlowRuleForCourtCache : AARTOCache
    {
        public const string CacheKey = "WorkFlowRuleForCourtCacheKey";
        public static TList<WorkFlowRuleForCourt> GetAll()
        {
            return AARTOCache.GetCacheData("WorkFlowRuleForCourt", "WorkFlowRuleForCourt") as TList<WorkFlowRuleForCourt>;
        }
    }
}
