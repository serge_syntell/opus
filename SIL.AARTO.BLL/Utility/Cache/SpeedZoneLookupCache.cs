﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class SpeedZoneLookupCache : AARTOCache
    {
        public const string CacheKey = "SpeedZoneCacheKey";
        public static TList<SpeedZoneLookup> GetAll()
        {
            return AARTOCache.GetCacheData("SpeedZoneLookup", "SpeedZoneLookup") as TList<SpeedZoneLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<SpeedZoneLookup> lookups = GetAll();
                foreach (SpeedZoneLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.SzIntNo, item.SzDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.SzIntNo, item.SzDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

