﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class LocationLookupCache : AARTOCache
    {
        public const string CacheKey = "LocationCacheKey";
        public static TList<LocationLookup> GetAll()
        {
            return AARTOCache.GetCacheData("LocationLookup", "LocationLookup") as TList<LocationLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<LocationLookup> lookups = GetAll();
                foreach (LocationLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.LocIntNo, item.LocDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.LocIntNo, item.LocDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

