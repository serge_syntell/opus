﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class OffenderTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "OffenderTypeCacheKey";
        public static TList<OffenderTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("OffenderTypeLookup", "OffenderTypeLookup") as TList<OffenderTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<OffenderTypeLookup> lookups = GetAll();
                foreach (OffenderTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.OtIntNo, item.OtDescription);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.OtIntNo, item.OtDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

