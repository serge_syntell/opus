﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class NoticeTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "NoticeTypeCacheKey";
        public static TList<NoticeTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("NoticeTypeLookup", "NoticeTypeLookup") as TList<NoticeTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<NoticeTypeLookup> lookups = GetAll();
                foreach (NoticeTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.NtIntNo, item.NtDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.NtIntNo, item.NtDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

