﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{

    // Jake 2013-08-28 added CourtRoom cache interface
    public class CourtRoomLookupCache : AARTOCache
    {
        public const string CacheKey = "CourtRoomCacheKey";
        public static TList<CourtRoomLookup> GetAll()
        {
            return AARTOCache.GetCacheData("CourtRoomLookup", "CourtRoomLookup") as TList<CourtRoomLookup>;
        }

        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<CourtRoomLookup> lookups = GetAll();
                foreach (CourtRoomLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.CrtRintNo, item.CrtRoomName);
                    }
                    if (item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.CrtRintNo, item.CrtRoomName);
                    }
                }

                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }

    public class CourtRoomCache : AARTOCache
    {
        public const string CacheKey = "CourtRoom_CacheKey";

        public static TList<CourtRoom> GetAll()
        {
            return AARTOCache.GetCacheData("CourtRoom", "CourtRoom") as TList<CourtRoom>;
        }

        public static List<CourtRoom> GetAvailableCourtRoom(int crtIntNo)
        {
            List<CourtRoom> courtRooms = new List<CourtRoom>();
            //List<CourtRoom> selectedCourtRooms = GetAll().Where(c => c.CrtIntNo.Equals(crtIntNo) ).OrderBy(c => c.CrtRoomNo).ToList();
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            List<CourtRoom> CourtRoomlist = GetAll().ToList();
            var search = from p in CourtRoomlist where p.CrtIntNo.Equals(crtIntNo) && (p.CrtRstatusFlag.ToUpper().Equals("C") || p.CrtRstatusFlag.ToUpper().Equals("P") ) select p;
            List<CourtRoom> selectedCourtRooms = search.OrderBy(c => c.CrtRoomNo).ToList();

            // Jake opend 2013-10-17 show IsActive =Y
            // removed check CrtASctive equal to Y
            // We need to show all court rooms on WOA page according court
            //foreach (CourtRoom cr in selectedCourtRooms)
            //{
            //    if (!String.IsNullOrEmpty(cr.CrtRactive) && cr.CrtRactive.ToUpper().Trim() == "Y")
            //    {
            //        courtRooms.Add(cr);
            //    }
            //}
            return selectedCourtRooms;
        }
    }

    
}

