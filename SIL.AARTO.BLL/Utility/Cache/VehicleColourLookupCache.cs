﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class VehicleColourLookupCache : AARTOCache
    {
        public const string CacheKey = "VehicleColourCacheKey";
        public static TList<VehicleColourLookup> GetAll()
        {
            return AARTOCache.GetCacheData("VehicleColourLookup", "VehicleColourLookup") as TList<VehicleColourLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<VehicleColourLookup> lookups = GetAll();
                foreach (VehicleColourLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.VcIntNo, item.VcDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.VcIntNo, item.VcDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

