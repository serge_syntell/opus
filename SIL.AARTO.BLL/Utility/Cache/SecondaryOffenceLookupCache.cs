﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class SecondaryOffenceLookupCache : AARTOCache
    {
        public const string CacheKey = "SecondaryOffenceCacheKey";
        public static TList<SecondaryOffenceLookup> GetAll()
        {
            return AARTOCache.GetCacheData("SecondaryOffenceLookup", "SecondaryOffenceLookup") as TList<SecondaryOffenceLookup>;
        }

        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<SecondaryOffenceLookup> lookups = GetAll();
                foreach (SecondaryOffenceLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.SeOfIntNo, item.SeOfDescription);
                    }
                    if (item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.SeOfIntNo, item.SeOfDescription);
                    }
                }

                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}
