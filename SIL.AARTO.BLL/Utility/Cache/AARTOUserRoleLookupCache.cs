﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOUserRoleLookupCache : AARTOCache
    {
        public const string CacheKey = "AARTOUserRoleCacheKey";
        public static TList<AartoUserRoleLookup> GetAll()
        {
            return AARTOCache.GetCacheData("AartoUserRoleLookup", "AARTOUserRoleLookup") as TList<AartoUserRoleLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<AartoUserRoleLookup> lookups = GetAll();
                foreach (AartoUserRoleLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.AaUserRoleId, item.AaUserRoleDescription);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.AaUserRoleId, item.AaUserRoleDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

