﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOOfficerErrorLookupCache : AARTOCache
    {
        public const string CacheKey = "AARTOOfficerErrorCacheKey";
        public static TList<AartoOfficerErrorLookup> GetAll()
        {
            return AARTOCache.GetCacheData("AartoOfficerErrorLookup", "AARTOOfficerErrorLookup") as TList<AartoOfficerErrorLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<AartoOfficerErrorLookup> lookups = GetAll();
                foreach (AartoOfficerErrorLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.AaOfErId, item.AaOfErDescription);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.AaOfErId, item.AaOfErDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

