﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class SysParamLookupCache : AARTOCache
    {
        public const string CacheKey = "SysParamCacheKey";
        public static TList<SysParamLookup> GetAll()
        {
            return AARTOCache.GetCacheData("SysParamLookup", "SysParamLookup") as TList<SysParamLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<SysParamLookup> lookups = GetAll();
                foreach (SysParamLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.SpIntNo, item.SpDescr);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.SpIntNo, item.SpDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

