﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class TransactionTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "TransactionTypeCacheKey";
        public static TList<TransactionTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("TransactionTypeLookup", "TransactionTypeLookup") as TList<TransactionTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<TransactionTypeLookup> lookups = GetAll();
                foreach (TransactionTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.TtIntNo, item.TtDescription);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.TtIntNo, item.TtDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

