﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class VehicleTypeGroupLookupCache : AARTOCache
    {
        public const string CacheKey = "VehicleTypeGroupCacheKey";
        public static TList<VehicleTypeGroupLookup> GetAll()
        {
            return AARTOCache.GetCacheData("VehicleTypeGroupLookup", "VehicleTypeGroupLookup") as TList<VehicleTypeGroupLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<VehicleTypeGroupLookup> lookups = GetAll();
                foreach (VehicleTypeGroupLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.VtgIntNo, item.VtgDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.VtgIntNo, item.VtgDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

