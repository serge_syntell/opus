﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class TranNoLookupCache : AARTOCache
    {
        public const string CacheKey = "TranNoCacheKey";
        public static TList<TranNoLookup> GetAll()
        {
            return AARTOCache.GetCacheData("TranNoLookup", "TranNoLookup") as TList<TranNoLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<TranNoLookup> lookups = GetAll();
                foreach (TranNoLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.TnIntNo, item.TnDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.TnIntNo, item.TnDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

