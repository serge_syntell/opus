﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class RoadTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "RoadTypeCacheKey";
        public static TList<RoadTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("RoadTypeLookup", "RoadTypeLookup") as TList<RoadTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<RoadTypeLookup> lookups = GetAll();
                foreach (RoadTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.RdTintNo, item.RdTypeDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.RdTintNo, item.RdTypeDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

