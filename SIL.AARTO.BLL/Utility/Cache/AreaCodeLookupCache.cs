﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AreaCodeLookupCache : AARTOCache
    {
        public const string CacheKey = "AreaCodeCacheKey";
        public static TList<AreaCodeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("AreaCodeLookup", "AreaCodeLookup") as TList<AreaCodeLookup>;
        }
        
        public static Dictionary<string, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<string, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<string, string>;
            Dictionary<string, string> enLanguage = new Dictionary<string, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<string, string>();
                TList<AreaCodeLookup> lookups = GetAll();
                foreach (AreaCodeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.AreaCode, item.AreaDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.AreaCode, item.AreaDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

