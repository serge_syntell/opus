﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class DateRuleLookupCache : AARTOCache
    {
        public const string CacheKey = "DateRuleCacheKey";
        public static TList<DateRuleLookup> GetAll()
        {
            return AARTOCache.GetCacheData("DateRuleLookup", "DateRuleLookup") as TList<DateRuleLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<DateRuleLookup> lookups = GetAll();
                foreach (DateRuleLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.Drid, item.DtRdescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.Drid, item.DtRdescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

