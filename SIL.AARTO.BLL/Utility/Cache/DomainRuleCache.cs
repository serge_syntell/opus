﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stalberg.TMS;
using SIL.AARTO.BLL.Culture;
using System.Data.SqlClient;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class DomainRuleCache
    {
        public static List<DomainRuleDetails> GetAllDomainRules()
        {
            if (AARTOCache.Cache["DomainRules"] == null)
            {
                List<DomainRuleDetails> domainDetailList = new List<DomainRuleDetails>();
                DomainRuleDB domainRule = new DomainRuleDB(Config.ConnectionString);
                using (SqlDataReader domainDetails = domainRule.GetDomainsList())
                {
                    while (domainDetails.Read())
                    {
                        DomainRuleDetails domDetails = new DomainRuleDetails();
                        domDetails.DomainURL = domainDetails["DomainURL"].ToString();
                        domDetails.DRLoginReq = domainDetails["DRLoginReq"].ToString();
                        domDetails.DREmailReq = domainDetails["DREmailReq"].ToString();
                        domDetails.DRPasswdReq = domainDetails["DRPasswdReq"].ToString();
                        domDetails.DRBackground = domainDetails["DRBackground"].ToString();
                        domDetails.DRStylesheet = domainDetails["DRStyleSheet"].ToString();
                        domDetails.DRTitle = domainDetails["DRTitle"].ToString();
                        domDetails.DRDescription = domainDetails["DRDescription"].ToString();
                        domDetails.DRKeywords = domainDetails["DRKeywords"].ToString();
                        domDetails.DRMainImage = domainDetails["DRMainImage"].ToString();
                        domDetails.DRAuthorityReq = domainDetails["drAuthorityReq"].ToString();
                        domainDetailList.Add(domDetails);
                    }
                }
                return domainDetailList;
            }
            else
            {
                return (List<DomainRuleDetails>)AARTOCache.Cache["DomainRules"];
            }
        }

        public static Dictionary<string, DomainRuleDetails> GetAllDomainRulesDic()
        {
            Dictionary<string, DomainRuleDetails> domainDetailsDic = new Dictionary<string, DomainRuleDetails>();
            List<DomainRuleDetails> domainDetailList = GetAllDomainRules();
            foreach (var domainRul in domainDetailList)
            {
                domainDetailsDic.Add(domainRul.DomainURL, domainRul);
            }
            return domainDetailsDic;
        }
    }
}
