﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOSystemFileTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "AARTOSystemFileTypeCacheKey";
        public static TList<AartoSystemFileTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("AartoSystemFileTypeLookup", "AARTOSystemFileTypeLookup") as TList<AartoSystemFileTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<AartoSystemFileTypeLookup> lookups = GetAll();
                foreach (AartoSystemFileTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.AaSysFileTypeId, item.AaSysFileDescription);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.AaSysFileTypeId, item.AaSysFileDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

