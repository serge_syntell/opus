﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class ContractorLookupCache : AARTOCache
    {
        public const string CacheKey = "ContractorCacheKey";
        public static TList<ContractorLookup> GetAll()
        {
            return AARTOCache.GetCacheData("ContractorLookup", "ContractorLookup") as TList<ContractorLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<ContractorLookup> lookups = GetAll();
                foreach (ContractorLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.ConIntNo, item.ConName);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.ConIntNo, item.ConName);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

