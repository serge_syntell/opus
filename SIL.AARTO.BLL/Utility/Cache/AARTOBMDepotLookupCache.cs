﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOBMDepotLookupCache : AARTOCache
    {
        public const string CacheKey = "AARTOBMDepotCacheKey";
        public static TList<AartobmDepotLookup> GetAll()
        {
            return AARTOCache.GetCacheData("AartobmDepotLookup", "AARTOBMDepotLookup") as TList<AartobmDepotLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<AartobmDepotLookup> lookups = GetAll();
                foreach (AartobmDepotLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.AaBmDepotId, item.AaBmDepotDescription);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.AaBmDepotId, item.AaBmDepotDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

