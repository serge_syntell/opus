﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIL.AARTO.DAL.Entities;

using SIL.AARTO.DAL.Services;
using System.Web.UI.WebControls;
using SIL.AARTO.BLL.Utility.UserMenu;
using System.Web;
using System.Web.Caching;
using System.Configuration;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOMenuLookUpCache : AARTOCache
    {
        //public readonly string className="AartoMenu";
        //public readonly string tableName = "AARTOMenu";

        public TList<AartoMenuLookup> GetAll()
        {
            className = "AartoMenuLookup";
            tableName = "AARTOMenuLookup";

            return GetCacheData(className, tableName) as TList<AartoMenuLookup>;

        }
    }

    public class AARTOMenuCache : AARTOCache
    {
        public AARTOMenuCache()
        {
            className = "GenerateUserMenuByLanguageAndLevel";
            tableName = "AARTOMenu";
        }
        private static AartoMenuService menuService = new AartoMenuService();
        public TList<AartoMenu> GetAll()
        {
            className = "AartoMenu";
            tableName = "AARTOMenu";

            return GetCacheData(className, tableName) as TList<AartoMenu>;

        }

        public UserMenuEntityCollection GetUserMenuFromDB(string language, int level)
        {
            UserMenuEntityCollection returnCollection = new UserMenuEntityCollection();
            UserMenuEntityCollection containerCollection = new UserMenuEntityCollection();
            UserMenuEntityCollection currentCollection = new UserMenuEntityCollection();
            //int currentLevel = 0;
            int oldlevel = 2;

            using (IDataReader reader = menuService.GenerateUserMenuByLanguageAndLevel(language, level))
            {
                while (reader.Read())
                {
                    UserMenuEntity menu = GetUserMenuEntityFromDataReader(reader);

                    if (menu.Level == 1)
                    {
                        returnCollection.Add(menu);
                        containerCollection.Add(menu);
                    }
                    else
                    {
                        if (oldlevel != menu.Level)
                        {
                            containerCollection.Clear();

                            foreach (UserMenuEntity menuEntity in currentCollection)
                            {
                                containerCollection.Add(menuEntity);
                            }
                            currentCollection.Clear();
                            oldlevel = menu.Level;
                        }

                        currentCollection.Add(menu);

                        CreateUserMenu(containerCollection, menu);
                    }
                }
            }
            return returnCollection;
        }

        public UserMenuEntityCollection GetUserMenuCollection(string language, int level)
        {
            
            UserMenuEntityCollection returnCollection = HttpContext.Current.Cache[className] as UserMenuEntityCollection;
            if (returnCollection != null)
            {
                return returnCollection;
            }
            returnCollection = GetUserMenuFromDB(language, level);
             
            if (!SqlCacheDependencyAdmin.GetTablesEnabledForNotifications(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString).Contains(tableName))
            {
                SqlCacheDependencyAdmin.EnableTableForNotifications(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString, tableName);
            }
            string dataBaseName = "";
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["AARTOConnectionString"];
            string[] connectParameterArray = connectionStringSettings.ConnectionString.Split(';');
            foreach (string connectParameter in connectParameterArray)
            {
                if (connectParameter.Substring(0, connectParameter.IndexOf("=")) == "Initial Catalog")
                {
                    //dataBaseName = connectParameter.Substring(connectParameter.IndexOf("="));
                    dataBaseName = connectParameter.Substring(connectParameter.IndexOf("=") + 1);
                    break;
                }
            }
            SqlCacheDependency sqlDependency = new SqlCacheDependency(dataBaseName, tableName);
            HttpContext.Current.Cache.Insert(className, returnCollection, sqlDependency);
            
            return returnCollection;
        }

        private void CreateUserMenu(UserMenuEntityCollection container, UserMenuEntity menu)
        {
            foreach (UserMenuEntity menuEntity in container)
            {
                if (menuEntity.AaMeID == menu.ParentAaMeID)
                {
                    menu.ParentMenuEntity = menuEntity;
                    menuEntity.UserMenuList.Add(menu);
                    break;
                }
            }
        }

        private UserMenuEntity GetUserMenuEntityFromDataReader(IDataReader reader)
        {
            UserMenuEntity userMenu = new UserMenuEntity();
            userMenu.AaMeID = Convert.ToDecimal(reader["AaMeID"]);
            userMenu.ParentAaMeID = reader["AaParentMeIDMeID"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["AaParentMeIDMeID"]);
            userMenu.AMLMenuItemName = reader["AaMLMenuItemName"].ToString();
            userMenu.PageID = reader["AaPageID"].ToString();
            userMenu.UserRole = reader["AaUserRoleID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AaUserRoleID"]);
            userMenu.Level = Convert.ToInt32(reader["Level"]);
            userMenu.AaMeOrderNo = Convert.ToInt32(reader["AaMeOrderNo"]);
            userMenu.IsVisable = true;
            userMenu.IsAarto = reader["IsAARTO"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsAARTO"]);
            userMenu.AaMePageURL = reader["AaPageURL"].ToString();
            userMenu.IsNewWindow = reader["IsNewWindow"] == DBNull.Value ? false : Convert.ToBoolean(reader["IsNewWindow"]);
            return userMenu;
        }

    }
}
