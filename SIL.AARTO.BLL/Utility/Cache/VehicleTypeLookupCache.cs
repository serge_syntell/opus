﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class VehicleTypeLookupCache : AARTOCache
    {
        public const string CacheKey = "VehicleTypeCacheKey";
        public static TList<VehicleTypeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("VehicleTypeLookup", "VehicleTypeLookup") as TList<VehicleTypeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<VehicleTypeLookup> lookups = GetAll();
                foreach (VehicleTypeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.VtIntNo, item.VtDescr);
                    }
                    if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.VtIntNo, item.VtDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

