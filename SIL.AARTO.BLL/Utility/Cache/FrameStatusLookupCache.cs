﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class FrameStatusLookupCache : AARTOCache
    {
        public const string CacheKey = "FrameStatusCacheKey";
        public static TList<FrameStatusLookup> GetAll()
        {
            return AARTOCache.GetCacheData("FrameStatusLookup", "FrameStatusLookup") as TList<FrameStatusLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<FrameStatusLookup> lookups = GetAll();
                foreach (FrameStatusLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.FsCode, item.FsDescr);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.FsCode, item.FsDescr);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

