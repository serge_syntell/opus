﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIL.AARTO.DAL.Entities;
using System.Web;

namespace SIL.AARTO.BLL.Utility.Cache
{
    public class AARTOLearnerCodeLookupCache : AARTOCache
    {
        public const string CacheKey = "AARTOLearnerCodeCacheKey";
        public static TList<AartoLearnerCodeLookup> GetAll()
        {
            return AARTOCache.GetCacheData("AartoLearnerCodeLookup", "AARTOLearnerCodeLookup") as TList<AartoLearnerCodeLookup>;
        }
        
        public static Dictionary<int, string> GetCacheLookupValue(string lsCode)
        {
            Dictionary<int, string> cacheLanguage = HttpContext.Current.Cache[CacheKey + lsCode] as Dictionary<int, string>;
            Dictionary<int, string> enLanguage = new Dictionary<int, string>();
            if (cacheLanguage == null)
            {
                cacheLanguage = new Dictionary<int, string>();
                TList<AartoLearnerCodeLookup> lookups = GetAll();
                foreach (AartoLearnerCodeLookup item in lookups)
                {
                    if (item.LsCode == lsCode)
                    {
                        cacheLanguage.Add(item.AaLeCodeId, item.AaLeCodeDescription);
                    }
                    else if(item.LsCode == "en-US")
                    {
                        enLanguage.Add(item.AaLeCodeId, item.AaLeCodeDescription);
                    }
                }
                
                foreach (var item in enLanguage)
                {
                    if (cacheLanguage.ContainsKey(item.Key))
                    {
                        continue;
                    }
                    cacheLanguage.Add(item.Key, item.Value);
                }
            }
            return cacheLanguage;
        }
    }
}

