﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace SIL.AARTO.BLL.EntLib
{
    public abstract class EntLibExceptionHandler
    {
        public static void HandleException(Exception ex, string policy)
        {
            Boolean rethrow = false;
            try
            {
                rethrow = ExceptionPolicy.HandleException(ex, policy);
                if (rethrow)
                {
                    throw ex;
                }
            }
            catch (Exception innerEx)
            {
                string errorMsg = "An unexpected exception occured while " +
                    "calling HandleException with policy '" + policy + "'. ";
                errorMsg += Environment.NewLine + innerEx.ToString();

                EntLibLogger.LogCriticalException(errorMsg);

                throw ex;
            }
        }
    }
}
