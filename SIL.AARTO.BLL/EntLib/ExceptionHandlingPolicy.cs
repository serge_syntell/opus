﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.EntLib
{
    public struct ExceptionHandlingPolicy
    {
        public const string BllPolicy = "BLL Exception Policy";
        public const string BllRethrowPolicy = "BLL Rethrow Exception Policy";
        public const string UILayerPolicy = "UI Exception Policy";
    }
}
