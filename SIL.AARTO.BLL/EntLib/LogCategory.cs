﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.EntLib
{
    public struct LogCategory
    {
        public const string General     = "General";
        public const string Exception   = "Exception";
        public const string Critical    = "Critical";
        public const string Error       = "Error";
        public const string Warning     = "Warning";
    }
}
