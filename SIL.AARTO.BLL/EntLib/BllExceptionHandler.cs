﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIL.AARTO.BLL.EntLib
{
    public static class BllExceptionHandler
    {
        public static void HandleException(Exception ex)
        {
            EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.BllPolicy);
        }
        public static void HandleExceptionAndRethrow(Exception ex)
        {
            EntLibExceptionHandler.HandleException(ex, ExceptionHandlingPolicy.BllRethrowPolicy);
        }
    }
}
