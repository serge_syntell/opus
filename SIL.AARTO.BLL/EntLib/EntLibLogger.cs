﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Database;
using System.Diagnostics;
using System.Web;

namespace SIL.AARTO.BLL.EntLib
{
    public abstract class EntLibLogger
    {
        public static void WriteLine(string message, params object[] parameters)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories.Add(LogCategory.General);
            log.Priority = LogPriority.Normal;

            Logger.Write(log);

            System.Console.WriteLine(String.Format(message,parameters));
        }

        public static void LogCriticalException(string message,params object[] parameters)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories.Add(LogCategory.Critical);
            log.Priority = LogPriority.Highest;
            log.Severity = TraceEventType.Critical;
            log.Title = "Clock Manager Critical Exception";

            Logger.Write(log);

            System.Console.WriteLine(String.Format(message, parameters));
        }

        public static string GetServerAndDatabaseName(string connectionStr)
        {
            System.Data.SqlClient.SqlConnection tempConn = new System.Data.SqlClient.SqlConnection();
            tempConn.ConnectionString = connectionStr;

            StringBuilder sbr = new StringBuilder();
            return sbr.AppendFormat(@"Database Configurations Server:{0}; Database: {1}.", tempConn.DataSource, tempConn.Database).ToString();
        }

        public static void WriteLog(string category, string title, string message)
        {
            LogEntry log = new LogEntry();
            log.Message = message;
            log.TimeStamp = DateTime.Now;
            log.Categories.Add(category);
            if (title != null)
            {
                log.Title = title;
            }            
            switch (category)
            {
                case "Critical":
                    log.Priority = LogPriority.Highest;
                    log.Severity = TraceEventType.Critical;
                    break;
                case "Exception":
                    log.Priority = LogPriority.High;
                    log.Severity = TraceEventType.Error;
                    break;                
                case "Error":
                    log.Priority = LogPriority.High;
                    log.Severity = TraceEventType.Error;
                    break;
                case "Warning":
                    log.Priority = LogPriority.Normal;
                    log.Severity = TraceEventType.Warning;
                    break;
                case "General":                   
                default:
                    log.Priority = LogPriority.Normal;
                    log.Severity = TraceEventType.Information;
                    break;
            }
            Logger.Write(log);
        }

        /// <summary>
        /// EnterpriseLibrary log
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="processName"></param>
        public static void WriteErrorLog(Exception ex, string category, string processName)
        {
            LogEntry log = new LogEntry();
            log.Message = String.Format("Type:{0} \r\n Error Message:{1} \r\n Thrown by Method:{2}.{3} \r\n Stack Trace:{4} \r\n",
               ex.GetType().FullName,
               ex.Message,
               ex.Source.GetType().FullName,
               ex.TargetSite.Name,
               ex.StackTrace, ex.Source);

            switch (category)
            {
                case "Critical":
                    log.Priority = LogPriority.Highest;
                    log.Severity = TraceEventType.Critical;
                    break;
                case "Exception":
                    log.Priority = LogPriority.High;
                    log.Severity = TraceEventType.Error;
                    break;
                case "Error":
                    log.Priority = LogPriority.High;
                    log.Severity = TraceEventType.Error;
                    break;
                case "Warning":
                    log.Priority = LogPriority.Normal;
                    log.Severity = TraceEventType.Warning;
                    break;
                case "General":
                default:
                    log.Priority = LogPriority.Normal;
                    log.Severity = TraceEventType.Information;
                    break;
            }     

            //log.AppDomainName =appli HttpContext.Current.Request.UserHostName;
            log.ProcessName = processName;
            log.Title = processName;
            log.TimeStamp = DateTime.Now;
            //log.MachineName = HttpContext.Current.Request.UserHostAddress;
            //log.Severity = System.Diagnostics.TraceEventType.Error;
            log.Categories.Add(category);

            Logger.Write(log);
        }
    }
}
