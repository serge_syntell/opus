﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.RoadBlockSession" Codebehind="RoadBlockSession.aspx.cs" %>

<%@ Register Src="RepresentationOnTheFly.ascx" TagName="RepresentationOnTheFly" TagPrefix="uc2" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <style type="text/css">
        .style1
        {
            width: 268px;
            height: 65px;
        }
        .style2
        {
            width: 252px;
        }
        .style3
        {
            width: 83px;
        }
        .style4
        {
            width: 297px;
        }
        .NormalButton
        {
            margin-left: 23px;
        }
        .style5
        {
            height: 26px;
        }
        .style6
        {
            height: 57px;
        }
        .style7
        {
            width: 53px;
        }
        .style8
        {
            height: 26px;
            width: 53px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=background %>" topmargin="0"
    rightmargin="0">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table height="85%" cellspacing="0" width="95%" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center" style="width: 182px">
                <img style="height: 1px" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label5" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnShowAddPnl0" runat="server" Text="<%$Resources:btnShowAddPnl0.Text %>" CssClass="NormalButton"
                                    Width="135px" OnClick="btnShowAddPnl0_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                            <p style="text-align: center;">
                                <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Height="40px" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pnlErrMessage" runat="server">
                            <asp:Label ID="lblMessage" runat="server" Font-Bold="True" CssClass="NormalRed"></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="pnlSessionIDFilter" runat="server">
                            <fieldset>
                                <legend>
                                    <asp:Label ID="lblTitle" CssClass="NormalBold" runat="server" Text="<%$Resources:lblTitle.Text %>"></asp:Label></legend>
                                <table>
                                    <tr>
                                        <td rowspan="3">
                                            <p>
                                                <asp:CheckBox ID="chbOpen" Text="<%$Resources:chbOpen.Text %>" runat="server" CssClass="Normal" Checked="true" /></p>
                                            <p>
                                                <asp:CheckBox ID="chbClose" Text="<%$Resources:chbClose.Text %>" runat="server" CssClass="Normal" /></p>
                                        </td>
                                        <td rowspan="3" style="width: 100px">
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkLocation" runat="server" Text="<%$Resources:chkLocation.Text %>" AutoPostBack="True"
                                                CssClass="Normal" OnCheckedChanged="chkLocation_CheckedChanged" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLocation" runat="server" CssClass="Normal" Width="370px"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkDate" CssClass="Normal" runat="server" Text="<%$Resources:chkDate.Text %>" AutoPostBack="True"
                                                OnCheckedChanged="chkDate_CheckedChanged" />
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblStrDate" CssClass="Normal" Text="<%$Resources:lblStrDate.Text %>" runat="server"></asp:Label>
                                            <asp:TextBox ID="txtStartDate" runat="server" CssClass="Normal"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="wdcRoadCourtDate0_CalendarExtender" runat="server"
                                                Format="yyyy-MM-dd" TargetControlID="txtStartDate">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:Label ID="lblEndDate" runat="server" Text="<%$Resources:lblEndDate.Text %>" CssClass="Normal"></asp:Label>
                                            <asp:TextBox ID="txtEndDate" runat="server" CssClass="Normal"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd"
                                                TargetControlID="txtEndDate">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkSessionID" CssClass="Normal" runat="server" Text="<%$Resources:chkSessionID.Text %>"
                                                AutoPostBack="True" OnCheckedChanged="chkSessionID_CheckedChanged" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSessionID" CssClass="Normal" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnFilter" runat="server" CssClass="NormalButton" Text="<%$Resources:btnFilter.Text %>"
                                                OnClick="btnFilter_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset></asp:Panel>
                        <asp:Panel ID="pnlRoadBlockSessionList" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="grdRBSessionList" Width="100%" runat="server" AllowPaging="False"
                                            AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" ShowFooter="True"
                                            OnSelectedIndexChanged="grdRBSessionList_SelectedIndexChanged">
                                            <FooterStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundField DataField="RBSIntNo" HeaderText="RBSIntNo" Visible="False" />
                                                <asp:BoundField DataField="RBSessionID" HeaderText="<%$Resources:chkSessionID.Text %>" />
                                                <asp:BoundField DataField="LocDescr" HeaderText="<%$Resources:chkLocation.Text %>" />
                                                <asp:BoundField DataField="CrtTime" HeaderText="<%$Resources:grdRBSessionList.HeaderText2 %>" htmlEncode="False" DataFormatString="{0:yyyy-MM-dd HH:mm}" />
                                                <asp:BoundField DataField="ClsTime" HeaderText="<%$Resources:grdRBSessionList.HeaderText3 %>" htmlEncode="False" DataFormatString="{0:yyyy-MM-dd HH:mm}"/>
                                                <asp:BoundField DataField="IndcFlag" HeaderText="<%$Resources:grdRBSessionList.HeaderText4 %>" />
                                                <asp:CommandField HeaderText="<%$Resources:grdRBSessionList.HeaderText5 %>" ShowSelectButton="True" SelectText="<%$Resources:grdRBSessionList.HeaderText5 %>" />
                                            </Columns>
                                            <HeaderStyle CssClass="CartListHead" />
                                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                                        </asp:GridView>
                                        <pager:AspNetPager id="grdRBSessionListPager" runat="server" 
                                            showcustominfosection="Right" width="608px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"   PageSize="15" onpagechanged="grdRBSessionListPager_PageChanged"  UpdatePanelId="upd"
                                        ></pager:AspNetPager>

                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlConfirm" runat="server">
                            <table width="100%">
                                <tr>
                                    <td  align="center">
                                    <asp:Label ID="lblAlert" runat="server" Text="<%$Resources:lblAlert.Text %>"
                                        Font-Bold="True" CssClass="NormalRed" />
                                    </td>
                                </tr>
                                <tr>
                                    <td  align="center">
                                        <asp:Button ID="btnOK" runat="server" Text="<%$Resources:btnOK.Text %>" Width="75px" CssClass="NormalButton"
                                            OnClick="btnOK_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="<%$Resources:btnCancel.Text %>" Width="75px" CssClass="NormalButton"
                                            OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlShowButton" runat="server" Width="100%">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlSessionDetail" runat="server">
                            <table cellpadding="0" cellspacing="0" width="100%" style="height: 359px">
                                <tr>
                                    <td colspan="5" style="height: 29px">
                                        <asp:Label ID="lblSessionDetail" runat="server" Text="<%$Resources:chkSessionID.Text %>"  CssClass="SubContentHead"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="height:35px;">
                                    <td style="height: 35px; width: 120px;">
                                        <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                    </td>
                                    <td class="style1" style="width: 71px; height: 35px">
                                        <asp:TextBox ID="txtLocationDetail" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                    </td>
                                    <td style="height: 35px; width: 270px">
                                        <%--<asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblLocationGPRS.Text %>"></asp:Label>--%>
                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblLocationLatitude.Text %>"></asp:Label>
                                    </td>
                                    <td style="height: 35px">
                                        <%--<asp:TextBox ID="txtGPSDetail" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>--%>
                                        <asp:TextBox ID="txtGPSLatitude" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                    </td>
                                    <td valign="top" class="style12" style="height: 35px">
                                        
                                    </td>
                                </tr>
                                <tr style="height:35px;">
                                    <td></td>
                                    <td></td>
                                    <td style="height: 35px; width: 270px">
                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Text="<%$Resources:lblLocationLongitude.Text %>"></asp:Label>
                                    </td>
                                    <td style="height: 35px">
                                        <asp:TextBox ID="txtGPSLongitude" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style5" colspan="2" valign="top">
                                        <table cellpadding="0" cellspacing="0" style="height: 115px" width="100%">
                                            <tr>
                                                <td class="style6" valign="top">
                                                    <asp:Label ID="lblCourtList" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCourtList.Text %>"></asp:Label>
                                                </td>
                                                <td class="style6" valign="top">
                                                    <asp:CheckBoxList ID="chkClerkofCourtList" runat="server" AutoPostBack="True" BorderStyle="Ridge"
                                                        CssClass="Normal" Height="93px" OnSelectedIndexChanged="chkClerkofCourtList_SelectedIndexChanged"
                                                        Width="200px">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style5" valign="top">
                                                    <asp:Label ID="lblComments" runat="server" CssClass="NormalBold" Text="<%$Resources:lblComments.Text %>"></asp:Label>
                                                </td>
                                                <td class="style5" valign="top">
                                                    <asp:TextBox ID="txtComments" runat="server" CssClass="Normal" Height="95px" MaxLength="200"
                                                        TextMode="MultiLine" Width="222px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="style8" valign="top" style="width: 96px">
                                        <asp:Label ID="lblLA" runat="server" CssClass="NormalBold" Text="<%$Resources:lblLA.Text %>"></asp:Label>
                                    </td>
                                    <td class="style5" valign="top">
                                        <asp:Panel ID="pnlLaList" runat="server" HorizontalAlign="Left" ScrollBars="Auto"
                                            Width="200px">
                                            <asp:CheckBoxList ID="chkLocalAuthList" runat="server" BorderStyle="Ridge" CssClass="Normal"
                                                Enabled="False" Height="50px" Width="200px">
                                            </asp:CheckBoxList>
                                        </asp:Panel>
                                    </td>
                                    <td class="style5" valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr style="height: 29px">
                                                <td>
                                                    <asp:Button ID="btnAdd" runat="server" Text="<%$Resources:btnAdd.Text %>" Width="150px" CssClass="NormalButton"
                                                        OnClick="btnAdd_Click" OnClientClick="document.getElementById('<%=btnAdd.ClientID %>').disabled = 'true';" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnUpdate" runat="server" Text="<%$Resources:btnUpdate.Text %>" Width="150px" CssClass="NormalButton"
                                                        OnClick="btnUpdate_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnDelete" runat="server" Text="<%$Resources:btnDelete.Text %>" Width="150px" CssClass="NormalButton"
                                                        OnClick="btnDelete_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnAccessSession" runat="server" Text="<%$Resources:btnAccessSession.Text %>" Width="150px"
                                                        CssClass="NormalButton" OnClick="btnAccessSession_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:Button ID="btnCloseSession" runat="server" Text="<%$Resources:btnCloseSession.Text %>" Width="150px"
                                                        CssClass="NormalButton" OnClick="btnCloseSession_Click" />
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnCloseSession"
                                                        ConfirmText="<%$Resources:lblCloseSessionConfirm.Text %>" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnPrintRegsiter" runat="server" Text="<%$Resources:btnPrintRegsiter.Text %>" Width="150px"
                                                        CssClass="NormalButton" OnClick="btnPrintRegsiter_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlSummonsPrint" runat="server">
                            <p>
                                <asp:LinkButton ID="btnReturn" Text="<%$Resources:btnReturn.Text %>" runat="server" CssClass="NormalBold"
                                    OnClick="btnReturn_Click"></asp:LinkButton></p>
                            <asp:GridView ID="grdSumonsMultiplePrint" runat="server" AllowPaging="False" Width="50%"
                                AutoGenerateColumns="False" CellPadding="3" CssClass="Normal"
                                OnSelectedIndexChanged="grdSumonsMultiplePrint_SelectedIndexChanged"
                                ShowFooter="True">
                                <FooterStyle CssClass="CartListHead" />
                                <Columns>
                                    <%--Jerry 2014-02-17 Remove Charge from SP and related Grid in web page-- %>
                                    <%--<asp:BoundField DataField="ChgIntNo" HeaderText="ChgIntNo" Visible="False" />--%>
                                    <asp:BoundField DataField="SumIntNo" HeaderText="SumIntNo" Visible="False" />
                                    <asp:BoundField DataField="PrintAutIntNo" HeaderText="PrintAutIntNo" Visible="False" />
                                    <asp:BoundField DataField="SummonsNo" HeaderText="<%$Resources:grdSumonsMultiplePrint.HeaderText %>" />
                                    <asp:BoundField DataField="PrintFile" HeaderText="<%$Resources:grdSumonsMultiplePrint.HeaderText1 %>" />
                                    <asp:CommandField HeaderText="" SelectText="<%$Resources:grdSumonsMultiplePrint.SelectText %>" ShowSelectButton="true" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="labelNoPrintData" runat="server" CssClass="NormalRed" Text="<%$Resources:labelNoPrintData.Text %>"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="CartListHead" />
                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                            </asp:GridView>
                            <pager:AspNetPager id="grdSumonsMultiplePrintPager" runat="server" 
                                showcustominfosection="Right" width="50%" 
                                CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                  FirstPageText="|&amp;lt;" 
                                LastPageText="&amp;gt;|" 
                                CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                CustomInfoStyle="float:right;"  PageSize="15" onpagechanged="grdSumonsMultiplePrintPager_PageChanged" UpdatePanelId="upd"
                            ></pager:AspNetPager>

                        </asp:Panel>
                        <asp:Panel ID="plSelectRoadBlock" runat="server" Width="100%" Font-Underline="False">
                            <p>
                                <asp:LinkButton ID="LinkButton1" Text="<%$Resources:btnReturn.Text %>" runat="server"
                                    CssClass="NormalBold" OnClick="btnReturn_Click"></asp:LinkButton></p>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblSelectRoadBlock12" CssClass="NormalBold" runat="server" Text="<%$Resources:lblSelectRoadBlock12.Text %>" />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRoadBlock12" CssClass="NormalBold" runat="server" Text="<%$Resources:lblRoadBlock12.Text %>" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboRoadBlockSession" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <asp:Button ID="OKlButton" runat="server" Text="<%$Resources:btnOK.Text %>" Width="64px" OnClick="OKButton_Click"
                                            CssClass="NormalButton" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="style2" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
