﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Representation"
    CodeBehind="Representation.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-Strict.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script src="Scripts/Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            $("#btnYes").click(function () {
                $("#hidIsSelectedYes").val('Yes');
                $.unblockUI();
                document.forms[0].submit();
                return false;
            });
            $("#btnNo").click(function () {
                $("#hidIsSelectedYes").val('No');
                $.unblockUI();
                document.forms[0].submit();
                return false;
            });
        });

        function load() {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        }

        function EndRequestHandler() {
            $(function () {

            //Jacob 2013-01-08 change 
//                $("#txtStreet1").focus(function () {

//                    if ($("#chxCopyPostalAdsToStreet").is(":checked")) {

//                        if ($("#txtStreet1").val() == "") {
//                            $("#txtStreet1").val($("#txtPO1").val());
//                        }
//                        if ($("#txtStreet2").val() == "") {
//                            $("#txtStreet2").val($("#txtPo2").val());
//                        }
//                        if ($("#txtStreet3").val() == "") {
//                            $("#txtStreet3").val($("#txtPo3").val());
//                        }
//                        if ($("#txtStreet4").val() == "") {
//                            $("#txtStreet4").val($("#txtPo4").val());
//                        }


//                    }

//                });
                
                //
                $("#txtPO1").blur(function () {
                    copyPoToStreet("#txtPO1", "#txtStreet1");
                });

                $("#txtPo2").blur(function () {
                    copyPoToStreet("#txtPo2", "#txtStreet2");
                });
                $("#txtPo3").blur(function () {
                    copyPoToStreet("#txtPo3", "#txtStreet3");
                });
                $("#txtPo4").blur(function () {
                    copyPoToStreet("#txtPo4", "#txtStreet4");
                });
                //2013-01-08 Jacob scr and des should with prefix #.copyPoToStreet("#", "#");
                var copyPoToStreet = function (src, des) {
                    if ($("#chxCopyPostalAdsToStreet").attr("checked")) {
                        $(des).val($(src).val());
                    }
                };

//                //2012-12-12 heidi added checkBox CopyPostalAdsToStreet
//                $("#txtStreet1").focus(function () {

//                    if ($("#chxCopyPostalAdsToStreet").is(":checked")) {

//                        if ($("#txtStreet1").val() == "") {
//                            $("#txtStreet1").val($("#txtPO1").val());
//                        }
//                        if ($("#txtStreet2").val() == "") {
//                            $("#txtStreet2").val($("#txtPo2").val());
//                        }
//                        if ($("#txtStreet3").val() == "") {
//                            $("#txtStreet3").val($("#txtPo3").val());
//                        }
//                        if ($("#txtStreet4").val() == "") {
//                            $("#txtStreet4").val($("#txtPo4").val());
//                        }


//                    }

//                })
            });
        }
        function checkWithdraw() {
            var prCode = $("#rblCodes input[checked='checked']").val();
            if (prCode == 13 || prCode == 30 || prCode == 3) {
                if (confirm($("#WithdrawMsg").val())) {
                    return false;
                }
                return true;
            }
            return false;
        }
    </script>
    <style type="text/css">
        .style1
        {
            height: 26px;
            width: 212px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0" onload="load()">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
        <tr>
            <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" height="85%">
        <tr>
            <td>
                <img style="height: 1px" src="images/1x1.gif" width="167" />
            </td>
            <td>
                <asp:UpdateProgress ID="udpProgress" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" />&nbsp;Loading...</p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" rowspan="2">
                <img style="height: 1px" src="images/1x1.gif" width="167" />
                <asp:Panel ID="pnlMainMenu" runat="server">
                </asp:Panel>
            </td>
            <td valign="top" align="left" colspan="1" style="width: 879px">
                <asp:UpdatePanel ID="UPD" runat="server">
                    <ContentTemplate>
                        <table border="0" style="width: 800px">
                            <tr>
                                <td valign="top" style="width: 800px; text-align: center; height: 30px;">
                                    <asp:Label ID="lblPageName" runat="server" Width="500px" CssClass="ContentHead"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 800px;">
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                    <asp:HiddenField ID="hdnType_rep" runat="Server" Value="Register"></asp:HiddenField>
                                    <asp:HiddenField ID="hdnType_srep" runat="Server" Value="Register"></asp:HiddenField>
                                    <asp:HiddenField ID="hdnTicketNo" runat="Server" Value=""></asp:HiddenField>
                                    <asp:Panel ID="pnlRetrieve" runat="server">
                                        <table border="0" style="width: 800px">
                                            <tr>
                                                <td style="width: 250px">
                                                </td>
                                                <td style="width: 542px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 250px">
                                                    <asp:Label ID="lblSelAuthority" runat="server" Width="240px" CssClass="NormalBold"
                                                        Text="<%$Resources:lblSelAuthority.Text%>"></asp:Label>
                                                </td>
                                                <td style="width: 542px">
                                                    <asp:DropDownList ID="ddlSelectLA" runat="server" Width="220px" CssClass="Normal"
                                                        OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 250px">
                                                    <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Width="200px" Text="<%$Resources:lblTicket.Text%>"></asp:Label>
                                                </td>
                                                <td style="width: 542px">
                                                    <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected">
                                                    </uc1:TicketNumberSearch>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 250px">
                                                    <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="240px" Text="<%$Resources:lblFullTicket.Text%>"></asp:Label>
                                                </td>
                                                <td style="width: 542px">
                                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="Normal" MaxLength="30" Width="215px"></asp:TextBox>
                                                    <asp:Button ID="btnSearch" Text="<%$Resources:btnSearchTicket.Text%>" runat="server"
                                                        CssClass="NormalButton" OnClick="btnSearch_Click" Width="80px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 21px; width: 250px;">
                                                </td>
                                                <td style="height: 21px; width: 542px;">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlCharges" runat="server" Width="100%" CssClass="Normal" Visible="false">
                                        <%--<asp:GridView ID="grdCharges" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                                            ShowFooter="True" OnSelectedIndexChanged="grdCharges_SelectedIndexChanged" OnRowCreated="grdCharges_RowCreated"
                                            OnRowEditing="grdCharges_RowEditing">
                                            <FooterStyle CssClass="CartListFooter" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundField DataField="RepIntNo" HeaderText="  Ref No. "></asp:BoundField>
                                                <asp:BoundField DataField="ChgIntNo" HeaderText=" Charge Ref No. "></asp:BoundField>
                                                <asp:BoundField DataField="CSDescr" HeaderText="Charge Status"></asp:BoundField>
                                                <asp:BoundField DataField="ChgOffenceDescr" HeaderText="Charge">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Rep. Status">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Rep. Date">
                                                    <ItemStyle VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("RepDate", "{0:yyyy-MM-dd}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText=" Representation " ShowEditButton="True">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" HorizontalAlign="Center" />
                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Center" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="IsSummons" HeaderText="IsSummons" Visible=false /> 
                                            </Columns>
                                        </asp:GridView>--%>
                                        <%--Oscar 20101018 - redesign the grdCharges and add new grdRepresentation--%>
                                        <div id="divNoticeTitle" runat="server" visible="false">
                                            <table width="100%">
                                                <tr style="font-size: 14px;">
                                                    <td>
                                                        <asp:Label ID="lblSummonsNo" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNoticeNo" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNoticeDescr" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <asp:GridView ID="grdCharges" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                                            ShowFooter="True" OnRowCreated="grdCharges_RowCreated" OnSelectedIndexChanging="grdCharges_SelectedIndexChanging"
                                            OnRowCommand="grdCharges_RowCommand" OnRowDataBound="grdCharges_RowDataBound">
                                            <FooterStyle CssClass="CartListFooter" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <Columns>
                                                <%--<asp:BoundField DataField="RepIntNo" HeaderText="  Ref No. " Visible="false" ></asp:BoundField>--%>
                                                <asp:BoundField DataField="ChgIntNo" HeaderText=" Charge Ref No. " Visible="false">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ChargeNo" HeaderText=" <%$Resources:grdCharges.ChargeNo.HeaderText %> "
                                                    HeaderStyle-Wrap="false"></asp:BoundField>
                                                <asp:BoundField DataField="ChgOffenceDescr" HeaderText="<%$Resources:grdCharges.ChgOffenceDescr.HeaderText %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="true" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IsMainCharge" HeaderText=" <%$Resources:grdCharges.IsMainCharge.HeaderText %> "
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <%-- Oscar 20100927 - add --%>
                                                <asp:BoundField DataField="CSDescr" HeaderText="<%$Resources:grdCharges.CSDescr.HeaderText %>">
                                                </asp:BoundField>
                                                <%--<asp:TemplateField HeaderText="Court Date">
                                                    <ItemStyle VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SumCourtDate", "{0:yyyy-MM-dd}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:BoundField DataField="SumCourtDate" HeaderText="<%$Resources:grdCharges.SumCourtDate.HeaderText %>"
                                                    DataFormatString="{0:yyyy-MM-dd}" ItemStyle-VerticalAlign="Middle" ItemStyle-Wrap="false" />
                                                <%--<asp:TemplateField HeaderText=" Action ">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnSelect" CommandName='<%# Eval("ChgIntNo") %>' runat="server">Select</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:CommandField HeaderText=" <%$Resources:grdCharges.Command.HeaderText %> " ShowSelectButton="true"
                                                    SelectText="<%$Resources:btnSelect.Text %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" HorizontalAlign="Center" />
                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Center" />
                                                </asp:CommandField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtnDocument" CommandName="Document" CommandArgument='<%# Eval("NotIntNo")+","+Eval("ChgIntNo")+","+Eval("IsSummons") %>'
                                                            Visible="false" Text="<%$Resources:PresentationOfDocument %>" runat="server"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IsSummons" HeaderText="IsSummons" Visible="false" />
                                            </Columns>
                                        </asp:GridView>
                                        <asp:Label ID="lblEmptyData" runat="server" Text="" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                        <asp:GridView ID="grdRepresentation" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                                            ShowFooter="True" OnRowCreated="grdRepresentation_RowCreated" OnSelectedIndexChanging="grdRepresentation_SelectedIndexChanging">
                                            <FooterStyle CssClass="CartListFooter" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundField DataField="ChargeNo" HeaderText="  <%$Resources:grdRepresentation.ChargeNo.HeaderText %> ">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RepIntNo" HeaderText="  <%$Resources:grdRepresentation.RepIntNo.HeaderText %> ">
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ChgIntNo" HeaderText=" <%$Resources:grdRepresentation.ChgIntNo.HeaderText %> "
                                                    Visible="false"></asp:BoundField>
                                                <asp:BoundField DataField="RepDetails" HeaderText=" <%$Resources:grdRepresentation.RepDetails.HeaderText %> ">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="true" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RCDescr" HeaderText="<%$Resources:grdRepresentation.RCDescr.HeaderText %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="true" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="<%$Resources:grdRepresentation.Date.HeaderText %>">
                                                    <ItemStyle VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("RepDate", "{0:yyyy-MM-dd}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField HeaderText=" <%$Resources:grdRepresentation.Command.HeaderText %> "
                                                    ShowSelectButton="True" SelectText="<%$Resources:btnSelect.Text %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" HorizontalAlign="Center" />
                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Center" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="IsSummons" HeaderText="IsSummons" Visible="false" />
                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                        <!-- Visible="False" -->
                                        <asp:Button ID="btnAdd" runat="server" CssClass="NormalButton" OnClick="btnAdd_Click"
                                            Text=" <%$Resources:btnAdd.Text %> " Width="80px" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="NormalButton" OnClick="btnReport_Click"
                                            Text="<%$Resources:btnReport.Text %>" Width="157px" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlNoResults" runat="server" Width="100%">
                                        <br />
                                        <asp:Button ID="btnNoResults" runat="server" CssClass="NormalButton" OnClick="btnNoResults_Click"
                                            Text="<%$Resources:btnNoResults.Text %>" Width="293px" /></asp:Panel>
                                    <asp:Panel ID="pnlRepresentation" runat="server" Width="100%">
                                        <table cellspacing="1" cellpadding="1" border="0">
                                            <tr>
                                                <td style="text-align: center" colspan="4">
                                                    <asp:Label ID="Label2" runat="server" CssClass="ContentHead" Visible="False" Text="<%$Resources:lblRepersent.Text %>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="width: 164px;">
                                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTictNo.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 212px;" valign="top">
                                                    <asp:TextBox ID="txtTicketNo" runat="server" CssClass="Normal" MaxLength="50" ReadOnly="True"
                                                        Width="180px"></asp:TextBox>
                                                    <asp:HiddenField ID="WithdrawMsg" runat="server" />
                                                </td>
                                                <td valign="top" style="width: 116px;">
                                                    <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="113px" Text="<%$Resources:lblChargeStatus.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 250px;">
                                                    <asp:TextBox ID="txtChargeStatus" runat="server" CssClass="Normal" MaxLength="50"
                                                        ReadOnly="True" Width="293px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="width: 164px;">
                                                    <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Width="160px" Text="<%$Resources:lblDateRepre.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" style="width: 212px;">
                                                    <asp:TextBox runat="server" ID="wdcRepDate" CssClass="Normal" Height="20px" autocomplete="off"
                                                        UseSubmitBehavior="False" />
                                                    <cc1:CalendarExtender ID="DateCalendar" runat="server" TargetControlID="wdcRepDate"
                                                        Format="yyyy-MM-dd">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="wdcRepDate"
                                                        CssClass="NormalRed" Display="Dynamic" ErrorMessage="<%$Resources:valDateErrorMsg %>"
                                                        ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                                </td>
                                                <td valign="top" style="width: 116px;">
                                                    <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Text="<%$Resources:lblChargeOffence.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" style="width: 250px;">
                                                    <asp:TextBox ID="txtOffenceDescr" runat="server" CssClass="Normal" MaxLength="18"
                                                        Width="292px" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblVehicleReg.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" style="width: 212px">
                                                    <asp:TextBox ID="txtRegNo" runat="server" CssClass="Normal" MaxLength="18" ReadOnly="True"
                                                        Width="180px"></asp:TextBox>
                                                </td>
                                                <td valign="top" style="width: 116px">
                                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOffenderName.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" style="width: 250px">
                                                    <asp:TextBox ID="txtRepOffenderName" runat="server" CssClass="Normal" MaxLength="50"
                                                        ReadOnly="True" Width="251px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="width: 164px">
                                                    <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddressDetail.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" style="width: 212px">
                                                    <asp:TextBox ID="txtRepOffenderAddress" runat="server" CssClass="Normal" TextMode="MultiLine"
                                                        ReadOnly="True" Rows="3" Width="180px"></asp:TextBox>
                                                </td>
                                                <td valign="top" style="width: 116px">
                                                </td>
                                                <td valign="top" style="width: 250px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="width: 164px">
                                                    <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Width="158px" Text="<%$Resources:lblDetailOfRepre.Text %>"></asp:Label>
                                                </td>
                                                <td style="" valign="top" colspan="3">
                                                    <asp:TextBox ID="txtRepDetails" runat="server" CssClass="Normal" TextMode="MultiLine"
                                                        Rows="2" Width="600px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="width: 164px">
                                                    <asp:Label ID="lblReversalReason" runat="server" CssClass="NormalBold" Width="158px"
                                                        Text="<%$Resources:lblReversalReason.Text %>"></asp:Label>
                                                </td>
                                                <td style="" valign="top" colspan="3">
                                                    <asp:TextBox ID="txtReversalReason" runat="server" CssClass="Normal" Rows="1" Width="600px"
                                                        Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="width: 164px; height: 26px;">
                                                    <asp:Label ID="lblRecommend" runat="server" CssClass="NormalBold" Width="158px" Text="<%$Resources:lblRecommend.Text %>"></asp:Label>
                                                </td>
                                                <td style="height: 26px" valign="top" colspan="3">
                                                    <asp:TextBox ID="txtRecommend" runat="server" CssClass="Normal" Rows="1" Width="600px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td valign="top" style="width: 164px; height: 26px;">
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="NormalButton" OnClick="btnCancel_Click"
                                                        Text="<%$Resources:btnCancel.Text %>" Width="104px" />
                                                </td>
                                                <td style="text-align: left; width: 400px;" valign="top" align="left" class="style1">
                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" OnClick="btnUpdate_Click"
                                                        Text="<%$Resources:btnUpdate.Text %>" Width="140px" />
                                                    <asp:Button ID="btnDecide" runat="server" CssClass="NormalButton" OnClick="btnDecide_Click" 
                                                        Visible="false" Text=" <%$Resources:btnDecide.Text %> " Width="100px" OnClientClick="if(checkWithdraw()) return false;" />
                                                    <asp:Button ID="btnReverse" runat="server" CssClass="NormalButton" OnClick="btnReverse_Click"
                                                        Text="<%$Resources:btnReverse.Text %>" Width="120px" />&nbsp;
                                                </td>
                                                <td style="width: 50px; height: 26px;">
                                                </td>
                                                <td valign="bottom" style="text-align: center; width: 150px; height: 26px;" class="NormalBold">
                                                    <asp:Panel ID="pnlShowDocs" runat="server" BorderColor="Blue" BorderWidth="2px" Height="50px"
                                                        Width="125px" HorizontalAlign="Center">
                                                        <br />
                                                        <a href="javascript:showDocuments('Form1.txt');">
                                                            <asp:Label ID="lblShowDoc" runat="server" Text="<%$Resources:lblShowDoc.Text %>"></asp:Label></a></asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlDecide" runat="server" Visible="false" Width="100%">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top" style="width: 164px">
                                                        <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Width="161px" Text="<%$Resources:lblOriginal.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="top" style="width: 263px">
                                                        <asp:TextBox ID="txtOriginalAmount" runat="server" CssClass="Normal" MaxLength="50"
                                                            Width="180px" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                    <td valign="top" style="width: 251px">
                                                        <asp:Label ID="Label17" runat="server" CssClass="NormalBold" Width="144px" Text="<%$Resources:lblRevised.Text %>"></asp:Label>
                                                        <asp:TextBox ID="txtRevAmount" runat="server" CssClass="Normal" Width="101px" MaxLength="6"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width: 164px;">
                                                        <asp:Label ID="Label22" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOfficial.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="top" colspan="2">
                                                        <asp:DropDownList ID="ddlRepOfficial" runat="server"
                                                            AutoPostBack="True" 
                                                            onselectedindexchanged="ddlRepOfficial_SelectedIndexChanged" />
                                                        <asp:TextBox ID="txtRepOfficial" runat="server" CssClass="Normal" MaxLength="50"
                                                            Width="180px" Visible="False"></asp:TextBox>
                                                        &nbsp;<asp:Label ID="lblRepOfficial" runat="server" CssClass="Normal" Text="" Visible="False" />
                                                    </td>
                                                    <%--<td valign="top" style="width: 251px;">
                                                    </td>--%>
                                                </tr>
                                                <tr>
                                                    <td style="width: 164px;" valign="top">
                                                    </td>
                                                    <td style="width: 263px;" valign="top">
                                                        <asp:CheckBox ID="chkChangeRegistration" runat="server" AutoPostBack="true" CssClass="NormalBold"
                                                            OnCheckedChanged="chkChangeRegistration_CheckedChanged" Text="<%$Resources:chkChangeRegistration.Text %>"
                                                            Width="210px" />
                                                    </td>
                                                    <td style="width: 251px;" valign="top">
                                                        <asp:CheckBox ID="chkChangeOffender" runat="server" CssClass="NormalBold" OnCheckedChanged="chkChangeOffender_onChecked"
                                                            AutoPostBack="true" Text="<%$Resources:chkChangeOffender.Text %>" Width="208px">
                                                        </asp:CheckBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td valign="top" colspan="1" style="height: 115px">
                                                        <fieldset>
                                                            <legend>
                                                                <asp:Label ID="legendRepre" runat="server" Text="<%$Resources:lblLegendRepre.Text %>"></asp:Label></legend>
                                                            <asp:RadioButtonList ID="rblCodes" runat="server" CssClass="Normal" RepeatColumns="1"
                                                                RepeatLayout="Table" Width="251px" OnSelectedIndexChanged="rblCodes_OnSelectedIndexChanged"
                                                                AutoPostBack="True">
                                                                <asp:ListItem Selected="True" Value="0" Text="<%$Resources: rblCodes.ListItem1.Text %>"></asp:ListItem>
                                                                <asp:ListItem Value="3" Text="<%$Resources: rblCodes.ListItem2.Text %>"></asp:ListItem>
                                                                <asp:ListItem Value="4" Text="<%$Resources: rblCodes.ListItem3.Text %>"></asp:ListItem>
                                                                <asp:ListItem Value="5" Text="<%$Resources: rblCodes.ListItem4.Text %>"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </fieldset>
                                                    </td>
                                                    <td valign="top" colspan="1">
                                                        <asp:Panel runat="server" ID="pnlLetterTo" Visible="true" BorderColor="White" BorderStyle="None">
                                                            <fieldset>
                                                                <legend>
                                                                    <asp:Label runat="server" ID="lblLetter" Text="<%$Resources: lblLetter.Text %>"></asp:Label></legend>
                                                                <asp:RadioButtonList ID="rdlLetter" runat="server" CssClass="Normal" RepeatColumns="2"
                                                                    RepeatLayout="Table" Width="251px">
                                                                    <asp:ListItem Selected="True" Value="O" Text="<%$Resources: rdlLetter.ListItem1.Text%>"></asp:ListItem>
                                                                    <asp:ListItem Value="D" Text="<%$Resources: rdlLetter.ListItem2.Text%>"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </fieldset>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:Panel runat="server" ID="pnlChangeRegNoDecision" Visible="true" Width="100%"
                                                            BorderColor="#8080FF" BorderStyle="Inset" BorderWidth="2px">
                                                            <fieldset>
                                                                <legend>
                                                                    <asp:Label ID="lblReasonReg" runat="server" Text="<%$Resources: lblReasonReg.Text %>"></asp:Label></legend>
                                                                <asp:RadioButtonList ID="rdlRegDecision" runat="server" CssClass="Normal" RepeatColumns="2"
                                                                    RepeatLayout="Table" Width="100%">
                                                                    <asp:ListItem Selected="True" Value="N" Text="<%$Resources: rdlRegDecision.ListItem1.Text %>"></asp:ListItem>
                                                                    <asp:ListItem Value="D" Text="<%$Resources: rdlRegDecision.ListItem2.Text %>"></asp:ListItem>
                                                                    <asp:ListItem Value="I" Text="<%$Resources: rdlRegDecision.ListItem3.Text %>"></asp:ListItem>
                                                                    <asp:ListItem Value="L" Text="<%$Resources: rdlRegDecision.ListItem4.Text %>"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </fieldset>
                                                            <asp:CheckBox ID="chkNoFurtherDetails" runat="server" AutoPostBack="true" CssClass="NormalBold"
                                                                OnCheckedChanged="chkNoFurtherDetails_CheckedChanged" Text="<%$Resources: chkNoFurtherDetails.Text %>"
                                                                Width="545px" /></asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Panel ID="pnlRegistration" runat="server" Width="100%" BorderColor="#8080FF"
                                                BorderStyle="Inset" BorderWidth="2px">
                                                <table class="NormalBold" width="100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label21" runat="server" CssClass="SubContentHead" Text="<%$Resources:lblExistReg.Text %>"
                                                                Width="368px"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label20" runat="server" CssClass="SubContentHead" Text="<%$Resources:lblNewReg.Text %>"
                                                                Width="378px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 164px;">
                                                            <asp:Label ID="lblRegNum" runat="server" Text="<%$Resources:lblRegNum.Text %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 212px;">
                                                            <asp:TextBox ID="txtPrevRegNo" runat="server" CssClass="Normal" MaxLength="10" Width="100px"
                                                                ReadOnly="True"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 215px;">
                                                            <asp:Label ID="Label25" runat="server" Text="<%$Resources:lblRegNum.Text %>"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNewRegNo" runat="server" CssClass="Normal" MaxLength="10" Width="100px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 164px">
                                                            <asp:Label ID="lblVehicleMake" runat="server" Text="<%$Resources:lblVehicleMake.Text %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 212px">
                                                            <asp:DropDownList ID="ddlPrevVehMake" runat="server" CssClass="NormalBold" Width="169px"
                                                                Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 215px">
                                                            <asp:Label ID="Label26" runat="server" Text="<%$Resources:lblVehicleMake.Text %>"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlNewVehMake" runat="server" CssClass="Normal" Width="169px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 164px;">
                                                            <asp:Label ID="lblVehicleType" runat="server" Text="<%$Resources:lblVehicleType.Text %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 212px;">
                                                            <asp:DropDownList ID="ddlPrevVehType" runat="server" CssClass="NormalBold" Width="169px"
                                                                Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 215px;">
                                                            <asp:Label ID="Label27" runat="server" Text="<%$Resources:lblVehicleType.Text %>"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlNewVehType" runat="server" CssClass="Normal" Width="169px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 164px;">
                                                            <asp:Label ID="lblColour" runat="server" Text="<%$Resources:lblColour.Text %>"></asp:Label>
                                                        </td>
                                                        <td style="width: 212px;">
                                                            <asp:DropDownList ID="ddlNotVehicleColour" runat="server" CssClass="Normal" Width="169px"
                                                                Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 215px;">
                                                            <asp:Label ID="Label28" runat="server" Text="<%$Resources:lblColour.Text %>"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlNewVehicleColourDescr" runat="server" CssClass="Normal"
                                                                Width="169px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlChangeOfOffender" runat="server" Visible="false" Width="100%" BorderColor="#8080FF"
                                        BorderStyle="Inset" BorderWidth="2px">
                                      
                                        <asp:Label ID="Label19" runat="server" CssClass="SubContentHead" Text="<%$Resources:lblNewDriver.Text %>"
                                            Width="218px"></asp:Label><br />
                                        <table border="0" class="NormalBold">
                                            <tr>
                                                <td style="width: 164px;">
                                                    <asp:Label runat="server" ID="lblSurname" Text="<%$Resources:lblSurname.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px;">
                                                    <asp:TextBox ID="txtSurname" runat="server" Width="100%" MaxLength="100" CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px;">
                                                </td>
                                                <td style="width: 150px;">
                                                    <asp:Label runat="server" ID="lblIdNumber" Text="<%$Resources:lblIdNumber.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px;">
                                                    <asp:TextBox ID="txtIDNumber" runat="server" MaxLength="13" Width="61%" CssClass="Normal"
                                                        OnTextChanged="CalculateAge" AutoPostBack="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label runat="server" ID="lblForename" Text="<%$Resources:lblForename.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtForenames" runat="server" MaxLength="100" Width="100%" CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                    <asp:Label runat="server" ID="lblPassportNo" Text="<%$Resources:lblPassportNo.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtPassport" runat="server" MaxLength="25" Width="61%" CssClass="Normal"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label runat="server" ID="lblInitials" Text="<%$Resources:lblInitials.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtInitials" runat="server" MaxLength="10" Width="100%" CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                </td>
                                                <td style="width: 150px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label runat="server" ID="lblNationality" Text="<%$Resources:lblNationality.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtNationality" runat="server" MaxLength="3" Width="100px" CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                    <asp:Label runat="server" ID="lblAge" Text="<%$Resources:lblAge.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 150px">
                                                    <asp:TextBox ID="txtAge" runat="server" MaxLength="3" Width="100px" CssClass="Normal"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label runat="server" ID="Label30" Text="<%$Resources:lblCopyPostalAddressToStreet.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 800px">
                                                    <input id="chxCopyPostalAdsToStreet" type="checkbox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label runat="server" ID="lblPostalAddre" Text="<%$Resources:lblPostalAddre.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtPO1" ClientIDMode="Static" runat="server" MaxLength="100" Width="100%"
                                                        CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                    <asp:Label runat="server" ID="lblStreetAddress" Text="<%$Resources:lblStreetAddress.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtStreet1" ClientIDMode="Static" runat="server" MaxLength="100" se
                                                        Width="100%" CssClass="Normal" TabIndex=1000></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtPo2" ClientIDMode="Static" runat="server" MaxLength="100" Width="100%"
                                                        CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtStreet2" ClientIDMode="Static" runat="server" MaxLength="100"
                                                        Width="100%" CssClass="Normal" TabIndex=1000></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtPo3" ClientIDMode="Static" runat="server" MaxLength="100" Width="100%"
                                                        CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtStreet3" ClientIDMode="Static" runat="server" MaxLength="100"
                                                        Width="100%" CssClass="Normal" TabIndex=1000></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px" align="right">
                                                    <asp:Label ID="Label7" runat="Server" CssClass="NormalRed">*</asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtPo4" ClientIDMode="Static" runat="server" MaxLength="100" Width="100%"
                                                        CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px" align="right">
                                                    <asp:Label ID="Label8" runat="Server" CssClass="NormalRed">*</asp:Label>
                                                    <td style="width: 300px">
                                                        <asp:TextBox ID="txtStreet4" ClientIDMode="Static" runat="server" MaxLength="100"
                                                            Width="100%" CssClass="Normal" TabIndex=1000></asp:TextBox>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtPo5" ClientIDMode="Static" runat="server" MaxLength="100" Width="100%"
                                                        CssClass="Normal"></asp:TextBox>
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                </td>
                                                <td style="width: 300px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label runat="server" ID="lblCodeLookup" Text="<%$Resources:lblCodeLookup.Text %>"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnPostalCodesA" Text="..." class="NormalButton" OnClick="btnPostalCodesA_Click"
                                                        runat="server" Width="18px"></asp:Button>
                                                    &nbsp;
                                                    <asp:DropDownList ID="ddlPostalCodesA" runat="server" Width="225px" CssClass="Normal"
                                                        OnSelectedIndexChanged="ddlPostalCodesA_SelectedIndexChanged" AutoPostBack="true"
                                                        Visible="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="Label29" Text="<%$Resources:lblCodeLookup.Text %>"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnPostalCodesB" Text="..." class="NormalButton" OnClick="btnPostalCodesB_Click"
                                                        runat="server" Width="19px"></asp:Button>
                                                    &nbsp;
                                                    <asp:DropDownList ID="ddlPostalCodesB" runat="server" Width="225px" CssClass="Normal"
                                                        OnSelectedIndexChanged="ddlPostalCodesB_SelectedIndexChanged" AutoPostBack="true"
                                                        Visible="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label runat="server" ID="lblPostCode" Text="<%$Resources:lblPostCode.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtPoArea" runat="server" MaxLength="4" Width="100px" CssClass="Normal" />
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                    <asp:Label runat="server" ID="lblStreetCode" Text="<%$Resources:lblStreetCode.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtStreetArea" runat="server" MaxLength="4" Width="100px" CssClass="Normal" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Text="<%$Resources:lblHomeTel.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtHomeNo" runat="server" MaxLength="20" Width="120px" CssClass="Normal" />
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                    <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblWorkTel.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtWorkNo" runat="server" MaxLength="20" Width="120px" CssClass="Normal" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 164px">
                                                    <asp:Label ID="Label23" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCellNo.Text %>"></asp:Label>
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:TextBox ID="txtCellNo" runat="server" MaxLength="20" Width="120px" CssClass="Normal" />
                                                </td>
                                                <td style="width: 50px">
                                                </td>
                                                <td style="width: 150px">
                                                    &nbsp;
                                                </td>
                                                <td style="width: 300px">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4">
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="Label24" runat="server" CssClass="NormalRed" Text="<%$Resources:lblDescriptForm.Text %>"></asp:Label>&nbsp;
                                                    &nbsp;
                                                </td>
                                                <td style="width: 300px">
                                                    <asp:Button ID="btnInsufficientDetails" runat="server" CssClass="NormalButton" Text="<%$Resources:btnInsufficientDetails.Text %>"
                                                        OnClick="btnInsufficientDetails_Click" Width="200px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>

                        <div id="blockNoAOG" runat="server" Visible="False" >
                            <div class="blockUI blockOverlay" style="border-style: none; border-color: inherit; border-width: medium; z-index: 1000; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 1px; background-color: rgb(0, 0, 0); opacity: 0.6; cursor: wait; position: fixed;">
                            </div>
                            <div class="blockUI blockMsg blockPage" style="z-index: 1001; position: fixed; padding: 0px; margin: 0px; width: 30%; top: 40%; left: 35%; text-align: center; color: rgb(0, 0, 0); border: 3px solid rgb(170, 170, 170); background-color: rgb(255, 255, 255); cursor: wait;">
                                <div>
                                    <div style="padding: 10px; font-family: @Arial Unicode MS; font-size: 12px; line-height: 17px; font-weight: normal;">
                                        <asp:Label ID="Label31" runat="server" Text="<%$Resources:NoAOGAlert %>" />
                                    </div>
                                    <div style="text-align: center; margin: 15px 0 10px 0;">
                                        <asp:Button ID="btnBlockNoAOGYes" runat="server" Text="Yes" OnClick="btnBlockNoAOGYes_Click" />&nbsp;
                                        <asp:Button ID="btnBlockNoAOGNo" runat="server" Text="No" onclick="btnBlockNoAOGNo_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="" valign="top" style="width: 100%">
            </td>
        </tr>
    </table>
    <!--BlockUI begin-->
    <div id="faceboxCopy" style="display: none;">
        <div>
            <table>
                <tbody>
                    <tr>
                        <td>
                            <div style="padding: 10px; font-family: @Arial Unicode MS; font-size: 12px; line-height: 17px;
                                font-weight: normal;">
                                <asp:Label ID="lblDescriptLetter" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDescriptLetter.Text %>"></asp:Label>
                            </div>
                            <div style="text-align: center; margin: 15px 0 10px 0;">
                                <input id="btnYes" name="btnYes" value="Yes" runat="server" type="button" />
                                <input id="btnNo" name="btnNo" value="No" type="button" />
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!--BlockUI end-->
    <input id="hidIsSelectedYes" type="hidden" runat="server" />
    </form>
    <script language="javascript" type="text/javascript">
        function showDocuments() {
            var code_rep = document.getElementById("hdnType_rep");
            var code_srep = document.getElementById("hdnType_srep");
            var ticket = document.getElementById("hdnTicketNo");

            if (code_rep.value != "0" || code_srep.value != "0") {
                window.open('SupportingDocuments.aspx?code_rep=' + code_rep.value + '&code_srep=' + code_srep.value + '&ticket=' + ticket.value, 'SupportingDocuments', 'width=680,height=450, left=100,top=200 ,resizable=yes');
            }
            else {
                alert("Please add a representation before adding the supporting documents!");
            }
        }
       
    </script>
</body>
</html>
