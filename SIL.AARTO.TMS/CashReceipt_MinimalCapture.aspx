﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CashReceipt_MinimalCapture.aspx.cs"
    Inherits="SIL.AARTO.TMS.CashReceipt_NoNoticeMatch" EnableEventValidation="false" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%=styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%=description %>" name="Description" />
    <meta content="<%=keywords %>" name="Keywords" />
    <%--  <script src='<% =ResolveUrl("Scripts/Jquery/jquery.blockUI.js") %>' type="text/javascript"></script>--%>
    <link href="<% =ResolveUrl("stylesheets/themes/base/jquery.ui.all.css") %>" rel="stylesheet" />

    <script src='<% =ResolveUrl("Scripts/Jquery/jquery-1.3.2.min.js") %>' type="text/javascript"></script>
    <script src='<% =ResolveUrl("Scripts/Jquery/jquery-ui-1.8.11.min.js") %>' type="text/javascript"></script>
    <script src='<% =ResolveUrl("Scripts/CashReceipt_MinimalCapture.js") %>' type="text/javascript"></script>
    <style type="text/css">
        .style1
        {
            width: 200px;
        }
        .tdLabel
        {
            width:220px;
        }
    </style>
    <script type="text/javascript">
        var jquryPostUrl = '<% =ResolveUrl("~/CashReceipt_MinimalCapture.aspx") %>';
        
    </script>
    <script type="text/javascript">
        function load() {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        }
        function EndRequestHandler() {
            $("#txtOffenceDate").datepicker({ dateFormat: 'yy-mm-dd', dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, maxDate: new Date() });
        }
</script> 

</head>


<body onload="load()">
    <form id="form1" runat="server" >
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="UDP">
            <ContentTemplate>
                <asp:Panel ID="divMain" runat="server">
                    <div>
                        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td class="HomeHead" valign="middle" align="center" width="100%">
                                    <%--<hdr1:Header ID="Header1" runat="server"></hdr1:Header>--%>
                                </td>
                            </tr>
                        </table>
                        <p align="center">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                        </p>

                        <p align="center">
                            <table align="center">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel" style="width:150px;">
                                        <asp:Label ID="Label1" runat="server"
                                            Text="<%$ Resources:lblDocumentType.Text %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlFileType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFileType_SelectedIndexChanged" CssClass="Normal">
                                            <asp:ListItem Text="<%$Resources:ddlFileType.Item1 %>" Value=""></asp:ListItem>
                                            <asp:ListItem Text="<%$Resources:ddlFileType.Item2 %>" Value="H~P"></asp:ListItem>
                                            <asp:ListItem Text="<%$Resources:ddlFileType.Item3 %>" Value="H~M"></asp:ListItem>
                                            <asp:ListItem Text="<%$Resources:ddlFileType.Item4 %>" Value="M~P"></asp:ListItem>
                                            <asp:ListItem Text="<%$Resources:ddlFileType.Item5 %>" Value="M~M"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:lblIdNumber.Text %>"
                                            CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textId" runat="server" Width="200px" CssClass="Normal"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:lblForename.Text %>"
                                            CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtForenname" runat="server" Width="100px" CssClass="Normal"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:lblSurname.Text %>"
                                            CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSurname" runat="server" Width="100px" CssClass="Normal"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label12" runat="server"
                                            Text="<%$ Resources:lblRegNo.Text %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtRegNo"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label13" runat="server"
                                            Text="<%$ Resources:lblCourt.Text %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="dllCourt" AutoPostBack="true" CssClass="Normal" OnSelectedIndexChanged="dllCourt_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="<%$Resources:listOptions.Item %>"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCourtRoom" runat="server"
                                            Text="<%$ Resources:lblCourt.RoomText %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="dllCourtRoom" CssClass="Normal" AutoPostBack="true" OnSelectedIndexChanged="dllCourtRoom_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="<%$Resources:listOptions.Item %>"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCourtDate" runat="server"
                                            Text="<%$ Resources:lblCourtDate.Text %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="dllCourtDate" AutoPostBack="true" CssClass="Normal">
                                            <asp:ListItem Value="0" Text="<%$Resources:listOptions.Item %>"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label17" runat="server"
                                            Text="<%$ Resources:lblOfficer.Text %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="dllOfficer" AutoPostBack="true" CssClass="Normal">
                                            <asp:ListItem Value="0" Text="<%$Resources:listOptions.Item %>"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label18" runat="server"
                                            Text="<%$ Resources:lblPaymentOriginator.Text %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="dllOriginator" AutoPostBack="true" CssClass="Normal">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label5" runat="server"
                                            Text="<%$ Resources:lblNoticeNumber.Text %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtNoticeNumber" runat="server" Width="200px" CssClass="Normal"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label15" runat="server"
                                            Text="<%$ Resources:lblOffenceDate.Text %>" CssClass="NormalBold"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtOffenceDate" ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="NormalBold">
                                        <asp:Label ID="Label6" runat="server"
                                            Text="<%$ Resources:lblOffenceCode1.Text %>" CssClass="NormalBold"></asp:Label>1:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlOffenceCode1" Width="800px" runat="server" AutoPostBack="true" CssClass="Normal"
                                            OnSelectedIndexChanged="ddlOffenceCode1_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="NormalBold">
                                        <asp:Label ID="Label7" runat="server"
                                            Text="<%$ Resources:lblOffenceCode1.Text %>" CssClass="NormalBold"></asp:Label>2:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlOffenceCode2" Width="800px" runat="server" AutoPostBack="true" CssClass="Normal"
                                            OnSelectedIndexChanged="ddlOffenceCode2_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="NormalBold">
                                        <asp:Label ID="labOffenceCode3" runat="server"
                                            Text="<%$ Resources:lblOffenceCode1.Text %>" CssClass="NormalBold"></asp:Label>
                                        <asp:Label ID="labOffenceCode3_Ext" runat="server" Text="3:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlOffenceCode3" Width="800px" runat="server" AutoPostBack="true" CssClass="Normal"
                                            OnSelectedIndexChanged="ddlOffenceCode3_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="NormalBold">
                                        <asp:Label ID="Label8" runat="server"
                                            Text="<%$ Resources:lblFineAmount.Text %>" CssClass="NormalBold"></asp:Label>1:
                                    </td>
                                    <td>

                                        <asp:TextBox ID="txtFineAmount1" ClientIDMode="Static" runat="server" Width="100px"
                                            CssClass="Normal"></asp:TextBox>
                                        <asp:HiddenField ID="hidFineAmount1" ClientIDMode="Static" runat="server" />
                                        <asp:Label ID="lblAmount1Msg" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="NormalBold">
                                        <asp:Label ID="Label9" runat="server"
                                            Text="<%$ Resources:lblFineAmount.Text %>" CssClass="NormalBold"></asp:Label>2:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFineAmount2" runat="server" Width="100px" CssClass="Normal"></asp:TextBox>
                                        <asp:HiddenField ID="hidFineAmount2" runat="server" />
                                        <asp:Label ID="lblAmount2Msg" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                    </td>
                                </tr>



                                <tr>
                                    <td class="NormalBold">
                                        <asp:Label ID="labFineAmount3" runat="server"
                                            Text="<%$ Resources:lblFineAmount.Text %>" CssClass="NormalBold"></asp:Label>
                                        <asp:Label ID="labFinAmount3_Ext" runat="server" Text="3"></asp:Label>
                                    </td>
                                    <td class="style1">
                                        <asp:TextBox ID="txtFineAmount3" runat="server" Width="100px" CssClass="Normal"></asp:TextBox>
                                        <asp:HiddenField ID="hidFineAmount3" runat="server" />
                                        <asp:Label ID="lblAmount3Msg" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                                    </td>
                                </tr>


                                <tr>
                                    <td></td>
                                    <td align="left">
                                        <asp:Button ID="btnManually" runat="server" Text="<%$Resources:lblSave.Text %>" CssClass="NormalButton" OnClick="btnManually_Click" />
                                        <asp:Button ID="btnConfirm" runat="server" Text="<%$Resources:lblConfirm.Text %>" CssClass="NormalButton"
                                            OnClick="btnConfirm_Click" />
                                        <asp:Button ID="btnCancelFineAmount" runat="server" CssClass="NormalButton" Text="<%$Resources:lblCancel.Text %>"
                                            OnClick="btnCancelFineAmount_Click" />
                                    </td>
                                </tr>
                            </table>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Panel ID="divConfirm" runat="server" Visible="false">
                    <p align="center">
                        <asp:Label ID="Label10" runat="server" Text="<%$ Resources:lblTip.Text %>"
                            CssClass="NormalBold"></asp:Label>
                    </p>
                    <p align="center">
                        <asp:Button ID="btnProceed" runat="server" Text="<%$Resources:lblProceed.Text %>" CssClass="NormalButton"
                            OnClick="btnProceed_Click" />&nbsp;
                    <asp:Button ID="btnChange" runat="server" CssClass="NormalButton" Text="<%$Resources:lblChange.Text %>" OnClick="btnChange_Click" />&nbsp;
                    <asp:Button ID="btnCancelCapture" runat="server" CssClass="NormalButton" Text="<%$Resources:lblCancel.Text %>"
                        OnClick="btnCancelCapture_Click" />
                    </p>

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <p class="Normal" style="text-align: center;">
                    <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label11"
                        runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label>
                </p>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <script type="text/javascript">
            //var url = '<% =ResolveUrl("~/CashReceiptTraffic.aspx") %>';
        </script>
    </form>
    <%-- <script type="text/javascript">
        $(document).ready(function () {

            //            $("#btnChange").click(function () {
            //                $.unblockUI();
            //            });

            //            $("#btnCancelCapture").click(function () {
            //                $.unblockUI();
            //                window.location.href = url;
            //            });

            //            $("#txtFineAmount1").blur(function () {
            //                if ($("#txtFineAmount1").val() != $("#hidFineAmount1").val() && $("#hidFineAmount1").val() != '') {
            //                    alert("A");
            //                }
            //            });
        })
    </script>

     <script type="text/javascript">
         var txtPrintingCompany=

    </script>--%>
</body>
</html>
