using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Stalberg.TMS.Data.Datasets;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the end of day cash box balancing page
    /// </summary>
    public partial class ReceiptEODBalancingViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private ReportDocument _reportDoc = new ReportDocument();
        private dsReceiptEODBalancing ds;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Receipt EOD Balancing Report";
        protected string thisPageURL = "ReceiptEODBalancingViewer.aspx";
        protected string login = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            
            userDetails = (UserDetails)Session["userDetails"];

            login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (Request.QueryString["Date"] == null)
            {
                string error = (string)GetLocalResourceObject("error");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            DateTime dt;
            
            if (!DateTime.TryParse(Request.QueryString["Date"].ToString(), out dt))
            {
                string error = (string)GetLocalResourceObject("error");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            int cbIntNo = (Request.QueryString["cbIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["cbIntNo"].ToString()));
            string cbName = (Request.QueryString["cbName"] == null ? "All" : Request.QueryString["cbName"].ToString());
            int viewAutIntNo = (Request.QueryString["AutIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString()));

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "ReceiptEODBalancing");
            if (reportPage.Equals(""))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "ReceiptEODBalancing.rpt", "ReceiptEODBalancing", "system", "");
                reportPage = "ReceiptEODBalancing.rpt";
            }

            //dls 070816 - need an Authority Rule as to whether to include electronic payments in the EOD report
            // Check if this authority processes NaTIS data last
            //AuthorityRulesDB arDB = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails details = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "4591",
            //    "Rule that indicates whether to include electronic/manual captured payments in the EOD report", 0,
            //    "N", "Y = Yes; N = No (Default)",
            //    userDetails.UserLoginName);

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails details = new AuthorityRulesDetails();
            details.AutIntNo = this.autIntNo;
            details.ARCode = "4591";
            details.LastUser = login;

            DefaultAuthRules authRule = new DefaultAuthRules(details, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            string includeElectronicPayments = value.Value;

            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "ReceiptEODBalancing");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error1"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {

                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error2"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            _reportDoc.Load(reportPath);

            // Fill the DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("ReceiptEODBalancingReport");
            da.SelectCommand.Connection = new SqlConnection(this.connectionString);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = viewAutIntNo;
            //da.SelectCommand.Parameters.Add("@Date", SqlDbType.VarChar, 10).Value = dt;
            da.SelectCommand.Parameters.Add("@Date", SqlDbType.SmallDateTime).Value = dt;
            //dls 080111 - the report selection has changed to select by cashbox instead of user/cashier
            //da.SelectCommand.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = nUser;
            da.SelectCommand.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
            da.SelectCommand.Parameters.Add("@IncludeElectronicPayments", SqlDbType.Char, 1).Value = includeElectronicPayments;

            this.ds = new dsReceiptEODBalancing();
            this.ds.DataSetName = "dsReceiptEODBalancing";
            da.Fill(this.ds);
            da.Dispose();

            // Populate the report
            _reportDoc.SetDataSource(this.ds.Tables[1]);
            ds.Dispose();

            //set filter subreport data
            string filterParameters = string.Format("FILTER PARAMETERS:\tCashbox: {0}\t\tDate Selected: {1}\t\t\t\t"
                + "Produced By:  {2} on {3}", cbName, string.Format("{0:yyyy-MM-dd}", dt),
                userDetails.UserInit + " " + userDetails.UserSName, string.Format("{0:yyyy-MM-dd}", DateTime.Today));

            dsFilterParameters dsFilterParameters = new dsFilterParameters();
            DataRow row = dsFilterParameters.Tables[0].NewRow();
            row["FilterParameters"] = filterParameters;
            dsFilterParameters.Tables[0].Rows.Add(row);

            _reportDoc.Subreports["FilterParameters"].SetDataSource(dsFilterParameters.Tables[0]);
            dsFilterParameters.Dispose();

            //export the pdf file

            MemoryStream ms = new MemoryStream();
            
            var  s = _reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            _reportDoc.Dispose();
            BinaryReader sr = new BinaryReader(s);

            //stuff the PDF file into rendering stream
            //first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(sr.ReadBytes((int)s.Length));
            Response.End();


        }

    }
}