﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.Data.Datasets;
using ceTe.DynamicPDF.ReportWriter.ReportElements;


namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the page that creates the Admission of Guilt report
    /// </summary>
    public partial class ReceiptAtCourtViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString;
        private string login;
        private int autIntNo;
        //private string thisPage = "Receipts At Court Viewer";
        private string thisPageURL = "ReceiptAtCourtViewer.aspx";
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;

		protected string keywords = string.Empty;
		protected string title = string.Empty;
        protected string description = string.Empty;

       // private dsReceiptAtCourt ds;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 

            this.autIntNo = int.Parse(Request.QueryString["rAutIntNo"]);

            if (Request.QueryString["rReceiptStartDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }
            DateTime dtReceiptStartDate = DateTime.Parse(Request.QueryString["rReceiptStartDate"]);

            if (Request.QueryString["rReceiptEndDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error1");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            DateTime dtReceiptEndDate = DateTime.Parse(Request.QueryString["rReceiptEndDate"]);

            //set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            int CrtIntNo = int.Parse(Request.QueryString["CrtIntNo"]);
            string SortOrder = Request.QueryString["SortOrder"];
            string sortOrderText="";
            if (SortOrder == "RptDate")
                sortOrderText = "Receipt Date";
            else
                sortOrderText = "Court Date";

            DateTime? dtCourt;
            if (Request.QueryString["rCourtDate"] != null && Request.QueryString["rCourtDate"] != "0001-1-01")
                dtCourt = DateTime.Parse(Request.QueryString["rCourtDate"]);
            else
                dtCourt = null;

            string crtText = Request.QueryString["crtText"];
       
            // Setup the report 
            string reportName = "ReceiptAtCourt";
            ReportDocument reportDoc = new ReportDocument();

            AuthReportNameDB db = new AuthReportNameDB(this.connectionString);
            reportName = db.GetAuthReportName(this.autIntNo, reportName);
            if (reportName.Equals(string.Empty))
            {
                reportName = "ReceiptAtCourt.rpt";
                db.AddAuthReportName(autIntNo, "ReceiptsAtCourtReport.rpt", "GetReceiptsAtCourtlist", "System", "");           
            }
            //else
            //{
            //    reportName = "ReceiptAtCourt.rpt";
            //}

            string reportPath = Server.MapPath("reports/" + reportName);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = db.GetAuthReportNameTemplate(this.autIntNo, "ReceiptAtCourt");

            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error2"), reportName);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error3"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            reportDoc.Load(reportPath);

            // Get the PageMargins structure and set the margins for the report.
            PageMargins margins = reportDoc.PrintOptions.PageMargins;
            margins.leftMargin = 0;
            margins.topMargin = 0;
            margins.rightMargin = 0;
            margins.bottomMargin = 0;

            // Apply the page margins.
            reportDoc.PrintOptions.ApplyPageMargins(margins);

            // Retrieve the data source
            //string tempFile = printFile;
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("ReceiptsAtCourt", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = autIntNo;
            com.Parameters.Add("@RctDateFrom", SqlDbType.DateTime).Value = dtReceiptStartDate;
            com.Parameters.Add("@RctDateTo", SqlDbType.DateTime).Value = dtReceiptEndDate;
            if (dtCourt==null)
                com.Parameters.Add("@SumCourtDateFrom", SqlDbType.DateTime).Value =  DBNull.Value;
            else
                com.Parameters.Add("@SumCourtDateFrom", SqlDbType.DateTime).Value =  dtCourt;
            com.Parameters.Add("@CrtIntNo", SqlDbType.Int).Value = CrtIntNo;
            com.Parameters.Add("@SortOrder", SqlDbType.VarChar, 20).Value = SortOrder;

            SqlDataAdapter da = new SqlDataAdapter(com);
            dsReceiptAtCourt ds = new dsReceiptAtCourt(); 
            ds.DataSetName = "dsReceiptAtCourt";
            da.Fill(ds, "ReceiptsAtCourt");

            if (ds.Tables[0].Rows.Count == 0)
            {
                ds.Dispose();
                Response.Write((string)GetLocalResourceObject("strWriteMsg"));
                Response.End();
                return;
            }
            else
                // Bind the report to the data
                reportDoc.SetDataSource(ds.Tables[0]);

            ds.Dispose();

            string spaceStr="\t\t\t";
            string dtCourtText = string.Format("{0:dd/MM/yyyy}", dtCourt);
            if (dtCourtText == string.Empty)
            {
                dtCourtText = "[All dates]";
                spaceStr = "\t\t\t\t";
            }

            //set filter subreport data
            string filterParameters = string.Format("FILTER PARAMETERS:\nCourt Date:  {0}" + spaceStr + "Date Start:  {1}\nCourt: {2}\t\t\t\t\t\tDate End:  {3}\t\t\t\t\t\t\tOrder By: {4}"
                , dtCourtText, string.Format("{0:dd/MM/yyyy}", dtReceiptStartDate), crtText,
                string.Format("{0:dd/MM/yyyy}", dtReceiptEndDate), sortOrderText);

            dsFilterParameters dsFilterParameters = new dsFilterParameters();
            DataRow row = dsFilterParameters.Tables[0].NewRow();
            row["FilterParameters"] = filterParameters;
            dsFilterParameters.Tables[0].Rows.Add(row);

            reportDoc.Subreports["FilterParameters"].SetDataSource(dsFilterParameters.Tables[0]);
            dsFilterParameters.Dispose();

            //export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            reportDoc.Dispose();

            //stuff the PDF file into rendering stream
            //first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

        private void printDate_LaidOut(object sender, FormattedRecordAreaLaidOutEventArgs e)
        {
            e.FormattedTextArea.Text = "Printed: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        }

    }
}
