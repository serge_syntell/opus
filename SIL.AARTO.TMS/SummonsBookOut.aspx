<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SummonsBookOut" Codebehind="SummonsBookOut.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    &nbsp;
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:UpdatePanel ID="udpPrintSummons" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" border="0" height="90%" width="100%">
                                <tr>
                                    <td valign="top" style="height: 49px">
                                        <p align="center">
                                            <asp:Label ID="lblPageName" runat="server" Width="856px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                        <p align="center">
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="center">
                                        <asp:Panel ID="pnlGeneral" runat="server">
                                            <table style="width: 877px">
                                                <tr>
                                                    <td style="width: 160px; height: 11px">
                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="309px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                                    <td style="width: 255px; height: 11px" valign="middle">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px">
                                                        </asp:DropDownList></td>
                                                    <td style="width: 408px; height: 11px" valign="middle">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px; height: 11px;">
                                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="304px" Text="<%$Resources:lblSummonsNum.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 255px; height: 11px;" valign="middle">
                                                        <asp:TextBox ID="txtSummonsNo" runat="server" Width="215px"></asp:TextBox></td>
                                                    <td style="height: 11px; width: 408px;" valign="middle">
                                                        <asp:Button ID="btnBookOut" runat="server" Text=" <%$Resources:btnBookOut.Text %>" 
                                                            CssClass="NormalButton" Width="214px" OnClick="btnBookOut_Click" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        &nbsp;
                                        <br />
                                        <asp:DataGrid ID="dgPrintrun" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem" Width="695px"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                            Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgPrintrun_ItemCommand"
                                            CssClass="Normal" >
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="SumPrintFileName" HeaderText="<%$Resources:dgPrintrun.HeaderText %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoOfSummons" HeaderText="<%$Resources:dgPrintrun.HeaderText1 %>"></asp:BoundColumn>
                                                <asp:EditCommandColumn CancelText="Cancel" EditText="<%$Resources:dgPrintrun.EditText %>" HeaderText="<%$Resources:dgPrintrun.HeaderText2 %>"
                                                    UpdateText="Update"></asp:EditCommandColumn>
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <pager:AspNetPager id="dgPrintrunPager" runat="server" 
                                            showcustominfosection="Right" width="695px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                            FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"
                                            onpagechanged="dgPrintrunPager_PageChanged"  UpdatePanelId="udpPrintSummons"></pager:AspNetPager>
                                        &nbsp;<br />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle" /><asp:Label
                                    ID="Label1" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
