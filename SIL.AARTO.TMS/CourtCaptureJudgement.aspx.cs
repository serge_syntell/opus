using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.JudgementSnapshot;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Stalberg.TMS
{

    // Jake 2013-08-27 modified Populate court room function
    // use sp:SILCustom_CourtRoom_GetActiveCourtRoomList instead of CourtRoomList
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class CourtCaptureJudgement : System.Web.UI.Page
    {
        [Serializable]
        class PageCache
        {
            public bool? DirectByCaseNo { get; set; }
            public int? SelectedSumIntNo { get; set; }
        }

        /// <summary>
        /// Lists the process in recording judgement
        /// </summary>
        private enum JudgementProcess
        {
            None,
            SelectCourt,
            GetCourtRoll,
            SummonsJudgement,
            //GetSumChargeRoll,
            DeferredPaymentCheck,
            SumChargeJudgement,
            DeferredPaymentJudgement
        }

        #region Fields

        PageCache cache;

        private string connectionString = String.Empty;
        private string login;
        private int autIntNo;

        JudgementList summonsList
        {
            get
            {
                return ViewState["PageSumList"] as JudgementList;
            }
            set
            {
                ViewState["PageSumList"] = value;
            }
        }
        JudgementList sumChargeList
        {
            get
            {
                return ViewState["SumChargeListSource"] as JudgementList;
            }
            set
            {
                ViewState["SumChargeListSource"] = value;
            }
        }

        private decimal crDefaultContempt;
        private PersonaType personaType = PersonaType.Accused;
        int SumIntNo
        {
            get
            {
                if (ViewState["SumIntNo"] != null)
                    return (int)ViewState["SumIntNo"];
                else
                    return 0;
            }
            set { ViewState["SumIntNo"] = value; }
        }

        protected string styleSheet;
        protected string backgroundImage;
        protected string title;
        protected DateTime cDate;
        private bool usePostalReceipt = false;

        private const string DATE_FORMAT = "yyyy-MM-dd";
        private const string DATE_AND_TIME_FORMAT = "yyyy-MM-dd_HH-mm";

        JudgementProcess JudgeProcess
        {
            get
            {
                if (ViewState["JudgeProcess"] != null)
                    return (JudgementProcess)ViewState["JudgeProcess"];
                else
                    return JudgementProcess.None;
            }
            set { ViewState["JudgeProcess"] = value; }
        }
        int JudgementsLeft
        {
            get
            {
                if (ViewState["JudgementsLeft"] != null)
                    return (int)ViewState["JudgementsLeft"];
                else
                    return 0;
            }
            set { ViewState["JudgementsLeft"] = value; }
        }

        int TotalJudgementsRequired
        {
            get
            {
                if (ViewState["TotalJudgementsRequired"] != null)
                    return (int)ViewState["TotalJudgementsRequired"];
                else
                    return 0;
            }
            set { ViewState["TotalJudgementsRequired"] = value; }
        }

        int CJTIntNoOfPayment
        {
            get
            {
                if (ViewState["CJTIntNoOfPayment"] != null)
                    return (int)ViewState["CJTIntNoOfPayment"];
                else
                    return 0;
            }
            set { ViewState["CJTIntNoOfPayment"] = value; }
        }

        // Oscar 20110225 - added for 4410C for contempt
        int FirstMainChargeIndex
        {
            get
            {
                if (ViewState["FirstMainChargeIndex"] != null)
                    return (int)ViewState["FirstMainChargeIndex"];
                else
                    return 1;
            }
            set
            {
                if (value <= this.sumChargeList.Count)
                    ViewState["FirstMainChargeIndex"] = value;
            }
        }

        string WOAType
        {
            get
            {
                if (ViewState["WOAType"] != null)
                {
                    return ViewState["WOAType"].ToString();
                }
                else
                {
                    return "N";
                }
            }
            set
            {
                ViewState["WOAType"] = value;
            }
        }


        bool IsFirstMainCharge
        {
            get { return this.sumChargeList.CurrentIndex == FirstMainChargeIndex ? true : false; }
        }

        bool CaptureAmountDetails
        {
            get { return this.txtContemptAmount.Visible ? true : false; }
        }

        public int CTNLAIntNo
        {
            get
            {
                if (ViewState["CTNLAIntNo"] != null)
                {
                    return Convert.ToInt32(ViewState["CTNLAIntNo"]);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["CTNLAIntNo"] = value;
            }
        }

        private string cRTranType = "WOA";
        private string cRTranDescr = "Next {0} WOA number for court.";

        //Iris 2014-02-27 add for AutRule 6310
        protected string _ISHRK = "none";

        #endregion


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            //ViewState["AutIntNo"] = this.autIntNo;  //Removed by Oscar , unnecessary

            // Iris 2014-02-27 added
            var authRule = new AuthorityRulesDB(this.connectionString).GetAuthorityRule(autIntNo, "6310");
            if (authRule != null && authRule.ARString.Trim().Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                this._ISHRK = "";
            }

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (ViewState["PageCache"] != null)
                cache = ViewState["PageCache"] as PageCache;
            else
            {
                cache = new PageCache();
                ViewState["PageCache"] = cache;
            }

            if (ViewState["CrDefaultContempt"] != null)
                this.crDefaultContempt = decimal.Parse(ViewState["CrDefaultContempt"].ToString());

            if (ViewState["CDate"] != null)
                this.cDate = DateTime.Parse(ViewState["CDate"].ToString());

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                //mrs 20080621 set up authrule defaults if necessary
                CheckAuthRules(autIntNo);

                this.ddlReceivedFrom.SelectedIndex = 3;//default to accused
                this.GetCourts();
                this.ddlReceivedFrom_SelectedIndexChanged(this.ddlReceivedFrom, null);
                this.SetControlsForStatus(JudgementProcess.SelectCourt);
            }

            RegisterScriptUnblock();
        }


        void RegisterScriptUnblock()
        {

            ScriptManager.RegisterStartupScript(this.upd, GetType(), "upd", @"$.unblockUI();", true);
        }

        private void CheckAuthRules(int autIntNo)
        {
            ////7111	1	5600	Rule for grace period (no of days) for payment after judgement captured	7	NULL	No. of Days (default = 7)
            //AuthorityRulesDB ar1DB = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails ard1 = ar1DB.GetAuthorityRulesDetailsByCode(autIntNo, "5600", "Rule for grace period (no of days) for payment after judgement captured", 7, "", "No. of Days (default = 7)", this.login);
            ////functions handle the rest

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails ard1 = new AuthorityRulesDetails();
            ard1.AutIntNo = autIntNo;
            ard1.ARCode = "5600";
            ard1.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(ard1, this.connectionString);
            KeyValuePair<int, string> value1 = authRule.SetDefaultAuthRule();

            //7127	1	6100	Rule for period (no of days) within which the court roll must be captured (payments not allowed)	14	NULL	No. of Days (default = 14)
            //AuthorityRulesDB ar2DB = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails ard2 = ar2DB.GetAuthorityRulesDetailsByCode(autIntNo, "6100", "Rule for period (no of days) within which the court roll must be captured (payments not allowed)", 14, "", "No. of Days (default = 7)", this.login);
            //functions handle the rest

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails ard2 = new AuthorityRulesDetails();
            ard2.AutIntNo = this.autIntNo;
            ard2.ARCode = "6100";
            ard2.LastUser = this.login;

            DefaultAuthRules authRule1 = new DefaultAuthRules(ard2, this.connectionString);
            KeyValuePair<int, string> value2 = authRule1.SetDefaultAuthRule();
        }

        private void GetJudgementTypes(int crtIntNo, JudgementProcess process)
        {
            this.cboJudgementType.Items.Clear();

            CourtJudgementTypeDB db = new CourtJudgementTypeDB(this.connectionString);
            DataSet ds = db.GetListForCourt(crtIntNo, this.login);
            ListItem item;

            item = new ListItem();
            item.Value = string.Format("{0}~{1}", -1, -1);
            item.Text = (string)GetLocalResourceObject("item.Text");
            this.cboJudgementType.Items.Add(item);


            Dictionary<int, string> lookups =
                CourtJudgementTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);


            foreach (DataRow row in ds.Tables[0].Rows)
            {
                item = new ListItem();
                //item.Value = string.Format("{0}~{1}", row["CJTIntNo"], row["CJTCode"]);
                //item.Text = row["Judgement"].ToString();
                //this.cboJudgementType.Items.Add(item);

                // Oscar 20110224 - added script check                   
                if (row["IsSummonsLevel"] == DBNull.Value)
                {
                    Application["ErrorMessage"] = (string)GetLocalResourceObject("ErrorMessage");
                    Response.Redirect("Error.aspx?error");
                    return;
                }
                // Oscar 20101118 - changed, differentiate between Summons Judgement and Charge Judgement
                switch (process)
                {
                    case JudgementProcess.SummonsJudgement:
                        if (Convert.ToBoolean(row["IsSummonsLevel"]).Equals(true))
                        {
                            if (row["CJTCode"].ToString().Equals("16"))
                            {
                                if (this.summonsList != null && this.summonsList.CurrentValue != null && this.summonsList.CurrentValue.NotFilmType.Equals('M'))
                                {
                                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                                    continue;
                                }
                            }

                            int cJTIntNo = (int)row["CJTIntNo"];
                            if (lookups.ContainsKey(cJTIntNo))
                            {
                                cboJudgementType.Items.Add(new ListItem(lookups[cJTIntNo], string.Format("{0}~{1}", cJTIntNo, row["CJTCode"])));
                            }
                            //item.Value = string.Format("{0}~{1}", row["CJTIntNo"], row["CJTCode"]);
                            //item.Text = row["Judgement"].ToString();
                            //this.cboJudgementType.Items.Add(item);
                        }

                        break;

                    case JudgementProcess.SumChargeJudgement:
                        if (!Convert.ToBoolean(row["IsSummonsLevel"]).Equals(true))
                        {
                            int cJTIntNo = (int)row["CJTIntNo"];
                            if (lookups.ContainsKey(cJTIntNo))
                            {
                                cboJudgementType.Items.Add(new ListItem(lookups[cJTIntNo], string.Format("{0}~{1}", cJTIntNo, row["CJTCode"])));
                            }
                            //item.Value = string.Format("{0}~{1}", row["CJTIntNo"], row["CJTCode"]);
                            //item.Text = row["Judgement"].ToString();
                            //this.cboJudgementType.Items.Add(item);
                            // Oscar 20101214 - added for cjtIntNo of payment, used by deferred payment
                            if (Convert.ToInt32(row["CJTCode"]) == 5)
                                CJTIntNoOfPayment = Convert.ToInt32(row["CJTIntNo"]);
                        }

                        break;

                    default:
                        break;
                }

            }
        }

        // Populate the ddl court        
        private void GetCourts()
        {
            this.cboCourt.Items.Clear();
            ListItem item;

            CourtDB db = new CourtDB(this.connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(this.autIntNo);
            while (reader.Read())
            {
                item = new ListItem();
                item.Value = reader["CrtIntNo"].ToString();
                item.Text = reader["CrtDetails"].ToString();
                this.cboCourt.Items.Add(item);
            }
            reader.Close();

            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("cboCourt.Items");
            this.cboCourt.Items.Insert(0, item);
            this.cboCourt.SelectedIndex = 0;

            //cboCourtDates.Enabled = false;
            DDLDetailsStatus(DetailsStatus.Initialized);   // Oscar 20100920
        }

        private void SetControlsForStatus(JudgementProcess process)
        {
            this.lblError.Text = string.Empty;

            this.pnlDetails.Visible = false;
            this.pnlCourtRoll.Visible = false;
            this.pnlJudgement.Visible = false;

            // Oscar 20100915 - differentiate between ChargesListPage's next and CaptureJudgementPage's next
            this.pnlChargesListNext.Visible = false;
            this.pnlCaptureNext.Visible = false;

            // Oscar 20101122 -added
            this.pnlJudgementType.Visible = false;
            this.trSelectJudgementType.Visible = false;
            this.trVerdict.Visible = false;
            this.trSentence.Visible = false;
            this.trSentenceAmount.Visible = false;
            this.btnCancel.Visible = false;
            this.btnConfirm.Visible = false;
            this.trContemptAmount.Visible = false;
            this.trOtherAmount.Visible = false;
            this.trNewCourtDate.Visible = false;
            this.trReceiptDate.Visible = false;
            this.pnlAddress.Visible = false;
            this.trDeferredPayments.Visible = false;
            this.trDeferredPaymentAble.Visible = false;
            this.btnRecordJudgement.Visible = true;
            this.lblRemandedSummonsFlag.Visible = false;
            this.pnlBenchWOA.Visible = false;
            this.pnlDoubleWOA.Visible = false;
            //this.trContinueToCharge.Visible = false;

            this.dtpReceiptDate.Text = string.Empty;

            switch (process)
            {
                case JudgementProcess.SelectCourt:
                    this.pnlDetails.Visible = true;

                    break;
                case JudgementProcess.GetCourtRoll:
                    this.pnlDetails.Visible = true;
                    this.pnlCourtRoll.Visible = true;
                    break;

                // Oscar 20101117 - added, for summons judgement
                case JudgementProcess.SummonsJudgement:
                    this.pnlJudgement.Visible = true;
                    this.pnlChargesListNext.Visible = true;
                    this.pnlJudgementType.Visible = true;
                    this.trSelectJudgementType.Visible = true;
                    this.trVerdict.Visible = true;
                    this.trSentence.Visible = true;
                    //this.trDeferredPaymentAble.Visible = true;
                    //this.trContinueToCharge.Visible = true;

                    SetCaptureJudgementContent(process);

                    break;

                case JudgementProcess.DeferredPaymentCheck:
                    this.pnlJudgement.Visible = true;
                    this.pnlChargesListNext.Visible = true;
                    this.pnlJudgementType.Visible = true;
                    this.trDeferredPaymentAble.Visible = true;
                    this.btnRecordJudgement.Visible = false;
                    break;

                case JudgementProcess.SumChargeJudgement:

                    this.pnlCaptureNext.Visible = true;  // Oscar 20100915 - show capture's next

                    this.pnlJudgement.Visible = true;
                    this.pnlJudgementType.Visible = true;
                    ////BD dont show new court date if it is not a remanded case...
                    //this.dtpNewCourtDate.Visible = false;
                    //this.lblNewCourtDate.Visible = false;
                    ////BD dont show deferred payment grid unless it is a payment case
                    //this.gridDeferredPayments.Visible = false;
                    //this.lblDeferredPayments.Visible = false;
                    //this.lblRemainderAmount.Visible = false;
                    this.trSelectJudgementType.Visible = true;
                    this.trVerdict.Visible = true;
                    this.trSentence.Visible = true;
                    //this.trSentenceAmount.Visible = true;                        
                    //this.trContemptAmount.Visible = true;
                    //this.trOtherAmount.Visible = true;

                    SetCaptureJudgementContent(process);

                    //DeferredPayments_Bind(); //+

                    break;

                case JudgementProcess.DeferredPaymentJudgement:
                    this.pnlJudgement.Visible = true;
                    this.pnlJudgementType.Visible = true;
                    this.trDeferredPayments.Visible = true;

                    SetCaptureJudgementContent(process);

                    //BD need to set the datasource of the deferred payment grid
                    DeferredPayments_Bind(); //?
                    break;
            }
        }

        //BD bind data for deferred payments
        private void DeferredPayments_Bind()
        {
            SumChargeDB db = new SumChargeDB(this.connectionString);
            //DataSet ds = db.GetDeferredPayments(this.sumChargeList.CurrentValue.SChIntNo);
            DataSet ds = db.GetDeferredPayments(this.summonsList.CurrentValue.SumIntNo);
            this.gridDeferredPayments.DataSource = ds;
            //this.gridDeferredPayments.DataKeyField = "SCDefPayIntNo";
            this.gridDeferredPayments.DataKeyField = "PaymentDate";
            this.gridDeferredPayments.DataBind();

            decimal SumPayment = this.GetDeferredPaymentsAmount();

            //Oscar 20101208 - changed
            decimal SumTotal = GetDeferredPaymentsAmountOriginal();

            //update by Rachel 20140811 for 5337
            //this.lblRemainderAmount.Text = string.Format("(RemainderAmount={0})", SumTotal - SumPayment);

            this.lblRemainderAmount.Text = string.Format(CultureInfo.InvariantCulture, "(RemainderAmount={0})", SumTotal - SumPayment);
            //end update by Rachel 20140811 for 5337

            CheckCommitDeferredPayments();
        }

        protected void cboCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            int crtIntNo = int.Parse(this.cboCourt.SelectedValue);
            if (crtIntNo < 1)
            {
                this.SetControlsForStatus(JudgementProcess.SelectCourt);
                return;
            }

            // Populate the judgement type list
            GetJudgementTypes(crtIntNo, JudgementProcess.SummonsJudgement); //?

            //// Enable the DTP
            //this.dtpCourtDate.Enabled = true;
            //this.dtpCourtDate.Value = DateTime.Today;

            // Populate the court rooms
            this.cboCourtRoom.Items.Clear();
            ListItem item;

            // jake 2013-09-11 removed translate from lookup table
            //Jake 2013-08-27  only populate court room which isactive equal true
            //-----------------------------------------------------------------
            //List<SIL.AARTO.DAL.Entities.CourtRoom> courtRooms = CourtRoomCache.GetAvailableCourtRoom(crtIntNo);


            CourtRoomDB db = new CourtRoomDB(this.connectionString);
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            SqlDataReader reader = db.GetCourtRoomListByCrtRStatusFlag(crtIntNo);//db.GetCourtRoomList(crtIntNo);

            this.cboCourtRoom.DataSource = reader;
            this.cboCourtRoom.DataValueField = "CrtRIntNo";
            this.cboCourtRoom.DataTextField = "CrtRoomDetails";
            this.cboCourtRoom.DataBind();
            reader.Close();

            //Dictionary<int, string> lookups =
            //    CourtRoomLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);

            //foreach (SIL.AARTO.DAL.Entities.CourtRoom cr in courtRooms)
            //{

            //    if (lookups.ContainsKey(cr.CrtRintNo))
            //    {
            //        cboCourtRoom.Items.Add(new ListItem(lookups[cr.CrtRintNo], cr.CrtRintNo.ToString()));
            //    }
            //}
            //----------------------------------------------------------------
            //while (reader.Read())
            //{
            //    int crtRIntNo = (int)reader["CrtRIntNo"];
            //    if (lookups.ContainsKey(crtRIntNo))
            //    {
            //        cboCourtRoom.Items.Add(new ListItem(lookups[crtRIntNo], crtRIntNo.ToString()));
            //    }
            //}
            //while (reader.Read())
            //{
            //    item = new ListItem();
            //    item.Value = reader["CrtRIntNo"].ToString();
            //    item.Text = reader["CrtRoomDetails"].ToString();

            //    this.cboCourtRoom.Items.Add(item);
            //}
            //reader.Close();

            // Insert the dummy item
            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("cboCourtRoom.Text");
            this.cboCourtRoom.Items.Insert(0, item);
            this.cboCourtRoom.SelectedIndex = 0;

            this.cboCourtRoom.Enabled = true;

            // Obtain court details
            CourtDB court = new CourtDB(this.connectionString);
            Stalberg.TMS.CourtDetails courtDetails = court.GetCourtDetails(crtIntNo);

            this.crDefaultContempt = courtDetails.CrDefaultContempt;
            ViewState.Add("CrDefaultContempt", this.crDefaultContempt);

            //this.cboCourtDates.Enabled = false;  
            DDLDetailsStatus(DetailsStatus.CourtSelected);    //oscar 20100920 - Change
        }

        protected void cboCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            //mrs 20081130 added
            //int autIntNo = Convert.ToInt32(ViewState["AutIntNo"].ToString()); // Removed by Oscar, unnecessary
            int crtRIntNo = int.Parse(this.cboCourtRoom.SelectedValue);
            if (crtRIntNo < 1)
            {
                this.SetControlsForStatus(JudgementProcess.SelectCourt);
                return;
            }

            // Populate the court dates
            this.cboCourtDates.Items.Clear();
            ListItem item;
            CourtDatesDB db = new CourtDatesDB(this.connectionString);

            //SqlDataReader reader = db.GetCourtDatesList(crtRIntNo, autIntNo);
            //SqlDataReader reader = db.GetCourtDatesList(crtRIntNo, this.autIntNo);    // Oscar 20100920 - Change 
            SqlDataReader reader = db.GetWarrantAndSummonsCountDatesList(crtRIntNo, this.autIntNo);   // Jake 2013-10-29 added new function
            while (reader.Read())
            {
                item = new ListItem();
                item.Value = String.Format("{0:yyyy-MM-dd}", reader["CDate"]);  //reader["CDIntNo"].ToString();
                item.Text = String.Format("{0:yyyy-MM-dd}", reader["CDate"]);

                this.cboCourtDates.Items.Add(item);
            }
            reader.Close();

            // Insert the dummy item
            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("cboCourtDatesItem.Text");
            this.cboCourtDates.Items.Insert(0, item);
            this.cboCourtDates.SelectedIndex = 0;

            //this.cboCourtDates.Enabled = true;
            DDLDetailsStatus(DetailsStatus.RoomSelected);     // Oscar 20100920 - Change

            //// Obtain court details
            //CourtDB court = new CourtDB(this.connectionString);
            //Stalberg.TMS.CourtDetails courtDetails = court.GetCourtDetails(crtIntNo);

            //this.crDefaultContempt = courtDetails.CrDefaultContempt;
            //ViewState.Add("CrDefaultContempt", this.crDefaultContempt);
        }

        protected void cboCourtDates_SelectedIndexChanged(object sender, EventArgs e)
        {
            //mrs 20081130 added
            //int cdIntNo = Convert.ToInt32(this.cboCourtDates.SelectedValue.ToString());
            //if (cdIntNo < 1)
            if (this.cboCourtDates.SelectedIndex < 0)
            {
                this.SetControlsForStatus(JudgementProcess.SelectCourt);
                return;
            }
            DateTime dt;

            if (!DateTime.TryParse(this.cboCourtDates.SelectedItem.ToString(), out dt))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                this.lblError.Visible = true;
                return;
            }
            ViewState["CourtDate"] = dt;

            DDLDetailsStatus(DetailsStatus.DateSelected);
            //Jake 2014-05-26 added when court date change ,populate summons in grid
            btnSelectCourtRoom_Click(sender, e);
        }

        protected void btnSelectCourtRoom_Click(object sender, EventArgs e)
        {
            // Check that there is a selected court room
            int crtRIntNo = int.Parse(this.cboCourtRoom.SelectedValue);
            if (crtRIntNo < 1)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                this.pnlCourtRoll.Visible = false;
                return;
            }

            //Jerry 2012-04-23 add for Shirley pointed at here is a bug
            //int cdIntNo = Convert.ToInt32(this.cboCourtDates.SelectedValue.ToString());
            //if (cdIntNo < 1)
            if (this.cboCourtDates.SelectedIndex < 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text56");
                this.pnlCourtRoll.Visible = false;
                return;
            }

            DateTime dt;

            dt = Convert.ToDateTime(ViewState["CourtDate"].ToString());
            if (dt.Ticks > DateTime.Now.Ticks)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                this.pnlCourtRoll.Visible = false;
                return;
            }

            cache.SelectedSumIntNo = null;
            this.lnkNext.Visible = true;
            cache.DirectByCaseNo = null;

            this.lblCourtRollTitle.Text = string.Format((string)GetLocalResourceObject("lblCourtRollTitle.Text"), dt.ToString("yyyy/MM/dd"));


            bool reload = true;
            //this.summonsList = GetJudgementListSource(true, null);
            if (sender.GetType() != typeof(Button)) { reload = false; }

            int totalCount = CourtRollDataBind(reload);

            if (!string.IsNullOrWhiteSpace(this.txtCaseNo.Text) && this.summonsList.Count == 1)
            {
                cache.DirectByCaseNo = true;
                //SetNextJudgement(false);
                //2013-04-08 Henry mdofiy
                SetNextJudgementSums();
                return;
            }

            if (totalCount <= 0)
            {
                this.lblCourtRoll.Text = string.Format((string)GetLocalResourceObject("lblCourtRoll.Text"), this.cboCourtRoom.SelectedItem.Text, dt.ToString("dddd, yyyy-MM-dd"));
                this.btnCourtRollAccept.Visible = false;
            }
            else
            {
                this.btnCourtRollAccept.Visible = true;
                this.lblCourtRoll.Text = string.Empty;
            }

            this.SetControlsForStatus(JudgementProcess.GetCourtRoll);
        }

        protected void btnPrintCourtReport_Click(object sender, EventArgs e)
        {
            PageToOpen page = null;
            List<PageToOpen> pages = new List<PageToOpen>();

            // Check that there is a selected court room
            //string sReportType = "P";
            int crtRIntNo = int.Parse(this.cboCourtRoom.SelectedValue);
            if (crtRIntNo < 1)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                return;
            }

            DateTime dtCourtDate = Convert.ToDateTime(ViewState["CourtDate"].ToString());

            if (dtCourtDate.Ticks > DateTime.Now.Ticks)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                return;
            }

            // Get the court roll for that day
            this.lblCourtRollTitle.Text = string.Format((string)GetLocalResourceObject("lblCourtRollTitle.Text"), dtCourtDate.ToString("yyyy-MM-dd"));

            CourtRoomDB db = new CourtRoomDB(this.connectionString);
            DataSet ds = db.GetSummonsForJudgementReport(crtRIntNo, dtCourtDate); //mrs 20081130 added separate SP for the report - can't use the judgement proc
            //this.grdCourtRoll.DataSource = ds;
            //this.grdCourtRoll.DataKeyNames = new string[] { "SumIntNo" };
            //this.grdCourtRoll.DataBind();

            if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                this.lblCourtRoll.Visible = true;
                this.lblCourtRoll.Text = string.Format((string)GetLocalResourceObject("lblCourtRoll.Text"),
                    this.cboCourtRoom.SelectedItem.Text, dtCourtDate.ToString("dddd, yyyy-MM-dd"));
                this.btnCourtRollAccept.Visible = false;

                this.ViewState.Add("CourtRoll", ds);

                this.SetControlsForStatus(JudgementProcess.GetCourtRoll);
            }
            else
            {
                //dt = dt.AddDays((double)nDays);
                //if (dt.CompareTo(DateTime.Now) <= 0)
                //    sReportType = "F";
                page = new PageToOpen(this, "CourtJudgementRecordViewer.aspx");
                page.Parameters.Add(new QSParams("CrtIntNo", this.cboCourt.SelectedValue));
                page.Parameters.Add(new QSParams("CrtRIntNo", crtRIntNo));
                page.Parameters.Add(new QSParams("CourtDate", dtCourtDate.ToString("yyyy-MM-dd")));
                //page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                pages.Add(page);

                Helper_Web.BuildPopup(pages.ToArray());
            }
            //PunchStats805806 enquiry CaseResult
        }

        // Oscar 20100916 - Rewrite this function
        protected void btnCourtRollAccept_Click(object sender, EventArgs e)
        {
            ViewState.Remove("CDate");

            //this.SetNextJudgement(false);
            //2013-04-08 Henry mdofiy
            SetNextJudgementSums();
            //this.SetControlsForStatus(JudgementProcess.SummonsJudgement);
        }

        // 2013-04-08 Henry separate SetNextJudgement
        private void SetNextJudgementSums(int? sumIntNo = null)
        {
            OffenderJudgement oj;
            // Oscar 2013-02-18 changed for case no filter
            if (!cache.SelectedSumIntNo.HasValue)
            {
                oj = this.summonsList.GetNext();
                if (oj == null &&
                    grdCourtRollPager.CurrentPageIndex + 1 <= grdCourtRollPager.PageCount)
                {
                    grdCourtRollPager.CurrentPageIndex++;
                    int totalCount = 0;
                    this.summonsList = GetSummonsJudgementList(out totalCount);
                    if (summonsList.Count > 0)
                    {
                        oj = this.summonsList.GetNext();
                    }
                }
            }
            else if (sumIntNo.HasValue)
            {
                this.summonsList.LocateCurrent(sumIntNo.Value, null);
                oj = this.summonsList.CurrentValue;
            }
            else
                oj = null;

            JudgeProcess = JudgementProcess.SummonsJudgement;

            if (oj == null)
            {
                //judgeProcess = JudgementProcess.None;
                this.SetControlsForStatus(JudgementProcess.SelectCourt);
                if (!cache.DirectByCaseNo.GetValueOrDefault())
                    btnSelectCourtRoom_Click(new object(), new EventArgs());
                return;
            }
            SumIntNo = oj.SumIntNo;
            this.SetControlsForStatus(JudgementProcess.SummonsJudgement);
        }

        // 2013-04-08 Henry separate SetNextJudgement
        private void SetNextJudgementCharges()
        {
            OffenderJudgement oj = this.summonsList.CurrentValue;
            if (oj == null)
            {
                //judgeProcess = JudgementProcess.None;
                this.SetControlsForStatus(JudgementProcess.SelectCourt);
                if (!cache.DirectByCaseNo.GetValueOrDefault())
                    btnSelectCourtRoom_Click(new object(), new EventArgs());
                return;
            }
            SumIntNo = oj.SumIntNo;

            //this.sumChargeList = GetJudgementListSource(false, SumIntNo);
            this.sumChargeList = GetSumChargesJudgementList(SumIntNo);

            TotalJudgementsRequired = 0;

            for (int i = 0; i < this.sumChargeList.Count; i++)
            {
                if (this.sumChargeList[i].SChIsMain)
                {
                    JudgementsLeft++;
                    TotalJudgementsRequired++;
                }
            }
            FirstMainChargeIndex = 1;   // Oscar 20110225 - added for 4410C for contempt
            InitializeCaptureJudgement(0, JudgementProcess.SumChargeJudgement);
        }

        // Oscar 20100916 - add function "NextCharge"
        private void SetNextCharge()
        {
            // dls 2011-05-25 - we only want to show the user the Main charges. Skip over the Alternate charges
            OffenderJudgement oj = this.sumChargeList.GetNext();

            if (oj != null)
            {
                bool isMainCharge = oj.SChIsMain;

                while (!isMainCharge && oj != null)
                {
                    oj = this.sumChargeList.GetNext();
                    if (oj != null)
                        isMainCharge = oj.SChIsMain;
                }
            }

            if (oj == null)
            {
                //SetNextJudgement(false);
                //2013-04-08 Henry mdofiy
                SetNextJudgementSums();

                return;
            }
            InitializeCaptureJudgement(this.sumChargeList.CurrentIndex - 1, JudgementProcess.SumChargeJudgement);  //?
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            //SetNextJudgement(false);
            //2013-04-08 Henry mdofiy
            SetNextJudgementSums();
        }

        //Oscar 20100916 - add next charge link click
        protected void lnkCaptureNext_Click(object sender, EventArgs e)
        {
            SetNextCharge();
        }

        protected void cboJudgementType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblError.Text = "";
            this.WOAType = "S";
            //this.btnRecordJudgement.Enabled = (this.cboJudgementType.SelectedIndex > 0);   

            //Barry Dickson - need to set default contempt amount based on court and if it is a warrant judgement
            string[] split = this.cboJudgementType.SelectedValue.Split(new char[] { '~' });
            int type = int.Parse(split[0]);
            int code = int.Parse(split[1]);

            this.btnRecordJudgement.Enabled = code > 0;   //Oscar 20101208 - added

            //BD dont show new court date if it is not a remanded case...
            //this.dtpNewCourtDate.Visible = false;
            //this.lblNewCourtDate.Visible = false;
            this.trNewCourtDate.Visible = false;

            //BD dont show deferred payment grid unless it is a payment case
            //this.gridDeferredPayments.Visible = false;
            //this.lblDeferredPayments.Visible = false;
            //this.lblRemainderAmount.Visible = false;
            this.trDeferredPayments.Visible = false;

            btnCancel.Visible = false;
            btnConfirm.Visible = false;

            this.pnlAddress.Visible = false;

            this.trSentence.Visible = true;
            this.trVerdict.Visible = true;
            this.trReceiptDate.Visible = false;

            this.trSentenceAmount.Visible = false;
            this.trContemptAmount.Visible = false;
            this.trOtherAmount.Visible = false;

            // when judgement type changed we need to hidden bench WOA and double WOA panel here
            this.pnlBenchWOA.Visible = false;
            this.pnlDoubleWOA.Visible = false;
            this.btnRecordJudgement.Visible = true;

            //Edge 2013-10-31 added
            OffenderJudgement oj = null;
            if (this.sumChargeList != null)
            {
                oj = this.sumChargeList.CurrentValue;

                if (oj != null)
                {
                    //update by Rachel 20140811 for 5337

                    //this.txtContemptAmount.Text = IsFirstMainCharge ? this.crDefaultContempt.ToString("N") : "0.00";
                    //this.txtOtherAmount.Text = oj.OtherAmount.ToString("N");
                    //this.txtSentenceAmount.Text = oj.SentenceAmount.ToString("N");
                    this.txtContemptAmount.Text = IsFirstMainCharge ? this.crDefaultContempt.ToString("N", CultureInfo.InvariantCulture) : "0.00";
                    this.txtOtherAmount.Text = oj.OtherAmount.ToString("N", CultureInfo.InvariantCulture);
                    this.txtSentenceAmount.Text = oj.SentenceAmount.ToString("N", CultureInfo.InvariantCulture);
                    //end update by Rachel 20140811 for 5337
                }
            }

            this.txtSentenceAmount.ReadOnly = false;
            this.txtContemptAmount.ReadOnly = false;
            this.txtOtherAmount.ReadOnly = false;

            // Oscar 20110225 - disabled this mystery for 4410C
            //if (this.crDefaultContempt.ToString().Equals(txtContemptAmount.Text))
            //{
            //    this.txtContemptAmount.Text = "0.00";
            //}

            switch (code)
            {
                case (int)CourtJudgementType.Payment_5:
                    this.trSentenceAmount.Visible = true;
                    this.trContemptAmount.Visible = true;
                    this.trOtherAmount.Visible = true;

                    break;

                case (int)CourtJudgementType.Remand_19:
                    this.trNewCourtDate.Visible = true;

                    break;

                case (int)CourtJudgementType.WOA_20:
                    string woaType = string.Empty;
                    CourtRoomDB db = new CourtRoomDB(this.connectionString);
                    db.CheckWOAType(this.summonsList.CurrentValue.SumIntNo, out woaType);
                    this.WOAType = woaType;
                    if (WOAType == "D")
                    {
                        //update by Rachel 20140811 for 5337
                        //txtContemptAmount.Text = (this.crDefaultContempt * 2).ToString("N");
                        txtContemptAmount.Text = (this.crDefaultContempt * 2).ToString("N", CultureInfo.InvariantCulture);
                        //end update by Rachel 20140811 for 5337
                    }
                    else
                    {
                        //update by Rachel 20140811 for 5337
                        //txtContemptAmount.Text = this.crDefaultContempt.ToString("N");
                        txtContemptAmount.Text = this.crDefaultContempt.ToString("N", CultureInfo.InvariantCulture);
                        //end update by Rachel 20140811 for 5337
                    }

                    trContemptAmount.Visible = true;	// Oscar 20110322 added for woa contemptamount
                    break;
                case (int)CourtJudgementType.ReduceFineAmountPayment_21:
                    this.trSentenceAmount.Visible = true;
                    this.trContemptAmount.Visible = true;

                    //this.txtSentenceAmount.Text = this.lblRevisedFineAmount.Text;
                    break;

                //Jake 2015-01-22 modified ,set sentence and contempt amount to zero when cjt is guilty or not guilty, bontq ref:1804

                case (int)CourtJudgementType.Guilty_26:   //add by Rachel 2014-11-12 for 5352
                //this.trSentenceAmount.Visible = true;
                //this.trContemptAmount.Visible = true;
                //break;
                case (int)CourtJudgementType.NotGuilty_27:  //add by Rachel 2014-11-12 for 5352
                    this.trSentenceAmount.Visible = true;
                    this.trContemptAmount.Visible = true;
                    this.txtContemptAmount.Text = 0.ToString("N", CultureInfo.InvariantCulture);
                    this.txtSentenceAmount.Text = 0.ToString("N", CultureInfo.InvariantCulture);

                    break;
                case (int)CourtJudgementType.PaidAtCourt_49:
                    this.trSentence.Visible = false;
                    this.trVerdict.Visible = false;
                    this.trSentenceAmount.Visible = true;
                    this.trContemptAmount.Visible = true;
                    this.trOtherAmount.Visible = true;
                    this.trReceiptDate.Visible = true;
                    this.pnlAddress.Visible = true;
                    this.ddlReceivedFrom.SelectedIndex = 3;
                    ddlReceivedFrom_SelectedIndexChanged(ddlReceivedFrom, null);

                    this.txtSentenceAmount.ReadOnly = true;
                    this.txtContemptAmount.ReadOnly = true;
                    this.txtOtherAmount.ReadOnly = true;

                    //update by Rachel 20140811 for 5337
                    //this.txtSentenceAmount.Text = oj.RevisedFineAmount.ToString("N");
                    //this.txtContemptAmount.Text = oj.ContemptAmount.ToString("N");
                    //this.txtOtherAmount.Text = oj.OtherAmount.ToString("N");
                    this.txtSentenceAmount.Text = oj.RevisedFineAmount.ToString("N", CultureInfo.InvariantCulture);
                    this.txtContemptAmount.Text = oj.ContemptAmount.ToString("N", CultureInfo.InvariantCulture);
                    this.txtOtherAmount.Text = oj.OtherAmount.ToString("N", CultureInfo.InvariantCulture);
                    //end update by Rachel 20140811 for 5337
                    break;

                case (int)CourtJudgementType.MoreChargeJudgement_59:
                    //Edge 2013-11-20 added
                    if (this.sumChargeList != null)
                    {
                        this.sumChargeList.CurrentIndex = 0;
                    }
                    SetControlsForStatus(JudgementProcess.DeferredPaymentCheck);

                    break;

                case (int)CourtJudgementType.Other_99:
                    this.trSentence.Visible = false;
                    this.trVerdict.Visible = false;
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    lblError.Visible = true;

                    break;
                case (int)CourtJudgementType.CaseFinalised_LicenceSuspended:
                    if (oj.FineAmount == 999999)
                    {
                        //update by Rachel 20140811 for 5337
                        //this.txtSentenceAmount.Text = 0.ToString("N");
                        this.txtSentenceAmount.Text = 0.ToString("N", CultureInfo.InvariantCulture);
                        //end update by Rachel 20140811 for 5337
                    }
                    this.trSentenceAmount.Visible = true;
                    break;

                case (int)CourtJudgementType.StruckOffRoll_10:
                case (int)CourtJudgementType.WithdrawnCharge_15:
                case (int)CourtJudgementType.WithdrawnToReissue_16:
                case (int)CourtJudgementType.WithdrawnSummons_17:
                case (int)CourtJudgementType.CommunityService_25:
                default:
                    break;
            }

        }

        protected void btnRecordJudgement_Click(object sender, EventArgs e)
        {
            //Create snap shot for summons before judgement

            this.RecordJudgement(1);
        }

        private void CheckUsePostalReceipt()
        {
            // LMZ Added (2007-03-02): To check for Authority Rule - print all receipts to Postal Receipt
            if (this.ViewState["UsePostalReceipt"] == null)
            {
                //ard = authRules.GetAuthorityRulesDetailsByCode(this.autIntNo, "4510", "Rule to indicate using of postal receipt for all receipts", 0, "N", "Y = Yes; N= No(default)", this.login);

                //20090113 SD	
                //AutIntNo, ARCode and LastUser need to be set from here
                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = this.autIntNo;
                ard.ARCode = "4510";
                ard.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this.usePostalReceipt = ard.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ? true : false;
                this.ViewState["UsePostalReceipt"] = this.usePostalReceipt;
            }
            else
                this.usePostalReceipt = (bool)this.ViewState["UsePostalReceipt"];

        }

        private void SetErrorState(string message)
        {
            this.lblError.Visible = true;
            lblError.Text = message;
        }

        protected void gridDeferredPayments_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            this.lblError.Visible = false;

            gridDeferredPayments.SelectedIndex = e.Item.ItemIndex;

            //if (gridDeferredPayments.SelectedIndex > -1)
            //{
            //pnlUpdate.Visible = true;
            //lblPrintFile.Text = dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString();

            // Oscar 20101215 - added
            string amountList = string.Empty;
            string schIntNoList = GetAvailableSChIntNoList(out amountList);

            if (e.CommandName == "Insert")
            {
                int row = this.gridDeferredPayments.EditItemIndex;
                if (row != -1)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text7"), Convert.ToString(row + 1));
                    this.lblError.Visible = true;
                    return;
                }

                //mrs 20080621 must test for empty rows / text boxes here
                TextBox txtPaymentDate = (TextBox)e.Item.FindControl("txtPaymentDate");
                TextBox txtPaymentAmount = (TextBox)e.Item.FindControl("txtPaymentAmount");

                if (txtPaymentDate.Text == string.Empty || txtPaymentAmount.Text == string.Empty)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                    this.lblError.Visible = true;
                    return;
                }

                //Try Parse the date field and the numeric
                DateTime dt;
                if (!DateTime.TryParse(txtPaymentDate.Text, out dt))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                    this.lblError.Visible = true;
                    return;
                }

                if (DateTime.Compare(dt, this.cDate) < 0)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                    this.lblError.Visible = true;
                    return;
                }

                decimal amount;
                if (!Decimal.TryParse(txtPaymentAmount.Text, out amount))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                    this.lblError.Visible = true;
                    return;
                }
                else
                {
                    if (amount <= 0)
                    {
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                        this.lblError.Visible = true;
                        return;
                    }
                }

                decimal SumPayment = this.GetDeferredPaymentsAmount();
                if (SumPayment >= 0)
                {
                    //decimal SumTotal = decimal.Parse(this.txtSentenceAmount.Text) + decimal.Parse(this.txtContemptAmount.Text) + decimal.Parse(this.txtOtherAmount.Text);
                    //Oscar 20101208 - changed
                    decimal SumTotal = GetDeferredPaymentsAmountOriginal();
                    if (SumTotal < SumPayment + amount)
                    {
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                        this.lblError.Visible = true;
                        return;
                    }
                }

                SumChargeDB dbSC = new SumChargeDB(this.connectionString);
                int newSCDefPayIntNo = dbSC.AddDeferredPayment(this.summonsList.CurrentValue.SumIntNo, txtPaymentDate.Text, txtPaymentAmount.Text, schIntNoList, amountList);
                if (newSCDefPayIntNo == -1)
                {
                    //there was an error adding the deferred payment.
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                    this.lblError.Visible = true;
                    return;
                }
                else
                {
                    DeferredPayments_Bind();
                }
            }
            else if (e.CommandName == "Edit")
            {
                gridDeferredPayments.EditItemIndex = e.Item.ItemIndex;
                DeferredPayments_Bind();
            }
            else if (e.CommandName == "Update")
            {
                //int scDefPayIntNo = int.Parse(gridDeferredPayments.DataKeys[gridDeferredPayments.SelectedIndex].ToString());
                TextBox txtPaymentDate = (TextBox)e.Item.FindControl("txtPaymentDate");
                TextBox txtPaymentAmount = (TextBox)e.Item.FindControl("txtPaymentAmount");
                //mrs 20080802 need all the textbox validation here too
                if (txtPaymentDate.Text == string.Empty || txtPaymentAmount.Text == string.Empty)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                    this.lblError.Visible = true;
                    return;
                }

                DateTime dt;
                if (!DateTime.TryParse(txtPaymentDate.Text, out dt))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                    this.lblError.Visible = true;
                    return;
                }
                else
                {
                    if (DateTime.Compare(dt, this.cDate) < 0)
                    {
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                        this.lblError.Visible = true;
                        return;
                    }
                }

                decimal amount = 0;

                if (!Decimal.TryParse(txtPaymentAmount.Text, out amount))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                    this.lblError.Visible = true;
                    return;
                }
                else
                {
                    if (amount <= 0)
                    {
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                        this.lblError.Visible = true;
                        return;
                    }
                }

                decimal SumPayment = this.GetDeferredPaymentsAmount();
                if (SumPayment >= 0)
                {
                    decimal SumTotal = GetDeferredPaymentsAmountOriginal();
                    if (SumTotal < SumPayment)
                    {
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                        this.lblError.Visible = true;
                        return;
                    }
                }

                SumChargeDB dbSC = new SumChargeDB(this.connectionString);
                //int result = dbSC.UpdateDeferredPayment(scDefPayIntNo, this.sumChargeList.CurrentValue.SChIntNo, txtPaymentDate.Text, txtPaymentAmount.Text);
                int result = dbSC.UpdateDeferredPayment(this.summonsList.CurrentValue.SumIntNo, this.gridDeferredPayments.DataKeys[gridDeferredPayments.SelectedIndex].ToString(), txtPaymentDate.Text, txtPaymentAmount.Text, schIntNoList, amountList);
                if (result == -1)
                {
                    //there was an error updating the deferred payment.
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                    this.lblError.Visible = true;
                    return;
                }
                else
                {
                    gridDeferredPayments.EditItemIndex = -1;
                    DeferredPayments_Bind();
                }
            }
            else if (e.CommandName == "Cancel")
            {
                gridDeferredPayments.EditItemIndex = -1;
                DeferredPayments_Bind();
            }
            else if (e.CommandName == "Delete")
            {
                //need to allow the user to delete a line in the deferred payments table.
                //int scDefPayIntNo = int.Parse(gridDeferredPayments.DataKeys[gridDeferredPayments.SelectedIndex].ToString());
                SumChargeDB dbSC = new SumChargeDB(this.connectionString);
                //int result = dbSC.DeleteDeferredPayment(scDefPayIntNo);
                int result = dbSC.DeleteDeferredPayment(this.summonsList.CurrentValue.SumIntNo, this.gridDeferredPayments.DataKeys[gridDeferredPayments.SelectedIndex].ToString());
                if (result == -1)
                {
                    //there was an error updating the deferred payment.
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text15");
                    this.lblError.Visible = true;
                    return;
                }
                else
                {
                    DeferredPayments_Bind();
                }
            }
        }

        protected void ddlReceivedFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlReceivedFrom.SelectedItem.Value)
            {
                case "D":       //Driver
                    this.personaType = PersonaType.Driver;
                    ShowOrHideAddress(true);
                    break;
                case "O":       //Other
                    this.personaType = PersonaType.Owner;
                    ShowOrHideAddress(true);
                    break;
                case "P":       //Proxy
                    this.personaType = PersonaType.Proxy;
                    ShowOrHideAddress(true);
                    break;
                case "X":       //Other
                    this.personaType = PersonaType.Other;
                    ShowOrHideAddress(false);
                    break;
                case "A":
                    this.personaType = PersonaType.Accused;
                    ShowOrHideAddress(true);
                    break;
            }

            ViewState.Add("PersonaType", this.personaType);

            GetAddressDetails(false);
        }

        protected void ShowOrHideAddress(bool show)
        {
            lblName.Visible = show;
            lblAddress.Visible = show;
            lblPostalCode.Visible = show;
            txtReceivedFrom.Visible = show;
            txtAddress1.Visible = show;
            txtAddress2.Visible = show;
            txtAddress3.Visible = show;
            txtAddress4.Visible = show;
            txtAddress5.Visible = show;
            txtPoCode.Visible = show;
        }

        private void GetAddressDetails(bool forceChange)
        {
            NoticeForPaymentDB notice = new NoticeForPaymentDB(connectionString);
            DataSet dsAddress;

            dsAddress = notice.GetSummonsAddressDetails(SumIntNo, this.personaType);

            foreach (DataRow dr in dsAddress.Tables[0].Rows)
            {
                if (dr["Persona"].ToString().Equals(this.personaType.ToString()))
                {
                    this.txtAddress1.Text = dr["Address1"].ToString();
                    this.txtAddress2.Text = dr["Address2"].ToString();
                    this.txtAddress3.Text = dr["Address3"].ToString();
                    this.txtAddress4.Text = dr["Address4"].ToString();
                    this.txtAddress5.Text = dr["Address5"].ToString();
                    this.txtPoCode.Text = dr["PoCode"].ToString();
                    this.txtReceivedFrom.Text = dr["FullName"].ToString();
                    break;
                }
            }

            if (dsAddress.Tables[0].Rows.Count == 0)
            {
                this.txtAddress1.Text = string.Empty;
                this.txtAddress2.Text = string.Empty;
                this.txtAddress3.Text = string.Empty;
                this.txtAddress4.Text = string.Empty;
                this.txtAddress5.Text = string.Empty;
                this.txtPoCode.Text = string.Empty;
                this.txtReceivedFrom.Text = string.Empty;
            }
            //store dataset back into Session
            //Session.Add("NoticeAddressDetails", dsAddress);

            if (SumIntNo > 0)
            {
                //need to re-populate Received from list, depending on whether there is a proxy/accused or not
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            this.RecordJudgement(2);
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.RecordJudgement(0);
        }

        // Jake 2013-10-14 added new parameter checkWoaType
        private void RecordJudgement(int confirmDifferentAmountsForCourtPayment, bool checkWOAType = true)
        {

            //user is cancelling the updated because the payment amount is incorrect
            if (confirmDifferentAmountsForCourtPayment == 0)
            {
                this.btnConfirm.Visible = false;
                this.btnCancel.Visible = false;
                this.lblError.Visible = false;
                return;
            }

            this.lblError.Visible = true;
            this.lblError.Text = string.Empty;

            string[] split = this.cboJudgementType.SelectedValue.Split(new char[] { '~' });
            int type = int.Parse(split[0]);
            int code = int.Parse(split[1]);
            bool YesDeferred = false;
            int returnID = 0;

            StringBuilder sb = new StringBuilder();
            StringBuilder sb1 = new StringBuilder();
            sb.Append((string)GetLocalResourceObject("lblError.Text16") + "\n<ul>\n");
            bool isValid = true;
            string errMessage = string.Empty;

            string verdict = this.txtVerdict.Text.Trim();
            string sentence = this.txtSentence.Text.Trim();
            string rptDate = this.dtpReceiptDate.Text.Trim();
            string crtDate = this.dtpNewCourtDate.Text.Trim();
            string sentenceAmountStr = this.txtSentenceAmount.Text.Trim();
            string contemptAmountStr = this.txtContemptAmount.Text.Trim();
            string otherAmountStr = this.txtOtherAmount.Text.Trim();
            string fineAmountStr = this.lblFineAmount.Text.Trim();
            string revisedFineAmountStr = this.lblRevisedFineAmount.Text.Trim();

            decimal sentenceAmount = 0;
            decimal contemptAmount = 0;
            decimal otherAmount = 0;
            decimal fineAmount = 0;
            decimal revisedFineAmount = 0;

            // Jake 2013-10-21 add new variable here to get the transaction pk
            int ctnlaIntNo = 0;  //from CourtTranNoListAvailable

            DateTime newCourtDate = DateTime.Now;
            DateTime dt;
            decimal d;

            if ((type == 0 || type == -1) && JudgeProcess != JudgementProcess.DeferredPaymentJudgement)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text17");
                return;
            }
            if (code == (int)CourtJudgementType.Other_99)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                return;
            }

            if (JudgeProcess != JudgementProcess.DeferredPaymentJudgement && code != (int)CourtJudgementType.PaidAtCourt_49)
            {
                if (string.IsNullOrEmpty(verdict))
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text18") + "</li>\n");
                    isValid = false;
                }
                //jerry 2011-11-14 add
                if (code != (int)CourtJudgementType.WithdrawnCharge_15 && code != (int)CourtJudgementType.WithdrawnSummons_17 && code != (int)CourtJudgementType.WithdrawnToReissue_16)
                {
                    if (string.IsNullOrEmpty(sentence))
                    {
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text19") + "</li>\n");
                        isValid = false;
                    }
                }
            }
            //Jake 2014-04-22 added regex to validate the amount which capture on judgement page.
            Regex reg = new Regex(@"(^[0-9]\d*$)|(^0\.\d{1,2}$)|(^[1-9]\d*\.\d{1,2}$)");
            string amount = "";
            // Oscar 20110322 added for woa contemptamount
            //update by Rachel 2014-11-13 for task 5352
            //if ((JudgeProcess == JudgementProcess.SummonsJudgement && code == (int)CourtJudgementType.WOA_20) || (this.CaptureAmountDetails == true && JudgeProcess == JudgementProcess.SumChargeJudgement))
            if ((JudgeProcess == JudgementProcess.SummonsJudgement && (code == (int)CourtJudgementType.WOA_20 || code == (int)CourtJudgementType.Guilty_26 || code == (int)CourtJudgementType.NotGuilty_27)) || (this.CaptureAmountDetails == true && JudgeProcess == JudgementProcess.SumChargeJudgement))
            {
                if (string.IsNullOrEmpty(contemptAmountStr))
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text20") + "</li>\n");
                    isValid = false;
                }
                else // Iris 2014/03/27 added for task 5248
                {

                    if (!decimal.TryParse(contemptAmountStr, NumberStyles.Any, CultureInfo.InvariantCulture, out d))
                    {
                        sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text67"), "Contempt Amount") + "</li>\n");
                        isValid = false;
                    }
                    else if (decimal.TryParse(contemptAmountStr, NumberStyles.Any, CultureInfo.InvariantCulture, out d) && decimal.Parse(contemptAmountStr, CultureInfo.InvariantCulture) < 0)
                    {
                        sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text68"), "Contempt Amount") + "</li>\n");
                        isValid = false;
                    }
                    else
                    {
                        amount = contemptAmountStr.Replace(",", "");
                        if (reg.IsMatch(amount))
                        {
                            contemptAmount = Helper.ParseMoney(amount);
                        }
                        else
                        {
                            sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text67"), "Contempt Amount") + "</li>\n");
                            isValid = false;
                        }
                    }

                }

            }

            //update by Rachel 2014-11-13 for task 5352
            //if (this.CaptureAmountDetails == true && JudgeProcess == JudgementProcess.SumChargeJudgement)
            if ((JudgeProcess == JudgementProcess.SummonsJudgement && (code == (int)CourtJudgementType.Guilty_26 || code == (int)CourtJudgementType.NotGuilty_27)) || (this.CaptureAmountDetails == true && JudgeProcess == JudgementProcess.SumChargeJudgement))
            {

                if (string.IsNullOrEmpty(sentenceAmountStr))
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text21") + "</li>\n");
                    isValid = false;
                }
                else if (!decimal.TryParse(sentenceAmountStr, NumberStyles.Any, CultureInfo.InvariantCulture, out d))
                {
                    sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text67"), "Sentence Amount") + "</li>\n");
                    isValid = false;
                }
                else if (decimal.TryParse(sentenceAmountStr, NumberStyles.Any, CultureInfo.InvariantCulture, out d) && decimal.Parse(sentenceAmountStr, CultureInfo.InvariantCulture) < 0)
                {
                    sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text68"), "Sentence Amount") + "</li>\n");
                    isValid = false;
                }
                else
                {
                    amount = sentenceAmountStr.Replace(",", "");
                    if (reg.IsMatch(amount))
                    {
                        sentenceAmount = Helper.ParseMoney(amount);
                    }
                    else
                    {
                        sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text67"), "Sentence Amount") + "</li>\n");
                        isValid = false;
                    }
                }

                //if (string.IsNullOrEmpty(contemptAmountStr) || !decimal.TryParse(contemptAmountStr, out d))
                //{
                //    sb.Append("<li>You need to enter a valid contempt amount.</li>\n");
                //    isValid = false;
                //}
                //else
                //    contemptAmount = Helper.ParseMoney(contemptAmountStr);

                //update by Rachel 2014-11-14 for 5352
                //if (this.CaptureAmountDetails == true && string.IsNullOrEmpty(otherAmountStr))
                if (this.CaptureAmountDetails == true && JudgeProcess == JudgementProcess.SumChargeJudgement)
                {
                    if (string.IsNullOrEmpty(otherAmountStr))
                    {
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text22") + "</li>\n");
                        isValid = false;
                    }

                    else if (!decimal.TryParse(otherAmountStr, NumberStyles.Any, CultureInfo.InvariantCulture, out d))
                    {
                        sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text67"), "Other Amount") + "</li>\n");
                        isValid = false;
                    }
                    else if (decimal.TryParse(otherAmountStr, NumberStyles.Any, CultureInfo.InvariantCulture, out d) && decimal.Parse(otherAmountStr, CultureInfo.InvariantCulture) < 0)
                    {
                        sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text68"), "Other Amount") + "</li>\n");
                        isValid = false;
                    }
                    else
                    {
                        amount = otherAmountStr.Replace(",", "");
                        if (reg.IsMatch(amount))
                        {
                            otherAmount = Helper.ParseMoney(amount);
                        }
                        else
                        {
                            sb.Append("<li>" + string.Format((string)GetLocalResourceObject("lblError.Text67"), "Other Amount") + "</li>\n");
                            isValid = false;
                        }
                    }
                }
            }
            if (JudgeProcess == JudgementProcess.SumChargeJudgement && code == (int)CourtJudgementType.CaseFinalised_LicenceSuspended)
            {
                if (this.summonsList != null && this.summonsList.CurrentValue != null
                    && this.summonsList.CurrentValue.NotFilmType != null
                    && Convert.ToString(this.summonsList.CurrentValue.NotFilmType).Equals("M", StringComparison.OrdinalIgnoreCase))
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text59") + "</li>\n");
                    isValid = false;
                }
                else
                {
                    sentenceAmount = Helper.ParseMoney(sentenceAmountStr);
                }
            }

            if (code == (int)CourtJudgementType.Remand_19)
            {
                newCourtDate = DateTime.MinValue;
                //BD need to handle the passing of the new court date if case remanded.
                if (!DateTime.TryParse(crtDate, out newCourtDate))
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text23") + "</li>\n");
                    isValid = false;
                }
                // Jake 2014-08-18 changed ,newCourtDate  DateTime.Today => newCourtDate  DateTime.Today
                //Iris 2014-02-21 modified newCourtDate must greate exclude all court dates that are passed.
                else if (newCourtDate <= DateTime.Today)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text24") + "</li>\n");
                    isValid = false;
                }
            }
            else if (code == (int)CourtJudgementType.PaidAtCourt_49)
            {
                if (string.IsNullOrEmpty(rptDate))
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text25") + "</li>\n");
                    isValid = false;
                }
                else if (!DateTime.TryParse(rptDate, out dt))
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text25") + "</li>\n");
                    isValid = false;
                }
                else if (dt.CompareTo(DateTime.Today) > 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text26") + "</li>\n");
                    isValid = false;
                }

                fineAmount = Helper.ParseMoney(lblFineAmount.Text);
                revisedFineAmount = Helper.ParseMoney(lblRevisedFineAmount.Text);
            }

            bool invalidDeferredPaymentSelected = false;

            if (JudgeProcess == JudgementProcess.DeferredPaymentJudgement)
            {
                if (isValid)
                {
                    if (gridDeferredPayments.Items.Count == 0)
                    //some silly user selected deferred payments but the charge they have judged does not need it! Need to be able to update the previous data!
                    {
                        invalidDeferredPaymentSelected = true;
                    }

                    //BD 20080612 need to check that they have not added one line in deferred table, must be 2 or more
                    if (gridDeferredPayments.Items.Count == 1)
                    {
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text27") + "</li>\n");
                        isValid = false;
                    }

                    //BD 20080612 need to check that the total deferred amount (if there is) equals total sentence + contempt + other
                    if (gridDeferredPayments.Items.Count > 0)
                    {
                        int TotalCount = gridDeferredPayments.Items.Count;
                        decimal SumPayment = 0;
                        for (int RowCount = 0; RowCount <= TotalCount - 1; RowCount++)
                        {
                            Label lblAmount = (Label)gridDeferredPayments.Items[RowCount].FindControl("lblPaymentAmount");
                            SumPayment += decimal.Parse(lblAmount.Text);
                        }
                        decimal SumTotal = GetDeferredPaymentsAmountOriginal();
                        if (SumPayment != SumTotal)
                        {
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text28") + "</li>\n");
                            isValid = false;
                        }
                        else
                        {
                            //need to set an indicator use later in setting the charge status to 734 for deferred payments
                            YesDeferred = true;
                        }
                    }
                }
                else
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text28") + "</li>\n");
                    isValid = false;
                }
            }

            NoticeSummons noticeSummons = new NoticeSummonsService().GetBySumIntNo(this.summonsList.CurrentValue.SumIntNo).FirstOrDefault();
            SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(noticeSummons.NotIntNo);
            if (notice.NoticeStatus == (int)ChargeStatusList.FullyPaid || notice.NoticeStatus == (int)ChargeStatusList.FullyPaidAtCourt)
            {
                if (code != (int)CourtJudgementType.PaidAtCourt_49)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text63") + "</li>\n");
                    isValid = false;
                }
            }
            if (code == (int)CourtJudgementType.PaidAtCourt_49)
            {
                // Iris 2014-02-20 added AutRule 6310 verification Start
                //if (notice.NoticeStatus != (int)ChargeStatusList.FullyPaid && notice.NoticeStatus != (int)ChargeStatusList.FullyPaidAtCourt)
                //{
                //    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text65") + "</li>\n");
                //    isValid = false;
                //}
                if (notice.NoticeStatus != (int)ChargeStatusList.FullyPaid
                    && notice.NoticeStatus != (int)ChargeStatusList.FullyPaidAtCourt
                    && _ISHRK == "")
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text66") + "</li>\n");
                    isValid = false;
                }
                // Iris 2014-02-20 added AutRule 6310 verification End
            }

            if (!isValid)
            {
                sb.Append("</ul>\n\n");
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;  // Oscar 20100921 - added
                return;
            }

            //dls 100106 - need to make sure the user is thinking clearly about the amounts that have been entered: validate against fine amount and revised fine amount
            //if (confirmDifferentAmountsForCourtPayment == 1 && code == (int)CourtJudgementType.PaidAtCourt_49)
            //{
            //    if (sentenceAmount != fineAmount && sentenceAmount != revisedFineAmount)
            //    {
            //        sb1.Append(string.Format("<ul><li>" + (string)GetLocalResourceObject("lblError.Text29") + "</li>\n", fineAmount, revisedFineAmount, sentenceAmount));
            //        sb1.Append("<li>" + (string)GetLocalResourceObject("lblError.Text30") + "</li>\n");
            //        sb1.Append("</ul>\n");
            //        this.lblError.Text = sb1.ToString();
            //        btnCancel.Visible = true;
            //        btnConfirm.Visible = true;
            //        return;
            //    }
            //}

            // Jake 2013-10-14 added - check bench woa or double woa here 
            if (confirmDifferentAmountsForCourtPayment == 1 && code == (int)CourtJudgementType.WOA_20 && checkWOAType)
            {

                string msg = CheckIsStandardWOA(this.summonsList.CurrentValue.SumIntNo);

                if (WOAType != "S")
                {
                    if (WOAType == "D" && !String.IsNullOrEmpty(msg)) // There is a double woa existing in our DB
                    {
                        sb1.Append("<ul>");
                        sb1.Append("<li>" + msg + "</li>\n");
                        sb1.Append("</ul>\n");
                        this.lblError.Text = sb1.ToString();
                        return;
                    }

                    string woaNumber = GenerateWOANumber(this.autIntNo, this.login, out ctnlaIntNo);
                    this.CTNLAIntNo = ctnlaIntNo;
                    if (String.IsNullOrEmpty(woaNumber))
                    {
                        sb1.Append("<ul>");
                        sb1.Append("<li>" + (string)GetLocalResourceObject("lblError.Text60") + "</li>\n");
                        sb1.Append("</ul>\n");
                        this.lblError.Text = sb1.ToString();
                    }
                    else
                    {
                        if (WOAType == "B")
                        {
                            this.lblBenchWOANumber.Text = woaNumber;
                        }
                        else
                        {
                            this.lblDoubleWOANumber.Text = woaNumber;
                        }

                    }
                    return;
                }
            }

            //Create snapshot for each summons before judegement
            JudgementSnapshotDB js = new JudgementSnapshotDB(this.connectionString);
            //Jake 2013-11-26 comment out , we need to create snapshot for every judgement
            //if (js.CheckSnapshotIsCreated(this.summonsList.CurrentValue.SumIntNo) == false)
            //{
            if ((JudgeProcess == JudgementProcess.SumChargeJudgement && this.sumChargeList.CurrentIndex == 1)
                || JudgeProcess == JudgementProcess.SummonsJudgement)
            {
                if (js.SetSnapshot(this.summonsList.CurrentValue.SumIntNo, false, "TMS") == false)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text31");
                    return;
                }
            }
            //}


            SumChargeDB db = new SumChargeDB(this.connectionString);
            if (JudgeProcess == JudgementProcess.SummonsJudgement)
            {
                //Oscar 20111221 - add transaction for push queue
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    AuthorityDB authDB = new AuthorityDB(this.connectionString);
                    AuthorityDetails auth = authDB.GetAuthorityDetails(this.autIntNo);
                    string strAutCode = auth.AutCode.Trim();
                    string printFilenamePrefix = string.Empty;
                    string woaNumber, issueWOANotice, displayRepresentative, woaPrintFileName;
                    woaNumber = issueWOANotice = displayRepresentative = woaPrintFileName = string.Empty;
                    //This is a Bench WOA or Double WOA
                    if (WOAType != "S" && WOAType != "N")
                    {
                        int ttIntNo = 0;

                        if (WOAType == "B")
                        {
                            woaNumber = this.lblBenchWOANumber.Text;
                            ttIntNo = (int)TransactionTypeList.Bench_WOA;
                        }
                        else
                        {
                            woaNumber = this.lblDoubleWOANumber.Text;
                            ttIntNo = (int)TransactionTypeList.Double_WOA;
                        }
                        TransactionType tType = new TransactionTypeService().GetByTtIntNo(ttIntNo);
                        printFilenamePrefix = tType.TtPrefix;


                        KeyValuePair<int, string> rule_5801 = GetAuthorityRuleByCode("5801");
                        CourtDB courtDB = new CourtDB(this.connectionString);
                        string courtPrefix = string.Empty;
                        CourtDetails court = courtDB.GetCourtDetails(int.Parse(this.cboCourt.SelectedValue));
                        courtPrefix = String.IsNullOrEmpty(court.CrtPrefix) == true ? "" : court.CrtPrefix.Trim();
                        //DW-WOA-CP-2013-04-02-B/D
                        issueWOANotice = rule_5801.Value;
                        displayRepresentative = "N";
                        courtPrefix = Helper.ReplaceSpecialCharacters(courtPrefix); //2014-01-16 Heidi added for use "/" not create file(5101)
                        woaPrintFileName = string.Format("{0}_WOA-{1}-{2}-{3}-CourtDate-{4}", //Jake 2014-08-14 added courtdate in print file name for bench and double woa, package: 5345
                            printFilenamePrefix,
                            strAutCode,
                            DateTime.Now.ToString(DATE_AND_TIME_FORMAT),
                            courtPrefix,
                            this.summonsList.CurrentValue.CourtDate.ToString(DATE_FORMAT));

                    }

                    returnID = db.RecordJudgement(verdict, sentence, sentenceAmount, contemptAmount, otherAmount, type, this.login,
                        0, newCourtDate, YesDeferred, this.summonsList.CurrentValue.SumIntNo,
                        true, this.WOAType, woaNumber, issueWOANotice, displayRepresentative, woaPrintFileName, this.CTNLAIntNo, ref errMessage);

                    //Oscar 20111221 - add push queue
                    if (returnID > 0)
                    {
                        QueueItemProcessor queProcessor = new QueueItemProcessor();
                        if (auth != null && auth.AutIntNo > 0)
                        {
                            // Jake move get authority detail to the top of the transaction 

                            if (code == 20)
                            {
                                if (WOAType != "B" && WOAType != "D")
                                {
                                    AuthorityRulesDetails ar = new AuthorityRulesDetails();
                                    DefaultAuthRules dfAuthRule = new DefaultAuthRules(ar, this.connectionString);
                                    // Jake 2013-10-16 comment it out to use a function to return the specific authority rule
                                    //ar.AutIntNo = autIntNo;
                                    //ar.ARCode = "5801";
                                    //ar.LastUser = this.login;
                                    //DefaultAuthRules dfAuthRule = new DefaultAuthRules(ar, this.connectionString);
                                    //KeyValuePair<int, string> arValue = dfAuthRule.SetDefaultAuthRule();


                                    KeyValuePair<int, string> arValue = GetAuthorityRuleByCode("5801");

                                    //int crtIntNo = int.Parse(this.cboCourt.SelectedValue);
                                    //CourtDB crt = new CourtDB(connectionString);
                                    //CourtDetails crtDet = crt.GetCourtDetails(crtIntNo);
                                    //string crtNo = crtDet.CrtNo;

                                    //get another auth rule for no. of days after court date to generate notice of WOA
                                    ar = new AuthorityRulesDetails();
                                    ar.AutIntNo = autIntNo;
                                    ar.ARCode = "5800";
                                    ar.LastUser = this.login;

                                    dfAuthRule = new DefaultAuthRules(ar, connectionString);
                                    KeyValuePair<int, string> daysElapseNoticeGenerateRule = dfAuthRule.SetDefaultAuthRule();
                                    int daysNotOfWOALoad = daysElapseNoticeGenerateRule.Key;

                                    //get another auth rule for no. of days after court date to generate WOA
                                    DateRulesDetails drWOA = new DateRulesDetails();
                                    drWOA.AutIntNo = autIntNo;
                                    drWOA.DtRStartDate = "SumCourtDate";
                                    drWOA.DtREndDate = "WOALoadDate";
                                    drWOA.LastUser = this.login;

                                    DefaultDateRules dfDateRule = new DefaultDateRules(drWOA, connectionString);
                                    int daysWOALoad = dfDateRule.SetDefaultDateRule();

                                    string shortCrtDate = Convert.ToDateTime(ViewState["CourtDate"].ToString()).ToString("d");
                                    //QueueItemProcessor queProcessor = new QueueItemProcessor();
                                    queProcessor.Send(
                                        new QueueItem()
                                        {
                                            Body = this.summonsList.CurrentValue.SumIntNo,
                                            //Group = arValue.Value == "Y" ? strAutCode + "~" + crtNo : strAutCode + "~" + crtNo + "~" + shortCrtDate,
                                            Group = arValue.Value == "Y" ? strAutCode + "~" + this.summonsList.CurrentValue.CrtNo : strAutCode + "~" + this.summonsList.CurrentValue.CrtNo + "~" + shortCrtDate,
                                            ActDate = DateTime.Now.AddDays(arValue.Value == "Y" ? daysNotOfWOALoad : daysWOALoad),
                                            QueueType = arValue.Value == "Y" ? ServiceQueueTypeList.GenerateNoticeOfWOA : ServiceQueueTypeList.GenerateWOA
                                        }
                                        );
                                }
                                else if (WOAType == "B")
                                {
                                    // we need to push a woa expire queue for a bench woa
                                    DateRulesDetails drExpireWOA = new DateRulesDetails();
                                    drExpireWOA.AutIntNo = autIntNo;
                                    drExpireWOA.DtRStartDate = "SumCourtDate";
                                    drExpireWOA.DtREndDate = "WOAExpireDate";
                                    drExpireWOA.LastUser = this.login;

                                    DefaultDateRules dfDateRule = new DefaultDateRules(drExpireWOA, connectionString);
                                    int daysWOAExpire = dfDateRule.SetDefaultDateRule();

                                    //Jerry 2014-04-01 check WOACancel != true
                                    //Woa woa = new WoaService().GetBySumIntNo(this.summonsList.CurrentValue.SumIntNo).Where(w => w.WoaNumber == woaNumber).FirstOrDefault();
                                    Woa woa = new WoaService().GetBySumIntNo(this.summonsList.CurrentValue.SumIntNo).Where(w => w.WoaNumber == woaNumber
                                        && w.WoaCancel != true).FirstOrDefault();

                                    queProcessor.Send(
                                        new QueueItem()
                                        {
                                            Body = woa.WoaIntNo.ToString(),  // woaIntNo
                                            Group = strAutCode,
                                            //Jake 2015-02-28 modified
                                            //ActDate = DateTime.Now.AddDays(daysWOAExpire),
                                            ActDate = this.summonsList.CurrentValue.CourtDate.AddDays(daysWOAExpire),
                                            QueueType = ServiceQueueTypeList.CancelExpiredWOA
                                        }
                                        );

                                }
                            }
                            else if (code == 16)
                            {
                                //Jerry 2013-03-07 need to get Notice.IsVideo
                                SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(this.summonsList.CurrentValue.NotIntNo);

                                var group = new[]
                                {
                                    strAutCode,
                                    //Jerry 2013-03-07 change
                                    //Convert.ToString(this.summonsList.CurrentValue.NotFilmType).Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "",
                                    (Convert.ToString(this.summonsList.CurrentValue.NotFilmType).Equals("H", StringComparison.OrdinalIgnoreCase) && !noticeEntity.IsVideo) ? "H" : "",
                                    this.summonsList.CurrentValue.CrtName
                                };

                                //Henry 2013-03-27 add
                                AuthorityRulesDetails ar = new AuthorityRulesDetails()
                                {
                                    AutIntNo = autIntNo,
                                    ARCode = "5050",
                                    LastUser = this.login
                                };
                                KeyValuePair<int, string> aR_5050 = (new DefaultAuthRules(ar, connectionString)).SetDefaultAuthRule();
                                queProcessor.Send(
                                    new QueueItem
                                    {
                                        Body = this.summonsList.CurrentValue.NotIntNo,
                                        Group = string.Join("|", group),
                                        ActDate = DateTime.Now,
                                        Priority = noticeEntity.NotOffenceDate.Subtract(DateTime.Now).Days,   // 2013-09-06, Oscar added
                                        QueueType = ServiceQueueTypeList.GenerateSummons
                                    },
                                    //Henry 2031-03-27 add
                                    (aR_5050.Value.Trim().ToUpper() == "Y" ?
                                    new QueueItem
                                    {
                                        Body = this.summonsList.CurrentValue.NotIntNo,
                                        Group = string.Join("|", group),
                                        ActDate = noticeEntity.NotOffenceDate.AddMonths(aR_5050.Key).AddDays(-5),
                                        Priority = noticeEntity.NotOffenceDate.Subtract(DateTime.Now).Days,   // 2013-09-06, Oscar added
                                        QueueType = ServiceQueueTypeList.GenerateSummons
                                    } : null));

                            }
                            else if (code == 19) //remand
                            {
                                int pfnIntNo = 0;
                                DateTime actionDate;
                                SetPrintFileName(strAutCode, autIntNo, this.summonsList.CurrentValue.SumIntNo, newCourtDate,
                                    this.login, out pfnIntNo, out actionDate);
                                if (pfnIntNo > 0)
                                {
                                    DateTime printActionDate = DateTime.Now.AddHours(new SysParamService().GetBySpColumnName(SysParamList.DelayActionDate_InHours.ToString()).SpIntegerValue);
                                    new QueueItemProcessor().Send(
                                        new QueueItem()
                                        {
                                            Body = pfnIntNo,
                                            Group = strAutCode,
                                            ActDate = actionDate > printActionDate ? actionDate : printActionDate,
                                            QueueType = ServiceQueueTypeList.SecondAppearanceCCR
                                        }
                                    );
                                }
                            }
                        }
                        // Jake 2012-06-26 only returnValue >0 then commit the transaction
                        scope.Complete();
                    }

                    // Jake 2012-06-26 modified, 
                    //scope.Complete();
                }

                // Jake 2013-10-21 added 
                // JudgementType is Warrant, we have generated a Bench WOA or Double WOA and failed,
                // we need to reverse next available number from CourtTranNoListAvailable
                if (code == 20 && (WOAType == "B" || WOAType == "D") && returnID <= 0 && this.CTNLAIntNo > 0)
                {
                    new CourtDB(this.connectionString).SetForCourtTranNoListAvailable(CTNLAIntNo, 0);
                }
            }
            else if (JudgeProcess == JudgementProcess.SumChargeJudgement)
            {
                if (NeedDeferredPayment())
                {
                    this.sumChargeList.CurrentValue.Verdict = verdict;
                    this.sumChargeList.CurrentValue.Sentence = sentence;
                    //this.sumChargeList.CurrentValue.SentenceAmount = sentenceAmount;          //dls 2011-05-21 - this should not be set if the charge is withdrawn
                    //this.sumChargeList.CurrentValue.ContemptAmount = contemptAmount;
                    //this.sumChargeList.CurrentValue.OtherAmount = otherAmount;
                    // Oscar 20110225 - changed for 4410C for contempt
                    if (code != (int)CourtJudgementType.WithdrawnCharge_15)
                    {
                        this.sumChargeList.CurrentValue.SentenceAmount = sentenceAmount;
                        this.sumChargeList.CurrentValue.ContemptAmount = contemptAmount;
                        this.sumChargeList.CurrentValue.OtherAmount = otherAmount;
                    }

                    if (code == (int)CourtJudgementType.WithdrawnCharge_15)
                    {
                        this.sumChargeList.CurrentValue.SentenceAmount = 0;                     //dls 2011-05-21 - this should not be set if the charge is withdrawn
                        this.sumChargeList.CurrentValue.ContemptAmount = 0;
                        this.sumChargeList.CurrentValue.OtherAmount = 0;
                    }

                }

                //dls 2011-05-27 - this needs to happen even if we are not doing deferred payments
                if (code == (int)CourtJudgementType.WithdrawnCharge_15)
                {
                    foreach (OffenderJudgement judge in this.sumChargeList)
                    {
                        if (judge.MainSumChargeID.Equals(this.sumChargeList.CurrentValue.SChIntNo))
                        {
                            judge.SChIsMain = true;
                            judge.ChargeType = "Main";
                            JudgementsLeft++;
                            break;
                        }
                    }
                    // Oscar 20110225 - added for 4410C for contempt
                    if (IsFirstMainCharge)
                        FirstMainChargeIndex++;
                }

                this.sumChargeList.CurrentValue.CJTIntNo = type;
                this.sumChargeList.CurrentValue.CJTCode = code;
                JudgementsLeft--;

                if (code == (int)CourtJudgementType.PaidAtCourt_49)
                {
                    // Jake 2011-06-23 added a parameter isFromCancelWarrant
                    bool isFromCancelWarrant = false;
                    returnID = db.RecordCashReceipt(sentenceAmount, contemptAmount, otherAmount, type, this.login, this.sumChargeList.CurrentValue.SChIntNo, rptDate, this.txtAddress1.Text, this.txtAddress2.Text, this.txtAddress3.Text, this.txtAddress4.Text, this.txtAddress5.Text, this.txtReceivedFrom.Text, this.txtPoCode.Text, this.txtPhone.Text, isFromCancelWarrant, ref errMessage);
                }
                else if (!NeedDeferredPayment())
                {
                    returnID = db.RecordJudgement(verdict, sentence, sentenceAmount, contemptAmount, otherAmount, type, this.login, this.sumChargeList.CurrentValue.SChIntNo, newCourtDate, YesDeferred, ref errMessage, this.sumChargeList.CurrentValue.SumIntNo, false);
                }
                else if (NeedDeferredPayment() && !NeedDeferredPaymentReally())
                {
                    OffenderJudgement judge = new OffenderJudgement(this.sumChargeList.CurrentValue.SumIntNo);
                    for (int i = 0; i < this.sumChargeList.Count; i++)
                    {
                        judge = this.sumChargeList[i];
                        if (judge.CJTCode > 0 && judge.CJTCode != (int)CourtJudgementType.PaidAtCourt_49)
                        {
                            returnID = db.RecordJudgement(judge.Verdict, judge.Sentence, judge.SentenceAmount, judge.ContemptAmount, judge.OtherAmount, judge.CJTIntNo, this.login, this.sumChargeList[i].SChIntNo, newCourtDate, false, ref errMessage, this.sumChargeList.CurrentValue.SumIntNo, false);
                            if (returnID < 0) break;
                        }
                    }
                }
            }
            //silly user has selected deferred payment even though it doesn't apply
            else if (JudgeProcess == JudgementProcess.DeferredPaymentJudgement && (YesDeferred || invalidDeferredPaymentSelected))
            {
                if (YesDeferred)
                {
                    type = CJTIntNoOfPayment;
                    code = 5;
                }

                OffenderJudgement judge = new OffenderJudgement(this.sumChargeList.CurrentValue.SumIntNo);
                for (int i = 0; i < this.sumChargeList.Count; i++)
                {
                    judge = this.sumChargeList[i];
                    if (judge.CJTCode > 0 && judge.CJTCode != (int)CourtJudgementType.PaidAtCourt_49)
                    {
                        returnID = db.RecordJudgement(judge.Verdict, judge.Sentence, judge.SentenceAmount, judge.ContemptAmount, judge.OtherAmount, judge.CJTIntNo, this.login, this.sumChargeList[i].SChIntNo, newCourtDate, false, ref errMessage, this.sumChargeList.CurrentValue.SumIntNo, false);
                        if (returnID < 0) break;
                    }
                }

                if (returnID >= 0)
                    returnID = db.RecordJudgement(string.Empty, string.Empty, 0, 0, 0, type, this.login, this.sumChargeList.CurrentValue.SChIntNo, newCourtDate, YesDeferred, ref errMessage, this.sumChargeList.CurrentValue.SumIntNo, true);

            }

            string error = (string)GetLocalResourceObject("Stringerror");

            if (returnID < 0)
            {

                if (errMessage.Length > 0)
                {
                    this.lblError.Text = error + errMessage;
                    this.lblError.Visible = true;
                    return;
                }

                switch (returnID)
                {
                    case -1:
                        this.lblError.Text = error + errMessage;
                        this.lblError.Visible = true;
                        return;
                    case -2:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text32");
                        this.lblError.Visible = true;
                        return;
                    case -3:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text33");
                        this.lblError.Visible = true;
                        return;
                    case -4:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text34");
                        this.lblError.Visible = true;
                        return;
                    case -5:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text35");
                        this.lblError.Visible = true;
                        return;
                    case -6:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text36");
                        this.lblError.Visible = true;
                        return;
                    case -7:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text37");
                        this.lblError.Visible = true;
                        return;
                    case -8:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text38");
                        this.lblError.Visible = true;
                        return;
                    case -9:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text39");
                        this.lblError.Visible = true;
                        return;
                    case -10:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text40");
                        this.lblError.Visible = true;
                        return;
                    case -11:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text41");
                        this.lblError.Visible = true;
                        return;
                    case -12:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text42");
                        this.lblError.Visible = true;
                        return;
                    case -13:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text43");
                        this.lblError.Visible = true;
                        return;
                    case -14:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text44");
                        this.lblError.Visible = true;
                        return;
                    case -15:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text45");
                        this.lblError.Visible = true;
                        return;
                    case -16:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text46");
                        this.lblError.Visible = true;
                        return;
                    case -17:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text47");
                        this.lblError.Visible = true;
                        return;
                    case -18:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text48");
                        this.lblError.Visible = true;
                        return;
                    case -19:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text49");
                        this.lblError.Visible = true;
                        return;
                    case -20:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text50");
                        this.lblError.Visible = true;
                        return;
                    case -21:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text51");
                        this.lblError.Visible = true;
                        return;
                    case -22:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text52");
                        this.lblError.Visible = true;
                        return;
                    case -23:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text53");
                        this.lblError.Visible = true;
                        return;
                    case -24:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text62");
                        this.lblError.Visible = true;
                        return;
                    case -25:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text64");
                        this.lblError.Visible = true;
                        return;
                    case -40:  // creating WOA failed
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text57");
                        this.lblError.Visible = true;
                        return;
                    case -41: // creatng print file name fialed
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text58");
                        this.lblError.Visible = true;
                        return;
                    case -42: //Jake 2014-08-12 added new court date can not be a weekend day
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text81");
                        this.lblError.Visible = true;
                        return;
                    case -43: //Jake 2014-08-12 added new court date can not be a public holiday
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text91");
                        this.lblError.Visible = true;
                        return;
                    default:
                        this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text54");
                        this.lblError.Visible = true;
                        return;
                }
            }

            if (returnID >= 0)
            {
                if (code == (int)CourtJudgementType.Remand_19)
                {
                    // Save new Court to snapshot

                    js.SaveCourtDatesToSnapshot(this.summonsList.CurrentValue.SumIntNo, newCourtDate, this.login);

                }
                else
                {
                    if ((JudgeProcess == JudgementProcess.SumChargeJudgement && this.sumChargeList.CurrentIndex == 1)
                        || JudgeProcess == JudgementProcess.SummonsJudgement)
                    {
                        js.UnLockSnapshot(this.summonsList.CurrentValue.SumIntNo, this.login);
                    }
                }
            }
            //Linda 2012-9-12 add for list report19
            if (returnID >= 0)
            {
                //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.login, PunchStatisticsTranTypeList.CaseResult, PunchAction.Add);
            }

            if (returnID > 0 && code == (int)CourtJudgementType.PaidAtCourt_49)
            {
                int rctIntNo = returnID;

                //check if postal receipts are used for all receipts
                this.CheckUsePostalReceipt();

                // Jake 2011-06-28 added
                sb = new StringBuilder();
                sb.Append(rctIntNo.ToString()).Append(",");

                if (usePostalReceipt)
                    Helper_Web.BuildPopup(this, "CashReceiptTraffic_PostalReceiptViewer.aspx", "date", DateTime.Now.ToString("yyyy-MM-dd"), "CBIntNo", "0", "RCtIntNo", rctIntNo.ToString());
                else
                    Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", sb.ToString());
            }


            //Oscar 20101123 - 
            switch (JudgeProcess)
            {
                case JudgementProcess.SumChargeJudgement:
                default:

                    //if ((this.sumChargeList.Count == this.sumChargeList.CurrentIndex) && NeedDeferredPayment() && NeedDeferredPaymentReally())
                    if (JudgementsLeft == 0 && NeedDeferredPayment() && NeedDeferredPaymentReally())
                    {
                        SetControlsForStatus(JudgementProcess.DeferredPaymentJudgement);
                        JudgeProcess = JudgementProcess.DeferredPaymentJudgement;
                    }
                    else
                        SetNextCharge();

                    break;

                case JudgementProcess.SummonsJudgement:
                case JudgementProcess.DeferredPaymentJudgement:
                    //SetNextJudgement(false);
                    //2013-04-08 Henry mdofiy
                    SetNextJudgementSums();

                    break;
            }
        }

        private void CheckCommitDeferredPayments()
        {
            if (GetDeferredPaymentsAmountOriginal() == GetDeferredPaymentsAmount())
                this.btnRecordJudgement.Enabled = true;
            else
                this.btnRecordJudgement.Enabled = false;
        }

        //Oscar 20101208 - added
        private decimal GetDeferredPaymentsAmountOriginal()
        {
            decimal result = 0;
            foreach (OffenderJudgement judge in this.sumChargeList)
            {
                if (judge.SChIsMain && judge.CJTCode > 0)
                {
                    if (judge.CJTCode != (int)CourtJudgementType.WithdrawnCharge_15 && judge.CJTCode != (int)CourtJudgementType.PaidAtCourt_49)
                        result += judge.SentenceAmount + judge.ContemptAmount + judge.OtherAmount;
                }
            }
            return result;
        }

        protected decimal GetDeferredPaymentsAmount()
        {
            decimal SumPayment = 0;
            int TotalCount = gridDeferredPayments.Items.Count;
            if (TotalCount > 0)
            {
                for (int RowCount = 0; RowCount < TotalCount; RowCount++)
                {
                    Label lblAmount = (Label)gridDeferredPayments.Items[RowCount].FindControl("lblPaymentAmount");
                    TextBox txtAmount = (TextBox)gridDeferredPayments.Items[RowCount].FindControl("txtPaymentAmount");
                    if (lblAmount != null)
                    {
                        SumPayment += decimal.Parse(lblAmount.Text);
                    }
                    else if (txtAmount != null)
                    {
                        SumPayment += decimal.Parse(txtAmount.Text);
                    }
                }
            }
            return SumPayment;
        }

        //Oscar 20100915 - Get row's data by type
        private T GetValue<T>(object rowData)
        {
            return rowData == DBNull.Value ? default(T) : (T)rowData;
        }

        /// <summary>
        /// Initialize Capture Judgement Page
        /// </summary>
        /// <param name="chargeIndex">Index of charge</param>
        private void InitializeCaptureJudgement(int chargeIndex, JudgementProcess process)
        {
            //Oscar 20100916 - Use chargeIndex for charges' loop
            OffenderJudgement oj = this.sumChargeList[chargeIndex];
            sumChargeList.CurrentIndex = chargeIndex + 1;
            JudgeProcess = JudgementProcess.SumChargeJudgement;
            SetControlsForStatus(process);
        }

        private void SetCaptureJudgementContent(JudgementProcess process)
        {
            this.cboJudgementType.SelectedIndex = 0;
            this.txtVerdict.Text = string.Empty;
            this.txtSentence.Text = string.Empty;
            this.txtSentenceAmount.Text = string.Empty;
            this.lblFineAmount.Text = string.Empty;
            this.lblRevisedFineAmount.Text = string.Empty;
            this.txtContemptAmount.Text = string.Empty;
            this.txtOtherAmount.Text = string.Empty;
            this.dtpNewCourtDate.Text = string.Empty;
            this.dtpReceiptDate.Text = string.Empty;
            this.rblDeferredPayment.SelectedIndex = -1;

            int crtIntNo;
            int.TryParse(this.cboCourt.SelectedValue, out crtIntNo);
            GetJudgementTypes(crtIntNo, process);
            OffenderJudgement oj;

            switch (process)
            {
                case JudgementProcess.SummonsJudgement:
                    oj = this.summonsList.CurrentValue;
                    if (oj.SumRemandedSummonsFlag)
                    {
                        this.lblRemandedSummonsFlag.Visible = true;
                        this.lblRemandedSummonsFlag.Text = string.Format((string)GetLocalResourceObject("lblRemandedSummonsFlag.Text"), String.Format("{0:yyyy/MM/dd}", oj.SumRemandedFromCourtDate), oj.SumRemandedFromCaseNumber);
                    }
                    this.lblJudgementNavigation.Text = string.Format((string)GetLocalResourceObject("lblJudgementNavigation.Text"), oj.Name, oj.SummonsNo, oj.CaseNo,
                        (grdCourtRollPager.PageSize * (grdCourtRollPager.CurrentPageIndex - 1)) + summonsList.CurrentIndex, grdCourtRollPager.RecordCount);

                    break;

                case JudgementProcess.SumChargeJudgement:
                    oj = this.sumChargeList.CurrentValue;
                    //this.lblJudgementNavigation.Text = string.Format("{0} ({1} of {2})<br/>Case No. : {3}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{4}", oj.Name, this.sumChargeList.CurrentIndex, this.sumChargeList.Count, oj.CaseNo, oj.GetChargeDetails);
                    this.lblJudgementNavigation.Text = string.Format((string)GetLocalResourceObject("lblJudgementNavigation.Text1"), oj.Name, (TotalJudgementsRequired - JudgementsLeft) + 1, TotalJudgementsRequired, oj.CaseNo, oj.GetChargeDetails);

                    //this.txtContemptAmount.Text = oj.ContemptAmount.ToString("N");
                    // Oscar 20110225 - changed for 4410C for contempt

                    //update by Rachel 20140811 for 5337
                    //this.txtContemptAmount.Text = IsFirstMainCharge ? this.crDefaultContempt.ToString("N") : "0.00";
                    //this.txtOtherAmount.Text = oj.OtherAmount.ToString("N");
                    this.txtContemptAmount.Text = IsFirstMainCharge ? this.crDefaultContempt.ToString("N", CultureInfo.InvariantCulture) : "0.00";
                    this.txtOtherAmount.Text = oj.OtherAmount.ToString("N", CultureInfo.InvariantCulture);
                    //end update by Rachel 20140811 for 5337

                    this.txtSentence.Text = oj.Sentence;
                    //if (oj.IsNoAOG || oj.IsSection35)
                    //{
                    //    this.txtSentenceAmount.Text = 0.ToString("N");
                    //}
                    //else
                    //{

                    //update by Rachel 20140811 for 5337
                    //this.txtSentenceAmount.Text = oj.SentenceAmount.ToString("N");
                    this.txtSentenceAmount.Text = oj.SentenceAmount.ToString("N", CultureInfo.InvariantCulture);
                    //end update by Rachel 20140811 for 5337

                    //}
                    this.txtVerdict.Text = oj.Verdict;

                    //dls 100105 - need to add the current fine and revised fine amounts so that they can be validated against

                    //update by Rachel 20140811 for 5337
                    //this.lblFineAmount.Text = oj.SentenceAmount.ToString("N");
                    //this.lblRevisedFineAmount.Text = oj.SentenceAmount.ToString("N");
                    this.lblFineAmount.Text = oj.SentenceAmount.ToString("N", CultureInfo.InvariantCulture);
                    this.lblRevisedFineAmount.Text = oj.SentenceAmount.ToString("N", CultureInfo.InvariantCulture);
                    //end update by Rachel 20140811 for 5337

                    if (this.cboJudgementType.Items.Count > 0)
                        this.cboJudgementType.SelectedIndex = 0;
                    this.btnRecordJudgement.Enabled = false;

                    //TodZhang 090806 - displayed the correspondence controls when the judgement type is 'no judgement'
                    if (cboJudgementType.SelectedIndex == 0)
                    {
                        this.trVerdict.Visible = true;
                        this.trSentence.Visible = true;

                        /*
                         * Oscar 2013-02-22 why we need "dtpNewCourtDate" again?
                         * Maybe merge caused the problem.
                         * We should use "trNewCourtDate"
                         */
                        //this.dtpNewCourtDate.Visible = false;
                        this.trNewCourtDate.Visible = false;

                        this.trReceiptDate.Visible = false;
                        this.pnlAddress.Visible = false;
                        //this.lblDeferredPayments.Visible = false;
                        //this.gridDeferredPayments.Visible = false;
                        this.trDeferredPayments.Visible = false;
                    }

                    break;

                case JudgementProcess.DeferredPaymentJudgement:
                    oj = this.summonsList.CurrentValue;
                    this.lblJudgementNavigation.Text = string.Format((string)GetLocalResourceObject("lblJudgementNavigation.Text2"), oj.Name, oj.SummonsNo, oj.CaseNo, summonsList.CurrentIndex, summonsList.Count);

                    break;

                default:
                    break;
            }

        }

        private int CourtRollDataBind(bool isReload)
        {
            if (isReload)
            {
                grdCourtRollPager.CurrentPageIndex = 1;
                grdCourtRollPager.RecordCount = 0;
            }
            int totalCount = 0;
            this.summonsList = GetSummonsJudgementList(out totalCount);
            //Oscar 20100916 - GridViewCourtRoll DataBind
            this.grdCourtRoll.DataSource = this.summonsList;
            //if (newPageIndex != null)
            //    this.grdCourtRoll.PageIndex = (int)newPageIndex;
            if (this.summonsList.Count == 0 && isReload == false)
            {
                if (grdCourtRollPager.CurrentPageIndex > 0)
                {
                    grdCourtRollPager.CurrentPageIndex--;
                    this.summonsList = GetSummonsJudgementList(out totalCount);
                }
            }
            this.grdCourtRoll.DataKeyNames = new string[] { "SumIntNo" };
            this.grdCourtRoll.DataBind();
            grdCourtRollPager.RecordCount = totalCount;
            return totalCount;
        }

        // Oscar 20100920 - when dropdownlist selected
        private enum DetailsStatus
        {
            Initialized,
            CourtSelected,
            RoomSelected,
            DateSelected
        }
        private void DDLDetailsStatus(DetailsStatus status)
        {

            switch (status)
            {
                case DetailsStatus.Initialized:
                    this.cboCourtRoom.Enabled = false;
                    this.cboCourtDates.Enabled = false;
                    this.btnPrintCourtReport.Enabled = false;
                    this.btnSelectCourtRoom.Enabled = false;
                    break;
                case DetailsStatus.CourtSelected:
                    this.cboCourtRoom.Enabled = true;
                    this.cboCourtDates.Enabled = false;
                    this.btnPrintCourtReport.Enabled = false;
                    this.btnSelectCourtRoom.Enabled = false;
                    break;
                case DetailsStatus.RoomSelected:
                    this.cboCourtRoom.Enabled = true;
                    this.cboCourtDates.Enabled = true;
                    this.btnPrintCourtReport.Enabled = false;
                    this.btnSelectCourtRoom.Enabled = false;
                    break;
                case DetailsStatus.DateSelected:
                    this.cboCourtRoom.Enabled = true;
                    this.cboCourtDates.Enabled = true;
                    this.btnPrintCourtReport.Enabled = true;
                    this.btnSelectCourtRoom.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// Fill Data to OffenderJudgement
        /// </summary>
        /// <param name="source">Source Data from database</param>
        /// <param name="isSummonsList">Is summons list or detail list</param>
        /// <returns>JudgementList</returns>
        // 2013-04-17 add by Henry for pagination
        private JudgementList FillOffenderJudgementData(DataTable source, bool isSummonsList)
        {
            OffenderJudgement oj = null;
            JudgementList result = new JudgementList();
            foreach (DataRow row in source.Rows)
            {
                oj = new OffenderJudgement((int)row["SumIntNo"]);
                oj.CaseNo = row["SumCaseNo"].ToString();
                oj.CourtDate = (DateTime)row["SumCourtDate"];
                oj.Name = row["AccFullName"].ToString();
                oj.SummonsNo = row["SummonsNo"].ToString();
                oj.NotFilmType = Helper.GetDataRowValue<char>(row, "NotFilmType");

                if (row["SumRemandedFromCourtDate"] != null && !String.IsNullOrEmpty(row["SumRemandedFromCourtDate"].ToString()))
                    oj.SumRemandedFromCourtDate = GetValue<DateTime>(row["SumRemandedFromCourtDate"]);
                oj.SumRemandedFromCaseNumber = row["SumRemandedFromCaseNumber"] == null ? "" : row["SumRemandedFromCaseNumber"].ToString();
                if (row["SumRemandedSummonsFlag"] != null)
                    oj.SumRemandedSummonsFlag = GetValue<bool>(row["SumRemandedSummonsFlag"]);

                // Oscar 20120606 add for push generate summons queue
                if (!row.IsNull("NotIntNo"))
                    oj.NotIntNo = Convert.ToInt32(row["NotIntNo"]);
                if (!row.IsNull("CrtNo"))
                    oj.CrtNo = row["CrtNo"].ToString().Trim();
                if (!row.IsNull("CrtName"))
                    oj.CrtName = row["CrtName"].ToString().Trim();

                if (!isSummonsList)
                {
                    oj.SChIntNo = GetValue<int>(row["SChIntNo"]);
                    oj.Charge = row["SChStatRef"].ToString();
                    oj.Number = row["SChNumber"].ToString();
                    oj.Code = row["SChCode"].ToString();
                    oj.Descr = row["SChDescr"].ToString();
                    oj.FineAmount = GetValue<decimal>(row["SChFineAmount"]);
                    oj.RevisedFineAmount = GetValue<decimal>(row["SChRevAmount"]);
                    oj.SentenceAmount = GetValue<decimal>(row["SchSentenceAmount"]);
                    oj.ContemptAmount = GetValue<decimal>(row["SChContemptAmount"]);
                    oj.OtherAmount = GetValue<decimal>(row["SChOtherAmount"]);
                    oj.Verdict = row["SChVerdict"].ToString();
                    oj.Sentence = row["SChSentence"].ToString();
                    oj.ChargeType = row["ChargeType"].ToString();
                    oj.SChIsMain = GetValue<bool>(row["SChIsMain"]);
                    oj.MainSumChargeID = GetValue<int>(row["MainSumCHargeID"]);
                    oj.IsNoAOG = GetValue<bool>(row["IsNoAOG"]);
                    oj.IsSection35 = GetValue<bool>(row["IsSection35"]);
                }
                if (ViewState["CDate"] == null)
                    ViewState["CDate"] = oj.CourtDate;
                result.Add(oj);
            }
            return result;
        }

        //2013-04-08 Henry separate GetJudgementListSourceRefresh
        private JudgementList GetSummonsJudgementList(out int totalCount)
        {
            int crtRIntNo;
            DateTime crtDate;
            int.TryParse(this.cboCourtRoom.SelectedValue, out crtRIntNo);
            DateTime.TryParse(this.cboCourtDates.SelectedItem.ToString(), out crtDate);

            CourtRoomDB db = new CourtRoomDB(this.connectionString);
            DataTable table = db.GetSummonsForJudgement(crtRIntNo, crtDate, this.txtCaseNo.Text,
                grdCourtRollPager.PageSize, grdCourtRollPager.CurrentPageIndex, out totalCount, this.chkPaidAtCourt.Checked).Tables[0];
            return FillOffenderJudgementData(table, true);
        }

        private JudgementList GetSumChargesJudgementList(int sumIntNo)
        {
            int crtRIntNo;
            DateTime crtDate;
            int.TryParse(this.cboCourtRoom.SelectedValue, out crtRIntNo);
            DateTime.TryParse(this.cboCourtDates.SelectedItem.ToString(), out crtDate);

            CourtRoomDB db = new CourtRoomDB(this.connectionString);
            DataTable table = db.GetSummonsForJudgementSumCharge(crtRIntNo, crtDate, this.txtCaseNo.Text, sumIntNo, this.chkPaidAtCourt.Checked).Tables[0];
            return FillOffenderJudgementData(table, false);
        }

        private bool NeedDeferredPayment()
        {
            if (ViewState["DefferredPayment"] != null)
            {
                if (ViewState["DefferredPayment"].Equals("Y"))
                    return true;
            }
            return false;
        }

        private bool NeedDeferredPaymentReally()
        {
            //result is set to true if there are any charges with "Payment" judgement
            bool result = false;
            foreach (OffenderJudgement judge in this.sumChargeList)
            {
                if (judge.SChIsMain && judge.CJTCode > 0)
                {
                    if (judge.CJTCode != (int)CourtJudgementType.WithdrawnCharge_15 && judge.CJTCode != (int)CourtJudgementType.PaidAtCourt_49)
                    {
                        result = true;
                        break;
                    }
                }
            }

            if (!result && JudgementsLeft != 0)
                result = true;
            return result;
        }

        private string GetAvailableSChIntNoList(out string amountList)
        {
            List<string> tmpSChIntNoList = new List<string>();
            List<string> tmpAmountList = new List<string>();
            foreach (OffenderJudgement judge in this.sumChargeList)
            {
                if (judge.SChIsMain && judge.CJTCode > 0)
                {
                    if (judge.CJTCode != (int)CourtJudgementType.WithdrawnCharge_15 && judge.CJTCode != (int)CourtJudgementType.PaidAtCourt_49)
                    {
                        tmpSChIntNoList.Add(judge.SChIntNo.ToString());
                        //update by rachel 20140808 for 5337
                        //tmpAmountList.Add((judge.SentenceAmount + judge.ContemptAmount + judge.OtherAmount).ToString());
                        tmpAmountList.Add((judge.SentenceAmount + judge.ContemptAmount + judge.OtherAmount).ToString(CultureInfo.InvariantCulture));
                        //end update by rachel 20140808 for 5337
                    }
                }
            }
            amountList = string.Join(",", tmpAmountList.ToArray());
            return string.Join(",", tmpSChIntNoList.ToArray());
        }

        private void CheckDeferredPaymentStatus()
        {
            ViewState["DefferredPayment"] = this.rblDeferredPayment.SelectedValue;
            this.rblDeferredPayment.SelectedIndex = -1;
        }

        protected void btnContinueToCharge_Click(object sender, EventArgs e)
        {
            if (this.rblDeferredPayment.SelectedIndex < 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text55");
                this.lblError.Visible = true;
            }
            else
            {
                CheckDeferredPaymentStatus();
                //SetNextJudgement(true);
                SetNextJudgementCharges();
            }
        }

        // Oscar 2013-02-18 add for case no filter
        protected void grdCourtRoll_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Select":
                    int sumIntNo;
                    if (int.TryParse(Convert.ToString(e.CommandArgument), out sumIntNo))
                    {
                        cache.SelectedSumIntNo = sumIntNo;
                        //SetNextJudgement(false, cache.SelectedSumIntNo);
                        //2013-04-08 Henry mdofiy
                        SetNextJudgementSums(cache.SelectedSumIntNo);
                    }
                    break;
            }
        }

        // Jake 2014-02-24 add 2ND in print file name and court room trim issue,
        // this function only apply to get printfilename for 2ndCCR
        private void SetPrintFileName(string autCode, int autIntNo, int sumIntNo, DateTime newCourtDate, string lastUser, out int pfnIntNo, out DateTime actDate)
        {
            string crtName = string.Empty, crtRoomNo = string.Empty;

            SIL.AARTO.DAL.Entities.Summons summons = new SIL.AARTO.DAL.Services.SummonsService().GetBySumIntNo(sumIntNo);

            SIL.AARTO.DAL.Entities.Court court = new CourtService().GetByCrtIntNo(summons.CrtIntNo);
            SIL.AARTO.DAL.Entities.CourtRoom courtRoom = new CourtRoomService().GetByCrtRintNo(summons.CrtRintNo ?? 0);

            DateRulesDetails drRule = new DateRulesDetails()
            {
                AutIntNo = autIntNo,
                DtRStartDate = "CDate",
                DtREndDate = "FinalCourtRoll",
                LastUser = lastUser
            };
            DefaultDateRules drRuleDB = new DefaultDateRules(drRule, this.connectionString);
            int noOfDays = drRuleDB.SetDefaultDateRule();

            string crtDate = newCourtDate.ToString("yyyy-MM-dd");
            actDate = newCourtDate.AddDays(noOfDays);
            string printDate = actDate.ToString("yyyy-MM-dd HH-mm");
            string printFileName = string.Format("CR2ND_{0}_{1}_{2}_{3}_{4}", autCode, court == null ? "" : court.CrtName.Trim(), courtRoom == null ? "" : courtRoom.CrtRoomNo.Trim(), crtDate, printDate);

            PrintFileNameService pfnSvc = new PrintFileNameService();
            PrintFileNameSummonsService pfnsSvc = new PrintFileNameSummonsService();
            PrintFileName pfnEntity = pfnSvc.GetByPrintFileName(printFileName);
            if (pfnEntity == null)
            {
                pfnEntity = new PrintFileName()
                {
                    PrintFileName = printFileName,
                    AutIntNo = autIntNo,
                    LastUser = lastUser,
                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                };
                pfnEntity = pfnSvc.Save(pfnEntity);
            }
            PrintFileNameSummons pfnsEntity = pfnsSvc.GetBySumIntNoPfnIntNo(summons.SumIntNo, pfnEntity.PfnIntNo);
            if (pfnsEntity == null)
            {
                pfnsEntity = new PrintFileNameSummons()
                {
                    SumIntNo = summons.SumIntNo,
                    PfnIntNo = pfnEntity.PfnIntNo,
                    LastUser = lastUser,
                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                };
                pfnsEntity = pfnsSvc.Save(pfnsEntity);
            }
            pfnIntNo = pfnEntity.PfnIntNo;

        }

        // 2013-04-12 add by Henry for pagination
        protected void grdCourtRollPager_PageChanged(object sender, EventArgs e)
        {
            CourtRollDataBind(false);
        }

        private string CheckIsStandardWOA(int sumIntNo)
        {
            string msg = string.Empty;
            string woaType = string.Empty;
            CourtRoomDB db = new CourtRoomDB(this.connectionString);
            DataTable dtWOA = db.CheckWOAType(sumIntNo, out woaType);
            this.WOAType = woaType;
            if (WOAType == "S") //Standard WOA
            {

            }
            else if (WOAType == "B") // Bench WOA
            {
                this.btnRecordJudgement.Visible = false;
                this.pnlBenchWOA.Visible = true;

            }
            else if (WOAType == "D") // Double WOA
            {
                if (dtWOA != null && dtWOA.Rows.Count > 0)
                {
                    this.gvPreviousWOA.DataSource = dtWOA;
                    this.gvPreviousWOA.DataBind();

                    if (dtWOA.Rows[0]["WOAType"].ToString() != "D") // previous WOA is not a double WOA
                    {
                        this.btnRecordJudgement.Visible = false;
                        this.pnlDoubleWOA.Visible = true;
                    }
                    else
                    {
                        msg = (string)GetLocalResourceObject("lblError.Text61");
                    }
                }
            }

            return msg;


        }

        protected void btnGenerateBenchWOA_Click(object sender, EventArgs e)
        {
            RecordJudgement(1, false);
        }

        protected void btnGenerateDoubleWOA_Click(object sender, EventArgs e)
        {
            RecordJudgement(1, false);
        }

        private string GenerateWOANumber(int autIntNo, string lastUser, out int cntlaIntNo)
        {
            string woaNumber = string.Empty;
            string tranDescr = String.Empty;
            int year = DateTime.Now.Year;
            string courtPrefix = string.Empty;
            string woaNumberPrefix = string.Empty;
            cntlaIntNo = 0;
            int ttIntNo = 0;

            int crtRIntNo = int.Parse(this.cboCourtRoom.SelectedValue);
            int crtIntNo = int.Parse(this.cboCourt.SelectedValue);
            //CourtRoomDB room = new CourtRoomDB(this.connectionString);
            CourtDB courtDB = new CourtDB(this.connectionString);
            CourtRoomTranDB courtRoomTranDB = new CourtRoomTranDB(this.connectionString);

            CourtDetails court = courtDB.GetCourtDetails(crtIntNo);

            if (court != null)
            {
                courtPrefix = court.CrtPrefix;
            }

            if (this.WOAType == "B")
            {
                tranDescr = String.Format(cRTranDescr, "Bench");
                woaNumberPrefix = "BW";
                ttIntNo = (int)TransactionTypeList.Bench_WOA;
            }
            else if (this.WOAType == "D")
            {
                tranDescr = String.Format(cRTranDescr, "Double");
                woaNumberPrefix = "DW";
                ttIntNo = (int)TransactionTypeList.Double_WOA;
            }

            //int cRTranNumber = courtRoomTranDB.GetCaseNoAndWOANextNum(crtRIntNo, cRTranType, cRTranDescr, year, lastUser);

            // 2014-06-16, Oscar added
            var courtYear = Convert.ToDateTime(ViewState["CourtDate"].ToString()).Year.ToString();

            int seqNumber = courtDB.GetAvailabledNextSequenceNumberForWOANumber(crtIntNo,
                ttIntNo,
                this.login, out cntlaIntNo, courtYear);

            if (!String.IsNullOrEmpty(courtPrefix) && !String.IsNullOrEmpty(woaNumberPrefix) && seqNumber > 0)
            {
                woaNumber = String.Format("{0}/{1}/{2}/{3}", woaNumberPrefix.Trim(), courtPrefix.Trim(),
                    //seqNumber.ToString().PadLeft(6, '0'), year.ToString().Substring(2, 2));
                    seqNumber.ToString().PadLeft(6, '0'), courtYear.Substring(2, 2));
            }
            return woaNumber;
        }

        private KeyValuePair<int, string> GetAuthorityRuleByCode(string code)
        {
            AuthorityRulesDetails ar = new AuthorityRulesDetails();
            ar.AutIntNo = autIntNo;
            ar.ARCode = code;
            ar.LastUser = this.login;
            DefaultAuthRules dfAuthRule = new DefaultAuthRules(ar, this.connectionString);
            KeyValuePair<int, string> arValue = dfAuthRule.SetDefaultAuthRule();

            return arValue;
        }

        // Iris 2014/03/28 added for task 5248 Start
        protected void btnAccept_Click(object sender, EventArgs e)
        {
            this.RecordJudgement(1);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            return;
        }
        // Iris 2014/03/28 added for task 5248 End
    }
}
