<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.PrintSummons" Codebehind="PrintSummons.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>
<%@ Register Src="SummonsNumberSearch.ascx" TagName="SummonsNumberSearch" TagPrefix="uc2" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server"/>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        &nbsp;
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    &nbsp;
                </td>
                <td valign="top" align="center" width="100%" colspan="1">
                    <asp:UpdatePanel ID="udpPrintSummons" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" border="0" height="90%" width="100%">
                                <tr>
                                    <td valign="top" style="height: 49px">
                                        <p align="center">
                                            <asp:Label ID="lblPageName" runat="server" Width="856px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                        <p align="center">
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="center">
                                        <asp:Panel ID="pnlGeneral" runat="server">
                                            <table style="width: 877px">
                                                <tr>
                                                    <td style="width: 343px">
                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="309px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                                    <td style="width: 181px" valign="middle">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px">
                                                        </asp:DropDownList></td>
                                                    <td valign="middle" style="width: 408px">
                                                        
                                                        <asp:CheckBox ID="chkSumControlRegister" runat="server" AutoPostBack="True" CssClass="NormalBold"
                                                            OnCheckedChanged="chkShowAll_CheckedChanged" Text="<%$Resources:chkSumControlRegister.Text %>" /></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px; height: 66px;">
                                                        <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="310px" Text="<%$Resources:lblTicketNumberSearch.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="height: 66px; width: 255px;">
                                                        <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                                    </td>
                                                    <td style="width: 408px; height: 66px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px; height: 19px;">
                                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Width="304px" Text="<%$Resources:lblPrint.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 255px; height: 19px;" valign="middle">
                                                        <asp:TextBox ID="txtPrint" runat="server" Width="215px"></asp:TextBox></td>
                                                    <td style="height: 19px; width: 408px;" valign="middle">
                                                        <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOR.Text %>"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px; height: 2px;">
                                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="304px" Text="<%$Resources:lblSequenceNum.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 285px; height: 2px;" valign="middle">
                                                        &nbsp;<uc2:SummonsNumberSearch ID="SummonsNumberSearch" runat="server" OnSummonsSelected="SummonsSelected" />
                                                        </td>
                                                    <td style="height: 2px; width: 408px;" valign="middle">
                                                        <asp:Button ID="btnPrint" runat="server" CssClass="NormalButton" 
                                                            OnClick="btnPrint_Click" Text="<%$Resources:btnPrint.Text %>"  Width="222px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px; height: 2px;">
                                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="304px" Text="<%$Resources:lblSummonsNum.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 285px; height: 2px;" valign="middle">
                                                        <asp:TextBox ID="txtSummons" runat="server" Width="215px"></asp:TextBox>
                                                        </td>
                                                    <td style="height: 2px; width: 408px;" valign="middle">
                                                    </td>
                                                </tr> 
                                                  <tr>
                                                    <td rowspan="2" >
                                                       <asp:CheckBox ID="chkShowAll" runat="server" AutoPostBack="True" CssClass="NormalBold"
                                                            OnCheckedChanged="chkShowAll_CheckedChanged" Text="<%$Resources:chkShowAll.Text %>" />
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblDateFrom" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateFrom.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:TextBox ID="dateFrom" runat="server" autocomplete="off" CssClass="Normal" 
                                                            Height="20px" UseSubmitBehavior="False" />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" Format="yyyy-MM-dd" 
                                                            TargetControlID="dateFrom">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td rowspan="2"  valign="middle">
                                                        <asp:Button ID="btnRefresh" runat="server" CssClass="NormalButton" 
                                                            onclick="btnRefresh_Click" Text="<%$Resources:btnRefresh.Text %>" Width="130px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  valign="middle">
                                                        <asp:Label ID="lblDateTo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateTo.Text %>"></asp:Label>
                                                    </td>
                                                    <td  valign="middle">
                                                        <asp:TextBox ID="dateTo" runat="server" autocomplete="off" CssClass="Normal" 
                                                            Height="20px" UseSubmitBehavior="False" />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd" 
                                                            TargetControlID="dateTo">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px">
                                                        &nbsp
                                                       
                                                    </td>
                                                    <td style="width: 255px;" valign="middle">
                                                        &nbsp;</td>
                                                    <td style="width: 408px;" valign="middle">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:GridView ID="grdErrors" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                            CssClass="Normal" ShowFooter="True">
                                            <FooterStyle CssClass="CartListHead" />
                                            <Columns>
                                                <%--<asp:BoundField DataField="ChargeStatus" HeaderText="Status Code">
                                                Oscar 2010-12 changed for 4416--%>
                                                <asp:BoundField DataField="NoticeStatus" HeaderText="<%$Resources:grdErrors.HeaderText %>">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CSDescr" HeaderText="<%$Resources:grdErrors.HeaderText1 %>">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Count" HeaderText="<%$Resources:grdErrors.HeaderText2 %>">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <%--<asp:HyperLinkField DataNavigateUrlFields="AutIntNo,ChargeStatus" DataNavigateUrlFormatString="SummonsErrorViewer.aspx?AutIntNo={0}&amp;Status={1}" HeaderText="Report" Text="View" Target="_blank">
                                                Oscar 2010-12 changed for 4416--%>
                                                <asp:HyperLinkField DataNavigateUrlFields="AutIntNo,NoticeStatus" DataNavigateUrlFormatString="SummonsErrorViewer.aspx?AutIntNo={0}&amp;Status={1}" HeaderText="<%$Resources:grdErrors.HeaderText3 %>" Text="<%$Resources:grdErrorsItem.Text %>" Target="_blank">
                                                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                                </asp:HyperLinkField>
                                            </Columns>
                                            <HeaderStyle CssClass="CartListHead" />
                                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                                        </asp:GridView>
                                        <pager:AspNetPager id="grdErrorsPager" runat="server" 
                                            showcustominfosection="Right" width="400px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"   PageSize="10" onpagechanged="grdErrorsPager_PageChanged"  UpdatePanelId="udpPrintSummons"
                                        ></pager:AspNetPager>
                                        <br />
                                        <asp:DataGrid ID="dgPrintrun" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                            Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgPrintrun_ItemCommand"
                                            CssClass="Normal" OnEditCommand="dgPrintrun_EditCommand" Width="90%" Visible="False" OnItemCreated="dgPrintrun_ItemCreated">
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="PrintFile" HeaderText="<%$Resources:dgPrintrun.HeaderText %>">
                                                    <HeaderStyle Width="200px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoOFNotices" HeaderText="<%$Resources:dgPrintrun.HeaderText1 %>">
                                                    <HeaderStyle Width="30px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="CrtName" HeaderText="<%$Resources:dgPrintrun.HeaderText2 %>"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText3 %>">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# string.Format("{0:yyyy-MM-dd}",DataBinder.Eval(Container, "DataItem.SumCourtDate")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="50px" />
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="SumSSCompany" HeaderText="<%$Resources:dgPrintrun.HeaderText4 %>">
                                                    <HeaderStyle Width="150px" />
                                                </asp:BoundColumn>
                                              <%--  <asp:EditCommandColumn CancelText="Cancel" EditText="Print Summons" HeaderText="Print Summons"
                                                    UpdateText="Update"></asp:EditCommandColumn>--%>
                                                     <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText5 %>" >
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server"  NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.PrintFile", "SummonsViewer.aspx?printfile={0}&printType=s") %>'
                                                            Target="_blank" Text="<%$Resources:hlPrint.Text %>" ID="hlPrint" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="SumPrintSummonsDate" DataFormatString="{0:yyyy-MM-dd}"
                                                    HeaderText="<%$Resources:dgPrintrun.HeaderText6 %>">
                                                    <HeaderStyle Width="50px" />
                                                </asp:BoundColumn>
                                                <asp:ButtonColumn CommandName="PrintControl" Text="<%$Resources:dgPrintrunItem.Text %>" HeaderText="<%$Resources:dgPrintrun.HeaderText7 %>" />
                                                <asp:BoundColumn DataField="SumPrintControlRegisterDate" DataFormatString="{0:yyyy-MM-dd}"
                                                    HeaderText="<%$Resources:dgPrintrun.HeaderText8 %>">
                                                    <HeaderStyle Width="50px" />
                                                </asp:BoundColumn>
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <pager:AspNetPager id="dgPrintrunPager" runat="server" 
                                            showcustominfosection="Right" width="895px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"
                                              onpagechanged="dgPrintrunPager_PageChanged" UpdatePanelId="udpPrintSummons"></pager:AspNetPager>

                                        &nbsp;<br />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle" /><asp:Label
                                    ID="Label6" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
