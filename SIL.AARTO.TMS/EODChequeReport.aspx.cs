using System;
using System.Web.UI.HtmlControls;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class EODChequeReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "EODChequeReport.aspx";
        //protected string thisPage = "End of Day Cheque Report";
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.dtpStartDate.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
                this.dtpEndDate.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
            }
        }

        

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DateTime dt;

            if (!DateTime.TryParse(this.dtpStartDate.Text, out dt))
            {
                lblError.Text = (string)GetLocalResourceObject("reqStartDate.ErrorMsg");
                lblError.Visible = true;
                return;
            }

            if (!DateTime.TryParse(this.dtpEndDate.Text, out dt))
            {
                lblError.Text = (string)GetLocalResourceObject("reqStartDate.ErrorMsg");
                lblError.Visible = true;
                return;
            }

            if (Convert.ToDateTime(dtpStartDate.Text) > Convert.ToDateTime(dtpEndDate.Text))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            string startDate = string.Format("{0:yyyy-MM-dd}", this.dtpStartDate.Text);
            string endDate = string.Format("{0:yyyy-MM-dd}", this.dtpEndDate.Text);

            Helper_Web.BuildPopup(this, "EODChequeReportViewer.aspx", "StartDate", startDate, "EndDate", endDate, "Option", rdlOption.SelectedValue.ToString());
        }
}
}
