using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS 
{
    public partial class ExclusionList : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "ExclusionList.aspx";
        //protected string thisPage = "Vehicle Exclusion List";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
		    autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddExclusion.Visible = false;
				pnlUpdateExclusion.Visible = false;
				btnOptDelete.Visible = false;

				PopulateAuthorites(autIntNo);

                if (autIntNo > 0)
                {
                    BindGrid(autIntNo);
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    lblError.Visible = true;
                }
			}		
		}

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
		protected void PopulateAuthorites(int autIntNo)
		{
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
			ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

			//reader.Close();

		}

		protected void BindGrid(int autIntNo)
		{
			// Obtain and bind a list of all exclusions for the authority
			ExclusionListDB excList = new ExclusionListDB(connectionString);

            //DataSet data = excList.GetExclusionListDS(autIntNo, txtSearch.Text, "LocCameraCode");
            DataSet data = excList.GetExclusionListDS(autIntNo, txtSearch.Text);
			dgExclusionList.DataSource = data;
			dgExclusionList.DataKeyField = "ExcIntNo";

            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgExclusionList.DataBind();
            }
            catch
            {
                dgExclusionList.CurrentPageIndex = 0;
                dgExclusionList.DataBind();
            }

            if (dgExclusionList.Items.Count == 0)
			{
                dgExclusionList.Visible = false;
				lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
			else
			{
                dgExclusionList.Visible = true;
			}

			data.Dispose();
			pnlAddExclusion.Visible = false;
			pnlUpdateExclusion.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddExclusion.Visible = true;
            txtAddExcRegNo.Text = string.Empty;
            txtAddExcOfficialCapacity.Text = string.Empty;
            dtpAddExcFromDate.Text = string.Empty;
            dtpAddExcToDate.Text = string.Empty;
			pnlUpdateExclusion.Visible = false;
			btnOptDelete.Visible = false;

			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
		}

		protected void btnAddExclusion_Click(object sender, System.EventArgs e)
		{
            if (txtAddExcRegNo.Text.Trim().Length == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            else if (txtAddExcOfficialCapacity.Text.Trim().Length == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }

			// add the new transaction to the database table
			Stalberg.TMS.ExclusionListDB excAdd = new ExclusionListDB(connectionString);

            DateTime addFromDate = DateTime.MinValue;
            DateTime addToDate = DateTime.MaxValue;
            DateTime dt;

            if (DateTime.TryParse(dtpAddExcFromDate.Text.Trim(), out dt))
            {
                addFromDate = dt;
            }
            if (DateTime.TryParse(dtpAddExcToDate.Text.Trim(), out dt))
            {
                addToDate = dt;
            }
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int addExcIntNo = excAdd.AddExclusion
                (Convert.ToInt32(ddlSelectLA.SelectedValue), txtAddExcRegNo.Text.Trim(), txtAddExcOfficialCapacity.Text.Trim(),
                    addFromDate, addToDate, loginUser);
                // Tracy up to here - must convert Datepicker dates to datetime fields for add
                //txtAddLocDescr.Text, 
                //txtAddLocStreetCode.Text, txtAddLocStreetName.Text, ddlAddTravelDirection.SelectedItem.ToString().Substring(0,1),
                //Convert.ToInt32(txtAddLocOffenceSpeedStart.Text));

			if (addExcIntNo == -2)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
			}
			else if (addExcIntNo == -1)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
			}
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ExclusionList, PunchAction.Add);  

            }

			lblError.Visible = true;

			
			BindGrid(autIntNo);
		}

		protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

			BindGrid(autIntNo);
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
            lblError.Visible = true;

            if (txtUpdExcRegNo.Text.Trim().Length == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            else if (txtUpdExcOfficialCapacity.Text.Trim().Length == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }
			// add the new transaction to the database table
			Stalberg.TMS.ExclusionListDB excUpdate = new ExclusionListDB(connectionString);

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int excIntNo = Convert.ToInt32(Session["editExcIntNo"]);

            DateTime updFromDate = DateTime.MinValue;
            DateTime updToDate = DateTime.MaxValue;
            DateTime dt;

            if (DateTime.TryParse(dtpUpdExcFromDate.Text.Trim(), out dt))
            {
                updFromDate = dt;
            }
            if (DateTime.TryParse(dtpUpdExcToDate.Text.Trim(), out dt))
            {
                updToDate = dt;
            }

            int updExcIntNo = excUpdate.UpdateExclusion(autIntNo, txtUpdExcRegNo.Text.Trim(), txtUpdExcOfficialCapacity.Text.Trim(), 
                updFromDate, updToDate, loginUser, excIntNo);
				
			if (updExcIntNo == -2)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
			}
			else if (updExcIntNo == -1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            }
            else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ExclusionList, PunchAction.Change); 
			}

            //int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
			BindGrid(autIntNo);
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgExclusionList.Visible.Equals(true))
			{
				//hide it
				dgExclusionList.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptShow.Text");
			}
			else
			{
				//show it
				dgExclusionList.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		protected void dgExclusionList_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgExclusionList.SelectedIndex = e.Item.ItemIndex;

			if (dgExclusionList.SelectedIndex > -1) 
			{			
				int editExcIntNo = Convert.ToInt32(dgExclusionList.DataKeys[dgExclusionList.SelectedIndex]);

				Session["editExcIntNo"] = editExcIntNo;
				Session["prevPage"] = thisPageURL;

				ShowExclusionDetails(editExcIntNo);
			}
		}

		protected void ShowExclusionDetails(int editExcIntNo)
		{
			// Obtain and bind a list of all exclusions
			Stalberg.TMS.ExclusionListDB exc = new Stalberg.TMS.ExclusionListDB(connectionString);
			
			Stalberg.TMS.ExclusionDetails excDetails = exc.GetExclusionDetails(editExcIntNo);

            txtUpdExcRegNo.Text = excDetails.ExcRegNo;
            txtUpdExcOfficialCapacity.Text = excDetails.ExcOfficialCapacity;

            if (excDetails.ExcFromDate != null && excDetails.ExcFromDate.CompareTo(DateTime.MinValue) > 0)
                dtpUpdExcFromDate.Text = excDetails.ExcFromDate.ToString("yyyy-MM-dd");
            else
                dtpUpdExcFromDate.Text = string.Empty;

            dtpUpdExcFromDate.Enabled = true;

            if (excDetails.ExcToDate != null && excDetails.ExcToDate.CompareTo(DateTime.MinValue) > 0)
                dtpUpdExcToDate.Text = excDetails.ExcToDate.ToString("yyyy-MM-dd");
            else
                dtpUpdExcToDate.Text = string.Empty;

            dtpUpdExcToDate.Enabled = true;	
					
			pnlUpdateExclusion.Visible = true;
			pnlAddExclusion.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
			BindGrid(autIntNo);
		}

		protected void dgExclusionList_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
            dgExclusionList.CurrentPageIndex = e.NewPageIndex;

			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

			BindGrid(autIntNo);
		}

		 

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int excIntNo = Convert.ToInt32(Session["editExcIntNo"]);

            ExclusionListDB exc = new Stalberg.TMS.ExclusionListDB(connectionString);

            string delExcIntNo = exc.DeleteExclusion(excIntNo);
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            if (delExcIntNo.Equals("-2"))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            }
            else if (delExcIntNo.Equals("-1"))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ExclusionList, PunchAction.Delete); 
            }

            lblError.Visible = true;

           

            BindGrid(autIntNo);
        }

        protected void dtpAddExcFromDate_TextChanged(object sender, EventArgs e)
        {
            this.dtpAddExcFromDate.Text = this.dtpAddExcFromDate.Text.Trim();

            if (!this.dtpAddExcFromDate.Text.Equals(string.Empty))
            {
                DateTime dtStart = DateTime.MaxValue;

                if (!DateTime.TryParse(this.dtpAddExcFromDate.Text, out dtStart))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                    lblError.Visible = true;
                    return;
                }

                this.dtpAddExcToDate.Text = dtStart.AddDays(30).ToString("yyyy-MM-dd");
                return;
            }
        }

        protected void dtpUpdExcFromDate_TextChanged(object sender, EventArgs e)
        {
            this.dtpUpdExcFromDate.Text = this.dtpUpdExcFromDate.Text.Trim();

            if (!this.dtpUpdExcFromDate.Text.Equals(string.Empty))
            {
                DateTime dtStart = DateTime.MaxValue;

                if (!DateTime.TryParse(this.dtpUpdExcFromDate.Text, out dtStart))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                    lblError.Visible = true;
                    return;
                }

                this.dtpUpdExcToDate.Text = dtStart.AddDays(30).ToString("yyyy-MM-dd");
                return;
            }
        }
}
}
