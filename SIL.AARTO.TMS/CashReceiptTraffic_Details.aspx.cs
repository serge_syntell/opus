using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using Stalberg.TMS.Data.Util;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Contains all the logic for collecting payments for traffic fines
    /// </summary>
    public partial class CashReceiptTraffic_Details : System.Web.UI.Page
    {
        // Constants
        private const string CURRENCY_FORMAT = "R ###0.00";
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private const string CHECKED_IMAGE = "images/checked.gif";
        private const string UNCHECKED_IMAGE = "images/unchecked.gif";

        // Fields    
        private string _connectionString = string.Empty;
        private string _login;
        private int _autIntNo;
        private bool _requireAddresses;
        private bool _allowOnTheFlyRepresentations;
        private bool _hasRepresentation;
        private bool _mandatoryPhoneNo = true;
        private bool _mandatoryReceivedFrom = true;
        private bool _isSuspenseNotice;
        private decimal _currentFineAmount;
        private int _minStatus = 255;
        private int _tranIntNo;
        private int _maxStatus = 900;
        private DateTime _tranDate = DateTime.MinValue;
        private int _noDaysForLastPaymentDate;
        private int _noOfDaysForPayment;
        private DataSet _dsReceiptPayments;
        private Cashier _cashier;
        private CashType _cashType = CashType.Cash;
        private PersonaType _personaType = PersonaType.Driver;
        private List<NoticeForPayment> _notices;
        private CashReceiptDB _cashReceipt;
        private bool _isOneReceiptPerNotice;
        private bool _isDisplayByID;
        private bool _isSelectAllNotices;
        private bool _usePostalReceipt;
        private int _isBankPayment;
        //Barry Dickson 20080408 adding in authority rules
        private bool _cashAfterSummons;
        private bool _cashAfterCourtDate;
        private bool _cashAfterWarrant;
        //BD 20080424 adding in rule for cash after grace period
        private bool _cashAfterGracePeriod;
        //SD
        private bool _cashAfterCaseNumber;
        //private AuthorityRulesDB authRules;
        //private AuthorityRulesDetails ard;
        private int _noOfNotices;

        protected string styleSheet;
        protected string backgroundImage;
        protected double myTotal = 0;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "CashReceiptTraffic_Details.aspx";
        //protected string thisPage = "Cash Receipt Details: Traffic Department";
        protected static string _pymtOriginator = String.Format("{0}", (int)PaymentOriginatorList.OPUS);


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this._connectionString = Application["constr"].ToString();
            _noOfDaysForPayment = 0;
            _noDaysForLastPaymentDate = 0;

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["BAIntNo"] == null)
                Response.Redirect("CashReceiptTraffic.aspx");

            //int.Parse(Session["BAIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            _login = userDetails.UserLoginName;
            this._autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // May need to check user access level here....
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            this.GetAuthRuleSettings();

            // FBJ Added (2006-08-15): Check to see if the current user is a valid cashier
            BankAccountDB bankAccount = new BankAccountDB(_connectionString);
            this._cashier = bankAccount.GetUserBankAccount(int.Parse(Session["userIntNo"].ToString()));
            if (this._cashier == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
            {
                //dls 100715 - please can we do things properly and call a spade a spade - trying to reuse columns when we've renamed them is not very helpful
                //this.Session.Add("BAIntNo", _cashier.CashBoxUserIntNo);
                this.Session.Add("BAIntNo", _cashier.BAIntNo);
                this.Session.Add("CashBoxUserIntNo", _cashier.CashBoxUserIntNo);

                Session.Add("CBIntNo", _cashier.CBIntNo);

                // FBJ Added (2006-09-26): If the cashier must count the cash box before receiving anything then redirect them
                if (_cashier.MustCountCashBox)
                    Response.Redirect("CashBoxCount.aspx");

                this._requireAddresses = (this._cashier.CashBoxType == CashboxType.PostalReceipts);
            }

            _cashReceipt = new CashReceiptDB(this._connectionString);

            this.GetSettingsFromViewState();

            //// Hook up the Representation event handler
            //this.RepresentationOnTheFly1.RepresentationSuccessful += new RepresentationOnTheFlyEventHandler(RepresentationOnTheFly1_RepresentationSuccessful);

            //this.txtTotal.Enabled = (this.allowOnTheFlyRepresentations || this.isSuspenseNotice);

            if (!this.Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.wdcDateReceived.Text = DateTime.Today.ToString(DATE_FORMAT);

                //hide representation option
                this.chkCashierRep.Visible = false;
                this.lblRepresentations.Visible = false;
                this.btnContinue.Visible = false;
                this.btnBackFromRepresentations.Visible = false;

                this.PopulateCrCardExpiryYear(ddlCreditCardYear);
                this.ddlCardMonth.SelectedIndex = 0;

                this.PopulateCrCardExpiryYear(ddlDebitCardYear);
                this.ddlDCardMonth.SelectedIndex = 0;

                this.PopulatePaymentTypes();
                this.PopulateAuthorites();
                this.pnlAddPayment.Visible = true;
                this.pnlAddress.Visible = false;
                this.pnlSuspenseNotice.Visible = false;

                string id = string.Empty;
                if (Request.QueryString["IDNo"] != null)
                    id = Request.QueryString["IDNo"].Trim();

                string ticket = string.Empty;
                if (Request.QueryString["TicketNo"] != null)
                    ticket = Request.QueryString["TicketNo"].Replace(" ", "");

                if (Request.QueryString["PymtOriginator"] != null)
                    _pymtOriginator = Request.QueryString["PymtOriginator"].Trim();
                //Comment out suspense account at the moment but it will come back later
                if (id.Length == 0 && ticket.Length == 0)
                {
                    // Check in the query string to see if this is a suspense account notice
                    for (int i = 0; i < Request.QueryString.Keys.Count; i++)
                    {
                        if (Request.QueryString[i].Equals("Suspense", StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.pnlSuspenseNotice.Visible = true;
                            this._isSuspenseNotice = true;
                            this.txtTotal.Enabled = true;
                            this.ViewState.Add("isSuspenseNotice", this._isSuspenseNotice);
                            //if (Request.QueryString["TicketNo"] != null)
                            //    ticket = Request.QueryString["TicketNo"].Replace(" ", "");
                            this.txtNoticeNumber.Text = ticket;
                            this.StartSuspenseNoticeProcessing();
                            return;
                        }
                    }

                    //if (!this.isSuspenseNotice)
                    //{
                    // It is an invalid search - no criteria
                    // Jake 20120208 added 
                    Response.Redirect("CashReceiptTraffic.aspx");
                    //}
                }

                Session["MinimalCapture_TicketNo"] = ticket;
                // Populate the main grid
                this.ShowNoticeSearchResults(id, ticket);
                this.ShowHideSummonsLink();

            }
            else if (this.dgTicketDetails.Items.Count > 0)
            {
                // Set the items in the grid from the notice list
                int notIntNo = 0;

                foreach (DataGridItem item in this.dgTicketDetails.Items)
                {
                    ImageButton check = null;
                    notIntNo = int.Parse(item.Cells[0].Text);

                    foreach (NoticeForPayment notice in this._notices)
                    {
                        if (notice.NotIntNo == notIntNo)
                        {
                            check = (ImageButton)item.FindControl("igPayNow");
                            check.ImageUrl = notice.IsSelected ? CHECKED_IMAGE : UNCHECKED_IMAGE;
                            break;
                        }
                    }
                }

                CalculateTotal(false, "");
            }
            else if (this.dgDeferredPayments.Items.Count > 0)
            {
                // Set the items in the grid from the notice list
                int scDPIntNo = 0;

                foreach (DataGridItem item in this.dgDeferredPayments.Items)
                {
                    ImageButton check = null;
                    scDPIntNo = int.Parse(item.Cells[11].Text);
                    foreach (NoticeForPayment notice in this._notices)
                    {
                        if (notice.DPIntNo == scDPIntNo)
                        {
                            check = (ImageButton)item.FindControl("igDeferredPayNow");
                            check.ImageUrl = notice.IsSelected ? CHECKED_IMAGE : UNCHECKED_IMAGE;
                            break;
                        }
                    }
                }

                CalculateTotal(false, "deferred");
            }
            else if (this._isSuspenseNotice)
            {
                CalculateTotal(false, "");
            }
        }

        private void PopulateCrCardExpiryYear(DropDownList ddlYear)
        {
            ddlYear.Items.Clear();

            int year = DateTime.Today.Year;

            for (int i = year; i <= year + 5; i++)
            {
                ddlYear.Items.Add(i.ToString());
            }

            ddlYear.Items.Insert(0, (string)GetLocalResourceObject("ddlYear.Items"));
            ddlYear.SelectedIndex = 0;

        }

        private DataSet PopulateSeniorOfficers()
        {
            DataSet ds;

            if (Session["SeniorOfficers"] == null)
            {
                TrafficOfficerDB db = new TrafficOfficerDB(this._connectionString);
                ds = db.GetTrafficOfficerListByAuthorityDS(this._autIntNo, OfficerSeniority.Senior);

                Session["SeniorOfficers"] = ds;
            }
            else
            {
                ds = (DataSet)Session["SeniorOfficers"];
            }
            return ds;
        }

        private void RefreshDeferredNoticeGrid()
        {
            //ShowHideColumns(this.hasRepresentation);

            // Set the items in the grid from the notice list
            int notIntNo = 0;

            decimal fullAmount = 0;

            foreach (DataGridItem item in this.dgDeferredPayments.Items)
            {
                //DropDownList ddlSeniorOfficer = null;
                //TextBox txtAmountToPay = null;

                notIntNo = int.Parse(item.Cells[0].Text);

                fullAmount = this.ParseValue(item.Cells[9].Text.Trim());

                foreach (NoticeForPayment notice in this._notices)
                {
                    if (notice.NotIntNo == notIntNo)
                    {
                        if (this._hasRepresentation)
                        {
                            //item.Visible = notice.IsSelected;

                            //if (notice.IsSelected)
                            //{
                            //    DataSet ds = PopulateSeniorOfficers();

                            //    ddlSeniorOfficer = (DropDownList)item.FindControl("ddlSeniorOfficer");
                            //    txtAmountToPay = (TextBox)item.FindControl("txtAmountToPay");

                            //    if (ddlSeniorOfficer != null)
                            //    {
                            //        ddlSeniorOfficer.DataSource = ds;
                            //        ddlSeniorOfficer.DataTextField = "TODescr";
                            //        ddlSeniorOfficer.DataBind();

                            //        ddlSeniorOfficer.Items.Insert(0, "Public Prosecutor");

                            //        if (!notice.SeniorOfficer.Equals(string.Empty))
                            //            ddlSeniorOfficer.Text = notice.SeniorOfficer;
                            //    }

                            //    if (notice.AmountToPay == 0)
                            //        notice.AmountToPay = fullAmount;

                            //    if (txtAmountToPay != null)
                            //        txtAmountToPay.Text = string.Format("{0:0.00}", notice.AmountToPay);


                            //    txtAmountToPay.Enabled = !notice.HasJudgement;
                            //    ddlSeniorOfficer.Enabled = !notice.HasJudgement;
                            //}
                        }
                        else
                            item.Visible = true;

                        break;
                    }
                }
            }
        }

        private void RefreshNoticeGrid()
        {
            ShowHideColumns(this._hasRepresentation);

            // Set the items in the grid from the notice list
            int notIntNo = 0;
            decimal fullAmount = 0;

            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                DropDownList ddlSeniorOfficer = null;
                TextBox txtAmountToPay = null;

                notIntNo = int.Parse(item.Cells[0].Text);

                fullAmount = this.ParseValue(item.Cells[7].Text.Trim());

                string tickNo = ((Label)item.FindControl("lblTicketNo")).Text;

                NoticeForPaymentDB noticeDB = new NoticeForPaymentDB(_connectionString);

                foreach (NoticeForPayment notice in this._notices)
                {
                    if (notice.NotIntNo == notIntNo)
                    {
                        if (this._hasRepresentation)
                        {
                            item.Visible = notice.IsSelected;

                            if (notice.IsSelected)
                            {
                                DataSet ds = PopulateSeniorOfficers();

                                ddlSeniorOfficer = (DropDownList)item.FindControl("ddlSeniorOfficer");
                                txtAmountToPay = (TextBox)item.FindControl("txtAmountToPay");

                                if (ddlSeniorOfficer != null)
                                {
                                    ddlSeniorOfficer.DataSource = ds;
                                    ddlSeniorOfficer.DataTextField = "TODescr";
                                    ddlSeniorOfficer.DataBind();

                                    ddlSeniorOfficer.Items.Insert(0, (string)GetLocalResourceObject("ddlSeniorOfficer.Items"));

                                    if (!notice.SeniorOfficer.Equals(string.Empty))
                                        ddlSeniorOfficer.Text = notice.SeniorOfficer;
                                }

                                if (notice.AmountToPay == 0)
                                    notice.AmountToPay = fullAmount;

                                //update by Rachel 20140811 for 5337
                                //if (txtAmountToPay != null)
                                //    txtAmountToPay.Text = string.Format("{0:0.00}", notice.AmountToPay);

                                if (txtAmountToPay != null)
                                    txtAmountToPay.Text = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", notice.AmountToPay);
                                //end update by Rachel 20140811 for 5377

                                string filmType = string.Empty;

                                if (!tickNo.Equals(string.Empty))
                                {
                                    filmType = noticeDB.GetFilmType(tickNo.Replace("-", string.Empty).Replace("/", string.Empty));
                                }

                                // dls 2011-06-30 - disabling 'H' as well - the grids do not handle multiple charges under a single notice

                                if (filmType.Equals("M") || filmType.Equals("H"))
                                {
                                    txtAmountToPay.Enabled = false;
                                    ddlSeniorOfficer.Enabled = false;
                                    if (item.Cells.Count == 18)
                                        item.Cells[16].Enabled = false;
                                }
                                else
                                {
                                    txtAmountToPay.Enabled = !notice.HasJudgement;
                                    ddlSeniorOfficer.Enabled = !notice.HasJudgement;
                                }


                            }
                        }
                        else
                            item.Visible = true;

                        break;
                    }
                }
            }
        }

        private void ShowHideColumns(bool show)
        {
            foreach (DataGridColumn col in dgTicketDetails.Columns)
            {
                switch (col.HeaderText)
                {
                    case "New Fine Amount":
                    case "Senior Officer":
                    case "":
                        col.Visible = show;
                        break;
                    default:
                        break;
                }
            }
        }

        private void ShowHideSummonsLink()
        {
            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                if (item.Cells[8].Text == "Notice issued")
                {
                    //need to disable link to summons details as there is no summons
                    item.Cells[11].Enabled = false;
                }
            }
        }

        #region Settings

        private void GetAuthRuleSettings()
        {
            //use this for all authority rules
            //authRules = new AuthorityRulesDB(_connectionString);

            // Check minimum payable status
            this.CheckMinimumStatusRule();

            // Check maximum payable status
            this.CheckMaximumStatusRule();

            // Check the date rule for the last payment date
            this.CheckLastPaymentDate();

            // Check authority rule for returning all tickets for the owner/driver/proxy of the vehicle
            this.CheckIDRule();

            this.CheckSelectAllRule();

            // Updated LMZ 2007-03-06: check the rule to make this optional
            this.CheckMandatoryPhoneNo();

            // Updated LMZ 2007-09-13: check the rule to make this optional
            this.CheckMandatoryReceivedFrom();

            this.CheckAllOnTheFlyReps();

            this.CheckOneReceiptPerNoticeRule();

            //Barry Dickson 20080408 new rules for cashier
            this.CheckCashAfterCourtDateRule();
            this.CheckCashAfterSummonsServedRule();
            this.CheckCashAfterWarrantRule();

            //BD 20080624 cash after grace period
            this.CheckCashAfterGracePeriodRule();

            // SD 20081202 cash after case number
            this.CheckCashAfterCaseNumberRule();
        }

        private void CheckMaximumStatusRule()
        {
            if (this.ViewState["MaxStatus"] != null)
                this._maxStatus = (int)ViewState["MaxStatus"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4570";
                //rule.ARComment = "900 (Default); See ChargeStatus table for allowable values (status must be 255 or greater)";
                //rule.ARDescr = "Rule to set maximum charge status at which payment/representations are allowed";
                //rule.ARNumeric = 900;
                //rule.ARString = string.Empty;
                //rule.AutIntNo = this.autIntNo;
                //rule.LastUser = this.login;

                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4570";
                rule.LastUser = this._login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();


                this._maxStatus = value.Key; //rule.ARNumeric;
                this.ViewState.Add("MaxStatus", this._maxStatus);
            }
        }

        private void GetSettingsFromViewState()
        {
            if (Session["IsBankPayment"] != null)
                _isBankPayment = (int)Session["IsBankPayment"];

            // FBJ Added (2006-11-13): Set the type of cash received
            if (_isBankPayment == 2)
            {
                this._cashType = CashType.BankPayment;
                this.ViewState.Add("CashType", this._cashType);
            }
            else if (this.ViewState["CashType"] != null)
                this._cashType = (CashType)this.ViewState["CashType"];
            else
                this.ViewState.Add("CashType", this._cashType);

            // FBJ Added (2006-11-13): Set the type of cash received
            if (this.ViewState["HasRepresentation"] != null)
                this._hasRepresentation = (bool)this.ViewState["HasRepresentation"];
            else
                this.ViewState.Add("HasRepresentation", this._hasRepresentation);

            // dls 080122 - get the person receiving the payment
            if (this.ViewState["PersonaType"] != null)
                this._personaType = (PersonaType)this.ViewState["PersonaType"];
            else
                this.ViewState.Add("PersonaType", this._personaType);

            // FBJ Added (2007-01-29): Extract the notices list from the session
            if (this.Session["NoticesToPay"] == null)
                Response.Redirect("CashReceiptTraffic.aspx");

            this._notices = (List<NoticeForPayment>)this.Session["NoticesToPay"];

            //dls 080125 - get the data from the session state if it has been created
            this.GetReceiptTranSettingsFromSession();

            if (this.ViewState["isSuspenseNotice"] != null)
                this._isSuspenseNotice = (bool)this.ViewState["isSuspenseNotice"];

            if (this.ViewState["currentFineAmount"] != null)
                this._currentFineAmount = (decimal)this.ViewState["currentFineAmount"];

            if (this.ViewState["noOfDaysForPayment"] != null)
                this._noOfDaysForPayment = (int)this.ViewState["noOfDaysForPayment"];
            else
            {
                this._noOfDaysForPayment = this.GetNoOfDaysForPayment(_autIntNo);
                this.ViewState["noOfDaysForPayment"] = this._noOfDaysForPayment;
            }
        }

        private void CheckMandatoryReceivedFrom()
        {
            if (this.ViewState["mandatoryReceivedFrom"] == null)
            {
                // ard = authRules.GetAuthorityRulesDetailsByCode(this._autIntNo, "4521", "Rule to make the receipt received from field optional", 0, "N", "Y = Yes; N= No(default) mandatory", this.login);
                //20090113 SD	
                //AutIntNo, ARCode and LastUser need to be set from here
                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = this._autIntNo;
                ard.ARCode = "4521";
                ard.LastUser = this._login;

                DefaultAuthRules authRule = new DefaultAuthRules(ard, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                if (ard.ARString == "Y")
                    this._mandatoryReceivedFrom = false;
                this.ViewState["mandatoryReceivedFrom"] = this._mandatoryReceivedFrom;
            }
            else
                this._mandatoryReceivedFrom = (bool)this.ViewState["mandatoryReceivedFrom"];

        }

        private void CheckMandatoryPhoneNo()
        {
            if (this.ViewState["mandatoryPhoneNo"] == null)
            {
                //
                //ard = authRules.GetAuthorityRulesDetailsByCode(this._autIntNo, "4520", "Rule to make the receipt telephone number optional", 0, "N", "Y = Yes; N= No(default) mandatory", this.login);
                //20090113 SD	
                //AutIntNo, ARCode and LastUser need to be set from here
                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = this._autIntNo;
                ard.ARCode = "4520";
                ard.LastUser = this._login;

                DefaultAuthRules authRule = new DefaultAuthRules(ard, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                if (ard.ARString == "Y")
                    this._mandatoryPhoneNo = false;
                this.ViewState["mandatoryPhoneNo"] = this._mandatoryPhoneNo;
            }
            else
                this._mandatoryPhoneNo = (bool)this.ViewState["mandatoryPhoneNo"];

        }

        private void CheckUsePostalReceipt()
        {
            // LMZ Added (2007-03-02): To check for Authority Rule - print all receipts to Postal Receipt
            if (this.ViewState["UsePostalReceipt"] == null)
            {
                //ard = authRules.GetAuthorityRulesDetailsByCode(this._autIntNo, "4510", "Rule to indicate using of postal receipt for all receipts", 0, "N", "Y = Yes; N= No(default)", this.login);

                //20090113 SD	
                //AutIntNo, ARCode and LastUser need to be set from here
                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = this._autIntNo;
                ard.ARCode = "4510";
                ard.LastUser = this._login;

                DefaultAuthRules authRule = new DefaultAuthRules(ard, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._usePostalReceipt = ard.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ? true : false;
                this.ViewState["UsePostalReceipt"] = this._usePostalReceipt;
            }
            else
                this._usePostalReceipt = (bool)this.ViewState["UsePostalReceipt"];

        }

        private void CheckAllOnTheFlyReps()
        {
            // FBJ: Enable/disable based on the Authority rule for making representations on the fly.
            if (this.ViewState["AllowOnTheFlyRepresentations"] == null)
            {
                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4600";
                //rule.ARDescr = "Rule to Allow On-The-Fly Representations in cash receipting.";
                //rule.ARComment = "N = on-the-fly representations are NOT allowed (Default), Y = they are!";
                //rule.ARNumeric = 0;
                //rule.ARString = "N";
                //rule.AutIntNo = this._autIntNo;
                //rule.LastUser = this.login;
                //authRules.GetDefaultRule(rule);

                //SD: 20090107 
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4600";
                rule.LastUser = this._login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._allowOnTheFlyRepresentations = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                //rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("AllowOnTheFlyRepresentations", _allowOnTheFlyRepresentations);
            }
            else
                this._allowOnTheFlyRepresentations = (bool)this.ViewState["AllowOnTheFlyRepresentations"];

        }

        private void CheckMinimumStatusRule()
        {
            if (this.ViewState["MinStatus"] != null)
                this._minStatus = (int)ViewState["MinStatus"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4605";
                //rule.ARComment = "Default = 255 (posted).";
                //rule.ARDescr = "Status to start showing notices on enquiry screen";
                //rule.ARNumeric = 255;
                //rule.ARString = string.Empty;
                //rule.AutIntNo = this._autIntNo;
                //rule.LastUser = this.login;

                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4605";
                rule.LastUser = this._login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._minStatus = value.Key; //rule.ARNumeric;
                this.ViewState.Add("MinStatus", this._minStatus);
            }
        }

        private int GetNoOfDaysForPayment(int autIntNo)
        {
            AuthorityRulesDetails arule = new AuthorityRulesDetails();
            arule.AutIntNo = autIntNo;
            arule.ARCode = "4660";
            arule.LastUser = this._login;

            DefaultAuthRules paymentRule = new DefaultAuthRules(arule, this._connectionString);
            KeyValuePair<int, string> payment = paymentRule.SetDefaultAuthRule();

            return payment.Key;
        }

        private void CheckLastPaymentDate()
        {
            if (this.ViewState["NoDaysForLastPaymentDate"] != null)
                this._noDaysForLastPaymentDate = (int)ViewState["NoDaysForLastPaymentDate"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4505";
                //rule.ARComment = "The number of days that will be subtracted from the court date as the cut-off date for payment.";
                //rule.ARDescr = "Rule indicating the number of days before the court date that payments will still be accepted.";
                //rule.ARString = string.Empty;
                //rule.ARNumeric = 1;
                //rule.LastUser = this.login;

                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                //SD:20080107
                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4505";
                rule.LastUser = this._login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._noDaysForLastPaymentDate = value.Key; //rule.ARNumeric;
                this.ViewState.Add("NoDaysForLastPaymentDate", this._noDaysForLastPaymentDate);
            }
        }

        private void CheckSelectAllRule()
        {
            if (this.ViewState["AllNoticesForPayment"] != null)
                this._isSelectAllNotices = (bool)ViewState["AllNoticesForPayment"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4400";
                //rule.ARComment = "N=No (Default); Y=Yes; default selects only those specifically searched for or previously selected";
                //rule.ARDescr = "Rule to check all notices as ready for payment.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);


                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4400";
                rule.LastUser = this._login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._isSelectAllNotices = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("AllNoticesForPayment", this._isSelectAllNotices);
            }
        }
        //Barry Dickson 20080408 - new rules to check when allowed to receipt cash
        private void CheckCashAfterSummonsServedRule()
        {
            if (this.ViewState["AcceptCashAfterSummons"] != null)
                this._cashAfterSummons = (bool)ViewState["AcceptCashAfterSummons"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4710";
                //rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash once the court date has passed.";
                //rule.ARDescr = "Rule to check if allowed to accept cash for notices where the court date has passed.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4710";
                rule.LastUser = this._login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._cashAfterSummons = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("AcceptCashAfterSummons", this._cashAfterSummons);
            }
        }
        //Barry Dickson 20080408 - new rules to check when allowed to receipt cash
        private void CheckCashAfterWarrantRule()
        {
            if (this.ViewState["AcceptCashAfterWarrant"] != null)
                this._cashAfterWarrant = (bool)ViewState["AcceptCashAfterWarrant"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4720";
                //rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash after warrant issued";
                //rule.ARDescr = "Rule to check if allowed to accept cash for notices where a warrant has been issued.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4720";
                rule.LastUser = this._login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._cashAfterWarrant = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("AcceptCashAfterWarrant", this._cashAfterWarrant);
            }
        }
        //Barry Dickson 20080408 - new rules to check when allowed to receipt cash
        private void CheckCashAfterCourtDateRule()
        {
            if (this.ViewState["AcceptCashAfterCourtDate"] != null)
                this._cashAfterCourtDate = (bool)ViewState["AcceptCashAfterCourtDate"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4730";
                //rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash once the court date has passed.";
                //rule.ARDescr = "Rule to check if allowed to accept cash for notices where the court date has passed.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4730";
                rule.LastUser = this._login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._cashAfterCourtDate = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("AcceptCashAfterCourtDate", this._cashAfterCourtDate);
            }
        }
        //BD 20080624 - new rules to check when allowed to receipt cash after grace period
        private void CheckCashAfterGracePeriodRule()
        {
            if (this.ViewState["AcceptCashAfterGracePeriod"] != null)
                this._cashAfterGracePeriod = (bool)ViewState["AcceptCashAfterGracePeriod"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4740";
                //rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash after the grace period has expired.";
                //rule.ARDescr = "Rule to check if allowed to accept cash for notices where the grace period has expired.";
                //rule.ARString = "Y";
                //rule.ARNumeric = 1;
                //rule.LastUser = "System";
                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4740";
                rule.LastUser = this._login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._cashAfterGracePeriod = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("AcceptCashAfterGracePeriod", this._cashAfterGracePeriod);
            }
        }


        //SD: 20081202:- new rules to check when allowed to receipt cash after CaseNumber has generated
        private void CheckCashAfterCaseNumberRule()
        {
            if (this.ViewState["AcceptCashAfterCaseNumber"] != null)
                this._cashAfterCaseNumber = (bool)ViewState["AcceptCashAfterCaseNumber"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "6011";
                //rule.ARComment = "N=No (Default); Y=Yes;";
                //rule.ARDescr = "Allow receipts after the case number has been generated (court roll)";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.ARCode = "6011";
                rule.AutIntNo = this._autIntNo;
                rule.LastUser = this._login;

                DefaultAuthRules ar = new DefaultAuthRules(rule, this._connectionString);

                KeyValuePair<int, string> cashAfterCase = ar.SetDefaultAuthRule();

                //this.CashAfterGracePeriod = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this._cashAfterCaseNumber = cashAfterCase.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("AcceptCashAfterCaseNumber", this._cashAfterCaseNumber);
            }
        }


        private void CheckIDRule()
        {
            if (this.ViewState["DisplayByID"] != null)
                this._isDisplayByID = (bool)ViewState["DisplayByID"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "3300";
                //rule.ARComment = "N = No (Default); Y = Yes";
                //rule.ARDescr = "Rule for whether to display all notices for an ID when processing a payment.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";
                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "3300";
                rule.LastUser = this._login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this._isDisplayByID = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("DisplayByID", this._isDisplayByID);
            }
        }

        private void CheckOneReceiptPerNoticeRule()
        {
            if (ViewState["isOneReceiptPerNotice"] != null)
                _isOneReceiptPerNotice = (bool)ViewState["isOneReceiptPerNotice"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4550";
                //rule.ARComment = "N = No (Default); Y = Yes";
                //rule.ARDescr = "Rule to set one receipt per notice.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";
                //AuthorityRulesDB db = new AuthorityRulesDB(this._connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this._autIntNo;
                rule.ARCode = "4550";
                rule.LastUser = this._login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                _isOneReceiptPerNotice = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                //rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("isOneReceiptPerNotice", this._isOneReceiptPerNotice);
            }

        }

        private void GetReceiptTranSettingsFromSession()
        {
            //dls 080207 - need to use the Session state to store these values, as the page is reloaded if new notices are added from CashReceiptTraffic screen
            if (Session["TRHIntNo"] != null)
            {
                _tranIntNo = Int32.Parse(Session["TRHIntNo"].ToString());

                BindPaymentGrid();
            }

            if (Session["TRHTranDate"] != null)
            {
                _tranDate = DateTime.Parse(Session["TRHTranDate"].ToString());
            }
        }

        #endregion

        #region Initialise

        private ListItem GetListItem(string text, string val)
        {
            return new ListItem((string)GetLocalResourceObject(text), val);
        }

        private void InitBaseComboPaymentType(DropDownList comboPaymentType)
        {
            comboPaymentType.Items.Add(GetListItem("comboPaymentType.Items", "Cash"));
            comboPaymentType.Items.Add(GetListItem("comboPaymentType.Items1", "Cheque"));
            comboPaymentType.Items.Add(GetListItem("comboPaymentType.Items2", "Credit Card"));
            comboPaymentType.Items.Add(GetListItem("comboPaymentType.Items3", "Debit Card")); // SD:20081209 - added debit card
            comboPaymentType.Items.Add(GetListItem("comboPaymentType.Items4", "Postal Order"));

            if (NeedOtherCashType())
                comboPaymentType.Items.Add(GetListItem("comboPaymentType.Items7", "Other"));
        }
        private bool NeedOtherCashType()
        {
            bool flag = false;
            int autIntNo = 0;
            if (Session["autIntNo"] != null)
                autIntNo = Convert.ToInt32(Session["autIntNo"]);
            AuthorityRulesDB rulesDB = new AuthorityRulesDB(this._connectionString);
            AuthorityRulesDetails rulesDetail = rulesDB.GetAuthorityRule(autIntNo, "9520");
            if (rulesDetail != null)
            {
                if (rulesDetail.ARString.ToUpper() == "Y")
                    flag = true;
            }
            return flag;
        }
        private void PopulatePaymentTypes()
        {
            comboPaymentType.Items.Clear();

            //0 = All; 1 = Exclude Bank Payments; 2 = Only Bank Payments
            switch (_isBankPayment)
            {
                case 0:
                    InitBaseComboPaymentType(comboPaymentType);
                    comboPaymentType.Items.Add(GetListItem("comboPaymentType.Items5", "Bank Payment"));
                    break;

                case 1:
                    InitBaseComboPaymentType(comboPaymentType);
                    break;

                case 2:
                    comboPaymentType.Items.Add(GetListItem("comboPaymentType.Items5", "Bank Payment"));
                    break;
            }

            //find the index for the selected cash type
            // SD:20081209 - added debit card
            int index = 0;
            switch (Helper.GetCashTypeChar(this._cashType).ToString())
            {
                case "A":
                    index = comboPaymentType.Items.IndexOf(comboPaymentType.Items.FindByText((string)GetLocalResourceObject("comboPaymentType.Items")));
                    break;
                case "Q":
                    index = comboPaymentType.Items.IndexOf(comboPaymentType.Items.FindByText((string)GetLocalResourceObject("comboPaymentType.Items1")));
                    break;
                case "D":
                    index = comboPaymentType.Items.IndexOf(comboPaymentType.Items.FindByText((string)GetLocalResourceObject("comboPaymentType.Items2")));
                    break;
                case "B":
                    index = comboPaymentType.Items.IndexOf(comboPaymentType.Items.FindByText((string)GetLocalResourceObject("comboPaymentType.Items3")));
                    break;
                case "P":
                    index = comboPaymentType.Items.IndexOf(comboPaymentType.Items.FindByText((string)GetLocalResourceObject("comboPaymentType.Items4")));
                    break;
                case "K":
                    index = comboPaymentType.Items.IndexOf(comboPaymentType.Items.FindByText((string)GetLocalResourceObject("comboPaymentType.Items5")));
                    break;
                case "J":
                    index = comboPaymentType.Items.IndexOf(comboPaymentType.Items.FindByText((string)GetLocalResourceObject("comboPaymentType.Items6")));
                    break;
            }

            comboPaymentType.SelectedIndex = index;
        }

        protected void PopulateAuthorites()
        {
            int autIntNo = 0;
            if (Session["autIntNo"] != null)
                autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(this._connectionString);

            AuthorityDetails autDetail = autList.GetAuthorityDetails(autIntNo);
            if (autDetail != null) mtrIntNo = autDetail.MtrIntNo;

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");
            List<ListItem> items = new List<ListItem>();
            foreach (DataRow dr in data.Tables[0].Rows)
            {
                items.Add(new ListItem(dr["AutName"].ToString() + " (" + dr["AutCode"].ToString().Trim() + ")", dr["AutIntNo"].ToString()));
            }
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            this.ddlAuthority.DataSource = items;
            this.ddlAuthority.DataValueField = "Value";// "AutIntNo";
            this.ddlAuthority.DataTextField = "Text";
            this.ddlAuthority.DataBind();

            this.ddlAuthority.SelectedValue = autIntNo.ToString();

            //reader.Close();
        }


        #endregion


        private void StartSuspenseNoticeProcessing()
        {
            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            //if (this.requireAddresses)
            this.pnlAddress.Visible = true;
            this.txtTotal.Visible = true;

            this.dgTicketDetails.Visible = false;
            this.dgDeferredPayments.Visible = false;

            if (Session["MinimalCapture_TicketNo"] != null)
            {
                this.txtNoticeNumber.Text = Session["MinimalCapture_TicketNo"].ToString();
                this.txtNoticeNumber.ReadOnly = true;
            }
        }

        /// <summary>
        /// Shows the notice search results.
        /// </summary>
        /// <param name="id">The ID Number.</param>
        /// <param name="ticket">The ticket number.</param>
        private void ShowNoticeSearchResults(string id, string ticket)
        {
            StringBuilder sb = new StringBuilder();

            //Barry Dickson 20080408 -  1. need to add in checks for new rules which are passed into the stored proc
            //                          2. need to add new fields to grid (status, contempt of court amount, show court date for notices that are passed summons stage)
            //                          3. to complete point 1 and 2 must change GetNoticesById

            //dls 070712 - added autIntNo to proc so that Auth-rule can be checked and only notices for single authority can be processed
            //dls 081223 - add rule for allowing cash after case no has been generated

            NoticeForPaymentDB notice = new NoticeForPaymentDB(_connectionString);
            DataSet ds = new DataSet();
            string filmType = string.Empty;

            //check if this is a S341
            if (!ticket.Equals(string.Empty))
            {
                filmType = notice.GetFilmType(ticket.Replace("-", string.Empty).Replace("/", string.Empty));

            }

            //ds = notice.GetNoticesById(id, ticket, this._notices, _isDisplayByID, _noDaysForLastPaymentDate, this._minStatus, this._cashAfterCourtDate, this._cashAfterSummons, this._cashAfterWarrant, this._cashAfterGracePeriod, this._cashAfterCaseNumber, this._maxStatus, filmType);

            ds = notice.GetNoticesById(id, ticket, this._notices, _isDisplayByID, _noDaysForLastPaymentDate, this._minStatus, this._cashAfterCourtDate, filmType.Equals("M") ? true : this._cashAfterSummons, this._cashAfterWarrant, this._cashAfterGracePeriod, this._cashAfterCaseNumber, this._maxStatus, filmType);

            //dls 100202 - Authrule removed from SP
            //dls 070713 - we now check the AuthRule inside the proc, so there may be two tables. We want to use the 2nd one in this case
            int tableID = 0;

            if (ds.Tables.Count > 1)
                tableID = 1;

            if (ds.Tables[tableID].Rows.Count > 0)
            {
                //The dataset could have deferred or normal payment information. If deferred information then need to handle it seperately to the current structure 
                //for display purposes and capturing of information.
                //If the user had already selected a normal payment and then tried to select a deferred payment the column DPComments will have text in, explaining
                //that you have to pay deferred payments seperately, and there will be no deferred payment information.
                int chargeStatus = 0;
                foreach (DataRow row in ds.Tables[tableID].Rows)
                {
                    chargeStatus = (int)row["DPChargeStatus"];
                    if (chargeStatus == 734)
                        break;
                }

                if (chargeStatus == 734)
                {
                    btnAddNotices.Visible = false;
                    //Deferred Payments
                    // Add the results to the notice list
                    int notIntNo = 0;

                    bool isChecked = false;
                    string noticeNo = string.Empty;
                    bool isThisNoticePresent = ticket.Length == 0 ? true : false;
                    int scDefPayIntNo = 0;

                    //foreach (DataRow row in ds.Tables[0].Rows)
                    foreach (DataRow row in ds.Tables[tableID].Rows)
                    {
                        notIntNo = (int)row["NoticeIntNo"];
                        isChecked = (bool)row["IsChecked"];
                        noticeNo = (string)row["TicketNo"];
                        scDefPayIntNo = (int)row["SCDefPayIntNo"];

                        NoticeForPayment nfp = new NoticeForPayment(notIntNo, isChecked);

                        if (noticeNo.Equals(ticket, StringComparison.InvariantCultureIgnoreCase))
                        {
                            isThisNoticePresent = true;
                            nfp.IsSelected = true;
                            nfp.HasJudgement = (bool)row["HasJudgement"].ToString().Equals("Y") ? true : false;
                            nfp.DPIntNo = scDefPayIntNo;
                        }

                        //if (!this.notices.Contains(nfp))
                        this._notices.Add(nfp);
                    }

                    // There are tickets to pay
                    if (isThisNoticePresent)
                    {
                        if (_isSelectAllNotices)
                        {
                            foreach (NoticeForPayment n in this._notices)
                                n.IsSelected = true;
                        }
                        //display deferred payment details

                        this.panelDetails.Visible = true;
                        this.lblError.Visible = false;
                        //New grid to show deferred payment breakdown
                        this.dgDeferredPayments.DataSource = ds.Tables[tableID];
                        this.dgDeferredPayments.DataKeyField = "NoticeIntNo";
                        this.dgDeferredPayments.DataBind();
                        //dont show ticket details grid
                        this.dgTicketDetails.Visible = false;
                        //this.dgTicketDetails.DataSource = ds.Tables[tableID];
                        //this.dgTicketDetails.DataKeyField = "NoticeIntNo";
                        //this.dgTicketDetails.DataBind();

                        //If defered must not allow to link back to add another notice
                        this.linkBack.Visible = false;

                        ShowHideColumns(false);

                        string name = ds.Tables[tableID].Rows[0]["Name"].ToString();
                        this.txtReceivedFrom.Text = name;
                        this.txtChequeDrawer.Text = name;
                        this.txtNameOnCard.Text = name;
                        //BD name on card for debot card as well
                        this.txtNameOnDCard.Text = name;

                        // dls 08012 - they want to be able to toggle between owner, driver & proxy addresses
                        int selectedNotIntNo = 0;

                        //find the first notice that is selected
                        foreach (NoticeForPayment n in this._notices)
                        {
                            if (n.IsSelected)
                            {
                                selectedNotIntNo = n.NotIntNo;
                                break;
                            }
                        }

                        if (selectedNotIntNo > 0)
                            GetAddressDetails(selectedNotIntNo, true);

                        //dls 080122 - show the address all the time
                        this.pnlAddress.Visible = true;

                        this.CalculateTotal(true, "deferred");

                        //need to reset the session object here so that it remembers which ones were selected
                        this.Session["NoticesToPay"] = this._notices;

                        return;
                    }
                }
                else
                {
                    btnAddNotices.Visible = true;
                    // Add the results to the notice list
                    int notIntNo = 0;

                    bool isChecked = false;
                    string noticeNo = string.Empty;
                    bool isThisNoticePresent = ticket.Length == 0 ? true : false;
                    string deferredErrorMsg = string.Empty;

                    //foreach (DataRow row in ds.Tables[0].Rows)
                    foreach (DataRow row in ds.Tables[tableID].Rows)
                    {
                        notIntNo = (int)row["NoticeIntNo"];

                        isChecked = (bool)row["IsChecked"];
                        noticeNo = (string)row["TicketNo"];
                        deferredErrorMsg = (string)row["DPComments"];

                        NoticeForPayment nfp = new NoticeForPayment(notIntNo, isChecked);

                        if (noticeNo.Equals(ticket, StringComparison.InvariantCultureIgnoreCase))
                        {
                            isThisNoticePresent = true;
                            nfp.IsSelected = true;
                            nfp.HasJudgement = (bool)row["HasJudgement"].ToString().Equals("Y") ? true : false;
                        }

                        if (!this._notices.Contains(nfp))
                            this._notices.Add(nfp);
                    }

                    // There are tickets to pay
                    if (isThisNoticePresent || !deferredErrorMsg.Equals(string.Empty))
                    {
                        if (_isSelectAllNotices)
                        {
                            foreach (NoticeForPayment n in this._notices)
                                n.IsSelected = true;
                        }

                        this.panelDetails.Visible = true;
                        this.lblError.Visible = false;
                        this.dgTicketDetails.DataSource = ds.Tables[tableID];
                        this.dgTicketDetails.DataKeyField = "NoticeIntNo";
                        this.dgTicketDetails.DataBind();

                        // jake 2012-06-04 add
                        this.dgTicketDetails.Visible = true;

                        // jerry 2010-10-29 if NotFilmType = "M" to disable checkbox
                        // dls 2011-06-30 - disabling 'H' as well - the grids do not handle multiple charges under a single notice

                        //TO DO: handle each charge as an individual entity
                        //

                        foreach (DataGridItem item in this.dgTicketDetails.Items)
                        {
                            Label lblTicketNo = (Label)item.FindControl("lblTicketNo");

                            if (!lblTicketNo.Text.Equals(string.Empty))
                            {
                                filmType = notice.GetFilmType(lblTicketNo.Text.Replace("-", string.Empty).Replace("/", string.Empty));
                            }

                            if (filmType.Equals("M") || filmType.Equals("H"))
                            {
                                ImageButton check = (ImageButton)item.FindControl("igPayNow");
                                check.Enabled = false;

                                TextBox txtAmountToPay = (TextBox)item.FindControl("txtAmountToPay");
                                txtAmountToPay.Enabled = false;

                                DropDownList ddlSeniorOfficer = (DropDownList)item.FindControl("ddlSeniorOfficer");
                                ddlSeniorOfficer.Enabled = false;
                            }
                        }

                        //dont show deferred payments grid
                        this.dgDeferredPayments.Visible = false;
                        ShowHideColumns(false);

                        //BD Set the comment from sql, incase they have selected to pay a deferred notice after they selected a normal notice.
                        this.lblDeferredError.Text = deferredErrorMsg; //"You are not allowed to receipt a deferred transaction and a normal transaction together. First receipt your currently selected transaction and then receipt the deferred transaction.";

                        string name = ds.Tables[tableID].Rows[0]["Name"].ToString();
                        this.txtReceivedFrom.Text = name;
                        this.txtChequeDrawer.Text = name;
                        this.txtNameOnCard.Text = name;
                        //BD name on card for debot card as well
                        this.txtNameOnDCard.Text = name;

                        // dls 08012 - they want to be able to toggle between owner, driver & proxy addresses
                        int selectedNotIntNo = 0;

                        //find the first notice that is selected
                        foreach (NoticeForPayment n in this._notices)
                        {
                            if (n.IsSelected)
                            {
                                selectedNotIntNo = n.NotIntNo;
                                break;
                            }
                        }

                        if (selectedNotIntNo > 0)
                            GetAddressDetails(selectedNotIntNo, true);

                        //dls 080122 - show the address all the time
                        this.pnlAddress.Visible = true;

                        this.CalculateTotal(true, "");

                        //need to reset the session object here so that it remembers which ones were selected
                        this.Session["NoticesToPay"] = this._notices;

                        return;
                    }
                }
            }

            // There's nothing outstanding
            this.lblError.Visible = true;
            this.panelDetails.Visible = true;
            sb.Length = 0;
            string sReason = string.Empty;

            try
            {
                //LMZ 09-03-2007 - added this to give back Charge status description 
                ChargeStatusDB cdb = new ChargeStatusDB(this._connectionString);

                //dls 080417 ============= - add meaningful error if ticket is not returned
                //sReason = cdb.GetChargeStatusById(ticket, id);
                //BD 20080624 changed to include the error for grace period expired disallowedDPComments
                sReason = cdb.GetChargeStatusById(ticket, id, this._cashAfterCourtDate, this._cashAfterSummons, this._cashAfterWarrant, this._cashAfterGracePeriod, this._cashAfterCaseNumber);

                if (sReason != null && sReason != "")
                {
                    Session["CacheReceiptTraffic_Details_Error"] = "Y";
                }
                else
                {
                    Session["CacheReceiptTraffic_Details_Error"] = "N";
                }
            }
            catch (Exception cdbEx)
            {
                // mrs 20080719 added message
                sb.Append(string.Format((string)GetLocalResourceObject("lblError.Text1") + cdbEx.Message));
            }

            if (sReason == string.Empty)
                sReason = (string)GetLocalResourceObject("UnknownTicket.Text");

            if (id.Length > 0)
                sb.Append(string.Format((string)GetLocalResourceObject("lblError.Text2"), id, sReason));
            if (ticket.Length > 0)
                sb.Append(string.Format((string)GetLocalResourceObject("lblError.Text3"), ticket, sReason));

            if (this._isSuspenseNotice)
            {
                this.lblError.Text = sb.ToString();
            }
            else
            {
                this.Session.Add("CashReceiptError", sb.ToString());
                Response.Redirect("CashReceiptTraffic.aspx");
            }
        }

        private void GetAddressDetails(int notIntNo, bool forceChange)
        {
            NoticeForPaymentDB notice = new NoticeForPaymentDB(_connectionString);
            DataSet dsAddress;

            //check if there already is an AddressDetails dataset in the Session, if not go back to the database and fetch all addresses for Notice
            if (Session["NoticeAddressDetails"] == null || forceChange)
                dsAddress = notice.GetNoticeAddressDetails(notIntNo);
            else
                dsAddress = (DataSet)Session["NoticeAddressDetails"];

            // jerry 2010-11-18 if hidPayClick.Value = 1, pay click
            if (hidPayClick.Value != "1")
            {
                foreach (DataRow dr in dsAddress.Tables[0].Rows)
                {
                    if (dr["Persona"].ToString().Equals(this._personaType.ToString()))
                    {
                        this.txtAddress1.Text = dr["Address1"].ToString();
                        this.txtAddress2.Text = dr["Address2"].ToString();
                        this.txtAddress3.Text = dr["Address3"].ToString();
                        this.txtAddress4.Text = dr["Address4"].ToString();
                        this.txtAddress5.Text = dr["Address5"].ToString();
                        this.txtPoCode.Text = dr["PoCode"].ToString();
                        this.txtReceivedFrom.Text = dr["FullName"].ToString();
                        break;
                    }
                }
            }

            //store dataset back into Session
            Session.Add("NoticeAddressDetails", dsAddress);

            if (notIntNo > 0)
            {
                //need to re-populate Received from list, depending on whether there is a proxy/accused or not

            }
        }

        private decimal CalculateTotal(bool setValues, string isDeferred)
        {
            if (isDeferred == "deferred")
            {
                return CalculateDeferredTotal(setValues, false);
            }
            else
            {
                return CalculateTotal(setValues, false);
            }
        }

        //Barry Dickson 20080410 Need to check that the amount they are paying is greater than the total contempt amt.
        private decimal CheckContemptDiff()
        {
            decimal totalContempt = 0;
            decimal totalReceived = 0;
            decimal contemptDiff = 0;
            int i = 0;

            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                ImageButton check = (ImageButton)item.FindControl("igPayNow");
                if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                {
                    string valueContempt = string.Empty;
                    valueContempt = item.Cells[9].Text;
                    totalContempt += this.ParseValue(valueContempt);

                }
                i++;
            }

            if (_dsReceiptPayments != null)
            {
                foreach (DataRow dr in _dsReceiptPayments.Tables[0].Rows)
                    totalReceived += Decimal.Parse(dr["TRDAmount"].ToString());
            }

            contemptDiff = totalReceived - totalContempt;

            return contemptDiff;

        }

        private decimal CalculateDeferredTotal(bool setValues, bool calcRep)
        {
            decimal totalToPay = 0;
            decimal totalReceived = 0;
            bool foundAddress = false;

            int i = 0;
            foreach (DataGridItem item in this.dgDeferredPayments.Items)
            {
                ImageButton check = (ImageButton)item.FindControl("igDeferredPayNow");
                if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                {
                    string value = string.Empty;
                    //string valueContempt = string.Empty;

                    //if (calcRep)
                    //{
                    //value = ((TextBox)item.FindControl("txtAmountToPay")).Text;
                    //Barry Dickosn - need to include the contempt court amt
                    //dls 080418 - why were these commented out - I put them back
                    //valueContempt = item.Cells[9].Text;
                    //totalToPay += this.ParseValue(valueContempt);
                    //}
                    //else
                    //{
                    //Barry Dickosn - need to include the contempt court amt
                    value = item.Cells[9].Text;
                    //valueContempt = item.Cells[9].Text;
                    //totalToPay += this.ParseValue(valueContempt);
                    //}

                    totalToPay += this.ParseValue(value);

                    item.CssClass = "RowHighlighted";
                }
                else
                    item.CssClass = ((i % 2) == 0) ? "CartListItemAlt" : "CartListItem";

                int notIntNo = (int)this.dgDeferredPayments.DataKeys[item.ItemIndex];
                int scDPIntNo = Convert.ToInt32(item.Cells[11].Text);
                if (!foundAddress)
                {
                    GetAddressDetails(notIntNo, true);
                    foundAddress = true;
                }
                foreach (NoticeForPayment notice in this._notices)
                {
                    if (notice.DPIntNo == scDPIntNo)
                    {
                        notice.IsSelected = check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase);

                        if (notice.IsSelected && !foundAddress)
                        {
                            GetAddressDetails(notice.NotIntNo, true);
                            foundAddress = true;
                        }
                        break;
                    }
                }

                i++;
            }
            if (this._isSuspenseNotice)
            {
                Decimal.TryParse(this.txtTotal.Text.Trim(), out totalToPay);
                decimal gridAmount = CountSuspenseAmount();
                totalToPay = (gridAmount > 0 ? gridAmount : totalToPay);
            }

            //update by Rachel 20140819 for 5337
            //this.labelTotal.Text = "&nbsp;     " + (string)GetLocalResourceObject("lblTotal.Text") + totalToPay.ToString(CURRENCY_FORMAT);
            this.labelTotal.Text = "&nbsp;     " + (string)GetLocalResourceObject("lblTotal.Text") + totalToPay.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337

            //dls 080122 - new totals panel
            //calculate over / under payment

            decimal received = 0;

            bool test = Decimal.TryParse(txtTotal.Text, out received);

            if (_dsReceiptPayments != null)
            {
                foreach (DataRow dr in _dsReceiptPayments.Tables[0].Rows)
                    totalReceived += Decimal.Parse(dr["TRDAmount"].ToString());
            }

            //SD: 20081205, Check for null values
            if (txtTotal.Text.Trim().Length < 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append((string)GetLocalResourceObject("lblError.Text4") + "\n<ul>\n");
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text5") + "</li>\n</ul>");
                lblError.Text = sb.ToString();
                lblError.Visible = true;
            }


            decimal diff = totalToPay - totalReceived;

            if (setValues)
                SetTotalValues(totalToPay, totalReceived, diff, calcRep);

            return diff;
        }

        private decimal CalculateTotal(bool setValues, bool calcRep)
        {
            decimal totalToPay = 0;
            decimal totalReceived = 0;
            bool foundAddress = false;

            int i = 0;
            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                ImageButton check = (ImageButton)item.FindControl("igPayNow");
                if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                {
                    string value = string.Empty;
                    string valueContempt = string.Empty;

                    if (calcRep)
                    {
                        value = ((TextBox)item.FindControl("txtAmountToPay")).Text;
                        //Barry Dickosn - need to include the contempt court amt
                        //dls 080418 - why were these commented out - I put them back
                        valueContempt = item.Cells[9].Text;
                        totalToPay += this.ParseValue(valueContempt);
                    }
                    else
                    {
                        //Barry Dickosn - need to include the contempt court amt
                        value = item.Cells[7].Text;
                        valueContempt = item.Cells[9].Text;
                        totalToPay += this.ParseValue(valueContempt);
                    }

                    totalToPay += this.ParseValue(value);

                    item.CssClass = "RowHighlighted";
                }
                else
                    item.CssClass = ((i % 2) == 0) ? "CartListItemAlt" : "CartListItem";

                int notIntNo = (int)this.dgTicketDetails.DataKeys[item.ItemIndex];
                foreach (NoticeForPayment notice in this._notices)
                {
                    if (notice.Equals(notIntNo))
                    {
                        notice.IsSelected = check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase);

                        if (notice.IsSelected && !foundAddress)
                        {
                            GetAddressDetails(notice.NotIntNo, true);
                            foundAddress = true;
                        }
                        break;
                    }
                }

                i++;
            }
            if (this._isSuspenseNotice)
            {
                Decimal.TryParse(this.txtTotal.Text.Trim(), out totalToPay);
                decimal gridAmount = CountSuspenseAmount();
                totalToPay = (gridAmount > 0 ? gridAmount : totalToPay);
            }

            //update by Rachel 20140819 for 5337
            //this.labelTotal.Text = "&nbsp;    " + (string)GetLocalResourceObject("lblTotal.Text") + totalToPay.ToString(CURRENCY_FORMAT);
            this.labelTotal.Text = "&nbsp;    " + (string)GetLocalResourceObject("lblTotal.Text") + totalToPay.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337

            //dls 080122 - new totals panel
            //calculate over / under payment

            decimal received = 0;

            bool test = Decimal.TryParse(txtTotal.Text, out received);

            if (_dsReceiptPayments != null)
            {
                foreach (DataRow dr in _dsReceiptPayments.Tables[0].Rows)
                    totalReceived += Decimal.Parse(dr["TRDAmount"].ToString());
            }

            //SD: 20081205, Check for null values
            if (txtTotal.Text.Trim().Length < 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append((string)GetLocalResourceObject("lblError.Text4") + "\n<ul>\n");
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text5") + "</li>\n</ul>");
                lblError.Text = sb.ToString();
                lblError.Visible = true;
            }


            decimal diff = totalToPay - totalReceived;

            if (setValues)
                SetTotalValues(totalToPay, totalReceived, diff, calcRep);

            return diff;
        }

        private decimal CountSuspenseAmount()
        {
            decimal flag = 0;
            string amount_text = string.Empty;
            decimal amount = 0;
            foreach (GridViewRow item in grdPayments.Rows)
            {
                amount_text = ((Label)item.FindControl("Label2")).Text;
                decimal.TryParse(amount_text, out amount);
                flag += amount;
            }
            return flag;
        }

        private void SetTotalValues(decimal totalToPay, decimal totalReceived, decimal diff, bool calcRep)
        {
            //update by Rachel 20140819 for 5337
            //this.lblTotalDue.Text = totalToPay.ToString(CURRENCY_FORMAT);
            //this.lblTotalPaymentReceived.Text = totalReceived.ToString(CURRENCY_FORMAT);
            this.lblTotalDue.Text = totalToPay.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture);
            this.lblTotalPaymentReceived.Text = totalReceived.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337

            //display the absolute difference, not the negative amount
            decimal displayDiff = System.Math.Abs(diff);

            //update by Rachel 20140819 for 5337
            //this.lblOverOrUnder.Text = displayDiff.ToString(CURRENCY_FORMAT);
            this.lblOverOrUnder.Text = displayDiff.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337

            if (diff > 0)
                this.lblDifference.Text = (string)GetLocalResourceObject("lblDifference.Text1");
            else if (diff == 0)
                this.lblDifference.Text = (string)GetLocalResourceObject("lblDifference.Text2");
            else
                this.lblDifference.Text = (string)GetLocalResourceObject("lblDifference.Text3");

            if (diff > 0 && !calcRep)
            {
                //update by Rachel 20140819 for 5337
                //txtTotal.Text = diff.ToString("#,##0.00");
                txtTotal.Text = diff.ToString("#,##0.00", CultureInfo.InvariantCulture);
                //end update by Rachel 20140819 for 5337
            }
            else
                txtTotal.Text = "0.00";

        }

        private decimal ParseValue(string value)
        {
            StringBuilder sb = new StringBuilder(value);
            int currencyPos = value.LastIndexOf('R');
            if (currencyPos != -1)
                sb.Remove(0, currencyPos + 1);
            sb.Replace(",", "");
            sb.Replace(" ", "");
            return decimal.Parse(sb.ToString(), System.Globalization.NumberStyles.Any);
        }

        //protected void btnHideMenu_Click(object sender, EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void comboPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblError.Visible = false;
            lblError.Text = string.Empty;

            this.SetPaymentType();
        }

        private void SetPaymentType()
        {
            //dls 080122 - added for new cashier function
            this.wdcDateReceived.Enabled = true;

            if (this.comboPaymentType.SelectedValue == "Cash")
            {
                //this.wdcDateReceived.Enabled = false;
                this.pnlCheque.Visible = false;
                this.pnlCreditCard.Visible = false;
                this.pnlPostalOrder.Visible = false;
                this.pnlBankPayment.Visible = false;
                this.pnlDCardDetails.Visible = false;
                this._cashType = CashType.Cash;
            }
            if (this.comboPaymentType.SelectedValue == "Cheque")
            {
                this.pnlCheque.Visible = true;
                this.pnlCreditCard.Visible = false;
                this.pnlPostalOrder.Visible = false;
                this.pnlBankPayment.Visible = false;
                this.pnlDCardDetails.Visible = false;
                this._cashType = CashType.Cheque;
            }
            if (this.comboPaymentType.SelectedValue == "Credit Card")
            {
                //this.wdcDateReceived.Enabled = false;
                this.pnlCheque.Visible = false;
                this.pnlCreditCard.Visible = true;
                this.pnlPostalOrder.Visible = false;
                this.pnlBankPayment.Visible = false;
                this.pnlDCardDetails.Visible = false;
                this._cashType = CashType.CreditCard;
            }
            if (this.comboPaymentType.SelectedValue == "Debit Card")
            {
                //this.wdcDateReceived.Enabled = false;
                this.pnlCheque.Visible = false;
                this.pnlCreditCard.Visible = false;
                this.pnlDCardDetails.Visible = true;
                this.pnlPostalOrder.Visible = false;
                this.pnlBankPayment.Visible = false;
                this._cashType = CashType.DebitCard;
            }
            if (this.comboPaymentType.SelectedValue == "Postal Order")
            {
                this.pnlCheque.Visible = false;
                this.pnlCreditCard.Visible = false;
                this.pnlPostalOrder.Visible = true;
                this.pnlBankPayment.Visible = false;
                this.pnlDCardDetails.Visible = false;
                this._cashType = CashType.PostalOrder;
            }
            if (this.comboPaymentType.SelectedValue == "Bank Payment")
            {
                this.pnlCheque.Visible = false;
                this.pnlDCardDetails.Visible = false;
                this.pnlCreditCard.Visible = false;
                this.pnlPostalOrder.Visible = false;
                this.pnlBankPayment.Visible = true;
                this._cashType = CashType.BankPayment;
            }
            if (this.comboPaymentType.SelectedValue == "Other")
            {
                //this.wdcDateReceived.Enabled = false;
                this.pnlCheque.Visible = false;
                this.pnlCreditCard.Visible = false;
                this.pnlPostalOrder.Visible = false;
                this.pnlBankPayment.Visible = false;
                this.pnlDCardDetails.Visible = false;
                this._cashType = CashType.Other;
            }

            Session["IsBankPayment"] = _isBankPayment;
            this.ViewState.Add("CashType", this._cashType);
        }

        private bool ValidateCommonData(StringBuilder sb)
        {
            bool response = true;

            //DateTime dt;

            //if (!DateTime.TryParse(wdcDateReceived.Text, out dt))
            //{
            //    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text6") + "</li>\n");
            //    response = false;
            //}

            decimal amount;
            if (!decimal.TryParse(this.txtTotal.Text.Trim(), NumberStyles.Any, NumberFormatInfo.CurrentInfo, out amount))
            {
                response = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text7") + "</li>\n");
            }
            else
            {
                decimal originalAmount = this.ParseValue(this.labelTotal.Text.Trim());

                if (amount > originalAmount && !this._isSuspenseNotice)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text8") + "</li>\n");
                }
                else
                {
                    //dls 070726 save original fine amount in view state.
                    this.ViewState["currentFineAmount"] = originalAmount;
                    _currentFineAmount = originalAmount;
                }

                //SD: 20081205, Check for null values
                if (txtTotal.Text.Trim().Length == 0)
                {
                    sb.Append((string)GetLocalResourceObject("lblError.Text9") + "\n<ul>\n");
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text10") + "</li>\n</ul>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }

                //dls 070406 - don't allow zero amounts
                if (amount < 1)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text11") + "</li>\n");
                }
            }


            // Jake 2012-05-18 add to validate notice number for suspense notice 
            if (this._isSuspenseNotice)
            {
                if (String.IsNullOrEmpty(this.txtNoticeNumber.Text))
                {
                    response = false;
                    sb.Append("<li>Notice number is required</li>\n");
                }
                //if (grdPayments.Rows.Count > 0)
                //{
                //    response = false;
                //    sb.Append("<li>Suspense Notice only can add one row for payment detail.</li>\n");
                //}
                //Jake 2012-06-04 suspense notice need no validation for notice number
                //else
                //{
                //    string validateFunction = string.Empty;
                //    if (this.txtNoticeNumber.Text.IndexOf("/") > 0) validateFunction = "cdv";
                //    else
                //    {
                //        validateFunction = "verhoeff";
                //    }
                //    if (Stalberg.TMS.Data.Util.Validation.ValidateTicketNumber(this.txtNoticeNumber.Text.Trim(), validateFunction))
                //    {
                //        response = false;
                //        sb.Append("<li>Notice number is invalid</li>\n");
                //    }
                //}
            }

            if (_noOfNotices > 1 && grdPayments.Rows.Count >= 1)
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text12") + "</li>\n");
                response = false;
            }
            //
            if (grdPayments.Rows.Count > 0)
            {
                for (int i = 0; i < grdPayments.Rows.Count; i++)
                {
                    if (grdPayments.DataKeys[i]["TRDCashType"].ToString() == ((char)(int)CashType.Other).ToString())
                    {
                        response = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.OtherCashType") + "</li>\n");
                        break;
                    }
                }
            }
            if (this._cashType.ToString() == "Other" && response == true)
            {
                if (grdPayments.Rows.Count > 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.OtherCashType") + "</li>\n");
                    response = false;
                }
            }

            return response;
        }

        private bool VaidateComtemptData(StringBuilder sb)
        {
            bool response = true;

            if (CheckContemptDiff() < 0)
            {
                response = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text13") + "</li>\n");
            }

            return response;

        }

        private bool ValidateHeaderData(StringBuilder sb)
        {
            bool response = true;

            if (this._requireAddresses)
            {
                if (this.txtAddress1.Text.Trim().Length == 0)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text14") + "</li>\n");
                }
                if (this.txtAddress2.Text.Trim().Length == 0)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text15") + "</li>\n");
                }
                if (this.txtPoCode.Text.Trim().Length == 0)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text16") + "</li>\n");
                }
            }

            string phone = this.txtPhone.Text.Trim();

            if (_mandatoryPhoneNo)
            {
                if (phone.Length < 7)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text17") + "</li>\n");
                }
            }

            string receivedFrom = string.Empty;

            receivedFrom = this.txtReceivedFrom.Text.Trim();
            if (receivedFrom.Length == 0)
            {
                response = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text18") + "</li>\n");
            }

            DateTime dt;

            if (!DateTime.TryParse(this.wdcDateReceived.Text, out dt))
            {
                response = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text19") + "</li>\n");
            }

            //Jerry 2014-12-04 do not allow future receipts date
            if (dt > DateTime.Now.Date)
            {
                response = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text47") + "</li>\n");
            }

            return response;
        }

        private bool CheckForRepresentations(StringBuilder sb, bool isValid)
        {
            bool response = true;
            decimal diff;
            if (this.dgDeferredPayments.Visible == true)
                diff = CalculateTotal(true, "deferred");
            else
                diff = CalculateTotal(true, "");

            //decimal diff = CalculateTotal(false);

            if (diff != 0)
            {
                response = false;
                isValid = false;

                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text20") + "</li>\n");

                // Check if this receipt allows for on-the-fly representations
                if (!this._allowOnTheFlyRepresentations)//or the notice is a s56 one then show the following message
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text8") + "</li>\n");
                }
                else
                {
                    this.lblError.Visible = false;
                    this.panelConfirm.Visible = false;

                    if (diff > 0)
                    {
                        int noOfNoticesAllowedReps = 0;
                        //BD check to lock rep for defered payments
                        int chkDeferredPayment = 0;

                        //dls 080519 - need to check that the selected notices are allowed to be rep'd: no on-the-fly reps after judgement
                        foreach (NoticeForPayment notice in this._notices)
                        {
                            if (!notice.HasJudgement)
                                noOfNoticesAllowedReps++;

                            if (notice.DPChargeStatus == 734)
                                chkDeferredPayment = 1;
                        }

                        if (noOfNoticesAllowedReps == 0)
                        {
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text21") + "</li>\n");
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text22") + "</li>\n");
                            this.chkCashierRep.Visible = false;
                        }
                        else if (chkDeferredPayment == 1)
                        {
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text23") + "</li>\n");
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text24") + "</li>\n");
                            this.chkCashierRep.Visible = false;
                        }
                        else
                        {
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text25") + "</li>\n");
                            this.chkCashierRep.Visible = true;
                        }
                    }
                    else
                    {
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text26") + "</li>\n");
                        this.chkCashierRep.Visible = false;
                    }


                }
            }

            return response;
        }

        private void ShowError(StringBuilder sb, bool isValid)
        {
            sb.Append("</ul>");
            if (!isValid)
            {
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return;
            }
        }

        private void RefreshPaymentList(StringBuilder sb, bool isValid)
        {
            if (!isValid)
            {
                ShowError(sb, isValid);
                return;
            }

            AddToPaymentList();

            if (this.dgDeferredPayments.Visible == true)
                this.CalculateTotal(true, "deferred");

            else if (this.dgTicketDetails.Visible == true || this._isSuspenseNotice)
                CalculateTotal(true, "");

        }

        private void ShowConfirmation()
        {
            this.lblError.Text = string.Empty;
            lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

            int chkTypePayment = 0;
            if (this.dgDeferredPayments.Visible == true)
            {
                chkTypePayment = 1;
            }


            this.panelDetails.Visible = false;
            this.lblError.Visible = false;
            this.panelConfirm.Visible = true;

            StringBuilder sb = new StringBuilder();
            sb.Append("<br/>&nbsp;<br/><p align=\"center\" style=\"font-weight: bold;\">This is NOT a Receipt.</p>");

            // Payment details
            //sb.Append("<table border=\"0\" cellpadding=\"0\" class=\"CartListItem\" style=\"border-left: none; text-align: left;\"><tr>");
            sb.Append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">");
            sb.Append("<tr><th class=\"CartListHead\">" + (string)GetLocalResourceObject("grdPayments.HeaderText") + "</th>");
            sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("grdPayments.HeaderText2") + "</th>");
            sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("grdPayments.HeaderText1") + "</th></tr>");

            foreach (DataRow dr in _dsReceiptPayments.Tables[0].Rows)
            {
                //update by Rachel 20140811 for 5337
                //string amount = string.Format("{0:0.00}", Decimal.Parse(dr["TRDAmount"].ToString()));
                string amount = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", dr["TRDAmount"]);
                //end update by Rachel 20140811 for 5337
                CashType ct = Helper.GetCashTypeValue(Convert.ToChar(dr["TRDCashType"].ToString()));

                switch (ct)
                {
                    case CashType.Cash:
                    case CashType.RoadBlockCash:
                        sb.Append("<tr><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("comboPaymentType.Items"));
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append(amount);
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">&nbsp;&nbsp;"); ;
                        sb.Append("</td></tr>\n");
                        break;

                    case CashType.Cheque:
                    case CashType.RoadBlockCheque:
                        sb.Append("<tr><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("comboPaymentType.Items1"));
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append(amount);
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("createTable.Text") + "&nbsp;");
                        sb.Append(dr["TRDChequeNo"].ToString());
                        sb.Append("<br/>" + (string)GetLocalResourceObject("createTable.Text1") + "&nbsp;");
                        sb.Append(dr["TRDChequeDrawer"].ToString());
                        sb.Append("<br/>" + (string)GetLocalResourceObject("createTable.Text2") + "&nbsp;");
                        sb.Append(dr["TRDChequeBank"].ToString());
                        sb.Append("<br/>" + (string)GetLocalResourceObject("lblChequeDate.Text") + "&nbsp;");
                        sb.Append(string.Format("{0:yyyy-MM-dd}", dr["TRDChequeDate"]));
                        sb.Append("</td></tr>\n");
                        break;

                    case CashType.CreditCard:
                    case CashType.RoadBlockCard:
                        sb.Append("<tr><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("comboPaymentType.Items2"));
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append(amount);
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("createTable.Text3") + "&nbsp;");
                        sb.Append(dr["TRDCardType"].ToString());
                        sb.Append("<br/>" + (string)GetLocalResourceObject("createTable.Text4") + "&nbsp;");
                        sb.Append(dr["TRDPayee"].ToString());
                        sb.Append("<br/>" + (string)GetLocalResourceObject("createTable.Text5") + "&nbsp;");
                        sb.Append(((DateTime)dr["TRDExpiryDate"]).ToString("yyyy-MM"));
                        sb.Append("</td></tr>\n");
                        break;
                    // SD:20081210 : Added for debit card

                    case CashType.DebitCard:
                        sb.Append("<tr><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("comboPaymentType.Items3"));
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append(amount);
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("createTable.Text3") + "&nbsp;");
                        sb.Append(dr["TRDCardType"].ToString());
                        sb.Append("<br/>" + (string)GetLocalResourceObject("createTable.Text4") + "&nbsp;");
                        sb.Append(dr["TRDPayee"].ToString());
                        sb.Append("<br/>" + (string)GetLocalResourceObject("createTable.Text5") + ":&nbsp;");
                        sb.Append(((DateTime)dr["TRDExpiryDate"]).ToString("yyyy-MM"));
                        sb.Append("</td></tr>\n");
                        break;

                    case CashType.PostalOrder:
                        sb.Append("<tr><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("comboPaymentType.Items4"));
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append(amount);
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("lblPostalNum.Text") + "&nbsp;");
                        sb.Append(dr["TRDPostalOrderNo"].ToString());
                        sb.Append("</td></tr>\n");
                        break;

                    case CashType.BankPayment:
                        sb.Append("<tr><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("comboPaymentType.Items5"));
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append(amount);
                        sb.Append("</td><td style=\"border-bottom: dimgray 1px solid; border-right: dimgray 1px solid;\" class=\"CartListItem\">");
                        sb.Append((string)GetLocalResourceObject("createTable.Text6") + "&nbsp;");
                        sb.Append(dr["TRDReference"].ToString());
                        sb.Append("</td></tr>\n");
                        break;
                }
            }

            sb.Append("</table><br/>");

            sb.Append("<table border=\"0\" cellpadding=\"0\" class=\"CartListItem\" style=\"border-left: none; text-align: left;\"><tr>");
            sb.Append("<tr><td>" + (string)GetLocalResourceObject("lblReceivedFrom.Text") + "</td><td>");
            sb.Append(this.txtReceivedFrom.Text);
            this.WriteAddressDetails(sb);
            sb.Append("</td></tr>\n");
            sb.Append("<tr><td>" + (string)GetLocalResourceObject("createTable.Text7") + "</td><td>");
            sb.Append(this.txtPhone.Text);
            sb.Append("<tr><td>" + (string)GetLocalResourceObject("createTable.Text8") + "</td><td>");
            sb.Append(string.Format("{0:yyyy-MM-dd}", this.wdcDateReceived.Text));
            sb.Append("</td></tr>\n");

            if (this.txtComment.Text.Length > 0)
            {
                sb.Append("<tr><td>" + (string)GetLocalResourceObject("createTable.Text9") + "</td><td>");
                sb.Append(this.txtComment.Text);
                sb.Append("</td></tr>\n");
            }

            sb.Append("</table><br/>");

            decimal total = 0;
            if (this._isSuspenseNotice)
            {
                // Jake 2012-05-18 modified, use currentFineAmount
                //total = decimal.Parse(this.txtTotal.Text);
                total = this._currentFineAmount;
                //TODO: Display information that this is a payment into the suspense account
                sb.Append("<table border=\"0\" cellpadding=\"0\" class=\"CartListItem\" style=\"border-left: none; text-align: left;\"><tr>");
                sb.Append("<td colspan=\"2\">");
                sb.Append(SuspenseAccount.SUSPENSE_MESSAGE);
                sb.Append("</td></tr>\n");
                sb.Append("</table><br/>");
            }
            else
            {
                // Offences table
                //sb.Append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">");
                //sb.Append("<tr><th class=\"CartListHead\">Ticket Number</th>");
                //sb.Append("<th class=\"CartListHead\">Offence Date</th>");
                //sb.Append("<th class=\"CartListHead\">ID Number</th>");
                //sb.Append("<th class=\"CartListHead\">Vehicle Registration</th>");
                //sb.Append("<th class=\"CartListHead\">Original Amount</th>");
                //sb.Append("<th class=\"CartListHead\">Contempt Amount</th>");
                //sb.Append("<th class=\"CartListHead\">Amount Paid</th></tr>\n");

                //BD need to check which grid to use. 
                if (chkTypePayment == 0)
                {
                    // Offences table
                    sb.Append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">");
                    sb.Append("<tr><th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text10") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text11") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text12") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text13") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text14") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text15") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text16") + "</th></tr>\n");

                    foreach (DataGridItem item in this.dgTicketDetails.Items)
                    {
                        ImageButton check = (ImageButton)item.FindControl("igPayNow");
                        if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                        {
                            //Barry Dickson - 20080410 included contempt court amount
                            decimal origAmount = this.ParseValue(item.Cells[7].Text.Trim());
                            decimal contemptAmount = this.ParseValue(item.Cells[9].Text.Trim());

                            decimal value = 0;
                            if (this._allowOnTheFlyRepresentations && this._hasRepresentation)
                                value = decimal.Parse(((TextBox)item.FindControl("txtAmountToPay")).Text) + this.ParseValue(item.Cells[9].Text.Trim());
                            else
                                //dls 080417 ============= - value = this.ParseValue(item.Cells[7].Text.Trim())
                                value = this.ParseValue(item.Cells[7].Text.Trim()) + this.ParseValue(item.Cells[9].Text.Trim());

                            //dls 080417 ============= - total += value + contemptAmount;
                            total += value;

                            sb.Append("<tr><td class=\"CartListItem\">");
                            //sb.Append(item.Cells[2].Text);
                            sb.Append(((Label)item.FindControl("lblTicketNo")).Text);
                            sb.Append("</td><td class=\"CartListItem\">");
                            sb.Append(item.Cells[4].Text);
                            sb.Append("</td><td class=\"CartListItem\">");
                            sb.Append(item.Cells[3].Text);
                            sb.Append("</td><td class=\"CartListItem\">");
                            sb.Append(item.Cells[5].Text);
                            sb.Append("</td><td class=\"CartListItem\">");

                            //update by Rachel 20140819 for 5337
                            //sb.Append(origAmount.ToString(CURRENCY_FORMAT));
                            sb.Append(origAmount.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture));
                            //end update by Rachel 20140819 for 5337

                            sb.Append("</td><td class=\"CartListItem\">");

                            //update by Rachel 20140819 for 5337
                            //sb.Append(contemptAmount.ToString(CURRENCY_FORMAT));
                            sb.Append(contemptAmount.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture));
                            //end update by Rachel 20140819 for 5337
                            sb.Append("</td><td style=\"border-right: dimgray 1px solid; border-left: dimgray 1px solid;\"");
                            if ((origAmount + contemptAmount) == value)
                                sb.Append(" class=\"CartListItem\">");
                            else
                                sb.Append(" class=\"NormalRed\">");

                            //update by Rachel 20140819 for 5337
                            //sb.Append(value.ToString(CURRENCY_FORMAT));
                            sb.Append(value.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture));
                            //end update by Rachel 20140819 for 5337

                            sb.Append("</td></tr>\n");
                        }
                    }
                }
                else
                {
                    // Offences table
                    sb.Append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">");
                    sb.Append("<tr><th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text10") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text11") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text12") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text13") + "</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text17") + "</th>");
                    //sb.Append("<th class=\"CartListHead\">Contempt Amount</th>");
                    sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("createTable.Text16") + "</th></tr>\n");

                    foreach (DataGridItem item in this.dgDeferredPayments.Items)
                    {
                        ImageButton check = (ImageButton)item.FindControl("igDeferredPayNow");
                        if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                        {
                            //Barry Dickson - 20080410 included contempt court amount
                            //decimal origAmount = this.ParseValue(item.Cells[7].Text.Trim());
                            //decimal contemptAmount = this.ParseValue(item.Cells[9].Text.Trim());
                            decimal deferredAmount = this.ParseValue(item.Cells[9].Text.Trim());

                            //decimal value = 0;
                            //if (this.allowOnTheFlyRepresentations && this.hasRepresentation)
                            //    value = decimal.Parse(((TextBox)item.FindControl("txtAmountToPay")).Text) + this.ParseValue(item.Cells[9].Text.Trim());
                            //else
                            //dls 080417 ============= - value = this.ParseValue(item.Cells[7].Text.Trim())
                            //    value = this.ParseValue(item.Cells[7].Text.Trim()) + this.ParseValue(item.Cells[9].Text.Trim());

                            //dls 080417 ============= - total += value + contemptAmount;
                            //total += value;
                            total += deferredAmount;

                            sb.Append("<tr><td class=\"CartListItem\">");
                            sb.Append(item.Cells[2].Text);
                            sb.Append("</td><td class=\"CartListItem\">");
                            sb.Append(item.Cells[4].Text);
                            sb.Append("</td><td class=\"CartListItem\">");
                            sb.Append(item.Cells[3].Text);
                            sb.Append("</td><td class=\"CartListItem\">");
                            sb.Append(item.Cells[5].Text);
                            sb.Append("</td><td class=\"CartListItem\">");

                            //update by Rachel 20140819 for 5337
                            //sb.Append(deferredAmount.ToString(CURRENCY_FORMAT));
                            sb.Append(deferredAmount.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture));
                            //end update by Rachel 20140819 for 5337

                            //sb.Append("</td><td class=\"CartListItem\">");
                            //sb.Append(contemptAmount.ToString(CURRENCY_FORMAT));
                            sb.Append("</td><td style=\"border-right: dimgray 1px solid; border-left: dimgray 1px solid;\"");
                            //if ((origAmount + contemptAmount) == value)
                            sb.Append(" class=\"CartListItem\">");
                            //else
                            //    sb.Append(" class=\"NormalRed\">");

                            //update by Rachel 20140819 for 5337
                            //sb.Append(deferredAmount.ToString(CURRENCY_FORMAT));
                            sb.Append(deferredAmount.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture));
                            //end update by Rachel 20140819 for 5337
                            sb.Append("</td></tr>\n");
                        }
                    }
                }
                sb.Append("<tr>");
                for (int i = 0; i < 7; i++)
                    sb.Append("<td style=\"border-top: dimgray 1px solid;\">&nbsp;</td>");
                sb.Append("</tr>\n");

                sb.Append("</td></tr></table><br/>\n");
            }
            if (this._isSuspenseNotice)
            {
                SumGridTotal(ref total);
            }
            sb.Append("<table border=\"0\" cellpadding=\"0\" class=\"CartListItem\" style=\"border-left: none; text-align: left;\">");
            sb.Append("<tr><td style=\"text-align: right\" class=\"NormalBold\">" + (string)GetLocalResourceObject("createTable.Text18") + "</td><td>");

            //update by Rachel 20140819 for 5337
            //sb.Append(total.ToString(CURRENCY_FORMAT));
            sb.Append(total.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture));
            //end update by Rachel 20140819 for 5337
            sb.Append("</td></tr>\n");
            sb.Append("</table><br/>");

            this.labelConfirm.Text = sb.ToString();
        }

        private void SumGridTotal(ref decimal total)
        {
            decimal gridTotal = CountSuspenseAmount();
            this.ViewState["currentFineAmount"] = gridTotal;
            _currentFineAmount = gridTotal;
            total = gridTotal;
        }



        private void WriteAddressDetails(StringBuilder sb)
        {
            sb.Append("</td></tr>\n");
            sb.Append("<tr><td>Address:</td><td>");
            sb.Append(this.txtAddress1.Text);
            sb.Append("</td></tr>\n");
            sb.Append("<tr><td>&nbsp;</td><td>");
            sb.Append(this.txtAddress2.Text);
            if (this.txtAddress3.Text.Trim().Length > 0)
            {
                sb.Append("</td></tr>\n");
                sb.Append("<tr><td>&nbsp;</td><td>");
                sb.Append(this.txtAddress3.Text);
            }
            if (this.txtAddress4.Text.Trim().Length > 0)
            {
                sb.Append("</td></tr>\n");
                sb.Append("<tr><td>&nbsp;</td><td>");
                sb.Append(this.txtAddress4.Text);
            }
            if (this.txtAddress5.Text.Trim().Length > 0)
            {
                sb.Append("</td></tr>\n");
                sb.Append("<tr><td>&nbsp;</td><td>");
                sb.Append(this.txtAddress5.Text);
            }
            sb.Append("</td></tr>\n");
            sb.Append("<tr><td>&nbsp;</td><td>");
            sb.Append(this.txtPoCode.Text);
        }

        protected void buttonBack_Click(object sender, EventArgs e)
        {
            this.panelConfirm.Visible = false;
            this.panelDetails.Visible = true;
            this.lblError.Visible = false;
        }

        protected void buttonVoid_Click(object sender, EventArgs e)
        {
            // FBJ Added (2007-01-29): Reset the notice list
            this._notices.Clear();
            this.Session["NoticesToPay"] = this._notices;
            this.Session["NoticeAddressDetails"] = null;
            this.Session["TRHIntNo"] = null;
            this.Session["TRHTranDate"] = null;

            Response.Redirect("CashReceiptTraffic.aspx");
        }

        protected void UpdateReceiptHeader()
        {
            // Create the receipt object
            ReceiptTransaction receipt = null;

            //receipt header data
            receipt = new ReceiptTransaction();
            receipt.AddressRequired = this._requireAddresses;
            receipt.Cashier = this._cashier;

            receipt.IsOneReceiptPerNotice = _isOneReceiptPerNotice;
            receipt.AutIntNo = this._autIntNo;
            //receipt.Details = ((this.txtComment.Text.Trim().Length > 0) ? this.txtComment.Text : "Receipt created by Cash Receipt - Traffic Page");
            receipt.Details = ((this.txtComment.Text.Trim().Length > 0) ? this.txtComment.Text : "Payment");
            receipt.BAIntNo = int.Parse(Session["BAIntNo"].ToString());
            receipt.CBIntNo = int.Parse(Session["CBIntNo"].ToString());
            receipt.ContactNumber = this.txtPhone.Text.Trim();
            //Jerry 2013-10-28 add time portion for EvidencePack.EPItemDate
            //receipt.ReceivedDate = DateTime.Parse(this.wdcDateReceived.Text);
            receipt.ReceivedDate = DateTime.Parse(this.wdcDateReceived.Text + " " + DateTime.Now.ToString("HH:mm:ss"));
            receipt.ReceivedFrom = this.txtReceivedFrom.Text.Trim();

            receipt.Address1 = this.txtAddress1.Text.Trim();
            receipt.Address2 = this.txtAddress2.Text.Trim();
            receipt.Address3 = this.txtAddress3.Text.Trim();
            receipt.Address4 = this.txtAddress4.Text.Trim();
            receipt.Address5 = this.txtAddress5.Text.Trim();
            receipt.AddressAreaCode = this.txtPoCode.Text.Trim();

            receipt.MaxStatus = this._maxStatus;

            UpdateReceipts(receipt, true);
        }

        protected void AddToPaymentList()
        {
            // Create the receipt object
            ReceiptTransaction receipt = null;
            this._cashType = (CashType)this.ViewState["CashType"];

            switch (this._cashType)
            {
                case CashType.Cash:
                case CashType.Cheque:
                case CashType.Other:
                    receipt = new ChequeTransaction();
                    receipt.ReceivedFrom = this.txtReceivedFrom.Text.Trim();
                    ((ChequeTransaction)receipt).ChequeBank = this.txtBankName.Text.Trim();
                    ((ChequeTransaction)receipt).ChequeBranch = this.txtBranch.Text.Trim();
                    object o = this.wdcChequeDate.Text;
                    ((ChequeTransaction)receipt).ChequeDate = (o == null || o.ToString() == string.Empty) ? new DateTime(1900, 1, 1) : DateTime.Parse(o.ToString());
                    ((ChequeTransaction)receipt).ChequeDrawer = this.txtChequeDrawer.Text.Trim();
                    ((ChequeTransaction)receipt).ChequeNo = (this.txtChequeNo.Text.Trim().Length > 0) ? int.Parse(this.txtChequeNo.Text.Trim()) : 0;
                    break;

                case CashType.CreditCard:

                    receipt = new CardReceipt();
                    ((CardReceipt)receipt).CardIssuer = this.txtCardType.Text.Trim();
                    ((CardReceipt)receipt).NameOnCard = this.txtNameOnCard.Text.Trim();
                    int day = DateTime.DaysInMonth(int.Parse(this.ddlCreditCardYear.SelectedItem.ToString()), int.Parse(this.ddlCardMonth.SelectedValue));
                    string dateString = string.Format("{0}-{1}-{2}", this.ddlCreditCardYear.SelectedItem, this.ddlCardMonth.SelectedValue, day);
                    ((CardReceipt)receipt).ExpiryDate = DateTime.Parse(dateString);
                    break;

                // SD: 20081209: Added for debit card transactions
                case CashType.DebitCard:
                    receipt = new CardReceipt();
                    ((CardReceipt)receipt).CardIssuer = this.txtDCardIssuer.Text.Trim();
                    ((CardReceipt)receipt).NameOnCard = this.txtNameOnDCard.Text.Trim();
                    string dateStringDC = string.Format("{0}-{1}-1", this.ddlDebitCardYear.SelectedItem.ToString(), this.ddlDCardMonth.SelectedValue);
                    ((CardReceipt)receipt).ExpiryDate = DateTime.Parse(dateStringDC);

                    break;

                case CashType.PostalOrder:
                    receipt = new PostalOrderReceipt();
                    ((PostalOrderReceipt)receipt).PostalOrderNumber = this.txtPONo.Text.Trim();
                    break;

                case CashType.BankPayment:
                    receipt = new BankPaymentReceipt();
                    ((BankPaymentReceipt)receipt).Reference = txtReference.Text.Trim();
                    break;
            }

            receipt.Cashier = this._cashier;

            // Adjust the CashType if this is a roadblock
            if (receipt.Cashier.CashBoxType == CashboxType.RoadblockReceipts)
            {
                switch (this._cashType)
                {
                    case CashType.Cash:
                        receipt.CashType = CashType.RoadBlockCash;
                        break;

                    case CashType.Cheque:
                        receipt.CashType = CashType.RoadBlockCheque;
                        break;

                    case CashType.CreditCard:
                    case CashType.DebitCard:
                        receipt.CashType = CashType.RoadBlockCard;
                        break;

                    default:
                        this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text27"), this._cashier.CashBoxType);
                        return;
                }
            }

            // Common receipt data
            receipt.CashType = this._cashType;
            //Jerry 2013-10-28 add time portion for EvidencePack.EPItemDate
            //receipt.ReceivedDate = Convert.ToDateTime(wdcDateReceived.Text);
            receipt.ReceivedDate = Convert.ToDateTime(wdcDateReceived.Text + " " + DateTime.Now.ToString("HH:mm:ss"));

            decimal amount = decimal.Parse(this.txtTotal.Text, NumberStyles.Any);
            receipt.Amount = amount;

            receipt.ContemptAmount = 0;

            UpdateReceipts(receipt, false);
        }

        private void UpdateReceipts(ReceiptTransaction receipt, bool header)
        {
            //add the receipt transaction to the list in the view state
            string errMessage = string.Empty;

            if (_tranDate.Equals(DateTime.MinValue))
            {
                _tranDate = DateTime.Now;
                Session.Add("TRHTranDate", _tranDate);
            }

            int updTRHIntNo = _cashReceipt.AddReceiptToPaymentList(receipt, _tranDate, _tranIntNo, ref errMessage, header, _login, (Int32)Session["userIntNo"]);

            lblError.Visible = true;

            if (updTRHIntNo > 0)
            {
                _tranIntNo = updTRHIntNo;
                Session.Add("TRHIntNo", _tranIntNo);

                BindPaymentGrid();

                ClearPaymentDetails();

                if (!header)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text28");
                    //Linda 2012-9-12 add for list report19
                    //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                    //2013-11-7 Heidi commented out for only on confirm Payment button add PunchStatisticsTransaction
                    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(_connectionString);
                    //punchStatistics.PunchStatisticsTransactionAdd(this._autIntNo, this._login, PunchStatisticsTranTypeList.PaymentTraffic,PunchAction.Add);   
                }
                else
                {
                    ShowConfirmation();
                }
            }
            else if (!header)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text29") + errMessage;
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text30") + errMessage;
            }

        }

        private void ClearPaymentDetails()
        {
            txtCardType.Text = string.Empty;
            // txtCardYear.Text = string.Empty;
            txtReference.Text = string.Empty;
            txtPONo.Text = string.Empty;
            txtChequeNo.Text = string.Empty;
            txtBranch.Text = string.Empty;
            txtBankName.Text = string.Empty;
        }

        private void BindPaymentGrid()
        {
            _dsReceiptPayments = _cashReceipt.GetReceiptPaymentListDS(_tranIntNo);

            grdPayments.DataSource = _dsReceiptPayments;
            grdPayments.DataKeyNames = new string[] { "TRDIntNo", "TRHIntNo", "TRHTranDate", "TRDCashType" };
            grdPayments.DataBind();

            if (grdPayments.Rows.Count > 0)
            {
                grdPayments.Visible = true;
                lblNoPayments.Visible = false;
            }
            else
            {
                lblNoPayments.Visible = true;
                lblNoPayments.Text = (string)GetLocalResourceObject("lblError.Text31");

                //if there are no payments left, re-populate the list of types with the full list
                if (_isBankPayment != 0)
                {
                    _isBankPayment = 0;

                    // dls 10-08-02 - need to clear the payment list setting
                    this._cashType = CashType.Cash;
                    this.ViewState["CashType"] = null;

                    Session["IsBankPayment"] = _isBankPayment;
                    PopulatePaymentTypes();

                    this.SetPaymentType();
                }
            }
        }

        protected void buttonPay_Click(object sender, EventArgs e)
        {
            string errMessage = string.Empty;

            //receipts = new ReceiptList(this._connectionString);

            //List<int> receipts = new List<int>();
            List<ReceiptList> receipts = new List<ReceiptList>();
            //2014-05-15 Heidi changed for displaying 'OPUS' on the Cash Receipt page paid(5249)
            receipts = _cashReceipt.CreateReceiptFromMultiplePayments(_tranIntNo, ref errMessage, this._noOfDaysForPayment, null, false, _pymtOriginator);

            //dls 080417 ============= - code errors here with empty resultset even though the receipt has actually been created!
            if (receipts[0].rctIntNo <= 0)
            {
                this.SetErrorState((string)GetLocalResourceObject("lblError.Text32") + errMessage);
                return;
            }

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this._connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this._login, PunchStatisticsTranTypeList.PaymentTraffic, PunchAction.Add);


            //dls 090701 - moved the udpate of the SumComments to the SP SummonsChargeUpdatePayment which is called from ReceiptProcessMultiplePayments
            //// 20090531 tf Update column SumComments in Summons
            //SummonsDB summonsDb = new SummonsDB(this._connectionString);
            //foreach (NoticeForPayment notice in notices)
            //{
            //    summonsDb.UpdateSummonsComments(notice.NotIntNo, "Case finalized �C payment of R rrrr.cc was received on yyyy/mm/dd HH:MM (receipt # 999999)", this.login);
            //}

            // FBJ Added (2007-01-29): Reset the notice list
            this._notices.Clear();
            this.Session["NoticesToPay"] = this._notices;

            //clear the address details
            this.Session["NoticeAddressDetails"] = null;
            this.Session["TRHIntNo"] = null;
            this.Session["TRHTranDate"] = null;
            this.Session["IsBankPayment"] = null;

            //check if postal receipts are used for all receipts
            this.CheckUsePostalReceipt();

            // Open a new window with the receipt report and print it
            if (this._cashier.CashBoxType != CashboxType.PostalReceipts)
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder sbWOA = new StringBuilder();
                foreach (ReceiptList rct in receipts)
                {
                    sb.Append(rct.rctIntNo);
                    sb.Append(',');
                    //BD 20080624 need to check if any of the rctIntNo are associated with the payment of a WOA, if so need to generate a report for the Receipt on the WOA
                    if (rct.woaIntNo != 0)
                    {
                        sbWOA.Append(rct.woaIntNo);
                        sbWOA.Append(',');
                    }
                }
                //BD Need to check if there are any woaIntNo, if so print out report
                if (sbWOA.Length != 0)
                {
                    Helper_Web.BuildPopup(this, "WOAPaymentsViewer.aspx", "woa", sbWOA.ToString());
                }

                //foreach (int rctIntNo in receipts)
                //{
                //    sb.Append(rctIntNo);
                //    sb.Append(',');
                //}

                if (_usePostalReceipt)
                    Helper_Web.BuildPopup(this, "CashReceiptTraffic_PostalReceiptViewer.aspx", "date", DateTime.Now.ToString("yyyy-MM-dd"), "CBIntNo", "0", "RCtIntNo", sb.ToString());
                else
                    Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", sb.ToString());

                this.panelConfirm.Visible = false;
                this.pnlAnotherPayment.Visible = true;
            }
            else
                Response.Redirect("CashReceiptTraffic.aspx");

        }

        private void SetErrorState(string message)
        {
            this.lblError.Visible = true;
            this.panelDetails.Visible = true;
            this.panelConfirm.Visible = false;

            lblError.Text = message;
        }

        protected void btnRecon_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashReceiptTraffic_CashBoxRecon.aspx");
        }

        protected void btnAddNotices_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashReceiptTraffic.aspx");
        }

        protected void buttonRepresentationYes_Click(object sender, EventArgs e)
        {
            // Check the entered amount
            decimal amount;
            if (!decimal.TryParse(this.txtTotal.Text, NumberStyles.Any, NumberFormatInfo.CurrentInfo, out amount))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text33");
                return;
            }

            // Get the charge int no
            string ticketNumber = string.Empty;
            ImageButton chk = null;
            int count = 0;
            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                chk = (ImageButton)item.FindControl("igPayNow");
                if (chk != null)
                {
                    if (chk.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                    {
                        count++;
                        ticketNumber = item.Cells[2].Text;
                    }
                }
            }

            if (count != 1)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text34");
                return;
            }

            this.panelDetails.Visible = false;
            this.lblError.Visible = false;
            this.panelConfirm.Visible = false;
        }

        protected void lnkBack2_Click(object sender, EventArgs e)
        {
            this.panelDetails.Visible = true;
            this.lblError.Visible = false;
            this.panelConfirm.Visible = false;
            this.lblError.Text = string.Empty;
        }

        protected void checkPayNow_CheckedChanged(object sender, EventArgs e)
        {

            if (this.dgDeferredPayments.Visible == true)
                this.CalculateTotal(true, "deferred");
            else
                CalculateTotal(true, "");
        }

        protected void dgTicketDetails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "SummonsDetails")
            {
                dgTicketDetails.SelectedIndex = e.Item.ItemIndex;

                if (dgTicketDetails.SelectedIndex > -1)
                {
                    int notIntNo = Convert.ToInt32(dgTicketDetails.DataKeys[dgTicketDetails.SelectedIndex]);
                    this.Session.Add("notIntNo", notIntNo);
                    Helper_Web.BuildPopup(this, "ViewSummons_Detail.aspx", null);
                }
            }
        }

        protected void dgDeferredPayments_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "SummonsDetails")
            {
                dgDeferredPayments.SelectedIndex = e.Item.ItemIndex;

                if (dgDeferredPayments.SelectedIndex > -1)
                {
                    int notIntNo = Convert.ToInt32(dgDeferredPayments.DataKeys[dgDeferredPayments.SelectedIndex]);
                    this.Session.Add("notIntNo", notIntNo);
                    Helper_Web.BuildPopup(this, "ViewSummons_Detail.aspx", null);
                }
            }
            else if (e.CommandName == "DeferredPaymentsReport")
            {
                dgDeferredPayments.SelectedIndex = e.Item.ItemIndex;

                if (dgDeferredPayments.SelectedIndex > -1)
                {
                    int notIntNo = Convert.ToInt32(dgDeferredPayments.DataKeys[dgDeferredPayments.SelectedIndex]);
                    this.Session.Add("notIntNo", notIntNo);
                    //Helper_Web.BuildPopup(this, "DeferredPaymentScheduleReportViewer.aspx", null);
                    Helper_Web.BuildPopup(this, "DeferredPaymentsReportViewer.aspx", "notIntNo", notIntNo.ToString());
                }
            }
        }

        protected void dgTicketDetails_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (this._notices == null)
                    this._notices = (List<NoticeForPayment>)this.Session["NoticesToPay"];

                // Get the grid row's NotIntNo
                int notIntNo = 0;

                if (e.Item.Cells[0].Text.Length > 0)
                    notIntNo = int.Parse(e.Item.Cells[0].Text);
                else
                {
                    DataRowView row = (DataRowView)e.Item.DataItem;
                    if (row == null)
                        return;
                    notIntNo = (int)row["NoticeIntNo"];
                }

                // Find the notice
                NoticeForPayment notice = null;
                foreach (NoticeForPayment n in this._notices)
                {
                    if (n.Equals(notIntNo))
                    {
                        notice = n;
                        break;
                    }
                }

                // Set the check box for the notice
                if (notice != null)
                {
                    ImageButton check = (ImageButton)e.Item.FindControl("igPayNow");
                    check.ImageUrl = notice.IsSelected ? CHECKED_IMAGE : UNCHECKED_IMAGE;

                    TextBox txtAmountToPay = ((TextBox)e.Item.FindControl("txtAmountToPay"));
                    txtAmountToPay.Enabled = !notice.HasJudgement;

                    DropDownList ddlSeniorOfficer = ((DropDownList)e.Item.FindControl("ddlSeniorOfficer"));
                    ddlSeniorOfficer.Enabled = !notice.HasJudgement;
                }

            }
        }

        protected void dgDeferredPayments_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (this._notices == null)
                    this._notices = (List<NoticeForPayment>)this.Session["NoticesToPay"];

                // Get the grid row's NotIntNo
                int notIntNo = 0;

                if (e.Item.Cells[0].Text.Length > 0)
                    notIntNo = int.Parse(e.Item.Cells[0].Text);
                else
                {
                    DataRowView row = (DataRowView)e.Item.DataItem;
                    if (row == null)
                        return;
                    notIntNo = (int)row["NoticeIntNo"];
                }

                // Find the notice
                NoticeForPayment notice = null;
                foreach (NoticeForPayment n in this._notices)
                {
                    if (n.Equals(notIntNo))
                    {
                        notice = n;
                        break;
                    }
                }

                // Set the check box for the notice
                if (notice != null)
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ImageButton check = (ImageButton)e.Item.FindControl("igDeferredPayNow");
                        check.ImageUrl = CHECKED_IMAGE; //notice.IsSelected ? CHECKED_IMAGE : UNCHECKED_IMAGE;
                    }
                    else
                    {
                        ImageButton check = (ImageButton)e.Item.FindControl("igDeferredPayNow");
                        check.ImageUrl = UNCHECKED_IMAGE; //notice.IsSelected ? CHECKED_IMAGE : UNCHECKED_IMAGE;
                    }
                    TextBox txtAmountToPay = ((TextBox)e.Item.FindControl("txtAmountToPay"));
                    txtAmountToPay.Enabled = !notice.HasJudgement;

                    DropDownList ddlSeniorOfficer = ((DropDownList)e.Item.FindControl("ddlSeniorOfficer"));
                    ddlSeniorOfficer.Enabled = !notice.HasJudgement;
                }

            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton check = (ImageButton)sender;
            if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                check.ImageUrl = UNCHECKED_IMAGE;
            else
                check.ImageUrl = CHECKED_IMAGE;

            //if (this.dgDeferredPayments.Visible == true)
            //    this.CalculateTotal(true, "deferred");
            //else
            CalculateTotal(true, "");

        }

        protected void ImageDeferredButton1_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton check = (ImageButton)sender;
            if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                check.ImageUrl = UNCHECKED_IMAGE;
            else
                check.ImageUrl = CHECKED_IMAGE;

            //if (this.dgDeferredPayments.Visible == true)
            this.CalculateTotal(true, "deferred");
            //else
            //    CalculateTotal(true, "");

        }

        protected void ddlReceivedFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlReceivedFrom.SelectedItem.Value)
            {
                case "D":       //Driver
                    this._personaType = PersonaType.Driver;
                    ShowOrHideAddress(true);
                    break;
                case "O":       //Other
                    this._personaType = PersonaType.Owner;
                    ShowOrHideAddress(true);
                    break;
                case "P":       //Proxy
                    this._personaType = PersonaType.Proxy;
                    ShowOrHideAddress(true);
                    break;
                case "X":       //Other
                    this._personaType = PersonaType.Other;
                    ShowOrHideAddress(false);
                    break;
                case "A":
                    this._personaType = PersonaType.Accused;
                    ShowOrHideAddress(true);
                    break;
            }

            ViewState.Add("PersonaType", this._personaType);

            if (Session["NoticeAddressDetails"] != null)
                GetAddressDetails(0, false);
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text35");
                lblError.Visible = true;
            }
        }

        protected void ShowOrHideAddress(bool show)
        {
            lblName.Visible = show;
            lblAddress.Visible = show;
            lblPostalCode.Visible = show;
            txtReceivedFrom.Visible = show;
            txtAddress1.Visible = show;
            txtAddress2.Visible = show;
            txtAddress3.Visible = show;
            txtAddress4.Visible = show;
            txtAddress5.Visible = show;
            txtPoCode.Visible = show;
        }

        protected void dgDeferredPayments_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (Session["AddressDetails"] == null || changeAddressDetails)
            //{
            int notIntNo = (int)this.dgDeferredPayments.DataKeys[dgDeferredPayments.SelectedIndex];

            GetAddressDetails(notIntNo, true);
            //}
        }

        protected void dgTicketDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (Session["AddressDetails"] == null || changeAddressDetails)
            //{
            int notIntNo = (int)this.dgTicketDetails.DataKeys[dgTicketDetails.SelectedIndex];

            GetAddressDetails(notIntNo, true);
            //}
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            lblError.Visible = false;
            lblError.Text = string.Empty;

            _noOfNotices = 0;

            if (this._isSuspenseNotice)
            {
                if (String.IsNullOrEmpty(this.txtNoticeNumber.Text))
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = (string)GetLocalResourceObject("NoticeNumberIsRequired");
                    return;
                }
                //else
                //{
                //    string notType = GetNoticeTypeFromTicketNo(this.txtNoticeNumber.Text.Trim());
                //    string validateType = string.Empty;
                //    if (notType == "M" || notType == "H")
                //    {
                //        validateType = "cdv";
                //    }
                //    else
                //    {
                //        validateType = "verhoeff";
                //    }
                //    if (Validation.ValidateTicketNumber(this.txtNoticeNumber.Text.Trim(), validateType))
                //    {
                //        this.lblError.Visible = true;
                //        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text46");
                //        return;
                //    }
                //}
                this.ShowNoticeSearchResults(string.Empty, this.txtNoticeNumber.Text.Trim());
            }

            if (this.dgTicketDetails.Visible == true)
            {
                foreach (DataGridItem item in this.dgTicketDetails.Items)
                {
                    ImageButton check = null;
                    int notIntNo = int.Parse(item.Cells[0].Text);

                    foreach (NoticeForPayment notice in this._notices)
                    {
                        if (notice.NotIntNo == notIntNo)
                        {
                            check = (ImageButton)item.FindControl("igPayNow");
                            if (notice.IsSelected) _noOfNotices++;
                            break;
                        }
                    }
                }
            }
            else
            {
                foreach (DataGridItem item in this.dgDeferredPayments.Items)
                {
                    ImageButton check = null;
                    int notIntNo = int.Parse(item.Cells[0].Text);
                    foreach (NoticeForPayment notice in this._notices)
                    {
                        if (notice.NotIntNo == notIntNo)
                        {
                            check = (ImageButton)item.FindControl("igDeferredPayNow");
                            if (notice.IsSelected) _noOfNotices++;
                            break;
                        }
                    }
                }
            }

            //add the current set of payment details to the Dataset that is driving the grid
            switch (comboPaymentType.SelectedItem.Value)
            {
                case "Cash":
                case "Bank Payment":
                    this.ConfirmCash();
                    break;

                case "Cheque":
                    this.ConfirmCheque();
                    break;

                case "Credit Card":
                    this.ConfirmCreditCard();
                    break;

                case "Debit Card":
                    this.ConfirmDebitCard();
                    break;

                case "Postal Order":
                    this.ConfirmPostalOrder();
                    break;

                case "Other":
                    this.ConfirmOtherCashType();
                    break;
            }

            if (comboPaymentType.SelectedItem.Value.Equals("Bank Payment"))
                this._isBankPayment = 2;
            else
                // remove bank payment option
                this._isBankPayment = 1;

            //remove all the other options from the list of available payment types
            PopulatePaymentTypes();

            Session["IsBankPayment"] = _isBankPayment;
        }

        string GetNoticeTypeFromTicketNo(string ticketNo)
        {
            //string notType = string.Empty;
            if (!String.IsNullOrEmpty(ticketNo))
            {
                string notPrefix = string.Empty;
                if (ticketNo.IndexOf("/") > 0)
                {
                    notPrefix = ticketNo.Split('/')[0];
                }
                else if (ticketNo.IndexOf("-") > 0)
                {
                    notPrefix = ticketNo.Split('-')[0];
                }

                TList<NoticePrefixHistory> noticePrefixHistory = new NoticePrefixHistoryService().GetByNprefixAndAuthIntNo(notPrefix, _autIntNo);
                if (noticePrefixHistory != null && noticePrefixHistory.Count >= 1)
                {
                    return noticePrefixHistory[0].NphType;
                }
            }

            return string.Empty;
        }

        private void ConfirmPostalOrder()
        {
            bool isValid = true;
            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("RefreshPaymentList") + "<br/><ul>");

            isValid = this.ValidateCommonData(sb);

            string postalOrderNo = this.txtPONo.Text.Trim();
            if (postalOrderNo.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList1") + "</li>\n");
            }

            this.RefreshPaymentList(sb, isValid);
        }

        private void ConfirmCreditCard()
        {
            bool isValid = true;
            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("RefreshPaymentList") + "<br/><ul>");

            isValid = ValidateCommonData(sb);

            string cardType = this.txtCardType.Text.Trim();
            if (cardType.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList2") + "</li>\n");
            }

            string nameOnCard = txtNameOnCard.Text.Trim();
            if (nameOnCard.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList3") + "</li>\n");
            }

            //Month defaults to January (0)
            if (this.ddlCreditCardYear.SelectedIndex < 1)            //|| this.ddlCardMonth.SelectedIndex < 1)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList4") + "</li>\n");
            }
            else
            {
                //SD:05.11.2008 : changed year to ddlCreditCardYear.SelectedItem from txtCardYear.Text.Trim()
                int day = DateTime.DaysInMonth(int.Parse(this.ddlCreditCardYear.SelectedItem.ToString()), int.Parse(this.ddlCardMonth.SelectedValue));
                string dateString = string.Format("{0}-{1}-{2}", this.ddlCreditCardYear.SelectedItem, this.ddlCardMonth.SelectedValue, day);
                //this.txtCardYear.Text.Trim()
                DateTime dtExpiry;
                if (!DateTime.TryParse(dateString, out dtExpiry))
                {
                    isValid = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList5") + "</li>\n");
                }
                else
                {
                    if (DateTime.Compare(dtExpiry, DateTime.Today) < 0)
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList6") + "</li>\n");
                    }
                }
            }

            this.RefreshPaymentList(sb, isValid);
        }

        // SD: 20081209 validate debit card details
        private void ConfirmDebitCard()
        {
            bool isValid = true;
            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("RefreshPaymentList") + "<br/><ul>");

            isValid = ValidateCommonData(sb);

            string cardType = this.txtDCardIssuer.Text.Trim();
            if (cardType.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList7") + "</li>\n");
            }

            string nameOnCard = txtNameOnDCard.Text.Trim();
            if (nameOnCard.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList8") + "</li>\n");
            }

            string dateString = string.Format("{0}-{1}-01", this.ddlDebitCardYear.SelectedItem.Text.Trim(), this.ddlDCardMonth.SelectedValue);
            DateTime dtExpiry;
            if (!DateTime.TryParse(dateString, out dtExpiry))
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList5") + "</li>\n");
            }
            else
            {
                if (DateTime.Compare(dtExpiry, DateTime.Today) < 1)
                {
                    isValid = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList6") + "</li>\n");
                }
            }

            this.RefreshPaymentList(sb, isValid);
        }

        private void ConfirmCheque()
        {
            bool isValid = true;
            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("RefreshPaymentList") + "<br/><ul>");

            isValid = this.ValidateCommonData(sb);

            string bankName = this.txtBankName.Text.Trim();
            if (bankName.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList9") + "</li>\n");
            }

            DateTime dt;

            if (!DateTime.TryParse(wdcChequeDate.Text, out dt))
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList10") + "</li>\n");
            }
            else
            {
                if (Convert.ToDateTime(this.wdcChequeDate.Text) > DateTime.Now)
                {
                    isValid = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList11") + "</li>\n");
                }

                //dls 080417 ============= - test for stale cheques
                if (Convert.ToDateTime(this.wdcChequeDate.Text) <= DateTime.Now.AddMonths(-6))
                {
                    isValid = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList12") + "</li>\n");
                }
            }

            string drawer = this.txtChequeDrawer.Text.Trim();
            if (drawer.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList13") + "</li>\n");
            }
            int chequeNo;
            if (this.txtChequeNo.Text.Trim().Length == 0 || !int.TryParse(this.txtChequeNo.Text.Trim(), out chequeNo))
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList14") + "</li>\n");
            }
            if (!this.checkVerified.Checked)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList15") + "</li>\n");
            }
            if (this.txtBranch.Text.Trim().Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("RefreshPaymentList16") + "</li>\n");
            }

            this.RefreshPaymentList(sb, isValid);
        }

        private void ConfirmCash()
        {
            bool isValid = true;

            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("RefreshPaymentList") + "<br/><ul>");

            isValid = this.ValidateCommonData(sb);

            this.RefreshPaymentList(sb, isValid);
        }
        private void ConfirmOtherCashType()
        {
            bool isValid = true;

            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("RefreshPaymentList") + "<br/><ul>");

            isValid = this.ValidateCommonData(sb);

            this.RefreshPaymentList(sb, isValid);
        }

        protected void btnPayNow_Click(object sender, EventArgs e)
        {
            //dls 080130 - need to check a few things here like addresses, etc.
            bool isValid = true;

            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("RefreshPaymentList17") + "<br/><ul>");

            isValid = this.ValidateHeaderData(sb);

            if (!this.VaidateComtemptData(sb) || isValid == false)
            {
                this.ShowError(sb, false);
                return;
            }

            _dsReceiptPayments = _cashReceipt.GetReceiptPaymentListDS(_tranIntNo);

            //dls 070406 - check this first, as otherwise it gets overwritten
            if (!this._isSuspenseNotice && !this.CheckForRepresentations(sb, isValid))
            {
                //hide all the payment options
                if (_isBankPayment != 2)
                    this.comboPaymentType.SelectedValue = "Cash";

                SetPaymentType();

                this.ShowError(sb, false);
                return;
            }

            isValid = AddNoticesForPayment();

            if (isValid)
                UpdateReceiptHeader();

        }

        private bool AddNoticesForPayment()
        {
            bool isValid = true;

            string errMessage = string.Empty;
            string noticeList = string.Empty;
            StringBuilder sb = new StringBuilder(string.Empty);

            AccountDB accountCharge = new AccountDB(_connectionString);

            int isDeferredPayment = 0;

            sb = new StringBuilder("<root>");

            if (this._isSuspenseNotice)
            {
                decimal total = 0;
                SumGridTotal(ref total);
                SuspenseAccount suspenseAccount = new SuspenseAccount(_connectionString);
                DummyNotice dummyNotice = suspenseAccount.GetDummyNotice(_autIntNo, this._currentFineAmount, this._login, this.txtNoticeNumber.Text.Trim());
                if (dummyNotice != null && dummyNotice.NotIntNo > 0 && dummyNotice.ChgIntNo > 0)
                {
                    ChargeDB chargeDB = new ChargeDB(_connectionString);
                    ChargeDetails chargeDetail = chargeDB.GetChargeDetails(dummyNotice.ChgIntNo);
                    int accIntNo = accountCharge.GetAccount(chargeDetail.CTIntNo);
                    if (accIntNo <= 0)
                    {
                        isValid = false;
                        this.SetErrorState("Unable to find a valid ledger account for this charge. Receipt not processed");
                        return isValid;
                    }
                    string seniorOfficer = string.Empty;
                    //Barry Dickson 20080409 - add in contempt amount to xml
                    sb.Append("<notice notintno=\"");
                    sb.Append(dummyNotice.NotIntNo);
                    sb.Append("\"");
                    sb.Append(" chgintno=\"");
                    sb.Append(chargeDetail.ChgIntNo);
                    sb.Append("\"");
                    sb.Append(" ctintno=\"");
                    sb.Append(chargeDetail.CTIntNo);
                    sb.Append("\"");
                    sb.Append(" accintno=\"");
                    sb.Append(accIntNo);
                    sb.Append("\"");
                    sb.Append(" chgrevfineamount=\"");
                    //update by Rachel 20140811 for 5337
                    //sb.Append(chargeDetail.RevFineAmount);
                    sb.Append(chargeDetail.RevFineAmount.ToString(CultureInfo.InvariantCulture));
                    //end update by Rachel 20140811 for 5337
                    sb.Append("\"");
                    sb.Append(" contemptamount=\"");
                    sb.Append(0);
                    sb.Append("\"");
                    sb.Append(" amountpaid=\"");
                    //update by Rachel 20140811 for 5337
                    //sb.Append(chargeDetail.RevFineAmount);
                    sb.Append(chargeDetail.RevFineAmount.ToString(CultureInfo.InvariantCulture));
                    //end update by Rachel 20140811 for 5337
                    sb.Append("\"");
                    sb.Append(" seniorofficer=\"");
                    sb.Append(seniorOfficer);
                    sb.Append("\"");
                    sb.Append(" />");
                }
                else
                {
                    isValid = false;
                    this.SetErrorState("Unable to create dummy notice. Receipt not processed");
                    return isValid;
                }
            }
            else
            {

                ImageButton check = null;
                //int x = 0;

                // Process each charge/notice in the list
                // NB: ANY CHANGE TO THE DataGrid COLUMNS WILL BREAK THE FOLOWING CODE !!!!!

                //BD Need to check which grid to use as deferred payments is a different grid
                if (this.dgTicketDetails.Visible == true)
                {
                    foreach (DataGridItem item in this.dgTicketDetails.Items)
                    {
                        // Is this item included in the payment
                        check = (ImageButton)item.FindControl("igPayNow");
                        if (!check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                            continue;

                        int notIntNo = int.Parse(item.Cells[0].Text);
                        int chgIntNo = int.Parse(item.Cells[1].Text);

                        //Barry Dickson - added 3 fields to grid 9 now = 12
                        //dls 080416 - this may be blank - seems like it ended up being column 13 not 12????
                        //int ctIntNo = int.Parse(item.Cells[12].Text);
                        int ctIntNo = 0;

                        if (Int32.TryParse(item.Cells[13].Text, out ctIntNo))
                            ctIntNo = Int32.Parse(item.Cells[13].Text);

                        //Barry Dickson - add in contempt court amt to total - amount to pay must be the addition of both
                        //dls - 080417 - no the rev fine amount is the fine amount before the rep
                        decimal chgRevFineAmount = this.ParseValue(item.Cells[7].Text); //+ this.ParseValue(item.Cells[9].Text);
                        decimal contemptAmount = this.ParseValue(item.Cells[9].Text);
                        decimal amountPaid = chgRevFineAmount + contemptAmount;
                        string seniorOfficer = string.Empty;

                        if (this._allowOnTheFlyRepresentations && this._hasRepresentation)
                        {
                            //dls 080417 ============= - amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text) + contemptAmount;
                            //Barry Dickson putting contempt amount back in here...
                            //amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text);
                            amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text) + contemptAmount;
                            seniorOfficer = ((DropDownList)item.FindControl("ddlSeniorOfficer")).Text;
                        }

                        int accIntNo = accountCharge.GetAccount(ctIntNo);
                        if (accIntNo <= 0)
                        {
                            isValid = false;
                            this.SetErrorState((string)GetLocalResourceObject("lblError.Text36"));
                            return isValid;
                        }
                        //Barry Dickson 20080409 - add in contempt amount to xml
                        sb.Append("<notice notintno=\"");
                        sb.Append(notIntNo);
                        sb.Append("\"");
                        sb.Append(" chgintno=\"");
                        sb.Append(chgIntNo);
                        sb.Append("\"");
                        sb.Append(" ctintno=\"");
                        sb.Append(ctIntNo);
                        sb.Append("\"");
                        sb.Append(" accintno=\"");
                        sb.Append(accIntNo);
                        sb.Append("\"");
                        sb.Append(" chgrevfineamount=\"");
                        //update by Rachel 20140811 for 5337
                        //sb.Append(chgRevFineAmount);
                        sb.Append(chgRevFineAmount.ToString(CultureInfo.InvariantCulture));
                        //end update by Rachel 20140811 for 5337
                        sb.Append("\"");
                        sb.Append(" contemptamount=\"");
                        //update by Rachel 20140811 for 5337
                        //sb.Append(contemptAmount);
                        sb.Append(contemptAmount.ToString(CultureInfo.InvariantCulture));
                        //end update by Rachel 20140811 for 5337
                        sb.Append("\"");
                        sb.Append(" amountpaid=\"");
                        //update by Rachel 20140811 for 5337
                        //sb.Append(amountPaid);
                        sb.Append(amountPaid.ToString(CultureInfo.InvariantCulture));
                        //end update by Rachel 20140811 for 5337
                        sb.Append("\"");
                        sb.Append(" seniorofficer=\"");
                        sb.Append(seniorOfficer);
                        sb.Append("\"");
                        sb.Append(" />");
                    }
                }
                else
                {
                    //Deferred Payment information
                    foreach (DataGridItem item in this.dgDeferredPayments.Items)
                    {
                        isDeferredPayment = 1;

                        // Is this item included in the payment
                        check = (ImageButton)item.FindControl("igDeferredPayNow");
                        if (!check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                            continue;

                        int notIntNo = int.Parse(item.Cells[0].Text);
                        int chgIntNo = int.Parse(item.Cells[1].Text);
                        int SCDefPayIntNo = int.Parse(item.Cells[11].Text);

                        //Barry Dickson - added 3 fields to grid 9 now = 12
                        //dls 080416 - this may be blank - seems like it ended up being column 13 not 12????
                        //int ctIntNo = int.Parse(item.Cells[12].Text);
                        int ctIntNo = 0;

                        //Jake 2014-11-10 modified the index from 15 to 14 ->CTIntNO
                        if (Int32.TryParse(item.Cells[14].Text, out ctIntNo))
                            ctIntNo = Int32.Parse(item.Cells[14].Text);

                        //Barry Dickson - add in contempt court amt to total - amount to pay must be the addition of both
                        //dls - 080417 - no the rev fine amount is the fine amount before the rep
                        //decimal chgRevFineAmount = this.ParseValue(item.Cells[7].Text); //+ this.ParseValue(item.Cells[9].Text);
                        //decimal contemptAmount = this.ParseValue(item.Cells[9].Text);
                        //decimal amountPaid = chgRevFineAmount + contemptAmount;
                        //string seniorOfficer = string.Empty;
                        decimal deferredAmount = this.ParseValue(item.Cells[9].Text);

                        //if (this.allowOnTheFlyRepresentations && this.hasRepresentation)
                        //{
                        //dls 080417 ============= - amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text) + contemptAmount;
                        //Barry Dickson putting contempt amount back in here...
                        //amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text);
                        //amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text) + contemptAmount;
                        //seniorOfficer = ((DropDownList)item.FindControl("ddlSeniorOfficer")).Text;
                        //}

                        int accIntNo = accountCharge.GetAccount(ctIntNo);
                        if (accIntNo <= 0)
                        {
                            isValid = false;
                            this.SetErrorState((string)GetLocalResourceObject("lblError.Text36"));
                            return isValid;
                        }
                        //Barry Dickson 20080409 - add in contempt amount to xml
                        sb.Append("<notice notintno=\"");
                        sb.Append(notIntNo);
                        sb.Append("\"");
                        sb.Append(" chgintno=\"");
                        sb.Append(chgIntNo);
                        sb.Append("\"");
                        sb.Append(" ctintno=\"");
                        sb.Append(ctIntNo);
                        sb.Append("\"");
                        sb.Append(" accintno=\"");
                        sb.Append(accIntNo);
                        sb.Append("\"");
                        sb.Append(" deferredAmount=\"");
                        //update by Rachel 20140811 for 5337
                        //sb.Append(deferredAmount);
                        sb.Append(deferredAmount.ToString(CultureInfo.InvariantCulture));
                        //end update by Rachel 20140811 for 5337
                        sb.Append("\"");
                        sb.Append(" SCDefPayIntNo=\"");
                        sb.Append(SCDefPayIntNo);
                        sb.Append("\"");
                        //sb.Append(" contemptamount=\"");
                        //sb.Append(contemptAmount);
                        //sb.Append("\"");
                        //sb.Append(" amountpaid=\"");
                        //sb.Append(amountPaid);
                        //sb.Append("\"");
                        //sb.Append(" seniorofficer=\"");
                        //sb.Append(seniorOfficer);
                        //sb.Append("\"");
                        sb.Append(" />");
                    }
                }
            }

            sb.Append("</root>");
            noticeList = sb.ToString();

            int updTRHIntNo = _cashReceipt.AddNoticesToReceiptList(_tranIntNo, noticeList, isDeferredPayment, ref errMessage);

            if (updTRHIntNo < 0)
            {
                isValid = false;
                this.SetErrorState((string)GetLocalResourceObject("lblError.Text37") + errMessage);
                return isValid;
            }

            return true;
        }

        protected void grdPayments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                int nRow = Convert.ToInt32(e.CommandArgument);

                int trdIntNo = (int)this.grdPayments.DataKeys[nRow].Values["TRDIntNo"];

                string errMessage = string.Empty;

                int delTRDIntNo = _cashReceipt.RemovePaymentFromList(trdIntNo, ref errMessage);

                if (delTRDIntNo < 1)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text38") + errMessage;
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text39") + errMessage;

                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this._connectionString);
                    //punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this._login, PunchStatisticsTranTypeList.PaymentTraffic, PunchAction.Delete);
                }

                lblError.Visible = true;

                BindPaymentGrid();

                if (this.dgDeferredPayments.Visible == true)
                    CalculateTotal(true, "deferred");
                else
                    CalculateTotal(true, "");
            }
        }

        protected void chkCashierRep_CheckedChanged(object sender, EventArgs e)
        {
            ProcessRepresentations();
        }

        protected void dgTicketDetails_EditCommand(object source, DataGridCommandEventArgs e)
        {
            lblError.Visible = false;

            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("lblError.Text40") + "</br><ul>");
            bool isValid = true;

            TextBox txtAmountToPay = (TextBox)e.Item.FindControl("txtAmountToPay");
            DropDownList ddlSeniorOfficer = (DropDownList)e.Item.FindControl("ddlSeniorOfficer");

            int notIntNo = int.Parse(e.Item.Cells[0].Text);

            //decimal fullAmount = this.ParseValue(e.Item.Cells[7].Text) + this.ParseValue(e.Item.Cells[7].Text);
            decimal fullAmount = this.ParseValue(e.Item.Cells[7].Text) + this.ParseValue(e.Item.Cells[9].Text);

            decimal amountToPay = 0;

            if (!Decimal.TryParse(txtAmountToPay.Text, out amountToPay))
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text41") + "</li>\n");
                isValid = false;
            }
            else if (amountToPay > fullAmount)
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text42") + "</li>\n");
                isValid = false;
            }
            else if (amountToPay == 0)
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text43") + "</li>\n");
                isValid = false;
            }

            if (ddlSeniorOfficer.SelectedIndex < 0)
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text44") + "</li>\n");
                isValid = false;
            }

            if (!isValid)
            {
                pnlAddPayments.BorderStyle = BorderStyle.Solid;
                ShowError(sb, false);
                return;
            }

            //Barry Dickson
            //amountToPay += this.ParseValue(e.Item.Cells[9].Text); 

            foreach (NoticeForPayment notice in this._notices)
            {
                if (notice.NotIntNo == notIntNo)
                {
                    notice.AmountToPay = amountToPay;
                    notice.SeniorOfficer = ddlSeniorOfficer.SelectedItem.Text;
                    break;
                }
            }

            this.Session["NoticesToPay"] = this._notices;

            RefreshNoticeGrid();

            decimal diff = CalculateTotal(true, true);

            if (diff == 0)
                btnContinue.Enabled = true;
            else
                btnContinue.Enabled = false;

            lblError.Visible = true;
            lblError.Text = (string)GetLocalResourceObject("lblError.Text45");

        }

        protected void dgDeferredPayments_EditCommand(object source, DataGridCommandEventArgs e)
        {
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            RefreshNoticeGrid();

            bool isValid = AddNoticesForPayment();

            if (isValid)
                UpdateReceiptHeader();
        }

        protected void btnBackFromRepresentations_Click(object sender, EventArgs e)
        {
            chkCashierRep.Checked = false;
            ProcessRepresentations();
        }

        private void ProcessRepresentations()
        {
            this._hasRepresentation = chkCashierRep.Checked;
            chkCashierRep.Visible = false;
            pnlAddPayments.BorderStyle = this._hasRepresentation ? BorderStyle.None : BorderStyle.Solid;
            btnBackFromRepresentations.Visible = this._hasRepresentation;
            this.ViewState.Add("HasRepresentation", this._hasRepresentation);

            lblError.Visible = false;


            lblRepresentations.Visible = this._hasRepresentation;
            btnContinue.Visible = this._hasRepresentation;

            pnlPaymentType.Visible = !this._hasRepresentation;
            btnAdd.Visible = !this._hasRepresentation;
            pnlReceiptHeader.Visible = !this._hasRepresentation;

            if (this._hasRepresentation)
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text1");
            else
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

            if (btnContinue.Visible)
                btnContinue.Enabled = false;

            RefreshNoticeGrid();

            this.pnlBankPayment.Visible = _isBankPayment == 2 ? !this._hasRepresentation : false;

        }

        //protected void txtTotal_TextChanged(object sender, EventArgs e)
        //{
        //    if (this._isSuspenseNotice && !String.IsNullOrEmpty(this.txtTotal.Text))
        //    {
        //        decimal amountToPay;
        //        Decimal.TryParse(this.txtTotal.Text.Trim(), out amountToPay);

        //        this.labelTotal.Text = amountToPay.ToString(CURRENCY_FORMAT);
        //    }
        //}
    }
}
