﻿<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.0 Transitional//EN" >
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS._Default" Codebehind="Default.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">

    <script language="javascript" type="text/javascript">
		
    </script>

    <tg1:Tag ID="Tag1" runat="server"></tg1:Tag>
</head>
<body bottommargin="0" leftmargin="0" bgcolor="black" topmargin="0" rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="right" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="upd" runat="server">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" border="0" height="85%">
                    <tr>
                        <td align="center" valign="top" style="height: 351px">
                            <img style="height: 1px" src="images/1x1.gif" width="167"> 
                        </td>
                        <td valign="middle" align="left" width="100%" style="height: 351px">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <table height="100%" width="100%">
                                <tr>
                                    <td width="90%" style="height: 309px">
                                        <asp:ImageButton ID="imgMain" runat="server"></asp:ImageButton>
                                    </td>
                                    <td align="left" valign="top" style="height: 309px">
                                        <table width="100%">
                                            
                                            <tr>
                                                <td align="right" style="width: 205px">
                                                    <asp:Button ID="btnSystemOverview" runat="server" Text="<%$Resources:btnSystemOverview.Text %>"  
                                                        OnClick="btnSystemOverview_Click" Width="200px"/>
                                                   <%-- <igtxt:WebImageButton ID="System Overview" runat="server" OnClick="WebImageButton3_Click"
                                                        UseBrowserDefaults="False" Text="System Overview" Height="22px" ImageDirectory="/ig_common/20071CLR20/Styles/Office2007Blue/WebImageButton/"
                                                        Width="200px">
                                                        <Appearance>
                                                            <Image Width="150px" />
                                                            <Style BackColor="Control" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px"></Style>
                                                        </Appearance>
                                                        <HoverAppearance>
                                                            <Style>
<BorderDetails ColorTop="248, 248, 248" ColorLeft="248, 248, 248" ColorBottom="104, 104, 104" ColorRight="104, 104, 104"></BorderDetails>
</Style>
                                                        </HoverAppearance>
                                                        <FocusAppearance>
                                                            <Style BorderColor="#909090"></Style>
                                                        </FocusAppearance>
                                                        <RoundedCorners RenderingType="FileImages" DisabledImageUrl="ig_butXP5s.gif" FocusImageUrl="ig_butXP3s.gif"
                                                            HoverImageUrl="igwib_bg-office12_blue_hover.gif" ImageUrl="igwib_bg-office12_blue_normal.gif"
                                                            MaxHeight="22" MaxWidth="200" PressedImageUrl="igwib_bg-office12_blue_pressedl.gif"
                                                            HeightOfBottomEdge="0" WidthOfRightEdge="5" />
                                                        <PressedAppearance>
                                                            <Style>
<BorderDetails ColorTop="104, 104, 104" ColorLeft="104, 104, 104" ColorBottom="248, 248, 248" ColorRight="248, 248, 248"></BorderDetails>
</Style>
                                                        </PressedAppearance>
                                                    </igtxt:WebImageButton>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 205px">
                                                    <%--<asp:Button ID="btnCameraMgt" runat="server" Text="Camera Management" 
                                                        OnClick="btnCameraMgt_Click" Width="200px" Visible="False" />--%>
                                                   <%-- <igtxt:WebImageButton ID="WebImageButton2" runat="server" OnClick="WebImageButton2_Click"
                                                        UseBrowserDefaults="False" Text="Camera Management" Height="22px" ImageDirectory="/ig_common/20071CLR20/Styles/Office2007Blue/WebImageButton/"
                                                        Visible="False" Width="200px">
                                                        <Appearance>
                                                            <Image Width="150px" />
                                                            <Style BackColor="Control" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px"></Style>
                                                        </Appearance>
                                                        <HoverAppearance>
                                                            <Style>
<BorderDetails ColorTop="248, 248, 248" ColorLeft="248, 248, 248" ColorBottom="104, 104, 104" ColorRight="104, 104, 104"></BorderDetails>
</Style>
                                                        </HoverAppearance>
                                                        <FocusAppearance>
                                                            <Style BorderColor="#909090"></Style>
                                                        </FocusAppearance>
                                                        <RoundedCorners RenderingType="FileImages" DisabledImageUrl="ig_butXP5s.gif" FocusImageUrl="ig_butXP3s.gif"
                                                            HoverImageUrl="igwib_bg-office12_blue_hover.gif" ImageUrl="igwib_bg-office12_blue_normal.gif"
                                                            MaxHeight="22" MaxWidth="200" PressedImageUrl="igwib_bg-office12_blue_pressedl.gif"
                                                            HeightOfBottomEdge="0" WidthOfRightEdge="5" />
                                                        <PressedAppearance>
                                                            <Style>
<BorderDetails ColorTop="104, 104, 104" ColorLeft="104, 104, 104" ColorBottom="248, 248, 248" ColorRight="248, 248, 248"></BorderDetails>
</Style>
                                                        </PressedAppearance>
                                                    </igtxt:WebImageButton>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="height: 15px; width: 205px;" class="NormalBold" align="right">
                                                  <%--  <asp:Button ID="btnPaymentStats" runat="server" Text="Payment Statistics" 
                                                        OnClick="btnPaymentStats_Click" Width="200px" Visible="False"/>--%>
                                                   <%-- <igtxt:WebImageButton ID="WebImageButton1" runat="server" OnClick="WebImageButton1_Click"
                                                        UseBrowserDefaults="False" Text="Payment Statistics" Height="22px" ImageDirectory="/ig_common/20071CLR20/Styles/Office2007Blue/WebImageButton/"
                                                        Width="200px" Visible="False">
                                                        <Appearance>
                                                            <Image Width="150px" />
                                                            <Style BackColor="Control" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px"></Style>
                                                        </Appearance>
                                                        <HoverAppearance>
                                                            <Style>
<BorderDetails ColorTop="248, 248, 248" ColorLeft="248, 248, 248" ColorBottom="104, 104, 104" ColorRight="104, 104, 104"></BorderDetails>
</Style>
                                                        </HoverAppearance>
                                                        <FocusAppearance>
                                                            <Style BorderColor="#909090"></Style>
                                                        </FocusAppearance>
                                                        <RoundedCorners RenderingType="FileImages" DisabledImageUrl="ig_butXP5s.gif" FocusImageUrl="ig_butXP3s.gif"
                                                            HoverImageUrl="igwib_bg-office12_blue_hover.gif" ImageUrl="igwib_bg-office12_blue_normal.gif"
                                                            MaxHeight="22" MaxWidth="200" PressedImageUrl="igwib_bg-office12_blue_pressedl.gif"
                                                            HeightOfBottomEdge="0" WidthOfRightEdge="5" />
                                                        <PressedAppearance>
                                                            <Style>
<BorderDetails ColorTop="104, 104, 104" ColorLeft="104, 104, 104" ColorBottom="248, 248, 248" ColorRight="248, 248, 248"></BorderDetails>
</Style>
                                                        </PressedAppearance>
                                                    </igtxt:WebImageButton>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
