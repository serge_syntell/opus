using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// Represents an editor page for Court Transaction numbers
    /// </summary>
    public partial class Court_TransactionNos : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo;
        protected string styleSheet;
        protected string backgroundImage;
        //protected string thisPage = "Court Transaction Number Maintenance";
        protected string thisPageURL = "Court_TransactionNos.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        // Constants
        private const string COPY_TEXT = "Copy";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName;
            int userAccessLevel = userDetails.UserAccessLevel;

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlEditTranNo.Visible = false;
                btnOptAdd.Visible = true;
                btnOptCopy.Visible = false;
                this.btnOptCopy.Text = (string)GetLocalResourceObject("btnOptCopy.Text");
                this.pnlTransactionList.Visible = false;
                this.pnlEditTranNo.Visible = false;

                this.PopulateCourts();
            }
        }


        protected void PopulateCourts()
        {
            CourtDB courts = new CourtDB(this.connectionString);

            ddlCourt.DataSource = courts.GetCourtList();
            ddlCourt.DataValueField = "CrtIntNo";
            ddlCourt.DataTextField = "CrtDetails";
            ddlCourt.DataBind();
            ddlCourt.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourt.Items"), string.Empty));
        }

        protected void BindGrid(int crtIntNo)
        {
            Stalberg.TMS.CourtTranDB ctTranNo = new Stalberg.TMS.CourtTranDB(this.connectionString);
            int totalCount = 0; //2013-04-07 add by Henry for pagination
            DataSet ds = ctTranNo.GetCourtTranListDS(crtIntNo, gvCourtTranPager.PageSize, gvCourtTranPager.CurrentPageIndex, out totalCount);
            this.gvCourtTran.DataSource = ds;
            this.gvCourtTran.DataKeyNames = new string[] { "CTIntNo" };
            this.gvCourtTran.DataBind();
            gvCourtTranPager.RecordCount = totalCount;

            if (ds.Tables[0].Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }

            this.pnlTransactionList.Visible = true;
            this.btnOptAdd.Visible = true;
            if (this.btnOptCopy.Text.Equals(COPY_TEXT))
                this.btnOptCopy.Visible = false;
            this.pnlEditTranNo.Visible = false;
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            this.AddNewCourtTransaction();

            txtCTYear.Enabled = true;
            btnUpdateTranNo.Text = (string)GetLocalResourceObject("lblAddTransaction.Text");
        }

        private void AddNewCourtTransaction()
        {
            CourtTranDetails detail = new CourtTranDetails();
            detail.CTYear = DateTime.Now.Year;
            detail.CurrentNumber = 1;
            this.BindDetails(detail);
            this.lblEditTranNo.Text = (string)GetLocalResourceObject("lblEditTranNo.Text1");
            this.pnlEditTranNo.Visible = true;
        }

        protected void btnUpdateTranNo_Click(object sender, System.EventArgs e)
        {
            bool isValid = true;
            StringBuilder sb = new StringBuilder("<p>" + (string)GetLocalResourceObject("lblError.Text1") + "\n<ul>");

            if (string.IsNullOrEmpty(this.ddlCourt.SelectedValue))
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text6") + "</li>\n");
            }

            string type = this.txtCTType.Text.Trim();
            if (type.Length == 0 || type.Length > 3)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text2") + "</li>\n");
            }

            int currentNo = 0;
            if (!int.TryParse(this.txtCTNumber.Text, out currentNo) || currentNo < 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text7") + "</li>\n");
            }

            if (!isValid)
            {
                sb.Append("</ul>\n");
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return;
            }

            CourtTranDetails details = new CourtTranDetails();
            details.CtIntNo = Convert.ToInt32(hidCTIntNo.Value);
            details.Description = this.txtCTDescr.Text.Trim();
            details.CurrentNumber = currentNo;
            details.Type = type;
            details.CourtIntNo = Convert.ToInt32(this.ddlCourt.SelectedValue);
            details.CTYear = Convert.ToInt32(txtCTYear.Text.Trim());

            Stalberg.TMS.CourtTranDB db = new CourtTranDB(this.connectionString);
            if (!db.UpdateCourtTranDetails(details, this.login))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                if (details.CtIntNo == -1)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtTransactionNumberMaintenance, PunchAction.Add); 
                }
                if (details.CtIntNo > 0)
                {
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtTransactionNumberMaintenance, PunchAction.Change);  

                }
                //2013-04-07 add by Henry for pagination
                //Edge 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                //gvCourtTranPager.CurrentPageIndex = 1;
                //gvCourtTranPager.RecordCount = 0;
                this.BindGrid(Convert.ToInt32(this.ddlCourt.SelectedValue));
            }

            lblError.Visible = true;
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (this.ddlCourt.SelectedValue.Length == 0)
            {
                this.pnlEditTranNo.Visible = false;
                this.pnlTransactionList.Visible = false;
                this.btnOptAdd.Visible = false;
                if (this.btnOptCopy.Text.Equals(COPY_TEXT))
                    this.btnOptCopy.Visible = false;

                return;
            }
            //2013-04-07 add by Henry for pagination
            gvCourtTranPager.CurrentPageIndex = 1;
            gvCourtTranPager.RecordCount = 0;
            this.BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));
        }


        //Jerry 2012-05-07 change it to gvCourtTran_SelectedIndexChanging
        //protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    int ctIntNo = (int)this.gvCourtTran.DataKeys[e.NewEditIndex]["CTIntNo"];

        //    CourtTranDB db = new CourtTranDB(this.connectionString);
        //    CourtTranDetails details = db.GetDetails(ctIntNo);
        //    this.BindDetails(details);

        //    this.lblEditTranNo.Text = "Edit Court Transaction";
        //    this.pnlEditTranNo.Visible = true;
        //    this.btnOptCopy.Visible = true;
        //}

        private void BindDetails(CourtTranDetails detail)
        {
            this.txtCTType.Text = detail.Type;
            this.txtCTDescr.Text = detail.Description;
            this.txtCTNumber.Text = detail.CurrentNumber.ToString();
            this.hidCTIntNo.Value = detail.CtIntNo.ToString();
            //Jerry 2012-05-07 add
            this.txtCTYear.Text = detail.CTYear.ToString();
        }

        protected void btnOptCopy_Click(object sender, EventArgs e)
        {
            if (this.btnOptCopy.Text.Equals(COPY_TEXT))
            {
                this.ViewState.Add("CTType", this.txtCTType.Text);
                this.ViewState.Add("CTDescr", this.txtCTDescr.Text);
                //this.ddlCourt.SelectedIndex = 0;
                this.btnOptCopy.Text = (string)GetLocalResourceObject("btnOptCopy.Text");
                //this.pnlTransactionList.Visible=false;
                //this.pnlEditTranNo.Visible=false;
            }
            else
            {
                this.AddNewCourtTransaction();
                this.txtCTType.Text = this.ViewState["CTType"].ToString();
                this.txtCTDescr.Text = this.ViewState["CTDescr"].ToString();

                this.ViewState.Remove("CTTYpe");
                this.ViewState.Remove("CTDescr");
                this.btnOptCopy.Text = (string)GetLocalResourceObject("btnOptCopy.Text");
            }
        }

        /// <summary>
        /// Jerry 2012-05-07 add
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvCourtTran_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int ctIntNo = (int)this.gvCourtTran.DataKeys[e.NewSelectedIndex]["CTIntNo"];

            CourtTranDB db = new CourtTranDB(this.connectionString);
            CourtTranDetails details = db.GetDetails(ctIntNo);
            this.BindDetails(details);

            this.lblEditTranNo.Text = (String)GetLocalResourceObject("lblEditTranNo.Text2");
            this.pnlEditTranNo.Visible = true;
            this.btnOptCopy.Visible = true;
            txtCTYear.Enabled = false;
            btnUpdateTranNo.Text = (string)GetLocalResourceObject("btnUpdateTranNo.Text");
        }

        //2013-04-07 add by Henry for pagination
        protected void gvCourtTranPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid(Convert.ToInt32(this.ddlCourt.SelectedValue));
        }

    }
}
