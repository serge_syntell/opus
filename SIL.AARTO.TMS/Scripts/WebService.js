﻿function Action(value)
{
    if(document.getElementById("lblError") != null)
        document.getElementById("lblError").innerHTML = "";
        
    document.getElementById("btnAccept").disabled = true;
    if (document.getElementById("lnkPrevious") != null) 
    {
        document.getElementById("lnkPrevious").disabled = true;
    }
    if (document.getElementById("lnkNext") != null) 
    {
        document.getElementById("lnkNext").disabled = true;
    }

    var param = new Stalberg.TMS.ActionParam();
    param.ProcessName = value;
    param.RejReason = document.Form1.ddlRejection.options[document.Form1.ddlRejection.selectedIndex].text;
    param.RejIntNo = document.Form1.ddlRejection.options[document.Form1.ddlRejection.selectedIndex].value;
    param.Registration = document.getElementById("txtRegNo").value;
    param.ChangedMake = document.getElementById("ddlVehMake").disabled;
    param.VehMake = document.getElementById("ddlVehMake").value;
    param.VehType = document.getElementById("ddlVehType").value;
    param.UserName = document.getElementById("hidUserName").value;
    param.ProcessType = document.getElementById("btnAccept").value;
    param.FrameNo = document.getElementById("txtFrameNo").value;
    param.StatusNatis = document.getElementById("hidStatusNatis").value;
    param.FrameIntNo = document.getElementById("hidFrameIntNo").value;
    param.PreUpdatedRegNo = document.getElementById("hidPreUpdatedRegNo").value;
    param.PreUpdatedRejReason = document.getElementById("hidPreUpdatedRejReason").value;
    param.AllowContinue = document.getElementById("chkAllowContinue").checked;
    param.PreUpdatedAllowContinue = document.getElementById("hidPreUpdatedAllowContinue").value;
    param.RowVersion = document.getElementById("hidRowVersion").value; 
    if(document.getElementById("hidStatusVerified"))
        param.StatusVerified = document.getElementById("hidStatusVerified").value;
    if(document.getElementById("hidStatusRejected"))
        param.StatusRejected = document.getElementById("hidStatusRejected").value;
    if(document.getElementById("hidStatusProsecute"))
        param.StatusProsecute = document.getElementById("hidStatusProsecute").value;
    if(document.getElementById("hidStatusCancel"))
        param.StatusCancel = document.getElementById("hidStatusCancel").value;
    if(document.getElementById("txtOffenceDate")&&document.getElementById("txtOffenceTime"))
        param.OffenceDateTime = document.getElementById("txtOffenceDate").value + " " + document.getElementById("txtOffenceTime").value;
    if(document.getElementById("txtFirstSpeed"))
        param.Speed1 = document.getElementById("txtFirstSpeed").value;
    if(document.getElementById("txtSecondSpeed"))
        param.Speed2 = document.getElementById("txtSecondSpeed").value;
        
    document.getElementById("hidUpdateMainImage").value = value;

    if (document.getElementById("chkSaveImageSettings"))
        param.SaveImageSettings = document.getElementById("chkSaveImageSettings").checked;

    if (param.SaveImageSettings && document.getElementById("imageViewer1$contrast"))
        param.Contrast = document.getElementById("imageViewer1$contrast").value;
    else
        param.Contrast = "99";

    if (param.SaveImageSettings && document.getElementById("imageViewer1$brightness"))
        param.Brightness = document.getElementById("imageViewer1$brightness").value;
    else
        param.Brightness = "99";
    
    WebService.FrameAction(param,SucceededCallback,FailedCallback);
}
function SucceededCallback(result, userContext, methodName)
{
    switch(methodName)
    {
        case "FrameAction":
            if (result != "")
                alert(result);
            if (result == "Frame cannot be adjudicated - there is no corresponding offence code for the values on the frame") {
                document.getElementById("lblMessage").innerHTML = "The Frame is not Prosecutable";
                document.getElementById("btnAccept").disabled = true;
            }
            break;
        default:
            break;
    }
}
function FailedCallback(error, userContext, methodName)
{
    switch(methodName)
    {
        case "FrameAction":
            alert(error.message);
            break;
        default:
            break;
    }
}
