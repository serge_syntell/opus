﻿function VerifytLookupRequired() {
    var validLookUp = 0;
    $(".table_lookup input").each(function () {
        //if ($.trim($(this).parent().prev().text()).toUpperCase() == "ENGLISH") {
        if ($.trim($(this).attr("class")) == "en-US") {
            if ($.trim($(this).val()) == "") {
                $(this).next().show();
                validLookUp = +1;
            }
            else {
                $(this).next().hide();
            }
        }
    });
  
    if (typeof (Page_ClientValidate) == "undefined") {
        return validLookUp <= 0;
    }
    else {
        return Page_ClientValidate()&&validLookUp <= 0;
    }

}


