<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.CaptureSpotFineBatch" Codebehind="CaptureSpotFineBatch.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <script src="Scripts/Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body style="margin: 0px 0px 0px 0px" background="<%=backgroundImage %>">
    <form id="Form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center" style="width: 182px">
                    <img height="26" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlDetails" runat="server" Width="101%">
                                <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />&nbsp;</p></asp:Panel>
                            <table id="tblControls" border="0" class="NormalBold">
                                <tr>
                                    <td class="NormalBold">
                                        <asp:Label ID="Label8" runat="server" Text="<%$Resources:lblAuthority.Text %>"></asp:Label> </td>
                                    <td>
                                        <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged" Width="140px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 86px">
                                        <asp:CheckBox ID="chkCompleted" runat="server" CssClass="Normal" Text="<%$Resources:chkCompleted.Text %>"
                                            Width="141px" /></td>
                                    <td style="width: 86px">
                                        <asp:CheckBox ID="chkPrinted" runat="server" CssClass="Normal" Text="<%$Resources:chkPrinted.Text %>"
                                            Width="173px" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;&nbsp;
                                    </td>
                                    <td colspan="1" style="width: 86px">
                                    </td>
                                    <td colspan="1" style="width: 86px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnViewBatches" runat="server" CssClass="NormalButton" OnClick="btnViewBatches_Click"
                                            Text="<%$Resources:btnViewBatches.Text %>" Width="150px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnNewBatch" runat="server" CssClass="NormalButton" OnClick="btnNewBatch_Click"
                                            Text="<%$Resources:btnNewBatch.Text %>" Width="150px" /></td>
                                    <td style="width: 86px">
                                    </td>
                                    <td style="width: 86px">
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Panel ID="pnlBatches" runat="server">
                                <asp:GridView ID="GridViewBatches" runat="server" AutoGenerateColumns="False" Font-Size="8pt"
                                    CellPadding="4" GridLines="Vertical"
                                    ShowFooter="True" AllowPaging="False" BorderColor="Black" 
                                    onselectedindexchanged="GridViewBatches_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="<%$Resources:GridViewBatches.HeaderText %>">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("SFBIntNo") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("SFBIntNo") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SFBRegisterDate" HeaderText="<%$Resources:GridViewBatches.HeaderText1 %>"
                                            DataFormatString="{0:yyyy-MM-dd}" HtmlEncode="False">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle Height="2px" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SFBCreateDate" HeaderText="<%$Resources:GridViewBatches.HeaderText2 %> " DataFormatString="{0:yyyy-MM-dd}"
                                            HtmlEncode="False">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SFBCompleteDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText=" <%$Resources:GridViewBatches.HeaderText3 %>"
                                            SortExpression="SFBCompleteDate" HtmlEncode="False">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SFBPrintDate" HeaderText="<%$Resources:GridViewBatches.HeaderText4 %> " ReadOnly="True"
                                            SortExpression="SFBPrintDate" DataFormatString="{0:yyyy-MM-dd}" HtmlEncode="False">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="<%$Resources:GridViewBatches.HeaderText5 %> " ReadOnly="True" DataField="TotalNotices">
                                            <ItemStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="<%$Resources:GridViewBatches.HeaderText6 %>  " ReadOnly="True" DataField="SFBType">
                                            <ItemStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:CommandField ShowSelectButton="True" />
                                    </Columns>
                                    <FooterStyle CssClass="CartListFooter" />
                                    <RowStyle CssClass="CartListItem" />
                                    <SelectedRowStyle CssClass="CartListSelected" />
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                                <pager:AspNetPager id="GridViewBatchesPager" runat="server" 
                                    showcustominfosection="Right" width="400px" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                      FirstPageText="|&amp;lt;" 
                                    LastPageText="&amp;gt;|" 
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                    CustomInfoStyle="float:right;" PageSize="10" onpagechanged="GridViewBatchesPager_PageChanged"  UpdatePanelId="UpdatePanel1"
                                    ></pager:AspNetPager>

                                <!-- <asp:BoundField HeaderText="No. of N without Summons" ReadOnly="True" DataField="TotalNoticesWithOutSummons">
                                            <ItemStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="No. of N with Summons" ReadOnly="True" DataField="TotalNoticesWithSummons">
                                            <ItemStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                        </asp:BoundField> -->
                            </asp:Panel>
                            <asp:Panel ID="pnlFixNotice" runat="server" Width="100%" CssClass="Normal" Visible="False"
                                DefaultButton="btnAccept">
                                <br />
                                <fieldset>
                                    <legend>
                                        <asp:Label ID="Label9" runat="server" Text="<%$Resources:lblNoticeReceipt.Text %>"></asp:Label></legend>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSFBNIntNo" runat="server" CssClass="NormalBold" Visible="false"></asp:Label></td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblNoticeNo.Text %>" CssClass="NormalBold"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblNoticePrefix" runat="server" CssClass="NormalBold"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblNoticeSeqNo" runat="server" CssClass="NormalBold"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblFullNoticeNo" runat="server" CssClass="NormalBold"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblOffenceDate.Text %>" CssClass="NormalBold"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblOffenceDate" runat="server" CssClass="NormalBold"></asp:Label></td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblReceiptNo.Text %>" CssClass="NormalBold"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtRctNumber" runat="server" CssClass="NormalBold"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblAmount.Text %>" CssClass="NormalBold"></asp:Label></td>
                                            <td>
                                                <%--<asp:Label ID="Label6" runat="server" Text="R" CssClass="NormalBold"></asp:Label>--%><asp:TextBox
                                                    ID="lblRTAmount" runat="server" CssClass="NormalBold"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAccept" runat="server" CssClass="NormalButton" OnClick="btnUpdateNotice_Click"
                                                    Text="<%$Resources:btnAccept.Text %>" Width="100px" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnReject" runat="server" CssClass="NormalButton" OnClick="btnCancelNotice_Click"
                                                    Text="<%$Resources:btnReject.Text %>" Width="100px" />
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnlNotices" runat="server" Width="100%" Visible="False">
                                <table>
                                    <tr>
                                        <td style="width: 86px">
                                            <asp:CheckBox ID="chkSpotFine" runat="server" CssClass="Normal" Text="<%$Resources:chkSpotFine.Text %>"
                                                Width="141px" AutoPostBack="true" OnCheckedChanged="chkSpotFine_CheckedChanged" Visible="false" /></td>
                                        <td style="width: 86px">
                                            <asp:CheckBox ID="chkSummons" runat="server" CssClass="Normal" Text="<%$Resources:chkSummons.Text %>" Width="110px"
                                                AutoPostBack="true" OnCheckedChanged="chkSummons_CheckedChanged" Visible="false" /></td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnNoticeSelect" runat="server" Text="<%$Resources:btnNoticeSelect.Text %>" OnClick="btnNoticeSelect_Click"
                                                OnClientClick="return confirm($('#btnHideCofirmMsg').val());" />
                                            <asp:Button ID="btnReceiptSelect" runat="server" Text="<%$Resources:btnReceiptSelect.Text %>" OnClick="btnReceiptSelect_Click"
                                                OnClientClick="return confirm($('#btnHideCofirmMsg').val());" />
                                            <asp:Label ID="lblNoticeHeader" runat="server" CssClass="Normal" Visible="false" />
                                            <input type="hidden" value="<%$Resources:btnNoticeSelect.ClickTip %>" id="hidSelectConfirmMsg" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label7" runat="server" CssClass="Normal" Text="<%$Resources:lblSortOrder.Text %>" />
                                            <asp:DropDownList ID="ddlSortOrder" runat="server" AutoPostBack="true" CssClass="Normal"
                                                OnSelectedIndexChanged="ddlSortOrder_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFind" runat="server" CssClass="Normal" />
                                            <asp:Button ID="btnFind" runat="server" Text="<%$Resources:btnFind.Text %>" CssClass="Normal" OnClick="btnFind_Click" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="GridViewNotices" runat="server" AutoGenerateColumns="False"
                                    CellPadding="4" GridLines="Vertical"
                                    OnRowCommand="GridViewNotices_RowCommand" ShowFooter="True" CssClass="Normal"
                                    AllowSorting="False" OnSorting="GridViewNotices_Sorting">
                                    <Columns>
                                        <asp:BoundField DataField="NotIntNo" HeaderText="NotIntNo" Visible="False" />
                                        <asp:TemplateField HeaderText="<%$Resources:GridViewNotices.HeaderText %>">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="printNotice" runat="server" Checked='<%# TrueFalse(Eval("PrintNotice").ToString()) %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="center" />
                                            <ItemStyle HorizontalAlign="center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$Resources:GridViewNotices.HeaderText1 %> ">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="printReceipt" runat="server" Checked='<%# TrueFalse(Eval("PrintReceipt").ToString()) %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="center" />
                                            <ItemStyle HorizontalAlign="center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$Resources:GridViewNotices.HeaderText2 %> ">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NotTicketNo") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("NotTicketNo") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=" <%$Resources:GridViewNotices.HeaderText3 %>">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ChgOffenceCode") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("ChgOffenceCode") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$Resources:GridViewNotices.HeaderText4 %> ">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("NotOffenceDate") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=" <%$Resources:GridViewNotices.HeaderText5 %>">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("SFBNRctNumber") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("SFBNRctNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$Resources:GridViewNotices.HeaderText6 %> ">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("SFBNRTAmount", "{0:0.00}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("SFBNRTAmount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:CommandField SelectText="Edit Notice" ShowSelectButton="True" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="SFBNIntNo" HeaderText="SFBNIntNo" Visible="False" />
                                    </Columns>
                                    <FooterStyle CssClass="CartListFooter" />
                                    <RowStyle CssClass="CartListItem" />
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                                <pager:AspNetPager id="GridViewNoticesPager" runat="server" 
                                    showcustominfosection="Right" width="400px" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                      FirstPageText="|&amp;lt;" 
                                    LastPageText="&amp;gt;|" 
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                    CustomInfoStyle="float:right;" PageSize="15" onpagechanged="GridViewNoticesPager_PageChanged" UpdatePanelId="UpdatePanel1"
                                    ></pager:AspNetPager>
                            </asp:Panel>
                            <asp:Panel ID="pnlPrintBatch" runat="server">
                                <br />
                                <asp:Label Visible="false" ID="lblSupervisor" runat="server" CssClass="Normal" Text="<%$Resources:lblSupervisor.Text %>" />
                                <br>
                                <br></br>
                                <asp:GridView ID="grdPrintBatch" runat="server" AllowPaging="False" 
                                    AutoGenerateColumns="False" CellPadding="4" CssClass="Normal" 
                                    GridLines="Vertical" OnRowCommand="grdPrintBatch_RowCommand" 
                                    OnSelectedIndexChanged="grdPrintBatch_SelectedIndexChanged" ShowFooter="True">
                                    <Columns>
                                        <asp:BoundField DataField="SFBNPrintFile" 
                                            HeaderText="<%$Resources:grdPrintBatch.HeaderText %>" />
                                        <asp:BoundField DataField="UserCompleteDate" DataFormatString="{0:yyyy-MM-dd}" 
                                            HeaderText="<%$Resources:grdPrintBatch.HeaderText1 %>" HtmlEncode="false" />
                                        <asp:BoundField DataField="SFBNPrintDate" DataFormatString="{0:yyyy-MM-dd}" 
                                            HeaderText="<%$Resources:grdPrintBatch.HeaderText2 %>" HtmlEncode="false" />
                                        <asp:BoundField DataField="NoNotices" 
                                            HeaderText="<%$Resources:grdPrintBatch.HeaderText3 %>" />
                                        <asp:HyperLinkField DataNavigateUrlFields="SFBNPrintFile" 
                                            DataNavigateUrlFormatString="CaptureSpotFineBatchViewer.aspx?Batch={0}" 
                                            Target="_blank" Text="<%$Resources:grdPrintBatchItem.Text %>" Visible="False" />
                                        <asp:CommandField SelectText="Print" ShowSelectButton="True" />
                                        <asp:TemplateField HeaderText="User Capture Print Date" Visible="false">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtUserPrintDate" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnUserPrintUpdate" runat="server" Text="Update" 
                                                    Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnDelete" runat="server" 
                                                    CommandArgument='<%# Eval("SFBNPrintFile") %>' CommandName="pfDelete" 
                                                    OnClientClick="return confirm('Are you sure you want to delete this print file ?');" 
                                                    Text="Delete" Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="CartListFooter" />
                                    <RowStyle CssClass="CartListItem" />
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                                <pager:AspNetPager ID="grdPrintBatchPager" runat="server" 
                                    CurrentPageButtonStyle="color:#000;" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                    CustomInfoSectionWidth="" CustomInfoStyle="float:right;" 
                                    FirstPageText="|&amp;lt;" Font-Size="12px" Height="20px" 
                                    LastPageText="&amp;gt;|" onpagechanged="grdPrintBatchPager_PageChanged" 
                                      showcustominfosection="Right" 
                                    ShowDisabledButtons="False" width="400px" UpdatePanelId="UpdatePanel1">
                                </pager:AspNetPager>
                                </br>
                            </asp:Panel>
                            <asp:Panel ID="pnlButtons" runat="server" Width="100%">
                                <br />
                                <table style="border-style: none;">
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:Button ID="btnUpdatePrintSchedule" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdatePrintSchedule.Text %>"
                                                Width="136px" OnClick="btnUpdatePrintSchedule_Click" />
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Button ID="btnPrintRegister" runat="server" CssClass="NormalButton" Text="<%$Resources:btnPrintRegister.Text %>"
                                                Width="136px" OnClick="btnPrintRegister_Click" />
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Button ID="btnCompleteBatch" runat="server" CssClass="NormalButton" OnClick="btnCompleteBatch_Click"
                                                Text="<%$Resources:btnCompleteBatch.Text %>" Width="136px" OnClientClick="return confirm($('#btnHideCofirmMsg').val());" />
                                            <input type="hidden" value="<%$Resources:btnCompleteBatch.Tip %>" id="btnHideCofirmMsg" runat="server" />
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Button ID="btnPrintBatch" runat="server" CssClass="NormalButton" Text="<%$Resources:btnPrintBatch.Text %>"
                                                Width="136px" OnClick="btnPrintBatch_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;<asp:Label
                                    ID="Label6" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center" style="width: 182px">
                </td>
                <td valign="top" align="left" style="width: 104%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
