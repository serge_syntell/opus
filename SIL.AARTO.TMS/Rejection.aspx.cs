using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{

    public partial class Rejection : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        protected string thisPageURL = "Rejection.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Rejection category";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                //this.lbPageHeader.Text = (string)GetLocalResourceObject("lblPageName.Text");
                //pnlAddRejReason.Visible = false;
                pnlUpdateRejCategory.Visible = false;
                pnlRejReasUpdate.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptHide.Visible = true;
                btnOptDeleteReason.Visible = false;
                btnOptAddRejectionReason.Visible = false;

                // BindGrid();
                dgCategoryBindGrid();
                PopulateRejectionCategoryList();

                // mrs20081126 need reject categories here
                PopulateRejectionCategoryListForUpdate();

            }
        }

        //SD:14.07.2008: data to bind Rejection Category grid
        protected void dgCategoryBindGrid()
        {

            Stalberg.TMS.RejectionDB conList = new Stalberg.TMS.RejectionDB(connectionString);

            DataSet data = conList.GetRejectionCategoryDS();
            dgRejCategory.DataSource = data;
            dgRejCategory.DataKeyField = "RejCIntNo";
            dgRejCategory.DataBind();
     
            if (dgRejCategory.Items.Count == 0)
            {
                dgRejCategory.Visible = false;
                lblError.Visible = true;
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgRejCategory.Visible = true;
            }

            btnOptDelete.Visible = false;
        }

        // SD: 15.07.2008 data to bind rejection category details grid
        protected void dgRejectionReasonBindGrid(int editRejCIntNo)
        {
            // display all the reject reasons bound to the reject category
            Stalberg.TMS.RejectionDB conList = new Stalberg.TMS.RejectionDB(connectionString);

            DataSet result = conList.GetRejectionCategoryDetails(editRejCIntNo);

            //mrs 20081127 this actually the rejection table display
            dgRejectionReason.DataSource = result;
            dgRejectionReason.DataKeyField = "RejIntNo";

            try
            {
                dgRejectionReason.DataBind();
            }
            catch
            {
                dgRejectionReason.CurrentPageIndex = 0;
                dgRejectionReason.DataBind();
            }
            
            result.Dispose();

            if (dgRejectionReason.Items.Count == 0)
            {
                dgRejectionReason.Visible = false;
                lblError.Visible = true;
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgRejectionReason.Visible = true;
            }

            btnOptDeleteReason.Visible = false;
        }

        //SD:16.07.2008
        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // make add rejection category visible
            pnlAddCategory.Visible =true;
            pnlUpdateRejCategory.Visible = false;
            pnlAddRejectionReason.Visible = false;
            pnlRejReasUpdate.Visible = false;
            dgRejectionReason.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
        }

        protected void btnAddRejCat_Click(object sender, System.EventArgs e)
        {
            // add rejection category
            string rejSkip = "N";
            if (chkRejCategory.Checked) rejSkip = "Y";

            // add the new transaction to the database table
            RejectionDB toAdd = new RejectionDB(connectionString);
            int rejCIntNo = toAdd.AddRejectionCategory(txtAddRejCategory.Text, rejSkip, loginUser);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(rejCIntNo, lgEntityList, "RejectCategoryLookup", "RejCIntNo", "RejCCategory", this.loginUser);

            dgCategoryBindGrid();
            pnlAddCategory.Visible = false;
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RejectionReasons, PunchAction.Add);  

        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            // update rejection category
            int rejCIntNo = Convert.ToInt32(ViewState["editRejCIntNo"].ToString());

            string rejSkip = "N";

            if (chkRejSkip.Checked) rejSkip = "Y";

            // add the new transaction to the database table
            RejectionDB conUpdate = new RejectionDB(connectionString);

            int updRejCIntNo = conUpdate.UpdateRejectionCategories(rejCIntNo, txtRejectUpdate.Text, rejSkip, loginUser);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(updRejCIntNo, lgEntityList, "RejectCategoryLookup", "RejCIntNo", "RejCCategory", this.loginUser);
           
            if (updRejCIntNo <= 0)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RejectionReasons, PunchAction.Change);  
            }

            lblError.Visible = true;
            dgCategoryBindGrid();
        }

        protected void ShowRejectionCatDetails(int editRejIntNo)
        {
            // show rejection Cctegory
            Stalberg.TMS.RejectionDB rejection = new Stalberg.TMS.RejectionDB(connectionString);

            Stalberg.TMS.RejectionCategoryDetails rejDetails = rejection.GetRejectionCategory(editRejIntNo);
            txtRejectUpdate.Text = rejDetails.RejCategory;
            chkRejCategory.Checked = rejDetails.RejSkip.Equals("Y") ? true : false;

            pnlUpdateRejCategory.Visible = true;
            btnOptAddRejectionReason.Visible = true;
            btnOptDelete.Visible = true;
        }


        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
            // delete reject category
            int rejCIntNo = Convert.ToInt32(ViewState["editRejCIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "RejectCategoryLookup", "RejCIntNo");

            RejectionDB rejection = new Stalberg.TMS.RejectionDB(connectionString);

            int delRejCIntNo = rejection.DeleteRejectionCategory(rejCIntNo);

            if (delRejCIntNo == 0 )
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            }
            else if (delRejCIntNo == -1)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            }
            else
            {

                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RejectionReasons, PunchAction.Delete); 
            }
            lblError.Visible = true;
            dgCategoryBindGrid();
            pnlUpdateRejCategory.Visible = false;
        }

        protected void btnOptDeleteReason_Click(object sender, System.EventArgs e)
        {
            // delete reject category
            int rejIntNo = Convert.ToInt32(ViewState["editRejIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate1.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "RejectionLookup", "RejIntNo");

            RejectionDB rejection = new Stalberg.TMS.RejectionDB(connectionString);

            int delRejIntNo = rejection.DeleteRejection(rejIntNo);

            if (delRejIntNo == 0)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            }
            else if (delRejIntNo == -1)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
            }
            else
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RejectionReasons, PunchAction.Delete); 
            }
            lblError.Visible = true;

            int rejCIntNo = Convert.ToInt32(ViewState["editRejCIntNo"]);
            dgRejectionReasonBindGrid(rejCIntNo);
            pnlRejReasUpdate.Visible = false;
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgRejCategory.Visible.Equals(true))
            {
                //hide it
                dgRejCategory.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
            }
            else
            {
                //show it
                dgRejCategory.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        

        protected void dgRejCategory_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            pnlUpdateRejCategory.Visible = false;
            pnlAddCategory.Visible = false;
            dgRejCategory.SelectedIndex = e.Item.ItemIndex;

            if (dgRejCategory.SelectedIndex > -1)
            {
                int editRejCIntNo = Convert.ToInt32(dgRejCategory.DataKeys[dgRejCategory.SelectedIndex]);

                ViewState["editRejCIntNo"] = editRejCIntNo;
                ViewState["prevPage"] = thisPageURL;
                
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editRejCIntNo.ToString(), "RejectCategoryLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                //Jerry 2012-07-03 change
                if (e.CommandName == "Edit")
                {
                    ShowRejectionCatDetails(editRejCIntNo);
                    dgRejectionReason.Visible = false;
                    pnlAddRejectionReason.Visible = false;
                    pnlRejReasUpdate.Visible = false;
                }

                if (e.CommandName == "Select")
                {
                    dgRejectionReasonBindGrid(editRejCIntNo);
                    pnlAddRejectionReason.Visible = false;
                    pnlRejReasUpdate.Visible = false;
                }
            }
        }

        protected void dgRejCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //pnlAddRejectionReason.Visible = true;
            //int RejIntNo = -1;
            //RejIntNo = Convert.ToInt32(dgRejCategory.SelectedIndex.ToString());
            //RejIntNo = Convert.ToInt32(e..ToString());
            //dgRejectionReasonBindGrid(RejIntNo);
        }
        protected void btnAddRejection_Click(object sender, EventArgs e)
        {
            //add rejection category
            string rejSkip = "N";
            if (chkAddRejectionReason.Checked) 
                rejSkip = "Y";

            // add the new transaction to the database table
            Stalberg.TMS.RejectionDB toAdd = new RejectionDB(connectionString);
            int addRejCIntNo = toAdd.AddRejectionCategory(txtAddRejCategory.Text, rejSkip, loginUser);

            if (addRejCIntNo < 0)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            }
            else if (addRejCIntNo == 0)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            }
            else
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            }

            dgCategoryBindGrid();
            pnlAddRejectionReason.Visible = false;
        }


        // SD: 16.07.2008, gets the list of CategoryRejections to bind Dropdown list 
        protected void PopulateRejectionCategoryList()
        {
            RejectionDB rejCat = new RejectionDB(connectionString);
            SqlDataReader reader = rejCat.GetRejectionCategoryList();
            
            Dictionary<int, string> lookups =
                RejectCategoryLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int rejCIntNo = (int)reader["RejCIntNo"];
                if (lookups.ContainsKey(rejCIntNo))
                {
                    ddlRejectionCategory.Items.Add(new ListItem(lookups[rejCIntNo], rejCIntNo.ToString()));
                }
            }
            //ddlRejectionCategory.DataSource = reader;
            ////ddlRejectionCategory.SelectedValue = "Please Select";
            //ddlRejectionCategory.DataValueField = "RejCIntNo";
            //ddlRejectionCategory.DataTextField = "RejCCategory";
            //ddlRejectionCategory.DataBind();
            reader.Close();
        }

     
        protected void btnOptAddRejectionReason_Click(object sender, System.EventArgs e)
        {
            // mrs 20081128 make add rejection reason visible
            pnlAddRejectionReason.Visible = true;
            pnlRejReasUpdate.Visible = false;
            pnlUpdateRejCategory.Visible = false;
            pnlAddCategory.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd1.DataBind(entityList);
        }

        //SD: 16.07.2008, add button  
        protected void btnAddRejectionReason_Click(object sender, EventArgs e)
        {
            string rejSkip = "N";
            if (chkAddRejectionReason.Checked) 
                rejSkip = "Y";


            string natisBSkip = "N";
            if (chkAddNatisBSkip.Checked)
                natisBSkip = "Y";

            // add the new transaction to the database table
            RejectionDB toAdd = new RejectionDB(connectionString);
            int rejCIntNo = Convert.ToInt32(ddlRejectionCategory.SelectedValue);

            int addRejIntNo = toAdd.AddRejection(txtRejectReason.Text, rejSkip, loginUser, rejCIntNo, natisBSkip);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd1.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addRejIntNo, lgEntityList, "RejectionLookup", "RejIntNo", "RejReason", this.loginUser);


            if (addRejIntNo <= 0)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
            }
            else
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RejectionReasons, PunchAction.Add); 
            }

            dgRejectionReasonBindGrid(rejCIntNo);
            pnlAddRejectionReason.Visible = false;
        }

        protected void dgRejectionReason_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            pnlRejReasUpdate.Visible = true; 
            pnlAddRejectionReason.Visible = false;
            pnlAddCategory.Visible = false;
            pnlUpdateRejCategory.Visible = false;
                        
            dgRejectionReason.SelectedIndex = e.Item.ItemIndex;
            //if (dgRejectionReason.Items.Count > -1)
            if (dgRejectionReason.SelectedIndex > -1)
            {
                int editRejIntNo = Convert.ToInt32(dgRejectionReason.DataKeys[dgRejectionReason.SelectedIndex]);
                ViewState["editRejIntNo"] = editRejIntNo;
                Session["prevPage"] = thisPageURL;
                ShowRejection(editRejIntNo);

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editRejIntNo.ToString(), "RejectionLookup");
                this.ucLanguageLookupUpdate1.DataBind(entityList);

            }

        }
        //// Rejection Category
        //protected void dgRejCategory_EditCommand(object source, DataGridCommandEventArgs e)
        //{
        //    //store details of page in case of re-direct
            
        //    pnlAddCategory.Visible = false;
        //    pnlRejReasUpdate.Visible = false;
        //    dgRejCategory.SelectedIndex = e.Item.ItemIndex;

        //    pnlUpdateRejCategory.Visible = true;
            
        //    if (dgRejCategory.SelectedIndex > -1)
        //    {
        //        int editRejCIntNo = Convert.ToInt32(dgRejCategory.DataKeys[dgRejCategory.SelectedIndex]);

        //        ViewState["editRejCIntNo"] = editRejCIntNo;
        //        Session["prevPage"] = thisPageURL;

        //        ShowRejectionCatDetails(editRejCIntNo);
        //    }
        //}

        protected void btnUpdateRejection_Click(object sender, EventArgs e)
        {
            // update rejection reason
            int rejIntNo = Convert.ToInt32(ViewState["editRejIntNo"]);

            //int selectedItem = dgRejCategory.SelectedIndex; 
            //mrs 20081126 this must come from the update selection
            int rejCIntNo = Convert.ToInt32(ddlUpdateRejectionCategory.SelectedValue);
            string rejSkip = "N";
            string natisBSkip = "N";

            if (chkUpdateCSkip.Checked) 
                rejSkip = "Y";

            if (chkupdateNatisBSkip.Checked) 
                natisBSkip = "Y";

            // add the new transaction to the database table
            Stalberg.TMS.RejectionDB conUpdate = new RejectionDB(connectionString);

            //mrs 20081126 added FK to rejectionCategory
            int updRejIntNo = conUpdate.UpdateRejection(rejIntNo, txtUpdateRejection.Text, rejSkip, loginUser, rejCIntNo, natisBSkip);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate1.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(updRejIntNo, lgEntityList, "RejectionLookup", "RejIntNo", "RejReason", this.loginUser);
           

            if (updRejIntNo <= 0)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
            }
            else
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text15");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RejectionReasons, PunchAction.Change); 
            }

            lblError.Visible = true;

            // mrs 20081128 hide edit panel on completion
            pnlUpdateRejCategory.Visible = false;
            pnlRejReasUpdate.Visible = false;

            dgRejectionReasonBindGrid(rejCIntNo);
            
        }
        //protected void dgRejectionReason_EditCommand(object source, DataGridCommandEventArgs e)
        //{
        //    {
        //        //store details of page in case of re-direct

        //        pnlAddCategory.Visible = false;
        //        pnlAddRejectionReason.Visible = false;
        //        pnlRejReasUpdate.Visible = true;
        //        pnlUpdateRejCategory.Visible = false;

        //        dgRejectionReason.Visible = true;
        //        dgRejCategory.SelectedIndex = e.Item.ItemIndex;
        //        if (dgRejectionReason.SelectedIndex > -1)
        //        {
        //            int editRejIntNo = Convert.ToInt32(dgRejectionReason.DataKeys[dgRejectionReason.SelectedIndex]);
        //            ViewState["editRejIntNo"] = editRejIntNo;
        //            ViewState["prevPage"] = thisPageURL;
        //            ShowRejection(editRejIntNo);
        //        }
        //    }
        //}

        protected void btnDeleteRejection_Click(object sender, EventArgs e)
        {
            int rejIntNo = Convert.ToInt32(ViewState["editRejIntNo"]);

            RejectionDB contractor = new Stalberg.TMS.RejectionDB(connectionString);

            int delRejIntNo = contractor.DeleteRejection(rejIntNo);

            //Jerry 2012-07-03 add ToString()
            if (delRejIntNo.ToString().Equals("0"))
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            }
            else if (delRejIntNo.ToString().Equals("-1"))
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
            }
            else
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                //2013-12-18 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RejectionReasons, PunchAction.Delete); 
            }

            lblError.Visible = true;

            pnlRejReasUpdate.Visible = false;

            dgCategoryBindGrid();
            int editRejCIntNo = Convert.ToInt32(dgRejCategory.DataKeys[dgRejCategory.SelectedIndex]);
            dgRejectionReasonBindGrid(editRejCIntNo);
        }

        // rejection details
        protected void ShowRejection(int editRejIntNo)
        {
            // show the rejection reason
            Stalberg.TMS.RejectionDB rejection = new Stalberg.TMS.RejectionDB(connectionString);

            Stalberg.TMS.RejectionDetails rejDetails = rejection.GetRejectionDetails(editRejIntNo);
            
            //Dictionary<int, string> lookups =
            //   RejectionLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            //if (lookups.ContainsKey(editRejIntNo))
            //    {
            //        txtUpdateRejection.Text = lookups[editRejIntNo];
            //    }


            txtUpdateRejection.Text = rejDetails.RejReason;
            chkUpdateCSkip.Checked = rejDetails.RejSkip.Equals("Y") ? true : false;
            chkupdateNatisBSkip.Checked = rejDetails.NatisBasketSkip.Equals("Y") ? true : false;

            ddlUpdateRejectionCategory.SelectedIndex = ddlUpdateRejectionCategory.Items.IndexOf(ddlUpdateRejectionCategory.Items.FindByValue(rejDetails.RejCIntNo.ToString()));
            
            pnlRejReasUpdate.Visible = true;
            btnOptDeleteReason.Visible = true;
        }

        // mrs 20081126 need a ddl on the update panel
        protected void PopulateRejectionCategoryListForUpdate()
        {
            RejectionDB rejCat = new RejectionDB(connectionString);
            SqlDataReader reader = rejCat.GetRejectionCategoryList();

            Dictionary<int, string> lookups =
                RejectCategoryLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int rejCIntNo = (int)reader["RejCIntNo"];
                if (lookups.ContainsKey(rejCIntNo))
                {
                    ddlUpdateRejectionCategory.Items.Add(new ListItem(lookups[rejCIntNo], rejCIntNo.ToString()));
                }
            }
            //ddlUpdateRejectionCategory.DataSource = reader;
            //ddlUpdateRejectionCategory.DataValueField = "RejCIntNo";
            //ddlUpdateRejectionCategory.DataTextField = "RejCCategory";
            //ddlUpdateRejectionCategory.DataBind();
            reader.Close();

        }
        protected void ddlUpdateRejectionCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            // mrs 20081126 added
            int editRejCIntNo = Convert.ToInt32(ddlUpdateRejectionCategory.SelectedValue.ToString());

            ViewState["editRejCIntNo"] = editRejCIntNo;

        }
        protected void dgRejectionReason_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            //this.GetNoticesForPostage(e.NewPageIndex);

            dgRejectionReason.CurrentPageIndex = e.NewPageIndex;

            int editRejCIntNo = Convert.ToInt32(ViewState["editRejCIntNo"]);
            dgRejectionReasonBindGrid(editRejCIntNo);
        }

}
}