using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.IO;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the page that creates the Admission of Guilt report
    /// </summary>
    public partial class AGListViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString;
        private string login;
        private int autIntNo;
        //protected string thisPage = "Spotfine Batch / AG List Viewer";
        protected string thisPageURL = "AGListViewer.aspx";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 

            this.autIntNo = int.Parse(Request.QueryString["rAutIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if(Request.QueryString["rReceiptStartDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error,(string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            DateTime dtReceiptStartDate = DateTime.Parse(Request.QueryString["rReceiptStartDate"]);

            if(Request.QueryString["rReceiptEndDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error1");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error,(string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            DateTime dtReceiptEndDate = DateTime.Parse(Request.QueryString["rReceiptEndDate"]);

            int CrtIntNo = int.Parse(Request.QueryString["CrtIntNo"]);
            int CrtRIntNo = int.Parse(Request.QueryString["CrtRIntNo"]);
            string SortOrder = Request.QueryString["SortOrder"];
            string AGorSpot = Request.QueryString["AGorSpot"];

            // Setup the report - BD need to check if we want a Spot Fine or AG report
            string reportName = string.Empty;
            string sTemplate = string.Empty;
 
            if (AGorSpot == "S")
            {
                reportName = "SpotFineList";
                AuthReportNameDB db = new AuthReportNameDB(this.connectionString);
                reportName = db.GetAuthReportName(this.autIntNo, reportName);
                if (reportName.Equals(string.Empty))
                {
                    db.AddAuthReportName(autIntNo, "SpotFineList.dplx", "GetAGList", "System", "");
                    reportName = "SpotFineList.dplx";
                    sTemplate = db.GetAuthReportNameTemplate(this.autIntNo, "SpotFineList");
                }
            }
            else
            {
                reportName = "AGList";
                AuthReportNameDB db = new AuthReportNameDB(this.connectionString);
                reportName = db.GetAuthReportName(this.autIntNo, reportName);
                if (reportName.Equals(string.Empty))
                {
                    db.AddAuthReportName(autIntNo, "AGList.dplx", "GetAGList", "System", "");
                    reportName = "AGList.dplx";
                    sTemplate = db.GetAuthReportNameTemplate(this.autIntNo, "AGLIst");
                }
            }

            string reportPath = Server.MapPath("Reports/" + reportName);

            //****************************************************

            string templatePath = string.Empty;

            //SD:  20081120 - check that report actually exists
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error2"), reportName);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error,(string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                //dls 081117 - we can only check that the template path exists if there is actually a template!
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error3"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error,(string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            List<Int32> courtIntList = new List<Int32>();
            CourtDB courtDB = new CourtDB(this.connectionString);
            SqlDataReader reader = courtDB.GetAuth_CourtListByAuth(this.autIntNo);
            while (reader.Read())
            {
                if (reader["CrtNo"].ToString() == "000000")
                    continue;

                courtIntList.Add(Convert.ToInt32(reader["CrtIntNo"]));
            }
            reader.Dispose();

            DocumentLayout document = new DocumentLayout(reportPath);
            StoredProcedureQuery query = (StoredProcedureQuery)document.GetQueryById("Query");
            query.ConnectionString = this.connectionString;

            ParameterDictionary parameters = new ParameterDictionary( );

            FormattedRecordArea printDate = (FormattedRecordArea)document.GetElementById("printDate");
            printDate.LaidOut += new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);
            Document report = new Document();

            if (CrtIntNo == 0)
            {
                foreach (int crtIntNo in courtIntList)
                {
                    parameters = new ParameterDictionary();
                    parameters.Add("ReceiptStartDate", dtReceiptStartDate);
                    parameters.Add("ReceiptEndDate", dtReceiptEndDate);
                    parameters.Add("AutIntNo", autIntNo);
                    parameters.Add("CrtIntNo", crtIntNo);
                    if (AGorSpot == "A")
                        parameters.Add("CrtRIntNo", CrtRIntNo);
                    parameters.Add("SortOrder", SortOrder);
                    parameters.Add("AGorSpot", AGorSpot);

                    Document tempReport = document.Run(parameters);
                    byte[] bufferSummons = tempReport.Draw();
                    //if the AGList.dlpx changed, the bufferSummons.Length also need to be changed
                    if (AGorSpot == "A" && bufferSummons.Length <= 1493)
                        continue;
                    if (AGorSpot == "S" && bufferSummons.Length <= 1432)
                        continue;

                    foreach (ceTe.DynamicPDF.Page page in tempReport.Pages)
                    {
                        report.Pages.Add(page);
                    }
                }
            }
            else
            {
                parameters.Add("ReceiptStartDate", dtReceiptStartDate);
                parameters.Add("ReceiptEndDate", dtReceiptEndDate);
                parameters.Add("AutIntNo", autIntNo);
                parameters.Add("CrtIntNo", CrtIntNo);
                if (AGorSpot == "A")
                    parameters.Add("CrtRIntNo", CrtRIntNo);
                parameters.Add("SortOrder", SortOrder);
                parameters.Add("AGorSpot", AGorSpot);

                Document tempReport = document.Run(parameters);
                byte[] bufferSummons = tempReport.Draw();

                //if the AGList.dlpx changed, the bufferSummons.Length also need to be changed
                if (AGorSpot == "A" && bufferSummons.Length > 1493)
                {
                    foreach (ceTe.DynamicPDF.Page page in tempReport.Pages)
                    {
                        report.Pages.Add(page);
                    }
                }

                if (AGorSpot == "S" && bufferSummons.Length > 1432)
                {
                    foreach (ceTe.DynamicPDF.Page page in tempReport.Pages)
                    {
                        report.Pages.Add(page);
                    }
                }
            }

            //Jerry 2014-11-06 add for the WOA AGListReport
            if (AGorSpot == "A")
            {
                //Get AGList _WOA Report
                reportPath = Server.MapPath("Reports/AGList_WOA.dplx");
                if (!File.Exists(reportPath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error2"), reportName);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }

                document = new DocumentLayout(reportPath);
                query = (StoredProcedureQuery)document.GetQueryById("Query");
                query.ConnectionString = this.connectionString;
                FormattedRecordArea woaPrintDate = (FormattedRecordArea)document.GetElementById("printDate");
                woaPrintDate.LaidOut += new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);

                if (CrtIntNo == 0)
                {
                    foreach (int crtIntNo in courtIntList)
                    {
                        parameters = new ParameterDictionary();
                        parameters.Add("ReceiptStartDate", dtReceiptStartDate);
                        parameters.Add("ReceiptEndDate", dtReceiptEndDate);
                        parameters.Add("AutIntNo", autIntNo);
                        parameters.Add("CrtIntNo", crtIntNo);
                        parameters.Add("CrtRIntNo", CrtRIntNo);
                        parameters.Add("SortOrder", SortOrder);
                        parameters.Add("AGorSpot", AGorSpot);

                        Document reportWOA = document.Run(parameters);
                        byte[] bufferWOA = reportWOA.Draw();
                        //if the AGList_WOA.dlpx changed, the bufferSummons.Length also need to be changed
                        if (bufferWOA.Length > 1508)
                        {
                            foreach (ceTe.DynamicPDF.Page page in reportWOA.Pages)
                            {
                                report.Pages.Add(page);
                            }
                        }
                    }
                }
                else
                {
                    parameters = new ParameterDictionary();
                    parameters.Add("ReceiptStartDate", dtReceiptStartDate);
                    parameters.Add("ReceiptEndDate", dtReceiptEndDate);
                    parameters.Add("AutIntNo", autIntNo);
                    parameters.Add("CrtIntNo", CrtIntNo);
                    parameters.Add("CrtRIntNo", CrtRIntNo);
                    parameters.Add("SortOrder", SortOrder);
                    parameters.Add("AGorSpot", AGorSpot);

                    Document reportWOA = document.Run(parameters);
                    byte[] bufferWOA = reportWOA.Draw();
                    if (bufferWOA.Length > 1508)
                    {
                        foreach (ceTe.DynamicPDF.Page page in reportWOA.Pages)
                        {
                            report.Pages.Add(page);
                        }
                    }
                }

                woaPrintDate.LaidOut -= new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);
            }

            Response.ClearContent();
            Response.ClearHeaders();
            if (report.Pages.Count > 0)
            {
                Response.ContentType = "application/pdf";
                byte[] buffer = report.Draw();
                Response.BinaryWrite(buffer);
            }
            else
            {
                Response.ContentType = "text/html";
                Response.Write("No data!");
            }
            printDate.LaidOut -= new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);
           
            
            Response.End();
        }

        private void printDate_LaidOut(object sender, FormattedRecordAreaLaidOutEventArgs e)
        {
            e.FormattedTextArea.Text = (string)GetLocalResourceObject("FormattedTextArea.Text") + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        }

    }
}
