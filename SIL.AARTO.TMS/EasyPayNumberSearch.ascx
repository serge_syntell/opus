<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.EasyPayNumberSearch" Codebehind="EasyPayNumberSearch.ascx.cs" %>
<table>
    <tr>
        
        <td>
            <asp:TextBox ID="txtNumber" runat="server" Width="212px" CssClass="Normal" MaxLength="20"></asp:TextBox>
        </td>
      
        <td>
            <asp:Button ID="btnSearch" runat="server" Width="80px" Text="<%$Resources:btnSearch.Text %>" CssClass="NormalButton"
                OnClick="btnSearch_Click"></asp:Button>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td colspan="3">
            <asp:Panel ID="pnlGrid" runat="server" Width="100%" Visible="true">
                <asp:GridView ID="grdHeader" runat="server" AutoGenerateColumns="False" CellPadding="3"
                    CssClass="Normal" GridLines="Horizontal" ShowFooter="True" OnSelectedIndexChanging="grdHeader_SelectedIndexChanging">
                    <FooterStyle CssClass="CartListHead" />
                    <Columns>
                        <asp:BoundField DataField="NotTicketNo" HeaderText="<%$Resources:grdHeader.HeaderText %>" />
                        <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:grdHeader.HeaderText1 %>" />
                        <asp:BoundField DataField="Name" HeaderText="<%$Resources:grdHeader.HeaderText2 %>" />
                        <asp:CommandField HeaderText="<%$Resources:grdHeader.HeaderText3 %>" ShowSelectButton="true">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:CommandField>
                    </Columns>
                    <HeaderStyle CssClass="CartListHead" />
                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                </asp:GridView>
            </asp:Panel>
            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>
        </td>
    </tr>
</table>
