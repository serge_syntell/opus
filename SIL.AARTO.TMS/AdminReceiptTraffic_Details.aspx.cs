using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Drawing;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Contains all the logic for collecting payments for traffic fines
    /// </summary>
    public partial class AdminReceiptTraffic_Details : System.Web.UI.Page
    {
        // Fields    
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private bool requireAddresses = false;
        private bool allowOnTheFlyRepresentations = false;
        private bool hasRepresentation = false;
        private bool mandatoryPhoneNo = true;
        private bool mandatoryReceivedFrom = true;
        private decimal currentFineAmount = 0;
        private int minStatus = 255;
        private int trhIntNo = 0;
        private int maxStatus = 900;

        private DateTime trhTranDate = DateTime.MinValue;
        private int noDaysForLastPaymentDate = 0;

        private DataSet dsReceiptPayments;
        private Cashier cashier = null;
        private CashType cashType = CashType.Cash;
        private PersonaType personaType = PersonaType.Driver;
        private List<NoticeForPayment> notices = null;
        private CashReceiptDB cashReceipt;
        private bool isOneReceiptPerNotice;
        private bool isDisplayByID;
        private bool isSelectAllNotices;
        private bool usePostalReceipt = false;
        private int adminRctIntNo = 0;
        private decimal adminRctAmount = 0;
        private string adminRctNumber = string.Empty;
        
        //Barry Dickson 20080408 adding in authority rules
        private bool CashAfterSummons;
        private bool CashAfterCourtDate;
        private bool CashAfterWarrant;

        //BD 20080624 adding in grace period rule
        private bool CashAfterGracePeriod;
        //dls 081223 - add rule to allow cash receipt after case no
        private bool CashAfterCaseNumber;

        protected string styleSheet;
        protected string backgroundImage;
        protected double myTotal = 0;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "AdminReceiptTraffic_Details.aspx";
        //protected string thisPage = "Re-apply Admin Reversed Payments";

        // Constants
        private const string CURRENCY_FORMAT = "R ###0.00";
        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm";
        private const string CHECKED_IMAGE = "images/checked.gif";
        private const string UNCHECKED_IMAGE = "images/unchecked.gif";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;

            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
           
            // May need to check user access level here....
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.GetAuthRuleSettings();

            cashReceipt = new CashReceiptDB(this.connectionString);

            GetSettingsFromViewState();           

            if (!this.Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                //hide representation option
                this.chkCashierRep.Visible = false;
                this.lblRepresentations.Visible = false;
                this.btnContinue.Visible = false;
                this.btnBackFromRepresentations.Visible = false;

                this.pnlAddress.Visible = false;

                string id = string.Empty;
                if (Request.QueryString["IDNo"] != null)
                    id = Request.QueryString["IDNo"].ToString().Trim();

                string ticket = string.Empty;
                if (Request.QueryString["TicketNo"] != null)
                    ticket = Request.QueryString["TicketNo"].ToString().Replace(" ", "");

                if (id.Length == 0 && ticket.Length == 0)
                {
                        Response.Redirect("AdminReceiptTraffic.aspx");
                }

                // Populate the main grid
                this.ShowNoticeSearchResults(id, ticket);

            }
            else if (this.dgTicketDetails.Items.Count > 0)
            {
                // Set the items in the grid from the notice list
                int notIntNo = 0;
                foreach (DataGridItem item in this.dgTicketDetails.Items)
                {
                    ImageButton check = null;
                    notIntNo = int.Parse(item.Cells[0].Text);
                    foreach (NoticeForPayment notice in this.notices)
                    {
                        if (notice.NotIntNo == notIntNo)
                        {
                            check = (ImageButton)item.FindControl("igPayNow");
                            check.ImageUrl = notice.IsSelected ? CHECKED_IMAGE : UNCHECKED_IMAGE;
                            break;
                        }
                    }
                }

                decimal totalReceived = adminRctAmount;
                CalculateTotal(false, ref totalReceived);
            }
        }

        private DataSet PopulateSeniorOfficers()
        {
            DataSet ds;

            if (Session["SeniorOfficers"] == null)
            {
                TrafficOfficerDB db = new TrafficOfficerDB(this.connectionString); 
                ds = db.GetTrafficOfficerListByAuthorityDS(this.autIntNo, OfficerSeniority.Senior);

                Session["SeniorOfficers"] = ds;
            }
            else
            {
                ds = (DataSet)Session["SeniorOfficers"];
            }
            return ds;
        }


        private void RefreshNoticeGrid()
        {
            ShowHideColumns(this.hasRepresentation);

            // Set the items in the grid from the notice list
            int notIntNo = 0;
            decimal fullAmount = 0;

            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                DropDownList ddlSeniorOfficer = null;
                TextBox txtAmountToPay = null;

                notIntNo = int.Parse(item.Cells[0].Text);
                fullAmount = this.ParseValue(item.Cells[7].Text.Trim());

                foreach (NoticeForPayment notice in this.notices)
                {
                    if (notice.NotIntNo == notIntNo)
                    {
                        if (this.hasRepresentation)
                        {
                            item.Visible = notice.IsSelected;

                            if (notice.IsSelected)
                            {
                                DataSet ds = PopulateSeniorOfficers();

                                ddlSeniorOfficer = (DropDownList)item.FindControl("ddlSeniorOfficer");
                                txtAmountToPay = (TextBox)item.FindControl("txtAmountToPay");

                                if (ddlSeniorOfficer != null)
                                {
                                    ddlSeniorOfficer.DataSource = ds;
                                    ddlSeniorOfficer.DataTextField = "TODescr";
                                    ddlSeniorOfficer.DataBind();

                                    ddlSeniorOfficer.Items.Insert(0, (string)GetLocalResourceObject("ddlSeniorOfficer.Items"));

                                    if (!notice.SeniorOfficer.Equals(string.Empty))
                                        ddlSeniorOfficer.Text = notice.SeniorOfficer;
                                }

                                if (notice.AmountToPay == 0)
                                    notice.AmountToPay = fullAmount;

                                //update by Rachel 20140811 for 5337
                                //if (txtAmountToPay != null)
                                //    txtAmountToPay.Text = string.Format("{0:0.00}", notice.AmountToPay);

                                if (txtAmountToPay != null)
                                    txtAmountToPay.Text = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", notice.AmountToPay);
                                //end update by Rachel 20140811 for 5337

                                txtAmountToPay.Enabled = !notice.HasJudgement;
                                ddlSeniorOfficer.Enabled = !notice.HasJudgement;
                            }
                        }
                        else
                            item.Visible = true;

                        break;
                    }
                }
            }
        }

        private void ShowHideColumns(bool show)
        {          
            foreach (DataGridColumn col in dgTicketDetails.Columns)
            {
                switch (col.HeaderText)
                {
                    case "New Fine Amount":
                    case "Senior Officer":
                    case "":
                        col.Visible = show;
                        break;
                    default:
                        break;
                }
            }
        }

        #region Settings

        private void GetAuthRuleSettings()
        {
            //use this for all authority rules
            //authRules = new AuthorityRulesDB(connectionString);

            // Check minimum payable status
            this.CheckMinimumStatusRule();

            // Check maximum payable status
            this.CheckMaximumStatusRule();

            // Check the date rule for the last payment date
            this.CheckLastPaymentDate();

            // Check authority rule for returning all tickets for the owner/driver/proxy of the vehicle
            this.CheckIDRule();

            this.CheckSelectAllRule();

            // Updated LMZ 2007-03-06: check the rule to make this optional
            this.CheckMandatoryPhoneNo();

            // Updated LMZ 2007-09-13: check the rule to make this optional
            this.CheckMandatoryReceivedFrom();

            this.CheckAllOnTheFlyReps();

            this.CheckOneReceiptPerNoticeRule();

            //Barry Dickson 20080408 new rules for cashier
            this.CheckCashAfterCourtDateRule();
            this.CheckCashAfterSummonsServedRule();
            this.CheckCashAfterWarrantRule();

            //BD 20080624 new rule for grace period
            this.CheckCashAfterGracePeriodRule();

            //dls 081223 - new rule for cash afer case no
            this.CheckCashAfterCaseNumberRule();
        }

        //DLS: 20081223:- new rules to check when allowed to receipt cash after CaseNumber has generated
        private void CheckCashAfterCaseNumberRule()
        {
            if (this.ViewState["AcceptCashAfterCaseNumber"] != null)
                this.CashAfterCaseNumber = (bool)ViewState["AcceptCashAfterCaseNumber"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "6011";
                //rule.ARComment = "N=No (Default); Y=Yes;";
                //rule.ARDescr = "Allow receipts after the case number has been generated (court roll)";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.ARCode = "6011";
                rule.AutIntNo = this.autIntNo;
                rule.LastUser = this.login;

                DefaultAuthRules ar = new DefaultAuthRules(rule, this.connectionString);

                KeyValuePair<int, string> cashAfterCase = ar.SetDefaultAuthRule();

                //this.CashAfterGracePeriod = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.CashAfterCaseNumber = cashAfterCase.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("AcceptCashAfterCaseNumber", this.CashAfterCaseNumber);
            }
        }

        //Barry Dickson 20080408 - new rules to check when allowed to receipt cash
        private void CheckCashAfterSummonsServedRule()
        {
            if (this.ViewState["AcceptCashAfterSummons"] != null)
                this.CashAfterSummons = (bool)ViewState["AcceptCashAfterSummons"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4710";
                //rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash once the court date has passed.";
                //rule.ARDescr = "Rule to check if allowed to accept cash for notices where the court date has passed.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                //AutIntNo, ARCode and LastUser need to be set from here
                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4710";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                //this.CashAfterSummons = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.CashAfterSummons = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                this.ViewState.Add("AcceptCashAfterSummons", this.CashAfterSummons);
            }
        }
        //Barry Dickson 20080408 - new rules to check when allowed to receipt cash
        private void CheckCashAfterWarrantRule()
        {
            if (this.ViewState["AcceptCashAfterWarrant"] != null)
                this.CashAfterWarrant = (bool)ViewState["AcceptCashAfterWarrant"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4720";
                //rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash after warrant issued";
                //rule.ARDescr = "Rule to check if allowed to accept cash for notices where a warrant has been issued.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";
                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                //AutIntNo, ARCode and LastUser need to be set from here
                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4720";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                //this.CashAfterWarrant = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.CashAfterWarrant = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                this.ViewState.Add("AcceptCashAfterWarrant", this.CashAfterWarrant);
            }
        }
        //Barry Dickson 20080408 - new rules to check when allowed to receipt cash
        private void CheckCashAfterCourtDateRule()
        {
            if (this.ViewState["AcceptCashAfterCourtDate"] != null)
                this.CashAfterCourtDate = (bool)ViewState["AcceptCashAfterCourtDate"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4730";
                //rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash once the court date has passed.";
                //rule.ARDescr = "Rule to check if allowed to accept cash for notices where the court date has passed.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4730";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                //this.CashAfterCourtDate = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.CashAfterCourtDate = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                this.ViewState.Add("AcceptCashAfterCourtDate", this.CashAfterCourtDate);
            }
        }
        //BD 20080624 - new rules to check when allowed to receipt cash after grace period
        private void CheckCashAfterGracePeriodRule()
        {
            if (this.ViewState["AcceptCashAfterGracePeriod"] != null)
                this.CashAfterGracePeriod = (bool)ViewState["AcceptCashAfterGracePeriod"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4740";
                //rule.ARComment = "N=No (Default); Y=Yes; If allowed to accept cash after the grace period has expired.";
                //rule.ARDescr = "Rule to check if allowed to accept cash for notices where the grace period has expired.";
                //rule.ARString = "Y";
                //rule.ARNumeric = 1;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4740";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                //this.CashAfterGracePeriod = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.CashAfterGracePeriod = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                this.ViewState.Add("AcceptCashAfterGracePeriod", this.CashAfterGracePeriod);
            }
        }

        private void CheckMaximumStatusRule()
        {
            if (this.ViewState["MaxStatus"] != null)
                this.maxStatus = (int)ViewState["MaxStatus"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4570";
                //rule.ARComment = "900 (Default); See ChargeStatus table for allowable values (status must be 255 or greater)";
                //rule.ARDescr = "Rule to set maximum charge status at which payment/representations are allowed";
                //rule.ARNumeric = 900;
                //rule.ARString = string.Empty;
                //rule.AutIntNo = this.autIntNo;
                //rule.LastUser = this.login;

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                //this.maxStatus = rule.ARNumeric;

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4570";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this.maxStatus = value.Key;

                this.ViewState.Add("MaxStatus", this.maxStatus);
            }
        }

        private void GetSettingsFromViewState()
        {
            //dls 080221 - re-application of reversed payments via admin receipt reversal
            if (Session["AdminReceiptReversal"] != null)
            {
                pnlApplyReversed.Visible = true;

                adminRctIntNo = Int32.Parse(Session["AdminRctIntNo"].ToString());
                adminRctAmount = decimal.Parse(Session["AdminRctAmount"].ToString());
                adminRctNumber = Session["AdminRctNumber"].ToString();

                //update by Rachel 20140819 for 5337
                //lblReversedPaymentAmount.Text = string.Format((string)GetLocalResourceObject("lblReversedPaymentAmount.Text1"), adminRctNumber, adminRctAmount.ToString(CURRENCY_FORMAT));
                lblReversedPaymentAmount.Text = string.Format((string)GetLocalResourceObject("lblReversedPaymentAmount.Text1"), adminRctNumber, adminRctAmount.ToString(CURRENCY_FORMAT,CultureInfo.InvariantCulture));
                //end update by Rachel 20140819 for 5337
            }
            else
            {
                pnlApplyReversed.Visible = false;
            }

            this.cashType = CashType.AdminReceipt;

            this.ViewState.Add("CashType", this.cashType);

            // FBJ Added (2006-11-13): Set the type of cash received
            if (this.ViewState["HasRepresentation"] != null)
                this.hasRepresentation = (bool)this.ViewState["HasRepresentation"];
            else
                this.ViewState.Add("HasRepresentation", this.hasRepresentation);

            // dls 080122 - get the person receiving the payment
            if (this.ViewState["PersonaType"] != null)
                this.personaType = (PersonaType)this.ViewState["PersonaType"];
            else
                this.ViewState.Add("PersonaType", this.personaType);

            // FBJ Added (2007-01-29): Extract the notices list from the session
            if (this.Session["NoticesToPay"] == null)
                Response.Redirect("AdminReceiptTraffic.aspx");

            this.notices = (List<NoticeForPayment>)this.Session["NoticesToPay"];

            //dls 080125 - get the data from the session state if it has been created
            this.GetReceiptTranSettingsFromSession();

            if (this.ViewState["currentFineAmount"] != null)
                this.currentFineAmount = (decimal)this.ViewState["currentFineAmount"];
        }

        private void CheckMandatoryReceivedFrom()
        {
            if (this.ViewState["mandatoryReceivedFrom"] == null)
            {
                //ard = authRules.GetAuthorityRulesDetailsByCode(this.autIntNo, "4521", "Rule to make the receipt received from field optional", 0, "N", "Y = Yes; N= No(default) mandatory", this.login);
                //if (ard.ARString == "Y")
                //    this.mandatoryReceivedFrom = false;

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4521";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                if (value.Value == "Y")
                    this.mandatoryReceivedFrom = false;

                this.ViewState["mandatoryReceivedFrom"] = this.mandatoryReceivedFrom;
            }
            else
                this.mandatoryReceivedFrom = (bool)this.ViewState["mandatoryReceivedFrom"];

        }

        private void CheckMandatoryPhoneNo()
        {
            if (this.ViewState["mandatoryPhoneNo"] == null)
            {
                //ard = authRules.GetAuthorityRulesDetailsByCode(this.autIntNo, "4520", "Rule to make the receipt telephone number optional", 0, "N", "Y = Yes; N= No(default) mandatory", this.login);
                //if (ard.ARString == "Y")
                //    this.mandatoryPhoneNo = false;

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4520";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                if (value.Value == "Y")
                    this.mandatoryPhoneNo = false;

                this.ViewState["mandatoryPhoneNo"] = this.mandatoryPhoneNo;
            }
            else
                this.mandatoryPhoneNo = (bool)this.ViewState["mandatoryPhoneNo"];

        }

        private void CheckUsePostalReceipt()
        {
            // LMZ Added (2007-03-02): To check for Authority Rule - print all receipts to Postal Receipt
            if (this.ViewState["UsePostalReceipt"] == null)
            {          
                //ard = authRules.GetAuthorityRulesDetailsByCode(this.autIntNo, "4510", "Rule to indicate using of postal receipt for all receipts", 0, "N", "Y = Yes; N= No(default)", this.login);
                //this.usePostalReceipt = ard.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ? true : false;

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4510";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this.usePostalReceipt = value.Value.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ? true : false;

                this.ViewState["UsePostalReceipt"] = this.usePostalReceipt;
            }
            else
                this.usePostalReceipt = (bool)this.ViewState["UsePostalReceipt"];

        }

        private void CheckAllOnTheFlyReps()
        {
            // FBJ: Enable/disable based on the Authority rule for making representations on the fly.
            if (this.ViewState["AllowOnTheFlyRepresentations"] == null)
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4600";
                //rule.ARDescr = "Rule to Allow On-The-Fly Representations in cash receipting.";
                //rule.ARComment = "N = on-the-fly representations are NOT allowed (Default), Y = they are!";
                //rule.ARNumeric = 0;
                //rule.ARString = "N";
                //rule.AutIntNo = this.autIntNo;
                //rule.LastUser = this.login;
                //authRules.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4600";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                //this.allowOnTheFlyRepresentations = rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.allowOnTheFlyRepresentations = value.Value.Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                this.ViewState.Add("AllowOnTheFlyRepresentations", allowOnTheFlyRepresentations);
            }
            else
                this.allowOnTheFlyRepresentations = (bool)this.ViewState["AllowOnTheFlyRepresentations"];

        }

        private void CheckMinimumStatusRule()
        {
            if (this.ViewState["MinStatus"] != null)
                this.minStatus = (int)ViewState["MinStatus"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4605";
                //rule.ARComment = "Default = 255 (posted).";
                //rule.ARDescr = "Status to start showing notices on enquiry screen";
                //rule.ARNumeric = 255;
                //rule.ARString = string.Empty;
                //rule.AutIntNo = this.autIntNo;
                //rule.LastUser = this.login;

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                //this.minStatus = rule.ARNumeric;

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4605";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this.minStatus = value.Key;
                this.ViewState.Add("MinStatus", this.minStatus);
            }
        }

        private void CheckLastPaymentDate()
        {
            if (this.ViewState["NoDaysForLastPaymentDate"] != null)
                this.noDaysForLastPaymentDate = (int)ViewState["NoDaysForLastPaymentDate"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4505";
                //rule.ARComment = "The number of days that will be subtracted from the court date as the cut-off date for payment.";
                //rule.ARDescr = "Rule indicating the number of days before the court date that payments will still be accepted.";
                //rule.ARString = string.Empty;
                //rule.ARNumeric = 1;
                //rule.LastUser = this.login;

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                //this.noDaysForLastPaymentDate = rule.ARNumeric;

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4505";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this.noDaysForLastPaymentDate = value.Key;
                this.ViewState.Add("NoDaysForLastPaymentDate", this.noDaysForLastPaymentDate);
            }
        }

        private void CheckSelectAllRule()
        {
            if (this.ViewState["AllNoticesForPayment"] != null)
                this.isSelectAllNotices = (bool)ViewState["AllNoticesForPayment"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4400";
                //rule.ARComment = "N=No (Default); Y=Yes; default selects only those specifically searched for or previously selected";
                //rule.ARDescr = "Rule to check all notices as ready for payment.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                //this.isSelectAllNotices = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4400";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this.isSelectAllNotices = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                this.ViewState.Add("AllNoticesForPayment", this.isSelectAllNotices);
            }
        }

        private void CheckIDRule()
        {
            if (this.ViewState["DisplayByID"] != null)
                this.isDisplayByID = (bool)ViewState["DisplayByID"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "3300";
                //rule.ARComment = "N = No (Default); Y = Yes";
                //rule.ARDescr = "Rule for whether to display all notices for an ID when processing a payment.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                //this.isDisplayByID = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "3300";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this.isDisplayByID = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                this.ViewState.Add("DisplayByID", this.isDisplayByID);
            }
        }

        private void CheckOneReceiptPerNoticeRule()
        {
            if (ViewState["isOneReceiptPerNotice"] != null)
                isOneReceiptPerNotice = (bool)ViewState["isOneReceiptPerNotice"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.AutIntNo = this.autIntNo;
                //rule.ARCode = "4550";
                //rule.ARComment = "N = No (Default); Y = Yes";
                //rule.ARDescr = "Rule to set one receipt per notice.";
                //rule.ARString = "N";
                //rule.ARNumeric = 0;
                //rule.LastUser = "System";

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                //isOneReceiptPerNotice = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4550";
                rule.LastUser = this.login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                isOneReceiptPerNotice = rule.ARString.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

                this.ViewState.Add("isOneReceiptPerNotice", this.isOneReceiptPerNotice);
            }

        }

        private void GetReceiptTranSettingsFromSession()
        {
            //dls 080207 - need to use the Session state to store these values, as the page is reloaded if new notices are added from CashReceiptTraffic screen
            if (Session["TRHIntNo"] != null)
            {
                trhIntNo = Int32.Parse(Session["TRHIntNo"].ToString());
            }
            else
                AddToPaymentList();

            if (Session["TRHTranDate"] != null)
            {
                trhTranDate = DateTime.Parse(Session["TRHTranDate"].ToString());
            }
        }

    #endregion

        #region Initialise

        #endregion

        protected void AddToPaymentList()
        {
            // Create the receipt object
            ReceiptTransaction receipt = null;

            receipt = new AdminReceipt();
            
            ((AdminReceipt)receipt).Reference = "Re-Apply payment reversed for Receipt #" + adminRctNumber;
            
            receipt.CashType = CashType.AdminReceipt;
            receipt.ReceivedDate = DateTime.Today;

            receipt.Amount = adminRctAmount;
            receipt.Cashier = this.cashier;

            receipt.ContemptAmount = 0;

            UpdateReceipts(receipt, false);
        }

        /// <summary>
        /// Shows the notice search results.
        /// </summary>
        /// <param name="id">The ID Number.</param>
        /// <param name="ticket">The ticket number.</param>
        private void ShowNoticeSearchResults(string id, string ticket)
        {
            StringBuilder sb = new StringBuilder();

            //dls 070712 - added autIntNo to proc so that Auth-rule can be checked and only notices for single authority can be processed
            NoticeForPaymentDB notice = new NoticeForPaymentDB(connectionString);
            DataSet ds = notice.GetNoticesById(id, ticket, this.notices, isDisplayByID, noDaysForLastPaymentDate, this.minStatus, this.CashAfterCourtDate, this.CashAfterSummons, this.CashAfterWarrant, this.CashAfterGracePeriod, this.CashAfterCaseNumber, this.maxStatus, string.Empty);

            //dls 100202 - AUthRule removed from SP
            //dls 070713 - we now check the AuthRule inside the proc, so there may be two tables. We want to use the 2nd one in this case
            int tableID = 0;

            if (ds.Tables.Count > 1)
                tableID = 1;

            if (ds.Tables[tableID].Rows.Count > 0)
            {
                // Add the results to the notice list
                int notIntNo = 0;
                bool isChecked = false;
                string noticeNo = string.Empty;
                bool isThisNoticePresent = ticket.Length == 0 ? true : false;

                //foreach (DataRow row in ds.Tables[0].Rows)
                foreach (DataRow row in ds.Tables[tableID].Rows)
                {
                    notIntNo = (int)row["NoticeIntNo"];
                    isChecked = (bool)row["IsChecked"];
                    noticeNo = (string)row["TicketNo"];

                    NoticeForPayment nfp = new NoticeForPayment(notIntNo, isChecked);

                    if (noticeNo.Equals(ticket, StringComparison.InvariantCultureIgnoreCase))
                    {
                        isThisNoticePresent = true;
                        nfp.IsSelected = true;
                        nfp.HasJudgement = (bool)row["HasJudgement"].ToString().Equals("Y") ? true : false;
                    }
         
                    if (!this.notices.Contains(nfp))
                        this.notices.Add(nfp);

                }

                // There are tickets to pay
                if (isThisNoticePresent)
                {
                    if (isSelectAllNotices)
                    {
                        foreach (NoticeForPayment n in this.notices)
                            n.IsSelected = true;
                    }

                    this.panelDetails.Visible = true;
                    this.lblError.Visible = false;
                    this.dgTicketDetails.DataSource = ds.Tables[tableID];
                    this.dgTicketDetails.DataKeyField = "NoticeIntNo";
                    this.dgTicketDetails.DataBind();

                    ShowHideColumns(false);

                    string name = ds.Tables[tableID].Rows[0]["Name"].ToString();
                    this.txtReceivedFrom.Text = name;

                    // dls 08012 - they want to be able to toggle between owner, driver & proxy addresses
                    int selectedNotIntNo = 0;

                    //find the first notice that is selected
                    foreach (NoticeForPayment n in this.notices)
                    {
                        if (n.IsSelected)
                        {
                            selectedNotIntNo = n.NotIntNo;
                            break;
                        }
                    }

                    if (selectedNotIntNo > 0)
                        GetAddressDetails(selectedNotIntNo, true);

                    //dls 080122 - show the address all the time
                    this.pnlAddress.Visible = true;

                    decimal totalReceived = adminRctAmount;
                    this.CalculateTotal(true, ref totalReceived);

                    //need to reset the session object here so that it remembers which ones were selected
                    this.Session["NoticesToPay"] = this.notices;

                    return;
                }
            }

            // There's nothing outstanding
            this.lblError.Visible = true;
            this.panelDetails.Visible = false;
            sb.Length = 0;
            string sReason = string.Empty;

            try
            {
                //LMZ 09-03-2007 - added this to give back Charge status description 
                ChargeStatusDB cdb = new ChargeStatusDB(this.connectionString);
                //sReason = cdb.GetChargeStatusById(ticket, id);
                sReason = cdb.GetChargeStatusById(ticket, id, this.CashAfterCourtDate, this.CashAfterSummons, this.CashAfterWarrant, this.CashAfterGracePeriod, this.CashAfterCaseNumber);
            }
            catch { }

            if (id.Length > 0)
                sb.Append(string.Format((string)GetLocalResourceObject("CashReceiptError"), id, sReason));
            if (ticket.Length > 0)
                sb.Append(string.Format((string)GetLocalResourceObject("CashReceiptError1"), ticket, sReason));

            this.Session.Add("CashReceiptError", sb.ToString());
            Response.Redirect("AdminReceiptTraffic.aspx");
        }

        private bool VaidateComtemptData(StringBuilder sb)
        {
            bool response = true;

            if (CheckContemptDiff() < 0)
            {
                response = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("CashReceiptError2") + "</li>\n");
            }

            return response;

        }

        //Barry Dickson 20080410 Need to check that the amount they are paying is greater than the total contempt amt.
        private decimal CheckContemptDiff()
        {
            decimal totalContempt = 0;
            decimal totalReceived = 0;
            decimal contemptDiff = 0;
            int i = 0;

            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                ImageButton check = (ImageButton)item.FindControl("igPayNow");
                if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                {
                    string valueContempt = string.Empty;
                    valueContempt = item.Cells[9].Text;
                    totalContempt += this.ParseValue(valueContempt);
                }
                i++;
            }

            if (dsReceiptPayments != null)
            {
                foreach (DataRow dr in dsReceiptPayments.Tables[0].Rows)
                    totalReceived += Decimal.Parse(dr["TRDAmount"].ToString());
            }

            contemptDiff = totalReceived - totalContempt;

            return contemptDiff;

        }

        private void GetAddressDetails(int notIntNo, bool forceChange)
        {
            NoticeForPaymentDB notice = new NoticeForPaymentDB(connectionString);
            DataSet dsAddress;

            //check if there already is an AddressDetails dataset in the Session, if not go back to the database and fetch all addresses for Notice
            if (Session["NoticeAddressDetails"] == null || forceChange)
                dsAddress = notice.GetNoticeAddressDetails(notIntNo);
            else
                dsAddress = (DataSet)Session["NoticeAddressDetails"];

            foreach (DataRow dr in dsAddress.Tables[0].Rows)
            {
                if (dr["Persona"].ToString().Equals(this.personaType.ToString()))
                {
                    this.txtAddress1.Text = dr["Address1"].ToString();
                    this.txtAddress2.Text = dr["Address2"].ToString();
                    this.txtAddress3.Text = dr["Address3"].ToString();
                    this.txtAddress4.Text = dr["Address4"].ToString();
                    this.txtAddress5.Text = dr["Address5"].ToString();
                    this.txtPoCode.Text = dr["PoCode"].ToString();
                    this.txtReceivedFrom.Text = dr["FullName"].ToString();
                    this.txtPhone.Text = dr["CellNo"].ToString().Trim().Equals(string.Empty) ? (dr["WorkNo"].ToString().Trim().Equals(string.Empty) ? dr["HomeNo"].ToString() : dr["WorkNo"].ToString().Trim()) : dr["CellNo"].ToString().Trim();
                    break;
                }
            }

            //store dataset back into Session
            Session.Add("NoticeAddressDetails", dsAddress);

            if (notIntNo > 0)
            {
                //need to re-populate Received from list, depending on whether there is a proxy/accused or not
                
            }
        }

        private decimal CalculateTotal(bool setValues, ref decimal totalReceived)
        {
            return CalculateTotal(setValues, false, ref totalReceived);
        }

        private decimal CalculateTotal(bool setValues, bool calcRep, ref decimal totalReceived)
        {
            decimal totalToPay = 0;
            bool foundAddress = false;

            int i = 0;
            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                ImageButton check = (ImageButton)item.FindControl("igPayNow");
                if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                {
                    string value = string.Empty;

                    if (calcRep)
                        value = ((TextBox)item.FindControl("txtAmountToPay")).Text;
                    else
                        value = item.Cells[7].Text;

                    totalToPay += this.ParseValue(value);

                    item.CssClass = "RowHighlighted";          
                }
                else
                    item.CssClass = ((i % 2) == 0) ? "CartListItemAlt" : "CartListItem";

                int notIntNo = (int)this.dgTicketDetails.DataKeys[item.ItemIndex];
                foreach (NoticeForPayment notice in this.notices)
                {
                    if (notice.Equals(notIntNo))
                    {
                        notice.IsSelected = check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase);

                        if (notice.IsSelected && !foundAddress)
                        {
                            GetAddressDetails(notice.NotIntNo, true);
                            foundAddress = true;
                        }
                        break;
                    }
                }

                i++;
            }

            //update by Rachel 20140819 for 5337
            //this.labelTotal.Text = "&nbsp;    Total: " + totalToPay.ToString(CURRENCY_FORMAT);
            this.labelTotal.Text = "&nbsp;    Total: " + totalToPay.ToString(CURRENCY_FORMAT,CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337
            
            //dls 080122 - new totals panel
            //calculate over / under payment
            decimal diff = totalToPay - totalReceived;

            if (setValues)
                SetTotalValues(totalToPay, totalReceived, diff, calcRep);
            
            return diff;
        }

        private void SetTotalValues(decimal totalToPay, decimal totalReceived, decimal diff, bool calcRep)
        {
            //update by Rachel 20140819 for 5337
            //this.lblTotalDue.Text = totalToPay.ToString(CURRENCY_FORMAT);
            //this.lblTotalPaymentReceived.Text = totalReceived.ToString(CURRENCY_FORMAT);
            this.lblTotalDue.Text = totalToPay.ToString(CURRENCY_FORMAT,CultureInfo.InvariantCulture);
            this.lblTotalPaymentReceived.Text = totalReceived.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337

            //display the absolute difference, not the negative amount
            decimal displayDiff = System.Math.Abs(diff);

            //update by Rachel 20140819 for 5337
            //this.lblOverOrUnder.Text = displayDiff.ToString(CURRENCY_FORMAT);
            this.lblOverOrUnder.Text = displayDiff.ToString(CURRENCY_FORMAT,CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337

            if (diff > 0)
                this.lblDifference.Text = "Under";
            else if (diff == 0)
                this.lblDifference.Text = "Balanced";
            else
                this.lblDifference.Text = "Over";

        }

        private decimal ParseValue(string value)
        {
            StringBuilder sb = new StringBuilder(value);
            int currencyPos = value.LastIndexOf('R');
            if (currencyPos != -1)
                sb.Remove(0, currencyPos + 1);
            sb.Replace(",", "");
            sb.Replace(" ", "");
            return decimal.Parse(sb.ToString(), System.Globalization.NumberStyles.Any);
        }

        //protected void btnHideMenu_Click(object sender, EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

         private bool ValidateCommonData(StringBuilder sb)
        {
            bool response = true;

            decimal originalAmount = this.ParseValue(this.labelTotal.Text.Trim());

            if (adminRctAmount != originalAmount)
            {
                response = false;
                sb.Append("<li>The amount tendered is different to the fine amount being re-applied.</li>\n");
            }
            else
            {
                //dls 070726 save original fine amount in view state.
                this.ViewState["currentFineAmount"] = originalAmount;
                currentFineAmount = originalAmount;
            }

            return response;
        }

        private bool ValidateHeaderData(StringBuilder sb)
        {
            bool response = true;

            if (this.requireAddresses)
            {
                if (this.txtAddress1.Text.Trim().Length == 0)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("stringValdateHeader") + "</li>\n");
                }
                if (this.txtAddress2.Text.Trim().Length == 0)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("stringValdateHeader1") + "</li>\n");
                }
                if (this.txtPoCode.Text.Trim().Length == 0)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("stringValdateHeader2") + "</li>\n");
                }
            }

            string phone = this.txtPhone.Text.Trim();

            if (mandatoryPhoneNo)
            {
                if (phone.Length < 7)
                {
                    response = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("stringValdateHeader3") + "</li>\n");
                }
            }

            string receivedFrom = string.Empty;

            receivedFrom = this.txtReceivedFrom.Text.Trim();
            if (receivedFrom.Length == 0)
            {
                response = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("stringValdateHeader4") + "</li>\n");
            }

            return response;
        }

        private bool CheckForRepresentations(StringBuilder sb, bool isValid)
        {
            bool response = true;
            decimal totalReceived = adminRctAmount;

            decimal diff = CalculateTotal(false, ref totalReceived);

            if (diff != 0)
            {
                response = false;
                isValid = false;

                sb.Append("<li>" + (string)GetLocalResourceObject("stringCheckForRepresentations") + "</li>\n");

                // Check if this receipt allows for on-the-fly representations
                if (!this.allowOnTheFlyRepresentations)
                 {
                    sb.Append("<li>"+(string)GetLocalResourceObject("stringCheckForRepresentations1")+"</li>\n");
                }
                else 
                {
                    this.lblError.Visible = false;
                    this.panelConfirm.Visible = false;

                    if (diff > 0)
                    {
                        int noOfNoticesAllowedReps = 0;

                        //dls 080519 - need to check that the selected notices are allowed to be rep'd: no on-the-fly reps after judgement
                        foreach (NoticeForPayment notice in this.notices)
                        {
                            if (!notice.HasJudgement)
                                noOfNoticesAllowedReps++;
                        }

                        if (noOfNoticesAllowedReps == 0)
                        {
                            sb.Append("<li>"+(string)GetLocalResourceObject("stringCheckForRepresentations2")+"</li>\n");
                            sb.Append("<li>"+(string)GetLocalResourceObject("stringCheckForRepresentations3")+"</li>\n");
                            this.chkCashierRep.Visible = false;
                        }
                        else
                        {
                            sb.Append("<li>"+(string)GetLocalResourceObject("stringCheckForRepresentations4")+"</li>\n");
                            this.chkCashierRep.Visible = true;
                        }
                    }
                    else
                    {
                        sb.Append("<li>" + (string)GetLocalResourceObject("stringCheckForRepresentations5") + "</li>\n");
                        this.chkCashierRep.Visible = false;
                    }

                }                
            }

            //////dls 080227 - these payments will be cleared against
            //if (adminRctIntNo > 0)
            //{
            //    if (adminRctAmount != ParseValue(lblTotalDue.Text))
            //    {
            //        sb.Append("<li>This payment has been selected in order to re-apply and admin receipt reversal - the amount to pay does not match the amount previously reversed on Receipt # " + adminRctNumber + "</li>\n");
            //        this.chkCashierRep.Visible = false;
            //        response = false;
            //        isValid = false;
            //    }
            //}

            return response;
        }

        private void ShowError(StringBuilder sb, bool isValid)
        {
            sb.Append("</ul>");
            if (!isValid)
            {
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return;
            }
        }

        private void ShowConfirmation()
        {
            this.lblError.Text = string.Empty;
            lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text1");

            this.panelDetails.Visible = false;
            this.lblError.Visible = false;
            this.panelConfirm.Visible = true;

            StringBuilder sb = new StringBuilder();
            sb.Append("<br/>&nbsp;<br/><p align=\"center\" style=\"font-weight: bold;\">" + (string)GetLocalResourceObject("CreateTableText") + "</p>");

            sb.Append("<table border=\"0\" cellpadding=\"0\" class=\"CartListItem\" style=\"border-left: none; text-align: left;\"><tr>");
            sb.Append("<tr><td>"+(string)GetLocalResourceObject("CreateTableText1")+"</td><td>");
            sb.Append(this.txtReceivedFrom.Text);
            this.WriteAddressDetails(sb);
            sb.Append("</td></tr>\n");
            sb.Append("<tr><td>"+(string)GetLocalResourceObject("CreateTableText2")+"</td><td>");
            sb.Append(this.txtPhone.Text);
            sb.Append("<tr><td>"+(string)GetLocalResourceObject("CreateTableText3")+"</td><td>");
            sb.Append(DateTime.Today.ToString(DATE_FORMAT));
            sb.Append("</td></tr>\n");

            if (this.txtComment.Text.Length > 0)
            {
                sb.Append("<tr><td>"+(string)GetLocalResourceObject("CreateTableText4")+"</td><td>");
                sb.Append(this.txtComment.Text);
                sb.Append("</td></tr>\n");
            }

            sb.Append("</table><br/>");

            decimal total = 0;
            // Offences table
            sb.Append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">");
            sb.Append("<tr><th class=\"CartListHead\">"+(string)GetLocalResourceObject("CreateTableText5")+"</th>");
            sb.Append("<th class=\"CartListHead\">"+(string)GetLocalResourceObject("CreateTableText6")+"</th>");
            sb.Append("<th class=\"CartListHead\">"+(string)GetLocalResourceObject("CreateTableText7")+"</th>");
            sb.Append("<th class=\"CartListHead\">"+(string)GetLocalResourceObject("CreateTableText8")+"</th>");
            sb.Append("<th class=\"CartListHead\">"+(string)GetLocalResourceObject("CreateTableText9")+"</th>");
            sb.Append("<th class=\"CartListHead\">"+(string)GetLocalResourceObject("CreateTableText10")+"</th>");
            sb.Append("<th class=\"CartListHead\">" + (string)GetLocalResourceObject("CreateTableText11") + "</th></tr>\n");

            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                ImageButton check = (ImageButton)item.FindControl("igPayNow");
                if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                {
                    //Barry Dickson - 20080410 included contempt court amount
                    decimal origAmount = this.ParseValue(item.Cells[7].Text.Trim());
                    decimal contemptAmount = this.ParseValue(item.Cells[9].Text.Trim());

                    decimal value = 0;
                    if (this.allowOnTheFlyRepresentations && this.hasRepresentation)
                        value = decimal.Parse(((TextBox)item.FindControl("txtAmountToPay")).Text) + this.ParseValue(item.Cells[9].Text.Trim());
                    else
                        //dls 080417 ============= - value = this.ParseValue(item.Cells[7].Text.Trim())
                        value = this.ParseValue(item.Cells[7].Text.Trim()) + this.ParseValue(item.Cells[9].Text.Trim());

                    //dls 080417 ============= - total += value + contemptAmount;
                    total += value;

                    sb.Append("<tr><td class=\"CartListItem\">");
                    sb.Append(item.Cells[2].Text);
                    sb.Append("</td><td class=\"CartListItem\">");
                    sb.Append(item.Cells[4].Text);
                    sb.Append("</td><td class=\"CartListItem\">");
                    sb.Append(item.Cells[3].Text);
                    sb.Append("</td><td class=\"CartListItem\">");
                    sb.Append(item.Cells[5].Text);
                    sb.Append("</td><td class=\"CartListItem\">");

                    //update by Rachel 20140819 for 5337
                    //sb.Append(origAmount.ToString(CURRENCY_FORMAT));
                    sb.Append(origAmount.ToString(CURRENCY_FORMAT,CultureInfo.InvariantCulture));
                    //end update by Rachel 20140819 for 5337

                    sb.Append("</td><td class=\"CartListItem\">");

                    //update by Rachel 20140819 for 5337
                    //sb.Append(contemptAmount.ToString(CURRENCY_FORMAT);
                    sb.Append(contemptAmount.ToString(CURRENCY_FORMAT, CultureInfo.InvariantCulture));
                    //end update by Rachel 20140819 for 5337

                    sb.Append("</td><td style=\"border-right: dimgray 1px solid; border-left: dimgray 1px solid;\"");
                    if ((origAmount + contemptAmount) == value)
                        sb.Append(" class=\"CartListItem\">");
                    else
                        sb.Append(" class=\"NormalRed\">");

                    //update by Rachel 20140819 for 5337
                    //sb.Append(value.ToString(CURRENCY_FORMAT));
                    sb.Append(value.ToString(CURRENCY_FORMAT,CultureInfo.InvariantCulture));
                    //end update by Rachel 20140819 for 5337

                    sb.Append("</td></tr>\n");
                }
            }
            sb.Append("<tr>");
            for (int i = 0; i < 7; i++)
                sb.Append("<td style=\"border-top: dimgray 1px solid;\">&nbsp;</td>");
            sb.Append("</tr>\n");

            sb.Append("</td></tr></table><br/>\n");

            sb.Append("<table border=\"0\" cellpadding=\"0\" class=\"CartListItem\" style=\"border-left: none; text-align: left;\">");
            sb.Append("<tr><td style=\"text-align: right\" class=\"NormalBold\">Total Amount Received:</td><td>");

            //update by Rachel 20140819 for 5337
            //sb.Append(total.ToString(CURRENCY_FORMAT));
            sb.Append(total.ToString(CURRENCY_FORMAT,CultureInfo.InvariantCulture));
            //end update by Rachel 20140819 for 5337

            sb.Append("</td></tr>\n");
            sb.Append("</table><br/>");
           
            //dls 080227 - these payments will be cleared against
            if (adminRctIntNo > 0)
            {
                sb.Append("<table border=\"0\" cellpadding=\"0\" class=\"CartListItem\" style=\"border-left: none; text-align: left;\">");
                sb.Append("<tr><td style=\"text-align: right\" class=\"NormalBold\">" + (string)GetLocalResourceObject("CreateTableText12") + " # ");
                sb.Append(adminRctNumber);
                sb.Append(" (R ");

                //update by Rachel 20140819 for 5337
                //sb.Append(adminRctAmount.ToString(CURRENCY_FORMAT));
                sb.Append(adminRctAmount.ToString(CURRENCY_FORMAT,CultureInfo.InvariantCulture));
                //end update by Rachel 20140819 for 5337

                sb.Append(") </td><td>");
                sb.Append("</table><br/>");
            }

            this.labelConfirm.Text = sb.ToString();
        }

        private void WriteAddressDetails(StringBuilder sb)
        {
            sb.Append("</td></tr>\n");
            sb.Append("<tr><td>" + (string)GetLocalResourceObject("lblAddress.Text") + "</td><td>");
            sb.Append(this.txtAddress1.Text);
            sb.Append("</td></tr>\n");
            sb.Append("<tr><td>&nbsp;</td><td>");
            sb.Append(this.txtAddress2.Text);
            if (this.txtAddress3.Text.Trim().Length > 0)
            {
                sb.Append("</td></tr>\n");
                sb.Append("<tr><td>&nbsp;</td><td>");
                sb.Append(this.txtAddress3.Text);
            }
            if (this.txtAddress4.Text.Trim().Length > 0)
            {
                sb.Append("</td></tr>\n");
                sb.Append("<tr><td>&nbsp;</td><td>");
                sb.Append(this.txtAddress4.Text);
            }
            if (this.txtAddress5.Text.Trim().Length > 0)
            {
                sb.Append("</td></tr>\n");
                sb.Append("<tr><td>&nbsp;</td><td>");
                sb.Append(this.txtAddress5.Text);
            }
            sb.Append("</td></tr>\n");
            sb.Append("<tr><td>&nbsp;</td><td>");
            sb.Append(this.txtPoCode.Text);
        }

        protected void buttonBack_Click(object sender, EventArgs e)
        {
            this.panelConfirm.Visible = false;
            this.panelDetails.Visible = true;
            this.lblError.Visible = false;
        }

        protected void buttonVoid_Click(object sender, EventArgs e)
        {
            // FBJ Added (2007-01-29): Reset the notice list
            this.notices.Clear();
            this.Session["NoticesToPay"] = this.notices;
            this.Session["NoticeAddressDetails"] = null;
            this.Session["TRHIntNo"] = null;
            this.Session["TRHTranDate"] = null;
            this.Session["AdminRctIntNo"] = null;
            this.Session["AdminRctAmount"] = null;
            this.Session["AdminRctNumber"] = null;

            Response.Redirect("AdminReceiptReversal.aspx");
        }

        protected void UpdateReceiptHeader()
        {
            // Create the receipt object
            ReceiptTransaction receipt = null;

            //receipt header data
            receipt = new ReceiptTransaction();
            receipt.AddressRequired = this.requireAddresses;
            receipt.Cashier = this.cashier;

            receipt.IsOneReceiptPerNotice = isOneReceiptPerNotice;
            receipt.AutIntNo = this.autIntNo;
            receipt.Details = ((this.txtComment.Text.Trim().Length > 0) ? this.txtComment.Text : "Admin Receipt Payment");
            receipt.BAIntNo = 0;
            receipt.CBIntNo = 0;
            receipt.ContactNumber = this.txtPhone.Text.Trim();

            receipt.ReceivedFrom = this.txtReceivedFrom.Text.Trim();

            receipt.Address1 = this.txtAddress1.Text.Trim();
            receipt.Address2 = this.txtAddress2.Text.Trim();
            receipt.Address3 = this.txtAddress3.Text.Trim();
            receipt.Address4 = this.txtAddress4.Text.Trim();
            receipt.Address5 = this.txtAddress5.Text.Trim();
            receipt.AddressAreaCode = this.txtPoCode.Text.Trim();

            receipt.MaxStatus = this.maxStatus;

            receipt.CashType = CashType.AdminReceipt;

            UpdateReceipts(receipt, true);
        }

        private void UpdateReceipts(ReceiptTransaction receipt, bool header)
        {
            //add the receipt transaction to the list in the view state
            string errMessage = string.Empty;

            if (trhTranDate.Equals(DateTime.MinValue))
            {
                trhTranDate = DateTime.Now;
                Session.Add("TRHTranDate", trhTranDate);
            }

            int updTRHIntNo = cashReceipt.AddReceiptToPaymentList(receipt, trhTranDate, trhIntNo, ref errMessage, header, login, (Int32)Session["userIntNo"]);

            lblError.Visible = true;

            if (updTRHIntNo > 0)
            {
                trhIntNo = updTRHIntNo;
                Session.Add("TRHIntNo", trhIntNo);

                if (!header)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    //Linda 2012-9-12 add for list report19
                    //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                    //2013-11-7 Heidi commented out for only on confirm Payment button add PunchStatisticsTransaction
                    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    //punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.login, PunchStatisticsTranTypeList.PaymentTraffic, PunchAction.Add);   
                  
                }
                else
                {
                    ShowConfirmation();
                }
            }
            else if (!header)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1") + errMessage;
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2") + errMessage;
            }
        
        }

        protected void buttonPay_Click(object sender, EventArgs e)
        {
            string errMessage = string.Empty;

            List<int> receipts = new List<int>();

            receipts = cashReceipt.CreateAdminReceipt(trhIntNo, ref errMessage, adminRctIntNo, this.noDaysForLastPaymentDate);

            if (receipts[0] <= 0)
            {
                this.SetErrorState((string)GetLocalResourceObject("lblError.Text3") + errMessage);
                return;
            }
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.login, PunchStatisticsTranTypeList.PaymentTraffic, PunchAction.Add);  

            //dls 090701 - moved the udpate of the SumComments to the SP SummonsChargeUpdatePayment which is called from ProcessAdminReceipt
            //// 20090601 tf Update column SumComments in Summons
            //SummonsDB summonsDb = new SummonsDB(this.connectionString);
            //foreach (NoticeForPayment notic in notices)
            //{
            //    summonsDb.UpdateSummonsComments(notic.NotIntNo, "Case finalized �C payment of R rrrr.cc was received on yyyy/mm/dd HH:MM (receipt # 999999)", this.login);
            //}

            // FBJ Added (2007-01-29): Reset the notice list
            this.notices.Clear();
            this.Session["NoticesToPay"] = this.notices;

            //clear the address details
            this.Session["NoticeAddressDetails"] = null;
            this.Session["TRHIntNo"] = null;
            this.Session["TRHTranDate"] = null;
            this.Session["AdminRctIntNo"] = null;
            this.Session["AdminRctAmount"] = null;
            this.Session["AdminRctNumber"] = null;

            //check if postal receipts are used for all receipts
            this.CheckUsePostalReceipt();

            // Open a new window with the receipt report and print it 
            StringBuilder sb = new StringBuilder();
            foreach (int rctIntNo in receipts)
            {
                sb.Append(rctIntNo);
                sb.Append(',');
            }

            if (this.cashier == null || this.cashier.CashBoxType != CashboxType.PostalReceipts)
            {

                if (usePostalReceipt)
                    Helper_Web.BuildPopup(this, "CashReceiptTraffic_PostalReceiptViewer.aspx", "date",
                                          DateTime.Now.ToString("yyyy-MM-dd"), "CBIntNo", "0", "RCtIntNo", sb.ToString());
                else
                    Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", sb.ToString());
            }
            else
            {
                Helper_Web.BuildPopup(this, "CashReceiptTraffic_PostalReceiptViewer.aspx", "date",
                                          DateTime.Now.ToString("yyyy-MM-dd"), "CBIntNo", "0", "RCtIntNo", sb.ToString());
            }
            this.panelConfirm.Visible = false;
            this.pnlAnotherPayment.Visible = true;         
        }

        private void SetErrorState(string message)
        {
            this.lblError.Visible = true;
            this.panelDetails.Visible = true;
            this.panelConfirm.Visible = false;

            lblError.Text = message;
        }

        protected void btnAddNotices_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminReceiptTraffic.aspx");
        }

        protected void buttonRepresentationYes_Click(object sender, EventArgs e)
        {
            // Get the charge int no
            string ticketNumber = string.Empty;
            ImageButton chk = null;
            int count = 0;
            foreach (DataGridItem item in this.dgTicketDetails.Items)
            {
                chk = (ImageButton)item.FindControl("igPayNow");
                if (chk != null)
                {
                    if (chk.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                    {
                        count++;
                        ticketNumber = item.Cells[2].Text;
                    }
                }
            }

            if (count != 1)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                return;
            }

            this.panelDetails.Visible = false;
            this.lblError.Visible = false;
            this.panelConfirm.Visible = false;
        }

        protected void lnkBack2_Click(object sender, EventArgs e)
        {
            this.panelDetails.Visible = true;
            this.lblError.Visible = false;
            this.panelConfirm.Visible = false;
            this.lblError.Text = string.Empty;
        }

        protected void checkPayNow_CheckedChanged(object sender, EventArgs e)
        {
            decimal totalReceived = adminRctAmount;
            this.CalculateTotal(true, ref totalReceived);
        }

        protected void dgTicketDetails_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (this.notices == null)
                    this.notices = (List<NoticeForPayment>)this.Session["NoticesToPay"];

                // Get the grid row's NotIntNo
                int notIntNo = 0;
                if (e.Item.Cells[0].Text.Length > 0)
                    notIntNo = int.Parse(e.Item.Cells[0].Text);
                else
                {
                    DataRowView row = (DataRowView)e.Item.DataItem;
                    if (row == null)
                        return;
                    notIntNo = (int)row["NoticeIntNo"];
                }

                // Find the notice
                NoticeForPayment notice = null;
                foreach (NoticeForPayment n in this.notices)
                {
                    if (n.Equals(notIntNo))
                    {
                        notice = n;
                        break;
                    }
                }

                // Set the check box for the notice
                if (notice != null)
                {
                    ImageButton check = (ImageButton)e.Item.FindControl("igPayNow");
                    check.ImageUrl = notice.IsSelected ? CHECKED_IMAGE : UNCHECKED_IMAGE;

                    TextBox txtAmountToPay = ((TextBox)e.Item.FindControl("txtAmountToPay"));
                    txtAmountToPay.Enabled = !notice.HasJudgement;

                    DropDownList ddlSeniorOfficer = ((DropDownList)e.Item.FindControl("ddlSeniorOfficer"));
                    ddlSeniorOfficer.Enabled = !notice.HasJudgement;
                }
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton check = (ImageButton)sender;
            if (check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                check.ImageUrl = UNCHECKED_IMAGE;
            else
                check.ImageUrl = CHECKED_IMAGE;

            decimal totalReceived = adminRctAmount;
            this.CalculateTotal(true, ref totalReceived);
        }

        protected void ddlReceivedFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlReceivedFrom.SelectedItem.Value)
            {
                case "D":       //Driver
                    this.personaType = PersonaType.Driver;
                    ShowOrHideAddress(true);
                    break;
                case "O":       //Other
                    this.personaType = PersonaType.Owner;
                    ShowOrHideAddress(true);
                    break;
                case "P":       //Proxy
                    this.personaType = PersonaType.Proxy;
                    ShowOrHideAddress(true);
                    break;
                case "X":       //Other
                    this.personaType = PersonaType.Other;
                    ShowOrHideAddress(false);
                    break;
                case "A":
                    this.personaType = PersonaType.Accused;
                    ShowOrHideAddress(true);
                    break;
            }

            ViewState.Add("PersonaType", this.personaType);

            if (Session["NoticeAddressDetails"] != null)
                GetAddressDetails(0, false);
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                lblError.Visible = true;
            }
        }

        protected void ShowOrHideAddress(bool show)
        {
            lblName.Visible = show;
            lblAddress.Visible = show;
            lblPostalCode.Visible = show;
            txtReceivedFrom.Visible = show;
            txtAddress1.Visible = show;
            txtAddress2.Visible = show;
            txtAddress3.Visible = show;
            txtAddress4.Visible = show;
            txtAddress5.Visible = show;
            txtPoCode.Visible = show;
            txtPhone.Visible = show;
            lblPhone.Visible = show;
        }

        protected void dgTicketDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (Session["AddressDetails"] == null || changeAddressDetails)
            //{
                int notIntNo = (int)this.dgTicketDetails.DataKeys[dgTicketDetails.SelectedIndex];

                GetAddressDetails(notIntNo, true);
            //}
        }

        protected void btnPayNow_Click(object sender, EventArgs e)
        {
            //dls 080130 - need to check a few things here like addresses, etc.
            bool isValid = true;

            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("StringMsg") + "<br/><ul>");

            isValid = this.ValidateHeaderData(sb);

            dsReceiptPayments = cashReceipt.GetReceiptPaymentListDS(trhIntNo);

            //dls 070406 - check this first, as otherwise it gets overwritten
            if (!this.CheckForRepresentations(sb, isValid))
            {
                this.ShowError(sb, false);
                return;
            }

            isValid = AddNoticesForPayment();

            if (isValid)
                UpdateReceiptHeader();

        }

        private bool AddNoticesForPayment()
        {
            bool isValid = true;

            string errMessage = string.Empty;
            string noticeList = string.Empty;
            StringBuilder sb = new StringBuilder(string.Empty);

            AccountDB accountCharge = new AccountDB(connectionString);

 
                sb = new StringBuilder("<root>");

                ImageButton check = null;
                // Process each charge/notice in the list
                // NB: ANY CHANGE TO THE DataGrid COLUMNS WILL BREAK THE FOLOWING CODE !!!!!
                foreach (DataGridItem item in this.dgTicketDetails.Items)
                {
                    // Is this item included in the payment
                    check = (ImageButton)item.FindControl("igPayNow");
                    if (!check.ImageUrl.Equals(CHECKED_IMAGE, StringComparison.InvariantCultureIgnoreCase))
                        continue;

                    int notIntNo = int.Parse(item.Cells[0].Text);
                    int chgIntNo = int.Parse(item.Cells[1].Text);
                    //Barry Dickson - added 3 fields to grid 9 now = 12
                    //dls 080416 - this may be blank - seems like it ended up being column 13 not 12????
                    //int ctIntNo = int.Parse(item.Cells[12].Text);
                    int ctIntNo = 0;

                    if (Int32.TryParse(item.Cells[13].Text, out ctIntNo))
                        ctIntNo = Int32.Parse(item.Cells[13].Text);

                    //Barry Dickson - add in contempt court amt to total - amount to pay must be the addition of both
                    //dls - 080417 - no the rev fine amount is the fine amount before the rep
                    decimal chgRevFineAmount = this.ParseValue(item.Cells[7].Text); //+ this.ParseValue(item.Cells[9].Text);
                    decimal contemptAmount = this.ParseValue(item.Cells[9].Text);
                    decimal amountPaid = chgRevFineAmount + contemptAmount;
                    string seniorOfficer = string.Empty;

                    if (this.allowOnTheFlyRepresentations && this.hasRepresentation)
                    {
                        //dls 080417 ============= - amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text) + contemptAmount;
                        //Barry Dickson putting contempt amount back in here...
                        //amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text);
                        amountPaid = this.ParseValue((string)((TextBox)item.FindControl("txtAmountToPay")).Text) + contemptAmount;
                        seniorOfficer = ((DropDownList)item.FindControl("ddlSeniorOfficer")).Text;
                    }

                    int accIntNo = accountCharge.GetAccount(ctIntNo);
                    if (accIntNo <= 0)
                    {
                        isValid = false;
                        this.SetErrorState((string)GetLocalResourceObject("ErrorState"));
                        return isValid;
                    }
                    //Barry Dickson 20080409 - add in contempt amount to xml
                    sb.Append("<notice notintno=\"");
                    sb.Append(notIntNo);
                    sb.Append("\"");
                    sb.Append(" chgintno=\"");
                    sb.Append(chgIntNo);
                    sb.Append("\"");
                    sb.Append(" ctintno=\"");
                    sb.Append(ctIntNo);
                    sb.Append("\"");
                    sb.Append(" accintno=\"");
                    sb.Append(accIntNo);
                    sb.Append("\"");
                    sb.Append(" chgrevfineamount=\"");
                    //update by Rachel 20140811 for 5337
                    //sb.Append(chgRevFineAmount);
                    sb.Append(chgRevFineAmount.ToString(CultureInfo.InvariantCulture));
                    //end update by Rachel 20140811 for 5337
                    sb.Append("\"");
                    sb.Append(" contemptamount=\"");
                    //update by Rachel 20140811 for 5337
                    //sb.Append(contemptAmount);
                    sb.Append(contemptAmount.ToString(CultureInfo.InvariantCulture));
                    //end update by Rachel 20140811 for 5337
                    sb.Append("\"");
                    sb.Append(" amountpaid=\"");
                    //update by Rachel 20140811 for 5337
                    //sb.Append(amountPaid);
                    sb.Append(amountPaid.ToString(CultureInfo.InvariantCulture));
                    //end update by Rachel 20140811 for 5337
                    sb.Append("\"");
                    sb.Append(" seniorofficer=\"");
                    sb.Append(seniorOfficer);
                    sb.Append("\"");
                    sb.Append(" />");
                }

                sb.Append( "</root>");
                noticeList = sb.ToString();
            
            
            int updTRHIntNo = cashReceipt.AddNoticesToReceiptList(trhIntNo, noticeList, 0, ref errMessage);

            if (updTRHIntNo <= 0)
            {
                isValid = false;
                this.SetErrorState((string)GetLocalResourceObject("ErrorState1") + errMessage);
                return isValid;
            }

            return true;
        }

        protected void chkCashierRep_CheckedChanged(object sender, EventArgs e)
        {
            ProcessRepresentations();
        }

        protected void dgTicketDetails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName == "SummonsDetails")
            {
                dgTicketDetails.SelectedIndex = e.Item.ItemIndex;

                if (dgTicketDetails.SelectedIndex > -1)
                {
                    int notIntNo = Convert.ToInt32(dgTicketDetails.DataKeys[dgTicketDetails.SelectedIndex]);
                    this.Session.Add("notIntNo", notIntNo);
                    Helper_Web.BuildPopup(this, "ViewSummons_Detail.aspx", null);
                }
            }
        }

        protected void dgTicketDetails_EditCommand(object source, DataGridCommandEventArgs e)
        {
            lblError.Visible = false;

            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("ErrorText") + "</br><ul>");
            bool isValid = true;

            TextBox txtAmountToPay = (TextBox)e.Item.FindControl("txtAmountToPay");
            DropDownList ddlSeniorOfficer = (DropDownList)e.Item.FindControl("ddlSeniorOfficer");

            int notIntNo = int.Parse(e.Item.Cells[0].Text);
            //decimal fullAmount = this.ParseValue(e.Item.Cells[7].Text) + this.ParseValue(e.Item.Cells[7].Text);
            decimal fullAmount = this.ParseValue(e.Item.Cells[7].Text) + this.ParseValue(e.Item.Cells[9].Text);

            decimal amountToPay = 0;

            if (!Decimal.TryParse(txtAmountToPay.Text, out amountToPay))
            {
                sb.Append("<li>"+(string)GetLocalResourceObject("ErrorText1")+"</li>\n");
                isValid = false;
            }
            else if (amountToPay > fullAmount)
            {
                sb.Append("<li>"+(string)GetLocalResourceObject("ErrorText2")+"</li>\n");
                isValid = false;
            }
            else if (amountToPay == 0)
            {
                sb.Append("<li>"+(string)GetLocalResourceObject("ErrorText3")+"</li>\n");
                isValid = false;
            }

            if (ddlSeniorOfficer.SelectedIndex < 0)
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("ErrorText4") + "</li>\n");
                isValid = false;
            }

            if (!isValid)
            {
                ShowError(sb, false);
                return;
            }


            foreach (NoticeForPayment notice in this.notices)
            {
                if (notice.NotIntNo == notIntNo)
                {
                    notice.AmountToPay = amountToPay;
                    notice.SeniorOfficer = ddlSeniorOfficer.SelectedItem.Text;
                    break;
                }
            }

            this.Session["NoticesToPay"] = this.notices;

            RefreshNoticeGrid();

            decimal totalReceived = adminRctAmount;
            decimal diff = CalculateTotal(true, true, ref totalReceived);

            if (diff == 0)
                btnContinue.Enabled = true;
            else
                btnContinue.Enabled = false;

            lblError.Visible = true;
            lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            //Linda 2012-9-12 add for list report19
            //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
            //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            //punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.login, PunchStatisticsTranTypeList.PaymentTraffic,PunchAction.Add);   
                  
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            RefreshNoticeGrid();

            bool isValid = AddNoticesForPayment();

            if (isValid)
                UpdateReceiptHeader();
        }

        protected void btnBackFromRepresentations_Click(object sender, EventArgs e)
        {
            chkCashierRep.Checked = false;
            ProcessRepresentations();
        }

        private void ProcessRepresentations()
        {
            this.hasRepresentation = chkCashierRep.Checked;
            chkCashierRep.Visible = false;
            btnBackFromRepresentations.Visible = this.hasRepresentation;
            this.ViewState.Add("HasRepresentation", this.hasRepresentation);

            lblError.Visible = false;

            lblRepresentations.Visible = this.hasRepresentation;
            btnContinue.Visible = this.hasRepresentation;

            pnlReceiptHeader.Visible = !this.hasRepresentation;

            if (this.hasRepresentation)
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text3");
            else
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text4");

            if (btnContinue.Visible)
                btnContinue.Enabled = false;

            RefreshNoticeGrid();

        }
    }
}