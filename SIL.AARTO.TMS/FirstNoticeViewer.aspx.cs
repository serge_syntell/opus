using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
//using BarcodeNETWorkShop;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF.Merger;
using System.Text.RegularExpressions;
using Stalberg.TMS.Data.Util;
using System.Collections.Generic;
using System.Drawing;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Handles printing the first notice
    /// </summary>
    public partial class FirstNoticeViewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private string printFile = string.Empty;
        private int autIntNo = 0;
        private int NRIntNo = 0;
        protected string loginUser = string.Empty;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.loginUser = userDetails.UserLoginName;

            this.autIntNo = Convert.ToInt32(Session["printAutIntNo"]);
            this.NRIntNo = Convert.ToInt32(Session["NRIntNo"]);


            // Retrieve the Print File name from the Session
            if (Request.QueryString["printfile"] == null)
            {
                Response.Write((string)GetLocalResourceObject("strWriteMsg"));
                Response.End();
                return;
            }
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            string printType = Request.QueryString["printType"] == null ? "" : Request.QueryString["printType"].ToString().Trim();
            this.printFile = Request.QueryString["printfile"].ToString().Trim(); // Ciprus print request file name or none is the default
            if (printFile != "-1")
            {
                //have to place this here to cater for printing 2nd notices for new offenders/changed regno's
                if (printFile.IndexOf("2ND") > -1)
                {
                    Response.Redirect("SecondNoticeViewer.aspx?printfile=" + printFile + "&printType=" + printType);
                }
            }


            // use module to create print pdf file, Oscar 20120119
            PrintFileProcess process = new PrintFileProcess(this.connectionString, this.loginUser);
            PrintFileModuleFirstNotice firstNotice = new PrintFileModuleFirstNotice(this.NRIntNo);
            firstNotice.page = this;

            process.BuildPrintFile(firstNotice, this.autIntNo, Request.QueryString["printfile"]);
            if (printType == "ReprintNewOffender")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ReprintNewOffenderNotices, PunchAction.Change);  

            }
            if (printType == "PrintFirstNotice")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.PrintFirstNotice, PunchAction.Change);  
            }

        }
    }
}
