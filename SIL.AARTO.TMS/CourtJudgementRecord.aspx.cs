using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class CourtJudgementRecord : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
       
        protected string styleSheet;
        protected string backgroundImage;
        protected string title;
        //protected string thisPage = "Court - Print Judgements";
        protected string thisPageURL = "CourtJudgementRecord.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            ViewState["AutIntNo"] = autIntNo;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);
           
            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                //mrs 20080621 set up authrule defaults if necessary
                CheckAuthRules(autIntNo);

                this.GetCourts();
            }
        }

        private void CheckAuthRules(int autIntNo)
        {
            ////7111	1	5600	Rule for grace period (no of days) for payment after judgement captured	7	NULL	No. of Days (default = 7)
            //AuthorityRulesDB ar1DB = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails ard1 = ar1DB.GetAuthorityRulesDetailsByCode(autIntNo, "5600", "Rule for grace period (no of days) for payment after judgement captured", 7, "", "No. of Days (default = 7)", this.login);
            ////functions handle the rest

            AuthorityRulesDetails ard1 = new AuthorityRulesDetails();
            ard1.AutIntNo = autIntNo;
            ard1.ARCode = "5600";
            ard1.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(ard1, this.connectionString);
            KeyValuePair<int, string> value1 = authRule.SetDefaultAuthRule();

            ////7127	1	6100	Rule for period (no of days) within which the court roll must be captured (payments not allowed)	14	NULL	No. of Days (default = 14)
            //AuthorityRulesDB ar2DB = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails ard2 = ar2DB.GetAuthorityRulesDetailsByCode(autIntNo, "6100", "Rule for period (no of days) within which the court roll must be captured (payments not allowed)", 14, "", "No. of Days (default = 7)", this.login);
            ////functions handle the rest

            AuthorityRulesDetails ard2 = new AuthorityRulesDetails();
            ard2.AutIntNo = this.autIntNo;
            ard2.ARCode = "6100";
            ard2.LastUser = this.login;

            DefaultAuthRules authRule1 = new DefaultAuthRules(ard2, this.connectionString);
            KeyValuePair<int, string> value2 = authRule1.SetDefaultAuthRule();
        }

        private void GetCourts()
        {
            this.cboCourt.Items.Clear();
            ListItem item;
            
            CourtDB db = new CourtDB(this.connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(this.autIntNo);
            while (reader.Read())
            {
                item = new ListItem();
                item.Value = reader["CrtIntNo"].ToString();
                item.Text = reader["CrtDetails"].ToString();
                this.cboCourt.Items.Add(item);
            }
            reader.Close();

            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("cboCourtItem.Text");
            this.cboCourt.Items.Insert(0, item);
            this.cboCourt.SelectedIndex = 0;
        }

        

        //mrs 20081130 reewrote to capturecourt judgement standard
        protected void cboCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            int crtIntNo = int.Parse(this.cboCourt.SelectedValue);
            if (crtIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                return;
            }

            // Populate the court rooms
            this.cboCourtRoom.Items.Clear();
            ListItem item;
            CourtRoomDB db = new CourtRoomDB(this.connectionString);
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            SqlDataReader reader = db.GetCourtRoomListByCrtRStatusFlag(crtIntNo);//db.GetCourtRoomList(crtIntNo);

            this.cboCourtRoom.DataSource = reader;
            this.cboCourtRoom.DataValueField = "CrtRIntNo";
            this.cboCourtRoom.DataTextField = "CrtRoomDetails";
            this.cboCourtRoom.DataBind();
            reader.Close();
            
            //Dictionary<int, string> lookups =
            //   CourtRoomLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            //while (reader.Read())
            //{
            //    int crtRIntNo = (int)reader["CrtRIntNo"];
            //    if (lookups.ContainsKey(crtRIntNo))
            //    {
            //        cboCourtRoom.Items.Add(new ListItem(lookups[crtRIntNo], crtRIntNo.ToString()));
            //    }
            //}
            ////while (reader.Read())
            ////{
            ////    item = new ListItem();
            ////    item.Value = reader["CrtRIntNo"].ToString();
            ////    item.Text = reader["CrtRoomDetails"].ToString();

            ////    this.cboCourtRoom.Items.Add(item);
            ////}
            //reader.Close();

            // Insert the dummy item
            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("cboCourtRoomItem.Text");
            this.cboCourtRoom.Items.Insert(0, item);
            this.cboCourtRoom.SelectedIndex = 0;

            this.cboCourtRoom.Enabled = true;
        }

        protected void cboCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ViewState["AutIntNo"].ToString());
            int crtRIntNo = int.Parse(this.cboCourtRoom.SelectedValue);
            if (crtRIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }

            // Populate the court dates
            this.cboCourtDates.Items.Clear();
            ListItem item;
            CourtDatesDB db = new CourtDatesDB(this.connectionString);
            SqlDataReader reader = db.GetCourtDatesList(crtRIntNo, autIntNo);
            while (reader.Read())
            {
                item = new ListItem();
                item.Value = reader["CDIntNo"].ToString();
                item.Text = DateTime.Parse(reader["CDate"].ToString()).ToString("yyyy-MM-dd");

                this.cboCourtDates.Items.Add(item);
            }
            reader.Close();

            // Insert the dummy item
            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("cboCourtDatesItem.Text");
            this.cboCourtDates.Items.Insert(0, item);
            this.cboCourtDates.SelectedIndex = 0;

            this.cboCourtDates.Enabled = true;
        }

        protected void cboCourtDates_SelectedIndexChanged(object sender, EventArgs e)
        {
            int cdIntNo = Convert.ToInt32(this.cboCourtDates.SelectedValue.ToString());
            if (cdIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                lblError.Visible = true;
                return;
            }
            DateTime dt;

            if (!DateTime.TryParse(this.cboCourtDates.SelectedItem.ToString(), out dt))
            {
                this.lblError.Text =(string)GetLocalResourceObject("lblError.Text3");
                this.lblError.Visible = true;
                return;
            }
            ViewState["CourtDate"] = dt;
        }

        protected void btnPrintJudgements_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;
            PageToOpen page = null;
            List<PageToOpen> pages = new List<PageToOpen>();

            // Check that there is a selected court room
            int crtRIntNo = int.Parse(this.cboCourtRoom.SelectedValue);
            if (crtRIntNo < 1)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                lblError.Visible = true;
                return;
            }

            DateTime dt = Convert.ToDateTime(ViewState["CourtDate"].ToString());
            DateTime dtCourtDate = dt;

            if (dt.Ticks > DateTime.Now.Ticks)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                this.lblError.Visible = true;
                return;
            }

            page = new PageToOpen(this, "CourtJudgementRecordViewer.aspx");
            page.Parameters.Add(new QSParams("CrtIntNo", this.cboCourt.SelectedValue));
            page.Parameters.Add(new QSParams("CrtRIntNo", crtRIntNo));
            page.Parameters.Add(new QSParams("CourtDate", dtCourtDate.ToString("yyyy-MM-dd")));
            pages.Add(page);

            Helper_Web.BuildPopup(pages.ToArray());
        }
    }
}
    

