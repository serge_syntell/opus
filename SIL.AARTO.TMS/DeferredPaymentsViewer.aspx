<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.DeferredPaymentsViewer" Codebehind="DeferredPaymentsViewer.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body onload="self.focus();" style="margin: 0px" background="<%=backgroundImage %>" >
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                  
                                        </td>
                            </tr>                            
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table width="100%" border="0" class="Normal">
                        <tr>
                            <td valign="top" style="height: 47px">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                
                        </tr>
                        <tr>
                            <td valign="top">
                                <!-- Start Content -->
                                <asp:UpdatePanel runat="server" ID="UDP">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlDetails" runat="server">
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed"></asp:Label></td>
                                            <table style="width: 700px;"> 
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="200px" Text="<%$Resources:lblSelectLA.Text %>"></asp:Label>
                                                        </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                 Width="200px">
                                            </asp:DropDownList>
                                                    </td>
                                                </tr>                                              
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblTicketSeqNum.Text %>"></asp:Label></td>
                                                    <td>
                                                        <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblFullTicketNum.Text %>"></asp:Label></td>
                                                    <td>
                                                        <asp:TextBox ID="textTicket" runat="server" Width="400px" CssClass="Normal"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">
                                                    </td>
                                                    <td align="right">
                                                        <asp:Button ID="btnReport" runat="server" OnClick="btnReport_Click" Text="<%$Resources:btnReport.Text %>" CssClass="NormalButton" Width="120px" />&nbsp;
                                                        </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlNotices" runat="server" Width="100%">
                                            &nbsp;
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>

    <script type="text/javascript" language="javascript">
   function CheckMe()
   {
        var id = document.getElementById("textId");
        var ticket = document.getElementById("textTicket");
       
        if(id.length == 0 &&  ticket.length == 0)
        { 
            alert("You need to supply either an ID number or a ticket number."); 
        } 
   }
    </script>

</body>
</html>
