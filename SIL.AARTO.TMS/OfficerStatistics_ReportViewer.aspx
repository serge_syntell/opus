<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.OfficerStatistics_ReportViewer" Codebehind="OfficerStatistics_ReportViewer.aspx.cs" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript"  >
        // not great to use global variables - but here goes anyway !!!
        var bRunOnce; 
        
        function doProcess() {
            if (!bRunOnce) { 
                // we do this cos we don't want the function to keep on firing  
                bRunOnce = true; 
                document.forms['form1'].action="OfficerStatistics_ReportViewer.aspx?action=doProcess";
		        document.forms['form1'].method="POST";  
		        document.forms['form1'].submit();
		   }  
        } 
    </script>  
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
</head>
<body onload="javascript:window.setTimeout('doProcess()',1000);self.focus()" style="margin: 0px" background="<%=backgroundImage %>">
    <form id="form1" runat="server">
    </form>
</body>
</html>
