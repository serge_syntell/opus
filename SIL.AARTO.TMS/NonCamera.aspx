﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NonCamera.aspx.cs" Inherits="SIL.AARTO.TMS.NonCamera" %>
<%@ Register Src="NonCameraImageControl.ascx" TagName="NonCameraImageViewer1" TagPrefix="iv1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
     <div style="width: 700px; margin-left: auto; margin-right: auto; border:solid 1px #ccc">
        <table width="100%">
            <tr style="">
                <td style="width: 700px; border-bottom: solid 1px #ccc; padding-bottom: 4px;">
                     <p style="text-align: center">
                    <asp:Label ID="lblPageName" runat="server" Width="552px" CssClass="ContentHead" Text="document"></asp:Label>
                    </p>
                </td>
            </tr>
        </table>
         
     <iv1:NonCameraImageViewer1 ID="nonCameraImageViewer2" runat="server" />
    </div>
    <input id="hd_notIntNO" runat="server" clientidmode="Static" type="hidden" />
    </form>
</body>
</html>

