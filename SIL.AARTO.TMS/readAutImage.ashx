﻿<%@ WebHandler Language="C#" Class="readAutImage" %>

using System;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public class readAutImage : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = ConfigurationManager.ConnectionStrings["TMSConnectionString"].ToString();
        conn.Open();
        string sql = "select AutLogo from authority where AutIntNo=" + context.Request.QueryString["id"];
        SqlCommand comm = new SqlCommand(sql,conn);
        SqlDataReader dr = comm.ExecuteReader(CommandBehavior.CloseConnection);
        if (dr.Read())
        {
            if (string.IsNullOrEmpty(dr["AutLogo"].ToString()) == false)
            {
                context.Response.BinaryWrite((byte[])dr["AutLogo"]);
            }
        }
        conn.Close();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}