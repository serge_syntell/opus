using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class AuthorityRules : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected int userAccessLevel;
		protected string thisPageURL = "AuthorityRules.aspx";
        //protected string thisPage = "Authority Rules Maintenance";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
			int autIntNo = Convert.ToInt32(Session["autIntNo"]);
			
			Session["userLoginName"] = userDetails.UserLoginName;
			userAccessLevel = userDetails.UserAccessLevel;

            ////may need to check user access level here....
            //if (userAccessLevel<10)
            //    btnOptAdd.Visible = false;
            //else
            //    btnOptAdd.Visible = true;

			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddAuthorityRule.Visible = false;
				pnlEditAuthorityRule.Visible = false;
				pnlCopy.Visible = false;
				btnOptHide.Visible = true;

				PopulateAuthorites(ddlSelectLA,  autIntNo);

				if (autIntNo>0)
				{
					BindGrid(autIntNo,txtSearch.Text);
				}
				else
				{
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
					lblError.Visible = true;
				}
			}		
		}

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
		protected void PopulateAuthorites(DropDownList ddlAuthority, int autIntNo)
		{
            //Jerry 2013-10-16 need clear the original DDL firstly, or there has repeat items
            if (ddlAuthority.Items.Count > 0)
                ddlAuthority.Items.Clear();

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");


            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlAuthority.DataSource = data;
            //ddlAuthority.DataValueField = "AutIntNo";
            //ddlAuthority.DataTextField = "AutName";
            //ddlAuthority.DataBind();
			ddlAuthority.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

			//reader.Close();
		}

		protected void BindGrid(int autIntNo, string searchText)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.AuthorityRulesDB authorityRuleList = new Stalberg.TMS.AuthorityRulesDB(connectionString);

            int totalCount = 0;
            DataSet data = authorityRuleList.GetAuthorityRulesListDS(autIntNo, dgAuthorityRulePager.PageSize, dgAuthorityRulePager.CurrentPageIndex, searchText, out totalCount);
			dgAuthorityRule.DataSource = data;
            dgAuthorityRule.DataKeyField = "WFRFAID";
			dgAuthorityRule.DataBind();
            dgAuthorityRulePager.RecordCount = totalCount;

			if (dgAuthorityRule.Items.Count == 0)
			{
				dgAuthorityRule.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
			else
			{
				dgAuthorityRule.Visible = true;
                this.dgAuthorityRulePager.Visible = true;
			}

			data.Dispose();

			pnlAddAuthorityRule.Visible = false;
			pnlEditAuthorityRule.Visible = false;
			pnlCopy.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow authority rules to be added to the database table
			pnlAddAuthorityRule.Visible = true;
			pnlEditAuthorityRule.Visible = false;

            // Jake 2013-09-12 added to hidden pnlCopy
            pnlCopy.Visible = false;

            //Get ddlSelectWFRFAName data
            TList<WorkFlowRuleForAuthority> workFlowRuleForAuthorityList = new WorkFlowRuleForAuthorityService().GetAll();
            ddlSelectWFRFAName.DataSource = workFlowRuleForAuthorityList;
            ddlSelectWFRFAName.DataTextField = "WFRFAName";
            ddlSelectWFRFAName.DataValueField = "WFRFAID";
            ddlSelectWFRFAName.DataBind();

            #region Jerry 2012-05-24 change
            //SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            //List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            //this.ucLanguageLookupAdd.DataBind(entityList);

            //SIL.AARTO.TMS.ReflectionUsualMethod rUMethodComment = new SIL.AARTO.TMS.ReflectionUsualMethod();
            //List<LanguageLookupEntity> entityListComment = rUMethodComment.BindUCLanguageLookup();
            //this.ucLanguageLookupAddComment.DataBind(entityListComment);
            #endregion

            //Get current WFRFAID
            int wFRFAID = Convert.ToInt32(ddlSelectWFRFAName.SelectedValue);
            GetAddWorkFlowRuleForAuthorityByWFRFAID(wFRFAID);
		}

		protected void btnAddAuthorityRule_Click(object sender, System.EventArgs e)
        {
            #region Jerry 2012-05-24 add
            // add the new authority rule to the database table
            //Stalberg.TMS.AuthorityRulesDB authorityRuleAdd = new AuthorityRulesDB(connectionString);
			
            //int addARIntNo = authorityRuleAdd.AddAuthorityRules(Convert.ToInt32(ddlSelectLA.SelectedValue), txtAddARCode.Text,
            //    txtAddARDescr.Text, Convert.ToInt32(txtAddARNumeric.Text), txtAddARString.Text, txtAddARComment.Text,  
            //    loginUser);

            //List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            //SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            //rUMethod.UpdateIntoLookup(addARIntNo, lgEntityList, "AuthorityRulesLookup", "ARIntNo", "ARDescr");

            //if (addARIntNo <= 0)
            //{
            //    //Modefied By Linda 2012-2-29
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            //}
            //else
            //{
            //    //Modefied By Linda 2012-2-29
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            //}

            //lblError.Visible = true;

            //int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            //BindGrid(autIntNo);
            #endregion

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int wFRFAID = Convert.ToInt32(ddlSelectWFRFAName.SelectedValue);

            AuthorityRuleService authorityRuleService = new AuthorityRuleService();
            AuthorityRule authorityRule = authorityRuleService.GetByWfrfaidAutIntNo(wFRFAID, autIntNo);
            if (authorityRule == null)
            {
                //Save AuthorityRule
                authorityRule = new AuthorityRule();
                authorityRule.Wfrfaid = wFRFAID;
                authorityRule.AutIntNo = autIntNo;
                authorityRule.ArNumeric = txtAddARNumeric.Text.Trim() == "" ? 0 : Convert.ToInt32(txtAddARNumeric.Text.Trim());
                authorityRule.ArString = txtAddARString.Text.Trim();
                authorityRule.LastUser = this.loginUser;
                authorityRuleService.Save(authorityRule);

                //save multilanguage
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(wFRFAID, lgEntityList, "WorkFlowRuleForAuthorityDescrLookup", "WFRFAID", "WFRFADescr", this.loginUser);

                //Update Comment multilanguage
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityListComment = this.ucLanguageLookupAddComment.Save();
                rUMethod.UpdateIntoLookup(wFRFAID, lgEntityListComment, "WorkFlowRuleForAuthorityCommentLookup", "WFRFAID", "WFRFAComment", this.loginUser);
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");

               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                int toAutIntNo = Convert.ToInt32(Session["autIntNo"]);
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(toAutIntNo, this.loginUser, PunchStatisticsTranTypeList.AuthorityRules, PunchAction.Add); 

                // Jake 2013-09-12 commented out, let grid stay at current page
                //dgAuthorityRulePager.RecordCount = 0;
                //dgAuthorityRulePager.CurrentPageIndex = 1;
                BindGrid(autIntNo, txtSearch.Text);
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            }
            lblError.Visible = true;
        }

		protected void btnUpdateAuthorityRule_Click(object sender, System.EventArgs e)
		{

			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int wFRFAID = Convert.ToInt32(dgAuthorityRule.DataKeys[dgAuthorityRule.SelectedIndex]);

            #region Jerry 2012-05-24 change
            //Stalberg.TMS.AuthorityRulesDB authorityRuleUpdate = new AuthorityRulesDB(connectionString);
			
            //int updAuthorityRuleIntNo = authorityRuleUpdate.UpdateAuthorityRules(Convert.ToInt32(ddlSelectLA.SelectedValue),
            //    txtARCode.Text, txtARDescr.Text, Convert.ToInt32(txtARNumeric.Text), txtARString.Text, txtARComment.Text,
            //     loginUser, Convert.ToInt32(Session["editARIntNo"]));
            #endregion

            //Update AuthorityRule
            AuthorityRuleService authorityRuleService = new AuthorityRuleService();
            AuthorityRule authorityRule = authorityRuleService.GetByWfrfaidAutIntNo(wFRFAID, autIntNo);

            authorityRule.ArString = txtARString.Text.Trim();
            authorityRule.ArNumeric = txtARNumeric.Text.Trim() == "" ? 0 : Convert.ToInt32(txtARNumeric.Text.Trim());
            authorityRule.LastUser = this.loginUser;
            authorityRuleService.Save(authorityRule);

            //Update Descr multilanguage
            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(wFRFAID, lgEntityList, "WorkFlowRuleForAuthorityDescrLookup", "WFRFAID", "WFRFADescr", this.loginUser);

            //Update Comment multilanguage
            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityListComment = this.ucCommentLanguageLookupUpdate.Save();
            rUMethod.UpdateIntoLookup(wFRFAID, lgEntityListComment, "WorkFlowRuleForAuthorityCommentLookup", "WFRFAID", "WFRFAComment", this.loginUser);

            if (wFRFAID.Equals("-1"))
			{
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
			}
			else
			{
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                //Linda 2012-9-12 add for list report19
                //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
               int toAutIntNo = Convert.ToInt32(Session["autIntNo"]);
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(toAutIntNo, this.loginUser, PunchStatisticsTranTypeList.AuthorityRules, PunchAction.Change);   
                  
			}

			lblError.Visible = true;
            // Jake 2013-09-12 commented out, let grid stay at current page
            //dgAuthorityRulePager.RecordCount = 0;
            //dgAuthorityRulePager.CurrentPageIndex = 1;
            BindGrid(autIntNo, txtSearch.Text);
		}

		protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgAuthorityRule.Visible = true;
			btnOptHide.Text = "Hide list";
            dgAuthorityRulePager.RecordCount = 0;
            dgAuthorityRulePager.CurrentPageIndex = 1;
            BindGrid(Convert.ToInt32(ddlSelectLA.SelectedValue), txtSearch.Text);
		}

        protected void PopulateAuthorityRuleTextBoxes(int wFRFAID)
		{
            #region Jerry 2012-05-24 change
            //// Obtain and bind a list of all users
            //Stalberg.TMS.AuthorityRulesDB authorityRule = new Stalberg.TMS.AuthorityRulesDB(connectionString);
	
            //Stalberg.TMS.AuthorityRulesDetails authorityRuleDetails = authorityRule.GetAuthorityRulesDetails(arIntNo);
			
            //if (arIntNo >0)
            //{
            //    txtARCode.Text = authorityRuleDetails.ARCode;
            //    txtARString.Text = authorityRuleDetails.ARString;
            //    txtARNumeric.Text = authorityRuleDetails.ARNumeric.ToString();
            //    txtARDescr.Text = authorityRuleDetails.ARDescr;
            //    txtARComment.Text = authorityRuleDetails.ARComment;

            //    pnlEditAuthorityRule.Visible = true;

            //    //if (userAccessLevel<10)
            //    //{
            //    //    //make text boxes read-only
            //    //    txtARCode.ReadOnly = true;
            //    //    txtARString.ReadOnly = true;
            //    //    txtARDescr.ReadOnly = true;
            //    //    txtARNumeric.ReadOnly = true;
            //    //    txtARComment.ReadOnly = true;
            //    //}
            //    //else
            //    //{
            //    //    txtARCode.ReadOnly = false;
            //    //    txtARString.ReadOnly = false;
            //    //    txtARDescr.ReadOnly = false;
            //    //    txtARNumeric.ReadOnly = false;
            //    //    txtARComment.ReadOnly = false;
            //    //}
            //}
            //else
            //{
            //    //Modefied By Linda 2012-2-29
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            //    lblError.Visible = true;
            //    pnlEditAuthorityRule.Visible = false;
            //}
            #endregion

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            WorkFlowRuleForAuthority workFlowRuleForAuthority = new WorkFlowRuleForAuthorityService().GetByWfrfaid(wFRFAID);

            AuthorityRuleService authorityRuleService = new AuthorityRuleService();
            AuthorityRule authorityRule = authorityRuleService.GetByWfrfaidAutIntNo(wFRFAID, autIntNo);
            if (authorityRule != null && workFlowRuleForAuthority != null)
            {
                //get from WorkFlowRuleForAuthority table
                txtARCode.Text = workFlowRuleForAuthority.WfrfaName;
                txtARDescr.Text = workFlowRuleForAuthority.WfrfaDescr;
                txtARComment.Text = workFlowRuleForAuthority.WfrfaComment;
                //get from AuthorityRule table
                txtARString.Text = authorityRule.ArString;
                txtARNumeric.Text = authorityRule.ArNumeric.ToString();

                pnlEditAuthorityRule.Visible = true;
            }
            else
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                lblError.Visible = true;
                pnlEditAuthorityRule.Visible = false;
            }
		}

		protected void dgAuthorityRule_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			dgAuthorityRule.SelectedIndex = e.Item.ItemIndex;

			if (dgAuthorityRule.SelectedIndex > -1) 
			{			
				pnlAddAuthorityRule.Visible = false;
                //Jake 2013-09-12 added 
                this.pnlCopy.Visible = false;

                int wFRFAID = Convert.ToInt32(dgAuthorityRule.DataKeys[dgAuthorityRule.SelectedIndex]);

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(wFRFAID.ToString(), "WorkFlowRuleForAuthorityDescrLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityListComment = rUMethod.BindUCLanguageLookupByID(wFRFAID.ToString(), "WorkFlowRuleForAuthorityCommentLookup");
                this.ucCommentLanguageLookupUpdate.DataBind(entityListComment);

                PopulateAuthorityRuleTextBoxes(wFRFAID);
			}
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgAuthorityRule.Visible.Equals(true))
			{
				//hide it
				dgAuthorityRule.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1"); ;
                // Jake 2013-09-12 added, hidden pager control  
                dgAuthorityRulePager.Visible = false;
			}
			else
			{
				//show it
				dgAuthorityRule.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
                dgAuthorityRulePager.Visible = true;
			}
		}

		protected void btnOptCopy_Click(object sender, System.EventArgs e)
		{
			pnlAddAuthorityRule.Visible = false;
			pnlEditAuthorityRule.Visible = false;
			pnlCopy.Visible = true;

			//int 
			int autIntNo = Convert.ToInt32(Session["autIntNo"]);

			PopulateAuthorites(ddlAutFrom,  autIntNo);
			PopulateAuthorites(ddlAutTo,  autIntNo);

		}

		protected void btnCopyAuthorityRules_Click(object sender, System.EventArgs e)
		{
			int fromAutIntNo = Convert.ToInt32(ddlAutFrom.SelectedValue);
			int toAutIntNo = Convert.ToInt32(ddlAutTo.SelectedValue);

			if (fromAutIntNo == toAutIntNo)
			{
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
				return;
            }

            #region Jerry 2012-05-24 change
            //AuthorityRulesDB authorityRule = new AuthorityRulesDB(connectionString);

            //int copyAutIntNo = authorityRule.CopyAuthorityRules(fromAutIntNo, autIntNo, loginUser);

            //if (copyAutIntNo == -1)
            //    //Modefied By Linda 2012-2-29
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            //else if (copyAutIntNo == -2)
            //    //Modefied By Linda 2012-2-29
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            //else if (copyAutIntNo == 0)
            //    //Modefied By Linda 2012-2-29
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            //else
            //    //Modefied By Linda 2012-2-29
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            #endregion

            //Jerry 2013-10-16 change the logic, allow the user to copy all authority rules that do not exist for the "to" authority
            AuthorityRuleService authorityRuleService = new AuthorityRuleService();
            //TList<AuthorityRule> toAuthorityRuleList = authorityRuleService.GetByAutIntNo(toAutIntNo);
            //if (toAuthorityRuleList.Count > 0)
            //{
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            //    return;
            //}

            TList<AuthorityRule> fromAuthorityRuleList = authorityRuleService.GetByAutIntNo(fromAutIntNo);
            if (fromAuthorityRuleList.Count == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                return;
            }

            #region Jerry 2013-10-16 change the logic, allow the user to copy all authority rules that do not exist for the "to" authority
            //Copy
            //foreach (AuthorityRule authorityRule in fromAuthorityRuleList)
            //{
            //    AuthorityRule newAuthorityRule = new AuthorityRule();
            //    newAuthorityRule.Wfrfaid = authorityRule.Wfrfaid;
            //    newAuthorityRule.AutIntNo = toAutIntNo;
            //    newAuthorityRule.ArString = authorityRule.ArString;
            //    newAuthorityRule.ArNumeric = authorityRule.ArNumeric;
            //    newAuthorityRule.LastUser = this.loginUser;

            //    authorityRuleService.Save(newAuthorityRule);
            //}
            #endregion
            
            //Copy
            foreach (AuthorityRule fromAuthorityRule in fromAuthorityRuleList)
            {
                AuthorityRule toAuthorityRule = authorityRuleService.GetByWfrfaidAutIntNo(fromAuthorityRule.Wfrfaid, toAutIntNo);
                if (toAuthorityRule == null)
                {
                    toAuthorityRule = new AuthorityRule();
                    toAuthorityRule.Wfrfaid = fromAuthorityRule.Wfrfaid;
                    toAuthorityRule.AutIntNo = toAutIntNo;
                    toAuthorityRule.ArString = fromAuthorityRule.ArString;
                    toAuthorityRule.ArNumeric = fromAuthorityRule.ArNumeric;
                    toAuthorityRule.LastUser = this.loginUser;

                    authorityRuleService.Save(toAuthorityRule);
                }
            }
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(toAutIntNo, this.loginUser, PunchStatisticsTranTypeList.AuthorityRules, PunchAction.Add); 
            lblError.Text = (string)GetLocalResourceObject("lblError.Text11");

            // Iris 20140310 changed for showing the selected Authority list in current page when complete 'copy'.
            // toAutIntNo = Convert.ToInt32(Session["autIntNo"]);
            toAutIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            // Jake 2013-09-12 commented out, let grid stay at current page
            //dgAuthorityRulePager.RecordCount = 0;
            //dgAuthorityRulePager.CurrentPageIndex = 1;
            BindGrid(toAutIntNo, txtSearch.Text);
		}

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnPrintAuthorityRules_Click(object sender, System.EventArgs e)
        {
            if (this.ddlSelectLA.SelectedValue.Equals(string.Empty) || this.ddlSelectLA.SelectedIndex < 0)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                return;
            }

            //else if (this.dateFrom.Value.ToString () == string.Empty)
            //{
            //    this.lblError.Text = "You must enter a value for at least of the search criteria";
            //    return;
            //}

            this.lblError.Text = string.Empty;

            int autIntNo = int.Parse(this.ddlSelectLA.SelectedValue);
            Helper_Web.BuildPopup(this, "ViewAuthorityRules_Report.aspx", "autintno", autIntNo.ToString(), "printType", "ViewAuthorityRules");
            
        }

        /// <summary>
        /// Jerry 2012-05-24 get information by you selected WFRFAName
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSelectWFRFAName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Get current WFRFAID
            int wFRFAID = Convert.ToInt32(ddlSelectWFRFAName.SelectedValue);
            GetAddWorkFlowRuleForAuthorityByWFRFAID(wFRFAID);
        }

        /// <summary>
        /// Jerry 2012-05-24 get information by you selected WFRFAID
        /// </summary>
        /// <param name="wFRFAID"></param>
        private void GetAddWorkFlowRuleForAuthorityByWFRFAID(int wFRFAID)
        {
            if (wFRFAID > 0)
            {
                //Bind des and comment
                WorkFlowRuleForAuthority workFlowRuleForAuthority = new WorkFlowRuleForAuthorityService().GetByWfrfaid(wFRFAID);
                txtAddARDescr.Text = workFlowRuleForAuthority.WfrfaDescr;
                txtAddARComment.Text = workFlowRuleForAuthority.WfrfaComment;

                //Bind Multi language
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(wFRFAID.ToString(), "WorkFlowRuleForAuthorityDescrLookup");
                this.ucLanguageLookupAdd.DataBind(entityList);

                List<LanguageLookupEntity> entityListComment = rUMethod.BindUCLanguageLookupByID(wFRFAID.ToString(), "WorkFlowRuleForAuthorityCommentLookup");
                this.ucLanguageLookupAddComment.DataBind(entityListComment);
            }

            txtAddARNumeric.Text = "0";
            txtAddARString.Text = "";
        }

        protected void dgAuthorityRulePager_PageChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, txtSearch.Text);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo,txtSearch.Text);
        }
}
}
