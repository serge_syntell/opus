using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the end of day cash box balancing page
    /// </summary>
    public partial class EODChequeReportViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        Stalberg.TMS.UserDetails userDetails = new UserDetails();
        private int autIntNo = 0;
        private DateTime dtStart;
        private DateTime dtEnd;
        private string optionDescr = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;

        protected string thisPageURL = "EODChequeReportViewer.aspx";
        private const string DATE_FORMAT = "yyyy-MM-dd";
        //protected string thisPage = "EOD Cheque Report Viewer";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);

            userDetails = (UserDetails)Session["userDetails"];

            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (Request.QueryString["StartDate"] == null)
            {
                Response.Redirect("Default.aspx");
                return;
            }

            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (Request.QueryString["StartDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            if (Request.QueryString["EndDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error1");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            dtStart = DateTime.Parse(Request.QueryString["StartDate"].ToString());
            dtEnd = DateTime.Parse(Request.QueryString["EndDate"].ToString());
            int option = int.Parse(Request.QueryString["Option"].ToString());

            optionDescr = option == 0 ? "BOTH" : "SUMMARY";

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "EOD_Cheque");
            if (reportPage.Equals(""))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "EOD_Cheque.rpt", "EODCheque", "system", "");
                reportPage = "EOD_Cheque.rpt";
            }

            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "EOD_Cheque");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error2"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                //dls 081117 - we can only check that the template path exists if there is actually a template!
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error3"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            ReportDocument report = new ReportDocument();
            report.Load(reportPath);

            SqlDataAdapter daDetails = new SqlDataAdapter();
            daDetails.SelectCommand = new SqlCommand("EODChequeDetails", new SqlConnection(this.connectionString));
            daDetails.SelectCommand.CommandType = CommandType.StoredProcedure;
            daDetails.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            daDetails.SelectCommand.Parameters.Add("@StartDate", SqlDbType.SmallDateTime).Value = dtStart;
            daDetails.SelectCommand.Parameters.Add("@EndDate", SqlDbType.SmallDateTime).Value = dtEnd;
            daDetails.SelectCommand.Parameters.Add("@Option", SqlDbType.TinyInt).Value = option;

            dsEODCheque dsEODChequeDetails = new dsEODCheque();
            dsEODChequeDetails.DataSetName = "dsEODCheque";
            try
            {
                daDetails.Fill(dsEODChequeDetails);
            }
            catch
            {
                Response.Write((string)GetLocalResourceObject("strMsgWrite"));
                return;
            }
            finally
            {
                daDetails.Dispose();
            }

            report.SetDataSource(dsEODChequeDetails.Tables[1]);
            dsEODChequeDetails.Dispose();

            //set filter subreport data
            string filterParameters = string.Format("FILTER PARAMETERS:\tDate Start:  {0}\tDate End:  {1}\tOption:  {2}\n\t\t\t\t\tProduced By:  {3} on {4}", 
                string.Format("{0:yyyy-MM-dd}", dtStart), string.Format("{0:yyyy-MM-dd}", dtEnd),
                optionDescr, userDetails.UserInit + " " + userDetails.UserSName, DateTime.Today.ToString(DATE_FORMAT));

            dsFilterParameters dsFilterParameters = new dsFilterParameters();
            DataRow row = dsFilterParameters.Tables[0].NewRow();
            row["FilterParameters"] = filterParameters;
            dsFilterParameters.Tables[0].Rows.Add(row);

            report.Subreports["FilterParameters"].SetDataSource(dsFilterParameters.Tables[0]);
            dsFilterParameters.Dispose();

            //set summary subreport data
            SqlDataAdapter daSummary = new SqlDataAdapter();
            daSummary.SelectCommand = new SqlCommand("EODChequeSummary", new SqlConnection(this.connectionString));
            daSummary.SelectCommand.CommandType = CommandType.StoredProcedure;
            daSummary.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            daSummary.SelectCommand.Parameters.Add("@StartDate", SqlDbType.SmallDateTime).Value = dtStart;
            daSummary.SelectCommand.Parameters.Add("@EndDate", SqlDbType.SmallDateTime).Value = dtEnd;

            dsChequeSummary dsEODChequeSummary = new dsChequeSummary();
            dsEODChequeSummary.DataSetName = "dsChequeSummary";

            try
            {
                daSummary.Fill(dsEODChequeSummary);
            }
            catch
            {
                Response.Write((string)GetLocalResourceObject("strMsgWrite"));
                return;
            }
            finally
            {
                daSummary.Dispose();
            }

            report.Subreports["EOD_ChequeSummary"].SetDataSource(dsEODChequeSummary.Tables[1]);
            dsEODChequeSummary.Dispose();

            try
            {
                // Export the pdf file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat);
                //report.Dispose();

                // Flush the PDF into the response stream
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                daSummary = null;
                daDetails = null;

                if (report != null)
                    report.Dispose();
            }

        }
    }
}