using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.Text;
using Stalberg.TMS.Data.Datasets;
using ceTe.DynamicPDF.Merger;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents  viewer page for the Representation letter
    /// </summary>
    public partial class Representation_LetterViewer : DplxWebForm // System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string sLetterTo = string.Empty;
        private string sTicketNo = string.Empty;
        private int repIntNo = -1;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPage = "Representation Result Letter";
        protected string thisPageURL = "Representation_LetterViewer.aspx";

        //2011-10-18 jerry add
        private string printFileName = string.Empty;
        private bool printFlag = false;

        #region 2012-02-15 jerry disabled for backup
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    // Set domain specific variables
        //    General gen = new General();
        //    backgroundImage = gen.SetBackground(Session["drBackground"]);
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    //2011-10-18 jerry add
        //    if (Request.QueryString["PrintFileName"] != null && Request.QueryString["PrintFlag"] != null)
        //    {
        //        this.printFileName = Request.QueryString["PrintFileName"].ToString();
        //        string tempPrintFlag = Request.QueryString["PrintFlag"].ToString();
        //        this.printFlag = tempPrintFlag == "N" ? false : true;
        //    }
        //    else
        //    {
        //        if (Request.QueryString["RepIntNo"] == null)
        //        {
        //            Response.Redirect("Login.aspx");
        //            return;
        //        }
        //        this.repIntNo = int.Parse(Request.QueryString["RepIntNo"].ToString());
        //        this.sLetterTo = Request.QueryString["LetterTo"].ToString();
        //        if (Request.QueryString["TicketNo"] != null)
        //        {
        //            this.sTicketNo = Request.QueryString["TicketNo"].ToString();
        //            //BD: going to pass through ticket number and RepIntNo  or SRIntNo
        //            //this.repIntNo = -1;
        //        }
        //    }

        //    //get user details
        //    Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
        //    Stalberg.TMS.UserDetails userDetails = new UserDetails();
        //    userDetails = (UserDetails)Session["userDetails"];

        //    if (Request.QueryString["AutIntNo"] == null)
        //        autIntNo = Convert.ToInt32(Session["autIntNo"]);
        //    else if (!int.TryParse (Request.QueryString["AutIntNo"].ToString(), out this.autIntNo))
        //        autIntNo = Convert.ToInt32(Session["autIntNo"]);

        //    byte[] buffer = null;
        //    byte[] bufferTemplate = null;

        //    // Setup the report
        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);
        //    string reportPage = arn.GetAuthReportName(this.autIntNo, "RepresentationLetter");
        //    if (reportPage.Equals(string.Empty))
        //    {
        //        arn.AddAuthReportName(autIntNo, "RepresentationLetter_PL.dplx", 
        //            "RepresentationLetter", "system", "");
        //        reportPage = "RepresentationLetter_PL.dplx";
        //    }

        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "RepresentationLetter");
        //    string path = Server.MapPath("reports/" + reportPage);

        //    //****************************************************
        //    //SD:  20081120 - check that report actually exists
        //    string templatePath = string.Empty;

        //    if (!File.Exists(path))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {

        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //****************************************************

        //    DocumentLayout doc = new DocumentLayout(path);
        //    Query query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;

        //    RecordArea recordChanged = (RecordArea)doc.GetElementById("recordChanged");
        //    RecordArea recordUnsuccessful = (RecordArea)doc.GetElementById("recordUnsuccessful");
        //    RecordArea recordWithdrawn = (RecordArea)doc.GetElementById("recordWithdrawn");
        //    RecordArea recordName = (RecordArea)doc.GetElementById("recordName");
        //    RecordArea recordEnglishOffence = (RecordArea)doc.GetElementById("recordEnglishOffence");
        //    RecordArea recordAfrikaansOffence = (RecordArea)doc.GetElementById("recordAfrikaansOffence");
        //    RecordArea recordOriginalAmount = (RecordArea)doc.GetElementById("recordOriginalAmount");
        //    RecordArea recordSumCourtDate = (RecordArea)doc.GetElementById("recordSumCourtDate");

        //    recordChanged.LaidOut += new RecordAreaLaidOutEventHandler(ph_LaidOut);
        //    recordUnsuccessful.LaidOut += new RecordAreaLaidOutEventHandler(recordUnsuccessful_LaidOut);
        //    recordWithdrawn.LaidOut += new RecordAreaLaidOutEventHandler(recordWithdrawn_LaidOut);
        //    recordName.LayingOut += new LayingOutEventHandler(recordName_LayingOut);
        //    recordEnglishOffence.LayingOut += new LayingOutEventHandler(recordEnglishOffence_LayingOut);
        //    recordAfrikaansOffence.LayingOut += new LayingOutEventHandler(recordAfrikaansOffence_LayingOut);
        //    recordOriginalAmount.LayingOut += new LayingOutEventHandler(recordOriginalAmount_LayingOut);

        //    if (recordSumCourtDate != null)
        //        recordSumCourtDate.LayingOut += new LayingOutEventHandler(recordSumCourtDate_LayingOut);

        //    //2011-10-18 jerry change
        //    if (string.IsNullOrEmpty(this.printFileName))
        //    {
        //        ParameterDictionary parameters = new ParameterDictionary();
        //        parameters.Add("RepIntNo", repIntNo);
        //        parameters.Add("NotTicketNo", sTicketNo);
        //        parameters.Add("AutIntNo", Int32.Parse(Session["AutIntNo"].ToString()));

        //        if (!sTemplate.Equals(""))
        //        {
        //            DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
        //            Query queryTemplate = (Query)template.GetQueryById("Query");
        //            queryTemplate.ConnectionString = this.connectionString;
        //            ParameterDictionary parametersTemplate = new ParameterDictionary();
        //            Document reportTemplate = template.Run(parametersTemplate);
        //            bufferTemplate = reportTemplate.Draw();
        //        }

        //        Document report = doc.Run(parameters);
        //        ImportedPageArea importedPage;
        //        if (!sTemplate.Equals(""))
        //        {
        //            if (sTemplate.ToLower().IndexOf(".dplx") > 0)
        //            {
        //                PdfDocument pdf = new PdfDocument(bufferTemplate);
        //                PdfPage page = pdf.Pages[0];
        //                importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
        //            }
        //            else
        //            {
        //                //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //            }

        //            ceTe.DynamicPDF.Page rptPage = report.Pages[0];
        //            rptPage.Elements.Insert(0, importedPage);
        //        }

        //        buffer = report.Draw();
        //    }
        //    else
        //    {
        //        RepresentationDB represent = new RepresentationDB(this.connectionString);
        //        SqlDataReader reader = represent.GetRepInfoByPrintFile(this.printFileName, this.printFlag);

        //        MergeDocument merge = new MergeDocument();
        //        Document report;

        //        while (reader.Read())
        //        {
        //            ParameterDictionary parameters = new ParameterDictionary();
        //            parameters.Add("RepIntNo", Convert.ToInt32(reader["RepIntNo"]));
        //            parameters.Add("NotTicketNo", reader["NotTicketNo"]);
        //            parameters.Add("AutIntNo", Int32.Parse(Session["AutIntNo"].ToString()));

        //            if (!sTemplate.Equals(""))
        //            {
        //                DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
        //                Query queryTemplate = (Query)template.GetQueryById("Query");
        //                queryTemplate.ConnectionString = this.connectionString;
        //                ParameterDictionary parametersTemplate = new ParameterDictionary();
        //                Document reportTemplate = template.Run(parametersTemplate);
        //                bufferTemplate = reportTemplate.Draw();
        //            }

        //            report = doc.Run(parameters);
        //            ImportedPageArea importedPage;
        //            if (!sTemplate.Equals(""))
        //            {
        //                if (sTemplate.ToLower().IndexOf(".dplx") > 0)
        //                {
        //                    PdfDocument pdf = new PdfDocument(bufferTemplate);
        //                    PdfPage page = pdf.Pages[0];
        //                    importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
        //                }
        //                else
        //                {
        //                    //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                    importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                }

        //                ceTe.DynamicPDF.Page rptPage = report.Pages[0];
        //                rptPage.Elements.Insert(0, importedPage);
        //            }
        //            buffer = report.Draw();
        //            merge.Append(new PdfDocument(buffer));

        //            buffer = null;
        //            bufferTemplate = null;
        //        }

        //        buffer = merge.Draw();
        //    }

            

        //    recordChanged.LaidOut -= new RecordAreaLaidOutEventHandler(ph_LaidOut);
        //    recordUnsuccessful.LaidOut -= new RecordAreaLaidOutEventHandler(recordUnsuccessful_LaidOut);
        //    recordWithdrawn.LaidOut -= new RecordAreaLaidOutEventHandler(recordWithdrawn_LaidOut);
        //    recordName.LayingOut -= new LayingOutEventHandler(recordName_LayingOut);
        //    recordEnglishOffence.LayingOut -= new LayingOutEventHandler(recordEnglishOffence_LayingOut);
        //    recordAfrikaansOffence.LayingOut -= new LayingOutEventHandler(recordAfrikaansOffence_LayingOut);
        //    recordOriginalAmount.LayingOut -= new LayingOutEventHandler(recordOriginalAmount_LayingOut);

        //    if (recordSumCourtDate != null)
        //        recordSumCourtDate.LayingOut -= new LayingOutEventHandler(recordSumCourtDate_LayingOut);

        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/pdf";
        //    Response.BinaryWrite(buffer);
        //    Response.End();

        //}

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //2011-10-18 jerry add
            if (Request.QueryString["PrintFileName"] != null && Request.QueryString["PrintFlag"] != null)
            {
                this.printFileName = Request.QueryString["PrintFileName"].ToString();
                string tempPrintFlag = Request.QueryString["PrintFlag"].ToString();
                this.printFlag = tempPrintFlag == "N" ? false : true;
            }
            else
            {
                if (Request.QueryString["RepIntNo"] == null)
                {
                    Response.Redirect("Login.aspx");
                    return;
                }
                this.repIntNo = int.Parse(Request.QueryString["RepIntNo"].ToString());
                this.sLetterTo = Request.QueryString["LetterTo"].ToString();
                if (Request.QueryString["TicketNo"] != null)
                {
                    this.sTicketNo = Request.QueryString["TicketNo"].ToString();
                    //BD: going to pass through ticket number and RepIntNo  or SRIntNo
                    //this.repIntNo = -1;
                }
            }

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];

            if (Request.QueryString["AutIntNo"] == null)
                autIntNo = Convert.ToInt32(Session["autIntNo"]);
            else if (!int.TryParse(Request.QueryString["AutIntNo"].ToString(), out this.autIntNo))
                autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //jerry 2012-02-15, use module to create print pdf file
            PrintFileProcess process = new PrintFileProcess(this.connectionString, userDetails.UserLoginName);
            process.BuildPrintFile(new PrintFileModuleRepresentationResultLetter(printFlag, repIntNo,  sLetterTo, sTicketNo), this.autIntNo, this.printFileName);

            if (Request.QueryString["printType"] != null && Request.QueryString["printType"] == "PrintRepresentationLetter")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                string lastuser = userDetails != null ? userDetails.UserLoginName : "";
                int _autIntNo = Session["printAutIntNo"] == null ? autIntNo : Convert.ToInt32(Session["printAutIntNo"]);
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, lastuser, PunchStatisticsTranTypeList.PrintRepresentationLetter, PunchAction.Change);

            }

        }

        //private void recordAfrikaansOffence_LayingOut(object sender, LayingOutEventArgs e)
        //{
        //    RecordArea lbl = (RecordArea)sender;

        //    //check to proc for which reg no is getting used here. 
        //    //Use OrigRegNo if ChangeOfRegNo=Y and LetterTo=O
        //    string regNo = e.LayoutWriter.RecordSets.Current["NotRegNo"].ToString().Trim();
        //    string speedZone = e.LayoutWriter.RecordSets.Current["NotSpeedLimit"].ToString().Trim();
        //    string speed = e.LayoutWriter.RecordSets.Current["MinSpeed"].ToString().Trim();

        //    string description = e.LayoutWriter.RecordSets.Current["OcTDescr1"].ToString().Trim();
        //    // Remove the first sentence
        //    int pos = description.IndexOf('.');
        //    if (pos >= 0 && pos + 1 < description.Length)
        //        description = description.Substring(pos + 1);

        //    lbl.Text = description.Replace("(X~1)", regNo).Replace("(X~2)", speedZone).Replace("(X~3)", speed);
        //}

        //private void recordEnglishOffence_LayingOut(object sender, LayingOutEventArgs e)
        //{
        //    RecordArea lbl = (RecordArea)sender;

        //    //check to proc for which reg no is getting used here. Use OrigRegNo if ChangeOfRegNo=Y and LetterTo=O
        //    string regNo = e.LayoutWriter.RecordSets.Current["NotRegNo"].ToString().Trim();
        //    string speedZone = e.LayoutWriter.RecordSets.Current["NotSpeedLimit"].ToString().Trim();
        //    string speed = e.LayoutWriter.RecordSets.Current["MinSpeed"].ToString().Trim();

        //    string description = e.LayoutWriter.RecordSets.Current["OcTDescr"].ToString().Trim();
        //    // Remove the first sentence
        //    int pos = description.IndexOf('.');
        //    if (pos >= 0 && pos + 1 < description.Length)
        //        description = description.Substring(pos + 1);

        //    lbl.Text = description.Replace("(X~1)", regNo).Replace("(X~2)", speedZone).Replace("(X~3)", speed);
        //}

        //private void recordOriginalAmount_LayingOut(object sender, LayingOutEventArgs e)
        //{
        //    RecordArea lbl = (RecordArea)sender;

        //    decimal originalAmount = Convert.ToDecimal(e.LayoutWriter.RecordSets.Current["ChgFineAmount"]);
        //    if (originalAmount == 999999)
        //        lbl.Text = "[ No AoG ]";
        //    else
        //        lbl.Text = e.LayoutWriter.RecordSets.Current["OriginalAmount"].ToString();
        //}

        //private void recordSumCourtDate_LayingOut(object sender, LayingOutEventArgs e)
        //{
        //    RecordArea lbl = (RecordArea)sender;

        //    DateTime sumCourtDate;
        //    string crtDate = e.LayoutWriter.RecordSets.Current["SumCourtDate"].ToString();

        //    if (!DateTime.TryParse(crtDate, out sumCourtDate))
        //        lbl.Text = string.Empty;
        //    else
        //        lbl.Text = string.Format("{0:yyyy/MM/dd}", sumCourtDate);
        //}

        //void recordName_LayingOut(object sender, LayingOutEventArgs e)
        //{
        //    RecordArea lbl = (RecordArea)sender;
        //    StringBuilder sb = new StringBuilder();
        //    string temp = string.Empty;

        //    object o = e.LayoutWriter.RecordSets.Current["NotProxyFlag"];
        //    if(o==null)
        //        return;

        //    string proxyFlag =o.ToString().Trim();
        //    bool isProxy = proxyFlag.Equals("Y", StringComparison.InvariantCultureIgnoreCase);

        //    //dls 070730 - first need to check whether to send letter to owner/driver, and only check for Proxy if its going to the owner\
        //    // LMZ 2007-07-27 change to allow switch to effect owner driver printing
        //    if (this.sLetterTo == "O")  // Owner (using O and D instead of 0 & 1)
        //    {
        //        if (isProxy) //Proxy
        //        {
        //            temp = e.LayoutWriter.RecordSets.Current["PrxInitials"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append(' ');
        //            }
        //            sb.Append(e.LayoutWriter.RecordSets.Current["PrxSurname"].ToString().Trim());
        //            sb.Append("\n");
        //            temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd1"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd2"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd3"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd4"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["PrxPOAdd5"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["PrxPOCode"].ToString().Trim();
        //            if (temp.Length > 0)
        //                sb.Append(temp);
        //        }
        //        else  // owner
        //        {
        //            temp = e.LayoutWriter.RecordSets.Current["OwnInitials"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append(' ');
        //            }
        //            sb.Append(e.LayoutWriter.RecordSets.Current["OwnSurname"].ToString().Trim());
        //            sb.Append("\n");
        //            temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd1"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd2"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd3"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd4"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["OwnPOAdd5"].ToString().Trim();
        //            if (temp.Length > 0)
        //            {
        //                sb.Append(temp);
        //                sb.Append("\n");
        //            }
        //            temp = e.LayoutWriter.RecordSets.Current["OwnPOCode"].ToString().Trim();
        //            if (temp.Length > 0)
        //                sb.Append(temp);
        //        }
        //    }
        //    else if (sLetterTo == "D") // driver
        //    {
        //        temp = e.LayoutWriter.RecordSets.Current["DrvInitials"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append(' ');
        //        }
        //        sb.Append(e.LayoutWriter.RecordSets.Current["DrvSurname"].ToString().Trim());
        //        sb.Append("\n");
        //        temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd1"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd2"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd3"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd4"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["DrvPOAdd5"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["DrvPOCode"].ToString().Trim();
        //        if (temp.Length > 0)
        //            sb.Append(temp);
        //    }
        //    else if (sLetterTo == "S") // Summons - Accused
        //    {
        //        temp = e.LayoutWriter.RecordSets.Current["AccInitials"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append(" ");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["AccSurname"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["AccPOAdd1"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["AccPOAdd2"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["AccPOAdd3"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["AccPOAdd4"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["AccPOAdd5"].ToString().Trim();
        //        if (temp.Length > 0)
        //        {
        //            sb.Append(temp);
        //            sb.Append("\n");
        //        }
        //        temp = e.LayoutWriter.RecordSets.Current["AccPOCode"].ToString().Trim();
        //        if (temp.Length > 0)
        //            sb.Append(temp);
        //    }

        //    lbl.Text = sb.ToString();
        //}

        //void recordWithdrawn_LaidOut(object sender, RecordAreaLaidOutEventArgs e)
        //{
        //    object o = e.LayoutWriter.RecordSets.Current["RepresentationCode"];
        //    if (o == null)
        //        return;

        //    string repCode = o.ToString().Trim();

        //    switch (repCode)
        //    {
        //        //case "200": // Summons
        //        case "13":      // Summons
        //        case "1":
        //        case "3":
        //            e.ReportTextArea.TextColor = CmykColor.Black;
        //            return;
        //    }
        //    e.ReportTextArea.TextColor = CmykColor.White;
        //}

        //void recordUnsuccessful_LaidOut(object sender, RecordAreaLaidOutEventArgs e)
        //{
        //    object o = e.LayoutWriter.RecordSets.Current["RepresentationCode"];
        //    if (o == null)
        //        return;

        //    string repCode = o.ToString().Trim();
        //    switch (repCode)
        //    {
        //        //case "220": // Summons
        //        case "15": // Summons
        //        case "5":
        //            e.ReportTextArea.TextColor = CmykColor.Black;
        //            return;
        //    }
        //    e.ReportTextArea.TextColor = CmykColor.White;
        //}

        //void ph_LaidOut(object sender, RecordAreaLaidOutEventArgs e)
        //{
        //    // Reduced Fine
        //    object o = e.LayoutWriter.RecordSets.Current["RepresentationCode"];
        //    if (o == null)
        //        return;

        //    string repCode = o.ToString().Trim();
        //    switch (repCode)
        //    {
        //        //case "210": // Summons
        //        case "14": // Summons      
        //        case "4":
        //            e.ReportTextArea.TextColor = CmykColor.Black;
        //            return;
        //    }
        //    e.ReportTextArea.TextColor = CmykColor.White;
        //}


    }
}
