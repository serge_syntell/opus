<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.NoticeReprint_Search" Codebehind="NoticeReprint_Search.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>
<%@ Register Src="EasyPayNumberSearch.ascx" TagName="EasyPayNumberSearch" TagPrefix="uc2" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
 
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0" rightmargin="0" >
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
          
                            </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" style="text-align: center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
        <tr>
            <td align="center" valign="top" rowspan="2">
                <img style="height: 1px" src="images/1x1.gif" width="167" />
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center">
                                <asp:Label ID="Label9" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" height="21">
                                 </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" colspan="1" style="width: 879px">
                <table cellspacing="0" cellpadding="0" border="0" height="90%" width="100%">
                    <tr>
                        <td valign="top" style="height: 49px">
                            <p style="text-align: center">
                                <asp:Label ID="lblPageName" runat="server" Width="552px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                            <p style="text-align: center">
                                &nbsp;</p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="text-align: center">
                            &nbsp;<br />
                            
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <table width="800">
                                <tr>
                                    <td style="text-align: center; height: 21px">
                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSearchTip.Text %>"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="height: 241px">
                                        <table style="vertical-align: top;">
                                            <tr>
                                                <td style="width: 200px">
                                                    <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Text="<%$Resources:lblRegistrationno.Text %>"></asp:Label></td>
                                                <td style="width: 541px">
                                                    <asp:TextBox ID="txtRegNo" runat="server" CssClass="Normal" MaxLength="10" Width="215px"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 26px; width: 200px;" valign="middle">
                                                    <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTicketNo.Text %>"></asp:Label></td>
                                                <td align="left" style="width: 541px">
                                                    &nbsp;<uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td style="height: 26px; width: 200px;" valign="middle">
                                                    <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Text="<%$Resources:lblEasyPayNumber.Text %>"></asp:Label></td>
                                                <td style="height: 26px; width: 541px;">
                                                    <uc2:EasyPayNumberSearch ID="EasyPayNumber" runat="server" OnEasyPayNoticeSelected="EasyPayNoticeSelected" />
                                                </td>
                                            </tr>
                                             <tr>
                                                <td style="height: 26px; width: 200px;">
                                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTicNo.Text %>"></asp:Label></td>
                                                <td align="left" style="width: 541px">
                                                    <asp:TextBox ID="txtTicketNo" runat="server" CssClass="Normal" MaxLength="25" Width="215px"></asp:TextBox>
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>" OnClick="btnSearch_Click"
                                                        Width="81px" /></td>
                                            </tr>
                                           <tr>
                                                <td style="width: 200px">
                                                    <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Text="<%$Resources:lblInitialsSurname.Text %>"></asp:Label></td>
                                                <td align="left" style="width: 541px">
                                                    <asp:TextBox ID="txtInitials" runat="server" CssClass="Normal" MaxLength="10" Width="43px"></asp:TextBox>&nbsp;
                                                    <asp:TextBox ID="txtSurname" runat="server" CssClass="Normal" MaxLength="30" Width="160px"></asp:TextBox>
                                                </td>
                                            </tr> 
                                            
                                            <tr>
                                                <td style="width: 200px">
                                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblIDNo.Text %>"></asp:Label></td>
                                                <td align="left" style="width: 541px">
                                                    <asp:TextBox ID="txtIDNo" runat="server" CssClass="Normal" MaxLength="13" Width="215px"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 200px">
                                                    <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSince.Text %>"></asp:Label></td>
                                                <td align="left" style="width: 541px">
                                                 
                                                   <asp:TextBox runat="server" ID="dtpSince" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpSince" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="dtpSince" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqDate.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>  </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 200px">
                                                </td>
                                                <td align="right" style="width: 541px">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlResults" runat="server">
                                            <asp:GridView ID="grvOffenceList" runat="server" AlternatingRowStyle-CssClass="CartListItemAlt"
                                                RowStyle-CssClass="CartListItem" FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead"
                                                ShowFooter="True" Font-Size="8pt" CellPadding="4" GridLines="Vertical" AutoGenerateColumns="False"
                                                OnRowEditing="grvOffenceList_RowEditing" OnRowCreated="grvOffenceList_RowCreated"  OnRowCommand="grvOffenceList_RowCommand">
                                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                                <AlternatingRowStyle CssClass="CartListItemAlt"></AlternatingRowStyle>
                                                <RowStyle CssClass="CartListItem"></RowStyle>
                                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundField DataField="NotIntNo" HeaderText="NotIntNo" Visible="False">
                                                        <ItemStyle Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NotTicketNo" HeaderText="<%$Resources:grvOffenceList.HeaderText %>">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="<%$Resources:grvOffenceList.HeaderText1 %>">
                                                        <EditItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <ItemStyle VerticalAlign="Top" Wrap="False" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:grvOffenceList.HeaderText2 %>">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AutName" HeaderText="<%$Resources:grvOffenceList.HeaderText3 %>">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ChgOffenceCode" HeaderText="<%$Resources:grvOffenceList.HeaderText4 %>">
                                                        <ItemStyle VerticalAlign="Top" Wrap="False" />
                                                    </asp:BoundField>
                                                     <asp:BoundField DataField="CSCode" HeaderText="<%$Resources:grvOffenceList.HeaderText5 %>">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CSDescr" HeaderText="<%$Resources:grvOffenceList.HeaderText6 %>">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ChgRevFineAmount" DataFormatString="R {0:#, ##0.00}" HeaderText="<%$Resources:grvOffenceList.HeaderText7 %>">
                                                        <ItemStyle VerticalAlign="Top" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Offender" HeaderText="<%$Resources:grvOffenceList.HeaderText8 %>">
                                                        <ItemStyle VerticalAlign="Top" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:CommandField ShowSelectButton="True" HeaderText="<%$Resources:grvOffenceList.HeaderText9 %>" SelectText="View">
                                                        <ItemStyle HorizontalAlign="Right" Wrap="False" VerticalAlign="Top" />
                                                    </asp:CommandField>
                                              </Columns>
                                            </asp:GridView>
                                    <pager:AspNetPager id="grvOffenceListPager" runat="server" 
                                    showcustominfosection="Right" width="400px" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                      FirstPageText="|&amp;lt;" 
                                    LastPageText="&amp;gt;|" 
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                    CustomInfoStyle="float:right;"   PageSize="10" 
                                                onpagechanged="grvOffenceListPager_PageChanged"  UpdatePanelId="UpdatePanel1"></pager:AspNetPager>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlReprintResults" runat="server">
                                            <asp:GridView ID="grvNoticeReprint" runat="server" AlternatingRowStyle-CssClass="CartListItemAlt"
                                                RowStyle-CssClass="CartListItem" FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead"
                                                ShowFooter="True" Font-Size="8pt" CellPadding="4" GridLines="Vertical" AutoGenerateColumns="False"
                                                OnRowCommand="grvNoticeReprint_RowCommand">
                                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                                <AlternatingRowStyle CssClass="CartListItemAlt"></AlternatingRowStyle>
                                                <RowStyle CssClass="CartListItem"></RowStyle>
                                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundField DataField="NRIntNo" HeaderText="NRIntNo" Visible="False">
                                                        <ItemStyle Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NotIntNo" HeaderText="NotIntNo" Visible="False">
                                                        <ItemStyle Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AutIntNo" HeaderText="AutIntNo" Visible="False">
                                                        <ItemStyle Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NotTicketNo" HeaderText="<%$Resources:grvNoticeReprint.HeaderText %>">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NRVersionDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="<%$Resources:grvNoticeReprint.HeaderText1 %>" />
                                                    <asp:BoundField DataField="NotPrint1stNoticeDate" HeaderText="<%$Resources:grvNoticeReprint.HeaderText2 %> " DataFormatString="{0:yyyy-MM-dd}">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NotPrint2ndNoticeDate" HeaderText="<%$Resources:grvNoticeReprint.HeaderText3 %> " DataFormatString="{0:yyyy-MM-dd}">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                   <%-- <asp:BoundField DataField="NRChargeStatus" HeaderText="Charge Status">
                                                    Oscar 2010-12 changed for 4416--%>
                                                    <asp:BoundField DataField="NRChargeStatus" HeaderText="<%$Resources:grvNoticeReprint.HeaderText4 %>">
                                                        <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                    </asp:BoundField>
                                                   <asp:CommandField EditText="View" HeaderImageUrl="Images/Acrobat.png" ShowSelectButton="True" HeaderText="PDF" />
                                                   
                                              </Columns>
                                            </asp:GridView>
                                            <pager:AspNetPager id="grvNoticeReprintPager" runat="server" 
                                            showcustominfosection="Right" width="400px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"   PageSize="10" onpagechanged="grvNoticeReprintPager_PageChanged" UpdatePanelId="UpdatePanel1" 
                                            ></pager:AspNetPager>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
          </td>
          </tr>
          </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 5%;
            width: 100%;">
            <tr>
                <td class="SubContentHeadSmall" valign="top">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
