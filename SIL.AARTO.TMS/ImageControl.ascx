﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ImageControl" Codebehind="ImageControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script type="text/javascript" src="Scripts/Jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="Scripts/Jquery/ui.core.js"></script>
<script type="text/javascript" src="Scripts/Jquery/ui.draggable.js"></script>
<script type="text/javascript" src="Scripts/Jquery/jquery.rightClick.js"></script>
<table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td align="center" valign="middle">
                                <%--put the main image--%>
                                <table cellpadding="0" cellspacing="0" style="font-family:Verdana; font-size:xx-small;">
                                    <tr>
                                        <td style="border: solid 1px black; font-weight:700;">
                                            <asp:Label ID="mainImageTitle" runat="server" Text="<%$Resources:mainImageTitle.Text %>"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input ID="lblImageSettingsSaved" clientidmode="Static" type="text" value="<%$Resources:lblImageSettingsSaved.Value %>" runat="server" style='color:Red; width:500px; font-weight:700;' />&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                             <div id="mainImageDiv" style="width:657px; height:420px; line-height:420px; overflow:hidden; position:relative; ">
                                                <div id="rect" class="Rect">
                                                </div>
                                              </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <span id="lblFeedback" runat="server" style="border-right: black 1px solid; border-top: black 1px solid;
                                    border-left: black 1px solid; width: 657px; border-bottom: black 1px solid;"
                                    class="NormalRed">&nbsp;<asp:Label ID="Label3" runat="server" Text="<%$Resources:lblFeedback.Text %>"></asp:Label></span>
                                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                                    <tr>
                                        <td align="center" valign="bottom" style="height: 24px">
                                            <div style="padding-left:5px;">
                                                <input ID="btnZoom" class="NormalButton" type="button" clientidmode="Static" runat="server" value="<%$Resources:btnZoom.Value %>" style="width: 150px" />&nbsp;
                                                <input id="btnCrossHairs" class="NormalButton" clientidmode="Static" type="button" onclick="CrossHairs();" runat="server" value="<%$Resources:btnCrossHairs.Value %>" style="width: 150px"/>&nbsp;
                                                <input id="btnCenter" class="NormalButton" clientidmode="Static" type="button" runat="server"  value="<%$Resources:btnCenter.Value %>" style="width: 150px" />&nbsp;
                                                <input ID="btnZoomOriginalImage" clientidmode="Static" class="NormalButton" type="button" onclick="OriginalImage();" runat="server" value="<%$Resources:btnZoomOriginalImage.Value %>" style="width:150px"/>
                                                <br />
                                                <input id="btnRbZoom" clientidmode="Static" class="NormalButton" type="button" runat="server" value="<%$Resources:btnRbZoom.Value %>" 
                                                    style="width: 150px;" />&nbsp;
                                                <input id="btnCrossHairsHide" clientidmode="Static" class="NormalButton" type="button" onclick="HideCrossHairs();" runat="server" value="<%$Resources:btnCrossHairsHide.Value %>" style="width: 150px"/>&nbsp;
                                                <input ID="btnInvert" clientidmode="Static" class="NormalButton" type="button" onclick="Invert();" runat="server" value="<%$Resources:btnInvert.Value %>" style="width:150px"/>&nbsp;
                                                <input ID="btnZoomReset" clientidmode="Static" class="NormalButton" type="button" onclick="Reset();" runat="server" value="<%$Resources:btnZoomReset.Value %>" style="width:150px"/>
                                            </div>
                                            <table style="width:100%; height: 28px;">
                                                <tr><td valign="middle" style="width:50%; border-top: solid 1px black; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black;">
                                                <div style="padding-left:2px;">
                                                    <div style="width: 120px; float:left; ">
                                                    <table><tr><td><asp:TextBox ID="contrast" runat="server" Text="1.0"></asp:TextBox></td>
                                                    <td><asp:TextBox ID="Contrast_BoundControl" runat="server" Width="30px"></asp:TextBox></td></tr></table>
                                                        <ajaxToolkit:SliderExtender ID="sliderExtender" runat="server"
                                                        BehaviorID="Slider"
                                                        TargetControlID="Contrast"
                                                        BoundControlID="Contrast_BoundControl"                                                
                                                        Orientation="Horizontal"
                                                        EnableHandleAnimation="true"
                                                        Minimum="0"
                                                        Maximum="10"
                                                        Decimals="1"
                                                        TooltipText="{0}" />
                                                    </div>
                                                    <div style="float:right; width: 120px;">
                                                    <input ID="btnContrast" clientidmode="Static" class="NormalButton" type="button" onclick="btnContrast_Click();" runat="server" value="<%$Resources:btnContrast.Text %>" style="width:120"/></div>
                                                </div></td>
                                                <td valign="middle" style="border-top: solid 1px black; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black;">
                                                <div style="padding-left:2px; ">
                                            <div style="width: 120px; float:left; ">
                                             <table><tr><td><asp:TextBox ID="brightness" runat="server" Text="0.0"></asp:TextBox></td>
                                                    <td><asp:TextBox ID="brightness_BoundControl" runat="server" Width="30px"></asp:TextBox></td></tr></table>
                                                <ajaxToolkit:SliderExtender ID="sliderExtender1" runat="server"
                                                    BehaviorID="BrightNess"
                                                    TargetControlID="brightness"   
                                                    BoundControlID ="brightness_BoundControl"                                    
                                                    Orientation="Horizontal"
                                                    EnableHandleAnimation="true"
                                                    Minimum="-10"
                                                    Maximum="10"
                                                    Decimals="1"
                                                    TooltipText="{0}" />
                                            </div>
                                            <div style="float:right; width: 120;">
                                            <input ID="btnBrightness" clientidmode="Static" class="NormalButton" type="button" onclick="btnBrightness_Click();" runat="server" value="<%$Resources:btnBrightness.Text %>" style="width:120px"/>
                                                </div>
                                            </div>
                                            </td></tr></table>
                                        </td>
                                    </tr>
                                </table>
                                <input type="button" class="NormalButton" ID="btnFirst" OnClick="btnFirst_Click();" value="|<" CssClass="NormalButton" />&nbsp;
                                <input type="button" class="NormalButton" ID="btnPrevious" OnClick="btnPrevious_Click();" value="<<" CssClass="NormalButton" />&nbsp;
                                <select ID="ddlScan" onchange="ddlScan_SelectedIndexChanged();" style="width:297px"></select>&nbsp;
                                <input type="button" class="NormalButton" ID="btnNext" OnClick="btnNext_Click();" value=">>" CssClass="NormalButton" />&nbsp;
                                <input type="button" class="NormalButton" ID="btnLast" OnClick="btnLast_Click();"  value=">|" CssClass="NormalButton" />&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" align="center">
                    <table>
                        <tr>
                            <td align="center" valign="middle">
                                <%--register plate image--%>
                                <div id="regDiv">
                                <asp:Panel ID="regArea" runat="server">
                                    <table cellpadding="0" cellspacing="0" style="font-family:Tahoma; font-size:xx-small;">
                                        <tr>
                                            <td style="border: solid 1px black;">
                                                <asp:Label ID="regImageTitle" runat="server" Text="<%$Resources:regImageTitle.Text %>"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                                 <div id="regImageDiv" style="width:160px; height:100px; line-height:100px;">
                                                    <img ID="regImage" ImageUrl="" Width="160px" Height="100px"/>
                                                </div>
                                            </td>
                                        </tr>                                    
                                    </table>
                                </asp:Panel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle">
                            <div id="pre1Div">
                            <div style="border-style: none; border-color: inherit; border-width: 1; background-color:#6977B0; width:100%; font:white" >
                            <input  type="radio"  id="rdoPreviewA" onclick="rdoPreviewA_CheckedChanged();" name="rdoPreview"  checked="checked"/>
                                <asp:Label ID="Label2" runat="server" Text="<%$Resources:lbl1stImage.Text %>"></asp:Label></div>
                                <%--Preview image 1--%>
                                <asp:Panel ID="pre1Area" runat="server">
                                    <table cellpadding="0" cellspacing="0" style="font-family:Tahoma; font-size:xx-small;">
                                        <tr>
                                            <td style="border: solid 1px black;">
                                                <asp:Label ID="pre1ImageTitle" runat="server" Text="<%$Resources:pre1ImageTitle.Text %>"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                                 <div id="pre1ImageDiv" style="width:160px; height:100px; line-height:100px; overflow:hidden; position:relative; ">
                                                    <img ID="pre1Image" ImageUrl="" Width="160px" Height="100px"/>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr  id="pre1ImageCaptionRow" runat="server" visible="false">
                                            <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                                <asp:Label ID="pre1ImageCaption" runat="server" Text="<%$Resources:pre1ImageCaption.Text %>"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle">
                            <div id="pre2Div">
                            <div style="border-style: none; border-color: inherit; border-width: 1; background-color:#6977B0; width:100%; font:white" >
                             <input  type="radio" id="rdoPreviewB" onclick="rdoPreviewB_CheckedChanged();" name="rdoPreview"  value="2"/>
                                <asp:Label ID="Label1" runat="server" Text="<%$Resources:lbl2ndImage.Text %>"></asp:Label></div>
                                <%--Preview image 2--%>
                                <asp:Panel ID="pre2Area" runat="server">
                                    <table cellpadding="0" cellspacing="0" style="font-family:Tahoma; font-size:xx-small;">
                                        <tr>
                                            <td style="border: solid 1px black;">
                                                <asp:Label ID="pre2ImageTitle" runat="server" Text="<%$Resources:pre2ImageTitle.Text %>"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                                 <div id="pre2ImageDiv" style="width:160px; height:100px; line-height:100px; overflow:hidden; position:relative; ">
                                                    <img ID="pre2Image" ImageUrl="" Width="160px" Height="100px"/>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr  id="pre2ImageCaptionRow" runat="server" visible="false">
                                            <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                                <asp:Label ID="pre2ImageCaption" runat="server" Text="<%$Resources:pre1ImageCaption.Text %>"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
<script type="text/javascript">
    var mainImageID = "mainImage";    
    var lblFeedbackID = "<%=lblFeedback.ClientID %>";
    var CONTROLID = "<%= this.ClientID %>";

    //dls 090601 - need this code to reset the disabled link buttons and display the message on the screen
    function OnRemoteInvoke() {
        var update;

        if (document.getElementById("hidUpdateMainImage") != null)
            update = document.getElementById("hidUpdateMainImage").value;

        if (update == "AdjudicateFrame" || update == "VerifyFrame" || update == "ReturnToNatis" || "RejectFrame") {
            document.getElementById("hidUpdateMainImage").value = "";
            setTimeout(enableButtons, 3000);
            document.getElementById("lblMessage").style.display = "";

            var msg = "";
            switch (update) {
                case "AdjudicateFrame":
                case "VerifyFrame":
                case "RejectFrame":
                    msg = "Frame " + document.getElementById("txtFrameNo").value + " has been updated";
                    break;
                case "ReturnToNatis":
                case "ReturnToNatisAdj":
                    msg = "Frame " + document.getElementById("txtFrameNo").value + " will be re-sent to Natis";
                    break;
            }
            document.getElementById("lblMessage").innerHTML = msg;

            //dls 070416 - another aattempt to stop them moving before update is complete
            if (document.getElementById("lnkPrevious") != null) {
                document.getElementById("lnkPrevious").disabled = false;
            }

            if (document.getElementById("lnkNext") != null) {
                document.getElementById("lnkNext").disabled = false;
            }
        }
    }
</script>
<script type="text/javascript" src="Scripts/Zoom.js"></script>