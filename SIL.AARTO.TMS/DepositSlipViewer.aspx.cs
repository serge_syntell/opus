using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;

namespace Stalberg.TMS.Reports
{
    /// <summary>
    /// Represents a viewing page for creating the Deposit Slips.
    /// </summary>
    public partial class DepositSlipViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private ReportDocument _reportDoc = new ReportDocument();
        private string login;
        private int autIntNo = 0;
        protected string thisPageURL = "DepositSlipViewer.aspx";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Deposit Slip Viewer";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            int dsIntNo = 0;
            string reportPath = string.Empty;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "DepositSlip");
            if (reportPage.Equals(string.Empty))
            {
                //int arnIntNo = arn.AddAuthReportName(autIntNo, "DepositSlip.rpt", "DepositSlip", "system");
                int arnIntNo = arn.AddAuthReportName(autIntNo, "DepositSlip.rpt", "DepositSlip", "system", "");
                reportPage = "DepositSlip.rpt";
            }

            reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "DepositSlip");

            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                //dls 081117 - we can only check that the template path exists if there is actually a template!
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************
            _reportDoc.Load(reportPath);

            if (Request.QueryString["deposit"] != null)
            {
                dsIntNo = Convert.ToInt32(Request.QueryString["deposit"]);

                // Get data and populate dataset
                SqlConnection con = new SqlConnection(Application["constr"].ToString());
                SqlCommand com = new SqlCommand("DepositSlipPrint", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.Add("@DSIntNo", SqlDbType.Int, 4).Value = dsIntNo;
                //com.Parameters.Add("@DSDate", SqlDbType.DateTime).Value = dtDate;

                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                da.Dispose();

                //set up new report based on .rpt report class
                _reportDoc.SetDataSource(ds.Tables[0]);
                ds.Dispose();

                // Export the pdf file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
                _reportDoc.Dispose();

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SupervisorEndOfDayBanking, PunchAction.Other);

                // Stuff the PDF file into rendering stream first clear everything dynamically created and just send PDF file
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }
            else
            {
                DateTime dtDate = new DateTime(2000, 1, 1);
                if (Request.QueryString["date"] != null) 
                    DateTime.TryParse(Request.QueryString["date"], out dtDate);

                // Get data and populate dataset
                SqlConnection con = new SqlConnection(Application["constr"].ToString());
                SqlCommand com = new SqlCommand("DepositSlipPrint", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.Add("@DSDate", SqlDbType.DateTime).Value = dtDate;

                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                da.Dispose();

                //set up new report based on .rpt report class
                _reportDoc.SetDataSource(ds.Tables[0]);
                ds.Dispose();

                if (ds.Tables[0].Rows.Count < 1)
                {
                    Response.Write(String.Format((string)GetLocalResourceObject("strMsgWrite"), dtDate));
                    Response.End();
                    return;
                }


                // Export the pdf file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
                _reportDoc.Dispose();

                // Stuff the PDF file into rendering stream first clear everything dynamically created and just send PDF file
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }


        }


        private string GetAuthCode()
        {
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            //get LA code
            AuthorityDB authority = new AuthorityDB(connectionString);
            AuthorityDetails authDet = authority.GetAuthorityDetails(autIntNo);

            return authDet.AutCode.Trim();
        }

        protected void SetMargins(int left, int top, int right, int bottom)
        {
            PageMargins margins;

            // Get the PageMargins structure and set the 
            // margins for the report.
            margins = _reportDoc.PrintOptions.PageMargins;
            margins.leftMargin = left;
            margins.topMargin = top;
            margins.rightMargin = right;
            margins.bottomMargin = bottom;

            // Apply the page margins.
            _reportDoc.PrintOptions.ApplyPageMargins(margins);
        }

    }
}
