using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for FirstNotice1.
    /// </summary>
    public partial class StatisticsViewer : System.Web.UI.Page
    {
        // Fields
        private System.Data.SqlClient.SqlDataAdapter sqlDAStatistics;
        private System.Data.SqlClient.SqlConnection cn;
        private System.Data.SqlClient.SqlParameter paramAutIntNo;
        private dsFilmStatistics dsFilmStatistics;
        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private ReportDocument _reportDoc;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Statistics Viewer";
        protected string thisPageURL = "StatisticsViewer.aspx";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            //string tempFileLoc;
            //int batIntNo = 0;
            string reportPage = "MainStatistics.rpt";
            string reportPath = Server.MapPath("reports/" + reportPage);
            string title = "Statistics Viewer";

            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            _reportDoc = new ReportDocument();
            _reportDoc.Load(reportPath);

            ////set filter for batch to print
            paramAutIntNo.Value = Convert.ToString(Session["autIntNo"]);

            //get data and populate dataset
            sqlDAStatistics.SelectCommand.Connection.ConnectionString = Application["constr"].ToString();
            dsFilmStatistics.Clear();
            sqlDAStatistics.Fill(dsFilmStatistics);
            sqlDAStatistics.Dispose();

            //set up new report based on .rpt report class
            _reportDoc.SetDataSource(dsFilmStatistics);
            dsFilmStatistics.Dispose();

            //export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            _reportDoc.Dispose();

            // Stuff the PDF file into rendering stream first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

        protected void SetMargins(int left, int top, int right, int bottom)
        {
            PageMargins margins;

            // Get the PageMargins structure and set the 
            // margins for the report.
            margins = _reportDoc.PrintOptions.PageMargins;
            margins.leftMargin = left;
            margins.topMargin = top;
            margins.rightMargin = right;
            margins.bottomMargin = bottom;

            // Apply the page margins.
            _reportDoc.PrintOptions.ApplyPageMargins(margins);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        protected void InitializeComponent()
        {
            //this.Load += new System.EventHandler(this.Page_Load);
            this.sqlDAStatistics = new System.Data.SqlClient.SqlDataAdapter();
            this.cn = new System.Data.SqlClient.SqlConnection();
            this.dsFilmStatistics = new dsFilmStatistics();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            ((System.ComponentModel.ISupportInitialize)(this.dsFilmStatistics)).BeginInit();
            // 
            // sqlDAStatistics
            // 
            this.sqlDAStatistics.SelectCommand = this.sqlSelectCommand1;
            this.sqlDAStatistics.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
			new System.Data.Common.DataTableMapping("Table", "FilmStatistics", new System.Data.Common.DataColumnMapping[] {
			    new System.Data.Common.DataColumnMapping("BatIntNo", "BatIntNo"),
			    new System.Data.Common.DataColumnMapping("NotIntNo", "NotIntNo"),
			    new System.Data.Common.DataColumnMapping("NotTicketNo", "NotTicketNo"),
			    new System.Data.Common.DataColumnMapping("NotVehicleMake", "NotVehicleMake"),
			    new System.Data.Common.DataColumnMapping("NotVehicleType", "NotVehicleType"),
			    new System.Data.Common.DataColumnMapping("NotVehicleMakeCode", "NotVehicleMakeCode"),
			    new System.Data.Common.DataColumnMapping("NotVehicleTypeCode", "NotVehicleTypeCode"),
			    new System.Data.Common.DataColumnMapping("NotOffenceType", "NotOffenceType"),
			    new System.Data.Common.DataColumnMapping("NotLocDescr", "NotLocDescr"),
			    new System.Data.Common.DataColumnMapping("NotLocCode", "NotLocCode"),
			    new System.Data.Common.DataColumnMapping("NotSpeedLimit", "NotSpeedLimit"),
			    new System.Data.Common.DataColumnMapping("NotOfficerSName", "NotOfficerSName"),
			    new System.Data.Common.DataColumnMapping("BatNo", "BatNo"),
			    new System.Data.Common.DataColumnMapping("CrtName", "CrtName"),
			    new System.Data.Common.DataColumnMapping("NotIssueDate", "NotIssueDate"),
			    new System.Data.Common.DataColumnMapping("NotOfficerInit", "NotOfficerInit"),
			    new System.Data.Common.DataColumnMapping("NotOfficerNo", "NotOfficerNo"),
			    new System.Data.Common.DataColumnMapping("NotRegNo", "NotRegNo"),
			    new System.Data.Common.DataColumnMapping("NotSpeed1", "NotSpeed1"),
			    new System.Data.Common.DataColumnMapping("NotSpeed2", "NotSpeed2"),
			    new System.Data.Common.DataColumnMapping("NotOffenderType", "NotOffenderType"),
			    new System.Data.Common.DataColumnMapping("NotFilmNo", "NotFilmNo"),
			    new System.Data.Common.DataColumnMapping("NotFrameNo", "NotFrameNo"),
			    new System.Data.Common.DataColumnMapping("NotRefNo", "NotRefNo"),
			    new System.Data.Common.DataColumnMapping("NotSeqNo", "NotSeqNo"),
			    new System.Data.Common.DataColumnMapping("NotOffenceDate", "NotOffenceDate"),
			    new System.Data.Common.DataColumnMapping("NotSource", "NotSource"),
			    new System.Data.Common.DataColumnMapping("ChgOffenceCode", "ChgOffenceCode"),
			    new System.Data.Common.DataColumnMapping("NotRdTypeCode", "NotRdTypeCode"),
			    new System.Data.Common.DataColumnMapping("NotTravelDirection", "NotTravelDirection"),
			    new System.Data.Common.DataColumnMapping("NotJPegNameA", "NotJPegNameA"),
			    new System.Data.Common.DataColumnMapping("NotJPegNameB", "NotJPegNameB"),
			    new System.Data.Common.DataColumnMapping("NotRegNoImage", "NotRegNoImage"),
			    new System.Data.Common.DataColumnMapping("NotPaymentDate", "NotPaymentDate"),
			    new System.Data.Common.DataColumnMapping("ChgFineAmount", "ChgFineAmount"),
			    new System.Data.Common.DataColumnMapping("ChgFineAlloc", "ChgFineAlloc"),
			    new System.Data.Common.DataColumnMapping("ChgStatutoryRef", "ChgStatutoryRef"),
			    new System.Data.Common.DataColumnMapping("DrvSurname", "DrvSurname"),
			    new System.Data.Common.DataColumnMapping("DrvInitials", "DrvInitials"),
			    new System.Data.Common.DataColumnMapping("DrvPOAdd1", "DrvPOAdd1"),
			    new System.Data.Common.DataColumnMapping("DrvPOAdd2", "DrvPOAdd2"),
			    new System.Data.Common.DataColumnMapping("DrvPOAdd3", "DrvPOAdd3"),
			    new System.Data.Common.DataColumnMapping("DrvPOAdd4", "DrvPOAdd4"),
			    new System.Data.Common.DataColumnMapping("DrvPOAdd5", "DrvPOAdd5"),
			    new System.Data.Common.DataColumnMapping("DrvPOCode", "DrvPOCode"),
			    new System.Data.Common.DataColumnMapping("OcTDescr", "OcTDescr"),
			    new System.Data.Common.DataColumnMapping("OcTDescr1", "OcTDescr1"),
			    new System.Data.Common.DataColumnMapping("NotImageA", "NotImageA"),
			    new System.Data.Common.DataColumnMapping("NotImageB", "NotImageB"),
			    new System.Data.Common.DataColumnMapping("NotImageRegNo", "NotImageRegNo"),
			    new System.Data.Common.DataColumnMapping("AutName", "AutName"),
			    new System.Data.Common.DataColumnMapping("AutPostAddr1", "AutPostAddr1"),
			    new System.Data.Common.DataColumnMapping("AutPostAddr2", "AutPostAddr2"),
			    new System.Data.Common.DataColumnMapping("AutPostAddr3", "AutPostAddr3"),
			    new System.Data.Common.DataColumnMapping("AutPostCode", "AutPostCode"),
			    new System.Data.Common.DataColumnMapping("AutTel", "AutTel")})});
            // 
            // cn
            // 
            this.cn.ConnectionString = "workstation id=XE4500;packet size=4096;integrated security=SSPI;data source=SERVE" +
                "R2;persist security info=False;initial catalog=TMS";
            // 
            // dsFilmStatistics
            // 
            this.dsFilmStatistics.DataSetName = "dsFilmStatistics";
            this.dsFilmStatistics.Locale = new System.Globalization.CultureInfo("en-US");
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "[FilmStatistics]";
            this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand1.Connection = this.cn;
            this.sqlSelectCommand1.Parameters.Add(
                new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", 
                    System.Data.SqlDbType.Int, 4, 
                    System.Data.ParameterDirection.ReturnValue, 
                    false, ((System.Byte)(0)),
                    ((System.Byte)(0)), 
                    "", 
                    System.Data.DataRowVersion.Current,
                    null));
            paramAutIntNo = this.sqlSelectCommand1.Parameters.Add(
                new System.Data.SqlClient.SqlParameter("@AutIntNo", System.Data.SqlDbType.Int));
            ((System.ComponentModel.ISupportInitialize)(this.dsFilmStatistics)).EndInit();

        }
        #endregion
    }
}
