﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="RollbackJudgement" Codebehind="RollbackJudgement.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
   
    <style type="text/css">
        .style1
        {
            width: 227px;
        }
    </style>
   
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="form1" runat="server">
    <div>
        <p align="center">
            <asp:Label ID="lblPageName" runat="server" Width="856px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
        <table width="90%" align="center">
            <tr>
                <td align="center">
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%" CssClass="Normal">
                     <asp:Label ID="lblError" runat="server" CssClass="NormalRed" Width="600px"></asp:Label>
                        <table style="border-style: none" class="Normal">
                            <tr>
                                <th class="NormalBold" align="left">
                                    <asp:Label ID="Label1" runat="server" Text="<%$Resources:lblCourt.Text %>"></asp:Label>
                                </th>
                                <td align="left">
                                    <asp:DropDownList ID="cboCourt" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboCourt_SelectedIndexChanged"
                                        CssClass="Normal" Width="203px" />
                                </td>
                            </tr>
                            <tr>
                                <th align="left" class="NormalBold">
                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblRoom.Text %>"></asp:Label>
                                </th>
                                <td align="left">
                                    <asp:DropDownList ID="cboCourtRoom" runat="server" CssClass="Normal" Width="203px"
                                        AutoPostBack="True" OnSelectedIndexChanged="cboCourtRoom_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Text="<%$Resources:cboCourtRoomItem.Text %>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <th align="left" class="NormalBold">
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblSummonsNo.Text %>"></asp:Label>
                                </th>
                                <td align="left">
                                    <asp:TextBox ID="txtSummonsNo" runat="server" CssClass="Normal"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td></td>
                                <td style="text-align: left;"  class="NormalBold">
                                    <asp:Button ID="btnSearch" runat="server" Text="<%$Resources:btnSearch.Text %>" CssClass="NormalButton" OnClick="btnSearch_Click" />
                                </td>
                            </tr>
                             <tr><td></td></tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="panelTestRollback" runat="server">
                    
                        <table align="center">
                            <tr>
                                <td class="NormalBold" align="center">
                                    <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblSummonsNo.Text %>"></asp:Label>&nbsp;<asp:Label 
                                        ID="lblSummonsNo" runat="server" CssClass="NormalBold"></asp:Label>
                                       
                                </td>
                            </tr>
                            <tr><td></td></tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblMessage" runat="server" CssClass="NormalRed" Width="600px"></asp:Label>
                                    <br />
                                </td>
                            </tr>
                             <tr><td></td></tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnRollback" runat="server" Text="<%$Resources:btnRollback.Text %>" CssClass="NormalButton"
                                        OnClick="btnRollback_Click" />
                                    <asp:Button ID="btnReturn" runat="server" Text="<%$Resources:btnReturn.Text %>" 
                                        CssClass="NormalButton" OnClick="btnReturn_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align=center>
                    <asp:Panel ID="pnlCourtRoll" runat="server" Width="97%">
                        <p class="SubContentHeadSmall">
                            <asp:Label ID="lblCourtRollTitle" runat="server" />
                        </p>
                        <asp:Label ID="lblCourtRoll" runat="server" CssClass="NormalRed" />
                        <asp:GridView ID="grdCourtRoll" runat="server" CssClass="Normal" AllowPaging="False"
                            AutoGenerateColumns="False" ShowFooter="true"
                            OnRowCommand="grdCourtRoll_RowCommand" >
                            <FooterStyle CssClass="CartListHead" />
                            <RowStyle CssClass="CartListItem" />
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                            <Columns>
                                <asp:BoundField DataField="SumIntNo" HeaderText="<%$Resources:grdCourtRoll.HeaderText %>" Visible="false" />
                                <asp:BoundField DataField="SummonsNo" HeaderText="<%$Resources:grdCourtRoll.HeaderText1 %>" ItemStyle-Width="200">
                                    <ItemStyle Width="200px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CSName" HeaderText="<%$Resources:grdCourtRoll.HeaderText2 %>" ItemStyle-Width="200">
                                    <ItemStyle Width="200px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AccfullName" HeaderText="<%$Resources:grdCourtRoll.HeaderText3 %>" ItemStyle-Width="200">
                                    <ItemStyle Width="200px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:grdCourtRoll.HeaderText4 %>" />
                                <asp:ButtonField ButtonType="Link" CommandName="RollbackSummons" Text="<%$Resources:grdCourtRoll.Text %>" />
                            </Columns>
                        </asp:GridView>
                        <pager:AspNetPager id="grdCourtRollPager" runat="server" 
                                showcustominfosection="Right" width="50%" 
                                CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                FirstPageText="|&amp;lt;" 
                                LastPageText="&amp;gt;|" 
                                CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                CustomInfoStyle="float:right;" PageSize="20" onpagechanged="grdCourtRollPager_PageChanged"
                            ></pager:AspNetPager>
                        <p style="text-align: right">
                            &nbsp;</p>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
