<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<%@ Page language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.VehicleColour" Codebehind="VehicleColour.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head>
		<title>
			<%= title %>
		</title>
		<link href=<%= styleSheet %> type=text/css rel=stylesheet>
			<meta content=<%= description %> name=Description>
			<meta content=<%= keywords %> name=Keywords>
         <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
         <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
	</head>
	<body bottomMargin="0" leftMargin="0" background =<%=backgroundImage %> topMargin="0"
		rightMargin="0" >
		<form id="Form1" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0" height="10%">
				<tr>
					<td class="HomeHead" align="center" width="100%" colSpan="2" vAlign="middle">
						<hdr1:Header id="Header1" runat="server"></hdr1:Header></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" border="0" height="85%">
				<tr>
					<td align="center" vAlign="top" style="height: 667px"><IMG style="height: 1px" src="images/1x1.gif" width="167">
						<asp:Panel id="pnlMainMenu" Runat="server">
							
						</asp:Panel>

                        
							<asp:panel id="pnlSubMenu" Runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
								BorderColor="#000000">
								<table id="tblMenu" cellSpacing="1" cellPadding="1" border="0" runat="server">
									<tr>
										<td align="center" style="width: 139px">
											<asp:label id="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:label></td>
									</tr>
									<tr>
										<td align="center" style="width: 139px">
											<asp:button id="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>" OnClick="btnOptAdd_Click" Visible ="true"></asp:button></td>
									</tr>
									<tr>
										<td align="center" style="width: 139px">
											<asp:button id="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click" Visible ="true"></asp:button></td>
									</tr>
									<tr>
										<td align="center" style="width: 139px">
											<asp:button id="btnOptHide" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:button></td>
									</tr>
									<tr>
										<td align="center" height="21" style="width: 139px">
											 </td>
									</tr>
								</table>
							</asp:panel>
					</td>
					<td vAlign="top" align="left" colSpan="1" style="width: 100%">
                        &nbsp;<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
						<table border="0" width="568" height="482">
							<tr>
								<td vAlign="top" height="47">
									<P align="center">
										<asp:label id="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:label></P>
									<P>
										<asp:label  id="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:label></P>
								</td>
							</tr>
							<tr>
								<td vAlign="top" style="height: 580px">
									
                                    
										<table id="table1" height="30" cellSpacing="1" cellPadding="1" width="542" border="0">
											<tr>
												<td width="162"></td>
												<td width="7"></td>
												<td width="7"></td>
											</tr>
											<tr>
												<td width="162">
													<asp:Label id="lblSelVTGroup" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblSelVTGroup.Text %>"></asp:Label></td>
												<td width="7">
													<asp:DropDownList id="ddlColour" runat="server" Width="234px" CssClass="Normal" AutoPostBack="true" OnSelectedIndexChanged="ddlColour_SelectedIndexChanged"></asp:DropDownList></td>
												<td width="7"></td>
											</tr>
											
										</table>
									  &nbsp;&nbsp;
									<asp:panel id="pnlAddVehicleColour" runat="server" Height="127px">
										<table id="table2" height="48" cellSpacing="1" cellPadding="1" width="654" border="0">
											<tr>
												<td width="157" style="height: 2px">
													<asp:label id="lblAddVehicleColour" runat="server" CssClass="ProductListHead"  Text="<%$Resources:lblAddVehicleColour.Text %>"></asp:label></td>
												<td width="248" style="height: 2px"></td>
												<td style="height: 2px"></td>
											</tr>
											<tr>
												<td width="157" height="2">
													<asp:Label id="Label14" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblVehiclecode.Text %>"></asp:Label></td>
												<td width="248" height="2">
													<asp:textbox id="txtAddCode" runat="server" Width="83px" CssClass="NormalMand" Height="24px"
														MaxLength="3"></asp:textbox></td>
												<td height="2" valign="top">
													<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" Width="210px" CssClass="NormalRed" Display="dynamic"
														ErrorMessage="<%$Resources:reqVehicleCode.ErrorMsg %>" ControlToValidate="txtAddCode" ForeColor=" "></asp:requiredfieldvalidator></td>
											</tr>
											<tr>
												<td width="157" height="2">
													<asp:Label id="Label2" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblVehiclecolour.Text %>"></asp:Label></td>
												<td width="248" height="2">
													<asp:textbox id="txtAddColour" runat="server" Width="83px" CssClass="NormalMand" Height="24px"
														MaxLength="30"></asp:textbox></td>
												<td height="2" valign="top">
													<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" Width="210px" CssClass="NormalRed" Display="dynamic"
														ErrorMessage="<%$Resources:reqVehicleColour.ErrorMsg %>" ControlToValidate="txtAddColour" ForeColor=" "></asp:requiredfieldvalidator></td>
											</tr>
										<tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
											<tr>
												<td width="157"></td>
												<td width="248"></td>
												<td>
													<asp:Button id="btnAddVehicleColour" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddVehicleColour.Text %>" OnClick="btnAddVehicleColour_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
											</tr>
										</table>
									</asp:panel>
									<asp:panel id="pnlUpdateVehicleColour" runat="server" Height="127px">
										<table id="table3" height="118" cellSpacing="1" cellPadding="1" width="654" border="0">
											<tr>
												<td width="157" height="2">
													<asp:label id="Label19" runat="server" Width="194px" CssClass="ProductListHead"  Text="<%$Resources:lblUpdVehicleColour.Text %>"></asp:label></td>
												<td width="248" height="2"></td>
												<td height="2"></td>
											</tr>
											<tr>
												<td width="157" height="2">
													<asp:Label id="Label3" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblVehiclecode.Text %>"></asp:Label></td>
												<td width="248" height="2">
													<asp:textbox id="txtUpdateCode" runat="server" Width="83px" CssClass="NormalMand" Height="24px"
														MaxLength="3"></asp:textbox></td>
												<td height="2" valign="top">
													<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" Width="210px" CssClass="NormalRed" Display="dynamic"
														ErrorMessage="<%$Resources:reqVehicleCode.ErrorMsg %>" ControlToValidate="txtUpdateCode" ForeColor=" "></asp:requiredfieldvalidator></td>
											</tr>
											<tr>
												<td width="157" height="2">
													<asp:Label id="Label4" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblVehiclecolour.Text %>"></asp:Label></td>
												<td width="248" height="2">
													<asp:textbox id="txtUpdateColour" runat="server" Width="83px" CssClass="NormalMand" Height="24px"
														MaxLength="30"></asp:textbox></td>
												<td height="2" valign="top">
													<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" Width="210px" CssClass="NormalRed" Display="dynamic"
														ErrorMessage="<%$Resources:reqVehicleColour.ErrorMsg %>" ControlToValidate="txtUpdateColour" ForeColor=" "></asp:requiredfieldvalidator></td>
											</tr>
                                            <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                           <tr>
												<td width="157"></td>
												<td width="248"></td>
												<td>
													<asp:Button id="btnUpdateColour" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdateColour.Text %>" OnClick="btnUpdateVehicleColour_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
											</tr>
										</table>
									</asp:panel>
									 </ContentTemplate>
                                    </asp:UpdatePanel>
									</td>
	
							</tr>
						</table>
			
			  <table cellSpacing="0" cellPadding="0" width="100%" border="0" height="5%">
				<tr>
					<td class="SubContentHeadSmall" vAlign="top" width="100%"></td>
				</tr>
			</table>
		</form>
	</body>
</html>
