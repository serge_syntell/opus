using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.IO;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the page that creates the Admission of Guilt report
    /// </summary>
    public partial class CivitasLoadErrorListViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString;
        private string login;
        private int autIntNo;

        //protected string thisPage = "Civitas Load Error List Viewer";
        protected string thisPageURL = "CivitasLoadErrorListViewer.aspx";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string loginUser = string.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 

            // Variable retrieval
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (Request.QueryString["Start"] == null)
                Response.Redirect("CivitasLoadErrorList.aspx");
            DateTime dtStart = DateTime.Parse(Request.QueryString["Start"]);

            if (Request.QueryString["End"] == null)
                Response.Redirect("CivitasLoadErrorList.aspx");
            DateTime dtEnd = DateTime.Parse(Request.QueryString["End"]);

            // Setup the report
            string reportName = "CivitasLoadErrorList.dplx";
            AuthReportNameDB db = new AuthReportNameDB(this.connectionString);
            reportName = db.GetAuthReportName(this.autIntNo, reportName);

            if (reportName.Equals(string.Empty))
            {
                db.AddAuthReportName(autIntNo, "CivitasLoadErrorList.dplx", "CivitasLoadErrorList", "System", "");
                reportName = "CivitasLoadErrorList.dplx";
            }

            string path = Server.MapPath("Reports/" + reportName);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = db.GetAuthReportNameTemplate(this.autIntNo, "CivitasLoadErrorList");

            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportName);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                //dls 081117 - we can only check that the template path exists if there is actually a template!
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************


            DocumentLayout document = new DocumentLayout(path);
            StoredProcedureQuery query = (StoredProcedureQuery)document.GetQueryById("Query");
            query.ConnectionString = this.connectionString;

            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("StartDate", dtStart);
            parameters.Add("EndDate", dtEnd);

            //FormattedRecordArea printDate = (FormattedRecordArea)document.GetElementById("printDate");

            //printDate.LaidOut += new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);

            Document report = document.Run(parameters);

            byte[] buffer = report.Draw();

            //printDate.LaidOut -= new FormattedRecordAreaLaidOutEventHandler(printDate_LaidOut);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(buffer);
            Response.End();
        }

        private void printDate_LaidOut(object sender, FormattedRecordAreaLaidOutEventArgs e)
        {
            e.FormattedTextArea.Text = (string)GetLocalResourceObject("FormattedTextArea.Text") + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        }

    }
}