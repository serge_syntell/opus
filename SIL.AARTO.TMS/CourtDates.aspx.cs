using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceQueueLibrary.DAL.Entities;
using Stalberg.TMS.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;

// mrs 20080809 changed courtDates structure now child of courtroom and authority

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page for managing court dates
    /// </summary>
    public partial class CourtDates : System.Web.UI.Page
    {
        // Feilds
        private string connectionString = string.Empty;
        private string loginUser;

        protected string styleSheet;
        protected string backgroundImage;
        //protected string thisPage = "Court dates maintenance";
        protected string thisPageURL = "CourtDates.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            //int 
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            loginUser = userDetails.UserLoginName;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddCourt.Visible = false;
                pnlUpdateCourt.Visible = false;
                //pnlAuthority.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAuthority.Visible = false;
                btnOptAdd.Visible = true;
                btnOptHide.Visible = true;

                PopulateAuthorites(autIntNo);
                PopulateCourt(ddlSelCourt);

                //BindGrid();
            }
        }

        //mrs 20080809 courtdates are authority specific
        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorites(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            int autIntno = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));
            Session["editAutIntNo"] = autIntNo;

            //reader.Close();

        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            //int crtIntNo = Convert.ToInt32(ddlSelCourt.SelectedValue);
            Session["editAutIntNo"] = autIntNo;
            PopulateCourt(ddlSelCourt);
            //BindGrid(autIntNo, crtIntno);

        }

        //protected void PopulateCourt(int crtIntNo, DropDownList ddlSelCourt)
        //{
        //    CourtDatesDB courts = new CourtDatesDB(connectionString);

        //    SqlDataReader reader = courts.GetCourtList();
        //    ddlSelCourt.DataSource = reader;
        //    ddlSelCourt.DataValueField = "CrtIntNo";
        //    ddlSelCourt.DataTextField = "CrtName";
        //    ddlSelCourt.DataBind();
        //    ddlSelCourt.Items.Insert(0, "Select a court");
        //    reader.Close();
        //}

        protected void PopulateCourt(DropDownList ddlSelCourt)
        {
            CourtDatesDB courts = new CourtDatesDB(connectionString);

            SqlDataReader reader = courts.GetAuth_CourtListByAuth(Convert.ToInt32(Session["editAutIntNo"].ToString()));
            ddlSelCourt.DataSource = reader;
            ddlSelCourt.DataValueField = "CrtIntNo";
            ddlSelCourt.DataTextField = "CrtName";
            ddlSelCourt.DataBind();
            ddlSelCourt.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlSelCourt.Items"), "0"));
            reader.Close();
            PopulateCourtRooms(0);
        }

        protected void ddlSelCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            int crtIntNo = Convert.ToInt32(ddlSelCourt.SelectedValue);
            //int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            //Jerry 2012-07-25 add
            if (crtIntNo == 0)
            {
                ddlSelCourtRoom.Items.Clear();
                ddlSelCourtRoom.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlSelCourtRoom.Items"), "0"));
                dgCourtDates.Visible = false;
                return;
            }

            Session["editCrtIntNo"] = crtIntNo;

            PopulateCourtRooms(crtIntNo);
            //2011/11/03 jerry add 
            PopulateOriginGroup(ddlAddOriginGroup);
            PopulateOriginGroup(ddlEditOriginGroup);

            //BindGrid(autIntNo, crtIntNo);
        }

        private void PopulateCourtRooms(int crtIntNo)
        {
            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            SqlDataReader reader = room.GetCourtRoomListByCrtRStatusFlag(crtIntNo);//room.GetCourtRoomList(crtIntNo);


            this.ddlSelCourtRoom.DataSource = reader;
            this.ddlSelCourtRoom.DataValueField = "CrtRIntNo";
            this.ddlSelCourtRoom.DataTextField = "CrtRoomDetails";
            this.ddlSelCourtRoom.DataBind();


            // jake 2013-09-11 removed translate from lookup table
            //--------------
            ////Jerry 2012-07-25 add
            //ddlSelCourtRoom.Items.Clear();

            //Dictionary<int, string> lookups =
            //    CourtRoomLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            //while (reader.Read())
            //{
            //    int crtRIntNo = (int)reader["CrtRIntNo"];
            //    if (lookups.ContainsKey(crtRIntNo))
            //    {
            //        ddlSelCourtRoom.Items.Add(new ListItem(lookups[crtRIntNo], crtRIntNo.ToString()));
            //    }
            //}
            ////ddlSelCourtRoom.DataSource = reader;
            ////ddlSelCourtRoom.DataValueField = "CrtRIntNo";
            ////ddlSelCourtRoom.DataTextField = "CrtRoomDetails";
            ////ddlSelCourtRoom.DataBind();
            //---------------------
            ddlSelCourtRoom.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlSelCourtRoom.Items"), "0"));
            reader.Close();
            dgCourtDates.Visible = false;

            //Seawen 2013-09-13 pager visible equal to grid
            dgCourtDatesPager.Visible = dgCourtDates.Visible;
        }

        //jerry 2011-11-03 add
        protected void PopulateOriginGroup(DropDownList ddlOriginGroup)
        {
            // get court rules and set OGIntNo
            string ogIntNo;

            //Jerry 2012-05-17 add 3040 court rule
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = Convert.ToInt32(ddlSelCourt.SelectedValue);
            courtRulesDetails.CRCode = "3040";
            courtRulesDetails.LastUser = this.loginUser;
            DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
            KeyValuePair<int, string> rule3040 = courtRule.SetDefaultCourtRule();
            string defaultRule3040 = rule3040.Value;
            if (defaultRule3040 == "Y")
            {
                ogIntNo = ((int)OriginGroupList.ALL).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.S56).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.CAM).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.HWO).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.NoAOG_only_All).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.NoAOG_only_CAM_only).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.NoAOG_only_HWO_only).ToString();
                ogIntNo = ogIntNo + "," + ((int)OriginGroupList.NoAOG_only_S56_only).ToString();
            }
            else
            {
                courtRulesDetails = new CourtRulesDetails();
                courtRulesDetails.CrtIntNo = Convert.ToInt32(ddlSelCourt.SelectedValue);
                courtRulesDetails.CRCode = "3010";
                courtRulesDetails.LastUser = this.loginUser;
                courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
                KeyValuePair<int, string> rule3010 = courtRule.SetDefaultCourtRule();
                string defaultRule3010 = rule3010.Value;
                if (defaultRule3010 == "N")
                {
                    ogIntNo = ((int)OriginGroupList.ALL).ToString();
                }
                else
                {
                    courtRulesDetails = new CourtRulesDetails();
                    courtRulesDetails.CrtIntNo = Convert.ToInt32(ddlSelCourt.SelectedValue);
                    courtRulesDetails.CRCode = "3020";
                    courtRulesDetails.LastUser = this.loginUser;
                    courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
                    KeyValuePair<int, string> rule3020 = courtRule.SetDefaultCourtRule();
                    string defaultRule3020 = rule3020.Value;
                    if (defaultRule3020 == "N")// is HWO or CAM
                    {
                        ogIntNo = ((int)OriginGroupList.HWO).ToString();
                        ogIntNo = ogIntNo + "," + ((int)OriginGroupList.CAM).ToString();
                    }
                    else
                    {
                        ogIntNo = ((int)OriginGroupList.HWO).ToString();
                        ogIntNo = ogIntNo + "," + ((int)OriginGroupList.S56).ToString();
                        ogIntNo = ogIntNo + "," + ((int)OriginGroupList.CAM).ToString();
                    }
                }
            }

            OriginGroupDB originGroup = new OriginGroupDB(this.connectionString);
            SqlDataReader reader = originGroup.OriginGroupListByArrayStrOGIntNo(ogIntNo);
            //Jerry 2012-06-05 add
            ddlOriginGroup.Items.Clear();

            Dictionary<int, string> lookups =
                OriginGroupLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int oGIntNo = (int)reader["OGIntNo"];
                if (lookups.ContainsKey(oGIntNo))
                {
                    ddlOriginGroup.Items.Add(new ListItem(lookups[oGIntNo], oGIntNo.ToString()));
                }
            }
            //ddlOriginGroup.DataSource = reader;
            //ddlOriginGroup.DataValueField = "OGIntNo";
            //ddlOriginGroup.DataTextField = "OGDescrption";
            //ddlOriginGroup.DataBind();
            reader.Close();
        }

        protected void ddlSelCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            int crtRIntNo = 0;
            int autIntNo = Convert.ToInt32(Session["editAutIntNo"]);
            if (ddlSelCourtRoom.SelectedIndex > 0)
            {
                //PopulateCourtRooms(Convert.ToInt32(ddlCourt.SelectedValue));
                crtRIntNo = Convert.ToInt32(ddlSelCourtRoom.SelectedValue);
                Session["editCrtRIntNo"] = crtRIntNo;
                dgCourtDatesPager.CurrentPageIndex = 1;
                dgCourtDatesPager.RecordCount = 0;
                BindGrid(autIntNo, crtRIntNo);
            }
            else
            {
                ddlSelCourtRoom.SelectedIndex = 0;
                crtRIntNo = 0;
                Session["editCrtRIntNo"] = crtRIntNo;
            }
        }

        protected void BindGrid(int autIntNo, int crtRIntNo)
        {
            ////int crtIntNo = Convert.ToInt32(ddlSelCourt.SelectedValue);
            //int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Stalberg.TMS.CourtDatesDB tranNoList = new Stalberg.TMS.CourtDatesDB(connectionString);

            int totalCount = 0;
            DataSet data = tranNoList.GetCourtDatesListDS(crtRIntNo, autIntNo, dgCourtDatesPager.PageSize, dgCourtDatesPager.CurrentPageIndex - 1, out totalCount);
            dgCourtDates.DataSource = data;
            dgCourtDates.DataKeyField = "CDIntNo";
            dgCourtDatesPager.RecordCount = totalCount;

            try
            {
                dgCourtDates.DataBind();
            }
            catch { }

            if (dgCourtDates.Items.Count == 0)
            {
                dgCourtDates.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgCourtDates.Visible = true;
            }
            data.Dispose();
            pnlAddCourt.Visible = false;
            pnlUpdateCourt.Visible = false;

            //Seawen 2013-0913 pager visible equal to grid
            dgCourtDatesPager.Visible = dgCourtDates.Visible;
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlUpdateCourt.Visible = false;
            btnOptDelete.Visible = false;
            btnOptAuthority.Visible = false;
            //jerry 2011-11-03 add
            int crtIntNo = Convert.ToInt32(ddlSelCourt.SelectedValue);
            if (crtIntNo == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }
            pnlAddCourt.Visible = true;
            PopulateOriginGroup(ddlAddOriginGroup);
        }

        protected void dgCourtDates_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgCourtDates.SelectedIndex = e.Item.ItemIndex;

            if (dgCourtDates.SelectedIndex > -1)
            {
                int cdIntNo = Convert.ToInt32(dgCourtDates.DataKeys[dgCourtDates.SelectedIndex]);
                Session["editCDIntNo"] = cdIntNo;
                Session["prevPage"] = thisPageURL;
                ShowCourtDatesDetails(cdIntNo);
            }
        }

        /// <summary>
        /// Shows the court dates details.
        /// </summary>
        /// <param name="cdIntNo">The cd int no.</param>
        protected void ShowCourtDatesDetails(int cdIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.CourtDatesDB court = new Stalberg.TMS.CourtDatesDB(connectionString);
            Stalberg.TMS.CourtDatesDetails cdDetails = court.GetCourtDatesDetails(cdIntNo);

            //Stalberg.TMS.AuthorityRulesDB ardb = new Stalberg.TMS.AuthorityRulesDB(connectionString);
            //AuthorityRulesDetails det = ardb.GetAuthorityRulesDetailsByCode(Convert.ToInt32(Session["autIntNo"]), "5020", "", 10, "", "", loginUser);
            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails det = new AuthorityRulesDetails();
            det.AutIntNo = Convert.ToInt32(Session["autIntNo"]);
            det.ARCode = "5020";
            det.LastUser = loginUser;

            DefaultAuthRules authRule = new DefaultAuthRules(det, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            //this.dtpDate.Value = cdDetails.CDate;
            this.dtpDate.Text = cdDetails.CDate.ToString(DATE_FORMAT);

            txtCDNoOfCases.Text = Convert.ToString(cdDetails.CDNoOfCases);
            txtAddCDNoOfCases.Text = Convert.ToString(cdDetails.CDNoOfCases);
            txtS54.Text = cdDetails.sS54;
            txtS54Add.Text = cdDetails.sS54;
            txtS56.Text = cdDetails.sS56;
            txtS56Add.Text = cdDetails.sS56;
            txtPercent.Text = value.Value; //det.ARNumeric.ToString();
            txtPercentageAdd.Text = value.Value; //det.ARNumeric.ToString();
            txtTotal.Text = cdDetails.sCurrentTotal;
            txtTotalAdd.Text = cdDetails.sCurrentTotal;

            //mrs 20080811 added reserved columns
            txtS56Reserved.Text = Convert.ToString(cdDetails.CDS56Reserved);
            txtS54Reserved.Text = Convert.ToString(cdDetails.CDS54Reserved);

            txtUtilised.Text = Convert.ToString(cdDetails.CDTotalUtilised);
            //jerry 2011-11-03 add
            PopulateOriginGroup(ddlEditOriginGroup);
            foreach (ListItem item in ddlEditOriginGroup.Items)
            {
                if (item.Value == cdDetails.OGIntNo.ToString())
                {
                    ddlEditOriginGroup.SelectedValue = cdDetails.OGIntNo.ToString();
                }
            }
            //jerry 2012-03-23 add
            txtNoAOGMax.Text = cdDetails.CDMaximumNumberOfNoAOGAllowed.ToString();
            txtNoAOGS35Max.Text = cdDetails.CCrtMaxNumberOfS35AllowedForS54.ToString();
            pnlUpdateCourt.Visible = true;
            pnlAddCourt.Visible = false;
            //pnlAuthority.Visible = false;
            //btnOptAuthority.Visible = true;
            //btnOptDelete.Visible = true;

            if (cdDetails.CDateLocked) {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text18");
                this.lblError.Visible = true;
                Button2.Enabled = false;
                return;
            }

            if (cdDetails.CDate.Ticks < DateTime.Today.Ticks)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                this.lblError.Visible = true;
                Button2.Enabled = false;
                return;
            }
            else
                Button2.Enabled = true;

        }


        protected void btnOptAuthority_Click(object sender, System.EventArgs e)
        {
        }

        protected void PopulateUserGroupAuthorityList()
        {
        }

        protected void btnAddAuthority_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
        }

        protected void btnDelAuthority_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
        }

        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgCourtDates.Visible.Equals(true))
            {
                //hide it
                dgCourtDates.Visible = false;
                //2012-3-1 Linda Modified into a multi- language
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
            }
            else
            {
                //show it
                dgCourtDates.Visible = true;
                //2012-3-1 Linda Modified into a multi- language
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }

            //Seawen 2013-0913 pager visible equal to grid
            dgCourtDatesPager.Visible = dgCourtDates.Visible;
        }

        protected void btnUpdateCDate_Click(object sender, EventArgs e)
        {
            lblError.Visible = true;
            int autIntNo = Convert.ToInt32(Session["editAutIntNo"]);

            int cdIntNo = Convert.ToInt32(Session["editCDIntNo"]);

            DateTime cDate;

            if (!DateTime.TryParse(this.dtpDate.Text, out cDate))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }

            short cdNoOfCases = 0;
            if (!short.TryParse(txtCDNoOfCases.Text, out cdNoOfCases))
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text4"), this.txtAddCDNoOfCases.Text);
                return;
            }

            //jerry 2012-03-22 add
            Int32 noAOGMax = 0;
            if (!Int32.TryParse(txtNoAOGMax.Text.Trim(), out noAOGMax))
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text17"), this.txtNoAOGMax.Text);
                return;
            }

            // Jake 2013-12-23 added 
            int noAOGS35Max = 0;
            if (!Int32.TryParse(txtNoAOGS35Max.Text.Trim(), out noAOGS35Max))
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text171"), this.txtNoAOGS35Max.Text);
                return;
            }

            // SD: 20081205 - test for null values
            StringBuilder sb = new StringBuilder();
            sb.Append((string)GetLocalResourceObject("lblError.Text5") + "\n<ul>\n");
            if (txtAddCDNoOfCases.Text.Trim().Length == 0)
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text6") + "</li>");
                sb.Append("</ul>");
                lblError.Text = sb.ToString();
                lblError.Visible = true;

            }

            Stalberg.TMS.CourtDatesDB cdUpdate = new CourtDatesDB(connectionString);
            int updCDIntNo = cdUpdate.UpdateCourtDates(cdIntNo, cDate, cdNoOfCases, loginUser, Convert.ToInt32(ddlEditOriginGroup.SelectedValue), noAOGMax, noAOGS35Max);

            if (updCDIntNo == 0)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
            else if (updCDIntNo == -2)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            else if (updCDIntNo == -3)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                int crtRIntNo = Convert.ToInt32(ddlSelCourtRoom.SelectedValue);
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtDatesMaintenance, PunchAction.Change);  


                //Seawen 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                //dgCourtDatesPager.RecordCount = 0;
                //dgCourtDatesPager.CurrentPageIndex = 1;
                BindGrid(autIntNo, crtRIntNo);
            }
        }

        protected void btnAddCDate_Click(object sender, EventArgs e)
        {
            lblError.Visible = true;

            int crtRIntNo = Convert.ToInt32(Session["editCrtRIntNo"]);
            int autIntNo = Convert.ToInt32(Session["editAutIntNo"]);

            DateTime cDate;

            if (!DateTime.TryParse(this.dtpAddDate.Text, out cDate))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }

            //dls 080418 - check date is >= today
            if (cDate <= DateTime.Today)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                return;
            }

            short cdNoOfCases = 0;
            if (!short.TryParse(txtAddCDNoOfCases.Text, out cdNoOfCases))
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text12"), this.txtAddCDNoOfCases.Text);
                return;
            }

            //jerry 2012-03-22 add
            Int32 noAOGMax = 0;
            if (!Int32.TryParse(txtAddNoAOGMax.Text.Trim(), out noAOGMax))
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text17"), this.txtAddNoAOGMax.Text);
                return;
            }

            // Jake 2013-12-23 added 
            int noAOGS35Max = 0;
            if (!Int32.TryParse(txtAddNoAofS35Max.Text.Trim(), out noAOGS35Max))
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text171"), this.txtAddNoAofS35Max.Text);
                return;
            }

            Stalberg.TMS.CourtDatesDB cdAdd = new CourtDatesDB(connectionString);

            // 2013-08-28, Oscar add push queue for service NewCourtDate
            var processor = new QueueItemProcessor();
            var authService = new AuthorityService();
            try
            {
                using (var scope = ServiceUtility.CreateTransactionScope())
                {
                    int addCDIntNo = cdAdd.AddCourtDates(crtRIntNo, autIntNo, cDate, cdNoOfCases, loginUser, Convert.ToInt32(ddlAddOriginGroup.SelectedValue), noAOGMax, noAOGS35Max);

                    if (addCDIntNo == 0 || addCDIntNo == -1)    // 2013-08-29, Oscar added missed -1
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                    else if (addCDIntNo == -2)
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                    else if (addCDIntNo == -3)
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text15");
                    else if (addCDIntNo > 0)
                    {
                        // 2013-08-28, Oscar add push queue for service NewCourtDate
                        processor.Send(new QueueItem
                        {
                            Body = Convert.ToInt32(ddlSelCourt.SelectedValue),
                            Group = string.Join("|", authService.GetByAutIntNo(autIntNo).AutCode.Trim(), dtpAddDate.Text),
                            ActDate = DateTime.Now,
                            QueueType = ServiceQueueTypeList.NewCourtDate,
                            LastUser = loginUser
                        });

                        lblError.Text = (string)GetLocalResourceObject("lblError.Text16");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtDatesMaintenance, PunchAction.Add);  

                        //Seawen 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                        //dgCourtDatesPager.CurrentPageIndex = 1;
                        //dgCourtDatesPager.RecordCount = 0;
                        BindGrid(autIntNo, crtRIntNo);

                        if (ServiceUtility.TransactionCanCommit())
                            scope.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        protected void dgCourtDatesPager_PageChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(Session["editAutIntNo"]);
            int crtRIntNo = Convert.ToInt32(ddlSelCourtRoom.SelectedValue);
            Session["editCrtRIntNo"] = crtRIntNo;
            BindGrid(autIntNo, crtRIntNo);
        }

    }
}
