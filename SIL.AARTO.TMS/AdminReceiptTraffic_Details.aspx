<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.AdminReceiptTraffic_Details" Codebehind="AdminReceiptTraffic_Details.aspx.cs" %>
<%@ Register Src="RepresentationOnTheFly.ascx" TagName="RepresentationOnTheFly" TagPrefix="uc1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%--<%@ Register Src="_Header1.ascx" TagName="Header" TagPrefix="hdr1" %>--%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0" rightmargin="0">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="6000">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" id="TABLE1" onclick="return TABLE1_onclick()">
        <tr>
            <td align="left" width="100%" valign="middle">
               <%-- <hdr1:Header ID="Header1" runat="server"></hdr1:Header>--%>
            </td>
        </tr>
    </table>
    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center" style="width: 180px; height: 90%;">
                <img style="height: 1px" src="images/1x1.gif" width="167" />
                &nbsp;
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="140px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 138px">
                                 </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Button ID="btnAddNotices" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$Resources:btnAddNotices.Text %>" OnClick="btnAddNotices_Click" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" style="text-align: center;" class="Normal">
                <!-- Start Content -->
                <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Width="610px" Text="<%$Resources:lblPageName.Text %>"></asp:Label>&nbsp;
                <br />
                <asp:UpdatePanel ID="udpRevceipting" runat="server" >
                    <ContentTemplate>
                        &nbsp;<asp:Panel ID="pnlApplyReversed" runat="server" Width="100%">
                            &nbsp;<asp:Label ID="lblReversedPaymentAmount" runat="server" CssClass="SubContentHead"
                                EnableViewState="False" Text="<%$Resources:lblReversedPaymentAmount.Text %>"> </asp:Label></asp:Panel>
                        <br />
                        
                        <asp:Panel ID="panelDetails" runat="server"  HorizontalAlign="Left">
                        <asp:Label ID="lblRepresentations" runat="server" CssClass="SubContentHead" Text="<%$Resources:lblRepresentations.Text %>" Visible="False" Width="712px"></asp:Label><br />
                            <br />
                            <table border="0" >
                                <tr>
                                    <td >
                                        <asp:DataGrid ID="dgTicketDetails" runat="server" AlternatingItemStyle-CssClass="CartListItemAlt"
                                            AutoGenerateColumns="False" BorderColor="Black" CellPadding="4" CssClass="Normal"
                                            Font-Size="8pt" FooterStyle-CssClass="cartlistfooter" GridLines="Vertical" HeaderStyle-CssClass="CartListHead"
                                            ItemStyle-CssClass="CartListItem" OnEditCommand="dgTicketDetails_EditCommand"
                                            OnItemCommand="dgTicketDetails_ItemCommand" OnItemCreated="dgTicketDetails_ItemCreated"
                                            OnSelectedIndexChanged="dgTicketDetails_SelectedIndexChanged" ShowFooter="True"
                                            Width="100%">
                                            <FooterStyle CssClass="CartListFooter" />
                                            <AlternatingItemStyle CssClass="CartListItemAlt" />
                                            <ItemStyle CssClass="CartListItem" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundColumn DataField="NoticeIntNo" HeaderText="NotIntNo" ReadOnly="True" Visible="False">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="ChgIntNo" HeaderText="ChgIntNo" ReadOnly="True" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="TicketNo" HeaderText="<%$Resources:dgTicketDetails.Text %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="ID" HeaderText="<%$Resources:dgTicketDetails.Text1 %> "></asp:BoundColumn>
                                                <asp:BoundColumn DataField="OffenceDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="<%$Resources:dgTicketDetails.Text2 %> ">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="RegistrationNo" HeaderText="<%$Resources:dgTicketDetails.Text3 %> ">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="OffenceDescription" HeaderText="<%$Resources:dgTicketDetails.Text4 %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Amount" DataFormatString="R {0:#, ##0.00}" HeaderText="<%$Resources:dgTicketDetails.Text5 %> ">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoticeStatus" HeaderText="<%$Resources:dgTicketDetails.Text6 %> ">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="ContemptCourtAmt" DataFormatString="R {0:#, ##0.00}"
                                                    HeaderText=" <%$Resources:dgTicketDetails.Text7 %>">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="CourtDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="<%$Resources:dgTicketDetails.Text8 %> ">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.Text9 %> ">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbSummonsDetails" runat="server" CommandArgument="Select" CommandName="SummonsDetails"
                                                            Text="<%$Resources:lbSummonsDetails.Text %>"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.Text10 %>">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:ImageButton ID="igPayNow" runat="server" OnClick="ImageButton1_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="ChargeTypeIntNo" HeaderText="ChargeTypeIntNo" ReadOnly="True"
                                                    Visible="False"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.Text11 %>">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtAmountToPay" runat="server" CssClass="Normal" ReadOnly="false"
                                                            Width="95px"></asp:TextBox>
<%--                                                        <asp:Label runat="server" ID="lblAmountToPay" Text='<%# DataBinder.Eval(Container, "DataItem.AmountToPay") %>'></asp:Label>
--%>                                                    
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.Text12 %>">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlSeniorOfficer" runat="server" CssClass="Normal" Width="242px">
                                                        </asp:DropDownList>
<%--                                                        <asp:Label runat="server" ID="lblSeniorOfficer" Text='<%# DataBinder.Eval(Container, "DataItem.SeniorOfficer") %>'></asp:Label>
--%>                                                    
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:EditCommandColumn CancelText="<%$Resources:dgTicketDetails.CancelText %>" EditText="<%$Resources:dgTicketDetailsItem.Text %>" UpdateText="<%$Resources:dgTicketDetails.UpdateText %>"></asp:EditCommandColumn>
                                                <asp:BoundColumn DataField="HasJudgement" HeaderText="Has Judgement" Visible="False">
                                                </asp:BoundColumn>
                                            </Columns>
                                            <PagerStyle Visible="False" />
                                        </asp:DataGrid></td>
                                </tr>
                                <tr>
                                    <td align="right" style="height: 21px; width: 711px;">
                                    <asp:Label ID="labelTotal" runat="server" CssClass="NormalBold"></asp:Label>
                                        &nbsp; &nbsp; &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;" align="center">
                                        &nbsp;<br/>                                            
                                        <asp:Label ID="lblError" runat="server" CssClass="NormalRed" Width="100%"></asp:Label><br />
                                        <br />
                                        <table>
                                            <tr><td valign=middle align="center" >
                                                <asp:Button ID="btnBackFromRepresentations" runat="server" Text="Back to Payment Details" CssClass="NormalButton"
                                    OnClick="btnBackFromRepresentations_Click" Width="170px" /></td><td valign="top">
                                                <asp:Panel ID="pnlTotals" runat="server" BackColor="Silver" HorizontalAlign="Center">
                                                        <table>
                                                            <tr>
                                                                <td class="NormalBold" >
                                                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblTotalDue.Text %>" CssClass="NormalBold" Font-Bold="True" Font-Size="Larger"></asp:Label></td>
                                                                <td align="right" >
                                                                    <asp:Label ID="lblTotalDue" runat="server" CssClass="NormalBold" Font-Bold="True" Font-Size="Larger" Height="24px"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="NormalBold" >
                                                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblPaymentApplied.Text %>" CssClass="NormalBold" Width="163px" Font-Bold="True" Font-Size="Larger"></asp:Label></td>
                                                                <td align="right">
                                                                    <asp:Label ID="lblTotalPaymentReceived" runat="server" CssClass="NormalBold" Font-Bold="True" Font-Size="Larger" Height="24px"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="NormalBold"  rowspan="2" style="height: 41px" >
                                                                    <asp:Label ID="lblDifference" runat="server" Text="<%$Resources:lblDifference.Text %>" CssClass="NormalBold" Font-Bold="True" Font-Size="Larger" ForeColor="Red"></asp:Label></td>
                                                                <td align="right" bgcolor="#ffffff" colspan="1" rowspan="1" style="height: 41px">
                                                                    <asp:Label ID="lblOverOrUnder" runat="server" CssClass="NormalBold" BackColor="White" BorderStyle="None" BorderWidth="1px" Font-Bold="True" Font-Size="Larger" ForeColor="Red" Height="24px"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    <br />
                                                        <asp:CheckBox ID="chkCashierRep" runat="server" Text="<%$Resources:chkCashierRep.Text %>" CssClass="NormalBold" Width="355px" BackColor="Silver" Font-Bold="True" Font-Size="Larger" Height="25px" AutoPostBack="True" OnCheckedChanged="chkCashierRep_CheckedChanged" />
                                                        <br /><asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="<%$Resources:btnContinue.Text %>"
                                                            CssClass="NormalButton" Width="115px" /></asp:Panel>
                                            </td></tr></table>
                                        &nbsp; &nbsp; &nbsp;
                                        <asp:Panel ID="pnlReceiptHeader" runat="server" Height="50px" Width="125px">
                                            <table ><tr><td class="NormalBold" style="width: 20%" valign="top">
                                                <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblComment.Text %>"></asp:Label></td><td width="80%">
                                                        <asp:TextBox ID="txtComment" runat="server" CssClass="Normal" TextMode="MultiLine"
                                                            Width="500px"  Text="<%$Resources:txtPayment.Text %>"></asp:TextBox></td></tr>
                                                <tr>
                                                    <td class="NormalBold" style="width: 20%; height: 21px;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 3px; height: 21px">
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        <asp:Panel ID="pnlAddress" runat="server" CssClass="NormalBold" Visible="False">
                                                            <table>
                                            <tr><td  class="NormalBold" valign="top" style="width: 20%; height: 40px;">
                                                <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblReceivedFrom.Text %>"></asp:Label></td><td valign="top" style="width: 129px; height: 40px;">
                                                            <asp:DropDownList ID="ddlReceivedFrom" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReceivedFrom_SelectedIndexChanged"
                                                        CssClass="Normal" Width="91px">
                                                                <asp:ListItem Value="D" Text="<%$Resources:ddlReceivedFromItem.Text %>"></asp:ListItem>
                                                                <asp:ListItem Value="O" Text="<%$Resources:ddlReceivedFromItem.Text1 %>"></asp:ListItem>
                                                                <asp:ListItem Value="P" Text="<%$Resources:ddlReceivedFromItem.Text2 %>"></asp:ListItem>
                                                                <asp:ListItem Value="A" Text="<%$Resources:ddlReceivedFromItem.Text3 %>"></asp:ListItem>
                                                                <asp:ListItem Value="X" Text="<%$Resources:ddlReceivedFromItem.Text4 %>"></asp:ListItem>
                                                            </asp:DropDownList></td>
                                                <td valign="top" width="20%" style="height: 40px">
                                                                        <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="40%" style="height: 40px">
                                                                        <asp:TextBox ID="txtReceivedFrom" runat="server" CssClass="Normal" Width="224px"></asp:TextBox></td>
                                            </tr>
                                                                <tr>
                                                                    <td class="NormalBold" valign="top" width="20%">
                                                                    </td>
                                                                    <td valign="top">
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:Label ID="lblAddress" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddress.Text %>"></asp:Label></td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="Normal" Width="224px" Height="22px"></asp:TextBox><br />
                                                        <asp:TextBox ID="txtAddress2" runat="server" Width="224px" CssClass="Normal" Height="22px"></asp:TextBox><br />
                                                        <asp:TextBox ID="txtAddress3" runat="server" Width="224px" CssClass="Normal" Height="22px"></asp:TextBox><br />
                                                        <asp:TextBox ID="txtAddress4" runat="server" Width="224px" CssClass="Normal" Height="22px"></asp:TextBox><br />
                                                        <asp:TextBox ID="txtAddress5" runat="server" Width="224px" CssClass="Normal" Height="22px"></asp:TextBox></td>
                                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" style="width: 20%; height: 26px;">
                                                        <asp:Label ID="lblPhone" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPhone.Text %>"></asp:Label></td>
                                                    <td style="height: 26px">
                                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="Normal" Width="119px"></asp:TextBox></td>
                                                    <td style="height: 26px">
                                                                        <asp:Label ID="lblPostalCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPostalCode.Text %>"
                                                                            Width="87px"></asp:Label></td>
                                                    <td width="40%" style="height: 26px">
                                                        <asp:TextBox ID="txtPoCode" runat="server" MaxLength="5" Width="80px" CssClass="Normal"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                            <br />
                                        <asp:Panel ID="pnlAllowPayment" runat="server" HorizontalAlign="Center">
                                        <table><tr><td><asp:LinkButton ID="linkBack" runat="server" PostBackUrl="AdminReceiptTraffic.aspx" Width="409px" Text="<%$Resources:lbllinkBack.Text %>"></asp:LinkButton></td>
                                        <td style="width: 150px"><asp:Button ID="btnPayNow" runat="server" Text="<%$Resources:btnPayNow.Text %>" CssClass="NormalButton"
                                    OnClick="btnPayNow_Click" Width="90px" /></td></tr></table>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>

                            </asp:Panel>
                        &nbsp;<!-- On-The-Fly Representations-->
                        <!-- Payment Confirmation -->
                        <asp:Panel ID="panelConfirm" runat="server" Visible="False" Width="100%">
                            <p style="text-align: left">
                                <asp:Label ID="labelConfirm" runat="server" CssClass="Normal"></asp:Label></p>
                            <p style="text-align: center">
                                <asp:Button ID="buttonBack" runat="server" Text="<%$Resources:buttonBack.Text %>" CssClass="NormalButton"
                                    OnClick="buttonBack_Click" Width="170px" />
                                <asp:Button ID="buttonVoid" runat="server" Text="<%$Resources:buttonVoid.Text %>" OnClick="buttonVoid_Click"
                                    CssClass="NormalButton" Width="170px" />&nbsp;
                                <asp:Button ID="buttonPay" runat="server" Text="<%$Resources:buttonPay.Text %>" CssClass="NormalButton"
                                    OnClick="buttonPay_Click" Width="170px" />
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pnlAnotherPayment" Width="100%" runat="server" Visible="false" HorizontalAlign="Center">
                            <br />
                            <a href="AdminReceiptReversal.aspx" class="NormalBold">
                                <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblApplyPayment.Text %>"></asp:Label></a>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="udp" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label7"
                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td style="width: 621px">
                &nbsp;</td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
