using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Collections.Generic;
using System.Configuration;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that displays the details for summonses for an LA
    /// </summary>
    public partial class SummonsClerkOfCourt : System.Web.UI.Page
    {
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "SummonsClerkOfCourt.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Book Summons OUT/IN to the Clerk Of The Court";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            //LF:  (11/06/2008) - Killed this on account of the Application["constr"]
            //      This connection string is hydrated with user id=stalber_synimag;password=3pZ9hVlY
            //      Why?:  I don't know.
            this.connectionString = Application["constr"].ToString( );
            //this.connectionString =
            //    ConfigurationManager.ConnectionStrings["TMSConnectionString"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            dgPrintrun.Visible = true;

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                //LF: (11/06/2008) - This page is a copy of SummonsClerkOfCourt..
                //PopulateAuthorites(ugIntNo, autIntNo);
                this.BindSummonsToClerkGrid( );
            }
        }

        private void PopulateAuthorites(int ugIntNo, int autIntNo)
        {
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = reader;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            //ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void BindSummonsToClerkGrid( )
        {
            Stalberg.TMS.SummonsDB summonsToCofC = new SummonsDB(connectionString);
            //mrs 20081019 added autintno parameter
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int totalCount = 0;
            dgPrintrun.DataSource = summonsToCofC.GetSummonsToBeSentOrReceivedFromCofC(autIntNo, dgPrintrunPager.PageSize, dgPrintrunPager.CurrentPageIndex, out totalCount);
            //dgPrintrun.DataKeyField = "SumPrintFileName";
            //dgPrintrun.DataKeyField = "SumChargeStatus";
            // Oscar 2011-03 changed for 4416
            dgPrintrun.DataKeyField = "SummonsStatus";
            dgPrintrun.DataBind( );
            dgPrintrunPager.RecordCount = totalCount;

            try
            {
                dgPrintrun.DataBind( );
            }
            catch
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.DataBind( );
            }

            if(dgPrintrun.Items.Count == 0)
            {
                dgPrintrun.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                EnableBtnsAccordingToChargeStatus( );
                dgPrintrun.Visible = true;
            }
        }

        private void EnableBtnsAccordingToChargeStatus( )
        {
            Button btnBookOut = null;
            Button btnBookIn = null;

            foreach(DataGridItem summonsRow in dgPrintrun.Items)
            {
                if(summonsRow != null)
                {
                    string strChargeCode = summonsRow.Cells[0].Text;

                    switch (strChargeCode)
                    {
                        // dls 080621 - the summons print control register status 621 must be used instead of 620 throughtout the clerk of court process

                        //case "620":
                        case "621":
                            {                                
                                btnBookIn = (Button)summonsRow.Cells[3].Controls[0];
                                btnBookOut = (Button)summonsRow.Cells[4].Controls[0];
                                btnBookIn.Enabled = true;
                                btnBookOut.Enabled = false;
                                break;
                            }
                        case "630":
                            {
                                btnBookIn = (Button)summonsRow.Cells[3].Controls[0];
                                btnBookOut = (Button)summonsRow.Cells[4].Controls[0];
                                btnBookIn.Enabled = false;
                                btnBookOut.Enabled = true;
                                break;
                            }
                    }
                    
                }
            }
            //DataTable dtSummonsFiles = ((DataSet)dgPrintrun.DataSource).Tables[0];
            
            ////foreach(DataRow dr in dtSummonsFiles)
            //for(int i = 0; i < dtSummonsFiles.Rows.Count - 1; i++ )
            //{
            //    DataRow dr = dtSummonsFiles.Rows[i];
            //    if((int)dr["SumChargeStatus"] == 620)
            //    {
            //        Button btnBookOut = (Button)dgPrintrun.Items[i].Cells[3].Controls[0];
            //        btnBookOut.Enabled = false;
            //    }
            //}
            ////foreach(DataGridItem dgRowItem in dgPrintrun.Items)
            ////{
            //    //object something = dgRowItem.Cells[0];
            ////}
        }

        //private void BindGrid(int autIntNo)
        //{
        //    Stalberg.TMS.SummonsDB printList = new Stalberg.TMS.SummonsDB(connectionString);
        //    dgPrintrun.DataSource = printList.GetBookOutList(autIntNo);
        //    dgPrintrun.DataKeyField = "SumPrintFileName";
        //    dgPrintrun.DataBind();

        //    try
        //    {
        //        dgPrintrun.DataBind();
        //    }
        //    catch
        //    {
        //        dgPrintrun.CurrentPageIndex = 0;
        //        dgPrintrun.DataBind();
        //    }

        //    if (dgPrintrun.Items.Count == 0)
        //    {
        //        dgPrintrun.Visible = false;
        //        lblError.Visible = true;
        //        lblError.Text = "There are no print files available for booking out to summons servers";
        //    }
        //    else
        //    {
        //        dgPrintrun.Visible = true;
        //    }
        //}

        protected void dgPrintrun_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            dgPrintrun.SelectedIndex = e.Item.ItemIndex;


            if (dgPrintrun.SelectedIndex > -1)
            {
                // Name of the file to be used for book-out/in
                string sumPrintFileName = e.Item.Cells[1].Text;

                Button btnCommandItem = ((Button)e.CommandSource);

                string outSummons = (string)GetLocalResourceObject("dgPrintrun.EditText");
                string inSummons = (string)GetLocalResourceObject("dgPrintrun.EditText1") ;
                if (btnCommandItem.Text==outSummons)
                    BookOut(sumPrintFileName);
                if (btnCommandItem.Text == inSummons)
                    BookIn(sumPrintFileName);
                //USEFULL: Add a Confirmation Message box to ensure
                // that the user want's to perform these actions.
                //switch(btnCommandItem.Text)
                //{
                //    case ("Book out Summons -->"):
                //    {
                //        BookOut(sumPrintFileName);
                //        //btnCommandItem.Enabled = false;
                //        break;
                //    }
                //    case "<-- Book in Summons":
                //    {
                //        //lblError.Text = sumPrintFileName + ": Summons booked IN!";
                //        BookIn(sumPrintFileName);
                //        break;
                //    }
                //}

                // 2013-09-12, Oscar removed, it reset wrong page
                //dgPrintrunPager.RecordCount = 0; //2013-03-15 add by Henry for pagination
                //dgPrintrunPager.CurrentPageIndex = 1;

                BindSummonsToClerkGrid( );
            }
        }

        private void BookIn(string sumPrintFileName)
        {
            SummonsDB db = new SummonsDB(this.connectionString);

            string errMessage = string.Empty;

            int noOfRows =
                db.BookSummonsInOutToClerkOfTheCourt(sumPrintFileName, 630, login, ref errMessage);

            if (noOfRows > 0)
            {
                if (sumPrintFileName.Substring(0, 1).Equals("*"))
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text1"), sumPrintFileName.Substring(1, sumPrintFileName.Length - 1));
                else
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), noOfRows.ToString(), sumPrintFileName);

               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ClerkOfCourtBookOutOrIn, PunchAction.Change);  

            }
            else
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3") + errMessage;

            lblError.Visible = true;
            //BindSummonsToClerkGrid( );
        }

        protected void BookOut(string sumPrintFileName)
        {
            SummonsDB db = new SummonsDB(this.connectionString);

            // dls 080621 - the summons print control register status 621 must be used instead of 620 throughtout the clerk of court process

            string errMessage = string.Empty;

            int noOfRows =
                //db.BookSummonsInOutToClerkOfTheCourt(sumPrintFileName, 620, login, ref errMessage);
                //db.BookOutSummonsToClerkOfTheCourt(sumPrintFileName, login, ref errMessage);
                db.BookSummonsInOutToClerkOfTheCourt(sumPrintFileName, 621, login, ref errMessage);

            if (noOfRows > 0)
            {
                if (sumPrintFileName.Substring(0, 1).Equals("*"))
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text4"), sumPrintFileName.Substring(1, sumPrintFileName.Length - 1));
                else
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), noOfRows.ToString(), sumPrintFileName);

                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ClerkOfCourtBookOutOrIn, PunchAction.Change);  
            }
            else
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6") + errMessage;

            lblError.Visible = true;
            //BindSummonsToClerkGrid();
        }
        
        protected void btnBookOut_Click(object sender, EventArgs e)
        {
            //BookOut("*"+txtSummonsNo.Text.Trim());
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //2013-03-15 add by Henry for pagination
        protected void dgPrintrunPager_PageChanged(object sender, EventArgs e)
        {
            BindSummonsToClerkGrid();
        }
}
}
