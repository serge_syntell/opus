﻿<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>


<%@ Page Language="C#" AutoEventWireup="true" Inherits="NatisReturnCodeList" Codebehind="NatisReturnCodeList.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" 
                                        Text="<%$Resources:btnOptAdd.Text %>" onclick="btnOptAdd_Click"
                                        ></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" onclick="btnOptDelete_Click"  ></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>"  ></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:DataGrid ID="dgNatisRCList" Width="800px" runat="server" 
                                    BorderColor="Black" AutoGenerateColumns="False"
                                    AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" 
                                    onitemcommand="dgNatisRCList_ItemCommand" onpageindexchanged="dgNatisRCList_PageIndexChanged"  
                                     >
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="NRCIntNo" HeaderText="NRCIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NRCDescr" HeaderText="<%$Resources:dgNatisRCList.HeaderText %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NRCode" HeaderText="<%$Resources:dgNatisRCList.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NRCPriority" HeaderText="<%$Resources:dgNatisRCList.HeaderText2 %>">
                                        </asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgNatisRCListItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <asp:Panel ID="pnlAddNRC" runat="server" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="157" 
                                        border="0">
                                        <tr>
                                            <td height="2" >
                                                <asp:Label ID="lblAddNRC" runat="server" CssClass="ProductListHead" 
                                                    Width="224px" Text="<%$Resources:lblAddNRC.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="2" valign="top" >
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddNRCDescr.Text %>"></asp:Label></td>
                                            <td width="248" height="2" valign="top">
                                                <asp:TextBox ID="txtAddNRCDescr" runat="server" Width="500px" CssClass="NormalMand" TextMode=MultiLine
                                                    MaxLength="255"></asp:TextBox></td>
                                            <td height="2" >
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:reqDesc.ErrorMsg %>"
                                                    ControlToValidate="txtAddNRCDescr" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="NormalBold">
                                                <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblReturnCode.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddNRCode" runat="server" CssClass="NormalMand" 
                                                    Height="24px" MaxLength="3" Width="80px"></asp:TextBox>
                                            </td>
                                            <td >
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                    ControlToValidate="txtAddNRCode" 
                                                    ErrorMessage="Code may not right." CssClass= "NormalRed" 
                                                    ValidationExpression="^[A-Za-z]+$"></asp:RegularExpressionValidator>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" CssClass="NormalRed"
                                                    ControlToValidate="txtAddNRCode" ErrorMessage="<%$Resources:reqCode.ErrorMsg %>"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td class="NormalBold">
                                                <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblPriority.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddNRCPriority" runat="server" CssClass="NormalMand" 
                                                    Height="24px" MaxLength="2" Width="80px"></asp:TextBox>
                                             </td>
                                            <td >
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                    ControlToValidate="txtAddNRCPriority" CssClass="NormalRed"
                                                    ErrorMessage="<%$Resources:reqPriority.ErrorMsg %>" 
                                                    ValidationExpression="^[1-9]\d*$"></asp:RegularExpressionValidator>
                                                </td>
                                        </tr>
                                         <tr>
                                            <td >
                                               </td>
                                            <td width="248">
                                               
                                             </td>
                                            <td >
                                                <asp:Button ID="btnAdd" runat="server" CssClass="NormalButton" 
                                                    Text="<%$Resources:btnAdd.Text %>" onclick="btnAdd_Click"
                                                    ></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateNRC" runat="server" Height="127px">
                                    <table id="Table3" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157">
                                                <asp:Label ID="Label19" runat="server" Width="224px" CssClass="ProductListHead" Text="<%$Resources:lblUpdReturnCode.Text %>"></asp:Label></td>
                                            <td width="248">
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddNRCDescr.Text %>"></asp:Label></td>
                                            <td valign="top" width="248">
                                                <asp:TextBox ID="txtNRCDescr" runat="server" Width="500px" CssClass="NormalMand" TextMode="MultiLine"
                                                    MaxLength="255"></asp:TextBox></td>
                                            <td valign="top">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:reqDesc.ErrorMsg %>" 
                                                    ControlToValidate="txtNRCDescr" ></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="157" class="NormalBold">
                                                <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblReturnCode.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtNRCode" runat="server" CssClass="NormalMand" 
                                                    Height="24px" MaxLength="3" Width="80px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                                    ControlToValidate="txtNRCode" CssClass="NormalRed"
                                                    ErrorMessage="<%$Resources:reqCodeRight.ErrorMsg %>" 
                                                    ValidationExpression="^[A-Za-z]+$"></asp:RegularExpressionValidator>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" CssClass="NormalRed"
                                                    ControlToValidate="txtNRCode" ErrorMessage="<%$Resources:reqCode.ErrorMsg %>"></asp:RequiredFieldValidator>
                                               </td>
                                        </tr>
                                        
                                         <tr>
                                            <td width="157" class="NormalBold">
                                                <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblPriority.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtNRCPriority" runat="server" CssClass="NormalMand" 
                                                    Height="24px" MaxLength="2" Width="80px"></asp:TextBox>
                                             </td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                    ControlToValidate="txtNRCPriority" 
                                                    ErrorMessage="<%$Resources:reqPriority.ErrorMsg %>"  CssClass="NormalRed"
                                                    ValidationExpression="^[1-9]\d*$"></asp:RegularExpressionValidator>
                                                </td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248">
                                                
                                             </td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" 
                                                    Text="<%$Resources:lblbtnUpdate.Text %>" onclick="btnUpdate_Click"
                                                     ></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
