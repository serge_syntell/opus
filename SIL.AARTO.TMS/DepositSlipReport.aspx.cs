using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// A page where the user can select the date to display deposit slips for
    /// </summary>
    public partial class DepositSlipReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        //protected string thisPage = "Deposit Slip Report";
        protected string description = String.Empty;
        protected string thisPageURL = "DepositSlipReport.aspx";
        protected string title = string.Empty;

        private const string DATE_FORMAT = "yyyy-MM-dd";


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.dtpDate.Text = DateTime.Today.ToString(DATE_FORMAT);
            }
        }

         

        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime dt;

            if (!DateTime.TryParse(dtpDate.Text, out dt))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            this.lblError.Text = string.Empty;
            Helper_Web.BuildPopup(this, "DepositSlipViewer.aspx", "date", (dt.ToString(DATE_FORMAT)));

        }

    }
}


