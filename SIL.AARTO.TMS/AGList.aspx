﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.AGList" Codebehind="AGList.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=title%>
    </title>
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"     rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table style="height: 10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" style="width: 100%; height: 35px;" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table style="height: 85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" alt="" width="167" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" colspan="1" style="width: 100%; text-align: center">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <asp:UpdatePanel ID="upd" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblError" CssClass="NormalRed" runat="server" />
                                <br />
                                <asp:Label ID="lblTip" CssClass="NormalRed" runat="server" Text="<%$Resources:selectDateTip.Text %>" />
                                <table class="Normal" style="border-style: none;">
                                    <tr>
                                        <th style="text-align: left;">
                                            </th>
                                        <td>
                                            <asp:RadioButtonList runat="server" ID="rblSForAG" AutoPostBack="true" OnSelectedIndexChanged="rblSForAG_SelectedIndexChanged"  CssClass="Normal">
                                            <asp:ListItem Text="<%$Resources:rblSForAGItem.Text %>" Value="A" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="<%$Resources:lblPageName.Text %>" Value="S"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                     <tr>
                                        <th style="text-align: left;">
                                            <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="153px" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></th>
                                        <td>
                                            &nbsp;<asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px">
                                            </asp:DropDownList></td>
                                         <td>
                                             &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left;" class="NormalBold">
                                            <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblBetween.Text %>"></asp:Label></th>
                                        <td>
                                            <asp:TextBox runat="server" ID="dtpStartDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                OnTextChanged="dtpStartDate_TextChanged" AutoPostBack="True"/>
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpStartDate" Format="yyyy-MM-dd" ></cc1:CalendarExtender>
                                        </td>
                                        <td align="left">
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                ControlToValidate="dtpStartDate" CssClass="NormalRed" Display="Dynamic" 
                                                ErrorMessage="<%$Resources:reqStartDate.ErrorMsg %>" 
                                                ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="dtpStartDate" CssClass="NormalRed" Display="Dynamic" 
                                                ErrorMessage="<%$Resources:reqStartDate.ErrorMsg %>"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left;" class="style1">
                                            <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblAnd.Text %>"></asp:Label></th>
                                        <td class="style1">
                                            <asp:TextBox runat="server" ID="dtpEndDate" CssClass="Normal" Height="20px" autocomplete="off" UseSubmitBehavior="False" />
                                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="dtpEndDate" Format="yyyy-MM-dd" ></cc1:CalendarExtender>
                                        </td>
                                        <td align="left" class="style1">
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                ControlToValidate="dtpEndDate" CssClass="NormalRed" Display="None" 
                                                ErrorMessage="<%$Resources:reqStartDate.ErrorMsg %>" 
                                                ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                ControlToValidate="dtpEndDate" CssClass="NormalRed" Display="Dynamic" 
                                                ErrorMessage="<%$Resources:reqStartDate.ErrorMsg %>"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left;">
                                            <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="153px" Text="<%$Resources:lblCourt.Text %>"></asp:Label></th>
                                        <td>
                                            &nbsp;<asp:DropDownList ID="cboCourt" runat="server" CssClass="Normal" AutoPostBack="true"
                                                Width="217px" OnSelectedIndexChanged="cboCourt_SelectedIndexChanged">
                                            </asp:DropDownList></td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left;">
                                            <asp:Label ID="lblCourtRoom" runat="server" CssClass="NormalBold" Width="153px" Text="<%$Resources:lblCourtRoom.Text %>"></asp:Label></th>
                                        <td>
                                            &nbsp;<asp:DropDownList ID="cboCourtRoom" runat="server" CssClass="Normal"
                                                Width="217px">
                                            </asp:DropDownList></td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: left;">
                                            <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="153px" Text="<%$Resources:lblSortOrder.Text %>"></asp:Label></th>
                                        <td>
                                            &nbsp;<asp:DropDownList ID="ddlSort" runat="server" CssClass="Normal"
                                                Width="217px">
                                            </asp:DropDownList></td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right;">
                                            <asp:Button ID="btnShow" runat="server" Text="<%$Resources:btnShow.Text %>" CssClass="NormalButton" OnClick="btnShow_Click" />
                                        </td>
                                        <td style="text-align: right;">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="udp" runat="server">
                            <ProgressTemplate>
                                <p class="Normal" style="text-align: center;">
                                    <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;<asp:Label
                                        ID="Label7" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" style="width: 100%">
                </td>
            </tr>
        </table>
        <table style="height: 5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width: 100%; height: 39px;">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
