<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.CourtRollManualCaseNos" Codebehind="CourtRollManualCaseNos.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <script src="Scripts/Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
<script type="text/javascript">
    // 2013-09-11, Oscar added for avoiding mutiple clicks.
    function LoadingBlocker(text) {
        var content = '<div style="text-align:center; width:150px; margin: auto"><img alt="loading" src="Images/ig_progressIndicator.gif" style="display:block; float: left" />&nbsp;' + text + '</div>';
        $.blockUI({
            message: content
        });
    }
</script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height:5%">
            <tr>
                <td class="HomeHead" align="center" style="width:100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
    <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167" alt="images/1x1.gif" />
                </td>
                <td valign="top" align="left" colspan="1"  style="height:90%" width="100%">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed" /><br />
                                <br />
                            <asp:Panel ID="pnlCourt" runat="server" Width="125px">
                                <table id="tblControls" border="0" class="NormalBold" style="width: 554px">
                                    
                                    <tr>
                                        <td style="width: 147px">
                                            <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblSelectCourt.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlCourt" runat="server" CssClass="Normal" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlCourt_SelectedIndexChanged" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 147px">
                                            <asp:Label ID="lblCourtRoom" runat="server" CssClass="NormalBold" Visible="true" Text="<%$Resources:lblCourtRoom.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlCourtRoom" runat="server" CssClass="Normal" AutoPostBack="True"
                                                Width="200px" OnSelectedIndexChanged="ddlCourtRoom_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 147px">
                                        </td>
                                        <td>
                                            <asp:Panel ID="pnlCourtDates" runat="server" Width="100%">
                                                <br />
                                                <asp:GridView ID="grdDates" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                                    CssClass="Normal" GridLines="Horizontal" OnSelectedIndexChanged="grdDates_SelectedIndexChanged"
                                                    ShowFooter="True" OnRowCreated="grdDates_RowCreated">
                                                    <FooterStyle CssClass="CartListHead" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="<%$Resources:grdDates.HeaderText %>">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CDate" , "{0:yyyy-MM-dd }") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("CDate", "{0:yyyy-MM-dd }") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="CDNoOfCases" HeaderText="<%$Resources:grdDates.HeaderText1 %>">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CDTotalAllocated" HeaderText="<%$Resources:grdDates.HeaderText2 %>">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:CommandField ShowSelectButton="True" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="CartListHead" />
                                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                                </asp:GridView>
                                                <div style="font-weight:lighter;">
                                                <pager:AspNetPager id="grdDatesPager" runat="server" 
                                            showcustominfosection="Right" width="100%"
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                              PageSize="10" onpagechanged="grdDatesPager_PageChanged" UpdatePanelId="UpdatePanel1"></pager:AspNetPager>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlCaseNos" runat="server" HorizontalAlign="left" Width="550px">
                                    &nbsp;
                                    <table width="100%" id="TABLE1" >
                                        <tr>
                                            <td style="width: 164px">
                                                <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSetAutofill.Text %>"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 164px">
                                                <asp:Label ID="lblCasePrefix" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCasePrefix.Text %>"></asp:Label></td>
                                            <td style="width: 117px">
                                                <asp:TextBox ID="txtCasePrefix" runat="server" Width="121px" ReadOnly="true"></asp:TextBox></td>
                                            <td style="width: 103px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 164px; ">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblNextCaseNo.Text %>"></asp:Label></td>
                                            <td style="width: 117px; ;">
                                                <asp:TextBox ID="txtNextCaseNo" runat="server" Width="119px" Enabled="false"></asp:TextBox></td>
                                            <td style="width: 103px; ">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 164px">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCaseSuffix.Text %>"></asp:Label></td>
                                            <td style="width: 117px">
                                                <asp:TextBox ID="txtCaseSuffix" runat="server" Width="119px" ReadOnly="true"></asp:TextBox></td>
                                            <td style="width: 103px">
                                            </td>
                                        </tr>
                                     </table>
                                     <br />
                                    <asp:Label ID="lblPersonSummons" runat="server" Visible="false" Text="<%$Resources:lblPersonSummons.Text %>"></asp:Label>
                                    <br />
                                    <br />
                                    <asp:GridView ID="grdEnterCaseNos" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                                    CssClass="Normal" GridLines="Horizontal"
                                                    ShowFooter="True" OnRowEditing="grdEnterCaseNos_RowEditing" OnRowCancelingEdit="grdEnterCaseNos_RowCancelingEdit" OnRowUpdating="grdEnterCaseNos_RowUpdating" OnSelectedIndexChanged="grdEnterCaseNos_SelectedIndexChanged" AllowPaging="False" OnRowDataBound="grdEnterCaseNos_RowDataBound" OnRowCreated="grdEnterCaseNos_RowCreated">
                                        <FooterStyle CssClass="CartListHead" />
                                        <Columns>
                                            <asp:BoundField DataField="SumNoticeNo" HeaderText="<%$Resources:grdEnterCaseNos.HeaderText %>" ReadOnly="True" />
                                            <asp:BoundField HeaderText="<%$Resources:grdEnterCaseNos.HeaderText1 %>" DataField="SummonsNo" ReadOnly="True">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Served" HeaderText="<%$Resources:grdEnterCaseNos.HeaderText2 %>" ReadOnly="True" />
                                            <asp:BoundField DataField="SumType" HeaderText="<%$Resources:grdEnterCaseNos.HeaderText3 %>" ReadOnly="True" />
                                            <asp:TemplateField HeaderText="<%$Resources:grdEnterCaseNos.HeaderText4 %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtSumCaseNo" runat="server" Text='<%# Bind("SumCaseNo") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemStyle Width="100px" />
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("SumCaseNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ShowEditButton="True" />
                                            <asp:CommandField SelectText="<%$Resources:grdEnterCaseNosItem.Text %>" ShowSelectButton="True" />
                                        </Columns>
                                        <HeaderStyle CssClass="CartListHead" />
                                        <AlternatingRowStyle CssClass="CartListItemAlt" />
                                    </asp:GridView>
                                    <pager:AspNetPager id="grdEnterCaseNosPager" runat="server" 
                                            showcustominfosection="Right" width="100%"
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CustomInfoStyle="float:right;"
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            PageSize="15" 
                                        onpagechanged="grdEnterCaseNosPager_PageChanged"  UpdatePanelId="UpdatePanel1"></pager:AspNetPager>
                                    <!--   Impersonal Summons       -->
                                    <br />
                                    <asp:Label ID="lblImpersonSummons" runat="server" Visible="false" Text="<%$Resources:lblImpersonSummons.Text %>"></asp:Label>
                                    <br />
                                    <br />
                                    <asp:GridView ID="grdImpersonSummons" runat="server" 
                                        AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" 
                                        GridLines="Horizontal" ShowFooter="True" AllowPaging="False"
                                        Visible="false" HorizontalAlign="Left" 
                                        OnSelectedIndexChanged="grdImpersonSummons_SelectedIndexChanged" 
                                        OnRowDataBound="grdImpersonSummons_RowDataBound"> 
                                        <FooterStyle CssClass="CartListHead" />
                                        <Columns>
                                            <asp:BoundField DataField="SumNoticeNo" HeaderText="<%$Resources:grdEnterCaseNos.HeaderText %>" ReadOnly="True" />
                                            <asp:BoundField HeaderText="<%$Resources:grdEnterCaseNos.HeaderText1 %>" DataField="SummonsNo" ReadOnly="True">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Served" HeaderText="<%$Resources:grdEnterCaseNos.HeaderText2 %>" ReadOnly="True" />
                                            <asp:BoundField DataField="SumType" HeaderText="<%$Resources:grdEnterCaseNos.HeaderText3 %>" ReadOnly="True" />
                                            <asp:BoundField DataField="SumCaseNo" HeaderText="<%$Resources:grdEnterCaseNos.HeaderText4 %>" ReadOnly="True" />
                                            <asp:TemplateField HeaderText="SummonsStatus" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSummonStatus" runat="server" Text='<%# Bind("SummonsStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField SelectText="<%$Resources:grdImpersonSummons.SelectText %>" ShowSelectButton = "true" />
                                        </Columns>
                                        <HeaderStyle CssClass="CartListHead" />
                                        <AlternatingRowStyle CssClass="CartListItemAlt" />
                                    </asp:GridView>
                                     <pager:AspNetPager id="grdImpersonSummonsPager" runat="server" 
                                            showcustominfosection="Right" width="100%"
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CustomInfoStyle="float:right;"
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                              PageSize="15" 
                                            Visible="false"
                                        onpagechanged="grdImpersonSummonsPager_PageChanged" UpdatePanelId="UpdatePanel1"></pager:AspNetPager>
                                </asp:Panel>
                            </asp:Panel>
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label2"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
      <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height:5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width:100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
