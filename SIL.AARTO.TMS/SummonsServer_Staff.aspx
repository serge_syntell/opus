<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.SummonsServer_Staff" Codebehind="SummonsServer_Staff.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
  
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                 <asp:UpdatePanel ID="udpSummonsServerStaff" runat="server">
                 <ContentTemplate>
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>&nbsp;</p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                        <p class="NormalBold">
                            <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblSummonsServer.Text %>"></asp:Label></p>
                        <asp:GridView ID="grdSummonsServer" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="Normal" OnSelectedIndexChanged="grdSummonsServer_SelectedIndexChanged"
                            ShowFooter="True">
                            <FooterStyle CssClass="CartListHead" />
                            <Columns>
                                <asp:BoundField DataField="ACIntNo" HeaderText="ACIntNo" Visible="False" />
                                <asp:BoundField DataField="SSIntNo" HeaderText="SSIntNo" Visible="False" />
                                <asp:BoundField DataField="SSFullName" HeaderText="<%$Resources:grdSummonsServer.HeaderText %>">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SSForceNo" HeaderText="<%$Resources:grdSummonsServer.HeaderText1 %>">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:CommandField HeaderText="<%$Resources:grdSummonsServer.HeaderText2 %>" EditText="<%$Resources:grdSummonsServer.SelectText %>" SelectText="<%$Resources:grdSummonsServer.SelectText %>" ShowSelectButton="True">
                                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                </asp:CommandField>
                            </Columns>
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                        </asp:GridView>
                        &nbsp;
                    </asp:Panel>
                    <asp:Panel ID="pnlStaff" runat="server" Width="100%">
                        <p class="NormalBold">
                                    <asp:Button ID="btnNew" runat="server" Text="<%$Resources:btnNew.Text %>" Width="135px" CssClass="NormalButton"
                                        OnClick="btnNew_Click" />
                                    <asp:Button ID="btnDelete" runat="server" Text="<%$Resources:btnDelete.Text %>" Width="135px" CssClass="NormalButton"
                                        OnClick="btnDelete_Click" /></p>
                        <p class="NormalBold">
                            <asp:Label ID="lblStaff" runat="server" CssClass="Normal" Text="<%$Resources:lblStaff.Text %>"></asp:Label>&nbsp;</p>
                        <asp:GridView ID="grdStaff" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="Normal" ShowFooter="True" 
                            OnSelectedIndexChanged="grdStaff_SelectedIndexChanged" AllowPaging="True" 
                            onpageindexchanging="grdStaff_PageIndexChanging" PageSize="10">
                            <FooterStyle CssClass="CartListHead" />
                            <Columns>
                                <asp:BoundField DataField="SS_SintNo" HeaderText="SS_SIntNo" Visible="False">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FullName" HeaderText="<%$Resources:grdStaff.HeaderText %>">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ForceNo" HeaderText="<%$Resources:grdStaff.HeaderText1 %>">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Certificate" HeaderText="<%$Resources:grdStaff.HeaderText2 %>">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HtmlEncode="false"  DataField="CertDate" HeaderText="<%$Resources:grdStaff.HeaderText3 %>" DataFormatString="{0:yyyy-MM-dd}">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:CommandField HeaderText="<%$Resources:grdStaff.HeaderText4 %>" SelectText="<%$Resources:grdStaff.HeaderText4 %>" ShowSelectButton="True">
                                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                </asp:CommandField>
                            </Columns>
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                        </asp:GridView>
                        &nbsp;
                    </asp:Panel>
                    <asp:Panel ID="pnlStaffDetails" runat="Server" Width="100%">
                        <p class="NormalBold">
                            <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblMemberDetail.Text %>"></asp:Label> </p>
                                <table>
                                    <tr>
                                        <td><asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                        </td> 
                                        <td><asp:HiddenField ID="hdnRef" runat ="server" Visible ="false" ></asp:HiddenField > 
                                       </td>
                                       <td><asp:TextBox ID="txtName" runat ="server" Width="150px"></asp:TextBox>
                                       </td> 
                                    </tr>
                                   <tr>
                                        <td><asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblForceNo.Text %>"></asp:Label>
                                        </td> 
                                        <td>
                                       </td>
                                       <td><asp:TextBox ID="txtForceNo" runat ="server" Width="150px"></asp:TextBox>
                                       </td> 
                                    </tr> 
                                     <tr>
                                        <td><asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCertificate.Text %>"></asp:Label>
                                        </td> 
                                        <td>
                                       </td>
                                       <td><asp:TextBox ID="txtCertificate" runat ="server" Width="150px"></asp:TextBox>
                                       </td> 
                                    </tr>  
                                     <tr>
                                        <td><asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCertificateDate.Text %>"></asp:Label>
                                        </td> 
                                        <td>
                                       </td>
                                       <td>
                                    
                                           <asp:TextBox runat="server" ID="wdcDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="wdcDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="wdcDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqwdcDate.ErrorMsg %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                           </td>
                                    </tr>   
                                   <tr>
                                        <td>
                                        </td> 
                                        <td>
                                            <asp:CheckBox ID="chkActive" runat="server" CssClass="Normal" Text="<%$Resources:chkActive.Text %>" /></td>
                                       <td><asp:Button ID="btnSave" Text= " <%$Resources:btnSave.Text %> " runat ="server" CssClass ="NormalButton"  OnClick ="btnSave_Click" />
                                       </td> 
                                    </tr>   
                                </table>
                                        
                               
                        <asp:Label ID="lblStaffError" runat="server" CssClass="NormalRed"></asp:Label></asp:Panel></ContentTemplate></asp:UpdatePanel>
                                                                          <asp:UpdateProgress ID="udp" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label8"
                                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
