<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.ExclusionList" Codebehind="ExclusionList.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <table id="Table4" cellspacing="1" cellpadding="1" width="300" border="0">
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelAuthority" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:DropDownList ID="ddlSelectLA" runat="server" Width="217px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:TextBox ID="txtSearch" runat="server" Width="169px" CssClass="Normal" MaxLength="20"></asp:TextBox></td>
                                            <td>
                                                <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                    OnClick="btnSearch_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgExclusionList" Width="495px" runat="server" 
                                    BorderColor="Black" AutoGenerateColumns="False"
                                    AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgExclusionList_ItemCommand"
                                    OnPageIndexChanged="dgExclusionList_PageIndexChanged">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="ExcIntNo" HeaderText="ExcIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ExcRegNo" HeaderText="<%$Resources:dgExclusionList.HeaderText %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ExcOfficialCapacity" HeaderText="<%$Resources:dgExclusionList.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgExclusionListItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <asp:Panel ID="pnlAddExclusion" runat="server" Height="127px">
                                    <table id="Table2" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="lblAddExclusion" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddExclusion.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="lblAddExcRegNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddExcRegNo.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddExcRegNo" runat="server" Width="145px" CssClass="NormalMand"
                                                    MaxLength="20" Height="24px"></asp:TextBox></td>
                                            <td height="25">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <p>
                                                    <asp:Label ID="lblAddExcOfficialCapacity" runat="server" Width="112px" CssClass="NormalBold" Text="<%$Resources:lblAddExcOfficialCapacity.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddExcOfficialCapacity" runat="server" Width="264px" CssClass="Normal" MaxLength="255"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                            <td>
                                                </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 13px" width="157">
                                                <asp:Label ID="lblAddExcFromDate" runat="server" Width="133px" CssClass="NormalBold" Text="<%$Resources:lblAddExcFromDate.Text %>"></asp:Label></td>
                                            <td style="height: 13px" width="248">
                                        
                                      <asp:TextBox runat="server" ID="dtpAddExcFromDate" CssClass="Normal" Height="20px" 
                                                    autocomplete="off" UseSubmitBehavior="False" 
                                                    ontextchanged="dtpAddExcFromDate_TextChanged" AutoPostBack="true"/>
                                                        <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="dtpAddExcFromDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                                </td>
                                            <td style="height: 13px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <asp:Label ID="lblAddExcToDate" runat="server" Width="147px" CssClass="NormalBold" Text="<%$Resources:lblAddExcToDate.Text %>"></asp:Label></td>
                                            <td width="248">
                                           
                                
                                   <asp:TextBox runat="server" ID="dtpAddExcToDate" CssClass="Normal" Height="20px" autocomplete="off" UseSubmitBehavior="False" />
                                   
                                                        <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="dtpAddExcToDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddExclusion" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddExclusion.Text %>"
                                                    OnClick="btnAddExclusion_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateExclusion" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="279" height="2">
                                                <asp:Label ID="lblUpdateExclusion" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblUpdateExclusion.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="279" height="25">
                                                <asp:Label ID="lblUpdExcRegNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddExcRegNo.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtUpdExcRegNo" runat="server" Width="134px" CssClass="NormalMand"
                                                    MaxLength="20" Height="24px"></asp:TextBox></td>
                                            <td height="25">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="279">
                                                <p>
                                                    <asp:Label ID="Label16" runat="server" Width="112px" CssClass="NormalBold" Text="<%$Resources:lblAddExcOfficialCapacity.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248">
                                                <asp:TextBox ID="txtUpdExcOfficialCapacity" runat="server" Width="264px" CssClass="Normal" MaxLength="255"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                            <td>
                                                </td>
                                        </tr>
                                        <tr>
                                            <td width="279">
                                                <asp:Label ID="Label15" runat="server" Width="133px" CssClass="NormalBold" Text="<%$Resources:lblAddExcFromDate.Text %>"></asp:Label></td>
                                            <td width="248">
                                            
                                            <asp:TextBox runat="server" ID="dtpUpdExcFromDate" CssClass="Normal" Height="20px" 
                                                    autocomplete="off" UseSubmitBehavior="False" AutoPostBack="true"
                                                    ontextchanged="dtpUpdExcFromDate_TextChanged" />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="dtpUpdExcFromDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="279" style="height: 21px">
                                                <asp:Label ID="Label14" runat="server" Width="147px" CssClass="NormalBold" Text="<%$Resources:lblAddExcToDate.Text %>"></asp:Label></td>
                                            <td width="248" style="height: 21px">    
                                   <asp:TextBox runat="server" ID="dtpUpdExcToDate" CssClass="Normal" Height="20px" autocomplete="off" UseSubmitBehavior="False" />
                                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="dtpUpdExcToDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                            </td>
                                            <td style="height: 21px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="279">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
         </ContentTemplate>
                </asp:UpdatePanel>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
