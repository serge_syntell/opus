using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace Stalberg.TMS 
{

    public partial class MenuBuilder : System.Web.UI.Page {

        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected string thisPageURL = "MenuBuilder.aspx";
		protected int type = 0;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Menu Builder";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

    	protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//anyone with Access level < 10 may not access the system menu
			Stalberg.TMS.UserDetails userDetails = new UserDetails();
			userDetails = (UserDetails)Session["userDetails"];

			loginUser = userDetails.UserLoginName;

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			if (userAccessLevel<10)
				type = 2;
			else 
				type = 0; //if a system admin user, show all pages

			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
				lblError.Visible = false;
				pnlShowMenuItem.Visible = false;
				pnlAddMenuItem.Visible = false;

				PopulateASPPageList();

				if (Request.QueryString["mmiIntNo"] != null)
				{
					// show main menu item
					ShowMenuItem("mmiIntNo", Convert.ToInt32(Request.QueryString["mmiIntNo"]));
				}
				else if (Request.QueryString["miIntNo"] != null)
				{
					// show menu item
					ShowMenuItem("miIntNo", Convert.ToInt32(Request.QueryString["miIntNo"]));
				}
				else if (Request.QueryString["smiIntNo"] != null)
				{
					// show sub menu item
					ShowMenuItem("smiIntNo", Convert.ToInt32(Request.QueryString["smiIntNo"]));
				}
				else if (Request.QueryString["menuIntNo"] != null)
				{
					// show main menu item
					ShowMenuItem("menuIntNo", Convert.ToInt32(Request.QueryString["menuIntNo"]));
				}
			}
		}

		protected void PopulateASPPageList()
		{
			ASPPageListDB pages = new ASPPageListDB(connectionString);

			SqlDataReader reader = pages.GetASPPageListList(type);

			lstASPPageList.DataSource = reader;
			lstASPPageList.DataValueField = "APLIntNo";
			lstASPPageList.DataTextField = "APLPageName";
			lstASPPageList.DataBind();
			
			reader.Close();

		}

		protected void ShowMenuItem (string colName, int colValue)
		{
			pnlShowMenuItem.Visible = true;
			pnlAddMenuItem.Visible = false;

			txtColName.Text = colName;
			txtColValue.Text = colValue.ToString();

			switch (colName)
			{
				case "menuIntNo":
					//only allow them to add new main menu items
					pnlAddMenuItem.Visible = true;
					pnlShowMenuItem.Visible = false;

					txtAddColName.Text = "mmiIntNo";
					txtAddFKValue.Text = txtColValue.Text;
					break;

				case "mmiIntNo":
					//show main menu item
					MainMenuItemDB mainMenuItem = new MainMenuItemDB(connectionString);
					MainMenuItemDetails mainMenuItemDet = mainMenuItem.GetMainMenuItemDetails(colValue);

					txtName.Text = mainMenuItemDet.MMIName;
					txtDescr.Text = mainMenuItemDet.MMIDescr;
					txtURL.Text = mainMenuItemDet.MMIurl;
					txtOrder.Text = mainMenuItemDet.MMIOrder.ToString();
					txtFKValue.Text = mainMenuItemDet.MenuIntNo.ToString();
					break;

				case "miIntNo":
					//show menu item
					MenuItemDB menuItem = new MenuItemDB(connectionString);
					MenuItemDetails menuItemDet = menuItem.GetMenuItemDetails(colValue);

					txtName.Text = menuItemDet.MIName;
					txtDescr.Text = menuItemDet.MIDescr;
					txtURL.Text = menuItemDet.MIurl;
					txtOrder.Text = menuItemDet.MIOrder.ToString();
					txtFKValue.Text = menuItemDet.MMIIntNo.ToString();
					break;

				case "smiIntNo":
					//show sub-menu item
					SubMenuItemDB subMenuItem = new SubMenuItemDB(connectionString);
					SubMenuItemDetails subMenuItemDet = subMenuItem.GetSubMenuItemDetails(colValue);

					txtName.Text = subMenuItemDet.SMIName;
					txtDescr.Text = subMenuItemDet.SMIDescr;
					txtURL.Text = subMenuItemDet.SMIurl;
					txtOrder.Text = subMenuItemDet.SMIOrder.ToString();
					txtFKValue.Text = subMenuItemDet.MIIntNo.ToString();
					break;
			}
		}

		protected void btnSelectPage_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (lstASPPageList.SelectedIndex < 0)
			{
				lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
				return;
			}

			int aplIntNo = Convert.ToInt32(lstASPPageList.SelectedValue);

			ASPPageListDB pages = new ASPPageListDB(connectionString);

			ASPPageListDetails pageDetails = pages.GetASPPageListDetails(aplIntNo);

			txtURL.Text = pageDetails.APLPageURL;
			txtName.Text = pageDetails.APLPageName;
			txtDescr.Text = pageDetails.APLPageDescr;
		}

		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			if (txtName.Text.Equals(""))
			{
				lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
				return;
			}
			int updValue = 0;
			int colValue = Convert.ToInt32(txtColValue.Text);
			int fkValue = Convert.ToInt32(txtFKValue.Text);
			int order = Convert.ToInt32(txtOrder.Text);

			switch (txtColName.Text)
			{
				case "mmiIntNo":
					//update main menu item
					MainMenuItemDB mainMenuItem = new MainMenuItemDB(connectionString);
					updValue = mainMenuItem.UpdateMainMenuItem(colValue, fkValue, txtName.Text, txtDescr.Text, txtURL.Text, order, this.loginUser); 
					break;

				case "miIntNo":
					//update main menu item
					MenuItemDB menuItem = new MenuItemDB(connectionString);
					updValue = menuItem.UpdateMenuItem(colValue, fkValue, txtName.Text, txtDescr.Text, txtURL.Text, order, this.loginUser); 				
					break;

				case "smiIntNo":
					//show sub-menu item
					SubMenuItemDB subMenuItem = new SubMenuItemDB(connectionString);
					updValue = subMenuItem.UpdateSubMenuItem(colValue, fkValue, txtName.Text, txtDescr.Text, txtURL.Text, order, this.loginUser); 				
					break;
			}

			if (updValue == 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
			}
	
			lblError.Visible = true;
		}

		protected void btnAddSubItem_Click(object sender, System.EventArgs e)
		{
			pnlShowMenuItem.Visible = false;
			pnlAddMenuItem.Visible = true;

			txtAddFKValue.Text = txtColValue.Text;
			txtAddOrder.Text = "0";
			
			switch (txtColName.Text)
			{
				case "mmiIntNo":
					//adding a menu item
					txtAddColName.Text = "miIntNo";		
					break;
				case "miIntNo":
					//adding a sub menu item
					txtAddColName.Text = "smiIntNo";
					break;
				case "menuIntNo":
					//adding a main menu item
					txtAddColName.Text = "mmiIntNo";
					break;
			}

		}

		protected void btnAddItem_Click(object sender, System.EventArgs e)
		{
			if (txtAddName.Text.Equals(""))
			{
				lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
				return;
			}

			int addValue = 0;
			int fkValue = Convert.ToInt32(txtAddFKValue.Text);
			int order = Convert.ToInt32(txtAddOrder.Text);

			switch (txtAddColName.Text)
			{
				case "mmiIntNo":
					//add main menu item
					MainMenuItemDB mainMenuItem = new MainMenuItemDB(connectionString);
					addValue = mainMenuItem.AddMainMenuItem(fkValue, txtAddName.Text, txtAddDescr.Text, txtAddURL.Text, order, this.loginUser); 
					break;

				case "miIntNo":
					//add main menu item
					MenuItemDB menuItem = new MenuItemDB(connectionString);
					addValue = menuItem.AddMenuItem(fkValue, txtAddName.Text, txtAddDescr.Text, txtAddURL.Text, order, this.loginUser); 				
					break;

				case "smiIntNo":
					//add sub-menu item
					SubMenuItemDB subMenuItem = new SubMenuItemDB(connectionString);
					addValue = subMenuItem.AddSubMenuItem(fkValue, txtAddName.Text, txtAddDescr.Text, txtAddURL.Text, order, this.loginUser); 				
					break;
			}

			if (addValue == 0)
			{
				lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
			}
			else
			{	
				lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
			}
	
			lblError.Visible = true;		
		}

		protected void btnReturn_Click(object sender, System.EventArgs e)
		{
			int fkValue = Convert.ToInt32(txtAddFKValue.Text);

			switch (txtAddColName.Text)
			{
				case "mmiIntNo":
					ShowMenuItem("menuIntNo", fkValue);
					break;

				case "miIntNo":
					ShowMenuItem("mmiIntNo", fkValue);
					break;

				case "smiIntNo":
					ShowMenuItem("miIntNo", fkValue);
					break;
			}	
		}

		protected void btnAddSelectPage_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (lstASPPageList.SelectedIndex < 0)
			{
				lblError.Visible = true;
				lblError.Text = (string)GetLocalResourceObject("lblError.Text");
				return;
			}

			int aplIntNo = Convert.ToInt32(lstASPPageList.SelectedValue);

			ASPPageListDB pages = new ASPPageListDB(connectionString);

			ASPPageListDetails pageDetails = pages.GetASPPageListDetails(aplIntNo);

			txtAddURL.Text = pageDetails.APLPageURL;
			txtAddName.Text = pageDetails.APLPageName;
			txtAddDescr.Text = pageDetails.APLPageDescr;		
		}

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
			string delValue = string.Empty;
			int colValue = Convert.ToInt32(txtColValue.Text);

			switch (txtColName.Text)
			{
				case "mmiIntNo":
					//update main menu item
					MainMenuItemDB mainMenuItem = new MainMenuItemDB(connectionString);
					delValue = mainMenuItem.DeleteMainMenuItem(colValue); 
					break;

				case "miIntNo":
					//update main menu item
					MenuItemDB menuItem = new MenuItemDB(connectionString);
					delValue = menuItem.DeleteMenuItem(colValue); 				
					break;

				case "smiIntNo":
					//show sub-menu item
					SubMenuItemDB subMenuItem = new SubMenuItemDB(connectionString);
					delValue = subMenuItem.DeleteSubMenuItem(colValue); 				
					break;
			}

			if (delValue.Equals(""))
			{
				lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
			}
			else if (Int32.Parse(delValue) < 0)
			{
				lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
			}
			else
			{	
				lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
			}
	
			lblError.Visible = true;		
		}

    }
}
