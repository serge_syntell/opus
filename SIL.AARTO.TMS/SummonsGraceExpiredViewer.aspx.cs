using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for FirstNotice1.
    /// </summary>
    public partial class SummonsGraceExpiredViewer : System.Web.UI.Page
    {
        // Fields
        private string login = string.Empty;
        private string connectionString = string.Empty;
        private Int32 autIntNo = 0;
        protected string thisPageURL = "SummonsGraceExpiredViewer.aspx";
        //protected string thisPage = "Summons - Grace Expired Report Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //if (Request.QueryString["printfile"] == null)
            //    Server.Transfer("Login.aspx?Login=invalid");

            autIntNo = (int)Session["AutIntNo"];

            // Generate the report details
            ReportDocument reportDoc = new ReportDocument();

            //// Check the QueryString for the 'Summary' flag
            //string printFile = Request.QueryString["printfile"].ToString().Trim();

            AuthReportNameDB arn = new AuthReportNameDB(this.connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "SummonsGracePeriodExpired");

            if (reportPage.Equals(string.Empty))
            {
                reportPage = "SummonsGracePeriodExpired.rpt";
                arn.AddAuthReportName(autIntNo, reportPage, "SummonsGracePeriodExpired", "System", "");
            }
            else
            {
                reportPage = "SummonsGracePeriodExpired.rpt";
            }

            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "SummonsGracePeriodExpired");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            reportDoc.Load(reportPath);

            // Get the PageMargins structure and set the margins for the report.
            PageMargins margins = reportDoc.PrintOptions.PageMargins;
            margins.leftMargin = 0;
            margins.topMargin = 0;
            margins.rightMargin = 0;
            margins.bottomMargin = 0;

            // Apply the page margins.
            reportDoc.PrintOptions.ApplyPageMargins(margins);

            // Retrieve the data source
            //string tempFile = printFile;
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand com = new SqlCommand("SummonsGraceExpiry", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int).Value = autIntNo;

            SqlDataAdapter da = new SqlDataAdapter(com);

            dsPrintWOA ds = new dsPrintWOA();
            da.Fill(ds);
            da.Dispose();

            if (ds.Tables[1].Rows.Count == 0)
            {
                ds.Dispose();
                Response.Write((string)GetLocalResourceObject("strWriteMsg"));
                Response.End();
                return;
            }

            //reportDoc.PrintOptions.PaperSize = PaperSize.PaperFanfoldStdGerman; // 8.5" x 12"

            // Bind the report to the data
            reportDoc.SetDataSource(ds.Tables[1]);
            ds.Dispose();

            // Perform the export
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            reportDoc.Dispose();

            // Insert the PDF file as the only content in the Web stream
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();


        }
    }
}