using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;


namespace Stalberg.TMS
{
	/// <summary>
	/// Defines a page for setting up the relationships between ledger accounts and charge types, and between users and 
	/// bank accounts so that they can act as cashiers
	/// </summary>
	public partial class ReceiptEndOfDay : System.Web.UI.Page
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
		protected string thisPageURL = "ReceiptEndOfDay.aspx";
        //protected string thisPage = "Receipts: End of day routines";

		// Fields
		private int autIntNo = 0;

		// Constants
        //private const string LA_SELECT_TEXT = "Select an Authority";

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event to initialize the page.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
		override protected void OnInit (EventArgs e)
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}

		protected void Page_Load (object sender, System.EventArgs e)
		{
            connectionString = Application["constr"].ToString();

			btnOptAdd.Visible = false;
			//get user info from session variable
			if (Session["userDetails"] == null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"] == null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
			int userAccessLevel = userDetails.UserAccessLevel;
			loginUser = userDetails.UserLoginName;

			//int 
			this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

			// Set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
			title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlGeneral.Visible = true;
				panelCashierAccount.Visible = false;
				btnOptDelete.Visible = false;

				PopulateCashboxes(autIntNo);

				if (autIntNo > 0)
				{
					// BindGrid(autIntNo);
					// PopulateCourt (ddlAuth_Court, autIntNo);
				}
				else
				{
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
					lblError.Visible = true;
				}
			}
		}
        protected void PopulateCashboxes(int autIntNo)
        {
            Stalberg.TMS.CashboxDB cashboxes = new Stalberg.TMS.CashboxDB(connectionString);

            SqlDataReader reader = cashboxes.GetCashboxList(autIntNo);
            ddlCashboxes.DataSource = reader;
            ddlCashboxes.DataValueField = "CBIntNo";
            ddlCashboxes.DataTextField = "CBName";
            ddlCashboxes.DataBind();
            //ddlCashboxes.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            reader.Close();

        }

        protected void btnPrintBalancingReport_Click(object sender, EventArgs e)
        {
            DateTime tempDate = DateTime.Today;
            string cbDate = "messed up";
            string cbIntNo = ddlCashboxes.SelectedValue.ToString();
            try
            {
                tempDate = Convert.ToDateTime(txtReceiptDate.Text);
                cbDate = txtReceiptDate.Text;
            }
            catch
            {
            }
            if (cbDate == "messed up")
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }
            //Display report viewer in new window
            String strRedirectScript;
            // Generate the JavaScript to pop up a print-only window
            strRedirectScript = "<script language =javascript>";
            strRedirectScript += "window.open('ReceiptEODBalancingViewer.aspx?cbIntNo=" + cbIntNo + "&cbDate=" + cbDate + "','new','toolbar=1,scrollbars=1,location=0,statusbar=1,menubar=1,resizable=1,fullscreen=0,height=750,width=1024')";
            strRedirectScript += "</script> ";
            // Add our JavaScript to the currently rendered page
            Response.Write(strRedirectScript);
        }

        //protected void PopulateAccount (int autIntNo, DropDownList ddlSelectAccount)
        //{
        //    AccountDB road = new AccountDB(connectionString);

        //    SqlDataReader reader = road.GetAccountList(autIntNo);
        //    ddlSelectAccount.DataSource = reader;
        //    ddlSelectAccount.DataValueField = "AccIntNo";
        //    ddlSelectAccount.DataTextField = "LongName";
        //    ddlSelectAccount.DataBind();

        //    reader.Close();
        //}

        //protected void PopulateBankAccount (int autIntNo, DropDownList ddlSelectBankAccount)
        //{
        //    BankAccountDB road = new BankAccountDB(connectionString);

        //    SqlDataReader reader = road.GetBankAccountList(autIntNo);

        //    ddlSelectBankAccount.Items.Clear();
        //    ddlSelectBankAccount.Items.Add(new ListItem("[ None ]", "0"));
        //    while (reader.Read())
        //        ddlSelectBankAccount.Items.Add(new ListItem(reader["LongName"].ToString(), reader["BAIntNo"].ToString()));

        //    //ddlSelectBankAccount.DataSource = reader;
        //    //ddlSelectBankAccount.DataValueField = "BAIntNo";
        //    //ddlSelectBankAccount.DataTextField = "LongName";
        //    //ddlSelectBankAccount.DataBind();

        //    reader.Close();
        //}

        //protected void PopulateChargeType (DropDownList ddlChargeType)
        //{
        //    ChargeTypeDB ct = new ChargeTypeDB(connectionString);

        //    SqlDataReader reader = ct.GetChargeTypeList("All");
        //    ddlChargeType.DataSource = reader;
        //    ddlChargeType.DataValueField = "CTIntNo";
        //    ddlChargeType.DataTextField = "ChargeType";
        //    ddlChargeType.DataBind();
        //    reader.Close();
        //}

        //protected void PopulateAuthorities (int ugIntNo, int autIntNo)
        //{
        //    UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

        //    SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);

        //    ddlSelectLA.Items.Add(LA_SELECT_TEXT);
        //    while (reader.Read())
        //    {
        //        ListItem item = new ListItem(reader["AutName"].ToString(), reader["AutIntNo"].ToString());
        //        if (item.Value.Equals(this.autIntNo.ToString()))
        //            item.Selected = true;
        //        this.ddlSelectLA.Items.Add(item);
        //    }

        //    reader.Close();

        //    if (this.autIntNo >= 0)
        //        this.PopulateCashierGrid();
        //}

        //protected void btnAddAccountCharge_Click (object sender, EventArgs e)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    if (this.ddlSelectLA.SelectedValue == LA_SELECT_TEXT)
        //        sb.Append("You need to select a local authority before you can link ledger accounts to charge types.<br/>");

        //    if (string.IsNullOrEmpty(this.ddlSelectAccount.SelectedValue))
        //        sb.Append("You need to select a ledger account.<br/>");

        //    if (string.IsNullOrEmpty(this.ddlSelectChargeType.SelectedValue))
        //        sb.Append("You need to select a Charge type to bind a Ledger account to.<br/>");

        //    if (sb.ToString().Length > 0)
        //    {
        //        this.lblError.Text = sb.ToString();
        //        return;
        //    }

        //    int accIntNo = Convert.ToInt32(ddlSelectAccount.SelectedValue);
        //    int ctIntNo = Convert.ToInt32(ddlSelectChargeType.SelectedValue);
        //    Stalberg.TMS.AccountDB acCTAdd = new AccountDB(connectionString);
        //    int acCtIntNo = acCTAdd.AddAcc_CT(accIntNo, ctIntNo, loginUser);
        //    if (acCtIntNo <= 0)
        //        lblError.Text = "Unable to add this Account / Charge type pair";
        //    else
        //        lblError.Text = "Account / Charge pair has been added";
        //}

            protected void btnOptDelete_Click (object sender, EventArgs e)
            {
        //    int rctIntNo = Convert.ToInt32(Session["editRctIntNo"]);

        //    CashReceiptDB receipt = new Stalberg.TMS.CashReceiptDB(connectionString);

        //    string delRctIntNo = receipt.DeleteCashReceipt(rctIntNo);

        //    if (delRctIntNo.Equals("0"))
        //        lblError.Text = "Unable to delete this receipt";
        //    else if (delRctIntNo.Equals("-1"))
        //        lblError.Text = "Unable to delete this receipt - it has been linked to payment details";
        //    else
        //        lblError.Text = "Receipt has been deleted";

        //    lblError.Visible = true;

        //    int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            }

        //protected void ddlSelectLA_SelectedIndexChanged (object sender, EventArgs e)
        //{
        //    if (this.ddlSelectLA.SelectedValue == LA_SELECT_TEXT)
        //    {
        //        this.panelCashierAccount.Visible = false;
        //        this.ddlSelectAccount.Items.Clear();
        //        this.ddlSelectChargeType.Items.Clear();
        //        return;
        //    }

        //    this.PopulateCashierGrid();
        //}

        //private void PopulateCashierGrid ()
        //{
        //    int authIntNo = int.Parse(ddlSelectLA.SelectedValue);
        //    this.ViewState.Add("AutIntNo", authIntNo);
        //    this.PopulateBankAccount(authIntNo, this.ddlBankAccount);
        //    this.PopulateCashierGrid(authIntNo);
        //    this.labelCashiers.Text = "Cashiers for " + this.ddlSelectLA.SelectedItem.Text;
        //    this.panelCashierAccount.Visible = true;
        //    this.PopulateAccount(authIntNo, this.ddlSelectAccount);
        //    this.PopulateChargeType(this.ddlSelectChargeType);
        //}

        //private void PopulateCashierGrid (int authIntNo)
        //{
        //    BankAccountDB bankAccount = new BankAccountDB(connectionString);
        //    DataSet ds = bankAccount.GetAuthorityBankAccounts(authIntNo);
        //    this.gridCashiers.DataSource = ds;
        //    this.gridCashiers.DataBind();
        //}

        protected void gridCashiers_ItemCommand (object source, DataGridCommandEventArgs e)
        {
        //    int userIntNo = int.Parse(e.Item.Cells[0].Text);
        //    int baIntNo = int.Parse(this.ddlBankAccount.SelectedValue);

        //    BankAccountDB bankAccount = new BankAccountDB(connectionString);
        //    bankAccount.SetAuthorityCashierBankAccount(userIntNo, baIntNo);

        //    int authIntNo = (int)this.ViewState["AutIntNo"];
        //    this.PopulateCashierGrid(authIntNo);

        }
    }
}
