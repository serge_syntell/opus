﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stalberg.TMS;
using System.Data.SqlClient;

namespace SIL.AARTO.TMS
{
    public partial class NonCameraImageControl : System.Web.UI.UserControl
    {
        public string imageUrls;
        public string notIntNo;
        private string connectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["_notIntNo"] != null)
                {
                    this.hd_notIntNO.Value = Convert.ToInt32(Session["_notIntNo"]).ToString();

                   // Session["_notIntNo"] = null;
                }
              
               
            }
            imageUrls = GetNonCameraImage();
           
           // ljj.Value = notIntNo;
        }
        protected override void OnInit(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();
          
            base.OnInit(e);
        }
        private string GetNonCameraImage()
        {
            string str = "";
            int notIntNo = 0;
            int.TryParse(this.hd_notIntNO.Value, out notIntNo);
            NoticeDB notice = new NoticeDB(this.connectionString);
            SqlDataReader reader = notice.GetGetNonCameraImgs(notIntNo, this.connectionString);
            // StringBuilder sb = new StringBuilder();

            while (reader.Read())
            {
                //ad.,n.,n.NotTicketNo
                string NotIntNo = reader["NotIntNo"] == null ? "" : Convert.ToString(reader["NotIntNo"]);
                //string ImageMachineName = reader["ImageMachineName"] == null ? "" : Convert.ToString(reader["ImageMachineName"]);
                //string ImageShareName = reader["ImageShareName"] == null ? "" : Convert.ToString(reader["ImageShareName"]);
                string AaDocImgPath = reader["AaDocImgPath"] == null ? "" : Convert.ToString(reader["AaDocImgPath"]);
                string IFSIntNo = reader["IFSIntNo"] == null ? "" : Convert.ToString(reader["IFSIntNo"]);

                str += "NonCameraImage.aspx?IFSIntNo=" + HttpUtility.UrlEncode(IFSIntNo)
              + "&JpegName=" + HttpUtility.UrlEncode(AaDocImgPath);
                str += ",";
               
            }
            if (str.Length > 0)
            {
                str = str.Substring(0, str.Length - 1);
            }
            reader.Close();
            reader.Dispose();

            return str;
        }
    }
}