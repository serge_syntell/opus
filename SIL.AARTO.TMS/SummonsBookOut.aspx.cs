using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that displays the details for summonses for an LA
    /// </summary>
    public partial class SummonsBookOut : System.Web.UI.Page
    {
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "SummonsBookOut.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Book out Summons to Sumons Servers";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            pnlGeneral.Visible = true;
            dgPrintrun.Visible = true;

            if (ddlSelectLA.SelectedIndex > -1)
            {
                autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                Session["printAutIntNo"] = autIntNo;
            }

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                PopulateAuthorites( autIntNo);
                this.BindGrid(this.autIntNo);
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorites(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void BindGrid(int autIntNo)
        {
            Stalberg.TMS.SummonsDB printList = new Stalberg.TMS.SummonsDB(connectionString);
            int totalCount = 0;
            dgPrintrun.DataSource = printList.GetBookOutList(autIntNo, dgPrintrunPager.PageSize, dgPrintrunPager.CurrentPageIndex, out totalCount);
            dgPrintrun.DataKeyField = "SumPrintFileName";
            dgPrintrun.DataBind();
            dgPrintrunPager.RecordCount = totalCount;

            try
            {
                dgPrintrun.DataBind();
            }
            catch
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.DataBind();
            }

            if (dgPrintrun.Items.Count == 0)
            {
                dgPrintrun.Visible = false;
                lblError.Visible = true;
                if (!IsPostBack)
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgPrintrun.Visible = true;
            }
        }
       
        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            Session["printAutIntNo"] = autIntNo;
            dgPrintrunPager.CurrentPageIndex = 1; // 2013-03-15 add by Henry for pagination
            dgPrintrunPager.RecordCount = 0;
            this.BindGrid(autIntNo);
        }

        protected void dgPrintrun_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            dgPrintrun.SelectedIndex = e.Item.ItemIndex;

            if (dgPrintrun.SelectedIndex > -1)
            {
                string sumPrintFileName = e.Item.Cells[0].Text;

                BookOut(sumPrintFileName);
            }
        }

        protected void BookOut(string sumPrintFileName)
        {
            SummonsDB db = new SummonsDB(this.connectionString);

            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            string errMessage = string.Empty;

            int noOfRows = db.BookOutSummons(autIntNo, sumPrintFileName, login, ref errMessage);

            if (noOfRows > 0)
            {
                if (sumPrintFileName.Substring(0, 1).Equals("*"))
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text1"), sumPrintFileName.Substring(1, sumPrintFileName.Length - 1));
                else
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), noOfRows.ToString(), sumPrintFileName);

               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.BookOutSummons, PunchAction.Change);  

            }
            else
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3") + errMessage;

            lblError.Visible = true;
            dgPrintrunPager.CurrentPageIndex = 1; // 2013-03-15 add by Henry for pagination
            dgPrintrunPager.RecordCount = 0;
            BindGrid(autIntNo);

            //2013-05-15 start added by Nancy for opening not booked out report
            if (Session["SumPrintFileName"] == null)
                Session.Add("SumPrintFileName", sumPrintFileName);
            else
            {
                Session.Remove("SumPrintFileName");
                Session.Add("SumPrintFileName", sumPrintFileName);
            }

            if (db.GetNotBookedOutCount(sumPrintFileName) > 0)
            {
                PageToOpen page = new PageToOpen(this, "SummonsNotBookedOut.aspx");
                Helper_Web.BuildPopup(page);
            }
            //2013-05-15 end added by Nancy for opening not booked out report
        }


        protected void btnBookOut_Click(object sender, EventArgs e)
        {
            BookOut("*"+txtSummonsNo.Text.Trim());
        }

        protected void dgPrintrunPager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo);
        }
}
}
