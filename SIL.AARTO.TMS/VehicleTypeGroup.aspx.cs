using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class VehicleTypeGroup : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "VehicleTypeGroup.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "Vehicle type group maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;
			
			//int 
            //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
			autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddVehicleTypeGroup.Visible = false;
				pnlUpdateVehicleTypeGroup.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

                PopulateOffenceTypes();

				BindGrid();
			}		
		}

		protected void BindGrid()
		{
			// Obtain and bind a list of all user
			Stalberg.TMS.VehicleTypeGroupDB vtgList = new Stalberg.TMS.VehicleTypeGroupDB(connectionString);

            int totalCount = 0;
			DataSet data = vtgList.GetVehicleTypeGroupListDS(txtSearch.Text, dgVehicleTypeGroupPager.PageSize, dgVehicleTypeGroupPager.CurrentPageIndex, out totalCount);
			dgVehicleTypeGroup.DataSource = data;
			dgVehicleTypeGroup.DataKeyField = "VTGIntNo";
            dgVehicleTypeGroupPager.RecordCount = totalCount;

            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgVehicleTypeGroup.DataBind();
            }
            catch
            {
                dgVehicleTypeGroup.CurrentPageIndex = 0;
                dgVehicleTypeGroup.DataBind();
            }

			if (dgVehicleTypeGroup.Items.Count == 0)
			{
				dgVehicleTypeGroup.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
				dgVehicleTypeGroup.Visible = true;
			}

			pnlAddVehicleTypeGroup.Visible = false;
			pnlUpdateVehicleTypeGroup.Visible = false;

			data.Dispose();
		}

        protected void PopulateOffenceTypes()
        {
            OffenceTypeDB ot = new OffenceTypeDB(connectionString);

            DataSet ds = ot.GetOffenceTypeListForSpeedOffencesDS();
            ddlOffenceType.DataSource = ds;
            ddlOffenceType.DataValueField = "OcTIntNo";
            ddlOffenceType.DataTextField = "OffenceTypeDescr";
            ddlOffenceType.DataBind();

            ddlAddOffenceType.DataSource = ds;
            ddlAddOffenceType.DataValueField = "OcTIntNo";
            ddlAddOffenceType.DataTextField = "OffenceTypeDescr";
            ddlAddOffenceType.DataBind();

            //2012-3-6 linda modified into multi-language
            ddlAddOffenceType.Items.Insert(0, (string)GetLocalResourceObject("ddlAddOffenceType.Items"));

            ds.Dispose();
        }

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddVehicleTypeGroup.Visible = true;
			pnlUpdateVehicleTypeGroup.Visible = false;

			btnOptDelete.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
		}

		protected void btnAddVehicleTypeGroup_Click(object sender, System.EventArgs e)
		{
            if (ddlAddOffenceType.SelectedIndex < 1)
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            int maxSpeed = 0;

            if (!int.TryParse(txtAddVTGMaxSpeed.Text, out maxSpeed))
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            //SD: 20081209 -test for null speed
            StringBuilder sb = new StringBuilder();
            //Modefied By Linda 2012-2-27
            //Display multi - language error message is the value from the resource
            sb.Append((string)GetLocalResourceObject("lblError.Text3"));
            sb.Append("\n<ul>\n");
            if (maxSpeed < 0)
            {
                sb.Append("<li>");
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                sb.Append((string)GetLocalResourceObject("lblError.Text4"));
                sb.Append("</li></ul>");
                lblError.Text = sb.ToString();
            }

			// add the new transaction to the database table
			Stalberg.TMS.VehicleTypeGroupDB toAdd = new VehicleTypeGroupDB(connectionString);

            int ocTIntNo = Convert.ToInt32(ddlAddOffenceType.SelectedValue);

			int addVTGIntNo = toAdd.AddVehicleTypeGroup(txtAddVTGCode.Text, 
				txtAddVTGDescr.Text, loginUser, ocTIntNo, maxSpeed);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addVTGIntNo, lgEntityList, "VehicleTypeGroupLookup", "VTGIntNo", "VTGDescr", this.loginUser);


			if (addVTGIntNo <= 0)
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
				lblError.Text =(string)GetLocalResourceObject("lblError.Text5") ;
			}
			else
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleTypeGroupsMaintenance, PunchAction.Add);  

				
			}

			lblError.Visible = true;
            dgVehicleTypeGroupPager.RecordCount = 0;
            dgVehicleTypeGroupPager.CurrentPageIndex = 1;
			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
            int maxSpeed = 0;
            //SD: 20081209 -test for null speed
            StringBuilder sb1 = new StringBuilder();
            //Modefied By Linda 2012-2-27
            //Display multi - language error message is the value from the resource
            sb1.Append((string)GetLocalResourceObject("lblError.Text3"));
            sb1.Append("\n<ul>\n");
            if (maxSpeed < 0)
            {
                sb1.Append("<li>");
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                sb1.Append((string)GetLocalResourceObject("lblError.Text4"));
                sb1.Append("</li></ul>");
                lblError.Text = sb1.ToString();
            }


            if (!int.TryParse(txtVTGMaxSpeed.Text, out maxSpeed))
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            

			int vtgIntNo = Convert.ToInt32(Session["editVTGIntNo"]);
            int ocTIntNo = Convert.ToInt32(ddlOffenceType.SelectedValue);

			// add the new transaction to the database table
			Stalberg.TMS.VehicleTypeGroupDB vtgUpdate = new VehicleTypeGroupDB(connectionString);
			
			int updVTGIntNo = vtgUpdate.UpdateVehicleTypeGroup(vtgIntNo, 
				txtVTGCode.Text, txtVTGDescr.Text, loginUser, ocTIntNo, maxSpeed);

			if (updVTGIntNo <= 0)
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
			}
			else
			{
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(updVTGIntNo, lgEntityList, "VehicleTypeGroupLookup", "VTGIntNo", "VTGDescr", this.loginUser);
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleTypeGroupsMaintenance, PunchAction.Change); 
			}

			lblError.Visible = true;
            //Heidi 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
           // dgVehicleTypeGroupPager.RecordCount = 0;
           //dgVehicleTypeGroupPager.CurrentPageIndex = 1;
			BindGrid();
		}

		protected void dgVehicleTypeGroup_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgVehicleTypeGroup.SelectedIndex = e.Item.ItemIndex;

			if (dgVehicleTypeGroup.SelectedIndex > -1) 
			{			
				int editVTGIntNo = Convert.ToInt32(dgVehicleTypeGroup.DataKeys[dgVehicleTypeGroup.SelectedIndex]);

				Session["editVTGIntNo"] = editVTGIntNo;
				Session["prevPage"] = thisPageURL;


                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editVTGIntNo.ToString(), "VehicleTypeGroupLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);


				ShowVehicleTypeGroupDetails(editVTGIntNo);
			}
		}

		protected void ShowVehicleTypeGroupDetails(int editVTGIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.VehicleTypeGroupDB vtg = new Stalberg.TMS.VehicleTypeGroupDB(connectionString);
			
			Stalberg.TMS.VehicleTypeGroupDetails vtgDetails = vtg.GetVehicleTypeGroupDetails(editVTGIntNo);

			txtVTGCode.Text = vtgDetails.VTGCode;
			txtVTGDescr.Text = vtgDetails.VTGDescr;

            ddlOffenceType.SelectedIndex = ddlOffenceType.Items.IndexOf(ddlOffenceType.Items.FindByValue(vtgDetails.OcTIntNo.ToString()));
            txtVTGMaxSpeed.Text = vtgDetails.VTGMaxSpeed.ToString();

			pnlUpdateVehicleTypeGroup.Visible = true;
			pnlAddVehicleTypeGroup.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int vtgIntNo = Convert.ToInt32(Session["editVTGIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "VehicleTypeGroupLookup", "VTGIntNo");

			VehicleTypeGroupDB vtg = new Stalberg.TMS.VehicleTypeGroupDB(connectionString);

			string delVTGIntNo = vtg.DeleteVehicleTypeGroup(vtgIntNo);

			if (delVTGIntNo.Equals("0"))
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
			}
			else if (delVTGIntNo.Equals("-1"))
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
			}
            else if (delVTGIntNo.Equals("-2"))
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            }
            else
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                //Heidi 2013-09-12 added for solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                if (dgVehicleTypeGroup.Items.Count == 1)
                {
                    dgVehicleTypeGroupPager.CurrentPageIndex--;
                }
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleTypeGroupsMaintenance, PunchAction.Delete); 

            }
			lblError.Visible = true;
            //Heidi 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            //dgVehicleTypeGroupPager.RecordCount = 0;
            //dgVehicleTypeGroupPager.CurrentPageIndex = 1;
           
          
			BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgVehicleTypeGroup.Visible.Equals(true))
			{
				//hide it
				dgVehicleTypeGroup.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				//show it
				dgVehicleTypeGroup.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
            dgVehicleTypeGroupPager.RecordCount = 0;
            dgVehicleTypeGroupPager.CurrentPageIndex = 1;
			BindGrid();
            //PunchStats805806 enquiry VehicleTypeGroups
		}

        protected void dgVehicleTypeGroupPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
	}
}
