using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class RoadType : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "RoadType.aspx";
        protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "Road type maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
			autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddRoadType.Visible = false;
				pnlUpdateRoadType.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

				BindGrid();
			}		
		}

		protected void BindGrid()
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.RoadTypeDB rdtList = new Stalberg.TMS.RoadTypeDB(connectionString);

			DataSet data = rdtList.GetRoadTypeListDS();
			dgRoadType.DataSource = data;
			dgRoadType.DataKeyField = "RdTIntNo";
			dgRoadType.DataBind();

			if (dgRoadType.Items.Count == 0)
			{
				dgRoadType.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
				dgRoadType.Visible = true;
			}

			data.Dispose();

			pnlAddRoadType.Visible = false;
			pnlUpdateRoadType.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddRoadType.Visible = true;
			pnlUpdateRoadType.Visible = false;

			btnOptDelete.Visible = false;


            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
		}

		protected void btnAddRoadType_Click(object sender, System.EventArgs e)
		{		
			// add the new transaction to the database table
			
            Stalberg.TMS.RoadTypeDB rdtAdd = new RoadTypeDB(connectionString);
			
			int addRdTIntNo = rdtAdd.AddRoadType(Convert.ToInt32(txtAddRdTCode.Text), 
				txtAddRdTDescr.Text, this.loginUser);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addRdTIntNo, lgEntityList, "RoadTypeLookup", "RdTIntNo", "RdTypeDescr", this.loginUser);


			if (addRdTIntNo <= 0)
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1"); 
			}
			else
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RoadTypesMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			int rdTIntNo = Convert.ToInt32(Session["editRdTIntNo"]);

			// add the new transaction to the database table
			Stalberg.TMS.RoadTypeDB rdtUpdate = new RoadTypeDB(connectionString);
			
			int updRdTIntNo = rdtUpdate.UpdateRoadType(rdTIntNo, 
				Convert.ToInt32(txtRdTCode.Text), txtRdTDescr.Text, this.loginUser);

			if (updRdTIntNo <= 0)
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3"); ;
			}
			else
			{

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(updRdTIntNo, lgEntityList, "RoadTypeLookup", "RdTIntNo", "RdTypeDescr", this.loginUser);
            
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RoadTypesMaintenance, PunchAction.Change);  
			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void dgRoadType_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgRoadType.SelectedIndex = e.Item.ItemIndex;

			if (dgRoadType.SelectedIndex > -1) 
			{			
				int editRdTIntNo = Convert.ToInt32(dgRoadType.DataKeys[dgRoadType.SelectedIndex]);

				Session["editRdTIntNo"] = editRdTIntNo;
				Session["prevPage"] = thisPageURL;


                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editRdTIntNo.ToString(), "RoadTypeLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);


				ShowRoadTypeDetails(editRdTIntNo);
			}
		}

		protected void ShowRoadTypeDetails(int editRdTIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.RoadTypeDB roadType = new Stalberg.TMS.RoadTypeDB(connectionString);
			
			Stalberg.TMS.RoadTypeDetails rdtDetails = roadType.GetRoadTypeDetails(editRdTIntNo);

			txtRdTCode.Text = rdtDetails.RdTypeCode.ToString();
			txtRdTDescr.Text = rdtDetails.RdTypeDescr;
			
			pnlUpdateRoadType.Visible = true;
			pnlAddRoadType.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

	
		protected void dgRoadType_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgRoadType.CurrentPageIndex = e.NewPageIndex;

			BindGrid();
				
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int rdTIntNo = Convert.ToInt32(Session["editRdTIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "RoadTypeLookup", "RdTIntNo");

			RoadTypeDB roadType = new Stalberg.TMS.RoadTypeDB(connectionString);

			string delRdTIntNo = roadType.DeleteRoadType(rdTIntNo);

			if (delRdTIntNo.Equals("0"))
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5"); ;
			}
            else if (delRdTIntNo.Equals("-1"))
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6"); ;
            }
            else
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RoadTypesMaintenance, PunchAction.Delete);  
            }
			lblError.Visible = true;

			BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgRoadType.Visible.Equals(true))
			{
				//hide it
				dgRoadType.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				//show it
				dgRoadType.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		 
	}
}
