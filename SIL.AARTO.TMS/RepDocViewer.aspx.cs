using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;

namespace Stalberg.TMS
{
    public partial class RepDocViewer : System.Web.UI.Page
    {
        // Fields
        protected string connectionString = string.Empty;
        protected int rdIntNo = 0;
        protected string filename = string.Empty;
        protected string rdType = string.Empty;
        private int repIntNo = 0;
        private int srIntNo = 0;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string thisPageURL = "RepDocViewer.aspx";
        //protected string thisPage = "Representation Document Viewer";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
	    {
            connectionString = Application["constr"].ToString();

		    // Get user info from session variable
		    if (Session["userDetails"] == null)
			    Server.Transfer("Login.aspx?Login=invalid");

		    if (Session["userIntNo"] == null)
			    Server.Transfer("Login.aspx?Login=invalid");

            if (Session["ShowRDIntNo"] == null)
            {
                Response.Redirect("Login.aspx");
                return;
            }

            int rdIntNo = Convert.ToInt32(Session["ShowRDIntNo"]);
            this.repIntNo = Convert.ToInt32(Session["ShowRepIntNo"]);
            this.srIntNo = Convert.ToInt32(Session["ShowSRIntNo"]);


            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            RepDocumentDB repDoc = new RepDocumentDB(connectionString);
            //AccessDBMethods dbMethods = new AccessDBMethods(connectionString);
            ImageProcesses imageProcess = new ImageProcesses(this.connectionString);

            RepDocumentDetails repDocDetails = repDoc.GetRepDocumentDetails(rdIntNo, this.repIntNo, this.srIntNo);

            //set temp location for pdf export
            string tempFileLoc = repDocDetails.RDName + "." + repDocDetails.RDType;

            string tableName = string.Empty;

            if (repIntNo > 0)
                tableName = "RepDocument";
            else if (srIntNo > 0)
                tableName = "SummonsRepDocument";

            byte[] imageData = imageProcess.GetImageFromDB(rdIntNo, "RDIntNo", tableName, "RDImage");

            //dbMethods.DownLoadBLOB(rdIntNo, "RDIntNo", tableName, "RDImage", ref tempFileLoc, Server.MapPath("temp"));

            //string imageToView = "temp/" + tempFileLoc;

            title = (string)GetLocalResourceObject("title");

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/" + repDocDetails.RDType.ToLower();
            //Response.WriteFile(Server.MapPath(imageToView));
            Response.BinaryWrite(imageData);
            Response.End();

            //try
            //{
            //    if (File.Exists(Server.MapPath(imageToView)))
            //        File.Delete(Server.MapPath(imageToView));
            //}
            //catch { }
	    }

    }
}