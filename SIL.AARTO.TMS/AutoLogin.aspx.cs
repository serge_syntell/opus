﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIL.AARTO.TMS.UserAutoLogin;
using System.Data.SqlClient;
using Stalberg.TMS;
using System.Collections;
using System.Configuration;
using System.IO;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Culture;

public partial class AutoLogin : System.Web.UI.Page
{
    private AartoAndTMSAutoLogin autoLoginService = new AartoAndTMSAutoLogin();

    protected string styleSheet;
    public string environment = "P";
    private string connectionString = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        connectionString = Application["constr"].ToString();
        string PageID = Request.QueryString["PageID"];

        string Key = Request.QueryString["Key"];
        int autIntNo = string.IsNullOrEmpty(Request.QueryString["AutIntNo"]) ? 0 : int.Parse(Request.QueryString["AutIntNo"]);

        //SetLanguage(lsCode);
        string authName = Request.QueryString["AuthName"];

        //AartoLoginSynchronization aartoLoginSynchronization = null;
        AartoAndTMSAutoLoginEntity aartoLoginSynchronization = null;
        if (String.IsNullOrEmpty(Key))
        {
            aartoLoginSynchronization = new AartoAndTMSAutoLoginEntity();
        }
        else
        {
            try
            {
                aartoLoginSynchronization = autoLoginService.GetLoginSynchronizationByKey(Key);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        if (aartoLoginSynchronization == null || String.IsNullOrEmpty(aartoLoginSynchronization.AaLogSynSecretKey))
        {
            throw new Exception((string)GetLocalResourceObject("strException"));
        }
        else
        {
            //Request from  AARTO to TMS
            if (!String.IsNullOrEmpty(Key))
            {
                if (Key == aartoLoginSynchronization.AaLogSynSecretKey)
                {
                    AartoPageEntity page = autoLoginService.GetPageEntityByPageID(Convert.ToInt32(PageID));
                    if (page == null)
                    {
                        return;
                    }
                    // TMS user role level need to set here 

                    string url = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf(@"/"));

                    if (page.AaPageUrl.StartsWith(@"/"))
                    {
                        url = url + page.AaPageUrl;
                    }
                    else
                    {
                        url = url + @"/" + page.AaPageUrl;
                    }

                    //Response.Write("URL: " + url);
                    //Auto login aarto
                    this.connectionString = Application["constr"].ToString();
                    if (Session["IsAuthenticated"] != null && (bool)Session["IsAuthenticated"])//Page.Request.IsAuthenticated
                    {
                        //Jake 2011-01-11 added 
                        if (autIntNo > 0)
                        {
                            if (Session["autIntNo"] == null || autIntNo != (int)Session["autIntNo"])
                            {
                                InitSessonOfAuthority(autIntNo, ((UserDetails)Session["userDetails"]).UserLoginName);
                            }
                        }
                        if (!String.IsNullOrEmpty(authName))
                        {
                            Session["autName"] = authName;
                        }

                        //jerry 2011-0-11 added
                        AuthorityDB authority = new AuthorityDB(connectionString);
                        AuthorityDetails authDet = authority.GetAuthorityDetails(autIntNo);
                        Session["TicketProcessor"] = authDet.AutTicketProcessor;
                        //Response.Write(url);
                        //Response.End();
                        Response.Redirect(url);
                    }
                    else
                    {
                        // Jake 2011-01-10  Removed, this two functon has been added  at Login 
                        //GetDomainSettings();
                        //SetDomainSettings();
                        Session["AllImageFileServerAccessible"] = null;
                        //******************************
                        //Jerry 2012-08-20 get image path
                        string fullImagePath = string.Empty;
                        if (Session["AllImageFileServerAccessible"] == null)
                        {
                            fullImagePath = this.ShowUNCTestImage();
                        }
                        if (Session["AllImageFileServerAccessible"] == null
                            || (bool)Session["AllImageFileServerAccessible"] == false)
                        {
                            //Response.Write((string)GetLocalResourceObject("strMsg"));
                            if (string.IsNullOrWhiteSpace(fullImagePath))
                            {
                                Response.Write(string.Format((string)GetLocalResourceObject("ErrNotSetPath"), "AARTO.ImageFileServer"));
                            }
                            else
                            {
                                Response.Write(string.Format((string)GetLocalResourceObject("ErrAccessFail"), fullImagePath));
                            }
                            Response.End();
                        }
                        try
                        {
                            int autIntNoRequest = autIntNo;
                            if (Login(aartoLoginSynchronization.AaLogSynUserID,
                                autIntNoRequest, page.AaPageUrl))
                            {
                                if (autoLoginService.RemoveSecrectKeyByUserIDAndAuthID(
                                     aartoLoginSynchronization.AaLogSynUserID,
                                     aartoLoginSynchronization.AaLogSynAutIntNo) != null)
                                {
                                    if (autIntNo > 0)
                                    {
                                        Session["autIntNo"] = autIntNo;
                                    }
                                    if (!String.IsNullOrEmpty(authName))
                                    {
                                        Session["autName"] = authName;
                                    }

                                    //jerry 2011-0-11 added
                                    AuthorityDB authority = new AuthorityDB(connectionString);
                                    AuthorityDetails authDet = authority.GetAuthorityDetails(autIntNo);
                                    Session["TicketProcessor"] = authDet.AutTicketProcessor;
                                    //Response.Write(url);
                                    //Response.End();
                                    Response.Redirect(url);
                                }
                            }
                            //Jake 2014-05-12 added 
                            else
                            {
                                if (Session["ErrorMessage_Auto_Login"] != null)
                                {
                                    string msg = Session["ErrorMessage_Auto_Login"].ToString();
                                    Session["ErrorMessage_Auto_Login"] = null;
                                    Response.Redirect("~/Error.aspx?error=" + msg);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
            }


        }
    }

    /// <summary>
    /// test whether can access image file server
    /// Jerry 2012-08-20 change it, return image root path
    /// </summary>
    private string ShowUNCTestImage()
    {
        // Get image file servers
        try
        {
            string strTestImagePath = string.Empty;
            ImageFileServerDB imageFileServerDB = new ImageFileServerDB(connectionString);
            List<ImageFileServerDetails> fileServerList = imageFileServerDB.GetAllImageFileServers();
            if (fileServerList.Count > 0)
            {
                //IdentityAnalogue ia = null;
                foreach (ImageFileServerDetails imageFileServer in fileServerList)
                {
                    string strRootPath = @"\\" + imageFileServer.ImageMachineName + Path.DirectorySeparatorChar
                                   + imageFileServer.ImageShareName;

                    strTestImagePath = strRootPath + Path.DirectorySeparatorChar + "test.jpg";

                    //WebService service = new WebService();
                    bool isConnected = true;

                    if (!File.Exists(strTestImagePath))
                    {
                        isConnected = false;
                    }

                    // Set a flag about AllImageFileServerAccessible to protect image related pages.
                    Session["AllImageFileServerAccessible"] = isConnected;
                }
                return strTestImagePath;
            }
            else
            {
                Session["AllImageFileServerAccessible"] = false;
                return strTestImagePath;
            }
        }
        catch (Exception ex)
        {
            Session["AllImageFileServerAccessible"] = false;
            throw ex;
        }
    }

    private void InitSessonOfAuthority(int autIntNo, string userName)
    {
        Session["autIntNo"] = autIntNo;

        //20090113 SD	
        //AutIntNo, ARCode and LastUser need to be set from here
        AuthorityRulesDetails details = new AuthorityRulesDetails();
        details.AutIntNo = autIntNo;
        details.ARCode = "0400";
        details.LastUser = userName;

        DefaultAuthRules authRule = new DefaultAuthRules(details, this.connectionString);
        KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

        //this.Session.Add("NaTISLast", details.ARString.Equals("N") ? false : true);
        this.Session.Add("NaTISLast", value.Value.Equals("N") ? false : true);

        //dls 070120 - add rules for showing cross-hairs
        //details = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "3100", "Display cross-hairs for DigiCam images", 0, "Y", "Y = Yes (Default); N = No", txtLoginName.Text);

        AuthorityRulesDetails details3100 = new AuthorityRulesDetails();
        details.AutIntNo = autIntNo;
        details.ARCode = "3100";
        details.LastUser = userName;

        DefaultAuthRules authRule3100 = new DefaultAuthRules(details, this.connectionString);
        KeyValuePair<int, string> value3100 = authRule.SetDefaultAuthRule();

        //this.Session.Add("ShowCrossHairs", details.ARString.Equals("Y") ? true : false);
        this.Session.Add("ShowCrossHairs", value3100.Value.Equals("Y") ? true : false);

        //details = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "3150", "Style of DigiCam cross-hairs", 0, "", "0 = Standard yellow cross (Default); 1 = Yellow Cross with missing centre; 2 = Standard inverted cross; 3 = Inverted cross with missing centre", txtLoginName.Text);
        AuthorityRulesDetails details3150 = new AuthorityRulesDetails();
        details.AutIntNo = autIntNo;
        details.ARCode = "3150";
        details.LastUser = userName;

        DefaultAuthRules authRule3150 = new DefaultAuthRules(details, this.connectionString);
        KeyValuePair<int, string> value3150 = authRule.SetDefaultAuthRule();

        //this.Session.Add("CrossHairStyle", details.ARNumeric);
        this.Session.Add("CrossHairStyle", value3150.Key);

        AuthorityDB authority = new AuthorityDB(connectionString);
        AuthorityDetails authDet = authority.GetAuthorityDetails(autIntNo);

        Session["mtrIntNo"] = authDet.MtrIntNo;
        Session["TicketProcessor"] = authDet.AutTicketProcessor;
        Session["autName"] = authDet.AutName;
        //dls 080108 - added
        Session["autNo"] = authDet.AutNo;
    }

    private bool Login(int userIntNo, int authIntNo, string Url)
    {

        bool status = false;

        try
        {
            //check that login details match those in table
            UserDB user = new UserDB(connectionString);

            UserEntity userEntity = autoLoginService.GetUserByUserID(userIntNo);

            GetDomainSettings();
            SetDomainSettings();

            if (userIntNo == 0)
            {
                //lblError.Visible = true;
                //lblError.Text = "Invalid login details - login denied!";
                return status;
            }
            // LMZ 14-03-2007 - used for officer statistics
            int nUSIntNo = 0;
            if (Session["USIntNo"] == null)
                nUSIntNo = user.UserShiftUpdate(userIntNo, userEntity.UserLoginName);

            // LMZ 22-03-2007 - used to get assosciated traffic officer number
            string sOfficerNo = user.GetOfficerNo(userIntNo);
            if (sOfficerNo.Equals("0"))
            {
                Session["ErrorMessage_Auto_Login"] = "There is an exception when trying to get officer number for current login user.";
                return status;
            }
            int currentAuthIntNo;
            if (authIntNo > 0)
            {
                currentAuthIntNo = authIntNo;
            }
            else
            {
                currentAuthIntNo = userEntity.UserDefaultAutIntNo;
            }

            InitSessonOfAuthority(currentAuthIntNo, userEntity.UserLoginName);

            //pass userIntNo to session variables
            Session["userIntNo"] = userIntNo;
            Session["USIntNo"] = nUSIntNo;
            Session["OfficerNo"] = sOfficerNo;

            UserDetails userDet = ((UserDetails)Session["userDetails"]);
            //dls 030730 - added this at login
            user.UserShiftEdit(nUSIntNo, sOfficerNo, "Officer", userDet == null ? string.Empty : userDet.UserLoginName);

            // get the user authority levels, etc
            Stalberg.TMS.UserDetails userDetails = user.GetUserDetails(userIntNo);
            Session["userDetails"] = userDetails;
            Session["IsAuthenticated"] = true;
            status = true;

            return status;
        }
        catch
        {
            return status;
        }

    }

    private void SetDomainSettings()
    {
        //set domain specific variables
        General gen = new General();

        styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);

        //these have to get specifically set on the home page, as they are not populated earlier
        //title = gen.SetTitle(Session["drTitle"]);
        //description = gen.SetDescription(Session["drDescription"]);
        //keywords = gen.SetKeywords(Session["drKeywords"]);

        //imgLogo.ImageUrl = "~/App_Code/logos/Syntell_logo1.jpg";
        //imgEnter.ImageUrl = "~/images/enter.png";

        GetSystemParameters();
    }

    private void GetSystemParameters()
    {
        SysParamDB sysParam = new SysParamDB(connectionString);

        SqlDataReader reader = sysParam.GetSysParamList();

        ArrayList sysParamList = new ArrayList();


        while (reader.Read())
        {
            SysParamDetails spDetails = new SysParamDetails();

            spDetails.SPIntNo = Convert.ToInt32(reader["SPIntNo"]);
            spDetails.SPColumnName = reader["SPColumnName"].ToString();
            spDetails.SPIntegerValue = Convert.ToInt32(reader["SPIntegerValue"]);
            spDetails.SPStringValue = reader["SPStringValue"].ToString();

            sysParamList.Add(spDetails);

            if (spDetails.SPColumnName.Equals("EnvironmentType"))
                environment = spDetails.SPStringValue;
        }
        reader.Dispose();

        Application.Lock();
        Application["sysParamList"] = sysParamList;
        Application.UnLock();
    }

    public string GetEnvironmentType()
    {
        return environment;
    }

    private void GetDomainSettings()
    {
        //check rules and login requirements for domain
        DomainRuleDB domainRule = new DomainRuleDB(connectionString);

        //get the calling page URL

        string[] url = Request.Url.AbsoluteUri.ToString().Split('?');//ConfigurationManager.AppSettings["TMSDomainURL"]  + Application["website"] + "/Home.aspx";
        string webUrl = url[0].Substring(0, url[0].LastIndexOf("/") + 1);
        Session["homeUrl"] = webUrl;

        SqlDataReader domainDetails = domainRule.GetDRDetailsByURL(webUrl);

        while (domainDetails.Read())
        {
            Session["drLoginReq"] = domainDetails["DRLoginReq"].ToString();
            Session["drEmailReq"] = domainDetails["DREmailReq"].ToString();
            Session["drPasswdReq"] = domainDetails["DRPasswdReq"].ToString();
            Session["drBackground"] = domainDetails["DRBackground"].ToString();
            Session["drStyleSheet"] = domainDetails["DRStyleSheet"].ToString();
            Session["drTitle"] = domainDetails["DRTitle"].ToString();
            Session["drDescription"] = domainDetails["DRDescription"].ToString();
            Session["drKeywords"] = domainDetails["DRKeywords"].ToString();
            Session["drMainImage"] = domainDetails["DRMainImage"].ToString();
            Session["drAuthorityReq"] = domainDetails["drAuthorityReq"].ToString();
        }
        domainDetails.Close();

    }

}

