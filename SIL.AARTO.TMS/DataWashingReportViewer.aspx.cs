﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SIL.AARTO.BLL.Report;

namespace Stalberg.TMS
{
    public partial class DataWashingReportViewer : System.Web.UI.Page
    {
        private string connectionString = String.Empty;

        protected string title = String.Empty;

        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            General gen = new General();
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                string filename = "DataWashingStatistics " + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();

                SIL.AARTO.BLL.Report.DataWashingReport report = new SIL.AARTO.BLL.Report.DataWashingReport(connectionString);

                MemoryStream stream = report.ExportToExcel();

                Response.BinaryWrite(stream.GetBuffer());
                Response.End();
            }
        }
    }
}