<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ReceiptEndOfDay" Codebehind="ReceiptEndOfDay.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>" >
                                    </asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table height="482" width="100%" border="0">
                        <tr>
                            <td valign="top" style="height: 47px">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Width="332px" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneral" runat="server" Width="100%">
                                    <table id="Table2" height="125" cellspacing="1" cellpadding="1" border="0">
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:DropDownList ID="ddlCashboxes" runat="server" Width="194px">
                                                </asp:DropDownList></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSelectAccount" runat="server" CssClass="NormalBold" Width="103px" Text="<%$Resources:lblSelectAccount.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtReceiptDate" runat="server"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label11" runat="server" Width="382px" CssClass="Normal" Text="<%$Resources:lblBalanceReport.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:Button ID="btnPrintBalancingReport" runat="server" CssClass="NormalButton" OnClick="btnPrintBalancingReport_Click"
                                                    Text="<%$Resources:btnPrintBalancingReport.Text %>" Width="113px" /></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="panelCashierAccount" runat="server" CssClass="Normal" Width="100%">
                                    &nbsp;<br />
                                    <p align="center">
                                        <asp:Label ID="labelCashiers" runat="server" Text="<%$Resources:labelCashiers.Text %>" CssClass="ContentHead"></asp:Label>&nbsp;</p>
                                    <p align="center" class="NormalBold">
                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblBankAccount.Text %>"></asp:Label>
                                        <asp:DropDownList ID="ddlBankAccount" runat="server" CssClass="Normal">
                                        </asp:DropDownList></p>
                                    <p align="center">
                                        <asp:DataGrid ID="gridCashiers" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                                            ShowFooter="True" CellPadding="5" OnItemCommand="gridCashiers_ItemCommand">
                                            <HeaderStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundColumn DataField="UserIntNo" HeaderText="UserIntNo" Visible="False">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="BaIntNo" HeaderText="BAIntNo" Visible="False">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Name" HeaderText="<%$Resources:gridCashiers.HeaderText %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
<%--                                                <asp:BoundColumn DataField="UGName" HeaderText="<%$Resources:gridCashiers.HeaderText1 %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="USGName" HeaderText="<%$Resources:gridCashiers.HeaderText2 %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>--%>
                                                <asp:BoundColumn DataField="BAAccountName" HeaderText="<%$Resources:gridCashiers.HeaderText3 %>"></asp:BoundColumn>
                                                <asp:ButtonColumn CommandName="Select" HeaderText="<%$Resources:gridCashiers.HeaderText4 %>" Text="<%$Resources:gridCashiersItem.Text %>"></asp:ButtonColumn>
                                            </Columns>
                                            <FooterStyle CssClass="CartListHead" />
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>&nbsp;
                                    </p>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center" style="height: 20px">
                </td>
                <td valign="top" align="left" width="100%" style="height: 20px">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
