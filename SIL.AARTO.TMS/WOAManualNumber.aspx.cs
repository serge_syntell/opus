﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using SIL.ServiceBase;
using Stalberg.TMS_TPExInt.Components;
using System.Text.RegularExpressions;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class WOAManualNumber : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private int nDays = 0;
        private string issueWOANotice = string.Empty;
        private string woaNumberFormat = string.Empty;
        private string numericOnly = @"^\d+$";

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "WOAManualNumber.aspx";
        //protected string thisPage = "WOA - Manual allocation of WOA numbers";
        private string autTicketProcessor = string.Empty;

        private const string DATE_FORMAT = "yyyy-MM-dd";
        private const string DATE_AND_TIME_FORMAT = "yyyy-MM-dd_HH-mm";
        private int currentYear = DateTime.Now.Year;

        //jerry 2012-01-04 add
        private int noDaysForWOAExpiry;
        private string autCode = string.Empty;

        //Jerry 2012-05-04 add
        private string cRTranType = "WOA";
        private string cRTranDescr = "Next WOA number for court room.";

        //2014-01-10 Heidi added for not repeat push the queue(5101) 
        List<Int32> pfnIntNoList = new List<Int32>();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            string autName = Session["autName"].ToString();

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            lblError.Text = string.Empty;
            if (ViewState["WOA_days"] == null)
            {
                nDays = CheckDateRule(this.autIntNo);
                ViewState.Add("WOA_days", nDays);
            }
            else
            {
                nDays = int.Parse(ViewState["WOA_days"].ToString());
            }

            if (ViewState["IssueWOANotice"] == null)
            {
                issueWOANotice = CheckAuthorityRule(this.autIntNo);
                ViewState.Add("IssueWOANotice", issueWOANotice);
            }
            else
            {
                issueWOANotice = ViewState["IssueWOANotice"].ToString();
            }
            if (ViewState["WoaNumberFormat"] == null)
            {
                woaNumberFormat = CheckAuthorityRuleForWoaNumberFormat(this.autIntNo);
                ViewState.Add("WoaNumberFormat", woaNumberFormat);
            }
            else
            {
                woaNumberFormat = ViewState["WoaNumberFormat"].ToString();
            }

            if (ViewState["AutTicketProcessor"] == null)
            {
                AuthorityDetails myAuthorityDetails = new AuthorityDB(connectionString).GetAuthorityDetails(this.autIntNo);
                autTicketProcessor = myAuthorityDetails.AutTicketProcessor;
                ViewState.Add("AutTicketProcessor", autTicketProcessor);
            }
            else
            {
                autTicketProcessor = ViewState["AutTicketProcessor"].ToString();
            }

            if (ViewState["PrintFileDate"] == null)
            {
                ViewState.Add("PrintFileDate", DateTime.Now.ToString(DATE_AND_TIME_FORMAT));
            }

            //jerry 2012-01-04 add for pushing queue of cancel expired WOA
            GetDefaultDateRule();
            GetAutCode();

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlWOANos.Visible = false;
                pnlDecision.Visible = false;
                this.PopulateCourts();
                //2014-01-10 Heidi added for not repeat push the queue(5101) 
                pfnIntNoList.Clear();
            }
            this.btnYes.OnClientClick = "LoadingBlocker('" + (string)GetLocalResourceObject("msg_Processing") + "');";
        }

        /// <summary>
        /// jerry 2012-01-04 add 
        /// </summary>
        private void GetDefaultDateRule()
        {
            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = this.autIntNo;
            //Jake 2015-02-28 modified
            //rule.DtRStartDate = "WOAIssueDate";
            rule.DtRStartDate = "SumCourtDate";
            rule.DtREndDate = "WOAExpireDate";
            rule.LastUser = this.login;

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            noDaysForWOAExpiry = dateRule.SetDefaultDateRule();
        }

        /// <summary>
        /// jerry 2012-01-04 add autCode for push queue
        /// </summary>
        /// <param name="autIntNo"></param>
        private void GetAutCode()
        {
            this.autCode = new AuthorityDB(this.connectionString).GetAuthorityDetails(this.autIntNo).AutCode.Trim();
        }

        private int CheckDateRule(int nAutIntNo)
        {
            DateRulesDetails ruleWOAGenerate = new DateRulesDetails();
            ruleWOAGenerate.AutIntNo = nAutIntNo;
            ruleWOAGenerate.LastUser = this.login;
            ruleWOAGenerate.DtRStartDate = "SumCourtDate";
            ruleWOAGenerate.DtREndDate = "WOALoadDate";

            DefaultDateRules dateRule = new DefaultDateRules(ruleWOAGenerate, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return noOfDays;
        }

        private string CheckAuthorityRule(int nAutIntNo)
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = nAutIntNo;
            rule.ARCode = "5801";
            rule.LastUser = this.login;
            DefaultAuthRules authrule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> woaNoticePrintRule = authrule.SetDefaultAuthRule();
            return woaNoticePrintRule.Value;
        }

        private string CheckAuthorityRuleForWoaNumberFormat(int autIntNo)
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.ARCode = "9510";
            rule.LastUser = this.login;
            DefaultAuthRules authrule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> woaNoticePrintRule = authrule.SetDefaultAuthRule();
            return woaNoticePrintRule.Value;
        }

        private string CheckCourtRule(int crtIntNo)
        {
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = crtIntNo;
            courtRulesDetails.CRCode = "5010";
            courtRulesDetails.LastUser = this.login;
            DefaultCourtRules defaultCourtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
            KeyValuePair<int, string> courtRule = defaultCourtRule.SetDefaultCourtRule();
            return courtRule.Value;
        }

        protected void ddlCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourt.SelectedIndex > 0)
            {
                pnlWOANos.Visible = false;
                pnlDecision.Visible = false;
                PopulateCourtRooms(Convert.ToInt32(ddlCourt.SelectedValue));
                ddlCourtDates.Items.Clear();
            }
            else
            {
                //ddlCourtRoom.SelectedIndex = 0;
                ddlCourtRoom.Items.Clear();
                ddlCourtDates.Items.Clear();
            }
        }

        private void PopulateCourts()
        {
            CourtDB court = new CourtDB(this.connectionString);
            SqlDataReader reader = court.GetAuth_CourtListByAuth(this.autIntNo);
            this.ddlCourt.DataSource = reader;
            this.ddlCourt.DataValueField = "CrtIntNo";
            this.ddlCourt.DataTextField = "CrtDetails";
            this.ddlCourt.DataBind();
            reader.Close();
            this.ddlCourt.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourt.Items"), string.Empty));
        }

        private void PopulateCourtRooms(int nCrtIntNo)
        {
            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            SqlDataReader reader = room.GetCourtRoomListByCrtRStatusFlag(nCrtIntNo);//room.GetCourtRoomList(nCrtIntNo);

            //Jerry 2012-07-25 add
            ddlCourtRoom.Items.Clear();

            this.ddlCourtRoom.DataSource = reader;
            this.ddlCourtRoom.DataValueField = "CrtRIntNo";
            this.ddlCourtRoom.DataTextField = "CrtRoomDetails";
            this.ddlCourtRoom.DataBind();
            reader.Close();

            //Heidi 2013-09-23 removed translate from lookup table
            //Dictionary<int, string> lookups =
            //    CourtRoomLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            //while (reader.Read())
            //{
            //    int crtRIntNo = (int)reader["CrtRIntNo"];
            //    if (lookups.ContainsKey(crtRIntNo))
            //    {
            //        ddlCourtRoom.Items.Add(new ListItem(lookups[crtRIntNo], crtRIntNo.ToString()));
            //    }
            //}
            ////this.ddlCourtRoom.DataSource = reader;
            ////this.ddlCourtRoom.DataValueField = "CrtRIntNo";
            ////this.ddlCourtRoom.DataTextField = "CrtRoomDetails";
            ////this.ddlCourtRoom.DataBind();
            this.ddlCourtRoom.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourtRoom.Items"), "0"));
            //reader.Close();
        }

        //jerry 2012-03-15 add
        private void PopulateCourtDates(int crtRIntNo)
        {
            // Populate the court dates
            this.ddlCourtDates.Items.Clear();
            ListItem item;
            CourtDatesDB db = new CourtDatesDB(this.connectionString);

            SqlDataReader reader = db.GetCourtDatesList(crtRIntNo, this.autIntNo);
            while (reader.Read())
            {
                item = new ListItem();
                item.Value = reader["CDIntNo"].ToString();
                item.Text = String.Format("{0:yyyy-MM-dd}", reader["CDate"]);

                this.ddlCourtDates.Items.Add(item);
            }
            reader.Close();

            // Insert the dummy item
            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("ddlCourtDates.Items");
            this.ddlCourtDates.Items.Insert(0, item);
            this.ddlCourtDates.SelectedIndex = 0;
        }

        protected void ddlCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourtRoom.SelectedIndex > 0)
            {
                pnlWOANos.Visible = false;
                pnlDecision.Visible = false;
                PopulateCourtDates(Convert.ToInt32(ddlCourtRoom.SelectedValue));
            }
            else
            {
                //this.ddlCourtDates.SelectedIndex = 0;
                this.ddlCourtDates.Items.Clear();
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            pnlDecision.Visible = false;
            BindWOANoGrid(true);
            RegisterScriptUnblock();
        }

        private void BindWOANoGrid(bool isReload)
        {
            //check court selected
            int crtIntNo = (Convert.ToInt32(ddlCourt.SelectedValue));
            if (crtIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                return;
            }

            //check for court room and court date selected
            int crtRIntNo = Convert.ToInt32(ddlCourtRoom.SelectedValue);
            if (crtRIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }

            //jerry 2012-12-15 add Court Dates
            int cdIntNo = Convert.ToInt32(ddlCourtDates.SelectedValue);
            if (cdIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                lblError.Visible = true;
                return;
            }

            // get prefix, next, suffix
            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            SqlDataReader reader = room.GetCourtRoomDetails(crtRIntNo);
            while (reader.Read())
            {
                if (woaNumberFormat == "Y")
                {
                    txtPrefix.Text = Helper.GetReaderValue<string>(reader, "CrtPrefix");
                    this.lblPrefix.Text = GetLocalResourceObject("lblCourtPrefix.Text").ToString();
                    this.lblNext.Text = GetLocalResourceObject("lblCourtNext.Text").ToString();
                }
                else
                {
                    this.lblPrefix.Text = GetLocalResourceObject("lblPrefix.Text").ToString();
                    this.lblNext.Text = GetLocalResourceObject("lblNext.Text").ToString();

                    txtPrefix.Text = Helper.GetReaderValue<string>(reader, "CrtRPrefix");
                }
                txtSuffix.Text = currentYear.ToString().Substring(2, 2);
                int crtRNextWOANo = Helper.GetReaderValue<Int32>(reader, "CrtRNextWOANo");
                int crtRLastWOAYear = Helper.GetReaderValue<Int32>(reader, "CrtRLastWOAYear");
                if (crtRLastWOAYear != currentYear)
                {
                    crtRNextWOANo = 1;
                }
                //Jerry 2012-05-04 change
                //txtNext.Text = crtRNextWOANo.ToString();
            }
            reader.Dispose();

            //Jerry 2012-04-27 add, ? here is current day or SumCourtDate
            //if (!BindNextCaseNo(DateTime.Now.Year))
            // 2014-06-20, Oscar changed
            if (!BindNextCaseNo(Convert.ToDateTime(this.ddlCourtDates.SelectedItem.Text).Year))
                return;

            // 2013-04-11 add by Henry for pagination
            //if (isReload)
            //{
            //    grdEnterWOANoPager.RecordCount = 0;
            //    grdEnterWOANoPager.CurrentPageIndex = 1;
            //}

            WOADB woaDB = new WOADB(this.connectionString);
            int totalCount = 0; // 2013-04-11 add by Henry for pagination
            //jerry 2012-03-16 add CDIntNo parameter
            DataSet ds = woaDB.GetGetWOAList(this.autIntNo, nDays, "TMS", crtIntNo, crtRIntNo,
                grdEnterWOANoPager.PageSize, grdEnterWOANoPager.CurrentPageIndex, out totalCount,   // 2013-04-11 add by Henry for pagination
                cdIntNo);
            grdEnterWOANo.DataSource = ds;
            grdEnterWOANo.DataKeyNames = new string[] { "SumIntNo", "SumCourtDate", "CrtPrefix", "CrtRPrefix", "CrtRoomNo", "SchSentenceAmount", "SchTotalAddAmount", "WOANumber" };
            grdEnterWOANo.DataBind();
            grdEnterWOANoPager.RecordCount = totalCount;
            ViewState.Add("WOAForNo", ds);

            //need to store acintno for later

            if (grdEnterWOANo.Rows.Count == 0)
            {
                grdEnterWOANo.Visible = false;
                pnlWOANos.Visible = false;
                lblError.Visible = true;
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), ddlCourt.SelectedItem.Text);
            }
            else
            {
                grdEnterWOANo.Visible = true;
                pnlWOANos.Visible = true;
            }
            ds.Dispose();
        }

        /// <summary>
        /// Jerry 2012-05-04 add method to get next case no from CourtRoom_Tran
        /// </summary>
        /// <param name="year"></param>
        private bool BindNextCaseNo(int year)
        {
            // 2014-06-20, Oscar added try-catch and TransactionScope
            try
            {
                //Jake comment out the transaction, because there is only one insert script in the stored procedure, no need transaction at all
                //using (var scope = new TransactionScope())
                //{
                int crtRIntNo = Convert.ToInt32(ddlCourtRoom.SelectedValue);
                CourtRoomTranDB courtRoomTranDB = new CourtRoomTranDB(this.connectionString);
                int cRTranNumber = courtRoomTranDB.GetCaseNoAndWOANextNum(crtRIntNo, cRTranType, cRTranDescr, year, this.login);
                txtNext.Text = cRTranNumber.ToString();

                //    scope.Complete();
                //}
                return true;
            }
            catch (Exception e)
            {
                this.lblError.Text = e.Message;
                this.lblError.Visible = true;
            }
            return false;
        }

        protected void grdEnterWOANo_RowEditing(object sender, GridViewEditEventArgs e)
        {
            RegisterScriptUnblock();
            grdEnterWOANo.EditIndex = e.NewEditIndex;
            //Jerry 2014-04-08 trim the space of court room num
            //string woaNumberPrefix = txtPrefix.Text.Trim() + (woaNumberFormat == "Y" ? "/W/" : "/V/") + grdEnterWOANo.Rows[e.NewEditIndex].Cells[1].Text + "/";
            string woaNumberPrefix = txtPrefix.Text.Trim() + (woaNumberFormat == "Y" ? "/W/" : "/V/") + grdEnterWOANo.Rows[e.NewEditIndex].Cells[1].Text.Trim() + "/";
            string woaNumberSuffix = "/" + txtSuffix.Text.Trim();
            string woaNextNum = txtNext.Text.Trim().PadLeft(6, '0');
            BindWOANoGrid(true);
            TextBox txtWOANo = grdEnterWOANo.Rows[e.NewEditIndex].FindControl("txtWOANumber") as TextBox;
            txtWOANo.Text = woaNumberPrefix + woaNextNum + woaNumberSuffix;
            txtWOANo.Enabled = false;
        }

        protected void grdEnterWOANo_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            RegisterScriptUnblock();
            TextBox txtWOANo = grdEnterWOANo.Rows[e.RowIndex].FindControl("txtWOANumber") as TextBox;
            if (txtWOANo.Text != null)
            {
                if (txtWOANo.Text.Trim().Equals(""))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    lblError.Visible = true;
                    return;
                }

                int sumIntNo = int.Parse(grdEnterWOANo.DataKeys[e.RowIndex].Values["SumIntNo"].ToString());

                //get woaNumber
                string woaNumber = txtWOANo.Text.Trim();

                //get nextWOANumber
                int nextWOANumber = 0;
                string[] sNextWOANumber = woaNumber.Split('/');
                //look for the first numeric section in S/V/A/000007/11
                //Jerry 2012-05-15 change
                //for (int n = 0; n < sNextWOANumber.Length; n++)
                //{
                //    if (Regex.IsMatch(sNextWOANumber[n], numericOnly))
                //    {
                //        nextWOANumber = Convert.ToInt32(sNextWOANumber[n]) + 1;
                //        break;
                //    }
                //}
                nextWOANumber = Convert.ToInt32(txtNext.Text.Trim()) + 1;

                //get current PrintFile from the row
                DateTime sumCourtDate = Convert.ToDateTime(grdEnterWOANo.DataKeys[e.RowIndex].Values["SumCourtDate"].ToString());
                string crtPrefix = grdEnterWOANo.DataKeys[e.RowIndex].Values["CrtPrefix"].ToString();
                //jerry 2012-03-05 change it's name according to print service document
                //string printFile = string.Format("WOA-{0}-{1}-CourtDate-{2}", ViewState["PrintFileDate"].ToString(), crtPrefix.Trim(), sumCourtDate.ToString(DATE_FORMAT));
                crtPrefix = Helper.ReplaceSpecialCharacters(crtPrefix);//2014-01-16 Heidi added for use "/" not create file(5101)
                string printFile = string.Format("WOA-{0}-{1}-{2}-CourtDate-{3}", this.autCode, ViewState["PrintFileDate"].ToString(), crtPrefix.Trim(), sumCourtDate.ToString(DATE_FORMAT));

                //get schSentenceAmount, schTotalAddAmount
                decimal schSentenceAmount = Convert.ToDecimal(grdEnterWOANo.DataKeys[e.RowIndex].Values["SchSentenceAmount"].ToString());
                decimal schTotalAddAmount = Convert.ToDecimal(grdEnterWOANo.DataKeys[e.RowIndex].Values["SchTotalAddAmount"].ToString());

                //generate WOA
                string errMessage = "";
                string error = string.Empty;
                WOADB db = new WOADB(this.connectionString);

                //jerry 2012-01-04 push queue
                QueueItem item = new QueueItem();
                QueueItemProcessor queProcessor = new QueueItemProcessor();
                using (TransactionScope scope = new TransactionScope())
                {

                    //2012-11-22 linda add for check new rule 5065
                    AuthorityRulesDetails arDetails5065 = new AuthorityRulesDetails();
                    arDetails5065.AutIntNo = this.autIntNo;
                    arDetails5065.ARCode = "5065";
                    arDetails5065.LastUser = this.login;
                    DefaultAuthRules ar5065 = new DefaultAuthRules(arDetails5065, this.connectionString);
                    bool checkResult = ar5065.SetDefaultAuthRule().Value.Equals("Y");

                    int woaIntNo = db.AddWOAByWOANumber(sumIntNo, this.autIntNo, woaNumber, nextWOANumber, printFile, schSentenceAmount, schTotalAddAmount, this.login, issueWOANotice, ref errMessage, checkResult ? "Y" : "N");
                    if (woaIntNo > 0)
                    {
                        grdEnterWOANo.EditIndex = -1;
                        e.Cancel = true;
                        //Jerry 2014-04-08 add
                        if (grdEnterWOANo.Rows.Count == 1)
                        {
                            grdEnterWOANoPager.RecordCount = 0;
                            grdEnterWOANoPager.CurrentPageIndex = 1;
                        }
                        else
                        {
                            BindWOANoGrid(true);
                        }
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text4");

                        //jerry 2012-01-04 add
                        item = new QueueItem();
                        item.Body = woaIntNo.ToString();
                        item.Group = this.autCode.Trim();
                        item.QueueType = ServiceQueueTypeList.CancelExpiredWOA;
                        //Jake 2015-02-28 modified
                        //item.ActDate = DateTime.Now.AddDays(noDaysForWOAExpiry);
                        item.ActDate = sumCourtDate.AddDays(noDaysForWOAExpiry);
                        queProcessor.Send(item);

                        // jerry 2012-03-02 push Print WOA service
                        // Save PrintFileName
                        SIL.AARTO.DAL.Entities.PrintFileName printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().GetByPrintFileName(printFile);
                        if (printFileName == null)
                        {
                            printFileName = new SIL.AARTO.DAL.Entities.PrintFileName();
                            printFileName.PrintFileName = printFile;
                            printFileName.AutIntNo = this.autIntNo;
                            printFileName.DateAdded = DateTime.Now;
                            printFileName.LastUser = this.login;
                            printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().Save(printFileName);
                        }

                        SIL.AARTO.DAL.Entities.PrintFileNameWoa printFileNameWOA = new SIL.AARTO.DAL.Services.PrintFileNameWoaService().GetByWoaIntNoPfnIntNo(woaIntNo, printFileName.PfnIntNo);
                        if (printFileNameWOA == null)
                        {
                            printFileNameWOA = new SIL.AARTO.DAL.Entities.PrintFileNameWoa();
                            printFileNameWOA.PfnIntNo = printFileName.PfnIntNo;
                            printFileNameWOA.WoaIntNo = woaIntNo;
                            printFileNameWOA.LastUser = this.login;
                            new SIL.AARTO.DAL.Services.PrintFileNameWoaService().Save(printFileNameWOA);
                        }

                        //2014-01-10 Heidi added for WOA Direct Printing (5101)
                        AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                        arDetails.AutIntNo = this.autIntNo;
                        arDetails.ARCode = "6209";
                        arDetails.LastUser = this.login;
                        DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
                        bool isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

                        if (!isIBMPrinter)
                        {
                            if (!this.pfnIntNoList.Contains(printFileName.PfnIntNo))
                            {
                                this.pfnIntNoList.Add(printFileName.PfnIntNo);

                                item = new QueueItem();
                                item.Body = printFileName.PfnIntNo.ToString();
                                item.Group = this.autCode.Trim();
                                item.QueueType = ServiceQueueTypeList.PrintWOA;
                                item.LastUser = this.login;

                                queProcessor.Send(item);
                            }
                        }
                        else
                        {
                            if (!this.pfnIntNoList.Contains(printFileName.PfnIntNo))
                            {
                                this.pfnIntNoList.Add(printFileName.PfnIntNo);

                                item = new QueueItem();
                                item.Body = printFileName.PfnIntNo.ToString();
                                item.Group = this.autCode.Trim() + "|" + (int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.WOADirectPrinting;
                                item.QueueType = ServiceQueueTypeList.PrintToFile;
                                item.LastUser = this.login;

                                queProcessor.Send(item);
                            }
                        }

                        //item = new QueueItem();
                        //item.Body = printFileName.PfnIntNo.ToString();
                        //item.Group = this.autCode.Trim(); ;
                        //item.QueueType = ServiceQueueTypeList.PrintWOA;
                        //queProcessor.Send(item); 

                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.WOAManualAllocationOfWOANumbers, PunchAction.Add);

                    }
                    else
                    {
                        error = GetError(woaIntNo, errMessage);
                        lblError.Text = error;
                        lblError.Visible = true;
                    }

                    scope.Complete();

                }
            }
        }

        protected void grdEnterWOANo_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdEnterWOANo.EditIndex = -1;
            e.Cancel = true;
            BindWOANoGrid(true);
        }



        protected void grdEnterWOANo_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdEnterWOANo.EditIndex = -1;
            //if (ViewState["WOAForNo"] == null)
            //{
            //    BindWOANoGrid(true);
            //}
            DataSet wOAForNoDS = (DataSet)ViewState["WOAForNo"];

            int countWOA = 0;
            string error = string.Empty;
            //set up a loop
            bool found = false;
            bool foundStart = false;

            //find the current row
            int sumIntNo = (int)this.grdEnterWOANo.SelectedDataKey["SumIntNo"];

            foreach (DataRow row in wOAForNoDS.Tables[0].Rows)
            {

                //get current PrintFile from the row
                int currentSumIntNo = Int32.Parse(row["SumIntNo"].ToString());

                if (foundStart == false)
                {
                    if (Int32.Parse(row["SumIntNo"].ToString()) == sumIntNo)
                    {
                        //found the row selected by the user to start the autofill with
                        found = true;
                        foundStart = true;
                    }
                }
                if (found)
                {
                    if (!String.IsNullOrEmpty(row["WoaNumber"].ToString()))
                    {
                        continue;
                    }

                    DateTime sumCourtDate = Convert.ToDateTime(row["SumCourtDate"].ToString());
                    string crtPrefix = row["CrtPrefix"].ToString();
                    if (!String.IsNullOrEmpty(crtPrefix)) crtPrefix = crtPrefix.Trim();
                    //jerry 2012-03-05 change it's name according to print service document
                    //string printFile = string.Format("WOA-{0}-{1}-CourtDate-{2}", ViewState["PrintFileDate"].ToString(), crtPrefix.Trim(), sumCourtDate.ToString(DATE_FORMAT));
                    string _crtPrefix = Helper.ReplaceSpecialCharacters(crtPrefix);//2014-01-16 Heidi added for use "/" not create file(5101)
                    string printFile = string.Format("WOA-{0}-{1}-{2}-CourtDate-{3}", this.autCode, ViewState["PrintFileDate"].ToString(), _crtPrefix.Trim(), sumCourtDate.ToString(DATE_FORMAT));

                    //get wOANumberPrefix, wOANumberSuffix
                    string crtRPrefix = row["CrtRPrefix"].ToString();
                    //Jerry 2014-04-08 trim the space of court room num
                    //string crtRoomNo = row["CrtRoomNo"].ToString();
                    string crtRoomNo = row["CrtRoomNo"].ToString().Trim();
                    string woaNumberPrefix = (woaNumberFormat == "Y" ? crtPrefix + "/W/" : crtRPrefix + "/V/") + crtRoomNo + "/";
                    //string woaNumberSuffix = "/" + currentYear.ToString().Substring(2, 2);
                    // 2014-06-20, Oscar changed
                    var woaNumberSuffix = "/" + sumCourtDate.Year.ToString().Substring(2, 2);

                    //get schSentenceAmount, schTotalAddAmount
                    decimal schSentenceAmount = Convert.ToDecimal(row["SchSentenceAmount"].ToString());
                    decimal schTotalAddAmount = Convert.ToDecimal(row["SchTotalAddAmount"].ToString());

                    //generate WOA
                    string errMessage = "";
                    WOADB db = new WOADB(this.connectionString);

                    //jerry 2012-03-13 change, push print file after checked it
                    QueueItem item = new QueueItem();
                    QueueItemProcessor queProcessor = new QueueItemProcessor();
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //2012-11-22 linda add for check new rule 5065
                        AuthorityRulesDetails arDetails5065 = new AuthorityRulesDetails();
                        arDetails5065.AutIntNo = this.autIntNo;
                        arDetails5065.ARCode = "5065";
                        arDetails5065.LastUser = this.login;
                        DefaultAuthRules ar5065 = new DefaultAuthRules(arDetails5065, this.connectionString);
                        bool checkResult = ar5065.SetDefaultAuthRule().Value.Equals("Y");

                        int woaIntNo = db.AddWOA(currentSumIntNo, this.autIntNo, woaNumberPrefix, woaNumberSuffix, printFile, schSentenceAmount, schTotalAddAmount, this.login, issueWOANotice, ref errMessage, checkResult ? "Y" : "N");
                        if (woaIntNo > 0)
                        {
                            countWOA++;

                            //jerry 2012-01-04 add
                            item = new QueueItem();
                            item.Body = woaIntNo.ToString();
                            item.Group = this.autCode.Trim();
                            item.QueueType = ServiceQueueTypeList.CancelExpiredWOA;
                            //Jake 2015-02-28 modified
                            //item.ActDate = DateTime.Now.AddDays(noDaysForWOAExpiry);
                            item.ActDate = sumCourtDate.AddDays(noDaysForWOAExpiry);
                            queProcessor.Send(item);

                            // jerry 2012-03-14 add PrintFileName
                            // Save PrintFileName
                            SIL.AARTO.DAL.Entities.PrintFileName printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().GetByPrintFileName(printFile);
                            if (printFileName == null)
                            {
                                printFileName = new SIL.AARTO.DAL.Entities.PrintFileName();
                                printFileName.PrintFileName = printFile;
                                printFileName.AutIntNo = this.autIntNo;
                                printFileName.DateAdded = DateTime.Now;
                                printFileName.LastUser = this.login;
                                printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().Save(printFileName);
                            }
                            SIL.AARTO.DAL.Entities.PrintFileNameWoa printFileNameWOA = new SIL.AARTO.DAL.Services.PrintFileNameWoaService().GetByWoaIntNoPfnIntNo(woaIntNo, printFileName.PfnIntNo);
                            if (printFileNameWOA == null)
                            {
                                printFileNameWOA = new SIL.AARTO.DAL.Entities.PrintFileNameWoa();
                                printFileNameWOA.PfnIntNo = printFileName.PfnIntNo;
                                printFileNameWOA.WoaIntNo = woaIntNo;
                                printFileNameWOA.LastUser = this.login;
                                new SIL.AARTO.DAL.Services.PrintFileNameWoaService().Save(printFileNameWOA);
                            }
                        }
                        else
                        {
                            error = GetError(woaIntNo, errMessage);
                            break;//break current foreach
                        }

                        scope.Complete();
                    }
                }

            }
            if (string.IsNullOrEmpty(error))
            {
                //Jerry 2014-04-08 change it
                BindWOANoGrid(true);
                //grdEnterWOANoPager.RecordCount = 0;
                //grdEnterWOANoPager.CurrentPageIndex = 1;
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), countWOA);
            }
            else
            {
                lblError.Text = error;
            }
            lblError.Visible = true;
            RegisterScriptUnblock();
        }

        private string GetError(int woaIntNo, string returnError)
        {
            string error = string.Empty;
            switch (woaIntNo)
            {
                case 0:
                    error = returnError;
                    break;
                case -1:
                    error = (string)GetLocalResourceObject("error");
                    break;
                case -2:
                    error = (string)GetLocalResourceObject("error1");
                    break;
                case -3:
                    error = (string)GetLocalResourceObject("error2");
                    break;
                case -4:
                    error = (string)GetLocalResourceObject("error3");
                    break;
                case -5:
                    error = (string)GetLocalResourceObject("error4");
                    break;
                case -6:
                    error = (string)GetLocalResourceObject("error5");
                    break;
                case -7:
                    error = (string)GetLocalResourceObject("error6");
                    break;
                default:
                    error = (string)GetLocalResourceObject("error7");
                    break;
            }
            return error;
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            pnlTitle.Visible = false;
            pnlDetails.Visible = false;
        }

        protected void ddlCourtDates_SelectedIndexChanged(object sender, EventArgs e)
        {

            RegisterScriptUnblock();
            string courtRuleValue = CheckCourtRule(Convert.ToInt32(ddlCourt.SelectedValue));
            if (courtRuleValue == "Y")
            {
                pnlDecision.Visible = true;
                pnlWOANos.Visible = false;
            }
            else
            {
                grdEnterWOANo.EditIndex = -1;
                //Jerry 2014-04-08 change it, there will be executed twice when the param value is true.
                BindWOANoGrid(true);
                //BindWOANoGrid(false);
            }
        }

        //2013-04-11 add by Henry for pagination
        protected void grdEnterWOANoPager_PageChanged(object sender, EventArgs e)
        {
            BindWOANoGrid(false);
        }


        protected void grdEnterWOANo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType != DataControlRowType.DataRow) return;
            //foreach (Control c in e.Row.Cells[5].Controls)
            //{
            //    if (c is LinkButton)
            //    {
            //        LinkButton autoFillButton = c as LinkButton;
            //        if (autoFillButton.Text == (string)GetLocalResourceObject("grdEnterWOANo.SelectText"))
            //        {
            //            autoFillButton.OnClientClick = "LoadingBlocker('" + (string)GetLocalResourceObject("msg_Processing") + "');";
            //        }
            //    }
            //}

            SetLinkStatus(e);
        }

        void RegisterScriptUnblock()
        {
            //ClientScript.RegisterClientScriptBlock(this.GetType(), "message", @"$(document).ready(function(){$.unblockUI();})", true);
            ScriptManager.RegisterStartupScript(this.UpdatePanel1, GetType(), "UpdatePanel1", @"$.unblockUI();", true);
        }

        protected void grdEnterWOANo_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType != DataControlRowType.DataRow) return;

            var ctrlUpdate = e.Row.Cells[4].Controls;
            if (ctrlUpdate.Count == 3)
            {
                var lnkUpdate = ctrlUpdate[0] as LinkButton;
                if (lnkUpdate != null
                    && lnkUpdate.CommandName.Equals("Update", StringComparison.OrdinalIgnoreCase))
                {
                    lnkUpdate.OnClientClick = "LoadingBlocker('" + (string)GetLocalResourceObject("msg_Processing") + "');";
                }
            }


        }

        void SetLinkStatus(GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;

            Label lblWoanumber = e.Row.FindControl("lblWOANumber") as Label;
            LinkButton linkAutoFill = null;
            LinkButton linkEdit = null;

            if (e.Row.Cells[5].Controls.Count == 1)
            {
                linkAutoFill = e.Row.Cells[5].Controls[0] as LinkButton;

            }
            if (e.Row.Cells[4].Controls.Count == 1)
            {
                linkEdit = e.Row.Cells[4].Controls[0] as LinkButton;

            }

            if (lblWoanumber != null && !String.IsNullOrEmpty(lblWoanumber.Text))
            {
                if (linkAutoFill != null)
                    linkAutoFill.Enabled = false;
                if (linkEdit != null)
                    linkEdit.Enabled = false;
            }

            if (linkAutoFill != null && linkAutoFill.Enabled)
            {
                linkAutoFill.OnClientClick = "LoadingBlocker('" + (string)GetLocalResourceObject("msg_Processing") + "');";
            }
            if (linkEdit != null && linkEdit.Enabled)
            {
                linkEdit.OnClientClick = "LoadingBlocker('" + (string)GetLocalResourceObject("msg_Processing") + "');";
            }
        }
    }
}