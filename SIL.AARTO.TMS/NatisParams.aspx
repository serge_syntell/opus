<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.NatisParams" Codebehind="NatisParams.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" Text="<%$Resources:lblOptions.Text %>" CssClass="ProductListHead"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                        </p>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnll" runat="Server" Width="100%">
                        <table>
                            <tr>
                                <td style="width: 157; height: 2">
                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelectAuth.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" Width="197px"
                                        OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="10" height="2">
                                    &nbsp;</td>
                                <td width="157" height="2">
                                </td>
                                <td width="20" height="2">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlGrid" runat="Server" Width="100%">
                        <asp:GridView ID="grdHeader" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="Normal" OnSelectedIndexChanged="grdHeader_SelectedIndexChanged" ShowFooter="True"
                            OnPageIndexChanging="grdHeader_PageIndexChanging" OnRowCreated="grdHeader_RowCreated"
                            OnRowDeleting="grdHeader_RowDeleting" 
                            onselectedindexchanging="grdHeader_SelectedIndexChanging">
                            <FooterStyle CssClass="CartListHead" />
                            <Columns>
                                <asp:BoundField DataField="NPIntNo" HeaderText="<%$Resources:grdHeader.NPIntNo.HeaderText %>" />
                                <asp:BoundField DataField="NPTerminalID" HeaderText="<%$Resources:grdHeader.NPTerminalID.HeaderText %>" />
                                <asp:BoundField DataField="NPSendNatisFolder" HeaderText="<%$Resources:grdHeader.NPSendNatisFolder.HeaderText %>">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NPSendNatisEmail" HeaderText="<%$Resources:grdHeader.NPSendNatisEmail.HeaderText %>">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NPGetNatisFolder" HeaderText="<%$Resources:grdHeader.NPGetNatisFolder.HeaderText %>">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$Resources:grdHeader.TemplateField1.HeaderText %>">
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkBox" runat="server" Text='<%# Bind("NPChecked") %>'></asp:CheckBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblChecked" runat="server" Text='<%# Bind("NPChecked") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdHeader.TemplateField2.HeaderText %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NPStartRunTime") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartRunDate" runat="server" Text='<%# Bind("NPStartRunTime") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NPInterval" HeaderText="<%$Resources:grdHeader.TemplateField3.HeaderText %>">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NPNumberOfRuns" HeaderText="<%$Resources:grdHeader.TemplateField4.HeaderText %>">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <%--<asp:CommandField HeaderText="Edit" ShowEditButton="True">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>--%>
                                <asp:CommandField ShowSelectButton="true" HeaderText="<%$Resources:grdHeader.CommandEdit.HeaderText %>" SelectText="<%$Resources:grdHeader.CommandEdit.HeaderText %>" >
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>
                                <asp:CommandField HeaderText="<%$Resources:grdHeader.CommandDelete.HeaderText %>" ShowDeleteButton="True" DeleteText="<%$Resources:grdHeader.CommandDelete.HeaderText %>">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>
                            </Columns>
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                        </asp:GridView>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetails" Visible="false" runat="server" Width="100%">
                        <table>
                            <tr>
                                <td width="250" height="2">
                                    &nbsp;
                                </td>
                                <td width="157" height="2">
                                    &nbsp;
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="250" height="2">
                                    <asp:Label ID="lblAction" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAction.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    &nbsp;
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="250" height="2">
                                    &nbsp;
                                </td>
                                <td width="157" height="2">
                                    &nbsp;
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="250" height="2">
                                    <asp:Label ID="Label7" runat="server" CssClass="Normal" Text="<%$Resources:lblSetCurrent.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:CheckBox ID="chkBox" runat="server" Width="180px" CssClass="Normal"></asp:CheckBox>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="250" height="2">
                                    <asp:Label ID="Label5" runat="server" CssClass="Normal" Text="<%$Resources:lblNatisTerinalID.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:HiddenField ID="txtNPIntNo" runat="server"></asp:HiddenField>
                                    <asp:TextBox ID="txtNatisTerminal" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="250" height="2">
                                    <asp:Label ID="Label2" runat="server" CssClass="Normal" Text="<%$Resources:lblSendNatis.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtSendNatisFolder" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="250" style="height: 2px">
                                    <asp:Label ID="Label12" runat="server" CssClass="Normal" Text="<%$Resources:lblSendNatisEmail.Text %>"></asp:Label></td>
                                <td width="157" style="height: 2px">
                                    <asp:TextBox ID="txtSentNatisEmail" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="157" style="height: 2px">
                                    <asp:Label ID="Label17" runat="server" CssClass="Normal" Text="<%$Resources:lblGetNatisFolder.Text %>"></asp:Label>
                                </td>
                                <td id="tdSel" width="157" onmouseover="" style="height: 2px">
                                    <asp:TextBox ID="txtGetNatisFolder" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                                <td style="height: 2px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td width="250" height="2">
                                    <asp:Label ID="lblFirstRun" runat="server" CssClass="Normal" Text="<%$Resources:lblFirstRun.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtFirstRun" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                                <td height="2">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td width="250" height="2">
                                    <asp:Label ID="Label3" runat="server" CssClass="Normal" Text="<%$Resources:lblInterval.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtInterval" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                                <td height="2">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td width="250" height="2">
                                    <asp:Label ID="Label6" runat="server" CssClass="Normal" Text="<%$Resources:lblNumberOfRun.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtNumberRuns" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                                <td height="2">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td width="10" height="2">
                                    &nbsp;</td>
                                <td width="157" height="2">
                                    <asp:Button ID="btnAction" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>" OnClick="btnAction_Click">
                                    </asp:Button>&nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="NormalButton" Text="<%$Resources:btnCancel.Text %>" OnClick="btnCancel_Click">
                                    </asp:Button></td>
                                <td width="20" height="2">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
