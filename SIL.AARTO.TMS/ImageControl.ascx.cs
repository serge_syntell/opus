﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Stalberg.TMS;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;

public partial class ImageControl : System.Web.UI.UserControl
{
    private const int MAIN_IMAGE_MAX_WIDTH = 657;
    private const int MAIN_IMAGE_MAX_HEIGHT = 420;
    private const int PREVIEW_IMAGE_MAX_WIDTH = 160;
    private const int PREVIEW_IMAGE_MAX_HEIGHT = 100;

    private const string DEFAULT_BRIGHTNESS = "0.0";
    private const string DEFAULT_CONTRAST = "1.0";

    private int filmIntNo = 0;
    private int frameIntNo = 0;
    private int scImIntNo = 0;
    private string filmNo = "";
    private string imgType = "A";
    private int cvPhase = 0;
    protected string _login;

    private string connectionString;
    private string imageFolder = "";

    /// <summary>
    /// Gets or sets the film int no.
    /// </summary>
    /// <value>The film int no.</value>
    public int FilmIntNo
    {
        get { return this.filmIntNo; }
        set { this.filmIntNo = value; }
    }
    /// <summary>
    /// Gets or sets the frame int no.
    /// </summary>
    /// <value>The frame int no.</value>
    public int FrameIntNo
    {
        get { return this.frameIntNo; }
        set { this.frameIntNo = value; }
    }
    /// <summary>
    /// Gets or sets the scan image int no.
    /// </summary>
    /// <value>The scan image int no.</value>
    public int ScanImageIntNo
    {
        get { return this.scImIntNo; }
        set { this.scImIntNo = value; }
    }
    /// <summary>
    /// Gets or sets the film int no.
    /// </summary>
    /// <value>The film number</value>
    public string FilmNo
    {
        get { return this.filmNo; }
        set { this.filmNo = value; }
    }
    /// <summary>
    /// Gets or sets the type of the image.
    /// </summary>
    /// <value>The type of the image.</value>
    public string ImageType
    {
        get { return this.imgType; }
        set { this.imgType = value; }
    }
    /// <summary>
    /// Gets or sets the phase.
    /// </summary>
    /// <value>The phase.</value>
    public int Phase
    {
        get { return this.cvPhase; }
        set { this.cvPhase = value; }
    }

    private List<string> ImageURLs;
    private string ImageList = "";
    private bool ShowCrossHair = false;
    private bool OriginalSize = false;
    private int XValue;
    private int YValue;
    private string CrossStyle;


    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void GetLoginName()
    {
        if (_login == null)
        {
            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            //int 
            this._login = userDetails.UserLoginName;
        }
    }

    public void Initialise()
    {
        GetLoginName();

        Session["ImageA"] = new Dictionary<string, byte[]>();
        Session["ImageB"] = new Dictionary<string, byte[]>();
        Session["ImageR"] = new Dictionary<string, byte[]>();
        Session["ImageD"] = new Dictionary<string, byte[]>();

        if (Session["Brightness"] != null)
        {
            //update by Rachel 20140821 for 5337
            //this.brightness.Text = string.Format("{0:F1}", Convert.ToDecimal(Session["Brightness"].ToString()));
            this.brightness.Text = string.Format(CultureInfo.InvariantCulture,"{0:F1}", Session["Brightness"]);
            //end update by Rachel 201040821 for 5337
        }
        else
        {
            this.brightness.Text = DEFAULT_BRIGHTNESS;
        }
        if (Session["Contrast"] != null)
        {
            //update by Rachel 20140821 for 5337
            //this.contrast.Text = string.Format("{0:F1}", Convert.ToDecimal(Session["Contrast"].ToString()));
            this.contrast.Text = string.Format(CultureInfo.InvariantCulture, "{0:F1}",Session["Contrast"]);
            //end update by Rachel 201040821 for 5337
        }
        else
        {
            this.contrast.Text = DEFAULT_CONTRAST;
        }

        LoadFilmImages();

        #region Corss Hair
        ScanImageDB scan = new ScanImageDB(this.connectionString);
        ScanImageDetails scanDetails = scan.GetScanImageDetails(scImIntNo);
        XValue = scanDetails.XValue;
        YValue = scanDetails.YValue;

        if (Session["DisplayCrossHair"] == null)
        {
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = Convert.ToInt32(Session["autIntNo"]);
            arDetails.ARCode = "0650";
            arDetails.LastUser = this._login;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            ar.SetDefaultAuthRule();
            if (arDetails.ARString == "Y")
            {
                Session["DisplayCrossHair"] = true;
            }
            else
            {
                Session["DisplayCrossHair"] = false;
            }
        }

        if (Session["ShowCrossHairs"] == null)
        {
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = Convert.ToInt32(Session["autIntNo"]);
            arDetails.ARCode = "3100";
            arDetails.ARCode = this._login;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            ar = new DefaultAuthRules(arDetails, this.connectionString);
            ar.SetDefaultAuthRule();

            if (arDetails.ARString == "Y")
            {
                Session["ShowCrossHairs"] = true;
            }
            else
            {
                Session["ShowCrossHairs"] = false;
            }
        }
        ShowCrossHair = ShowCrossHairs();
        if (ShowCrossHair && (Convert.ToBoolean(Session["DisplayCrossHair"]) || Convert.ToBoolean(Session["ShowCrossHairs"].ToString())))
        {
            ShowCrossHair = true;
        }

        if (Convert.ToBoolean(Session["DisplayCrossHair"]))
            OriginalSize = true;

        if (!ShowCrossHair)
            OriginalSize = false;
        #endregion
    }
    protected override void OnInit(EventArgs e)
    {
        this.connectionString = Application["constr"].ToString();
        //this.useJp2 = (bool)Application["UseJP2Conversion"];
        GetLoginName();

        //if (Session["CheckFileSystemForImages"] != null)
        //{
        //    checkFileSystemForImages = (bool)Session["CheckFileSystemForImages"];
        //}

        if (Application["imageFolder"] != null)
        {
            imageFolder = Application["imageFolder"].ToString();
        }
        base.OnInit(e);
    }

    #region Load images
    /// <summary>
    /// Load images based on film or frame, then bind to dropdownlist
    /// </summary>
    private void LoadFilmImages()
    {
        ScanImageDB scan = new ScanImageDB(this.connectionString);

        DataSet scanDS;

        switch (cvPhase)
        {
            case 0:
                scanDS = scan.GetScanImageListDS(filmIntNo, 0, "A");
                break;
            default:
                scanDS = scan.GetScanImageListDS(0, frameIntNo, "%");
                break;
        }

        ImageList = "";
        ImageURLs = new List<string>();
        foreach (DataRow dr in scanDS.Tables[0].Rows)
        {
            decimal contrast;
            decimal brightness;
            int scImPrintVal = int.Parse(dr["scImPrintVal"].ToString());

            ImageList += dr["ImageDetails"].ToString() + "&&" + dr["ScImIntNo"].ToString() + "@@@";
            
            //GetImage("", Convert.ToInt32(dr["ScImIntNo"]), dr["ImageDetails"].ToString(), 0, 0);
            if (Decimal.TryParse(dr["Contrast"].ToString(), out contrast) && Decimal.TryParse(dr["Brightness"].ToString(), out brightness))
                GetImage("", Convert.ToInt32(dr["ScImIntNo"]), dr["ImageDetails"].ToString(), 0, 0, contrast, brightness * 10.0m, scImPrintVal);
            else
                GetImage("", Convert.ToInt32(dr["ScImIntNo"]), dr["ImageDetails"].ToString(), 0, 0, 99, 99, 0);
        }
    }

    /// <summary>
    /// WebImageViewer get the image from the file sytem if the image is stored on the hard disk or from the database
    /// If it is loaded from database and it is allowed to save back to hard disk, then save it in the corrsponding cached folder.
    /// If the image stored in DB is empty, load an "NoImage.jpg" to the WebImageViewer
    /// </summary>
    /// <param name="image"></param>
    /// <param name="scanImage"></param>
    /// <param name="imageDetails"></param>
    //protected void GetImage(string ImageName, int scanImage, string imageDetails, int width, int height)
    protected void GetImage(string ImageName, int scanImage, string imageDetails, int width, int height, decimal contrast, decimal brightness, int scImPrintVal)
    {
        string jpegName = "";

        if (!imageDetails.Equals(""))
            jpegName = GetJpegName(imageDetails);

        if (imageDetails.ToLower().Contains("main "))
        {
            imgType = "A";
            scImIntNo = scanImage;
            Session["scImIntNo"] = scImIntNo;
        }
        else if (imageDetails.ToLower().Contains("2nd "))
            imgType = "B";
        else if (imageDetails.ToLower().Contains("registration "))
            imgType = "R";
        else if (imageDetails.ToLower().Contains("driver "))
            imgType = "D";

        //For ASD
        if (imageDetails.ToLower().Contains("main ") && imageDetails.ToLower().Contains("_001.j"))
        {
            imgType = "A";
            scImIntNo = scanImage;
        }
        else if (imageDetails.ToLower().Contains("main ") && imageDetails.ToLower().Contains("_002.j"))
        {
            imgType = "B";
            scImIntNo = scanImage;
        }

        string strUrl = "";

        strUrl = "Image.aspx?FilmNo=" + HttpUtility.UrlEncode(filmNo)
            + "&ScImIntNo=" + HttpUtility.UrlEncode(Convert.ToString(scanImage))
            + "&JpegName=" + HttpUtility.UrlEncode(jpegName)
            + "&ImgType=" + HttpUtility.UrlEncode(imgType);

        if (contrast != 99 && brightness != 99)
        {
            strUrl += "&SavedContrast=" + HttpUtility.UrlEncode(contrast.ToString()) 
                + "&SavedBrightness=" + HttpUtility.UrlEncode(brightness.ToString())
                + "&ScImPrintVal=" + HttpUtility.UrlEncode(scImPrintVal.ToString());
        }
        
        ImageURLs.Add("'" + strUrl + "'");
    }
    #endregion

    public bool ShowCrossHairs()
    {
        switch (cvPhase)
        {
            case 0:
                return false;
            default:
                if ((Convert.ToBoolean(Session["DisplayCrossHair"]) || Convert.ToBoolean(Session["ShowCrossHairs"].ToString()))
                    && XValue > 0 && YValue > 0)
                {
                    if (Session["CrossHairStyle"] == null)
                        Session["CrossHairStyle"] = 0;

                    CrossStyle = Session["CrossHairStyle"].ToString();
                    return true;
                }
                else
                    return false;
        }
    }

    /// <summary>
    /// Get the image name from the passed imageDetails variable
    /// </summary>
    /// <param name="imageDetails"></param>
    /// <returns>image name</returns>
    protected string GetJpegName(string imageDetails)
    {
        if (imageDetails.Length > 0)
        {
            string jpegDetails = imageDetails;
            int posStart = jpegDetails.IndexOf('(') + 1;
            int posEnd = jpegDetails.IndexOf(')') - 1;

            string jpegName = "";

            if (posStart > 1 && posEnd > 0)
                jpegName = jpegDetails.Substring(posStart, (posEnd - posStart) + 1);

            return jpegName;
        }
        else
            return string.Empty;
    }

    public string GetParameters()
    {
        string strParameters = "new Array(";
        strParameters += "new Array(" + string.Join(",", ImageURLs.ToArray()) + "),";
        strParameters += AddParamete(ImageList);
        strParameters += AddParamete(OriginalSize.ToString());
        strParameters += AddParamete(ShowCrossHair.ToString());
        strParameters += AddParamete(XValue.ToString());
        strParameters += AddParamete(YValue.ToString());
        strParameters += AddParamete(CrossStyle);
        //strParameters += AddParamete(this.brightness.Text);
        //strParameters += AddParamete(this.contrast.Text);
        if (Session["Brightness"]!= null)
            strParameters += AddParamete(Session["Brightness"].ToString());
        else
            strParameters += AddParamete(DEFAULT_BRIGHTNESS);

        if (Session["Contrast"] != null)
            strParameters += AddParamete(Session["Contrast"].ToString());
        else
            strParameters += AddParamete(DEFAULT_CONTRAST);
        strParameters += AddParamete(this.ShowPreView.ToString(), true);

        strParameters += ")";
        return strParameters;
    }

    private string AddParamete(string str)
    {
        return "'" + str + "',";
    }

    private string AddParamete(string str, bool bEnd)
    {
        return "'" + str + "'";
    }

    public int ShowPreView = 1;

    public int SaveImageSetting()
    {
        ImageProcesses img = new ImageProcesses(this.connectionString);

        Session["Contrast"] = Convert.ToDecimal(this.contrast.Text);
        Session["Brightness"] = Convert.ToDecimal(this.brightness.Text);

        return img.UpdateImageSetting(Convert.ToInt32(Session["scImIntNo"]), Convert.ToDecimal(this.contrast.Text), Convert.ToDecimal(this.brightness.Text) * 0.1m, _login, 0);
    }

    public void SaveImageSettingsToSession()
    {
        Session["SaveImageSettingsForFilm"] = true;
    }

    public void ClearSettings()
    {
        //Session["Contrast"] = null;
        //Session["Brightness"] = null;
        Session["SaveImageSettingsForFilm"] = null;
    }
}
