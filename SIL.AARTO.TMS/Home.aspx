<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Home" Codebehind="Home.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        Opus-AARTO
    </title>
    <link href="<%= styleSheet %>" rel="stylesheet" type="text/css" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <link rel="shortcut icon"  href="Images/favicon.ico" />
</head>
<body style="margin: 0px 0px 0px 0px; background-color: #6977b0;">
    <center>
        <form id="form1" runat="server">
            <table style="text-align: center;">
                <tr>
                    <td valign="middle">
                        <img visible="false" src="images/opus.jpg" id="OdysseyMain" style="border: 0px; cursor: pointer;"
                            alt="Enter" onclick="Login();" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lnkLastUpdated" runat="server" CssClass="NormalBold" ForeColor="White">
                                            <%= lastUpdated %></asp:Label>
                            </td>
                            <td align="right">
                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" ForeColor="White" Text="<%$Resources:lblResolution.Text %>"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </form>
    </center>

    <script language="javascript" type="text/javascript">
   
    function Login()
    {
        document.forms[0].submit();
    }
    
    </script>

</body>
</html>
