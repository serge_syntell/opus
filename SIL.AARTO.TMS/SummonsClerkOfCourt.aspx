<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SummonsClerkOfCourt" Codebehind="SummonsClerkOfCourt.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
    <body style="margin:0 0 0 0;" background="<%=backgroundImage %>" >
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height:5%;">
            <tr>
                <td class="HomeHead" align="center" colspan="2" valign="middle" style="width:100%;">
                    <hdr1:Header ID="Header1" runat="server" EnableTheming="true"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" style="height:85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167" alt="images/1x1.gif" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    &nbsp;
                </td>
                <td valign="top" align="left" style="width:100%;" colspan="1">
                    <asp:UpdatePanel ID="udpPrintSummons" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" border="0" style="height:90%" width="100%">
                                <tr>
                                    <td valign="top" style="height: 49px">
                                        <p align="center">
                                            <asp:Label ID="lblPageName" runat="server" Width="856px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                        <p align="center">
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="center">
                                        <br />
                                        &nbsp;
                                        &nbsp;&nbsp;
                                        <br />
                                        <asp:DataGrid ID="dgPrintrun" runat="server" BorderColor="Blue" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                            Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgPrintrun_ItemCommand"
                                            CssClass="Normal" Width="795px" >
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <%--<asp:BoundColumn DataField="SumChargeStatus" HeaderText="Charge Status" Visible="False">  -- Oscar 2011-03 changed for 4416--%>
                                                <asp:BoundColumn DataField="SummonsStatus" HeaderText="Summons Status" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="SumPrintFileName" HeaderText="<%$Resources:dgPrintrun.HeaderText %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoOfSummons" HeaderText="#"></asp:BoundColumn>
                                                <asp:EditCommandColumn CancelText="Cancel" EditText="<%$Resources:dgPrintrun.EditText %>" HeaderText="<%$Resources:dgPrintrun.HeaderText1 %>"
                                                    UpdateText="Update" ButtonType="PushButton"></asp:EditCommandColumn>
                                                <asp:EditCommandColumn ButtonType="PushButton" CancelText="Cancel" EditText="<%$Resources:dgPrintrun.EditText1 %>"
                                                    HeaderText="<%$Resources:dgPrintrun.HeaderText2 %>" UpdateText="Update"></asp:EditCommandColumn>
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" BorderStyle="Ridge" Position="TopAndBottom" />
                                        </asp:DataGrid>
                                        <pager:AspNetPager id="dgPrintrunPager" runat="server" 
                                            showcustominfosection="Right" width="795px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                            FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"
                                            onpagechanged="dgPrintrunPager_PageChanged" UpdatePanelId="udpPrintSummons"></pager:AspNetPager>
                                        &nbsp;<br />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle" /><asp:Label
                                    ID="Label1" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height:5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width:100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
