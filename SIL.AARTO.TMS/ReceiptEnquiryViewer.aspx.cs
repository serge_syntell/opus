using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    public partial class ReceiptEnquiryViewer : System.Web.UI.Page
    {
        // Fields
        protected string connectionString = string.Empty;
        protected System.Data.SqlClient.SqlDataAdapter sqlDAReceiptEnquiry;
        protected System.Data.SqlClient.SqlConnection cn;
        protected System.Data.SqlClient.SqlParameter paramRctIntNo;
        protected ReportDocument _reportDoc = new ReportDocument();
        protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        protected dsReceiptEnquiry dsReceiptEnquiry;
        protected int autIntNo = 0;
        protected int rctIntNo = 0;

        //protected string thisPage = "Receipt Enquiry Viewer";
        protected string thisPageURL = "ReceiptEnquiryViewer.aspx";
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            NameValueCollection qs = Request.QueryString;
            if (qs["receipt"] == null)
            {
                string error = (string)GetLocalResourceObject("error");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);

            string reportPage = arn.GetAuthReportName(autIntNo, "ReceiptEnquiry");

            if (reportPage.Equals(""))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "ReceiptEnquiry.rpt", "ReceiptEnquiry", "system", string.Empty);
                reportPage = "ReceiptEnquiry.rpt";
            }

            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "ReceiptEnquiry");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error1"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {

                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error2"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************
            _reportDoc.Load(reportPath);

            // Fill the DataSet
            int rctIntNo = int.Parse(qs["receipt"].ToString());
            //string cbDate = qs["CBDate"];
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("ReceiptEnquiryReport");
            da.SelectCommand.Connection = new SqlConnection(Application["constr"].ToString());
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
            da.SelectCommand.Parameters.Add("@RctIntNo", SqlDbType.Int, 4).Value = rctIntNo;
            //da.SelectCommand.Parameters.Add("@CBDate", SqlDbType.VarChar, 10).Value = cbDate;
            this.dsReceiptEnquiry = new dsReceiptEnquiry();
            this.dsReceiptEnquiry.DataSetName = "dsReceiptEnquiry";
            da.Fill(this.dsReceiptEnquiry);
            da.Dispose();

            // Populate the report
            _reportDoc.SetDataSource(this.dsReceiptEnquiry.Tables[1]);
            this.dsReceiptEnquiry.Dispose();

            //export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            _reportDoc.Dispose();

            //stuff the PDF file into rendering stream
            //first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }


    }

}