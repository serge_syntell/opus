<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Metro" Codebehind="Metro.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
    <style type="text/css">
        .Normal
        {
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="false">
        </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
        <tr>
            <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" height="85%">
        <tr>
            <td align="center" valign="top">
                <img style="height: 1px" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>  "
                                    OnClick="btnOptAdd_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" width="100%" colspan="1">
                <table border="0" width="568" height="482">
                    <tr>
                        <td valign="top" height="47">
                            <p align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                            <p>
                                <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="Panel1" runat="server">
                                <table id="Table1" height="30" cellspacing="1" cellpadding="1" width="542" border="0">
                                    <tr>
                                        <td width="162">
                                        </td>
                                        <td width="7">
                                        </td>
                                        <td width="7">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="162">
                                            <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label>
                                        </td>
                                        <td width="7">
                                            <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="10"></asp:TextBox>
                                        </td>
                                        <td width="7">
                                            <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                OnClick="btnSearch_Click"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:DataGrid ID="dgMetro" Width="495px" runat="server" BorderColor="Black" AllowPaging="True"
                                GridLines="Vertical" CellPadding="4" Font-Size="8pt" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                AutoGenerateColumns="False" OnItemCommand="dgMetro_ItemCommand" OnPageIndexChanged="dgMetro_PageIndexChanged">
                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="MtrIntNo" HeaderText="MtrIntNo"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MtrCode" HeaderText="<%$Resources:dgMetro.HeaderText1 %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MtrName" HeaderText="<%$Resources:dgMetro.HeaderText2 %>"></asp:BoundColumn>
                                    <asp:ButtonColumn Text="<%$Resources:dgMetroItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                </Columns>
                                <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                            </asp:DataGrid>
                            <asp:Panel ID="pnlAddMetro" runat="server" Height="127px">
                                <table id="Table2" height="418" cellspacing="1" cellpadding="1" width="735" border="0">
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="lblAddMetro" runat="server" Width="120px" CssClass="ProductListHead" Text="<%$Resources:lblAddMetro.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label5" runat="server" Width="71px" CssClass="NormalBold" Height="21px" Text="<%$Resources:lblAddRegion.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:DropDownList ID="ddlAddRegion" runat="server" Width="217px" CssClass="NormalMand"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="2">
                                            <asp:Label ID="lblAddMetroCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroCode.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtAddMetroCode" runat="server" Width="50px" CssClass="NormalMand"
                                                MaxLength="3" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                ControlToValidate="txtAddMetroCode"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="2">
                                            <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroNo.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtAddMetroNo" runat="server" Width="72px" CssClass="NormalMand"
                                                MaxLength="6" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqNo.ErrorMessage %>"
                                                ControlToValidate="txtAddMetroNo"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroName.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtAddMetroName" runat="server" Width="242px" CssClass="NormalMand"
                                                MaxLength="100" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqName.ErrorMessage %>"
                                                ControlToValidate="txtAddMetroName"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                            <td colspan="2">
                                                <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                <tr bgcolor='#FFFFFF'>
                                                <td height="100"><asp:Label ID="lblAddTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                </tr>
                                                </table>
                                            </td>
                                            <td height="25">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label36" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMtrAfri.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtAddMtrAfri" runat="server" Width="242px" CssClass="NormalMand"
                                                MaxLength="100" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                            <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="MtrNameAfrikaans may not be left blank."
                                                ControlToValidate="txtAddMtrAfri"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="2">
                                            <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroPostAddr.Text %>"></asp:Label>&nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtAddMetroPostAddr1" runat="server" Width="290px" CssClass="NormalMand"
                                                MaxLength="100" Height="24px"></asp:TextBox><br />
                                            <asp:TextBox ID="txtAddMetroPostAddr2" runat="server" Width="289px" CssClass="Normal"
                                                MaxLength="100" Height="24px"></asp:TextBox><br />
                                            <asp:TextBox ID="txtAddMetroPostAddr3" runat="server" Width="288px" CssClass="Normal"
                                                MaxLength="100" Height="24px"></asp:TextBox><br />
                                        </td>
                                        <td valign="top" height="2">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqAddress.ErrorMessage %>"
                                                ControlToValidate="txtAddMetroPostAddr1"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroPostCode.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtAddMetroPostCode" runat="server" Width="72px" CssClass="NormalMand"
                                                MaxLength="4" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:reqPostalcode.ErrorMessage %>"
                                                ControlToValidate="txtAddMetroPostCode"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroTel.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtAddMetroTel" runat="server" Width="122px" CssClass="Normal" MaxLength="30"
                                                Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroFax.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtAddMetroFax" runat="server" Width="122px" CssClass="Normal" MaxLength="30"
                                                Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  style="height: 2px">
                                            <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroEmail.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 2px">
                                            <asp:TextBox ID="txtAddMetroEmail" runat="server" Width="232px" CssClass="NormalMand"
                                                MaxLength="50" Height="24px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="emailValid" runat="server" CssClass="NormalRed"
                                                ForeColor=" " Display="Dynamic" ErrorMessage="<%$Resources:ReqEmail.ErrorMessage %>"
                                                ControlToValidate="txtAddMetroEmail" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="height: 2px">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqEmailBlank.ErromMessage %>"
                                                ControlToValidate="txtAddMetroEmail"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label20" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPhysAdd.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPhysAdd1" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPhysAdd2" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPhysAdd3" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPhysAdd4" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label23" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPhysCode.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPhysCode" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="2">
                                            &nbsp;</td>
                                        <td class="ProductListHead" height="2">
                                            <asp:Label ID="Label59" runat="server" Text="<%$Resources:tblTd.Text %>"></asp:Label></td>
                                        <td class="Normal" height="2" valign="top">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="2">
                                            <asp:Label ID="Label68" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblPayPoint.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            &nbsp;</td>
                                        <td height="2" class="Normal" valign="top">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label24" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Add.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint1Add1" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label60" runat="server" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint1Add2" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label71" runat="server" Text="<%$Resources:lblStreet.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint1Add3" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint1Add4" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label25" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Code.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint1Code" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label26" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Tel.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint1Tel" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label27" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Fax.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint1Fax" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label28" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Email.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint1Email" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="2">
                                            <asp:Label ID="Label69" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblPayPoint2.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            &nbsp;</td>
                                        <td height="2" class="Normal" valign="top">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label29" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Add.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint2Add1" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label72" runat="server" Text="<%$Resources:lblAuthority2.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint2Add2" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label73" runat="server" Text="<%$Resources:lblStreet.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint2Add3" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint2Add4" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label30" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroCode.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint2Code" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label31" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Tel.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint2Tel" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label32" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Fax.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint2Fax" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label33" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Email.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrPayPoint2Email" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label34" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrDepartName.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrDepartName" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label74" runat="server" Text="<%$Resources:lblDepartment.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label35" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrFooter.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Add_MtrFooter" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label75" runat="server" Text="<%$Resources:lblAddrDetails.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label21" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrNoticePaymentInfo.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Add_MtrNoticePaymentInfo" runat="server" CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label22" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrNoticeIssuedByInfo.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Add_MtrNoticeIssuedByInfo" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label36" runat="server" CssClass="NormalBold" Width="150px">Document From:</asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Add_MtrDocumentFrom" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label37" runat="server" CssClass="NormalBold" Width="150px">Receipt From:</asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Add_MtrReceiptFrom" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label38" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrDocumentFromEnglish.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Add_MtrDocumentFromEnglish" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                            <asp:Label ID="Label76" runat="server" Text="<%$Resources:lblEG.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label39" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrDocumentFromAfrikaans.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Add_MtrDocumentFromAfrikaans" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                            <asp:Label ID="Label77" runat="server" Text="<%$Resources:lblEgName.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label40" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrReceiptEnglishAct.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Add_MtrReceiptEnglishAct" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                            <asp:Label ID="Label78" runat="server" Text="<%$Resources:lblCriminal.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label41" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrReceiptAfrikaansAct.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Add_MtrReceiptAfrikaansAct" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                            <asp:Label ID="Label79" runat="server" Text="<%$Resources:lblStrafproseswet.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                    <td><asp:Label ID="Label42" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblLogo.Text %>"></asp:Label></td>
                                    <td><asp:FileUpload id="upload" runat="server"/></td>
                                    <td class="Normal" valign="top">
                                        <asp:Label ID="Label80" runat="server" Text="<%$Resources:lblUpl.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td  style="height: 26px">
                                        </td>
                                        <td style="height: 26px">
                                        </td>
                                        <td style="height: 26px">
                                            <asp:Button ID="btnAddMetro" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddMetro.Text %>"
                                                OnClick="btnAddMetro_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlUpdateMetro" runat="server" Height="127px">
                                <table id="Table3" height="418" cellspacing="1" cellpadding="1" width="734" border="0">
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label19" runat="server" Width="158px" CssClass="ProductListHead" Text="<%$Resources:lblUpdMetro.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label6" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblAddRegion.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:DropDownList ID="ddlRegion" runat="server" Width="217px" CssClass="Normal" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="2">
                                            <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroCode.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="txtMetroCode" runat="server" Width="41px" CssClass="NormalMand"
                                                MaxLength="5" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                ControlToValidate="txtMetroCode"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="25">
                                            <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroNo.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" height="25">
                                            <asp:TextBox ID="txtMetroNo" runat="server" Width="72px" CssClass="NormalMand" MaxLength="6"
                                                Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="25">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqNo.ErrorMessage %>"
                                                ControlToValidate="txtMetroNo"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="25">
                                            <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroName.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" height="25">
                                            <asp:TextBox ID="txtMetroName" runat="server" Width="228px" CssClass="NormalMand"
                                                MaxLength="100" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="25">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqName.ErrorMessage %>"
                                                ControlToValidate="txtMetroName"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                            <tr bgcolor='#FFFFFF'>
                                            <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                            <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                            </tr>
                                            </table>
                                        </td>
                                        <td height="25">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="25">
                                            <asp:Label ID="Label37" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMtrAfri.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" height="25">
                                            <asp:TextBox ID="txtUpdateMtrNameAfri" runat="server" Width="228px" CssClass="NormalMand"
                                                MaxLength="100" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="25">
                                            <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="Metro name may not be left blank."
                                                ControlToValidate="txtMetroName"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="25">
                                            <asp:Label ID="Label9" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddMetroPostAddr.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" height="25">
                                            <asp:TextBox ID="txtMetroPostAddr1" runat="server" Width="289px" CssClass="NormalMand"
                                                MaxLength="100" Height="24px"></asp:TextBox><br />
                                            <asp:TextBox ID="txtMetroPostAddr2" runat="server" Width="286px" CssClass="Normal"
                                                MaxLength="100" Height="24px"></asp:TextBox><br />
                                            <asp:TextBox ID="txtMetroPostAddr3" runat="server" Width="287px" CssClass="Normal"
                                                MaxLength="100" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="25">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqAddress.ErrorMessage %>"
                                                ControlToValidate="txtMetroPostAddr1"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="25">
                                            <asp:Label ID="Label15" runat="server" CssClass="NormalBold"  Text="<%$Resources:lblAddMetroPostCode.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" height="25">
                                            <asp:TextBox ID="txtMetroPostCode" runat="server" Width="72px" CssClass="NormalMand"
                                                MaxLength="4" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="25">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:reqPostalcode.ErrorMessage %>"
                                                ControlToValidate="txtMetroPostCode"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="25">
                                            <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroTel.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" height="25">
                                            <asp:TextBox ID="txtMetroTel" runat="server" Width="122px" CssClass="Normal" MaxLength="30"
                                                Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="25">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  height="25">
                                            <asp:Label ID="Label17" runat="server" CssClass="NormalBold"  Text="<%$Resources:lblAddMetroFax.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" height="25">
                                            <asp:TextBox ID="txtMetroFax" runat="server" Width="122px" CssClass="Normal" MaxLength="30"
                                                Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="25">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" height="25">
                                            <asp:Label ID="Label18" runat="server" CssClass="NormalBold"  Text="<%$Resources:lblAddMetroEmail.Text %>">:</asp:Label>
                                        </td>
                                        <td valign="top"  height="25">
                                            <asp:TextBox ID="txtMetroEmail" runat="server" Width="154px" CssClass="NormalMand"
                                                MaxLength="50" Height="24px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" CssClass="NormalRed"
                                                ForeColor=" " Display="Dynamic" ErrorMessage="<%$Resources:ReqEmail.ErrorMessage %>"
                                                ControlToValidate="txtMetroEmail" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"></asp:RegularExpressionValidator>
                                        </td>
                                        <td height="25">
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" Width="210px"
                                                CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqEmailBlank.ErromMessage %>"
                                                ControlToValidate="txtMetroEmail"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label43" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPhysAdd.Text %>"></asp:Label>
                                        </td>
                                        <td  height="2">
                                            <asp:TextBox ID="Up_MtrPhysAdd1" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPhysAdd2" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="2">
                                            &nbsp;
                                        </td>
                                        <td  height="2">
                                            <asp:TextBox ID="Up_MtrPhysAdd3" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPhysAdd4" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label44" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPhysCode.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPhysCode" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="2">
                                            &nbsp;</td>
                                        <td class="ProductListHead" height="2">
                                            <asp:Label ID="Label81" runat="server" Text="<%$Resources:tblTd.Text%>"> 
                                            </asp:Label></td>
                                        <td height="2">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label67" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblPayPoint.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            &nbsp;</td>
                                        <td height="2" class="Normal" valign="top">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="2">
                                            <asp:Label ID="Label45" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Add.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint1Add1" runat="server" CssClass="NormalMand" 
                                                Height="24px" MaxLength="30" Width="285px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label82" runat="server"  Text="<%$Resources:lblAuthority.Text %>"> </asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint1Add2" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label83" runat="server" Text="<%$Resources:lblStreet.Text %>"></asp:Label> </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint1Add3" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint1Add4" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="2">
                                            <asp:Label ID="Label46" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroCode.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint1Code" runat="server" CssClass="NormalMand" 
                                                Height="24px" MaxLength="30" Width="285px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label47" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Tel.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint1Tel" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label48" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Fax.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint1Fax" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label49" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Email.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint1Email" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label70" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblPayPoint2.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            &nbsp;</td>
                                        <td height="2" class="Normal" valign="top">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label50" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Add.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint2Add1" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label84" runat="server"  Text="<%$Resources:lblAuthority2.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint2Add2" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label85" runat="server"  Text="<%$Resources:lblStreet.Text %>"></asp:Label> </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint2Add3" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            &nbsp;
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint2Add4" runat="server" Width="285px" CssClass="Normal"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label51" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMetroCode.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint2Code" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label52" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Tel.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint2Tel" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label53" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Fax.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint2Fax" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label54" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrPayPoint1Email.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrPayPoint2Email" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  height="2">
                                            <asp:Label ID="Label55" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrDepartName.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrDepartName" runat="server" Width="285px" CssClass="NormalMand"
                                                MaxLength="30" Height="24px"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label86" runat="server" Text="<%$Resources:lblDepartment.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td height="2" >
                                            <asp:Label ID="Label56" runat="server" CssClass="NormalBold" Text="<%$Resources:lblMtrFooter.Text %>"></asp:Label>
                                        </td>
                                        <td height="2">
                                            <asp:TextBox ID="Up_MtrFooter" runat="server" CssClass="NormalMand" Width="285px"
                                                Height="24px" MaxLength="30"></asp:TextBox>
                                        </td>
                                        <td height="2" class="Normal" valign="top">
                                            <asp:Label ID="Label87" runat="server"  Text="<%$Resources:lblAddrDetails.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label57" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblAddrDetails.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Up_MtrNoticePaymentInfo" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label58" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrNoticeIssuedByInfo.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="Up_MtrNoticeIssuedByInfo" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label59" runat="server" CssClass="NormalBold" Width="150px">Metro Document From:</asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Up_MtrDocumentFrom" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label60" runat="server" CssClass="NormalBold" Width="150px">Metro Receipt From:</asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Up_MtrReceiptFrom" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label61" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrDocumentFromEnglish.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Up_MtrDocumentFromEnglish" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                            <asp:Label ID="Label88" runat="server"  Text="<%$Resources:lblEG.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label62" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrDocumentFromAfrikaans.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Up_MtrDocumentFromAfrikaans" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                            <asp:Label ID="Label89" runat="server"  Text="<%$Resources:lblEgName.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <asp:Label ID="Label63" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblMtrReceiptEnglishAct.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="295">
                                            <asp:TextBox ID="Up_MtrReceiptEnglishAct" runat="server" 
                                                CssClass="NormalMand" Width="295px"
                                                Height="75px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td class="Normal" valign="top">
                                            <asp:Label ID="Label90" runat="server"  Text="<%$Resources:lblCriminal.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                    <td valign="top"><asp:Label ID="Label64" runat="server" CssClass="NormalBold" 
                                            Width="150px" Text="<%$Resources:lblMtrReceiptAfrikaansAct.Text %>"></asp:Label></td>
                                    <td valign="top" width="295">
                                        <asp:TextBox ID="Up_MtrReceiptAfrikaansAct" runat="server" 
                                            CssClass="NormalMand" Height="75px" TextMode="MultiLine" Width="295px"></asp:TextBox>
                                        </td>
                                    <td class="Normal" valign="top">
                                        <asp:Label ID="Label91" runat="server"  Text="<%$Resources:lblStrafproseswet.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                    <td><asp:Label ID="Label65" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblLogo.Text %>"></asp:Label></td>
                                    <td>
                                        <asp:Image ID="mtrlogo" runat="server" Height="100px" Width="100px" />
                                        </td>
                                    <td class="Normal" valign="top">
                                        <asp:Label ID="Label92" runat="server"  Text="<%$Resources:lblUpl.Text %>"></asp:Label></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label66" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblUpdLogo.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="uploads" runat="server" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="height: 21px">
                                        </td>
                                        <td style="height: 21px">
                                        </td>
                                        <td style="height: 21px">
                                            <asp:Button ID="btnUpdateMetro" runat="server" CssClass="NormalButton" 
                                                OnClick="btnUpdate_Click"  Text="<%$Resources:btnUpdateMetro.Text %>"  OnClientClick="return VerifytLookupRequired()"/>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
