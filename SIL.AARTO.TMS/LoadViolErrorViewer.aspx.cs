using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS.Reports
{
    /// <summary>
    /// Summary description for FirstNotice1.
    /// </summary>
    public partial class LoadViolErrorViewer : System.Web.UI.Page
    {
        protected System.Data.SqlClient.SqlConnection cn;
        protected System.Data.SqlClient.SqlParameter paramUserIntNo;
        protected System.Data.SqlClient.SqlParameter paramAutIntNo;
        protected ReportDocument _reportDoc = new ReportDocument();
        protected ExportOptions _exportOpt;
        protected DiskFileDestinationOptions _diskFileOpt;
        protected System.Data.SqlClient.SqlDataAdapter sqlDATempLoadViol;
        protected dsTempLoadViolErrors dsTempLoadViolErrors;
        protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;

        protected string thisPageURL = "LoadViolErrorViewer.aspx";
        //protected string thisPage = "Camera Unit Maintenance";
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string keywords = string.Empty;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            int userIntNo = Convert.ToInt32(Session["userIntNo"]);
            int autIntNo = 0;

            string reportPage = "LoadViolErrors.rpt";
            string reportPath = Server.MapPath("reports/" + reportPage);

            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            _reportDoc.Load(reportPath);

            //if (File.Exists(reportPath))
            //{
            if (Request.QueryString["Authority"] != null)
            {
                autIntNo = Convert.ToInt32(Request.QueryString["Authority"]);
                //set filter for batch to print
                paramAutIntNo.Value = autIntNo;
                paramUserIntNo.Value = userIntNo;

                //get data and populate dataset
                sqlDATempLoadViol.SelectCommand.Connection.ConnectionString = Application["constr"].ToString();
                sqlDATempLoadViol.Fill(dsTempLoadViolErrors);
                sqlDATempLoadViol.Dispose();

                //set up new report based on .rpt report class
                _reportDoc.SetDataSource(dsTempLoadViolErrors);
                dsTempLoadViolErrors.Dispose();

                //export the pdf file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
                _reportDoc.Dispose();

                //stuff the PDF file into rendering stream
                //first clear everything dynamically created and just send PDF file
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }
            //}

            //else
            //{
            //    // SD: 20.11.2008 : Added Error.aspx page
            //    Response.Write("<script language='javascript'> { self.close() }</script>");
            //    PageToOpen page = new PageToOpen(this, "Error.aspx");
            //    page.Parameters.Add(new QSParams("error", "Report page not available"));
            //    page.Parameters.Add(new QSParams("errorPage", "LoadViolErrors"));
            //    Helper_Web.BuildPopup(page); 


            //}
        }

        protected void SetMargins(int left, int top, int right, int bottom)
        {
            PageMargins margins;

            // Get the PageMargins structure and set the 
            // margins for the report.
            margins = _reportDoc.PrintOptions.PageMargins;
            margins.leftMargin = left;
            margins.topMargin = top;
            margins.rightMargin = right;
            margins.bottomMargin = bottom;

            // Apply the page margins.
            _reportDoc.PrintOptions.ApplyPageMargins(margins);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        protected void InitializeComponent()
        {
            this.sqlDATempLoadViol = new System.Data.SqlClient.SqlDataAdapter();
            this.cn = new System.Data.SqlClient.SqlConnection();
            this.dsTempLoadViolErrors = new dsTempLoadViolErrors();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            ((System.ComponentModel.ISupportInitialize)(this.dsTempLoadViolErrors)).BeginInit();
            // 
            // sqlDATempLoadViol
            // 
            this.sqlDATempLoadViol.SelectCommand = this.sqlSelectCommand1;
            this.sqlDATempLoadViol.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																										new System.Data.Common.DataTableMapping("Table", "TempLoadViolErrorList", new System.Data.Common.DataColumnMapping[] {
																																																								 new System.Data.Common.DataColumnMapping("TErIntNo", "TErIntNo"),
																																																								 new System.Data.Common.DataColumnMapping("TErDescr", "TErDescr"),
																																																								 new System.Data.Common.DataColumnMapping("TErColumn", "TErColumn"),
																																																								 new System.Data.Common.DataColumnMapping("TErValue", "TErValue"),
																																																								 new System.Data.Common.DataColumnMapping("TErCorrectVal", "TErCorrectVal"),
																																																								 new System.Data.Common.DataColumnMapping("TErRefNo", "TErRefNo")})});
            // 
            // cn
            // 
            this.cn.ConnectionString = "workstation id=XE4500;packet size=4096;integrated security=SSPI;data source=SERVE" +
                "R2;persist security info=False;initial catalog=TMS";
            // 
            // dsTempLoadViolErrors
            // 
            this.dsTempLoadViolErrors.DataSetName = "dsTempLoadViolErrors";
            this.dsTempLoadViolErrors.Locale = new System.Globalization.CultureInfo("en-US");
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "[TempLoadViolErrorList]";
            this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand1.Connection = this.cn;
            this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((System.Byte)(0)), ((System.Byte)(0)), "", System.Data.DataRowVersion.Current, null));
            paramUserIntNo = this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UserIntNo", System.Data.SqlDbType.Int, 4));
            paramAutIntNo = this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AutIntNo", System.Data.SqlDbType.Int, 4));
            ((System.ComponentModel.ISupportInitialize)(this.dsTempLoadViolErrors)).EndInit();

        }
        #endregion


    }
}
