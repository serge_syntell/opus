<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.VehicleMake" Codebehind="VehicleMake.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
     <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%" id="Table5">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%" id="Table6">
            <tr>
                <td align="center" valign="top" style="width: 182px">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$ Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$ Resources:btnOptAdd.Text %> "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$ Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$ Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482" id="Table7">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$ Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table id="Table1" height="30" cellspacing="1" cellpadding="1" width="542" border="0">
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$ Resources:lblSelection.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="10"></asp:TextBox></td>
                                            <td width="7">
                                                <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$ Resources:btnSearch.Text %>"
                                                    OnClick="btnSearch_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgVehicleMake" Width="495px" runat="server" BorderColor="Black"
                                    AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgVehicleMake_ItemCommand"
                                    >
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="VMIntNo" HeaderText="VMIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="VMCode" HeaderText="<%$Resources:dgVehicleMake.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="VMDescr" HeaderText="<%$Resources:dgVehicleMake.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgVehicleMakeItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <pager:AspNetPager id="dgVehicleMakePager" runat="server" 
                                            showcustominfosection="Right" width="495px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                            FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"
                                    onpagechanged="dgVehicleMakePager_PageChanged"></pager:AspNetPager>

                                <asp:Panel ID="pnlAddVehicleMake" runat="server" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="lblAddVehicleMake" runat="server" CssClass="ProductListHead" Text="<%$ Resources:lblAddVehicleMake.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2" valign="top">
                                                <asp:Label ID="lblAddVMCode" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblAddVMCode.Text%>"></asp:Label></td>
                                            <td width="248" height="2" valign="top">
                                                <asp:TextBox ID="txtAddVMCode" runat="server" Width="83px" CssClass="NormalMand"
                                                    MaxLength="3" Height="24px"></asp:TextBox></td>
                                            <td height="2" valign="top">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtAddVMCode" ErrorMessage="<%$ Resources:ReqCode.ErrorMessage%>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblVMDescr.Text%>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtAddVMDescr" runat="server" Width="242px" CssClass="NormalMand"
                                                    MaxLength="30" Height="24px"></asp:TextBox></td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td height="2" width="157">
                                                <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblNatis.Text %>"></asp:Label></td>
                                            <td height="2" width="248">
                                                <asp:TextBox ID="txtAddNatis" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="3" Width="83px"></asp:TextBox></td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="2" width="157">
                                                <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblAddTCS.Text %>"></asp:Label></td>
                                            <td height="2" width="248">
                                                <asp:TextBox ID="txtAddTCS" runat="server" CssClass="NormalMand" Height="24px" MaxLength="3"
                                                    Width="83px"></asp:TextBox></td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 2px" width="157">
                                                <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblCivitas.Text %>"></asp:Label></td>
                                            <td style="height: 2px" width="248">
                                                <asp:TextBox ID="txtAddCivitas" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="3" Width="83px"></asp:TextBox></td>
                                            <td style="height: 2px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddVehicleMake" runat="server" CssClass="NormalButton" Text="<%$ Resources:btnAddVehicleMake.Text %>"
                                                    OnClick="btnAddVehicleMake_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateVehicleMake" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label19" runat="server" Width="194px" CssClass="ProductListHead" Text="<%$ Resources:lblUpdate.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2" valign="top">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblAddVMCode.Text %>"></asp:Label></td>
                                            <td width="248" height="2" valign="top">
                                                <asp:TextBox ID="txtVMCode" runat="server" Width="83px" CssClass="NormalMand" MaxLength="3"
                                                    Height="24px"></asp:TextBox></td>
                                            <td height="2" valign="top">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtVMCode" ErrorMessage="<%$ Resources:ReqCode.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td height="25" valign="top" width="157">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblVMDescr.Text %>"></asp:Label></td>
                                            <td height="25" valign="top" width="248">
                                                <asp:TextBox ID="txtVMDescr" runat="server" Width="228px" CssClass="NormalMand" MaxLength="30"
                                                    Height="24px"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td height="25" valign="top" width="157">
                                                <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblNatis.Text %>"></asp:Label></td>
                                            <td height="25" valign="top" width="248">
                                                <asp:TextBox ID="txtNatis" runat="server" CssClass="NormalMand" Height="24px" MaxLength="3"
                                                    Width="83px"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" valign="top" width="157">
                                                <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblAddTCS.Text %>"></asp:Label></td>
                                            <td height="25" valign="top" width="248">
                                                <asp:TextBox ID="txtTCS" runat="server" CssClass="NormalMand" Height="24px" MaxLength="3"
                                                    Width="83px"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Text="<%$ Resources:lblCivitas.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtCivitas" runat="server" CssClass="NormalMand" Height="24px" MaxLength="3"
                                                    Width="83px"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <asp:Label ID="Label13" runat="server" CssClass="ProductListHead" Width="192px" Text="<%$ Resources:lbllVMLookups.Text %>"></asp:Label><asp:CheckBoxList
                                                    ID="cblVMLookups" runat="server" CssClass="NormalBold" Width="300px">
                                                </asp:CheckBoxList></td>
                                            <td width="248">
                                            </td>
                                            <td valign="top">
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$ Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%" id="Table8">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
