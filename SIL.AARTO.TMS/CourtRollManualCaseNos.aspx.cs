using System;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.ServiceBase;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using System.Linq;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    // Oscar 20120309 add for print service Q
    public class PrintCourtRollQueue
    {
        public int PFNIntNo { get; set; }
        public string AutCode { get; set; }
        public DateTime ActDate { get; set; }
    }
    public class PrintCourtRollQueueList : List<PrintCourtRollQueue>
    {
        public void Add(int pfnIntNo, string autCode, DateTime actDate)
        {
            PrintCourtRollQueue item = new PrintCourtRollQueue()
            {
                PFNIntNo = pfnIntNo,
                AutCode = autCode,
                ActDate = actDate
            };
            this.Add(item);
        }
        public bool ContainsKey(int pfnIntNo)
        {
            if (this == null || this.Count <= 0) return false;
            return this.FirstOrDefault(p => p.PFNIntNo == pfnIntNo) != null;
        }
    }

    public partial class CourtRollManualCaseNos : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private int nDays = 0;
        private string numericOnly = @"^\d+$";

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "CourtRollManualCaseNos.aspx";
        //protected string thisPage = "Court Roll - Manual entry of case numbers";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        //jerry 2011-12-15 push queue using in SummonsLock win service
        private int noOfDaysCourtRollCaptured;
        private string autCode = string.Empty;
        private DateTime sumCourtDate;

        //Jerry 2012-04-27 add
        private string cRTranType = "CAS";
        private string cRTranDescr = "Next case number for court room.";

        //Jerry 2013-01-04 add
        private string caseNoFormatValue;//if Y the case no is C/T/CourtRoomNo/SequenceNo/Year, else is CourtRoomPrefix/SequenceNo/Year.

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            string autName = Session["autName"].ToString();

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            lblError.Text = string.Empty;

            //jerry 2011-12-15 push queue using in SummonsLock win service
            SetAuthRuleDefaults();
            GetAutCode();

            //dls 080119 - check the view state before getting the daterule from the database again
            if (ViewState["FinalCourtRoll_days"] == null)
            {
                nDays = CheckDateRule(this.autIntNo);
                ViewState.Add("FinalCourtRoll_days", nDays);
            }
            else
            {
                nDays = int.Parse(ViewState["FinalCourtRoll_days"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlCourtDates.Visible = false;
                pnlCaseNos.Visible = false;
                pnlCourt.Visible = false;

                if (CheckRuleForAutoCaseNos())
                {
                    lblError.Visible = true;
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text"), "[6010]", autName);
                    return;
                }

                this.PopulateCourts();
                pnlCourt.Visible = true;

            }
        }

        private bool CheckRuleForAutoCaseNos()
        {
            //AuthorityRulesDB ardb = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails ardet = null;
            //ardet = ardb.GetAuthorityRulesDetailsByCode(autIntNo, "6010", "Case numbers allocated automatically ", 0, "Y", "Y or N; Y (default)", "TPExInt");

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails ardet = new AuthorityRulesDetails();
            ardet.AutIntNo = autIntNo;
            ardet.ARCode = "6010";
            ardet.LastUser = this.login;

            DefaultAuthRules authRule = new DefaultAuthRules(ardet, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            return value.Value.Equals("Y") ? true : false;
        }

        /// <summary>
        /// jerry 2011-12-15 push queue using in SummonsLock win service
        /// </summary>
        private void SetAuthRuleDefaults()
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule = new AuthorityRulesDetails();
            rule.ARCode = "6100";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.login;
            DefaultAuthRules ar;
            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> courtRollCapturedRule = ar.SetDefaultAuthRule();
            noOfDaysCourtRollCaptured = courtRollCapturedRule.Key;

            //Jerry 2013-01-04 add
            rule.ARCode = "6005";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.login;
            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> caseNoFormatRule = ar.SetDefaultAuthRule();
            caseNoFormatValue = caseNoFormatRule.Value;
        }

        /// <summary>
        /// jerry 2011-12-15 push queue using in SummonsLock win service
        /// </summary>
        private void GetAutCode()
        {
            this.autCode = new AuthorityDB(this.connectionString).GetAuthorityDetails(this.autIntNo).AutCode.Trim();
        }

        private void PopulateCourts()
        {
            CourtDB court = new CourtDB(this.connectionString);
            SqlDataReader reader = court.GetAuth_CourtListByAuth(this.autIntNo);
            this.ddlCourt.DataSource = reader;
            this.ddlCourt.DataValueField = "CrtIntNo";
            this.ddlCourt.DataTextField = "CrtDetails";
            this.ddlCourt.DataBind();
            reader.Close();
            this.ddlCourt.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourt.Items"), string.Empty));
        }

        private int CheckDateRule(int nAutIntNo)
        {
            //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
            //DateRulesDetails rule = new DateRulesDetails();
            //rule.AutIntNo = autIntNo;
            //rule.DtRDescr = "Amount in days that the final court roll must be printed";
            //rule.DtREndDate = "FinalCourtRoll";
            //rule.DtRNoOfDays = -3;
            //rule.DtRStartDate = "CDate";
            //rule.LastUser = "TMS";
            //rule = dateRule.GetDefaultDateRule(rule);
            //return rule.DtRNoOfDays;

            //AutIntNo, StartDate, EndDate and LastUser must be set up before the default rule is called
            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.LastUser = this.login;
            rule.DtRStartDate = "CDate";
            rule.DtREndDate = "FinalCourtRoll";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return noOfDays;
        }

        private void PopulateCourtRooms(int nCrtIntNo)
        {
            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            SqlDataReader reader = room.GetCourtRoomListByCrtRStatusFlag(nCrtIntNo);//room.GetCourtRoomList(nCrtIntNo);
            this.ddlCourtRoom.DataSource = reader;
            this.ddlCourtRoom.DataValueField = "CrtRIntNo";
            this.ddlCourtRoom.DataTextField = "CrtRoomDetails";
            this.ddlCourtRoom.DataBind();
            this.ddlCourtRoom.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourtRoom.Items"), "0"));
            reader.Close();
        }

        public void BindGrid(int crtIntNo)
        {
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Stalberg.TMS.CourtDatesDB tranNoList = new Stalberg.TMS.CourtDatesDB(connectionString);

            //mrs 20080812 CrtRIntNo required
            int crtRIntNo = Convert.ToInt32(ViewState["crtRIntNo"].ToString());

            int totalCount = 0;
            DataSet data = tranNoList.GetCourtDatesListDS(crtRIntNo, autIntNo, "Y", grdDatesPager.PageSize, grdDatesPager.CurrentPageIndex - 1, out totalCount);
            grdDates.DataSource = data;
            grdDates.DataKeyNames = new string[] { "CDIntNo" };
            grdDates.DataBind();
            grdDatesPager.RecordCount = totalCount;

            if (grdDates.Rows.Count == 0)
            {
                pnlCourtDates.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
            {
                pnlCourtDates.Visible = true;
            }

            pnlCaseNos.Visible = false;

            data.Dispose();
        }



        public void View(String sSelectedDate)
        {
            string sReportType = "P";
            lblError.Visible = true;
            lblError.Text = string.Empty;
            SqlDataReader readerCrtR = null;
            //PageToOpen page = null;
            int nCrtIntNo = -1;
            DateTime dt;
            List<PageToOpen> pages = new List<PageToOpen>();

            if (ddlCourt.SelectedIndex == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            //mrs 20080812 TODO CrtRIntNo required
            CourtDB court = new CourtDB(this.connectionString);
            nCrtIntNo = Convert.ToInt32(ddlCourt.SelectedValue);
            dt = court.CourtDateByCourt(nCrtIntNo, DateTime.Parse(sSelectedDate));

            // SD: 20081205 - test for null value
            if (nCrtIntNo < 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append((string)GetLocalResourceObject("lblError.Text3")+"\n<ul>\n");
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text4") + "</li>");
                sb.Append("</ul>");

                lblError.Text = sb.ToString();
                lblError.Visible = true;

            }

            if (dt.CompareTo(new DateTime(2000, 1, 1)) <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5") + ddlCourt.SelectedItem;
                return;
            }

            // All court rooms selected
            if (ddlCourtRoom.SelectedValue == "0")
            {
                CourtRoomDB room = new CourtRoomDB(this.connectionString);

                dt = dt.AddDays((double)nDays);
                if (dt.CompareTo(DateTime.Now) <= 0)
                    sReportType = "F";
                //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
                readerCrtR = room.GetCourtRoomListByCrtRStatusFlag(nCrtIntNo);//room.GetCourtRoomList(nCrtIntNo);
                if (readerCrtR.HasRows)
                {
                    // I think they may have to do this one court room at a time
                    while (readerCrtR.Read())
                    {
                        // get all the served summonses, not paid, not withdrawn not under rep - with no case number for the selected court date 

                        // user enters sequence number in first row

                        // user enters next number(s) in next row or clicks [autofill]

                        // user clicks [save], [continue] or [close]

                    }
                    readerCrtR.Close();
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6") + ddlCourt.SelectedItem;
                    return;
                }
            }
            else // single court room selected
            {
                dt = dt.AddDays((double)nDays);

                // get all the served summonses, not paid, not withdrawn not under rep - with no case number for the selected court date 

                // user enters sequence number in first row

                // user enters next number(s) in next row or clicks [autofill]

                // user clicks [save], [continue] or [close]
            }
        }

        // Not used as yet
        protected void btnViewAll_Click(object sender, EventArgs e)
        {
        }

        protected void ddlCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourt.SelectedIndex > 0)
            {
                pnlCourtDates.Visible = false;
                pnlCaseNos.Visible = false;
                PopulateCourtRooms(Convert.ToInt32(ddlCourt.SelectedValue));
                //BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));
            }
            else
            {
                ddlCourtRoom.SelectedIndex = 0;
                //Jerry 2012-05-11 change
                ddlCourtRoom.Items.Clear();
                pnlCourtDates.Visible = false;
                pnlCaseNos.Visible = false;
            }
        }

        protected void grdDates_SelectedIndexChanged(object sender, EventArgs e)
        {
            RegisterScriptUnblock();

            //Jerry 2013-01-05 add
            grdEnterCaseNos.EditIndex = -1;
            //Jerry 2012-05-16 add
            grdEnterCaseNos.Visible = false;

            int cdIntNo = 0;
            System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)grdDates.Rows[grdDates.SelectedIndex].Cells[0].Controls[1];

            DateTime dt;

            if (!DateTime.TryParse(lbl.Text, out dt))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                lblError.Visible = true;
                return;
            }

            View(lbl.Text);

            DateTime courtDate = DateTime.Parse(lbl.Text);
            string year = Convert.ToString(courtDate.Year);
            txtCaseSuffix.Text = year.Substring(2, 2);

            if (grdDates.SelectedIndex >= 0)
            {
                cdIntNo = (int)this.grdDates.SelectedDataKey["CDIntNo"];
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                lblError.Visible = true;
                return;
            }

            ViewState.Add("cdIntNo", cdIntNo);
            ViewState.Add("courtDate", courtDate);
            //Jerry 2012-04-27 add
            if (!BindNextCaseNo(courtDate.Year))
                return;
            grdImpersonSummonsPager.CurrentPageIndex = 1;
            grdImpersonSummonsPager.RecordCount = 0;
            grdEnterCaseNosPager.CurrentPageIndex = 1;
            grdEnterCaseNosPager.RecordCount = 0;
            BindEnterCaseNoGrid();
        }

        /// <summary>
        /// Jerry 2012-04-27 add method to get next case no from CourtRoom_Tran
        /// </summary>
        /// <param name="year"></param>
        private bool BindNextCaseNo(int year)
        {
            // 2014-06-20, Oscar added try-catch and TransactionScope
            try
            {
                using (var scope = ServiceUtility.CreateTransactionScope())
                {
                    int crtRIntNo = Convert.ToInt32(ddlCourtRoom.SelectedValue);
                    CourtRoomTranDB courtRoomTranDB = new CourtRoomTranDB(this.connectionString);
                    int cRTranNumber = courtRoomTranDB.GetCaseNoAndWOANextNum(crtRIntNo, cRTranType, cRTranDescr, year, this.login);
                    txtNextCaseNo.Text = cRTranNumber.ToString();

                    scope.Complete();
                }
                return true;
            }
            catch (Exception e)
            {
                this.lblError.Text = e.Message;
                this.lblError.Visible = true;
            }
            return false;
        }


        protected void ddlCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourtRoom.SelectedIndex > 0)
            {
                int crtRIntNo = Convert.ToInt32(ddlCourtRoom.SelectedValue);
                ViewState.Add("crtRIntNo", crtRIntNo);

                CourtRoomDB room = new CourtRoomDB(this.connectionString);
                SqlDataReader reader = room.GetCourtRoomDetails(crtRIntNo);
                while (reader.Read())
                {
                    //Jerry 2013-01-04 add
                    if (caseNoFormatValue == "Y")
                    {
                        lblCasePrefix.Text = (string)GetLocalResourceObject("lblCasePrefix2.Text");
                        txtCasePrefix.Text = Helper.GetReaderValue<string>(reader, "CrtRoomNo").Trim();
                    }
                    else
                    {
                        lblCasePrefix.Text = (string)GetLocalResourceObject("lblCasePrefix.Text");
                        txtCasePrefix.Text = Helper.GetReaderValue<string>(reader, "CrtRPrefix").Trim();
                    }
                    //Jerry 2012-04-27 change
                    //txtNextCaseNo.Text = Helper.GetReaderValue<string>(reader, "CrtRNextCaseNo");
                }
                reader.Dispose();
                grdDatesPager.CurrentPageIndex = 1;
                grdDatesPager.RecordCount = 0;
                BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));
            }
            else
            {
                ddlCourtRoom.SelectedIndex = 0;
                pnlCourtDates.Visible = false;
                pnlCaseNos.Visible = false;
            }

            grdEnterCaseNos.EditIndex = -1;
        }

        public void BindEnterCaseNoGrid()
        {
            //test some time later
            int cdIntNo = Convert.ToInt32(ViewState["cdIntNo"]);
            DateTime courtDate = Convert.ToDateTime(ViewState["courtDate"]);
            bool crLocked = false;
            //check court selected
            int crtIntNo = (Convert.ToInt32(ddlCourt.SelectedValue));
            if (crtIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                lblError.Visible = true;
                return;
            }

            //check for court room and court date selected
            int crtRIntNo = Convert.ToInt32(ddlCourtRoom.SelectedValue);
            if (crtRIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                lblError.Visible = true;
                return;
            }

            //check text boxes contain the case number defaults defaults
            if (txtCasePrefix.Text.Trim().Length <= 0 || txtNextCaseNo.Text.Length == 0 || txtCaseSuffix.Text.Length <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                lblError.Visible = true;
                return;
            }

            //check for valid autintno
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            if (autIntNo <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                lblError.Visible = true;
                return;
            }
            // NB 
            // Summonses are initially bound to court & court date. The court room allocation in tpexint is algorythmic in the sense that the available summonses are equally distributed over the available court rooms in a court
            // Doing the job manually will be somewhat different. We will have to force the selection of the court room and the user must then capture the case numbers consecutively
            // The sort order is Personal service; S54; S56; Impersonal service; S54; S56
            //we first have to make sure the court date has not been locked - CourtDates.CDCourtRollCreatedDate must be null
            Stalberg.TMS.CourtDatesDB court = new Stalberg.TMS.CourtDatesDB(connectionString);
            Stalberg.TMS.CourtDatesDetails cdDetails = court.GetCourtDatesDetails(cdIntNo);

            if (cdDetails.CDCourtRollCreatedDate > DateTime.MinValue)
            {
                crLocked = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                lblError.Visible = true;
                grdEnterCaseNos.Visible = false;

                return;
            }
            else
            {
                crLocked = false;
            }
            //now we need to get the rule that specifies how many days prior to the court date the court roll has to be prepared
            //when automatic we create the final court roll (allocate case numbers) x days prior to court date
            //thereafter the court roll is frozen and cases cannot be removed from the roll
            //does this apply to manual case numbers? No
            // once there is a case number allocated the case will appear on the final court roll, but user might take several days to capture all of them
            //button to finalise court roll & set date finalised
            //now we populate the case entry grid

            //jerry 2011-11-09 add court rules, if value = "Y" get person service case number, if value = "N" get person and impersonal service case number
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = crtIntNo;
            courtRulesDetails.CRCode = "1020";
            courtRulesDetails.LastUser = this.login;
            DefaultCourtRules defaultCourtRule = new DefaultCourtRules(courtRulesDetails, connectionString);
            KeyValuePair<int, string> courtRule = defaultCourtRule.SetDefaultCourtRule();

            Stalberg.TMS.SummonsDB caseList = new Stalberg.TMS.SummonsDB(connectionString);

            int totalCount1 = 0;
            int totalCount2 = 0;
            DataSet data = caseList.GetSummonsListForManualCaseNoEntry(autIntNo, crtIntNo, courtDate, courtRule.Value, crtRIntNo,
                grdEnterCaseNosPager.PageSize, grdEnterCaseNosPager.CurrentPageIndex - 1, out totalCount1,
                grdImpersonSummonsPager.PageSize, grdImpersonSummonsPager.CurrentPageIndex - 1, out totalCount2);
            grdEnterCaseNosPager.RecordCount = totalCount1;
            grdImpersonSummonsPager.RecordCount = totalCount2;

            if (courtRule.Value == "N")
            {
                grdEnterCaseNos.DataSource = data;
            }
            else//jerry 2011-11-09 add
            {
                grdEnterCaseNos.DataSource = data.Tables[0];
                grdImpersonSummons.DataSource = data.Tables[1];
                grdImpersonSummons.DataKeyNames = new string[] { "SumIntNo", "SumCourtDate" };
                grdImpersonSummons.DataBind();
            }
            grdEnterCaseNos.DataKeyNames = new string[] { "SumIntNo", "SumCourtDate" };
            grdEnterCaseNos.DataBind();
            ViewState.Add("SummonsForCaseNo", data);

            //need to store acintno for later

            if (grdEnterCaseNos.Rows.Count == 0)
            {
                grdEnterCaseNos.Visible = false;
                pnlCaseNos.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                lblPersonSummons.Visible = false;//jerry 2011-11-09 add
            }
            else
            {
                grdEnterCaseNos.Visible = true;
                pnlCaseNos.Visible = true;
                //jerry 2011-11-09 add
                if (courtRule.Value == "Y")
                {
                    lblPersonSummons.Visible = true;
                    if (grdImpersonSummons.Rows.Count > 0)
                    {
                        grdImpersonSummons.Visible = true;
                        lblImpersonSummons.Visible = true;
                    }
                    else
                    {
                        grdImpersonSummons.Visible = false;
                        lblImpersonSummons.Visible = false;
                    }
                }
            }

            data.Dispose();
        }

        protected void grdEnterCaseNos_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdEnterCaseNos.EditIndex = e.NewEditIndex;
            // 2013-09-12, Oscar removed, it reset wrong page
            //grdImpersonSummonsPager.CurrentPageIndex = 1;
            //grdImpersonSummonsPager.RecordCount = 0;
            //grdEnterCaseNosPager.CurrentPageIndex = 1;
            //grdEnterCaseNosPager.RecordCount = 0;
            BindEnterCaseNoGrid();
            //eureka - this works!
            int sumIntNo = int.Parse(grdEnterCaseNos.DataKeys[e.NewEditIndex].Values["SumIntNo"].ToString());
            ViewState.Add("sumIntNo", sumIntNo);

            //jerry 2011-11-22 add
            string prefix = txtCasePrefix.Text.Trim();
            string sequence = txtNextCaseNo.Text.Trim();
            string suffix = txtCaseSuffix.Text;
            TextBox txtSumCaseNo = grdEnterCaseNos.Rows[e.NewEditIndex].FindControl("txtSumCaseNo") as TextBox;
            //Jerry 2013-01-04 add
            if (caseNoFormatValue == "Y")
            {
                txtSumCaseNo.Text = "C/T/" + prefix + "/" + sequence.PadLeft(6, '0') + "/" + suffix;
            }
            else
            {
                txtSumCaseNo.Text = prefix + "/" + sequence.PadLeft(6, '0') + "/" + suffix;
            }

            txtSumCaseNo.Enabled = false;
        }

        protected void grdEnterCaseNos_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

            grdEnterCaseNos.EditIndex = -1;
            e.Cancel = true;
            // 2013-09-12, Oscar removed, it reset wrong page
            //grdImpersonSummonsPager.CurrentPageIndex = 1;
            //grdImpersonSummonsPager.RecordCount = 0;
            //grdEnterCaseNosPager.CurrentPageIndex = 1;
            //grdEnterCaseNosPager.RecordCount = 0;
            BindEnterCaseNoGrid();
        }

        protected void grdEnterCaseNos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //this event does not seem to fire
            string _commandName = e.CommandName;

        }

        //protected void grdEnterCaseNos_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        //{
        //    grdEnterCaseNos.PageIndex = e.NewPageIndex;
        //    BindEnterCaseNoGrid();
        //}


        protected void grdEnterCaseNos_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            // 2013-09-11, Oscar added for avoiding mutiple clicks.
            RegisterScriptUnblock();

            int sumIntNo = 0;
            string caseNo = string.Empty;
            //string errMessage = string.Empty;
            bool success = false;
            string error = string.Empty;
            GridViewRow row = grdEnterCaseNos.Rows[e.RowIndex];
            int autIntNo = int.Parse(Session["autIntNo"].ToString());
            int crtIntNo = int.Parse(ddlCourt.SelectedValue.ToString());
            int crtRIntNo = int.Parse(ddlCourtRoom.SelectedValue.ToString());
            if (row != null)
            {
                TextBox txtSumCaseNo = row.FindControl("txtSumCaseNo") as TextBox;
                if (txtSumCaseNo.Text != null)
                {
                    if (txtSumCaseNo.Text.Trim().Equals(""))
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                        lblError.Visible = true;
                        return;
                    }

                    //fire an update on summons
                    sumIntNo = int.Parse(ViewState["sumIntNo"].ToString());
                    caseNo = txtSumCaseNo.Text;
                    int lastSeqNo = 0;
                    string[] sCaseNo = txtSumCaseNo.Text.Split('/');
                    //look for the first numeric section in S/V/01234/07
                    //Jerry 2012-05-15 change
                    //for (int n = 0; n < 4; n++)
                    //{
                    //    if (Regex.IsMatch(sCaseNo[n], numericOnly))
                    //    {
                    //        lastSeqNo = Convert.ToInt32(sCaseNo[n]) + 1;
                    //        break;
                    //    }
                    //}
                    //lastSeqNo = Convert.ToInt32(txtNextCaseNo.Text.Trim()) + 1;

                    //jerry 2011-12-15 get SumCourtDate for push queue
                    sumCourtDate = Convert.ToDateTime(grdEnterCaseNos.DataKeys[e.RowIndex].Values["SumCourtDate"].ToString());

                    success = updateSummonsWithCaseNo(autIntNo, crtIntNo, crtRIntNo, caseNo, lastSeqNo, sumIntNo, ref error);

                    if (success)
                    {
                        grdEnterCaseNos.EditIndex = -1;
                        e.Cancel = true;

                        // 2013-09-12, Oscar moved, adding only happens when success.
                        int.TryParse(this.txtNextCaseNo.Text, out lastSeqNo);
                        lastSeqNo++;
                        txtNextCaseNo.Text = lastSeqNo.ToString(CultureInfo.InvariantCulture);
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtRollManualEntryOfCaseNumbers, PunchAction.Change);  

                        //if (lastSeqNo > 0)
                        //    txtNextCaseNo.Text = lastSeqNo.ToString();

                        // 2013-09-12, Oscar removed, it reset wrong page
                        //grdDatesPager.CurrentPageIndex = 1;
                        //grdDatesPager.RecordCount = 0;

                        BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));
                        //Jerry 2012-05-14 add
                        if (grdEnterCaseNos.Rows.Count == 1)
                        {
                            grdEnterCaseNos.Visible = false;
                        }
                        else
                        {
                            // 2013-09-12, Oscar removed, it reset wrong page
                            //grdImpersonSummonsPager.CurrentPageIndex = 1;
                            //grdImpersonSummonsPager.RecordCount = 0;
                            //grdEnterCaseNosPager.CurrentPageIndex = 1;
                            //grdEnterCaseNosPager.RecordCount = 0;
                            BindEnterCaseNoGrid();
                        }
                    }
                    lblError.Text = error;
                    lblError.Visible = true;
                }
            }
        }

         
        // Oscar 20120309 add for print file
        //Jacob 20140122 edited, add isNewFile parameter.
        //private void SetPrintFileName(int sumIntNo, out int pfnIntNo, out DateTime actDate)
        private void SetPrintFileName(int sumIntNo, out int pfnIntNo, out DateTime actDate, out bool isNewFile)
        {
            isNewFile = false; //Jacob 20140122 added
            string crtName = string.Empty, crtRoomNo = string.Empty;
            string selectedText = this.ddlCourt.SelectedItem.Text.Trim();
            //Edited by Jacob 20130114 start
            //  get the first '(', or if the crtRoomName has '(' in it, will get wrong crtRoomNo.
            //int index = selectedText.LastIndexOf("(");
            int index = selectedText.IndexOf("(");
            //Edited by Jacob 20130114 end
            if (index > -1)
                crtName = selectedText.Substring(0, index).Trim();
            selectedText = this.ddlCourtRoom.SelectedItem.Text.Trim();
            //Edited by Jacob 20130114 start
            //  get the first '(', or if the crtRoomName has '(' in it, will get wrong crtRoomNo.
            //int index = selectedText.LastIndexOf("(");
            index = selectedText.IndexOf("(");
            //Edited by Jacob 20130114 end
            if (index > -1)
                crtRoomNo = selectedText.Substring(0, index).Trim();

            DateRulesDetails drRule = new DateRulesDetails()
            {
                AutIntNo = this.autIntNo,
                DtRStartDate = "CDate",
                DtREndDate = "FinalCourtRoll",
                LastUser = this.login
            };
            DefaultDateRules drRuleDB = new DefaultDateRules(drRule, this.connectionString);
            int noOfDays = drRuleDB.SetDefaultDateRule();

            string crtDate = this.sumCourtDate.ToString("yyyy-MM-dd");
            actDate = this.sumCourtDate.AddDays(noOfDays);
            string printDate = actDate.ToString("yyyy-MM-dd HH-mm");
            string printFileName = string.Format("CR_{0}_{1}_{2}_{3}_{4}", this.autCode, crtName, crtRoomNo, crtDate, printDate);

            PrintFileNameService pfnSvc = new PrintFileNameService();
            PrintFileNameSummonsService pfnsSvc = new PrintFileNameSummonsService();
            PrintFileName pfnEntity = pfnSvc.GetByPrintFileName(printFileName);
            if (pfnEntity == null)
            {
                pfnEntity = new PrintFileName()
                {
                    PrintFileName = printFileName,
                    AutIntNo = autIntNo,
                    LastUser = this.login,
                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                };
                pfnEntity = pfnSvc.Save(pfnEntity);
                isNewFile = true; //Jacob 20140122 added
            }
            PrintFileNameSummons pfnsEntity = pfnsSvc.GetBySumIntNoPfnIntNo(sumIntNo, pfnEntity.PfnIntNo);
            if (pfnsEntity == null)
            {
                pfnsEntity = new PrintFileNameSummons()
                {
                    SumIntNo = sumIntNo,
                    PfnIntNo = pfnEntity.PfnIntNo,
                    LastUser = this.login,
                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                };
                pfnsEntity = pfnsSvc.Save(pfnsEntity);
            }
            pfnIntNo = pfnEntity.PfnIntNo;

        }

        bool needDelay = false;
        PrintCourtRollQueueList printQList = new PrintCourtRollQueueList();
        protected bool updateSummonsWithCaseNo(int autIntNo, int crtIntNo, int crtRIntNo, string caseNo, int lastSeqNo, int sumIntNo, ref string error)
        {
            //fire an update on summons
            string errMessage = string.Empty;
            bool success = false;
            Stalberg.TMS.SummonsDB caseNoUpdate = new SummonsDB(this.connectionString);

            //wl 20090415 - Add: Get the rule "case number generate - use original fine amount"
            AuthorityRulesDetails caseNoGenerateRule = new AuthorityRulesDetails();
            caseNoGenerateRule.AutIntNo = autIntNo;
            caseNoGenerateRule.ARCode = "6030";
            caseNoGenerateRule.LastUser = this.login;

            DefaultAuthRules ar = new DefaultAuthRules(caseNoGenerateRule, this.connectionString);

            KeyValuePair<int, string> caseNoGenerateAmnt = ar.SetDefaultAuthRule();

            //dls 090623 - added checking of auth rule about which amount to use on the summons after the case no is generated
            //jerry 2011-12-15 push queue using in SummonsLock win service
            QueueItem item;
            List<QueueItem> itemList = new List<QueueItem>();
            QueueItemProcessor queProcessor = new QueueItemProcessor();

            using (TransactionScope scope = new TransactionScope())
            {
                int updSumIntNo = caseNoUpdate.UpdateCaseNoFromManualEntry(autIntNo, crtIntNo, crtRIntNo, caseNo, lastSeqNo, this.login, sumIntNo, ref errMessage, caseNoGenerateAmnt.Value);

                switch (updSumIntNo)
                {
                    case -8:
                        error = (string)GetLocalResourceObject("error") + errMessage;
                        break;
                    case -7:
                        error = (string)GetLocalResourceObject("error1") + errMessage;
                        break;
                    case -6:
                        error = (string)GetLocalResourceObject("error2") + errMessage;
                        break;
                    case -5:
                        error = (string)GetLocalResourceObject("error3") + errMessage;
                        break;
                    case -4:
                        error = (string)GetLocalResourceObject("error4") + errMessage;
                        break;
                    case -3:
                        error = (string)GetLocalResourceObject("error5") + errMessage;
                        break;
                    case -2:
                        error = (string)GetLocalResourceObject("error6") + errMessage;
                        break;
                    case -1:
                        error = (string)GetLocalResourceObject("error7") + errMessage;
                        break;
                    case 0:
                        error = (string)GetLocalResourceObject("error8") + "\n";
                        success = true;
                        break;
                    default:
                        error = (string)GetLocalResourceObject("error9");
                        success = true;
                        break;
                }

                // 2013-11-27, Oscar added to handle unhandled minus value.
                success = updSumIntNo >= 0;

                //jerry 2011-12-15 push queue
                if (success)
                {
                    int pfnIntNo;
                    DateTime actDate;
                    bool isNewFile = false; //Jacob 20140122 added
                    
                    //Jacob 20140122 edited
                    //SetPrintFileName(sumIntNo, out pfnIntNo, out actDate);
                    SetPrintFileName(sumIntNo, out pfnIntNo, out actDate, out isNewFile);

                    //Jacob 20140122 edited
                    //if (!this.printQList.ContainsKey(pfnIntNo)) 
                    if (isNewFile)   
                    {

                        //this.printQList.Add(pfnIntNo, this.autCode.Trim(), actDate);  ////Jacob 20140122 removed
                        DateTime printActionDate = DateTime.Now.AddHours(new SysParamService().GetBySpColumnName(SysParamList.DelayActionDate_InHours.ToString()).SpIntegerValue);
                        item = new QueueItem()
                        {
                            Body = pfnIntNo,
                            Group = this.autCode.Trim(),
                            ActDate = this.needDelay && actDate < printActionDate ? printActionDate : actDate,
                            QueueType = ServiceQueueTypeList.PrintCourtRoll
                        };
                        itemList.Add(item);
                    }

                    //set lock
                    item = new QueueItem();
                    item.Body = sumIntNo.ToString();
                    item.QueueType = ServiceQueueTypeList.SummonsLock;
                    item.ActDate = sumCourtDate;
                    item.Group = this.autCode.Trim();
                    itemList.Add(item);

                    //set unlock
                    item = new QueueItem();
                    item.Body = sumIntNo.ToString();
                    item.QueueType = ServiceQueueTypeList.SummonsLock;
                    item.ActDate = sumCourtDate.AddDays(noOfDaysCourtRollCaptured);
                    item.Group = this.autCode.Trim();
                    itemList.Add(item);

                    queProcessor.Send(itemList);

                    scope.Complete();   // 2013-09-12, Oscar moved, Complete() happens only when success
                }
                //scope.Complete();
            }
            return success;
        }

        protected void grdEnterCaseNos_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 2013-09-11, Oscar added for avoiding mutiple clicks.
            RegisterScriptUnblock();

            // 2013-11-27, Oscar add to cancel editing.
            this.grdEnterCaseNos.EditIndex = -1;

            //this is the autofill button on the grid
            //the function is to take the values in the text boxes as defaults and automatically construct the case number = prefix/sequence/suffix
            //the application of the cae number generation should be line-by-line through the grid
            //must check for rows that already have case numbers allocated
            //get the index for the data on the current row
            //get the starting defaults
            string prefix = string.Empty;
            string sequence = string.Empty;
            string suffix = string.Empty;

            if (txtCasePrefix.Text.Trim().Length > 0)
            {
                prefix = txtCasePrefix.Text.Trim();
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text15");
                lblError.Visible = true;
                return;
            }
            if (txtNextCaseNo.Text.Trim().Length > 0)
            {
                //assume case no is 6 digits
                //sequence =  ("000000" + txtNextCaseNo.Text);
                sequence = txtNextCaseNo.Text;

                //test that this is only numeric characters
                if (!Regex.IsMatch(sequence, numericOnly))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text16");
                    lblError.Visible = true;
                    return;
                }
                else if (Convert.ToInt32(sequence) <= 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text17");
                    lblError.Visible = true;
                    return;
                }
                //could be valid
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text18");
                lblError.Visible = true;
                return;
            }

            if (txtCaseSuffix.Text.Trim().Length > 0)
            {
                suffix = txtCaseSuffix.Text;
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text19");
                lblError.Visible = true;
                return;
            }

            //find the current row
            int sumIntNo = (int)this.grdEnterCaseNos.SelectedDataKey["SumIntNo"];
            //ViewState.Add("sumIntNo", sumIntNo);
            int autIntNo = int.Parse(Session["autIntNo"].ToString());
            int crtIntNo = int.Parse(ddlCourt.SelectedValue.ToString());
            int crtRIntNo = int.Parse(ddlCourtRoom.SelectedValue.ToString());

            DataSet autoSummonsDS = (DataSet)ViewState["SummonsForCaseNo"];

            //set up a loop
            bool found = false;
            string fullCaseNumber = string.Empty;
            bool success = false;
            string error = string.Empty;
            bool foundStart = false;

            this.needDelay = true;
            foreach (DataRow row in autoSummonsDS.Tables[0].Rows)
            {
                if (foundStart == false)
                {
                    if (Int32.Parse(row["SumIntNo"].ToString()) == sumIntNo)
                    {
                        //found the row selected by the user to start the autofill with
                        found = true;
                        foundStart = true;
                    }
                }
                if (found)
                {
                    // 2013-09-12, Oscar added, to avoid allocating case number again.
                    if (HasCaseNo(row)) continue;

                    //get current sumIntNo from the row
                    int currentSumIntNo = Int32.Parse(row["SumIntNo"].ToString());

                    //jerry 2011-12-15 get SumCourtDate for push queue
                    sumCourtDate = Convert.ToDateTime(row["SumCourtDate"].ToString());

                    //calculate the case number
                    //Jerry 2013-01-04 change
                    if (caseNoFormatValue == "Y")
                    {
                        fullCaseNumber = "C/T/" + prefix + "/" + sequence.PadLeft(6, '0') + "/" + suffix;
                    }
                    else
                    {
                        fullCaseNumber = prefix + "/" + sequence.PadLeft(6, '0') + "/" + suffix;
                    }

                    //sequence = Convert.ToString(Convert.ToInt32(sequence) + 1);

                    //fire row update
                    //already checks for duplicates/rows with case numbers
                    success = false;
                    success = updateSummonsWithCaseNo(autIntNo, crtIntNo, crtRIntNo, fullCaseNumber, int.Parse(sequence), currentSumIntNo, ref error);
                    if (success)
                    {
                        // 2013-09-12, Oscar moved, adding only happens when success.
                        sequence = Convert.ToString(Convert.ToInt32(sequence) + 1);
                        this.txtNextCaseNo.Text = sequence;

                        //happy for you - do another one
                        //grdEnterCaseNos.EditIndex = -1;
                        //BindEnterCaseNoGrid();

                        //2013-12-16 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtRollManualEntryOfCaseNumbers, PunchAction.Change); 
                    }
                    else
                    {
                        lblError.Text = error;
                        lblError.Visible = true;

                        // 2013-11-27, Oscar added to refresh grid
                        BindEnterCaseNoGrid();

                        return;
                    }//end of success
                }//end of if found
                else
                {
                    //do nothing - row was not selected probably already has a number
                }
            }//end of for-each loop

            // 2013-09-12, Oscar added, the page should stay where it is.
            BindEnterCaseNoGrid();
            if (this.grdEnterCaseNos.Rows.Count <= 0) 
                BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));
            lblError.Text = error;
            lblError.Visible = true;
            return; // 2013-09-12, Oscar added, the page should stay where it is.

            //need to refresh the texbox txtNextCaseNo as the nextSeq will have changed after the update
            grdDates.EditIndex = -1;
            grdDatesPager.CurrentPageIndex = 1;
            grdDatesPager.RecordCount = 0;
            BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));

            lblError.Text = error;
            lblError.Visible = true;

            // is this redundant now
            //grdEnterCaseNos.EditIndex = -1;
            //BindEnterCaseNoGrid();
            ddlCourtRoom.SelectedIndex = 0;
            pnlCourtDates.Visible = false;
            pnlCaseNos.Visible = false;

        }

        protected void grdImpersonSummons_SelectedIndexChanged(object sender, EventArgs e)
        {
            int autIntNo = int.Parse(Session["autIntNo"].ToString());
            int crtIntNo = int.Parse(ddlCourt.SelectedValue.ToString());
            int crtRIntNo = int.Parse(ddlCourtRoom.SelectedValue.ToString());

            int sumIntNo = (int)this.grdImpersonSummons.SelectedDataKey["SumIntNo"];
            Stalberg.TMS.SummonsDB caseNoUpdate = new SummonsDB(this.connectionString);
            string errMessage = string.Empty;
            string error = string.Empty;
            bool success = false;

            int updSumIntNo = caseNoUpdate.NoCaseNoFromManualEntry(autIntNo, crtIntNo, crtRIntNo, this.login, sumIntNo, ref errMessage);

            switch (updSumIntNo)
            {
                case -8:
                    error = (string)GetLocalResourceObject("error") + errMessage;
                    break;
                case -7:
                    error = (string)GetLocalResourceObject("error1") + errMessage;
                    break;
                case -6:
                    error = (string)GetLocalResourceObject("error2") + errMessage;
                    break;
                case -5:
                    error = (string)GetLocalResourceObject("error3") + errMessage;
                    break;
                case -4:
                    error = (string)GetLocalResourceObject("error4") + errMessage;
                    break;
                case -3:
                    error = (string)GetLocalResourceObject("error5") + errMessage;
                    break;
                case -2:
                    error = (string)GetLocalResourceObject("error6") + errMessage;
                    break;
                case -1:
                    error = (string)GetLocalResourceObject("error7") + errMessage;
                    break;
                case 0:
                    error = (string)GetLocalResourceObject("error8")+" \n";
                    success = true;
                    break;
                default:
                    error = (string)GetLocalResourceObject("error9");
                    success = true;
                    break;
            }
            if (!success)
            {
                lblError.Text = error;
                lblError.Visible = true;
                return;
            }

            lblError.Text = error;
            lblError.Visible = true;

            if (updSumIntNo == 0)
            {
                grdDates.EditIndex = -1;
                // 2013-09-12, Oscar removed, it reset wrong page
                //grdDatesPager.CurrentPageIndex = 1;
                //grdDatesPager.RecordCount = 0;
                BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));

                ddlCourtRoom.SelectedIndex = 0;
                pnlCourtDates.Visible = false;
                pnlCaseNos.Visible = false;
            }
            else
            {
                // 2013-09-12, Oscar removed, it reset wrong page
                //grdImpersonSummonsPager.CurrentPageIndex = 1;
                //grdImpersonSummonsPager.RecordCount = 0;
                //grdEnterCaseNosPager.CurrentPageIndex = 1;
                //grdEnterCaseNosPager.RecordCount = 0;
                BindEnterCaseNoGrid();
            }
        }

        protected void grdImpersonSummons_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex > -1)
            {
                Label lblSummonStatus = (Label)e.Row.Cells[5].FindControl("lblSummonStatus");
                if (lblSummonStatus.Text == "651")
                {
                    e.Row.Cells[6].Enabled = false;
                }
            }


        }

        protected void grdDatesPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));
        }

        protected void grdEnterCaseNosPager_PageChanged(object sender, EventArgs e)
        {
            // 2013-09-12, Oscar added, discard editing when page changed.
            this.grdEnterCaseNos.EditIndex = -1;

            BindEnterCaseNoGrid();
        }

        protected void grdImpersonSummonsPager_PageChanged(object sender, EventArgs e)
        {
            BindEnterCaseNoGrid();
        }

        #region 2013-09-11, Oscar added for avoiding mutiple clicks.

        protected void grdEnterCaseNos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CheckAutoFill(e);
        }

        protected void grdEnterCaseNos_RowCreated(object sender, GridViewRowEventArgs e)
        {
            CheckUpdate(e);
        }

        void RegisterScriptUnblock()
        {
            ScriptManager.RegisterStartupScript(this.UpdatePanel1, GetType(), "UpdatePanel1", @"$.unblockUI();", true);
        }

        const string jsBlockProcessing = "LoadingBlocker('Processing...');",
                     jsBlockLoading = "LoadingBlocker('Loading...');",
                     cmdSelect = "Select",
                     cmdEdit = "Edit",
                     cmdUpdate = "Update";

        void CheckAutoFill(GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;

            var hasCaseNo = false;

            var ctrlNumber = e.Row.Cells[4].Controls;
            if (ctrlNumber.Count == 3)
            {
                var lblNumber = ctrlNumber[1] as Label;
                if (lblNumber != null
                    && !string.IsNullOrWhiteSpace(lblNumber.Text))
                    hasCaseNo = true;
            }

            var ctrlFill = e.Row.Cells[6].Controls;
            if (ctrlFill.Count == 1)
            {
                var lnkFill = ctrlFill[0] as LinkButton;
                if (lnkFill != null
                    && lnkFill.CommandName.Equals(cmdSelect, StringComparison.OrdinalIgnoreCase))
                {
                    if (hasCaseNo)
                        lnkFill.Enabled = false;
                    else
                        lnkFill.OnClientClick = jsBlockProcessing;
                }
            }

            var ctrlEdit = e.Row.Cells[5].Controls;
            if (ctrlEdit.Count == 1)
            {
                var lnkEdit = ctrlEdit[0] as LinkButton;
                if (lnkEdit != null
                    && lnkEdit.CommandName.Equals(cmdEdit, StringComparison.OrdinalIgnoreCase)
                    && hasCaseNo)
                {
                    lnkEdit.Enabled = false;
                }
            }
        }

        void CheckUpdate(GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;

            var ctrlUpdate = e.Row.Cells[5].Controls;
            if (ctrlUpdate.Count == 3)
            {
                var lnkUpdate = ctrlUpdate[0] as LinkButton;
                if (lnkUpdate != null
                    && lnkUpdate.CommandName.Equals(cmdUpdate, StringComparison.OrdinalIgnoreCase))
                {
                    lnkUpdate.OnClientClick = jsBlockProcessing;
                }
            }
        }

        bool HasCaseNo(DataRow row)
        {
            var caseNo = Convert.ToString(row["SumCaseNo"]);
            return !string.IsNullOrWhiteSpace(caseNo);
        }

        #endregion

        // 2013-09-13, Oscar added loading for selecting court date
        protected void grdDates_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;

            var ctrlUpdate = e.Row.Cells[3].Controls;
            if (ctrlUpdate.Count == 1)
            {
                var lnkUpdate = ctrlUpdate[0] as LinkButton;
                if (lnkUpdate != null
                    && lnkUpdate.CommandName.Equals(cmdSelect, StringComparison.OrdinalIgnoreCase))
                {
                    lnkUpdate.OnClientClick = jsBlockLoading;
                }
            }
        }

    }
}


