using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.DAL.Data;
using System.Transactions;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class CourtRoom : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = string.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "CourtRoom.aspx";
        //protected string thisPage = "Court Room Maintenance";

        private const string DATE_FORMAT = "yyyy-MM-dd";


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            lblError.Text = string.Empty;

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateCourts();
                this.PopulateCourtRoomStatusFlag();//2013-09-23 Heidi added for lookup court room status flag(5102)
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
                this.ucLanguageLookupUpdate.DataBind(entityList);
            }

           
        }

        private void PopulateCourts()
        {
            CourtDB court = new CourtDB(this.connectionString);

            //mrs 20080909 courts are not actually authority dependent
            //SqlDataReader reader = court.GetAuth_CourtListByAuth (this.autIntNo);
            SqlDataReader reader = court.GetCourtList();
            this.ddlCourt.DataSource = reader;
            this.ddlCourt.DataValueField = "CrtIntNo";
            this.ddlCourt.DataTextField = "CrtDetails";
            this.ddlCourt.DataBind();
            reader.Close();
            this.ddlCourt.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourt.Items"), string.Empty));
            
        }

        private void PopulateCourtRoomStatusFlag()
        {
            //2013-09-23 Heidi added for lookup court room status flag(5102)
            GetCrtRStatusFlagList(ddlCrtRStatusFlag);
        }

        private void PopulateCourtRooms(int nCrtIntNo)
        {
            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            
            SqlDataReader reader = room.GetCourtRoomList (nCrtIntNo);
            this.ddlCourtRoom.DataSource = reader;
            this.ddlCourtRoom.DataValueField = "CrtRIntNo";
            this.ddlCourtRoom.DataTextField = "CrtRoomDetails";
            this.ddlCourtRoom.DataBind();
            this.ddlCourtRoom.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourtRoom.Items"), "0"));
            reader.Close();
        }

        private void PopulateData(int nCrtIRntNo)
        {
            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            SqlDataReader reader = room.GetCourtRoomDetails(nCrtIRntNo);
            while (reader .Read()) 
            {
                txtRoomName .Text = Helper.GetReaderValue<string>(reader,"CrtRoomName");
                txtCasePrefix .Text = Helper.GetReaderValue<string>(reader,"CrtRPrefix");
                txtNextCaseNo .Text = Helper .GetReaderValue <string>(reader,"CrtRNextCaseNo");
                txtNextWOANo.Text = Helper .GetReaderValue <string>(reader,"CrtRNextWOANo");
                txtPresidingOfficer .Text = Helper .GetReaderValue <string>(reader,"CrtRPresidingOfficer");
                txtPublicProsecutor .Text = Helper .GetReaderValue <string>(reader,"CrtRPublicProsecutor");
                txtClerk .Text = Helper .GetReaderValue <string>(reader,"CrtRClerkOfTheCourt");
                txtInterpreter .Text = Helper .GetReaderValue <string>(reader,"CrtRInterpreter");
                chkActive.Checked = Helper .GetReaderValue <string>(reader,"CrtRActive") == "Y" ? true : false;
                //2013-09-23 Heidi added for lookup court room status flag(5102)
                ddlCrtRStatusFlag.SelectedValue = Helper.GetReaderValue<string>(reader, "CrtRStatusFlag");
            }
            reader.Close();
        }

        private void GetCrtRStatusFlagList(DropDownList ddlCrtRStatusFlag)
        {
            //2013-09-23 Heidi added for lookup court room status flag(5102)
            ddlCrtRStatusFlag.Items.Clear();
            ddlCrtRStatusFlag.Items.Add(new ListItem((string)GetLocalResourceObject("ddlCrtRStatusFlag.Items"), "0"));
            ddlCrtRStatusFlag.Items.Add(new ListItem((string)GetLocalResourceObject("ddlCrtRStatusFlag.ItemC"), "C"));
            ddlCrtRStatusFlag.Items.Add(new ListItem((string)GetLocalResourceObject("ddlCrtRStatusFlag.ItemP"), "P"));
            ddlCrtRStatusFlag.Items.Add(new ListItem((string)GetLocalResourceObject("ddlCrtRStatusFlag.ItemR"), "R"));
        }
         


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            this.lblError.Text = string.Empty;
            CourtRoomDB court = new CourtRoomDB(connectionString);

            if (btnUpdate.Text == (string)GetLocalResourceObject("btnUpdate.Text"))
            {
                int nRef = -1;

                if (ddlCourtRoom.SelectedIndex > 0)
                {
                    nRef = Convert.ToInt32(ddlCourtRoom.SelectedValue);
                }
                else
                {
                    //Modified by Henry 2012-3-9
                    //Mulity language error message lblError.Text1 - lblError.Text13
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                    lblError.Visible = true;
                    return;
                }

                if (txtCasePrefix.Text.Trim().Length < 1)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    lblError.Visible = true;
                    return;
                }

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();//2012-12-26 updated by Nancy for  detect whether translation is null
                if (lgEntityList.Count <= 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text17");
                    lblError.Visible = true;
                    return;
                }

                //2013-09-23 Heidi added
                if (ddlCrtRStatusFlag.SelectedIndex == 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text18");
                    lblError.Visible = true;
                    return;
                }

                //2013-09-23 Heidi added
                if (chkActive.Checked)
                {
                    if (ddlCrtRStatusFlag.SelectedValue == "P" || ddlCrtRStatusFlag.SelectedValue == "R")
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text19");
                        lblError.Visible = true;
                        return;
                    }

                }
                else
                {
                    if (ddlCrtRStatusFlag.SelectedValue == "C")
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text20");
                        lblError.Visible = true;
                        return;
                    }
                }

                if (!chkActive.Checked)
                {
                    CourtDatesQuery query = new CourtDatesQuery();
                    query.Append(CourtDatesColumn.CrtRintNo, nRef.ToString());
                    query.AppendGreaterThan(CourtDatesColumn.Cdate, DateTime.Now.ToString("yyyy-MM-dd"));
                    TList<SIL.AARTO.DAL.Entities.CourtDates> list = new CourtDatesService().Find(query as IFilterParameterCollection);

                    if (list != null && list.Count > 0)
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.LocateCourtDate");
                        lblError.Visible = true;
                        return;
                    }
                }
                else
                {
                    CourtRoomQuery query = new CourtRoomQuery();
                    query.Append(CourtRoomColumn.CrtIntNo, ddlCourt.SelectedValue);
                    query.AppendNotEquals(CourtRoomColumn.CrtRintNo, ddlCourtRoom.SelectedValue);
                    query.Append(CourtRoomColumn.CrtRactive, "Y");

                    TList<SIL.AARTO.DAL.Entities.CourtRoom> list = new CourtRoomService().Find(query as IFilterParameterCollection);

                    if (list != null && list.Count > 0)
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.MulActiveRoom");
                        lblError.Visible = true;
                        return;
                    }
                }

                int nNextCase = 0;
                int nNextWOA = 0;

                Int32.TryParse(txtNextCaseNo.Text, out nNextCase);
                Int32.TryParse(txtNextWOANo.Text, out nNextWOA);

                int nRes=0;
                //Jerry 2013-10-31 change it
                //using (ConnectionScope.CreateTransaction())
                using (TransactionScope scope = new TransactionScope())
                {//2012-12-26 ConnectionScope added by Nancy for multi-table operation
                    
                    // Heidi 2013-09-23 added CrtRStatusFlag parameter for the courtroom is active, inactive or no longer used(5102)
                    nRes = court.UpdateCourtRoom(nRef, txtRoomName.Text , txtCasePrefix.Text.Trim().ToUpper(), nNextCase , nNextWOA,
                        txtPresidingOfficer.Text, txtPublicProsecutor.Text, txtClerk.Text, txtInterpreter.Text, chkActive.Checked == true ? "Y" : "N", login,ddlCrtRStatusFlag.SelectedValue);

                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                    rUMethod.UpdateIntoLookup(nRes, lgEntityList, "CourtRoomLookup", "CrtRIntNo", "CrtRoomName", this.login);

                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtRoomMaintenance, PunchAction.Change);  

                    scope.Complete();
                }
           
                //SD: 20081205 Null value test for Next Case No
                StringBuilder sb = new StringBuilder();
                sb.Append((string)GetLocalResourceObject("lblError.Text14") + "\n<ul>\n");
                if (txtNextCaseNo.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text15") + "</li>");
                    sb.Append("</ul>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }

                //SD: 20081205 Null value test for Next WOANo
                if (txtNextWOANo.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text16") + "</li>");
                    sb.Append("</ul>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }


                if (nRes != nRef)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                }
                else
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
             
            }
            else if (btnUpdate.Text == (string)GetLocalResourceObject("btnOptAdd.Text"))
            {
                if (ddlCourt.SelectedIndex == 0) 
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                    lblError.Visible = true;
                    return;
                }
                if (txtCasePrefix.Text.Trim().Length < 1)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    lblError.Visible = true;
                    return;
                }
                if (txtRoomNo.Text.Length < 1)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text7"); 
                    lblError.Visible = true;
                    return;
                }

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();//2012-12-26 updated by Nancy for  detect whether translation is null
                if (lgEntityList.Count <= 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text17");
                    lblError.Visible = true;
                    return;
                }

                //2013-09-23 Heidi added
                if (ddlCrtRStatusFlag.SelectedIndex == 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text18");
                    lblError.Visible = true;
                    return;
                }

                if (txtNextCaseNo.Text.Length < 1)
                    txtNextCaseNo.Text = "1";
                if (txtNextWOANo.Text.Length < 1)
                    txtNextWOANo.Text = "1";

                if (chkActive.Checked)
                {
                    CourtRoomQuery query = new CourtRoomQuery();
                    query.Append(CourtRoomColumn.CrtIntNo, ddlCourt.SelectedValue);
                    query.Append(CourtRoomColumn.CrtRactive, "Y");

                    TList<SIL.AARTO.DAL.Entities.CourtRoom> list = new CourtRoomService().Find(query as IFilterParameterCollection);

                    if (list != null && list.Count > 0)
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.MulActiveRoom");
                        lblError.Visible = true;
                        return;
                    }
                }

                //2013-09-23 Heidi added
                if (chkActive.Checked)
                {
                    if (ddlCrtRStatusFlag.SelectedValue == "P" || ddlCrtRStatusFlag.SelectedValue == "R")
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text19");
                        lblError.Visible = true;
                        return;
                    }
                    
                }
                else
                {
                    if (ddlCrtRStatusFlag.SelectedValue == "C")
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text20");
                        lblError.Visible = true;
                        return;
                    }
                }

                int nNextCase = 0;
                int nNextWOA = 0;

                Int32.TryParse(txtNextCaseNo.Text, out nNextCase);
                Int32.TryParse(txtNextWOANo.Text, out nNextWOA);

                int nRes = 0;
                //Jerry 2013-10-31 change it
                //using (ConnectionScope.CreateTransaction())
                using (TransactionScope scope = new TransactionScope())
                {//2012-12-26 ConnectionScope added by Nancy for multi-table operation
                   // Heidi 2013-09-23 added CrtRStatusFlag parameter for the courtroom is active, inactive or no longer used(5102)
                    nRes = court.AddCourtRoom(Convert.ToInt32(ddlCourt.SelectedValue), txtRoomNo.Text.Trim().ToUpper(), txtRoomName.Text, txtCasePrefix.Text.Trim().ToUpper(), nNextCase, nNextWOA,
                        txtPresidingOfficer.Text, txtPublicProsecutor.Text, txtClerk.Text, txtInterpreter.Text, chkActive.Checked == true ? "Y" : "N", login,ddlCrtRStatusFlag.SelectedValue);



                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                    rUMethod.UpdateIntoLookup(nRes, lgEntityList, "CourtRoomLookup", "CrtRIntNo", "CrtRoomName", this.login);
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtRoomMaintenance, PunchAction.Add); 
                    scope.Complete();
                }

                //SD: 20081205 Null value test for Next CaseNo
               
                StringBuilder sb = new StringBuilder();
                if (txtNextCaseNo.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text15") + "</li>");
                    sb.Append("</ul>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }

                //SD: 20081205 Null value test for Next WOANo
                if (txtNextWOANo.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text16") + "</li>");
                    sb.Append("</ul>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }



                if (nRes <= 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text8"); 
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text9"); 
                    lblCourtRoom.Visible = true;
                    ddlCourtRoom.Visible = true;
                    lblRoomNo.Visible = false;
                    txtRoomNo .Visible = false;
                    regValTxtRoomNo.Visible = false;
                    PopulateCourts();
                    PopulateCourtRooms(0);
                    //2013-09-23 Heidi added
                    PopulateCourtRoomStatusFlag();
                }
         
            }
            else if (btnUpdate.Text == (string)GetLocalResourceObject("btnOptDelete.Text"))
            {
                if (ddlCourt.SelectedIndex == 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                    lblError.Visible = true;
                    return;
                }
                if (ddlCourtRoom.SelectedIndex == 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text11"); 
                    lblError.Visible = true;
                    return;
                }

                int referencedCount = court.GetIfIdReferencedByOtherTable(Convert.ToInt32(ddlCourtRoom.SelectedValue));
                if (referencedCount > 0)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblErrorReferenced.Text");
                }
                else
                {
                    int nRes = 0;
                    //Jerry 2013-10-31 change it
                    //using (ConnectionScope.CreateTransaction())
                    using(TransactionScope scope = new TransactionScope())
                    {//2012-12-26 ConnectionScope added by Nancy for multi-table operation
                        List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                        SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                        rUMethod.DeleteLookup(lgEntityList, "CourtRoomLookup", "CrtRIntNo");

                        nRes = court.DeleteCourtRoom(Convert.ToInt32(ddlCourtRoom.SelectedValue));
                       
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtRoomMaintenance, PunchAction.Delete); 
                        scope.Complete();
                    }

                    if (nRes <= 0)
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                    }
                    else
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                        PopulateCourts();
                        PopulateCourtRooms(0);
                        //2013-09-23 Heidi added
                        PopulateCourtRoomStatusFlag();
                    }
                }
            }
            ddlCourt.SelectedIndex = 0;
            ddlCourtRoom.Items.Clear();
            ddlCrtRStatusFlag.SelectedIndex = 0;//2013-09-23 Heidi added
            ClearForm((string)GetLocalResourceObject("btnUpdate.Text"));
            lblError.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            lblCourtRoom.Visible = true;
            ddlCourtRoom.Visible = true;
            lblRoomNo.Visible = false;
            txtRoomNo.Visible = false;
            regValTxtRoomNo.Visible = false;
            ClearForm((string)GetLocalResourceObject("btnUpdate.Text"));
            ddlCourt.SelectedIndex = 0;
            ddlCourtRoom.Items.Clear();
            ddlCrtRStatusFlag.SelectedIndex = 0;//2013-09-23 Heidi added
        }

        protected void ddlCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourt.SelectedIndex > 0)
            {
                PopulateCourtRooms(Convert.ToInt32(ddlCourt.SelectedValue));
            }
        }

      
        protected void ddlCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourtRoom.SelectedIndex > 0)
            {
                PopulateData(Convert.ToInt32(ddlCourtRoom.SelectedValue));

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(ddlCourtRoom.SelectedValue.ToString(), "CourtRoomLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

            }
        }

        protected void btnOptAdd_Click(object sender, EventArgs e)
        {
            lblCourtRoom.Visible = false;
            ddlCourtRoom.Visible = false;
            lblRoomNo.Visible = true;
            txtRoomNo.Visible = true;
            regValTxtRoomNo.Visible = true;
            ddlCourt.SelectedIndex = 0;
            ddlCourtRoom.Items.Clear();
            ddlCrtRStatusFlag.SelectedIndex = 0; //2013-09-23 Heidi added
            ClearForm((string)GetLocalResourceObject("btnOptAdd.Text"));

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupUpdate.DataBind(entityList);
        }

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            ClearForm((string)GetLocalResourceObject("btnOptDelete.Text"));
            ddlCourt.SelectedIndex = 0;
            ddlCourtRoom.Items.Clear();
            ddlCrtRStatusFlag.SelectedIndex = 0; //2013-09-23 Heidi added
        }

        public void ClearForm(string sType)
        {
            txtRoomNo.Text = string.Empty;
            txtRoomName.Text = string.Empty;
            txtCasePrefix.Text = string.Empty;
            txtNextCaseNo.Text = string.Empty;
            txtNextWOANo.Text = string.Empty;
            txtPresidingOfficer.Text = string.Empty;
            txtPublicProsecutor.Text = string.Empty;
            txtClerk.Text = string.Empty;
            txtInterpreter.Text = string.Empty;
            chkActive.Checked = false;
            btnUpdate.Text = sType;
        }
    }
}


