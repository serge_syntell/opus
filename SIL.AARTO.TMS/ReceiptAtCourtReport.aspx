﻿<%@ Page Language="C#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ReceiptsAtCourtReport"  Codebehind="ReceiptAtCourtReport.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat=server>
    <title>
        <%=title%>
    </title>
    <%--<link href="<%= styleSheet %>" type="text/css" rel="stylesheet">--%>
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />

</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table height="85%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center">
                <img height="26" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                 
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" width="100%" colspan="1">
                <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                    <p style="text-align: center;">
                        <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                    <p>
                        &nbsp;</p>
                </asp:Panel>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                            <table id="tblControls" border="0" class="NormalBold" style="width: 554px">
                                <tr>
                                    <td style="width: 147px">
                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblCourtDate.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="dtpCourtDate" runat="server" autocomplete="off" 
                                            AutoPostBack="True" CssClass="Normal" Height="20px" 
                                             UseSubmitBehavior="False" />
                                        <cc1:CalendarExtender ID="dtpCourtDate_CalendarExtender" runat="server" 
                                            Format="yyyy-MM-dd" TargetControlID="dtpCourtDate">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td style="width: 3px" align="center">
                                    </td>
                                    <td rowspan="8" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 147px">
                                        <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblBetween.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="dtpStartDate" runat="server" autocomplete="off" AutoPostBack="True"
                                            CssClass="Normal" Height="20px" OnTextChanged="dtpStartDate_TextChanged" UseSubmitBehavior="False" />
                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" Format="yyyy-MM-dd" TargetControlID="dtpStartDate">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td align="center" style="width: 3px">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 147px">
                                        <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblAnd.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="dtpEndDate" runat="server" autocomplete="off" CssClass="Normal"
                                            Height="20px" UseSubmitBehavior="False" />
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="yyyy-MM-dd" TargetControlID="dtpEndDate">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td align="center" style="width: 3px">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 147px">
                                        <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblCourt.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboCourt" runat="server" AutoPostBack="true" CssClass="Normal"
                                             Width="217px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center" style="width: 3px">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 147px">
                                        <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblOrderBy.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlOrderBy" runat="server" CssClass="Normal" AutoPostBack="True"
                                            Width="197px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td style="text-align: left">
                                        <asp:Button ID="btnView" runat="server" CssClass="NormalButton" 
                                            Text="<%$Resources:btnView.Text %>" onclick="btnView_Click" />
                                    </td>
                                    <td style="width: 3px; height: 26px;" align="center">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" valign="top">
                                        <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
            </td>
            <td valign="top" align="left" width="100%">
            </td>
        </tr>
    </table>
    <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
