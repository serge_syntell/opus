﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="MetroTransaction" Codebehind="MetroTransaction.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= _title %>
    </title>
    <link href="<%= _styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= _description %>" name="Description">
    <meta content="<%= _keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=_background %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
        <tr>
            <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" height="85%">
        <tr>
            <td align="center" valign="top">
                <img style="height: 1px" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center">
                                <asp:Label ID="Label1" runat="server" Text="<%$Resources:lblOptions.Text %>" Width="118px" CssClass="ProductListHead"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                    OnClick="btnOptAdd_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptDelete.Text %>"
                                    OnClick="btnOptDelete_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptHide.Text %>"
                                     OnClick="btnOptHide_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" height="21">
                                <asp:Button ID="btnHideMenu" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnHideMenu.Text %>"
                                    OnClick="btnHideMenu_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" width="100%" colspan="1">
                <table border="0" width="568" height="482">
                    <tr>
                        <td valign="top" height="47">
                            <p align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="626px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                            <p>
                                <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="pnlGeneral" runat="server">
                                <table id="Table1" cellspacing="1" cellpadding="1" width="300" border="0">
                                    <tr>
                                        <td width="162">
                                        </td>
                                        <td width="7">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="162">
                                            <asp:Label ID="lblSelMetro" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblSelMetro.Text %>"></asp:Label>
                                        </td>
                                        <td width="7">
                                            <asp:DropDownList ID="ddlSelectLA" runat="server" Width="300px" CssClass="Normal"
                                                AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="162">
                                        </td>
                                        <td width="7">
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:DataGrid ID="dgMetroTran" Width="495px" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgMetroTran_ItemCommand"
                                OnPageIndexChanged="dgMetroTran_PageIndexChanged">
                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="MTIntNo" HeaderText="TNIntNo"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="MetroIntNo" HeaderText="AutIntNo"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TTCode" HeaderText="<%$Resources:dgMetroTran.TTCode.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="TTDescription" HeaderText="<%$Resources:dgMetroTran.TTDescription.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MTNumber" HeaderText="<%$Resources:dgMetroTran.MTNumber.HeaderText %>"></asp:BoundColumn>
                                    <asp:ButtonColumn Text="<%$Resources:dgMetroTran.Command.Text%>" CommandName="Select"></asp:ButtonColumn>
                                </Columns>
                                <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                            </asp:DataGrid>
                            <asp:Panel ID="pnlAddMetroTran" runat="server" Height="127px">
                                <table id="Table2" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                    <tr>
                                        <td height="2">
                                            <asp:Label ID="lblAddTrans" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddTrans.Text %>"></asp:Label>
                                        </td>
                                        <td width="248" height="2">
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" height="25">
                                            <asp:Label ID="lblAddTNType" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddTNType.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="248" height="25">                                            
                                            <asp:DropDownList ID="ddlAddSelectTT" runat="server" Width="217px" CssClass="Normal"></asp:DropDownList>
                                        </td>
                                        <td height="25">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>
                                                <asp:Label ID="lblAddMTNumber" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddMTNumber.Text %>"></asp:Label></p>
                                        </td>
                                        <td width="248">
                                            <asp:TextBox ID="txtAddMTNumber" runat="server" Width="62px" CssClass="Normal" MaxLength="10"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="txtAddMTNumber" ErrorMessage=" <%$Resources:reqAddMTNumber.ErrorMsg %>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                ControlToValidate="txtAddMTNumber" ErrorMessage="<%$Resources:regAddMTNumber.ErrorMsg %>" 
                                                ValidationExpression="[0-9]*"></asp:RegularExpressionValidator>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="revNumber" runat="server" ForeColor=" " ControlToValidate="txtAddMTNumber"
                                                ErrorMessage="<%$Resources:revNumber.ErrorMessage %>" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td width="248">
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddMetroTran" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddMetroTran.Text %>"
                                                OnClick="btnAddMetroTran_Click"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlEditMetroTran" runat="server" Height="127px">
                                <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                    <tr>
                                        <td height="2">
                                            <asp:Label ID="lblEditMetroTran" runat="server" Text="<%$Resources:lblEditMetroTran.Text %>" CssClass="ProductListHead"></asp:Label>
                                        </td>
                                        <td width="248" height="2">
                                        </td>
                                        <td height="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" height="25">
                                            <asp:Label ID="lblTNType" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTNType.Text %>"></asp:Label>
                                        </td>
                                        <td valign="top" width="248" height="25">                                           
                                            <asp:DropDownList ID="ddlSelectTT" runat="server" Width="217px" CssClass="Normal"></asp:DropDownList>
                                        </td>
                                        <td height="25">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>
                                                <asp:Label ID="lblTNumber" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblTNumber.Text %>"></asp:Label></p>
                                        </td>
                                        <td width="248">
                                            <asp:TextBox ID="txtMTNumber" runat="server" Width="62px" CssClass="Normal" MaxLength="10"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                ControlToValidate="txtMTNumber" ErrorMessage="<%$Resources:reqAddMTNumber.ErrorMsg %>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                ControlToValidate="txtMTNumber" ErrorMessage="<%$Resources:regAddMTNumber.ErrorMsg %>" 
                                                ValidationExpression="[0-9]*"></asp:RegularExpressionValidator>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="revTNumber" runat="server" ForeColor=" " ControlToValidate="txtMTNumber"
                                                ErrorMessage="<%$Resources:revTNumber.ErrorMessage %>" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td width="248">
                                        </td>
                                        <td>
                                            <asp:Button ID="btnUpdateMetroTran" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdateMetroTran.Text %>"
                                                OnClick="btnUpdateMetroTran_Click"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
