using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
//using BarcodeNETWorkShop;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF.Merger;
using System.Drawing;
using System.Collections.Generic;
using Stalberg.TMS.Data.Util;
using System.Globalization;

namespace Stalberg.TMS.Reports
{
    /// <summary>
    /// Represents a page that crates a PDF of a cash receipt note.
    /// </summary>
    public partial class ReceiptNoteViewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private string loginUser;
        //private ReportDocument _reportDoc = new ReportDocument();
        private int _autIntNo;
        //private BarcodeNETImage _barCode;
        System.Drawing.Image barCodeImage = null;
        System.Drawing.Image logoImage = null;
        private ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder _phBarCode;
        private ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder _logo;
        //private const string THIS_PAGE = "Receipt Note Viewer";


        byte[] mainImage;
        //private string thisPage = "Receipt Note Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "ReceiptNoteViewer.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            string autCode = GetAuthCode();
            _autIntNo = Convert.ToInt32(Session["autIntNo"]);

            AuthReportNameDB arn = new AuthReportNameDB(connectionString);

            string reportPage = arn.GetAuthReportName(_autIntNo, "Receipt");
            string sTemplate = arn.GetAuthReportNameTemplate(this._autIntNo, "Receipt");
            if (reportPage.Equals(""))
            {
                reportPage = "ReceiptNote.dplx";
                int arnIntNo = arn.AddAuthReportName(_autIntNo, reportPage, "Receipt", "system", string.Empty);
            }

            string path = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("THIS_PAGE"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            if (!sTemplate.Equals(""))
            {
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("THIS_PAGE"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************
            byte[] buffer = null;
            string sPaymentMethod = string.Empty;

            DocumentLayout doc = new DocumentLayout(path);

            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            ParameterDictionary parameters = new ParameterDictionary();

            //ceTe.DynamicPDF.ReportWriter.ReportElements.Image logo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Image)doc.GetElementById("logo");

            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblNoticeNumber = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblNoticeNumber");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblDate");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFineAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAmount");

            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPaymentMethod = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPaymentMethod");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReceipt = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReceipt");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReceived = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReceived");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAddress");
            _phBarCode = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phBarCode");
            _phBarCode.LaidOut += new PlaceHolderLaidOutEventHandler(ph_BarCode);

            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblName");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblContactNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblContactNo");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFilmNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFilmNo");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblMtrAfi = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblMtrAfi");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblMtrEng = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblMtrEng");

            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblVote = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblVote");
           
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCashier = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCashier");
            ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phImage = null;

            SqlConnection con = new SqlConnection(this.connectionString);
            SqlDataReader result = null;
            SqlCommand com = null;

            if (Request.QueryString["receipt"] != null)
            {
                con = new SqlConnection(this.connectionString);
                com = new SqlCommand("ReceiptNotePrint", con);
                com.CommandType = CommandType.StoredProcedure;
                com.CommandTimeout = 0;

                string receipts = Request.QueryString["receipt"].ToString(); //this should be rctintno

                //LMZ 13-04-2007 - added for batch reporting
                if (receipts.Length == 0)
                {
                    String sDate = Request.QueryString["date"] ?? DateTime.Now.ToString("yyyy-MM-dd");

                    DateTime date;
                    if (!DateTime.TryParse(sDate, out date))
                        sDate = DateTime.Now.ToString("yyyy-MM-dd");

                    com.Parameters.Add("@InDate", SqlDbType.SmallDateTime).Value = date;
                    //com.Parameters.Add("@Authority", SqlDbType.Int, 4).Value = _autIntNo;
                }

                else
                {
                    com.Parameters.Add("@Receipts", SqlDbType.VarChar, 100).Value = receipts;
                }

                com.Parameters.Add("@Authority", SqlDbType.Int, 4).Value = _autIntNo;

                Document report = null;
                //get data and populate result set
                try
                {
                    con.Open();
                    result = com.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (Exception ee)
                {
                    string sError = ee.Message;
                }
                try
                {
                    MergeDocument merge = new MergeDocument();
                    while (result.Read())
                    {
                        try
                        {
                            if (reportPage.IndexOf("_PL") > -1)
                            {
                                sPaymentMethod = sPaymentMethod.ToUpper();
                                lblFilmNo.Text = result["NotFilmNo"].ToString();
                                lblName.Text = result["ReceivedFrom"].ToString();
                                lblContactNo.Text = result["ContactNumber"].ToString();

                                lblMtrAfi.Text = result["MtrReceiptEnglishAct"].ToString().Trim();
                                lblMtrEng.Text = result["MtrReceiptAfrikaansAct"].ToString().Trim();
                            }
                            if (reportPage.IndexOf("_SW") > -1)
                            {
                                lblCashier.Text = result["RctUser"].ToString();
                                lblVote.Text = result["AccountNo"].ToString();

                                phImage = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("logo");
                                phImage.LaidOut += new PlaceHolderLaidOutEventHandler(ph_Image);
                                //_logo = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("logo");
                                //_logo.LaidOut += new PlaceHolderLaidOutEventHandler(logo_image);

                                mainImage = (byte[])result["MtrLogo"];
                            }

                            lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim();

                            //Jake 2011-02-25 Removed BarcodeNetImage
                            //_barCode = new BarcodeNETImage();
                            ////barcode.BarcodeText = result["NotTicketNo"].ToString();
                            //_barCode.BarcodeText = result["RctNumber"].ToString() + ":" + result["RctIntNo"].ToString();
                            //_barCode.ShowBarcodeText = false;
                            barCodeImage = Code128Rendering.MakeBarcodeImage(result["RctNumber"].ToString() + ":" + result["RctIntNo"].ToString(), 1, 25, true);

                            //using (MemoryStream stream = new MemoryStream((byte[])result["MtrLogo"],true))
                            //{
                            //    System.Drawing.Image Bitimage = null;
                            //    byte[] bytes = (byte[])result["MtrLogo"];
                            //    stream.Write(bytes, 0, bytes.Length);
                            //    Bitimage = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(stream);
                            //    logoImage = Bitimage;
                            //    stream.Close();
                            //    Bitimage.Dispose();
                            //}



                            lblDate.Text = string.Format("{0:d MMMM yyyy}", result["RctDate"]);
                            lblReceipt.Text = result["RctNumber"].ToString();

                            //update by Rachel 20140811 for 5377
                            //lblFineAmount.Text = string.Format((string)GetLocalResourceObject("lblFineAmount.Text"), string.Format("{0:0.00}", Convert.ToDecimal(result["RTAmount"]))); //(result["RTAmount"].ToString());
                            lblFineAmount.Text = string.Format((string)GetLocalResourceObject("lblFineAmount.Text"), string.Format(CultureInfo.InvariantCulture,"{0:0.00}",result["RTAmount"])); //(result["RTAmount"].ToString());
                            //end update by Rachel 20140811 for 5377
                            lblAddress.Text = result["Address1"].ToString() + "\n"
                                + result["Address2"].ToString() + "\n"
                                + result["Address3"].ToString() + "\n"
                                + result["Address4"].ToString() + "\n"
                                + result["AddressCode"].ToString();

                            //if (result["CashType"].ToString().Equals("A"))
                            //    sPaymentMethod = "Cash";
                            //if (result["CashType"].ToString().Equals("Q"))
                            //    sPaymentMethod = "Cheque";
                            //if (result["CashType"].ToString().Equals("D"))
                            //    sPaymentMethod = "Credit Card";
                            //if (result["CashType"].ToString().Equals("P"))
                            //    sPaymentMethod = "Postal Order";
                            //if (result["CashType"].ToString().Equals("E"))
                            //    sPaymentMethod = "Electronic Payment";
                            //if (result["CashType"].ToString().Equals(""))
                            //    sPaymentMethod = "Unknown";

                            switch (result["CashType"].ToString())
                            {
                                case "A":
                                    sPaymentMethod = "Cash/Kontant";
                                    break;
                                case "Q":
                                    sPaymentMethod = "Cheque/Tjek";
                                    break;
                                case "D":
                                    sPaymentMethod = "Credit Card/Kredietkaart";
                                    break;

                                case "P":
                                    sPaymentMethod = "Postal Order/Posorder";
                                    break;
                                case "B":
                                    sPaymentMethod = "Debit Card/Debietkaart";
                                    break;
                                case "T":
                                    sPaymentMethod = "Direct Deposit/Direkte Deposito";
                                    break;
                                case "H":
                                    sPaymentMethod = "Cash/Kontant";
                                    break;
                                case "U":
                                    sPaymentMethod = "Cheque/Tjek";
                                    break;
                                case "R":
                                    sPaymentMethod = "Card/Kaart";
                                    break;
                                case "E":
                                    sPaymentMethod = "Electronic Payment";
                                    break;
                                case "Z":
                                    sPaymentMethod = "Admin Receipt";
                                    break;
                                case "O":
                                    sPaymentMethod = "Other";
                                    break;
                                default:
                                    sPaymentMethod = "Unknown";
                                    break;
                            }

                            lblReceived.Text = string.Format((string)GetLocalResourceObject("lblFineAmount.Text"), result["Total"].ToString());


                            lblPaymentMethod.Text = sPaymentMethod;
                            report = doc.Run(parameters);
                            ImportedPageArea importedPage;
                            byte[] bufferTemplate;
                            if (!sTemplate.Equals(""))
                            {
                                if (sTemplate.ToLower().IndexOf(".dplx") > 0)
                                {
                                    DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
                                    StoredProcedureQuery queryTemplate = (StoredProcedureQuery)template.GetQueryById("Query");
                                    queryTemplate.ConnectionString = this.connectionString;
                                    ParameterDictionary parametersTemplate = new ParameterDictionary();
                                    Document reportTemplate = template.Run(parametersTemplate);
                                    bufferTemplate = reportTemplate.Draw();
                                    PdfDocument pdf = new PdfDocument(bufferTemplate);
                                    PdfPage page = pdf.Pages[0];
                                    importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
                                }
                                else
                                {
                                    //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                    importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
                                }
                                ceTe.DynamicPDF.Page rptPage = report.Pages[0];
                                rptPage.Elements.Insert(0, importedPage);
                            }
                            buffer = report.Draw();
                            merge.Append(new PdfDocument(buffer));

                            buffer = null;
                            bufferTemplate = null;
                        }

                        catch (Exception ex)
                        {
                            String sError = ex.Message;
                        }

                    }
                    result.Close();
                    con.Dispose();
                    if (merge.Pages.Count <= 0)
                        Response.End();

                    byte[] buf = merge.Draw();

                    _phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);
                    //phImage.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_Image);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(buf);
                    Response.End();
                    buf = null;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(string.Format((string)GetLocalResourceObject("strDebugWriteMsg"), ex.ToString()));
                }
                finally
                {
                    GC.Collect();
                }
            }
        }
        void ph_Image(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (mainImage != null)
            {
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(mainImage), 0, 0);
                img.Height = 75.0F;            //12
                img.Width = 70.0F;             //15
                e.ContentArea.Add(img);
            }
        }

        //Jake 2011-02-25 Removed BarcodeNETImage
        public void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        {
            //ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(_barCode.GetBarcodeBitmap(FILE_FORMAT.JPG)), 0, 0);
            //img.Height = 30.0F;
            //img.Width = 150.0F;
            //e.ContentArea.Add(img);

            if (barCodeImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 30.0F;
                    img.Width = 150.0F;
                    e.ContentArea.Add(img);
                }

                int generation = System.GC.GetGeneration(barCodeImage);
                barCodeImage = null;
                System.GC.Collect(generation);
            }
        }
        public void logo_image(object sender, PlaceHolderLaidOutEventArgs e)
        {
            if (logoImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    logoImage.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 74.0F;
                    img.Width = 71.0F;
                    e.ContentArea.Add(img);
                }

                int generation = System.GC.GetGeneration(logoImage);
                logoImage = null;
                System.GC.Collect(generation);
            }
        }


        private string GetAuthCode()
        {
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            //get LA code
            AuthorityDB authority = new AuthorityDB(connectionString);
            AuthorityDetails authDet = authority.GetAuthorityDetails(autIntNo);

            return authDet.AutCode.Trim();
        }
    }
}
