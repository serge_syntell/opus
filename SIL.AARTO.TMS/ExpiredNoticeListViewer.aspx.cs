using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
//using BarcodeNETWorkShop;
using SIL.AARTO.BLL.BarCode;
using System.Text.RegularExpressions;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Handles printing the first notice
    /// </summary>
    public partial class ExpiredNoticeListViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string printFile = string.Empty;
        private int autIntNo = 0;

        //private string thisPage = "Expired Notice List Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "ExpiredNoticeListViewer.aspx";
        protected string loginUser = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.loginUser = userDetails.UserLoginName;

            this.autIntNo = Convert.ToInt32(Session["printAutIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Retrieve the Print File name from the Session
            if (Request.QueryString["printfile"] == null)
            {
                Response.Write((string)GetLocalResourceObject("strMsgWrite"));
                Response.End();
                return;
            }
            this.printFile = Request.QueryString["printfile"].ToString().Trim(); // Ciprus print request file name or none is the default

            AuthReportNameDB arn = new AuthReportNameDB(this.connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "ExpiredNoticeList");
            if (reportPage.Equals(string.Empty))
            {
                reportPage = "ExpiredNoticeList.rpt";
                //arn.AddAuthReportName(autIntNo, reportPage, "ExpiredNoticeList", "System");
                arn.AddAuthReportName(autIntNo, reportPage, "ExpiredNoticeList", "System", "");
            }

            // Declare some SQL Objects
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = null;

            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "ExpiredNoticeList");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            if (Request.QueryString["printfile"] != null)
            {
                string tempFileLoc = string.Empty;
                SqlDataAdapter da = null;
                ReportDocument _reportDoc = null;

                try
                {
                    // Fill the DataSet
                    com = new SqlCommand("NoticePrintExpired", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.CommandTimeout = 0;
                    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
                    com.Parameters.Add("@PrintFile", SqlDbType.VarChar, 50).Value = printFile;

                    //get data and populate dataset
                    dsNoticePrintExpired dsNoticePrintExpired = new dsNoticePrintExpired();
                    da = new SqlDataAdapter(com);
                    dsNoticePrintExpired.DataSetName = "dsNoticePrintExpired";
                    da.Fill(dsNoticePrintExpired);

                    if (dsNoticePrintExpired.Tables[1].Rows.Count == 0)
                    {
                        dsNoticePrintExpired.Dispose();
                        Response.Write((string)GetLocalResourceObject("strMsgWrite"));
                        Response.End();
                        return;
                    }

                    con.Dispose();
                    com.Dispose();
                    da.Dispose();

                    _reportDoc = new ReportDocument();
                    _reportDoc.Load(reportPath);

                    title = this.printFile;

                    //// add barcode containing a hidden code to make it more difficult for users to fraudulently duplicate a receipt
                    //// hidden code should include the ticket No number
                    //// alphanumeric so use code128A symbology
                    //// bar code reader software will pick up the string and the program will then split out the rctIntNo using the :
                    //// validate that the receiptNo and the intNo point to the same physical document
                    //// at a later stage we can do rc4 encryption but will need a barcode reader to test it

                    //foreach (DataRow dr in dsPrintNotice.Tables[1].Rows)
                    //{
                    //    BarcodeNETImage1.BarcodeText = dr["NotTicketNo"].ToString();
                    //    BarcodeNETImage1.ShowBarcodeText = false;
                    //    dr["BarCode"] = BarcodeNETImage1.GetBarcodeBitmap(FILE_FORMAT.JPG);
                    //}

                    //set up new report based on .rpt report class
                    _reportDoc.SetDataSource(dsNoticePrintExpired.Tables[1]);
                    dsNoticePrintExpired.Dispose();

                    MemoryStream ms = new MemoryStream();
                    ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(ms.ToArray());
                    Response.End();
                }
                catch
                {

                }
                finally
                {
                    //_reportDoc.Dispose();
                    //_reportDoc = null;

                    con.Dispose();
                    con = null;

                    com.Dispose();
                    com = null;

                    da.Dispose();
                    da = null;

                    if (File.Exists(tempFileLoc))
                        File.Delete(tempFileLoc);

                    if (_reportDoc != null)
                        _reportDoc.Dispose();
                }
            }



        }

    }
}
