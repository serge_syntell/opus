using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that displays the details for summonses for an LA
    /// </summary>
    public partial class PrintSummons : System.Web.UI.Page
    {
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private string cType = "SUM";
        private int _statusSummons = 610;                                 //summons required
        //private int _statusSummonsPrinted = 620;
        private const string DATE_FORMAT = "yyyy-MM-dd";

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "PrintSummons.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Select the next print file for printing summons";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)            
        {
                this.connectionString = Application["constr"].ToString();

                //get user info from session variable
                if (Session["userDetails"] == null)
                    Server.Transfer("Login.aspx?Login=invalid");
                if (Session["userIntNo"] == null)
                    Server.Transfer("Login.aspx?Login=invalid");

                //get user details
                Stalberg.TMS.UserDB user = new UserDB(connectionString);
                Stalberg.TMS.UserDetails userDetails = new UserDetails();

                userDetails = (UserDetails)Session["userDetails"];
                this.login = userDetails.UserLoginName;

                //int 
                autIntNo = Convert.ToInt32(Session["autIntNo"]);

                Session["userLoginName"] = userDetails.UserLoginName.ToString();
                int userAccessLevel = userDetails.UserAccessLevel;

                // Set domain specific variables
                General gen = new General();
                backgroundImage = gen.SetBackground(Session["drBackground"]);
                styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
                title = gen.SetTitle(Session["drTitle"]);

                pnlGeneral.Visible = true;
                dgPrintrun.Visible = true;

                //dls 090506 - need this for Ajax Calendar extender
                HtmlLink link = new HtmlLink();
                link.Href = styleSheet;
                link.Attributes.Add("rel", "stylesheet");
                link.Attributes.Add("type", "text/css");
                Page.Header.Controls.Add(link);
            
                if (!Page.IsPostBack)
                {
                    this.lblDateFrom.Visible = false;
                    this.lblDateTo.Visible = false;
                    this.dateFrom.Visible = false;
                    this.dateTo.Visible = false;

                    this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                    this.chkShowAll.Checked = false;

                    this.PopulateAuthorites(autIntNo);

                    this.BindGrid(this.autIntNo, this.cType);
                    this.ShowErrors(this.autIntNo, false);

                    if (ddlSelectLA.SelectedIndex > -1)
                    {
                        autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                        Session["printAutIntNo"] = autIntNo;
                        this.TicketNumberSearch1.AutIntNo = this.autIntNo;
                    }

                    AuthorityRulesDetails arDetailsLoad = new AuthorityRulesDetails();
                    arDetailsLoad.AutIntNo = autIntNo;
                    arDetailsLoad.ARCode = "6209";
                    arDetailsLoad.LastUser = this.login;
                    DefaultAuthRules arLoad = new DefaultAuthRules(arDetailsLoad, this.connectionString);
                    //2014-05-15 Heidi rename "SecNoticeLoad" to "IsIBMPrinter"(5283)
                    bool isIBMPrinter = arLoad.SetDefaultAuthRule().Value.Equals("Y");

                    if (isIBMPrinter)
                    {
                        //this.btnPrint.Enabled = false; 2013-11-22 Removed by Nancy for reprinting CPA5&CPA6
                        this.lblError.Visible = true;
                        this.lblError.Text = (string)GetLocalResourceObject("errorMsg");
                    }
                }
                else 
                {
                    //2015-02-26 Heidi fix issue that the "Print Summons Control Register" button is not available but when click "look up","Print Summons Control Register" button is available(bontq1864)
                    if (Request.Form["SummonsNumberSearch$btnSearch"] != null || Request.Form["TicketNumberSearch1$btnSearch"] != null)
                    {
                        autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                        BindGrid(this.autIntNo, this.cType);
                    }
                    
                }

        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorites(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");
            
            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void BindGrid(int autIntNo, string cType)
        {
            string showAll = chkShowAll.Checked ? "Y" : "N";

            string showControlRegister = chkSumControlRegister.Checked ? "Y" : "N";

            DateTime dtFrom = lblDateFrom.Visible ? DateTime.Parse(dateFrom.Text) : DateTime.MinValue;
            DateTime dtTo = lblDateTo.Visible ? DateTime.Parse(dateTo.Text) : DateTime.MaxValue;

            Stalberg.TMS.NoticeDB printList = new Stalberg.TMS.NoticeDB(connectionString);
            int totalCount = 0; // 2013-03-14 add by Henry for pagination
            DataSet ds= printList.GetPrintrunDS(autIntNo, cType, _statusSummons, showAll, showControlRegister, dtFrom, dtTo, out totalCount, dgPrintrunPager.PageSize, dgPrintrunPager.CurrentPageIndex);
            dgPrintrun.DataSource = ds;
            dgPrintrun.DataKeyField = "PrintFile";
            //Heidi 2013-09-27 changed for fixed the problem that When printing the control register for a print file on page 3 (example) print is completed and the page returns to page one.
            if (ds!=null&&ds.Tables[0]!=null&&ds.Tables[0].Rows.Count==0 && totalCount > 0 && dgPrintrunPager.CurrentPageIndex>1)
            {
                dgPrintrunPager.CurrentPageIndex = dgPrintrunPager.CurrentPageIndex - 1;
                dgPrintrun.DataSource = printList.GetPrintrunDS(autIntNo, cType, _statusSummons, showAll, showControlRegister, dtFrom, dtTo, out totalCount, dgPrintrunPager.PageSize, dgPrintrunPager.CurrentPageIndex);
            }
            dgPrintrunPager.RecordCount = totalCount;
            //mrs 20080812 caused a crash
            //dgPrintrun.DataBind();

            try
            {
                dgPrintrun.DataBind();
            }
            catch
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.DataBind();
            }
           

            if (dgPrintrun.Items.Count == 0)
            {
                dgPrintrun.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgPrintrun.Visible = true;
                lblError.Visible = false;
            }

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)
            bool isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

            if (isIBMPrinter)
            {
                for (int i = 0; i < dgPrintrun.Items.Count; i++)
                {
                    dgPrintrun.Items[i].Cells[5].Enabled = false;
                }
                this.lblError.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("errorMsg");
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //2015-02-26 Heidi fix issue that the "Print Summons Control Register" button is not available but when click "look up","Print Summons Control Register" button is available(bontq1864)
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, cType);

            if (txtPrint.Text.Trim().Equals(string.Empty) &&  txtSummons.Text.Trim().Equals(string.Empty))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }

            string sValue = string.Empty;
            string sMode = "N";
            if (txtPrint.Text.Trim().Length > 1)
                sValue = txtPrint.Text.Trim();
            if (txtSummons.Text.Trim().Length > 1)
            {
                sValue = txtSummons.Text.Trim();
                sMode = "S";
            }

            // Generate the JavaScript to pop up a print-only window
            Helper_Web.BuildPopup(this, "SummonsViewer.aspx", "printfile", "*" + sValue, "mode", sMode, "printType", "s");
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintSummons, PunchAction.Change);  
            
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            Session["printAutIntNo"] = autIntNo;
            //Session.Add("autIntNo", autIntNo);
            this.TicketNumberSearch1.AutIntNo = this.autIntNo;
            dgPrintrunPager.RecordCount = 0; // 2013-03-14 add by Henry for pagination
            dgPrintrunPager.CurrentPageIndex = 1;
            this.BindGrid(autIntNo, cType);
            this.ShowErrors(autIntNo, true);
        }

        private void ShowErrors(int autIntNo, bool isReload)
        {
            if (isReload)
            {
                grdErrorsPager.RecordCount = 0;
                grdErrorsPager.CurrentPageIndex = 1;
            }

            NoticeDB summons = new NoticeDB(this.connectionString);
            int totalCount = 0;
            DataSet ds = summons.GetSummonsErrors(autIntNo,grdErrorsPager.PageSize, grdErrorsPager.CurrentPageIndex, out totalCount);
            this.grdErrors.DataSource = ds;
            this.grdErrors.DataBind();
            grdErrorsPager.RecordCount = totalCount;
        }

        protected void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            this.lblDateFrom.Visible = chkShowAll.Checked;
            this.lblDateTo.Visible = chkShowAll.Checked;
            this.dateFrom.Visible = chkShowAll.Checked;
            this.dateTo.Visible = chkShowAll.Checked;

            if (chkShowAll.Checked)
            {
                this.dateFrom.Text = DateTime.Now.AddMonths(-1).ToString(DATE_FORMAT);
                this.dateTo.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
            }
            dgPrintrunPager.RecordCount = 0; // 2013-03-14 add by Henry for pagination
            dgPrintrunPager.CurrentPageIndex = 1;
            BindGrid(autIntNo, cType);

            Session["showAllSummons"] = chkShowAll.Checked ? "Y" : "N";

            Session["showControlRegister"] = chkSumControlRegister.Checked ? "Y" : "N";
        }

        protected void dgPrintrun_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            dgPrintrun.SelectedIndex = e.Item.ItemIndex;

            if (dgPrintrun.SelectedIndex > -1)
            {
                lblError.Visible = true;
            }

            if (e.CommandName == "PrintControl")
            {
                
                string printFile = e.Item.Cells[0].Text;

                PageToOpen[] pages = new PageToOpen[1];
                List<QSParams> parameters = new List<QSParams>();
                parameters = new List<QSParams>();
                parameters.Add(new QSParams("printfile", printFile));
                parameters.Add(new QSParams("Summary"));
                parameters.Add(new QSParams("printType", "PrintSummonsControlRegister"));
                pages[0] = new PageToOpen(this, "SummonsViewer.aspx", parameters);

                Helper_Web.BuildPopup(pages);
                //Heidi 2013-09-27 Comment out for fixed the problem that When printing the control register for a print file on page 3 (example) print is completed and the page returns to page one.
                //dgPrintrunPager.RecordCount = 0; // 2013-03-14 add by Henry for pagination
                //dgPrintrunPager.CurrentPageIndex = 1;
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintSummonsPrintSummonsControlRegister, PunchAction.Change); 

                this.BindGrid(this.autIntNo, this.cType);
            }
        }

        protected void dgPrintrun_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string printFile = e.Item.Cells[0].Text;

            PageToOpen[] pages = new PageToOpen[1];
            List<QSParams> parameters = new List<QSParams>();
            parameters.Add(new QSParams("printfile", printFile));
            pages[0] = new PageToOpen(this, "SummonsViewer.aspx", parameters);

            Helper_Web.BuildPopup(pages);
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtPrint.Text = this.TicketNumberSearch1.TicketNumber;
            //2015-02-26 Heidi fix issue that the "Print Summons Control Register" button is not available but when click "select","Print Summons Control Register" button is available(bontq1864)
            if (Request.Form["TicketNumberSearch1$btnSearch"] == null && !string.IsNullOrEmpty(this.TicketNumberSearch1.TicketNumber))
            {
                autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                BindGrid(this.autIntNo, this.cType);
            }
            //PunchStats805806 enquiry PrintSummons
        }

        public void SummonsSelected(object sender, EventArgs e)
        {
            this.txtSummons.Text = this.SummonsNumberSearch.SummonsNumber;
            //2015-02-26 Heidi fix issue that the "Print Summons Control Register" button is not available but when click "select","Print Summons Control Register" button is available(bontq1864)
            if (Request.Form["SummonsNumberSearch$btnSearch"] == null && !string.IsNullOrEmpty(this.SummonsNumberSearch.SummonsNumber))
            {
                autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                BindGrid(this.autIntNo, this.cType);
            }
            //PunchStats805806 enquiry PrintSummons
        }

        protected void dgPrintrun_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.DataItem == null)
                return;

            DataRowView row = (DataRowView)e.Item.DataItem;

            //disable print control register option if the summons print date is null
            e.Item.Cells[7].Enabled = row["SumPrintSummonsDate"].ToString().Equals("") ? false : true;
            
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            // Jake 2013-09-12 comment out 
            //dgPrintrunPager.RecordCount = 0; // 2013-03-14 add by Henry for pagination
            //dgPrintrunPager.CurrentPageIndex = 1;
            BindGrid(autIntNo, this.cType);
        }

        // 2013-04-10 add by Henry for pagination
        protected void dgPrintrunPager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, cType);
        }

        // 2013-04-10 add by Henry for pagination
        protected void grdErrorsPager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            ShowErrors(autIntNo, false);
        }
}
}
