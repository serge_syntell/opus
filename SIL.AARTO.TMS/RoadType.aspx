<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.RoadType" Codebehind="RoadType.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
 <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <asp:DataGrid ID="dgRoadType" Width="495px" runat="server" BorderColor="Black" AllowPaging="True"
                                        GridLines="Vertical" CellPadding="4" Font-Name="Verdana" Font-Size="8pt" ShowFooter="True"
                                        HeaderStyle-CssClass="CartListHead" FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem"
                                        AlternatingItemStyle-CssClass="CartListItemAlt" AutoGenerateColumns="False" Font-Names="Verdana"
                                        OnItemCommand="dgRoadType_ItemCommand" OnPageIndexChanged="dgRoadType_PageIndexChanged">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="RdTIntNo" HeaderText="RdTIntNo"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="RdTypeCode" HeaderText="<%$Resources:dgRoadType.HeaderText1 %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="RdTypeDescr" HeaderText="<%$Resources:dgRoadType.HeaderText2 %>"></asp:BoundColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgRoadTypeItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                    </asp:DataGrid>
                                </asp:Panel>
                                <asp:Panel ID="pnlAddRoadType" runat="server" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="lblAddRoadType" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddRoadType.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="lblAddRdTCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddRdTCode.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddRdTCode" runat="server" Width="36px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="2"></asp:TextBox>
                                                <asp:RangeValidator ID="Rangevalidator2" runat="server" CssClass="NormalRed" ForeColor=" "
                                                    ControlToValidate="txtAddRdTCode" ErrorMessage="<%$Resources:RanCode.ErrorMessage %>" Type="Integer"
                                                    MinimumValue="0" MaximumValue="9999" ></asp:RangeValidator></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtAddRdTCode" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblAddRdTDescr" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddRdTDescr.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddRdTDescr" runat="server" Width="212px" CssClass="Normal" MaxLength="30"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtAddRdTDescr" ErrorMessage="<%$Resources:ReqDescription.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddRoadType" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddRoadType.Text %>"
                                                    OnClick="btnAddRoadType_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateRoadType" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label19" runat="server" Width="171px" CssClass="ProductListHead" Text="<%$Resources:lblUpdate.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblRdTCode.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtRdTCode" runat="server" Width="36px" CssClass="NormalMand" Height="24px"
                                                    MaxLength="2"></asp:TextBox>
                                                <asp:RangeValidator ID="Rangevalidator1" runat="server" CssClass="NormalRed" ForeColor=" "
                                                    ControlToValidate="txtRdTCode" ErrorMessage="<%$Resources:RanCode.ErrorMessage %>" Type="Integer" MinimumValue="0"
                                                    MaximumValue="9999"></asp:RangeValidator></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtRdTCode" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="Label17" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddRdTDescr.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtRdTDescr" runat="server" Width="235px" CssClass="Normal" MaxLength="30"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtRdTDescr" ErrorMessage="<%$Resources:ReqDescription.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
