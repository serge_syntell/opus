<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.BankAccount" Codebehind="BankAccount.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>" 
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                
                            <tr>
                                <td align="center" height="21">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">

                                <asp:DataGrid ID="dgAccount" Width="495px" runat="server" BorderColor="Black" AllowPaging="True"
                                    GridLines="Vertical" CellPadding="4" Font-Name="Verdana" Font-Size="8pt" ShowFooter="True"
                                    HeaderStyle-CssClass="CartListHead" FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem"
                                    AlternatingItemStyle-CssClass="CartListItemAlt" AutoGenerateColumns="False" Font-Names="Verdana"
                                    OnItemCommand="dgAccount_ItemCommand">
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="BAIntNo" HeaderText="BAIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="BAAccountNo" HeaderText="<%$Resources:dgAccount.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="BAAccountName" HeaderText="<%$Resources:dgAccount.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="BAName" HeaderText="<%$Resources:dgAccount.HeaderText3 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="BABranchName" HeaderText="<%$Resources:dgAccount.HeaderText4 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="BABranchNo" HeaderText="<%$Resources:dgAccount.HeaderText5 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgAccountItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <asp:Panel ID="pnlAddAccount" runat="server" Height="127px">
                                    <table id="Table2" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="lblAddAccount" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddAccount.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="lblAddBAAccountNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddBAAccountNo.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddBAAccountNo" runat="server" Width="168px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="20"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="28">
                                                <asp:Label ID="lblAddBAAccountName" runat="server" Width="168px" CssClass="NormalBold" Text="<%$Resources:lblAddBAAccountName.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="28">
                                                <asp:TextBox ID="txtAddBAAccountName" runat="server" Width="264px" CssClass="Normal"
                                                    Height="28px" MaxLength="50"></asp:TextBox></td>
                                            <td height="28">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="33">
                                                <p>
                                                    <asp:Label ID="lblAddBAName" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddBAName.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="33">
                                                <asp:TextBox ID="txtAddBAName" runat="server" Width="264px" CssClass="Normal" MaxLength="50"></asp:TextBox></td>
                                            <td height="33">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="24">
                                                <p>
                                                    <asp:Label ID="lblAddBranchName" runat="server" Width="176px" CssClass="NormalBold" Text="<%$Resources:lblAddBranchName.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="24">
                                                <asp:TextBox ID="txtAddBABranchName" runat="server" Width="264px" CssClass="NormalMand"
                                                    Height="28px" MaxLength="50"></asp:TextBox></td>
                                            <td height="24">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="22">
                                                <asp:Label ID="lblAddBranchNo" runat="server" Width="176px" CssClass="NormalBold" Text="<%$Resources:lblAddBranchNo.Text %>"></asp:Label></td>
                                            <td width="248" height="22">
                                                <asp:TextBox ID="txtAddBABranchNo" runat="server" Width="264px" CssClass="NormalMand"
                                                    Height="28px" MaxLength="20"></asp:TextBox></td>
                                            <td height="22">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddAccount" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddAccount.Text %>"
                                                    OnClick="btnAddAccount_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateAccount" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="279" height="2">
                                                <asp:Label ID="Label19" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblUpdAccount.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="279" height="25">
                                                <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddBAAccountNo.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtBAAccountNo" runat="server" Width="152px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="20"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="279" height="26">
                                                <p>
                                                    <asp:Label ID="Label17" runat="server" Width="232px" CssClass="NormalBold" Text="<%$Resources:lblAddBAAccountName.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="26">
                                                <asp:TextBox ID="txtBAAccountName" runat="server" Width="264px" CssClass="Normal"
                                                    MaxLength="50"></asp:TextBox></td>
                                            <td height="26">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="279">
                                                <asp:Label ID="Label15" runat="server" Width="168px" CssClass="NormalBold" Text="<%$Resources:lblAddBAName.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtBAName" runat="server" Width="256px" CssClass="NormalMand" Height="24px"
                                                    MaxLength="1"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="279">
                                                <asp:Label ID="Label2" runat="server" Width="168px" CssClass="NormalBold" Text="<%$Resources:lblAddBranchName.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtBABranchName" runat="server" Width="256px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="50"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="279">
                                                <asp:Label ID="Label3" runat="server" Width="168px" CssClass="NormalBold" Text="<%$Resources:lblAddBranchNo.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtBABranchNo" runat="server" Width="256px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="20"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="279">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdateAccount" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdateAccount.Text %>"
                                                    OnClick="btnUpdateAccount_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
        </ContentTemplate></asp:UpdatePanel>
    </form>
</body>
</html>
