using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class PostReturnType : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "PostReturnType.aspx";
        protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "Post return type maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
			autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				this.pnlAddPRType.Visible = false;
				this.pnlUpdatePostReturnType.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

				BindGrid();
			}		
		}

		protected void BindGrid()
		{
			// Obtain and bind a list of all users
            Stalberg.TMS.PostReturnTypeDB prtList = new Stalberg.TMS.PostReturnTypeDB(connectionString);

			DataSet data = prtList.GetPostReturnTypeListDS(this.chkSort.Checked );
			dgPostReturnType.DataSource = data;
            dgPostReturnType.DataKeyField = "PRTIntNo";
            dgPostReturnType.DataBind();

            if (dgPostReturnType.Items.Count == 0)
			{
                dgPostReturnType.Visible = false;
				lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
                dgPostReturnType.Visible = true;
			}

			data.Dispose();
            pnlAddPRType.Visible = false;
            pnlUpdatePostReturnType.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
            pnlAddPRType.Visible = true;
            pnlUpdatePostReturnType.Visible = false;
			btnOptDelete.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
		}

        protected void btnAddPostReturnType_Click(object sender, System.EventArgs e)
		{		
			// add the new transaction to the database table
            Stalberg.TMS.PostReturnTypeDB prtAdd = new PostReturnTypeDB(connectionString);
			
			int addPRTIntNo = prtAdd.AddPostReturnType(Convert.ToInt32(txtAddPRTCode.Text), 
				txtAddPRTDescr.Text, this.loginUser);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addPRTIntNo, lgEntityList, "PostReturnTypeLookup", "PRTIntNo", "PRTDescr", this.loginUser);

			if (addPRTIntNo <= 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.PostReturnTypeMaintenance, PunchAction.Add);  

			}
			lblError.Visible = true;
			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			int rdTIntNo = Convert.ToInt32(Session["editPRTIntNo"]);

			// add the new transaction to the database table
            Stalberg.TMS.PostReturnTypeDB prtUpdate = new PostReturnTypeDB(connectionString);
			
			int updRdTIntNo = prtUpdate.UpdatePostReturnType(rdTIntNo, 
				Convert.ToInt32(txtPRTCode.Text), txtPRTDescr.Text, this.loginUser);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(updRdTIntNo, lgEntityList, "PostReturnTypeLookup", "PRTIntNo", "PRTDescr", this.loginUser);
           
			if (updRdTIntNo <= 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.PostReturnTypeMaintenance, PunchAction.Change); 
			}
			lblError.Visible = true;
			BindGrid();
		}

        protected void dgPostReturnType_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgPostReturnType.SelectedIndex = e.Item.ItemIndex;

            if (dgPostReturnType.SelectedIndex > -1) 
			{
                int editPRTIntNo = Convert.ToInt32(dgPostReturnType.DataKeys[dgPostReturnType.SelectedIndex]);

				Session["editPRTIntNo"] = editPRTIntNo;
				Session["prevPage"] = thisPageURL;

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editPRTIntNo.ToString(), "PostReturnTypeLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

				ShowRoadTypeDetails(editPRTIntNo);
			}
		}

		protected void ShowRoadTypeDetails(int editRdTIntNo)
		{
			// Obtain and bind a list of all users
            Stalberg.TMS.PostReturnTypeDB prType = new Stalberg.TMS.PostReturnTypeDB(connectionString);
			
			Stalberg.TMS.PostReturnTypeDetails prtDetails = prType.GetPostReturnTypeDetails(editRdTIntNo);

            txtPRTCode.Text = prtDetails.PRTypeCode.ToString();
            txtPRTDescr.Text = prtDetails.PRTypeDescr;
			
			pnlUpdatePostReturnType.Visible = true;
			pnlAddPRType.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

	
		protected void dgPostReturnType_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgPostReturnType.CurrentPageIndex = e.NewPageIndex;
			BindGrid();
	    }

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int prTIntNo = Convert.ToInt32(Session["editPRTIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "PostReturnTypeLookup", "PRTIntNo");

            PostReturnTypeDB prType = new Stalberg.TMS.PostReturnTypeDB(connectionString);

			string delPRTIntNo = prType.DeletePostReturnType(prTIntNo);

            if (delPRTIntNo.Equals("0"))
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
			}
            else if (delPRTIntNo.Equals("-1"))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.PostReturnTypeMaintenance, PunchAction.Delete); 
            }

			lblError.Visible = true;
            if (dgPostReturnType.Items.Count == 1 && dgPostReturnType.CurrentPageIndex > 0)
            {
                dgPostReturnType.CurrentPageIndex--;
            }    
			BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgPostReturnType.Visible.Equals(true))
			{
				//hide it
				dgPostReturnType.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptShow.Text");
			}
			else
			{
				//show it
                dgPostReturnType.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		 

        protected void chkSort_CheckedChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}
