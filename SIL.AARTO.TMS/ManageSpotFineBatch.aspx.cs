using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class ManageSpotFineBatch : System.Web.UI.Page
    {
        // Fields
        private string connectionstring = string.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private int sfbIntNo = 0;
        private string sfbCompleteDate = string.Empty;
        private int notIntNo = 0;
        private int sfbnIntNo = 0;
        //private bool processBatch = false;
        private string notTicketNo = string.Empty;
        private string notOffenceDate = string.Empty;
        private string sfbnRctNumber = string.Empty;
        private string sfbnRTAmount = string.Empty;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = string.Empty;
        //protected string title = "Manage Notice and Receipt Batch Print";
        protected string title = "";
        protected string description = string.Empty;
        protected string thisPageURL = "ManageSpotFineBatch.aspx";

        private const string DATE_FORMAT = "yyyy-MM-dd";
        //private const string SELECT_USER = "[ Select a User ]";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionstring = Application["constr"].ToString();

            //will need to do security check for just supervisor or what not allowed...

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionstring);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (this.ViewState["sfbIntNo"] != null)
                this.sfbIntNo = (int)this.ViewState["sfbIntNo"];
            if (this.ViewState["notIntNo"] != null)
                this.notIntNo = (int)this.ViewState["notIntNo"];
            if (this.ViewState["sfbnIntNo"] != null)
                this.sfbnIntNo = (int)this.ViewState["sfbnIntNo"];
            //if (this.ViewState["processBatch"] != null)
            //	this.processBatch = Convert.ToBoolean(this.ViewState["processBatch"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                btnNewBatch.Visible = false;

                this.PopulateAuthorities();

                //BD need to also populate users drop down
                this.PopulateUsers(Convert.ToInt32(ddlAuthority.SelectedValue));

                this.btnPrintBatch.Visible = false;
                this.btnPrintRegister.Visible = false;

                //Make the Grid invisible until user clicks View Batches Button
                this.pnlBatches.Visible = false;
                this.pnlButtons.Visible = false;
                this.pnlPrintBatch.Visible = false;
                this.pnlChangeUser.Visible = false;

                //supervisor will always be allowed to start a new batch for the user selected.
                btnNewBatch.Visible = true;

                //need to set the spot fine priority date
                Stalberg.TMS.UserDB selectedUser = new UserDB(connectionstring);
                Stalberg.TMS.UserDetails selectedUserDetails = new UserDetails();
                selectedUserDetails = selectedUser.GetUserDetails(Convert.ToInt32(ddlUsers.SelectedValue));

                //Edge 2013-09-12 modify
                //dtpPriorityDate.Text = string.Format("{0:yyyy-MM-dd}", selectedUserDetails.SpotFinePriorityDate);
                if (!string.IsNullOrWhiteSpace(selectedUserDetails.SpotFinePriorityDate))
                {
                    dtpPriorityDate.Text = DateTime.Parse(selectedUserDetails.SpotFinePriorityDate).ToString("yyyy-MM-dd");
                }

            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(this.connectionstring);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionstring);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            this.ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void PopulateUsers(int autIntNo)
        {
            Stalberg.TMS.UserDB user = new UserDB(connectionstring);

            SqlDataReader reader = user.GetUserListInGroupForAuthority(autIntNo);
            this.ddlUsers.DataSource = reader;
            this.ddlUsers.DataValueField = "UserIntNo";
            this.ddlUsers.DataTextField = "UserFullName";
            this.ddlUsers.DataBind();

            reader.Close();
        }

        private void PopulateUserChange(int autIntNo)
        {
            Stalberg.TMS.UserDB user = new UserDB(connectionstring);
            
            SqlDataReader reader = user.GetUserListInGroupForAuthority(autIntNo);
            this.ddlUserChange.DataSource = reader;
            this.ddlUserChange.DataValueField = "UserIntNo";
            this.ddlUserChange.DataTextField = "UserFullName";
            this.ddlUserChange.DataBind();

            reader.Close();

            this.ddlUserChange.Items.Insert(0, (string)GetLocalResourceObject("SELECT_USER"));
            this.ddlUserChange.SelectedIndex = 0;
        }

        // Fetches the batches for the chosen authority and populates the grid
        private void PopulateSpotFineBatches()
        {
            this.pnlButtons.Visible = false;

            autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);
            string CompletedBatches = "N";
            string PrintedBatches = "N";

            if (chkCompleted.Checked)
                CompletedBatches = "Y";

            if (chkPrinted.Checked)
            {
                PrintedBatches = "Y";
                CompletedBatches = "Y"; // to ensure all completed batches are included
            }

            //BD need to get the user login name for which one has been selected
            Stalberg.TMS.UserDB selectedUser = new UserDB(connectionstring);
            Stalberg.TMS.UserDetails selectedUserDetails = new UserDetails();
            selectedUserDetails = selectedUser.GetUserDetails(Convert.ToInt32(ddlUsers.SelectedValue));


            Stalberg.TMS.SpotFineBatchDB sfb = new Stalberg.TMS.SpotFineBatchDB(connectionstring);
            int totalCount = 0;
            DataSet dsSFB = sfb.GetSpotFineBatchListDS(autIntNo, CompletedBatches, PrintedBatches, this.login, GridViewBatchesPager.PageSize, GridViewBatchesPager.CurrentPageIndex, out totalCount);
            this.GridViewBatches.DataSource = dsSFB;
            GridViewBatchesPager.RecordCount = totalCount;

            // Bind the data
            this.GridViewBatches.DataBind();

            // Check if there are any results; if not, the GridViewBatches will not display and an error message will
            if (GridViewBatches.Rows.Count == 0)
            {
                this.pnlBatches.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
                this.pnlBatches.Visible = true;
        }

        public bool TrueFalse(string bitValue)
        {
            if (bitValue == "1" || bitValue == "true")
                return true;

            return false;
        }

        // Fetches the notices for the chosen batch and populates the grid
        private void PopulateSpotFineBatchNotices()
        {
            this.autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);

            Stalberg.TMS.SpotFineBatchNoticesDB sfbn = new Stalberg.TMS.SpotFineBatchNoticesDB(connectionstring);
            int totalCount = 0; //2013-04-08 add by Henry for pagination
            DataSet dsSFBN = sfbn.GetSpotFineBatchNoticesListDS(this.autIntNo, this.sfbIntNo,
                string.Empty, string.Empty,
                GridViewBatchesPager.PageSize, GridViewBatchesPager.CurrentPageIndex, out totalCount);
            GridViewBatchesPager.RecordCount = totalCount;

            this.GridViewNotices.DataSource = dsSFBN;

            // Bind the data
            this.GridViewNotices.DataKeyNames = new string[] { "NotIntNo" };
            this.GridViewNotices.DataKeyNames = new string[] { "SFBNIntNo" };
            this.GridViewNotices.DataBind();

            //the notice check box does not need to be enabled
            //this needs to change as the check boxes are not being used and it will be by batch
            if (this.chkSummons.Checked)
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                        cbNotice.Enabled = false;
                    }

                }
            }
            else
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                        cbNotice.Enabled = true;
                    }

                }
            }
            //this.GridViewNotices.Sort("NotTicketNo", SortDirection.Ascending); //NotTicketNo = notice number

            // Check if there are any results
            if (GridViewNotices.Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            else
            {
                this.pnlBatches.Visible = false;

                this.pnlNotices.Visible = true;
                this.GridViewNotices.Visible = true;
                this.pnlButtons.Visible = true;
            }
        }

        // Populate the panel with details of the selected notice, but keep the GridView with Notices visible so 
        // the user can select another notice at any time
        private void PopulateFixNoticePanel()
        {
            // Split the Notice Ticket No. into prefix and sequence no for displaying on the panel
            //string[] splitNotTicketNoArray = this.notTicketNo.Split(new char[] { '/' });
            //string splitNotTicketNoPrefix = splitNotTicketNoArray[0];
            //string splitNotTicketNoSeqNo = splitNotTicketNoArray[1];

            //this.pnlFixNotice.Visible = true;

            ////Put the data from the selected GridViewNotices row into the fields on the Notice Panel
            //// this.lblSFBNIntNo.Text = Convert.Tostring(sfbnIntNo); 
            //this.ViewState.Add("sfbnIntNo", this.sfbnIntNo);
            //this.lblNoticePrefix.Text = splitNotTicketNoPrefix;
            //this.lblNoticeSeqNo.Text = splitNotTicketNoSeqNo;
            //this.lblFullNoticeNo.Text = notTicketNo;
            //this.lblOffenceDate.Text = DateTime.Parse(notOffenceDate).ToString(DATE_FORMAT);
            //this.txtRctNumber.Text = sfbnRctNumber;
            //this.lblRTAmount.Text = decimal.Parse(sfbnRTAmount).ToString("#,##0.00");

            //this.txtRctNumber.Focus();
        }

        private void ProcessOneNotice()
        {
            this.sfbnIntNo = (int)this.ViewState["sfbnIntNo"];
            this.UpdateSpotFineBatchNotice();

            //2013-04-08 add by Henry for pagination
            GridViewNoticesPager.CurrentPageIndex = 1;
            GridViewNoticesPager.RecordCount = 0;
            // Refresh the Batch Notices GridView
            this.PopulateSpotFineBatchNotices();
        }

        private void ProcessBatchOfNotices()
        {
            //this.sfbnIntNo = (int)this.ViewState["sfbnIntNo"];
            //this.UpdateSpotFineBatchNotice();

            //Stalberg.TMS.SpotFineBatchNoticesDB sfbngn = new Stalberg.TMS.SpotFineBatchNoticesDB(connectionstring);
            //DataSet dsSFBNGN = sfbngn.SpotFineBatch_NoticeGetNextDS(autIntNo, sfbIntNo);

            //// if a row is returned from the Get Next sql proc, then proceed
            //if (dsSFBNGN.Tables[0].Rows.Count != 0)
            //{
            //    processBatch = true;
            //    DataRow drSFBNGN = dsSFBNGN.Tables[0].Rows[0];
            //    sfbnIntNo = Convert.ToInt32(drSFBNGN["SFBNIntNo"]);
            //    notTicketNo = drSFBNGN["NotTicketNo"].ToString();
            //    notOffenceDate = drSFBNGN["NotOffenceDate"].ToString();
            //    sfbnRctNumber = drSFBNGN["SFBNRctNumber"].ToString();
            //    sfbnRTAmount = drSFBNGN["SFBNRTAmount"].ToString();

            //    this.PopulateFixNoticePanel();
            //}
            //// There were no rows returned from the Get Next sql proc
            //else
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "There are no more notices to process in this batch, please select the next batch for processing";
            //    // Should we automate the update of the SpotFineBatch table here by setting the SFBCompleteDate to current date at this point or 
            //    // must user click on Complete Batch button to do the above?
            //    this.processBatch = false;
            //    this.pnlBatches.Visible = false;
            //    this.pnlNotices.Visible = true;
            //    this.GridViewNotices.Visible = true;

            //    //hide fix notice panel -display notice grid
            //    this.PopulateSpotFineBatchNotices();
            //}

            //this.ViewState.Add("processBatch", processBatch);
        }

         

        protected void btnViewBatches_Click(object sender, EventArgs e)
        {
            this.lblError.Text = string.Empty;
            //this.processBatch = false;
            //this.ViewState.Add("processBatch", processBatch);
            this.pnlNotices.Visible = false;
            this.GridViewNotices.Visible = false;
            this.pnlChangeUser.Visible = false;
            this.pnlButtons.Visible = false;
            this.pnlPrintBatch.Visible = false;

            //Henry 2013-04-08 add for pagination
            GridViewBatchesPager.RecordCount = 0;
            GridViewBatchesPager.CurrentPageIndex = 1;
            this.PopulateSpotFineBatches();
        }

        protected void btnUpdatePriorityDate_Click(object sender, EventArgs e)
        {
            //if (dtpPriorityDate.Value == null)
            //{
            //    lblError.Text = "Please select a valid priority date ";
            //    lblError.Visible = true;
            //    return;
            //}

            if (dtpPriorityDate.Text.Equals(string.Empty))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                lblError.Visible = true;
                return;
            }

            //BD need to update the priority date for the user
            Stalberg.TMS.UserDB user = new UserDB(connectionstring);
            int iUpdate = user.PriorityDateUpdate(Convert.ToInt32(ddlUsers.SelectedValue), Convert.ToDateTime(dtpPriorityDate.Text), this.login);
            if (iUpdate == -1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                lblError.Visible = true;
            }
            else if(iUpdate>0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ManageNoticeAndReceiptBatchPrint, PunchAction.Change);  

            }
        }

        protected void btnNewBatch_Click(object sender, EventArgs e)
        {
            // BD here we need to go and fetch the latest batch to be processed 
            // This will be based on the Authority and the rules set by the supervisor
            //BD need to get the user login name for which one has been selected
            Stalberg.TMS.UserDB selectedUser = new UserDB(connectionstring);
            Stalberg.TMS.UserDetails selectedUserDetails = new UserDetails();
            selectedUserDetails = selectedUser.GetUserDetails(Convert.ToInt32(ddlUsers.SelectedValue));

            Stalberg.TMS.SpotFineBatchDB sfb = new Stalberg.TMS.SpotFineBatchDB(connectionstring);
            int newSFBIntNo = sfb.AllocateNextBatch(selectedUserDetails.UserLoginName, autIntNo);
            if (newSFBIntNo == -1)
            {
                //there was an error creating the new batch
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                //set the state of the screen back to the select batch button (as if they ghad just come into the screen)
                this.btnPrintBatch.Visible = false;
                this.btnPrintRegister.Visible = false;
                this.pnlNotices.Visible = false;
                this.GridViewNotices.Visible = false;
                this.pnlChangeUser.Visible = false;
                this.pnlBatches.Visible = false;
                this.pnlButtons.Visible = false;
                this.pnlPrintBatch.Visible = false;
            }
            else if (newSFBIntNo == -2)
            {
                //there was no more batches to allocate
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                //set the state of the screen back to the select batch button (as if they ghad just come into the screen)
                this.btnPrintBatch.Visible = false;
                this.btnPrintRegister.Visible = false;
                this.pnlNotices.Visible = false;
                this.GridViewNotices.Visible = false;
                this.pnlChangeUser.Visible = false;
                this.pnlBatches.Visible = false;
                this.pnlButtons.Visible = false;
                this.pnlPrintBatch.Visible = false;
            }
            else
            {

                // Once the new batch has been allocated then must carry on with the population of the batch grid
                this.lblError.Text = string.Empty;
                this.pnlNotices.Visible = false;
                this.GridViewNotices.Visible = false;
                this.pnlChangeUser.Visible = false;
                this.pnlButtons.Visible = false;
                this.pnlPrintBatch.Visible = false;
              
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ManageNoticeAndReceiptBatchPrint, PunchAction.Add); 

                //Henry 2013-04-08 add for pagination
                GridViewBatchesPager.RecordCount = 0;
                GridViewBatchesPager.CurrentPageIndex = 1;
                this.PopulateSpotFineBatches();
            }
        }

        protected void GridViewBatches_SelectedIndexChanged(object sender, EventArgs e)
        {
            //for managing the spot fine batches supervisor on select must be able to change the user
            // Get the selected SFBIntNo
            Label lbl = (Label)this.GridViewBatches.Rows[this.GridViewBatches.SelectedIndex].Cells[0].Controls[1];
            this.sfbIntNo = Convert.ToInt32(lbl.Text);
            this.ViewState.Add("sfbIntNo", this.sfbIntNo);

            this.PopulateUserChange(Convert.ToInt32(ddlAuthority.SelectedValue));

            this.pnlChangeUser.Visible = true;


            //// Reset necessary controls
            //lblError.Text = string.Empty;

            //// Check for a completed date
            //this.sfbCompleteDate = this.GridViewBatches.SelectedRow.Cells[3].Text.Trim();

            ////BD 3858 check that the user has selected either summons or spot fine only when it is a non completed batch.
            ////BD have moved this to the notices grid as the control for what to display. Defualt to show spot fines.
            ////if (chkSummons.Checked || chkSpotFine.Checked || !Helper_Web.IsWebStringEmpty(this.sfbCompleteDate))
            ////{
            //this.pnlButtons.Visible = false;
            ////this.pnlDetails.Visible = false;
            //this.pnlPrintBatch.Visible = false;
            //this.pnlFixNotice.Visible = false;
            //this.pnlNotices.Visible = false;

            ////BD set the default view to show spot fines
            //chkSpotFine.Checked = true;
            ////BD need to set the sort order drop down items
            //CreateSortList("SpotFine");
            ////BD 3858 set the text of the label for what has been selected
            ////if (chkSpotFine.Checked && !chkSummons.Checked)
            ////    this.lblNoticeHeader.Text = "Spot Fine Listing";

            ////if (!chkSpotFine.Checked && chkSummons.Checked)
            ////    this.lblNoticeHeader.Text = "AG Listing";

            ////BD not allowed to show both at the same time    
            ////if (chkSpotFine.Checked && chkSummons.Checked)
            ////    this.lblNoticeHeader.Text = "All Notices";

            //this.ViewState.Remove("PrintBatch");

            //// Get the selected SFBIntNo
            //Label lbl = (Label)this.GridViewBatches.Rows[this.GridViewBatches.SelectedIndex].Cells[0].Controls[1];
            //this.sfbIntNo = Convert.ToInt32(lbl.Text);
            //this.ViewState.Add("sfbIntNo", this.sfbIntNo);



            ////  Check if the batch has been completed and only populate the next Notice GridView if the batch is incomplete
            //if (Helper_Web.IsWebStringEmpty(this.sfbCompleteDate))
            //{
            //    //this.processBatch = true;

            //    // See if there are any more notices that need receipts
            //    //SpotFineBatchNoticesDB db = new SpotFineBatchNoticesDB(this.connectionstring);
            //    //DataSet ds = db.SpotFineBatch_NoticeGetNextDS(autIntNo, sfbIntNo);

            //    // There are still notices to process
            //    //if (ds.Tables[0].Rows.Count != 0)
            //    //{
            //    //    DataRow drSFBNGN = ds.Tables[0].Rows[0];

            //    //    this.sfbnIntNo = Convert.ToInt32(drSFBNGN["SFBNIntNo"]);
            //    //    this.notTicketNo = drSFBNGN["NotTicketNo"].ToString();
            //    //    this.notOffenceDate = drSFBNGN["NotOffenceDate"].ToString();
            //    //    this.sfbnRctNumber = drSFBNGN["SFBNRctNumber"].ToString();
            //    //    this.sfbnRTAmount = drSFBNGN["SFBNRTAmount"].ToString();

            //    //    this.PopulateFixNoticePanel();
            //    //    this.pnlButtons.Visible = false;
            //    //}
            //    //else // There are no more notices that require receipts
            //    //{
            //    //this.processBatch = false;
            //    //this.lblError.Visible = true;
            //    //this.lblError.Text = "There are no more notices to process in this batch, please select the next batch for processing";
            //    this.pnlNotices.Visible = false;
            //    this.GridViewNotices.Visible = false;
            //    this.pnlFixNotice.Visible = false;

            //    //The user will always be allowed to update the print batch allocation
            //    //bool isCompleted = Helper_Web.IsWebStringEmpty(this.GridViewBatches.SelectedRow.Cells[3].Text);
            //    //this.btnCompleteBatch.Visible = isCompleted;
            //    //this.btnUpdatePrintSchedule.Visible = isCompleted;
            //    this.pnlButtons.Visible = true;

            //    this.PopulateSpotFineBatchNotices();
            //    //}

            //    //this.ViewState.Add("processBatch", processBatch);
            //}
            //else
            //{
            //    //this.lblError.Visible = true;
            //    //this.lblError.Text = string.Format("Batch {0} has already been completed, please select another batch for processing or select the print file below for printing.", lbl.Text);

            //    this.pnlNotices.Visible = false;
            //    this.GridViewNotices.Visible = false;
            //    this.pnlFixNotice.Visible = false;

            //    //The user will always be allowed to update the print batch allocation
            //    //bool isCompleted = Helper_Web.IsWebStringEmpty(this.GridViewBatches.SelectedRow.Cells[3].Text);
            //    //this.btnCompleteBatch.Visible = isCompleted;
            //    //this.btnUpdatePrintSchedule.Visible = isCompleted;
            //    this.pnlButtons.Visible = true;

            //    this.PopulateSpotFineBatchNotices();

            //    this.PopulatePrintBatch(this.sfbIntNo);
            //}
            ////}
            ////else
            ////{
            ////    this.lblError.Visible = true;
            ////	this.lblError.Text = "Please select either Summons or Spot Fine data to view.";
            ////}
        }

        public void CreateSortList(string listName)
        {
            ListItem ddlRN = new ListItem(); //Receipt Number
            ddlRN.Text = (string)GetLocalResourceObject("ddlSortOrder.Items");
            ddlRN.Value = "SFBNRctNumber";

            ListItem ddlNN = new ListItem(); //Notice Number
            ddlNN.Text = (string)GetLocalResourceObject("ddlSortOrder.Items1");
            ddlNN.Value = "NotTicketNo";

            ListItem ddlAN = new ListItem(); //AG Number
            ddlAN.Text = (string)GetLocalResourceObject("ddlSortOrder.Items2");
            ddlAN.Value = "AG Numner";

            ListItem ddlSN = new ListItem(); //Summons Number
            ddlSN.Text = (string)GetLocalResourceObject("ddlSortOrder.Items3");
            ddlSN.Value = "Summons Numner";

            ddlSortOrder.Items.Clear();

            if (listName == "SpotFine")
            {
                //must create options for spot fine sort
                ddlSortOrder.Items.Add(ddlNN);
                ddlSortOrder.Items.Add(ddlRN);

            }
            else if (listName == "AG")
            {
                //must create options for AG sort
                ddlSortOrder.Items.Add(ddlAN);
                ddlSortOrder.Items.Add(ddlRN);
                ddlSortOrder.Items.Add(ddlSN);
            }
        }

        protected void GridViewNotices_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Only execute this section if user clicked Select to fix a specific notice, otherwise he has selected another page
            if (e.CommandName.Equals("Select"))
            {
                //this.processBatch = false;
                //this.ViewState.Add("processBatch", processBatch);

                this.GridViewNotices.SelectedIndex = Convert.ToInt32(e.CommandArgument);
                int nRow = GridViewNotices.SelectedIndex;

                this.notIntNo = (int)this.GridViewNotices.DataKeys[nRow].Value;
                this.sfbnIntNo = (int)this.GridViewNotices.DataKeys[nRow].Value;

                Label lblFullNoticeNo = (Label)this.GridViewNotices.SelectedRow.Cells[1].FindControl("Label1");
                Label lblOffenceDate = (Label)this.GridViewNotices.SelectedRow.Cells[3].FindControl("Label3");
                Label lblRctNumber = (Label)this.GridViewNotices.SelectedRow.Cells[4].FindControl("Label4");
                Label lblRTAmount = (Label)this.GridViewNotices.SelectedRow.Cells[5].FindControl("Label5");

                this.notTicketNo = lblFullNoticeNo.Text;
                this.notOffenceDate = lblOffenceDate.Text;
                this.sfbnRctNumber = lblRctNumber.Text;
                this.sfbnRTAmount = lblRTAmount.Text;

                this.ViewState.Add("notIntNo", notIntNo);
                this.ViewState.Add("sfbnIntNo", sfbnIntNo);

                //this.pnlNotices.Visible = false;
                //this.pnlFixNotice.Visible = true;

                this.PopulateFixNoticePanel();
            }
        }

        protected void btnRejectNotice_Click(object sender, EventArgs e)
        {
            //lblError.Visible = false;

            //this.txtRctNumber.Text = "0";

            //if (processBatch == false)
            //{
            //    this.ProcessOneNotice();
            //    this.pnlFixNotice.Visible = false;
            //}
            //else
            //{
            //    //this.pnlFixNotice.Visible = false;
            //    this.ProcessBatchOfNotices();
            //}
        }

        protected void btnCancelNotice_Click(object sender, EventArgs e)
        {
            //lblError.Visible = false;

            //this.txtRctNumber.Text = "0";

            //if (processBatch == false)
            //{
            //    this.ProcessOneNotice();
            //    this.pnlFixNotice.Visible = false;
            //}
            //else
            //{
            //    //this.pnlFixNotice.Visible = false;
            //    this.ProcessBatchOfNotices();
            //}

            //this.pnlFixNotice.Visible = false;
        }


        protected void btnAcceptNotice_Click(object sender, EventArgs e)
        {
            //lblError.Visible = false;

            //int iRctNumberLength = txtRctNumber.Text.Trim().Length;
            //if (iRctNumberLength == 0)
            //{
            //    //sfbnRctNumber.Value = "0";
            //    lblError.Visible = true;
            //    lblError.Text = "Please enter a valid receipt number";
            //    return;
            //}
            //else if (txtRctNumber.Text.Trim().Equals("0"))
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "Please enter a valid receipt number for this receipt to be accepted. Use 'Reject' for receipts that are not for spot fines";
            //    return;
            //}

            //// If the user wants to update only one notice
            //if (!processBatch)
            //{
            //    this.ProcessOneNotice();
            //    this.pnlFixNotice.Visible = false;
            //}// The user is processing a batch of notices
            //else
            //{
            //    this.ProcessBatchOfNotices();
            //    //this.pnlFixNotice.Visible = false;
            //}
        }

        protected void btnUpdateBatch_Click(object sender, EventArgs e)
        {
            if (ddlUserChange.SelectedIndex < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                lblError.Visible = true;
                return;
            }
            else if (txtReason.Text.Trim().Length == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                lblError.Visible = true;
                return;
            }
            lblError.Visible = false;
            this.sfbIntNo = (int)this.ViewState["sfbIntNo"];
            SpotFineBatchDB sfb = new SpotFineBatchDB(this.connectionstring);
            int updateU = sfb.UpdateSpotFineBatch(this.sfbIntNo, int.Parse(ddlUserChange.SelectedValue), this.login, txtReason.Text);
            if (updateU < 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                lblError.Visible = true;
            }
            else if (updateU > 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ManageNoticeAndReceiptBatchPrint, PunchAction.Change); 
            }

            // Refresh the Batch GridView
            this.pnlChangeUser.Visible = false;
            //Henry 2013-04-08 add for pagination

            Stalberg.TMS.UserDB selectedUser = new UserDB(connectionstring);
            Stalberg.TMS.UserDetails selectedUserDetails = new UserDetails();
            selectedUserDetails = selectedUser.GetUserDetails(Convert.ToInt32(ddlUserChange.SelectedValue));

            if(GridViewBatches.Rows.Count == 1)
            {
                if (selectedUserDetails.UserLoginName != this.login)
                {
                    GridViewBatchesPager.CurrentPageIndex--;
                }
            }

            //GridViewBatchesPager.RecordCount = 0;
            //GridViewBatchesPager.CurrentPageIndex = 1;
            this.PopulateSpotFineBatches();
        }

        protected void btnCancelBatch_Click(object sender, EventArgs e)
        {
            this.pnlChangeUser.Visible = false;
        }

        protected void btnUpdateNotice_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;

            //int iRctNumberLength = txtRctNumber.Text.Trim().Length;
            //if (iRctNumberLength == 0)
            //{
            //    //sfbnRctNumber.Value = "0";
            //    lblError.Visible = true;
            //    lblError.Text = "Please enter a valid receipt number";
            //    return;
            //}
            //else if (txtRctNumber.Text.Trim().Equals("0"))
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "Please enter a valid receipt number for this receipt to be accepted. Use 'Reject' for receipts that are not for spot fines";
            //    return;
            //}

            //BD no need to process a batch of notices they would only now and again be updating one notice.

            // If the user wants to update only one notice
            //if (!processBatch)
            //{
            this.ProcessOneNotice();
            //this.pnlFixNotice.Visible = false;
            //}// The user is processing a batch of notices
            //else
            //{
            //this.ProcessBatchOfNotices();
            //this.pnlFixNotice.Visible = false;
            //}
        }

        protected void UpdateSpotFineBatchNotice()
        {
            SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);

            //int updSFBNIntNo = sfbn.UpdateSpotFineBatchNotice(this.sfbnIntNo, this.txtRctNumber.Text, this.login);
            //int updSFBNIntNo = sfbn.UpdateSpotFineBatchNotice(this.sfbnIntNo, this.txtRctNumber.Text, Convert.ToDecimal(this.lblRTAmount.Text), this.login);
        }

        protected void btnUpdatePrintSchedule_Click(object sender, EventArgs e)
        {

            //BD update SpotFineBatchNotices with which notices and receipts must be printed so the 
            //user can save and come back to create the print file
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {
                    int rIndex = gvNotices.RowIndex;
                    GridViewNotices.SelectedIndex = rIndex;
                    string sfbnIntNo = GridViewNotices.SelectedDataKey["SFBNIntNo"].ToString();

                    CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                    CheckBox cbReceipt = (CheckBox)gvNotices.FindControl("printReceipt");
                    Label lblRctNumber = (Label)gvNotices.FindControl("Label4");
                    Label lblAmount = (Label)gvNotices.FindControl("Label5");

                    SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);
                    int updSFBNIntNo = sfbn.UpdateSpotFineBatchNotice(Convert.ToInt32(sfbnIntNo), lblRctNumber.Text, Convert.ToDecimal(lblAmount.Text), cbNotice.Checked, cbReceipt.Checked, this.login);
                }
            }
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ManageNoticeAndReceiptBatchPrint, PunchAction.Change); 

        }

        protected void btnCompleteBatch_Click(object sender, EventArgs e)
        {
            //BD the main event here is the creation of the print file 
            //BD must create a seperate print file for both AG and Spot fine

            //BD No need to create seperate print files based on AG or spotfine as they will already have been seperated... 
            //Need to create on print file name but multple print time stamps and then split the grid based on time stamps
            //this way we have one print file when needed but can break it down into each time they changed the print allocation

            if (this.sfbIntNo == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                return;
            }

            //BD Must first update SpotFineBatchNotices with which notices and receipts must be printed and 
            //then can generate print file based on those results
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {

                    int rIndex = gvNotices.RowIndex;
                    GridViewNotices.SelectedIndex = rIndex;
                    string sfbnIntNo = GridViewNotices.SelectedDataKey["SFBNIntNo"].ToString();

                    CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                    CheckBox cbReceipt = (CheckBox)gvNotices.FindControl("printReceipt");
                    Label lblRctNumber = (Label)gvNotices.FindControl("Label4");
                    Label lblAmount = (Label)gvNotices.FindControl("Label5");

                    SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);
                    int updSFBNIntNo = sfbn.UpdateSpotFineBatchNotice(Convert.ToInt32(sfbnIntNo), lblRctNumber.Text, Convert.ToDecimal(lblAmount.Text), cbNotice.Checked, cbReceipt.Checked, this.login);
                }
            }

            // Mark the batch as completed and put the notices into print batches of 50
            // BD do we need to put them into 50? They will be broken into managable sizes on creation, also going to be showing them broken down by date.
            SpotFineBatchNoticesDB db = new SpotFineBatchNoticesDB(this.connectionstring);
            db.MarkBatchAsCompleted(this.sfbIntNo, this.login);

            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ManageNoticeAndReceiptBatchPrint, PunchAction.Change); 

            //Henry 2013-04-08 add for pagination
            grdPrintBatchPager.RecordCount = 0;
            grdPrintBatchPager.CurrentPageIndex = 1;
            // Show the print batches panel and grid
            this.PopulatePrintBatch(this.sfbIntNo);

            //this.pnlButtons.Visible = false;
            //this.pnlFixNotice.Visible = false;
            //this.pnlDetails.Visible = false;
            //this.pnlNotices.Visible = false;

            // Repopulate the batch grid
            //this.PopulateSpotFineBatches();
        }

        private void PopulatePrintBatch(int sfbIntNo)
        {
            // Get the data & populate the grid
            SpotFineBatchNoticesDB db = new SpotFineBatchNoticesDB(this.connectionstring);
            int totalCount = 0;
            DataSet ds = db.GetPrintBatches(sfbIntNo, grdPrintBatchPager.PageSize, grdPrintBatchPager.CurrentPageIndex, out totalCount);

            this.grdPrintBatch.DataSource = ds;
            this.grdPrintBatch.DataKeyNames = new string[] { "SFBNPrintFile", "UserCompleteDate" };
            this.ViewState.Add("PrintBatch", ds);
            this.grdPrintBatch.DataBind();

            grdPrintBatchPager.RecordCount = totalCount;

            this.pnlPrintBatch.Visible = true;
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.PopulateSpotFineBatches();
            //need to update the list of users before can populate spot fine batches
            this.PopulateUsers(Convert.ToInt32(ddlAuthority.SelectedValue));
        }

        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Henry 2013-04-08 add for pagination
            GridViewBatchesPager.RecordCount = 0;
            GridViewBatchesPager.CurrentPageIndex = 1;
            this.PopulateSpotFineBatches();

            //also must populate the spot fine priority date
            Stalberg.TMS.UserDB selectedUser = new UserDB(connectionstring);
            Stalberg.TMS.UserDetails selectedUserDetails = new UserDetails();
            selectedUserDetails = selectedUser.GetUserDetails(Convert.ToInt32(ddlUsers.SelectedValue));

            //Edge 2013-09-12 modify
            //dtpPriorityDate.Text = string.Format("{0:yyyy-MM-dd}", selectedUserDetails.SpotFinePriorityDate);
            if (!string.IsNullOrWhiteSpace(selectedUserDetails.SpotFinePriorityDate))
            {
                dtpPriorityDate.Text = DateTime.Parse(selectedUserDetails.SpotFinePriorityDate).ToString("yyyy-MM-dd");
            }
            else
            {
                dtpPriorityDate.Text = "";
            }
        }

        protected void btnPrintRegister_Click(object sender, EventArgs e)
        {
            // TODO: Print the register (?)
        }

        protected void btnPrintBatch_Click(object sender, EventArgs e)
        {
            if (this.sfbIntNo == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                return;
            }
           
            //Henry 2013-04-08 add for pagination
            grdPrintBatchPager.RecordCount = 0;
            grdPrintBatchPager.CurrentPageIndex = 1;
            // Show the print viewer
            this.PopulatePrintBatch(this.sfbIntNo);
        }

        private void PrintBatch(string printName)
        {
            PageToOpen page = new PageToOpen(this, "CaptureSpotFineBatchViewer.aspx");
            page.AddParameter("Batch", printName);

            Helper_Web.BuildPopup(page);
        }

        protected void grdPrintBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            string printName = this.grdPrintBatch.SelectedDataKey["SFBNPrintFile"].ToString();
            //BD need to also handle the completion date so that we can control what they print in smaller numbers as they change their mind
            DateTime userCompleteDate = Convert.ToDateTime(this.grdPrintBatch.SelectedDataKey["UserCompleteDate"].ToString());
            string printDate = this.grdPrintBatch.SelectedRow.Cells[1].Text.Trim();

            // Mark the print batch as having been printed
            if (string.IsNullOrEmpty(printDate) || printDate.Equals("&nbsp;", StringComparison.CurrentCultureIgnoreCase))
                this.MarkBatchAsPrinted(printName);

            //dont need the panel with the notices showing, must enable or disable the create new batch button.
            //this.PopulateSpotFineBatches();
            btnNewBatch.Visible = true;
            this.ViewState.Remove("PrintBatch");
            //Henry 2013-04-08 add for pagination
            grdPrintBatchPager.RecordCount = 0;
            grdPrintBatchPager.CurrentPageIndex = 1;
            this.PopulatePrintBatch(this.sfbIntNo);

            PageToOpen page = new PageToOpen(this, "CaptureSpotFineBatchViewer.aspx");
            page.AddParameter("Batch", printName);
            page.AddParameter("UserCompleteDate", userCompleteDate);
            Helper_Web.BuildPopup(page);
        }

        protected void grdPrintBatch_OnRowDeleting(object sender, EventArgs e)
        {
            //BD 3858 - allow a supervisor to delete a print file for a batch so the user can go 
            //back and recreate the print file. This is only allowed to happen if the file has not been printed yet.

            //1. Need to delete the print file name from the SpotFineBatch_notice table
            //2. Need to delete the completed date out of the SpotFineBatch table
            //3. must audit trail the process

            //string printFile = string.Empty; //grdPrintBatch.["SFBNPrintFile"].ToString();
            //SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);
            //sfbn.DeletePrintFile(printFile, this.login);

            //must check which view and data I must show after delete.
            //this.PopulateSpotFineBatches();
            //this.ViewState.Remove("PrintBatch");

            //bool isCompleted = Helper_Web.IsWebStringEmpty(this.GridViewBatches.SelectedRow.Cells[3].Text);
            //this.btnCompleteBatch.Visible = isCompleted;
            //this.btnUpdatePrintSchedule.Visible = isCompleted;
            //this.pnlButtons.Visible = true;

            //this.PopulateSpotFineBatchNotices();
        }

        protected void grdPrintBatch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "pfDelete")
            {
                string printFile = e.CommandArgument.ToString();
                SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);
                sfbn.DeletePrintFile(printFile, this.login);

                //Henry 2013-04-08 add for pagination
                GridViewBatchesPager.RecordCount = 0;
                GridViewBatchesPager.CurrentPageIndex = 1;

                //must check which view and data I must show after delete.
                this.PopulateSpotFineBatches();
                this.ViewState.Remove("PrintBatch");

                //Henry 2013-04-08 add for pagination
                grdPrintBatchPager.RecordCount = 0;
                grdPrintBatchPager.CurrentPageIndex = 1;
                this.PopulatePrintBatch(this.sfbIntNo);

                //bool isCompleted = Helper_Web.IsWebStringEmpty(this.GridViewBatches.SelectedRow.Cells[3].Text);
                //this.btnCompleteBatch.Visible = isCompleted;
                //this.btnUpdatePrintSchedule.Visible = isCompleted;
                //this.pnlButtons.Visible = true;

                //this.PopulateSpotFineBatchNotices();
            }

        }

        private void MarkBatchAsPrinted(string printName)
        {
            SpotFineBatchNoticesDB db = new SpotFineBatchNoticesDB(this.connectionstring);
            db.MarkBatchAsPrinted(printName, this.login);
        }

        protected void chkSpotFine_CheckedChanged(object sender, EventArgs e)
        {
            chkSummons.Checked = false;
            CreateSortList("SpotFine");
            lblNoticeHeader.Text = (string)GetLocalResourceObject("lblNoticeHeader.Text");
            btnNoticeSelect.Enabled = true;
            //2013-04-08 add by Henry for pagination
            GridViewNoticesPager.CurrentPageIndex = 1;
            GridViewNoticesPager.RecordCount = 0;
            PopulateSpotFineBatchNotices();
        }
        protected void chkSummons_CheckedChanged(object sender, EventArgs e)
        {
            chkSpotFine.Checked = false;
            CreateSortList("AG");
            lblNoticeHeader.Text = (string)GetLocalResourceObject("lblNoticeHeader.Text1");
            //dont need to be able to select the receipt select all button
            btnNoticeSelect.Enabled = false;

            //2013-04-08 add by Henry for pagination
            GridViewNoticesPager.CurrentPageIndex = 1;
            GridViewNoticesPager.RecordCount = 0;
            PopulateSpotFineBatchNotices();
        }
        protected void GridViewNotices_Sorting(object sender, GridViewSortEventArgs e)
        {
            //BD sorting of notice grid
            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;

                //2013-04-08 add by Henry for pagination
                GridViewNoticesPager.CurrentPageIndex = 1;
                GridViewNoticesPager.RecordCount = 0;
                SortGridView(sortExpression, "DESC");
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;

                //2013-04-08 add by Henry for pagination
                GridViewNoticesPager.CurrentPageIndex = 1;
                GridViewNoticesPager.RecordCount = 0;
                SortGridView(sortExpression, "ASC");
            }
        }

        private void SortGridView(string sortExpression, string direction)
        {

            this.autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);

            Stalberg.TMS.SpotFineBatchNoticesDB sfbn = new Stalberg.TMS.SpotFineBatchNoticesDB(connectionstring);
            int totalCount = 0; //2013-04-08 add by Henry for pagination
            DataSet dsSFBN = sfbn.GetSpotFineBatchNoticesListDS(this.autIntNo, this.sfbIntNo, this.chkSpotFine.Checked, this.chkSummons.Checked,
                sortExpression, direction,
                GridViewBatchesPager.PageSize, GridViewBatchesPager.CurrentPageIndex, out totalCount);
            GridViewNotices.DataSource = dsSFBN;
            GridViewNotices.DataBind();

        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }


        protected void ddlSortOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            //the sort direction does not matter here as it is being handled in the view state.
            GridViewNotices.Sort(ddlSortOrder.SelectedValue.ToString(), SortDirection.Ascending);
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            //need to find the value entered or part there of in thegrid, muts just move to that position in the grid. 
            //the value entered is based on the sort order variable
            int iCheck = 0;
            lblError.Text = string.Empty;
            //need to reset the highlighted ones
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {
                    if (gvNotices.BackColor == System.Drawing.Color.Yellow)
                    {
                        gvNotices.BackColor = System.Drawing.Color.Empty;
                    }
                }

            }
            //need to set the first highlighted one
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {
                    Label lblRctNumber = (Label)gvNotices.FindControl("Label4");
                    Label lblNoticeNumber = (Label)gvNotices.FindControl("Label1");

                    //if (txtFind.Text == lblRctNumber.Text || txtFind.Text == lblNoticeNumber.Text)
                    if (lblRctNumber.Text.Contains(txtFind.Text) || lblNoticeNumber.Text.Contains(txtFind.Text))
                    {
                        //set the variable check that we do actually find a value
                        iCheck = 1;
                        //found the row we are looking for, must now hightlight it.
                        gvNotices.BackColor = System.Drawing.Color.Yellow;
                        break;
                    }

                }
            }
            if (iCheck == 0)
            {
                //must tell the user the value was not found.
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            }
        }
        protected void btnNoticeSelect_Click(object sender, EventArgs e)
        {
            // select or deselect all notices

            //the button has not been clicked as of yet or everything has been deselected
            if (Session["NoticeSelect"] == null || Session["NoticeSelect"].ToString() == "1")
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                        cbNotice.Checked = true;
                    }

                }
                Session["NoticeSelect"] = "0";
                btnNoticeSelect.Text = (string)GetLocalResourceObject("btnNoticeSelect.Text1");
            }
            else
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                        cbNotice.Checked = false;
                    }

                }
                Session["NoticeSelect"] = "1";
                btnNoticeSelect.Text = (string)GetLocalResourceObject("btnNoticeSelect.Text");
            }

        }
        protected void btnReceiptSelect_Click(object sender, EventArgs e)
        {
            // select or deselect all Receipts

            //the button has not been clicked as of yet or everything has been deselected
            if (Session["ReceiptSelect"] == null || Session["ReceiptSelect"].ToString() == "1")
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printReceipt");
                        cbNotice.Checked = true;
                    }

                }
                Session["ReceiptSelect"] = "0";
                btnReceiptSelect.Text = (string)GetLocalResourceObject("btnReceiptSelect.Text1");
            }
            else
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printReceipt");
                        cbNotice.Checked = false;
                    }

                }
                Session["ReceiptSelect"] = "1";
                btnReceiptSelect.Text = (string)GetLocalResourceObject("btnReceiptSelect.Text");
            }
        }

        //Henry 2013-04-08 add for pagination
        protected void GridViewBatchesPager_PageChanged(object sender, EventArgs e)
        {
            this.pnlNotices.Visible = false;
            this.GridViewNotices.Visible = false;
            this.pnlChangeUser.Visible = false;
            this.pnlButtons.Visible = false;
            this.pnlPrintBatch.Visible = false;

            autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);
            this.PopulateSpotFineBatches();
        }

        //Henry 2013-04-08 add for pagination
        protected void GridViewNoticesPager_PageChanged(object sender, EventArgs e)
        {
            PopulateSpotFineBatchNotices();
        }

        //Henry 2013-04-08 add for pagination
        protected void grdPrintBatchPager_PageChanged(object sender, EventArgs e)
        {
            PopulatePrintBatch(this.sfbIntNo);
        }
    }
}