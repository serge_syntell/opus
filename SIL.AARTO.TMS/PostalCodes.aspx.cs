using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;
using System.Collections.Generic;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Services;
using System.Transactions;


namespace Stalberg.TMS
{
    /// <summary>
    /// Represents an editor page for and Authority's details
    /// </summary>
    public partial class PostalCodes : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "PostalCodes.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Postal code maintenance";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;

            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlUpdateArea.Visible = false;
                this.pnlUpdateSuburb.Visible = false;
                this.btnOptAdd.Visible = false;
                this.btnOptAddSuburb.Visible = false;
                this.pnlUpdateArea.Visible = false;
                this.pnlUpdateSuburb.Visible = false;
                this.dgSuburb.Visible = false;
                this.btnOptDelete.Visible = false;
                this.btnOptDeleteSuburb.Visible = false;
                this.btnOptAddAreaSuburb.Visible = false;

                this.BindGrid();
            }
        }

        protected void BindGrid()
        {
            //int mtrIntNo = 0;

            Stalberg.TMS.AreaCodeDB pcList = new Stalberg.TMS.AreaCodeDB(connectionString);

            DataSet data = pcList.GetAllAreaCodesDS(txtSearch.Text);
            dgArea.DataSource = data;
            dgArea.DataKeyField = "AreaCode";
            try
            {
                dgArea.DataBind();
            }
            catch
            {
                dgArea.CurrentPageIndex = 0;
                dgArea.DataBind();
            }

            if (dgArea.Items.Count == 0)
            {
                dgArea.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgArea.Visible = true;
            }

            data.Dispose();

            this.btnOptAdd.Visible = false;
            this.btnOptAddSuburb.Visible = false;
            this.pnlUpdateArea.Visible = false;
            this.pnlUpdateSuburb.Visible = false;
            this.dgSuburb.Visible = false;
            this.btnOptDelete.Visible = false;
            this.btnOptDeleteSuburb.Visible = false;
            this.btnOptAddAreaSuburb.Visible = false;

        }

        protected void dgArea_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgArea.SelectedIndex = e.Item.ItemIndex;

            if (dgArea.SelectedIndex > -1)
            {
                if (e.CommandName.Equals("Edit"))
                {
                    string editAreaCode = Convert.ToString(dgArea.DataKeys[dgArea.SelectedIndex]);

                    this.ViewState.Add("editAreaCode", editAreaCode);
                    Session["prevPage"] = thisPageURL;

                    this.ShowAreaDetails(editAreaCode);

                    //Heidi 2014-04-01 added for maintaining multiple languages(5141)
                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                    List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editAreaCode, "AreaCodeLookup");
                    this.ucLanguageLookupUpdateAreaCode.DataBind(entityList);
                }
            }
        }

        protected void dgArea_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgArea.CurrentPageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        protected void ShowAreaDetails(string editAreaCode)
        {
            AreaCodeDB pc = new AreaCodeDB(connectionString);
            AreaDetails pcDetails = pc.GetAreaDetails(editAreaCode);

            this.txtAreaCode.Text = pcDetails.AreaCode;
            this.txtAreaDescr.Text = pcDetails.AreaDescr;

            this.btnOptAdd.Visible = true;
            this.btnOptAddSuburb.Visible = false;
            this.pnlUpdateArea.Visible = true;
            this.pnlUpdateSuburb.Visible = false;
            this.dgSuburb.Visible = false;
            this.btnOptDelete.Visible = true;
            this.btnOptDeleteSuburb.Visible = false;
            this.btnOptAddAreaSuburb.Visible = true;
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // prep for adding an area code to areacode table
            this.ViewState.Remove("editAreaCode");

            this.pnlUpdateArea.Visible = true;
            this.btnOptDelete.Visible = false;

            this.txtAreaCode.Text = string.Empty;
            this.txtAreaDescr.Text = string.Empty;

            //Heidi 2014-04-01 added for maintaining multiple languages(5141)
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupUpdateAreaCode.DataBind(entityList);
        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            bool isNew = false;
            //update areacode table selected row
            string areaCode = string.Empty;
            if (this.ViewState["editAreaCode"] != null)
            {
                areaCode = (string)this.ViewState["editAreaCode"];
            }
            else
            {
                areaCode = txtAreaCode.Text.Trim();
                this.ViewState.Add("editAreaCode", areaCode);
                isNew = true;
            }

            Stalberg.TMS.AreaCodeDB pcUpdate = new AreaCodeDB(connectionString);

            string areaDescr = this.txtAreaDescr.Text.Trim();

            using (ConnectionScope.CreateTransaction())
            {
                bool updAreaCode = pcUpdate.SaveArea(areaCode, areaDescr);

                //Heidi 2014-04-01 added for maintaining multiple languages(5141)
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdateAreaCode.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookupFkIsVarchar(areaCode, lgEntityList, "AreaCodeLookup", "AreaCode", "AreaDescr", this.login);

                if (updAreaCode != true)
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    if (isNew)
                    {

                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PostalCodesMaintenance, PunchAction.Add);

                    }
                    else
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PostalCodesMaintenance, PunchAction.Change);
                    }
                }

                ConnectionScope.Complete();
            }

            lblError.Visible = true;

            this.BindGrid();
        }

        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
            string areaCode = (string)this.ViewState["editAreaCode"];
            this.ViewState.Remove("editAreaCode");

            AreaCodeDB area = new Stalberg.TMS.AreaCodeDB(connectionString);
            string delAreaCode = area.DeleteArea(areaCode);

            switch (delAreaCode)
            {
                case "None":
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    break;
                case "Many":
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                    break;
                case "Link":
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                    break;
                case "Bad":
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    break;
                default:
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PostalCodesMaintenance, PunchAction.Delete);  
                    break;
            }
            this.lblError.Visible = true;

            this.BindGrid();
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
        //    if (dgArea.Visible.Equals(true))
        //    {
        //        //hide it
        //        dgArea.Visible = false;
        //        btnOptHide.Text = "Show list";
        //    }
        //    else
        //    {
        //        //show it
        //        dgArea.Visible = true;
        //        btnOptHide.Text = "Hide list";
        //    }
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            this.BindGrid();
            //PunchStats805806 enquiry PostalCodes
        }

         

        protected void dgArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            string editAreaCode = Convert.ToString(dgArea.DataKeys[dgArea.SelectedIndex]);

            this.ViewState.Add("editAreaCode", editAreaCode);
            Session["prevPage"] = thisPageURL;

            btnOptAddSuburb.Visible = false; //hide the add util the user has selected a suburb row
            this.btnOptAddAreaSuburb.Visible = true;

            this.BindSuburbGrid(editAreaCode);
        }

        protected void BindSuburbGrid(string areaCode)
        {
            Stalberg.TMS.SuburbDB subList = new Stalberg.TMS.SuburbDB(connectionString);

            DataSet data = subList.GetAllSuburbsForAreaDS(areaCode);
            dgSuburb.DataSource = data;
            dgSuburb.DataKeyField = "SubIntNo";

            try
            {
                dgSuburb.DataBind();
            }
            catch
            {
                dgSuburb.CurrentPageIndex = 0;
                dgSuburb.DataBind();
            }

            if (dgSuburb.Items.Count == 0)
            {
                this.dgSuburb.Visible = false;
                this.lblError.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            }
            else
            {
                this.dgSuburb.Visible = true;
            }

            data.Dispose();
            this.dgArea.Visible = false;
            this.dgSuburb.Visible = true;
            this.btnOptAdd.Visible = false;
            this.btnOptAddSuburb.Visible = false;
            this.pnlUpdateArea.Visible = false;
            this.pnlUpdateSuburb.Visible = false;
            this.btnOptDelete.Visible = false;
            this.btnOptDeleteSuburb.Visible = false;
            this.btnOptAddAreaSuburb.Visible = true;
        }

        protected void dgSuburb_ItemCommand(object source, DataGridCommandEventArgs e)
        {
        }

        protected void dgSuburb_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
        }
        
        protected void dgSuburb_SelectedIndexChanged(object sender, EventArgs e)
        {
            //store details of page in case of re-direct

            if (dgSuburb.SelectedIndex > -1)
            {
                int editSubIntNo = Convert.ToInt32(dgSuburb.DataKeys[dgSuburb.SelectedIndex]);
                this.ViewState.Add("editSubIntNo", editSubIntNo);
                Session["prevPage"] = thisPageURL;
                this.ShowSuburbDetails(editSubIntNo);

                //Heidi 2014-04-01 added for maintaining multiple languages(5141)
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editSubIntNo.ToString(), "SuburbLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);
            }
        }

        protected void ShowSuburbDetails(int subIntNo)
        {
            SuburbDB sub = new SuburbDB(connectionString);
            SuburbDetails subDetails = sub.GetSuburbDetails(subIntNo);

            this.txtSuburbCode.Text = subDetails.AreaCode;
            this.txtSubDescr.Text = subDetails.SubDescr;

            this.btnOptAdd.Visible = false;
            this.btnOptAddSuburb.Visible = true;
            this.pnlUpdateArea.Visible = false;
            this.pnlUpdateSuburb.Visible = true;
            this.btnUpdateSuburb.Visible = true;
            this.btnSuburbAdd.Visible = false;
            this.btnOptDelete.Visible = false;
            this.btnOptDeleteSuburb.Visible = true;
            this.btnOptAddAreaSuburb.Visible = false;
        }

        protected void btnOptAddSuburb_Click(object sender, EventArgs e)
        {
            // allow transactions to be added to the database table
            this.ViewState.Remove("editSuburbCode");

            this.pnlUpdateSuburb.Visible = true;
            this.btnUpdateSuburb.Visible = false;
            this.btnSuburbAdd.Visible = true;
            this.pnlUpdateArea.Visible = false;
            this.btnOptDelete.Visible = false;
            this.txtSuburbCode.Text = Convert.ToString(this.ViewState["editAreaCode"]);
            this.txtSubDescr.Text = string.Empty;

            //Heidi 2014-04-01 added for maintaining multiple languages(5141)
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupUpdate.DataBind(entityList);
        }

        protected void btnUpdateSuburb_Click(object sender, EventArgs e)
        {
            //update suburb table
            string suburbCode = string.Empty;
            int subIntNo = Int32.Parse(this.ViewState["editSubIntNo"].ToString());
            if (this.ViewState["editSuburbCode"] != null)
            {
                suburbCode = (string)this.ViewState["editSuburbCode"];
            }
            else
            {
                suburbCode = txtSuburbCode.Text.Trim();
                this.ViewState.Add("editSuburbCode", suburbCode);
            }

            //check that the area codes are identical
            if((string)this.ViewState["editSuburbCode"] != (string)this.ViewState["editAreaCode"])
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                this.lblError.Visible = true;
                return;
            }

            string subDescr = this.txtSubDescr.Text.Trim();

            Stalberg.TMS.SuburbDB subSave = new SuburbDB(connectionString);

            using (ConnectionScope.CreateTransaction())
            {
                int checkInt = subSave.SaveSuburb(suburbCode, subDescr, login, subIntNo);

                //Heidi 2014-04-01 added for maintaining multiple languages(5141)
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(subIntNo, lgEntityList, "SuburbLookup", "SubIntNo", "SubDescr", this.login);

                //different returns mean differnt things
                switch (checkInt)
                {
                    case -1:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                        break;
                    case -2:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                        break;
                    case 0:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                        break;
                    default:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text13");

                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PostalCodesMaintenance, PunchAction.Change);
                        break;
                }

                ConnectionScope.Complete();
            }
            this.lblError.Visible = true;

            this.BindSuburbGrid(ViewState["editAreaCode"].ToString());

        }

        protected void btnSuburbAdd_Click(object sender, EventArgs e)
        {
            this.lblError.Text = string.Empty;

            SuburbDB sub = new SuburbDB(connectionString);

            string areaCode = (string)txtSuburbCode.Text.Trim();
            string subDescr = (string)txtSubDescr.Text.Trim();

            using (ConnectionScope.CreateTransaction())
            {
                int subIntNo = sub.AddSuburb(areaCode, subDescr, login);

                //Heidi 2014-04-01 added for maintaining multiple languages(5141)
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(subIntNo, lgEntityList, "SuburbLookup", "SubIntNo", "SubDescr", this.login);

                if (subIntNo <= 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text15");

                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PostalCodesMaintenance, PunchAction.Add);
                }

                ConnectionScope.Complete(); //Heidi 2014-04-01 added for maintaining multiple languages(5141)
            }
            this.lblError.Visible = true;

            this.BindSuburbGrid(ViewState["editAreaCode"].ToString());

        }
        protected void btnOptDeleteSuburb_Click(object sender, EventArgs e)
        {
            int subIntNo = Int32.Parse(this.ViewState["editSubIntNo"].ToString());
            this.ViewState.Remove("editSubIntNo");
            using (TransactionScope scope = new TransactionScope())
            {
                //Heidi 2014-04-01 added for maintaining multiple languages(5141)
                SuburbLookupService suburbLookUpService = new SuburbLookupService();
                suburbLookUpService.Delete(suburbLookUpService.GetBySubIntNo(subIntNo));

                SuburbDB sub = new Stalberg.TMS.SuburbDB(connectionString);
                int delSubIntNo = sub.DeleteSuburb(subIntNo);

                switch (subIntNo)
                {
                    case -1:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text16");
                        break;
                    //case "Many":
                    //    lblError.Text = "Unable to delete this area code - it has been linked to one or more suburbs";
                    //    break;
                    case -2:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text17");
                        break;
                    default:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text18");

                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PostalCodesMaintenance, PunchAction.Delete);
                        break;
                }

                scope.Complete();
            }

            this.lblError.Visible = true;

            this.BindSuburbGrid(ViewState["editAreaCode"].ToString());

        }
        protected void btnOptAddAreaSuburb_Click(object sender, EventArgs e)
        {
            // allow transactions to be added to the database table
            this.ViewState.Remove("editSuburbCode");

            this.pnlUpdateSuburb.Visible = true;
            this.btnUpdateSuburb.Visible = false;
            this.btnSuburbAdd.Visible = true;
            this.pnlUpdateArea.Visible = false;
            this.btnOptDelete.Visible = false;
            this.txtSuburbCode.Text = Convert.ToString(this.ViewState["editAreaCode"]);
            this.txtSubDescr.Text = string.Empty;

            //Heidi 2014-04-01 added for maintaining multiple languages(5141)
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupUpdate.DataBind(entityList);
        }
}
}
