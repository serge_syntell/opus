<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.PrintNoticeOfWOA" Codebehind="PrintNoticeOfWOA.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body style="margin:0" background="<%=backgroundImage %>" >
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                </td>
                <td valign="top" align="center" width="100%" colspan="1">
                    &nbsp;<asp:UpdatePanel ID="udpPrintNoticeOfWOA" runat="server">
                        <ContentTemplate>
                            <table style="width: 93%">
                                <tr>
                                    <td colspan="3" style="text-align: center" align="center">
                                            <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Height="22px" Text="<%$Resources:lblPageName.Text %>"></asp:Label><br />
                                        <br />
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                            <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 30%">
                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="214px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px" DataTextField="AutName" DataValueField="AutIntNo">
                                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 100px">
                                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="padding: 10,0,0,0; text-align:center">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Font-Names="Helvetica"
                                            Font-Size="X-Small" GridLines="Vertical" OnRowCommand="GridView1_RowCommand" AllowPaging="False" Caption="<%$Resources:gvPrintNotice.Caption %>" OnDataBound="GridView1_DataBound" Height="1px" CssClass="Normal" ShowFooter="True" Width="608px">
                                            <FooterStyle CssClass="CartListFooter" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                                            <Columns>
                                                <asp:CheckBoxField DataField="SumNoticeOfWOAPrinted" HeaderText="<%$Resources:gvPrintNotice.HeaderText %>" SortExpression="SumNoticeOfWOAPrinted" />
                                                <asp:CheckBoxField DataField="SumNoticeOfWOAPosted" HeaderText="<%$Resources:gvPrintNotice.HeaderText1 %>" SortExpression="SumNoticeOfWOAPosted" />
                                                <asp:BoundField DataField="SumNoticeOfWOAPrintFile" HeaderText="<%$Resources:gvPrintNotice.HeaderText2 %>" SortExpression="SumNoticeOfWOAPrintFile" />
                                                <asp:ButtonField CommandName="cmdPrint" Text="<%$Resources:gvPrintNoticeItem.Text %>" >
                                                    <ItemStyle ForeColor="#8080FF" />
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="cmdUpdatePrintStatus" Text="<%$Resources:gvPrintNoticeItem.Text1 %>" >
                                                    <ItemStyle ForeColor="#8080FF" />
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="cmdPost" Text="<%$Resources:gvPrintNoticeItem.Text2 %>" >
                                                    <ItemStyle ForeColor="#8080FF" />
                                                </asp:ButtonField>
                                            </Columns>
                                        </asp:GridView>
                                        <pager:AspNetPager id="GridView1Pager" runat="server" 
                                            showcustominfosection="Right" width="608px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"   PageSize="10" onpagechanged="GridView1Pager_PageChanged"  UpdatePanelId="udpPrintNoticeOfWOA"
                                    ></pager:AspNetPager>
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <br />
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="udpPrintNoticeOfWOA">
                        <ProgressTemplate>
                            <img src="Images/ig_progressIndicator.gif" />
                            <asp:Label ID="Label1" runat="server" Text="<%$Resources:lblWait.Text %>"></asp:Label>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    &nbsp;
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                    &nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
