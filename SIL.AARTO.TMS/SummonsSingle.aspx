<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SummonsSingle" Codebehind="SummonsSingle.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-Strict.dtd">

<%@ Register Src="RepresentationOnTheFly.ascx" TagName="RepresentationOnTheFly" TagPrefix="uc2" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center" style="width: 182px">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1" class="Normal">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Height="40px" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                        </p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="upd" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblError" CssClass="NormalRed" runat="server" />
                            <!-- Search Criteria Panel -->
                            <asp:Panel ID="pnlSearch" runat="server" Width="100%" CssClass="Normal">
                                <table id="tableCriteria" style="border-style: none;">
                                    <tr>
                                        <td valign="top" class="NormalBold">
                                            <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblRegno.Text %>"></asp:Label> 
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="txtRegno" runat="server" Width="100%" CssClass="Normal" /></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="NormalBold">
                                            <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblIDNumber.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtIDNumber" runat="server" Width="100%" CssClass="Normal" /></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="NormalBold">
                                            <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblSequenceNum.Text %>"></asp:Label><p style="text-align: center" class="NormalBold">
                                                <asp:Label ID="Label17" runat="server" Text="<%$Resources:lblor.Text %>"></asp:Label></p>
                                        </td>
                                        <td valign="top">
                                            <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="NormalBold">
                                            <asp:Label ID="Label8" runat="server" Text="<%$Resources:lblTicketNo.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtTicketNo" runat="server" Width="100%" CssClass="Normal" /></td>
                                    </tr>
                                   <%-- <tr>
                                        <td valign="top" class="NormalBold">
                                            At Roadblock</td>
                                        <td>
                                            <asp:CheckBox ID="chkRoadblock" runat="server" Checked="false" />
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td colspan="2" style="text-align: right;" valign="top">
                                            <asp:Button ID="btnSearch" runat="server" Text="<%$Resources:btnSearch.Text %>" OnClick="btnSearch_Click"
                                                CssClass="NormalButton" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- Search Results Panel -->
                            <asp:Panel ID="pnlResults" runat="server" Width="100%" CssClass="Normal">
                                <asp:GridView ID="grdNotices" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CellPadding="3" CssClass="Normal" OnPageIndexChanging="grdNotices_PageIndexChanging"
                                    OnSelectedIndexChanged="grdNotices_SelectedIndexChanged" ShowFooter="True" 
                                    PageSize="15" onrowcreated="grdNotices_RowCreated">
                                    <FooterStyle CssClass="CartListHead" />
                                    <Columns>
                                        <asp:BoundField DataField="NotIntNo" HeaderText="NotIntNo" Visible="False" />
                                        <asp:BoundField DataField="ChgIntNo" HeaderText="ChgIntNo" Visible="False" />
                                        <asp:BoundField DataField="TicketNo" HeaderText="<%$Resources:grdNotices.HeaderText %>" />
                                        <asp:BoundField DataField="ChargeType" HeaderText="<%$Resources:grdNotices.HeaderText1 %>" />
                                        <asp:BoundField DataField="Status" HeaderText="<%$Resources:grdNotices.HeaderText2 %>" />
                                        <asp:BoundField DataField="StatusDescr" HeaderText="<%$Resources:grdNotices.HeaderText3 %>" />
                                        <asp:BoundField DataField="FullName" HeaderText="<%$Resources:grdNotices.HeaderText4 %>" />
                                        <asp:BoundField DataField="IDNumber" HeaderText="<%$Resources:grdNotices.HeaderText5 %>" />
                                        <asp:BoundField DataField="ChargeRowVersion" Visible="False" />
                                        <asp:BoundField DataField="NotProxyFlag" Visible="False" />
                                        <asp:BoundField DataField="NotFilmType" Visible="false" />
                                        <asp:CommandField HeaderText="<%$Resources:grdNotices.HeaderText6 %>" SelectText="<%$Resources:grdNotices.SelectItem %>" ShowSelectButton="True" />
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNoticeStatus" runat="server" Text='<%# Bind("NoticeStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                            </asp:Panel>
                            <%--<asp:Panel ID="pnlRoadBlock" runat="server" Width="100%" CssClass="Normal">
                                <asp:Label runat="server" ID="lblRoadBlock" Text="Processing at a road block: Please complete all the information below the grid. Once you have completed that information then either select a single summons to process or click on one of the multiple process buttons at the bottom. " />
                                <asp:GridView ID="grdRoadBlock" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CellPadding="3" CssClass="Normal" OnPageIndexChanging="grdRoadBlock_PageIndexChanging"
                                    OnSelectedIndexChanged="grdRoadBlock_SelectedIndexChanged" ShowFooter="True"
                                    PageSize="15">
                                    <FooterStyle CssClass="CartListHead" />
                                    <Columns>
                                        <asp:BoundField DataField="NotIntNo" HeaderText="NotIntNo" Visible="False" />
                                        <asp:BoundField DataField="ChgIntNo" HeaderText="ChgIntNo" Visible="False" />
                                        <asp:BoundField DataField="TicketNo" HeaderText="Ticket No." />
                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                        <asp:BoundField DataField="StatusDescr" HeaderText="Description" />
                                        <asp:BoundField DataField="FullName" HeaderText="Full Name" />
                                        <asp:BoundField DataField="IDNumber" HeaderText="ID Number" />
                                        <asp:BoundField DataField="ChargeRowVersion" Visible="False" />
                                        <asp:BoundField DataField="RevisedAmount" Visible="False" />
                                        <asp:BoundField DataField="OriginalAmount" Visible="False" />
                                        <asp:BoundField DataField="OffenceDate" Visible="False" />
                                        <asp:BoundField DataField="SummonsNo" Visible="False" />
                                        <asp:BoundField DataField="AreaCode" Visible="False" />
                                        <asp:BoundField DataField="NotProxyFlag" Visible="False" />
                                        <asp:CommandField HeaderText="Individual Process" ShowSelectButton="True" />
                                    </Columns>
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                                <table style="border-style: none;" class="NormalBold">
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            Forenames</td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtRoadForenames" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            Surname
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtRoadSurname" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            ID Number:
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtRoadIDNumber" Width="100%" CssClass="Normal"
                                                MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            Physical Address
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtRoadPhysical1" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            &nbsp;
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtRoadPhysical2" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            &nbsp;
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtRoadPhysical3" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            &nbsp;
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtRoadPhysical4" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            Area Code:
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtRoadAreaCode" MaxLength="4" Width="100px" CssClass="Normal" /></td>
                                    </tr>
                                    </table>
                                <table style="border-style: none;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblRoadSS" runat="server" CssClass="NormalBold" Width="105px">Your Officer Number:  </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtOfficerNo" CssClass="Normal" Width="233px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblRoadCourt" runat="server" CssClass="NormalBold" Width="105px">Select a Court:  </asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboRoadCourt" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboRoadCourt_SelectedIndexChanged"
                                                CssClass="Normal" Width="187px" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="143px">Select a Court room:  </asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="cboRoadCourtRoom" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboRoadCourtRoom_SelectedIndexChanged"
                                                CssClass="Normal" Width="187px" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblRoadCourtDate" runat="server" CssClass="NormalBold" Width="105px">Court Date:  </asp:Label>
                                        </td>
                                        <td>
                                         <asp:TextBox ID="wdcRoadCourtDate" 
                                                runat="server" autocomplete="off" CssClass="Normal" Height="20px" 
                                                UseSubmitBehavior="False" />
                                            <cc1:CalendarExtender ID="DateCalendar" runat="server" Format="yyyy-MM-dd" 
                                                TargetControlID="wdcRoadCourtDate">
                                            </cc1:CalendarExtender>
                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                            ControlToValidate="wdcRoadCourtDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="Please enter a valid date"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <asp:Button ID="btnRoadAllSummons" runat="server" Text="Process All Summons" OnClick="btnRoadAllSummons_Click"
                                                CssClass="NormalButton" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnRoadAllNotices" runat="server" Text="Process All Notices" OnClick="btnRoadAllNotices_Click"
                                                CssClass="NormalButton" />
                                        </td>
                                    </tr>            
                                </table>
                            </asp:Panel>--%>
                            <!--Ask user to decide how to handle no aog's-->
                            <asp:Panel ID="pnlNOAOGDecision" runat="server" Height="80px" Width="100%" HorizontalAlign="Center">
                                <asp:Label ID="lblNOAOGDecision" runat="server" Height="24px" Text="<%$Resources:lblNOAOGDecision.Text %>"
                                    Width="600px" CssClass="SubContentHead"></asp:Label>
                                <br />
                                <asp:Button ID="NOAOGDecisionYes" runat="server" OnClick="NOAOGDecisionYes_Click"
                                    Text="<%$Resources:NOAOGDecisionYes.Text %>" CssClass="NormalButton" Width="76px" />
                                &nbsp; &nbsp; &nbsp;
                                <asp:Button ID="NOAOGDecisionNo" runat="server" OnClick="NOAOGDecisionNo_Click" Text="<%$Resources:NOAOGDecisionNo..Text %>"
                                    Width="76px" CssClass="NormalButton" Height="24px" /></asp:Panel>
                            <!-- On-the-fly representation Panel -->
                            <asp:Panel ID="pnlRepresentation" runat="server" Width="100%">
                                <asp:Label ID="Label2" runat="server" Width="100%" CssClass="SubContentHead" Text="<%$Resources:lblCreateRep.Text %>"></asp:Label>
                                <asp:LinkButton ID="lnkSearch" runat="server" OnClick="lnkSearch_Click" Text="<%$Resources:btnSearch.Text %>"></asp:LinkButton>
                                &gt;
                                <asp:LinkButton ID="lblResults" runat="server" OnClick="lblResults_Click" Text="<%$Resources:lblResults.Text %>"></asp:LinkButton><br />
                                <table style="border-spacing: none;" class="Normal">
                                    <tr>
                                        <td style="width: 122px" class="NormalBold">
                                            <asp:Label ID="Label9" runat="server" Text="<%$Resources:lblNewAmount.Text %>"></asp:Label></td>
                                        <td style="width: 224px">
                                            <asp:TextBox ID="txtNewAmount" runat="server" AutoPostBack="True" OnTextChanged="txtNewAmount_TextChanged"
                                                Width="112px" /></td>
                                    </tr>
                                </table>
                                <uc2:RepresentationOnTheFly ID="RepresentationOnTheFly1" runat="server" />
                            </asp:Panel>
                            <!-- Summons Server Selection Panel -->
                            <asp:Panel ID="pnlSummonsServer" runat="server" Width="100%">
                                <asp:Label ID="lblSummonsTitle" runat="server" Width="100%" CssClass="SubContentHead" Text="<%$Resources:lblSummonsTitle.Text %>"></asp:Label>
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnkSearch_Click" Text="<%$Resources:btnSearch.Text %>"></asp:LinkButton>
                                &gt;
                                <asp:LinkButton ID="LinkButton2" runat="server" OnClick="lblResults_Click" Text="<%$Resources:lblResults.Text %>"></asp:LinkButton><br />
                                <table style="border-style: none;" class="Normal">
                                    <tr>
                                        <td style="width: 169px">
                                            <asp:Label ID="labelSummonsServerArea" runat="server" CssClass="NormalBold" Text="<%$Resources:labelSummonsServerArea.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label runat="server" ID="lblSummonsServerArea" CssClass="NormalBold" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 169px">
                                            <asp:Label ID="labelSelectSummonsServer" runat="server" CssClass="NormalBold" Width="176px" Text="<%$Resources:labelSelectSummonsServer.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboSummonsServer" runat="server" CssClass="Normal" Width="233px" /></td>
                                    </tr>
                                </table>
                                <%--<asp:Label ID="Label4" runat="server" Width="100%" CssClass="SubContentHead">Summons Location Selection</asp:Label>
                                <table style="border-style: none;" class="Normal">
                                    <tr>
                                        <td style="width: 169px">
                                            <asp:Label ID="label5" runat="server" CssClass="NormalBold">Summons Location:</asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="cboSummonsLocation" runat="server" CssClass="Normal" Width="233px" />
                                        </td>
                                    </tr>
                                </table>--%>
                                <table style="border-style: none;" class="Normal">
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Button runat="server" ID="btnAssign" Text="<%$Resources:btnAssign.Text %>" CssClass="NormalButton"
                                                OnClick="btnAssign_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- Court and Court Date selection -->
                            <asp:Panel ID="pnlCourt" runat="server" Width="100%">
                                <asp:Label ID="lblCourt" runat="server" Width="100%" CssClass="SubContentHead" Text="<%$Resources:lblCourt.Text %>"></asp:Label>
                                <asp:LinkButton ID="LinkButton3" runat="server" OnClick="lnkSearch_Click" Text="<%$Resources:btnSearch.Text %>"></asp:LinkButton>
                                &gt;
                                <asp:LinkButton ID="LinkButton4" runat="server" OnClick="lblResults_Click" Text="<%$Resources:lblResults.Text %>"></asp:LinkButton>
                                &gt;
                                <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton5_Click" Text="<%$Resources:lblSummonsServer.Text %>"></asp:LinkButton>
                                <br />
                                <table style="border-style: none;">
                                    <tr>
                                        <td style="width: 126px">
                                            <asp:Label ID="labelCourtName" runat="server" CssClass="NormalBold" Width="128px" Text="<%$Resources:labelCourtName.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboCourt" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboCourt_SelectedIndexChanged"
                                                CssClass="Normal" Width="187px" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 126px">
                                            <asp:Label ID="labelCourtRoom" runat="server" CssClass="NormalBold" Width="130px" Text="<%$Resources:labelCourtRoom.Text %>"> </asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboCourtRoom" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboCourtRoom_SelectedIndexChanged"
                                                CssClass="Normal" Width="187px" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 126px">
                                            <asp:Label ID="labelCourtDate" runat="server" CssClass="NormalBold" Width="126px" Text="<%$Resources:labelCourtDate.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblCourtDate" runat="server" CssClass="Normal" Width="111px" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right">
                                            <asp:HiddenField ID="hidCourtDate" runat="server" />
                                            &nbsp;<asp:Button ID="btnSelectCourt" runat="server" CssClass="NormalButton" Text="<%$Resources:btnSelectCourt.Text %>"
                                                OnClick="btnSelectCourt_Click" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- Update Offenders Details -->
                            <asp:Panel ID="pnlPersonalDetails" runat="server" Width="100%">
                                <asp:Label ID="lblPersonalDetails" runat="server" CssClass="SubContentHead" Width="100%" Text="<%$Resources:lblPersonalDetails.Text %>"></asp:Label>
                                <asp:LinkButton ID="LinkButton8" runat="server" OnClick="lnkSearch_Click" Text="<%$Resources:btnSearch.Text %>"></asp:LinkButton>
                                &gt;
                                <asp:LinkButton ID="LinkButton9" runat="server" OnClick="lblResults_Click" Text="<%$Resources:lblResults.Text %>"></asp:LinkButton>
                                <br />
                                <table style="border-style: none; width: 509px;" class="NormalBold">
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            <asp:Label ID="Label10" runat="server" Text="<%$Resources:lblForenames.Text %>"></asp:Label></td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtForenames" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            <asp:Label ID="Label11" runat="server" Text="<%$Resources:lblSurname.Text %>"></asp:Label>
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtSurname" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            <asp:Label ID="Label12" runat="server" Text="<%$Resources:lblIDNumber.Text %>"></asp:Label>
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtOffenderIDNumber" Width="100%" CssClass="Normal"
                                                MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            <asp:Label ID="Label13" runat="server" Text="<%$Resources:lblPhyAddress.Text %>"></asp:Label>
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtAddress1" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            &nbsp;
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtAddress2" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            &nbsp;
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtAddress3" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px; text-align: right;" class="NormalBold">
                                            &nbsp;
                                            <asp:Label ID="Label7" runat="Server" CssClass="NormalRed">*</asp:Label></td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtAddress4" Width="100%" CssClass="Normal" MaxLength="100" /></td>
                                    </tr>
                                    <tr>
                                        <td class="NormalBold" colspan="2" style="vertical-align: top; height: 37px">
                                            <asp:Label ID="Label24" runat="server" CssClass="NormalRed" Text="<%$Resources:lblEnterName.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="NormalBold" style="width: 115px">
                                            <asp:Label ID="Label14" runat="server" Text="<%$Resources:lblCodeLookup.Text %>"></asp:Label>
                                        </td>
                                        <td style="width: 286px">
                                            <asp:Button ID="btnPostalCodesA" runat="server" class="NormalButton" OnClick="btnPostalCodesA_Click"
                                                Text="..." Width="18px" /><asp:DropDownList ID="ddlPostalCodesA" runat="server" AutoPostBack="true"
                                                    CssClass="Normal" OnSelectedIndexChanged="ddlPostalCodesA_SelectedIndexChanged"
                                                    Visible="true" Width="225px">
                                                </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 115px" class="NormalBold">
                                            <asp:Label ID="Label15" runat="server" Text="<%$Resources:lblAreaCode.Text %>"></asp:Label>
                                        </td>
                                        <td style="width: 286px">
                                            <asp:TextBox runat="server" ID="txtAreaCode" MaxLength="4" Width="100px" CssClass="Normal" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right; height: 26px;" class="NormalBold">
                                            &nbsp; &nbsp; &nbsp; &nbsp;<asp:Button ID="btnPersonalDetails" runat="server" Text="<%$Resources:btnPersonalDetails.Text %>" CssClass="NormalButton"
                                                OnClick="btnPersonalDetails_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- Summons First Print -->
                            <asp:Panel ID="pnlSummonsFirstPrint" runat="server" Width="100%" Font-Underline="False">
                                <asp:Label ID="Label3" runat="server" Width="100%" CssClass="SubContentHead" Text="<%$Resources:lblSummonsPrint.Text %>"></asp:Label>
                                <asp:LinkButton ID="LinkButton6" runat="server" OnClick="lnkSearch_Click" Text="<%$Resources:btnSearch.Text %>"></asp:LinkButton>
                                &gt;
                                <asp:LinkButton ID="LinkButton7" runat="server" OnClick="lblResults_Click" Text="<%$Resources:lblResults.Text %>"></asp:LinkButton>
                                <p class="NormalBold">
                                    &nbsp;<asp:LinkButton ID="lnkSummonsPrint" runat="server" OnClick="lnkSummonsPrint_Click"></asp:LinkButton>&nbsp;</p>
                                <p>
                                    <asp:Label ID="lblStationeryMessage" runat="server" CssClass="SubContentHead" 
                                        Width="100%" Text="<%$Resources:lblStationeryMessage.Text %>"></asp:Label>
                                  </p>
                                  <p>
                                    <asp:Label ID="lblMsgForBatchPrint" runat="server" Width="100%" Text="<%$Resources:lblMsgForBatchPrint.Text  %>" ForeColor="Red" Visible = "false"></asp:Label>
                                  </p>
                            </asp:Panel>
                            <%--<asp:Panel ID="pnlSummonsMultiplePrint" runat="server" Width="100%" Font-Underline="False">
                                <asp:Label ID="Label17" runat="server" Width="100%" CssClass="SubContentHead">Print 
                                Multiple Summons:</asp:Label>
                                <asp:LinkButton ID="LinkButton10" runat="server" OnClick="lnkSearch_Click">Search</asp:LinkButton>
                                &gt;
                                <asp:LinkButton ID="LinkButton11" runat="server" OnClick="lblResults_Click">Results</asp:LinkButton>
                               
                                <br></br>
                                <asp:GridView ID="grdSumonsMultiplePrint" runat="server" AllowPaging="True" 
                                    AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" 
                                    OnPageIndexChanging="grdSumonsMultiplePrint_PageIndexChanging" 
                                    OnSelectedIndexChanged="grdSumonsMultiplePrint_SelectedIndexChanged" 
                                    PageSize="15" ShowFooter="True">
                                    <FooterStyle CssClass="CartListHead" />
                                    <Columns>
                                        <asp:BoundField DataField="ChgIntNo" HeaderText="ChgIntNo" Visible="False" />
                                        <asp:BoundField DataField="SumIntNo" HeaderText="SumIntNo" Visible="False" />
                                        <asp:BoundField DataField="PrintAutIntNo" HeaderText="PrintAutIntNo" Visible="False" />
                                        <asp:BoundField DataField="SummonsNo" HeaderText="Summons No." />
                                        <asp:BoundField DataField="PrintFile" HeaderText="Print File" />
                                        <asp:CommandField HeaderText="" SelectText="Print" ShowSelectButton="true" />
                                    </Columns>
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                                <p>
                                    Important: Please load CPA5 stationery into the printer before printing the 
                                    summons.</p>
                                <br></br>
                                <br>
                                </br>
                            </asp:Panel>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;<asp:Label
                                    ID="Label16" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center" style="width: 182px">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
        <!----Jerry 2012-11-20  popup msg---->
        <div>
            <asp:Button runat="server" ID="hiddenForPopup" style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="PopupAjax" runat="server" TargetControlID="hiddenForPopup"
                PopupControlID="PanelForPopup" BackgroundCssClass="modalBackground" BehaviorID="popupBehavior"
                RepositionMode="None" Y="180">
                </ajaxToolkit:ModalPopupExtender>
        </div>
        <asp:Panel runat="server" CssClass="modalPopup" ID="PanelForPopup" Style="display: none; width:500px;">
            <div style="background-color: #DDDDDD; padding: 5px; color: Black; text-align: center;
                font-size: larger; font-weight: bold;">
                <asp:Label ID="Label18" runat="server" Text = "<%$Resources:DialogTitle %>" />                
            </div>
            <div id="divMsg" runat="server">
                <div style="padding-top:25px;">
                   <asp:Label ID="lblPopupMsg" runat="server" Text = "<%$Resources:PopupMsg %>" />
                </div>
                <div style="padding-top:25px;">
                    <div style="text-align: center;">
                        <asp:Button ID="btnYes" Text="<%$Resources: btnYes.Text %>" runat="server" OnClick="btnYes_Click" />&nbsp;&nbsp;<asp:Button ID="btnNo" Text="<%$Resources: btnNo.Text %>" runat="server" OnClick="btnNo_Click" />
                    </div>   
                </div>
            </div>
            <!-------end popup -------------->
        </asp:Panel>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hidSumIntNo" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
