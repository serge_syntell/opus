using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class VehicleMake : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "VehicleMake.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "Vehicle make maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
            //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
			autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddVehicleMake.Visible = false;
				pnlUpdateVehicleMake.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

                PopulateVehicleMakes();

				BindGrid();

			}		
		}

        private void PopulateVehicleMakes()
        {
            VehicleMakeDB vm = new VehicleMakeDB(connectionString);

            cblVMLookups.DataSource = vm.GetVehicleMakeList("");
            cblVMLookups.DataValueField = "VMIntNo";
            cblVMLookups.DataTextField = "VMDescr";
            cblVMLookups.DataBind();

            ClearSelection();
        }

        protected void ClearSelection()
        {
            foreach (ListItem li in cblVMLookups.Items)
            {
                li.Selected = false;
            }
        }

		protected void BindGrid()
		{
			// Obtain and bind a list of all users

			Stalberg.TMS.VehicleMakeDB vmList = new Stalberg.TMS.VehicleMakeDB(connectionString);

            int totalCount = 0; // 2013-03-15 add by Henry for pagination
			DataSet data = vmList.GetVehicleMakeListDS(txtSearch.Text, out totalCount, dgVehicleMakePager.PageSize, dgVehicleMakePager.CurrentPageIndex);
			dgVehicleMake.DataSource = data;
			dgVehicleMake.DataKeyField = "VMIntNo";
            dgVehicleMakePager.RecordCount = totalCount;

            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgVehicleMake.DataBind();
            }
            catch
            {
                dgVehicleMake.CurrentPageIndex = 0;
                dgVehicleMake.DataBind();
            }


			if (dgVehicleMake.Items.Count == 0)
			{
				dgVehicleMake.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text");
			}
			else
			{
				dgVehicleMake.Visible = true;
			}

			data.Dispose();

			pnlAddVehicleMake.Visible = false;
			pnlUpdateVehicleMake.Visible = false;

		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddVehicleMake.Visible = true;
			pnlUpdateVehicleMake.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);

			btnOptDelete.Visible = false;
		}

		protected void btnAddVehicleMake_Click(object sender, System.EventArgs e)
		{	
			// add the new transaction to the database table
			Stalberg.TMS.VehicleMakeDB toAdd = new VehicleMakeDB(connectionString);
			
			int addVMIntNo = toAdd.AddVehicleMake(txtAddVMCode.Text, 
				txtAddVMDescr.Text, txtAddNatis.Text, txtAddTCS.Text, txtAddCivitas.Text, loginUser);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addVMIntNo, lgEntityList, "VehicleMakeLookup", "VMIntNo", "VMDescr", this.loginUser);

			if (addVMIntNo <= 0)
			{
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text1");
			}
			else
			{
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text2");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleMakesMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;
            dgVehicleMakePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            dgVehicleMakePager.CurrentPageIndex = 1;
			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			int vmIntNo = Convert.ToInt32(Session["editVMIntNo"]);

			// add the new transaction to the database table
			Stalberg.TMS.VehicleMakeDB vmUpdate = new VehicleMakeDB(connectionString);
			
			int updVMIntNo = vmUpdate.UpdateVehicleMake(vmIntNo, txtVMCode.Text, 
				txtVMDescr.Text, txtNatis.Text, txtTCS.Text, txtCivitas.Text, loginUser);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(updVMIntNo, lgEntityList, "VehicleMakeLookup", "VMIntNo", "VMDescr", this.loginUser);
            

			if (updVMIntNo <= 0)
			{
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text3");
			}
			else
			{
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text4");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleMakesMaintenance, PunchAction.Change);  
			}

			lblError.Visible = true;

            //need to update vehicle type lookups
            ArrayList vml = new ArrayList();

            foreach (ListItem li in cblVMLookups.Items)
            {
                if (li.Selected == true)
                    vml.Add(li.Value);
            }

            vmUpdate.AddVehicleMakeLookups(vmIntNo, vml, this.loginUser);

            //Adam 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!

            //dgVehicleMakePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            //dgVehicleMakePager.CurrentPageIndex = 1;
			BindGrid();
		}

		protected void dgVehicleMake_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgVehicleMake.SelectedIndex = e.Item.ItemIndex;

			if (dgVehicleMake.SelectedIndex > -1) 
			{			
				int editVMIntNo = Convert.ToInt32(dgVehicleMake.DataKeys[dgVehicleMake.SelectedIndex]);

				Session["editVMIntNo"] = editVMIntNo;
				Session["prevPage"] = thisPageURL;


                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editVMIntNo.ToString(), "VehicleMakeLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

				ShowVehicleMakeDetails(editVMIntNo);
			}
		}

		protected void ShowVehicleMakeDetails(int editVMIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.VehicleMakeDB vm = new Stalberg.TMS.VehicleMakeDB(connectionString);
			
			Stalberg.TMS.VehicleMakeDetails vmDetails = vm.GetVehicleMakeDetails(editVMIntNo);

			txtVMCode.Text = vmDetails.VMCode;
			txtVMDescr.Text = vmDetails.VMDescr;
            txtNatis.Text = vmDetails.Natis;
            txtTCS.Text = vmDetails.TCS;
            txtCivitas.Text = vmDetails.Civitas;
			
			pnlUpdateVehicleMake.Visible = true;
			pnlAddVehicleMake.Visible  = false;
			
			btnOptDelete.Visible = true;
            ClearSelection();

            SetSelection(editVMIntNo);
        }

        private void SetSelection(int editVMIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.VehicleMakeDB vm = new Stalberg.TMS.VehicleMakeDB(connectionString);

            DataSet ds = vm.GetVehicleMakeLookupListDS(editVMIntNo);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                foreach (ListItem li in cblVMLookups.Items)
                {
                    if (li.Value.Equals(dr["VMIntNo"].ToString()))
                        li.Selected = true;
                }
            }
        }

    	protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int vmIntNo = Convert.ToInt32(Session["editVMIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "VehicleMakeLookup", "VMIntNo");

			VehicleMakeDB vm = new Stalberg.TMS.VehicleMakeDB(connectionString);

			string delVMIntNo = vm.DeleteVehicleMake(vmIntNo);


            if (delVMIntNo.Equals("0"))
            {
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text5");
            }
            else
            {
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text6");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleMakesMaintenance, PunchAction.Delete);  
            }
            //Adam 20130912 Make code changes here to make sure the page will navigate back correctly after one row is deleted.
            if (dgVehicleMake.Items.Count == 1)
                this.dgVehicleMakePager.CurrentPageIndex--;
			lblError.Visible = true;
            //dgVehicleMakePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            //dgVehicleMakePager.CurrentPageIndex = 1;
			BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgVehicleMake.Visible.Equals(true))
			{
				//hide it
				dgVehicleMake.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
                // Adam 20130912 Hide the pagination control when the user clicks the hide button.
                dgVehicleMakePager.Visible = false;
			}
			else
			{
				//show it
				dgVehicleMake.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
                dgVehicleMakePager.Visible = true;
			}
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
            dgVehicleMakePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            dgVehicleMakePager.CurrentPageIndex = 1;
			BindGrid();
            //PunchStats805806 enquiry VehicleMakes
		}

        // 2013-03-15 add by Henry for pagination
        protected void dgVehicleMakePager_PageChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
	}
}
