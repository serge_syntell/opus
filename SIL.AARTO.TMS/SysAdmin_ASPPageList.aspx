<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SysAdmin_ASPPageList" Codebehind="SysAdmin_ASPPageList.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
        <tr>
            <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" height="85%">
        <tr>
            <td align="center" valign="top">
                <img style="height: 1px" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" height="2" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" height="21">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" height="21">
                                <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                    OnClick="btnOptAdd_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" height="21">
                                <asp:Button ID="btnHideMenu" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$Resources:btnHideMenu.Text %>" OnClick="btnHideMenu_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" width="100%" colspan="1">
                <asp:UpdatePanel runat="server" ID="update">
                    <ContentTemplate>
                        <table border="0" width="568" height="482">
                            <tr>
                                <td valign="top" height="47">
                                    <p align="center">
                                        <asp:Label ID="lblPageName" runat="server" Width="576px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table id="Table3" height="7" cellspacing="1" cellpadding="1" width="428" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSelect" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelect.Text %>:"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlSelect" runat="server" Width="222px" CssClass="Normal" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlSelect_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:DataGrid ID="dgPageList" Width="495px" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                        AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                        FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                        Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnDeleteCommand="dgPageList_DeleteCommand"
                                        OnItemCommand="dgPageList_ItemCommand" OnPageIndexChanged="dgPageList_PageIndexChanged"
                                        PageSize="15">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="APLIntNo" HeaderText="APLIntNo"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="APLPageName" HeaderText="<%$Resources:lblSelect.Text %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="APLPageURL" HeaderText="<%$Resources:dgPageList.HeaderText %>"></asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSysAdminOnly" runat="server" Text='<%# SetValue(DataBinder.Eval(Container, "DataItem.APLSysAdminOnly")) %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgPageList.Text %>" CommandName="Select"></asp:ButtonColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgPageList.Text1 %>" CommandName="Delete"></asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                    </asp:DataGrid>
                                    <asp:Panel ID="pnlPageDetails" runat="server">
                                        <table id="Table2" height="0" cellspacing="1" cellpadding="1" width="300" border="0">
                                            <tr>
                                                <td width="103">
                                                    <asp:Label ID="lblPageDetails" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblPageDetails.Text %>"></asp:Label>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="103">
                                                    <asp:Label ID="lblAPLPageName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelect.Text %>:"></asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtAPLPageName" runat="server" Width="252px" CssClass="NormalMand"
                                                        MaxLength="50" Text="<%$Resources:txtAPLPageName.Text %>"></asp:TextBox>
                                                </td>
                                                <td colspan="1">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Width="191px"
                                                        CssClass="NormalRed" ControlToValidate="txtAPLPageName" ErrorMessage="<%$Resources:reqPageName.ErrorMsg %>"
                                                        ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="103">
                                                    <asp:Label ID="lblAPLPageURL" runat="server" CssClass="NormalBold" Text="<%$Resources:dgPageList.HeaderText %>:"></asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtAPLPageURL" runat="server" Width="252px" CssClass="NormalMand"
                                                        MaxLength="50" Text="<%$Resources:txtAPLPageURL.Text %>"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Width="191px"
                                                        CssClass="NormalRed" ControlToValidate="txtAPLPageURL" ErrorMessage="<%$Resources:reqPageURL.ErrorMsg %>"
                                                        ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="103">
                                                    <asp:Label ID="lblAPLPageDescr" runat="server" Width="137px" CssClass="NormalBold" Text="<%$Resources:lblAPLPageDescr.Text %>"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAPLPageDescr" runat="server" Width="257px" CssClass="NormalMand"
                                                        MaxLength="100" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Width="191px"
                                                        CssClass="NormalRed" ControlToValidate="txtAPLPageDescr" ErrorMessage="<%$Resources:reqPageDescr.ErrorMsg %>"
                                                        ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="103">
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkAPLSysAdminOnly" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAPLSysAdminOnly.Text %>">
                                                    </asp:CheckBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                        OnClick="btnUpdate_Click"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlAddPage" runat="server">
                                        <table id="Table1" cellspacing="1" cellpadding="1" width="300" border="0">
                                            <tr>
                                                <td width="103">
                                                    <asp:Label ID="lblAddPage" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddPage.Text %>"></asp:Label>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="103">
                                                    <asp:Label ID="lblAddAPLPageName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelect.Text %>:"></asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtAddAPLPageName" runat="server" Width="256px" CssClass="NormalMand"
                                                        MaxLength="50" Text="<%$Resources:txtAPLPageName.Text %>"></asp:TextBox>
                                                </td>
                                                <td colspan="1">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="191px"
                                                        CssClass="NormalRed" ControlToValidate="txtAddAPLPageName" ErrorMessage="<%$Resources:reqPageName.ErrorMsg %>"
                                                        ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="103">
                                                    <asp:Label ID="lblAddAPLPageURL" runat="server" CssClass="NormalBold" Text="<%$Resources:dgPageList.HeaderText %>:"></asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtAddAPLPageURL" runat="server" Width="252px" CssClass="NormalMand"
                                                        MaxLength="50" Text="<%$Resources:txtAPLPageURL.Text %>"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Width="191px"
                                                        CssClass="NormalRed" ControlToValidate="txtAPLPageURL" ErrorMessage="<%$Resources:reqPageURL.ErrorMsg %>"
                                                        ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="103">
                                                    <asp:Label ID="lblAddPageDescr" runat="server" Width="137px" CssClass="NormalBold"  Text="<%$Resources:lblAPLPageDescr.Text %>"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddAPLPageDescr" runat="server" Width="257px" CssClass="NormalMand"
                                                        MaxLength="100" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Width="191px"
                                                        CssClass="NormalRed" ControlToValidate="txtAddAPLPageDescr" ErrorMessage="<%$Resources:reqPageDescr.ErrorMsg %>"
                                                        ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="103">
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkAddAPLSysAdminOnly" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAPLSysAdminOnly.Text %>">
                                                    </asp:CheckBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAdd" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAdd.Text %>" OnClick="btnAdd_Click">
                                                    </asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        </td> </tr> </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label2"
                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
                    <tr>
                        <td class="SubContentHeadSmall" valign="top" width="100%">
                        </td>
                    </tr>
                </table>
    </form>
</body>
</html>
