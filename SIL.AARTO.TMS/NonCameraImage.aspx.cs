﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Stalberg.TMS;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Util;
using System.Collections.Generic;
using Stalberg.TMS.Data;

namespace SIL.AARTO.TMS
{
    public partial class NonCameraImage : System.Web.UI.Page
    {
        private string connectionString;
        private string imageFolder = "";
        protected override void OnInit(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();
          
            if (Application["imageFolder"] != null)
            {
                imageFolder = Application["imageFolder"].ToString();
            }
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           // Response.Cache.SetNoStore();
            Response.Buffer = true;
            Response.ExpiresAbsolute = System.DateTime.Now.AddSeconds(-1);
            Response.Expires = 0;
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "No-Cache");
            Process();
        }

        private void Process()
        {
            try
            {
                using (Bitmap bitmap = ImagePreProcess())
                {
                    if (bitmap != null)
                    {
                        Response.ContentType = "image/jpeg";
                        bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                        bitmap.Dispose();
                    }
                    else
                    {
                        Bitmap image = null;
                        string strNoImagePath = Server.MapPath("images/defaultImg.jpg");
                        System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(strNoImagePath);//strTestImagePath
                        image = new Bitmap(imageConvert);
                        imageConvert.Dispose();
                        Response.ContentType = "image/jpeg";
                        image.Save(Response.OutputStream, ImageFormat.Jpeg);
                        image.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                Bitmap image = null;
                string strNoImagePath = Server.MapPath("images/defaultImg.jpg");
             
                System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(strNoImagePath);//strTestImagePath
                image = new Bitmap(imageConvert);
                imageConvert.Dispose();
                Response.ContentType = "image/jpeg";
                image.Save(Response.OutputStream, ImageFormat.Jpeg);
                image.Dispose();
            }
            finally
            {
                GC.Collect();
            }
        }

        private Bitmap ImagePreProcess()
        {
            Bitmap image = null;
            //if (!String.IsNullOrEmpty(HttpUtility.UrlDecode(Request.QueryString["NotIntNo"])))
            //{
            //    image = NoticeImageProcess();
            //}
            //else
            //{
            //if (!String.IsNullOrEmpty(Request.QueryString["JpegName"]) && File.Exists(Server.MapPath(imageFolder) + "\\" + HttpUtility.UrlDecode(Request.QueryString["FilmNo"]) + "\\" + HttpUtility.UrlDecode(Request.QueryString["JpegName"])))
            //{
            //    System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(Server.MapPath(imageFolder) + "\\" + HttpUtility.UrlDecode(Request.QueryString["FilmNo"]) + "\\" + HttpUtility.UrlDecode(Request.QueryString["JpegName"]));
            //    image = new Bitmap(imageConvert);
            //    imageConvert.Dispose();
            //}
            //else
            //{
            string jpegName = HttpUtility.UrlDecode(Request.QueryString["JpegName"]);
            byte[] data = null;
            WebService service = new WebService();
           if (!String.IsNullOrEmpty(Request.QueryString["IFSIntNo"]))
           {
                int ifsIntNo = Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["IFSIntNo"]));
                // get test image from ImageFileServer
                ImageFileServerDB ifsDB = new ImageFileServerDB(connectionString);
                ImageFileServerDetails testServer = ifsDB.GetImageFileServer(ifsIntNo);

                //if (service.RemoteConnect(testServer.ImageMachineName, testServer.ImageShareName, testServer.RemoteUserName, testServer.RemotePassword))
                //{
                string strTestImagePath = string.Format(@"\\{0}\{1}\{2}", testServer.ImageMachineName, testServer.ImageShareName, jpegName);

                if (!File.Exists(strTestImagePath))
                {
                   // strTestImagePath = string.Format(@"\\{0}\{1}\{2}", testServer.ImageMachineName, testServer.ImageShareName, jpegName);
                    strTestImagePath = Server.MapPath("images/defaultImg.jpg");
                }

               // string strTestImagePath = string.Format(@"\\{0}\{1}\sxj\111.JPG", "192.168.1.254","HeidiLiu");

                //if (!File.Exists(strTestImagePath))
                //    strTestImagePath = string.Format(@"\\{0}\{1}\sxj\111.JPG", "192.168.1.254", "HeidiLiu");

            System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(strTestImagePath);//strTestImagePath
                image = new Bitmap(imageConvert);
                imageConvert.Dispose();
                //}
            }
            return image;
        }

 
    }
}