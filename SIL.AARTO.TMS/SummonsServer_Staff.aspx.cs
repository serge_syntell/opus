using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// The starting point for ticket payment receipt
    /// </summary>
    public partial class SummonsServer_Staff : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = "SummonsServer_Staff";
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsServer_Staff.aspx";
        //protected string thisPage = "Summons Server Staff";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;
            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.btnNew.Visible = false;
                this.btnDelete.Visible = false;
                this.pnlStaff.Visible = false;
                this.pnlStaffDetails.Visible = false;

                this.PopulateSSGrid();
            }
        }

        private void PopulateSSGrid()
        {
            SummonsServerDB ss = new SummonsServerDB(this.connectionString);
            this.grdSummonsServer.DataSource = ss.GetAuth_SummonsServerListByAuthDS(this.autIntNo);
            this.grdSummonsServer.DataKeyNames = new string[] { "SSIntNo" };
            this.grdSummonsServer.DataBind();
        }

        

        protected void grdSummonsServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.GetStaffMembersDetails();
        }

        private void GetStaffMembersDetails()
        {
            int ssIntNo = (int)this.grdSummonsServer.DataKeys[this.grdSummonsServer.SelectedIndex][0];

            this.ViewState.Add("SSIntNo", ssIntNo);

            SummonsServerDB ss = new SummonsServerDB(this.connectionString);
            this.grdStaff.DataSource = ss.GetSummonServerStaff(ssIntNo, true, false);
            this.grdStaff.DataKeyNames = new string[] { "SS_SIntNo" };
            this.grdStaff.DataBind();

            if (this.grdStaff.Rows.Count == 0)
            {
                this.lblStaff.CssClass = "NormalRed";
                this.lblStaff.Text = (string)GetLocalResourceObject("lblStaff.Text1");
            }
            else
            {
                this.lblStaff.CssClass = "NormalBold";
                this.lblStaff.Text = (string)GetLocalResourceObject("lblStaff.Text2");
            }

            this.grdStaff.Visible = true;
            this.pnlStaff.Visible = true;
            this.btnNew.Visible = true;
            this.btnDelete.Visible = false;
            this.pnlStaffDetails.Visible = false;
        }

        protected void grdStaff_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.GetStaffMemberDetails();
        }

        private void GetStaffMemberDetails()
        {
            int staffIntNo = (int)this.grdStaff.DataKeys[this.grdStaff.SelectedIndex][0];

            SummonsServerDB ss = new SummonsServerDB(this.connectionString);
            this.PopulateStaffMemberDetails(ss.GetSummonServerStaff(staffIntNo, false, false));

            //this.btnDelete.Visible = true;
        }

        private void PopulateStaffMemberDetails(DataSet ds)
        {
            // LF:  2008/06/09 - When setting the grdStaff to show all members: both active and inactive
            // this method threw exception:  I'm busy verifying the adding of new staff members.
            // Test when view details of an active member.
            DataRow dr = ds.Tables[0].Rows[0];
            this.hdnRef.Value = dr[0].ToString();
            this.txtName.Text = dr[1].ToString();
            this.txtForceNo.Text = dr[2].ToString();
            this.txtCertificate.Text = dr[3].ToString();

            DateTime dt = DateTime.MinValue;
            DateTime.TryParse (string.Format("{0:yyyy-MM-dd}" , dr[4].ToString() ),out dt);
            
            if (dt.Year >= 1960)
                this.wdcDate.Text = dt.ToString(DATE_FORMAT);
            else
                this.wdcDate.Text = string.Empty;

            this.chkActive.Checked = (bool)dr[5];
            this.pnlStaffDetails.Visible = true;
            this.lblStaffError.Text = string.Empty;

            this.chkActive.Checked = (bool)dr["SS_Active"];

        }

        protected void dvStaffDetails_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            e.Cancel = true;
            this.GetStaffMemberDetails();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.lblStaffError.Text = string.Empty;

            int staffIntNo = Convert.ToInt32(hdnRef.Value);
            string name = this.txtName.Text;
            string forceNo = this.txtForceNo.Text;
            string certificate = this.txtCertificate.Text;

            DateTime dt;

            if (!DateTime.TryParse(this.wdcDate.Text, out dt))
            {
                lblStaffError.Text = "<br />" + (string)GetLocalResourceObject("lblStaffError.Text");
                lblStaffError.Visible = true;
                return;
            }
 
            if (name.Length == 0)
            {
                this.lblStaffError.Text = (string)GetLocalResourceObject("lblStaffError.Text1");
                return;
            }


            this.lblStaffError.Text = string.Empty;
            int ssIntNo = (int)this.ViewState["SSIntNo"];

            SummonsServerDB ss = new SummonsServerDB(this.connectionString);
            ss.UpdateStaffDetails(ssIntNo, staffIntNo, name, forceNo, certificate, dt, login, chkActive.Checked);
            if (staffIntNo < 1)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsServerStaff, PunchAction.Add);  

            }
            else if (staffIntNo > 0)
            {
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsServerStaff, PunchAction.Change);  
            }

            this.GetStaffMembersDetails();

            this.pnlStaffDetails.Visible = false;
            this.btnDelete.Visible = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            SummonsServerDB ss = new SummonsServerDB(this.connectionString);
            DataSet ds = ss.GetSummonServerStaff(0, false, true);
            ds.Tables[0].Rows.Add(new object[] { 0, "", "", "", DBNull.Value, true });
            ds.AcceptChanges();

            this.PopulateStaffMemberDetails(ds);

            this.btnDelete.Visible = false;
            this.lblStaffError.Text = string.Empty;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            return;

            ////LF:  2008/06/09 Killed because of the active flag.
            //int staffIntNo = Convert.ToInt32(this.hdnRef.Value);
            //SummonsServerDB ss = new SummonsServerDB(this.connectionString);
            //if (ss.SummonsServerStaffDelete(staffIntNo))
            //{
            //    this.GetStaffMembersDetails();
            //    this.pnlStaffDetails.Visible = false;
            //    this.btnDelete.Visible = false;
            //}
            //else
            //    this.lblStaffError.Text = "The staff member cannot be deleted";
        }

        protected void grdStaff_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.grdStaff.PageIndex = e.NewPageIndex;
            //Jerry 2012-04-13 change
            //PopulateSSGrid();
            GetStaffMembersDetails();
        }
}
}
