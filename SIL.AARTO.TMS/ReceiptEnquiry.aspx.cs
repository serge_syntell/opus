using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;


namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that allows a supervisor to query for a receipt
    /// </summary>
    /// 

    // dls 070716 - removed charge grid completely
    //              changed main key off notice grid to be RctIntNo - after all we are looking at a single receipt
    //              added parentRctIntNo to link a receipt reversal back to the one it reversed
    //              added a Reversed flag to indicate whether a receipt has already been reversed, or is a reversal itelf.

    public partial class ReceiptEnquiry : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private bool bThisPage = false;
        private Cashier cashier = null;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "ReceiptEnquiry.aspx";

        protected string alwaysUsePostalReceiptReport = "N";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            string sTicketNo = Request.QueryString["NotTicketNo"] == null ? string.Empty : Request.QueryString["NotTicketNo"].ToString();

            //if (Request.Browser.MajorVersion.Equals(6))
            //{
            //    string query = "ReceiptEnquiryNoAjax.aspx";

            //    if (!sTicketNo.Equals(string.Empty))
            //        query += "?NotTicketNo=" + sTicketNo;

            //    Server.Transfer(query);
            //    return;
            //}

            if (ViewState["alwaysUsePostalReceiptReport"] == null)
            {
                this.alwaysUsePostalReceiptReport = GetPostalReceiptRule();
                ViewState["alwaysUsePostalReceiptReport"] = this.alwaysUsePostalReceiptReport;
            }
            else
            {
                this.alwaysUsePostalReceiptReport = ViewState["alwaysUsePostalReceiptReport"].ToString();
            }

            BankAccountDB bankAccount = new BankAccountDB(this.connectionString);
            this.cashier = bankAccount.GetUserBankAccount(int.Parse(Session["userIntNo"].ToString()));

            if (!Page.IsPostBack)
            {
                HideResultsPanels();

                if (autIntNo < 1)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    lblError.Visible = true;
                }

                if (sTicketNo != string.Empty)
                {
                    this.txtTicketNumber.Text = sTicketNo;
                    //dl s071026 - needed to add these settings so that we don't get the AJAX error
                    bThisPage = true;
                    LookForNotice();
                }
            }
        }

        protected string GetPostalReceiptRule()
        {
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = this.autIntNo;
            ard.ARCode = "4510";
            ard.LastUser = this.login;

            DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
            KeyValuePair<int, string> postalReceiptRule = authRule.SetDefaultAuthRule();
            return postalReceiptRule.Value;
        }

        private void HideResultsPanels()
        {
            this.TicketNumberSearch1.AutIntNo = autIntNo;
            this.pnlDisplaySelectedTickets.Visible = false;
            //this.pnlShowCharges.Visible = false;
            this.btnOptPrintReceipt.Visible = false;
            this.btnOptPrintEnquiry.Visible = false;
            this.btnOptReverse.Visible = false;
            this.pnlSelectTicket.Visible = true;
            this.btnOptReverse.Visible = false;
        }

        protected void btnFindOffender_Click(object sender, System.EventArgs e)
        {
            //Session["editRctIntNo"] = null;
            
            this.pnlDisplaySelectedTickets.Visible = false;
           // this.pnlDisplaySingleTicket.Visible = false;
            //this.pnlShowCharges.Visible = false;

            // Get the ticket details from the notice/charge tables for owner surname
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            //dgTicketDetails.DataSource = db.ReceiptEnquiryOnOffender(autIntNo, this.txtOffenderName.Text);
            dgTicketDetails.DataSource = db.ReceiptEnquiryOnOffender(this.txtOffenderName.Text.Trim());
            //dgTicketDetails.DataKeyNames = new string[] { "NotIntNo" };
            dgTicketDetails.DataKeyNames = new string[] { "RctIntNo" };
            dgTicketDetails.DataBind();

            pnlDisplaySelectedTickets.Visible = true;
            this.btnOptPrintEnquiry.Visible = false;
            this.btnOptPrintReceipt.Visible = false;
            this.btnOptReverse.Visible = false;
            //PunchStats805806 enquiry ReceiptEnquiry
        }

        protected void btnFindTicket_Click(object sender, System.EventArgs e)
        {
            //this.bThisPage = true;
            this.bThisPage = false;
            LookForNotice();
        }

        protected void LookForNotice()
        {
            Session["editRctIntNo"] = null;

            this.pnlDisplaySelectedTickets.Visible = false;
            //this.pnlShowCharges.Visible = false;

            // Get the ticket details from the notice/charge tables for ticket / summons
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            if (this.txtTicketNumber.Text.Trim() == string.Empty)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            //DataSet ds = db.ReceiptEnquiryOnTicketNumber(autIntNo, this.txtTicketNumber.Text.Trim());
            DataSet ds = db.ReceiptEnquiryOnTicketNumber(this.txtTicketNumber.Text.Trim());
            dgTicketDetails.DataSource = ds;
            //dgTicketDetails.DataKeyNames = new string[] { "NotIntNo" };
            dgTicketDetails.DataKeyNames = new string[] { "RctIntNo" };
            dgTicketDetails.DataBind();

            pnlDisplaySelectedTickets.Visible = true;
            this.btnOptPrintEnquiry.Visible = false;
            this.btnOptPrintReceipt.Visible = false;
            this.btnOptReverse.Visible = false;

            if (ds.Tables[0].Rows.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            //dls 071119 - at the moment if only one row is returned, they cannot reverse it! Need to show them the grid for reversal purposes!
            if (ds.Tables[0].Rows.Count == 1)
            {
                //int rctIntNo = Convert.ToInt32(ds.Tables[0].Rows[0]["RctIntNo"].ToString());

                //if (chkReversal.Checked)
                //{
                //    Reversal(rctIntNo);
                //    return;
                //}

                
                PageToOpen page = null;

                if (this.cashier == null || this.cashier.CashBoxType != CashboxType.PostalReceipts)
                {
                    if (alwaysUsePostalReceiptReport.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                    {
                        page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                    }
                    else
                    {
                        page = new PageToOpen(this, "ReceiptNoteViewer.aspx");
                    }
                }
                else
                {
                    page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                }

                this.SetSelectedGridIndex(0);
                
                bThisPage = false;
            }

            this.lblError.Text = string.Empty;
        }

        private int GetReceiptID(int rctIntNo)
        {
            //dls 070716 - add fields to stop them from reversing a receipt already reversed/ or a reversal itself
            string reversed = "N";
            int parentRctIntNo = 0;

            Session["editRctIntNo"] = rctIntNo;
            //get the receipt no - if available
            Stalberg.TMS.CashReceiptDB receipt = new Stalberg.TMS.CashReceiptDB(connectionString);
            SqlDataReader reader = receipt.GetReceiptDetailsForReversal(rctIntNo);

            if (reader.Read())
            {
                rctIntNo = Convert.ToInt32(reader["RctIntNo"]);
                reversed = reader["Reversed"].ToString();
                // will return 0 if the value is null
                parentRctIntNo = Convert.ToInt32(reader["ParentRctIntNo"]);
            }
            reader.Close();

            if (rctIntNo > 0)
            {
                //receipt found
                Session["editRctIntNo"] = rctIntNo;
                //activate print receipt
                btnOptPrintReceipt.Visible = true;
                //activate receipt enquiry
                btnOptPrintEnquiry.Visible = true;
                //activate reversal
                if (parentRctIntNo == 0 && reversed.Equals("N"))
                    btnOptReverse.Visible = true;
                else
                    btnOptReverse.Visible = false;
            }
            else
            {
                //no receipt found
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                lblError.Visible = true;
            }

            return rctIntNo;
        }

        protected void btnPrintReceipt_Click(object sender, System.EventArgs e)
        {
            this.bThisPage = true;
            DoPrint();
            //PunchStats805806 enquiry ReceiptEnquiry
        }
        
        private void DoPrint() 
        {
            // Print receipt note and display page in new window
            string strRctIntNo = Convert.ToString(Session["editRctIntNo"]);
            
            if (strRctIntNo == "0" || strRctIntNo == string.Empty)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            else
            {
                PageToOpen page;

                if (this.cashier == null || this.cashier.CashBoxType != CashboxType.PostalReceipts)
                {
                    if (alwaysUsePostalReceiptReport.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                    {
                        page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                    }
                    else // (LF-13/11/2008): If Rule 4510 is N then..
                    {
                        page = new PageToOpen(this, "ReceiptNoteViewer.aspx");
                    }
                }
                else
                {
                    page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                }

                if (alwaysUsePostalReceiptReport.Equals("Y", StringComparison.InvariantCultureIgnoreCase) || (this.cashier != null && this.cashier.CashBoxType == CashboxType.PostalReceipts))
                {
                    page.Parameters.Add(new QSParams("date", DateTime.Today.ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("CBIntNo", "0"));
                    page.Parameters.Add(new QSParams("RCtIntNo", strRctIntNo));
                }
                else
                {
                    page.Parameters.Add(new QSParams("receipt", strRctIntNo));
                }

                // (LF-13/11/2008): Build the page.
                Helper_Web.BuildPopup(page);
            }
        }

        protected void btnPrintEnquiry_Click(object sender, EventArgs e)
        {
            // print receipt enquiry, Display page in new window, string strRedirectScript;
            string strRctIntNo = Convert.ToString(Session["editRctIntNo"]);

            //// (LF-13/11/2008): Get the CashBoxType
            //CashboxDB cashBoxDb = new CashboxDB(this.connectionString);
            //string mCashBoxType = cashBoxDb.GetCashBoxType(strRctIntNo, this.txtTicketNumber.Text);
            PageToOpen page = null;

            //// (LF-13/11/2008)
            //switch (mCashBoxType)
            //{
            //    // (LF-13/11/2008): Postal, then run CashReceiptTraffic_PostalReceiptViewer
            //    case "P":
            //        {
            //            //page = new PageToOpen(this, "ReceiptEnquiryViewer.aspx");
            //            Helper_Web.BuildPopup(this, "ReceiptEnquiryViewer.aspx", "receipt", strRctIntNo);
            //            break;
            //        }
            //    default: // Other, then run the standard ReceiptEnquiryViewer
            //        {
            //            Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", strRctIntNo);
            //            //page = new PageToOpen(this, "ReceiptNoteViewer.aspx");
            //            break;
            //        }
            //}
            
            if (strRctIntNo == "0" || strRctIntNo == string.Empty)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            }
            else
            {
                // Generate the JavaScript to pop up a print-only window
                //page.Parameters.Add(new QSParams("RCtIntNo", strRctIntNo));
                //Helper_Web.BuildPopup(page);
                Helper_Web.BuildPopup(this, "ReceiptEnquiryViewer.aspx", "receipt", strRctIntNo);
                //PunchStats805806 enquiry ReceiptEnquiry
            }
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtTicketNumber.Text = TicketNumberSearch1.TicketNumber;
            LookForNotice();
            //PunchStats805806 enquiry ReceiptEnquiry
        }

        protected void Reversal(int rctIntNo)
        {
            //need to know exactly what implications there is to reversing a receipt
            //clearly there is a cash implication - but will there be any cash movement???
            //how long after the original receipt can a reversal occur-today only - this week -any time?
            // return; until we resolve the questions

            // FBJ Added (2006-12-08): Implemented the first version of receipt reversal
            this.Session.Add("RctIntNo", rctIntNo);
            //Helper_Web.BuildPopup(this, "ReceiptReversal.aspx", null);
            Response.Redirect("ReceiptReversal.aspx");

            //dls 070711 - hide all the panels to stop them re-entering the same receipt to reverse
            this.HideResultsPanels();
        }

        protected void btnOptReverse_Click(object sender, EventArgs e)
        {
            int rctIntNo = Convert.ToInt32(Session["editRctIntNo"]);
            this.Reversal(rctIntNo);
            
        }

        protected void btnBarcode_Click(object sender, EventArgs e)
        {
            this.pnlDisplaySelectedTickets.Visible = false;
            //this.pnlShowCharges.Visible = false;
            this.btnOptPrintEnquiry.Visible = false;
            this.btnOptPrintReceipt.Visible = false;
            this.btnOptReverse.Visible = false;

            // Scan a barcode
            try
            {
                string value = this.txtBarcode.Text.Trim();
                string[] split = value.Split(new char[] { ':' });
                string receiptNo = split[0];
                int rctIntNo = int.Parse(split[1]);

                //this.Session.Add("editRctIntNo", rctIntNo);
                Session["editRctIntNo"] = rctIntNo;
                this.pnlDisplaySelectedTickets.Visible = false;
                this.btnOptPrintReceipt.Visible = true;
                this.btnOptPrintEnquiry.Visible = true;
                this.btnOptReverse.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
            }
            catch (Exception ex)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6") + "<br />" + ex.Message;
            }
        }

        protected void dgTicketDetails_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            this.SetSelectedGridIndex(e.NewSelectedIndex);
        }

        private void SetSelectedGridIndex(int index)
        {
            dgTicketDetails.SelectedIndex = index;
            int rctIntNo = Convert.ToInt32(dgTicketDetails.Rows[dgTicketDetails.SelectedIndex].Cells[0].Text);
            this.ViewState.Add("RctIntNo", rctIntNo);
            this.GetReceiptID(rctIntNo);
        }
    }
}
