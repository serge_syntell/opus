using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    public partial class EasyPayNumberSearch : System.Web.UI.UserControl
    {
        private string connectionString = String.Empty;
        private string login;
        private int userIntNo;
        private string sEasyPayNo = String.Empty;
        private int nAutIntNo;

        public event EventHandler EasyPayNoticeSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.nAutIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (!Page.IsPostBack)
            {
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            char cTemp = Convert.ToChar("0");

            if (txtNumber.Text.Trim().Length < 1)
                return;

            this.lblError.Text = string.Empty;

            SqlDataReader reader = EasyPaySearch(this.nAutIntNo, txtNumber.Text);

            this.grdHeader.DataSource = reader;
            this.grdHeader.DataBind();
            if (grdHeader.Rows.Count == 1)
            {
                grdHeader.SelectedIndex = 0;
                this.sEasyPayNo = grdHeader.SelectedRow.Cells[0].Text;
                if (this.EasyPayNoticeSelected != null)
                {
                    this.EasyPayNoticeSelected(this.sEasyPayNo, EventArgs.Empty);
                }
                this.pnlGrid.Visible = false;
                this.lblError.Visible = false;
            }
            else if (grdHeader.Rows.Count > 1)
            {
                this.pnlGrid.Visible = true;
                this.lblError.Visible = false;
            }
            else
            {
                this.pnlGrid.Visible = false;
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text"), txtNumber.Text);
                this.lblError.Visible = true;
                this.txtNumber.Text = string.Empty;
                this.EasyPayNoticeSelected("", EventArgs.Empty);
            }
        }

        protected void grdHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void grdHeader_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            grdHeader.SelectedIndex = e.NewSelectedIndex;
            this.sEasyPayNo = grdHeader.SelectedRow.Cells[0].Text;
            if (this.EasyPayNoticeSelected != null)
            {
                this.EasyPayNoticeSelected(this.sEasyPayNo, EventArgs.Empty);
            }
            this.pnlGrid.Visible = false;
        }

        public string EasyPayReturn
        {
            get { return sEasyPayNo; }
        }

        public int AutIntNo
        {
            get { return nAutIntNo; }
            set { this.nAutIntNo = value; }
        }

        /// <summary>
        /// Gets the new offender authorities.
        /// </summary>
        /// <returns></returns>
        public SqlDataReader EasyPaySearch(int nAutIntNo, string sNumber)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("EasyPayNumberSearch", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = nAutIntNo;
            com.Parameters.Add("@EasyPayNo", SqlDbType.VarChar, 50).Value = sNumber;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }
}
