﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.CourtRoll" Codebehind="CourtRoll.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img height="26" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                                <table id="tblControls" border="0" class="NormalBold" style="width: 648px">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 147px">
                                            <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblSelCourt.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlCourt" runat="server" CssClass="Normal" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlCourt_SelectedIndexChanged" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 147px">
                                            <asp:Label ID="lblCourtRoom" runat="server" CssClass="NormalBold" Visible="true" Text="<%$Resources:lblCourtRoom.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlCourtRoom" runat="server" CssClass="Normal" AutoPostBack="True"
                                                Width="200px" OnSelectedIndexChanged="ddlCourtRoom_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 147px">
                                        </td>
                                        <td>
                                            <asp:Panel ID="Panel1" runat="server" Width="100%">
                                                <asp:GridView ID="grdDates" Width="495px" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                                    CssClass="Normal" GridLines="Horizontal" OnSelectedIndexChanged="grdDates_SelectedIndexChanged"
                                                    ShowFooter="True" AllowPaging="False">
                                                    <FooterStyle CssClass="CartListHead" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="<%$Resources:grdDates.HeaderText %>">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CDate" , "{0:yyyy-MM-dd }") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("CDate", "{0:yyyy-MM-dd }") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="CDNoOfCases" HeaderText="<%$Resources:grdDates.HeaderText1 %>">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CDTotalAllocated" HeaderText="<%$Resources:grdDates.HeaderText2 %>">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:CommandField SelectText="<%$Resources:grdDates.SelectText %>" ShowSelectButton="True" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="CartListHead" />
                                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                                </asp:GridView>
                                                <div style="font-weight:normal;">
                                                <pager:AspNetPager id="grdDatesPager" runat="server" 
                                                showcustominfosection="Right" width="495px" 
                                                CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                                  FirstPageText="|&amp;lt;" 
                                                LastPageText="&amp;gt;|" 
                                                CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                                Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                                CustomInfoStyle="float:right;" 
                                                onpagechanged="grdDatesPager_PageChanged" UpdatePanelId="UpdatePanel1"></pager:AspNetPager>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: left">
                                            <asp:Button ID="btnView" runat="server" CssClass="NormalButton" Visible="false" Text=" <%$Resources:btnView.Text %> " />&nbsp;<asp:Button
                                                ID="btnViewAll" runat="server" CssClass="NormalButton" Text=" <%$Resources:btnViewAll.Text %> " Visible="false" /><td
                                                    style="text-align: left">
                                                </td>
                                            <td style="width: 3px; height: 26px;" align="center">
                                                &nbsp;
                                            </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                      <asp:UpdateProgress ID="udp" runat="server">
                          <ProgressTemplate>
                              <p class="Normal" style="text-align: center;">
                                  <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" /><asp:Label
                                      ID="Label15" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label>
                              </p>
                          </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
