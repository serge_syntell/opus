using System;
using System.Data;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;
using System.Globalization;


namespace Stalberg.TMS
{
    /// <summary>
    /// The starting point for ticket payment receipt
    /// </summary>
    public partial class AdminReceiptTraffic : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string loginUser;
        private Cashier cashier = null;

        protected string styleSheet;
        protected string backgroundImage;
        protected double myTotal = 0;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "AdminReceiptTraffic.aspx";
        //protected string thisPage = "Re-apply Payments which were Reversed by Admin";
        protected int autIntNo = 0;
        protected int userIntNo = 0;
        protected int trhIntNo = 0;

        private bool clearPaymentList = false;

        // Constants
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;

            userDetails = (UserDetails)Session["userDetails"];
            userIntNo = Int32.Parse(Session["userIntNo"].ToString());

            loginUser = userDetails.UserLoginName;

            BankAccountDB bankAccount = new BankAccountDB(connectionString);
            this.cashier = bankAccount.GetUserBankAccount(int.Parse(Session["userIntNo"].ToString()));

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            this.pnlDetails.Visible = false;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (ViewState["ClearPaymentList"] != null)
                clearPaymentList = (bool)ViewState["ClearPaymentList"];

            //dls 080221 - re-application of reversed payments via admin receipt reversal
            if (Session["AdminReceiptReversal"] != null)
            {
                pnlApplyReversed.Visible = true;

                decimal adminRctAmount = decimal.Parse(Session["AdminRctAmount"].ToString());
                string adminRctNumber = Session["AdminRctNumber"].ToString();

                //update by Rachel 20140820 for 5337
                //lblReversedPaymentAmount.Text = string.Format((string)GetLocalResourceObject("lblReversedPaymentAmount.Text1"), adminRctNumber, adminRctAmount.ToString("#, ##0.00"));
                lblReversedPaymentAmount.Text = string.Format((string)GetLocalResourceObject("lblReversedPaymentAmount.Text1"), adminRctNumber, adminRctAmount.ToString("#, ##0.00",CultureInfo.InvariantCulture));
                //end update by Rachel 20140820 for 5337
            }
            else
                pnlApplyReversed.Visible = false;

            // Multiple Notices
            if (Session["NoticesToPay"] == null)
            {
                this.pnlNotices.Visible = false;
                this.Session.Add("NoticesToPay", new List<NoticeForPayment>());

                this.ClearPaymentList(userIntNo);
            }
            else
            {
                List<NoticeForPayment> notices = (List<NoticeForPayment>)this.Session["NoticesToPay"];
                if (notices.Count > 0)
                {
                    this.lblNotices.Text = string.Format((string)GetLocalResourceObject("lblNotices.Text"), notices.Count);
                    this.pnlNotices.Visible = true;
                }
                else
                {
                    this.pnlNotices.Visible = false;
                    //dls 080129 - need to clear any other payments from the temporary table
                    this.ClearPaymentList(userIntNo);
                }
            }

            this.pnlDetails.Visible = true;

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.TicketNumberSearch1.AutIntNo = this.autIntNo;
            }
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            if (this.textId.Text.Trim().Length == 0 && this.textTicket.Text.Trim().Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            PageToOpen page = new PageToOpen(this, "AdminReceiptTraffic_Details.aspx");
            page.Parameters.Add(new QSParams("TicketNo", this.textTicket.Text.Trim()));
            page.Parameters.Add(new QSParams("IDNo", this.textId.Text.Trim()));

            Response.Redirect(page.ToString());
        }

        protected void btnRePrint_Click(object sender, EventArgs e)
        {
            // Get the ticket details from the notice/charge tables for ticket / summons
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            if (this.textTicket.Text.Trim() == string.Empty)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            //DataSet ds = db.ReceiptEnquiryOnTicketNumber(autIntNo, this.textTicket.Text.Trim());
            DataSet ds = db.ReceiptEnquiryOnTicketNumber(this.textTicket.Text.Trim());

            if (ds.Tables[0].Rows.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            else if (ds.Tables[0].Rows.Count == 1)
            {
                int rctIntNo = Convert.ToInt32(ds.Tables[0].Rows[0]["RctIntNo"].ToString());

                PageToOpen page = null;

                //// DLS Added (2007-11-02): Need To check for Authority Rule here - print all receipts to Postal Receipt
                //AuthorityRulesDB arDB = new AuthorityRulesDB(this.connectionString);
                //AuthorityRulesDetails ard = arDB.GetAuthorityRulesDetailsByCode(this.autIntNo, "4510", "Rule to indicate using of postal receipt for all receipts", 0, "N", "Y = Yes; N= No(default)", this.loginUser);

                //CashboxDB cashBoxDb = new CashboxDB(this.connectionString);
                //string mCashBoxType = cashBoxDb.GetCashBoxType(rctIntNo.ToString(), string.Empty);

                //20090113 SD	
                //AutIntNo, ARCode and LastUser need to be set from here
                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = this.autIntNo;
                ard.ARCode = "4510";
                ard.LastUser = this.loginUser;

                DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                if (this.cashier == null || this.cashier.CashBoxType != CashboxType.PostalReceipts)
                {

                    if (value.Value.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                    {
                        page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                    }
                    else
                    {
                        page = new PageToOpen(this, "ReceiptNoteViewer.aspx");
                    }
                }
                else
                {
                    page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                }

                if (value.Value.Equals("Y", StringComparison.InvariantCultureIgnoreCase) || (this.cashier != null && this.cashier.CashBoxType == CashboxType.PostalReceipts))
                {
                    page.Parameters.Add(new QSParams("date", DateTime.Today.ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("CBIntNo", "0"));
                    page.Parameters.Add(new QSParams("RCtIntNo", rctIntNo.ToString()));
                }
                else
                {
                    page.Parameters.Add(new QSParams("receipt", rctIntNo.ToString()));
                }
                //Linda 2012-9-12 add for list report19
                //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                //2013-11-7 Heidi commented out According to the requirements 
                //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                //punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.loginUser, PunchStatisticsTranTypeList.AdminReceiptTraffic, PunchAction.Other);           
            
                Helper_Web.BuildPopup(page);
            }
            else
            {
                PageToOpen page = new PageToOpen(this, "ReceiptEnquiry.aspx");
                page.Parameters.Add(new QSParams("NotTicketNo", this.textTicket.Text.Trim()));
                Response.Redirect(page.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.Session["NoticesToPay"] = new List<NoticeForPayment>();
            this.pnlNotices.Visible = false;

            ClearPaymentList(this.userIntNo);
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.textTicket.Text = this.TicketNumberSearch1.TicketNumber;
        }

        private void ClearPaymentList(int userIntNo)
        {
            if (!clearPaymentList)
            {
                CashReceiptDB cashReceipt = new CashReceiptDB(this.connectionString);
                cashReceipt.RemovePaymentListForUser(userIntNo);
                clearPaymentList = true;
                //2013-12-11 Heidi added for add all Punch Statistics Transaction(5084)
                //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                //punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.loginUser, PunchStatisticsTranTypeList.AdminReceiptTraffic, PunchAction.Delete);  
                ViewState.Add("ClearPaymentList", clearPaymentList);
            }
            else
            {
                Session["NoticeAddressDetails"] = null;
                Session["TRHIntNo"] = null;
                Session["TRHTranDate"] = null;
            }
        }

    }
}
