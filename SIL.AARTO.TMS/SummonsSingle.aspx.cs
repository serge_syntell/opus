using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using Stalberg.TMS_TPExInt.Components;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class SummonsSingle : System.Web.UI.Page
    {
        /// <summary>
        /// Lists the states that Single Summons can be in
        /// </summary>
        private enum SingleSummonsState
        {
            /// <summary>
            /// No State - invalid
            /// </summary>
            None = 0,
            /// <summary>
            /// Searching for a new result set
            /// </summary>
            Search = 1,
            /// <summary>
            /// Displaying a valid result set
            /// </summary>
            Results = 2,
            /// <summary>
            /// The user must decide what to do with a NoAOG
            /// </summary>
            NOAOGDecision = 3,
            /// <summary>
            /// The selected notice needs an on-the-fly representation
            /// </summary>
            OnTheFlyRepresentation = 4,
            /// <summary>
            /// A summons server needs to be selected for the offender's area
            /// </summary>
            SummonsServer = 5,
            /// <summary>
            /// A court and court date need to be selected before the summons can be generated
            /// </summary>
            CourtDate = 6,
            /// <summary>
            /// The notice is being converted to a summons
            /// </summary>
            GenerateSummons = 7,
            /// <summary>
            /// Prompts the user to confirm that offender's personal details
            /// </summary>
            OffenderDetails = 8
            ///// <summary>
            ///// Display the valid results for a roadblock
            ///// </summary>
            //RoadBlock = 9,
            ///// display the grid to show the multple print options for the roadbloack summons
            //MultipleSummonsPrint = 10
        }

        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private Regex regex = new Regex("^\\d{13}$", RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.Singleline);

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsSingle.aspx";
        //protected string thisPage = "Single Summons";

        protected double dPercentage = 0.0;
        protected string useOriginalFineAmount = "Y";

        // Constant
        protected const string stationeryType = "CPA5";         //dls rule no longer in use - misunderstanding on usage of different stationery types. CPA6 is used for HWO = separate issue
        //private const string SELECT_SUMMONS_SERVER = "";
        //private const string SELECT_COURT = "";
        //private const string SELECT_COURT_ROOM = "";
        //private const string NO_COURT_DATES = "";
        private const string SUM_STATUS = "Pending";
        private const string PROCESS_NAME = "TMS-SingleSummons";
        private const int STATUS_SUMMONS_PRINTED = 620;
        private const int STATUS_SUMMONS_SERVED_PERSONAL = 641;             //dls 090606 - added: need to set status when issuing summons at roadblock
        //private const string STATIONERY_MESSAGE = "";
        private const string STATIONERY_CPA5 = "CPA5";
        private const string STATIONERY_CPA6 = "CPA6";
        private const string DATE_AND_TIME_FORMAT = "yyyy-MM-dd_HH-mm";

        private DataSet dsSummonsMultiplePrint = null;
        NoticeFilmTypes nft = new NoticeFilmTypes();
        private NoticeFilmType noticeFilmType = NoticeFilmType.Others;

        //jerry 2011-11-15 add NotFilmType
        private string notFilmType = string.Empty;

        //jerry 2011-12-26 add
        private int noDaysForSummonsExpiry;
        private string autCode = string.Empty;
        //Jerry 2012-05-22 add
        List<string> chgIntNoList = new List<string>();
        List<string> chgRowVersionList = new List<string>();

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            ViewState.Add("AutIntNo", autIntNo);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.TicketNumberSearch1.NoticeSelected += new EventHandler(TicketNumberSearch1_NoticeSelected);
            this.RepresentationOnTheFly1.RepresentationSuccessful += new RepresentationOnTheFlyEventHandler(RepresentationOnTheFly1_RepresentationSuccessful);
            RepresentationOnTheFly1.AutIntNo = this.autIntNo;

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            ////dls 20081124 - store the list of summons for printing in a dataset in the view state
            //if (ViewState["dsSummonsMultiplePrint"] != null)
            //{
            //    dsSummonsMultiplePrint = (DataSet)ViewState["dsSummonsMultiplePrint"];
            //    this.SetPanelVisibility(SingleSummonsState.MultipleSummonsPrint);
            //}

            if (ViewState["dPercentage"] == null)
            {
                GetDefaultCourtPercentage();
                ViewState["dPercentage"] = dPercentage.ToString();
            }
            else
            {
                dPercentage = double.Parse(ViewState["dPercentage"].ToString());
            }

            //dls rule no longer in use - misunderstanding on usage of different stationery types. CPA6 is used for HWO = separate issue
            ////Modified by Jake 2010-04-16
            ////if (ViewState["stationeryType"] != null)
            //if (ViewState["stationeryType"] == null)
            //{
            //    GetDefaultStationeryType();
            //    ViewState["stationeryType"] = stationeryType;
            //}
            //else
            //{
            //    stationeryType = ViewState["stationeryType"].ToString();
            //}

            if (ViewState["useOriginalFineAmount"] == null)
            {
                GetDefaultOriginalFineAmountRule();
                ViewState["useOriginalFineAmount"] = useOriginalFineAmount;
            }
            else
            {
                useOriginalFineAmount = ViewState["useOriginalFineAmount"].ToString();
            }

            //jerry 2011-12-26 add for pushing queue of cancel expired summons
            GetDefaultDateRule();
            GetAutCode();

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.TicketNumberSearch1.AutIntNo = this.autIntNo;
                this.SetPanelVisibility(SingleSummonsState.Search);

                AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                arDetails.AutIntNo = autIntNo;
                arDetails.ARCode = "6209";
                arDetails.LastUser = this.login;
                DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
                //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)
                bool isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

                if (isIBMPrinter)
                {
                    this.lnkSummonsPrint.Visible = true;
                    this.lnkSummonsPrint.Enabled = false;
                    this.lblStationeryMessage.Text = (string)GetLocalResourceObject("errorMsg");
                }
            }
            //Jerry 2013-09-16 handle the problem the Select enable updated when click the lookup button
            else
            {
                if (grdNotices != null && grdNotices.Rows.Count > 0)
                {
                    for (int i = 0; i < grdNotices.Rows.Count; i++)
                    {
                        Label lblNoticeStatus = (Label)grdNotices.Rows[i].FindControl("lblNoticeStatus");
                        var noticeStatus = Convert.ToInt32(lblNoticeStatus.Text);
                        grdNotices.Rows[i].Cells[11].Enabled = noticeStatus < 610;
                    }
                }
            }
        }

        //Jake 2015-03-19 use new data rule
        /// <summary>
        /// jerry 2011-12-16 add 
        /// </summary>
        private void GetDefaultDateRule()
        {
            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.DtRStartDate = "NotOffenceDate";
            rule.DtREndDate = "SumExpireDate";
            //rule.DtRStartDate = "SumIssueDate";
            //rule.DtREndDate = "SumExpiredDate";
            rule.LastUser = this.login;

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            noDaysForSummonsExpiry = dateRule.SetDefaultDateRule();
        }

        /// <summary>
        /// jerry 2011-12-26 add autCode for push queue
        /// </summary>
        /// <param name="autIntNo"></param>
        private void GetAutCode()
        {
            this.autCode = new AuthorityDB(this.connectionString).GetAuthorityDetails(this.autIntNo).AutCode.Trim();
        }

        private void GetDefaultOriginalFineAmountRule()
        {
            //wl 20090415 - assign the different fine amount based on original fine rule
            AuthorityRulesDetails originalFineRule = new AuthorityRulesDetails();
            originalFineRule.AutIntNo = autIntNo;
            originalFineRule.ARCode = "5030";
            originalFineRule.LastUser = this.login;

            DefaultAuthRules ar = new DefaultAuthRules(originalFineRule, this.connectionString);
            KeyValuePair<int, string> originalFineAmntRule = ar.SetDefaultAuthRule();

            this.useOriginalFineAmount = originalFineAmntRule.Value;
        }

        //dls rule no longer in use - misunderstanding on usage of different stationery types. CPA6 is used for HWO = separate issue
        //private void GetDefaultStationeryType()
        //{
        //    // CPA6 for impersonal summons and CPA5 for summons personal
        //    AuthorityRulesDetails stationeryTypeRule = new AuthorityRulesDetails();
        //    stationeryTypeRule.AutIntNo = autIntNo;
        //    stationeryTypeRule.ARCode = "5070";
        //    stationeryTypeRule.LastUser = this.login;

        //    DefaultAuthRules ar = new DefaultAuthRules(stationeryTypeRule, this.connectionString);
        //    KeyValuePair<int, string> stationeryRule = ar.SetDefaultAuthRule();

        //    this.stationeryType = stationeryRule.Value;
        //}

        private void GetDefaultCourtPercentage()
        {
            AuthorityRulesDetails rule2 = new AuthorityRulesDetails();
            rule2.AutIntNo = this.autIntNo;
            rule2.ARCode = "5020";
            rule2.LastUser = "system";

            DefaultAuthRules authRule2 = new DefaultAuthRules(rule2, this.connectionString);
            KeyValuePair<int, string> percentageRule = authRule2.SetDefaultAuthRule();

            this.dPercentage = (double)percentageRule.Key;
        }

        private void RepresentationOnTheFly1_RepresentationSuccessful(object sender, RepresentationOnTheFlyEventArgs e)
        {
            //Jerry 2013-03-13 get new charge rowversion
            this.ViewState.Remove("SearchResults");
            string regNo = this.txtRegno.Text.Trim();
            string idNumber = this.txtIDNumber.Text.Trim();
            string ticketNo = this.txtTicketNo.Text.Trim();
            GetSingleSummonsNoticesSourceRefresh(regNo, idNumber, ticketNo);

            this.ViewState.Add("FineAmount", e.NewAmount);
            string areaCode = (string)this.grdNotices.SelectedDataKey["AreaCode"];
            this.ShowSummonsServer(areaCode);
            //Jerry 2013-03-13 add
            lblError.Text = (string)GetLocalResourceObject("lblError.Text40"); //"Representation Successful";

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.GenerateSingleSummonsMakeRepresentation, PunchAction.Change);

        }

        private void TicketNumberSearch1_NoticeSelected(object sender, EventArgs e)
        {
            this.txtTicketNo.Text = TicketNumberSearch1.TicketNumber;
            this.ViewState.Remove("SearchResults");
            this.Search();
        }

        /// <summary>
        /// Handles the Click event of the btnHideMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>


        /// <summary>
        /// Handles the Click event of the btnSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.ViewState.Remove("SearchResults");
            this.Search();
            //PunchStats805806 enquiry GenerateSingleSummons
        }

        // Oscar 20101112 - added, get notices data source
        private DataSet GetSingleSummonsNoticesSourceRefresh(string regNo, string idNumber, string ticketNo)
        {
            DataSet source = new DataSet();
            if (!string.IsNullOrEmpty(regNo) || !string.IsNullOrEmpty(idNumber) || !string.IsNullOrEmpty(ticketNo))
            {
                SummonsDB db = new SummonsDB(this.connectionString);
                source = db.GetSingleSummonsNotices(regNo, idNumber, ticketNo, this.autIntNo);

                ViewState["SearchResults"] = source;
            }
            return source;
        }
        private DataSet GetSingleSummonsNoticesSource(string regNo, string idNumber, string ticketNo)
        {
            DataSet source = new DataSet();
            if (ViewState["SearchResults"] != null)
            {
                source = ViewState["SearchResults"] as DataSet;
            }
            if (source.Tables.Count == 0 || source.Tables[0].Rows.Count == 0)
            {
                source = GetSingleSummonsNoticesSourceRefresh(regNo, idNumber, ticketNo);
            }
            return source;
        }

        private void Search()
        {
            DataSet ds = this.ViewState["SearchResults"] as DataSet;
            if (ds == null)
            {
                string regNo = this.txtRegno.Text.Trim();
                string idNumber = this.txtIDNumber.Text.Trim();
                string ticketNo = this.txtTicketNo.Text.Trim();

                if (regNo.Length == 0 && idNumber.Length == 0 && ticketNo.Length == 0)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    return;
                }

                //SummonsDB db = new SummonsDB(this.connectionString);
                //ds = db.GetSingleSummonsNotices(regNo, idNumber, ticketNo, this.chkRoadblock.Checked);
                //ds = db.GetSingleSummonsNotices(regNo, idNumber, ticketNo);
                //this.ViewState.Add("SearchResults", ds);
                // Oscar 20101112 - changed
                ds = GetSingleSummonsNoticesSourceRefresh(regNo, idNumber, ticketNo);
            }

            if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                this.SetPanelVisibility(SingleSummonsState.Search);
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            this.SetPanelVisibility(SingleSummonsState.Results);
            this.lblError.Text = string.Empty;
            this.grdNotices.DataSource = ds;
            this.grdNotices.DataKeyNames = new string[] { "NotIntNo", "ChgIntNo", "OriginalAmount", "RevisedAmount", "TicketNo", "OffenceDate", "SummonsNo", "AreaCode", "NoAOG", "PrintFile", "ChargeRowVersion", "NotProxyFlag", "ChargeType", "NotFilmType" };
            this.grdNotices.DataBind();

        }

        private void DisplaySummonsError(int addSumIntNo, string ticketNo, string errMessage)
        {
            switch (addSumIntNo)
            {
                case -1:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    break;
                case -2:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3") + ticketNo
                        + (string)GetLocalResourceObject("lblError.Text4");
                    break;
                case -3:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                    break;
                case -4:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    break;
                case -5:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                    break;
                case -6:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                    break;
                case -7:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                    break;
                case -8:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                    break;
                case -9:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                    break;
                case -10:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                    break;
                case -11:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                    break;
                case -12:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                    break;
                case -13:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text15");
                    break;
                case -14:
                case -15:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text16");
                    break;
                case -16:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text17");
                    break;
                case -17:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text18");
                    break;
                case -30:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text19") + errMessage;
                    break;
                default:
                    break;
            }
        }

        private void GetSummonsDetails(int notIntNo, ref string printFile, ref string summonsNumber)
        {
            SummonsDB summons = new SummonsDB(this.connectionString);
            SqlDataReader reader = summons.SummonsAndWarrantDetails(notIntNo);

            while (reader.Read())
            {
                printFile = reader["SumPrintFileName"].ToString();
                summonsNumber = reader["SummonsNo"].ToString();
            }

            reader.Dispose();
        }

        protected void grdNotices_SelectedIndexChanged(object sender, EventArgs e)
        {
            string areaCode = string.Empty;
            decimal revisedAmount = (decimal)this.grdNotices.SelectedDataKey["RevisedAmount"];
            decimal originalAmount = (decimal)this.grdNotices.SelectedDataKey["OriginalAmount"];
            string ticketNo = (string)this.grdNotices.SelectedDataKey["TicketNo"];
            int notIntNo = (int)this.grdNotices.SelectedDataKey["NotIntNo"];
            //jerry 2011-11-15 add
            this.notFilmType = (string)this.grdNotices.SelectedDataKey["NotFilmType"];

            //DateTime offenceDate = (DateTime)this.grdNotices.SelectedDataKey["OffenceDate"];
            DateTime offenceDate;
            if (!DateTime.TryParse(this.grdNotices.SelectedDataKey["OffenceDate"].ToString(), out offenceDate))
            {
                this.SetPanelVisibility(SingleSummonsState.Search);
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text20");
                return;
            }

            string summonsNo = (string)this.grdNotices.SelectedDataKey["SummonsNo"];

            //short area;
            //if (!short.TryParse(this.grdNotices.SelectedDataKey["AreaCode"], out area))
            if (this.grdNotices.SelectedDataKey["AreaCode"] != DBNull.Value)
            {
                areaCode = (string)this.grdNotices.SelectedDataKey["AreaCode"];
            }
            else
            {
                //this.lblError.Text = "The area code is not valid - probably null!";
                areaCode = string.Empty;
                //return;
            }
            //added noaog column
            string noaog = (string)this.grdNotices.SelectedDataKey["NoAOG"];
            string printFile = (string)this.grdNotices.SelectedDataKey["PrintFile"];

            //this.ViewState.Add("FineAmount", revisedAmount);
            this.ViewState.Add("RevisedAmount", revisedAmount);
            this.ViewState.Add("FineAmount", revisedAmount);
            this.ViewState.Add("noaog", noaog);
            this.ViewState.Add("notFilmType", this.notFilmType);
            this.ViewState.Add("NotIntNo", notIntNo); //Jerry 2014-05-30 add

            // Check if the offence was less than 18 months ago
            if (offenceDate.AddMonths(18).Ticks < DateTime.Now.Ticks)
            {
                this.SetPanelVisibility(SingleSummonsState.Search);
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text21");
                return;
            }

            // If the summons has been generated show it for printing
            if (summonsNo.Length == 0)
            {
                // Populate the offender's details
                OffenderDB db = new OffenderDB(this.connectionString);
                OffenderDetails details = db.GetOffenderDetails(notIntNo);

                this.txtAddress1.Text = details.Address1;
                this.txtAddress2.Text = details.Address2;
                this.txtAddress3.Text = details.Address3;
                this.txtAddress4.Text = details.Address4;
                this.txtAreaCode.Text = details.AreaCode;
                this.txtForenames.Text = details.ForeNames;
                this.txtSurname.Text = details.Surname;
                this.txtOffenderIDNumber.Text = details.IDNumber;

                this.ViewState.Add("OffenderDetails", details);

                this.SetPanelVisibility(SingleSummonsState.OffenderDetails);
                return;
            }
            else
            {
                this.ViewState.Add("PrintFile", printFile);
                this.CreateSummonsLink(summonsNo);
                this.SetPanelVisibility(SingleSummonsState.GenerateSummons);
                return;
            }

            //// If the offence is a noaog ask the user what to do about it
            //if (noaog == "Y")
            //{ 
            //    //is a noaog
            //    this.SetPanelVisibility(SingleSummonsState.NOAOGDecision);
            //    return;
            //}

        }

        private void ShowSummonsServer(string areaCode)
        {
            //areacode is now string
            //short area;
            //if (!short.TryParse(areaCode, out area))
            //{
            //    this.lblError.Text = string.Format("The area code ({0}) is not valid.", area);
            //    return;
            //}
            this.cboSummonsServer.Items.Clear();
            SummonsServerDB db = new SummonsServerDB(this.connectionString);

            SqlDataReader reader = db.GetSummonsServerForArea(this.autIntNo, areaCode);
            while (reader.Read())
            {
                ListItem item = new ListItem();
                item.Value = reader["ASIntNo"].ToString();
                item.Text = reader["SSFullName"].ToString();
                this.cboSummonsServer.Items.Add(item);
            }
            reader.NextResult();
            string areaDescription = areaCode;
            if (reader.Read())
            {
                if (reader["AreaCode"] != DBNull.Value)
                    areaDescription = reader["AreaCode"].ToString();
                if (reader["AreaDescr"] != DBNull.Value && reader["AreaDescr"].ToString().Trim().Length > 0)
                    areaDescription = areaDescription + " (" + reader["AreaDescr"].ToString() + ")";
            }
            this.lblSummonsServerArea.Text = areaDescription;

            reader.Close();

            this.cboSummonsServer.Items.Insert(0, (string)GetLocalResourceObject("SELECT_SUMMONS_SERVER"));

            this.SetPanelVisibility(SingleSummonsState.SummonsServer);
        }

        private void SetPanelVisibility(SingleSummonsState state)
        {
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)
            bool isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

            this.lblError.Text = string.Empty;

            this.pnlSearch.Visible = false;
            this.pnlResults.Visible = false;
            this.pnlRepresentation.Visible = false;
            this.pnlSummonsServer.Visible = false;
            this.pnlCourt.Visible = false;
            this.pnlSummonsFirstPrint.Visible = false;
            this.pnlPersonalDetails.Visible = false;
            this.pnlNOAOGDecision.Visible = false;
            //this.pnlRoadBlock.Visible = false;
            //this.pnlSummonsMultiplePrint.Visible = false;

            switch (state)
            {
                case SingleSummonsState.Search:
                    this.pnlSearch.Visible = true;
                    this.txtIDNumber.Text = string.Empty;
                    this.txtRegno.Text = string.Empty;
                    this.txtTicketNo.Text = string.Empty;
                    break;
                case SingleSummonsState.Results:
                    this.pnlSearch.Visible = true;
                    this.pnlResults.Visible = true;
                    break;
                //case SingleSummonsState.RoadBlock:
                //    // Populate the offender's details
                //    int notIntNo = (int)this.grdRoadBlock.DataKeys[0].Value;
                //    OffenderDB dbO = new OffenderDB(this.connectionString);
                //    OffenderDetails details = dbO.GetOffenderDetails(notIntNo);
                //    this.txtRoadPhysical1.Text = details.Address1;
                //    this.txtRoadPhysical2.Text = details.Address2;
                //    this.txtRoadPhysical3.Text = details.Address3;
                //    this.txtRoadPhysical4.Text = details.Address4;
                //    this.txtRoadAreaCode.Text = details.AreaCode;
                //    this.txtRoadForenames.Text = details.ForeNames;
                //    this.txtRoadSurname.Text = details.Surname;
                //    this.txtRoadIDNumber.Text = details.IDNumber;
                //    this.ViewState.Add("OffenderDetails", details);
                //    //set court drop down list
                //    this.cboRoadCourt.Items.Clear();
                //    CourtDB dbCourt = new CourtDB(this.connectionString);
                //    SqlDataReader reader = dbCourt.GetAuth_CourtListByAuth(this.autIntNo);
                //    while (reader.Read())
                //    {
                //        ListItem item = new ListItem();
                //        item.Value = reader["CrtIntNo"].ToString() + "~" + reader["ACIntNo"].ToString();
                //        item.Text = reader["CrtName"].ToString();
                //        this.cboRoadCourt.Items.Add(item);
                //    }
                //    reader.Close();
                //    this.cboRoadCourt.Items.Insert(0, SELECT_COURT);
                //    this.pnlSearch.Visible = true;
                //    this.pnlRoadBlock.Visible = true;
                //    break;
                case SingleSummonsState.NOAOGDecision:
                    this.pnlNOAOGDecision.Visible = true;
                    //this.txtNewAmount.Text = string.Empty;
                    break;
                case SingleSummonsState.OnTheFlyRepresentation:
                    this.pnlRepresentation.Visible = true;
                    this.txtNewAmount.Text = string.Empty;
                    break;
                case SingleSummonsState.SummonsServer:
                    ////BD need to populate the summons location drop down
                    //this.cboSummonsLocation.Items.Clear();
                    //SummonsDB db = new SummonsDB(this.connectionString);
                    //this.cboSummonsLocation.DataSource = db.GetSummonsLocationGenerated();
                    //this.cboSummonsLocation.DataTextField = "SummonsLocation";
                    //this.cboSummonsLocation.DataValueField = "SummonsLocation";
                    //this.cboSummonsLocation.DataBind();
                    //this.cboSummonsLocation.SelectedIndex = 0;
                    this.pnlSummonsServer.Visible = true;
                    break;
                case SingleSummonsState.CourtDate:
                    this.lblCourtDate.Text = (string)GetLocalResourceObject("NO_COURT_DATES");
                    this.pnlCourt.Visible = true;
                    break;
                case SingleSummonsState.GenerateSummons:
                    this.pnlSummonsFirstPrint.Visible = true;

                    if (isIBMPrinter)
                    {
                        this.lblStationeryMessage.Text = (string)GetLocalResourceObject("errorMsg");
                    }
                    else
                    {
                        if (this.noticeFilmType == NoticeFilmType.H)
                            lblStationeryMessage.Text = string.Format((string)GetLocalResourceObject("STATIONERY_MESSAGE"), STATIONERY_CPA6);
                        else
                            lblStationeryMessage.Text = string.Format((string)GetLocalResourceObject("STATIONERY_MESSAGE"), STATIONERY_CPA5);
                    }
                    break;
                case SingleSummonsState.OffenderDetails:
                    this.pnlPersonalDetails.Visible = true;
                    break;
                //case SingleSummonsState.MultipleSummonsPrint:
                //    this.pnlSummonsMultiplePrint.Visible = true;
                //    PopulateMultipleSummons();
                //    break;
            }
        }

        protected void grdNotices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.grdNotices.PageIndex = e.NewPageIndex;
            this.Search();
            //PunchStats805806 enquiry GenerateSingleSummons
        }

        protected void lnkSearch_Click(object sender, EventArgs e)
        {
            this.SetPanelVisibility(SingleSummonsState.Search);
        }

        protected void lblResults_Click(object sender, EventArgs e)
        {
            this.SetPanelVisibility(SingleSummonsState.Results);
        }

        protected void txtNewAmount_TextChanged(object sender, EventArgs e)
        {
            decimal amount = 0;
            decimal.TryParse(this.txtNewAmount.Text, out amount);
            this.RepresentationOnTheFly1.NewAmount = amount;
        }

        private DataTable MakeSummonsTable()
        {
            // Create a new DataTable titled 'Names.'
            DataTable table = new DataTable("Summons");

            // Add three column objects to the table.
            DataColumn dcChgIntNo = new DataColumn();
            dcChgIntNo.DataType = System.Type.GetType("System.Int32");
            dcChgIntNo.ColumnName = "ChgIntNo";
            table.Columns.Add(dcChgIntNo);

            DataColumn dcSumIntNo = new DataColumn();
            dcSumIntNo.DataType = System.Type.GetType("System.Int32");
            dcSumIntNo.ColumnName = "SumIntNo";
            table.Columns.Add(dcSumIntNo);

            DataColumn dcSummonsNo = new DataColumn();
            dcSummonsNo.DataType = System.Type.GetType("System.String");
            dcSummonsNo.ColumnName = "SummonsNo";
            table.Columns.Add(dcSummonsNo);

            DataColumn dcPrintFile = new DataColumn();
            dcPrintFile.DataType = System.Type.GetType("System.String");
            dcPrintFile.ColumnName = "PrintFile";
            table.Columns.Add(dcPrintFile);

            DataColumn dcAutIntNo = new DataColumn();
            dcAutIntNo.DataType = System.Type.GetType("System.Int32");
            dcAutIntNo.ColumnName = "PrintAutIntNo";
            table.Columns.Add(dcAutIntNo);

            // Create an array for DataColumn objects.
            DataColumn[] keys = new DataColumn[1];
            keys[0] = dcChgIntNo;
            table.PrimaryKey = keys;

            // Return the new DataTable.
            return table;
        }

        protected void btnAssign_Click(object sender, EventArgs e)
        {
            if (this.cboSummonsServer.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_SUMMONS_SERVER")))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text22");
                return;
            }

            this.cboCourt.Items.Clear();

            int autIntNo = Convert.ToInt32(ViewState["AutIntNo"].ToString());

            CourtDB db = new CourtDB(this.connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(this.autIntNo);
            while (reader.Read())
            {
                ListItem item = new ListItem();
                item.Value = reader["CrtIntNo"].ToString() + "~" + reader["ACIntNo"].ToString();
                item.Text = reader["CrtName"].ToString();
                this.cboCourt.Items.Add(item);
            }
            reader.Close();

            this.cboCourt.Items.Insert(0, (string)GetLocalResourceObject("SELECT_COURT"));

            this.SetPanelVisibility(SingleSummonsState.CourtDate);
        }

        protected void btnSelectCourt_Click(object sender, EventArgs e)
        {
            if (this.cboCourt.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_COURT")))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text23");
                return;
            }

            DateTime courtDate;
            if (this.hidCourtDate.Value.Equals(string.Empty))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text24");
                return;
            }
            //Jerry 2012-05-22 add
            else
            {
                courtDate = Convert.ToDateTime(hidCourtDate.Value);
            }

            // Get all the variables needed
            string errMessage = string.Empty;
            string ticketNo = (string)this.grdNotices.SelectedDataKey["TicketNo"];
            int chgIntNo = (int)this.grdNotices.SelectedDataKey["ChgIntNo"];
            string proxyFlag = (string)this.grdNotices.SelectedDataKey["NotProxyFlag"];

            //mrs 20081122 must store in viewstate as set in two different streams
            ViewState["ChgIntNo"] = chgIntNo;

            //dls 080602 - added to make sure the charge has not been updated by someone else
            Int64 chargeRowVersion = Int64.Parse(this.grdNotices.SelectedDataKey["ChargeRowVersion"].ToString());

            //int ssIntNo = int.Parse(this.cboSummonsServer.SelectedValue);
            int asIntNo = int.Parse(this.cboSummonsServer.SelectedValue);

            decimal amount = (decimal)this.ViewState["FineAmount"];
            //mrs 20080812 acintno redundant in summons
            //int acIntNo = (int)this.ViewState["ACIntNo"];
            //int acIntNo = 0;

            //string caseNo = CASE_NO;
            string sumStatus = SUM_STATUS;
            //mrs 20080812 get fk's etc from viewstate
            int autIntNo = Convert.ToInt32(ViewState["AutIntNo"].ToString());
            int crtIntNo = Convert.ToInt32(ViewState["CrtIntNo"].ToString());
            int crtRIntNo = Convert.ToInt32(ViewState["CrtRIntNo"].ToString());
            int notIntNo = (int)this.grdNotices.SelectedDataKey["NotIntNo"];
            ////BD variable to handle summons location
            //string sumLocation = this.cboSummonsLocation.SelectedValue;

            #region Jerry 2012-05-22 change
            //// Get the CourtDateRule
            //DateRulesDetails ruleCourtDate = this.CheckDateRule();

            ////Jerry 2012-05-17 add 3040 court rule
            //CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            //courtRulesDetails.CrtIntNo = Convert.ToInt32(crtIntNo);
            //courtRulesDetails.CRCode = "3040";
            //courtRulesDetails.LastUser = this.login;
            //DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
            //KeyValuePair<int, string> rule3040 = courtRule.SetDefaultCourtRule();

            ////jerry 2012-03-30 add court rule
            //courtRulesDetails = new CourtRulesDetails();
            //courtRulesDetails.CrtIntNo = Convert.ToInt32(crtIntNo);
            //courtRulesDetails.CRCode = "3030";
            //courtRulesDetails.LastUser = this.login;
            //courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
            //KeyValuePair<int, string> rule3030 = courtRule.SetDefaultCourtRule();


            ////jerry 2012-03-30 add
            //bool isNoAOG = false;
            //List<string> chgIntNoList = new List<string>();
            //List<string> chgRowVersionList = new List<string>();
            //DataView noticeSource = new DataView(GetSingleSummonsNoticesSource(this.txtRegno.Text.Trim(), this.txtIDNumber.Text.Trim(), ticketNo).Tables[0]);
            //noticeSource.RowFilter = string.Format("NotIntNo = {0}", notIntNo);
            //foreach (DataRowView dr in noticeSource)
            //{
            //    chgIntNoList.Add(dr["ChgIntNo"].ToString());
            //    chgRowVersionList.Add(dr["ChargeRowVersion"].ToString());
            //    this.noticeFilmType = nft.GetNoticeFilmType(dr["NotFilmType"].ToString());
            //    if (dr["NoAOG"].ToString() == "Y")
            //    {
            //        isNoAOG = true;
            //    }
            //}

            //ViewState["chgIntNoList"] = chgIntNoList;

            //string needRestrictNoAOG = null;
            ////Jerry 2012-05-21 add
            //string needSeparateNoAOG = null;

            //if (courtRule3040.Value == "Y")
            //{
            //    if (isNoAOG)
            //    {
            //        needSeparateNoAOG = "Y";
            //    }
            //}
            //else
            //{
            //    if (isNoAOG && courtRule3030.Value == "Y")
            //    {
            //        needRestrictNoAOG = "Y";
            //    }
            //}

            //// Update the court date
            //TMSData db = new TMSData(this.connectionString);

            ////mrs 20080811 changed to autintno, crtRintno & removed acintno return date or null
            ////dls 090623 - this proc will no longer increment the no. of S54 cases allocated to the court date - it has been moved inside the SummonsAdd SP so that
            ////  it is contained inside the transaction
            ////jerry 2011-11-15 add parameter
            //DateTime courtDate = db.GetSummonsCourtDate(autIntNo, crtIntNo, ruleCourtDate.DtRNoOfDays, this.dPercentage, PROCESS_NAME, ViewState["notFilmType"].ToString(), ref crtRIntNo, needRestrictNoAOG, needSeparateNoAOG);

            //// Log the detailed problem
            //switch (crtRIntNo)
            //{
            //    case -1:
            //        lblError.Text = "No court date was available for this summons.";
            //        return;

            //    case -2:
            //        lblError.Text = "No court date was available as either the authority or the court does not point to a valid courtRoom / courtDate row.";
            //        return;
            //}
            #endregion

            TMSData db = new TMSData(this.connectionString);
            if (ViewState["chgIntNoList"] != null)
            {
                chgIntNoList = (List<string>)ViewState["chgIntNoList"];
            }
            if (ViewState["chgRowVersionList"] != null)
            {
                chgRowVersionList = (List<string>)ViewState["chgRowVersionList"];
            }

            // Create the summons number
            SummonsNumbers sn = new SummonsNumbers();
            // Date Rule for Last AOG
            DateTime dtLastAOG = this.CheckLastAOGDate(courtDate);

            // DateRule for Summons Served By
            DateTime dtSumServedBy = this.CheckSummonsServedBy(courtDate);

            // Write the summons
            //jerry 2012-03-30 change
            //int notIntNo = (int)this.grdNotices.SelectedDataKey["NotIntNo"];
            //BD 20080527 change to the print file name to include more information
            //court nane + court date + summons server code + sequential Batch no
            //string printFile = PROCESS_NAME + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            //need to get court name
            CourtDB crt = new CourtDB(this.connectionString);
            CourtDetails crtDetails = new CourtDetails();
            crtDetails = crt.GetCourtDetails(crtIntNo);

            //need to get summons server code
            SummonsServerDB ssDB = new SummonsServerDB(this.connectionString);
            SummonsServerDetails ssDetails = new SummonsServerDetails();

            ssDetails = ssDB.GetSummonsServerDetails(0, asIntNo);

            //need to get and set summons server sequence
            CourtDatesDB crtDates = new CourtDatesDB(this.connectionString);

            //int addSumIntNo = db.AddSummons(notIntNo, chgIntNo, sumStatus, ticketNo, proxyFlag, this.stationeryType,
            //   courtDate, dtLastAOG, dtSumServedBy, this.login, asIntNo, sumLocation, chargeRowVersion,
            //   crtIntNo, autIntNo, crtRIntNo, this.useOriginalFineAmount, ref errMessage, 0);
            // Oscar 20101112 - changed 
            // Oscar 20101112 - Get multiple charges
            // jerry 2012-03-30 change
            //List<string> chgIntNoList = new List<string>();
            //List<string> chgRowVersionList = new List<string>();
            //DataView noticeSource = new DataView(GetSingleSummonsNoticesSource(this.txtRegno.Text.Trim(), this.txtIDNumber.Text.Trim(), ticketNo).Tables[0]);
            //noticeSource.RowFilter = string.Format("NotIntNo = {0}", notIntNo);
            //foreach (DataRowView dr in noticeSource)
            //{
            //    chgIntNoList.Add(dr["ChgIntNo"].ToString());
            //    chgRowVersionList.Add(dr["ChargeRowVersion"].ToString());
            //    this.noticeFilmType = nft.GetNoticeFilmType(dr["NotFilmType"].ToString());
            //}

            //ViewState["chgIntNoList"] = chgIntNoList;
            //ViewState["chgRowVersionList"] = chgRowVersionList;

            //dls 2011-12-06 Single summons are created one at a time ==> no batchsize required
            //// jerry 20101229 add rule to control the batches size
            //AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            //arDetails.AutIntNo = autIntNo;
            //arDetails.ARCode = "5100";
            //arDetails.LastUser = this.login;

            //DefaultAuthRules arDefaultRule = new DefaultAuthRules(arDetails, this.connectionString);
            //KeyValuePair<int, string> batchSize = arDefaultRule.SetDefaultAuthRule();

            // Oscar 20111125 add transaction for push queue
            int addSumIntNo = 0;
            QueueItem item = new QueueItem();
            List<QueueItem> itemList = new List<QueueItem>();
            QueueItemProcessor queProcessor = new QueueItemProcessor();
            using (TransactionScope scope = new TransactionScope())
            {

                string sumLocation = "Admin";

                string printFileName;

                //2012-11-22 linda add for check new rule 5065
                AuthorityRulesDetails arDetails5065 = new AuthorityRulesDetails();
                arDetails5065.AutIntNo = this.autIntNo;
                arDetails5065.ARCode = "5065";
                arDetails5065.LastUser = this.login;
                DefaultAuthRules ar5065 = new DefaultAuthRules(arDetails5065, this.connectionString);
                bool checkResult = ar5065.SetDefaultAuthRule().Value.Equals("Y");

                string s35Permitted = CheckS35NoAOGPermitted();

                addSumIntNo = db.AddSummons(notIntNo, sumStatus, ticketNo, proxyFlag, courtDate, dtLastAOG, dtSumServedBy, this.login, asIntNo, sumLocation, crtIntNo, autIntNo, crtRIntNo, this.useOriginalFineAmount, ref errMessage, 0, chgIntNoList, chgRowVersionList, 1, DateTime.Now.ToString(DATE_AND_TIME_FORMAT), out printFileName, "N", 0, checkResult, false, s35Permitted);

                switch (addSumIntNo)
                {
                    case -1:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                        break;
                    case -2:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3") + ticketNo
                            + (string)GetLocalResourceObject("lblError.Text4");
                        break;
                    case -3:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5") + crtIntNo + (string)GetLocalResourceObject("lblError.Text39");
                        break;
                    case -4:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                        break;
                    case -5:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                        break;
                    case -6:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                        break;
                    case -7:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                        break;
                    case -8:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                        break;
                    case -9:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                        break;
                    case -10:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                        break;
                    case -11:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                        break;
                    case -12:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                        break;
                    case -13:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text15");
                        break;
                    case -14:
                    case -15:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text16");
                        break;
                    case -16:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text18");
                        break;
                    //Jerry 2013-12-02 add error handling
                    case -17:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text41");
                        break;
                    case -18:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text42");
                        break;
                    case -19:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text43");
                        break;
                    case -20:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text44");
                        break;
                    case -21:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text45");
                        break;
                    case -22:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text46");
                        break;
                    case -23:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text47");
                        break;
                    case -30:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text19") + errMessage;
                        break;
                    default:
                        break;
                }

                if (addSumIntNo < 1)
                {
                    lblError.Visible = true;
                    return;
                }

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.GenerateSingleSummons, PunchAction.Add);

                //Jerry 2012-11-20 add
                hidSumIntNo.Value = addSumIntNo.ToString();
                Notice notice = new NoticeService().GetByNotIntNo(notIntNo);

                // Oscar 20111116 add push queue
                item = new QueueItem();
                item.Body = notIntNo.ToString();
                item.Group = this.autCode.Trim();
                item.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
                itemList.Add(item);

                //jerry 2011-12-26 add
                item = new QueueItem();
                item.Body = addSumIntNo.ToString();
                item.Group = this.autCode.Trim();
                item.QueueType = ServiceQueueTypeList.ExpireUnservedSummons;
                //item.ActDate = DateTime.Now.AddDays(noDaysForSummonsExpiry + 1);
                item.ActDate = notice.NotOffenceDate.AddDays(noDaysForSummonsExpiry + 1);
                itemList.Add(item);

                //Jerry 2014-05-22 push StagnantSummons queue
                item = new QueueItem();
                item.Body = addSumIntNo.ToString();
                item.ActDate = courtDate.AddDays(1);
                item.LastUser = this.login;
                item.QueueType = ServiceQueueTypeList.StagnantSummons;
                itemList.Add(item);

                queProcessor.Send(itemList);

                scope.Complete();
            }

            string printFile = string.Empty;
            string summonsNumber = string.Empty;

            GetSummonsDetails(notIntNo, ref printFile, ref summonsNumber);

            // Cleanup
            //mrs 20081119 no longer used
            //this.ViewState.Remove("ACIntNo");
            this.ViewState.Remove("FineAmount");
            this.ViewState.Remove("CrtIntNo");

            this.ViewState.Add("PrintFile", printFile);
            this.CreateSummonsLink(summonsNumber);

            this.SetPanelVisibility(SingleSummonsState.GenerateSummons);

            PopupAjax.Show();
        }

        private string CheckS35NoAOGPermitted()
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "6600";
            rule.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            //return rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            return value.Value;
        }


        private DateTime CheckSummonsServedBy(DateTime courtDate)
        {
            //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
            //DateRulesDetails ruleCourtDate = new DateRulesDetails();
            //ruleCourtDate.AutIntNo = this.autIntNo;
            //ruleCourtDate.DtRDescr = "Amount in days that a summons must be served by from the court date";
            //ruleCourtDate.DtRStartDate = "CDate";
            //ruleCourtDate.DtRNoOfDays = -30;
            //ruleCourtDate.DtREndDate = "SumServeByDate";
            //ruleCourtDate.LastUser = PROCESS_NAME;
            //ruleCourtDate = dateRule.GetDefaultDateRule(ruleCourtDate);

            //AutIntNo, StartDate, EndDate and LastUser must be set up before the default rule is called

            DateRulesDetails rule = new DateRulesDetails();

            rule.AutIntNo = autIntNo;
            rule.LastUser = PROCESS_NAME;
            rule.DtRStartDate = "CDate";
            rule.DtREndDate = "SumServeByDate";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return courtDate.AddDays(noOfDays);
        }

        private DateTime CheckLastAOGDate(DateTime courtDate)
        {
            //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
            //DateRulesDetails ruleCourtDate = new DateRulesDetails();
            //ruleCourtDate.AutIntNo = this.autIntNo;
            //ruleCourtDate.DtRDescr = "Amount in days that an AOG fine can be paid from court date";
            //ruleCourtDate.DtRStartDate = "CDate";
            //ruleCourtDate.DtRNoOfDays = -7;
            //ruleCourtDate.DtREndDate = "LastAOGDate";
            //ruleCourtDate.LastUser = PROCESS_NAME;
            //ruleCourtDate = dateRule.GetDefaultDateRule(ruleCourtDate);

            //return courtDate.AddDays(ruleCourtDate.DtRNoOfDays);

            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.LastUser = PROCESS_NAME;
            rule.DtRStartDate = "CDate";
            rule.DtREndDate = "LastAOGDate";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return courtDate.AddDays(noOfDays);
        }

        private DateRulesDetails CheckDateRule()
        {
            //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
            //DateRulesDetails ruleCourtDate = new DateRulesDetails();
            //ruleCourtDate.AutIntNo = this.autIntNo;
            //ruleCourtDate.DtRDescr = "Min no. of days from SUMMONS ISSUE DATE to COURT DATE";
            //ruleCourtDate.DtREndDate = "CDate";
            //ruleCourtDate.DtRNoOfDays = 30;
            //ruleCourtDate.DtRStartDate = "NotIssueSummonsDate";
            //ruleCourtDate.LastUser = PROCESS_NAME;
            //ruleCourtDate = dateRule.GetDefaultDateRule(ruleCourtDate);

            //return ruleCourtDate;

            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.LastUser = PROCESS_NAME;
            rule.DtRStartDate = "NotIssueSummonsDate";
            rule.DtREndDate = "CDate";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return rule;
        }

        private void CreateSummonsLink(string summonsNumber)
        {
            this.ViewState.Add("printAutIntNo", this.autIntNo);

            //mrs 20081126 added for compatibility with multiple
            Session["PrintAutIntNo"] = this.autIntNo;

            this.lnkSummonsPrint.Text = string.Format((string)GetLocalResourceObject("lnkSummonsPrint.Text"), summonsNumber);

        }

        protected void LinkButton5_Click(object sender, EventArgs e)
        {
            this.SetPanelVisibility(SingleSummonsState.SummonsServer);
        }

        protected void cboCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            //mrs 20080811 court is now parent of court room, when court selected populate courtroom
            this.cboCourtRoom.Items.Clear();

            string[] split = this.cboCourt.SelectedValue.Split(new char[] { '~' });
            if (split.Length != 2)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text27");
                return;
            }

            int crtIntNo = Convert.ToInt32(split[0]);
            ViewState["CrtIntNo"] = crtIntNo.ToString();
            // Jake 2013-09-11 changed to populate isactive ="Y"
            CourtRoomDB db = new CourtRoomDB(this.connectionString);
            //Heidi 2013-10-29 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            //SqlDataReader reader1 = db.GetCourtRoomListByCrtRStatusFlag(crtIntNo);//db.GetActiveCourtRooms(crtIntNo);
            //Jerry 2013-11-27 reset it to original code, get active court rooms
            SqlDataReader reader1 = db.GetActiveCourtRooms(crtIntNo);

            this.cboCourtRoom.DataSource = reader1;
            this.cboCourtRoom.DataValueField = "CrtRIntNo";
            this.cboCourtRoom.DataTextField = "CrtRoomDetails";
            this.cboCourtRoom.DataBind();

            //Dictionary<int, string> lookups =
            //    CourtRoomLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            //while (reader1.Read())
            //{
            //    int crtRIntNo = (int)reader1["CrtRIntNo"];
            //    if (lookups.ContainsKey(crtRIntNo))
            //    {
            //        cboCourtRoom.Items.Add(new ListItem(lookups[crtRIntNo], crtRIntNo.ToString()));
            //    }
            //}
            //while (reader1.Read())
            //{
            //    ListItem item = new ListItem();
            //    item.Value = reader1["CrtRIntNo"].ToString();
            //    item.Text = reader1["CrtRoomDetails"].ToString();
            //    this.cboCourtRoom.Items.Add(item);
            //}
            reader1.Close();

            this.cboCourtRoom.Items.Insert(0, (string)GetLocalResourceObject("SELECT_COURT_ROOM"));

            this.SetPanelVisibility(SingleSummonsState.CourtDate);

        }

        #region Jerry 2012-05-22 change
        //protected void cboCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    //mrs 20080811 moved courtdates to child of courtroom
        //    this.hidCourtDate.Value = string.Empty;
        //    this.lblCourtDate.Text = NO_COURT_DATES;

        //    if (this.cboCourt.SelectedValue.Equals(SELECT_COURT))
        //    {
        //        this.lblError.Text = "You must select a court before you can proceed with creating the summons.";
        //        return;
        //    }

        //    if (this.cboCourtRoom.SelectedValue.Equals(SELECT_COURT_ROOM))
        //    {
        //        this.lblError.Text = "You must select a court room before you can proceed with creating the summons.";
        //        return;
        //    }

        //    int crtRIntNo = Convert.ToInt32(this.cboCourtRoom.SelectedValue.ToString());

        //    // Store the Court room int ID
        //    this.ViewState.Add("CrtRIntNo", crtRIntNo);

        //    int autIntNo = Convert.ToInt32(ViewState["AutIntNo"]);

        //    // Authority date rule for Summons
        //    DateRulesDetails ruleCourtDate = this.CheckDateRule();

        //    // Check for valid court dates
        //    CourtDatesDB db = new CourtDatesDB(this.connectionString);
        //    SqlDataReader reader = db.GetCourtDatesAvailableList(crtRIntNo, autIntNo, ViewState["notFilmType"].ToString());

        //    while (reader.Read())
        //    {
        //        DateTime dt = (DateTime)reader["CDate"];
        //        if (dt.Ticks < DateTime.Now.AddDays(ruleCourtDate.DtRNoOfDays).Ticks)
        //            continue;

        //        int noCases = Helper.GetReaderValue<int>(reader, "CDNoOfCases");
        //        if (noCases <= 0)
        //            continue;

        //        double dNoCases = (double)noCases - ((double)noCases * (this.dPercentage / 100.0));

        //        int noAllocated = Helper.GetReaderValue<int>(reader, "CDS54Allocated");
        //        int noAvailableCases = (int)dNoCases - noAllocated;

        //        if (noAvailableCases > 0)
        //        {
        //            this.lblCourtDate.Text = string.Format("{1} cases available on {0}", dt.ToString("yyyy-MM-dd"), noAvailableCases);

        //            this.hidCourtDate.Value = dt.ToString("yyyy-MM-dd");
        //            break;
        //        }
        //    }
        //    reader.Close();
        //}
        #endregion
        protected void cboCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            //mrs 20080811 moved courtdates to child of courtroom
            this.hidCourtDate.Value = string.Empty;
            this.lblCourtDate.Text = (string)GetLocalResourceObject("NO_COURT_DATES");

            if (this.cboCourt.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_COURT")))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text23");
                return;
            }

            if (this.cboCourtRoom.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_COURT_ROOM")))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text28");
                return;
            }

            int crtRIntNo = Convert.ToInt32(this.cboCourtRoom.SelectedValue.ToString());

            // Store the Court room int ID
            this.ViewState.Add("CrtRIntNo", crtRIntNo);

            int autIntNo = Convert.ToInt32(ViewState["AutIntNo"]);

            // Authority date rule for Summons
            DateRulesDetails ruleCourtDate = this.CheckDateRule();
            string ticketNo = (string)this.grdNotices.SelectedDataKey["TicketNo"];
            int notIntNo = (int)this.grdNotices.SelectedDataKey["NotIntNo"];
            int crtIntNo = Convert.ToInt32(ViewState["CrtIntNo"].ToString());
            bool isNoAOG = false;

            DataView noticeSource = new DataView(GetSingleSummonsNoticesSource(this.txtRegno.Text.Trim(), this.txtIDNumber.Text.Trim(), ticketNo).Tables[0]);
            noticeSource.RowFilter = string.Format("NotIntNo = {0}", notIntNo);
            foreach (DataRowView dr in noticeSource)
            {
                chgIntNoList.Add(dr["ChgIntNo"].ToString());
                chgRowVersionList.Add(dr["ChargeRowVersion"].ToString());
                this.noticeFilmType = nft.GetNoticeFilmType(dr["NotFilmType"].ToString());
                if (dr["NoAOG"].ToString() == "Y")
                {
                    isNoAOG = true;
                }
            }
            ViewState["chgIntNoList"] = chgIntNoList;
            ViewState["chgRowVersionList"] = chgRowVersionList;

            //jerry 2012-03-30 add court rule
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = Convert.ToInt32(crtIntNo);
            courtRulesDetails.CRCode = "3030";
            courtRulesDetails.LastUser = this.login;
            DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
            KeyValuePair<int, string> rule3030 = courtRule.SetDefaultCourtRule();

            courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = Convert.ToInt32(crtIntNo);
            courtRulesDetails.CRCode = "3040";
            courtRulesDetails.LastUser = this.login;
            courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
            KeyValuePair<int, string> rule3040 = courtRule.SetDefaultCourtRule();

            string needRestrictNoAOG = null;
            //Jerry 2012-05-21 add
            string needSeparateNoAOG = null;

            if (rule3040.Value == "Y")
            {
                if (isNoAOG)
                {
                    needSeparateNoAOG = "Y";
                }
            }
            else
            {
                if (isNoAOG && rule3030.Value == "Y")
                {
                    needRestrictNoAOG = "Y";
                }
            }

            // Update the court date
            TMSData db = new TMSData(this.connectionString);
            DateTime courtDate = db.GetSummonsCourtDate(autIntNo, crtIntNo, ruleCourtDate.DtRNoOfDays, this.dPercentage, PROCESS_NAME, ViewState["notFilmType"].ToString(), ref crtRIntNo, needRestrictNoAOG, needSeparateNoAOG);

            // Log the detailed problem
            switch (crtRIntNo)
            {
                case -1:
                    lblError.Text = "No court date was available for this summons.";
                    return;

                case -2:
                    lblError.Text = "No court date was available as either the authority or the court does not point to a valid courtRoom / courtDate row.";
                    return;
            }

            //if the getted court date is null, so get default date time is 1900-01-01
            if (courtDate.Year != 1900)
            {
                this.lblCourtDate.Text = string.Format("cases available on {0}", courtDate.ToString("yyyy-MM-dd"));
                this.hidCourtDate.Value = courtDate.ToString("yyyy-MM-dd");
            }
        }

        protected void lnkSummonsPrint_Click(object sender, EventArgs e)
        {
            //int chgIntNo = Convert.ToInt32(ViewState["ChgIntNo"].ToString());

            // Update the print status - only use for single summons NOT roadblock
            //mrs 20081122 bug here grdNotices not valid for roadblock
            //int chgIntNo = (int)this.grdNotices.SelectedDataKey["ChgIntNo"]; 
            //mrs 20081122 must store in viewstate as set in two different streams
            //int chgIntNo = Convert.ToInt32(ViewState["ChgIntNo"].ToString());
            //mrs 20081122 this is not actually the problem either - the summons print link is created on the page, they will click the link 
            //and there is no way to get any number out of it currently - add a grid with the link and the summons number, which can then be picked up here

            List<string> chgIntNoList = (List<string>)ViewState["chgIntNoList"];
            //List<string> chgRowVersionList = (List<string>)ViewState["chgRowVersionList"];

            ChargeDB db = new ChargeDB(this.connectionString);

            for (int i = 0; i < chgIntNoList.Count; i++)
            {
                int chgIntNo = Convert.ToInt32(chgIntNoList[i]);
                db.UpdateStatus(chgIntNo, STATUS_SUMMONS_PRINTED, this.login);
            }

            // Open the viewer page
            string printFile = (string)this.ViewState["PrintFile"];
            //dls 070906 - no longer necessary to pass thru '~' for a single summons, otherwise print dates don't get set correctly
            //QSParams param = new QSParams("printfile", "~" + printFile);
            QSParams param = new QSParams("printfile", printFile);
            PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");
            pageToOpen.Parameters.Add(param);
            Helper_Web.BuildPopup(pageToOpen);
        }

        protected void btnPersonalDetails_Click(object sender, EventArgs e)
        {
            // Check the Auth rule for fore names
            bool mustHaveForenames = this.CheckForenamesRule();
            bool mustHaveIDNumber = this.CheckIDNumberRule();

            // Validate & Update personal details
            bool isValid = true;
            StringBuilder sb = new StringBuilder();
            sb.Append((string)GetLocalResourceObject("lblError.Text30") + "\n<ul>\n");
            string foreNames = this.txtForenames.Text.Trim();
            if (mustHaveForenames && foreNames.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text31") + "</li>\n");
            }
            string surname = this.txtSurname.Text.Trim();
            if (surname.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text32") + "</li>\n");
            }
            string address1 = this.txtAddress1.Text.Trim();
            if (address1.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text33") + "</li>\n");
            }
            string address2 = this.txtAddress2.Text.Trim();
            if (address2.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text34") + "</li>\n");
            }
            string address3 = this.txtAddress3.Text.Trim();
            string address4 = this.txtAddress4.Text.Trim();
            string areaCode = this.txtAreaCode.Text.Trim();
            if (areaCode.Length == 0)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text35") + "</li>\n");
            }
            string idNumber = this.txtOffenderIDNumber.Text.Trim();
            if (mustHaveIDNumber)
            {
                if (idNumber.Length == 0)
                {
                    isValid = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text36") + "</li>\n");
                }
                else if (!this.regex.IsMatch(idNumber))
                {
                    isValid = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text37") + "</li>\n");
                }
            }

            if (!isValid)
            {
                sb.Append("</ul>");
                this.lblError.Text = sb.ToString();
                return;
            }

            //Jerry 2014-05-30 add try catch
            try
            {
                //Jerry 2014-05-30 add transaction
                using (TransactionScope scope = SIL.ServiceBase.ServiceUtility.CreateTransactionScope())
                {
                    OffenderDetails details = (OffenderDetails)this.ViewState["OffenderDetails"];

                    //Jerry 2014-08-20 don't update if there none changed 
                    if (details.Address1.Trim().ToLower() != address1.ToLower() ||
                        details.Address2.Trim().ToLower() != address2.ToLower() ||
                        details.Address3.Trim().ToLower() != address3.ToLower() ||
                        details.Address4.Trim().ToLower() != address4.ToLower() ||
                        details.AreaCode.Trim().ToLower() != areaCode.ToLower() ||
                        details.ForeNames.Trim().ToLower() != foreNames.ToLower() ||
                        details.Surname.Trim().ToLower() != surname.ToLower() ||
                        details.IDNumber.Trim().ToLower() != idNumber.ToLower())
                    {
                        NoticeComment noticeCommentEntity = new NoticeComment();
                        noticeCommentEntity.NotIntNo = Convert.ToInt32(ViewState["NotIntNo"]);

                        if (details.Surname.Trim().ToLower() != surname.ToLower() && (
                            details.Address1.Trim().ToLower() != address1.ToLower() ||
                            details.Address2.Trim().ToLower() != address2.ToLower() ||
                            details.Address3.Trim().ToLower() != address3.ToLower() ||
                            details.Address4.Trim().ToLower() != address4.ToLower() ||
                            details.AreaCode.Trim().ToLower() != areaCode.ToLower()))
                        {
                            noticeCommentEntity.NcComment = string.Format((string)GetLocalResourceObject("NoticeCommentMsg1"), Environment.NewLine, details.FullName.Trim(), Environment.NewLine, details.Address1.Trim(), details.Address2.Trim(), details.Address3.Trim(), details.Address4.Trim(), details.AreaCode);
                            noticeCommentEntity.NcCreateDate = DateTime.Now;
                            noticeCommentEntity.NcUpdateDate = DateTime.Now;
                            noticeCommentEntity.LastUser = this.login;
                            new NoticeCommentService().Save(noticeCommentEntity);
                        }
                        else if (details.Surname.Trim().ToLower() != surname.ToLower())
                        {
                            noticeCommentEntity.NcComment = string.Format((string)GetLocalResourceObject("NoticeCommentMsg2"), Environment.NewLine, details.FullName.Trim());
                            noticeCommentEntity.NcCreateDate = DateTime.Now;
                            noticeCommentEntity.NcUpdateDate = DateTime.Now;
                            noticeCommentEntity.LastUser = this.login;
                            new NoticeCommentService().Save(noticeCommentEntity);
                        }
                        else if (details.Address1.Trim().ToLower() != address1.ToLower() ||
                            details.Address2.Trim().ToLower() != address2.ToLower() ||
                            details.Address3.Trim().ToLower() != address3.ToLower() ||
                            details.Address4.Trim().ToLower() != address4.ToLower() ||
                            details.AreaCode.Trim().ToLower() != areaCode.ToLower())
                        {
                            noticeCommentEntity.NcComment = string.Format((string)GetLocalResourceObject("NoticeCommentMsg3"), Environment.NewLine, details.Address1.Trim(), details.Address2.Trim(), details.Address3.Trim(), details.Address4.Trim(), details.AreaCode);
                            noticeCommentEntity.NcCreateDate = DateTime.Now;
                            noticeCommentEntity.NcUpdateDate = DateTime.Now;
                            noticeCommentEntity.LastUser = this.login;
                            new NoticeCommentService().Save(noticeCommentEntity);
                        }

                        details.Address1 = address1;
                        details.Address2 = address2;
                        details.Address3 = address3;
                        details.Address4 = address4;
                        details.AreaCode = areaCode;
                        details.ForeNames = foreNames;
                        details.Surname = surname;
                        details.IDNumber = idNumber;
                        details.LastUser = this.login;

                        string errMessage = string.Empty;
                        OffenderDB db = new OffenderDB(this.connectionString);
                        //Jerry 2015-02-27 change it
                        //bool success = db.UpdateOffenderDetails(details, ref errMessage);
                        //if (!success)
                        //{
                        //    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text38") + (errMessage.Length > 0 ? " - " + errMessage : string.Empty);
                        //    return;
                        //}

                        int returnValue = db.UpdateOffenderDetailsForSingleSummons(details, Convert.ToInt32(ViewState["NotIntNo"]), ref errMessage);
                        if (returnValue == 1)
                        {
                            scope.Complete();
                        }
                        else if (returnValue == 0)
                        {
                            if (string.IsNullOrEmpty(errMessage))
                                errMessage = string.Format((string)GetLocalResourceObject("lblError.Text48"), Convert.ToInt32(ViewState["NotIntNo"]));
                            else
                                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteLog("Exception", SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString(), errMessage);

                            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text38") + "-" + errMessage;
                            return;
                        }
                        else if (returnValue == -1)
                        {
                            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text38") + "-" + (string)GetLocalResourceObject("lblError.Text49");
                            return;
                        }
                        else if (returnValue == -2)
                        {
                            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text38") + "-" + (string)GetLocalResourceObject("lblError.Text50");
                            return;
                        }
                        else if (returnValue == -3)
                        {
                            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text38") + "-" + (string)GetLocalResourceObject("lblError.Text51");
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
                return;
            }

            //process a NOAOG with an on-the-fly rep on
            if (ViewState["noaogDecision"] == null)
            {
                decimal revisedAmount = (decimal)this.ViewState["RevisedAmount"];

                // If the offence is a noaog ask the user what to do about it
                // we shold only allow them to do a rep if the revised amount is still 0, otherwise, it means a rep has already been done, so don't want to do it again!
                if (ViewState["noaog"].ToString().Equals("Y") && (revisedAmount == 0 || revisedAmount >= 999999))
                {
                    //is a noaog
                    this.SetPanelVisibility(SingleSummonsState.NOAOGDecision);
                    return;
                }
            }
            else if (ViewState["noaogDecision"].ToString().Equals("Y"))
            {
                //decimal revisedAmount = (decimal)this.ViewState["RevisedAmount"];
                //if (revisedAmount == 0 || revisedAmount >= 999999)
                //{
                //    //check for whether user wants to rep the noaog or pass through as a zero
                //    //add another state "NOAOG"
                //    string ticketNo = (string)this.grdNotices.SelectedDataKey["TicketNo"];
                //    decimal originalAmount = (decimal)this.grdNotices.SelectedDataKey["OriginalAmount"];

                //    this.RepresentationOnTheFly1.TicketNumber = ticketNo;
                //    this.RepresentationOnTheFly1.AutIntNo = this.autIntNo;
                //    this.RepresentationOnTheFly1.OriginalAmount = originalAmount;
                //    this.RepresentationOnTheFly1.NewAmount = 0;
                //    this.RepresentationOnTheFly1.BindData();
                //    this.SetPanelVisibility(SingleSummonsState.OnTheFlyRepresentation);
                //    return;
                //}
            }
            //else 
            //process a NOAOG with no fine amount
            //if (ViewState["noaogDecision"] == "N")
            //{
            //decimal noaogAmount = 0;
            //if (revisedAmount == 0 || revisedAmount >= 999999)
            //{
            //check for whether user wants to rep the noaog or pass through as a zero
            //add another state "NOAOG"
            //string ticketNo = (string)this.grdNotices.SelectedDataKey["TicketNo"];
            //decimal originalAmount = (decimal)this.grdNotices.SelectedDataKey["OriginalAmount"];

            //this.RepresentationOnTheFly1.TicketNumber = ticketNo;
            //this.RepresentationOnTheFly1.AutIntNo = this.autIntNo;
            ////this.RepresentationOnTheFly1.OriginalAmount = originalAmount;
            //this.RepresentationOnTheFly1.NewAmount = 0;
            //this.RepresentationOnTheFly1.BindData();
            //    this.SetPanelVisibility(SingleSummonsState.GenerateSummons); //is this the right next step
            //    return;
            //    //}
            //}

            // Select a Summons Server to issue the summons

            //if the areas code was changed during the process, the incorrect area code appears in the grid
            //areaCode = (string)this.grdNotices.SelectedDataKey["AreaCode"];
            areaCode = txtAreaCode.Text;
            this.ShowSummonsServer(areaCode);

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.GenerateSingleSummons, PunchAction.Change);
        }

        private bool CheckIDNumberRule()
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "5060";
            //rule.ARComment = "Y = true (Default); N = false;";
            //rule.ARDescr = "Rule as to whether ID Numbers are required for summons generation.";
            //rule.ARNumeric = 1;
            //rule.ARString = "Y";
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "5060";
            rule.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            //return rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            return value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
        }

        private bool CheckForenamesRule()
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4900";
            //rule.ARComment = "Y = true (Default); N = false;";
            //rule.ARDescr = "Rule as to whether Fore names are required for summons generation.";
            //rule.ARNumeric = 1;
            //rule.ARString = "Y";
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "4900";
            rule.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            //return rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            return value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
        }

        protected void NOAOGDecisionYes_Click(object sender, EventArgs e)
        {
            //the user chose to enter a rep on the NOAOG
            this.ViewState.Add("noaogDecision", "Y");
            decimal revisedAmount = (decimal)this.ViewState["RevisedAmount"];
            if (revisedAmount == 0 || revisedAmount >= 999999)
            {
                string ticketNo = (string)this.grdNotices.SelectedDataKey["TicketNo"];
                decimal originalAmount = (decimal)this.grdNotices.SelectedDataKey["OriginalAmount"];
                this.RepresentationOnTheFly1.TicketNumber = ticketNo;
                this.RepresentationOnTheFly1.AutIntNo = this.autIntNo;
                this.RepresentationOnTheFly1.OriginalAmount = originalAmount;
                this.RepresentationOnTheFly1.NewAmount = 0;
                this.RepresentationOnTheFly1.BindData();
                this.SetPanelVisibility(SingleSummonsState.OnTheFlyRepresentation);
                return;
            }
            this.SetPanelVisibility(SingleSummonsState.OnTheFlyRepresentation);

        }
        protected void NOAOGDecisionNo_Click(object sender, EventArgs e)
        {
            //the user chose not to enter a rep on the NOAOG
            this.ViewState.Add("noaogDecision", "N");
            //this.SetPanelVisibility(SingleSummonsState.GenerateSummons); //is this the right next step
            // Select a Summons Server to issue the summons
            string areaCode = (string)this.grdNotices.SelectedDataKey["AreaCode"];
            this.ShowSummonsServer(areaCode);

        }

        protected void btnPostalCodesA_Click(object sender, EventArgs e)
        {
            this.PopulateAreaCodes(this.txtAddress4.Text, "A");
        }

        private void PopulateAreaCodes(string sTown, string sType)
        {
            SuburbDB acdb = new SuburbDB(connectionString); //changed from areadb to suburbdb
            DataSet ds = acdb.GetFilteredSuburbCodes("0", sTown);
            if (sType == "A")
            {
                Dictionary<string, string> lookups =
                    SuburbLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string subIntNo = Convert.ToString(ds.Tables[0].Rows[i]["SubIntNo"].ToString());
                    string areaCode = Convert.ToString(ds.Tables[0].Rows[i]["AreaCode"].ToString());
                    //2013-05-24 Heidi changed
                    if (lookups.ContainsKey(subIntNo))
                    {
                        ddlPostalCodesA.Items.Add(new ListItem(lookups[subIntNo], areaCode.ToString()));
                    }
                }
                //ddlPostalCodesA.DataSource = ds;
                //ddlPostalCodesA.DataValueField = "AreaCode";
                //ddlPostalCodesA.DataTextField = "Descr";
                //ddlPostalCodesA.DataBind();
                if (ds.Tables[0].Rows.Count == 1)
                {
                    if (ddlPostalCodesA.Items.Count > 0)
                    {
                        this.txtAreaCode.Text = ddlPostalCodesA.Items[0].Value;
                    }
                }
            }
            //else
            //{
            //    ddlPostalCodesB.DataSource = ds;
            //    ddlPostalCodesB.DataValueField = "AreaCode";
            //    ddlPostalCodesB.DataTextField = "Descr";
            //    ddlPostalCodesB.DataBind();
            //    if (ds.Tables[0].Rows.Count == 1)
            //        this.txtStreetArea.Text = ddlPostalCodesB.Items[0].Value;
            //}
            ds.Dispose();
        }

        protected void ddlPostalCodesA_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtAreaCode.Text = ddlPostalCodesA.SelectedValue;
        }

        protected void grdNotices_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;

            DataRowView row = (DataRowView)e.Row.DataItem;
            if (row == null)
                return;

            int chgSequence = (int)row["chgSequence"];

            if (chgSequence == 1)
            {
                e.Row.Cells[10].Enabled = true;
            }
            else
            {
                e.Row.Cells[10].Enabled = false;
            }

            // 2013-09-16, Oscar added, no need for summons
            var noticeStatus = Convert.ToInt32(row["NoticeStatus"]);
            e.Row.Cells[11].Enabled = noticeStatus < 610;
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            lblStationeryMessage.Visible = true;
            lblMsgForBatchPrint.Visible = false;
            PopupAjax.Hide();
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            QueueItem item = new QueueItem();
            QueueItemProcessor queProcessor = new QueueItemProcessor();

            string printFileName = (string)this.ViewState["PrintFile"];
            int sumIntNo = Convert.ToInt32(hidSumIntNo.Value.Trim());

            // Oscar 20120308 add PrintFileName_Summons Conjoin and push print summons Q
            PrintFileNameService pfnSvc = new PrintFileNameService();
            PrintFileNameSummonsService pfnsSvc = new PrintFileNameSummonsService();
            PrintFileName pfnEntity = pfnSvc.GetByPrintFileName(printFileName);
            if (pfnEntity == null)
            {
                pfnEntity = new PrintFileName()
                {
                    PrintFileName = printFileName,
                    AutIntNo = autIntNo,
                    LastUser = this.login,
                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                };
                pfnEntity = pfnSvc.Save(pfnEntity);
            }

            PrintFileNameSummons pfnsEntity = pfnsSvc.GetBySumIntNoPfnIntNo(sumIntNo, pfnEntity.PfnIntNo);
            if (pfnsEntity == null)
            {
                pfnsEntity = new PrintFileNameSummons()
                {
                    SumIntNo = sumIntNo,
                    PfnIntNo = pfnEntity.PfnIntNo,
                    LastUser = this.login,
                    EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                };
                pfnsEntity = pfnsSvc.Save(pfnsEntity);
            }

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)
            bool isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

            //Jake 2014-07-25 added new data rule to delay summons printing date
            //get another auth rule for no. of days after court date to generate WOA
            DateRulesDetails drSummosn = new DateRulesDetails();
            drSummosn.AutIntNo = autIntNo;
            drSummosn.DtRStartDate = "SumPrintDate";
            drSummosn.DtREndDate = "SumCourtDate";
            drSummosn.LastUser = this.login;

            DefaultDateRules dfDateRule = new DefaultDateRules(drSummosn, connectionString);
            int noOfDays = dfDateRule.SetDefaultDateRule();

            Summons summons = new SummonsService().GetBySumIntNo(sumIntNo);

            if (!isIBMPrinter)
            {
                item = new QueueItem()
                {
                    Body = pfnEntity.PfnIntNo,
                    Group = this.autCode.Trim(),
                    ActDate = summons.SumCourtDate.Value.AddDays(-1 * noOfDays),
                    QueueType = ServiceQueueTypeList.PrintSummons
                };
                queProcessor.Send(item);
            }
            else
            {//2012-12-12 added by Nancy (Push queue for PrintToFile Service)
                item = new QueueItem();
                item.Body = pfnEntity.PfnIntNo;
                item.ActDate = summons.SumCourtDate.Value.AddDays(-1 * noOfDays);
                item.Group = this.autCode.Trim() + "|" + ((printFileName.IndexOf("CPA6") >= 0) ? (int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.CPA6DirectPrinting : (int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.CPA5DirectPrinting);
                item.QueueType = ServiceQueueTypeList.PrintToFile;

                queProcessor.Send(item);
            }
            lnkSummonsPrint.Enabled = false;
            lblStationeryMessage.Visible = false;
            lblMsgForBatchPrint.Visible = true;

            PopupAjax.Hide();
        }
    }
}
