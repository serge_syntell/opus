<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.NoticePost" Codebehind="NoticePost.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="udpPost" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                            
                                <asp:GridView ID="grdPosted" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                    CssClass="Normal" OnSelectedIndexChanged="grdPosted_SelectedIndexChanged" ShowFooter="True"
                                    AllowPaging="True" OnPageIndexChanging="grdPosted_PageIndexChanging" PageSize="20"
                                    OnRowCreated="grdPosted_RowCreated">
                                    <FooterStyle CssClass="CartListHead" />
                                    <Columns>
                                        <asp:BoundField DataField="IsFirst" HeaderText="<%$Resources:grdPosted.HeaderText %>" />
                                        <asp:BoundField DataField="PrintFileName" HeaderText="<%$Resources:grdPosted.HeaderText1 %>" />
                                        <asp:BoundField DataField="NumberOfNotices" HeaderText="<%$Resources:grdPosted.HeaderText2 %>">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="<%$Resources:grdPosted.HeaderText3 %>">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("PrintDate", "{0:yyyy-MM-dd}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$Resources:grdPosted.HeaderText4 %>">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("FirstPostedDate", "{0:yyyy-MM-dd}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:CommandField SelectText="<%$Resources:grdPostedItem.SelectText %>" ShowSelectButton="True" />
                                    </Columns>
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                            </asp:Panel>
                            <asp:Panel ID="pnlUpdate" Visible="false" runat="server" Width="100%">
                                <table>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="text-align: center;">
                                                <asp:Label ID="lblHeader" runat="server" Width="100%" CssClass="ProductListHead" Text="<%$Resources:lblHeader.Text %>"></asp:Label></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px">
                                            <asp:Label ID="lblDate" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDate.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 26px">
                                              <asp:TextBox runat="server" ID="dtpActualDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False"                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpActualDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="dtpActualDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqDate.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator> </td>
                                        <td style="width: 3px; height: 26px;" align="center"></td>
                                        <td rowspan="5" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px">
                                            &nbsp;
                                        </td>
                                        <td style="height: 26px">
                                            <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" OnClick="btnUpdate_Click"
                                                Text="<%$Resources:btnUpdate.Text %>" Width="90px" /></td>
                                        <td style="width: 3px; height: 26px;" align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
					<asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label3"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                   
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
