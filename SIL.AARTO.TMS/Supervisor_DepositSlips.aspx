<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.Supervisor_DepositSlips" Codebehind="Supervisor_DepositSlips.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    <asp:Button ID="btnBalancingReport" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnBalancingReport.Text %>" OnClick="btnBalancingReport_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    <asp:Button ID="btnDespositSlip" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnDespositSlip.Text %>" OnClick="btnDepositSlip_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;</p>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                        <asp:GridView ID="grdCashBoxes" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                            ShowFooter="True" OnSelectedIndexChanged="grdCashBoxes_SelectedIndexChanged"
                            OnRowCreated="grdCashBoxes_RowCreated" CellPadding="3">
                            <FooterStyle CssClass="CartListHead" />
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListAlt" />
                            <Columns>
                                <asp:BoundField DataField="CBIntNo" HeaderText="CBIntNo" Visible="False" DataFormatString="{0}" />
                                <asp:BoundField DataField="CBName" HeaderText="<%$Resources:grdCashBoxes.HeaderText %>" DataFormatString="{0}" />
                                <asp:BoundField DataField="UserName" HeaderText="<%$Resources:grdCashBoxes.HeaderText1 %>" DataFormatString="{0}" />
                                <asp:TemplateField HeaderText="<%$Resources:grdCashBoxes.HeaderText2 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CashAmount") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCash" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdCashBoxes.HeaderText3 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ReconChequeAmount") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCheques" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdCashBoxes.HeaderText4 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ReconCardAmount") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCards" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdCashBoxes.HeaderText5 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("ReconDebitCardAmount") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDebitCards" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdCashBoxes.HeaderText6 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("ReconPOAmount") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPostalOrders" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdCashBoxes.HeaderText7 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("CBStartAmount") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFloat" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField HeaderText="<%$Resources:grdCashBoxes.HeaderText8 %>" SelectText="<%$Resources:grdCashBoxes.HeaderText8 %>" ShowSelectButton="True">
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:CommandField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                    <asp:Panel ID="pnlConfirm" runat="server" Width="100%">
                        <br />
                        <p class="NormalBold">
                            <asp:Label ID="lblCashbox" runat="server" Text="<%$Resources:lblCashbox.Text %>"></asp:Label>
                        </p>
                        <table border="0" class="Normal">
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblFloat.Text %>"></asp:Label> </td>
                                <td>
                                    <asp:Label ID="lblFloat" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblCash.Text %>"></asp:Label> </td>
                                <td>
                                    <asp:Label ID="lblCash" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblCheques.Text %>"></asp:Label> </td>
                                <td>
                                    <asp:Label ID="lblCheques" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblCreditCards.Text %>"></asp:Label> </td>
                                <td>
                                    <asp:Label ID="lblCards" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblDebitCards.Text %>"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblDebitCards" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblPostalOrders.Text %>"></asp:Label> </td>
                                <td>
                                    <asp:Label ID="lblPostalOrders" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right">
                                    <asp:Button ID="btnConfirm" runat="server" CssClass="NormalButton" OnClick="btnConfirm_Click"
                                        Text="<%$Resources:btnConfirm.Text %>" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlDepositSlips" runat="server">
                        <asp:GridView ID="grdDepositSlips" runat="server" CssClass="Normal" ShowFooter="True"
                            AutoGenerateColumns="False" OnRowEditing="grdDepositSlips_RowEditing" 
                            CellPadding="3" >
                            <FooterStyle CssClass="CartListHead" />
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListAlt" />
                            <Columns>
                                <asp:BoundField DataField="DSIntNo" HeaderText="<%$Resources:grdDepositSlips.HeaderText %>" Visible="False" />
                                <asp:BoundField DataField="DSDate" DataFormatString="{0:dd-MM-yyyy}" HeaderText="<%$Resources:grdDepositSlips.HeaderText1 %>" />
                                <asp:BoundField DataField="DSCashType" HeaderText="<%$Resources:grdDepositSlips.HeaderText2 %>" />
                                <asp:BoundField DataField="DSNoOfCheques" HeaderText="<%$Resources:grdDepositSlips.HeaderText3 %>" />
                                <asp:BoundField DataField="DSChequeAmount" HeaderText="<%$Resources:grdDepositSlips.HeaderText4 %>" />
                                <asp:BoundField DataField="DSCashAmount" HeaderText="<%$Resources:grdDepositSlips.HeaderText5 %>" />
                                <asp:BoundField DataField="DSAmount" HeaderText="<%$Resources:grdDepositSlips.HeaderText6 %>" />
                                <%--<asp:TemplateField HeaderText="Contents">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCashType" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:CommandField EditText="Print" HeaderText="<%$Resources:grdDepositSlips.HeaderText7 %>" SelectText="" ShowEditButton="True">
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:CommandField>
                            </Columns>
                        </asp:GridView>
                        <p>
                            <asp:Button ID="btnLock" runat="server" Text="<%$Resources:btnLock.Text %>" OnClick="btnLock_Click"
                                CssClass="NormalButton" /></p>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
