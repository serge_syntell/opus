using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Util;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    public partial class ReceiptReport : Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int userIntNo = 0;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "ReceiptReport.aspx";
        //protected string thisPage = "Consolidated Receipt Report";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            this.userIntNo = Convert.ToInt32(Session["userIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                //2012-3-6 linda modified into multi-language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateAuthorities();
                this.PopulateCashiers();
                this.PopulateMonth();
                txtYear.Text = DateTime.Today.Year.ToString();
                chkDateRange.Checked = false;
                wdcDateTo.Enabled = false;
                wdcDateFrom.Enabled = false;
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();

            this.ddlAuthority.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlAuthorityItems.Text"), "0"));
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));
            //reader.Close();
        }

        public void PopulateCashiers()
        {
            Cashbox_UserDB cashboxUser = new Cashbox_UserDB(this.connectionString);

            SqlDataReader reader = cashboxUser.GetCashierList();

            this.ddlCashier.DataSource = reader;
            this.ddlCashier.DataValueField = "UserIntNo";
            this.ddlCashier.DataTextField = "Cashier";
            this.ddlCashier.DataBind();
            this.ddlCashier.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlMonth.Items"), "0"));

            reader.Dispose();
        }

        private void PopulateMonth()
        {
            this.ddlMonth.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlMonth.Items"), "13"));
            this.ddlMonth.Items.Insert(1, new ListItem((string)GetLocalResourceObject("ddlMonth.Items1"), "01"));
            this.ddlMonth.Items.Insert(2, new ListItem((string)GetLocalResourceObject("ddlMonth.Items2"), "02"));
            this.ddlMonth.Items.Insert(3, new ListItem((string)GetLocalResourceObject("ddlMonth.Items3"), "03"));
            this.ddlMonth.Items.Insert(4, new ListItem((string)GetLocalResourceObject("ddlMonth.Items4"), "04"));
            this.ddlMonth.Items.Insert(5, new ListItem((string)GetLocalResourceObject("ddlMonth.Items5"), "05"));
            this.ddlMonth.Items.Insert(6, new ListItem((string)GetLocalResourceObject("ddlMonth.Items6"), "06"));
            this.ddlMonth.Items.Insert(7, new ListItem((string)GetLocalResourceObject("ddlMonth.Items7"), "07"));
            this.ddlMonth.Items.Insert(8, new ListItem((string)GetLocalResourceObject("ddlMonth.Items8"), "08"));
            this.ddlMonth.Items.Insert(9, new ListItem((string)GetLocalResourceObject("ddlMonth.Items9"), "09"));
            this.ddlMonth.Items.Insert(10, new ListItem((string)GetLocalResourceObject("ddlMonth.Items10"), "10"));
            this.ddlMonth.Items.Insert(11, new ListItem((string)GetLocalResourceObject("ddlMonth.Items11"), "11"));
            this.ddlMonth.Items.Insert(12, new ListItem((string)GetLocalResourceObject("ddlMonth.Items12"), "12"));
            this.ddlMonth.SelectedIndex = 0;
            return;
        }

         

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (chkDateRange.Checked)
            {
                DateTime dt;
                if (!DateTime.TryParse(wdcDateFrom.Text, out dt))
                {
                    //2012-3-6 linda modified into multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    return;
                }
                if (!DateTime.TryParse(wdcDateTo.Text, out dt))
                {
                    //2012-3-6 linda modified into multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    return;
                }

                if (Convert.ToDateTime(wdcDateFrom.Text) > Convert.ToDateTime(wdcDateTo.Text))
                {
                    //2012-3-6 linda modified into multi-language
                    this.lblError.Text =(string)GetLocalResourceObject("lblError.Text1");
                    return;
                }
            }
            else
            {
                if (this.ddlAuthority.SelectedValue.Equals(string.Empty))
                {
                    //2012-3-6 linda modified into multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    return;
                }
                if ((this.ddlMonth.SelectedIndex > 0) && this.txtYear.Text == string.Empty)
                {
                    //2012-3-6 linda modified into multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    return;
                }
            }
            this.lblError.Text = string.Empty;

            this.autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            this.userIntNo = int.Parse(this.ddlCashier.SelectedValue);

            //ReportingUtil reportUtil = new ReportingUtil(this.connectionString, this.login);
            //string hostName = reportUtil.GetReportUrl(Request.Url, "ReceiptReport");
             
            //PageToOpen pto = new PageToOpen(this.Page, hostName);

            //Modefied By Edge 2012-05-14
            PageToOpen pto = new PageToOpen(this.Page, "ReceiptReportViewer.aspx");
            pto.AddParameter("AutIntNo", autIntNo);
            pto.AddParameter("UserIntNo", userIntNo);
            pto.AddParameter("Authority", this.ddlAuthority.SelectedItem.Text);

            if (chkDateRange.Checked)
            {
                pto.AddParameter("From", wdcDateFrom.Text);
                pto.AddParameter("To", wdcDateTo.Text);
                Helper_Web.BuildPopup(pto);
            }
            else
            {
                pto.AddParameter("Year", this.txtYear.Text);
                pto.AddParameter("Month", this.ddlMonth.SelectedItem.Value);
                Helper_Web.BuildPopup(pto);
            }
            //PunchStats805806 enquiry ReceiptReport
        }

        protected void chkDateRange_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkDateRange.Checked)
            {
                wdcDateFrom.Enabled = true;
                wdcDateTo.Enabled = true;
                txtYear.Enabled = false;
                ddlMonth.Enabled = false;

                this.wdcDateFrom.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
                this.wdcDateTo.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
            }
            else
            {
                wdcDateFrom.Enabled = false;
                wdcDateTo.Enabled = false;

                wdcDateFrom.Text = string.Empty;
                wdcDateTo.Text = string.Empty;

                txtYear.Enabled = true;
                ddlMonth.Enabled = true;
            }
        }

    }
}


