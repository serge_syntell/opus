using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class Region : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "Region.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "Region maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];

			loginUser = userDetails.UserLoginName;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text"); ;
				pnlAddRegion.Visible = false;
				pnlUpdateRegion.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;

				//PopulateRegion(ddlRegion);

				BindGrid();
			}		
		}

        //protected void PopulateRegion(DropDownList ddlRegion)
        //{
        //    RegionDB region = new RegionDB(connectionString);

        //    SqlDataReader reader = region.GetRegionList("RegName");
        //    ddlSelRegion.DataSource = reader;
        //    ddlSelRegion.DataValueField = "RegIntNo";
        //    ddlSelRegion.DataTextField = "RegDescr";
        //    ddlSelRegion.DataBind();

        //    reader.Close();
        //}

		protected void BindGrid()
		{
			//int regIntNo = 0;

			Stalberg.TMS.RegionDB regionList = new Stalberg.TMS.RegionDB(connectionString);

			DataSet data = regionList.GetRegionListDS("RegName",Thread.CurrentThread.CurrentCulture.ToString());
			dgRegion.DataSource = data;
			dgRegion.DataKeyField = "RegIntNo";
            
            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgRegion.DataBind();
            }
            catch
            {
                dgRegion.CurrentPageIndex = 0;
                dgRegion.DataBind();
            }

			if (dgRegion.Items.Count == 0)
			{
				dgRegion.Visible = false;
				lblError.Visible = true;
                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
				dgRegion.Visible = true;
			}

			data.Dispose();
			pnlAddRegion.Visible = false;
			pnlUpdateRegion.Visible = false;

		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddRegion.Visible = true;
			pnlUpdateRegion.Visible = false;

			btnOptDelete.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);

			//PopulateRegion(ddlAddRegion);
			//ddlAddRegion.Items.Insert(0,"Select a Region");
		}

		protected void btnAddRegion_Click(object sender, System.EventArgs e)
		{	
			//if (ddlAddRegion.SelectedIndex < 1)
			//{
			//	lblError.Visible = true;
			//	lblError.Text = "Please select a valid Region";
			//	return;
			//}

            //int regIntNo = Convert.ToInt32(ddlAddRegion.SelectedValue);
            //int regIntNo = Convert.ToInt32(Session["editRegIntNo"]);
            //int regIntNo = Convert.ToInt32(ddlAddRegion.SelectedValue);

			// add the new transaction to the database table
			Stalberg.TMS.RegionDB toAdd = new RegionDB(connectionString);

            int addRegIntNo = toAdd.AddRegion(txtAddRegionCode.Text, txtAddRegionName.Text, txtAddRegionTel.Text, loginUser);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addRegIntNo, lgEntityList, "RegionLookup", "RegIntNo", "RegName", this.loginUser);


			if (addRegIntNo <= 0)
			{
                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
			else
			{
                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                Session["editRegIntNo"] = addRegIntNo;
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RegionMaintenance, PunchAction.Add);  

            }

			lblError.Visible = true;

			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
            //int regIntNo = Convert.ToInt32(dgRegion.SelectedValue);
			int regIntNo = Convert.ToInt32(Session["editRegIntNo"]);

			// add the new transaction to the database table
			Stalberg.TMS.RegionDB regUpdate = new RegionDB(connectionString);

            int updRegIntNo = regUpdate.UpdateRegion(txtRegionCode.Text, txtRegionName.Text, txtRegionTel.Text, loginUser, regIntNo);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(updRegIntNo, lgEntityList, "RegionLookup", "RegIntNo", "RegName", this.loginUser);
           

			if (updRegIntNo <= 0)
			{
                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
			}
			else
			{
                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RegionMaintenance, PunchAction.Change);
			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void dgRegion_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgRegion.SelectedIndex = e.Item.ItemIndex;

			if (dgRegion.SelectedIndex > -1) 
			{			
				int editRegIntNo = Convert.ToInt32(dgRegion.DataKeys[dgRegion.SelectedIndex]);

				Session["editRegIntNo"] = editRegIntNo;
				Session["prevPage"] = thisPageURL;

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editRegIntNo.ToString(), "RegionLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

				ShowRegionDetails(editRegIntNo);
			}
		}

		protected void ShowRegionDetails(int editRegIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.RegionDB aut = new Stalberg.TMS.RegionDB(connectionString);
			
			Stalberg.TMS.RegionDetails regDetails = aut.GetRegionDetails(editRegIntNo);

			txtRegionCode.Text = regDetails.RegCode;
			txtRegionName.Text = regDetails.RegName;
            txtRegionTel.Text = regDetails.RegTel;

			//ddlRegion.SelectedIndex = ddlRegion.Items.IndexOf( ddlRegion.Items.FindByValue(regDetails.RegIntNo.ToString()));
			
			pnlUpdateRegion.Visible = true;
			pnlAddRegion.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

	
		protected void dgRegion_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgRegion.CurrentPageIndex = e.NewPageIndex;

			BindGrid();		
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			RegionDB region = new Stalberg.TMS.RegionDB(connectionString);

            int regIntNo = Convert.ToInt32(Session["editRegIntNo"]);
            string errMessage = string.Empty;

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "RegionLookup", "RegIntNo");

            int delRegIntNo = region.DeleteRegion(regIntNo, ref errMessage);
            //2012-3-1 Linda  Modified into a multi-language
            lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
            if (delRegIntNo == 0)
			{
				lblError.Text += errMessage;
			}
            else if (delRegIntNo == -1)
            {
                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text += (string)GetLocalResourceObject("lblError.Text6");
            }
            else
            {

                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.RegionMaintenance, PunchAction.Delete);
            }
			lblError.Visible = true;

			BindGrid();
		}

	}
}
