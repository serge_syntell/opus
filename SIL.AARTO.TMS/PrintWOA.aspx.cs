using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Collections.Generic;
using SIL.QueueLibrary;
using System.Transactions;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that displays the details for summonses for an LA
    /// </summary>
    public partial class PrintWOA : System.Web.UI.Page
    {
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private string woaType = "S";
        private string cType = "SUM";
        // private string csCode = "200";                                    // must have successfully been returned from NaTis before we touch it
        //private int _statusSummons = 610;                                 //summons required
        //private int _statusSummonsPrinted = 620;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "PrintWOA.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Select the next print file for printing of warrants of arrest";

        //jerry 2012-03-14 add
        private string autCode = string.Empty;
        private string RuleSetWOAPrintDateForWoa = string.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            if (!String.IsNullOrEmpty(Request.QueryString["authIntNo"]))
            {
                autIntNo = Convert.ToInt32(Request.QueryString["authIntNo"]);
            }
            else
            {
                autIntNo = Convert.ToInt32(Session["autIntNo"]);
            }
            if (Session["WOAType"] != null)
            {
                woaType = Session["WOAType"].ToString();
            }
            //jerry 2012-03-14 add
            GetAutCode();
            //Heidi 2013-06-17 added for task 4973
            RuleSetWOAPrintDateForWoa=GetRuleSetWOAPrintDateForWoa(autIntNo);

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            pnlGeneral.Visible = true;
            dgPrintrun.Visible = true;

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                //this.chkShowAll.Checked = false;
                this.pnlUpdate.Visible = false;

                if (Session["showAllWoa"] != null && Session["showAllWoa"].ToString().Equals("Y"))
                {
                    chkShowAll.Checked = true;
                }

                this.PopulateAuthorites(autIntNo);

                woaType = ddlWOAType.SelectedValue;
                this.BindGrid(this.autIntNo, this.cType);
                this.ShowErrors(this.autIntNo);

                this.lblInstruct.Visible = false;
                //Jerry 2014-04-02 move it before BindGrid method
                //woaType = ddlWOAType.SelectedValue;
                Session["WOAType"] = woaType;
                if (ddlSelectLA.SelectedIndex > -1)
                {
                    autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

                    Session["printAutIntNo"] = autIntNo;

                    this.TicketNumberSearch1.AutIntNo = this.autIntNo;
                }
            }
        }

        /// <summary>
        /// jerry 2012-03-14 add autCode for push queue
        /// </summary>
        /// <param name="autIntNo"></param>
        private void GetAutCode()
        {
            this.autCode = new AuthorityDB(this.connectionString).GetAuthorityDetails(this.autIntNo).AutCode.Trim();
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorites(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void BindGrid(int autIntNo, string cType)
        {
            string showAll = "N";
            bool showViewButton = false;

            if (chkShowAll.Checked) showAll = "Y";

            Stalberg.TMS.WOADB printList = new Stalberg.TMS.WOADB(connectionString);

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)
            bool isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

            try
            {
                //disable the report print link of WOA if the elapse of the WOA is less than 14 days
                DateRulesDetails rule = new DateRulesDetails();
                rule.DtRStartDate = "WOALoadDate";
                rule.DtREndDate = "WOAPrintDate";
                rule.LastUser = this.login;
                rule.AutIntNo = autIntNo;
                DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
                int noDaysForNoticeExpiry = dateRule.SetDefaultDateRule();

                int totalCount = 0;
                dgPrintrun.DataSource = printList.GetWOAPrintrunDS(autIntNo, showAll, noDaysForNoticeExpiry, dgPrintrunPager.PageSize,
                    dgPrintrunPager.CurrentPageIndex - 1, out totalCount, woaType);
                dgPrintrun.DataKeyField = "PrintFile";
                dgPrintrun.DataBind();
                dgPrintrunPager.RecordCount = totalCount;

                if (GetRulePrintFileSplitForWoa(autIntNo))
                {
                    showViewButton = true;
                }

                //lblAuthorisationMsg.Visible = showViewButton;

                for (int i = 0; i < dgPrintrun.Items.Count; i++)
                {
                    // Jake 2013-06-14 removed, woa will be printed  before authorisation
                    //dgPrintrun.Items[i].Cells[11].Visible = showViewButton;

                    //2014-05-15 Heidi commented out the below line for Cells[5] and Cells[11] data is not in use(5283)
                    //if (dgPrintrun.Items[i].Cells[9].Text.ToString() == "0")
                    //{
                    //    //2014-05-15 Heidi commented out for hiding authorise button(5283)
                    //    //dgPrintrun.Items[i].Cells[5].Enabled = false;
                    //    //dgPrintrun.Items[i].Cells[11].Visible = false;
                    //}
                    //else
                    //{
                    //    //2014-05-15 Heidi commented out for hiding authorise button(5283)
                    //    //dgPrintrun.Items[i].Cells[5].Enabled = true;
                    //}

                    if (dgPrintrun.Items[i].Cells[8].Text.ToString() == "N")
                    {
                        dgPrintrun.Items[i].Cells[5].Enabled = false;
                        dgPrintrun.Items[i].Cells[6].Enabled = false;
                        dgPrintrun.Items[i].Cells[7].Enabled = false;
                    }
                    else
                    {
                        dgPrintrun.Items[i].Cells[5].Enabled = true;
                        dgPrintrun.Items[i].Cells[6].Enabled = true;
                        dgPrintrun.Items[i].Cells[7].Enabled = true;
                    }

                }

                if (isIBMPrinter)
                {
                    for (int i = 0; i < dgPrintrun.Items.Count; i++)
                    {
                        dgPrintrun.Items[i].Cells[5].Enabled = false;

                        // Jake 2013-06-14 added 
                        // If for IMB printer we need to disable Print woa and print court register button here
                        if (woaType == "S")
                        {
                            dgPrintrun.Items[i].Cells[5].Enabled = false;
                            dgPrintrun.Items[i].Cells[4].Enabled = false;
                            //2014-05-15 Heidi commented out for hiding authorise button(5283)
                            //dgPrintrun.Items[i].Cells[5].Enabled = false;
                            dgPrintrun.Items[i].Cells[6].Enabled = false;
                            dgPrintrun.Items[i].Cells[7].Enabled = false;
                        }
                        else {
                            dgPrintrun.Items[i].Cells[5].Enabled = true;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.DataBind();
            }

            if (dgPrintrun.Items.Count == 0)
            {
                dgPrintrun.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgPrintrun.Visible = true;
                lblError.Visible = false;
            }
            //Jake 2014-08-12 comment out
            //if (cType != "Authorise")
            //    pnlUpdate.Visible = false;

            if (isIBMPrinter && woaType == "S")
            {
                this.lblError.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("errorMsg");
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            if (txtPrint.Text.Trim().Equals(string.Empty) && txtSummonsNo.Text.Trim().Equals(string.Empty))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }

            string sValue = string.Empty;
            string sMode = "N";
            if (txtPrint.Text.Trim().Length > 1)
                sValue = txtPrint.Text.Trim();

            if (txtSummonsNo.Text.Trim().Length > 1)
            {
                sValue = txtSummonsNo.Text.Trim();
                sMode = "S";
            }

            // Generate the JavaScript to pop up a print-only window
            Helper_Web.BuildPopup(this, "WOAViewer.aspx", "printfile", "*" + sValue, "mode", sMode);
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            RetriveData();
        }

        private void RetriveData()
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            woaType = ddlWOAType.SelectedValue;
            Session["printAutIntNo"] = autIntNo;
            Session["WOAType"] = woaType;
            //Session.Add("autIntNo", autIntNo);
            this.TicketNumberSearch1.AutIntNo = this.autIntNo;
            dgPrintrunPager.RecordCount = 0;
            dgPrintrunPager.CurrentPageIndex = 1;
            this.BindGrid(autIntNo, cType);
            this.ShowErrors(autIntNo);

            pnlUpdate.Visible = false;
        }

        private void ShowErrors(int autIntNo)
        {
            //not implemented yet
            //woaDB woas = new woaDB(this.connectionString);
            //DataSet ds = woas.GetWOAErrors(autIntNo);
            //this.grdErrors.DataSource = ds;
            //this.grdErrors.DataBind();
        }

        protected void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            //Session["autIntNo"] = autIntNo;

            //Seawen 2013-09-13 show on current page index
            //dgPrintrunPager.RecordCount = 0;
            //dgPrintrunPager.CurrentPageIndex = 1;
            BindGrid(autIntNo, cType);

            if (chkShowAll.Checked)
                Session["showAllWoa"] = "Y";
            else
                Session["showAllWoa"] = "N";
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            //not implemented yet
            //NoticeDB notice = new NoticeDB(connectionString);
            //int noOfRows = notice.UpdateSummonsChargeStatus(autIntNo, lblPrintFile.Text);

            //if (noOfRows > 0)
            //{
            //    this.BindGrid(autIntNo, this.cType);
            //    pnlUpdate.Visible = false;
            //}

            //lblError.Visible = true;
            //lblError.Text = noOfRows.ToString() + " have been updated";
        }

        private string GetRuleSetWOAPrintDateForWoa(int authIntNo)
        {
            //2013-06-17 added by Heidi for getting authority rule start
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = authIntNo;
            arDetails.ARCode = "2570";
            arDetails.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(arDetails, this.connectionString);
            KeyValuePair<int, string> valueArDet = authRule.SetDefaultAuthRule();
            //2013-06-17 added by Heidi for getting authority rule end
            return valueArDet.Value;
        }


        protected void dgPrintrun_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            dgPrintrun.SelectedIndex = e.Item.ItemIndex;

            if (dgPrintrun.SelectedIndex > -1)
            {
                pnlUpdate.Visible = true;
                lblPrintFile.Text = dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString();

                //Barry Dickson - allow the visibility set on the update status button if they have selected to update status and not print
                //chnaged my mind, creates an extra step, just do it here and not on the next button click
                if (e.CommandName == "Select")
                {
                    //btnUpdateStatus.Visible = true;
                    //need to update the status of the print file
                    //?looking through code and stored procs, the print file status is just wether the WOAPrintDate is set or not...

                    //BD 20080507 need to check that they have generated the file before they can update the print status
                    Stalberg.TMS.WOADB printStatus = new Stalberg.TMS.WOADB(connectionString);
                    decimal WOAIntNo = printStatus.UpdateWOAPrintStatus(dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString(), this.login, RuleSetWOAPrintDateForWoa);
                    if (WOAIntNo > 0)
                    {
                        lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text");
                    }
                    else if (WOAIntNo == -10)
                    {
                        lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text1");
                    }
                    else
                    {
                        lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text2");
                    }
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintWOA, PunchAction.Change);  


                }
                else if (e.CommandName == "PrintControl")
                {
                    string printFile = e.Item.Cells[0].Text;

                    PageToOpen[] pages = new PageToOpen[1];
                    List<QSParams> parameters = new List<QSParams>();
                    parameters.Add(new QSParams("printfile", printFile));
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    parameters.Add(new QSParams("printType", "PrintWOAPrintControlRegister"));
                    pages[0] = new PageToOpen(this, "WOAControlViewer.aspx", parameters);
                    parameters = new List<QSParams>();

                    Helper_Web.BuildPopup(pages);

                    lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text3");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintWOAPrintControlRegister, PunchAction.Change);  

                }
                else if (e.CommandName == "PrintCourtRegister")
                {
                    string printFile = e.Item.Cells[0].Text;

                    PageToOpen[] pages = new PageToOpen[1];
                    List<QSParams> parameters = new List<QSParams>();
                    parameters.Add(new QSParams("woaPrintFile", printFile));
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    parameters.Add(new QSParams("printType", "PrintWOAPrintCourtRegister"));
                    pages[0] = new PageToOpen(this, "WOARegisterReport_Viewer.aspx", parameters);
                    parameters = new List<QSParams>();

                    Helper_Web.BuildPopup(pages);

                    lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text4");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintWOAPrintCourtRegister, PunchAction.Change); 
                }
                //bind the source code to the command AuthorizeWOA
                else if (e.CommandName == "AuthorizeWOA")
                {
                    //Anthorise the selected WOA
                    Stalberg.TMS.WOADB authorizeWOA = new Stalberg.TMS.WOADB(connectionString);

                    //jerry 2012-03-14 add, push print file after authorized it
                    QueueItem item = new QueueItem();
                    QueueItemProcessor queProcessor = new QueueItemProcessor();
                    using (TransactionScope scope = new TransactionScope())
                    {
                        decimal WOAIntNo = authorizeWOA.UpdateWOAAuthorizeStatus(dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString(), this.login);
                        if (WOAIntNo > 0)
                        {
                            dgPrintrunPager.RecordCount = 0;
                            dgPrintrunPager.CurrentPageIndex = 1;
                            this.BindGrid(this.autIntNo, "Authorise");
                            lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text5");

                            // jerry 2012-03-14 push Print WOA service
                            // Save PrintFileName
                            string printFile = dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString().Trim();
                            SIL.AARTO.DAL.Entities.PrintFileName printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().GetByPrintFileName(printFile);
                            if (printFileName == null)
                            {
                                printFileName = new SIL.AARTO.DAL.Entities.PrintFileName();
                                printFileName.PrintFileName = printFile;
                                printFileName.AutIntNo = this.autIntNo;
                                printFileName.DateAdded = DateTime.Now;
                                printFileName.LastUser = this.login;
                                printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().Save(printFileName);
                            }

                            List<Int32> woaList = new List<int>();
                            SqlDataReader reader = authorizeWOA.GetWoaIntNoByWoaPrintFileName(printFile);
                            while (reader.Read())
                            {
                                if (Convert.ToInt32(reader["WOAIntNo"]) != -1)
                                {
                                    woaList.Add(Convert.ToInt32(reader["WOAIntNo"]));
                                }
                            }
                            reader.Dispose();

                            if (woaList.Count > 0)
                            {
                                foreach (int tempWoaIntNo in woaList)
                                {
                                    SIL.AARTO.DAL.Entities.PrintFileNameWoa printFileNameWOA = new SIL.AARTO.DAL.Services.PrintFileNameWoaService().GetByWoaIntNoPfnIntNo(tempWoaIntNo, printFileName.PfnIntNo);
                                    if (printFileNameWOA == null)
                                    {
                                        printFileNameWOA = new SIL.AARTO.DAL.Entities.PrintFileNameWoa();
                                        printFileNameWOA.PfnIntNo = printFileName.PfnIntNo;
                                        printFileNameWOA.WoaIntNo = tempWoaIntNo;
                                        printFileNameWOA.LastUser = this.login;
                                        new SIL.AARTO.DAL.Services.PrintFileNameWoaService().Save(printFileNameWOA);
                                    }
                                }
                            }

                            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                            arDetails.AutIntNo = autIntNo;
                            arDetails.ARCode = "6209";
                            arDetails.LastUser = this.login;
                            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
                            //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)
                            bool isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

                            if (!isIBMPrinter)
                            {
                                item = new QueueItem();
                                item.Body = printFileName.PfnIntNo.ToString();
                                item.Group = this.autCode.Trim();
                                item.QueueType = ServiceQueueTypeList.PrintWOA;

                                queProcessor.Send(item);
                            }
                            else
                            {
                                //2014-01-07 Heidi added for WOA Direct Printing (5101)
                                item = new QueueItem();
                                item.Body = printFileName.PfnIntNo.ToString();
                                item.Group = this.autCode.Trim() + "|" + (int)SIL.AARTO.DAL.Entities.ReportConfigCodeList.WOADirectPrinting;
                                item.QueueType = ServiceQueueTypeList.PrintToFile;
                                item.LastUser = this.login;

                                queProcessor.Send(item);
                            }
                        }
                        else if (WOAIntNo == -10)
                        {
                            lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text6");
                        }
                        else
                        {
                            lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text7");
                        }
                        //Linda 2012-9-12 add for list report19
                        //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.login, PunchStatisticsTranTypeList.WOASigning,SIL.AARTO.BLL.Utility.Printing.PunchAction.Change);

                        scope.Complete();
                    }
                }
                else if (e.CommandName == "ViewWoa")
                {
                    string printFile = e.Item.Cells[0].Text;
                    //PageToOpen[] pages = new PageToOpen[1];
                    //List<QSParams> parameters = new List<QSParams>();
                    //parameters.Add(new QSParams("woaPrintFile", printFile));
                    //pages[0] = new PageToOpen(this, "WOAPrintDetail.aspx", parameters);
                    //parameters = new List<QSParams>();

                    //Helper_Web.BuildPopup(pages);
                    //string showAll = "N";
                    int authIntNo = Convert.ToInt32(this.ddlSelectLA.SelectedItem.Value);
                    //if (chkShowAll.Checked) showAll = "Y";
                    //PunchStats805806 enquiry PrintWOA

                    Response.Redirect(String.Format("WOAPrintDetail.aspx?woaPrintFile={0}&authIntNo={1}", printFile, authIntNo));

                }
                else
                {
                    //btnUpdateStatus.Visible = false;
                    lblPFile.Text = (string)GetLocalResourceObject("lblPFile.Text8");
                }

                lblError.Visible = true;
                chkShowAll_CheckedChanged(source, e);
            }
        }

        //protected void dgPrintrun_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        //{
        //    dgPrintrun.CurrentPageIndex = e.NewPageIndex;

        //    autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
        //    BindGrid(autIntNo, cType);
        //}

        protected void dgPrintrun_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string printFile = e.Item.Cells[0].Text;

            PageToOpen[] pages = new PageToOpen[1];
            List<QSParams> parameters = new List<QSParams>();
            parameters.Add(new QSParams("printfile", printFile));
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            parameters.Add(new QSParams("printType", "PrintWOA"));
            pages[0] = new PageToOpen(this, "WOAViewer.aspx", parameters);
            parameters = new List<QSParams>();

            Helper_Web.BuildPopup(pages);
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtPrint.Text = this.TicketNumberSearch1.TicketNumber;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
        }

        protected void dgPrintrun_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private bool GetRulePrintFileSplitForWoa(int authIntNo)
        {

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();

            arDetails.AutIntNo = authIntNo;
            arDetails.ARCode = "6305";
            arDetails.LastUser = this.login;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            KeyValuePair<int, string> printFileSplit = ar.SetDefaultAuthRule();
            return printFileSplit.Value.Equals("Y");
        }

        protected void dgPrintrunPager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            woaType = ddlWOAType.SelectedValue;
            BindGrid(autIntNo, cType);
        }

        protected void ddlWOAType_SelectedIndexChanged(object sender, EventArgs e)
        {
            RetriveData();
        }
    }
}
