<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Court" CodeBehind="Court.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAuthority" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptAuthority.Text %>" OnClick="btnOptAuthority_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <asp:DataGrid ID="dgCourt" Width="495px" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                        AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                        FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                        Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgCourt_ItemCommand"
                                        OnPageIndexChanged="dgCourt_PageIndexChanged">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="CrtIntNo" HeaderText="CrtIntNo"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="CrtName" HeaderText="<%$Resources:pnlGeneral.HeaderText1 %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="CrtPrefix" HeaderText="<%$Resources:pnlGeneral.HeaderText2 %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="CrtNo" HeaderText="<%$Resources:pnlGeneral.HeaderText3 %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="CrDefaultContempt" DataFormatString="{0:N0}" HeaderText="<%$Resources:pnlGeneral.HeaderText4 %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MagisterialCD" HeaderText="<%$Resources:pnlGeneral.HeaderText5 %>"></asp:BoundColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgCourtItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                    </asp:DataGrid>
                                </asp:Panel>
                                <asp:Panel ID="pnlAddCourt" runat="server" Height="127px">
                                    <table id="Table2" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="lblAddCourt" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddCourt.Text %>"></asp:Label></td>
                                            <td width="248" height="2"></td>
                                            <td height="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="lblAddCrtName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddCrtName.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddCrtName" runat="server" Width="233px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="30"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" ErrorMessage="<%$Resources:lblCourt.Text %>" ControlToValidate="txtAddCrtName"
                                                    Display="dynamic" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="lblAddFullCrtName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddFullCrtName.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddFullCrtName" runat="server" Width="233px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="30"></asp:TextBox></td>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblAddCrtNo" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddCrtNo.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddCrtNo" runat="server" Width="90px" CssClass="Normal" MaxLength="6"></asp:TextBox></td>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblAddCourtPrefix" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddCourtPrefix.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddCourtPrefix" runat="server" Width="90px" CssClass="Normal"
                                                    MaxLength="5"></asp:TextBox></td>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblAddMagisterialCD" runat="server" Width="94px" CssClass="NormalBold"
                                                        Text="<%$Resources:lblAddMagisterialCD.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddMagisterialCD" runat="server" Width="90px" CssClass="Normal"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblAddCourtContempt" runat="server" Width="139px" CssClass="NormalBold" Text="<%$Resources:lblAddCourtContempt.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddCourtContempt" runat="server" Width="87px" CssClass="Normal"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAddCourtContempt"
                                                    CssClass="NormalRed" ErrorMessage="<%$Resources:ReqAddCourtContempt.ErrorMessage %>" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <p>
                                                    <asp:Label ID="lblAddCrtAddress" runat="server" Width="112px" CssClass="NormalBold" Text="<%$Resources:lblAddCrtAddress.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddCrtAddress" runat="server" Width="264px" CssClass="Normal" MaxLength="255" TextMode="MultiLine"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 21px" width="157">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="112px" Text="<%$Resources:lblAddPayAddr.Text %>"></asp:Label>
                                            </td>
                                            <td style="height: 21px" width="248">
                                                <asp:TextBox ID="txtAddPayAddr1" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="25" Width="233px"></asp:TextBox></td>
                                            <td style="height: 21px"></td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddPayAddr2" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="25" Width="233px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddPayAddr3" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="25" Width="233px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddPayAddr4" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="25" Width="233px"></asp:TextBox></td>
                                            <td>
                                                <asp:Button ID="btnAddCourt" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddCourt.Text %>"
                                                    OnClick="btnAddCourt_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateCourt" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label19" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblUpdCourt.Text %>"></asp:Label></td>
                                            <td width="248" height="2"></td>
                                            <td height="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddCrtName.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtCrtName" runat="server" Width="236px" CssClass="NormalMand" Height="24px"
                                                    MaxLength="30"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                    CssClass="NormalRed" ErrorMessage="<%$Resources:lblCourt.Text %>" ControlToValidate="txtCrtName"
                                                    Display="dynamic" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="lblFullCrtName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddFullCrtName.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtFullCrtName" runat="server" Width="233px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="30"></asp:TextBox></td>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="Label17" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddCrtNo.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtCrtNo" runat="server" Width="87px" CssClass="Normal" MaxLength="21"></asp:TextBox></td>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblCourtPrefix" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddCourtPrefix.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtCourtPrefix" runat="server" Width="87px" CssClass="Normal" MaxLength="5"></asp:TextBox></td>
                                            <td height="25"></td>






                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblMagisterialCD" runat="server" Width="145px" CssClass="NormalBold"
                                                        Text="<%$Resources:lblMagisterialCD.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtMagisterialCD" runat="server" Width="87px" CssClass="Normal"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblCourtContempt" runat="server" Width="145px" CssClass="NormalBold" Text="<%$Resources:lblAddCourtContempt.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtCourtContempt" runat="server" Width="87px" CssClass="Normal"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCourtContempt"
                                                    CssClass="NormalRed" ErrorMessage="<%$Resources:ReqAddCourtContempt.ErrorMessage %>" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <p>
                                                    <asp:Label ID="Label16" runat="server" Width="112px" CssClass="NormalBold" Text="<%$Resources:lblAddCrtAddress.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248">
                                                <asp:TextBox ID="txtCrtAddress" runat="server" Width="264px" CssClass="Normal" MaxLength="60"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 21px" width="157">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="112px" Text="<%$Resources:lblAddPayAddr.Text %>"></asp:Label></td>
                                            <td style="height: 21px" width="248">
                                                <asp:TextBox ID="txtPayAddr1" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="25" Width="233px"></asp:TextBox></td>
                                            <td style="height: 21px"></td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtPayAddr2" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="25" Width="233px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtPayAddr3" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="25" Width="233px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtPayAddr4" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="25" Width="233px"></asp:TextBox></td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlAuthority" runat="server">
                                    <table id="Table9" height="195" cellspacing="1" cellpadding="1" width="650" border="0">
                                        <tr>
                                            <td valign="top"></td>
                                            <td valign="top" width="292">
                                                <asp:Label ID="Label2" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                            <td></td>
                                            <td>
                                                <asp:Label ID="lblAuthority" runat="server" Width="292px" CssClass="Normal" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td valign="top"></td>
                                            <td valign="top" width="292">
                                                <asp:DropDownList ID="ddlAuthority" runat="server" Width="282px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                            <td align="center">
                                                <p>
                                                    <asp:ImageButton ID="btnAddAuthority" runat="server" ToolTip="<%$Resources:btnAddAuthority.ToolTip %>"
                                                        ImageUrl="images/arrow_right.jpg" OnClick="btnAddAuthority_Click"></asp:ImageButton>
                                                </p>
                                                <p>
                                                    <asp:ImageButton ID="btnDelAuthority" runat="server" ToolTip="<%$Resources:btnDelAuthority.ToolTip %>"
                                                        ImageUrl="images/arrow_left.jpg" OnClick="btnDelAuthority_Click"></asp:ImageButton>
                                                </p>
                                            </td>
                                            <td>
                                                <asp:ListBox ID="lstAuth_Court" runat="server" Width="224px" CssClass="Normal" Height="144px"
                                                    AutoPostBack="True"></asp:ListBox></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
