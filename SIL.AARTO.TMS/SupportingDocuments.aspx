<%@ Page Language="c#" Inherits="Stalberg.TMS.SupportingDocuments" Codebehind="SupportingDocuments.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script>
		   
			function Close()
			{
				window.close();
			}
    </script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form2" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                   
                </td>
            </tr>
        </table>
      
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center" style="width: 505px">
                   
                </td>
                <td valign="top" align="center" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%" HorizontalAlign=Center>
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            <asp:Label ID="lblTicketNo" runat="server" Text="<%$Resources:lblTicketNo.Text %>" CssClass="SubContentHead"></asp:Label>&nbsp;</p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                    <asp:Panel ID="pnlDocumentation" runat="server" Width="100%" HorizontalAlign=Center>
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblDocuments" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblDocuments.Text %>"></asp:Label>
                                    <br />
                                    <asp:GridView ID="grvRepDocs" runat="server" AutoGenerateColumns="False" DataKeyNames="RDIntNo,RDType"
                                        CssClass="Normal" OnSelectedIndexChanged="grvRepDocs_SelectedIndexChanged" OnRowCommand="grvRepDocs_RowCommand" Width="472px" HorizontalAlign="Left" OnRowDeleting="grvRepDocs_RowDeleting">
                                        <FooterStyle CssClass="CartListFooter" />
                                        <HeaderStyle CssClass="CartListHead" HorizontalAlign="Left" />
                                        <Columns>
                                            <asp:BoundField DataField="RDIntNo" Visible="False" />
                                            <asp:BoundField DataField="RDName" HeaderText="<%$Resources:grvRepDocs.HeaderText %>" />
                                            <asp:TemplateField HeaderText="<%$Resources:grvRepDocs.HeaderText1 %>" SortExpression="RDDateLoaded">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("RDDateLoaded", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField CancelText="" EditText="" HeaderText="<%$Resources:grvRepDocs.HeaderText2 %>" InsertText=""
                                                SelectText="<%$Resources:grvRepDocs.SelectText %>" ShowSelectButton="True" UpdateText="" >
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:CommandField>
                                            <asp:CommandField ShowDeleteButton="True" DeleteText="<%$Resources:grvRepDocs.DelectText %>" />
                                        </Columns>
                                    </asp:GridView>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblUpLoad.Text %>"></asp:Label>
                                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="Normal" Width="486px" /></td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <br />
                                    <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDocumentName.Text %>"></asp:Label>
                                    &nbsp;
                                    <asp:TextBox ID="txtDocName" runat="server" CssClass="Normal" Width="352px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <br />
                                    <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Text="<%$Resources:lblComments.Text %>"></asp:Label><br />
                                    <asp:TextBox ID="txtAddRDComments" runat="server" CssClass="Normal" Height="45px"
                                        Rows="6" TextMode="MultiLine" Width="474px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td valign="top" style="height: 22px" align=center>
                                    <asp:Button ID="btnAddDoc" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddDoc.Text %>" Width="138px"
                                        OnClick="btnAddDoc_Click" />
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    <asp:HyperLink ID="hlBack" runat="server" CssClass="NormalBold" NavigateUrl="javascript:Close()" Text="<%$Resources:hlBack.Text %>"></asp:HyperLink></td>
                            </tr>
                        </table>
                    </asp:Panel>
            </tr>
            <tr align="center">
                <td align="center" style="width: 505px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top" align="center" style="width: 505px">
                </td>
                <td valign="top" align="left" width="100%">
                    &nbsp;</td>
            </tr>
        </table>
       
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
