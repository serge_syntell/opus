<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.SuspenseNoticeAssign" Codebehind="SuspenseNoticeAssign.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1" style="text-align: center">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                    </asp:Panel>
                    <%--<asp:UpdatePanel ID="udpSuspenseAssign" runat="server">
                        <ContentTemplate>--%>
                            <p>
                                <asp:Label ID="lblError" runat="server" ForeColor="Red" CssClass="NormalRed"></asp:Label>&nbsp;</p>
                            <asp:Panel ID="pnlSuspense" runat="server" Width="100%">
                                <fieldset>
                                    <legend>
                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblNotice.Text %>"></asp:Label></legend>
                                    <table border="0" class="Normal" style="width: 510px">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblSuspenseNotice.Text %>"></asp:Label></td>
                                            <td style="text-align: right">
                                                <asp:TextBox ID="txtSuspenseNotice" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:Button ID="btnSuspenseNotice" runat="server" Text="<%$Resources:btnSuspenseNotice.Text %>" CssClass="NormalButton"
                                                    OnClick="btnSuspenseNotice_Click" Width="160px" /></td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="grdSuspense" runat="server" CssClass="Normal" ShowFooter="True"
                                        AutoGenerateColumns="False" CellPadding="4" OnRowCreated="grdSuspense_RowCreated"
                                        OnSelectedIndexChanged="grdSuspense_SelectedIndexChanged">
                                        <FooterStyle CssClass="CartListHead" />
                                        <HeaderStyle CssClass="CartListHead" />
                                        <Columns>
                                            <asp:BoundField DataField="NotTicketNo" HeaderText="<%$Resources:grdSuspense.HeaderText %>" />
                                            <asp:TemplateField HeaderText="<%$Resources:grdSuspense.HeaderText1 %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtOffenceDate" runat="server" Text='<%# Bind("NotOffenceDate") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOffenceDate" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$Resources:grdSuspense.HeaderText2 %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAmount" runat="server" Text='<%# Bind("ChgRevFineAmount") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAmount" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField HeaderText="<%$Resources:grdSuspense.HeaderText3 %>" ShowSelectButton="True" />
                                        </Columns>
                                    </asp:GridView>
                                </fieldset>
                            </asp:Panel>
                            <asp:Panel ID="pnlDestination" Visible="false" runat="server" Width="100%">
                                <fieldset>
                                    <legend>
                                        <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblDestinationNotice.Text %>"></asp:Label></legend>
                                    <table border="0" class="Normal" style="width: 510px">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblDestination.Text %>"></asp:Label></td>
                                            <td style="text-align: right">
                                                <asp:TextBox ID="txtDestination" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:Button ID="btnDestination" runat="server" CssClass="NormalButton" Text="<%$Resources:btnDestination.Text %>"
                                                    OnClick="btnDestination_Click" Width="160px" /></td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="grdDestination" runat="server" CssClass="Normal" ShowFooter="True"
                                        AutoGenerateColumns="False" OnRowCreated="grdDestination_RowCreated" OnSelectedIndexChanged="grdDestination_SelectedIndexChanged"
                                        CellPadding="4">
                                        <FooterStyle CssClass="CartListHead" />
                                        <HeaderStyle CssClass="CartListHead" />
                                        <Columns>
                                            <asp:BoundField DataField="TicketNo" HeaderText="<%$Resources:grdDestination.HeaderText %>" />
                                            <asp:TemplateField HeaderText="<%$Resources:grdDestination.HeaderText1 %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("OffenceDate") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOffenceDate" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Name" HeaderText="<%$Resources:grdDestination.HeaderText2 %>" />
                                            <asp:BoundField DataField="RegistrationNo" HeaderText="<%$Resources:grdDestination.HeaderText3 %>" />
                                            <asp:TemplateField HeaderText="<%$Resources:grdDestination.HeaderText4  %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Amount") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAmount" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField HeaderText="<%$Resources:grdDestination.HeaderText5  %>" ShowSelectButton="True" />
                                        </Columns>
                                    </asp:GridView>
                                </fieldset>
                            </asp:Panel>
                            <asp:Panel ID="pnlConfirm" Visible="false" runat="server" Width="100%">
                                <fieldset>
                                    <legend>
                                        <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblConfirm.Text %>"></asp:Label></legend>
                                    <table border="0" style="text-align: center;">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lblConfirmation" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnNo" runat="server" Text="<%$Resources:btnNo.Text %>" ForeColor="red" Width="50px" CssClass="NormalButton"
                                                    OnClick="btnNo_Click" /></td>
                                            <td>
                                                <asp:Button ID="btnYes" runat="server" Text="<%$Resources:btnYes.Text %>" ForeColor="blue" Width="50px" CssClass="NormalButton"
                                                    OnClick="btnYes_Click" /></td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </asp:Panel>
                     <%--   </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>--%>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
