﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.NoticeExpiryReport" Codebehind="NoticeExpiryReport.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                        </p>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlReport" runat="Server" Width="100%">
                        <table style="width: 917px">
                            <tr>
                                <td>
                                <asp:Label ID="lblSelectLA" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelectLA.Text %>"></asp:Label>
                                    </td>
                                <td class="style1">
                                    <asp:DropDownList ID="ddlSelAuthority" runat="server" Width="217px" CssClass="Normal">
                                        </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                <asp:Label ID="lblExpiryRule" runat="server" CssClass="NormalBold" Text="<%$Resources:lblExpiryRule.Text %>"></asp:Label>
                                   </td>
                                <td class="style3">
                                 <asp:DropDownList ID="ddlDateRule" runat="server" CssClass="Normal" 
                                        Width="680px" Height="16px">
                                        </asp:DropDownList>
                                    </td>
                            </tr>
                            <tr>
                                <td style="width: 157; height: 2">
                                    <asp:Label ID="lblDateRange" runat="server" CssClass="NormalBold" Width="200px" Text="<%$Resources:lblDateRange.Text %>"></asp:Label>
                                </td>
                                <td height="2" class="style1">
                                <asp:TextBox ID="tbWaningPeriod" runat="server" Width="122px">30</asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 157; height: 2">
                                    <asp:Label ID="lblSort" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSort.Text %>"></asp:Label>
                                </td>
                                <td height="2" class="style1">
                                    <asp:DropDownList ID="ddlSort" runat="server" CssClass="Normal" 
                                        Height="16px" Width="226px">
                                        <asp:ListItem Text="<%$Resources:ddlSortItem.Text %>"></asp:ListItem>
                                        <asp:ListItem Text="<%$Resources:ddlSortItem.Text1 %>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td width="10" height="2">
                                    &nbsp;</td>
                                <td height="2" class="style1">
                                    <asp:Button ID="btnViewReport" runat="server" CssClass="NormalButton" Text="<%$Resources:btnViewReport.Text %>"
                                        OnClick="btnViewReport_Click"></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%">
                    &nbsp;</td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>