<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.MenuBuilder" Codebehind="MenuBuilder.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <tg1:Tag ID="Tag1" runat="server"></tg1:Tag>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <table height="562" cellspacing="0" cellpadding="0" width="185" border="0">
        <tr valign="top">
            <td width="185" height="562">
                <form runat="server">
                    <table height="183" cellspacing="0" cellpadding="0" width="560" border="0">
                        <tr valign="top">
                            <td width="1" height="7">
                            </td>
                            <td width="559">
                            </td>
                        </tr>
                        <tr valign="top">
                            <td height="176">
                            </td>
                            <td>
                                <table id="Table1" height="175" cellspacing="1" cellpadding="1" width="558" border="0">
                                    <tr>
                                        <td valign="top" width="355">
                                            <asp:Panel ID="pnlShowMenuItem" runat="server" Width="365px">
                                                <table id="tblShowMenuItem" height="170" cellspacing="0" cellpadding="1" width="363"
                                                    border="0">
                                                    <tr>
                                                        <td valign="top" align="left" height="25">
                                                            <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label></td>
                                                        <td height="25">
                                                            <p>
                                                                <asp:TextBox ID="txtName" runat="server" Width="264px" CssClass="NormalMand" MaxLength="50"></asp:TextBox></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <asp:Label ID="lblDescr" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDescr.Text %>"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtDescr" runat="server" Width="268px" CssClass="Normal" MaxLength="255"
                                                                TextMode="MultiLine"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <asp:Label ID="lblURL" runat="server" CssClass="NormalBold" Text="<%$Resources:lblURL.Text %>"></asp:Label></td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="txtURL" runat="server" Width="250px" CssClass="NormalMand" MaxLength="100"
                                                                ReadOnly="True"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSelectPage" runat="server" ImageUrl="images/arrow_left.jpg"
                                                                ToolTip="Select page to display" OnClick="btnSelectPage_Click"></asp:ImageButton></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <asp:Label ID="lblOrder" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOrder.Text %>"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtOrder" runat="server" Width="36px" CssClass="NormalMand" MaxLength="2">0</asp:TextBox>
                                                            <asp:RangeValidator ID="RangeValidator1" runat="server" CssClass="NormalRed" ErrorMessage="RangeValidator"
                                                                ControlToValidate="txtOrder" Type="Integer" MinimumValue="0" MaximumValue="9999"
                                                                ForeColor=" " Text="<%$Resources:reqOrder.Text %>"></asp:RangeValidator>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="269px"
                                                                CssClass="NormalRed" ErrorMessage="<%$Resources:reqOrder.ErrorMsg %>" ControlToValidate="txtOrder"
                                                                ForeColor=" " Height="11px"></asp:RequiredFieldValidator></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">
                                                            <asp:TextBox ID="txtColName" runat="server" Width="12px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox>
                                                            <asp:TextBox ID="txtColValue" runat="server" Width="13px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox>
                                                            <asp:TextBox ID="txtFKValue" runat="server" Width="10px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox></td>
                                                        <td>
                                                            <asp:Button ID="btnSave" runat="server" Width="94px" CssClass="NormalButton" Text="<%$Resources:btnSave.Text %>"
                                                                OnClick="btnSave_Click"></asp:Button>
                                                            <asp:Button ID="btnAddSubItem" runat="server" Width="96px" CssClass="NormalButton"
                                                                Text="<%$Resources:btnAddSubItem.Text %>" OnClick="btnAddSubItem_Click"></asp:Button>
                                                            <asp:Button ID="btnDelete" runat="server" Width="89px" CssClass="NormalButton" Text="<%$Resources:btnDelete.Text %>"
                                                                OnClick="btnDelete_Click"></asp:Button></td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAddMenuItem" runat="server" Width="365px">
                                                <asp:Label ID="lblAdd" runat="server" Width="307px" CssClass="ProductListHead" Text="<%$Resources:lblAdd.Text %>"></asp:Label>
                                                <table id="Table2" height="112" cellspacing="0" cellpadding="1" width="355" border="0">
                                                    <tr>
                                                        <td valign="top" align="left" height="25">
                                                            <asp:Label ID="lblAddName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label></td>
                                                        <td height="25">
                                                            <asp:TextBox ID="txtAddName" runat="server" Width="264px" CssClass="NormalMand" MaxLength="50"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <asp:Label ID="lblAddDescr" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDescr.Text %>"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtAddDescr" runat="server" Width="268px" CssClass="Normal" MaxLength="255"
                                                                TextMode="MultiLine"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <asp:Label ID="lblAddURL" runat="server" CssClass="NormalBold" Text="<%$Resources:lblURL.Text %>"></asp:Label></td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="txtAddURL" runat="server" Width="250px" CssClass="NormalMand" MaxLength="100"
                                                                ReadOnly="True"></asp:TextBox>
                                                            <asp:ImageButton ID="btnAddSelectPage" runat="server" ImageUrl="images/arrow_left.jpg"
                                                                ToolTip="Select page to display" OnClick="btnAddSelectPage_Click"></asp:ImageButton></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <asp:Label ID="lblAddOrder" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOrder.Text %>"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtAddOrder" runat="server" Width="36px" CssClass="NormalMand" MaxLength="2">0</asp:TextBox>
                                                            <asp:RangeValidator ID="Rangevalidator2" runat="server" CssClass="NormalRed" ErrorMessage="RangeValidator"
                                                                ControlToValidate="txtAddOrder" Type="Integer" MinimumValue="0" MaximumValue="9999"
                                                                ForeColor=" " Text="<%$Resources:reqOrder.Text %>"></asp:RangeValidator>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Width="269px"
                                                                CssClass="NormalRed" ErrorMessage="<%$Resources:reqOrder.ErrorMsg %>" ControlToValidate="txtAddOrder"
                                                                ForeColor=" " Height="11px"></asp:RequiredFieldValidator></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left">
                                                            <asp:TextBox ID="txtAddColName" runat="server" Width="12px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox>
                                                            <asp:TextBox ID="txtAddColValue" runat="server" Width="13px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox>
                                                            <asp:TextBox ID="txtAddFKValue" runat="server" Width="10px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox></td>
                                                        <td>
                                                            <asp:Button ID="btnAddItem" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddItem.Text %>"
                                                                OnClick="btnAddItem_Click"></asp:Button>
                                                            <asp:Button ID="btnReturn" runat="server" Width="144px" CssClass="NormalButton" Text="<%$Resources:btnReturn.Text %>"
                                                                OnClick="btnReturn_Click"></asp:Button></td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Label ID="lblError" runat="Server" EnableViewState="false" CssClass="NormalRed"
                                                ForeColor=" "></asp:Label></td>
                                        <td valign="top" width="200">
                                            <asp:ListBox ID="lstASPPageList" runat="server" CssClass="Normal" Width="193px" Height="218px">
                                            </asp:ListBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
    </table>
</body>
</html>
