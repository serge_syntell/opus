using System;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Drawing;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
//using ceTe.DynamicPDF;
//using ceTe.DynamicPDF.ReportWriter;
//using ceTe.DynamicPDF.ReportWriter.ReportElements;
//using ceTe.DynamicPDF.ReportWriter.Data;
//using ceTe.DynamicPDF.Merger;
using System.IO;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for Deferred Payments on a Notice
    /// </summary>
    public partial class DeferredPaymentsReportViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private ReportDocument _reportDoc = new ReportDocument();
        private string thisPageURL = "DeferredPaymentsReportViewer.aspx";
        //private string thisPage = "Deferred Payments Report Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            int notIntNo = 0;
            string reportPath = string.Empty;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "DeferredPaymentsReport");

            if (reportPage.Equals(string.Empty))
            {
                //int arnIntNo = arn.AddAuthReportName(autIntNo, "DepositSlip.rpt", "DepositSlip", "system");
                int arnIntNo = arn.AddAuthReportName(autIntNo, "DeferredPaymentsReport.rpt", "DeferredPaymentsReport", "system", "");
                reportPage = "DeferredPaymentsReport.rpt";
            }

            reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "DeferredPaymentsReport");

            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                //dls 081117 - we can only check that the template path exists if there is actually a template!
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            _reportDoc.Load(reportPath);

            //if (Request.QueryString["deposit"] != null)
            if (Request.QueryString["NotIntNo"] != null)
            {
                notIntNo = Convert.ToInt32(Request.QueryString["NotIntNo"]);

                int viewAutIntNo = (Request.QueryString["AutIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString()));

                // Get data and populate dataset
                SqlConnection con = new SqlConnection(Application["constr"].ToString());
                SqlCommand com = new SqlCommand("DeferredPaymentsReport", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = viewAutIntNo;
                com.Parameters.Add("@NotIntNo", SqlDbType.Int, 4).Value = notIntNo;
                //com.Parameters.Add("@DSDate", SqlDbType.DateTime).Value = dtDate;

                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                da.Dispose();

                //set up new report based on .rpt report class
                _reportDoc.SetDataSource(ds.Tables[0]);
                ds.Dispose();

                // Export the pdf file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
                _reportDoc.Dispose();

                // Stuff the PDF file into rendering stream first clear everything dynamically created and just send PDF file
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }

        }    
    }
}