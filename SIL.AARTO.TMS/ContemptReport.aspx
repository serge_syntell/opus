<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.ContemptReport" Codebehind="ContemptReport.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%----%>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        <%----%>
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="upd" runat="server"><ContentTemplate>
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                        <table id="tblControls" border="0" class="NormalBold" style="width: 629px">
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" Width="197px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblCourt.Text %>"></asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlCourt" runat="server" CssClass="Normal" Width="197px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblYear.Text %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtYear" runat="server" CssClass="Normal"></asp:TextBox>
                                    <asp:RangeValidator ID="RangeValidator1" runat="server" 
                                        ControlToValidate="txtYear" ErrorMessage="<%$Resources:reqYear.ErrorMsg %>" 
                                        MaximumValue="2050" MinimumValue="2005" Display="Dynamic"></asp:RangeValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                        ControlToValidate="txtYear" ErrorMessage="<%$Resources:reqYear.ErrorMsg %>" 
                                        ValidationExpression="\d{4}"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 26px">
                                    <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblMonth.Text %>"></asp:Label>
                                </td>
                                <td style="height: 26px">
                                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="Normal" Width="197px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblDateRange.Text %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkDateRange" runat="server" CssClass="Normal" OnCheckedChanged="chkDateRange_CheckedChanged"    AutoPostBack="true" ></asp:CheckBox></td>
                            </tr>
                              <tr>
                                <td style="height: 43px">
                                    <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblDateFrom.Text %>"></asp:Label>
                                </td>
                                <td style="height: 43px">
                                
                                    <asp:TextBox runat="server" ID="wdcDateFrom" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="wdcDateFrom" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>                
                                    </td>
                            </tr>
                             <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="<%$Resources:lblDateTo.Text %>"></asp:Label>
                                </td>
                                <td>
                             
                                                               <asp:TextBox runat="server" ID="wdcDateTo" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                TargetControlID="wdcDateTo" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>  
                                    </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right">
                                    <asp:Button ID="btnView" runat="server" CssClass="NormalButton" OnClick="btnView_Click"
                                        Text="<%$Resources:btnView.Text %>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    </ContentTemplate> </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
