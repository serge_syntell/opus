using System;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// The starting point for ticket payment receipt
    /// </summary>
    public partial class PublicHolidays : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private List<PublicHoliday> holidays = null;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "TemplPublicHolidaysate.aspx";
        //protected string thisPage = "Public Holidays";

        // Constant
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.SetPublicHolidays();

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlDetails.Visible = false;
                this.pnlPaste.Visible = false;
            }
        }

        private void SetPublicHolidays()
        {
            int year = DateTime.Today.Year;
            if (this.ViewState["Year"] != null)
                year = (int)this.ViewState["Year"];
            else
                this.ViewState.Add("Year", year);

            this.lnkNext.Text = string.Format("{0} >>", year + 1);
            this.lnkPrevious.Text = string.Format("<< {0}", year - 1);
            this.lblCurrentYear.Text = year.ToString();

            this.calJanuary.VisibleDate = new DateTime(year, 1, 1);
            this.calFebruary.VisibleDate = new DateTime(year, 2, 1);
            this.calMarch.VisibleDate = new DateTime(year, 3, 1);
            this.calApril.VisibleDate = new DateTime(year, 4, 1);
            this.calMay.VisibleDate = new DateTime(year, 5, 1);
            this.calJune.VisibleDate = new DateTime(year, 6, 1);
            this.calJuly.VisibleDate = new DateTime(year, 7, 1);
            this.calAugust.VisibleDate = new DateTime(year, 8, 1);
            this.calSeptember.VisibleDate = new DateTime(year, 9, 1);
            this.calOctober.VisibleDate = new DateTime(year, 10, 1);
            this.calNovember.VisibleDate = new DateTime(year, 11, 1);
            this.calDecember.VisibleDate = new DateTime(year, 12, 1);

            PublicHolidaysDB db = new PublicHolidaysDB(this.connectionString);
            this.holidays = db.GetHolidays(year);
        }

         

        protected void calJanuary_SelectionChanged(object sender, EventArgs e)
        {
            Calendar cal = sender as Calendar;

            DateTime dt = cal.SelectedDate;

            if (this.calApril != cal)
                this.calApril.SelectedDates.Clear();
            if (this.calAugust != cal)
                this.calAugust.SelectedDates.Clear();
            if (this.calDecember != cal)
                this.calDecember.SelectedDates.Clear();
            if (this.calFebruary != cal)
                this.calFebruary.SelectedDates.Clear();
            if (this.calJanuary != cal)
                this.calJanuary.SelectedDates.Clear();
            if (this.calJuly != cal)
                this.calJuly.SelectedDates.Clear();
            if (this.calJune != cal)
                this.calJune.SelectedDates.Clear();
            if (this.calMarch != cal)
                this.calMarch.SelectedDates.Clear();
            if (this.calMay != cal)
                this.calMay.SelectedDates.Clear();
            if (this.calNovember != cal)
                this.calNovember.SelectedDates.Clear();
            if (this.calOctober != cal)
                this.calOctober.SelectedDates.Clear();
            if (this.calSeptember != cal)
                this.calSeptember.SelectedDates.Clear();

            PublicHolidaysDB db = new PublicHolidaysDB(this.connectionString);
            PublicHoliday holiday = db.GetHoliday(dt);

            this.txtDate.Text = (holiday.Date.HasValue ? holiday.Date.Value.ToString(DATE_FORMAT) : dt.ToString(DATE_FORMAT));
            this.txtDescription.Text = holiday.Description;
            this.hidId.Value = holiday.ID.ToString();
            this.btnDelete.Visible = !holiday.IsNew;

            this.pnlDetails.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DateTime dt;


            //SD: 20081208 - test for null date
            if((this.txtDate.Text.Trim().Length ==  0) || (this.txtDate.Text.Trim().Length ==  0))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<ul>");
                sb.Append("<li> " + (string)GetLocalResourceObject("lblError.Text") + "</li>");
                sb.Append("</ul>");
                lblError.Text = sb.ToString();
                lblError.Visible = true;
                return;
            }
            
            if (!DateTime.TryParse(this.txtDate.Text, out dt))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                this.lblError.Visible = true;
                return;
            }
            this.lblError.Text = string.Empty;

            PublicHoliday holiday = new PublicHoliday();
            holiday.Date = dt;
            holiday.Description = this.txtDescription.Text.Trim();
            holiday.ID = int.Parse(this.hidId.Value);

            PublicHolidaysDB db = new PublicHolidaysDB(this.connectionString);
            db.UpdateHoliday(holiday, this.login);

            this.hidId.Value = holiday.ID.ToString();
            this.btnDelete.Visible = true;
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PublicHolidays, PunchAction.Change);  

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.hidId.Value);

            PublicHolidaysDB db = new PublicHolidaysDB(this.connectionString);
            if (db.DeleteHoliday(id))
            {
                this.pnlDetails.Visible = false;
                this.SetPublicHolidays();
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PublicHolidays, PunchAction.Delete); 
            }
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            int year = (int)this.ViewState["Year"];
            this.ViewState["Year"] = ++year;
            this.SetPublicHolidays();
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            int year = (int)this.ViewState["Year"];
            this.ViewState["Year"] = --year;
            this.SetPublicHolidays();
        }

        protected void Calendar_DayRender(object sender, DayRenderEventArgs e)
        {
            if (this.holidays == null)
                return;

            if (e.Day.IsSelected)
                return;

            foreach (PublicHoliday holiday in this.holidays)
            {
                if (DateTime.Compare(holiday.Date.Value, e.Day.Date) == 0)
                    e.Cell.CssClass = "NormalButton";
            }
        }

        protected void btnCopy_Click(object sender, EventArgs e)
        {
            this.pnlCalendar.Visible = false;
            this.pnlPaste.Visible = true;

            int year = (int)this.ViewState["Year"];
            for (int i = year + 1; i < year + 5; i++)
                this.cmbPasteYear.Items.Add(i.ToString());
        }

        protected void btnPaste_Click(object sender, EventArgs e)
        {
            PublicHolidaysDB db = new PublicHolidaysDB(this.connectionString);
            int yearTo=int.Parse(this.cmbPasteYear.SelectedValue);
            db.CopyHolidays((int)this.ViewState["Year"], yearTo, this.login);

            this.ViewState["Year"] = yearTo;
            this.SetPublicHolidays();

            this.pnlCalendar.Visible = true;
            this.pnlPaste.Visible = false;
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PublicHolidays, PunchAction.Add); 
        }

        protected void lnkBack_Click(object sender, EventArgs e)
        {
            this.pnlCalendar.Visible = true;
            this.pnlPaste.Visible = false;
        }

    }
}
