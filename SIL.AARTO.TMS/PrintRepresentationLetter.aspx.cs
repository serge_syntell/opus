﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class PrintRepresentationLetter : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "PrintRepresentationLetter.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Print Representation Letters";

        private const string DATE_FORMAT = "yyyy-MM-dd";
        protected bool IsIBMPrinter = false;
        protected bool hasDisabledPrint = false;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;
            //int 

            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            this.pnlGeneral.Visible = true;
            this.dgPrintrun.Visible = true;

            if (ddlSelectLA.SelectedIndex > -1)
            {
                this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            }
            else
            {
                this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            }

            AuthorityDB authDB = new AuthorityDB(this.connectionString);
            AuthorityDetails authDetails = authDB.GetAuthorityDetails(autIntNo);

            // 20120815 Nick added for report engine
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            IsIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

            if (!Page.IsPostBack)
            {

                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                this.chkShowAll.Checked = false;

                this.PopulateAuthorities(this.autIntNo);

                // Check all remote image file server 
                bool isAllImageFileServerAccessible = false;
                if (Session["AllImageFileServerAccessible"] != null)
                {
                    isAllImageFileServerAccessible = (bool)Session["AllImageFileServerAccessible"];
                }
                if (isAllImageFileServerAccessible == false)
                {
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorScriptIFS", "alter('Sorry, Image file server unaccessible, you can't do anything for this page. Please check the connection first!');", true);
                    //Server.Transfer("Login.aspx?Login=invalid");
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    return;
                }
                else
                {
                    this.BindGrid(this.autIntNo);

                    this.lblInstruct.Visible = false;
                    if (ddlSelectLA.SelectedIndex > -1)
                    {
                        this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                        Session["printAutIntNo"] = autIntNo;
                    }
                }
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorities(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        public void BindGrid(int autIntNo)
        {
            string showAll = chkShowAll.Checked ? "Y" : "N";
            //showAll = "Y";
            hasDisabledPrint = false;
            RepresentationDB represent = new RepresentationDB(this.connectionString);

            int totalCount = 0;
            DataSet dsPrintList = represent.RepresentationGetPrintBatchFile(autIntNo, showAll, dgPrintrunPager.PageSize, dgPrintrunPager.CurrentPageIndex - 1, out totalCount);
            dgPrintrun.DataSource = dsPrintList.Tables[0];
            dgPrintrun.DataKeyField = "PrintFileName";
            dgPrintrunPager.RecordCount = totalCount;

            try
            {
                dgPrintrun.DataBind();
            }
            catch
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.DataBind();
            }

            if (dgPrintrun.Items.Count == 0)
            {
                dgPrintrun.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError_Print.Visible = false;
            }
            else
            {
                if (hasDisabledPrint)
                {
                    lblError_Print.Text = (string)GetLocalResourceObject("errorMsg_Print");
                    lblError_Print.Visible = true;
                }
                else
                {
                    lblError_Print.Visible = false;
                }
                dgPrintrun.Visible = true;
                lblError.Visible = false;
            }
            this.lblInfo.Visible = false;
        }


        protected void dgPrintrun_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            dgPrintrun.SelectedIndex = e.Item.ItemIndex;

            if (dgPrintrun.SelectedIndex > -1)
            {
                string printFileName = dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString();

                if (e.CommandName == "Select")
                {
                    SetChargeStatus(printFileName);
                }
            }
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            Session["printAutIntNo"] = autIntNo;
            dgPrintrunPager.RecordCount = 0;
            dgPrintrunPager.CurrentPageIndex = 1;
            BindGrid(this.autIntNo);
        }


        private void SetChargeStatus(string printFileName)
        {
            RepresentationDB represent = new RepresentationDB(this.connectionString);
            int noOfRows = represent.UpdateRepresentationLetterPrinted(this.autIntNo, printFileName);

            if (noOfRows < 1)
                this.lblInfo.Text = string.Format((string)this.GetLocalResourceObject("lblInfo.Text1"), noOfRows);
            else
            {
                this.lblInfo.Text = (string)this.GetLocalResourceObject("lblInfo.Text2");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintRepresentationLetter, PunchAction.Change);  

            }

            this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            this.Session["printAutIntNo"] = autIntNo;
            //Seawen 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            //dgPrintrunPager.RecordCount = 0;
            //dgPrintrunPager.CurrentPageIndex = 1;
            this.BindGrid(autIntNo);

            this.lblInfo.Visible = true;

        }

        protected void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            //Seawen 2013-09-13 show on current page index
            //dgPrintrunPager.RecordCount = 0;
            //dgPrintrunPager.CurrentPageIndex = 1;
            BindGrid(autIntNo);
        }

        //protected void dgPrintrun_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        //{
        //    dgPrintrun.CurrentPageIndex = e.NewPageIndex;

        //    autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
        //    BindGrid(autIntNo);
        //}

        protected void dgPrintrun_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.DataItem == null)
                return;
            LinkButton btnUpdate = (LinkButton)e.Item.FindControl("btnUpdate");
            HyperLink hLink = (HyperLink)e.Item.FindControl("hlPrint");
            DataRowView row = (DataRowView)e.Item.DataItem;
            //DateTime printDate = DateTime.MinValue;
            if (row["RepPrintedFlag"] != null && Convert.ToBoolean(row["RepPrintedFlag"].ToString()))
            {
                //added to disable update button if the file has not been printed
                btnUpdate.Enabled = false;
            }
            else
                btnUpdate.Enabled = true;

            string printFileName = row["PrintFileName"].ToString();
            if (printFileName.IndexOf("RepDecision") >= 0)
            {
                if (btnUpdate.Enabled)
                {
                    hLink.NavigateUrl = "Representation_LetterViewer.aspx?PrintFileName=" + printFileName + "&PrintFlag=N&printType=PrintRepresentationLetter";
                }
                else
                {
                    hLink.NavigateUrl = "Representation_LetterViewer.aspx?PrintFileName=" + printFileName + "&PrintFlag=Y&printType=PrintRepresentationLetter";
                }
            }
            else
            {
                if (btnUpdate.Enabled)
                {
                    hLink.NavigateUrl = "RepresentationInsufficientDetails_LetterViewer.aspx?PrintFileName=" + printFileName + "&PrintFlag=N&printType=PrintRepresentationLetter";
                }
                else
                {
                    hLink.NavigateUrl = "RepresentationInsufficientDetails_LetterViewer.aspx?PrintFileName=" + printFileName + "&PrintFlag=Y&printType=PrintRepresentationLetter";
                }
            }

            if (IsIBMPrinter && (printFileName.IndexOf("RepAckReg_") >= 0 || printFileName.IndexOf("RepDec_") >= 0 || printFileName.IndexOf("RepInsufDetails_") >= 0))
            {
                hLink.Enabled = false;
                if (btnUpdate.Enabled) btnUpdate.Enabled = false;
                hasDisabledPrint = true;
            }

        }

        protected void dgPrintrunPager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo);
        }

    }
}