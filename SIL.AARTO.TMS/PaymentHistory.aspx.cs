using System;
using System.Data.SqlClient;
using System.Web.UI;
using Stalberg.TMS.Data.Util;
using System.Data;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using System.Web.UI.WebControls;

namespace Stalberg.TMS
{
    public partial class PaymentHistory : Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo;
        private int ugIntNo;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "PaymentHistory.aspx";
        //protected string thisPage = "Payment History and Performance Report";


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateAuthorities();
                txtYear.Text = DateTime.Today.Year.ToString();
                //DrawPieChart();
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");


            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            //pnlChart.Visible = false;

            if (this.ddlAuthority.SelectedValue.Equals(string.Empty))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            this.lblError.Text = string.Empty;

            int selectedAutIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            // Helper_Web.BuildPopup(this, "PaymentHistory_Viewer.aspx", "AutIntNo", selectedAutIntNo.ToString(), "Year", this.txtYear.Text);

            //ReportingUtil util = new ReportingUtil(this.connectionString, this.login);
            //String url = util.GetReportUrl(Request.Url, "PaymentHistory");
            //PageToOpen pto = new PageToOpen(this.Page, url);

            //Edge 2012-05-24 added
            PageToOpen pto = new PageToOpen(this.Page, "PaymentHistoryViewer.aspx");
            pto.AddParameter("AutIntNo", selectedAutIntNo);
            pto.AddParameter("Year", string.IsNullOrEmpty(this.txtYear.Text) ? "9999" : this.txtYear.Text);
            Helper_Web.BuildPopup(pto);
            //PunchStats805806 enquiry PaymentHistoryReport
        }

      }
}


