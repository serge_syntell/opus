using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that enables a user to verify that a film has been recorded as having been taken in the right location
    /// </summary>
    public partial class Verification_Film : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private int filmIntNo = 0;
        private int mtrIntNo = 0;
        private int processType = int.MinValue;

        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string _ISASD = "none";
        protected string thisPageURL = "Verification_Film.aspx";
        protected int asdInvalidRejIntNo = 0;

        protected FrameList frames = null;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;
            int userAccessLevel = userDetails.UserAccessLevel;

            //int 

            if (Session["mtrIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No metro has been selected - please login correctly";
                return;
            }

            if (Session["autIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No authority has been selected - please login correctly";
                return;
            }
            else if (Session["filmIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No film has been selected - please login correctly";
                return;
            }

            //dls 090618 - add this into the session variable, so that we only ahve to do it once, not for every frame!
            if (Session["asdInvalidRejIntNo"] == null)
            {
                this.asdInvalidRejIntNo = GetASDInvalidRejIntNo();
                Session.Add("asdInvalidRejIntNo", this.asdInvalidRejIntNo);
            }
            else
            {
                this.asdInvalidRejIntNo = (int)Session["asdInvalidRejIntNo"];
            }

            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            filmIntNo = Convert.ToInt32(Session["filmIntNo"]);
            mtrIntNo = Convert.ToInt32(Session["mtrIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            HtmlMeta metaKeywords = new HtmlMeta();
            metaKeywords.Attributes.Add("name", "Keywords");
            metaKeywords.Attributes.Add("content", keywords);
            Page.Header.Controls.Add(metaKeywords);

            HtmlMeta metaDescription = new HtmlMeta();
            metaDescription.Attributes.Add("name", "Description");
            metaDescription.Attributes.Add("content", description);
            Page.Header.Controls.Add(metaDescription);

            frames = (FrameList)this.Session["FrameList"];

            if (!Page.IsPostBack)
            {
                ddlCamera.Enabled = false;
                ddlCourt.Enabled = false;
                ddlLocation.Enabled = false;
                ddlOfficer.Enabled = false;
                ddlRoadType.Enabled = false;
                ddlSpeed.Enabled = false;

                btnContinue.Visible = false;
                btnNo.Visible = false;

                if (autIntNo > 0)
                {
                    this.PopulateOfficers();
                    this.PopulateCourts();
                    this.PopulateSpeedZones();
                    this.PopulateRoadTypes();
                    this.PopulateLocations();
                    this.PopulateCameras();

                    this.LoadFilmData();
                }

                btnAccept.Attributes.Add("onclick", "return confirm('Are you sure you want to accept these settings for this film?');");
            }
        }

        private void LoadFilmData()
        {
            btnAccept.Enabled = false;

            FrameDB frame = new FrameDB(connectionString);
            FrameDetails frameDet = frame.GetFrameDetailsForFilm(filmIntNo);

            //select correct values from drop down lists
            ddlCourt.SelectedIndex = ddlCourt.Items.IndexOf(ddlCourt.Items.FindByValue(frameDet.CrtIntNo.ToString()));
            ddlLocation.SelectedIndex = ddlLocation.Items.IndexOf(ddlLocation.Items.FindByValue(frameDet.LocIntNo.ToString()));
            ddlCamera.SelectedIndex = ddlCamera.Items.IndexOf(ddlCamera.Items.FindByValue(frameDet.CamIntNo.ToString()));
            ddlRoadType.SelectedIndex = ddlRoadType.Items.IndexOf(ddlRoadType.Items.FindByValue(frameDet.RdTIntNo.ToString()));
            ddlSpeed.SelectedIndex = ddlSpeed.Items.IndexOf(ddlSpeed.Items.FindByValue(frameDet.SzIntNo.ToString()));
            ddlOfficer.SelectedIndex = ddlOfficer.Items.IndexOf(ddlOfficer.Items.FindByValue(frameDet.TOIntNo.ToString()));

            if (frameDet.ASD2ndCameraIntNo != 0)
            {
                _ISASD = "";

                ddlCamera.SelectedIndex = ddlCamera.Items.IndexOf(ddlCamera.Items.FindByValue(frameDet.LCSCameraIntNo1.ToString()));
                ddlCamera2.SelectedIndex = ddlCamera.Items.IndexOf(ddlCamera.Items.FindByValue(frameDet.LCSCameraIntNo2.ToString()));

                tbSectionName.Text = frameDet.SectionName;
                tbSectionCode.Text = frameDet.SectionCode;
                tbDistance.Text =  frameDet.ASDSectionDistance.ToString();

                btnChange.Visible = false;

                if (frameDet.LCSActive)
                {
                    btnAccept.Enabled = true;
                    btnAccept.ToolTip = string.Empty;
                }
                else
                {
                    btnAccept.Enabled = false;
                    btnAccept.ToolTip = "The selected location camera section details are invalid";
                }
            }
            else
            {
                btnChange.Visible = true;
                _ISASD = "none";
                btnAccept.Enabled = true;
            }

            FilmDB film = new FilmDB(connectionString);
            FilmDetails filmDet = film.GetFilmDetails(filmIntNo);

            this.lblFilmNo.Text = filmDet.FilmNo;

            AuthorityDB auth = new AuthorityDB(connectionString);
            AuthorityDetails authDet = auth.GetAuthorityDetails(autIntNo);

            this.lblAuthority.Text = authDet.AutName;

            // Image Viewer Control
            this.imageViewer1.FilmNo = filmDet.FilmNo;
            this.imageViewer1.FrameIntNo = frames.Current;
            this.imageViewer1.ScanImageIntNo = 0;
            this.imageViewer1.ImageType = "A";
            this.imageViewer1.FilmIntNo = this.filmIntNo;
            this.imageViewer1.ShowPreView = 0;
            this.imageViewer1.Phase = this.Session["cvPhase"] == null ? 1 : Convert.ToInt32(this.Session["cvPhase"]);
            this.imageViewer1.Initialise();

            string strURLs = imageViewer1.GetParameters();

            ScriptManager.RegisterStartupScript(udpFrame,
                    udpFrame.GetType(), "UpdatePanelChangeImageViewer", "LoadImage(" + strURLs + ");", true);

            //btnAccept.Enabled = true;
        }

        private void PopulateCameras()
        {
            Stalberg.TMS.CameraDB camList = new Stalberg.TMS.CameraDB(connectionString);
            
            SqlDataReader reader = camList.GetCameraList();
            ddlCamera.DataSource = reader;
            ddlCamera.DataValueField = "CamIntNo";
            ddlCamera.DataTextField = "CamSerialNo";
            ddlCamera.DataBind();

            reader.Close();

            reader = camList.GetCameraList();
            ddlCamera2.DataSource = reader;
            ddlCamera2.DataValueField = "CamIntNo";
            ddlCamera2.DataTextField = "CamSerialNo";
            ddlCamera2.DataBind();

            reader.Close();

            
        }

        protected void PopulateOfficers()
        {
            TrafficOfficerDB officer = new TrafficOfficerDB(connectionString);
            
            SqlDataReader reader = officer.GetTrafficOfficerList(mtrIntNo);
            ddlOfficer.DataSource = reader;
            ddlOfficer.DataValueField = "TOIntNo";
            ddlOfficer.DataTextField = "TODescr";
            ddlOfficer.DataBind();

            reader.Close();
        }

        protected void PopulateLocations()
        {
            LocationDB location = new LocationDB(connectionString);
            
            SqlDataReader reader = location.GetLocationList(autIntNo, "", "LocDescr");


            ddlLocation.DataSource = reader;
            ddlLocation.DataValueField = "LocIntNo";
            ddlLocation.DataTextField = "LocDescr";
            ddlLocation.DataBind();

            reader.Close();
        }

        protected void PopulateSpeedZones()
        {
            SpeedZoneDB speed = new SpeedZoneDB(connectionString);

            SqlDataReader reader = speed.GetSpeedZoneList();


            ddlSpeed.DataSource = reader;
            ddlSpeed.DataValueField = "SZIntNo";
            ddlSpeed.DataTextField = "SZSpeed";
            ddlSpeed.DataBind();

            reader.Close();
        }

        protected void PopulateRoadTypes()
        {
            RoadTypeDB road = new RoadTypeDB(connectionString);

            SqlDataReader reader = road.GetRoadTypeList();


            ddlRoadType.DataSource = reader;
            ddlRoadType.DataValueField = "RdTIntNo";
            ddlRoadType.DataTextField = "RdTypeDescr";
            ddlRoadType.DataBind();

            reader.Close();
        }


        protected void PopulateCourts()
        {
            CourtDB court = new CourtDB(connectionString);
            
            SqlDataReader reader = court.GetAuth_CourtListByAuth(autIntNo);
            ddlCourt.DataSource = reader;
            ddlCourt.DataValueField = "CrtIntNo";
            ddlCourt.DataTextField = "CrtDetails";
            ddlCourt.DataBind();

            reader.Close();
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            this.UpdateAllFrames("N");
        }

        private void UpdateAllFrames(string proceed)
        {
            //user is happy with settings - update all frames according to current values
            //int toIntNo = Convert.ToInt32(ddlOfficer.SelectedValue);
            int locIntNo = Convert.ToInt32(ddlLocation.SelectedValue);
            int camIntNo = Convert.ToInt32(ddlCamera.SelectedValue);
            //int szIntNo = Convert.ToInt32(ddlSpeed.SelectedValue);
            //int rdtIntNo = Convert.ToInt32(ddlRoadType.SelectedValue);
            //int crtIntNo = Convert.ToInt32(ddlCourt.SelectedValue);
            bool isNaTISLast = (bool)this.Session["NaTISLast"];

            FrameDB frame = new FrameDB(connectionString);

            //dls 070818 - have removed the road type, court and speedzone from this update, as they need to come from the location, not the user
            //           - officer cannot be udpates, as there may be multiple officers per film

            //dls 070212 - officer still passed thru to stored proc, but no longer used for updating - may be multiple officers on a film
            //int success = frame.UpdateAllFramesForFilm(filmIntNo, locIntNo, camIntNo, toIntNo,
            //    rdtIntNo, szIntNo, crtIntNo, proceed, loginUser);
            int success = frame.UpdateAllFramesForFilm(filmIntNo, locIntNo, camIntNo, proceed, loginUser);

            if (success == 0)
            {
                Session["frameIntNo"] = 0;
                Session["cvPhase"] = 1;
                
                if (Session["processType"] != null)
                    this.processType = int.Parse(Session["processType"].ToString());

                if (this.frames.NoFrames)
                {
                    Response.Redirect("Verification_PreFilm.aspx?process=" + this.processType);
                }
                else if (isNaTISLast)
                {
                    //need to add this here, otherwise the phase is incorrect    
                    Response.Redirect("Adjudication_Frame.aspx");
                }
                else
                {
                    // Move onto next stage of the Verification process
                    Response.Redirect("Verification_Frame.aspx");
                }
            }
            else if (success == -1)
            {
                lblError.Visible = true;
                lblError.Text = "If you proceed with the selected values, some of the frames will contain invalid/missing offence codes. Are you sure you wish to continue?";
                btnContinue.Visible = true;
                btnNo.Visible = true;
                btnAccept.Enabled = false;
                btnChange.Enabled = false;
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "Unable to update frames";
                btnContinue.Visible = false;
                btnNo.Visible = false;
            }
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            //dls 070818 - only camera and court can be changed.
            ddlCamera.Enabled = true;
            //ddlCourt.Enabled = true;
            ddlLocation.Enabled = true;
            //ddlOfficer.Enabled = true;
            //ddlRoadType.Enabled = true;
            //dls 070612 - they should not be allowed to change the speed independently of the location
            //ddlSpeed.Enabled = true;
        }

        private int GetASDInvalidRejIntNo()
        {
            RejectionDB rejDB = new RejectionDB(this.connectionString);
            //add ASD speed rejection reason
            string asdInvalidSetup = "ASD Camera Setup Invalid";

            return rejDB.UpdateRejection(0, asdInvalidSetup, "N", this.loginUser, 0, "Y");
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            bool isNaTISLast = (bool)Session["NaTISLast"];
            bool adjAt100 = (bool)Session["adjAt100"];

            int FrameStatus = 999;
            if (adjAt100)
            {
                if (isNaTISLast)
                    FrameStatus = 800;
                else
                    FrameStatus = 500;
            }

            string errMessage = string.Empty;

            FilmDB filmDB = new FilmDB(this.connectionString);
            int success = filmDB.RejectFilm(filmIntNo, this.asdInvalidRejIntNo, FrameStatus, loginUser, ref errMessage);

            if (Session["processType"] != null)
                this.processType = int.Parse(Session["processType"].ToString());

            if (success > 0)
            {
                Response.Redirect("Verification_PreFilm.aspx?process=" + this.processType);
            }
            else
            {
                LoadFilmData();
                lblError.Visible = true;

                switch (success)
                {
                    case -1:
                        errMessage = "Error updating frame";
                        break;
                    case -2:
                        errMessage = "Error updating frame_offence";
                        break;
                    case -3:
                        errMessage = "Error updating film";
                        break;
                }

                lblError.Text = "Unable to reject film - " + errMessage;
                btnContinue.Visible = false;
                btnNo.Visible = false;
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            btnContinue.Visible = false;
            btnNo.Visible = false;

            UpdateAllFrames("Y");
        }
        protected void btnNo_Click(object sender, EventArgs e)
        {
            btnContinue.Visible = false;
            btnNo.Visible = false;
            lblError.Visible = false;
            btnAccept.Enabled = true;
            btnChange.Enabled = true;
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAccept.Enabled = false;

            //need to pick up new road type, court, speed zone for changed location
            int locIntNo = Convert.ToInt32(ddlLocation.SelectedValue);

            LocationDB loc = new LocationDB(connectionString);
            LocationDetails locDetails = loc.GetLocationDetails(locIntNo);

            ddlRoadType.SelectedIndex = ddlRoadType.Items.IndexOf(ddlRoadType.Items.FindByValue(locDetails.RdTIntNo.ToString()));
            ddlSpeed.SelectedIndex = ddlSpeed.Items.IndexOf(ddlSpeed.Items.FindByValue(locDetails.SZIntNo.ToString()));

            btnAccept.Enabled = true;
        }
}
}
