<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.SummonsServingStatus" Codebehind="SummonsServingStatus.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body style="margin: 0, 0, 0, 0" background="<%=backgroundImage %>">
    <form id="Form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="height: 10%; width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table style="height: 85%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center" style="width: 182px">
                <img height="26" src="images/1x1.gif" width="167" alt="1x1" />
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" style="width: 100%" colspan="1">
                <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                    <p style="text-align: center;">
                        <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                    <p>
                        &nbsp;</p>
                </asp:Panel>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Label runat="server" ID="labelError" CssClass="NormalRed" />
                        <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                            <table id="tblControls" border="0" class="Normal" >
                                <tr>
                                    <td colspan="2" valign="top">
                                        <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                                    </td>
                                </tr>
                                 <tr>
                                         <td valign="top">
                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblAuthority.Text %>" CssClass="NormalBold"
                                            Visible="true"></asp:Label>
                                    </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" AutoPostBack="True"
                                                Width="197px" OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:Label ID="lblSelectCourt" runat="server" Text="<%$Resources:lblSelectCourt.Text %>" CssClass="NormalBold"
                                            Visible="true"></asp:Label>
                                    </td>
                                    <td> 
                                        <asp:DropDownList ID="ddlCourt" runat="server" AutoPostBack="True" 
                                            CssClass="Normal" DataTextField="CrtName" DataValueField="CrtIntNo" 
                                            onselectedindexchanged="ddlCourt_SelectedIndexChanged" Visible="true" 
                                            Width="200px">
                                        </asp:DropDownList>
                                        </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;" class="NormalBold">
                                        <asp:Label Text="<%$Resources:CourtDateOptions %>" runat="server"/>:
                                    </td>
                                    <td style="vertical-align: top;">
                                        <asp:RadioButtonList ID="rblDateOption" AutoPostBack="True" 
                                            RepeatDirection="Horizontal" CssClass="Normal" runat="server" 
                                            onselectedindexchanged="rblDateOption_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="<%$Resources:SpecificCourtDate %>"/>
                                            <asp:ListItem Value="1" Text="<%$Resources:CourtDateRange %>"/>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tbody id="tbdSpecific" runat="server">
                                <tr>
                                <td valign="top">
                                    <asp:Label ID="lblCourtRoom" runat="server" CssClass="NormalBold" 
                                        Visible="true" Text="<%$Resources:lblCourtRoom.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCourtRoom" runat="server" AutoPostBack="True" 
                                            CssClass="Normal" Visible="true" Width="200px" 
                                            onselectedindexchanged="ddlCourtRoom_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:Label ID="Label3" runat="server" CssClass="NormalBold" visible=true
                                            text="<%$Resources:lblCourtRoomDates.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCourtRoomDates" runat="server" AutoPostBack="True" 
                                            CssClass="Normal" Visible="true" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                </tbody>
                                <tbody id="tbdRange" runat="server">
                                    <tr>
                                        <td style="vertical-align: top;" class="NormalBold">
                                            <asp:Label Text="<%$Resources:DateRange %>" runat="server"/>:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCrtDateStart" runat="server"/>
                                            <asp:TextBox ID="txtCrtDateEnd" runat="server"/>
                                            <cc1:CalendarExtender ID="calCrtDateStart" TargetControlID="txtCrtDateStart" Format="yyyy-MM-dd" runat="server"/>
                                            <cc1:CalendarExtender ID="calCrtDateEnd" TargetControlID="txtCrtDateEnd" Format="yyyy-MM-dd" runat="server"/>
                                        </td>
                                    </tr>
                                </tbody>
                                <tr>
                                    <td valign="top" colspan="2">
                                        <br />
                                        <asp:Panel ID="Panel2" runat="server" Width="100%" GroupingText="<%$Resources:pnlSelectionOptions.GroupingText %>">
                                            <asp:RadioButtonList ID="rlOptions" runat="server" AutoPostBack="True" RepeatColumns="2"
                                                CssClass="Normal" OnSelectedIndexChanged="rlOptions_SelectedIndexChanged">
                                                <asp:ListItem Value="Served and Unserved" Selected=True Text="<%$Resources:rlOptions.Text %>"></asp:ListItem>
                                                <asp:ListItem Value="Only Served" Text="<%$Resources:rlOptions.Text1 %>"></asp:ListItem>
                                                <asp:ListItem Value="Only Unserved" Text="<%$Resources:rlOptions.Text2 %>"></asp:ListItem>
                                               <%-- <asp:ListItem Value="Withdrawn">Withdrawn from Summons Server</asp:ListItem>--%>
                                                <asp:ListItem Value="Not returned from Clerk Of Court" Text="<%$Resources:rlOptions.Text3 %>"></asp:ListItem>
                                                <asp:ListItem Value="Not returned from Summons Server" Text="<%$Resources:rlOptions.Text4 %>"></asp:ListItem>
                                                <asp:ListItem Value="By Summons Number" Enabled="false" Text="<%$Resources:rlOptions.Text5 %>"></asp:ListItem>
                                            </asp:RadioButtonList>
                                                <br />
                                            </asp:Panel>
                                            <asp:Panel ID="Panel1" runat="server" Width="100%" GroupingText="<%$Resources:pnlOrderBy.Text %>">
                                            <asp:RadioButtonList ID="rlOrderBy" runat="server" AutoPostBack="True" 
                                                CssClass="Normal" >
                                                <asp:ListItem Value="0" Selected=True Text="<%$Resources:rlOrderByItem.Text %>"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="<%$Resources:rlOrderByItem.Text1 %>"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="<%$Resources:rlOrderByItem.Text2 %>"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="<%$Resources:rlOrderByItem.Text3 %>"></asp:ListItem>
                                            </asp:RadioButtonList>
                                                <br />
                                            </asp:Panel>
                                        <br />
                                        <table><tr>
                                        <td style="height: 26px; width: 147px;">
                                            <asp:Label runat="server" ID="lblPartialTicketNo"  CssClass="NormalBold" Width="139px" Visible=false Text="<%$Resources:lblPartialTicketNo.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 26px">
                                            <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" Visible=false />
                                        </td>
                                        <td style="width: 3px; height: 26px;" align="center">
                                            <asp:Label runat="server"  CssClass="NormalBold" ID="lblPartialOr" Visible=false Text="<%$Resources:lblOR.Text %>"></asp:Label>
                                        </td>
                                        </tr>
                                         <tr>
                                        <td style="height: 26px; width: 147px;">
                                            <asp:Label runat="server" ID="lblTicketNo"  CssClass="NormalBold" Visible=false Text="<%$Resources:lblTicketNo.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 26px">
                                            <asp:TextBox Enabled="false" BackColor="#efefef" ID="txtTicketNo" runat="server" Visible=false
                                                CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 3px; height: 26px;" align="center">
                                            <asp:Label runat="server"  CssClass="NormalBold" ID="lblOr" Visible=false Text="<%$Resources:lblOR.Text %>"></asp:Label>
                                        </td>
                                    </tr></table>
                                        <asp:Label ID="lblSummonsNo" runat="server" Text="Summons No:" Visible="False" 
                                            CssClass="NormalBold"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtSummonsNo" runat="server" Visible="False" Width="199px" 
                                            CssClass="Normal"></asp:TextBox>
                                        <br />                                       
                                        <asp:Button ID="btnViewReport" runat="server" CssClass="NormalButton" OnClick="btnViewReport_Click"
                                            Text="<%$Resources:btnViewReport.Text %>" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center" style="width: 182px">
            </td>
            <td valign="top" align="left" width="100%">
                &nbsp;
            </td>
        </tr>
    </table>
    <table style="height: 5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%" style="height: 37px">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
