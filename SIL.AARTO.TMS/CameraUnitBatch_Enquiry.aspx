<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.CameraUnitBatch_Enquiry" Codebehind="CameraUnitBatch_Enquiry.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed" /></p>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                        <table border="0" class="Normal">
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblAuthority.Text %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlAuthority" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged"
                                        Width="200px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblCameraUnit.Text %>"></asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlCameraUnit" runat="server" Width="200px" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlCameraUnit_SelectedIndexChanged" /></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:CheckBox ID="chkOutstanding" runat="server" Text="<%$Resources:chkOutstanding.Text %>"
                                        Checked="true" AutoPostBack="True" OnCheckedChanged="chkOutstanding_CheckedChanged" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:GridView ID="grdContraventionFiles" runat="server" AllowPaging="False" CssClass="Normal"
                            ShowFooter="True" AutoGenerateColumns="False" CellPadding="1">
                            <FooterStyle CssClass="CartListHead" />
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                            <Columns>
                                <asp:BoundField DataField="FileName" HeaderText="<%$Resources:grdContraventionFiles.HeaderText %>">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="StartNo" HeaderText="<%$Resources:grdContraventionFiles.HeaderText1 %> ">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$Resources:grdContraventionFiles.HeaderText2 %> ">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblSentDate" runat="server" Text='<%#  Eval("FileDate") != System.DBNull.Value ? ((DateTime)Eval("FileDate")).ToString("yyyy-MM-dd HH:mm"):"" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdContraventionFiles.HeaderText3 %> ">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ResponseDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblResponseDate" runat="server" Text='<%# Eval("ResponseDate") != System.DBNull.Value ? ((DateTime)Eval("ResponseDate")).ToString("yyyy-MM-dd HH:mm"):"" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdContraventionFiles.HeaderText4 %> ">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("AllocationDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAllocationDate" runat="server" Text='<%# Eval("AllocationDate") != System.DBNull.Value ? ((DateTime)Eval("AllocationDate")).ToString("yyyy-MM-dd HH:mm"):"" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NoRecords" HeaderText="<%$Resources:grdContraventionFiles.HeaderText5 %> ">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NoSuccess" HeaderText=" <%$Resources:grdContraventionFiles.HeaderText6 %>">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NoFailed" HeaderText="<%$Resources:grdContraventionFiles.HeaderText7 %> ">
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ReasonFailed" HeaderText="<%$Resources:grdContraventionFiles.HeaderText8 %> " />
                            </Columns>
                        </asp:GridView>
                        <pager:AspNetPager id="grdContraventionFilesPager" runat="server" 
                            showcustominfosection="Right" width="400px" 
                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                              FirstPageText="|&amp;lt;" 
                            LastPageText="&amp;gt;|" 
                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                            CustomInfoStyle="float:right;" PageSize="20" onpagechanged="grdContraventionFilesPager_PageChanged"
                            ></pager:AspNetPager>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
