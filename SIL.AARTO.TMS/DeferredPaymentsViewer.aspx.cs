using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;


namespace Stalberg.TMS
{
    /// <summary>
    /// Allows selection of a notice in order to print out Deferred Payments Report 
    /// </summary>
    public partial class DeferredPaymentsViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string loginUser;
        //private Cashier cashier = null;

        protected string styleSheet;
        protected string backgroundImage;
        protected double myTotal = 0;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "DeferredPaymentsViewer.aspx";
        protected int autIntNo = 0;
        protected int userIntNo = 0;

        //private bool clearPaymentList = false;

        // Constants
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;

            userDetails = (UserDetails)Session["userDetails"];
            userIntNo = Int32.Parse(Session["userIntNo"].ToString());

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //this.pnlPostal.Visible = false;
            //this.pnlDetails.Visible = false;
            //this.btnPrintPostalReceipts.Visible = false;
            //this.btnSuspenseReceipt.Visible = false;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //if (ViewState["ClearPaymentList"] != null)
            //    clearPaymentList = (bool)ViewState["ClearPaymentList"];

            //// Multiple Notices
            //if (Session["NoticesToPay"] == null)
            //{
            //    this.pnlNotices.Visible = false;
            //    this.Session.Add("NoticesToPay", new List<NoticeForPayment>());

            //    this.ClearPaymentList(userIntNo);            
            //}
            //else
            //{
            //    List<NoticeForPayment> notices = (List<NoticeForPayment>)this.Session["NoticesToPay"];
            //    if (notices.Count > 0)
            //    {
            //        this.lblNotices.Text = string.Format("There are {0} other notices already associated with this payment.", notices.Count);
            //        this.pnlNotices.Visible = true;
            //    }
            //    else
            //    {
            //        this.pnlNotices.Visible = false;
            //        //dls 080129 - need to clear any other payments from the temporary table
            //        this.ClearPaymentList(userIntNo);
            //    }
            //}

            // FBJ Added (2006-08-15): Check to see if the current user is a valid cashier
            //BankAccountDB bankAccount = new BankAccountDB(this.connectionString);
            //this.cashier = bankAccount.GetUserBankAccount(int.Parse(Session["userIntNo"].ToString()), autIntNo);
            //if (this.cashier == null)
            //{
            //    this.lblError.Text = "Your User Account has not been configured to receive cash.";
            //    return;
            //}
            //else
            //{
            //    this.Session.Add("Cashier", this.cashier);
            //    this.Session.Add("BAIntNo", cashier.BankAccountIntNo);

            //    // FBJ Added (2006-09-26): If the cashier must count the cash box before receiving anything then redirect them
            //    if (cashier.MustCountCashBox)
            //        Response.Redirect("CashBoxCount.aspx");

            //    // FBJ Added (2007-01-09): IsPostalCapture check added for alternative, batch postal receipt printing
            //    // FBJ Added (2007-05-31): Changed the IsPostalCapture field in the database to an enumeration to handle roadblocks
            //    this.pnlPostal.Visible = (this.cashier.CashBoxType == CashboxType.PostalReceipts);
            //}

            //// FBJ Added (2006-08-14): Errors from the details page are redirected here
            //if (Session["CashReceiptError"] != null)
            //{
            //    this.lblError.Text = Session["CashReceiptError"].ToString();
            //    Session.Remove("CashReceiptError");
            //}
            //this.pnlDetails.Visible = true;
            //if (autIntNo < 1)
            //{
            //    lblError.Text = "Cash receipting for unknown authority not allowed. Please logout and login with a valid authority";
            //    lblError.Visible = true;
            //}

            //if (this.cashier.CashBoxType == CashboxType.PostalReceipts)
            //{
            //    this.btnPrintPostalReceipts.Visible = true;

            //    //temp remove this until we get spec for suspense payments
            //    //this.btnSuspenseReceipt.Visible = true;
            //}

            if (!Page.IsPostBack)
            {
                //PopulateAuthorites(ugIntNo, autIntNo);
                this.TicketNumberSearch1.AutIntNo = this.autIntNo;
            }
        }

        //private void PopulateAuthorites(int ugIntNo, int autIntNo)
        //{
        //    UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

        //    SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
        //    ddlSelectLA.DataSource = reader;
        //    ddlSelectLA.DataValueField = "AutIntNo";
        //    ddlSelectLA.DataTextField = "AutName";
        //    ddlSelectLA.DataBind();
        //    ddlSelectLA.Items.Insert(0, new ListItem("[ All ]", "0"));
        //    ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

        //    reader.Close();
        //}

        //private void ClearPaymentList(int userIntNo)
        //{
        //    if (!clearPaymentList)
        //    {
        //        CashReceiptDB cashReceipt = new CashReceiptDB(this.connectionString);
        //        cashReceipt.RemovePaymentListForUser(userIntNo);
        //        clearPaymentList = true;
        //        ViewState.Add("ClearPaymentList", clearPaymentList);
        //    }
        //    else
        //    {
        //        Session["NoticeAddressDetails"] = null;
        //        Session["TRHIntNo"] = null;
        //        Session["TRHTranDate"] = null;
        //        Session["IsBankPayment"] = null;
        //    }
        //}

        //protected void dgShowCharges_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        //{
        //    switch ((int)(e.Item.ItemType))
        //    {
        //        case (int)ListItemType.Item:
        //            //Calculate total for the field of each row and alternating row.
        //            myTotal += Convert.ToDouble(e.Item.Cells[3].Text);
        //            break;
        //        case (int)ListItemType.AlternatingItem:
        //            //Calculate total for the field of each row and alternating row.
        //            myTotal += Convert.ToDouble(e.Item.Cells[3].Text);
        //            break;
        //        case (int)ListItemType.Header:
        //            break; // do nothing
        //        case (int)ListItemType.Footer:
        //            //Use the footer to display the summary row.
        //            e.Item.Cells[2].Text = "Total Fine";
        //            e.Item.Cells[2].Attributes.Add("align", "left");
        //            e.Item.Cells[3].Attributes.Add("align", "right");
        //            e.Item.Cells[3].Text = myTotal.ToString("c");
        //            break;
        //    }
        //    Session["editFineTotal"] = myTotal;
        //}

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnReport_Click(object sender, EventArgs e)
        {
            string editTicketNo = string.Empty;
            int NotIntNo = 0;
            
            if (this.textTicket.Text.Trim().Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            editTicketNo = this.textTicket.Text.Trim().ToString();

            // Find the associated NotIntNo for the selected Notice/Ticket no.
            NoticeDB notice = new NoticeDB(this.connectionString);

            //NoticeDetails noticedetails = notice.GetNoticeByNumber(this.autIntNo, editTicketNo);
            NotIntNo = notice.GetNoticeByNumber(this.autIntNo, editTicketNo);

            if (NotIntNo == 0)
            {
                lblError.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1") + editTicketNo;
                //return;
            }
            else
            {
                PageToOpen page = new PageToOpen(this, "DeferredPaymentsReportViewer.aspx");
                //page.Parameters.Add(new QSParams("TicketNo", this.textTicket.Text.Trim()));
                page.Parameters.Add(new QSParams("NotIntNo", NotIntNo.ToString()));
                //page.Parameters.Add(new QSParams("IDNo", this.textId.Text.Trim()));
                page.Parameters.Add(new QSParams("AutIntNo", ddlSelectLA.SelectedValue));
                Response.Redirect(page.ToString());
            }
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.textTicket.Text = this.TicketNumberSearch1.TicketNumber;
        }

    }
}
