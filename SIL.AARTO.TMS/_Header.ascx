<%@ Control Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.C_Header" Codebehind="_Header.ascx.cs" %>
<%--
test
    This user control form the header for each page, once logged in

--%>
<%--<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td valign="middle" align="left" colspan="2" class="HomeHead">
            <table>
                <tr>
                    <td>
                        <asp:Image ID="imgLogo" BorderStyle="None" runat="server"></asp:Image><br />
                        <asp:HyperLink ID="hlMainMenuTMS" runat="server" CssClass="menuitem" NavigateUrl="Default.aspx"
                            Width="100px" BorderStyle="None">Home</asp:HyperLink>
                        <asp:HyperLink ID="hlLogout" runat="server" CssClass="menuitem" NavigateUrl="javascript:closeWindow();"
                            Width="100px" BorderStyle="None">Logout</asp:HyperLink>
                        <asp:HyperLink ID="hlMainMenuNotice" runat="server" CssClass="menuitem" NavigateUrl="javascript:openWindow();"
                            Width="150px" BorderStyle="None">Enquiries</asp:HyperLink>
                    </td>
                </tr>
            </table>
        </td>
        <td align="right" class="HomeHead" valign="top">
            <asp:Label ID="lblEnvironment" runat="server" Text="" Visible="false"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Label ID="lblHeader" runat="server" CssClass="ContentHead">The Contravention Solution</asp:Label></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script  language="javascript" type="text/javascript">
    // 600,000 milliseconds if 10 minutes; after which the script logs the user out
   //window.setTimeout("javascript:window.navigate('Login.aspx?Login=logout')", 600000);
   //window.setTimeout("javascript:closeWindow()", 1200000);
   
   // LMZ 18-06-2007 added to close child and parent windows 
   var childsWindow;
   
   function openWindow() {
        childsWindow = window.open('ViewOffence_Search.aspx','NoticeEnquiry','',true);
   }
   
   function closeWindow() {
        //window.navigate('Login.aspx?Login=logout');
        window.location = 'Login.aspx?Login=logout';
        if (childsWindow != null) 
            childsWindow.close();
        //alert(window.name);
        if(window.name == 'NoticeEnquiry')
            window.parent.close();
         if (window.name != "main")  
            window.parent.close();
        window.close();  
   }  
</script>
--%>
