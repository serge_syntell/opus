﻿using System;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Xml.Linq;
using Stalberg.TMS_TPExInt.Components;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.BLL.Utility;

namespace Stalberg.TMS
{
    public partial class SummonsRoadBlock : System.Web.UI.Page
    {
        #region Fields

        // Fields
        protected string styleSheet;
        protected string background;
        protected string title;
        protected string keywords = String.Empty;
        //protected string thisPage = "Block Session - Active Session";
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsRoadBlock.aspx";

        private string connectionString = string.Empty;
        private string login;
        private int autIntNo;
        private DataSet dsSummonsMultiplePrint;
        private int rbsIntNo;
        // Constant
        //private const string SELECT_SUMMONS_SERVER = "[ Select a Summons Server ]";
        //private const string SELECT_COURT = "[ Select a Court ]";
        //private const string SELECT_COURT_ROOM = "[ Select a Court Room ]";
        //private const string NO_COURT_DATES = "[ There are no available court dates ]";
        //private const string SELECT_CofC_OFFICER = "[Select a Clerk of Court or an Officer]";
        //private const string SELECT_SS_COFC = "(Clerk of Court or Traffic Officer)";
        //private const string SUM_STATUS = "Pending";
        //private const string PROCESS_NAME = "TMS-RoadBlockSummons";
        private const int STATUS_SUMMONS_PRINTED = 620;
        private const string AUTHORISED_AUTINTNOS = "Authorised Authority Int No.s";
        private const string DATE_AND_TIME_FORMAT = "yyyy-MM-dd_HH-mm";

        protected double dPercentage = 0.0;
        protected string stationeryType = "CPA5";
        protected string useOriginalFineAmount = "Y";

        //jerry 2011-12-26 add
        private int noDaysForSummonsExpiry;
        private string autCode = string.Empty;

        //Jerry 2014-01-24 add, display process count
        private int totalItemsCount = 0;
        private int processedCount = 0;
        private int currentProcessedCount = 0;
        //Jerry 2014-02-11 invisible court/courtroom DDL, it comes from notice
        private NoticeService noticeService = new NoticeService();
        private ChargeService chargeService = new ChargeService();
        private CourtService courtService = new CourtService();
        private CourtRoomService courtRoomService = new CourtRoomService();
        private NoticeCommentService noticeCommentService = new NoticeCommentService();
        private AccusedService accusedService = new AccusedService();

        //Jerry 2014-03-14 add
        private bool allowS35Rule = false;
        private bool allowNoAogRule = false;

        //Jerry 2014-04-09 add AR_9140 and  AR_9150 rules
        private string dataWashingActived = null;
        private string useRoadblockAddress = null;
        private int roadblockAddrExpiryPeriod = 0;

        #endregion

        #region User Functions

        // Oscar 20101112 - added, get roadblock notice data source
        // 2013-11-29, Oscar added string regNo = null, string surname = null, string initials = null
        //2014-08-14 add errMsg parameter
        private DataSet GetSumNoticeRoadBlockSourceRefresh(string idNumber, string autIntNos, int pageSize, int pageIndex, out int totalCount, ref string errMsg, string regNo = null, string surname = null, string initials = null)
        {
            DataSet source = new DataSet();
            int nTotalCount = 0;
            //if (!string.IsNullOrEmpty(idNumber) && !string.IsNullOrEmpty(autIntNos))
            // 2013-11-29, Oscar changed
            //if ((!string.IsNullOrWhiteSpace(idNumber)
            //     || !string.IsNullOrWhiteSpace(regNo)
            //     || !string.IsNullOrWhiteSpace(surname)
            //     || !string.IsNullOrWhiteSpace(initials))
            //    && !string.IsNullOrEmpty(autIntNos))
            //2014-09-05 Jerry here just searched by IDNumber
            if (!string.IsNullOrWhiteSpace(idNumber)
                && !string.IsNullOrEmpty(autIntNos))
            {
                RoadBlockDB db = new RoadBlockDB(this.connectionString);

                //2014-08-14 add errMsg parameter
                //2014-09-05 Jerry here just searched by IDNumber
                //source = db.GetSumNotWithRoadBlock(0, idNumber, autIntNos, pageSize, pageIndex, out nTotalCount, ref errMsg, regNo, surname, initials);
                source = db.GetSumNotWithRoadBlock(0, idNumber, autIntNos, pageSize, pageIndex, out nTotalCount, ref errMsg);

                ViewState["SumNoticeRoadBlock"] = source;
            }
            totalCount = nTotalCount;
            return source;
        }

        //Jerry 2014-08-14 add errMsg parameter
        private DataSet GetIDNumberDataSetWithRoadBlock(string idNumber, string autIntNos, ref string errMsg, string regNo = null, string surname = null, string initials = null)
        {
            DataSet source = new DataSet();
            if ((!string.IsNullOrWhiteSpace(idNumber)
                 || !string.IsNullOrWhiteSpace(regNo)
                 || !string.IsNullOrWhiteSpace(surname)
                 || !string.IsNullOrWhiteSpace(initials))
                && !string.IsNullOrEmpty(autIntNos))
            {
                RoadBlockDB db = new RoadBlockDB(this.connectionString);

                source = db.GetIDNumberDataSetWithRoadBlock(idNumber, autIntNos, ref errMsg, regNo, surname, initials);

                ViewState["SumNoticeRoadBlock"] = source;
            }

            return source;
        }

        // 2013-11-29, Oscar added string regNo = null, string surname = null, string initials = null
        private DataSet GetSumNoticeRoadBlockSource(string idNumber, string autIntNos, int pageSize, int pageIndex, out int totalCount, string regNo = null, string surname = null, string initials = null)
        {
            DataSet source = new DataSet();
            int nTotalCount = 0;
            string errMsg = string.Empty;

            if (ViewState["SumNoticeRoadBlock"] != null)
            {
                source = ViewState["SumNoticeRoadBlock"] as DataSet;
            }
            if (source.Tables.Count == 0 || source.Tables[0].Rows.Count == 0)
            {
                //2014-08-14 add errMsg parameter
                source = GetSumNoticeRoadBlockSourceRefresh(idNumber, autIntNos, pageSize, pageIndex, out totalCount, ref errMsg, regNo, surname, initials);
            }
            totalCount = nTotalCount;
            return source;
        }

        private void BindData()
        {
            if (hidRBSIntNo.Value == "")
            {
                return;
            }

            //if (txtIdentityNumber.Text == "")
            // 2013-11-29, Oscar changed
            if (string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text)
                && string.IsNullOrWhiteSpace(this.txtQueryRegNo.Text)
                && string.IsNullOrWhiteSpace(this.txtQuerySurname.Text)
                && string.IsNullOrWhiteSpace(this.txtQueryInitials.Text))
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text");
                return;
            }

            chkSelectAll.Enabled = true;
            ViewState["IsSearchClick"] = null;
            ViewState["HasShowOffenceDetail"] = null;
            string autIntNos = (string)this.ViewState[AUTHORISED_AUTINTNOS];
            int rbsIntNo = Convert.ToInt32(hidRBSIntNo.Value);
            //Jerry 2014-09-19 the idNumber value come from the grdIdNumberInfo grid
            //string idNumber = txtIdentityNumber.Text.Trim();
            //string surname = this.txtQuerySurname.Text.Trim();
            //string initials = this.txtQueryInitials.Text.Trim();
            int totalCount = 0;

            string idNumber = string.Empty;
            if (ViewState["IdNumberSelected"] != null)
                idNumber = ViewState["IdNumberSelected"].ToString();

            //if (ViewState["SurnameSelected"] != null)
            //    surname = ViewState["SurnameSelected"].ToString();
            //if (ViewState["InitialsSelected"] != null)
            //    initials = ViewState["InitialsSelected"].ToString();

            //RoadBlockDB roadBlockDB = new RoadBlockDB(connectionString);
            //DataSet dsSummNoticeRoadBlock = roadBlockDB.GetSumNotWithRoadBlock(rbsIntNo, idNumber, autIntNos);
            // Oscar 20101112 - changed
            //DataSet dsSummNoticeRoadBlock = GetSumNoticeRoadBlockSourceRefresh(idNumber, autIntNos);
            // Oscar 20101112 - changed

            //2014-08-14 add errMsg parameter
            //2014-09-05 searched by IDNumber.
            string errMsg = string.Empty;
            DataSet dsSummNoticeRoadBlock = GetSumNoticeRoadBlockSourceRefresh(idNumber, autIntNos, dgSummonsNoticesListPager.PageSize, dgSummonsNoticesListPager.CurrentPageIndex, out totalCount, ref errMsg, "", "", "");
            ViewState["TotalItemsCount"] = totalCount;

            if (dsSummNoticeRoadBlock.Tables.Count > 0)
            {
                grdSummonsNoticesList.DataSource = dsSummNoticeRoadBlock.Tables[0];
                //Jerry 2013-11-28 get one row per notice
                //grdSummonsNoticesList.DataKeyNames = new string[] { "NotIntNo", "SumIntNo","TicketNo", "SumNotFlag", "OriginalAmount", 
                //"RevisedAmount",  "AreaCode", "NoAOG","PrintFile", "ChargeRowVersion" ,"ChgIntNo","SummonsNo","OffenceDate", "NotProxyFlag", "AutIntNo", "NotFilmType"};
                grdSummonsNoticesList.DataKeyNames = new string[] { "NotIntNo", "SumIntNo","TicketNo", "SumNotFlag",  
                "AreaCode", "PrintFile", "NoticeRowVersion" , "SummonsNo", "OffenceDate", "NotProxyFlag", "AutIntNo", "NotFilmType", 
                "RevisedAmount", "OriginalAmount", "S35OrNoAOG", "Surname", "Initials", "IDNumber", "NotSendTo"};
                grdSummonsNoticesList.DataBind();
                dgSummonsNoticesListPager.RecordCount = totalCount;

                foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
                {
                    string[] Autdescs = Session["AutDesc"].ToString().Split('-');
                    bool flag = false;
                    if (grd.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkBoxAut = grd.FindControl("chkBoxAut") as CheckBox;
                        foreach (string Autdesc in Autdescs)
                        {
                            if (Autdesc == grd.Cells[16].Text && Autdesc != "")
                                flag = true;

                        }
                        if (flag == false && chkBoxAut != null)
                        {
                            chkBoxAut.Checked = false;
                            chkBoxAut.Enabled = false;
                        }
                    }
                }

                OffenderDB offenderDB = new OffenderDB(connectionString);
                Dictionary<string, string> idNumberDic = new Dictionary<string, string>();
                foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
                {
                    if (grd.RowType == DataControlRowType.DataRow)
                    {
                        int currentNotIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotIntNo"].ToString());
                        string s35OrNoAOG = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["S35OrNoAOG"].ToString();
                        CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");

                        #region Jerry 2014-10-21 don't use rule because delete representation by Bontq 1601 client required.
                        //switch (s35OrNoAOG)
                        //{
                        //    case "S35":
                        //        if (allowS35Rule)
                        //            cb.Checked = false;
                        //        break;
                        //    case "NoAOG":
                        //        if (allowNoAogRule)
                        //            cb.Checked = false;
                        //        break;
                        //}
                        #endregion

                        //show the first checked offence details
                        if (cb.Checked && ViewState["HasShowOffenceDetail"] == null)
                        {
                            VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlOffenceDetail.ID, this.pnlIdNumberInfo.ID });
                            ShowOffenceDetail(currentNotIntNo);
                            ViewState["HasShowOffenceDetail"] = true;
                        }
                    }
                }

                //Jerry 2014-03-17 move it from Search() method, show offence details if grid summons notice rows > 0 and offence details has not been shown
                if (grdSummonsNoticesList.Rows.Count > 0 && ViewState["HasShowOffenceDetail"] == null)
                {
                    int notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[0].Value.ToString());
                    VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlOffenceDetail.ID, this.pnlIdNumberInfo.ID });
                    ShowOffenceDetail(notIntNo);
                }
            }

            if (dsSummNoticeRoadBlock.Tables.Count > 0 && grdSummonsNoticesList.Rows.Count > 0)
            {
                var sb = new StringBuilder((string)GetLocalResourceObject("lblResultTitle.Text"));
                if (!string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text))
                    sb.AppendFormat((string)GetLocalResourceObject("OfID"), this.txtIdentityNumber.Text);
                if (!string.IsNullOrWhiteSpace(this.txtQueryRegNo.Text))
                {
                    if (!string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text))
                        sb.Append(";");
                    sb.AppendFormat((string)GetLocalResourceObject("OfRegNo"), this.txtQueryRegNo.Text);
                }

                if (!string.IsNullOrWhiteSpace(this.txtQuerySurname.Text))
                {
                    if (!string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text) || !string.IsNullOrWhiteSpace(this.txtQueryRegNo.Text))
                        sb.Append(";");
                    sb.AppendFormat((string)GetLocalResourceObject("OfSurname"), this.txtQuerySurname.Text);
                }
                if (!string.IsNullOrWhiteSpace(this.txtQueryInitials.Text))
                    sb.AppendFormat((string)GetLocalResourceObject("OfInitials"), this.txtQueryInitials.Text);

                //Jerry 2014-09-17 add
                if (ViewState["IdNumberSelected"] != null && string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text.Trim()))
                {
                    sb.Append(" (");
                    sb.AppendFormat(((string)GetLocalResourceObject("OfID")).Trim(), ViewState["IdNumberSelected"].ToString());
                    sb.Append(")");
                }
                this.lblResultTitle.Text = sb.ToString();

                chkSelectAll.Checked = true;
            }
            else if (grdIdNumberInfo.Rows.Count > 0)
            {
                VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlIdNumberInfo.ID });
            }

            //Jerry 2014-08-14 add
            if (totalCount <= 0 && !string.IsNullOrEmpty(errMsg))
            {
                lblMessage.Text = errMsg;
                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteLog("Exception", SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString(), errMsg);
            }

            //Jerry 2014-08-14 change
            if (totalCount <= 0)
            {
                if (string.IsNullOrWhiteSpace(lblMessage.Text))
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text23");
                else
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text23") + " " + lblMessage.Text;
            }
        }

        private void BindIDNumberGrid()
        {
            if (string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text)
                && string.IsNullOrWhiteSpace(this.txtQueryRegNo.Text)
                && string.IsNullOrWhiteSpace(this.txtQuerySurname.Text)
                && string.IsNullOrWhiteSpace(this.txtQueryInitials.Text))
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text");
                return;
            }

            string autIntNos = (string)this.ViewState[AUTHORISED_AUTINTNOS];

            //Jerry 2014-08-14 add errMsg parameter
            string errMsg = string.Empty;
            DataSet dsIDNumberDataSet = GetIDNumberDataSetWithRoadBlock(txtIdentityNumber.Text.Trim(), autIntNos, ref errMsg, this.txtQueryRegNo.Text.Trim(), this.txtQuerySurname.Text.Trim(), this.txtQueryInitials.Text.Trim());
            if (dsIDNumberDataSet.Tables.Count > 0)
            {
                grdIdNumberInfo.DataSource = dsIDNumberDataSet.Tables[0];
                this.grdIdNumberInfo.DataKeyNames = new string[] { "IDNumber", "Surname", "Initials", "DriverOrProxy" };
                this.grdIdNumberInfo.DataBind();
                lblDriverOrProxyInfo.Text = (string)GetLocalResourceObject("lblDriverOrProxyInfo.Text");
            }

            if (grdIdNumberInfo.Rows.Count > 0)
            {
                VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlIdNumberInfo.ID });
            }

            //Jerry 2014-08-14 add
            if (grdIdNumberInfo.Rows.Count <= 0 && !string.IsNullOrEmpty(errMsg))
            {
                lblMessage.Text = errMsg;
                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteLog("Exception", SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString(), errMsg);
            }
        }

        private void ShowRoadBlockSessionDetail(int rbsIntNo)
        {
            pnlSessionIDFilter.Visible = true;

            string cofcIntNo = string.Empty;
            string cofcName = string.Empty;
            string authority = string.Empty;

            RoadBlockSessions roadBlock = new RoadBlockSessions();
            RoadBlockDB roadDB = new RoadBlockDB(connectionString);
            roadBlock = roadDB.GetRoadBlockSessionByNo(rbsIntNo);

            if (roadBlock != null)
            {
                lblLocationText.Text = roadBlock.LocDescr;
                lblGPRSText.Text = roadBlock.LocGps;
                lblUserText.Text = this.login;
                //Jerry 2014-01-07 change date format
                //lblSessionStartText.Text = roadBlock.CrtTime.Date.ToShortDateString();
                lblSessionStartText.Text = roadBlock.CrtTime.Date.ToString("yyyy/MM/dd");
                lblTitle.Text = string.Format((string)GetLocalResourceObject("lblTitle.Text1"), roadBlock.RBSessionID);
            }
            else
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text1");
                return;
            }

            ClerkofCourtDB clerkDB = new ClerkofCourtDB(connectionString);
            DataView dv_AuthClerk = new DataView(clerkDB.GetAutClerkofCourt().Tables[0]);
            SqlDataReader result = clerkDB.GetClerkofCourtByRBSNo(rbsIntNo);
            while (result.Read())
            {
                cofcName += result["CofcName"].ToString().Trim() + ", ";
                cofcIntNo += result["CofCIntNo"].ToString().Trim() + ", ";
            }
            result.Close();

            if (cofcName.EndsWith(", "))
                cofcName = cofcName.Substring(0, cofcName.Length - 2);

            lblClerkofCourtText.Text = cofcName;
            //if (cofcIntNo.Length > 0)
            //{
            //    cofcIntNo.Substring(0, cofcIntNo.Length - cofcIntNo.LastIndexOf(",")-1);
            //}
            // jake 2013-09-13 added check if cofcIntNo is null
            if (!String.IsNullOrEmpty(cofcIntNo))
                dv_AuthClerk.RowFilter = "CofCIntNo In (" + cofcIntNo + ")";
            if (dv_AuthClerk.Count > 0)
            {
                XElement element = new XElement("root");
                foreach (DataRowView drv in dv_AuthClerk)
                {
                    if (authority.IndexOf(drv["AutName"].ToString().Trim()) < 0)
                    {
                        element.Add(new XElement("autIntNo", drv["AutIntNo"]));
                        authority += drv["AutName"] + ", ";
                    }
                }

                if (authority.EndsWith(", "))
                {
                    authority = authority.Substring(0, authority.Length - 2);
                }
                lblLAText.Text = authority;
                this.ViewState.Add(AUTHORISED_AUTINTNOS, element.ToString());
            }

            //Jerry 2015-03-16 change
            //if (roadBlock.LocGps != "" & roadBlock.LocGps.Length >= 6)
            //{
            //    lblGPRSText.Text = "X" + roadBlock.LocGps.Substring(0, 3) + " Y" + roadBlock.LocGps.Substring(3, 3);
            //}

            if (!string.IsNullOrEmpty(roadBlock.LocGps))
            {
                string gpsValue = roadBlock.LocGps;
                string gPSLatitude = string.Empty;
                string gPSLongitude = string.Empty;

                GPSUtility.GetLatitudeAndLongitude(gpsValue, out gPSLatitude, out gPSLongitude);
                lblGPRSText.Text = (string)GetLocalResourceObject("lblLatitude.Text") + gPSLatitude + "  " + (string)GetLocalResourceObject("lblLongitude.Text") + gPSLongitude;
            }
        }

        private void ShowOffenceDetail(int notIntNo)
        {
            OffenderDetails details = new OffenderDetails();
            OffenderDB dbo = new OffenderDB(connectionString);
            details = dbo.GetOffenderDetails(notIntNo);
            //GetAccusedBySumIntNo
            //AccusedDB accusedDB = new AccusedDB(this.connectionString);
            //DriverDetails drvDetail = new DriverDetails();

            //drvDetail = accusedDB.GetAccusedBySumIntNo(sumIntNo);
            if (details != null)
            {
                //this.lblSessionDetail.Text = string.Format((string)GetLocalResourceObject("lblSessionDetail.Text"), txtIdentityNumber.Text);
                // 2013-11-29, Oscar changed
                //2014-09-17 comment out this code by bontq 1340 required.
                //var sb = new StringBuilder();
                //if (!string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text))
                //    sb.AppendFormat((string)GetLocalResourceObject("lblSessionDetail.Text"), this.txtIdentityNumber.Text);
                //if (!string.IsNullOrWhiteSpace(this.txtQueryRegNo.Text))
                //    sb.AppendFormat((string)GetLocalResourceObject("RegNoValue"), this.txtQueryRegNo.Text);
                //if (!string.IsNullOrWhiteSpace(this.txtQuerySurname.Text))
                //    sb.AppendFormat((string)GetLocalResourceObject("SurnameValue"), this.txtQuerySurname.Text);
                //if (!string.IsNullOrWhiteSpace(this.txtQueryInitials.Text))
                //    sb.AppendFormat((string)GetLocalResourceObject("InitialsValue"), this.txtQueryInitials.Text);
                //this.lblSessionDetail.Text = sb.ToString();

                this.txtPostalAddress1.Text = details.PoAddress1;
                this.txtPostalAddress2.Text = details.PoAddress2;
                this.txtPostalAddress3.Text = details.PoAddress3;
                this.txtPostalAddress4.Text = details.PoAddress4;
                this.txtPostalAreaCode.Text = details.PoAreaCode;
                this.txtFullName.Text = details.ForeNames;
                this.txtSurName.Text = details.Surname;
                this.txtInitials.Text = details.Initials; //Jerry 2014-04-15 add
                // FBJ (2009-06-23): Commented below as sometimes the returned ID number is not the same as that searched for!
                //this.txtIdentityNumber.Text = details.IDNumber;
                this.txtLandLine.Text = details.HomeNo;
                this.txtMobileNo.Text = details.CellNo;

                this.txtStreetAddress1.Text = details.Address1;
                this.txtStreetAddress2.Text = details.Address2;
                this.txtStreetAddress3.Text = details.Address3;
                this.txtStreetAddress4.Text = details.Address4;
                this.txtStreetAreaCode.Text = details.AreaCode;
                this.hidIDType.Value = details.IDType;
                this.ViewState.Add("OffenderDetails", details);
            }
        }

        /// <summary>
        /// Update Offender Details For Notice
        /// Jerry 2014-02-21 change it, check and update all selected row
        /// </summary>
        /// <param name="errMessage"></param>
        /// <param name="lastUser"></param>
        /// <returns></returns>
        private bool UpdateOffenderDetailsForNotice(ref string errMessage, string lastUser)
        {
            #region Jerry 2014-02-21 change it, check and update all selected row
            //try
            //{
            //    OffenderDetails details;
            //    if (ViewState["OffenderDetails"] != null)
            //        details = (OffenderDetails)ViewState["OffenderDetails"];
            //    else
            //        details = new OffenderDetails();

            //    details.PoAddress1 = this.txtPostalAddress1.Text;
            //    details.PoAddress2 = this.txtPostalAddress2.Text;
            //    details.PoAddress3 = this.txtPostalAddress3.Text;
            //    details.PoAddress4 = this.txtPostalAddress4.Text;
            //    details.PoAreaCode = this.txtPostalAreaCode.Text;
            //    details.ForeNames = this.txtFullName.Text;
            //    details.Surname = this.txtSurName.Text;
            //    details.IDNumber = this.txtIdentityNumber.Text;

            //    details.Address1 = this.txtStreetAddress1.Text;
            //    details.Address2 = this.txtStreetAddress2.Text;
            //    details.Address3 = this.txtStreetAddress3.Text;
            //    details.Address4 = this.txtStreetAddress4.Text;
            //    details.AreaCode = this.txtStreetAreaCode.Text;
            //    details.CellNo = this.txtMobileNo.Text;
            //    details.HomeNo = this.txtLandLine.Text;
            //    details.LastUser = lastUser;

            //    OffenderDB dbo = new OffenderDB(connectionString);
            //    bool updateOffender = dbo.UpdateOffenderDetails(details, ref errMessage);
            //    //Jerry 2014-01-22 if there has error it will be wrote to log file
            //    if (!updateOffender && !string.IsNullOrEmpty(errMessage))
            //    {
            //        SIL.AARTO.BLL.EntLib.EntLibLogger.WriteLog("Exception", SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString(), errMessage);
            //    }

            //    return updateOffender;

            //}
            //catch (Exception ex)
            //{
            //    //Jerry 2014-01-22 add
            //    SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
            //    errMessage = ex.Message;

            //    return false;
            //}
            #endregion
            OffenderDB dbo = new OffenderDB(connectionString);
            Stalberg.TMS.Data.SearchIDDB searchIDDB = new Stalberg.TMS.Data.SearchIDDB(connectionString);
            Stalberg.TMS.Data.SearchSurNameDB searchSurNameDB = new Stalberg.TMS.Data.SearchSurNameDB(connectionString);

            int notIntNo = 0;
            List<int> updateNoticeList = new List<int>();
            foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    //none check the row
                    CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                    if (!cb.Checked)
                        continue;

                    //notice not exists
                    notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotIntNo"].ToString());
                    if (notIntNo == 0)
                        continue;

                    updateNoticeList.Add(notIntNo);

                    //check the id number in grdSummonsNoticesList does not equal selected id number in pnlIdNumberInfo
                    string currentRowIdNumber = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["IDNumber"].ToString().Trim();
                    string idNumberSelected = ViewState["IdNumberSelected"] == null ? string.Empty : ViewState["IdNumberSelected"].ToString();
                    if (string.IsNullOrEmpty(currentRowIdNumber))
                        continue;
                    if (!string.IsNullOrEmpty(idNumberSelected) && currentRowIdNumber != idNumberSelected.Trim())
                        continue;

                    try
                    {
                        //Jerry 2014-08-20 don't update offender details if there none changed 
                        OffenderDetails originalDetails = dbo.GetOffenderDetails(notIntNo);
                        if (originalDetails == null)
                            continue;
                        if (originalDetails.PoAddress1.Trim().ToLower() == this.txtPostalAddress1.Text.Trim().ToLower() &&
                            originalDetails.PoAddress2.Trim().ToLower() == this.txtPostalAddress2.Text.Trim().ToLower() &&
                            originalDetails.PoAddress3.Trim().ToLower() == this.txtPostalAddress3.Text.Trim().ToLower() &&
                            originalDetails.PoAddress4.Trim().ToLower() == this.txtPostalAddress4.Text.Trim().ToLower() &&
                            originalDetails.PoAreaCode.Trim().ToLower() == this.txtPostalAreaCode.Text.Trim().ToLower() &&
                            originalDetails.ForeNames.Trim().ToLower() == this.txtFullName.Text.Trim().ToLower() &&
                            originalDetails.Surname.Trim().ToLower() == this.txtSurName.Text.Trim().ToLower() &&
                            originalDetails.Initials.Trim().ToLower() == this.txtInitials.Text.Trim().ToLower() &&
                            originalDetails.Address1.Trim().ToLower() == this.txtStreetAddress1.Text.Trim().ToLower() &&
                            originalDetails.Address2.Trim().ToLower() == this.txtStreetAddress2.Text.Trim().ToLower() &&
                            originalDetails.Address3.Trim().ToLower() == this.txtStreetAddress3.Text.Trim().ToLower() &&
                            originalDetails.Address4.Trim().ToLower() == this.txtStreetAddress4.Text.Trim().ToLower() &&
                            originalDetails.AreaCode.Trim().ToLower() == this.txtStreetAreaCode.Text.Trim().ToLower() &&
                            originalDetails.CellNo.Trim().ToLower() == this.txtMobileNo.Text.Trim().ToLower() &&
                            originalDetails.HomeNo.Trim().ToLower() == this.txtLandLine.Text.Trim().ToLower())
                            continue;

                        OffenderDetails details;
                        details = new OffenderDetails();

                        details.PoAddress1 = this.txtPostalAddress1.Text.Trim();
                        details.PoAddress2 = this.txtPostalAddress2.Text.Trim();
                        details.PoAddress3 = this.txtPostalAddress3.Text.Trim();
                        details.PoAddress4 = this.txtPostalAddress4.Text.Trim();
                        details.PoAreaCode = this.txtPostalAreaCode.Text.Trim();

                        details.ForeNames = this.txtFullName.Text.Trim();
                        details.Surname = this.txtSurName.Text.Trim();
                        details.Initials = this.txtInitials.Text.Trim().Trim();
                        //details.IDNumber = this.txtIdentityNumber.Text;
                        details.IDNumber = string.IsNullOrEmpty(idNumberSelected) ? currentRowIdNumber : idNumberSelected.Trim();

                        details.Address1 = this.txtStreetAddress1.Text.Trim();
                        details.Address2 = this.txtStreetAddress2.Text.Trim();
                        details.Address3 = this.txtStreetAddress3.Text.Trim();
                        details.Address4 = this.txtStreetAddress4.Text.Trim();
                        details.AreaCode = this.txtStreetAreaCode.Text.Trim();

                        details.CellNo = this.txtMobileNo.Text.Trim();
                        details.HomeNo = this.txtLandLine.Text.Trim();
                        details.LastUser = lastUser;

                        int returnValue = dbo.UpdateOffenderDetails(details, notIntNo, ref errMessage);
                        if (returnValue < 1 && string.IsNullOrEmpty(errMessage))
                        {
                            if (returnValue == 0)
                                errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text58"), notIntNo);
                        }

                        //Jerry 2014-01-22 if there has error it will be wrote to log file
                        if (returnValue < 1 && !string.IsNullOrEmpty(errMessage))
                        {
                            SIL.AARTO.BLL.EntLib.EntLibLogger.WriteLog("Exception", SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString(), errMessage);
                            break;
                        }

                        //Jerry 2014-05-04 Update SearchID
                        if (string.IsNullOrEmpty(errMessage))
                        {
                            int result = searchIDDB.UpdateSearchID(notIntNo, ref errMessage);
                            switch (result)
                            {
                                case -1:
                                    errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text71"), notIntNo);
                                    break;
                                case -2:
                                    errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text72"), notIntNo);
                                    break;
                                case -3:
                                    errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text73"), notIntNo);
                                    break;
                                case -4:
                                    errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text74"), notIntNo);
                                    break;
                            }
                        }

                        //Jerry 2014-05-04 Update SearchID
                        if (string.IsNullOrEmpty(errMessage))
                        {
                            int result = searchSurNameDB.UpdateSearchSurName(notIntNo, ref errMessage);
                            switch (result)
                            {
                                case -1:
                                    errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text75"), notIntNo);
                                    break;
                                case -2:
                                    errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text76"), notIntNo);
                                    break;
                                case -3:
                                    errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text77"), notIntNo);
                                    break;
                                case -4:
                                    errMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text78"), notIntNo);
                                    break;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
                        errMessage = ex.Message;
                        break;
                    }
                }
            }

            if (string.IsNullOrEmpty(errMessage))
            {
                ViewState["UpdateNoticeList"] = updateNoticeList;
                return true;
            }
            else
                return false;
        }

        private void DataWashing(ref string errMessage)
        {
            //check rules are Y
            if (dataWashingActived == "Y" && useRoadblockAddress == "Y")
            {
                //get the id number
                string idNumberSelected = ViewState["IdNumberSelected"].ToString();

                //get last data wash date
                Mi5Persona mI5Persona = new Mi5PersonaService().GetByMipidNumber(idNumberSelected);
                DateTime mIPLastWashedDate;

                if (mI5Persona == null)
                    mIPLastWashedDate = DateTime.Parse("1900-01-01");
                else
                    mIPLastWashedDate = mI5Persona.MipLastWashedDate.HasValue ? mI5Persona.MipLastWashedDate.Value : DateTime.Parse("1900-01-01");
                if (roadblockAddrExpiryPeriod == 0)
                    roadblockAddrExpiryPeriod = 12;

                //data washing if last date data washing add roadblockAddrExpiryPeriod months  is less than now.
                if (mIPLastWashedDate.AddMonths(roadblockAddrExpiryPeriod) < DateTime.Now)
                {
                    MI5DB mI5DB = new MI5DB(this.connectionString);
                    Persona persona = new Persona();

                    persona.Surname = this.txtSurName.Text.Trim();
                    persona.Initials = this.txtInitials.Text.Trim();
                    persona.FirstName = this.txtFullName.Text.Trim();
                    persona.IDType = this.hidIDType.Value.Trim();

                    persona.IDNumber = idNumberSelected;

                    persona.PostalAddress1 = this.txtPostalAddress1.Text.Trim();
                    persona.PostalAddress2 = this.txtPostalAddress2.Text.Trim();
                    persona.PostalAddress3 = this.txtPostalAddress3.Text.Trim();
                    persona.PostalAddress4 = this.txtPostalAddress4.Text.Trim();
                    persona.PostalAddress5 = "";
                    persona.PostalCode = this.txtPostalAreaCode.Text.Trim();

                    persona.StreetAddress1 = this.txtStreetAddress1.Text.Trim();
                    persona.StreetAddress2 = this.txtStreetAddress2.Text.Trim();
                    persona.StreetAddress3 = this.txtStreetAddress3.Text.Trim();
                    persona.StreetAddress4 = this.txtStreetAddress4.Text.Trim();
                    persona.StreetCode = this.txtStreetAreaCode.Text.Trim();
                    persona.Source = "Roadblock";

                    try
                    {
                        mI5DB.UpdatePersona(persona, this.login);
                    }
                    catch (Exception ex)
                    {
                        SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
                        errMessage = ex.Message;
                    }
                }
            }
        }

        private void UpdateNoticeComment(ref string errMessage)
        {
            OffenderDetails details = null;
            OffenderDB dbo = new OffenderDB(connectionString);

            int notIntNo = 0;
            foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    //none check the row
                    CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                    if (!cb.Checked)
                        continue;

                    notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotIntNo"].ToString());
                    if (notIntNo == 0)
                        continue;

                    details = dbo.GetOffenderDetails(notIntNo);
                    if (details == null)
                        continue;

                    //Jerry 2014-08-20 don't add NoticeComment if there none changed 
                    if (details.Surname.Trim().ToLower() == this.txtSurName.Text.Trim().ToLower() &&
                        details.PoAddress1.Trim().ToLower() == this.txtPostalAddress1.Text.Trim().ToLower() &&
                        details.PoAddress2.Trim().ToLower() == this.txtPostalAddress2.Text.Trim().ToLower() &&
                        details.PoAddress3.Trim().ToLower() == this.txtPostalAddress3.Text.Trim().ToLower() &&
                        details.PoAddress4.Trim().ToLower() == this.txtPostalAddress4.Text.Trim().ToLower() &&
                        details.PoAreaCode.Trim().ToLower() == this.txtPostalAreaCode.Text.Trim().ToLower() &&
                        details.Address1.Trim().ToLower() == this.txtStreetAddress1.Text.Trim().ToLower() &&
                        details.Address2.Trim().ToLower() == this.txtStreetAddress2.Text.Trim().ToLower() &&
                        details.Address3.Trim().ToLower() == this.txtStreetAddress3.Text.Trim().ToLower() &&
                        details.Address4.Trim().ToLower() == this.txtStreetAddress4.Text.Trim().ToLower() &&
                        details.AreaCode.Trim().ToLower() == this.txtStreetAreaCode.Text.Trim().ToLower())
                        continue;

                    NoticeComment noticeCommentEntity = new NoticeComment();
                    noticeCommentEntity.NotIntNo = notIntNo;

                    //If Surname AND any of the address details change
                    if (details.Surname.Trim().ToLower() != this.txtSurName.Text.Trim().ToLower() && (
                        details.PoAddress1.Trim().ToLower() != this.txtPostalAddress1.Text.Trim().ToLower() ||
                        details.PoAddress2.Trim().ToLower() != this.txtPostalAddress2.Text.Trim().ToLower() ||
                        details.PoAddress3.Trim().ToLower() != this.txtPostalAddress3.Text.Trim().ToLower() ||
                        details.PoAddress4.Trim().ToLower() != this.txtPostalAddress4.Text.Trim().ToLower() ||
                        details.PoAreaCode.Trim().ToLower() != this.txtPostalAreaCode.Text.Trim().ToLower() ||
                        details.Address1.Trim().ToLower() != this.txtStreetAddress1.Text.Trim().ToLower() ||
                        details.Address2.Trim().ToLower() != this.txtStreetAddress2.Text.Trim().ToLower() ||
                        details.Address3.Trim().ToLower() != this.txtStreetAddress3.Text.Trim().ToLower() ||
                        details.Address4.Trim().ToLower() != this.txtStreetAddress4.Text.Trim().ToLower() ||
                        details.AreaCode.Trim().ToLower() != this.txtStreetAreaCode.Text.Trim().ToLower()))
                    {
                        noticeCommentEntity.NcComment = string.Format((string)GetLocalResourceObject("NoticeCommentMsg1"), Environment.NewLine, details.FullName.Trim(), Environment.NewLine, details.Address1.Trim(),
                            details.Address2.Trim(), details.Address3.Trim(), details.Address4.Trim(), details.AreaCode, Environment.NewLine, details.PoAddress1.Trim(), details.PoAddress2.Trim(), details.PoAddress3.Trim(), details.PoAddress4.Trim(),
                            details.PoAreaCode);
                    }
                    //If Surname changes 
                    else if (details.Surname.Trim().ToLower() != this.txtSurName.Text.Trim().ToLower())
                    {
                        noticeCommentEntity.NcComment = string.Format((string)GetLocalResourceObject("NoticeCommentMsg2"), Environment.NewLine, details.FullName.Trim());
                    }
                    //If Address changes
                    else if (details.PoAddress1.Trim().ToLower() != this.txtPostalAddress1.Text.Trim().ToLower() ||
                        details.PoAddress2.Trim().ToLower() != this.txtPostalAddress2.Text.Trim().ToLower() ||
                        details.PoAddress3.Trim().ToLower() != this.txtPostalAddress3.Text.Trim().ToLower() ||
                        details.PoAddress4.Trim().ToLower() != this.txtPostalAddress4.Text.Trim().ToLower() ||
                        details.PoAreaCode.Trim().ToLower() != this.txtPostalAreaCode.Text.Trim().ToLower() ||
                        details.Address1.Trim().ToLower() != this.txtStreetAddress1.Text.Trim().ToLower() ||
                        details.Address2.Trim().ToLower() != this.txtStreetAddress2.Text.Trim().ToLower() ||
                        details.Address3.Trim().ToLower() != this.txtStreetAddress3.Text.Trim().ToLower() ||
                        details.Address4.Trim().ToLower() != this.txtStreetAddress4.Text.Trim().ToLower() ||
                        details.AreaCode.Trim().ToLower() != this.txtStreetAreaCode.Text.Trim().ToLower())
                    {
                        noticeCommentEntity.NcComment = string.Format((string)GetLocalResourceObject("NoticeCommentMsg3"), Environment.NewLine, details.Address1.Trim(), details.Address2.Trim(), details.Address3.Trim(), details.Address4.Trim(), details.AreaCode,
                            Environment.NewLine, details.PoAddress1.Trim(), details.PoAddress2.Trim(), details.PoAddress3.Trim(), details.PoAddress4.Trim(), details.PoAreaCode);
                    }
                    else
                        return;

                    noticeCommentEntity.NcCreateDate = DateTime.Now;
                    noticeCommentEntity.NcUpdateDate = DateTime.Now;
                    noticeCommentEntity.LastUser = this.login;

                    try
                    {
                        noticeCommentService.Save(noticeCommentEntity);
                    }
                    catch (Exception ex)
                    {
                        SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
                        errMessage = ex.Message;
                    }
                }
            }

        }

        private void PopulateCourt()
        {
            if (this.cboSummonsServer.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_SUMMONS_SERVER")))
            {
                this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text2");
                return;
            }

            this.cboCourt.Items.Clear();

            int autIntNo = Convert.ToInt32(ViewState["AutIntNo"].ToString());

            CourtDB db = new CourtDB(connectionString);
            System.Data.SqlClient.SqlDataReader reader = db.GetAuth_CourtListByAuth(this.autIntNo);
            while (reader.Read())
            {
                ListItem item = new ListItem();
                item.Value = reader["CrtIntNo"].ToString() + "~" + reader["ACIntNo"].ToString();
                item.Text = reader["CrtName"].ToString();
                this.cboCourt.Items.Add(item);
            }
            reader.Close();

            this.cboCourt.Items.Insert(0, (string)GetLocalResourceObject("SELECT_COURT"));
        }

        /// <summary>
        /// FT 20090914 Populate SummonsServerStaff
        /// </summary>
        /// <param name="i"></param>
        private void PopulateSummonsServerStaff()
        {
            SummonsServerStaffVisible();
            ClerkofCourtDB clerk = new ClerkofCourtDB(connectionString);
            SqlDataReader result = clerk.GetClerkofCourtByRBSNo(rbsIntNo);
            while (result.Read())
            {
                ListItem item = new ListItem(result["CofCName"].ToString(), "C" + result["CofCIntNo"].ToString());
                cboSummonsServerStaff.Items.Add(item);
            }

            TrafficOfficerDB traff = new TrafficOfficerDB(connectionString);
            result = traff.GetTrafficOfficerListByAuthority(autIntNo, OfficerSeniority.Senior);
            while (result.Read())
            {
                ListItem item = new ListItem(result["TOSName"].ToString(), "O" + result["TONo"].ToString());
                cboSummonsServerStaff.Items.Add(item);
            }
            this.cboSummonsServerStaff.Items.Insert(0, (string)GetLocalResourceObject("SELECT_CofC_OFFICER"));
        }

        private void ShowSummonsServer(string areaCode)
        {
            this.lblMessage.Text = string.Empty;

            //areacode is now string
            //short area;
            //if (!short.TryParse(areaCode, out area))
            //{
            //    this.lblMessage.Text = string.Format("The area code ({0}) is not valid.", area);
            //    return;
            //}
            this.cboSummonsServer.Items.Clear();

            //HANDLE SUMMONS SERVER
            //The summons server must be a default for roadblock (if not in table then must add it first time)
            SummonsServerDB ssDB = new SummonsServerDB(connectionString);
            int ssIntNo = ssDB.UpdateSummonsServer("RoadBlock", "RoadBlock", "", "", "", "opus");

            //SummonsServerDB db = new SummonsServerDB(connectionString);
            ListItem item = new ListItem();
            //item.Value = ssIntNo.ToString();
            // 2013-11-18, Oscar changed
            item.Value = string.Join("|", ssIntNo, 1);
            item.Text = (string)GetLocalResourceObject("SELECT_SS_COFC");
            this.cboSummonsServer.Items.Add(item);

            var ss = new SummonsServerService().GetBySsIntNo(ssIntNo);
            this.cboSummonsServer.Items.Add(new ListItem(ss.SsFullName, string.Join("|", ssIntNo, 2)));

            // 2013-11-11, Oscar removed - we don't need 'Unknown' (Dawn)
            //// Unknown summons server
            //SummonsServerDetails ss = db.GetUnknownSummonsServer();
            //item = new ListItem();
            //item.Value = ss.SSIntNo.ToString();
            //item.Text = ss.SSFullName;
            //this.cboSummonsServer.Items.Add(item);

            //20090914 Commented by FT, remove area summons server staff.
            // LA summons server
            //SqlDataReader reader = db.GetSummonsServerForArea(this.autIntNo, areaCode);
            //while (reader.Read())
            //{
            //    item = new ListItem();
            //    item.Value = reader["ASIntNo"].ToString();
            //    item.Text = reader["SSFullName"].ToString();
            //    this.cboSummonsServer.Items.Add(item);
            //}
            //reader.NextResult();
            //string areaDescription = areaCode;
            //if (reader.Read())
            //{
            //    if (reader["AreaCode"] != DBNull.Value)
            //        areaDescription = reader["AreaCode"].ToString();
            //    if (reader["AreaDescr"] != DBNull.Value && reader["AreaDescr"].ToString().Trim().Length > 0)
            //        areaDescription = areaDescription + " (" + reader["AreaDescr"].ToString() + ")";
            //}
            //this.lblSummonsServerArea.Text = areaDescription;

            //reader.Close();

            // Dummy SS seletion
            this.cboSummonsServer.Items.Insert(0, (string)GetLocalResourceObject("SELECT_SUMMONS_SERVER"));
        }

        // 2013-11-18, Oscar added
        private int GetCboSummonsServerSelectedValue()
        {
            if (this.cboSummonsServer.SelectedIndex == 0) return 0;

            var group = this.cboSummonsServer.SelectedValue.Split('|');
            int value;
            return int.TryParse(group[0], out value) ? value : 0;
        }

        private void VisiblePanel(string[] paneList)
        {
            //this.pnlNOAOGDecision.Visible = false; //Jerry 2014-10-21 delete it by Bontq 1601 client required.
            this.pnlOffenceDetail.Visible = false;
            this.pnlSessionIDFilter.Visible = false;
            this.pnlShowButton.Visible = false;
            this.pnlSummonOther.Visible = false;
            this.pnlSummonsServer.Visible = false;
            this.pnlSumonsNoticeList.Visible = false;
            //this.pnlRepresentation.Visible = false; //Jerry 2014-10-21 delete it by Bontq 1601 client required.
            this.pnlCourt.Visible = false;
            this.pnlSummonsMultiplePrint.Visible = false;
            this.pnlIdNumberInfo.Visible = false;

            foreach (string id in paneList)
            {
                Panel p = (Panel)this.Page.FindControl(id);
                p.Visible = true;
            }
        }

        private int CheckRoadBlockDataRule()
        {
            //DateRulesDB dateRule = new DateRulesDB(connectionString);
            //DateRulesDetails ruleCourtDate = new DateRulesDetails();
            //ruleCourtDate.AutIntNo = this.autIntNo;
            //ruleCourtDate.DtRDescr = "Number of days between a roadblock summons and COURT DATE";
            //ruleCourtDate.DtREndDate = "SumCourtDate";
            //ruleCourtDate.DtRNoOfDays = 10;
            //ruleCourtDate.DtRStartDate = "RoadBlockDate";
            //ruleCourtDate.LastUser = PROCESS_NAME;
            //ruleCourtDate = dateRule.GetDefaultDateRule(ruleCourtDate);

            //return ruleCourtDate;

            DateRulesDetails rule = new DateRulesDetails();

            rule.AutIntNo = autIntNo;
            rule.LastUser = (string)GetLocalResourceObject("PROCESS_NAME");
            rule.DtRStartDate = "RoadBlockDate";
            rule.DtREndDate = "SumCourtDate";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return noOfDays;
        }

        private bool CheckRoadBlockSession(int rbsIntNo)
        {
            //RoadBlockSession roadBlock = new RoadBlockSession();
            RoadBlockDB DB = new RoadBlockDB(connectionString);

            string status = DB.GetRoadBlockSessionByNo(rbsIntNo).IndcFlag;

            return status.Trim().Equals("Y");
        }

        private DateTime CheckSummonsServedBy(DateTime courtDate)
        {
            //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
            //DateRulesDetails ruleCourtDate = new DateRulesDetails();
            //ruleCourtDate.AutIntNo = this.autIntNo;
            //ruleCourtDate.DtRDescr = "Amount in days that a summons must be served by from the court date";
            //ruleCourtDate.DtRStartDate = "CDate";
            //ruleCourtDate.DtRNoOfDays = -30;
            //ruleCourtDate.DtREndDate = "SumServeByDate";
            //ruleCourtDate.LastUser = PROCESS_NAME;
            //ruleCourtDate = dateRule.GetDefaultDateRule(ruleCourtDate);

            //AutIntNo, StartDate, EndDate and LastUser must be set up before the default rule is called

            DateRulesDetails rule = new DateRulesDetails();

            rule.AutIntNo = autIntNo;
            rule.LastUser = (string)GetLocalResourceObject("PROCESS_NAME");
            rule.DtRStartDate = "CDate";
            rule.DtREndDate = "SumServeByDate";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return courtDate.AddDays(noOfDays);
        }

        private DateTime CheckLastAOGDate(DateTime courtDate)
        {
            //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
            //DateRulesDetails ruleCourtDate = new DateRulesDetails();
            //ruleCourtDate.AutIntNo = this.autIntNo;
            //ruleCourtDate.DtRDescr = "Amount in days that an AOG fine can be paid from court date";
            //ruleCourtDate.DtRStartDate = "CDate";
            //ruleCourtDate.DtRNoOfDays = -7;
            //ruleCourtDate.DtREndDate = "LastAOGDate";
            //ruleCourtDate.LastUser = PROCESS_NAME;
            //ruleCourtDate = dateRule.GetDefaultDateRule(ruleCourtDate);

            //return courtDate.AddDays(ruleCourtDate.DtRNoOfDays);

            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.LastUser = (string)GetLocalResourceObject("PROCESS_NAME");
            rule.DtRStartDate = "CDate";
            rule.DtREndDate = "LastAOGDate";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return courtDate.AddDays(noOfDays);
        }

        private DateRulesDetails CheckDateRule()
        {
            //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
            //DateRulesDetails ruleCourtDate = new DateRulesDetails();
            //ruleCourtDate.AutIntNo = this.autIntNo;
            //ruleCourtDate.DtRDescr = "Min no. of days from SUMMONS ISSUE DATE to COURT DATE";
            //ruleCourtDate.DtREndDate = "CDate";
            //ruleCourtDate.DtRNoOfDays = 30;
            //ruleCourtDate.DtRStartDate = "NotIssueSummonsDate";
            //ruleCourtDate.LastUser = PROCESS_NAME;
            //ruleCourtDate = dateRule.GetDefaultDateRule(ruleCourtDate);

            //return ruleCourtDate;

            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.LastUser = (string)GetLocalResourceObject("PROCESS_NAME");
            rule.DtRStartDate = "NotIssueSummonsDate";
            rule.DtREndDate = "CDate";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return rule;
        }

        //Jake 2015-03-19 use new data rule
        /// <summary>
        /// jerry 2011-12-16 add 
        /// </summary>
        private void GetDefaultDateRule()
        {
            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = this.autIntNo;
            //rule.DtRStartDate = "SumIssueDate";
            //rule.DtREndDate = "SumExpiredDate";
            rule.DtRStartDate = "NotOffenceDate";
            rule.DtREndDate = "SumExpireDate";
            rule.LastUser = this.login;

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            noDaysForSummonsExpiry = dateRule.SetDefaultDateRule();
        }

        private void GetAuthorityRule(int autIntNo)
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> keyValue;

            #region Jerry 2014-10-21 don't use rule because delete representation by Bontq 1601 client required.
            //rule.AutIntNo = autIntNo;
            //rule.ARCode = "4537";
            //rule.LastUser = this.login;

            //DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            //KeyValuePair<int, string> keyValue = authRule.SetDefaultAuthRule();
            //allowNoAogRule = keyValue.Value == "Y" ? true : false;

            //rule.AutIntNo = autIntNo;
            //rule.ARCode = "6610";
            //rule.LastUser = this.login;
            //authRule = new DefaultAuthRules(rule, this.connectionString);
            //keyValue = authRule.SetDefaultAuthRule();
            //allowS35Rule = keyValue.Value == "Y" ? true : false;
            #endregion

            rule = new AuthorityRulesDetails();
            rule.ARCode = "9060";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.login;
            authRule = new DefaultAuthRules(rule, this.connectionString);
            keyValue = authRule.SetDefaultAuthRule();
            dataWashingActived = keyValue.Value.ToUpper().Trim();

            rule = new AuthorityRulesDetails();
            rule.ARCode = "9140";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.login;
            authRule = new DefaultAuthRules(rule, this.connectionString);
            keyValue = authRule.SetDefaultAuthRule();
            useRoadblockAddress = keyValue.Value.ToUpper().Trim();

            rule = new AuthorityRulesDetails();
            rule.ARCode = "9150";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.login;
            authRule = new DefaultAuthRules(rule, this.connectionString);
            keyValue = authRule.SetDefaultAuthRule();
            roadblockAddrExpiryPeriod = keyValue.Key;
        }

        private bool CheckIDNumberRule()
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "5060";
            //rule.ARComment = "Y = true (Default); N = false;";
            //rule.ARDescr = "Rule as to whether ID Numbers are required for summons generation.";
            //rule.ARNumeric = 1;
            //rule.ARString = "Y";
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "5060";
            rule.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            //return rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            return value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
        }

        private string CheckS35NoAOGPermitted()
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "6600";
            rule.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            //return rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            return value.Value;
        }

        private bool CheckForenamesRule()
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4900";
            //rule.ARComment = "Y = true (Default); N = false;";
            //rule.ARDescr = "Rule as to whether Fore names are required for summons generation.";
            //rule.ARNumeric = 1;
            //rule.ARString = "Y";
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "4900";
            rule.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            //return rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            return value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// update Offender Details For Summons
        /// Jerry 2014-02-21 rebuild it
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool UpdateOffenderDetailsForSummons(ref string message, string lastUser)
        {
            AccusedDB accuseDB = new AccusedDB(connectionString);
            OffenderDB dbo = new OffenderDB(connectionString);
            int notIntNo = 0;

            int summIntNo = 0;
            foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    //Jerry 2014-02-21 change it, check and update all selected row
                    CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                    if (!cb.Checked)
                        continue;

                    string sumNotStatus = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["SumNotFlag"].ToString().Trim();
                    if (sumNotStatus.Equals("S"))
                    {
                        summIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["SumIntNo"].ToString());
                        if (summIntNo == 0)
                            continue;

                        notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotIntNo"].ToString());
                        if (notIntNo == 0)
                            continue;

                        try
                        {
                            //Jerry 2014-08-20 don't update offender details if there none changed 
                            OffenderDetails originalDetails = dbo.GetOffenderDetails(notIntNo);
                            if (originalDetails == null)
                                continue;
                            SIL.AARTO.DAL.Entities.TList<Accused> accusedList = accusedService.GetBySumIntNo(summIntNo);

                            if (originalDetails.PoAddress1.Trim().ToLower() == this.txtPostalAddress1.Text.Trim().ToLower() &&
                                originalDetails.PoAddress2.Trim().ToLower() == this.txtPostalAddress2.Text.Trim().ToLower() &&
                                originalDetails.PoAddress3.Trim().ToLower() == this.txtPostalAddress3.Text.Trim().ToLower() &&
                                originalDetails.PoAddress4.Trim().ToLower() == this.txtPostalAddress4.Text.Trim().ToLower() &&
                                originalDetails.PoAreaCode.Trim().ToLower() == this.txtPostalAreaCode.Text.Trim().ToLower() &&
                                originalDetails.ForeNames.Trim().ToLower() == this.txtFullName.Text.Trim().ToLower() &&
                                originalDetails.Surname.Trim().ToLower() == this.txtSurName.Text.Trim().ToLower() &&
                                originalDetails.Initials.Trim().ToLower() == this.txtInitials.Text.Trim().ToLower() &&
                                originalDetails.Address1.Trim().ToLower() == this.txtStreetAddress1.Text.Trim().ToLower() &&
                                originalDetails.Address2.Trim().ToLower() == this.txtStreetAddress2.Text.Trim().ToLower() &&
                                originalDetails.Address3.Trim().ToLower() == this.txtStreetAddress3.Text.Trim().ToLower() &&
                                originalDetails.Address4.Trim().ToLower() == this.txtStreetAddress4.Text.Trim().ToLower() &&
                                originalDetails.AreaCode.Trim().ToLower() == this.txtStreetAreaCode.Text.Trim().ToLower() &&
                                originalDetails.CellNo.Trim().ToLower() == this.txtMobileNo.Text.Trim().ToLower() &&
                                originalDetails.HomeNo.Trim().ToLower() == this.txtLandLine.Text.Trim().ToLower() &&
                                accusedList.Count > 0)
                                continue;

                            OffenderDetails details = new OffenderDetails();

                            details.PoAddress1 = this.txtPostalAddress1.Text;
                            details.PoAddress2 = this.txtPostalAddress2.Text;
                            details.PoAddress3 = this.txtPostalAddress3.Text;
                            details.PoAddress4 = this.txtPostalAddress4.Text;
                            details.PoAreaCode = this.txtPostalAreaCode.Text;
                            details.ForeNames = this.txtFullName.Text;
                            details.Surname = this.txtSurName.Text;
                            details.IDNumber = this.txtIdentityNumber.Text;
                            details.Address1 = this.txtStreetAddress1.Text;
                            details.Address2 = this.txtStreetAddress2.Text;
                            details.Address3 = this.txtStreetAddress3.Text;
                            details.Address4 = this.txtStreetAddress4.Text;
                            details.AreaCode = this.txtStreetAreaCode.Text;
                            details.CellNo = this.txtMobileNo.Text;
                            details.HomeNo = this.txtLandLine.Text;
                            details.LastUser = lastUser;

                            int effort = accuseDB.UpdateAccused(details, summIntNo, ref message);
                            //Jerry 2014-01-22 if there has error it will be wrote to log file
                            if (!string.IsNullOrEmpty(message))
                            {
                                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteLog("Exception", SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString(), message);
                                break;
                            }
                        }
                        catch (Exception ex)//Jerry 2014-01-22 add
                        {
                            SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
                            message = ex.Message;
                            break;
                        }
                    }
                }
            }

            if (summIntNo == 0)
            {
                message = "NoSummons";
            }

            if (string.IsNullOrEmpty(message))
                return true;
            else
                return false;

            #region Jerry 2014-02-21 change it, check and update all selected row
            //try
            //{
            //    OffenderDetails details = new OffenderDetails();
            //    AccusedDB accuseDB = new AccusedDB(connectionString);

            //    details.PoAddress1 = this.txtPostalAddress1.Text;
            //    details.PoAddress2 = this.txtPostalAddress2.Text;
            //    details.PoAddress3 = this.txtPostalAddress3.Text;
            //    details.PoAddress4 = this.txtPostalAddress4.Text;
            //    details.PoAreaCode = this.txtPostalAreaCode.Text;
            //    details.ForeNames = this.txtFullName.Text;
            //    details.Surname = this.txtSurName.Text;
            //    details.IDNumber = this.txtIdentityNumber.Text;

            //    details.Address1 = this.txtStreetAddress1.Text;
            //    details.Address2 = this.txtStreetAddress2.Text;
            //    details.Address3 = this.txtStreetAddress3.Text;
            //    details.Address4 = this.txtStreetAddress4.Text;
            //    details.AreaCode = this.txtStreetAreaCode.Text;
            //    details.CellNo = this.txtMobileNo.Text;
            //    details.HomeNo = this.txtLandLine.Text;

            //    int effort = accuseDB.UpdateAccused(details, summIntNo, ref message);
            //    //Jerry 2014-01-22 if there has error it will be wrote to log file
            //    if (!string.IsNullOrEmpty(message))
            //    {
            //        SIL.AARTO.BLL.EntLib.EntLibLogger.WriteLog("Exception", SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString(), message);
            //    }

            //    if (effort > 0)
            //    {
            //        return true;
            //    }
            //}
            //catch (Exception ex)//Jerry 2014-01-22 add
            //{
            //    SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
            //    message = ex.Message;
            //    return false;
            //}

            //return false;
            #endregion

        }

        private DataTable MakeSummonsTable()
        {
            // Create a new DataTable titled 'Names.'
            DataTable table = new DataTable("Summons");

            //Jerry 2014-02-20 the column ChgIntNo on the left hand side must be removed
            // Add three column objects to the table.
            //DataColumn dcChgIntNo = new DataColumn();
            //dcChgIntNo.DataType = System.Type.GetType("System.Int32");
            //dcChgIntNo.ColumnName = "ChgIntNo";
            //table.Columns.Add(dcChgIntNo);

            DataColumn dcSumIntNo = new DataColumn();
            dcSumIntNo.DataType = System.Type.GetType("System.Int32");
            dcSumIntNo.ColumnName = "SumIntNo";
            table.Columns.Add(dcSumIntNo);

            DataColumn dcSummonsNo = new DataColumn();
            dcSummonsNo.DataType = System.Type.GetType("System.String");
            dcSummonsNo.ColumnName = "SummonsNo";
            table.Columns.Add(dcSummonsNo);

            DataColumn dcPrintFile = new DataColumn();
            dcPrintFile.DataType = System.Type.GetType("System.String");
            dcPrintFile.ColumnName = "PrintFile";
            table.Columns.Add(dcPrintFile);

            DataColumn dcAutIntNo = new DataColumn();
            dcAutIntNo.DataType = System.Type.GetType("System.Int32");
            dcAutIntNo.ColumnName = "PrintAutIntNo";
            table.Columns.Add(dcAutIntNo);

            DataColumn dcSumNotFlag = new DataColumn();
            dcSumNotFlag.DataType = System.Type.GetType("System.String");
            dcSumNotFlag.ColumnName = "SumNotFlag";
            table.Columns.Add(dcSumNotFlag);

            //jerry 2011-12-09 add
            DataColumn dcCPAText = new DataColumn();
            dcCPAText.DataType = System.Type.GetType("System.String");
            dcCPAText.ColumnName = "CPAText";
            table.Columns.Add(dcCPAText);

            // Create an array for DataColumn objects.
            // FBJ (2009-06-25): this contraint can cause exceptions, so I commented it for now
            //DataColumn[] keys = new DataColumn[1];
            //keys[0] = dcChgIntNo;
            //table.PrimaryKey = keys;

            // Return the new DataTable.
            return table;
        }

        private int CheckClerkCourt(int cofcIntNo)
        {
            if (cofcIntNo <= 0)
            {
                return -1;
            }
            ClerkofCourtDB cofcourtDB = new ClerkofCourtDB(connectionString);
            ClerkOfCourt cofCourt = cofcourtDB.GetClerkofCourtByNo(cofcIntNo);
            if (cofCourt.CofCName == "")
            {
                return -2;
            }

            ViewState["TOIntNo"] = 0;
            //Jerry 2014-03-13 change it
            //ViewState["OfficerNo"] = cofCourt.CofCIntNo;
            var officerNo = cofCourt.CofCSurName + ", " + cofCourt.CofCName;
            ViewState["OfficerNo"] = officerNo.Length > 20 ? officerNo.Substring(0, 20) : officerNo;
            //Jerry 2014-03-24 changed by Paole required
            //ViewState["OfficerName"] = cofCourt.CofCName;
            ViewState["OfficerName"] = officerNo.Length > 100 ? officerNo.Substring(0, 100) : officerNo;
            return 1;
        }

        private int CheckOfficerNo(string toNo)
        {
            //bool proceed = true;
            string toName = "";

            if (toNo.Equals(""))
            {
                //lblMessage.Text = "Please enter a valid officer no.";
                //lblMessage.Visible = true;
                //proceed = false;
                return -1;
            }

            TrafficOfficerDB officer = new TrafficOfficerDB(connectionString);
            int toIntNo = officer.ValidateTrafficOfficer(autIntNo, toNo, ref toName);
            int iResult;
            if (toIntNo < 1)
            {
                //lblMessage.Text = "Please enter a valid officer no.";
                //lblMessage.Visible = true;
                //proceed = false;
                iResult = -2;
            }
            else
            {
                ViewState["TOIntNo"] = toIntNo;
                ViewState["OfficerNo"] = toNo;
                ViewState["OfficerName"] = toName;
                iResult = 1;
            }

            return iResult;
        }

        Dictionary<int, string> printQList = new Dictionary<int, string>();
        /// <summary>
        /// 20090915 FT merge generate summons process.
        /// Jerry 2014-01-22 add ref string errorMessage parameter
        /// Jerry 2014-02-12 add ref bool isSucessful param
        /// Jerry 2014-02-28 delete int chgIntNo parameter
        /// </summary>
        private void GenerateSummons(int notIntNo, int ssIntNo, string ticketNo, string proxyFlag, Int64 chargeRowVersion, string summNotStatus, int sumIntNo, int isBatchPrint, string notFilmType, ref bool isSuccessful, ref string errorMessage)
        {
            string sumStatus = (string)GetLocalResourceObject("SUM_STATUS");

            //mrs 20080812 get fk's etc from viewstate
            int autIntNo = Convert.ToInt32(ViewState["AutIntNo"]);

            #region Jerry 2014-02-11 invisible court/courtroom DDL, it comes from notice
            //int crtIntNo = Convert.ToInt32(ViewState["CrtIntNo"]);
            //int crtRIntNo = Convert.ToInt32(ViewState["CrtRIntNo"]);

            Notice noticeEntity = noticeService.GetByNotIntNo(notIntNo);
            if (noticeEntity == null)
            {
                errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text51"), notIntNo.ToString(), ticketNo);
                return;
            }

            //Jerry 2014-10-22 move the offence date check in here
            DateTime offenceDate = noticeEntity.NotOffenceDate;
            if (offenceDate.AddMonths(18).Ticks < DateTime.Now.Ticks)
            {
                errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text57"), "NoticeTicketNo: " + ticketNo, offenceDate.ToString("yyyy-MM-dd HH:mm:ss"));
                return;
            }

            string courtNo = noticeEntity.NotCourtNo;
            if (string.IsNullOrEmpty(courtNo))
            {
                errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text52"), notIntNo.ToString(), ticketNo);
                return;
            }
            SIL.AARTO.DAL.Entities.Court courtEntity = courtService.GetByCrtNo(courtNo.Trim());
            if (courtEntity == null)
            {
                errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text53"), courtNo, ticketNo);
                return;
            }
            int crtIntNo = courtEntity.CrtIntNo;
            CourtRoomDB courtRoomDB = new CourtRoomDB(this.connectionString);
            SqlDataReader reader = courtRoomDB.GetActiveCourtRooms(crtIntNo);
            if (!reader.Read())
            {
                reader.Dispose();
                errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text54"), crtIntNo.ToString(), ticketNo);
                return;
            }

            int crtRIntNo = Convert.ToInt32(reader["CrtRIntNo"].ToString());
            reader.Dispose();

            SIL.AARTO.DAL.Entities.CourtRoom courtRoomEntity = courtRoomService.GetByCrtRintNo(crtRIntNo);
            if (courtRoomEntity == null)
            {
                errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text59"), courtNo, ticketNo);
                return;
            }
            #endregion

            string errMessage = string.Empty;
            string sumLocation = "Road Block";

            // Get the CourtDateRule
            //DateRulesDetails ruleCourtDate = this.CheckRoadBlockDataRule();

            // Get the Section 56 auth rules
            AuthorityRulesDetails authRules = new AuthorityRulesDetails();
            authRules.AutIntNo = autIntNo;
            authRules.ARCode = "5020";
            authRules.LastUser = this.login;

            DefaultAuthRules ar = new DefaultAuthRules(authRules, this.connectionString);
            KeyValuePair<int, string> details = ar.SetDefaultAuthRule();
            double dPercentage = details.Key;

            //Jerry 2012-05-17 add 3040 court rule
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = Convert.ToInt32(crtIntNo);
            courtRulesDetails.CRCode = "3040";
            courtRulesDetails.LastUser = this.login;
            DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
            KeyValuePair<int, string> rule3040 = courtRule.SetDefaultCourtRule();

            //jerry 2012-03-30 add court rule
            courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = Convert.ToInt32(crtIntNo);
            courtRulesDetails.CRCode = "3030";
            courtRulesDetails.LastUser = this.login;
            courtRule = new DefaultCourtRules(courtRulesDetails, this.connectionString);
            KeyValuePair<int, string> rule3030 = courtRule.SetDefaultCourtRule();

            //jerry 2012-03-30 add
            bool isNoAOG = false;
            string isSection35 = "N";
            List<string> chgIntNoList = new List<string>();
            List<string> chgRowVersionList = new List<string>();

            #region Jerry 2013-11-28 get one row per notice
            //DataView noticeSource = new DataView(GetSumNoticeRoadBlockSource(this.txtIdentityNumber.Text.Trim(), Convert.ToString(ViewState[AUTHORISED_AUTINTNOS])).Tables[0]);
            //noticeSource.RowFilter = string.Format("NotIntNo = {0}", notIntNo);
            //foreach (DataRowView dr in noticeSource)
            //{
            //    chgIntNoList.Add(dr["ChgIntNo"].ToString());
            //    chgRowVersionList.Add(dr["ChargeRowVersion"].ToString());
            //    if (dr["NoAOG"].ToString() == "Y")
            //    {
            //        isNoAOG = true;
            //    }
            //}
            #endregion

            SIL.AARTO.DAL.Entities.TList<Charge> chargeList = new ChargeService().GetByNotIntNo(notIntNo);
            Notice notice = new NoticeService().GetByNotIntNo(notIntNo);
            foreach (Charge charge in chargeList)
            {
                ChargeDetails chargeDetails = new ChargeDB(connectionString).GetChargeDetails(charge.ChgIntNo);
                chgIntNoList.Add(charge.ChgIntNo.ToString());
                chgRowVersionList.Add(chargeDetails.RowVersion.ToString());
                if (charge.ChgNoAog.ToString() == "Y")
                {
                    isNoAOG = true;
                }

                if (notice.IsSection35)
                {
                    isSection35 = "Y";
                }
            }

            string needRestrictNoAOG = null;
            //Jerry 2012-05-21 add
            string needSeparateNoAOG = null;

            if (rule3040.Value == "Y")
            {
                if (isNoAOG)
                {
                    needSeparateNoAOG = "Y";
                }
            }
            else
            {
                if (isNoAOG && rule3030.Value == "Y")
                {
                    needRestrictNoAOG = "Y";
                }
            }

            // Update the court date
            TMSData db = new TMSData(connectionString);
            //mrs 20080811 changed to autintno, crtRintno & removed acintno return date or null
            //jerry 2011-11-15 add parameter
            //jerry 2012-03-30 add needRestrictNoAOG para
            // Jake 2013-12-26 added parameter IsSection35
            DateTime courtDate = db.GetSummonsCourtDate(autIntNo, crtIntNo, this.CheckRoadBlockDataRule(), dPercentage, (string)GetLocalResourceObject("PROCESS_NAME"), notFilmType, ref crtRIntNo, needRestrictNoAOG, needSeparateNoAOG, isSection35);

            // Log the detailed problem
            switch (crtRIntNo)
            {
                case -1:
                    errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text3"), "NoticeTicketNo: " + ticketNo,
                        "CourtName: " + courtEntity.CrtName, "CourtRoomName: " + courtRoomEntity.CrtRoomName);//Jerry 2014-01-22 add
                    //lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text3"), "NoticeTicketNo: " + ticketNo);
                    return;

                case -2:
                    errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text4"), "NoticeTicketNo: " + ticketNo,
                        "CourtName: " + courtEntity.CrtName);
                    //errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text4"), "NoticeTicketNo: " + ticketNo,
                    //    "CourtName: " + courtEntity.CrtName, "CourtRoomName: " + courtRoomEntity.CrtRoomName);//Jerry 2014-01-22 add
                    //lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text4"), "NoticeTicketNo: " + ticketNo);
                    return;
            }

            #region Jerry 2014-10-22 rebuild it by bontq 1624, put it at row 1529
            //Jerry 2014-02-25 check NoticeStatus == 591 and courtDate > offenceDate + 18 months
            //if (noticeEntity.NoticeStatus == (int)ChargeStatusList.UnableToSummonsInsufficientTimeBeforeExpiry)
            //{
            //    DateTime offenceDate = noticeEntity.NotOffenceDate.Date;
            //    if (courtDate > offenceDate.AddMonths(18))
            //    {
            //        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text57"), courtDate.ToShortDateString(),
            //           "NoticeTicketNo: " + ticketNo + ", NoticeStatus: " + (int)ChargeStatusList.UnableToSummonsInsufficientTimeBeforeExpiry, offenceDate.ToShortDateString());
            //        return;
            //    }
            //}
            #endregion

            // Create the summons number
            SummonsNumbers sn = new SummonsNumbers();

            // Date Rule for Last AOG
            DateTime dtLastAOG = this.CheckLastAOGDate(courtDate);

            // DateRule for Summons Served By
            DateTime dtSumServedBy = this.CheckSummonsServedBy(courtDate);



            //need to get court name
            //CourtDB crt = new CourtDB(connectionString);
            //CourtDetails crtDetails = crt.GetCourtDetails(crtIntNo);

            //need to get summons server code
            SummonsServerDB ssDB = new SummonsServerDB(connectionString);
            SummonsServerDetails ssDetails = new SummonsServerDetails();

            //ssDetails = ssDB.GetSummonsServerDetails(0, asIntNo);
            //ssDetails = ssDB.GetSummonsServerDetails(ssIntNo, 0);


            //dls 2011-08-12 - this is no longer required --> CPA6 is actually the summons document for S54 Handwritten (S341 Parking)
            // CPA6 for impersonal summons and CPA5 for summons personal
            //AuthorityRulesDetails stationeryTypeRule = new AuthorityRulesDetails();
            //stationeryTypeRule.AutIntNo = autIntNo;
            //stationeryTypeRule.ARCode = "5070";
            //stationeryTypeRule.LastUser = this.login;

            //ar = new DefaultAuthRules(stationeryTypeRule, this.connectionString);
            //KeyValuePair<int, string> stationeryRule = ar.SetDefaultAuthRule();
            //string summonsPrintStationeryType = "CPA5";

            //wl 20090415 - assign the different fine amount based on original fine rule
            AuthorityRulesDetails originalFineRule = new AuthorityRulesDetails();
            originalFineRule.AutIntNo = autIntNo;
            originalFineRule.ARCode = "5030";
            originalFineRule.LastUser = this.login;

            ar = new DefaultAuthRules(originalFineRule, this.connectionString);
            KeyValuePair<int, string> originalFineAmntRule = ar.SetDefaultAuthRule();

            //int addSumIntNo = db.AddSummons(notIntNo, chgIntNo, sumStatus, ticketNo, proxyFlag, stationeryRule.Value,
            //   courtDate, dtLastAOG, dtSumServedBy, this.login, 0, sumLocation, chargeRowVersion,
            //   crtIntNo, autIntNo, crtRIntNo, originalFineAmntRule.Value, ref errMessage, rbsIntNo, Convert.ToInt32(ViewState["TOIntNo"].ToString()), ViewState["OfficerNo"].ToString(), ViewState["OfficerName"].ToString(), sumIntNo, ssIntNo, isBatchPrint);
            // Oscar 20101112 - changed 
            // Oscar 20101112 - Get multiple charges
            // jerry 2012-03-30 change
            //List<string> chgIntNoList = new List<string>();
            //List<string> chgRowVersionList = new List<string>();
            //DataView noticeSource = new DataView(GetSumNoticeRoadBlockSource(this.txtIdentityNumber.Text.Trim(), Convert.ToString(ViewState[AUTHORISED_AUTINTNOS])).Tables[0]);
            //noticeSource.RowFilter = string.Format("NotIntNo = {0}", notIntNo);
            //foreach (DataRowView dr in noticeSource)
            //{
            //    chgIntNoList.Add(dr["ChgIntNo"].ToString());
            //    chgRowVersionList.Add(dr["ChargeRowVersion"].ToString());
            //}

            //dls 2011-12-06 Roadblock Summons are created in batches according to the nuo. of summons generated for a specific offender at one time ==> no batchsize required
            //// jerry 20101229 add rule to control the batches size
            //AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            //arDetails.AutIntNo = autIntNo;
            //arDetails.ARCode = "5100";
            //arDetails.LastUser = this.login;

            //DefaultAuthRules arDefaultRule = new DefaultAuthRules(arDetails, this.connectionString);
            //KeyValuePair<int, string> batchSize = arDefaultRule.SetDefaultAuthRule();

            // Oscar 20111125 add transaction for push queue
            int addSumIntNo = 0;
            QueueItem item = new QueueItem();
            List<QueueItem> itemList = new List<QueueItem>();
            QueueItemProcessor queProcessor = new QueueItemProcessor();

            using (TransactionScope scope = new TransactionScope())
            {
                string printFileName;

                //2012-11-22 linda add for check new rule 5065
                AuthorityRulesDetails arDetails5065 = new AuthorityRulesDetails();
                arDetails5065.AutIntNo = this.autIntNo;
                arDetails5065.ARCode = "5065";
                arDetails5065.LastUser = this.login;
                DefaultAuthRules ar5065 = new DefaultAuthRules(arDetails5065, this.connectionString);
                bool checkResult = ar5065.SetDefaultAuthRule().Value.Equals("Y");

                string s35Permitted = CheckS35NoAOGPermitted();

                addSumIntNo = db.AddSummons(notIntNo, sumStatus, ticketNo, proxyFlag, courtDate, dtLastAOG,
                    dtSumServedBy, this.login, 0, sumLocation, crtIntNo, autIntNo, crtRIntNo, originalFineAmntRule.Value,
                    ref errMessage, rbsIntNo, Convert.ToInt32(ViewState["TOIntNo"].ToString()),
                    ViewState["OfficerNo"].ToString(), ViewState["OfficerName"].ToString(), sumIntNo,
                    ssIntNo, isBatchPrint, chgIntNoList, chgRowVersionList, 1, ViewState["PrintFileDate"].ToString(),
                    out printFileName, "N", 0, checkResult, false, s35Permitted);

                switch (addSumIntNo)
                {
                    case -1:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text5"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text5"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -2:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text6"), ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text6"), ticketNo);
                        break;
                    case -3:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text22"), crtIntNo, "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text22"), crtIntNo, "NoticeTicketNo: " + ticketNo);
                        break;
                    case -4:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text7"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text7"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -5:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text8"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text8"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -6:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text9"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text9"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -7:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text10"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text10"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -8:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text11"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text11"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -9:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text12"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text12"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -10:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text13"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text13"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -11:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text14"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text14"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -12:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text15"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text15"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -13:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text16"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text16"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -14:
                    case -15:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text17"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text17"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -16:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text18"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text18"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -17:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text19"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text19"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -18:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text20"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text20"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -19:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text43"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text43"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -20:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text44"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text44"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -21:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text45"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text45"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -22:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text46"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text46"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -23:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text47"), "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text47"), "NoticeTicketNo: " + ticketNo);
                        break;
                    case -30:
                        //this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text21"), errMessage, "NoticeTicketNo: " + ticketNo);
                        errorMessage = string.Format((string)GetLocalResourceObject("lblMessage.Text21"), errMessage, "NoticeTicketNo: " + ticketNo);
                        break;
                }

                if (addSumIntNo > 0)
                {
                    // Jerry 2012-11-20 changes, no need to push print summons queue

                    // Oscar 20111116 add push queue
                    item = new QueueItem();
                    item.Body = notIntNo.ToString();
                    item.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
                    item.LastUser = this.login;
                    itemList.Add(item);

                    /*
                    //Jake 2015-03-24 comment out ,no need to push this queue item.because this summons has been served at roadblock
                    //jerry 2011-12-26 add
                    item = new QueueItem();
                    item.Body = addSumIntNo.ToString();
                    item.Group = this.autCode.Trim();
                    item.QueueType = ServiceQueueTypeList.ExpireUnservedSummons;
                    item.ActDate = notice.NotOffenceDate.AddDays(noDaysForSummonsExpiry + 1);
                    item.LastUser = this.login;
                    itemList.Add(item);
                    */

                    //Jerry 2015-01-13 comment out StagnantSummons queue
                    //Jerry 2014-05-22 push StagnantSummons queue
                    //item = new QueueItem();
                    //item.Body = addSumIntNo.ToString();
                    //item.ActDate = courtDate.AddDays(1);
                    //item.LastUser = this.login;
                    //item.QueueType = ServiceQueueTypeList.StagnantSummons;
                    //itemList.Add(item);

                    queProcessor.Send(itemList);

                    #region update data to PrintFileName, PrintFileName_Summons
                    // Heidi 2013-09-29 add PrintFileName_Summons Conjoin when tester test 5046 task ,not insert data to PrintFileName, PrintFileName_Summons table
                    PrintFileNameService pfnSvc = new PrintFileNameService();
                    PrintFileNameSummonsService pfnsSvc = new PrintFileNameSummonsService();
                    PrintFileName pfnEntity = pfnSvc.GetByPrintFileName(printFileName);
                    if (pfnEntity == null)
                    {
                        pfnEntity = new PrintFileName()
                        {
                            PrintFileName = printFileName,
                            AutIntNo = autIntNo,
                            LastUser = this.login,
                            EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                        };
                        pfnEntity = pfnSvc.Save(pfnEntity);
                    }

                    PrintFileNameSummons pfnsEntity = pfnsSvc.GetBySumIntNoPfnIntNo(addSumIntNo, pfnEntity.PfnIntNo);
                    if (pfnsEntity == null)
                    {
                        pfnsEntity = new PrintFileNameSummons()
                        {
                            SumIntNo = addSumIntNo,
                            PfnIntNo = pfnEntity.PfnIntNo,
                            LastUser = this.login,
                            EntityState = SIL.AARTO.DAL.Entities.EntityState.Added
                        };
                        pfnsEntity = pfnsSvc.Save(pfnsEntity);
                    }

                    #endregion

                    isSuccessful = true;

                    // 2013-11-08, Oscar - we only run Complete() when success.
                    scope.Complete();
                }
                //scope.Complete();
                // 2013-11-08, Oscar added
                else
                    return;
            }

            //dls 081124 - add the new summons to our dataset for displaying and printing
            string printFile = "";
            string summonsNumber = "";

            GetSummonsDetails(notIntNo, ref printFile, ref summonsNumber);

            DataTable table;
            if (dsSummonsMultiplePrint == null)
            {
                table = MakeSummonsTable();
                dsSummonsMultiplePrint = new DataSet();
                dsSummonsMultiplePrint.Tables.Add(table);
            }
            else
            {
                table = dsSummonsMultiplePrint.Tables[0];
            }

            DataRow drSummons = table.NewRow();

            //Jerry 2014-02-20 the column ChgIntNo on the left hand side must be removed
            //drSummons["ChgIntNo"] = chgIntNo;
            drSummons["SummonsNo"] = summonsNumber;
            drSummons["SumIntNo"] = addSumIntNo;
            drSummons["PrintFile"] = printFile;
            drSummons["PrintAutIntNo"] = autIntNo;
            drSummons["SumNotFlag"] = summNotStatus;

            //Heidi 2013-11-18 changed for fixed when notFilmType='H' and IsVideo=1 ,the generated file name should be cpa5
            //NoticeService noticeservice = new NoticeService();
            noticeEntity = noticeService.GetByNotIntNo(notIntNo);

            //Jerry 2012-11-20 changes, Do not send to IBMPrinter

            //jerry 2011-12-09 add
            if (notFilmType == "H" && noticeEntity != null && noticeEntity.IsVideo == false)
            {
                drSummons["CPAText"] = "CPA6";
                btnBatchPrintCPA6.Enabled = true;
            }
            else//else if (notFilmType != "H" && notFilmType != "M")
            {
                drSummons["CPAText"] = "CPA5";
                btnBatchPrintCPA5.Enabled = true;
            }

            table.Rows.Add(drSummons);

            ViewState["dsSummonsMultiplePrint"] = dsSummonsMultiplePrint;

            // Cleanup
            //mrs20081119 no longer used
            //this.ViewState.Remove("ACIntNo");
            this.ViewState.Remove("FineAmount");
            //this.ViewState.Remove("CrtIntNo"); //mrs 20081125 can't do this results in a crash on the next loop iteration

            //SHOW LINK TO PRINT SUMMONS
            this.ViewState.Add("PrintFile", printFile);
            this.ViewState.Add("printAutIntNo", this.autIntNo);
        }

        private void GetSummonsDetails(int notIntNo, ref string printFile, ref string summonsNumber)
        {
            SummonsDB summons = new SummonsDB(this.connectionString);
            SqlDataReader reader = summons.SummonsAndWarrantDetails(notIntNo);

            while (reader.Read())
            {
                printFile = reader["SumPrintFileName"].ToString();
                summonsNumber = reader["SummonsNo"].ToString();
            }

            reader.Dispose();
        }

        private void GetSummonsDetails(string ticketNo, ref string printFile, ref string summonsNumber, ref string AutIntNo)
        {
            SummonsDB summons = new SummonsDB(this.connectionString);
            SqlDataReader reader = summons.SummonsAndWarrantDetailsByTicketNo(ticketNo);

            while (reader.Read())
            {
                printFile = reader["SumPrintFileName"].ToString();
                summonsNumber = reader["SummonsNo"].ToString();
                AutIntNo = reader["AutIntNo"].ToString();
            }

            reader.Dispose();
        }

        protected void PopulateMultipleSummons()
        {
            this.grdSumonsMultiplePrint.DataSource = dsSummonsMultiplePrint;
            //Jerry 2014-02-20 the column ChgIntNo on the left hand side must be removed
            //this.grdSumonsMultiplePrint.DataKeyNames = new string[] { "ChgIntNo", "SumIntNo", "PrintFile", "PrintAutIntNo", "SummonsNo" };
            this.grdSumonsMultiplePrint.DataKeyNames = new string[] { "SumIntNo", "PrintFile", "PrintAutIntNo", "SummonsNo", "CPAText" };

            try
            {
                this.grdSumonsMultiplePrint.DataBind();
            }
            catch
            {
                this.grdSumonsMultiplePrint.PageIndex = 0;
                this.grdSumonsMultiplePrint.DataBind();
            }
            grdSumonsMultiplePrint.Visible = true;
        }

        private bool IsRoadblockSessionActive()
        {
            bool response = false;
            if (Session["RBSIntNo"] != null)
            {
                this.rbsIntNo = (int)Session["RBSIntNo"];

                RoadBlockDB db = new RoadBlockDB(this.connectionString);
                response = db.IsRoadblockSessionActive(this.rbsIntNo);
            }

            if (!response)
                Response.Redirect("RoadBlockSession.aspx");

            return response;
        }

        #endregion

        #region System Events

        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = Application["constr"].ToString();
            //connectionString = "Data Source=Jakezyz;Initial Catalog=TMS;Uid=sa;pwd=sa";
            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Make sure the roadblock is still active
            this.IsRoadblockSessionActive();

            // Get user details
            UserDB user = new UserDB(connectionString);
            UserDetails userDetails = new UserDetails();
            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            ViewState.Add("AutIntNo", autIntNo);

            // Set domain specific variables
            General gen = new General();
            background = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            #region Jerry 2014-10-21 delete this by Bontq 1601 client required.
            ////Jerry 2013-03-13 add
            //this.RepresentationOnTheFly1.RepresentationSuccessful += new RepresentationOnTheFlyEventHandler(RepresentationOnTheFly1_RepresentationSuccessful);
            //RepresentationOnTheFly1.AutIntNo = this.autIntNo;
            #endregion

            HtmlLink html = new HtmlLink();
            html.Href = styleSheet;

            html.Attributes.Add("rel", "stylesheet");
            html.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(html);

            if (ViewState["dPercentage"] == null)
            {
                GetDefaultCourtPercentage();
                ViewState["dPercentage"] = dPercentage.ToString();
            }
            else
            {
                dPercentage = double.Parse(ViewState["dPercentage"].ToString());
            }

            //jerry 2011-12-26 add for pushing queue of cancel expired summons
            GetDefaultDateRule();
            GetAutCode();
            //Jerry 2014-03-18 add
            GetAuthorityRule(this.autIntNo);

            if (!IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblBlockSession.Text");
                hidRBSIntNo.Value = rbsIntNo.ToString();

                this.ShowRoadBlockSessionDetail(rbsIntNo);
                //Jerry 2014-02-10 comment out it, the court got from notice
                //this.PopulateCourt();
                this.PopulateSummonsServerStaff();
                this.btnGenerateSummons.Enabled = false;

                this.VisiblePanel(new string[] { this.pnlSessionIDFilter.ID });
            }
        }

        #region Jerry 2014-10-21 delete this by Bontq 1601 client required.
        /// <summary>
        /// Jerry 2013-03-13 add
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void RepresentationOnTheFly1_RepresentationSuccessful(object sender, RepresentationOnTheFlyEventArgs e)
        //{
        //    #region Jerry 2014-03-17 rebuild it
        //    //Jerry 2013-03-13 get new charge rowversion
        //    //this.ViewState.Remove("SumNoticeRoadBlock");
        //    //string idNumber = txtIdentityNumber.Text.Trim();
        //    //string autIntNos = (string)this.ViewState[AUTHORISED_AUTINTNOS];
        //    //int totalCount = 0;
        //    //GetSumNoticeRoadBlockSourceRefresh(idNumber, autIntNos, grdSummonsNoticesList.PageSize, grdSummonsNoticesList.PageIndex, out totalCount, this.txtQueryRegNo.Text.Trim(), this.txtQuerySurname.Text.Trim(), this.txtQueryInitials.Text.Trim());
        //    #endregion

        //    BindData();
        //    string notTicketNo = ViewState["NotTicketNo"].ToString();
        //    if (!string.IsNullOrEmpty(notTicketNo))
        //    {
        //        foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
        //        {
        //            if (grd.RowType == DataControlRowType.DataRow)
        //            {
        //                CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
        //                string ticketNo = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["TicketNo"].ToString();
        //                if (ticketNo != notTicketNo)
        //                {
        //                    cb.Checked = false;
        //                }
        //                else
        //                    cb.Checked = true;

        //                cb.Enabled = false;
        //            }
        //        }
        //    }

        //    this.ViewState.Add("FineAmount", e.NewAmount);
        //    string areaCode = this.grdSummonsNoticesList.DataKeys[0]["AreaCode"].ToString();
        //    this.ShowSummonsServer(areaCode);
        //    //Jerry 2014-03-17 change it and add notTicketNo for message
        //    //lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text42"); //"Representation Successful";
        //    lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text42"), notTicketNo);
        //    VisiblePanel(new string[] { this.pnlSummonsServer.ID, this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID });
        //    SummonsServerStaffVisible();
        //    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        //    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
        //    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsRoadBlockMakeRepresentation, PunchAction.Change);  

        //}
        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            #region Jerry 2014-01-24 comment out
            //lblMessage.Text = "";
            //this.BindData();

            //if (grdSummonsNoticesList.Rows.Count > 0)
            //{
            //    lblResultTitle.Text = string.Format((string)GetLocalResourceObject("lblResultTitle.Text"), txtIdentityNumber.Text);
            //     2013-11-29, Oscar changed
            //    var sb = new StringBuilder((string)GetLocalResourceObject("lblResultTitle.Text"));
            //    if (!string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text))
            //        sb.AppendFormat((string)GetLocalResourceObject("OfID"), this.txtIdentityNumber.Text);
            //    if (!string.IsNullOrWhiteSpace(this.txtQueryRegNo.Text))
            //        sb.AppendFormat((string)GetLocalResourceObject("OfRegNo"), this.txtQueryRegNo.Text);
            //    if (!string.IsNullOrWhiteSpace(this.txtQuerySurname.Text))
            //        sb.AppendFormat((string)GetLocalResourceObject("OfSurname"), this.txtQuerySurname.Text);
            //    if (!string.IsNullOrWhiteSpace(this.txtQueryInitials.Text))
            //        sb.AppendFormat((string)GetLocalResourceObject("OfInitials"), this.txtQueryInitials.Text);
            //    this.lblResultTitle.Text = sb.ToString();

            //    int notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[0].Value.ToString());
            //    VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlOffenceDetail.ID });
            //    ShowOffenceDetail(notIntNo);
            //}
            //else
            //{
            //    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text23");
            //    VisiblePanel(new string[] { this.pnlSessionIDFilter.ID });
            //}
            #endregion

            if (ViewState["ProcessedCount"] != null)
                ViewState["ProcessedCount"] = 0;

            ViewState["IdNumberSelected"] = null;
            ViewState["SurnameSelected"] = null;
            ViewState["InitialsSelected"] = null;

            Search();
        }

        /// <summary>
        /// Add validation to the telephone field on the screen
        /// Jerry 2014-03-04 rebuild it
        /// </summary>
        /// <returns></returns>
        protected bool ValidatePhone()
        {
            #region Jerry 2014-03-04 rebuild it
            //string strMatch = "0123456789()-+.";
            //string cellPhone = this.txtMobileNo.Text;
            //char[] arrCell = cellPhone.ToCharArray();
            //foreach (char C in arrCell)
            //{
            //    if (C.ToString().Trim() != "" && strMatch.IndexOf(C.ToString()) < 0)
            //    {
            //        lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text49");
            //        return false;
            //    }
            //}
            //string landLine = this.txtLandLine.Text.Trim();
            //char[] arrL = landLine.ToCharArray();
            //foreach (char L in arrL)
            //{
            //    if (L.ToString().Trim() != "" && strMatch.IndexOf(L.ToString()) < 0)
            //    {
            //        lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text50");
            //        return false;
            //    }
            //}
            //return true;
            #endregion

            bool isCorrect = SIL.AARTO.BLL.Utility.Validate.CheckPhoneNumber(this.txtMobileNo.Text.Trim());
            if (!isCorrect)
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text49");
                return false;
            }

            isCorrect = SIL.AARTO.BLL.Utility.Validate.CheckPhoneNumber(this.txtLandLine.Text.Trim());
            if (!isCorrect)
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text50");
                return false;
            }

            return true;
        }

        protected void btnUpdateOffenceder_Click(object sender, EventArgs e)
        {
            try
            {
                this.lblMessage.Text = "";
                if (string.IsNullOrEmpty(txtSurName.Text.Trim()))
                {
                    this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text68");
                    return;
                }
                if (string.IsNullOrEmpty(txtInitials.Text.Trim()))
                {
                    this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text69");
                    return;
                }

                //#5165:Add validation to the telephone field on the screen. update by Teresa 2014/1/3
                if (!ValidatePhone())
                {
                    return;
                }

                #region Jerry 2014-03-13 rebuild it
                ////Jerry 2013-11-28 get one row per notice
                //int notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[0].Value.ToString());
                //SIL.AARTO.DAL.Data.ChargeQuery query = new SIL.AARTO.DAL.Data.ChargeQuery();
                //query.AppendEquals(ChargeColumn.NotIntNo, notIntNo.ToString());
                //query.AppendEquals(ChargeColumn.ChgIsMain, "true");
                //Charge charge = chargeService.Find(query)[0];


                ////Jerry 2014-02-18 rechange it 
                //decimal revisedAmount = Convert.ToDecimal(this.grdSummonsNoticesList.DataKeys[0]["RevisedAmount"].ToString());
                ////string noaog = (string)this.grdSummonsNoticesList.DataKeys[0]["NoAOG"];
                ////decimal revisedAmount = (decimal)charge.ChgRevFineAmount;
                //string noaog = (string)charge.ChgNoAog;

                //this.ViewState.Add("RevisedAmount", revisedAmount);
                //this.ViewState.Add("noaog", noaog);
                #endregion

                // FBJ (2009-06-23): Check forenames authority rule and validate data
                if (this.CheckForenamesRule())
                {
                    if (this.txtFullName.Text.Trim().Length == 0)
                    {
                        this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text24");
                        return;
                    }
                }

                bool hasCheckedData = false;
                foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
                {
                    if (grd.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                        if (cb.Checked)
                        {
                            hasCheckedData = true;
                            break;
                        }
                    }
                }
                //none checked any row
                if (!hasCheckedData)
                {
                    this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text81");
                    return;
                }

                //Jerry 2014-05-04 add transaction
                using (TransactionScope scope = SIL.ServiceBase.ServiceUtility.CreateTransactionScope())
                {
                    string message = string.Empty;
                    bool b = UpdateOffenderDetailsForSummons(ref message, this.login);
                    if (!b)
                    {
                        if (!message.Equals("NoSummons"))
                        {
                            //Jerry 2014-01-22 change it
                            //this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text25");
                            this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text25") + (message.Length > 0 ? " - " + message : string.Empty);
                            return;
                        }
                    }

                    //Jerry 2014-05-30 add notice comment
                    message = string.Empty;
                    UpdateNoticeComment(ref message);
                    if (!string.IsNullOrEmpty(message))
                    {
                        this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text79") + " - " + message;
                        return;
                    }

                    message = string.Empty;
                    b = UpdateOffenderDetailsForNotice(ref message, this.login);
                    if (!b)
                    {
                        this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text25") + (message.Length > 0 ? " - " + message : string.Empty);
                        return;
                    }

                    //Jerry 2014-04-28 add data washing
                    message = string.Empty;
                    DataWashing(ref message);
                    if (!string.IsNullOrEmpty(message))
                    {
                        this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text70") + " - " + message;
                        return;
                    }

                    scope.Complete();
                }

                #region Jerry 2014-03-14 rebuild it, show NoAog decision panel if allow NoAog representation
                //Jerry 2014-03-13 rechange it
                //if (ViewState["noaogDecision"] == null)
                //{
                //decimal revisedAmount = (decimal)this.ViewState["RevisedAmount"];

                // If the offence is a noaog ask the user what to do about it
                // we should only allow them to do a rep if the revised amount is still 0, otherwise, 
                // it means a rep has already been done, so don't want to do it again!
                //if (ViewState["noaog"].ToString().Equals("Y") || (revisedAmount == 0 || revisedAmount >= 999999))
                //if(!string.IsNullOrEmpty(noAog) && noAog.Equals("Y"))
                //{
                //    if (revisedAmount == 0 || revisedAmount >= 999999)
                //    {
                //        // Itis a no admission of Guilt
                //        VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlNOAOGDecision.ID });
                //        return;
                //    }
                //}
                //}
                #endregion

                //rebind grdSummonsNoticesList to update Surname, Initials changed
                if (ViewState["UpdateNoticeList"] != null)
                {
                    List<int> updateNoticeList = (List<int>)ViewState["UpdateNoticeList"];
                    if (updateNoticeList.Count > 0)
                    {
                        this.BindIDNumberGrid();
                        this.BindData();
                        if (this.grdSummonsNoticesList.Rows.Count < 1)
                        {
                            lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text23");
                            VisiblePanel(new string[] { this.pnlSessionIDFilter.ID });
                            return;
                        }

                        foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
                        {
                            if (grd.RowType == DataControlRowType.DataRow)
                            {
                                int currentNotIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotIntNo"].ToString());
                                CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                                if (updateNoticeList.Contains(currentNotIntNo))
                                    cb.Checked = true;
                                else
                                    cb.Checked = false;
                            }
                        }
                    }
                }

                #region Jerry 2014-10-21 delete this by Bontq 1601 client required.
                //bool allowRepresentation = false;
                //foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
                //{
                //    if (grd.RowType == DataControlRowType.DataRow)
                //    {
                //        CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                //        cb.Enabled = false;
                //        if (!cb.Checked)
                //            continue;

                //        string s35OrNoAOG = this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["S35OrNoAOG"].ToString();
                //        if (string.IsNullOrEmpty(s35OrNoAOG))
                //            continue;

                //        if (allowS35Rule && s35OrNoAOG == "S35")
                //        {
                //            allowRepresentation = true;
                //        }
                //        else if (allowNoAogRule && s35OrNoAOG == "NoAOG")
                //        {
                //            allowRepresentation = true;
                //        }
                //    }
                //}

                //if (allowRepresentation)
                //{
                //    VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlNOAOGDecision.ID });
                //    return;
                //}
                #endregion

                //this.lblMessage.Text = "Offenceder details update unsuccessful!";

                //Jerry 2014-10-12 set the check box unable after the updated offender details
                bool hasCheckedAll = true;
                foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
                {
                    if (grd.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                        if (!cb.Checked)
                            hasCheckedAll = false;

                        cb.Enabled = false;
                    }
                }
                if (!hasCheckedAll)
                    chkSelectAll.Checked = false;

                chkSelectAll.Enabled = false;

                this.pnlSummonsServer.Visible = true;
                VisiblePanel(new string[] { this.pnlSummonsServer.ID, this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID });

                string areaCode = string.Empty;
                object o = this.grdSummonsNoticesList.DataKeys[0]["AreaCode"];
                if (o != null && !(o is DBNull))
                    areaCode = o.ToString();
                this.ShowSummonsServer(areaCode);

                this.cboSummonsServerStaff.SelectedIndex = 0;
                this.cboSummonsServerStaff.Visible = false;
                this.labelSelectSummonsServerStaff.Visible = false;
                this.btnGenerateSummons.Enabled = false;

                #region Jerry 2014-02-11 no need to display this court/courtroom, it comes from notice
                //this.cboCourt.SelectedIndex = 0;
                //this.labelCourtRoom.Visible = false;
                //this.cboCourtRoom.Visible = false;
                //Jerry 2012-05-23 no need to display this court day
                //this.labelCourtDate.Visible = false;
                //this.lblCourtDate.Visible = false;
                #endregion

                this.lblMessage.Text = string.Empty;

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsRoadBlock, PunchAction.Change);
            }
            catch (Exception ex)
            {
                //throw ex;
                this.lblMessage.Text = ex.Message;
                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
            }
        }

        protected void btnGenerateSummons_Click(object sender, EventArgs e)
        {
            this.lblMessage.Text = string.Empty;

            #region Jerry 2014-02-11 invisible court/courtroom DDL, it comes from notice
            //if (this.cboCourt.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_COURT")))
            //{
            //    this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text27");
            //    return;
            //}

            //Jerry 2012-05-23 change
            //if (this.hidCourtDate.Value.Equals(string.Empty))
            //{
            //    this.lblMessage.Text = "You need to select a court where there are court dates available.";
            //    return;
            //}
            #endregion

            int rbsIntNo = Convert.ToInt32(Session["RBSIntNo"].ToString());

            bool roadBlockIsOpen = this.CheckRoadBlockSession(rbsIntNo);
            if (!roadBlockIsOpen)
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text29");
                return;
            }

            string summNotStatus = string.Empty;

            decimal revisedAmount;
            decimal originalAmount;
            string ticketNo;
            int notIntNo;
            string summonsNo;
            int chgIntNo;

            //Int64 chargeRowVersion;
            Int64 noticeRowVersion;

            int sumIntNo = 0;
            //int ssIntNo = int.Parse(this.cboSummonsServer.SelectedValue);
            // 2013-11-18, Oscar changed
            int ssIntNo = GetCboSummonsServerSelectedValue();
            //int serverType;
            DateTime offenceDate;

            //HANDLE PERSONAL DETAILS
            // Check the Auth rule for fore names
            bool mustHaveForenames = this.CheckForenamesRule();
            bool mustHaveIDNumber = this.CheckIDNumberRule();

            //At a roadblock the officer is not allowed to do a rep on a No AOG. They are allowed to re-issue a 
            //summons that is stagnant with a blank amount. The re-issue will also have a blank amount.
            //If the public prosecutor is at the roadblock they can then do a rep on the new summons, through the normal
            //rep process. Only the public prosecutor can do the rep on the No AOG.

            StringBuilder sb = new StringBuilder();
            sb.Append((string)GetLocalResourceObject("lblMessage.Text30") + "\n<ul>\n");

            int serverType = 0;

            string officerNo = string.Empty;
            int iResult = 0;
            bool isValid = true;
            // 20090914 FT get SummonsServer staff
            if (this.cboSummonsServer.SelectedIndex == 1)
            {
                string strOfficer = cboSummonsServerStaff.SelectedValue;
                officerNo = strOfficer.Substring(1);

                if (strOfficer[0] == 'C')
                {
                    serverType = (int)Stalberg.TMS.Data.ServerTypeEnum.ClerkofCourt;
                    iResult = this.CheckClerkCourt(Convert.ToInt32(officerNo));
                }
                else if (strOfficer[0] == 'O')
                {
                    serverType = (int)Stalberg.TMS.Data.ServerTypeEnum.Officer;
                    iResult = this.CheckOfficerNo(officerNo.ToString());
                }
            }
            else if (this.cboSummonsServer.SelectedIndex == 2)
            {
                ViewState["TOIntNo"] = 0;
                ViewState["OfficerNo"] = "";
                ViewState["OfficerName"] = "";
            }

            if (iResult == -1)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblMessage.Text31") + "</li>\n");
            }
            else if (iResult == -2)
            {
                isValid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("lblMessage.Text32") + "</li>\n");
            }

            //HANDLE SUMMONS LOCATION
            //string sumLocation = "Roadblock";
            //Handle Court

            #region Jerry 2014-02-11 invisible court/courtroom DDL, it comes from notice
            //if (this.cboCourt.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_COURT")))
            //{
            //    isValid = false;
            //    sb.Append("<li>" + (string)GetLocalResourceObject("lblMessage.Text33") + "</li>\n");
            //}

            //Jerry 2012-05-23 change
            //handle court date
            //DateTime dt;
            //if (!DateTime.TryParse(this.hidCourtDate.Value.ToString(), out dt))
            //{
            //    isValid = false;
            //    sb.Append("<li>The date for the court does not appear to be valid.</li>\n");
            //}
            #endregion

            if (!isValid)
            {
                sb.Append("</ul>");
                this.lblMessage.Text = sb.ToString();
                return;
            }
            int isBatchPrint = 1;
            int i = 1;
            int checkedNum = 0;

            foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                    if (cb.Checked)
                        checkedNum++;
                }
            }

            int tmpNotIntNo = 0;   // Oscar 20101112 - added, add summons by notice instead of charges.

            //dls 2011-12-06 - added so that each summons in the batch for this offender will have the same print file name
            if (ViewState["PrintFileDate"] == null)
            {
                ViewState.Add("PrintFileDate", DateTime.Now.ToString(DATE_AND_TIME_FORMAT));
            }

            //Jerry 2014-01-22 add
            StringBuilder errorMessage = new StringBuilder();
            foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    //GridViewRow
                    CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
                    if (!cb.Checked)
                        continue;

                    ticketNo = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["TicketNo"].ToString();
                    notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotIntNo"].ToString()); //Convert.ToInt32(RoadBlockItem.Cells[0].Text);

                    #region Jerry 2014-02-28 rechange it, revisedAmount and originalAmount get from SP
                    //Jerry 2013-11-28 get one row per notice
                    //SIL.AARTO.DAL.Data.ChargeQuery query = new SIL.AARTO.DAL.Data.ChargeQuery();
                    //query.AppendEquals(ChargeColumn.NotIntNo, notIntNo.ToString());
                    //query.AppendEquals(ChargeColumn.ChgIsMain, "true");
                    //Charge charge = chargeService.Find(query)[0];
                    //chgIntNo = charge.ChgIntNo;
                    //chgIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["ChgIntNo"].ToString());
                    //chargeRowVersion = Convert.ToInt64(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["ChargeRowVersion"].ToString());
                    #endregion

                    noticeRowVersion = Convert.ToInt64(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NoticeRowVersion"].ToString());
                    string proxyFlag = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotProxyFlag"].ToString();
                    summNotStatus = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["SumNotFlag"].ToString();
                    //jerry 2011-11-15 add
                    string notFilmType = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotFilmType"].ToString();

                    //ViewState["ChgIntNo"] = chgIntNo;

                    if (summNotStatus.Equals("S"))
                    {
                        //Jerry 2014-02-28 rechange it
                        revisedAmount = Convert.ToDecimal(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["RevisedAmount"].ToString()); //Convert.ToDecimal(RoadBlockItem.Cells[8].Text);
                        originalAmount = Convert.ToDecimal(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["OriginalAmount"].ToString()); // Convert.ToDecimal(RoadBlockItem.Cells[9].Text);
                        //revisedAmount = Convert.ToDecimal(charge.ChgRevFineAmount);
                        //originalAmount = Convert.ToDecimal(charge.ChgFineAmount);
                        summonsNo = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["SummonsNo"].ToString(); //RoadBlockItem.Cells[11].Text;
                        sumIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["SumIntNo"].ToString());
                        offenceDate = Convert.ToDateTime(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["OffenceDate"].ToString()); //Convert.ToDateTime(RoadBlockItem.Cells[10].Text);

                        this.ViewState.Add("RevisedAmount", revisedAmount);
                        this.ViewState.Add("FineAmount", originalAmount);
                    }
                    if (i < checkedNum)
                        isBatchPrint = 0;
                    else
                        isBatchPrint = 1;

                    //Jerry 2014-01-22 add
                    string message = string.Empty;
                    bool isSuccessful = false;
                    //this.GenerateSummons(notIntNo, chgIntNo, ssIntNo, ticketNo, proxyFlag, chargeRowVersion, summNotStatus, sumIntNo, isBatchPrint);
                    //Oscar 20101112 - changed
                    if (tmpNotIntNo != notIntNo)
                        this.GenerateSummons(notIntNo, ssIntNo, ticketNo, proxyFlag, noticeRowVersion, summNotStatus, sumIntNo, isBatchPrint, notFilmType, ref isSuccessful, ref message);
                    tmpNotIntNo = notIntNo;

                    //Jerry 2014-01-22 add
                    if (!string.IsNullOrEmpty(message))
                        //errorMessage = errorMessage + "<br/>" + message;
                        errorMessage.Append("<br/>" + message);

                    i++;
                    //Jerry 2014-02-11 display process count
                    if (isSuccessful)
                    {
                        if (ViewState["ProcessedCount"] != null)
                            processedCount = Convert.ToInt32(ViewState["ProcessedCount"].ToString());
                        processedCount++;
                        ViewState["ProcessedCount"] = processedCount;
                        currentProcessedCount++;
                    }
                }
            }

            //clear the print file name until the next batch is created
            this.ViewState.Remove("PrintFileDate");

            if (dsSummonsMultiplePrint != null)
            {
                if (dsSummonsMultiplePrint.Tables[0].Rows.Count > 0)
                {
                    VisiblePanel(new string[] { this.pnlSummonsMultiplePrint.ID });
                    PopulateMultipleSummons();

                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsRoadBlock, PunchAction.Add);
                    //sbprints.Append("</ul>");
                    //this.SetPanelVisibility(SingleSummonsState.MultipleSummonsPrint);

                    //Jerry 2014-02-11 add, display process count
                    totalItemsCount = Convert.ToInt32(ViewState["TotalItemsCount"].ToString());
                    processedCount = Convert.ToInt32(ViewState["ProcessedCount"].ToString());
                    int leftItemCount = totalItemsCount - currentProcessedCount;
                    ViewState.Add("LeftItemCount", leftItemCount);
                    lblProcessResult.Text = (string)GetLocalResourceObject("lblProcessed.Text") + processedCount.ToString() + " / " + (string)GetLocalResourceObject("lblRemaining.Text") + (leftItemCount > 0 ? leftItemCount.ToString() : "0");
                    if (leftItemCount <= 0)
                        ViewState["ProcessedCount"] = 0;

                    //Jerry 2014-02-28 add
                    if (!string.IsNullOrEmpty(errorMessage.ToString()))
                        //Jerry 2014-10-30 remove the lblMessage.Text35 message
                        //lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text35") + errorMessage.ToString();
                        lblMessage.Text = errorMessage.ToString();
                }
                else
                {
                    //sbprints.Append("<li>There are no stagnant summons for re-issue</li>\n");
                    //sbprints.Append("</ul>");
                    //Jerry 2014-01-22 change
                    //lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text35");
                    //Jerry 2014-10-30 remove the lblMessage.Text35 message
                    //lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text35") + (!string.IsNullOrEmpty(errorMessage.ToString()) ? errorMessage.ToString() : "");
                    lblMessage.Text = !string.IsNullOrEmpty(errorMessage.ToString()) ? errorMessage.ToString() : "";
                }
            }
            else
            {
                //lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text35");
                //lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text35") + (!string.IsNullOrEmpty(errorMessage.ToString()) ? errorMessage.ToString() : "");
                //Jerry 2014-10-30 remove the lblMessage.Text35 message
                lblMessage.Text = !string.IsNullOrEmpty(errorMessage.ToString()) ? errorMessage.ToString() : "";
            }
        }

        protected void btnAssign_Click(object sender, EventArgs e)
        {
            if (cboSummonsServer.SelectedIndex <= 0)
            {
                this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text36");
                return;
            }
            //20090914 FT check SummonsServer Staff selected.
            else if (this.cboSummonsServer.SelectedIndex == 1)
            {
                if (this.cboSummonsServerStaff.SelectedIndex <= 0)
                {
                    this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text37");
                    return;
                }
            }
            this.lblMessage.Text = "";
            this.VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlCourt.ID });
            //Jerry 2014-02-11 add, Jerry 2014-02-11 invisible court/courtroom DDL, it comes from notice
            this.btnGenerateSummons.Enabled = true;
        }

        protected void cboCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            //mrs 20080811 court is now parent of court room, when court selected populate courtroom
            this.cboCourtRoom.Items.Clear();
            if (Session["RBSIntNo"] == null)
            {
                Response.Redirect("RoadBlockSession.aspx");
                return;
            }

            string[] split = this.cboCourt.SelectedValue.Split(new char[] { '~' });
            if (split.Length != 2)
            {
                this.labelCourtRoom.Visible = false;
                this.cboCourtRoom.Visible = false;
                //Jerry 2012-05-23 no need to display this court day
                //this.labelCourtDate.Visible = false;
                //this.lblCourtDate.Visible = false;
                this.lblMessage.Text = string.Empty;
                return;
            }
            this.labelCourtRoom.Visible = true;
            this.cboCourtRoom.Visible = true;
            //Jerry 2012-05-23 no need to display this court day
            //this.labelCourtDate.Visible = true;
            //this.lblCourtDate.Visible = true;
            //this.lblCourtDate.Text = string.Empty;

            int crtIntNo = Convert.ToInt32(split[0]);
            ViewState["CrtIntNo"] = crtIntNo.ToString();

            CourtRoomDB db = new CourtRoomDB(connectionString);
            System.Data.SqlClient.SqlDataReader reader1 = db.GetActiveCourtRooms(crtIntNo);
            while (reader1.Read())
            {
                ListItem item = new ListItem();
                item.Value = reader1["CrtRIntNo"].ToString();
                item.Text = reader1["CrtRoomDetails"].ToString();
                this.cboCourtRoom.Items.Add(item);
            }
            reader1.Close();

            this.cboCourtRoom.Items.Insert(0, (string)GetLocalResourceObject("SELECT_COURT_ROOM"));
        }

        protected void cboCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            //mrs 20080811 moved courtdates to child of courtroom
            this.hidCourtDate.Value = string.Empty;
            //Jerry 2012-05-23 no need to display this court day
            //this.lblCourtDate.Text = NO_COURT_DATES;

            if (this.cboCourt.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_COURT")))
            {
                this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text38");
                return;
            }

            if (this.cboCourtRoom.SelectedValue.Equals((string)GetLocalResourceObject("SELECT_COURT_ROOM")))
            {
                this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text39");
                return;
            }

            int crtRIntNo = Convert.ToInt32(this.cboCourtRoom.SelectedValue.ToString());

            // Store the Court room int ID
            this.ViewState.Add("CrtRIntNo", crtRIntNo);

            //Jerry 2012-07-24 to enable GenerateSummons button
            this.btnGenerateSummons.Enabled = true;

            #region Jerry 2012-05-23 change, no need get court day in here
            //int autIntNo = Convert.ToInt32(ViewState["AutIntNo"]);

            //// Authority date rule for Summons
            ////DateRulesDetails ruleCourtDate = this.CheckDateRule();
            //int noOfDaysCourtDateRule = this.CheckRoadBlockDataRule();

            ////AuthorityRulesDB authRules = new AuthorityRulesDB(connectionString);
            ////AuthorityRulesDetails details = authRules.GetAuthorityRulesDetailsByCode(autIntNo,
            ////    "5020", "Rule to reserve % of court appearances for S56 summons", 20, "", "10 (Default)", "Opus");

            //// Check for valid court dates
            //CourtDatesDB db = new CourtDatesDB(connectionString);
            //System.Data.SqlClient.SqlDataReader reader = db.GetCourtDatesAvailableList(crtRIntNo, autIntNo, string.Empty);

            //while (reader.Read())
            //{
            //    DateTime dt = (DateTime)reader["CDate"];
            //    if (dt.Ticks < DateTime.Now.AddDays(noOfDaysCourtDateRule).Ticks)
            //        continue;

            //    int noCases = Helper.GetReaderValue<int>(reader, "CDNoOfCases");
            //    if (noCases <= 0)
            //        continue;

            //    double dNoCases = (double)noCases - ((double)noCases * (dPercentage / 100.0));

            //    int noAllocated = Helper.GetReaderValue<int>(reader, "CDS54Allocated");
            //    int noAvailableCases = (int)dNoCases - noAllocated;

            //    if (noAvailableCases > 0)
            //    {
            //        this.btnGenerateSummons.Enabled = true;
            //        this.lblCourtDate.Text = string.Format("{1} cases available on {0}", dt.ToString("yyyy-MM-dd"), noAvailableCases);

            //        this.hidCourtDate.Value = dt.ToString("yyyy-MM-dd");
            //        break;
            //    }
            //}
            //reader.Close();
            #endregion
        }

        private void GetDefaultCourtPercentage()
        {
            AuthorityRulesDetails rule2 = new AuthorityRulesDetails();
            rule2.AutIntNo = this.autIntNo;
            rule2.ARCode = "5020";
            rule2.LastUser = this.login;

            DefaultAuthRules authRule2 = new DefaultAuthRules(rule2, this.connectionString);
            KeyValuePair<int, string> percentageRule = authRule2.SetDefaultAuthRule();

            this.dPercentage = (double)percentageRule.Key;
        }

        #region Jerry 2014-10-21 delete this by Bontq 1601 client required.
        //protected void NOAOGDecisionYes_Click(object sender, EventArgs e)
        //{
        //    #region Jerry 2014-03-13 rebuild it
        //    //this.ViewState.Add("noaogDecision", "Y");
        //    //decimal revisedAmount = (decimal)this.ViewState["RevisedAmount"];
        //    //if (revisedAmount == 0 || revisedAmount >= 999999)
        //    //{
        //    //    string ticketNo = (string)this.grdSummonsNoticesList.DataKeys[0]["TicketNo"];

        //    //    //Jerry 2014-02-18 rechange it 
        //    //    //Jerry 2013-11-28 get one row per notice
        //    //    //int notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[0].Value.ToString());
        //    //    //SIL.AARTO.DAL.Data.ChargeQuery query = new SIL.AARTO.DAL.Data.ChargeQuery();
        //    //    //query.AppendEquals(ChargeColumn.NotIntNo, notIntNo.ToString());
        //    //    //query.AppendEquals(ChargeColumn.ChgIsMain, "true");
        //    //    //Charge charge = new ChargeService().Find(query)[0];
        //    //    decimal originalAmount = Convert.ToDecimal(this.grdSummonsNoticesList.DataKeys[0]["OriginalAmount"]);
        //    //    //decimal originalAmount = (decimal)charge.ChgFineAmount;

        //    //    this.RepresentationOnTheFly1.TicketNumber = ticketNo;
        //    //    this.RepresentationOnTheFly1.AutIntNo = this.autIntNo;
        //    //    this.RepresentationOnTheFly1.OriginalAmount = originalAmount;
        //    //    this.RepresentationOnTheFly1.NewAmount = 0;
        //    //    this.RepresentationOnTheFly1.BindData();

        //    //    VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, 
        //    //    this.pnlSumonsNoticeList.ID, this.pnlRepresentation.ID });

        //    //    return;
        //    //}
        //    #endregion

        //    #region Jerry 2014-03-17 rebuild it
        //    //foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
        //    //{
        //    //    if (grd.RowType == DataControlRowType.DataRow)
        //    //    {
        //    //        CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
        //    //        if (!cb.Checked)
        //    //            continue;

        //    //        int notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotIntNo"].ToString());
        //    //        Charge charge = chargeService.GetByNotIntNo(notIntNo).Find(c => c.ChgIsMain == true && c.ChgNoAog == "Y");
        //    //        if (charge == null)
        //    //            continue;

        //    //        decimal originalAmount = Convert.ToDecimal(this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["OriginalAmount"].ToString());
        //    //        decimal revisedAmount = Convert.ToDecimal(this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["RevisedAmount"].ToString());
        //    //        string noAog = charge.ChgNoAog;
        //    //        if (noAog.Equals("Y") && (revisedAmount == 0 || revisedAmount >= 999999))
        //    //        {
        //    //            string ticketNo = (string)this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["TicketNo"].ToString();

        //    //            this.RepresentationOnTheFly1.TicketNumber = ticketNo;
        //    //            this.RepresentationOnTheFly1.AutIntNo = this.autIntNo;
        //    //            this.RepresentationOnTheFly1.OriginalAmount = originalAmount;
        //    //            this.RepresentationOnTheFly1.NewAmount = 0;
        //    //            this.RepresentationOnTheFly1.BindData();

        //    //            VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlRepresentation.ID });
        //    //            txtNewAmount.Text = "";
        //    //            break;
        //    //        }
        //    //    }
        //    //}
        //    #endregion

        //    Notice noticeEntity;
        //    bool allowRepresentation = false;
        //    int notIntNo = 0;
        //    int selectedGridRowCount = 0;
        //    decimal originalAmount = 0;
        //    StringBuilder noAogRepMessage = new StringBuilder();

        //    foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
        //    {
        //        if (grd.RowType == DataControlRowType.DataRow)
        //        {
        //            CheckBox cb = (CheckBox)grd.FindControl("chkBoxSelect");
        //            cb.Enabled = false;
        //            if (!cb.Checked)
        //                continue;

        //            notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["NotIntNo"].ToString());
        //            noticeEntity = noticeService.GetByNotIntNo(notIntNo);
        //            if (noticeEntity == null)
        //                continue;

        //            selectedGridRowCount++;
        //            originalAmount = Convert.ToDecimal(this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["OriginalAmount"].ToString());
        //            GetAuthorityRule(noticeEntity.AutIntNo);
        //            if (noticeEntity.IsSection35)
        //            {
        //                if (allowS35Rule)
        //                {
        //                    allowRepresentation = true;
        //                    noAogRepMessage.Append("<br/>" + string.Format((string)GetLocalResourceObject("lblMessage.Text60"), noticeEntity.NotTicketNo));
        //                }
        //            }
        //            else if (noticeEntity.IsNoAog)
        //            {
        //                if (allowNoAogRule)
        //                {
        //                    allowRepresentation = true;
        //                    noAogRepMessage.Append("<br/>" + string.Format((string)GetLocalResourceObject("lblMessage.Text61"), noticeEntity.NotTicketNo));
        //                }
        //            }
        //        }
        //    }

        //    if (allowRepresentation && selectedGridRowCount > 1)
        //    {
        //        lblNoAOGRepMessage.Text = noAogRepMessage.ToString();
        //    }
        //    else if(allowRepresentation && selectedGridRowCount == 1)
        //    {
        //        noticeEntity = noticeService.GetByNotIntNo(notIntNo);
        //        this.RepresentationOnTheFly1.TicketNumber = noticeEntity.NotTicketNo;
        //        this.RepresentationOnTheFly1.AutIntNo = noticeEntity.AutIntNo;
        //        this.RepresentationOnTheFly1.OriginalAmount = originalAmount;
        //        this.RepresentationOnTheFly1.NewAmount = 0;
        //        this.RepresentationOnTheFly1.BindData();

        //        VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlRepresentation.ID });
        //        txtNewAmount.Text = "";
        //        ViewState.Add("NotTicketNo", noticeEntity.NotTicketNo);
        //    }
        //    else if (!allowRepresentation && selectedGridRowCount >= 0)
        //    {
        //        lblNoAOGRepMessage.Text = (string)GetLocalResourceObject("lblMessage.Text62");
        //    }
        //    else if (!allowRepresentation)
        //    {
        //        lblNoAOGRepMessage.Text = (string)GetLocalResourceObject("lblMessage.Text63");
        //    }
        //}
        #endregion

        #region Jerry 2014-10-21 delete this by Bontq 1601 client required.
        /// <summary>
        /// Jerry 2013-03-12 add
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void NOAOGDecisionNo_Click(object sender, EventArgs e)
        //{
        //    //the user chose not to enter a rep on the NOAOG
        //    //this.ViewState.Add("noaogDecision", "N");
        //    string areaCode = this.grdSummonsNoticesList.DataKeys[0]["AreaCode"].ToString();
        //    this.ShowSummonsServer(areaCode);
        //    VisiblePanel(new string[] { this.pnlSummonsServer.ID, this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID });
        //    SummonsServerStaffVisible();
        //}
        #endregion

        protected void lnkSearch_Click(object sender, EventArgs e)
        {
            this.txtIdentityNumber.Text = "";

            // 2013-11-29, Oscar added
            this.txtQueryRegNo.Text = "";
            this.txtQuerySurname.Text = "";
            this.txtQueryInitials.Text = "";
            //Jerry 2014-02-14 add
            this.txtNotTicketNo.Text = "";

            VisiblePanel(new string[] { this.pnlSessionIDFilter.ID });
            btnBatchPrintCPA5.Enabled = false;
            btnBatchPrintCPA6.Enabled = false;
        }

        #region Jerry 2014-10-21 delete this by Bontq 1601 client required.
        //protected void txtNewAmount_TextChanged(object sender, EventArgs e)
        //{
        //    decimal amount = 0;
        //    decimal.TryParse(this.txtNewAmount.Text, out amount);
        //    this.RepresentationOnTheFly1.NewAmount = amount;
        //}
        #endregion

        protected void grdSumonsMultiplePrint_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.grdSumonsMultiplePrint.PageIndex = e.NewPageIndex;
            this.PopulateMultipleSummons();
        }

        //protected void grdSumonsMultiplePrint_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    //mrs 20081125 new multiple summons print grid handler
        //    if (grdSumonsMultiplePrint.SelectedIndex > -1)
        //    {
        //        int sumIntNo = (int)this.grdSumonsMultiplePrint.SelectedDataKey["SumIntNo"];
        //        int chgIntNo = (int)this.grdSumonsMultiplePrint.SelectedDataKey["ChgIntNo"];
        //        int printAutIntNo = (int)this.grdSumonsMultiplePrint.SelectedDataKey["PrintAutIntNo"];
        //        Session["PrintAutIntNo"] = printAutIntNo;
        //        ChargeDB db = new ChargeDB(connectionString);
        //        db.UpdateStatus(chgIntNo, STATUS_SUMMONS_PRINTED);

        //        // Open the viewer page
        //        string printFile = (string)this.grdSumonsMultiplePrint.SelectedDataKey["PrintFile"];
        //        if (string.IsNullOrEmpty(printFile))
        //            printFile = "*" + (string)this.grdSumonsMultiplePrint.SelectedDataKey["SummonsNo"];

        //        QSParams param = new QSParams("printfile", printFile);

        //        // Jake 20090514 add new parameters roadBlock Status
        //        QSParams paramRoadBlock = new QSParams("roadBlock", "Open");
        //        PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");

        //        pageToOpen.Parameters.Add(param);
        //        pageToOpen.Parameters.Add(new QSParams("mode", "S"));
        //        pageToOpen.Parameters.Add(paramRoadBlock);

        //        Helper_Web.BuildPopup(pageToOpen);
        //    }
        //}

        //jerry 2011-12-09 change
        //protected void btnBatchPrint_Click(object sender, EventArgs e)
        //{
        //    int chgIntNo = 0;
        //    string printAutIntNo = "";
        //    string printFile = "";

        //    foreach (GridViewRow grd in this.grdSumonsMultiplePrint.Rows)
        //    {
        //        if (grd.RowType == DataControlRowType.DataRow)
        //        {
        //            if (!string.IsNullOrEmpty(grd.Cells[0].Text))
        //            {
        //                chgIntNo = Convert.ToInt32(grd.Cells[0].Text);

        //                printAutIntNo = grd.Cells[2].Text;


        //                Session["PrintAutIntNo"] = printAutIntNo;
        //                ChargeDB db = new ChargeDB(connectionString);
        //                db.UpdateStatus(chgIntNo, STATUS_SUMMONS_PRINTED);

        //                // Open the viewer page
        //                printFile = grd.Cells[4].Text;
        //            }

        //        }
        //    }

        //    if (string.IsNullOrEmpty(printFile))
        //    {
        //        lblMessage.Text = "No print file is available";
        //        return;
        //    }

        //    QSParams param = new QSParams("printfile", printFile);

        //    // Jake 20090514 add new parameters roadBlock Status
        //    QSParams paramRoadBlock = new QSParams("roadBlock", "Open");


        //    PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");


        //    pageToOpen.Parameters.Add(param);
        //    pageToOpen.Parameters.Add(new QSParams("mode", "S"));
        //    pageToOpen.Parameters.Add(paramRoadBlock);

        //    Helper_Web.BuildPopup(pageToOpen);


        //}

        protected void btnRBSessionList_Click(object sender, EventArgs e)
        {
            Response.Redirect("RoadBlockSession.aspx");
        }

        protected void cboSummonsServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            SummonsServerStaffVisible();
        }

        private void SummonsServerStaffVisible()
        {
            bool bVisible = false;
            if (cboSummonsServer.SelectedIndex == 1)
                bVisible = true;

            labelSelectSummonsServerStaff.Visible = bVisible;
            cboSummonsServerStaff.Visible = bVisible;
        }
        #endregion

        #region Jerry 2014-09-26 rebuild
        //jerry 2011-12-09 add
        //protected void btnBatchPrintCPA5_Click(object sender, EventArgs e)
        //{
        //    //Jerry 2014-02-20 the column ChgIntNo on the left hand side must be removed
        //    //int chgIntNo = 0;
        //    string printAutIntNo = "";
        //    string printFile = "";

        //    foreach (GridViewRow grd in this.grdSumonsMultiplePrint.Rows)
        //    {
        //        if (grd.RowType == DataControlRowType.DataRow)
        //        {
        //            if (grd.Cells[4].Text == "CPA5")
        //            {
        //                if (!string.IsNullOrEmpty(grd.Cells[0].Text))
        //                {
        //                    //chgIntNo = Convert.ToInt32(grd.Cells[0].Text);

        //                    //printAutIntNo = grd.Cells[2].Text;
        //                    printAutIntNo = grd.Cells[1].Text;


        //                    Session["PrintAutIntNo"] = printAutIntNo;
        //                    //#5165: when print, the status must not change. Edit by Teresa 2013/12/27
        //                    //ChargeDB db = new ChargeDB(connectionString);
        //                    //db.UpdateStatus(chgIntNo, STATUS_SUMMONS_PRINTED, this.login);

        //                    // Open the viewer page
        //                    //printFile = grd.Cells[4].Text;
        //                    printFile = grd.Cells[3].Text;
        //                }
        //            }
        //        }
        //    }

        //    if (string.IsNullOrEmpty(printFile))
        //    {
        //        lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text40");
        //        return;
        //    }

        //    QSParams param = new QSParams("printfile", printFile);

        //    // Jake 20090514 add new parameters roadBlock Status
        //    QSParams paramRoadBlock = new QSParams("roadBlock", "Open");


        //    PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");


        //    pageToOpen.Parameters.Add(param);
        //    pageToOpen.Parameters.Add(new QSParams("mode", "R"));
        //    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        //    pageToOpen.Parameters.Add(new QSParams("printType", "SummonsRoadBlockPrintCPA5"));
        //    pageToOpen.Parameters.Add(paramRoadBlock);

        //    Helper_Web.BuildPopup(pageToOpen);

        //    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        //    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
        //   //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsRoadBlockPrintCPA5, PunchAction.Change);
        //}
        #endregion

        protected void btnBatchPrintCPA5_Click(object sender, EventArgs e)
        {
            string printAutIntNo = "";
            string printFile = "";
            List<string> printedFiles = new List<string>();

            int i = 0;
            foreach (GridViewRow grd in this.grdSumonsMultiplePrint.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    CheckBox currentCheckBox = (CheckBox)grd.FindControl("chkBoxPrint");
                    printFile = grd.Cells[3].Text;
                    if (string.IsNullOrEmpty(printFile))
                    {
                        continue;
                    }

                    if (currentCheckBox.Checked && grd.Cells[4].Text == "CPA5" && !printedFiles.Contains(printFile))
                    {
                        printedFiles.Add(printFile);
                        i++;
                    }
                }
            }
            if (i == 0)
                return;

            PageToOpen[] pages = new PageToOpen[i];
            i = 0;
            printedFiles.Clear();

            foreach (GridViewRow grd in this.grdSumonsMultiplePrint.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    CheckBox currentCheckBox = (CheckBox)grd.FindControl("chkBoxPrint");
                    printFile = grd.Cells[3].Text;
                    if (string.IsNullOrEmpty(printFile))
                    {
                        continue;
                    }

                    if (currentCheckBox.Checked && grd.Cells[4].Text == "CPA5" && !printedFiles.Contains(printFile))
                    {
                        printAutIntNo = grd.Cells[1].Text;
                        Session["PrintAutIntNo"] = printAutIntNo;

                        QSParams param = new QSParams("printfile", printFile);
                        QSParams paramRoadBlock = new QSParams("roadBlock", "Open");
                        PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");

                        pageToOpen.Parameters.Add(param);
                        pageToOpen.Parameters.Add(new QSParams("mode", "R"));
                        pageToOpen.Parameters.Add(new QSParams("printType", "SummonsRoadBlockPrintCPA5"));
                        pageToOpen.Parameters.Add(paramRoadBlock);
                        pages[i] = pageToOpen;
                        printedFiles.Add(printFile);
                        i++;
                    }
                }
            }

            if (pages.Length > 0)
                Helper_Web.BuildPopup(pages);
        }

        #region Jerry 2014-09-26 rebuild
        //jerry 2011-12-09 add
        //protected void btnBatchPrintCPA6_Click(object sender, EventArgs e)
        //{
        //    //Jerry 2014-02-20 the column ChgIntNo on the left hand side must be removed
        //    //int chgIntNo = 0;
        //    string printAutIntNo = "";
        //    string printFile = "";

        //    foreach (GridViewRow grd in this.grdSumonsMultiplePrint.Rows)
        //    {
        //        if (grd.RowType == DataControlRowType.DataRow)
        //        {
        //            if (grd.Cells[4].Text == "CPA6")
        //            {
        //                if (!string.IsNullOrEmpty(grd.Cells[0].Text))
        //                {
        //                    //chgIntNo = Convert.ToInt32(grd.Cells[0].Text);

        //                    //printAutIntNo = grd.Cells[2].Text;
        //                    printAutIntNo = grd.Cells[1].Text;


        //                    Session["PrintAutIntNo"] = printAutIntNo;
        //                    //#5165: when print, the status must not change. Edit by Teresa 2013/12/27
        //                    //ChargeDB db = new ChargeDB(connectionString);
        //                    //db.UpdateStatus(chgIntNo, STATUS_SUMMONS_PRINTED, this.login);

        //                    // Open the viewer page
        //                    //printFile = grd.Cells[4].Text;
        //                    printFile = grd.Cells[3].Text;
        //                }
        //            }
        //        }
        //    }

        //    if (string.IsNullOrEmpty(printFile))
        //    {
        //        lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text41");
        //        return;
        //    }

        //    QSParams param = new QSParams("printfile", printFile);

        //    // Jake 20090514 add new parameters roadBlock Status
        //    QSParams paramRoadBlock = new QSParams("roadBlock", "Open");


        //    PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");


        //    pageToOpen.Parameters.Add(param);
        //    pageToOpen.Parameters.Add(new QSParams("mode", "R"));
        //     //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        //    pageToOpen.Parameters.Add(new QSParams("printType", "SummonsRoadBlockPrintCPA6"));
        //    pageToOpen.Parameters.Add(paramRoadBlock);

        //    Helper_Web.BuildPopup(pageToOpen);

        //    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        //    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
        //    //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsRoadBlockPrintCPA6, PunchAction.Change);
        //}
        #endregion

        protected void btnBatchPrintCPA6_Click(object sender, EventArgs e)
        {
            string printAutIntNo = "";
            string printFile = "";
            List<string> printedFiles = new List<string>();

            int i = 0;
            foreach (GridViewRow grd in this.grdSumonsMultiplePrint.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    CheckBox currentCheckBox = (CheckBox)grd.FindControl("chkBoxPrint");
                    printFile = grd.Cells[3].Text;
                    if (string.IsNullOrEmpty(printFile))
                    {
                        continue;
                    }

                    if (currentCheckBox.Checked && grd.Cells[4].Text == "CPA6" && !printedFiles.Contains(printFile))
                    {
                        printedFiles.Add(printFile);
                        i++;
                    }
                }
            }
            if (i == 0)
                return;

            PageToOpen[] pages = new PageToOpen[i];
            i = 0;
            printedFiles.Clear();

            foreach (GridViewRow grd in this.grdSumonsMultiplePrint.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    CheckBox currentCheckBox = (CheckBox)grd.FindControl("chkBoxPrint");
                    printFile = grd.Cells[3].Text;
                    if (string.IsNullOrEmpty(printFile))
                    {
                        continue;
                    }

                    if (currentCheckBox.Checked && grd.Cells[4].Text == "CPA6" && !printedFiles.Contains(printFile))
                    {
                        printAutIntNo = grd.Cells[1].Text;
                        Session["PrintAutIntNo"] = printAutIntNo;

                        QSParams param = new QSParams("printfile", printFile);
                        QSParams paramRoadBlock = new QSParams("roadBlock", "Open");
                        PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");

                        pageToOpen.Parameters.Add(param);
                        pageToOpen.Parameters.Add(new QSParams("mode", "R"));
                        pageToOpen.Parameters.Add(new QSParams("printType", "SummonsRoadBlockPrintCPA6"));
                        pageToOpen.Parameters.Add(paramRoadBlock);
                        pages[i] = pageToOpen;
                        printedFiles.Add(printFile);
                        i++;
                    }
                }
            }

            if (pages.Length > 0)
                Helper_Web.BuildPopup(pages);
        }

        protected void btnRePrint_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            //string printFile = "";
            //string summonsNumber = "";
            //string strAutIntNo = "";
            this.txtIdentityNumber.Text = "";
            this.txtQueryRegNo.Text = "";
            this.txtQuerySurname.Text = "";
            this.txtQueryInitials.Text = "";
            VisiblePanel(new string[] { this.pnlSessionIDFilter.ID });

            string strNotTicketNo = this.txtNotTicketNo.Text.Trim();
            //if (strNotTicketNo == string.Empty)
            if (string.IsNullOrEmpty(strNotTicketNo))
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text48");
                return;
            }

            //Jerry 20141017 get only current notice report
            //GetSummonsDetails(strNotTicketNo, ref printFile, ref summonsNumber, ref strAutIntNo);
            SIL.AARTO.DAL.Entities.TList<Notice> noticeList = noticeService.GetByNotTicketNo(strNotTicketNo);
            if (noticeList.Count == 0)
            {
                lblMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text80"), strNotTicketNo);
                return;
            }

            //if (strAutIntNo == string.Empty)
            //{
            //    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text48");
            //    return;
            //}
            //Session["printAutIntNo"] = strAutIntNo;
            Session["printAutIntNo"] = noticeList[0].AutIntNo;

            QSParams param = new QSParams("printfile", "*" + strNotTicketNo);
            // Jake 20090514 add new parameters roadBlock Status
            //QSParams paramRoadBlock = new QSParams("roadBlockRePrint", "Open");
            PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");
            pageToOpen.Parameters.Add(param);
            pageToOpen.Parameters.Add(new QSParams("mode", "N"));
            pageToOpen.Parameters.Add(new QSParams("printType", "s"));
            //pageToOpen.Parameters.Add(paramRoadBlock);

            Helper_Web.BuildPopup(pageToOpen);

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsRoadBlockPrintCPA6, PunchAction.Change);
        }

        /// <summary>
        /// jerry 2011-12-26 add autCode for push queue
        /// </summary>
        /// <param name="autIntNo"></param>
        private void GetAutCode()
        {
            this.autCode = new AuthorityDB(this.connectionString).GetAuthorityDetails(this.autIntNo).AutCode.Trim();
        }

        #region Jerry 2014-03-17 comment it out
        //protected void grdSummonsNoticesList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    this.grdSummonsNoticesList.PageIndex = e.NewPageIndex;
        //    BindData();
        //}
        #endregion

        //#5165:confirm that these grids are paginating correctly update by Teresa 2014/1/3
        protected void dgSummonsNoticesListPager_PageChanged(object sender, EventArgs e)
        {
            this.lblNoAOGRepMessage.Text = "";

            if (ViewState["IsSearchClick"] == null)
                BindData();
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            int leftItemCount = ViewState["LeftItemCount"] == null ? 0 : Convert.ToInt32(ViewState["LeftItemCount"].ToString());
            if (leftItemCount == 0)
            {
                ViewState["IdNumberSelected"] = null;
                ViewState["SurnameSelected"] = null;
                ViewState["InitialsSelected"] = null;
            }
            Search();
        }

        private void Search()
        {
            lblMessage.Text = "";
            this.txtNotTicketNo.Text = "";
            this.lblNoAOGRepMessage.Text = "";

            //Jerry 2014-02-14 check Surname and Initials
            if (string.IsNullOrEmpty(this.txtQuerySurname.Text.Trim()) && !string.IsNullOrEmpty(this.txtQueryInitials.Text.Trim()))
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text56");//Please input Surname
                return;
            }

            if (!string.IsNullOrEmpty(this.txtQuerySurname.Text.Trim()) && string.IsNullOrEmpty(this.txtQueryInitials.Text.Trim()))
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text55");//Please input initials
                return;
            }

            VisiblePanel(new string[] { this.pnlSessionIDFilter.ID });

            ViewState["IsSearchClick"] = "1";
            dgSummonsNoticesListPager.CurrentPageIndex = 1;

            this.BindIDNumberGrid();

            this.BindData();

            //Jerry 2014-09-17 move this code into BindData method
            //if (grdSummonsNoticesList.Rows.Count > 0)
            //{
            //    //lblResultTitle.Text = string.Format((string)GetLocalResourceObject("lblResultTitle.Text"), txtIdentityNumber.Text);
            //    // 2013-11-29, Oscar changed
            //    var sb = new StringBuilder((string)GetLocalResourceObject("lblResultTitle.Text"));
            //    if (!string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text))
            //        sb.AppendFormat((string)GetLocalResourceObject("OfID"), this.txtIdentityNumber.Text);
            //    if (!string.IsNullOrWhiteSpace(this.txtQueryRegNo.Text))
            //    {
            //        if (!string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text))
            //            sb.Append(";");
            //        sb.AppendFormat((string)GetLocalResourceObject("OfRegNo"), this.txtQueryRegNo.Text);
            //    }

            //    if (!string.IsNullOrWhiteSpace(this.txtQuerySurname.Text))
            //    {
            //        if (!string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text) || !string.IsNullOrWhiteSpace(this.txtQueryRegNo.Text))
            //            sb.Append(";");
            //        sb.AppendFormat((string)GetLocalResourceObject("OfSurname"), this.txtQuerySurname.Text);
            //    }
            //    if (!string.IsNullOrWhiteSpace(this.txtQueryInitials.Text))
            //        sb.AppendFormat((string)GetLocalResourceObject("OfInitials"), this.txtQueryInitials.Text);

            //    //Jerry 2014-09-17 add
            //    if (ViewState["IdNumberSelected"] != null && string.IsNullOrWhiteSpace(this.txtIdentityNumber.Text.Trim()))
            //    {
            //        sb.Append(" (");
            //        sb.AppendFormat(((string)GetLocalResourceObject("OfID")).Trim(), ViewState["IdNumberSelected"].ToString());
            //        sb.Append(")");
            //    }
            //    this.lblResultTitle.Text = sb.ToString();

            //    //Jerry 2014-03-17 move this code in BindData() method
            //    //int notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[0].Value.ToString());
            //    //VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlSumonsNoticeList.ID, this.pnlOffenceDetail.ID });
            //    //ShowOffenceDetail(notIntNo);
            //}
            //else if (grdIdNumberInfo.Rows.Count > 0)
            //{
            //    VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlIdNumberInfo.ID });
            //}
        }

        #region Jerry 2014-10-21 don't use rule because delete representation by Bontq 1601 client required.
        //protected void chkBoxSelect_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (!allowS35Rule && !allowNoAogRule)
        //        return;

        //    CheckBox currentCheckBox = (CheckBox)sender;
        //    GridViewRow currentRow = (GridViewRow)currentCheckBox.NamingContainer;
        //    string currentS35OrNoAOG = grdSummonsNoticesList.DataKeys[currentRow.RowIndex]["S35OrNoAOG"].ToString();
        //    if (string.IsNullOrEmpty(currentS35OrNoAOG))
        //        return;

        //    string currentRowTicketNo = grdSummonsNoticesList.DataKeys[currentRow.RowIndex]["TicketNo"].ToString();
        //    if (allowS35Rule && currentS35OrNoAOG == "S35")
        //    {
        //        foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
        //        {
        //            if (grd.RowType == DataControlRowType.DataRow)
        //            {
        //                string ticketNo = this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["TicketNo"].ToString();
        //                if (currentRowTicketNo == ticketNo)
        //                    continue;

        //                string otherS35OrNoAOG = this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["S35OrNoAOG"].ToString();
        //                CheckBox otherCheckBox = (CheckBox)grd.FindControl("chkBoxSelect");
        //                if (currentCheckBox.Checked)
        //                {
        //                    otherCheckBox.Checked = false;
        //                    otherCheckBox.Enabled = false;
        //                    lblNoAOGRepMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text60"), currentRowTicketNo);
        //                }
        //                else if (otherS35OrNoAOG == "S35")
        //                {
        //                    otherCheckBox.Checked = false;
        //                    otherCheckBox.Enabled = true;
        //                    lblNoAOGRepMessage.Text = "";
        //                }
        //                else if (otherS35OrNoAOG == "NoAOG" && allowNoAogRule)
        //                {
        //                    otherCheckBox.Checked = false;
        //                    otherCheckBox.Enabled = true;
        //                    lblNoAOGRepMessage.Text = "";
        //                }
        //                else
        //                {
        //                    otherCheckBox.Checked = true;
        //                    otherCheckBox.Enabled = true;
        //                    lblNoAOGRepMessage.Text = "";
        //                }
        //            }
        //        }

        //        return;
        //    }
        //    else if (allowNoAogRule && currentS35OrNoAOG == "NoAOG")
        //    {
        //        foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
        //        {
        //            if (grd.RowType == DataControlRowType.DataRow)
        //            {
        //                string ticketNo = this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["TicketNo"].ToString();
        //                if (currentRowTicketNo == ticketNo)
        //                    continue;

        //                string otherS35OrNoAOG = this.grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["S35OrNoAOG"].ToString();
        //                CheckBox otherCheckBox = (CheckBox)grd.FindControl("chkBoxSelect");
        //                if (currentCheckBox.Checked)
        //                {
        //                    otherCheckBox.Checked = false;
        //                    otherCheckBox.Enabled = false;
        //                    lblNoAOGRepMessage.Text = string.Format((string)GetLocalResourceObject("lblMessage.Text61"), currentRowTicketNo);
        //                }
        //                else if (otherS35OrNoAOG == "NoAOG")
        //                {
        //                    otherCheckBox.Checked = false;
        //                    otherCheckBox.Enabled = true;
        //                    lblNoAOGRepMessage.Text = "";
        //                }
        //                else if (otherS35OrNoAOG == "S35" && allowS35Rule)
        //                {
        //                    otherCheckBox.Checked = false;
        //                    otherCheckBox.Enabled = true;
        //                    lblNoAOGRepMessage.Text = "";
        //                }
        //                else
        //                {
        //                    otherCheckBox.Checked = true;
        //                    otherCheckBox.Enabled = true;
        //                    lblNoAOGRepMessage.Text = "";
        //                }
        //            }
        //        }

        //        return;
        //    }


        //}
        #endregion

        protected void rdoNumberSelect_CheckedChanged(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            lblNoAOGRepMessage.Text = "";
            UnSelectRdoNumberSelect();

            RadioButton currentRadioButton = (RadioButton)sender;
            currentRadioButton.Checked = true;
            GridViewRow currentRow = (GridViewRow)currentRadioButton.NamingContainer;
            ViewState["IdNumberSelected"] = grdIdNumberInfo.DataKeys[currentRow.RowIndex]["IDNumber"].ToString().Trim();
            ViewState["SurnameSelected"] = grdIdNumberInfo.DataKeys[currentRow.RowIndex]["Surname"].ToString().Trim();
            ViewState["InitialsSelected"] = grdIdNumberInfo.DataKeys[currentRow.RowIndex]["Initials"].ToString().Trim();
            ViewState["IsSearchClick"] = "1";
            dgSummonsNoticesListPager.CurrentPageIndex = 1;
            this.BindData();
            if (grdSummonsNoticesList.Rows.Count <= 0)
            {
                VisiblePanel(new string[] { this.pnlSessionIDFilter.ID, this.pnlIdNumberInfo.ID });
            }
        }

        private void UnSelectRdoNumberSelect()
        {
            foreach (GridViewRow grd in this.grdIdNumberInfo.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    RadioButton rdoButton = (RadioButton)grd.FindControl("rdoNumberSelect");
                    rdoButton.Checked = false;
                }
            }
        }

        protected void grdIdNumberInfo_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow grd in this.grdIdNumberInfo.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    if (ViewState["IdNumberSelected"] == null)
                    {
                        RadioButton firstRadioButton = (RadioButton)grd.FindControl("rdoNumberSelect");
                        firstRadioButton.Checked = true;
                        ViewState["IdNumberSelected"] = grdIdNumberInfo.DataKeys[grd.RowIndex]["IDNumber"].ToString().Trim();
                        ViewState["SurnameSelected"] = grdIdNumberInfo.DataKeys[grd.RowIndex]["Surname"].ToString().Trim();
                        ViewState["InitialsSelected"] = grdIdNumberInfo.DataKeys[grd.RowIndex]["Initials"].ToString().Trim();
                        break;
                    }
                    else
                    {
                        string idNumber = grdIdNumberInfo.DataKeys[grd.RowIndex]["IDNumber"].ToString().Trim();
                        string idNumberSelected = ViewState["IdNumberSelected"].ToString();
                        string surname = grdIdNumberInfo.DataKeys[grd.RowIndex]["Surname"].ToString().Trim();
                        string surnameSelected = ViewState["SurnameSelected"].ToString();
                        string initials = grdIdNumberInfo.DataKeys[grd.RowIndex]["Initials"].ToString().Trim();
                        string initialsSelected = ViewState["InitialsSelected"].ToString();

                        if (idNumber == idNumberSelected && surname == surnameSelected && initials == initialsSelected)
                        {
                            ViewState["SurnameSelected"] = grdIdNumberInfo.DataKeys[grd.RowIndex]["Surname"].ToString().Trim();
                            ViewState["InitialsSelected"] = grdIdNumberInfo.DataKeys[grd.RowIndex]["Initials"].ToString().Trim();

                            RadioButton firstRadioButton = (RadioButton)grd.FindControl("rdoNumberSelect");
                            firstRadioButton.Checked = true;
                            break;
                        }
                    }
                }
            }
        }

        #region Jerry 2014-10-21 rebuild it
        //protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    CheckBox allSelectedCheckBox = (CheckBox)sender;

        //    if (allSelectedCheckBox.Checked)
        //    {
        //        bool hasUnenableCheckBox = false;

        //        foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
        //        {
        //            CheckBox grdCheckBox = (CheckBox)grd.FindControl("chkBoxSelect");
        //            if (!grdCheckBox.Enabled)
        //                hasUnenableCheckBox = true;
        //        }

        //        foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
        //        {
        //            if (grd.RowType == DataControlRowType.DataRow)
        //            {
        //                CheckBox grdCheckBox = (CheckBox)grd.FindControl("chkBoxSelect");
        //                string grdS35OrNoAOG = grdSummonsNoticesList.DataKeys[grd.DataItemIndex]["S35OrNoAOG"].ToString();

        //                if (grdCheckBox.Enabled && !grdCheckBox.Checked)
        //                {
        //                    if (string.IsNullOrEmpty(grdS35OrNoAOG))
        //                        grdCheckBox.Checked = true;

        //                    if (!allowS35Rule && !allowNoAogRule)
        //                        grdCheckBox.Checked = true;

        //                    if(hasUnenableCheckBox)
        //                        grdCheckBox.Checked = true;

        //                    //if (allowS35Rule && grdS35OrNoAOG == "S35")
        //                    //{
        //                    //    grdCheckBox.Checked = false;
        //                    //}

        //                    //if (allowNoAogRule && grdS35OrNoAOG == "NoAOG")
        //                    //{
        //                    //    grdCheckBox.Checked = false;
        //                    //}


        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
        //        {
        //            if (grd.RowType == DataControlRowType.DataRow)
        //            {
        //                CheckBox grdCheckBox = (CheckBox)grd.FindControl("chkBoxSelect");
        //                if (grdCheckBox.Enabled && grdCheckBox.Checked)
        //                {
        //                    grdCheckBox.Checked = false;
        //                }
        //            }
        //        }
        //    }
        //}
        #endregion

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox allSelectedCheckBox = (CheckBox)sender;

            foreach (GridViewRow grd in this.grdSummonsNoticesList.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    CheckBox grdCheckBox = (CheckBox)grd.FindControl("chkBoxSelect");

                    if (allSelectedCheckBox.Checked)
                        grdCheckBox.Checked = true;
                    else
                        grdCheckBox.Checked = false;
                }
            }
        }

        protected void chkBoxPrint_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox selectedCheckBox = (CheckBox)sender;
            GridViewRow currentRow = (GridViewRow)selectedCheckBox.NamingContainer;

            int selectedSumIntNo = Convert.ToInt32(grdSumonsMultiplePrint.DataKeys[currentRow.RowIndex]["SumIntNo"].ToString());
            string selectedPrintFileName = grdSumonsMultiplePrint.DataKeys[currentRow.RowIndex]["PrintFile"].ToString().Trim();
            string selectedCPAText = grdSumonsMultiplePrint.DataKeys[currentRow.RowIndex]["CPAText"].ToString().Trim();
            if (string.IsNullOrEmpty(selectedPrintFileName) || string.IsNullOrEmpty(selectedCPAText))
                return;

            foreach (GridViewRow grd in this.grdSumonsMultiplePrint.Rows)
            {
                if (grd.RowType == DataControlRowType.DataRow)
                {
                    int currentSumIntNo = Convert.ToInt32(grdSumonsMultiplePrint.DataKeys[grd.DataItemIndex]["SumIntNo"].ToString());
                    string currentPrintFileName = this.grdSumonsMultiplePrint.DataKeys[grd.DataItemIndex]["PrintFile"].ToString();
                    string currentCPAText = this.grdSumonsMultiplePrint.DataKeys[grd.DataItemIndex]["CPAText"].ToString();
                    CheckBox currentCheckBox = (CheckBox)grd.FindControl("chkBoxPrint");
                    if (selectedSumIntNo == currentSumIntNo)
                        continue;

                    if (selectedCheckBox.Checked)
                    {
                        if (selectedPrintFileName == currentPrintFileName && selectedCPAText == currentCPAText)
                        {
                            currentCheckBox.Checked = true;
                        }
                    }
                    else
                    {
                        if (selectedPrintFileName == currentPrintFileName && selectedCPAText == currentCPAText)
                        {
                            currentCheckBox.Checked = false;
                        }
                    }

                }
            }

        }

        /// <summary>
        /// Get offender details by the selected row
        /// Jerry 2014-11-17 add
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdSummonsNoticesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int notIntNo = Convert.ToInt32(grdSummonsNoticesList.DataKeys[grdSummonsNoticesList.SelectedIndex]["NotIntNo"].ToString().Trim());
            if (notIntNo == 0)
                return;

            ShowOffenceDetail(notIntNo);
        }

    }
}
