using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using System.Transactions;
using SIL.AARTO.BLL.PostalManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Contains a list of second notice print batches and options to reprint a batch or an individual second notice
    /// </summary>
    public partial class PrintSecondNotice : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string loginUser;
        private string _cType = "2ND";
        private int autIntNo = 0;
        private int _status2ndNoticeCreated = 260;
        private int _status2ndNoticePrinted = 270;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "PrintSecondNotice.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Select the next print file for printing of the second notices";

        private const string DATE_FORMAT = "yyyy-MM-dd";
        private bool _AllowPrint;
        
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            //2013-12-02 Heidi fixed the problem that the object is not instantiated(5084)
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;


            

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            this.pnlGeneral.Visible = true;
            this.dgPrintrun.Visible = true;

            //modify by rachel 2014/10/28 for 5332
            if (ddlSelectLA.SelectedIndex > -1)
            {
                this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            }
            else
            {
                this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            }

            this.AllowPrint = CheckPrintAuthRule(); // AR_6209 
            //end modify by rachel 2014/10/28 for 5332

            if (!Page.IsPostBack)
            {               

                this.lblDateFrom.Visible = false;
                this.lblDateTo.Visible = false;
                this.dateFrom.Visible = false;
                this.dateTo.Visible = false;

                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.chkShowAll.Checked = false;
                this.pnlUpdate.Visible = false;

                this.PopulateAuthorities(autIntNo);

                this.BindGrid(this.autIntNo, this._cType);

                this.lblInstruct.Visible = false;
                if (ddlSelectLA.SelectedIndex > -1)
                {
                    this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                    this.Session["printAutIntNo"] = autIntNo;
                    this.TicketNumberSearch1.AutIntNo = this.autIntNo;
                }
            }
            else if (Request.Form["btnSearch"] != "")
            {
                BindGrid(this.autIntNo, this._cType);
            } 
           
            //else  //add by rachel for bontq 1512
            //{
            //    dgPrintrunPager.CurrentPageIndex = 1; 
            //    dgPrintrunPager.RecordCount = 0;
            //    BindGrid(autIntNo, _cType);
            //}
            //end add by rachel for bontq 1512


            //else
            //{

            //    BindGrid(this.autIntNo, _cType);
            //}
          
            //this.AllowPrint = CheckPrintAuthRule(); //added by jacob 20120726--20130912

            ////2014-01-21 Heidi fixed bug for when click "Update print status",there is something wrong with "Update print status" button Enable (5103)
            //if (Page.IsPostBack && Request.Form["btnSearch"] != "")
            //{
            //    BindGrid(this.autIntNo, _cType);
            //}
           
            
        }
        /// <summary>
        /// check if allowed to print
        /// added by jacob 20120726
        /// </summary>
        /// <returns></returns>
        private bool CheckPrintAuthRule()
        {
            //int aid = int.MinValue;
            //if (Session["printAutIntNo"] == null || 
            //    !int.TryParse(Session["printAutIntNo"].ToString(), out aid))
            //{
            //    return false;
            //}
            //autIntNo = Convert.ToInt32(Session["printAutIntNo"]);
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo =this.autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.loginUser;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            //2014-05-15 Heidi added "IsIBMPrinter"(5283)
            bool isIBMPrinter = (ar.SetDefaultAuthRule().Value.Equals("Y"));
            bool allowPrint = !isIBMPrinter;
            if(!allowPrint)
            {
                lblErrorForPrint.Visible = true;
                this.lblErrorForPrint.Text = (string)GetLocalResourceObject("errorMsg");
            }
            else
            {
                lblErrorForPrint.Visible = false;
            }
            btnPrint.Enabled = allowPrint;
            btnUpdateStatus.Enabled = allowPrint;
            return allowPrint;
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
           
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

          
        }

        private void BindGrid(int autIntNo, string _cType)
        {
            string showAll = "N";

            if (chkShowAll.Checked) showAll = "Y";

            DateTime dtFrom = lblDateFrom.Visible ? DateTime.Parse(dateFrom.Text) : DateTime.MinValue;
            DateTime dtTo = lblDateTo.Visible ? DateTime.Parse(dateTo.Text) : DateTime.MaxValue;

            Stalberg.TMS.NoticeDB printList = new Stalberg.TMS.NoticeDB(connectionString);

            int totalCount = 0;
            DataTable dtPrintList = printList.GetPrintrunDS(autIntNo, _cType, _status2ndNoticeCreated, showAll, dtFrom, dtTo, out totalCount,
                dgPrintrunPager.PageSize, dgPrintrunPager.CurrentPageIndex).Tables[0];
            dgPrintrun.DataSource = dtPrintList;
            dgPrintrun.DataKeyField = "PrintFile";
            dgPrintrunPager.RecordCount = totalCount;
            
            this.dgPrintrun.DataBind();

            if (dtPrintList.Rows.Count == 0)
            {
                
                this.lblError.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {                
                this.lblError.Visible = false;
            }
            this.pnlUpdate.Visible = false;
        }

        protected void dgPrintrun_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                dgPrintrun.SelectedIndex = e.Item.ItemIndex;

                if (dgPrintrun.SelectedIndex > -1)
                {

                    //2014-01-22 Heidi added for fixed when click "update print status","update print status" buttons's Enable attribute in the gridview are not correct.(5103)
                    BindGrid(this.autIntNo, _cType);

                    pnlUpdate.Visible = true;
                    lblPrintFile.Text = dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString();

                    lblError.Visible = true;
                    
                }
            }
            else if (e.CommandName == "PrintNotices")//jerry 2011-3-2 add
            {
                string printFileName = dgPrintrun.DataKeys[e.Item.ItemIndex].ToString();
                
                if (printFileName.Length > 0)
                {
                    if (printFileName.ToUpper().IndexOf("HWO") >= 0)
                    {
                        Helper_Web.BuildPopup(this, "SecondNotice_HWO_Viewer.aspx", "printfile", printFileName);
                    }
                    else
                    {
                        Helper_Web.BuildPopup(this, "SecondNoticeViewer.aspx", "printfile", printFileName);
                    }
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            if (txtPrint.Text.Trim().Equals(""))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }
            //added by jacob 20120726 
            //check new auth rule before printing.
            if (!CheckPrintAuthRule())
            {
                return;
            }
            
            // Generate the JavaScript to pop up a print-only window
            //dls 071119 - change this to match the first notice print page - the * will be appended later
            //Helper_Web.BuildPopup(this, "SecondNoticeViewer.aspx", "printfile", "*" + txtPrint.Text.Trim());
            Helper_Web.BuildPopup(this, "SecondNoticeViewer.aspx", "printfile", txtPrint.Text.Trim());
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.PrintSecondNotice, PunchAction.Change);  

        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            //Session["autIntNo"] = autIntNo;
            Session["printAutIntNo"] = autIntNo;
            this.AllowPrint = CheckPrintAuthRule();// added by jacob
            dgPrintrunPager.CurrentPageIndex = 1; // 2012-03-14 add by henry pagination
            dgPrintrunPager.RecordCount = 0;
            this.BindGrid(autIntNo, _cType);
            this.TicketNumberSearch1.AutIntNo = this.autIntNo;
           
          

        }
        protected void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            this.lblDateFrom.Visible = chkShowAll.Checked;
            this.lblDateTo.Visible = chkShowAll.Checked;
            this.dateFrom.Visible = chkShowAll.Checked;
            this.dateTo.Visible = chkShowAll.Checked;

            if (chkShowAll.Checked)
            {
                Session["showAllNotices"] = "Y";
                this.dateFrom.Text = DateTime.Now.AddMonths(-1).ToString(DATE_FORMAT);
                this.dateTo.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
            }
            else
            {
                Session["showAllNotices"] = "N";
            }

            this.AllowPrint=this.CheckPrintAuthRule();
            dgPrintrunPager.CurrentPageIndex = 1; // 2012-03-14 add by henry pagination
            dgPrintrunPager.RecordCount = 0;
            BindGrid(autIntNo, _cType);
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            int noOfRows = 0;
            string errMessage = string.Empty;

            using (TransactionScope scope = new TransactionScope())
            {
                NoticeDB notice = new NoticeDB(connectionString);
                noOfRows = notice.UpdateNoticeChargeStatus(autIntNo, lblPrintFile.Text, _status2ndNoticeCreated, _status2ndNoticePrinted, "SecondNotice", loginUser, ref errMessage);

                if (noOfRows > 0)
                {
                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "4200";
                    arDetails.LastUser = loginUser;
                    DefaultAuthRules ar = new DefaultAuthRules(arDetails, connectionString);
                    bool isPostDate = ar.SetDefaultAuthRule().Value.Equals("Y");

                    if (isPostDate)
                    {
                        PostDateNotice postDate = new PostDateNotice(connectionString);
                        noOfRows = postDate.SetNoticePostedDate(lblPrintFile.Text, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), loginUser, ref errMessage);
                    }
                }

                //if (noOfRows > 0)
                // 2013-10-21, Oscar changed, we have to commit when "noOfRows = 0"
                if (noOfRows >= 0)
                {
                    scope.Complete();
                }
            }

            if (noOfRows < 1)
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), noOfRows);
            else
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text3"), noOfRows);
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.PrintSecondNotice, PunchAction.Change); 
            }

            this.lblError.Visible = true;
            this.pnlUpdate.Visible = false;

            // Jake 2013-09-13 comment out
            //dgPrintrunPager.CurrentPageIndex = 1; // 2012-03-14 add by henry pagination
            //dgPrintrunPager.RecordCount = 0;
            this.BindGrid(this.autIntNo, this._cType);

        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtPrint.Text = this.TicketNumberSearch1.TicketNumber;
            //PunchStats805806 enquiry PrintSecondNotice
        }

        protected void dgPrintrun_ItemCreated(object sender, DataGridItemEventArgs e)
        {  
            //if (e.Item.DataItem == null)
            //    return;
           
            //Label lbl = (Label)e.Item.FindControl("lblType");
            //LinkButton btnUpdate = (LinkButton)e.Item.FindControl("btnUpdate");

            //HyperLink btnPrintNotices = (HyperLink)e.Item.FindControl("btnPrintNotices");

            //DateTime printDate = DateTime.MinValue;

            //DataRowView row = (DataRowView)e.Item.DataItem;

            ////jerry 2011-3-2 add
            //string printFileName = row["PrintFile"].ToString();
            //if (printFileName.Length == 0)
            //{
            //    btnPrintNotices.Enabled = false;
            //}
            //else
            //{
            //    //add by rachel 5332
            //    btnPrintNotices.Enabled = true;
            //    //end add by rachel 5332
            //}

            //if (row["NotPrint2ndNoticeDate"] != null)
            //{
            //    //dls 0901119 - added to disable update button if the file has not been printed
            //    if (!DateTime.TryParse(row["NotPrint2ndNoticeDate"].ToString(), out printDate))
            //    {
            //        btnUpdate.Enabled = false;
            //    }
            //    else
            //    {
            //        btnUpdate.Enabled = true;
            //    }
            //}
            //else
            //{
            //    btnUpdate.Enabled = false;
            //}
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            dgPrintrunPager.CurrentPageIndex = 1; // 2012-03-14 add by henry pagination
            dgPrintrunPager.RecordCount = 0;
            BindGrid(autIntNo, _cType);
            
        }

        /// <summary>
        /// added by jacob 20120726  , 
        /// Is it allowed to print at this web page.
        /// </summary>
        public bool AllowPrint
        {
            get { return _AllowPrint; }

            set { _AllowPrint = value;
                btnPrint.Enabled = value;
            }
        }

     

        public string UnPrintable
        {
            get { return "unprintable"; }
        }

        /// <summary>
        /// added by jacob 20120726  , 
        /// set enabled status to  'print notice' and 'update print status' links
        /// in the datagrid dgPrintrun.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPrintrun_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.DataItem == null)
                return;

            Label lbl = (Label)e.Item.FindControl("lblType");
            LinkButton btnUpdate = (LinkButton)e.Item.FindControl("btnUpdate");

            HyperLink btnPrintNotices = (HyperLink)e.Item.FindControl("btnPrintNotices");

            //update by rachel for 5332
            if (!AllowPrint)
            {
                btnUpdate.Enabled = false;
                btnPrintNotices.Enabled = false;
            }
            else
            {
                DateTime printDate = DateTime.MinValue;

                DataRowView row = (DataRowView)e.Item.DataItem;

                //jerry 2011-3-2 add
                string printFileName = row["PrintFile"].ToString();
                if (printFileName.Length == 0)
                {
                    btnPrintNotices.Enabled = false;
                }
                else
                {
                    //add by rachel 5332
                    btnPrintNotices.Enabled = true;
                    //end add by rachel 5332
                }

                if (row["NotPrint2ndNoticeDate"] != null)
                {
                    //dls 0901119 - added to disable update button if the file has not been printed
                    if (!DateTime.TryParse(row["NotPrint2ndNoticeDate"].ToString(), out printDate))
                    {
                        btnUpdate.Enabled = false;
                    }
                    else
                    {
                        btnUpdate.Enabled = true;
                    }
                }
                else
                {
                    btnUpdate.Enabled = false;
                }
            }
            //end update by rachel for 5332
        }

        //modify by rachel 2014/10/28 for 5332

        protected void dgPrintrunPager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, _cType);
        }

        protected void dgPrintrun_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgPrintrun.CurrentPageIndex = e.NewPageIndex;

            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, _cType);
        }

        //end modify by rachel 2014/10/28 for 5332
    }
}