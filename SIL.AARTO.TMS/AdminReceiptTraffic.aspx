<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.AdminReceiptTraffic" Codebehind="AdminReceiptTraffic.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"     rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    </asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1"> 
                <asp:UpdatePanel runat="server" ID="UDP">
                                    <ContentTemplate>
                    <table width="100%" border="0" class="Normal">
                        <tr>
                            <td valign="top" style="height: 47px">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                <asp:Panel ID="pnlApplyReversed" Width="100%" runat="server">
                                    &nbsp;<asp:Label ID="lblReversedPaymentAmount" runat="server" CssClass="SubContentHead" EnableViewState="False" Text="<%$Resources:lblReversedPaymentAmount.Text %>"></asp:Label></asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <!-- Start Content -->
                               
                                        <asp:Panel ID="pnlDetails" runat="server">
                                            <table style="width: 700px;">
                                                <tr>
                                                    <td class="NormalBold" style="width: 300px;">
                                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblId.Text %>"></asp:Label> </td>
                                                    <td style="width: 300px;">
                                                        <asp:TextBox ID="textId" runat="server" Width="400px" CssClass="Normal"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" style="text-align: center;">
                                                        &nbsp;<asp:Label ID="Label3" runat="server" Text="<%$Resources:lblOr.Text %>"></asp:Label></td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblSequenceNum.Text %>"></asp:Label></td>
                                                    <td>
                                                        <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblTicket.Text %>"></asp:Label></td>
                                                    <td>
                                                        <asp:TextBox ID="textTicket" runat="server" Width="400px" CssClass="Normal"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">
                                                    </td>
                                                    <td align="right">
                                                        <asp:Button ID="btnRePrint" runat="server" OnClick="btnRePrint_Click" Text="<%$Resources:btnRePrint.Text %>" CssClass="NormalButton" Width="120px" />&nbsp; 
                                                        <asp:Button ID="btnSearch" runat="server" OnClick="buttonSearch_Click" Text="<%$Resources:btnSearch.Text %>" CssClass="NormalButton" Width="120px" />&nbsp;
                                                        </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlNotices" runat="server" Width="100%">
                                            <asp:Label ID="lblNotices" runat="server" CssClass="NormalBold" />
                                            <asp:Button ID="btnClear" runat="server" Text="<%$Resources:btnClear.Text %>" CssClass="NormalButton" OnClick="btnClear_Click" />
                                        </asp:Panel>
                                    
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table></ContentTemplate>
                                </asp:UpdatePanel>
                                 <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label6"
                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                                </td>
                 </tr>
                 </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>

    <script type="text/javascript" language="javascript">
   function CheckMe()
   {
        var id = document.getElementById("textId");
        var ticket = document.getElementById("textTicket");
       
        if(id.length == 0 &&  ticket.length == 0)
        { 
            alert("You need to supply either an ID number or a ticket number."); 
        } 
   }
    </script>

</body>
</html>
