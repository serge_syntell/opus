using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
//using BarcodeNETWorkShop;
using ceTe.DynamicPDF.Merger;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS.Reports
{
    /// <summary>
    /// Represents a page that crates a PDF of a cash receipt note.
    /// </summary>
    public partial class WOAPaymentsViewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private string loginUser;
        
        private int autIntNo = 0;
        protected string thisPage = "WOA Payments Viewer";
        protected string thisPageURL = "WOAPaymentsViewer.aspx";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;

        //BarcodeNETImage barcode = null;
        //ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phBarCode;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //string reportPath = string.Empty;
            //int rctIntNo = 0;
            //string autCode = GetAuthCode();
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            AuthReportNameDB arn = new AuthReportNameDB(connectionString);

            string reportPage = arn.GetAuthReportName(autIntNo, "WOAPayments");
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "WOAPayments");
            if (reportPage.Equals(""))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "WOAPayments.dplx", "WOAPayments", "system", string.Empty);
                reportPage = "WOAPayments.dplx";
            }

            string woaPayments = Request.QueryString["woa"].ToString();

            string path = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {

                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            DocumentLayout doc = new DocumentLayout(path);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("woaPayments", woaPayments);

            Document report = doc.Run(parameters);
            byte[] buffer = report.Draw();

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(buffer);
            Response.End();

        }

    }
}
