using System;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
//using SpreadsheetGear;
using Stalberg.TMS.Data.Datasets;
using SIL.AARTO.BLL.Report;

namespace Stalberg.TMS
{
    /// <summary>
    /// represents a page that a cahier uses to reconcile their cashbox at the end of the day
    /// </summary>
    public partial class OfficerStatistics_ReportViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private int autIntNo;
        //private string thisPage = "Officer Statistics Report Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "OfficerStatistics_ReportViewer.aspx";
        protected string loginUser = string.Empty;
        private string printFile = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string

            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (Request.QueryString["OfficerNumber"] != null)
            {
                ViewState.Add("AutIntNo", Request.QueryString["AutIntNo"]);
                ViewState.Add("OfficerNumber", Request.QueryString["OfficerNumber"]);
                ViewState.Add("DateFrom", Request.QueryString["DateFrom"]);
                ViewState.Add("DateTo", Request.QueryString["DateTo"]);
            }

            if (Request.QueryString["action"] != null && ViewState.Count > 0)
            {
                if (Request.QueryString["action"].ToString() == "PDF")
                {
                    this.ExportPDF();
                }
                else if (Request.QueryString["action"].ToString() == "XLS")
                {
                    this.ExportExcel();
                }
            }
        }

        private void ExportPDF()
        {
            // Setup the report
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("GetUserShiftStatistics", new SqlConnection(this.connectionString));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            DateTime dtHold;
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = string.IsNullOrEmpty(ViewState["AutIntNo"].ToString()) ? 0 : int.Parse(ViewState["AutIntNo"].ToString());
            da.SelectCommand.Parameters.Add("@USOfficerNumber", SqlDbType.VarChar, 10).Value = string.IsNullOrEmpty(ViewState["OfficerNumber"].ToString()) ? "0" : ViewState["OfficerNumber"].ToString();
            string sHold = (ViewState["DateFrom"] == null ? "1900-01-01" : ViewState["DateFrom"].ToString());
            DateTime.TryParse(sHold, out dtHold);
            da.SelectCommand.Parameters.Add("@DateFrom", SqlDbType.DateTime).Value = dtHold;
            sHold = (ViewState["DateTo"] == null ? DateTime.Now.ToString() : ViewState["DateTo"].ToString());
            DateTime.TryParse(sHold, out dtHold);
            da.SelectCommand.Parameters.Add("@DateTo", SqlDbType.DateTime).Value = dtHold;

            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "OfficerStatistics.rpt");

            // Setup the report
            if (reportPage.Equals(""))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "OfficerStatistics.rpt", "OfficerStatisticsReport", "system", "");
                reportPage = "OfficerStatistics.rpt";
            }

            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportName(autIntNo, "OfficerStatistics.rpt");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            ReportDocument report = new ReportDocument();
            report.Load(reportPath);

            //dsNoticePostReturn ds = new dsNoticePostReturn();
            //ds.DataSetName = "dsNoticePostReturn";
            dsOfficerStatistics ds = new dsOfficerStatistics();
            ds.DataSetName = "dsOfficerStatistics";
            try
            {
                da.Fill(ds);
            }
            catch
            {
                Response.Write((string)GetLocalResourceObject("strWriteMsg"));
                return;
            }
            finally
            {
                da.Dispose();
            }

            report.SetDataSource(ds.Tables[1]);
            ds.Dispose();

            // Export the pdf file
            using (MemoryStream ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat))
            {
                // Flush the PDF into the response stream
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }

            da.Dispose();
            report.Dispose();
        }

        private void ExportExcel()
        {
            //GetUserShiftStatistics
            int autIntNo = string.IsNullOrEmpty(ViewState["AutIntNo"].ToString()) ? 0 : int.Parse(ViewState["AutIntNo"].ToString());

            DateTime before;
            string sHold = (ViewState["DateFrom"] == null ? "1900-01-01" : ViewState["DateFrom"].ToString());
            DateTime.TryParse(sHold, out before);

            DateTime after;
            sHold = (ViewState["DateTo"] == null ? DateTime.Now.ToString() : ViewState["DateTo"].ToString());
            DateTime.TryParse(sHold, out after);

            string filename = "OfficerStatistics " + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
            Response.Clear();

            SIL.AARTO.BLL.Report.OfficerStatisticsReport report = new SIL.AARTO.BLL.Report.OfficerStatisticsReport(connectionString);

            MemoryStream stream = report.ExportToExcel(autIntNo, string.IsNullOrEmpty(ViewState["OfficerNumber"].ToString()) ? "0" : ViewState["OfficerNumber"].ToString(), before, after);

            Response.BinaryWrite(stream.GetBuffer());
            Response.End();
        }

    }
}

