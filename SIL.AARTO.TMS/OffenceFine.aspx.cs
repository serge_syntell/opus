using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    public partial class OffenceFine : System.Web.UI.Page
    {
        // Feilds
        private string connectionString;
        private string autName = string.Empty;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string thisPageURL = "OffenceFine.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Offence maintenance";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            autName = Session["autName"].ToString();

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                lblPageName.Text = string.Format((string)GetLocalResourceObject("lblPageName.Text1"), autName);
                pnlAddOffenceFine.Visible = false;
                pnlUpdateOffenceFine.Visible = false;
                dgOffenceFine.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptShowAll.Visible = true;

                PopulateOffences();
                lstOffences.Visible = false;
                lblOffenceList.Visible = false;

                txtSearch.Text = string.Empty;

                //PopulateAuthorites(ugIntNo, autIntNo);

                PopulateOffenceGroups(ddlSelOffenceGroup);
            }
        }

        //protected void PopulateAuthorites(int ugIntNo, int autIntNo)
        //{
        //    UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

        //    SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
        //    ddlSelectLA.DataSource = reader;
        //    ddlSelectLA.DataValueField = "AutIntNo";
        //    ddlSelectLA.DataTextField = "AutName";
        //    ddlSelectLA.DataBind();
        //    ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));
        //    ddlSelectLA.Enabled = false;
        //    reader.Close();
        //}

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlAddOffenceFine.Visible = true;
            pnlGeneral.Visible = true;
            lstOffences.Visible = true;
            lblOffenceList.Visible = true;
            pnlUpdateOffenceFine.Visible = false;
            btnOptDelete.Visible = false;
            this.dtpAddEffectiveDate.Text = DateTime.Today.ToString(DATE_FORMAT);
            txtAddOFFineAmount.Text = "0";
        }

        protected void PopulateOffences()
        {
            int ogIntNo = 0;
            if (ddlSelOffenceGroup.SelectedIndex > -1)
                ogIntNo = Convert.ToInt32(ddlSelOffenceGroup.SelectedValue);

            OffenceDB offence = new OffenceDB(connectionString);

            SqlDataReader reader = offence.GetOffenceList(ogIntNo, txtSearch.Text);
            lstOffences.DataSource = reader;
            lstOffences.DataValueField = "OffIntNo";
            lstOffences.DataTextField = "OffenceName";
            lstOffences.DataBind();

            reader.Close();
        }

        protected void PopulateOffenceGroups(DropDownList ddlSelOffenceGroups)
        {
            OffenceGroupDB group = new OffenceGroupDB(connectionString);

            SqlDataReader reader = group.GetOffenceGroupList("");

            
            Dictionary<int, string> lookups =
                OffenceGroupLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);

            ddlSelOffenceGroups.Items.Add(new ListItem("Search All Groups","0"));

            while (reader.Read())
            {
                int oGIntNo = (int)reader["OGIntNo"];
                if (lookups.ContainsKey(oGIntNo))
                {
                    ddlSelOffenceGroups.Items.Add(new ListItem(lookups[oGIntNo], oGIntNo.ToString()));
                }
            }
            //ddlSelOffenceGroups.DataSource = reader;
            //ddlSelOffenceGroups.DataValueField = "OGIntNo";
            //ddlSelOffenceGroups.DataTextField = "OffenceGroupName";
            ddlSelOffenceGroups.DataBind();
            reader.Close();
        }

        protected void btnAddOffenceFine_Click(object sender, System.EventArgs e)
        {
            //20130107 Added by Nancy for check the fine amount convertion, if failed, return with error.
            double fineAmount = 0;
            if (!Double.TryParse(txtAddOFFineAmount.Text, out fineAmount))
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.InvalidFineAmount");
                this.txtOFFineAmount.Focus();
                return;
            }

            if (lstOffences.SelectedIndex < 0)
            {
                lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text"); 
                return;
            }

            DateTime effectiveDate;

            if (!DateTime.TryParse(dtpAddEffectiveDate.Text, out effectiveDate))
            {
                lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1"); 
                return;
            }

            int offIntNo = Convert.ToInt32(lstOffences.SelectedValue);

            // add the new transaction to the database table
            Stalberg.TMS.OffenceFineDB fineAdd = new OffenceFineDB(connectionString);

            int addOFIntNo = fineAdd.AddOffenceFine(offIntNo, autIntNo,
                effectiveDate, Convert.ToDouble(txtAddOFFineAmount.Text), this.loginUser);

            if (addOFIntNo <= 0)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2"); 
            }
            else
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceFinesMaintenance, PunchAction.Add);  

            }

            lblError.Visible = true;
            BindFineGrid();

        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            //Added by Jacob 20121227 start
            // check fine amount convertion, if failed, return with error.
            double fineAmount = 0;
            if (!Double.TryParse(txtOFFineAmount.Text, out fineAmount))
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.InvalidFineAmount");
                this.txtOFFineAmount.Focus();
                return;
            }
            //Added by Jacob 20121227 end


            DateTime effectiveDate;

            if (!DateTime.TryParse(dtpOFEEffectiveDate.Text, out effectiveDate))
            {
                lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1"); 
                return;
            }

            int ofIntNo = Convert.ToInt32(Session["editOFIntNo"]);

            // add the new transaction to the database table
            Stalberg.TMS.OffenceFineDB offUpdate = new OffenceFineDB(connectionString);

            //removing authority and Offence from the update stmt - they should not change!!!
            int updOFIntNo = offUpdate.UpdateOffenceFine(ofIntNo,
                effectiveDate, Convert.ToDouble(txtOFFineAmount.Text), this.loginUser);

            if (updOFIntNo <= 0)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4"); 
            }
            else
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceFinesMaintenance, PunchAction.Change); 
            }

            lblError.Visible = true;

            BindFineGrid();
        }

        protected void ShowOffenceFineDetails(int editOFIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.OffenceFineDB fine = new Stalberg.TMS.OffenceFineDB(connectionString);

            Stalberg.TMS.OffenceFineDetails fineDetails = fine.GetOffenceFineDetails(editOFIntNo);

            this.dtpOFEEffectiveDate.Text = fineDetails.OFEffectiveDate.ToString(DATE_FORMAT);
            txtOFFineAmount.Text = String.Format("{0:f}", fineDetails.OFFineAmount);

            pnlUpdateOffenceFine.Visible = true;
            pnlAddOffenceFine.Visible = false;

            btnOptDelete.Visible = true;
        }

        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
            int ofIntNo = Convert.ToInt32(Session["editOFIntNo"]);

            OffenceFineDB fine = new Stalberg.TMS.OffenceFineDB(connectionString);

            string delOFIntNo = fine.DeleteOffenceFine(ofIntNo);

            if (delOFIntNo.Equals("0"))
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            }
            else
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceFinesMaintenance, PunchAction.Delete); 
            }

            lblError.Visible = true;
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //2012-3-2 Linda Modified into a multi- language
            btnOptShowAll.Text = (string)GetLocalResourceObject("btnOptShowAll.Text1");

            BindOffenceFineGrid();

            pnlGeneral.Visible = false;
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            //2012-3-2 Linda Modified into a multi- language
            btnOptShowAll.Text = (string)GetLocalResourceObject("btnOptShowAll.Text1");

            BindOffenceFineGrid();

            pnlGeneral.Visible = false;
            pnlOffenceGroup.Visible = false;
            //PunchStats805806 enquiry OffenceFines
        }

        protected void lstOffences_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (lstOffences.SelectedIndex < 0)
            {
                lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8"); 
                return;
            }

            BindFineGrid();
        }

        protected void BindFineGrid()
        {
            //int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int offIntNo = Convert.ToInt32(lstOffences.SelectedValue);

            OffenceFineDB fine = new OffenceFineDB(connectionString);

            DataSet data = fine.GetOffenceFineListByOffenceDS(autIntNo, offIntNo);
            dgFines.DataSource = data;
            dgFines.DataKeyField = "OFIntNo";

            try
            {
                dgFines.DataBind();
            }
            catch
            {
                dgFines.CurrentPageIndex = 0;
                dgFines.DataBind();
            }

            if (dgFines.Items.Count == 0)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9"); 
                lblError.Visible = true;

                pnlAddOffenceFine.Visible = true;
                pnlUpdateOffenceFine.Visible = false;
                dgFines.Visible = false;

                Session["editOFIntNo"] = 0;
                this.dtpAddEffectiveDate.Text = DateTime.Today.ToString(DATE_FORMAT);
                txtAddOFFineAmount.Text = "0";
                return;
            }

            data.Dispose();

            lblError.Visible = false;
            pnlAddOffenceFine.Visible = false;
            pnlUpdateOffenceFine.Visible = false;
            dgFines.Visible = true;
        }

        protected void BindOffenceFineGrid()
        {
            //int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int ogIntNo = Convert.ToInt32(ddlSelOffenceGroup.SelectedValue);

            // Obtain and bind a list of all users
            Stalberg.TMS.OffenceFineDB fineList = new Stalberg.TMS.OffenceFineDB(connectionString);

            DataSet data = fineList.GetOffenceFineListByGroupDS(autIntNo, ogIntNo, txtSearch.Text);
            dgOffenceFine.DataSource = data;
            dgOffenceFine.DataKeyField = "OFIntNo";
            try
            {
                dgOffenceFine.DataBind();
            }
            catch
            {
                dgOffenceFine.CurrentPageIndex = 0;
                dgOffenceFine.DataBind();
            }
            if (dgOffenceFine.Items.Count == 0)
            {
                dgOffenceFine.Visible = false;
                //2012-3-2 Linda Modified into a multi- language
                btnOptShowAll.Text = (string)GetLocalResourceObject("btnOptShowAll.Text");
                lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10"); 
                pnlGeneral.Visible = true;

            }
            else
            {
                dgOffenceFine.Visible = true;
            }

            data.Dispose();
            pnlAddOffenceFine.Visible = false;
            pnlUpdateOffenceFine.Visible = false;

        }


        protected void btnOptShowAllFines_Click(object sender, System.EventArgs e)
        {
            if (dgOffenceFine.Visible.Equals(true))
            {
                //hide it
                dgOffenceFine.Visible = false;
                pnlGeneral.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                btnOptShowAll.Text = (string)GetLocalResourceObject("btnOptShowAll.Text");
            }
            else
            {
                //show it
                dgOffenceFine.Visible = true;
                pnlGeneral.Visible = false;
                //2012-3-2 Linda Modified into a multi- language
                btnOptShowAll.Text = (string)GetLocalResourceObject("btnOptShowAll.Text1");

                BindOffenceFineGrid();
            }
            pnlOffenceGroup.Visible = false;
        }

        protected void dgFines_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgFines.SelectedIndex = e.Item.ItemIndex;

            if (dgFines.SelectedIndex > -1)
            {
                int editOFIntNo = Convert.ToInt32(dgFines.DataKeys[dgFines.SelectedIndex]);

                Session["prevPage"] = thisPageURL;
                Session["editOFIntNo"] = editOFIntNo;

                ShowOffenceFineDetails(editOFIntNo);
            }
        }

        protected void ddlSelOffenceGroup_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //2012-3-2 Linda Modified into a multi- language
            btnOptShowAll.Text = (string)GetLocalResourceObject("btnOptShowAll.Text1");

            BindOffenceFineGrid();
            PopulateOffences();

            pnlGeneral.Visible = false;
        }

       

        protected void dgOffenceFine_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgOffenceFine.CurrentPageIndex = e.NewPageIndex;

            BindOffenceFineGrid();
        }

        protected void btnOptUpdateOffenceFine_Click(object sender, EventArgs e)
        {
            // allow transactions to be added to the database table
            btnOptDelete.Visible = false;
            pnlUpdateOffenceFine.Visible = false;
            pnlOffenceGroup.Visible = true;
            pnlAddOffenceFine.Visible = false;
        }

        protected void btnUpdateFine_Click(object sender, System.EventArgs e)
        {
            OffenceFineDB of = new Stalberg.TMS.OffenceFineDB(connectionString);

            DateTime dt;

            if (!DateTime.TryParse(dtpUpdateToDate.Text, out dt))
            {
                lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11"); 
                return;
            }

            int successRecordCount = 0;
            int ogIntNo = Convert.ToInt32(ddlSelOffenceGroup.SelectedItem.Value);
            //this.lblError.Text = of.UpdateSpecial(autIntNo.ToString(), this.txtEffectiveDate.Text, this.txtUpdateToDate.Text, sHold);
            //2013-12-18 Heidi changed for add all Punch Statistics Transaction(5084)
            this.lblError.Text = of.UpdateSpecial(autIntNo.ToString(), this.dtpEffectiveDate.Text,
                this.dtpUpdateToDate.Text, ogIntNo, this.loginUser, out successRecordCount);

            if (successRecordCount > 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceFinesMaintenance, PunchAction.Change);
            }
        }

        protected void tdSel_OnMouseOver(object sender, EventArgs e)
        {
            ddlSelOffenceGroup.ToolTip = ddlSelOffenceGroup.SelectedValue;
        }

    }
}
