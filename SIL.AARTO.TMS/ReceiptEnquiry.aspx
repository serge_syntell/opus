<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ReceiptEnquiry" Codebehind="ReceiptEnquiry.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet"/>
    <meta content="<%= description %>" name="Description"/>
    <meta content="<%= keywords %>" name="Keywords"/>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0" 
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table height="85%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center" style="width: 182px">
                <img style="height: 1px" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                &nbsp;
            </td>
            <td valign="top" align="left" width="100%" colspan="1">
                <asp:UpdatePanel ID="UDP" runat="server">
                    <ContentTemplate>
                        <table height="482" width="100%" border="0">
                            <tr>
                                <td valign="top" height="47">
                                    <p align="center">
                                        <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:Panel ID="pnlSelectTicket" runat="server" Visible="False" Width="100%">
                                        <asp:Panel ID="pnlSurname" DefaultButton="btnFindOffender" runat="server" Width="100%">
                                            <table id="Table4" height="78" cellspacing="1" cellpadding="1" border="0" class="Normal"
                                                width="500">
                                                <tr>
                                                    <td height="22" colspan="2">
                                                        <asp:Label ID="Label20" runat="server" CssClass="NormalRed" EnableViewState="False" Text="<%$Resources:lblEnterEither.Text %>"></asp:Label></td>
                                                    <td height="22" style="width: 67px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 210px">
                                                        <asp:Label ID="lblSelectName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelectName.Text %>"></asp:Label></td>
                                                    <td>
                                                        <asp:TextBox ID="txtOffenderName" runat="server" CssClass="Normal" MaxLength="50"
                                                            Width="200px"></asp:TextBox></td>
                                                    <td>
                                                        <asp:Button ID="btnFindOffender" runat="server" CssClass="NormalButton" OnClick="btnFindOffender_Click"
                                                            Text="<%$Resources:btnFindOffender.Text %>" Width="64px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblOr.Text %>"></asp:Label></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlTicketNo" DefaultButton="btnFindTicket" runat="server" Width="100%">
                                            <table id="Table1" height="78" cellspacing="1" cellpadding="1" border="0" class="Normal"
                                                width="500">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTicketNo.Text %>"
                                                            Width="208px"></asp:Label></td>
                                                    <td style="width: 312px">
                                                        <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected">
                                                        </uc1:TicketNumberSearch>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblSelectTicket" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelectTicket.Text %>"></asp:Label></td>
                                                    <td style="width: 312px">
                                                        &nbsp;<asp:TextBox ID="txtTicketNumber" runat="server" CssClass="Normal" MaxLength="25"
                                                            Width="199px"></asp:TextBox>
                                                        &nbsp;&nbsp;
                                                        <asp:Button ID="btnFindTicket" runat="server" CssClass="NormalButton" OnClick="btnFindTicket_Click"
                                                            Text="<%$Resources:btnFindTicket.Text %>" Width="79px" /></td>
                                                    <td align="center" style="width: 7px">
                                                        <asp:CheckBox ID="chkReversal" runat="server" CssClass="NormalBold" Text="<%$Resources:chkReversal.Text %>" />&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 210px">
                                                    </td>
                                                    <td style="width: 312px">
                                                    </td>
                                                    <td style="width: 7px">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlBarcode" DefaultButton="btnBarcode" runat="server" Width="100%">
                                            <table id="Table2" height="78" cellspacing="1" cellpadding="1" border="0" class="Normal"
                                                width="500">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblOr.Text %>"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" style="width: 210px">
                                                        <asp:Label ID="lblBarcode" runat="server" Text="<%$Resources:lblBarcode.Text %>"></asp:Label> </td>
                                                    <td>
                                                        <asp:TextBox ID="txtBarcode" runat="server" CssClass="Normal" MaxLength="25" TextMode="Password"
                                                            Width="200px"></asp:TextBox></td>
                                                    <td>
                                                        <asp:Button ID="btnBarcode" runat="server" CssClass="NormalButton" OnClick="btnBarcode_Click"
                                                            Text="<%$Resources:btnBarcode.Text %>" Width="64px" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </asp:Panel>
                                    <!-- Start Content -->
                                    <!-- End Content -->
                                    <asp:Panel ID="pnlDisplaySelectedTickets" runat="server">
                                        &nbsp;
                                        <asp:GridView ID="dgTicketDetails" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                            CssClass="Normal" GridLines="Horizontal" ShowFooter="True" OnSelectedIndexChanging="dgTicketDetails_SelectedIndexChanging">
                                            <FooterStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundField DataField="RctIntNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText %>"></asp:BoundField>
                                                <asp:BoundField DataField="NotTicketNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText1 %>"></asp:BoundField>
                                                <asp:TemplateField HeaderText="<%$Resources:dgTicketDetails.HeaderText7 %>">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%#Eval("NotOffenceDate", "{0:yyyy-dd-MM HH:mm}") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-dd-MM HH:mm}")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FullName" HeaderText="<%$Resources:dgTicketDetails.HeaderText2 %>"></asp:BoundField>
                                                <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText3 %>"></asp:BoundField>
                                                <asp:BoundField DataField="NotLocDescr" HeaderText="<%$Resources:dgTicketDetails.HeaderText4 %>"></asp:BoundField>
                                                <asp:BoundField DataField="RctNumber" HeaderText="<%$Resources:dgTicketDetails.HeaderText5 %>"></asp:BoundField>
                                                <asp:BoundField DataField="RTAmount" HeaderText="<%$Resources:dgTicketDetails.HeaderText6 %>"></asp:BoundField>
                                                <asp:CommandField HeaderText="<%$Resources:dgTicketDetails.HeaderText8 %>" ShowSelectButton="True" SelectText="<%$Resources:dgTicketDetails.HeaderText8 %>">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:CommandField>
                                            </Columns>
                                            <HeaderStyle CssClass="CartListHead" />
                                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                                            <SelectedRowStyle CssClass="RowHighlighted" />
                                        </asp:GridView>
                                    </asp:Panel>
                                    &nbsp;
                                    <table>
                                        <tr valign="top" width="100%">
                                            <td align="left">
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr valign="top" width="100%">
                                            <td align="left">
                                                <asp:Label ID="lblScript" runat="server" />
                                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                                                &nbsp;
                                                <asp:Button ID="btnOptReverse" runat="server" Width="135px" CssClass="NormalButton"
                                                    Text="<%$Resources:btnOptReverse.Text %>" OnClick="btnOptReverse_Click" Visible="False"></asp:Button>&nbsp;
                                                <asp:Button ID="btnOptPrintEnquiry" runat="server" CssClass="NormalButton" OnClick="btnPrintEnquiry_Click"
                                                    Text="<%$Resources:btnOptPrintEnquiry.Text %>" Visible="False" Width="135px"></asp:Button>&nbsp;
                                                <asp:Button OnClick="btnPrintReceipt_Click" ID="btnOptPrintReceipt" runat="server"
                                                    Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptPrintReceipt.Text %>" Visible="False"></asp:Button>&nbsp;
                                                                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label2"
                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center" style="width: 182px">
            </td>
            <td valign="top" align="left" width="100%">
            </td>
        </tr>
    </table>
    <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript" language="javascript">
   function CheckMe()
   {
        var id = document.getElementById("textId");
        var ticket = document.getElementById("textTicket");
       
        if(id.length == 0 &&  ticket.length == 0)
        { 
            alert("You need to supply either an ID number or a ticket number."); 
        } 
   }
    </script>

</body>
</html>
