<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.NoRecordFound" Codebehind="NoRecordFound.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <%=title%>
    </title>
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
      
    </script>

</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="form" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td align="center" class="NormalBold">
                <br />
                    <asp:Label ID="Label1" runat="server" Text="<%$Resources:lblRecords.Text %>"></asp:Label>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="button" value=" <%$Resources:button.Text %> " class="NormalButton" onclick="javascript:window.close();"
                        id="Button1" runat="server" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
