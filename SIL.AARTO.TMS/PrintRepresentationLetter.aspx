﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.PrintRepresentationLetter" Codebehind="PrintRepresentationLetter.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet"/>
    <meta content="<%= description %>" name="Description"/>
    <meta content="<%= keywords %>" name="Keywords"/>
    <style type="text/css">
        .style1
        {
            height: 2px;
        }
    </style>
</head>
<body style="margin:0px,0px,0px,0px; background:<%=backgroundImage %> " >
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height:5%">
            <tr>
                <td class="HomeHead" align="center" style="width:100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" style="height:85%">
            <tr>
                
                <td valign="top" align="left" style="width:100%" colspan="1">
                    <table cellspacing="0" cellpadding="0" border="0" style="height:90%" width="100%">
                        <tr>
                            <td valign="top" style="height: 49px; width: 819px;">
                                <p style="text-align:center">
                                    <asp:Label ID="lblPageName" runat="server" Width="790px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p style="text-align:center">
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="width: 819px">
                                <asp:UpdatePanel ID="upd" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lblError_Print" runat="Server" CssClass="NormalRed"></asp:Label>
                                        <asp:Panel ID="pnlGeneral" runat="server">
                                            <table style="width: 877px">
                                              <tr>
                                                    <td style="height: 2px">
                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                                    <td valign="middle" class="style1">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px">
                                                        </asp:DropDownList></td>
                                                    <td style="height: 2px; " valign="middle" align="left"><asp:CheckBox ID="chkShowAll" runat="server" AutoPostBack="True" 
                                                            CssClass="NormalBold" OnCheckedChanged="chkShowAll_CheckedChanged" 
                                                            Text="<%$Resources:chkShowAll.Text %>" />
                                                        &nbsp;</td>
                                                </tr>
                                                
                                                
                                            </table>
                                        </asp:Panel>
                                        &nbsp;
                                        <br />
                                        <div>
                                        <asp:DataGrid ID="dgPrintrun" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                            Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgPrintrun_ItemCommand"
                                            OnItemCreated="dgPrintrun_ItemCreated" >
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="PrintFileName" HeaderText="<%$Resources:dgPrintrun.PrintFileName.HeaderText %>" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoOfNotices" HeaderText="<%$Resources:dgPrintrun.NoOfNotices.HeaderText %>"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.Template1.HeaderText %>" >
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server"
                                                            Target="_blank" Text="<%$Resources:hkPrint.Text %>" ID="hlPrint" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.Template2.HeaderText %>">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID = "btnUpdate" runat="server" CausesValidation="false" 
                                                        CommandName="Select" Text="<%$Resources:dgPrintrun.btnUpdate.Text %>"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>                                                                           </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <pager:AspNetPager id="dgPrintrunPager" runat="server" 
                                    showcustominfosection="Right" width="400px" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                      FirstPageText="|&amp;lt;" 
                                    LastPageText="&amp;gt;|" 
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                    CustomInfoStyle="float:right;" PageSize="10" 
                                                onpagechanged="dgPrintrunPager_PageChanged" UpdatePanelId="upd"></pager:AspNetPager>
                                        </div>&nbsp;
                                        <br />
                                        <asp:Label ID="lblInfo" runat="Server" CssClass="NormalRed"></asp:Label>
                                        <br />
                                        <br />
                                        <asp:Label ID="lblInstruct" runat="server" Width="465px" BorderStyle="Inset" CssClass="CartListHead"
                                            Height="35px" EnableViewState="false" Text="<%$Resources:lblInstruct.Text %>"></asp:Label></td>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="udp" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height:5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width:100%">
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
