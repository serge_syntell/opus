﻿using System;
using Stalberg.TMS.Data.Util;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;
using System.Web.UI.WebControls;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Notice Post management page
    /// </summary>
    public partial class NoticeExpiryReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        //private string thisPage = "Notice Expiry Report";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "NoticeExpiryReport.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (!IsPostBack)
            {
                ddlDateRule.Items.Clear();
                ddlDateRule.Items.Insert(0, (string)GetLocalResourceObject("ddlDateRuleItems.Text"));
                ddlDateRule.Items.Insert(1, (string)GetLocalResourceObject("ddlDateRuleItems.Text1"));
                ddlDateRule.SelectedIndex = 0;
                PopulateAuthorityList();
            }
        }

       

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (ddlSelAuthority.SelectedIndex == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            int iWarningPeriod = 0;
            if (!Int32.TryParse(tbWaningPeriod.Text.ToString().Trim(), out iWarningPeriod))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            if (iWarningPeriod < 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            int AutIntNo = Convert.ToInt32(ddlSelAuthority.SelectedValue);

            DateRulesDetails dateRulesDetails = new DateRulesDetails();
            dateRulesDetails.AutIntNo = AutIntNo;
            dateRulesDetails.LastUser = this.login;

            if (ddlDateRule.SelectedIndex == 0)
            {
                dateRulesDetails.DtRStartDate = "NotOffenceDate";
                dateRulesDetails.DtREndDate = "NotExpireDate";              
            }
            else
            {
                dateRulesDetails.DtRStartDate = "NotOffenceDate";
                dateRulesDetails.DtREndDate = "NotSummonsBeforeDate";
            }

            DefaultDateRules defaultDateRules = new DefaultDateRules(dateRulesDetails, this.connectionString);
            defaultDateRules.SetDefaultDateRule();

            int iExpiryDays = dateRulesDetails.DtRNoOfDays;

            PageToOpen pto = new PageToOpen(this.Page, "NoticeExpiryReportViewer.aspx");

            pto.Parameters.Add(new QSParams("ExpiryDays", iExpiryDays.ToString()));
            pto.Parameters.Add(new QSParams("Sort", ddlSort.SelectedIndex.ToString()));
            pto.Parameters.Add(new QSParams("WarningPeriod", tbWaningPeriod.Text.ToString()));
            pto.Parameters.Add(new QSParams("AutIntNo", AutIntNo.ToString()));
            pto.Parameters.Add(new QSParams("Rule", ddlDateRule.SelectedIndex.ToString()));

            //PunchStats805806 enquiry NoticeExpiryReport

            Helper_Web.BuildPopup(pto);
        }

        protected void PopulateAuthorityList()
        {
            AuthorityDB auth = new AuthorityDB(connectionString);

            SqlDataReader reader = auth.GetAuthorityList(0, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int AutIntNo=(int)reader["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
	            {
                    ddlSelAuthority.Items.Add(new ListItem( lookups[AutIntNo],AutIntNo.ToString()));
	            }
            }
            
            //ddlSelAuthority.DataSource = reader;
            //ddlSelAuthority.DataValueField = "AutIntNo";
            //ddlSelAuthority.DataTextField = "AutDescr";
            //ddlSelAuthority.DataBind();

            ddlSelAuthority.Items.Insert(0, (string)GetLocalResourceObject("ddlSelAuthorityItems.Text"));
            ddlSelAuthority.SelectedIndex = ddlSelAuthority.Items.IndexOf(ddlSelAuthority.Items.FindByValue(this.autIntNo.ToString()));

            reader.Close();
        }
    }
}







