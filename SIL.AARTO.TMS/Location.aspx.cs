using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    public partial class Location : System.Web.UI.Page
    {
        private string conString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        protected string thisPageURL = "Location.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        LocationService locationService = new LocationService();
        IList<SIL.AARTO.DAL.Entities.Location> locationList;
        //protected string thisPage = "Location maintenance";

        protected override void OnLoad(EventArgs e)
        {
            conString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            //Stalberg.TMS.UserDB user = new UserDB(conString);
            //Stalberg.TMS.UserDetails userDetails = new UserDetails();
            //userDetails = (UserDetails)Session["userDetails"];

            loginUser = ((UserDetails)Session["userDetails"]).UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {//2012-3-1 Linda Modified into a multi- language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddLocation.Visible = false;
                pnlUpdateLocation.Visible = false;
                btnOptDelete.Visible = false;

                PopulateAuthorites( autIntNo);

                PopulateRoadTypes(ddlRoadType);
                PopulateSpeedZone(ddlSpeedZone);
                this.PopulateStaticLists();

                this.PopulateLocType();
                this.PopulateLocRegion();

                if (autIntNo > 0)
                {
                    BindGrid(autIntNo);
                    PopulateCourt(ddlAuth_Court, autIntNo);
                    PopulateLocationSuburb(ddlAuth_LocationSuburb, autIntNo);
                }
                else
                {
                    //2012-3-2 Linda Modified into a multi- language
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text"); 
                    lblError.Visible = true;
                }
            }
        }


        protected void PopulateCourt(DropDownList ddlSelCourt, int autIntNo)
        {
            ddlSelCourt.Items.Clear();
            CourtDB court = new CourtDB(conString);
            SqlDataReader reader = court.GetAuth_CourtListByAuth(autIntNo);
            ddlSelCourt.DataSource = reader;
            ddlSelCourt.DataValueField = "ACIntNo";
            ddlSelCourt.DataTextField = "CrtName";
            ddlSelCourt.DataBind();

            reader.Close();
        }

        protected void PopulateLocationSuburb(DropDownList ddlLoctionSuburb, int autIntNo)
        {
            #region Jerry 2013-09-12 change it
            //VehicleTypeDB vehicleTypeDB = new VehicleTypeDB(conString);

            //AuthorityService authorityService = new AuthorityService();
            //SIL.AARTO.DAL.Entities.Authority autEntity=authorityService.GetByAutIntNo(autIntNo);

            //string autCode = "";
            //if (autEntity != null)
            //{ autCode = autEntity.AutCode; }

            //DataSet ds = vehicleTypeDB.GetValueTextListByTableNameDS("LocationSuburb", autCode);
            #endregion

            LocationSuburbDB locationSuburbDB = new LocationSuburbDB(conString);
            DataSet ds = locationSuburbDB.GetLocationSuburbListByAutIntNo(autIntNo);

            ddlLoctionSuburb.DataSource = ds.Tables[0];
            ddlLoctionSuburb.DataValueField = "DTDLDValue";
            ddlLoctionSuburb.DataTextField = "DTDLDLText";
            ddlLoctionSuburb.DataBind();
            ListItem item = new ListItem();
            item.Value ="0";
            item.Text=(string)GetLocalResourceObject("ddlLoctionSuburb.Items");

            ddlLoctionSuburb.Items.Insert(0,item);

            
        }

        protected void PopulateRoadTypes(DropDownList ddlSelRoadType)
        {
            ddlSelRoadType.Items.Clear();
            RoadTypeDB road = new RoadTypeDB(conString);

            SqlDataReader reader = road.GetRoadTypeList();

            Dictionary<int, string> lookups =
                RoadTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int rdTIntNo = (int)reader["RdTIntNo"];
                if (lookups.ContainsKey(rdTIntNo))
                {
                    ddlSelRoadType.Items.Add(new ListItem(lookups[rdTIntNo], rdTIntNo.ToString()));
                }
            }
            //ddlSelRoadType.DataSource = reader;
            //ddlSelRoadType.DataValueField = "RdTIntNo";
            //ddlSelRoadType.DataTextField = "RdTypeDescr";
            //ddlSelRoadType.DataBind();

            reader.Close();
        }

        protected void PopulateStaticLists()
        {
            //2012-3-7 linda modified dropdownlist into multi-language
            this.ddlTravelDirection.Items.Clear();
            this.ddlTravelDirection.Items.Add((string)GetLocalResourceObject("ddlTravelDirection.Items1"));
            this.ddlTravelDirection.Items.Add((string)GetLocalResourceObject("ddlTravelDirection.Items2"));
            this.ddlTravelDirection.Items.Add((string)GetLocalResourceObject("ddlTravelDirection.Items3"));
            this.ddlTravelDirection.Items.Add((string)GetLocalResourceObject("ddlTravelDirection.Items4"));

            this.ddlAddTravelDirection.Items.Clear();
            this.ddlAddTravelDirection.Items.Add((string)GetLocalResourceObject("ddlTravelDirection.Items1"));
            this.ddlAddTravelDirection.Items.Add((string)GetLocalResourceObject("ddlTravelDirection.Items2"));
            this.ddlAddTravelDirection.Items.Add((string)GetLocalResourceObject("ddlTravelDirection.Items3"));
            this.ddlAddTravelDirection.Items.Add((string)GetLocalResourceObject("ddlTravelDirection.Items4"));

            this.comboProvince.Items.Clear();
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items1"));
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items2"));
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items3"));
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items4"));
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items5"));
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items6"));
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items7"));
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items8"));
            this.comboProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items9"));
            this.comboProvince.Items.Insert(0, (string)GetLocalResourceObject("comboProvince.Items10"));

            this.comboUpdProvince.Items.Clear();
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items1"));
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items2"));
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items3"));
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items4"));
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items5"));
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items6"));
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items7"));
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items8"));
            this.comboUpdProvince.Items.Add((string)GetLocalResourceObject("comboProvince.Items9"));
            this.comboUpdProvince.Items.Insert(0, (string)GetLocalResourceObject("comboProvince.Items10"));
        }

        //2013-09-09 Edge added for Loction type ddl
        protected void PopulateLocType()
        {
            this.ddlAddLocType.Items.Clear();
            this.ddlLocType.Items.Clear();

            ListItem item1 = new ListItem((string)GetLocalResourceObject("LocType.Item1"), "F");
            this.ddlAddLocType.Items.Add(item1);
            this.ddlLocType.Items.Add(item1);
            ListItem item2 = new ListItem((string)GetLocalResourceObject("LocType.Item2"), "M");
            this.ddlAddLocType.Items.Add(item2);
            this.ddlLocType.Items.Add(item2);
        }

        //2013-09-09 Edge added for Loction region ddl
        protected void PopulateLocRegion()
        {
            this.ddlAddLocRegion.Items.Clear();
            this.ddlLocRegion.Items.Clear();

            ListItem item1 = new ListItem((string)GetLocalResourceObject("ddlTravelDirection.Items1"), "North");
            this.ddlAddLocRegion.Items.Add(item1);
            this.ddlLocRegion.Items.Add(item1);

            ListItem item2 = new ListItem((string)GetLocalResourceObject("ddlTravelDirection.Items2"), "South");
            this.ddlAddLocRegion.Items.Add(item2);
            this.ddlLocRegion.Items.Add(item2);

            ListItem item3 = new ListItem((string)GetLocalResourceObject("ddlTravelDirection.Items3"), "East");
            this.ddlAddLocRegion.Items.Add(item3);
            this.ddlLocRegion.Items.Add(item3);

            ListItem item4 = new ListItem((string)GetLocalResourceObject("ddlTravelDirection.Items4"), "West");
            this.ddlAddLocRegion.Items.Add(item4);
            this.ddlLocRegion.Items.Add(item4);
        }

        protected void PopulateSpeedZone(DropDownList ddlSelSpeedZone)
        {
            ddlSelSpeedZone.Items.Clear();
            SpeedZoneDB speed = new SpeedZoneDB(conString);

            SqlDataReader reader = speed.GetSpeedZoneList();
            
            Dictionary<int, string> lookups =
                SpeedZoneLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int sZIntNo = (int)reader["SZIntNo"];
                if (lookups.ContainsKey(sZIntNo))
                {
                    ddlSelSpeedZone.Items.Add(new ListItem(lookups[sZIntNo], sZIntNo.ToString()));
                }
            }
            //ddlSelSpeedZone.DataSource = reader;
            //ddlSelSpeedZone.DataValueField = "SZIntNo";
            //ddlSelSpeedZone.DataTextField = "SZSpeed";
            //ddlSelSpeedZone.DataBind();

            reader.Close();
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorites( int autIntNo)
        {
            int mtrIntNo = 0;


            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(this.conString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(conString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        protected void BindGrid(int autIntNo)
        {
            // Obtain and bind a list of all users
            LocationDB locList = new LocationDB(conString);
            int totalCount = 0; // 2013-03-07 Henry add
            DataSet data = locList.GetLocationListDS(autIntNo, txtSearch.Text, "LocCameraCode", out totalCount, dgLocationPager.PageSize, dgLocationPager.CurrentPageIndex - 1);
            dgLocation.DataSource = data;
            dgLocation.DataKeyField = "LocIntNo";
            dgLocationPager.RecordCount = totalCount;

            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgLocation.DataBind();
            }
            catch
            {
                dgLocation.CurrentPageIndex = 0;
                dgLocation.DataBind();
            }

            if (dgLocation.Items.Count == 0)
            {
                dgLocation.Visible = false;
                lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1"); 
            }
            else
            {
                dgLocation.Visible = true;
            }

            data.Dispose();
            pnlAddLocation.Visible = false;
            pnlUpdateLocation.Visible = false;

            //Seawen 2013-09-13 pager visible equal to grid
            dgLocationPager.Visible = dgLocation.Visible;
        }

        protected void btnOptAdd_Click(object sender, EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlAddLocation.Visible = true;
            pnlUpdateLocation.Visible = false;
            btnOptDelete.Visible = false;

            txtAddLocOffenceSpeedStart.Text = "0";

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            PopulateCourt(ddlAddAuth_Court, autIntNo);
            //2012-3-7 linda modified dropdownlist into multi-language
            ddlAddAuth_Court.Items.Insert(0,new ListItem((string)GetLocalResourceObject("ddlAddAuth_Court.Items"),"0"));

            //2013-05-15 Heidi added for task 4958
            PopulateLocationSuburb(ddlAddAuth_LocationSuburb, autIntNo);
         
           

            PopulateRoadTypes(ddlAddRoadType);
            ddlAddRoadType.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlAddRoadType.Items"),"0"));

            PopulateSpeedZone(ddlAddSpeedZone);
            ddlAddSpeedZone.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlAddSpeedZone.Items"),"0"));

            this.PopulateStaticLists();
            ddlAddTravelDirection.SelectedIndex = ddlAddTravelDirection.Items.IndexOf(ddlAddTravelDirection.Items.FindByText("North"));

            ddlAddLocType.SelectedValue = "M";
            ddlAddLocRegion.SelectedValue = "North";

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
        }

        protected void btnAddLocation_Click(object sender, EventArgs e)
        {
            
            if (txtAddLocDescr.Text.Trim().Length > txtAddLocDescr.MaxLength)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2")+txtAddLocDescr.MaxLength.ToString();
                lblError.Visible = true;
                return;
            }

            if (ddlAddAuth_LocationSuburb.SelectedValue == "0")
            {
                //2013-05-23 Heidi added
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                lblError.Visible = true;
                return;

            }

            if (ddlAddAuth_Court.SelectedValue=="0"
                ||ddlAddRoadType.SelectedValue=="0"
                ||ddlAddSpeedZone.SelectedValue == "0")
            {
                //2013-05-23 Heidi added for fixed bug
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                lblError.Visible = true;
                return;

            }

            //2015-03-11 Heidi added checked UK constraint(bontq1913).
            SIL.AARTO.DAL.Entities.Location location = locationService.GetByLocDescrLocCodeAcIntNo(txtAddLocDescr.Text.Trim(), txtAddLocCode.Text.Trim(),Convert.ToInt32(ddlAddAuth_Court.SelectedValue));
            if (location!=null)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12")+
                string.Format((string)GetLocalResourceObject("lblError.Text13"), txtAddLocDescr.Text.Trim(), txtAddLocCode.Text.Trim(), ddlAddAuth_Court.SelectedItem.Text);
                lblError.Visible = true;
                return;
            }
            SIL.AARTO.DAL.Entities.Location location2 = locationService.GetByLocDescrLocCameraCodeAcIntNo(txtAddLocDescr.Text.Trim(), txtAddLocCameraCode.Text.Trim(), Convert.ToInt32(ddlAddAuth_Court.SelectedValue));
            if (location2 != null)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12")+
                string.Format((string)GetLocalResourceObject("lblError.Text14"), txtAddLocDescr.Text.Trim(), txtAddLocCameraCode.Text.Trim(), ddlAddAuth_Court.SelectedItem.Text);
                lblError.Visible = true;
                return;
            }

            // add the new transaction to the database table
            LocationDB locAdd = new LocationDB(conString);

            int addLocIntNo = locAdd.AddLocation
                (Convert.ToInt32(ddlAddAuth_Court.SelectedValue),
                Convert.ToInt32(ddlAddSpeedZone.SelectedValue), Convert.ToInt32(ddlAddRoadType.SelectedValue),
                txtAddLocCameraCode.Text.Trim(), txtAddLocCode.Text.Trim(), txtAddLocDescr.Text.Trim(),
                txtAddLocStreetCode.Text, txtAddLocStreetName.Text, ddlAddTravelDirection.SelectedItem.ToString().Substring(0, 1),
                Convert.ToInt32(txtAddLocOffenceSpeedStart.Text),
                this.comboUpdProvince.SelectedValue,
                this.textCity.Text,
                ddlAddAuth_LocationSuburb.SelectedItem.Text,//this.textSuburb.Text,
                this.textStreetA.Text,
                this.textStreetB.Text,
                this.textRoute.Text,
                this.textFrom.Text,
                this.textTo.Text,
                this.textGpsX.Text,
                this.textGpsY.Text,
                this.txtAddLocBranchCode.Text, this.loginUser,
                Convert.ToInt32(ddlAddAuth_LocationSuburb.SelectedValue),
                this.ddlAddLocType.SelectedValue,
                this.ddlAddLocRegion.SelectedValue,
                this.chkAddRailwayCrossing.Checked //2013-12-12 Heidi added for check Railway Crossing on Location Maintenance page(5149)
                );
            
            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addLocIntNo, lgEntityList, "LocationLookup", "LocIntNo", "LocDescr", this.loginUser);

            //2012-3-2 Linda Modified into a multi- language
            //2015-03-11 Heidi changed checked UK constraint(bontq1913).
            //lblError.Text = addLocIntNo <= 0 ? (string)GetLocalResourceObject("lblError.Text3") : (string)GetLocalResourceObject("lblError.Text4"); 
            if (addLocIntNo <= 0)
            {
                if (addLocIntNo == -1)
                {
                   lblError.Text =  (string)GetLocalResourceObject("lblError.Text12") 
                   + string.Format((string)GetLocalResourceObject("lblError.Text15"), txtAddLocCameraCode.Text.Trim(), ddlAddAuth_Court.SelectedItem.Text);
                }
                else
                { 
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                }
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            }
           
            lblError.Visible = true;
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            if (addLocIntNo > 0)
            {
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.conString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.LocationsMaintenance, PunchAction.Add);  

            }
           

            //Seawen 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            //dgLocationPager.RecordCount = 0;
            //dgLocationPager.CurrentPageIndex = 1;
            BindGrid(autIntNo);
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            dgLocationPager.RecordCount = 0;
            dgLocationPager.CurrentPageIndex = 1;
            BindGrid(autIntNo);

            PopulateCourt(ddlAuth_Court, autIntNo);
            PopulateLocationSuburb(ddlAuth_LocationSuburb, autIntNo);
            PopulateRoadTypes(ddlRoadType);
            PopulateSpeedZone(ddlSpeedZone);
            this.PopulateStaticLists();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtLocDescr.Text.Trim().Length > txtLocDescr.MaxLength)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2")+ txtLocDescr.MaxLength.ToString();
                lblError.Visible = true;
                return;
            }

            if (ddlAuth_LocationSuburb.SelectedValue == "0")
            {
                //2013-05-23 Heidi added
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                lblError.Visible = true;
                return;

            }
            if (ddlAuth_Court.SelectedValue == "0"
               || ddlRoadType.SelectedValue == "0"
               || ddlSpeedZone.SelectedValue == "0")
            {
                //2013-05-23 Heidi added for fixed bug
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                lblError.Visible = true;
                return;

            }

            // add the new transaction to the database table
            LocationDB locUpdate = new LocationDB(conString);

            int locIntNo = Convert.ToInt32(Session["editLocIntNo"]);

            //2015-03-11 Heidi added checked UK constraint(bontq1913).
            SIL.AARTO.DAL.Entities.Location location = locationService.GetByLocDescrLocCodeAcIntNo(txtLocDescr.Text.Trim(), txtLocCode.Text.Trim(), Convert.ToInt32(ddlAuth_Court.SelectedValue));
            if (location != null && locIntNo > 0 && location.LocIntNo != locIntNo)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12") +
                string.Format((string)GetLocalResourceObject("lblError.Text13"), txtLocDescr.Text.Trim(), txtLocCode.Text.Trim(), ddlAuth_Court.SelectedItem.Text);
                lblError.Visible = true;
                return;
            }
            SIL.AARTO.DAL.Entities.Location location2 = locationService.GetByLocDescrLocCameraCodeAcIntNo(txtLocDescr.Text.Trim(), txtLocCameraCode.Text.Trim(), Convert.ToInt32(ddlAuth_Court.SelectedValue));
            if (location2 != null && locIntNo > 0 && location2.LocIntNo != locIntNo)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12") +
                string.Format((string)GetLocalResourceObject("lblError.Text14"), txtLocDescr.Text.Trim(), txtLocCameraCode.Text.Trim(), ddlAuth_Court.SelectedItem.Text);
                lblError.Visible = true;
                return;
            }

            int updLocIntNo = locUpdate.UpdateLocation
                (locIntNo, Convert.ToInt32(ddlAuth_Court.SelectedValue),
                Convert.ToInt32(ddlSpeedZone.SelectedValue), Convert.ToInt32(ddlRoadType.SelectedValue),
                txtLocCameraCode.Text, txtLocCode.Text, txtLocDescr.Text,
                txtLocStreetCode.Text, txtLocStreetName.Text, ddlTravelDirection.SelectedItem.ToString().Substring(0, 1),
                Convert.ToInt32(txtOffenceSpeedStart.Text),
                this.comboUpdProvince.SelectedValue,
                this.textUpdCity.Text,
                this.ddlAuth_LocationSuburb.SelectedItem.Text,//this.textUpdSuburb.Text,2013-05-15 Heidi Comment out
                this.textUpdStreetA.Text,
                this.textUpdStreetB.Text,
                this.textUpdRoute.Text,
                this.textUpdFrom.Text,
                this.textUpdTo.Text,
                this.textUpdGpsX.Text,
                this.textUpdGpsY.Text,
                this.txtLocBranchCode.Text, this.loginUser,
                Convert.ToInt32(ddlAuth_LocationSuburb.SelectedValue),
                this.ddlLocType.SelectedValue,
                this.ddlLocRegion.SelectedValue,
                this.chkRailwayCrossing.Checked //2013-12-12 Heidi added for check Railway Crossing on Location Maintenance page(5149)
                );

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(updLocIntNo, lgEntityList, "LocationLookup", "LocIntNo", "LocDescr", this.loginUser);
           
            //2012-3-2 Linda Modified into a multi- language
            //2015-03-11 Heidi changed checked UK constraint(bontq1913).
            //lblError.Text = updLocIntNo <= 0 ? (string)GetLocalResourceObject("lblError.Text5") : (string)GetLocalResourceObject("lblError.Text6");
            if (updLocIntNo <= 0)
            {
                if (updLocIntNo == -1)
                {  
                    lblError.Text=(string)GetLocalResourceObject("lblError.Text12") +
                   string.Format((string)GetLocalResourceObject("lblError.Text15"), txtLocCameraCode.Text.Trim(), ddlAuth_Court.SelectedItem.Text);
                }
                else
                { 
                     lblError.Text=(string)GetLocalResourceObject("lblError.Text5");
                }
            }
            else
            {
                 lblError.Text=(string)GetLocalResourceObject("lblError.Text6");
            }
           
            
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            lblError.Visible = true;
            if (updLocIntNo > 0)
            {
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.conString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.LocationsMaintenance, PunchAction.Change);  
            }

           

            //Seawen 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            //dgLocationPager.RecordCount = 0;
            //dgLocationPager.CurrentPageIndex = 1;
            BindGrid(autIntNo);
        }

        protected void btnOptHide_Click(object sender, EventArgs e)
        {
            if (dgLocation.Visible)
            {
                //hide it
                dgLocation.Visible = false;
                //2012-3-1 Linda Modified into a multi- language
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
            }
            else
            {
                //show it
                dgLocation.Visible = true;
                //2012-3-1 Linda Modified into a multi- language
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }

            //Seawen 2013-0913 pager visible equal to grid
            dgLocationPager.Visible = dgLocation.Visible;
        }

        protected void dgLocation_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgLocation.SelectedIndex = e.Item.ItemIndex;

            if (dgLocation.SelectedIndex > -1)
            {
                int editLocIntNo = Convert.ToInt32(dgLocation.DataKeys[dgLocation.SelectedIndex]);

                Session["editLocIntNo"] = editLocIntNo;
                Session["prevPage"] = thisPageURL;

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editLocIntNo.ToString(), "LocationLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                ShowLocationDetails(editLocIntNo);
            }
        }

        protected void ShowLocationDetails(int editLocIntNo)
        {
            // Obtain and bind a list of all users
            LocationDB loc = new LocationDB(conString);

            Stalberg.TMS.LocationDetails details = loc.GetLocationDetails(editLocIntNo);

            txtLocCameraCode.Text = details.LocCameraCode;
            txtLocCode.Text = details.LocCode;
            txtLocDescr.Text = details.LocDescr;
            txtLocStreetCode.Text = details.LocStreetCode;
            txtLocStreetName.Text = details.LocStreetName;
            txtOffenceSpeedStart.Text = details.LocOffenceSpeedStart.ToString();
            this.textUpdTo.Text = details.To;
            this.textUpdCity.Text = details.City;
            this.textUpdFrom.Text = details.From;
            this.textUpdGpsX.Text = details.GpsX;
            this.textUpdGpsY.Text = details.GpsY;
            this.textUpdRoute.Text = details.Route;
            this.textUpdStreetA.Text = details.StreetA;
            this.textUpdStreetB.Text = details.StreetB;
            //this.textUpdSuburb.Text = details.Suburb;//Heidi Comment out
            this.txtLocBranchCode.Text = details.LocBranchCode;
            //2013-12-12 Heidi added for check Railway Crossing on Location Maintenance page(5149)
            this.chkRailwayCrossing.Checked = details.IsRailwayCrossing;

            //dls 090720 - this was not handling invalid data correctly
            this.comboUpdProvince.SelectedIndex = 0;
            if (!string.IsNullOrEmpty(details.Province))
            {
                try
                {
                    this.comboUpdProvince.SelectedValue = details.Province.ToUpper();
                }
                catch { }
            }

            ddlAuth_Court.SelectedIndex = ddlAuth_Court.Items.IndexOf(ddlAuth_Court.Items.FindByValue(details.ACIntNo.ToString()));
            ddlSpeedZone.SelectedIndex = ddlSpeedZone.Items.IndexOf(ddlSpeedZone.Items.FindByValue(details.SZIntNo.ToString()));
            ddlRoadType.SelectedIndex = ddlRoadType.Items.IndexOf(ddlRoadType.Items.FindByValue(details.RdTIntNo.ToString()));


            //Heidi 
            ddlAuth_LocationSuburb.SelectedIndex = ddlAuth_LocationSuburb.Items.IndexOf(ddlAuth_LocationSuburb.Items.FindByValue(details.LoSuIntNo.ToString()));

            switch (details.LocTravelDirection)
            {
                case "N":
                    ddlTravelDirection.SelectedIndex = ddlTravelDirection.Items.IndexOf(ddlTravelDirection.Items.FindByText("North"));
                    break;
                case "S":
                    ddlTravelDirection.SelectedIndex = ddlTravelDirection.Items.IndexOf(ddlTravelDirection.Items.FindByText("South"));
                    break;
                case "E":
                    ddlTravelDirection.SelectedIndex = ddlTravelDirection.Items.IndexOf(ddlTravelDirection.Items.FindByText("East"));
                    break;
                case "W":
                    ddlTravelDirection.SelectedIndex = ddlTravelDirection.Items.IndexOf(ddlTravelDirection.Items.FindByText("West"));
                    break;
            }

            ddlLocType.SelectedValue = details.LocType;
            ddlLocRegion.SelectedValue = details.LocRegion;

            pnlUpdateLocation.Visible = true;
            pnlAddLocation.Visible = false;

            btnOptDelete.Visible = true;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            dgLocationPager.RecordCount = 0;
            dgLocationPager.CurrentPageIndex = 1;
            BindGrid(autIntNo);
        }
       
        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int locIntNo = Convert.ToInt32(Session["editLocIntNo"]);

            LocationDB loc = new LocationDB(conString);

            string errMessage = string.Empty;

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "LocationLookup", "LocIntNo");

            int delLocIntNo = loc.DeleteLocation(locIntNo, ref errMessage);
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            if (delLocIntNo == 0)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7")+errMessage;
            }
            else if (delLocIntNo == -1)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8"); 
            }
            else if (delLocIntNo > 0)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.conString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.LocationsMaintenance, PunchAction.Delete);
                //Seawen 2013-09-13 last page last row
                if (dgLocation.Items.Count == 1)
                    dgLocationPager.CurrentPageIndex--;
            }

            lblError.Visible = true;

           
            //dgLocationPager.RecordCount = 0;
            //dgLocationPager.CurrentPageIndex = 1;

            BindGrid(autIntNo);
        }

        protected void dgLocationPager_PageChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo);
        }
    }
}
