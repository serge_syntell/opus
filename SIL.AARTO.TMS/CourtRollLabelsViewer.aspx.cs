using System;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.IO;
using SIL.AARTO.BLL.Utility.PrintFile;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class CourtRollLabelsViewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string sRoomNo = string.Empty;
        private string sCrtName = string.Empty;
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;

        protected string thisPageURL = "CourtRollLabelsViewer.aspx";
        protected string thisPage = "Court Roll Labels Viewer";

        #region Oscar 20120207 disabled for backup
        ///// <summary>
        ///// Handles the Load event of the Page control.
        ///// </summary>
        ///// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //protected override void OnLoad(EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();
        //    //int nCount = 0;

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    // Get user details
        //    Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
        //    Stalberg.TMS.UserDetails userDetails = new UserDetails();

        //    userDetails = (UserDetails)Session["userDetails"];
        //    autIntNo = Convert.ToInt32(Session["autIntNo"]);
        //    loginUser = userDetails.UserLoginName;

        //    General gen = new General();
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
        //    //DateRulesDetails rule = new DateRulesDetails();
        //    //rule.AutIntNo = autIntNo;
        //    //rule.DtRDescr = "Amount in days that the final court roll must be printed";
        //    //rule.DtREndDate = "FinalCourtRoll";
        //    //rule.DtRNoOfDays = -3;
        //    //rule.DtRStartDate = "CDate";
        //    //rule.LastUser = "TMS";
        //    //rule = dateRule.GetDefaultDateRule(rule);

        //    //AutIntNo, StartDate, EndDate and LastUser must be set up before the default rule is called
        //    DateRulesDetails rule = new DateRulesDetails();
        //    rule.AutIntNo = autIntNo;
        //    //2010/9/27 Paole changed rule.LastUser value according to TMS/TRUNCK  beacuse of "this.loginUser"=null
        //    //rule.LastUser = this.loginUser;
        //    rule.LastUser = "TMS";
        //    rule.DtRStartDate = "CDate";
        //    rule.DtREndDate = "FinalCourtRoll";

        //    DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
        //    int noOfDays = dateRule.SetDefaultDateRule();

        //    // Setup the report
        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);
        //    string reportPage = arn.GetAuthReportName(autIntNo, "CourtRollLabels");
        //    if (reportPage.Equals(string.Empty))
        //    {
        //        int arnIntNo = arn.AddAuthReportName(autIntNo, "CourtRollLabels.dplx", "CourtRollLabels", "system", "");
        //        reportPage = "CourtRollLabels.dplx";
        //    }

        //    string path = Server.MapPath("reports/" + reportPage);

        //    //****************************************************
        //    //SD:  20081120 - check that report actually exists
        //    string templatePath = string.Empty;
        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "CourtRollLabels");

        //    if (!File.Exists(path))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        //dls 081117 - we can only check that the template path exists if there is actually a template!
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //****************************************************

        //    DocumentLayout doc = new DocumentLayout(path);

        //    autIntNo = Request.QueryString["AutIntNo"] == null ? autIntNo : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString());
        //    DateTime dtCourtDate;
        //    if (!DateTime.TryParse(Request.QueryString["CourtDate"], out dtCourtDate))
        //    {
        //        string error = "Invalid court date selected";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);
        //        Response.Redirect(errorURL);
        //        return;
        //    }

        //    int nCrtIntNo = Request.QueryString["CrtIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["CrtIntNo"].ToString());
        //    int nCrtRIntNo = Request.QueryString["CrtRIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["CrtRIntNo"].ToString());
        //    DateTime dt;

        //    //dt = dtCourtDate.AddDays((double)rule.DtRNoOfDays);
        //    dt = dtCourtDate.AddDays(noOfDays);

        //    //jerry 2011-11-07 add
        //    int courtRollType = Convert.ToInt32(Request.QueryString["CourtRollType"].ToString());

        //    Query query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;
        //    ParameterDictionary parameters = new ParameterDictionary();
        //    parameters.Add("AutIntNo", autIntNo);
        //    if (nCrtIntNo > 0)
        //        parameters.Add("CrtIntNo", nCrtIntNo);
        //    if (nCrtRIntNo > 0)
        //        parameters.Add("CrtRIntNo", nCrtRIntNo);
        //    parameters.Add("SumCourtDate", dtCourtDate);
        //    parameters.Add("FinalPrintDate", dt);
        //    //jerry 2011-11-07 add
        //    parameters.Add("CourtRollType", courtRollType);

        //    Document report = doc.Run(parameters);
        //    byte[] buffer = report.Draw();

        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/pdf";
        //    Response.BinaryWrite(buffer);
        //    Response.End();

        //}
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();
            //int nCount = 0;

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            loginUser = userDetails.UserLoginName;

            General gen = new General();
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            PrintFileProcess process = new PrintFileProcess(this.connectionString, loginUser);
            process.BuildPrintFile(
                new PrintFileModuleCourtRoll(CourtRollType.Label, null, Request.QueryString["CrtIntNo"], Request.QueryString["CrtRIntNo"], Request.QueryString["CourtDate"], null, Request.QueryString["CourtRollType"].ToString()),
                this.autIntNo
            );

        }
    }
}
