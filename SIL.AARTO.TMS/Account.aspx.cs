using System;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;

namespace Stalberg.TMS 
{
	public partial class Account : System.Web.UI.Page 
	{
        // Fields
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //protected string thisPage = "Ledger account maintenance";
        protected string thisPageURL = "Account.aspx";
        protected string keywords = string.Empty;
		protected string title = string.Empty;
        protected string description = string.Empty;
        protected int autIntNo = 0;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnLoad(System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");
            //2013-12-02 Heidi added
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddAccount.Visible = false;
				pnlUpdateAccount.Visible = false;
				btnOptDelete.Visible = false;

				BindGrid();

			}		
		}

		protected void BindGrid ()
		{
			Stalberg.TMS.AccountDB accountList = new Stalberg.TMS.AccountDB(connectionString);

			DataSet data = accountList.GetAccountListDS();
			dgAccount.DataSource = data;
			dgAccount.DataKeyField = "AccIntNo";
           
            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgAccount.DataBind();
            }
            catch
            {
                dgAccount.CurrentPageIndex = 0;
                dgAccount.DataBind();
            }

			if (dgAccount.Items.Count == 0)
			{
				dgAccount.Visible = false;
				lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
				dgAccount.Visible = true;
			}

			data.Dispose();
			pnlAddAccount.Visible = false;
			pnlUpdateAccount.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddAccount.Visible = true;
			pnlUpdateAccount.Visible = false;
			btnOptDelete.Visible = false;

			txtAddVATStatus.Text = "Z";

		}

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindGrid();
        }

		protected void ShowAccountDetails(int editAccIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.AccountDB acc = new Stalberg.TMS.AccountDB(connectionString);
			Stalberg.TMS.AccountDetails accDetails = acc.GetAccountDetails(editAccIntNo);
            
            txtAccountNo.Text = accDetails.AccountNo;
            txtAccName.Text = accDetails.AccName;
            txtAccVATStatus.Text = accDetails.AccVATStatus;

            //StringBuilder sb = new StringBuilder();
            //sb.Append("There were some problems with the details:\n<ul>\n");
            //if (this.txtAccountNo.Text == null)
            //{
            //    sb.Append("<li>Account Number Should not be blank </li>");
            //    sb.Append("</ul>");
            //    lblError.Text = sb.ToString(); 
            //}
			pnlUpdateAccount.Visible = true;
			pnlAddAccount.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

		protected void btnAddAccount_Click(object sender, System.EventArgs e)
		{
			// add the new transaction to the database table
			//int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
			Stalberg.TMS.AccountDB accountAdd = new AccountDB(connectionString);

            int addAccIntNo = accountAdd.AddAccount(txtAddAccountNo.Text, txtAddAccName.Text, txtAddVATStatus.Text, this.loginUser);

			if (addAccIntNo <= 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.AccountsMaintenance, PunchAction.Add);  
			}

			lblError.Visible = true;

            BindGrid();

		}

		protected void btnUpdateAccount_Click(object sender, System.EventArgs e)
		{
			// add the new transaction to the database table
			Stalberg.TMS.AccountDB accUpdate = new AccountDB(connectionString);
			
			//int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
			int accIntNo = Convert.ToInt32(Session["editAccIntNo"]);

            int updAccIntNo = accUpdate.UpdateAccount(accIntNo, txtAccountNo.Text, txtAccName.Text, txtAddVATStatus.Text, loginUser);

			if (updAccIntNo <= 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.AccountsMaintenance, PunchAction.Change);  

			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void dgAccount_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgAccount.SelectedIndex = e.Item.ItemIndex;

			if (dgAccount.SelectedIndex > -1) 
			{			
				int editAccIntNo = Convert.ToInt32(dgAccount.DataKeys[dgAccount.SelectedIndex]);

				Session["editAccIntNo"] = editAccIntNo;
				Session["prevPage"] = thisPageURL;

				ShowAccountDetails(editAccIntNo);
			}

		}

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int accIntNo = Convert.ToInt32(Session["editAccIntNo"]);

            string errMessage = string.Empty;

            AccountDB account = new Stalberg.TMS.AccountDB(connectionString);

            int delAccIntNo = account.DeleteAccount(accIntNo, ref errMessage);

            lblError.Text = (string)GetLocalResourceObject("lblError.Text5");

            switch (delAccIntNo)
            {
                case 0:      
                    lblError.Text += errMessage;
                    break;
                case -1:
                    lblError.Text += (string)GetLocalResourceObject("lblError.Text6");
                    break;
                default:
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                    //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.AccountsMaintenance, PunchAction.Delete);  

                    break;
            }

            lblError.Visible = true;

            //int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            BindGrid();
        }
}
}
