using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS 
{

    public partial class Court : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo;
        //protected string thisPage = "Court maintenance";
	    protected string thisPageURL = "Court.aspx";
        protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			//int 
		    autIntNo = Convert.ToInt32(Session["autIntNo"]);
			Session["userLoginName"] = userDetails.UserLoginName.ToString();
			int userAccessLevel = userDetails.UserAccessLevel;

			loginUser = userDetails.UserLoginName;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                //2012-3-1 Linda Modified into a multi- language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddCourt.Visible = false;
				pnlUpdateCourt.Visible = false;
				pnlAuthority.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAuthority.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

				BindGrid();
			}		
		}

		protected void PopulateAuth_CourtList(int crtIntNo)
		{
			CourtDB court = new CourtDB(connectionString);

			lstAuth_Court.DataSource = court.GetAuth_CourtListByCourt(crtIntNo);
			lstAuth_Court.DataValueField = "ACIntNo";
			lstAuth_Court.DataTextField = "AutDescr";
			lstAuth_Court.DataBind();
		}

		protected void PopulateAuthorityList(int mtrIntNo)
		{
			AuthorityDB auth = new AuthorityDB(connectionString);

			SqlDataReader reader = auth.GetAuthorityList(mtrIntNo, "AutDescr");
			ddlAuthority.DataSource = reader;
			ddlAuthority.DataValueField = "AutIntNo";
			ddlAuthority.DataTextField = "AutDescr";
			ddlAuthority.DataBind();
            //2012-3-7 linda modified into a multi-language
            ddlAuthority.Items.Insert(0, (string)GetLocalResourceObject("ddlAuthority.Items1"));

			reader.Close();

		}


		protected void BindGrid()
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.CourtDB tranNoList = new Stalberg.TMS.CourtDB(connectionString);

			DataSet data = tranNoList.GetCourtListDS();
			dgCourt.DataSource = data;
			dgCourt.DataKeyField = "CrtIntNo";
			dgCourt.DataBind();

			if (dgCourt.Items.Count == 0)
			{
				dgCourt.Visible = false;
				lblError.Visible = true;
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text"); 
			}
			else
			{
				dgCourt.Visible = true;
			}

			data.Dispose();
			pnlAddCourt.Visible = false;
			pnlUpdateCourt.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddCourt.Visible = true;
			pnlUpdateCourt.Visible = false;
			pnlAuthority.Visible = false;

            txtAddCourtContempt.Text = "0";
			btnOptDelete.Visible = false;
			btnOptAuthority.Visible = false;
		}

		protected void btnAddCourt_Click(object sender, System.EventArgs e)
        {
            string pat = @"[\\/*:?<>|]";

            Regex rg = new Regex(pat);
            Match mh = rg.Match(txtAddCrtAddress.Text);
            if (mh.Success)
            {
                lblError.Text =GetLocalResourceObject("reqCrtAddress.ErrorMsg").ToString() ;
                return;
            }
            decimal contempt = 0;

            if (!decimal.TryParse(txtAddCourtContempt.Text, out contempt))
            {
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text =(string)GetLocalResourceObject("lblError.Text1"); 
                lblError.Visible = true;
                return;
            }


            //SD: 20081205 Test for null value
            StringBuilder sb = new StringBuilder();
            //2012-3-1 Linda Modified into a multi- language
            sb.Append((string)GetLocalResourceObject("lblError.Text2")+"\n<ul>\n");
            if (txtAddCourtContempt.Text.Trim().Length < 0)            
            {
                //2012-3-1 Linda Modified into a multi- language
                sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text3")+"</li>");
                sb.Append("</ul>");
                lblError.Text = sb.ToString();
                lblError.Visible = true;
            }


			// add the new transaction to the database table
			Stalberg.TMS.CourtDB crtAdd = new CourtDB(connectionString);
			
			int addCrtIntNo = crtAdd.AddCourt(txtAddCrtName.Text, txtAddFullCrtName.Text, txtAddCrtNo.Text,
                txtAddCrtAddress.Text, txtAddPayAddr1.Text, txtAddPayAddr2.Text, txtAddPayAddr3.Text, txtAddPayAddr4.Text, loginUser, txtAddCourtPrefix.Text, contempt, txtAddMagisterialCD.Text);

			if (addCrtIntNo <= 0)
			{
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4"); 
			}
			else
			{
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
            string pat = @"[\\/*:?<>|]";

            Regex rg = new Regex(pat);
            Match mh = rg.Match(txtCrtAddress.Text);
            if (mh.Success)
            {
                lblError.Text = GetLocalResourceObject("reqCrtAddress.ErrorMsg").ToString();
                return;
            }
            decimal contempt = 0;

            if (!decimal.TryParse(txtCourtContempt.Text, out contempt))
            {
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1"); 
                lblError.Visible = true;
                return;
            }

            // SD: 20081205 Test for null values

            if (txtAddCourtContempt.Text.Trim().Length < 0)
            {
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3"); 
                lblError.Visible = true;
            }

			int crtIntNo = Convert.ToInt32(Session["editCrtIntNo"]);

			// add the new transaction to the database table
			Stalberg.TMS.CourtDB crtUpdate = new CourtDB(connectionString);

			int updCrtIntNo = crtUpdate.UpdateCourt(crtIntNo, 
				txtCrtName.Text, txtFullCrtName.Text, txtCrtNo.Text, txtCrtAddress.Text,
                txtPayAddr1.Text, txtPayAddr2.Text, txtPayAddr3.Text, txtPayAddr4.Text, loginUser, txtCourtPrefix.Text, contempt, txtMagisterialCD.Text);

			if (updCrtIntNo <= 0)
			{
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6"); 
			}
			else
			{
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtMaintenance, PunchAction.Change);  
			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void dgCourt_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgCourt.SelectedIndex = e.Item.ItemIndex;

			if (dgCourt.SelectedIndex > -1) 
			{			
				int editCrtIntNo = Convert.ToInt32(dgCourt.DataKeys[dgCourt.SelectedIndex]);

				Session["editCrtIntNo"] = editCrtIntNo;
				Session["prevPage"] = thisPageURL;

				ShowCourtDetails(editCrtIntNo);
			}
		}

		protected void ShowCourtDetails(int editCrtIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.CourtDB court = new Stalberg.TMS.CourtDB(connectionString);
			
			Stalberg.TMS.CourtDetails courtDetails = court.GetCourtDetails(editCrtIntNo);

			txtCrtName.Text = courtDetails.CrtName;
            txtFullCrtName.Text = courtDetails.FullCrtName;//2013-03-05 added by Nancy for 4890
			txtCrtNo.Text = courtDetails.CrtNo;
			txtCrtAddress.Text = courtDetails.CrtAddress;
			txtPayAddr1.Text = courtDetails.CrtPaymentAddr1;
            txtPayAddr2.Text = courtDetails.CrtPaymentAddr2;
            txtPayAddr3.Text = courtDetails.CrtPaymentAddr3;
            txtPayAddr4.Text = courtDetails.CrtPaymentAddr4;
            //Jerry 2013-10-23 Take away decimals
            txtCourtContempt.Text = Convert.ToInt32(courtDetails.CrDefaultContempt).ToString();
            this.txtCourtPrefix.Text = courtDetails.CrtPrefix;
            txtMagisterialCD.Text = courtDetails.MagisterialCD;
			pnlUpdateCourt.Visible = true;
			pnlAddCourt.Visible  = false;
			pnlAuthority.Visible  = false;
			
			btnOptAuthority.Visible = true;
			btnOptDelete.Visible = true;
		}

	
		protected void dgCourt_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgCourt.CurrentPageIndex = e.NewPageIndex;

			BindGrid();
				
		}

		protected void btnOptAuthority_Click(object sender, System.EventArgs e)
		{
			pnlAddCourt.Visible = false;
			pnlUpdateCourt.Visible = false;
			pnlAuthority.Visible = true;

			//int 

			PopulateUserGroupAuthorityList();

			int crtIntNo = Convert.ToInt32(Session["editCrtIntNo"]);

			PopulateAuth_CourtList(crtIntNo);
		}

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
		protected void PopulateUserGroupAuthorityList()
		{
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            //UserGroup_AuthDB uga = new UserGroup_AuthDB(connectionString);

            //SqlDataReader reader = uga.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
			ddlAuthority.DataSource = data;
			ddlAuthority.DataValueField = "AutIntNo";
			ddlAuthority.DataTextField = "AutDescr";
			ddlAuthority.DataBind();
            //2012-3-7 linda modified into a multi-language
            ddlAuthority.Items.Insert(0, (string)GetLocalResourceObject("ddlAuthority.Items"));

			//reader.Close();
		}

		protected void btnAddAuthority_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (ddlAuthority.SelectedIndex < 1)
			{
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8"); 
				lblError.Visible = true;
				return;			
			}

			int autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);
			int crtIntNo = Convert.ToInt32(Session["editCrtIntNo"]);

			CourtDB court = new Stalberg.TMS.CourtDB(connectionString);

			int addACIntNo = court.AddAuth_Court(autIntNo, crtIntNo, loginUser);

            if (addACIntNo <= 0)
            {
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            }
            else
            {
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtMaintenance, PunchAction.Add);  
            }

			lblError.Visible = true;

			PopulateAuth_CourtList(crtIntNo);
		}

		protected void btnDelAuthority_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			if (lstAuth_Court.SelectedIndex < 0)
			{
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11"); 
				lblError.Visible = true;
				return;			
			}

			int acIntNo = Convert.ToInt32(lstAuth_Court.SelectedValue);

			CourtDB court = new Stalberg.TMS.CourtDB(connectionString);

			string delACIntNo = court.DeleteAuth_Court(acIntNo);

            //Jerry 2015-03-03 change it for bontq1884
            //if (delACIntNo.Equals("0"))
            if (delACIntNo.Equals("0") || string.IsNullOrEmpty(delACIntNo))
            {
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
            }
            else
            {
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtMaintenance, PunchAction.Delete);  
            }

			lblError.Visible = true;

			int crtIntNo = Convert.ToInt32(Session["editCrtIntNo"]);
			PopulateAuth_CourtList(crtIntNo);
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int crtIntNo = Convert.ToInt32(Session["editCrtIntNo"]);

			CourtDB court = new Stalberg.TMS.CourtDB(connectionString);

			string delCrtIntNo = court.DeleteCourt(crtIntNo);

			if (delCrtIntNo.Equals("0"))
			{
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text14"); 
			}
			else if (delCrtIntNo.Equals("-1"))
			{
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text15"); 
			}
            //Jerry 2013-10-31 add
            else if (string.IsNullOrEmpty(delCrtIntNo))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text17");
            }
            else
            {
                //2012-3-1 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text16");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtMaintenance, PunchAction.Delete);  
            }

			lblError.Visible = true;

			BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgCourt.Visible.Equals(true))
			{
				//hide it
				dgCourt.Visible = false;
                //2012-3-1 Linda Modified into a multi- language
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				//show it
				dgCourt.Visible = true;
                //2012-3-1 Linda Modified into a multi- language
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}
		 
    }
}
