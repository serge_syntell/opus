<%--<%@ Register Src="_Header1.ascx" TagName="Header" TagPrefix="hdr1" %>--%>


<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Login" Codebehind="Login.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tbody>
                <tr>
                    <td class="HomeHead" align="center" width="100%" colspan="2" height="10%" valign="middle">
                        <%--<hdr1:Header ID="Header1" runat="server"></hdr1:Header>--%>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="center" width="100%" height="85%">
                        <p>
                            &nbsp;</p>
                        <p>
                            &nbsp;</p>
                        <asp:Panel ID="pnlLogin" runat="server" HorizontalAlign="Center" BorderColor="Transparent">
                            <asp:Image ID="imgLogo" runat="server" BackColor="Transparent" ImageAlign="Middle"
                                Height="144px" Visible="False"></asp:Image>
                            <table id="Table1" cellspacing="1" cellpadding="1" border="0">
                                <tr>
                                    <td width="110" style="height: 21px">
                                        <asp:Label ID="WelcomeMsg" runat="server" CssClass="ContentHead" Width="93px" Text="<%$Resources:WelcomeMsg.Text %>"></asp:Label></td>
                                    <td width="110" style="height: 21px">
                                    </td>
                                    <td style="height: 21px">
                                        <p>
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" ForeColor=" " EnableViewState="false"></asp:Label></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="110">
                                    </td>
                                    <td width="110">
                                        <asp:Label ID="lblAuthority" runat="server" CssClass="NormalBold" Width="163px" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlSelAuthority" runat="server" Width="217px" CssClass="Normal">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td width="110">
                                    </td>
                                    <td width="110">
                                        <asp:Label ID="lblLoginName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblLoginName.Text %>"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtLoginName" runat="server" CssClass="NormalMand"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td width="110">
                                    </td>
                                    <td width="110">
                                        <asp:Label ID="lblEmail" runat="server" CssClass="NormalBold" Text="<%$Resources:lblEmail.Text %>"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="NormalMand" Width="327px"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="emailValid" runat="server" CssClass="NormalRed"
                                            ForeColor=" " ControlToValidate="txtEmail" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                                            Display="Dynamic" ErrorMessage="<%$Resources:emailValid.Text %>"></asp:RegularExpressionValidator></td>
                                </tr>
                                <tr>
                                    <td width="110">
                                    </td>
                                    <td width="110">
                                        <asp:Label ID="lblPasswd" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPasswd.Text %>"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtPasswd" runat="server" CssClass="NormalMand" TextMode="Password"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td width="110">
                                    </td>
                                    <td width="110">
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnSubmit" runat="server" Text="<%$Resources:btnSubmit.Text %>" CssClass="NormalButton" OnClick="btnSubmit_Click">
                                        </asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnClose" runat="server" Text="<%$Resources:btnClose.Text %>" CssClass="NormalButton"
                                            OnClick="btnClose_Click"></asp:Button></td>
                                </tr>
                            </table>
                            <br />
                            <asp:Label ID="lblServer" runat="server" CssClass="ContentHead" Text="<%$Resources:lblServer.Text %>" Visible="False"></asp:Label></asp:Panel>
                    </td>
                    <td valign="middle" align="center" width="100%" height="85%">
                    </td>
                </tr>
                <tr>
                    <td class="SubContentHeadSmall" valign="top" width="100%" height="5%">
                    </td>
                    <td class="SubContentHeadSmall" valign="top" width="100%" height="5%">
                    <asp:Repeater ID="rptTestImages" runat="server">
                        <ItemTemplate>
                            <asp:Image ID="imgUNCTestImage" runat="server" />
                        </ItemTemplate>                    
                    </asp:Repeater>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</body>
</html>
