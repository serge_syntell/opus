using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data.Util;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.BLL.Utility;

namespace Stalberg.TMS
{
    /// <summary>
    /// The starting point for ticket payment receipt
    /// </summary>
    public partial class CashReceiptTraffic : System.Web.UI.Page
    {
        // Constants
        private const string DATE_FORMAT = "yyyy-MM-dd";

        // Fields
        private string connectionString = String.Empty;
        private string loginUser;
        private Cashier cashier;

        protected string styleSheet;
        protected string backgroundImage;
        protected double myTotal = 0;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Cash Receipts: Traffic Department";
        protected string thisPageURL = "CashReceiptTraffic.aspx";
        protected int autIntNo = 0;
        protected int userIntNo = 0;
        protected string alwaysUsePostalReceiptReport = "N";
        protected bool mutliNoticesToPay = false;

        private bool clearPaymentList = false;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        ///
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            userIntNo = Int32.Parse(Session["userIntNo"].ToString());

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            this.pnlPostal.Visible = false;
            this.pnlDetails.Visible = false;
            this.btnPrintPostalReceipts.Visible = false;
            this.btnSuspenseReceipt.Visible = false;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (ViewState["ClearPaymentList"] != null)
                clearPaymentList = (bool)ViewState["ClearPaymentList"];

            if (ViewState["alwaysUsePostalReceiptReport"] == null)
            {
                this.alwaysUsePostalReceiptReport = GetPostalReceiptRule();
                ViewState["alwaysUsePostalReceiptReport"] = this.alwaysUsePostalReceiptReport;
            }
            else
            {
                this.alwaysUsePostalReceiptReport = ViewState["alwaysUsePostalReceiptReport"].ToString();
            }

            this.mutliNoticesToPay = false;
            // Multiple Notices
            if (Session["NoticesToPay"] == null)
            {
                this.pnlNotices.Visible = false;
                this.Session.Add("NoticesToPay", new List<NoticeForPayment>());

                this.ClearPaymentList(userIntNo);
            }
            else
            {
                List<NoticeForPayment> notices = (List<NoticeForPayment>)this.Session["NoticesToPay"];
                if (notices.Count > 0)
                {
                    this.lblNotices.Text = string.Format((string)GetLocalResourceObject("lblNotices.Text"), notices.Count);
                    this.pnlNotices.Visible = true;
                    if (notices.Count >= 1) mutliNoticesToPay = true;
                }
                else
                {
                    this.pnlNotices.Visible = false;
                    //dls 080129 - need to clear any other payments from the temporary table
                    this.ClearPaymentList(userIntNo);
                }
            }

            // FBJ Added (2006-08-15): Check to see if the current user is a valid cashier
            BankAccountDB bankAccount = new BankAccountDB(this.connectionString);
            this.cashier = bankAccount.GetUserBankAccount(int.Parse(Session["userIntNo"].ToString()));
            if (this.cashier == null)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            this.Session.Add("Cashier", this.cashier);

            //dls 100715 - please can we do things properly and call a spade a spade - trying to reuse columns when we've renamed them is not very helpful
            //this.Session.Add("BAIntNo", cashier.CashBoxUserIntNo);
            this.Session.Add("BAIntNo", cashier.BAIntNo);
            this.Session.Add("CashBoxUserIntNo", cashier.CashBoxUserIntNo);

            // FBJ Added (2006-09-26): If the cashier must count the cash box before receiving anything then redirect them
            if (cashier.MustCountCashBox || cashier.CBIntNo == -1)
                Response.Redirect("CashBoxCount.aspx");

            // FBJ Added (2007-01-09): IsPostalCapture check added for alternative, batch postal receipt printing
            // FBJ Added (2007-05-31): Changed the IsPostalCapture field in the database to an enumeration to handle roadblocks
            this.pnlPostal.Visible = (this.cashier.CashBoxType == CashboxType.PostalReceipts);

            // FBJ Added (2006-08-14): Errors from the details page are redirected here
            if (Session["CashReceiptError"] != null)
            {
                this.lblError.Text = Session["CashReceiptError"].ToString();
                Session.Remove("CashReceiptError");

                string ticketNo = "";
                if (Session["MinimalCapture_TicketNo"] != null)
                {
                    ticketNo = Session["MinimalCapture_TicketNo"].ToString();
                }

                //if (ticketNo.IndexOf("/") > 0)
                //{
                //Notice does not exists in DB
                if (Session["CacheReceiptTraffic_Details_Error"] != null && Session["CacheReceiptTraffic_Details_Error"].ToString() == "N")
                {
                    string notType = GetNoticeTypeFromTicketNo(ticketNo);
                    string validateType = string.Empty;
                    if (ticketNo.IndexOf('/') > 0)
                    {
                        validateType = "cdv";
                    }
                    else
                    {
                        validateType = "verhoeff";
                    }

                    //the value in this.textTicket.Text is blank because the page has been redirected
                    //bool InValidNotice = Validation.ValidateTicketNumber(this.textTicket.Text.Trim(), validateType);
                    bool InValidNotice = Validation.GetInstance(this.connectionString).ValidateTicketNumber(ticketNo, validateType);
                    if ((notType == "M" || notType == "H") && InValidNotice == false)
                    {
                        //get manual capture authority rule
                        string authRuleString = GetCaptureHWOOffenceRule();
                        if (authRuleString != null && authRuleString.Equals("Y"))
                        {
                            RegisterClientScript();
                            //ClientScript.RegisterClientScriptBlock(this.GetType(), "message", "$(document).ready(function(){$.blockUI({ message: $('#divNoMatchTicket')});})", true);
                            Session.Remove("CacheReceiptTraffic_Details_Error");
                        }
                        else
                        {
                            if (mutliNoticesToPay == false) this.btnSuspenseReceipt.Visible = true;

                            //ProcessSuspenseNotice();
                        }
                    }
                    else
                    {
                        if (mutliNoticesToPay == false) this.btnSuspenseReceipt.Visible = true;
                        //ProcessSuspenseNotice();
                    }
                }
                //else
                //{
                //    ProcessSuspenseNotice();
                //}
                //}
                //else if (ticketNo.IndexOf("-") > 0)
                //{
                //    ProcessSuspenseNotice();
                //}
            }

            this.pnlDetails.Visible = true;
            if (autIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                lblError.Visible = true;
            }

            if (this.cashier.CashBoxType == CashboxType.PostalReceipts)
            {
                this.btnPrintPostalReceipts.Visible = true;

                //temp remove this until we get spec for suspense payments
                this.btnSuspenseReceipt.Visible = true;
            }

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.TicketNumberSearch1.AutIntNo = this.autIntNo;
            }//
        }

        string GetNoticeTypeFromTicketNo(string ticketNo)
        {
            //string notType = string.Empty;
            if (!String.IsNullOrEmpty(ticketNo))
            {
                string notPrefix = string.Empty;
                if (ticketNo.IndexOf("/") > 0)
                {
                    notPrefix = ticketNo.Split('/')[0];
                }
                else if (ticketNo.IndexOf("-") > 0)
                {
                    notPrefix = ticketNo.Split('-')[0];
                }

                TList<NoticePrefixHistory> noticePrefixHistory = new NoticePrefixHistoryService().GetByNprefixAndAuthIntNo(notPrefix, autIntNo);
                if (noticePrefixHistory != null && noticePrefixHistory.Count > 0)
                {
                    return noticePrefixHistory[0].NphType;
                }
            }

            return string.Empty;
        }

        protected string GetPostalReceiptRule()
        {
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = this.autIntNo;
            ard.ARCode = "4510";
            ard.LastUser = this.loginUser;

            DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
            KeyValuePair<int, string> postalReceiptRule = authRule.SetDefaultAuthRule();
            return postalReceiptRule.Value;
        }

        private string GetCaptureHWOOffenceRule()
        {
            AuthorityRulesDetails arDetails9050 = new AuthorityRulesDetails();
            arDetails9050.AutIntNo = autIntNo;
            arDetails9050.ARCode = "9050";
            arDetails9050.LastUser = this.loginUser;

            DefaultAuthRules authRule9050 = new DefaultAuthRules(arDetails9050, this.connectionString);
            KeyValuePair<int, string> value9050 = authRule9050.SetDefaultAuthRule();
            return value9050.Value;
        }

        private void ClearPaymentList(int userIntNo)
        {
            if (!clearPaymentList)
            {
                CashReceiptDB cashReceipt = new CashReceiptDB(this.connectionString);
                cashReceipt.RemovePaymentListForUser(userIntNo);
                //2013-12-11 Heidi added for add all Punch Statistics Transaction(5084)
                //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
                //punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.loginUser, PunchStatisticsTranTypeList.CashReceiptsTrafficDepartment, PunchAction.Delete);  
                clearPaymentList = true;
                ViewState.Add("ClearPaymentList", clearPaymentList);


            }
            // 2012-05-18 add Jake
            //else
            //{
            Session["NoticeAddressDetails"] = null;
            Session["TRHIntNo"] = null;
            Session["TRHTranDate"] = null;
            Session["IsBankPayment"] = null;
            //}
        }

        protected void dgShowCharges_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            switch ((int)(e.Item.ItemType))
            {
                case (int)ListItemType.Item:
                    //Calculate total for the field of each row and alternating row.
                    myTotal += Convert.ToDouble(e.Item.Cells[3].Text);
                    break;
                case (int)ListItemType.AlternatingItem:
                    //Calculate total for the field of each row and alternating row.
                    myTotal += Convert.ToDouble(e.Item.Cells[3].Text);
                    break;
                case (int)ListItemType.Header:
                    break; // do nothing
                case (int)ListItemType.Footer:
                    //Use the footer to display the summary row.
                    e.Item.Cells[2].Text = "Total Fine";
                    e.Item.Cells[2].Attributes.Add("align", "left");
                    e.Item.Cells[3].Attributes.Add("align", "right");
                    e.Item.Cells[3].Text = myTotal.ToString("c");
                    break;
            }
            Session["editFineTotal"] = myTotal;
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            if (this.textId.Text.Trim().Length == 0 && this.textTicket.Text.Trim().Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            //Jake 2012-06-04 suspense notice need no validation for notice number
            //if (!String.IsNullOrEmpty(this.textTicket.Text.Trim()))
            //{
            //    //string noticeType = GetNoticeTypeFromTicketNo(this.textTicket.Text.Trim());
            //    string validateType = string.Empty;
            //    if (this.textTicket.Text.Trim().IndexOf('/') > 0)
            //    {
            //        validateType = "cdv";
            //    }
            //    else
            //    {
            //        validateType = "verhoeff";
            //    }

            //    if (Validation.ValidateTicketNumber(this.textTicket.Text.Trim(), validateType))
            //    {
            //        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            //        return;
            //    }
            //}

            PageToOpen page = new PageToOpen(this, "CashReceiptTraffic_Details.aspx");
            page.Parameters.Add(new QSParams("TicketNo", this.textTicket.Text.Trim()));
            page.Parameters.Add(new QSParams("IDNo", this.textId.Text.Trim()));

            Response.Redirect(page.ToString());
        }

        protected void btnRecon_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashReceiptTraffic_CashBoxRecon.aspx");
        }

        protected void btnRePrint_Click(object sender, EventArgs e)
        {
            // Get the ticket details from the notice/charge tables for ticket / summons
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            if (this.textTicket.Text.Trim() == string.Empty)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            //DataSet ds = db.ReceiptEnquiryOnTicketNumber(autIntNo, this.textTicket.Text.Trim());
            DataSet ds = db.ReceiptEnquiryOnTicketNumber(this.textTicket.Text.Trim());

            if (ds.Tables[0].Rows.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            else if (ds.Tables[0].Rows.Count == 1)
            {
                int rctIntNo = Convert.ToInt32(ds.Tables[0].Rows[0]["RctIntNo"].ToString());

                PageToOpen page = null;

                if (this.cashier.CashBoxType != CashboxType.PostalReceipts)
                {

                    if (this.alwaysUsePostalReceiptReport.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                    {
                        page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                    }
                    else
                    {
                        page = new PageToOpen(this, "ReceiptNoteViewer.aspx");
                    }
                }
                else
                {
                    page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                }

                if (this.alwaysUsePostalReceiptReport.Equals("Y", StringComparison.InvariantCultureIgnoreCase) || this.cashier.CashBoxType == CashboxType.PostalReceipts)
                {
                    page.Parameters.Add(new QSParams("date", DateTime.Today.ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("CBIntNo", "0"));
                    page.Parameters.Add(new QSParams("RCtIntNo", rctIntNo.ToString()));
                }
                else
                {
                    page.Parameters.Add(new QSParams("receipt", rctIntNo.ToString()));
                }

                Helper_Web.BuildPopup(page);
            }
            else
            {
                PageToOpen page = new PageToOpen(this, "ReceiptEnquiry.aspx");
                page.Parameters.Add(new QSParams("NotTicketNo", this.textTicket.Text.Trim()));
                Response.Redirect(page.ToString());
            }
        }

        protected void btnPrintPostalReceipts_Click(object sender, EventArgs e)
        {
            PageToOpen page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
            page.OverrideAjax = true;
            page.Parameters.Add(new QSParams("Date", DateTime.Today.ToString(DATE_FORMAT)));
            page.Parameters.Add(new QSParams("CBIntNo", this.cashier.CBIntNo.ToString()));

            Helper_Web.BuildPopup(page);
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.Session["NoticesToPay"] = new List<NoticeForPayment>();
            this.pnlNotices.Visible = false;

            ClearPaymentList(this.userIntNo);
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.textTicket.Text = this.TicketNumberSearch1.TicketNumber;
            //PunchStats805806 enquiry CashReceiptsTrafficDepartment

            //if (String.IsNullOrEmpty(this.TicketNumberSearch1.TicketNumber))
            //{
            //    string authRuleString = GetCaptureHWOOffenceRule();
            //    if (authRuleString != null && authRuleString.Equals("Y"))
            //    {
            //        //string ticketNo = "";
            //        //if (Session["MinimalCapture_TicketNo"] != null)
            //        //{
            //        //    ticketNo = Session["MinimalCapture_TicketNo"].ToString();
            //        //}
            //        //string js = "$(document).ready(function(){" +
            //        //            "   $.blockUI({ message: $('#divNoMatchTicket')});" +
            //        //            "   $('#btnYes').click(function () {" +
            //        //            "   $.unblockUI();" +
            //        //            "   window.location.href = '" + ResolveUrl("~/CashReceipt_MinimalCapture.aspx?TicketNo=" + ticketNo) + "';" +
            //        //            "   });" +
            //        //            "   })";

            //        //ClientScript.RegisterClientScriptBlock(this.GetType(), "message", "$(document).ready(function(){$.blockUI({ message: $('#divNoMatchTicket')});})", true);
            //        //ClientScript.RegisterClientScriptBlock(this.GetType(), "message", js, true);
            //        //OnLoad(e);
            //        RegisterClientScript();
            //    }

            //}

        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            PageToOpen page = new PageToOpen(this, "CashReceipt_MinimalCapture.aspx");
            page.OverrideAjax = true;

            Helper_Web.BuildPopup(page);
        }

        void RegisterClientScript()
        {
            string ticketNo = "";
            if (Session["MinimalCapture_TicketNo"] != null)
            {
                ticketNo = Session["MinimalCapture_TicketNo"].ToString();
            }
            string js = "$(document).ready(function(){" +
                        "   $.blockUI({ message: $('#divNoMatchTicket')});" +
                        "   $('#btnYes').click(function () {" +
                        "   $.unblockUI();" +
                        "   window.location.href = '" + ResolveUrl("~/CashReceipt_MinimalCapture.aspx?TicketNo=" + ticketNo) + "';" +
                        "   });" +
                        "   })";

            //ClientScript.RegisterClientScriptBlock(this.GetType(), "message", "$(document).ready(function(){$.blockUI({ message: $('#divNoMatchTicket')});})", true);
            ClientScript.RegisterClientScriptBlock(this.GetType(), "message", js, true);

            Session["MinimalCapture_TicketNo"] = null;
        }

        void ProcessSuspenseNotice()
        {
            //string ticketNo = "";
            //if (Session["MinimalCapture_TicketNo"] != null)
            //{
            //    ticketNo = Session["MinimalCapture_TicketNo"].ToString();
            //    Session["MinimalCapture_TicketNo"] = null;
            //}
            //PageToOpen page = new PageToOpen(this, "CashReceiptTraffic_Details.aspx");
            ////page.Parameters.Add(new QSParams("TicketNo", ticketNo));
            //page.Parameters.Add(new QSParams("Suspense", "Suspense"));

            //Response.Redirect(page.ToString());

            this.btnSuspenseReceipt.Visible = true;
        }

        protected void linkToNoTrafficChargePymt_Click(object sender, EventArgs e)
        {
            string aartoDomain = WebUtility.GetAartoDomainName();
            Response.Redirect(aartoDomain + "/NTCReceipt/Pay");
        }

    }
}
