using System;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.IO;

namespace Stalberg.TMS
{
    public partial class ContempReport_Viewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString;
        protected string styleSheet = string.Empty;
        protected string backgroundImage;
        protected string loginUser;
        protected string thisPageURL = "ContemptReport_Viewer.aspx";
        //protected string thisPage = "Contempt Report Viewer";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        private const string DATE_FORMAT = "yyyy-MM-dd";

        private int autIntNo = 0;
        private int crtIntNo = 0;
        private int InMonth = 0;
        private int InYear = 0;
        private string DateFrom;
        private string DateTo;
        private string login = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            this.connectionString = Application["constr"].ToString();

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.crtIntNo = int.Parse(Request.QueryString["CrtIntNo"]);
            //this.autIntNo = int.Parse(Request.QueryString["AutIntNo"]);
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int viewAutIntNo = (Request.QueryString["AutIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString()));

            if (Request.QueryString["Year"] == null && Request.QueryString["From"] != null)
            {
                this.DateFrom = Request.QueryString["From"].ToString();
                this.DateTo = Request.QueryString["To"].ToString();
            }
            else
            {
                this.InYear = int.Parse(Request.QueryString["Year"] == string.Empty ? "9999" : Request.QueryString["Year"].ToString());
                this.InMonth = int.Parse(Request.QueryString["Month"].ToString());
            }

            this.title = (string)GetLocalResourceObject("title");

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "ContemptAmounts");

            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "ContemptAmounts.dplx", "ContemptAmounts", "system", "");
                reportPage = "ContemptAmounts.dplx";
            }

            string path = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "ContemptAmounts");

            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                //dls 081117 - we can only check that the template path exists if there is actually a template!
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            DocumentLayout doc = new DocumentLayout(path);
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("AutIntNo", viewAutIntNo);
            parameters.Add("CrtIntNo", this.crtIntNo);
            parameters.Add("InYear", this.InYear);
            parameters.Add("InMonth", this.InMonth);
            parameters.Add("DateFrom", DateTime.Parse(this.DateFrom == null ? "1990/01/01" : this.DateFrom));
            parameters.Add("DateTo", DateTime.Parse(this.DateTo == null ? "1990/01/01" : this.DateTo));
            parameters.Add("LastUser", this.login);

            Document report = doc.Run(parameters);
            byte[] buffer = report.Draw();

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(buffer);
            Response.End();
        }

    }
}