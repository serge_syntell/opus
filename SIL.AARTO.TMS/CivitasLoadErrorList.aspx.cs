using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class CivitasLoadErrorList : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string title = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        //protected string thisPage = "A.G. List";
        protected string description = String.Empty;
        protected string thisPageURL = "AGList.aspx";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 

            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text1");
                this.dtpStartDate.Text = DateTime.Today.ToString(DATE_FORMAT);
                this.dtpEndDate.Text  = DateTime.Today.AddDays(1).ToString(DATE_FORMAT);
            }
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnShow_Click(object sender, EventArgs e)
        {
            DateTime dtStart;
            if (!DateTime.TryParse(this.dtpStartDate.Text, out dtStart))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            DateTime dtEnd;
            if (!DateTime.TryParse(this.dtpEndDate.Text, out dtEnd))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            if (dtEnd.Ticks < dtStart.Ticks)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            PageToOpen page = new PageToOpen(this, "CivitasLoadErrorListViewer.aspx");
            page.Parameters.Add(new QSParams("Start", dtStart.ToString("yyyy-M-dd")));
            page.Parameters.Add(new QSParams("End", dtEnd.ToString("yyyy-M-dd")));

            Helper_Web.BuildPopup(page);
        }

        protected void  dtpStartDate_OnTextChanged(object sender, EventArgs e)
        {
            DateTime dtStart;
            if (!DateTime.TryParse(this.dtpStartDate.Text, out dtStart))
                return;

            DateTime dtEnd;
            if (!DateTime.TryParse(this.dtpEndDate.Text, out dtEnd))
            {
                this.dtpEndDate.Text = dtStart.AddDays(1).ToString(DATE_FORMAT);
                return;
            }

            if (dtEnd.Ticks < dtStart.Ticks)
                this.dtpEndDate.Text = dtStart.AddDays(1).ToString(DATE_FORMAT);
        }
}
}
