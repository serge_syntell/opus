<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.ChangePasswd" Codebehind="ChangePasswd.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet"/>
    <meta content="<%= description %>" name="Description"/>
    <meta content="<%= keywords %>" name="Keywords"/>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                   
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" Text="<%$Resources:lblPageName.Text %>" runat="server" Width="379px" CssClass="ContentHead"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:TextBox ID="txtError" runat="server" Width="497px" CssClass="NormalRed" Height="47px"
                                    ForeColor=" " TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
                                <p>
                                </p>
                                <asp:Panel ID="pnlUserDetails" runat="server">
                                    <table id="Table2" cellspacing="1" cellpadding="1" width="300" border="0">
                                        <tr>
                                            <td class="ContentTitle">
                                                <asp:Label ID="lblUserDet" runat="server" Width="158px" CssClass="ProductListHead"
                                                    Height="14px" Text="<%$Resources:lblUserDet.Text %>"></asp:Label></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Content" align="left" width="142" height="17">
                                                <asp:Label ID="lblShowUserName" runat="server" Width="136px" CssClass="NormalBold"
                                                    Height="20px"  Text="<%$Resources:lblShowUserName.Text %>"></asp:Label></td>
                                            <td class="Content">
                                                <asp:Label ID="lblUserName" runat="server" Width="189px" CssClass="Normal" Height="20px"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="Content" align="left">
                                                <asp:Label ID="lblShowUserLogin" runat="server" Width="133px" CssClass="NormalBold"
                                                    Height="17px" Text="<%$Resources:lblShowUserLogin.Text %>"></asp:Label></td>
                                            <td class="Content">
                                                <asp:Label ID="lblUserLogin" runat="server" Width="294px" CssClass="Normal" Height="20px"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="Content" align="left">
                                                <asp:Label ID="lblShowUserEmail" runat="server" Width="133px" CssClass="NormalBold"
                                                    Height="17px" Text="<%$Resources:lblShowUserEmail.Text %>"></asp:Label></td>
                                            <td class="Content">
                                                <asp:Label ID="lblUserEmail" runat="server" Width="294px" CssClass="Normal" Height="20px"></asp:Label></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlPassword" runat="server">
                                    <table id="Table3" height="43" cellspacing="1" cellpadding="1" width="462" border="0">
                                           <tr>
                                            <td class="Content" align="left" width="183">
                                                <asp:Label ID="lblOldPassword" runat="server" Width="140px" CssClass="ProductListHead"
                                                    Height="17px" Text="Enter Old Password"></asp:Label></td>
                                            <td class="Content">
                                                <input id="Hidden3" style="width: 48px; height: 17px" type="hidden" size="2" value="0"
                                                    name="txtPasswd" runat="server"><input id="Hidden4" style="width: 48px; height: 17px"
                                                        type="hidden" size="2" value="0" name="hidSecIntNo" runat="server">&nbsp;&nbsp;&nbsp;
                                                <asp:TextBox ID="txtOldPassWord" runat="server" Height="17px" TextMode="Password" Width="111px"></asp:TextBox>
                                               </td>
                                            <td class="Content">
                                                <%--<asp:Button ID="btnCancel" runat="server" Width="89px" CssClass="NormalButton" Text="Cancel"
                                                    Height="22px" OnClick="btnCancel_Click"></asp:Button>--%></td>
                                        </tr>
                                        <tr>
                                            <td class="Content" align="left" width="183">
                                                <asp:Label ID="lblChangePasswd" runat="server" Width="140px" CssClass="ProductListHead"
                                                    Height="17px" Text="Enter new password"></asp:Label></td>
                                            <td class="Content">
                                                <input id="txtPasswd" style="width: 48px; height: 17px" type="hidden" size="2" value="0"
                                                    name="txtPasswd" runat="server"><input id="hidSecIntNo" style="width: 48px; height: 17px"
                                                        type="hidden" size="2" value="0" name="hidSecIntNo" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:TextBox ID="textNewPassWord" runat="server" Height="17px" TextMode="Password" Width="111px"></asp:TextBox>
                                                        &nbsp;&nbsp;&nbsp;</td>
                                            <td class="Content">
                                                <%--<asp:Button ID="btnCancel" runat="server" Width="89px" CssClass="NormalButton" Text="Cancel"
                                                    Height="22px" OnClick="btnCancel_Click"></asp:Button>--%></td>
                                        </tr>
                                         
                                        <tr>
                                            <td class="ContentTitle" width="183">
                                                <asp:Label ID="lblUserPasswd" runat="server" Width="140px" CssClass="NormalBold"
                                                    Height="17px" Text="<%$Resources:lblUserPasswd.Text %>"></asp:Label></td>
                                            <td class="ContentTitle">
                                                <p>
                                                    <asp:TextBox ID="txtAddUserPasswd" runat="server" Width="111px" CssClass="NormalMand"
                                                        Height="17px" TextMode="Password" MaxLength="20" OnTextChanged="txtAddUserPasswd_TextChanged"></asp:TextBox></p>
                                            </td>
                                            <td class="ContentTitle">
                                                <asp:Button ID="btnUpdPasswd" runat="server" Width="156px" CssClass="NormalButton"
                                                    Text="<%$Resources:btnUpdPasswd.Text %>" Height="23px" OnClick="btnUpdPasswd_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
