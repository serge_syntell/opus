using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;


namespace Stalberg.TMS
{
    /// <summary>
    /// Displays a list of Camera Unit so that enquiry can be made on Contraventin Batches
    /// </summary>
    public partial class CameraUnitBatch_Enquiry : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private DataSet ds = null;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "CameraUnitBatch_Enquiry.aspx";
        //protected string thisPage = "Camera Unit Batch Enquiry";

        // Constants
        //private const string ALL_CAMERAS = "[ All Cameras ]";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateAuthorities();
                this.PopulateCameraUnits(this.autIntNo);
                this.PopulateContraventionFiles();
            }
        }

        private void PopulateCameraUnits(int autIntNo)
        {
            CameraUnitDB db = new CameraUnitDB(this.connectionString);
            this.ddlCameraUnit.DataSource = db.GetCameraUnitList(autIntNo);
            this.ddlCameraUnit.DataValueField = "CamUnitID";
            this.ddlCameraUnit.DataTextField = "CamUnitID";
            this.ddlCameraUnit.DataBind();

            this.ddlCameraUnit.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ALL_CAMERAS"), string.Empty));
            this.ddlCameraUnit.SelectedIndex = 0;
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB db = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = db.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            this.ddlAuthority.SelectedIndex = this.ddlAuthority.Items.IndexOf(this.ddlAuthority.Items.FindByValue(this.autIntNo.ToString()));
            //reader.Close();
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            int autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            this.PopulateCameraUnits(autIntNo);
            //2013-03-28 Henry add for pagination
            grdContraventionFilesPager.RecordCount = 0;
            grdContraventionFilesPager.CurrentPageIndex = 1;
            this.PopulateContraventionFiles();
        }

        protected void chkOutstanding_CheckedChanged(object sender, EventArgs e)
        {
            //2013-03-28 Henry add for pagination
            grdContraventionFilesPager.CurrentPageIndex = 1;
            grdContraventionFilesPager.RecordCount = 0;
            this.PopulateContraventionFiles();
        }

        private void PopulateContraventionFiles()
        {
            int autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            string cameraUnit = this.ddlCameraUnit.SelectedValue;
            bool onlyOutstandingFiles = this.chkOutstanding.Checked;

            CameraUnitBatchDB db = new CameraUnitBatchDB(this.connectionString);
            int totalCount = 0; //2013-03-28 Henry add for pagination
            this.ds = db.GetContraventionFileList(autIntNo, cameraUnit, onlyOutstandingFiles, grdContraventionFilesPager.PageSize, grdContraventionFilesPager.CurrentPageIndex, out totalCount);
            grdContraventionFilesPager.RecordCount = totalCount;

            if (this.ds.Tables[0].Rows.Count == 0)
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            else
                this.lblError.Text = string.Empty;
            
            this.grdContraventionFiles.DataSource = this.ds;
            this.grdContraventionFiles.DataBind();
        }

        protected void ddlCameraUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            //2013-03-28 Henry add for pagination
            grdContraventionFilesPager.RecordCount = 0;
            grdContraventionFilesPager.CurrentPageIndex = 1;
            this.PopulateContraventionFiles();
        }

        //2013-03-28 Henry add for pagination
        protected void grdContraventionFilesPager_PageChanged(object sender, EventArgs e)
        {
            PopulateContraventionFiles();
        }

    }
}
