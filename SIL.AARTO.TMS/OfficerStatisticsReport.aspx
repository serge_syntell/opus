<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.OfficerStatisticsReport" Codebehind="OfficerStatisticsReport.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
 </head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
 
    </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptSearch" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptSearch.Text %>"  OnClick="btnOptSearch_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                        </p>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;
                        </p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                    <asp:Panel ID="pnlSearch" runat="Server" Width="100%">
                        <table>
                            <tr>
                                <td style="width: 157; height: 2">
                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAuthority.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" AutoPostBack="True"
                                        Width="197px" OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 157; height: 2">
                                    <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOfficer.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:DropDownList ID="ddlOfficer" runat="server" CssClass="Normal" Width="197px">
                                    </asp:DropDownList>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 157; height: 2">
                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateFrom.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                  
                                    <asp:TextBox runat="server" ID="dtpFrom" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpFrom" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="dtpFrom" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:RegDataFrom.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                            ControlToValidate="dtpFrom" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:RegDataFrom.ErrorMessage %>"></asp:RequiredFieldValidator>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 157; height: 2">
                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateTo.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                  
                                    <asp:TextBox runat="server" ID="dtpTo" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                TargetControlID="dtpTo" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                            ControlToValidate="dtpTo" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:RegDataFrom.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                            ControlToValidate="dtpTo" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:RegDataFrom.ErrorMessage %>"></asp:RequiredFieldValidator>
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 157; height: 2">
                                    <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblReportType.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:DropDownList ID="ddlReportType" runat="server" Width="197px" AutoPostBack="False" />
                                </td>
                                <td valign="top" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td width="10" height="2">
                                    &nbsp;</td>
                                <td width="157" height="2">
                                    <asp:Button ID="btnReport" runat="server" CssClass="NormalButton" Text="<%$Resources:btnReport.Text %>" OnClick="btnReport_Click">
                                    </asp:Button></td>
                                <td width="20" height="2">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel></ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
