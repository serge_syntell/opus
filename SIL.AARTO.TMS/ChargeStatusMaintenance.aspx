﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.ChargeStatusMaintenance" Codebehind="ChargeStatusMaintenance.aspx.cs" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
               <asp:UpdatePanel ID="udp" runat="server">
                        <ContentTemplate>
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47" style="width: 654px">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 600px">
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <asp:DataGrid ID="dgCS" Width="590px" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                        AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                        FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                        Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgCS_ItemCommand"
                                        OnPageIndexChanged="dgCS_PageIndexChanged">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn DataField="CSCode" HeaderText="<%$Resources:dgCS.CSCode.HeaderText %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="CSDescr" HeaderText="<%$Resources:dgCS.CSDescr.HeaderText %>" HeaderStyle-Width="400px">
                                                <HeaderStyle Width="400px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="StatisticsFlag" HeaderText="<%$Resources:dgCS.StatisticsFlag.HeaderText %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="CSRoadblockOption" HeaderText="<%$Resources:dgCS.CSRoadblockOption.HeaderText %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="CSRoadblockIdentifier" HeaderText="<%$Resources:dgCS.CSRoadblockIdentifier.HeaderText %>"></asp:BoundColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgCS.Command.Text %>" CommandName="Select"></asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                    </asp:DataGrid>
                                </asp:Panel>
                                <br />
                                <asp:Panel ID="pnlUpdateCS" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td height="2" style="width: 221px">
                                                <asp:Label ID="lblUpdateTitle" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblUpdateTitle.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" height="25" style="width: 221px">
                                                <asp:Label ID="lblCSCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCSCode.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:Label ID="lblCSCodeData" runat="server" CssClass="NormalBold"></asp:Label>
                                            </td>
                                            <td height="25">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 25px; width: 221px;">
                                                <p>
                                                    <asp:Label ID="lblCSRoadblockOption" runat="server" Width="94px" 
                                                        CssClass="NormalBold" Text="<%$Resources:lblCSRoadblockOption.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" style="height: 25px">
                                                <asp:DropDownList ID="ddlCSRoadblockOption" runat="server" Height="16px" 
                                                    Width="100px">
                                                    <asp:ListItem Text="<%$Resources:ddlCSRoadblockOptionItem.Text %>"></asp:ListItem>
                                                    <asp:ListItem Text="<%$Resources:ddlCSRoadblockOptionItem.Text1 %>"></asp:ListItem>
                                                    <asp:ListItem Text="<%$Resources:ddlCSRoadblockOptionItem.Text2 %>"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="height: 25px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" style="width: 221px">
                                                <p>
                                                    <asp:Label ID="lblCSRoadblockIdentifier" runat="server" CssClass="NormalBold" 
                                                        Width="94px" Text="<%$Resources:lblCSRoadblockIdentifier.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:DropDownList ID="ddlCSRoadblockIdentifier" runat="server" Height="16px" 
                                                    Width="190px">
                                                    <asp:ListItem Value="-1" Text="<%$Resources:ddlCSRoadblockIdentifierItem.Text %>"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="<%$Resources:ddlCSRoadblockIdentifier.Text1 %>"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="<%$Resources:ddlCSRoadblockIdentifier.Text2 %>"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="<%$Resources:ddlCSRoadblockIdentifier.Text3 %>"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="<%$Resources:ddlCSRoadblockIdentifier.Text4 %>"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="<%$Resources:ddlCSRoadblockIdentifier.Text5 %>"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="<%$Resources:ddlCSRoadblockIdentifier.Text6 %>"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td height="25">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                        <tr bgcolor='#FFFFFF'>
                                                            <td height="100">
                                                                <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                            <td height="100">
                                                                <uc1:uclanguagelookup id="ucLanguageLookupUpdate" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        <tr>
                                            <td style="width: 221px">
                                            </td>
                                            <td width="248">
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" 
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired(); " Text="<%$Resources:btnUpdate.Text %>" />
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                   </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label1"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
