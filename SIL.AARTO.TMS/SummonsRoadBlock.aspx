﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.SummonsRoadBlock" CodeBehind="SummonsRoadBlock.aspx.cs" %>

<%--<%@ Register Src="RepresentationOnTheFly.ascx" TagName="RepresentationOnTheFly" TagPrefix="uc2" %>--%>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%= background %>" topmargin="0"
    rightmargin="0">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" width="95%" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center" style="width: 90px">
                    <img style="height: 1px" src="images/1x1.gif" width="90">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
<%--                    <asp:Panel ID="pnlSubMenu" runat="server" Width="100px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label5" runat="server" Width="98px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px"></td>
                            </tr>
                        </table>
                    </asp:Panel>--%>
                </td>
                <td valign="top">
                    <asp:UpdatePanel ID="upd" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                                <p style="text-align: center;">
                                    <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Height="40px">Road 
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblBlockSession.Text %>"></asp:Label></asp:Label>
                                </p>
                            </asp:Panel>
                            <asp:Panel ID="pnlNavigation" runat="server">
                                <asp:LinkButton ID="btnRBSessionList" runat="server" Text="<%$Resources:btnRBSessionList.Text %>"
                                    OnClick="btnRBSessionList_Click"></asp:LinkButton>
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="pnlErrMessage" runat="server">
                                <asp:Label ID="lblMessage" runat="server" Font-Bold="True" CssClass="NormalRed"></asp:Label>
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="pnlSessionIDFilter" runat="server">
                                <table cellpadding="0" cellspacing="0" width="80%">
                                    <tr style="height: 29px">
                                        <td colspan="4" style="height: 29px">
                                            <asp:Label ID="lblTitle" runat="server" CssClass="SubContentHead" Text="<%$Resources:lblTitle.Text %>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblUser" runat="server" CssClass="NormalBold"
                                                Text="<%$Resources:lblUser.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblUserText" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSessionStart" CssClass="NormalBold" runat="server"
                                                Text="<%$Resources:lblSessionStart.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSessionStartText" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblLAopen" runat="server" CssClass="NormalBold"
                                                Text="<%$Resources:lblLAopen.Text %>"></asp:Label>
                                        </td>
                                        <td valign="middle">
                                            <asp:Label ID="lblLAText" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblLocation" runat="server" CssClass="NormalBold"
                                                Text="<%$Resources:lblLocation.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblLocationText" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblClerkofCourt" runat="server" CssClass="NormalBold"
                                                Text="<%$Resources:lblClerkofCourt.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblClerkofCourtText" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGPRS" runat="server" CssClass="NormalBold"
                                                Text="<%$Resources:lblGPRS.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGPRSText" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                    </tr>

                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td class="SubContentHead">
                                            <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblIdentitySearch.Text %>"></asp:Label></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblIdentityNumber" Text="<%$Resources:lblIdentityNumber.Text %>" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtIdentityNumber" runat="server" CssClass="Normal" Width="300px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label10" Text="<%$Resources:ORVehicleRegNo %>" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQueryRegNo" runat="server" CssClass="Normal" Width="300px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label11" Text="<%$Resources:ORSurname %>" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuerySurname" runat="server" CssClass="Normal" Width="119px" />&nbsp;&nbsp;
                                        <asp:Label ID="Label12" Text="<%$Resources:Initials %>" runat="server" CssClass="Normal" />
                                            <asp:TextBox ID="txtQueryInitials" runat="server" CssClass="Normal" Width="119px" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>" Width="150px"
                                                OnClick="btnSearch_Click" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label13" Text="<%$Resources:lblNotIntNo.Text %>" runat="server" CssClass="Normal"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNotTicketNo" runat="server" CssClass="Normal" Width="300px" />&nbsp;&nbsp;
                                       
                                        </td>
                                        <td>
                                            <asp:Button ID="btnRePrint" runat="server" CssClass="NormalButton" Text="<%$Resources:btnRePrint.Text %>" Width="150px"
                                                OnClick="btnRePrint_Click" />
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" runat="server" Width="100%">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlIdNumberInfo" runat="server" Width="100%">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblDriverOrProxyInfo" runat="server" CssClass="SubContentHead"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="grdIdNumberInfo" runat="server" AllowPaging="False"
                                                AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" ShowFooter="True" HeaderStyle-HorizontalAlign="Left" OnDataBound="grdIdNumberInfo_DataBound">
                                                <FooterStyle CssClass="CartListHead" />
                                                <Columns>
                                                    <asp:BoundField DataField="IDNumber" HeaderText="<%$Resources:grdIdNumberInfo.HeaderText1 %>" />
                                                    <asp:BoundField DataField="Surname" HeaderText="<%$Resources:grdIdNumberInfo.HeaderText2 %>" />
                                                    <asp:BoundField DataField="Initials" HeaderText="<%$Resources:grdIdNumberInfo.HeaderText3 %>" />
                                                    <asp:BoundField DataField="DriverOrProxy" HeaderText="<%$Resources:grdIdNumberInfo.HeaderText4 %>" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:RadioButton ID="rdoNumberSelect" runat="server" GroupName="rdoNumberSelectGroup"  AutoPostBack="True" 
                                                                OnCheckedChanged="rdoNumberSelect_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="CartListHead" />
                                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel1" runat="server" Width="100%">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlSumonsNoticeList" runat="server">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblResultTitle" runat="server" CssClass="SubContentHead"></asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Text="<%$Resources:SelectAll %>"></asp:Label><asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" Checked="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblNoAOGRepMessage" runat="server" Font-Bold="True" CssClass="NormalRed"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="height: 5px; line-height: 5px;">
                                        <td style="height: 5px; line-height: 5px;" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:GridView ID="grdSummonsNoticesList" runat="server" AllowPaging="False"
                                                AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" ShowFooter="True" HeaderStyle-HorizontalAlign="Left" OnSelectedIndexChanged="grdSummonsNoticesList_SelectedIndexChanged">
                                                <%--OnPageIndexChanging="grdSummonsNoticesList_PageIndexChanging"--%>
                                                <SelectedRowStyle BackColor="Yellow" Font-Bold="True" ForeColor="#000000" />
                                                <FooterStyle CssClass="CartListHead" />
                                                <Columns>
                                                    <%--<asp:BoundField DataField="ChgIntNo" HeaderText="ChgIntNo" Visible="False" />--%>
                                                    <%--<asp:BoundField DataField="RevisedAmount" Visible="False" />--%>
                                                    <%--<asp:BoundField DataField="OriginalAmount" Visible="False" />--%>
                                                    <%--   <asp:BoundField DataField="LocalAuthority" HeaderText="Authority" />--%>
                                                    <asp:BoundField DataField="TicketNo" HeaderText="<%$Resources:grdSummonsNoticesList.HeaderText5 %>" />
                                                    <asp:BoundField DataField="NotIntNo" Visible="False" />
                                                    <asp:BoundField DataField="SumIntNo" HeaderText="SumIntNo" Visible="False" />
                                                    <asp:BoundField DataField="SumNotFlag" HeaderText="SumNotFlag" Visible="False" />
                                                    <asp:BoundField DataField="OffenceDate" HeaderText="<%$Resources:grdSummonsNoticesList.HeaderText %>" HtmlEncode="False" DataFormatString="{0:yyyy-MM-dd HH:mm}" />
                                                    <asp:BoundField DataField="LocalAuthority" HeaderText="<%$Resources:grdSummonsNoticesList.HeaderText1 %>" />
                                                    <asp:BoundField DataField="VehicleRegNo" HeaderText="<%$Resources:grdSummonsNoticesList.HeaderText2 %>" />
                                                    <asp:BoundField DataField="VehicleMakeType" HeaderText="<%$Resources:grdSummonsNoticesList.HeaderText3 %>" />
                                                    <asp:BoundField DataField="Status" HeaderText="Status" Visible="false" />
                                                    <asp:BoundField DataField="StatusDescr" HeaderText="<%$Resources:grdSummonsNoticesList.HeaderText4 %>" />
                                                    <asp:BoundField DataField="TicketNo" HeaderText="Ticket No" Visible="false" />
                                                    <asp:BoundField DataField="AreaCode" Visible="False" />
                                                    <asp:BoundField DataField="NoticeRowVersion" Visible="False" />
                                                    <asp:BoundField DataField="OffenceDate" Visible="False" />
                                                    <asp:BoundField DataField="SummonsNo" Visible="False" />
                                                    <asp:BoundField DataField="AreaCode" Visible="False" />
                                                    <asp:CommandField HeaderText="Select" ShowSelectButton="True" Visible="false" />
                                                    <asp:BoundField DataField="AutIntNo" HeaderText="AutIntNo" Visible="False" />
                                                    <asp:BoundField DataField="NotFilmType" HeaderText="AutIntNo" Visible="False" />
                                                    <asp:BoundField DataField="RevisedAmount" HeaderText="<%$Resources:grdSummonsNoticesList.HeaderText6 %>" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="OriginalAmount" Visible="False" />
                                                    <asp:BoundField DataField="Surname" HeaderText="<%$Resources:grdIdNumberInfo.HeaderText2 %>" />
                                                    <asp:BoundField DataField="Initials" HeaderText="<%$Resources:grdIdNumberInfo.HeaderText3 %>" />
                                                    <asp:BoundField DataField="IDNumber" HeaderText="<%$Resources:grdIdNumberInfo.HeaderText1 %>" />
                                                    <asp:BoundField DataField="NotSendTo" Visible="false" />
                                                    <asp:BoundField DataField="S35OrNoAOG" HeaderText="<%$Resources:grdSummonsNoticesList.HeaderText7 %>" />
                                                    <asp:CommandField HeaderText="Offender Details Source" ShowSelectButton="True">
                                                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                                    </asp:CommandField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <%--<asp:CheckBox ID="chkBoxSelect" runat="server" Checked="True" AutoPostBack="true" OnCheckedChanged="chkBoxSelect_CheckedChanged" />--%>
                                                            <asp:CheckBox ID="chkBoxSelect" runat="server" Checked="True" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <asp:CheckBox ID="chkBoxAut" runat="server" />
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="CartListHead" />
                                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                                            </asp:GridView>
                                            <pager:AspNetPager ID="dgSummonsNoticesListPager" runat="server"
                                                ShowCustomInfoSection="Right" Width="695px"
                                                CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                                FirstPageText="|&amp;lt;"
                                                LastPageText="&amp;gt;|"
                                                CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                                Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                                CustomInfoStyle="float:right;"
                                                OnPageChanged="dgSummonsNoticesListPager_PageChanged" PageSize="10" UpdatePanelId="upd">
                                            </pager:AspNetPager>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlShowButton" runat="server" Width="100%">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>

                            <asp:Panel ID="pnlOffenceDetail" runat="server">
                                <table width="100%">
                                    
                                    <tr style="width: 150px">
                                        <td>
                                            <asp:Label ID="lblSurName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSurName.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSurName" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>

                                        <td>&nbsp;&nbsp;
                                        <asp:Label ID="lblLA" runat="server" CssClass="NormalBold" Text="<%$Resources:lblLA.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLandLine" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td rowspan="2">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblInitials" runat="server" CssClass="NormalBold" Text="<%$Resources:lblInitials.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtInitials" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td>&nbsp;&nbsp;
                                        <asp:Label ID="lblCellPhone" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCellPhone.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMobileNo" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                        </td>
                                        <td class="style1">
                                            <asp:TextBox ID="txtFullName" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPostalAddress1" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPostalAddress1.Text %>"></asp:Label>
                                        </td>
                                        <td class="style1">
                                            <asp:TextBox ID="txtPostalAddress1" CssClass="Normal" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td>&nbsp;&nbsp;
                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblStreetAddress.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtStreetAddress1" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td class="style1">
                                            <asp:TextBox ID="txtPostalAddress2" CssClass="Normal" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td>
                                            <asp:TextBox ID="txtStreetAddress2" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td class="style1">
                                            <asp:TextBox ID="txtPostalAddress3" CssClass="Normal" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td>
                                            <asp:TextBox ID="txtStreetAddress3" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                        <td class="style1">
                                            <asp:TextBox ID="txtPostalAddress4" CssClass="Normal" runat="server" Width="150px"></asp:TextBox>
                                        </td>
                                        <td>&nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtStreetAddress4" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPostalAreaCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPostalAreaCode.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPostalAreaCode" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td>&nbsp;&nbsp;
                                        <asp:Label ID="lblStrAreaCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblStrAreaCode.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtStreetAreaCode" runat="server" CssClass="Normal" Width="150px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnUpdateOffenceder" runat="server" CssClass="NormalButton" OnClientClick="return PhoneCheck(); " OnClick="btnUpdateOffenceder_Click"
                                                Text="<%$Resources:btnUpdateOffenceder.Text %>" Width="150px" />
                                            <asp:HiddenField ID="hidIDType" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!--Ask user to decide how to handle no aog's-->
                            <%--<asp:Panel ID="pnlNOAOGDecision" runat="server" Height="50px" Width="100%" HorizontalAlign="left">
                                <asp:Label ID="lblNOAOGDecision" runat="server" Height="24px" Text="<%$Resources:lblNOAOGDecision.Text %>"
                                    Width="600px" CssClass="SubContentHead"></asp:Label>
                                <br />
                                <br />
                                &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;
                            <asp:Button ID="NOAOGDecisionYes" runat="server" Text="<%$Resources:NOAOGDecisionYes.Text %>" CssClass="NormalButton"
                                Width="76px" OnClick="NOAOGDecisionYes_Click" />
                                &nbsp; &nbsp; &nbsp;
                            <asp:Button ID="NOAOGDecisionNo" runat="server"
                                Text="<%$Resources:NOAOGDecisionNo.Text %>" Width="76px" CssClass="NormalButton"
                                Height="24px" OnClick="NOAOGDecisionNo_Click" />
                                <br />

                            </asp:Panel>--%>

                            <%--<asp:Panel ID="pnlRepresentation" runat="server" Width="100%">
                                <asp:Label ID="Label1" runat="server" Width="100%" CssClass="SubContentHead" Text="<%$Resources:lblRepresentation.Text %>"></asp:Label>
                                <br />
                                <table style="border-spacing: none;" class="Normal">
                                    <tr>
                                        <td style="width: 122px" class="NormalBold">
                                            <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblFineAmount.Text %>"></asp:Label>
                                        </td>
                                        <td style="width: 224px">
                                            <asp:TextBox ID="txtNewAmount" runat="server" AutoPostBack="True" OnTextChanged="txtNewAmount_TextChanged"
                                                Width="112px" />
                                        </td>
                                    </tr>
                                </table>
                                <uc2:RepresentationOnTheFly ID="RepresentationOnTheFly1" runat="server" />
                            </asp:Panel>--%>
                            <!-- Summons Server Selection Panel -->
                            <asp:Panel ID="pnlSummonsServer" runat="server" Width="100%">
                                <asp:Label ID="lblSummonsTitle" runat="server" Width="100%" CssClass="SubContentHead" Text="<%$Resources:lblSummonsTitle.Text %>"></asp:Label>
                                <table style="border-style: none;" class="Normal">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:Label ID="labelSelectSummonsServer" runat="server" CssClass="Normal" Width="176px" Text="<%$Resources:labelSelectSummonsServer.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboSummonsServer" runat="server" CssClass="Normal" AutoPostBack="true"
                                                Width="233px" OnSelectedIndexChanged="cboSummonsServer_SelectedIndexChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:Label ID="labelSelectSummonsServerStaff" runat="server" CssClass="Normal"
                                                Width="199px" Text="<%$Resources:labelSelectSummonsServerStaff.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboSummonsServerStaff" runat="server" CssClass="Normal"
                                                Width="233px" />
                                        </td>
                                    </tr>
                                </table>
                                <table style="border-style: none;" class="Normal">
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Button runat="server" ID="btnAssign" Text="<%$Resources:btnAssign.Text %>" CssClass="NormalButton"
                                                OnClick="btnAssign_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- Court and Court Date selection -->
                            <asp:Panel ID="pnlCourt" runat="server" Width="100%">
                                <!--Jerry 2014-02-11 invisible court/courtroom DDL, it comes from notice-->
                                <asp:Label ID="lblCourt" runat="server" Width="100%" CssClass="SubContentHead" Text="<%$Resources:lblCourt.Text %>" Visible="false"></asp:Label>
                                <br />
                                <table style="border-style: none;">
                                    <tr>
                                        <td style="width: 126px">
                                            <asp:Label ID="labelCourtName" runat="server" CssClass="Normal" Width="128px" Text="<%$Resources:labelCourtName.Text %>" Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboCourt" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboCourt_SelectedIndexChanged"
                                                CssClass="Normal" Width="187px" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 126px">
                                            <asp:Label ID="labelCourtRoom" runat="server" CssClass="Normal" Width="130px" Text="<%$Resources:labelCourtRoom.Text %>" Visible="false"> </asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboCourtRoom" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboCourtRoom_SelectedIndexChanged"
                                                CssClass="Normal" Width="187px" Visible="false" />
                                        </td>
                                    </tr>
                                    <%--Jerry 2012-05-23 change no need display this court date<tr>
                                    <td style="width: 126px">
                                        <asp:Label ID="labelCourtDate" runat="server" CssClass="Normal" Width="126px">Court Date:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCourtDate" runat="server" CssClass="Normal" />
                                    </td>
                                </tr>--%>
                                    <tr>
                                        <td colspan="2" style="text-align: right">
                                            <asp:HiddenField ID="hidCourtDate" runat="server" />
                                            <asp:Button ID="btnGenerateSummons" runat="server" CssClass="NormalButton" OnClick="btnGenerateSummons_Click"
                                                Text="<%$Resources:btnGenerateSummons.Text %>" Width="150px" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlSummonsMultiplePrint" runat="server" Width="100%" Font-Underline="False">
                                <table cellpadding="0" cellspacing="0" width="80%">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnContinue" runat="server" Text="<%$Resources:lbtnContinue.Text %>" CssClass="NormalButton" Width="150px" OnClick="btnContinue_Click" />&nbsp;&nbsp;
                                        <asp:Label ID="lblProcessResult" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label17" runat="server" Width="100%" CssClass="SubContentHead" Text="<%$Resources:lblMultipleSummons.Text %>"></asp:Label>
                                            <%--<asp:LinkButton ID="LinkButton10" runat="server" OnClick="lnkSearch_Click" Text="<%$Resources:lblSearchPage.Text %>"></asp:LinkButton>&nbsp;&nbsp;--%>
                                         
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="grdSumonsMultiplePrint" Width="100%" runat="server" AllowPaging="True"
                                                AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" PageSize="15" ShowFooter="True"
                                                OnPageIndexChanging="grdSumonsMultiplePrint_PageIndexChanging">
                                                <FooterStyle CssClass="CartListHead" />
                                                <Columns>
                                                    <%--<asp:BoundField DataField="ChgIntNo" HeaderText="<%$Resources:grdSumonsMultiplePrint.HeaderText %>" Visible="false" />--%>
                                                    <asp:BoundField DataField="SumIntNo" HeaderText="<%$Resources:grdSumonsMultiplePrint.HeaderText1 %>" Visible="true" />
                                                    <asp:BoundField DataField="PrintAutIntNo" HeaderText="<%$Resources:grdSumonsMultiplePrint.HeaderText2 %>" Visible="true" />
                                                    <asp:BoundField DataField="SummonsNo" HeaderText="<%$Resources:grdSumonsMultiplePrint.HeaderText3 %>" />
                                                    <asp:BoundField DataField="PrintFile" HeaderText="<%$Resources:grdSumonsMultiplePrint.HeaderText4 %>" />
                                                    <asp:BoundField DataField="CPAText" HeaderText="" />
                                                     <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkBoxPrint" runat="server" AutoPostBack="true" OnCheckedChanged="chkBoxPrint_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="CartListHead" />
                                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <p>
                                    <table>
                                        <tr class="Normal">
                                            <td>
                                                <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblImportant.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:Button ID="btnBatchPrintCPA5" runat="server" CssClass="NormalButton" Text="<%$Resources:btnBatchPrintCPA5.Text %>" Width="150px" OnClick="btnBatchPrintCPA5_Click" Enabled="false" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="Normal">
                                            <td>
                                                <asp:Label ID="Label8" runat="server" Text="<%$Resources:lblImportant.Text1 %>"></asp:Label></td>
                                            <td>
                                                <asp:Button ID="btnBatchPrintCPA6" runat="server" CssClass="NormalButton" Text="<%$Resources:btnBatchPrintCPA6.Text %>" Width="150px" OnClick="btnBatchPrintCPA6_Click" Enabled="false" /></td>
                                        </tr>
                                    </table>
                                </p>


                            </asp:Panel>
                            <asp:Panel ID="pnlSummonOther" runat="server" Width="100%" Font-Underline="False">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboOfficer" runat="server" CssClass="Normal" />
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;<asp:Label
                                    ID="Label9" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label>
                            </p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <input id="hidRBSIntNo" type="hidden" runat="server" />
                    <input id="hidValidateMobileMsg" value="<%$Resources:lblMessage.Text49 %>" type="hidden" runat="server" />
                    <input id="hidValidateLandLineMsg" value="<%$Resources:lblMessage.Text50 %>" type="hidden" runat="server" />
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="style2" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
    <script src="Scripts/Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function PhoneCheck() {
            var strMatch = "0123456789()-+.";

            var lenM = $("#txtMobileNo").val().length;
            var valueM = $("#txtMobileNo").val();
            for (var i = 0; i < lenM; i++) {
                var charM = valueM.charAt(i);
                if (charM.trim() != "" && strMatch.indexOf(charM) < 0) {
                    alert($("#hidValidateMobileMsg").val());
                    return false;
                }
            }

            var lenL = $("#txtLandLine").val().length;
            var valueL = $("#txtLandLine").val()
            for (var i = 0; i < lenL; i++) {
                var charL = valueL.charAt(i);
                if (charL.trim() != "" && strMatch.indexOf(charL) < 0) {
                    alert($("#hidValidateLandLineMsg").val());
                    return false;
                }
            }
            return true;
        }

    </script>
</body>
</html>
