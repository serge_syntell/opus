<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="RepresentationOnTheFly" Codebehind="RepresentationOnTheFly.ascx.cs" %>
<asp:Label ID="labelRepresentation" runat="server" CssClass="SubContentHead" Width="100%" Text="<%$Resources:labelRepresentation.Text %>"></asp:Label>
<table border="0" style="text-align: left;">
    <tr>
        <td valign="top" class="NormalBold">
            <asp:Label ID="Label1" runat="server" Text="<%$Resources:lblRevisedAmount.Text %>"></asp:Label>
        </td>
        <td style="width: 300px;">
            <asp:Label ID="lblAmount" CssClass="Normal" runat="server" />
        </td>
    </tr>
    <tr>
        <td valign="top" class="NormalBold">
            <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblRepresentationDetail.Text %>"></asp:Label>
        </td>
        <td style="width: 300px;">
            <asp:TextBox ID="txtRepresentationDetails" runat="server" CssClass="Normal" Width="98%"
                Rows="3" TextMode="MultiLine" />
        </td>
    </tr>
    <tr>
        <td valign="top" class="NormalBold">
            <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblOffenderName.Text %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtRepresentationName" runat="server" CssClass="Normal" Width="97%" />
        </td>
    </tr>
    <tr>
        <td valign="top" class="NormalBold">
            <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblOffenderAddress.Text %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtRepresentationAddress" runat="server" CssClass="Normal" Rows="3"
                TextMode="MultiLine" Width="97%" />
        </td>
    </tr>
    <tr>
        <td valign="top" class="NormalBold">
            <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblOffenderPhone.Text %>"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtRepresentationPhone" runat="server" CssClass="Normal" Width="97%" />
        </td>
    </tr>
    <tr>
        <td valign="top" class="NormalBold">
            <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblOfficialName.Text %>"></asp:Label></td>
        <td>
            <asp:DropDownList ID="cboRepresentationOfficial" runat="server" Width="97%" CssClass="Normal">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td class="NormalBold">
            &nbsp;</td>
        <td style="text-align: right">
            <asp:Button ID="btnRepresentation" runat="server" Text="<%$Resources:btnRepresentation.Text %>" CssClass="NormalButton"
                OnClick="btnRepresentation_Click" /></td>
    </tr>
    <tr>
        <td colspan="2" class="NormalBold">
            <asp:Label ID="lblRepresentationError" ForeColor="Red" runat="server" CssClass="NormalRed" />
        </td>
    </tr>
</table>
