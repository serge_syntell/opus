using System;
using System.Web.UI.HtmlControls;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class SuspensePaymentReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
       
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SuspensePaymentReport.aspx";
        //protected string thisPage = "Payment Suspense Report";

        protected const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                // TODO: Page setup here
            }
        }

         
        
        protected void btnShow_Click(object sender, EventArgs e)
        {
            string dateString = "2000-01-01";
            string endDateString = "2100-01-01";
            string easyPayOnly = "0";

            if (!this.dtpDate.Text.Trim().Equals(string.Empty))
                dateString = Convert.ToDateTime(this.dtpDate.Text).ToString(DATE_FORMAT);

            if (!this.dtpEndDate.Text.Trim().Equals(string.Empty))
                endDateString = Convert.ToDateTime(this.dtpEndDate.Text).ToString(DATE_FORMAT);

            //TLP - 20081023 - new checkbox for EasyPay suspense payments
            if (chkShowEasypayOnly.Checked)
                easyPayOnly = "1";

            Helper_Web.BuildPopup(this, "SuspenseAccountPaymentReportViewer.aspx", "Date", dateString, "EndDate", endDateString, "EasyPayOnly", easyPayOnly);
            //PunchStats805806 enquiry ViewSuspensePayments
        }

    }
}
