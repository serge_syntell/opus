<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.UserMenuBuilder" Codebehind="UserMenuBuilder.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <tg1:Tag ID="Tag1" runat="server"></tg1:Tag>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <table cellspacing="0" cellpadding="0" width="90" border="0">
        <tr valign="top">
            <td width="90">
                <form runat="server">
                    <table height="88" cellspacing="0" cellpadding="0" width="372" border="0">
                        <tr valign="top">
                            <td width="1" height="7">
                            </td>
                            <td width="371">
                            </td>
                        </tr>
                        <tr valign="top">
                            <td height="81">
                            </td>
                            <td>
                                <table id="Table1" height="80" cellspacing="1" cellpadding="1" width="370" border="0">
                                    <tr>
                                        <td valign="top" width="355">
                                            <asp:Panel ID="pnlShowMenuItem" runat="server" Width="365px">
                                                <table id="tblShowMenuItem" height="27" cellspacing="0" cellpadding="1" width="363"
                                                    border="0">
                                                    <tr>
                                                        <td valign="top" align="left" colspan="2" height="25">
                                                            <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label></td>
                                                        <td height="25">
                                                            <p>
                                                                <asp:TextBox ID="txtName" runat="server" Width="264px" CssClass="Normal" MaxLength="50"
                                                                    ReadOnly="True"></asp:TextBox></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="left" colspan="2" height="25">
                                                            <asp:Label ID="lblURL" runat="server" CssClass="NormalBold" Text="<%$Resources:lblURL.Text %>"></asp:Label></td>
                                                        <td height="25">
                                                            <asp:TextBox ID="txtURL" runat="server" Width="250px" CssClass="NormalMand" MaxLength="100"
                                                                ReadOnly="True"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left" colspan="2">
                                                            <asp:TextBox ID="txtColName" runat="server" Width="12px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox>
                                                            <asp:TextBox ID="txtColValue" runat="server" Width="13px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox>
                                                            <asp:TextBox ID="txtAPLIntNo" runat="server" Width="10px" CssClass="Normal" MaxLength="2"
                                                                Visible="False"></asp:TextBox></td>
                                                        <td>
                                                            <asp:CheckBox ID="chkAllowAccess" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAllowAccess.Text %>">
                                                            </asp:CheckBox>
                                                            <asp:Button ID="btnSave" runat="server" Width="123px" CssClass="NormalButton" Text="<%$Resources:btnSave.Text %>"
                                                                OnClick="btnSave_Click"></asp:Button></td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" ForeColor=" " EnableViewState="false"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
    </table>
</body>
</html>
