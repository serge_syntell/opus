﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Stalberg.TMS
{
    public partial class ReceiptReportViewer : System.Web.UI.Page
    {
        private string connectionString = String.Empty;
        
        protected string title = String.Empty;

        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            General gen = new General();
            title = gen.SetTitle(Session["drTitle"]);

            if(!Page.IsPostBack)
            {
                int autIntNo = 0;
                int userIntNo = 0;
                DateTime fromDate = DateTime.MinValue;
                DateTime toDate = DateTime.MaxValue;
                int year = 0;
                int month = 0;
                string authority = "";

                if (Request.QueryString["AutIntNo"] != null)
                {
                    int.TryParse(Request.QueryString["AutIntNo"].ToString(), out autIntNo);
                }
                if (Request.QueryString["UserIntNo"] != null)
                {
                    int.TryParse(Request.QueryString["UserIntNo"].ToString(), out userIntNo);
                }
                if (Request.QueryString["From"] != null)
                {
                    DateTime.TryParse(Request.QueryString["From"].ToString(), out fromDate);
                }
                if (Request.QueryString["To"] != null)
                {
                    DateTime.TryParse(Request.QueryString["To"].ToString(), out toDate);
                }
                if (Request.QueryString["Year"] != null)
                {
                    int.TryParse(Request.QueryString["Year"].ToString(), out year);
                }
                if (Request.QueryString["Month"] != null)
                {
                    int.TryParse(Request.QueryString["Month"].ToString(), out month);
                }
                if (Request.QueryString["Authority"] != null)
                {
                    authority = Request.QueryString["Authority"].ToString().Trim();
                }

                if (fromDate.Equals(DateTime.MinValue) && toDate.Equals(DateTime.MaxValue))
                {
                    if (month == 13)
                    {
                        DateTime.TryParse(year.ToString() + "-01-01", out fromDate);
                        DateTime.TryParse(year.ToString() + "-12-31", out toDate);
                    }
                    else
                    {
                        DateTime date = DateTime.Now;
                        if (DateTime.TryParse(year.ToString() + "-" + month.ToString(), out date))
                        {
                            fromDate = date.AddDays(1 - date.Day);
                            toDate = date.AddDays(1 - date.Day).AddMonths(1).AddDays(-1);
                        }
                    }
                }

                string filename = "ReceiptReport.xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();

                SIL.AARTO.BLL.Report.ReceiptReport rr = new SIL.AARTO.BLL.Report.ReceiptReport(connectionString);

                MemoryStream stream = rr.ExportToExcel(userIntNo, authority, autIntNo, fromDate, toDate);

                Response.BinaryWrite(stream.GetBuffer());
                Response.End();
            }
        }
    }
}