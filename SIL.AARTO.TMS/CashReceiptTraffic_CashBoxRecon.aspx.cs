using System;
using System.Text;
using System.Collections.Generic;




namespace Stalberg.TMS
{
    /// <summary>
    /// represents a page that a cahier uses to reconcile their cashbox at the end of the day
    /// </summary>
    public partial class CashReceiptTraffic_CashBoxRecon : System.Web.UI.Page
    {
        // Constants
        private const string CASH_FORMAT = "#,##0";

        // Fields
        private string _connectionString = String.Empty;
        private string _login;
        private int _autIntNo;
        private Cashier _cashier;
        private bool _isBalancingHints;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "CashReceiptTraffic_CashBoxRecon.aspx";
        //protected string thisPage = "Daily Cashbox Reconciliation";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this._connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (this.Session["Cashier"] == null)
                Response.Redirect("");

            int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this._connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this._login = userDetails.UserLoginName;
            this._autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this._cashier = (Cashier)this.Session["Cashier"];

            this.CheckForEODBalancingHints();

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.GetCashingUpFigures();

                // Add client events
                this.txt500.Attributes.Add("onkeyup", "addCash(txt500, lbl500, 500)");
                this.txt200.Attributes.Add("onkeyup", "addCash(txt200, lbl200, 200)");
                this.txt100.Attributes.Add("onkeyup", "addCash(txt100, lbl100, 100)");
                this.txt50.Attributes.Add("onkeyup", "addCash(txt50, lbl50, 50)");
                this.txt20.Attributes.Add("onkeyup", "addCash(txt20, lbl20, 20)");
                this.txt10.Attributes.Add("onkeyup", "addCash(txt10, lbl10, 10)");
                this.txt5.Attributes.Add("onkeyup", "addCash(txt5, lbl5, 5)");
                this.txt2.Attributes.Add("onkeyup", "addCash(txt2, lbl2, 2)");
                this.txt1.Attributes.Add("onkeyup", "addCash(txt1, lbl1, 1)");
                this.txtCardAmount.Attributes.Add("onkeyup", "addOthers(txtCardAmount, lblCards)");
            //  SD:
                this.txtDebitCardAmount.Attributes.Add("onkeyup", "addOthers(txtDebitCardAmount, lblDCards)");
                this.txtChequeAmount.Attributes.Add("onkeyup", "addOthers(txtChequeAmount, lblCheques)");
                this.txtPostalOrderAmount.Attributes.Add("onkeyup", "addOthers(txtPostalOrderAmount, lblPostalOrders)");

                // Hide the count boxes based on authority rule
                if (this._isBalancingHints)
                {
                    this.txtCardCount.Visible = false;
                    this.txtDebitCardCount.Visible = false; //SD
                    this.txtChequeCount.Visible = false;
                    this.txtPostalOrderCount.Visible = false;

                    this.lblNoCards.Visible = false;
                    this.lblNoDebitCards.Visible = false; //SD
                    this.lblNoCheques.Visible = false;
                    this.lblNoPO.Visible = false;
                }
            }
        }

        private void CheckForEODBalancingHints()
        {
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this._autIntNo;
            rule.ARCode = "4580";
            rule.LastUser = this._login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this._connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            this._isBalancingHints = value.Value.Trim().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
        }

        private void GetCashingUpFigures()
        {
            this.lblFloat.Text = this._cashier.Float.ToString(CASH_FORMAT);

            CashboxDB db = new CashboxDB(this._connectionString);
            CashBoxRecon recon = db.GetCashBoxRecon(this._cashier.CBIntNo, this._isBalancingHints);

            decimal amount = 0;
            decimal temp = 0;
            this.txt1.Text = recon.R1.ToString();
            this.lbl1.Text = (amount = recon.R1).ToString(CASH_FORMAT);

            this.txt10.Text = recon.R10.ToString();
            amount += (temp = recon.R10 * 10);
            this.lbl10.Text = temp.ToString(CASH_FORMAT);
            this.txt100.Text = recon.R100.ToString();

            amount += (temp = recon.R100 * 100);
            lbl100.Text = temp.ToString(CASH_FORMAT);

            this.txt2.Text = recon.R2.ToString();
            amount += (temp = recon.R2 * 2);
            this.lbl2.Text = temp.ToString(CASH_FORMAT);

            this.txt20.Text = recon.R20.ToString();
            amount += (temp = recon.R20 * 20);
            this.lbl20.Text = temp.ToString(CASH_FORMAT);

            this.txt200.Text = recon.R200.ToString();
            amount += (temp = recon.R200 * 200);
            this.lbl200.Text = temp.ToString(CASH_FORMAT);

            this.txt5.Text = recon.R5.ToString();
            amount += (temp = recon.R5 * 5);
            this.lbl5.Text = temp.ToString(CASH_FORMAT);

            this.txt50.Text = recon.R50.ToString();
            amount += (temp = recon.R50 * 50);
            this.lbl50.Text = temp.ToString(CASH_FORMAT);

            this.txt500.Text = recon.R500.ToString();
            amount += (temp = recon.R500 * 500);
            this.lbl500.Text = temp.ToString(CASH_FORMAT);

            this.lblCash.Text = amount.ToString(CASH_FORMAT);
            
            amount += (temp = recon.CardAmount);
            this.txtCardAmount.Text = temp.ToString(CASH_FORMAT);
            this.lblCards.Text = temp.ToString(CASH_FORMAT);
            this.txtCardCount.Text = recon.CardCount.ToString();

            // sd:20081211 Added debit card details
            amount += (temp = recon.DebitCardAmount);
            this.txtDebitCardAmount.Text = temp.ToString(CASH_FORMAT);
            this.lblDCards.Text = temp.ToString(CASH_FORMAT);
            this.txtDebitCardCount.Text = recon.DebitCardCount.ToString();
            //  

            amount += (temp = recon.ChequeAmount);
            this.txtChequeAmount.Text = temp.ToString(CASH_FORMAT);
            this.lblCheques.Text = temp.ToString(CASH_FORMAT);
            this.txtChequeCount.Text = recon.ChequeCount.ToString();
            
            amount += (temp = recon.POAmount);
            this.txtPostalOrderAmount.Text = temp.ToString(CASH_FORMAT);
            this.lblPostalOrders.Text = temp.ToString(CASH_FORMAT);
            this.txtPostalOrderCount.Text = recon.POCount.ToString();

            this.lblTotalInBox.Text = (amount).ToString(CASH_FORMAT);

            if (this._isBalancingHints)
            {
                this.txtPostalOrderCount.Text = recon.POCount.ToString();
                this.txtChequeCount.Text = recon.ChequeCount.ToString();
                this.txtCardCount.Text = recon.CardCount.ToString();
                this.txtDebitCardCount.Text = recon.DebitCardCount.ToString();  // SD
            }
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnRecord_Click(object sender, EventArgs e)
        {
            this.RecordData();
            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            this.GetCashingUpFigures();
            //PunchStats805806 enquiry DailyCashboxReconciliation
        }

        private CashBoxRecon RecordData()
        {
            bool valid = true;
            string temp = null;
            StringBuilder sb = new StringBuilder("<ul>" + (string)GetLocalResourceObject("stringMsg") + "\n");

            // Validate the data entry
            int r500 = 0;
            temp = this.txt500.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r500))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R500") + "</li>\n");
            }
            int r200 = 0;
            temp = this.txt200.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r200))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R200") + "</li>\n");
            }
            int r100 = 0;
            temp = this.txt100.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r100))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R100") + "</li>\n");
            }
            int r50 = 0;
            temp = this.txt50.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r50))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R50") + "</li>\n");
            }
            int r20 = 0;
            temp = this.txt20.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r20))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R20") + "</li>\n");
            }
            int r10 = 0;
            temp = this.txt10.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r10))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R10") + "</li>\n");
            }
            int r5 = 0;
            temp = this.txt5.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r5))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R5") + "</li>\n");
            }
            int r2 = 0;
            temp = this.txt2.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r2))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R2") + "</li>\n");
            }
            int r1 = 0;
            temp = this.txt1.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out r1))
            {
                valid = false;
                sb.Append("<li>" + string.Format((string)GetLocalResourceObject("stringMsg1"), "R1") + "</li>\n");
            }
            int chequeCount = 0;
            temp = this.txtChequeCount.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out chequeCount))
            {
                valid = false;
                sb.Append("<li>" + (string)GetLocalResourceObject("stringMsg2") + "</li>\n");
            }
            decimal chequeAmount = 0;
            temp = this.txtChequeAmount.Text.Trim();
            if (temp.Length > 0 && !decimal.TryParse(temp, out chequeAmount))
            {
                valid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("stringMsg3")+"</li>\n");
            }
           
            int cardCount = 0;
            temp = this.txtCardCount.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out cardCount))
            {
                valid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("stringMsg4")+"</li>\n");
            }
            decimal cardAmount = 0;
            temp = this.txtCardAmount.Text.Trim();
            if (temp.Length > 0 && !decimal.TryParse(temp, out cardAmount))
            {
                valid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("stringMsg5")+"</li>\n");
            }

            // SD:20081210 Debit Card
            int dcardCount = 0;
            temp = this.txtDebitCardCount.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out dcardCount))
            {
                valid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("stringMsg6")+"</li>\n");
            }

            decimal dcardAmount = 0;
            temp = this.txtDebitCardAmount.Text.Trim();
            if (temp.Length > 0 && !decimal.TryParse(temp, out dcardAmount))
            {
                valid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("stringMsg7")+"</li>\n");
            }


            int poCount = 0;
            temp = this.txtPostalOrderCount.Text.Trim();
            if (temp.Length > 0 && !int.TryParse(temp, out poCount))
            {
                valid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("stringMsg8")+"</li>\n");
            }
            decimal poAmount = 0;
            temp = this.txtPostalOrderAmount.Text.Trim();
            if (temp.Length > 0 && !decimal.TryParse(temp, out poAmount))
            {
                valid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("stringMsg9")+"</li>\n");
            }

            if (!valid)
            {
                sb.Append("</ul>\n");

                this.lblError.Text = (string)GetLocalResourceObject("stringMsg10");
                return null;
            }
            this.lblError.Text = string.Empty;

            // Record the recon data
            CashBoxRecon recon = new CashBoxRecon();
            recon.CBIntNo = this._cashier.CBIntNo;
            recon.CardAmount = cardAmount;
            recon.CardCount = cardCount;
            
            //SD: 20081210 Debit card details
            recon.DebitCardAmount = dcardAmount;
            recon.DebitCardCount = dcardCount;

            recon.ChequeAmount = chequeAmount;
            recon.ChequeCount = chequeCount;

            recon.POAmount = poAmount;
            recon.POCount = poCount;

            recon.R1 = r1;
            recon.R10 = r10;
            recon.R100 = r100;
            recon.R2 = r2;
            recon.R20 = r20;
            recon.R200 = r200;
            recon.R5 = r5;
            recon.R50 = r50;
            recon.R500 = r500;

            recon.LastUser = this._login;   // 2013-07-23 add by Henry

            CashboxDB db = new CashboxDB(this._connectionString);
            db.RecordCashboxReconData(recon);

            return recon;
        }

        protected void buttonRecon_Click(object sender, EventArgs e)
        {
            CashBoxRecon recon = this.RecordData();
            if (recon == null)
                return;

            // Compare the entered data with the database
            CashboxDB db = new CashboxDB(this._connectionString);
            DBReconData reconData = db.GetDBReconData(this._cashier.CBIntNo, this._autIntNo);

            bool valid = true;
            StringBuilder sb = new StringBuilder("<ul>" + (string)GetLocalResourceObject("lblError.Text1") + "\n");
            if (recon.Cash != (reconData.Cash + reconData.Float))
            {
                valid = false;
                if (this._isBalancingHints)
                    sb.Append(string.Format("<li>" + (string)GetLocalResourceObject("lblError.Text2") + "</li>\n",
                        recon.Cash.ToString(CASH_FORMAT), reconData.Cash.ToString(CASH_FORMAT)));
                else
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text3")+"</li>\n");

            }

            if (recon.CardCount != reconData.CardCount)
            {
                valid = false;
                if (this._isBalancingHints)
                    sb.Append(string.Format("<li>"+(string)GetLocalResourceObject("lblError.Text4")+"</li>\n",
                        recon.CardCount, reconData.CardCount));
                else
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text5")+"</li>\n");
            }
            
            if (recon.CardAmount != reconData.CardAmount)
            {
                valid = false;
                if (this._isBalancingHints)
                    sb.Append(string.Format("<li>"+(string)GetLocalResourceObject("lblError.Text6")+"</li>\n",
                        recon.CardAmount.ToString(CASH_FORMAT), reconData.CardAmount.ToString(CASH_FORMAT)));
                else
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text7")+"</li>\n");
            }


            //SD:20081211 - debit card
            if (recon.DebitCardCount!= reconData.DebitCardCount)
            {
                valid = false;
                if (this._isBalancingHints)
                    sb.Append(string.Format("<li>" + (string)GetLocalResourceObject("lblError.Text8") + "</li>\n",
                        recon.DebitCardCount, reconData.DebitCardCount ));
                else
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text5")+"</li>\n");
            }

            if (recon.DebitCardAmount!= reconData.DebitCardAmount)
            {
                valid = false;
                if (this._isBalancingHints)
                    sb.Append(string.Format("<li>" + (string)GetLocalResourceObject("lblError.Text9") + "</li>\n",
                        recon.DebitCardAmount.ToString(CASH_FORMAT), reconData.DebitCardAmount.ToString(CASH_FORMAT)));
                else
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text10")+"</li>\n");
            }
            //=

            if (recon.ChequeCount != reconData.ChequeCount)
            {
                valid = false;
                if (this._isBalancingHints)
                sb.Append(string.Format("<li>"+(string)GetLocalResourceObject("lblError.Text11")+"</li>\n",
                    recon.ChequeCount, reconData.ChequeCount));
                else
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text12")+"</li>\n");
            }

            if (recon.ChequeAmount != reconData.ChequeAmount)
            {
                valid = false;
                if (this._isBalancingHints)
                    sb.Append(string.Format("<li>"+(string)GetLocalResourceObject("lblError.Text13")+"</li>\n",
                        recon.ChequeAmount.ToString(CASH_FORMAT), reconData.ChequeAmount.ToString(CASH_FORMAT)));
                else
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text14")+"</li>\n");
            }

            if (recon.POCount != reconData.PostalOrderCount)
            {
                valid = false;
                if (this._isBalancingHints)
                    sb.Append(string.Format("<li>"+(string)GetLocalResourceObject("lblError.Text15")+"</li>\n",
                        recon.POCount, reconData.PostalOrderCount));
                else 
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text16")+"</li>\n");
            }

            if (recon.POAmount != reconData.PostalOrderAmount)
            {
                valid = false;
                if (this._isBalancingHints)
                    sb.Append(string.Format("<li>"+(string)GetLocalResourceObject("lblError.Text17")+"</li>\n",
                        recon.POAmount.ToString(CASH_FORMAT), reconData.PostalOrderAmount.ToString(CASH_FORMAT)));
                else
                    sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text18")+"</li>\n");
            }

            if (!valid)
            {
                sb.Append("<ul>\n");
                this.lblError.Text = sb.ToString();
                this.GetCashingUpFigures();
                return;
            }

            // Redirect this page to the default page and open a new window with the cash up report in it
            Helper_Web.BuildPopup(this, "CashReceiptTraffic_ReconViewer.aspx", "CBIntNo", this._cashier.CBIntNo.ToString());

            this.pnlDetails.Visible = false;
            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text19");
            //PunchStats805806 enquiry DailyCashboxReconciliation
        }

       
}
}
