﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using System.IO;
using SIL.AARTO.BLL.Utility.PrintFile;

namespace Stalberg.TMS
{
    public partial class CourtRollChargeSheetViewer : DplxWebForm
    {

        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string sRoomNo = string.Empty;
        private string sCrtName = string.Empty;
        private string sReportType = "P";
        //RecordBox rb;
        //ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPage = "Court Roll Charge Sheet Viewer";
        protected string thisPageURL = "CourtRollChargeSheetViewer.aspx";

        #region Oscar 20120207 disabled for backup
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    // Get user details
        //    Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
        //    Stalberg.TMS.UserDetails userDetails = new UserDetails();

        //    userDetails = (UserDetails)Session["userDetails"];
        //    autIntNo = Request.QueryString["AutIntNo"] == null ? autIntNo : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString()); ;

        //    // Set domain specific variables
        //    General gen = new General();
        //    backgroundImage = gen.SetBackground(Session["drBackground"]);
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    // Setup the report
        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);
        //    string reportPage = arn.GetAuthReportName(autIntNo, "CourtRollChargeSheet");
        //    if (reportPage.Equals(string.Empty))
        //    {
        //        int arnIntNo = arn.AddAuthReportName(autIntNo, "CourtRollChargeSheet.dplx", "CourtRollChargeSheet", "system", string.Empty);
        //        reportPage = "CourtRollChargeSheet.dplx";
        //    }

        //    string path = Server.MapPath("reports/" + reportPage);

        //    //****************************************************
        //    // check that report actually exists
        //    string templatePath = string.Empty;
        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "CourtRollChargeSheet");

        //    if (!File.Exists(path))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        // we can only check that the template path exists if there is actually a template!
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //****************************************************

        //    DocumentLayout doc = new DocumentLayout(path);

        //    autIntNo = Request.QueryString["AutIntNo"] == null ? autIntNo : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString());

        //    sReportType = Request.QueryString["ReportType"] == null ? "P" : Request.QueryString["ReportType"].ToString();
        //    int nCrtIntNo = Request.QueryString["CrtIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["CrtIntNo"].ToString());
        //    int nCrtRIntNo = Request.QueryString["CrtRIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["CrtRIntNo"].ToString());
        //    DateTime dtCourtDate;
        //    DateTime dtFinalPrintDate;

        //    if (!DateTime.TryParse(Request.QueryString["CourtDate"], out dtCourtDate))
        //    {
        //        string error = "Please select a valid court date";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);
        //        Response.Redirect(errorURL);
        //        return;
        //    }

        //    if (!DateTime.TryParse(Request.QueryString["FinalPrintDate"], out dtFinalPrintDate))
        //    {
        //        string error = "Please select a valid final print date";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);
        //        Response.Redirect(errorURL);
        //        return;
        //    }

        //    //jerry 2011-11-07 add
        //    int courtRollType = Convert.ToInt32(Request.QueryString["CourtRollType"].ToString());

        //    Query query = (Query)doc.GetQueryById("Query");

        //    query.ConnectionString = this.connectionString;

        //    if (sReportType.Equals("F"))
        //    {
        //        // print charge sheet with S56  court roll
        //        AuthorityRulesDetails arChargeSheetWithS56 = new AuthorityRulesDetails();
        //        arChargeSheetWithS56.AutIntNo = this.autIntNo;
        //        arChargeSheetWithS56.ARCode = "6206";
        //        arChargeSheetWithS56.LastUser = userDetails.UserLoginName;

        //        DefaultAuthRules arDefaultWithS56 = new DefaultAuthRules(arChargeSheetWithS56, this.connectionString);
        //        arDefaultWithS56.SetDefaultAuthRule();

        //        // print charge sheet with S54  court roll
        //        AuthorityRulesDetails arChargeSheetWithS54 = new AuthorityRulesDetails();
        //        arChargeSheetWithS54.AutIntNo = this.autIntNo;
        //        arChargeSheetWithS54.ARCode = "6207";
        //        arChargeSheetWithS54.LastUser = userDetails.UserLoginName;

        //        DefaultAuthRules arDefaultWithS54 = new DefaultAuthRules(arChargeSheetWithS54, this.connectionString);
        //        arDefaultWithS54.SetDefaultAuthRule();

        //        if (arChargeSheetWithS56.ARString == "N" && arChargeSheetWithS54.ARString == "N")
        //        {
        //            return;
        //        }
        //        else
        //        {
        //            Document reportA = null;
        //            byte[] bufferA = null;
        //            if (arChargeSheetWithS56.ARString == "Y" || sReportType == "P")
        //            {
        //                ParameterDictionary parameters = new ParameterDictionary();
        //                parameters.Add("AutIntNo", autIntNo);
        //                parameters.Add("CrtIntNo", nCrtIntNo);
        //                parameters.Add("CrtRIntNo", nCrtRIntNo);
        //                parameters.Add("SumCourtDate", dtCourtDate);
        //                parameters.Add("FinalPrintDate", dtFinalPrintDate);
        //                parameters.Add("Type", "S56");
        //                parameters.Add("FinalFlag", sReportType);
        //                //jerry 2011-11-07 add
        //                parameters.Add("CourtRollType", courtRollType);

        //                reportA = doc.Run(parameters);
        //                bufferA = reportA.Draw();
        //            }

        //            Document reportB = null;
        //            byte[] bufferB = null;
        //            if (arChargeSheetWithS54.ARString == "Y" || sReportType == "P")
        //            {
        //                ParameterDictionary parameters = new ParameterDictionary();
        //                parameters.Add("AutIntNo", autIntNo);
        //                parameters.Add("CrtIntNo", nCrtIntNo);
        //                parameters.Add("CrtRIntNo", nCrtRIntNo);
        //                parameters.Add("SumCourtDate", dtCourtDate);
        //                parameters.Add("FinalPrintDate", dtFinalPrintDate);
        //                parameters.Add("Type", "S54");
        //                parameters.Add("FinalFlag", sReportType);
        //                //jerry 2011-11-07 add
        //                parameters.Add("CourtRollType", courtRollType);

        //                reportB = doc.Run(parameters);
        //                bufferB = reportB.Draw();
        //            }


        //            byte[] bufferMain;

        //            if (bufferA != null && bufferA.Length > 1488)
        //            {
        //                if (bufferB != null && bufferB.Length > 1488)
        //                    reportA.Pages.Append(reportB.Pages);

        //                bufferMain = reportA.Draw();

        //                Response.ClearContent();
        //                Response.ClearHeaders();
        //                Response.ContentType = "application/pdf";
        //                Response.BinaryWrite(bufferMain);
        //                Response.End();
        //            }
        //            else if (bufferB != null)
        //            {
        //                bufferMain = reportB.Draw();
        //                Response.ClearContent();
        //                Response.ClearHeaders();
        //                Response.ContentType = "application/pdf";
        //                Response.BinaryWrite(bufferMain);
        //                Response.End();

        //            }
        //        }
        //    }
        //    else
        //    {
        //        Document reportA = null;
        //        byte[] bufferA = null;

        //        ParameterDictionary parameters = new ParameterDictionary();
        //        parameters.Add("AutIntNo", autIntNo);
        //        parameters.Add("CrtIntNo", nCrtIntNo);
        //        parameters.Add("CrtRIntNo", nCrtRIntNo);
        //        parameters.Add("SumCourtDate", dtCourtDate);
        //        parameters.Add("FinalPrintDate", dtFinalPrintDate);
        //        parameters.Add("Type", "S56");
        //        parameters.Add("FinalFlag", sReportType);
        //        //jerry 2011-11-07 add
        //        parameters.Add("CourtRollType", courtRollType);

        //        reportA = doc.Run(parameters);
        //        bufferA = reportA.Draw();

        //        Document reportB = null;
        //        byte[] bufferB = null;

        //        parameters = new ParameterDictionary();
        //        parameters.Add("AutIntNo", autIntNo);
        //        parameters.Add("CrtIntNo", nCrtIntNo);
        //        parameters.Add("CrtRIntNo", nCrtRIntNo);
        //        parameters.Add("SumCourtDate", dtCourtDate);
        //        parameters.Add("FinalPrintDate", dtFinalPrintDate);
        //        parameters.Add("Type", "S54");
        //        parameters.Add("FinalFlag", sReportType);
        //        //jerry 2011-11-07 add
        //        parameters.Add("CourtRollType", courtRollType);

        //        reportB = doc.Run(parameters);
        //        bufferB = reportB.Draw();

        //        byte[] bufferMain;

        //        if (bufferA != null && bufferA.Length > 1488)
        //        {
        //            if (bufferB != null && bufferB.Length > 1488)
        //                reportA.Pages.Append(reportB.Pages);

        //            bufferMain = reportA.Draw();

        //            Response.ClearContent();
        //            Response.ClearHeaders();
        //            Response.ContentType = "application/pdf";
        //            Response.BinaryWrite(bufferMain);
        //            Response.End();
        //        }
        //        else if (bufferB != null)
        //        {
        //            bufferMain = reportB.Draw();
        //            Response.ClearContent();
        //            Response.ClearHeaders();
        //            Response.ContentType = "application/pdf";
        //            Response.BinaryWrite(bufferMain);
        //            Response.End();

        //        }
        //    }
        //}
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Request.QueryString["AutIntNo"] == null ? autIntNo : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString()); ;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            PrintFileProcess proscess = new PrintFileProcess(this.connectionString);
            proscess.BuildPrintFile(
                new PrintFileModuleCourtRoll(CourtRollType.ChargeSheet, Request.QueryString["ReportType"], Request.QueryString["CrtIntNo"], Request.QueryString["CrtRIntNo"], Request.QueryString["CourtDate"], Request.QueryString["FinalPrintDate"], Request.QueryString["CourtRollType"]),
                this.autIntNo
            );
        }
    }
}
