<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.PublicHolidays" Codebehind="PublicHolidays.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnCopy" runat="server" Text="<%$Resources:btnCopy.Text %>" Width="135px" CssClass="NormalButton"
                                        OnClick="btnCopy_Click" /></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1" style="text-align: center">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" Text="<%$Resources:lblPageName.Text %>" runat="server" Width="100%" CssClass="ContentHead"></asp:Label>&nbsp;</p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="udpPublicHolidays" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlCalendar" runat="Server" Width="100%">
                                <center>
                                    <table border="0">
                                        <tr>
                                            <td style="text-align: left;">
                                                <asp:LinkButton ID="lnkPrevious" runat="server" CssClass="NormalButton" Text="<%$Resources:lnkPrevious.Text %>" OnClick="lnkPrevious_Click"></asp:LinkButton></td>
                                            <td colspan="2" style="text-align: center;">
                                                <asp:Label ID="lblCurrentYear" runat="server" CssClass="NormalBold"></asp:Label></td>
                                            <td style="text-align: right;">
                                                <asp:LinkButton ID="lnkNext" runat="server" Text="<%$Resources:lnkNext.Text %>" CssClass="NormalButton" OnClick="lnkNext_Click"></asp:LinkButton></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Calendar ID="calJanuary" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td>
                                                <asp:Calendar ID="calFebruary" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td>
                                                <asp:Calendar ID="calMarch" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td style="width: 200px">
                                                <asp:Calendar ID="calApril" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Calendar ID="calMay" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td>
                                                <asp:Calendar ID="calJune" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td>
                                                <asp:Calendar ID="calJuly" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td style="width: 200px">
                                                <asp:Calendar ID="calAugust" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Calendar ID="calSeptember" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td>
                                                <asp:Calendar ID="calOctober" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td>
                                                <asp:Calendar ID="calNovember" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                            <td style="width: 200px">
                                                <asp:Calendar ID="calDecember" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                                    Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText="" PrevMonthText=""
                                                    Width="200px" OnSelectionChanged="calJanuary_SelectionChanged" OnDayRender="Calendar_DayRender"
                                                    ShowNextPrevMonth="False">
                                                    <SelectedDayStyle Font-Bold="True" CssClass="NormalRed" />
                                                    <TodayDayStyle CssClass="NormalButton" />
                                                    <SelectorStyle CssClass="NormalButton" />
                                                    <WeekendDayStyle CssClass="CartListItemAlt" />
                                                    <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                                    <NextPrevStyle VerticalAlign="Bottom" CssClass="Normal" />
                                                    <DayHeaderStyle Font-Size="7pt" CssClass="CartListHead" />
                                                    <TitleStyle CssClass="NormalButton" />
                                                    <DayStyle CssClass="Normal" />
                                                </asp:Calendar>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                                <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                                    <center>
                                        <table border="0" class="NormalBold">
                                            <tr>
                                                <td style="width: 100px">
                                                    <asp:Label ID="lblDate" runat="server" Text="<%$Resources:lblDate.Text %>"></asp:Label></td>
                                                <td style="width: 300px;">
                                                    <asp:TextBox ID="txtDate" runat="server" MaxLength="11"></asp:TextBox></td>
                                                <td style="width: 400px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblDescription" runat="server" Text="<%$Resources:lblDescription.Text %>"></asp:Label></td>
                                                <td>
                                                    <asp:TextBox ID="txtDescription" runat="server" Width="100%"></asp:TextBox></td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: right;">
                                                    <asp:HiddenField ID="hidId" runat="server" />
                                                    <asp:Button ID="btnDelete" runat="server" Text="<%$Resources:btnDelete.Text %>" OnClick="btnDelete_Click"
                                                        Width="70px" CssClass="NormalButton" BackColor="Red" />
                                                    &nbsp; &nbsp;
                                                    <asp:Button ID="btnSave" runat="server" Text="<%$Resources:btnSave.Text %>" OnClick="btnSave_Click" Width="70px"
                                                        CssClass="NormalButton" />&nbsp;
                                                </td>
                                                <td colspan="1" style="text-align: right">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </center>
                                </asp:Panel>
                            </asp:Panel>
                            <asp:Panel ID="pnlPaste" runat="server" Width="100%">
                                <table border="0">
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkBack" Text="<%$Resources:lnkBack.Text %>" runat="server" OnClick="lnkBack_Click" CssClass="NormalButton"></asp:LinkButton></td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCopyTo" runat="server" CssClass="Normal" Text="<%$Resources:lblCopyTo.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            &nbsp;<asp:DropDownList ID="cmbPasteYear" runat="server">
                                            </asp:DropDownList></td>
                                        <td>
                                            <asp:Button ID="btnPaste" runat="server" OnClick="btnPaste_Click" Text="<%$Resources:btnPaste.Text %>" CssClass="NormalButton" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
