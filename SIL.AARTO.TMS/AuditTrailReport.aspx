<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.AuditTrailReport" Codebehind="AuditTrailReport.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>
<%----%>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img height="26" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        <%----%>
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                                <table id="tblControls" border="0" class="NormalBold" style="width: 554px" 
                                    align="left">
                                    <tr>
                                        <td style="width: 147px">
                                            <asp:Label ID="lblAuthority" runat="server" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" AutoPostBack="True"
                                                Width="197px" OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 3px" align="center">
                                        </td>
                                        <td rowspan="5" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 147px">
                                            <asp:Label ID="lblAuditReport" runat="server" Text="<%$Resources:lblAuditReport.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlAuditReport" runat="server" CssClass="Normal" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlAuditReport_SelectedIndexChanged" Width="197px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 147px">
                                            <asp:Label ID="lblAfter" runat="server" Text="<%$Resources:lblAfter.Text %>"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox runat="server" ID="dtpAfter" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                               />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpAfter" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="dtpAfter" CssClass="NormalRed" Display="Dynamic" 
                                                ErrorMessage="<%$Resources:ReqAfter.ErrorMessage %>"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                ControlToValidate="dtpAfter" CssClass="NormalRed" Display="Dynamic" 
                                                ErrorMessage="<%$Resources:ReqAfter.ErrorMessage %>" 
                                                ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 3px" align="center">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px; width: 147px;">
                                            <asp:Label ID="lblBefore" runat="server" Text="<%$Resources:lblBefore.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 26px">
                                            <asp:TextBox runat="server" ID="dtpBefore" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                               />
                                                        <cc1:CalendarExtender ID="DateCalendar2" runat="server" 
                                                TargetControlID="dtpBefore" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                ControlToValidate="dtpBefore" CssClass="NormalRed" Display="Dynamic" 
                                                ErrorMessage="<%$Resources:ReqAfter.ErrorMessage %>" 
                                                ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                ControlToValidate="dtpBefore" CssClass="NormalRed" Display="Dynamic" 
                                                ErrorMessage="<%$Resources:ReqAfter.ErrorMessage %>"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="width: 3px; height: 26px;" align="center">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px; width: 147px;">
                                            <asp:Label runat="server" ID="lblPartialTicketNo" Width="139px" Text="<%$Resources:lblPartialTicketNo.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 26px">
                                            <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                        </td>
                                        <td style="width: 3px; height: 26px;" align="center">
                                            <asp:Label runat="server" ID="lblPartialOr" Text="<%$Resources:lblPartialOr.Text %>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px; width: 147px;">
                                            <asp:Label runat="server" ID="lblTicketNo" Text="<%$Resources:lblTicketNo.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 26px">
                                            <asp:TextBox Enabled="false" BackColor="#efefef" ID="txtTicketNo" runat="server"
                                                CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 3px; height: 26px;" align="center">
                                            <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblPartialOr.Text %>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px; width: 147px;">
                                            <asp:Label runat="server" ID="lblRegistration" Text="<%$Resources:lblRegistration.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 26px">
                                            <asp:TextBox Enabled="false" BackColor="#efefef" ID="txtRegNo" runat="server" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 3px; height: 26px;" align="center">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: left; ">
                                            <asp:Button ID="btnView" runat="server" CssClass="NormalButton" OnClick="btnView_Click"
                                                Text="<%$Resources:btnView.Text %>" />
                                        </td>
                                        <td style="width: 3px; height: 26px;" align="center">
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" valign="top">
                                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
