using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Stalberg.TMS;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.DAL.Entities;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the main home page of TMS / Opus
    /// </summary>
    public partial class Home : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        protected string styleSheet;
        private string description = string.Empty;
        private string keywords = string.Empty;
        private string thisPageURL = "Home.aspx";
        private string thisPage = "Home";
        public string lastUpdated = "Last Updated: 2014-01-09";
        public string environment = "P"; //P = production; D = Development
        protected string title = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string lastUpdated = "Last updated: " + Application["lastUpdated"].ToString();

            if (Page.IsPostBack)
            {
                //    GetDomainSettings();
                //    SetDomainSettings();
                ShowLogin();
                //    string s = Request.Url.AbsoluteUri;   //="http://localhost:80/TMS/Home.aspx"

            }
            //string AartoWebUrl = ConfigurationManager.AppSettings["AARTODomainURL"] + "Home/Index";
            //Response.Redirect(AartoWebUrl);
        }

        private void SetDomainSettings()
        {
            //set domain specific variables
            General gen = new General();

            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);

            //these have to get specifically set on the home page, as they are not populated earlier
            title = gen.SetTitle(Session["drTitle"]);
            description = gen.SetDescription(Session["drDescription"]);
            keywords = gen.SetKeywords(Session["drKeywords"]);

            //imgLogo.ImageUrl = "~/App_Code/logos/Syntell_logo1.jpg";
            //imgEnter.ImageUrl = "~/images/enter.png";

            GetSystemParameters();
        }

        private void GetSystemParameters()
        {
            SysParamDB sysParam = new SysParamDB(connectionString);

            SqlDataReader reader = sysParam.GetSysParamList();

            ArrayList sysParamList = new ArrayList();


            while (reader.Read())
            {
                SysParamDetails spDetails = new SysParamDetails();

                spDetails.SPIntNo = Convert.ToInt32(reader["SPIntNo"]);
                spDetails.SPColumnName = reader["SPColumnName"].ToString();
                spDetails.SPIntegerValue = Convert.ToInt32(reader["SPIntegerValue"]);
                spDetails.SPStringValue = reader["SPStringValue"].ToString();

                sysParamList.Add(spDetails);

                if (spDetails.SPColumnName.Equals("EnvironmentType"))
                    environment = spDetails.SPStringValue;
            }
            reader.Dispose();

            Application.Lock();
            Application["sysParamList"] = sysParamList;
            Application.UnLock();
        }

        public string GetEnvironmentType()
        {
            return environment;
        }

        private void GetDomainSettings()
        {
            //check rules and login requirements for domain
            DomainRuleDB domainRule = new DomainRuleDB(connectionString);

            //get the calling page URL
            string url = Request.Url.AbsoluteUri.ToString();

            Session["homeUrl"] = url;

            SqlDataReader domainDetails = domainRule.GetDRDetailsByURL(url);

            while (domainDetails.Read())
            {
                Session["drLoginReq"] = domainDetails["DRLoginReq"].ToString();
                Session["drEmailReq"] = domainDetails["DREmailReq"].ToString();
                Session["drPasswdReq"] = domainDetails["DRPasswdReq"].ToString();
                Session["drBackground"] = domainDetails["DRBackground"].ToString();
                Session["drStyleSheet"] = domainDetails["DRStyleSheet"].ToString();
                Session["drTitle"] = domainDetails["DRTitle"].ToString();
                Session["drDescription"] = domainDetails["DRDescription"].ToString();
                Session["drKeywords"] = domainDetails["DRKeywords"].ToString();
                Session["drMainImage"] = domainDetails["DRMainImage"].ToString();
                Session["drAuthorityReq"] = domainDetails["drAuthorityReq"].ToString();
            }
            domainDetails.Close();

        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        //moved to script on aspx page
        protected void ShowLogin()
        {
            //Display login page in new window
            String strRedirectScript;

            string AartoWebUrl = string.Empty;

            string Ip = string.Empty;
            try
            {
                Ip = GetServerIp();
            }
            catch (Exception ex)
            {
               // Application["ErrorMessage"] = String.Format("Error Message:{0}", ex.Message);
                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, "Error", "TMS");
                //Response.Redirect("Error.aspx");
            }
            string HostName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"]; ;

            if (String.IsNullOrEmpty(Ip))
            {
                AartoWebUrl = ConfigurationManager.AppSettings["AARTODomainURL"];
            }
            else if (ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(Ip) >= 0
                || ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(HostName) >= 0)
            {
                AartoWebUrl = ConfigurationManager.AppSettings["AARTODomainURL"] + "/Account/Login";
            }
            else if (ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(Ip) >= 0
               || ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(HostName) >= 0)
            {
                AartoWebUrl = ConfigurationManager.AppSettings["AARTODomainExternalURL"] + "/Account/Login";
            }
            else
            {
                Application["ErrorMessage"] = String.Format((string)GetLocalResourceObject("ApplicationErrorMessage"));
                Response.Redirect("Error.aspx");
            }
            // Generate the JavaScript to pop up a print-only window
            strRedirectScript = "<script language =javascript>";
            if (environment.Equals("D"))
                strRedirectScript += "window.open('" + AartoWebUrl + "','main','toolbar=1 ,scrollbars=1,location=0,statusbar=1,menubar=1,resizable=1,fullscreen=0,left=0,top=0,height=990,width=1270');";
            else
                //strRedirectScript += "window.open('Login.aspx',null,'toolbar=0 ,location=0,statusbar=0,menubar=0,resizable=0,fullscreen=0,left=0,top=0,height=980,width=1270');";
                strRedirectScript += "window.open('" + AartoWebUrl + "','main','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,fullscreen=0,left=0,top=0,height=980,width=1270');";
            strRedirectScript += "</script> ";

            // Add our JavaScript to the currently rendered page
            Response.Write(strRedirectScript);

            //string AartoWebUrl = ConfigurationManager.AppSettings["AARTODomainURL"] + "Account/Login";
            //Response.Redirect(AartoWebUrl);
        }

        private string GetServerIp()
        {
            string serverIP = string.Empty;
            try
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                {
                    serverIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    if (HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] != null)
                    {
                        serverIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                // string msg = ex.Message;
                throw ex;
            }
            return serverIP;
        }

    }
}
