using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
//using BarcodeNETWorkShop;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF.Merger;
using System.Collections.Generic;
using System.Drawing;
using Stalberg.TMS.Data.Datasets;
using Stalberg.TMS.Data.Util;
using SIL.AARTO.BLL.Utility.PrintFile;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents  viewer page for the ChangeRegNo letter
    /// </summary>
    public partial class ChangeRegNo_Viewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string thisPage = "Change Registration No Notice Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "ChangeRegNo_Viewer.aspx";
        protected string loginUser = string.Empty;
        private string printFile = string.Empty;

        // Jake 2011-02-25 Removed BarcodeNETImage
        //BarcodeNETImage barcode = null;
        System.Drawing.Image barCodeImage = null;
        ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phBarCode;
        byte[] bA = null;
        byte[] b = null;

        #region 20120116 Oscar disabled for backup
        ///// <summary>
        ///// Handles the Load event of the Page control.
        ///// </summary>
        ///// <param name="sender">The source of the event.</param>
        ///// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    //get user details
        //    Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
        //    Stalberg.TMS.UserDetails userDetails = new UserDetails();
        //    userDetails = (UserDetails)Session["userDetails"];
        //    autIntNo = Convert.ToInt32(Session["autIntNo"]);
        //    this.loginUser = userDetails.UserLoginName;

        //    // Set domain specific variables
        //    General gen = new General();
        //    backgroundImage = gen.SetBackground(Session["drBackground"]);
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    // Retrieve the Print File name from the Session
        //    if (Request.QueryString["printfile"] == null)
        //    {
        //        Response.Write("No Print File has been supplied!");
        //        Response.End();
        //        return;
        //    }

        //    this.printFile = Request.QueryString["printfile"].ToString().Trim();

        //    string prefix = this.printFile.Substring(0, 3).ToUpper();
        //    string longPrefix = printFile.Substring(0, 7).ToUpper();
        //    string reportPage = string.Empty;
        //    string sTemplate = string.Empty;

        //    // Setup the report
        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);

        //    switch (longPrefix)
        //    {
        //        //at the moment everyone except JMPD uses the same report for RLV as for SPD, e.g. FirstNotice_ST.rpt
        //        case "REG_RLV":
        //        case "REG_SPD":
        //        default:
        //            string function = string.Empty;
        //            string defaultTemplate = string.Empty;

        //            if (printFile.IndexOf("2ND") > 0)
        //            {
        //                function = "ChangeRegNo2ndNotice";
        //                defaultTemplate = "SecondNoticeTemplate_ST.pdf";
        //            }
        //            else
        //            //    if (printFile.IndexOf("RLV") > 0)
        //            //{
        //            //    function = "ChangeRegNoNoticeRLV";
        //            //    defaultTemplate = "FirstNoticeTemplate_ST.pdf";
        //            //}
        //            {
        //                function = "ChangeRegNoNotice";
        //                defaultTemplate = "FirstNoticeTemplate_ST.pdf";
        //            }

        //            reportPage = arn.GetAuthReportName(autIntNo, function);

        //            if (reportPage.Equals(string.Empty))
        //            {
        //                reportPage = function + ".dplx";
        //                arn.AddAuthReportName(autIntNo, reportPage, function, "System", defaultTemplate);
        //            }

        //            sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, function);

        //            break;
        //    }

        //    AuthorityRulesDetails ard = new AuthorityRulesDetails();
        //    ard.AutIntNo = autIntNo;
        //    ard.ARCode = "3200";
        //    ard.LastUser = this.loginUser;

        //    DefaultAuthRules printCrossHairsRule = new DefaultAuthRules(ard, this.connectionString);
        //    KeyValuePair<int, string> showCrossHairsRule = printCrossHairsRule.SetDefaultAuthRule();

        //    bool showCrossHairs = showCrossHairsRule.Value.Equals("Y") ? true : false;

        //    ard = new AuthorityRulesDetails();
        //    ard.AutIntNo = autIntNo;
        //    ard.ARCode = "3150";
        //    ard.LastUser = this.loginUser;

        //    DefaultAuthRules styleCrossHairsRule = new DefaultAuthRules(ard, this.connectionString);
        //    KeyValuePair<int, string> showCrossHairsStyleRule = styleCrossHairsRule.SetDefaultAuthRule();

        //    int crossHairStyle = showCrossHairsStyleRule.Key;

        //    //string reportPage = arn.GetAuthReportName(this.autIntNo, "ChangeRegNoNotice");
        //    //if (reportPage.Equals(string.Empty))
        //    //{
        //    //    //arn.AddAuthReportName(autIntNo, "ChangeRegNoNotice.dplx", "ChangeRegNoNotice", "system");
        //    //    //reportPage = "ChangeRegNoNotice.dplx";

        //    //    arn.AddAuthReportName(autIntNo, "ChangeRegNoNotice.dplx", "ChangeRegNoNotice", "system", "FirstNoticeTemplate_ST.pdf");
        //    //    reportPage = "ChangeRegNoNotice.dplx";
        //    //}

        //    string path = Server.MapPath("reports/" + reportPage);
        //    string sTempEng = string.Empty;
        //    string sTempAfr = string.Empty;

        //    //****************************************************
        //    //SD:  20081120 - check that report actually exists
        //    string templatePath = string.Empty;

        //    if (!File.Exists(path))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        //dls 081117 - we can only check that the template path exists if there is actually a template!
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //****************************************************

        //    DocumentLayout doc = new DocumentLayout(path);
        //    Query query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;
        //    ParameterDictionary parameters = new ParameterDictionary();

        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblId = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblId");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReference = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReference");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReferenceB = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReferenceB");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFormattedNotice = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFormattedNotice");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblForAttention = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblForAttention");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblNoticeNumber = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblNoticeNumber");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPaymentInfo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPaymentInfo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblStatRef = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblStatRef");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblTime = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblTime");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblLocDescr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblLocDescr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDescrEng = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDescrEng");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDescrAfr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDescrAfr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCode = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCode");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCameraNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCamera");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOfficerNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOfficerNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFineAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAmount");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPrintDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPrintDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceTime = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceTime");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAut = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblText");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblLocation = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblLocation");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSpeedLimit = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblSpeedLimit");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSpeed = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblSpeed");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOfficer = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOfficer");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRegNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRegNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblIssuedBy = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblIssuedBy");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPaymentDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPaymentDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCourtName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCourtName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblEasyPay");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFilmNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFilmNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFrameNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFrameNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblVehicle = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblVehicle");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAuthorityAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAuthorityAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutTel = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAutTel");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutFax = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAutFax");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblDisclaimer = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblDisclaimer");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReprintDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReprintDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReprintDateText = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReprintDateText");

        //    phBarCode = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phBarCode");
        //    phBarCode.LaidOut += new PlaceHolderLaidOutEventHandler(ph_BarCode);
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phImage = null;
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phImageA = null;

        //    // only do this is its a noaog or first notice
        //    if (reportPage.IndexOf("ChangeRegNo2nd") < 0)
        //    {
        //        phImage = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phImage");
        //        phImage.LaidOut += new PlaceHolderLaidOutEventHandler(ph_Image);
        //    }
        //    if (reportPage.IndexOf("_RLV") >= 0)
        //    {
        //        phImageA = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phImageA");
        //        phImageA.LaidOut += new PlaceHolderLaidOutEventHandler(ph_ImageA);
        //    }

        //    //get additional parameters for date rules and sysparam setting
        //    SysParamDB sp = new SysParamDB(this.connectionString);

        //    string violationCutOff = "N";
        //    int spValue = 0;

        //    bool found = sp.CheckSysParam("ViolationCutOff", ref spValue, ref violationCutOff);

        //    DateRulesDetails dateRule = new DateRulesDetails();
        //    dateRule.AutIntNo = autIntNo;
        //    dateRule.DtRStartDate = "NotOffenceDate";
        //    dateRule.DtREndDate = "NotIssue1stNoticeDate";
        //    dateRule.LastUser = this.loginUser;

        //    DefaultDateRules rule = new DefaultDateRules(dateRule, this.connectionString);
        //    int noOfDaysIssue = rule.SetDefaultDateRule();

        //    SqlConnection con = new SqlConnection(this.connectionString);
        //    SqlDataReader result = null;
        //    SqlCommand com = null;

        //    com = new SqlCommand("NoticePrint", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.CommandTimeout = 0;
        //    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = this.autIntNo;
        //    com.Parameters.Add("@PrintFile", SqlDbType.VarChar, 50).Value = Request.QueryString["printfile"].ToString();
        //    com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = 10;
        //    com.Parameters.Add("@ShowAll", SqlDbType.Char, 1).Value = "N";
        //    com.Parameters.Add("@Format", SqlDbType.VarChar, 10).Value = "TMS";
        //    com.Parameters.Add("@Option", SqlDbType.Int, 4).Value = 1;
        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = loginUser;
        //    com.Parameters.Add("@ViolationCutOff", SqlDbType.Char, 1).Value = violationCutOff;
        //    com.Parameters.Add("@NoOfDaysIssue ", SqlDbType.Int, 4).Value = noOfDaysIssue;

        //    //get data and populate resultset
        //    try
        //    {
        //        con.Open();
        //        result = com.ExecuteReader(CommandBehavior.CloseConnection);
        //    }
        //    catch (Exception ee)
        //    {
        //        string error = "Error: " + ee.Message;
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }

        //    try
        //    {
        //        bool ignore = false;

        //        MergeDocument merge = new MergeDocument();
        //        byte[] buffer;
        //        Document report = null;

        //        //dls 080325 - need to check for additional resultset because of nested call to newly added Notice Reprint
        //        while (!ignore)
        //        {
        //            while (result.Read())
        //            {
        //                try
        //                {
        //                    Int32 nValue = 0;

        //                    if (Int32.TryParse(result[0].ToString(), out nValue))
        //                    {
        //                        //ignore this result - it is returned from one of the nested stored procs
        //                        ignore = true;
        //                    }
        //                    else if (result[0] == System.DBNull.Value)
        //                    {
        //                        //ignore this result - it is returned from one of the nested stored procs
        //                        ignore = true;
        //                    }
        //                    else
        //                    {
        //                        ignore = false;
        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");

        //                        lblStatRef.Text = (result["ChgStatRefLine1"] == DBNull.Value ? " " : result["ChgStatRefLine1"].ToString().Trim())
        //                                    + (result["ChgStatRefLine2"] == DBNull.Value ? " " : result["ChgStatRefLine2"].ToString().Trim())
        //                                    + (result["ChgStatRefLine3"] == DBNull.Value ? " " : result["ChgStatRefLine3"].ToString().Trim())
        //                                    + (result["ChgStatRefLine4"] == DBNull.Value ? " " : result["ChgStatRefLine4"].ToString().Trim());

        //                        //dls 071221 - added reprint date
        //                        if (Convert.ToInt16(result["ChargeStatus"]) >= 250)
        //                        {
        //                            lblReprintDateText.Text = "Reprint date:";
        //                            lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);
        //                        }

        //                        if (reportPage.IndexOf("_RLV") < 0)
        //                        {
        //                            lblEasyPay.Text = result["NotEasyPayNumber"].ToString().Trim();
        //                            lblFilmNo.Text = result["NotFilmNo"].ToString().Trim();
        //                            lblFrameNo.Text = result["NotFrameNo"].ToString().Trim();
        //                            lblVehicle.Text = result["NotVehicleMake"].ToString().Trim();

        //                            lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPaymentDate"]);
        //                            lblCourtName.Text = result["NotCourtName"].ToString().ToUpper();
        //                            lblCameraNo.Text = result["NotCamSerialNo"].ToString();
        //                            lblOfficerNo.Text = result["NotOfficerNo"].ToString();
        //                            //lblFineAmount.Text = "R " + (result["NoticeOption"].ToString().Equals("1") ? result["ChgFineAmount"].ToString() : result["ChgRevFineAmount"].ToString());
        //                            lblFineAmount.Text = "R " + string.Format("{0:0.00}", Convert.ToDecimal(result["ChgRevFineAmount"]));  //result["ChgRevFineAmount"].ToString();
        //                            lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint1stNoticeDate"]);
        //                            lblOffenceDate.Text = string.Format("{0:yyyy-MM-dd}", result["NotOffenceDate"]);
        //                            lblOffenceTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                            lblLocation.Text = result["NotLocDescr"].ToString();
        //                            lblSpeedLimit.Text = result["NotSpeedLimit"].ToString();
        //                            lblSpeed.Text = result["NotSpeed1"].ToString();
        //                            lblOfficer.Text = result["NotOfficerNo"].ToString();            //result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();
        //                            lblRegNo.Text = result["NotRegNo"].ToString();
        //                            lblDate.Text = string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]);
        //                            lblTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                            lblLocDescr.Text = result["NotLocDescr"].ToString();
        //                            sTempEng = result["OcTDescr"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());
        //                            sTempAfr = result["OcTDescr1"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());
        //                            if (result["NotNewOffender"].ToString().Equals("Y"))
        //                            {
        //                                lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
        //                                lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
        //                            }
        //                            else
        //                            {
        //                                lblOffenceDescrEng.Text = sTempEng;
        //                                lblOffenceDescrAfr.Text = sTempAfr;
        //                            }

        //                        }
        //                        else
        //                        {
        //                            lblAutTel.Text = result["AutTel"].ToString();
        //                            lblAutFax.Text = result["AutFax"].ToString();
        //                            sTempEng = result["OcTDescr"].ToString().Replace("*001   ", result["OrigRegNo"].ToString());
        //                            sTempAfr = result["OcTDescr1"].ToString().Replace("*001   ", result["OrigRegNo"].ToString());
        //                            lblOffenceDescrAfr.Text = "DEUR DAT die bestuurder van voertuig " + result["NotVehicleMake"].ToString().Trim() +
        //                            " met registrasienommer " + result["NotRegNo"].ToString().Trim() + " op " + string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]) +
        //                            " om " + string.Format("{0:HH:mm}", result["NotOffenceDate"]) + " en te \n" +
        //                            result["NotLocDescr"].ToString() + "\n" + sTempAfr;
        //                            lblOffenceDescrEng.Text = "IN THAT the driver of motor vehicle " + result["NotVehicleMake"].ToString().Trim() +
        //                            " with registration number " + result["NotRegNo"].ToString().Trim() + " on " + string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]) +
        //                            " at " + string.Format("{0:HH:mm}", result["NotOffenceDate"]) + " and at \n" +
        //                            result["NotLocDescr"].ToString() + "\n" + sTempAfr;
        //                            lblFormattedNotice.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                            lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint1stNoticeDate"]);
        //                            //  lblAut.Text = result["AutName"].ToString().ToUpper() + "\n"
        //                            //+ result["AutPostAddr1"].ToString().ToUpper() + "\n"
        //                            //+ result["AutPostAddr2"].ToString().ToUpper() + "\n"
        //                            //+ result["AutPostAddr3"].ToString().ToUpper() + "\n"
        //                            //+ result["AutPostCode"].ToString().ToUpper() + "\n";
        //                        }

        //                        if (reportPage.ToLower().IndexOf("2nd") < 0)
        //                        {
        //                            lblAut.Text = result["AutNoticeIssuedByInfo"].ToString();
        //                        }

        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                            lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                        else
        //                            lblId.Text = result["DrvIDNUmber"].ToString().Trim();

        //                        lblDisclaimer.Text = result["Disclaimer"].ToString();

        //                        // Jake 2011-02-25 Removed BarcodeNETImage
        //                        //barcode = new BarcodeNETImage();
        //                        //barcode.BarcodeText = result["NotTicketNo"].ToString();
        //                        //barcode.ShowBarcodeText = false;
        //                        barCodeImage = Code128Rendering.MakeBarcodeImage(result["NotTicketNo"].ToString(), 1, 25, true);

        //                        lblCode.Text = result["ChgOffenceCode"].ToString();

        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                            lblForAttention.Text = result["PrxInitials"].ToString() + " " + result["PrxSurname"].ToString() + " as Representative of " + result["DrvSurname"].ToString();
        //                        else
        //                            lblForAttention.Text = result["DrvInitials"].ToString() + " " + result["DrvSurname"].ToString();

        //                        lblAddress.Text = (result["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd1"].ToString().Trim()) + "\n")
        //                                + (result["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd2"].ToString().Trim()) + "\n")
        //                                + (result["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd3"].ToString().Trim()) + "\n")
        //                                + (result["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd4"].ToString().Trim()) + "\n")
        //                                + (result["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd5"].ToString().Trim()) + "\n")
        //                                + result["DrvPOCode"].ToString().Trim();

        //                        lblIssuedBy.Text = result["AutNoticeIssuedByInfo"].ToString();

        //                        //if (reportPage.IndexOf("_ST") < 0)
        //                        //{
        //                        //    lblAut.Text = result["AutName"].ToString().ToUpper() + "\n"
        //                        //    + result["AutPostAddr1"].ToString().ToUpper() + "\n"
        //                        //    + result["AutPostAddr2"].ToString().ToUpper() + "\n"
        //                        //    + result["AutPostAddr3"].ToString().ToUpper() + "\n"
        //                        //    + result["AutPostCode"].ToString().ToUpper() + "\n";
        //                        //}

        //                        //if (result["ScanImage1"] != DBNull.Value)
        //                        //    b = (byte[])result["ScanImage1"];

        //                        //b = result["ScanImage1"] == System.DBNull.Value ? null : (byte[])result["ScanImage1"];

        //                        ScanImageDB imgDB = new ScanImageDB(this.connectionString);

        //                        if (result["ScanImage1"] != System.DBNull.Value)
        //                        {
        //                            ScanImageDetails image1 = imgDB.GetImageFullPath(Convert.ToInt32(result["ScanImage1"]));
        //                            b = GetImagesFromRemoteFileServer(image1);
        //                        }

        //                        // add cross-hair processing
        //                        int X = Convert.ToInt32(result["XValue1"]);
        //                        int Y = Convert.ToInt32(result["YValue1"]);

        //                        if (b != null && showCrossHairs && X > 0 && Y > 0)
        //                        {
        //                            Drawing draw = new Drawing();
        //                            System.Drawing.Image image = draw.CrossHairs(b, crossHairStyle.ToString(), X, Y);
        //                            MemoryStream ms = new MemoryStream();
        //                            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //                            b = ms.ToArray();
        //                        }

        //                        if (result["ScanImage2"] != DBNull.Value)
        //                        //bA = (byte[])result["ScanImage2"];
        //                        {
        //                            ScanImageDetails image2 = imgDB.GetImageFullPath(Convert.ToInt32(result["ScanImage3"]));
        //                            bA = GetImagesFromRemoteFileServer(image2);
        //                        }

        //                        report = doc.Run(parameters);

        //                        ImportedPageArea importedPage;
        //                        byte[] bufferTemplate;
        //                        if (!sTemplate.Equals(""))
        //                        {
        //                            if (sTemplate.ToLower().IndexOf(".dplx") > 0)
        //                            {
        //                                DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
        //                                Query queryTemplate = (Query)template.GetQueryById("Query");
        //                                queryTemplate.ConnectionString = this.connectionString;
        //                                ParameterDictionary parametersTemplate = new ParameterDictionary();
        //                                Document reportTemplate = template.Run(parametersTemplate);
        //                                bufferTemplate = reportTemplate.Draw();
        //                                PdfDocument pdf = new PdfDocument(bufferTemplate);
        //                                PdfPage page = pdf.Pages[0];
        //                                importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
        //                            }
        //                            else
        //                            {
        //                                //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                                importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                            }

        //                            ceTe.DynamicPDF.Page rptPage = report.Pages[0];
        //                            rptPage.Elements.Insert(0, importedPage);
        //                        }
        //                        buffer = report.Draw();
        //                        merge.Append(new PdfDocument(buffer));
        //                        buffer = null;
        //                        bufferTemplate = null;
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    String sError = ex.Message;
        //                }
        //            }

        //            // if there is another resultset go through the outer loop again 
        //            if (ignore && result.NextResult())
        //                ignore = false;
        //            else
        //                ignore = true;

        //        }

        //        result.Close();
        //        con.Dispose();


        //        byte[] buf = merge.Draw();

        //        phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);
        //        if (reportPage.IndexOf("ChangeRegNo2nd") < 0)
        //        {
        //            phImage.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_Image);
        //        }
        //        if (reportPage.IndexOf("_RLV") >= 0)
        //        {
        //            phImageA.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_ImageA);
        //        }

        //        Response.ClearContent();
        //        Response.ClearHeaders();
        //        Response.ContentType = "application/pdf";
        //        Response.BinaryWrite(buf);
        //        Response.End();
        //        buf = null;
        //    }
        //    //this is here for threading purposes - do not remove!
        //    catch (Exception ex)
        //    {
        //        string error = ex.Message;
        //    }
        //    finally
        //    {
        //        GC.Collect();
        //    }
        //}

        //public byte[] GetImagesFromRemoteFileServer(ScanImageDetails image)
        //{
        //    byte[] data = null;
        //    if (image.FileServer != null)
        //    {
        //        //WebService service = new WebService();
        //        //if (service.RemoteConnect(image.FileServer.ImageMachineName,
        //        //    image.FileServer.ImageShareName,
        //        //    image.FileServer.RemoteUserName,
        //        //    image.FileServer.RemotePassword))
        //        //{
        //        using (FileStream fs = File.OpenRead(image.ImageFullPath))
        //        {
        //            data = new byte[fs.Length];
        //            fs.Read(data, 0, data.Length);
        //        }
        //        //}
        //    }
        //    return data;
        //}

        ////Jake 2011-02-25 Removed BarCodeNetImage
        //public void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        //{
        //    //ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(barcode.GetBarcodeBitmap(FILE_FORMAT.JPG)), 0, 0);
        //    //img.Height = 25.0F;
        //    //e.ContentArea.Add(img);
        //    if (barCodeImage != null)
        //    {
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //            ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
        //            img.Height = 25.0F;
        //            e.ContentArea.Add(img);
        //        }

        //        int generation = System.GC.GetGeneration(barCodeImage);
        //        barCodeImage = null;
        //        System.GC.Collect(generation);
        //    }
        //}

        //void ph_Image(object sender, PlaceHolderLaidOutEventArgs e)
        //{
        //    if (b != null)
        //    {
        //        ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(b), 0, 0);
        //        img.Height = 160.0F;
        //        img.Width = 210.0F;
        //        e.ContentArea.Add(img);

        //        int generation = System.GC.GetGeneration(b);
        //        b = null;
        //        System.GC.Collect(generation);
        //    }
        //}

        //void ph_ImageA(object sender, PlaceHolderLaidOutEventArgs e)
        //{
        //    if (bA != null)
        //    {
        //        ceTe.DynamicPDF.PageElements.Image imgA = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(bA), 0, 0);
        //        imgA.Height = 160.0F;
        //        imgA.Width = 210.0F;
        //        e.ContentArea.Add(imgA);

        //        int generation = System.GC.GetGeneration(bA);
        //        bA = null;
        //        System.GC.Collect(generation);
        //    }
        //}
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            //Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            this.loginUser = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            this.backgroundImage = gen.SetBackground(Session["drBackground"]);
            this.styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            this.title = gen.SetTitle(Session["drTitle"]);

            // use module to create print pdf file, Oscar 20120119
            PrintFileProcess process = new PrintFileProcess(this.connectionString, this.loginUser);
            process.BuildPrintFile(new PrintFileModuleChangeRegNo(), this.autIntNo, Request.QueryString["printfile"]);
        }

    }

}