using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a control that provides a simplified ticket number search function
    /// </summary>
    public partial class TicketNumberSearch : UserControl
    {
        private string connectionString = String.Empty;
        private string login;
        private string sTicketNo = String.Empty;
        private int nAutIntNo;
        private string ticketProcessor;// jerry 2011-1-12 add
        private int processorSign;// jerry 2011-1-12 add

        public event EventHandler NoticeSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            if (this.ViewState["TicketNumberSearch_AutIntNo"] != null)
            {
                this.nAutIntNo = (int)this.ViewState["TicketNumberSearch_AutIntNo"];
            }

            //2013-06-25 added by Nancy start
            if (nAutIntNo>0 && string.IsNullOrEmpty(ticketProcessor))
            {
                AuthorityDB db = new AuthorityDB(this.connectionString);
                AuthorityDetails ad = db.GetAuthorityDetails(nAutIntNo);
                ticketProcessor = ad.AutTicketProcessor;
            }
            //2013-06-25 added by Nancy end

            // jerry 2011-1-12 add
            if (string.IsNullOrEmpty(ticketProcessor))//2013-06-25 added by Nancy
                ticketProcessor = Session["TicketProcessor"] != null ? Session["TicketProcessor"].ToString() : "TMS";          
            //if (ticketProcessor.Equals("TMS") || ticketProcessor.Equals("Cip_CofCT") || ticketProcessor.Equals("CiprusPI"))2012-12-24 updated by Nancy
            if (ticketProcessor.Equals("TMS"))
            {
                processorSign = 1;
            }
            else
            {
                processorSign = 2;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.lblError.Text = string.Empty;
            char cTemp = Convert.ToChar("0");
            int nCdv;
            int nSeq;
            String sNoticeNumber = string.Empty;

            NoticeDB ndb = new NoticeDB(connectionString);
            SqlDataReader reader = null;

            if (this.txtNoticePrefix.Text.Trim().Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                this.txtNoticePrefix.Focus();
                return;
            }

            if (processorSign == 1)//processor is TMS
            {
                // TMS Ticket Processors
                // Order of text boxes:
                //  1.	1st text box = Notice Prefix/Document Type ? blank, user must enter (might be hidden if AuthRule 4905 Rule to hide prefix in Ticket Search = �Y?
                //  2.	2nd text box = Sequence no ? blank, user must enter
                //  3.	3rd text box = Authority.AutNo for current Authority (pre-populate)

                if (txtNumber.Text.Trim().Length == 0)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text1"), ticketProcessor);
                    txtNumber.Focus();
                    return;
                }

                if (this.txtAuthCode.Text.Trim().Length == 0)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), ticketProcessor);
                    txtAuthCode.Focus();
                    return;
                }

                //Jerry 2012-08-21 add
                AuthorityRulesDetails rule9300 = new AuthorityRulesDetails();
                rule9300.AutIntNo = nAutIntNo;
                rule9300.ARCode = "9300";
                rule9300.LastUser = login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule9300, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
                txtNumber.Text = txtNumber.Text.PadLeft(value.Key, cTemp);

                if (!int.TryParse(txtNumber.Text.Trim(), out nSeq))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    txtNumber.Focus();
                    return;
                }

                nCdv = SIL.AARTO.BLL.Utility.TicketNumber.GetCdvOfTicketNumber(nSeq, txtNoticePrefix.Text.Trim(), txtAuthCode.Text.Trim());
                if (nCdv == -1)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                    txtNoticePrefix.Focus();
                    return;
                }

                this.txtCDV.Text = nCdv.ToString();

                sNoticeNumber = txtNoticePrefix.Text.Trim() + "/" + txtNumber.Text.Trim() + "/" + txtAuthCode.Text.Trim() + "/" + this.txtCDV.Text;

                reader = ndb.TicketNumberSearch(processorSign, nSeq, txtAuthCode.Text.Trim(), txtNoticePrefix.Text.Trim(), 0);

            }
            else// processor is JMPD
            {
                // PLEASE NOTE: for the AARTO Ticket Processor
                //	Order of text boxes is different!!!!
                //  1.	1st text box = Document type ? blank, user must enter (always show)
                //  2.	2nd text box = Authority.ENatisAuthorityNumber for current Authority (pre-populate)
                //  3.	3rd text box = sequence no ? blank, user must enter
                //	Instead of AutNo, we need to find ENatisAuthorityNumber for current Authority

                if (txtNoticePrefix.Text.Trim().Length == 0)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), ticketProcessor);
                    txtNumber.Focus();
                    return;
                }

                if (txtNumber.Text.Trim().Length == 0)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text6"), ticketProcessor);
                    txtNumber.Focus();
                    return;
                }

                if (this.txtAuthCode.Text.Trim().Length == 0)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text1"), ticketProcessor);
                    txtAuthCode.Focus();
                    return;
                }

                txtAuthCode.Text = txtAuthCode.Text.PadLeft(9, cTemp);

                if (!int.TryParse(txtAuthCode.Text.Trim(), out nSeq))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    txtAuthCode.Focus();
                    return;
                }

                sNoticeNumber = txtNoticePrefix.Text.Trim() + "-" + txtNumber.Text.Trim() + "-" + txtAuthCode.Text.Trim() + "-";
                //sNoticeNumber = txtNoticePrefix.Text.Trim() + "-" + txtAuthCode.Text.Trim() + "-" + txtNumber.Text.Trim() + "-";//2012-12-24 updated by Nancy(JMPD number format = prefix-authority-sequence-CDV)
                string tmpStr = sNoticeNumber.Replace("-", "");
                nCdv = Verhoeff.CalculateCheckDigit(tmpStr);
                this.txtCDV.Text = nCdv.ToString();
                sNoticeNumber += this.txtCDV.Text;

                reader = ndb.TicketNumberSearch(processorSign, nSeq, "", txtNoticePrefix.Text.Trim(), Convert.ToInt32(txtNumber.Text.Trim()));

            }

            // Jake 20120208 added for m
            Session["MinimalCapture_TicketNo"] = sNoticeNumber;

            this.grdHeader.DataSource = reader;
            this.grdHeader.DataBind();
            if (grdHeader.Rows.Count == 1)
            {
                grdHeader.SelectedIndex = 0;
                this.sTicketNo = grdHeader.SelectedRow.Cells[0].Text;
                if (this.NoticeSelected != null)
                {
                    this.NoticeSelected(this.sTicketNo, EventArgs.Empty);
                }
                //Jerry 2013-10-21 change
                //this.pnlGrid.Visible = false;
                this.pnlGrid.Visible = true;
                this.lblError.Visible = false;
            }
            else if (grdHeader.Rows.Count > 1)
            {
                this.pnlGrid.Visible = true;
                this.lblError.Visible = false;
            }
            else
            {
                this.pnlGrid.Visible = false;
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text7"), sNoticeNumber);
                this.lblError.Visible = true;
                this.txtNumber.Text = string.Empty;
                this.NoticeSelected(string.Empty, EventArgs.Empty);
            }
        }

        protected void grdHeader_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            grdHeader.SelectedIndex = e.NewSelectedIndex;
            this.sTicketNo = grdHeader.SelectedRow.Cells[0].Text;
            if (this.NoticeSelected != null)
            {
                this.NoticeSelected(this.sTicketNo, EventArgs.Empty);
            }
            this.pnlGrid.Visible = false;
        }

        public string TicketNumber
        {
            get { return sTicketNo; }
        }

        public int AutIntNo
        {
            get { return nAutIntNo; }
            set
            {
                this.nAutIntNo = value;
                this.ViewState.Add("TicketNumberSearch_AutIntNo", value);
                //2013-06-25 added by Nancy start
                this.connectionString = Application["constr"].ToString();
                AuthorityDB db = new AuthorityDB(this.connectionString);
                AuthorityDetails ad = db.GetAuthorityDetails(value);
                ticketProcessor = ad.AutTicketProcessor;
                //2013-06-25 added by Nancy end
                DataBind();
            }
        }

        public override void DataBind()
        {
            this.connectionString = Application["constr"].ToString();

            // jerry 2011-1-12 add
            if (string.IsNullOrEmpty(ticketProcessor))//2013-06-25 added by Nancy
                ticketProcessor = Session["TicketProcessor"] != null ? Session["TicketProcessor"].ToString() : "TMS";
            //if (ticketProcessor.Equals("TMS") || ticketProcessor.Equals("Cip_CofCT") || ticketProcessor.Equals("CiprusPI"))
            if (ticketProcessor.Equals("TMS")) //2013-06-25 updated by Nancy
            {
                processorSign = 1;
            }
            else
            {
                processorSign = 2;
            }

            //ticketProcessor = "TMS"; 2013-06-25 removed by Nancy
            //processorSign = 1;

            //if (!this.Page.IsPostBack)
            //{
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            AuthorityDB db = new AuthorityDB(this.connectionString);
            AuthorityDetails ad = db.GetAuthorityDetails(nAutIntNo);
            if (processorSign == 1)// jerry 2011-1-12 add, processor is TMS
            {
                this.txtAuthCode.Text = ad.AutNo;
            }
            else// processor is AARTO
            {
                this.txtNumber.Text = ad.ENatisAuthorityNumber.ToString();
            }
            
            NoticeTypeDB noticeType = new NoticeTypeDB(this.connectionString);
            Stalberg.TMS.NoticeTypeDetails ntDetails = noticeType.GetNoticeTypeDetailsByCode("6");
            NoticePrefixDB noticePrefix = new NoticePrefixDB(connectionString);
            
            //AuthorityRulesDB dbar = new AuthorityRulesDB(this.connectionString);
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4905";
                //rule.ARDescr = "Rule to hide prefix in Ticket Search";
                //rule.ARComment = "N = No (Default), Y = Yes";
                //rule.ARNumeric = 0;
                //rule.ARString = "N";
                //rule.AutIntNo = nAutIntNo;
                //rule.LastUser = login;
                //dbar.GetDefaultRule(rule);
            
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = nAutIntNo;
            rule.ARCode = "4905";
            rule.LastUser = login;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();


            Stalberg.TMS.NoticePrefixDetails npDetails = noticePrefix.GetNoticePrefixDetailsByType(ntDetails.NTIntNo, nAutIntNo);

            if (value.Value == "N") //rule.ARString
                this.txtNoticePrefix.Text = !string.IsNullOrEmpty(npDetails.NPrefix) ? npDetails.NPrefix : "";
            //}

            this.txtNoticePrefix.Focus();
        }

    }
}
