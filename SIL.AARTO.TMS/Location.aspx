<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>


<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Location" CodeBehind="Location.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
                    <tr>
                        <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                            <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" border="0" height="85%">
                    <tr>
                        <td align="center" valign="top">
                            <img style="height: 1px" src="images/1x1.gif" width="167">
                            <asp:Panel ID="pnlMainMenu" runat="server">
                            </asp:Panel>
                            <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                                BorderColor="#000000">
                                <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                                OnClick="btnOptAdd_Click"></asp:Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                                Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                                Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" height="21"></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        <td valign="top" align="left" width="100%" colspan="1">
                            <table border="0" width="568" height="482">
                                <tr>
                                    <td valign="top" height="47">
                                        <p align="center">
                                            <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                        </p>
                                        <p>
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:Panel ID="pnlGeneral" runat="server">
                                            <table id="Table4" cellspacing="1" cellpadding="1" width="300" border="0">
                                                <tr>
                                                    <td width="162"></td>
                                                    <td width="7"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="162">
                                                        <asp:Label ID="lblSelAuthority" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="7">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" Width="217px" CssClass="Normal"
                                                            AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="162">
                                                        <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="7">
                                                        <asp:TextBox ID="txtSearch" runat="server" Width="169px" CssClass="Normal" MaxLength="20"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                            OnClick="btnSearch_Click"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:DataGrid ID="dgLocation" Width="595px" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                            Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgLocation_ItemCommand">
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn Visible="False" DataField="LocIntNo" HeaderText="LocIntNo"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="LocCameraCode" HeaderText="<%$Resources:dgLocation.HeaderText1 %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="LocDescr" HeaderText="<%$Resources:dgLocation.HeaderText2 %>"></asp:BoundColumn>
                                                <asp:ButtonColumn Text="<%$Resources:dgLocationItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <pager:AspNetPager ID="dgLocationPager" runat="server"
                                            ShowCustomInfoSection="Right" Width="595px"
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                              FirstPageText="|&amp;lt;"
                                            LastPageText="&amp;gt;|"
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                            CustomInfoStyle="float:right;" OnPageChanged="dgLocationPager_PageChanged" UpdatePanelId="UpdatePanel1"
                                             >
                                        </pager:AspNetPager>

                                        <asp:Panel ID="pnlAddLocation" runat="server" Height="127px">
                                            <table id="Table2" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                                <tr>
                                                    <td width="157" height="2">
                                                        <asp:Label ID="lblAddLocation" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddLocation.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248" height="2"></td>
                                                    <td height="2"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="157" height="25">
                                                        <asp:Label ID="lblAddLocCameraCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddLocCameraCode.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="248" height="25">
                                                        <asp:TextBox ID="txtAddLocCameraCode" runat="server" Width="145px" CssClass="NormalMand"
                                                            MaxLength="12" Height="24px"></asp:TextBox>
                                                    </td>
                                                    <td height="25">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="210px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ControlToValidate="txtAddLocCameraCode"
                                                            ErrorMessage="<%$Resources:ReqAddLocCameraCode.Text %>"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="157" height="25">
                                                        <p>
                                                            <asp:Label ID="lblAddLocCode" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddLocCode.Text %>"></asp:Label>
                                                        </p>
                                                    </td>
                                                    <td width="248" height="25">
                                                        <asp:TextBox ID="txtAddLocCode" runat="server" Width="135px" CssClass="Normal" MaxLength="21"></asp:TextBox>
                                                    </td>
                                                    <td height="25"></td>
                                                </tr>
                                                <tr>
                                                    <td width="157" valign="top">
                                                        <p>
                                                            <asp:Label ID="lblAddTNDescr" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddTNDescr.Text %>"></asp:Label>
                                                        </p>
                                                    </td>
                                                    <td width="248">
                                                        <asp:TextBox ID="txtAddLocDescr" runat="server" Width="100%" CssClass="Normal" MaxLength="150"
                                                            TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Width="210px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ControlToValidate="txtAddLocDescr"
                                                            ErrorMessage="<%$Resources:ReqAddLocDescr.ErrorMessage %>"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                            <tr bgcolor='#FFFFFF'>
                                                                <td height="100">
                                                                    <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                                <td height="100">
                                                                    <uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td height="2"></td>
                                                </tr>
                                                <tr>
                                                    <td width="157">
                                                        <asp:Label ID="Label2" runat="server" Width="133px" CssClass="NormalBold" Text="<%$Resources:lblAddLocStreetCode.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:TextBox ID="txtAddLocStreetCode" runat="server" Width="88px" CssClass="NormalMand"
                                                            MaxLength="5" Height="24px"></asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="157">
                                                        <asp:Label ID="Label3" runat="server" Width="147px" CssClass="NormalBold" Text="<%$Resources:lblAddLocStreetName.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:TextBox ID="txtAddLocStreetName" runat="server" Width="261px" CssClass="NormalMand"
                                                            MaxLength="30" Height="24px"></asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="labelStreetA" CssClass="NormalBold" Text="<%$Resources:labelStreetA.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textStreetA" Height="22px" Width="261px" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label20" CssClass="NormalBold" Text="<%$Resources:lblStreetB.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textStreetB" Width="261px" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label21" CssClass="NormalBold" Text="<%$Resources:lblSuburb.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <%-- <asp:TextBox runat="server" ID="textSuburb" CssClass="Normal"/>--%>
                                                        <asp:DropDownList ID="ddlAddAuth_LocationSuburb" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label22" CssClass="NormalBold" Text="<%$Resources:lblCity.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textCity" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label23" CssClass="NormalBold" Text="<%$Resources:lblProvince.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="comboProvince" Width="261px" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label24" CssClass="NormalBold" Text="<%$Resources:lblRoute.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textRoute" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label25" CssClass="NormalBold" Text="<%$Resources:lblFrom.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textFrom" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label26" CssClass="NormalBold" Text="<%$Resources:lblTo.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textTo" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label27" CssClass="NormalBold" Text="<%$Resources:lblGpsX.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textGpsX" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label28" CssClass="NormalBold" Text="<%$Resources:GpsY.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textGpsY" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="157">
                                                        <asp:Label ID="Label4" runat="server" Width="135px" CssClass="NormalBold" Text="<%$Resources:lblAddTravelDirection.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlAddTravelDirection" runat="server" Width="109px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="157" height="29">
                                                        <asp:Label ID="Label5" runat="server" Width="192px" CssClass="NormalBold" Text="<%$Resources:lblAddLocOffenceSpeedStart.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="248" height="29">
                                                        <asp:TextBox ID="txtAddLocOffenceSpeedStart" runat="server" Width="43px" CssClass="NormalMand"
                                                            MaxLength="3" Height="24px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Width="199px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ControlToValidate="txtAddLocOffenceSpeedStart"
                                                            ErrorMessage="<%$Resources:ReqAddLocOffenceSpeedStart.ErrorMessage %>"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td height="29">
                                                        <asp:RangeValidator ID="RangeValidator2" runat="server" CssClass="NormalRed" ForeColor=" "
                                                            ControlToValidate="txtAddLocOffenceSpeedStart" ErrorMessage="<%$Resources:RanAddLocOffenceSpeedStart.ErrorMessage %>"
                                                            MaximumValue="200" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="157" height="15">
                                                        <asp:Label ID="Label6" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddRoadType.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248" height="15">
                                                        <asp:DropDownList ID="ddlAddRoadType" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td height="15"></td>
                                                </tr>
                                                <tr>
                                                    <td width="157">
                                                        <asp:Label ID="Label7" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddSpeedZone.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlAddSpeedZone" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="157">
                                                        <asp:Label ID="Label8" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddAuth_Court.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlAddAuth_Court" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label40" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddLocBranchCode.Text %>"></asp:Label></td>
                                                    <td width="248">
                                                        <asp:TextBox runat="server" ID="txtAddLocBranchCode" MaxLength="2"
                                                            ReadOnly="True" Width="35px" CssClass="Normal" /></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label41" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddLocType.Text %>"></asp:Label></td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlAddLocType" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label42" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddLocRegion.Text %>"></asp:Label></td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlAddLocRegion" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label46" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblRailwayCrossing.Text %>"></asp:Label></td>
                                                    <td width="248">
                                                       <asp:CheckBox ID="chkAddRailwayCrossing" runat="server" CssClass="NormalBold"  Text="" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="157"></td>
                                                    <td width="248"></td>
                                                    <td>
                                                        <asp:Button ID="btnAddLocation" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddLocation.Text %>"
                                                            OnClick="btnAddLocation_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlUpdateLocation" runat="server" Height="127px">
                                            <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                                <tr>
                                                    <td width="279" height="2">
                                                        <asp:Label ID="Label19" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblUpdLocation.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248" height="2"></td>
                                                    <td height="2"></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="279" height="25">
                                                        <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddLocCameraCode.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="248" height="25">
                                                        <asp:TextBox ID="txtLocCameraCode" runat="server" Width="134px" CssClass="NormalMand"
                                                            MaxLength="12" Height="24px"></asp:TextBox>
                                                    </td>
                                                    <td height="25">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ControlToValidate="txtLocCameraCode"
                                                            ErrorMessage="<%$Resources:ReqAddLocCameraCode.Text %>"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="279" height="25">
                                                        <p>
                                                            <asp:Label ID="Label17" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddLocCode.Text %>"></asp:Label>
                                                        </p>
                                                    </td>
                                                    <td width="248" height="25">
                                                        <asp:TextBox ID="txtLocCode" runat="server" Width="135px" CssClass="Normal" MaxLength="21"></asp:TextBox>
                                                    </td>
                                                    <td height="25"></td>
                                                </tr>
                                                <tr>
                                                    <td width="279" valign="top">
                                                        <p>
                                                            <asp:Label ID="Label16" runat="server" Width="112px" CssClass="NormalBold" Text="<%$Resources:lblAddTNDescr.Text %>"></asp:Label>
                                                        </p>
                                                    </td>
                                                    <td width="248">
                                                        <asp:TextBox ID="txtLocDescr" runat="server" Width="264px" CssClass="Normal" MaxLength="150"
                                                            TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Width="210px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ControlToValidate="txtLocDescr"
                                                            ErrorMessage="<%$Resources:ReqAddLocDescr.ErrorMessage %>"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                            <tr bgcolor='#FFFFFF'>
                                                                <td height="100">
                                                                    <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                                <td height="100">
                                                                    <uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td height="25"></td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label15" runat="server" Width="133px" CssClass="NormalBold" Text="<%$Resources:lblAddLocStreetCode.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:TextBox ID="txtLocStreetCode" runat="server" Width="88px" CssClass="NormalMand"
                                                            MaxLength="5" Height="24px"></asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label14" runat="server" Width="147px" CssClass="NormalBold" Text="<%$Resources:lblAddLocStreetName.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:TextBox ID="txtLocStreetName" runat="server" Width="261px" CssClass="NormalMand"
                                                            MaxLength="30" Height="24px"></asp:TextBox>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label29" CssClass="NormalBold" Text="<%$Resources:labelStreetA.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textUpdStreetA" Width="261px" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label30" CssClass="NormalBold" Text="<%$Resources:lblStreetB.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textUpdStreetB" Width="261px" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label31" CssClass="NormalBold" Text="<%$Resources:lblSuburb.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <%--<asp:TextBox runat="server" ID="textUpdSuburb"  CssClass="Normal" />--%>
                                                        <asp:DropDownList ID="ddlAuth_LocationSuburb" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label32" CssClass="NormalBold" Text="<%$Resources:lblCity.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textUpdCity" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label33" CssClass="NormalBold" Text="<%$Resources:lblProvince.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="comboUpdProvince" Width="261px" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label34" CssClass="NormalBold" Text="<%$Resources:lblRoute.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textUpdRoute" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label35" CssClass="NormalBold" Text="<%$Resources:lblFrom.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textUpdFrom" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label36" CssClass="NormalBold" Text="<%$Resources:lblTo.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textUpdTo" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label37" CssClass="NormalBold" Text="<%$Resources:lblGpsX.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textUpdGpsX" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="label38" CssClass="NormalBold" Text="<%$Resources:GpsY.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="textUpdGpsY" CssClass="Normal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label13" runat="server" Width="135px" CssClass="NormalBold" Text="<%$Resources:lblAddTravelDirection.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlTravelDirection" runat="server" Width="109px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="279" height="29">
                                                        <asp:Label ID="Label12" runat="server" Width="194px" CssClass="NormalBold" Text="<%$Resources:lblAddLocOffenceSpeedStart.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248" height="29">
                                                        <asp:TextBox ID="txtOffenceSpeedStart" runat="server" Width="43px" CssClass="NormalMand"
                                                            MaxLength="3" Height="24px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" Width="199px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ControlToValidate="txtOffenceSpeedStart"
                                                            ErrorMessage="<%$Resources:ReqAddLocOffenceSpeedStart.ErrorMessage %>"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td height="29">
                                                        <asp:RangeValidator ID="Rangevalidator1" runat="server" CssClass="NormalRed" ForeColor=" "
                                                            ControlToValidate="txtOffenceSpeedStart" ErrorMessage="<%$Resources:RanAddLocOffenceSpeedStart.ErrorMessage %>"
                                                            MaximumValue="200" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="279" height="23">
                                                        <asp:Label ID="Label11" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddRoadType.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248" height="23">
                                                        <asp:DropDownList ID="ddlRoadType" runat="server" Width="159px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td height="23"></td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label10" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddSpeedZone.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlSpeedZone" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label9" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddAuth_Court.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlAuth_Court" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label39" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddLocBranchCode.Text %>"></asp:Label></td>
                                                    <td width="248">
                                                        <asp:TextBox runat="server" ID="txtLocBranchCode" Height="22px" Width="31px" MaxLength="2" CssClass="Normal" /></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label43" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddLocType.Text %>"></asp:Label></td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlLocType" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label44" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddLocRegion.Text %>"></asp:Label></td>
                                                    <td width="248">
                                                        <asp:DropDownList ID="ddlLocRegion" runat="server" Width="162px" CssClass="Normal">
                                                        </asp:DropDownList></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="279">
                                                        <asp:Label ID="Label45" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblRailwayCrossing.Text %>"></asp:Label></td>
                                                    <td width="248">
                                                       <asp:CheckBox ID="chkRailwayCrossing" runat="server" CssClass="NormalBold"  Text="" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="279"></td>
                                                    <td width="248"></td>
                                                    <td>
                                                        <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                            OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <p class="Normal" style="text-align: center;">
                    <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...
                </p>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
