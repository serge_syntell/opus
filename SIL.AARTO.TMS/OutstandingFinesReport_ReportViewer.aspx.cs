using System;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Stalberg.TMS.Data.Datasets;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    public partial class OutstandingFinesReport_ReportViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private int autIntNo = 0;
        private string sReportType = "PDF";
        protected string styleSheet = string.Empty;
        protected NoticeQueryCriteria criteria;
        protected string backgroundImage;
        protected string loginUser;

        protected string thisPageURL = "OutstandingFinesReport_ReportViewer.aspx";
        //protected string thisPage = "Outstanding Fines Report Viewer";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string

            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.loginUser = userDetails.UserLoginName;
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            General gen = new General();
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            criteria = (NoticeQueryCriteria)Session["NoticeQueryCriteria"];

            doProcess();
        }

        private void doProcess()
        {
            //dls 090625 - need to get minimum status for showing fines
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = this.autIntNo;
            ard.ARCode = "4605";
            ard.LastUser = this.loginUser;

            DefaultAuthRules def = new DefaultAuthRules(ard, this.connectionString);

            KeyValuePair<int, string> minStatus = def.SetDefaultAuthRule();

            sReportType = (ViewState["ReportType"] == null ? "PDF" : ViewState["ReportType"].ToString());

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("OutstandingFinesReport", new SqlConnection(this.connectionString));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = criteria.AutIntNo;
            da.SelectCommand.Parameters.Add("@ColumnName", SqlDbType.VarChar, 15).Value = criteria.ColumnName;
            da.SelectCommand.Parameters.Add("@ColumnValue", SqlDbType.VarChar, 30).Value = criteria.Value;
            da.SelectCommand.Parameters.Add("@DateSince", SqlDbType.DateTime).Value = criteria.DateSince;
            da.SelectCommand.Parameters.Add("@MinStatus", SqlDbType.Int).Value = minStatus.Key;

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "OutstandingFinesReport");
            if (reportPage.Equals(""))
            {
                arn.AddAuthReportName(autIntNo, "OutstandingFinesReport.rpt", "OutstandingFinesReport", "system", "");
                reportPage = "OutstandingFinesReport.rpt";
            }
            
            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "OutstandingFinesReport");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {

                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************



            ReportDocument report = new ReportDocument();
            report.Load(reportPath);

            dsOutstandingFinesReport ds = new dsOutstandingFinesReport();
            ds.DataSetName = "dsOutstandingFinesReport";
            try
            {
                da.Fill(ds);
                report.SetDataSource(ds.Tables[1]);
                ds.Dispose();
            }
            catch (Exception e)
            {
                Response.Write(string.Format((string)GetLocalResourceObject("strWriteMsg"), e.Message));
                return;
            }
            finally
            {
                da.Dispose();
            }

            //report.SetDataSource(ds.Tables[1]);
            //ds.Dispose();

            // Export the pdf file
            using (MemoryStream ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat))
            {
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
            }
            Response.End();

            da.Dispose();
            report.Dispose();

        }

    }
}
