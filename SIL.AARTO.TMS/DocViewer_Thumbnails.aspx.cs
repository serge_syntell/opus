using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;

namespace Stalberg.TMS
{
	/// <summary>
	/// Summary description for ThumbnailDemo.
	/// </summary>
	public partial class Thumbnails : System.Web.UI.Page
	{
        protected string connectionString = string.Empty;
        //protected string tempFileLoc = string.Empty;

        private int repIntNo = 0;
        private int srIntNo = 0;

		protected void Page_Load(object sender, System.EventArgs e)
		{
            connectionString = Application["constr"].ToString();

            if (Session["ShowRDIntNo"] == null)
            {
                Response.Redirect("Login.aspx");
                return;
            }

            int rdIntNo = Convert.ToInt32(Session["ShowRDIntNo"]);
            this.repIntNo = Convert.ToInt32(Session["ShowRepIntNo"]);
            this.srIntNo = Convert.ToInt32(Session["ShowSRIntNo"]);

            RepDocumentDB repDoc = new RepDocumentDB(connectionString);
            AccessDBMethods dbMethods = new AccessDBMethods(connectionString);

            //RepDocumentDetails repDocDetails = repDoc.GetRepDocumentDetails(rdIntNo, this.repIntNo, this.srIntNo);

            //set temp location for pdf export
            //tempFileLoc = repDocDetails.RDName + "." + repDocDetails.RDType;

            string tableName = string.Empty;

            if (repIntNo > 0)
                tableName = "RepDocument";
            else if (srIntNo > 0)
                tableName = "SummonsRepDocument";

            //dbMethods.DownLoadBLOB(rdIntNo, "RDIntNo", tableName, "RDImage", ref tempFileLoc, Server.MapPath("temp"));

            //string imageToView = "temp/" + tempFileLoc;
          

			//int numberOfPages = RegisteredDecoders.GetImageInfo(Page.MapPath(imageToView)).FrameCount;
            //Bitmap bitmap = (Bitmap)System.Drawing.Image.FromFile(Page.MapPath(imageToView));
            MemoryStream memorySteam = new MemoryStream(dbMethods.DownLoadBLOB(rdIntNo, "RDIntNo", tableName,"RDImage"));
            Bitmap bitmap = (Bitmap)System.Drawing.Image.FromStream(memorySteam);
            int numberOfPages = bitmap.GetFrameCount(System.Drawing.Imaging.FrameDimension.Page);
           

			int cellsPerRow = 1;

			HtmlTable myTable = new HtmlTable(); // Table used for displaying the thumbs
			HtmlTableCell[] myCells = new HtmlTableCell[numberOfPages]; // Table cells used to populate myTable
			HtmlImage oneImage;					// Image tag to display the thumbs
			HtmlAnchor oneLink;					// Anchor tag for the links to the images
			HtmlGenericControl oneLabel;		// Tag used to write the filename
			HtmlTableRow oneRow;				// Row used to populate myTable

			// Build table cells
			for (int i = 0; i < numberOfPages; i++)
			{
				// Setup cell
				myCells[i] = new HtmlTableCell();
				myCells[i].Align = "center";
				myCells[i].VAlign = "middle";
				myCells[i].Style.Add("background-color", "gainsboro");
				myCells[i].Style.Add("border", "solid 1px #999999");

				// Setup image
				oneImage = new HtmlImage();
				//oneImage.Src = "DocViewer_GrabImage.aspx?path=" + imageToView + "&frame=" + i.ToString();
                oneImage.Src = "DocViewer_GrabImage.aspx?frame=" + i.ToString() + "&rdIntNo=" + rdIntNo + "&tableName=" + tableName;
				oneImage.Border = 0;

				// Setup anchor 
				oneLink = new HtmlAnchor();
				//oneLink.HRef = "DocViewer_Main.aspx?path=" + imageToView + "&frame=" + i.ToString();
                oneLink.HRef = "DocViewer_Main.aspx?frame=" + i.ToString() + "&rdIntNo=" + rdIntNo + "&tableName=" + tableName;
				oneLink.Target = "DocViewer";

				// Setup label
				oneLabel = new HtmlGenericControl("p");
                oneLabel.InnerText = (string)GetLocalResourceObject("oneLabel.InnerText") + (i + 1).ToString();

				// Add controls to the cell
				oneLink.Controls.Add(oneImage);
				oneLink.Controls.Add(oneLabel);
				myCells[i].Controls.Add(oneLink);
			}

			// Put cells into rows
			for (int i = 0; i < myCells.Length; i+=cellsPerRow)
			{
				oneRow = new HtmlTableRow();

				for (int j = 0; j < cellsPerRow && i + j < myCells.Length; j++)
					oneRow.Cells.Add(myCells[i+j]);

				myTable.Rows.Add(oneRow);
			}

			// Setup table attributes
			myTable.CellPadding = 6;
			myTable.CellSpacing = 0;

			// Add table to page
			this.Page.Controls.Add(myTable);

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
