using System;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.IO;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class ViewOffenceDetailAll_Report : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        //private string thisPage = "View All Offence Details Report";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "ViewOffenceDetailAll_Report.aspx";
        protected string loginUser = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            int nNotIntNo = 0; //Convert.ToInt32(Request.QueryString["NotIntNo"].ToString());
            // SD: 26.11.2008 Added noOfLines to count the number of Lines in Evidence pack of HistoryPrintAll stored Proc
            //int noOfLines = CheckNumberOfLines(nNotIntNo);

            //if (noOfLines <= 10)
            //{
            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticeEnquiryAll");
            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiryAll.dplx", "NoticeEnquiryAll", "system", "");
                reportPage = "NoticeEnquiryAll.dplx";
            }

            string path = Server.MapPath("reports/" + reportPage);

            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            try
            {
                DocumentLayout doc = new DocumentLayout(path);

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea raTotal = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("raTotal");
                raTotal.LayingOut += ra_Total;

                //Barry Dickson - add control in to not show receipt fields if there has been no payment made or there was a reversal
                //label for raTotal
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRctAmount");
                lblRctAmount.LayingOut += lbl_Amount;
                //Receipt Number
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbRctNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbRctNo");
                rcbRctNo.LayingOut += ra_RctNo;
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRctNo");
                lblRctNo.LayingOut += lbl_RctNo;
                ////EasyPay Number
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbEasyPay");
                rcbEasyPay.LayingOut += ra_Easy;
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblEasyPay");
                lblEasyPay.LayingOut += lbl_Easy;

                //Jerry 2012-11-26 Receipt AGNumber
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea raRctAGNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("raAGNumber");
                raRctAGNo.LayingOut += ra_RctAgNo;
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctAgNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAGNumber");
                lblRctAgNo.LayingOut += lbl_RctAgNo;

                //Remanded from case number and court date

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcdRemandedFromCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rdaSumRemandedFromCaseNumber");
                rcdRemandedFromCaseNo.LayingOut += new LayingOutEventHandler(ra_RemandedFromCaseNo);

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcdRemandedFromCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rdaSumRemandedFromCourtDate");
                rcdRemandedFromCourtDate.LayingOut += new LayingOutEventHandler(ra_RemandedFromCourtDate);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRemandedFromCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRemandedFromCourtDate");
                lblRemandedFromCourtDate.LayingOut += new LayingOutEventHandler(lbl_RemandedFromCourtDate);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRemandedFromCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRemandedFromCaseNumber");
                lblRemandedFromCaseNo.LayingOut += new LayingOutEventHandler(lbl_RemandedFromCaseNo);


                // Check that there's a Query criteria in the Session
                NoticeQueryCriteria criteria = Session["NoticeQueryCriteria"] as NoticeQueryCriteria;
                if (criteria == null)
                    Response.Redirect("Login.aspx");
                Session.Remove("NoticeQueryCriteria");

                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                query.ConnectionString = this.connectionString;

                StoredProcedureQuery query2 = (StoredProcedureQuery)doc.GetQueryById("Query2");
                query2.ConnectionString = this.connectionString;

                StoredProcedureQuery query3 = (StoredProcedureQuery)doc.GetQueryById("Query3");
                query3.ConnectionString = this.connectionString;

                ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder imagePlaceHolder = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("Img1");
                imagePlaceHolder.LaidOut += imagePlaceHolder_LaidOut;
                ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder imagePlaceHolder2 = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("Img2");
                imagePlaceHolder2.LaidOut += imagePlaceHolder2_LaidOut;

                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("NotIntNo", nNotIntNo);
                parameters.Add("AutIntNo", criteria.AutIntNo);
                parameters.Add("ColName", criteria.ColumnName);
                parameters.Add("ColValue", criteria.Value);
                parameters.Add("DateSince", criteria.DateSince);
                parameters.Add("MinStatus", criteria.MinStatus);

                Document report = doc.Run(parameters);

                byte[] buffer = report.Draw();

                imagePlaceHolder.LaidOut -= imagePlaceHolder_LaidOut;
                imagePlaceHolder2.LaidOut -= imagePlaceHolder2_LaidOut;

                raTotal.LayingOut -= ra_Total;
                lblRctAmount.LayingOut -= lbl_Amount;
                lblRctAmount.LayingOut -= lbl_Easy;
                lblRctAmount.LayingOut -= lbl_RctNo;
                lblRctAmount.LayingOut -= ra_RctNo;
                lblRctAmount.LayingOut -= ra_Easy;
                lblRctAmount.LayingOut -= ra_RctAgNo;
                lblRctAmount.LayingOut -= lbl_RctAgNo;

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(buffer);
                Response.End();
                buffer = null;
            }
            catch
            {
            }
            finally
            {
                GC.Collect();
            }

        }

        public void lbl_RemandedFromCourtDate(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString() != string.Empty)
                        lblA.Text = (string)GetLocalResourceObject("lblA.Text");

                }
            }
            catch { }
        }

        public void lbl_RemandedFromCaseNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"].ToString() != string.Empty)
                        lblA.Text = (string)GetLocalResourceObject("lblA.Text1");

                }
            }
            catch { }
        }

        public void ra_RemandedFromCaseNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"].ToString() != string.Empty)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"]);
                }
            }
            catch
            {
            }
        }

        public void ra_RemandedFromCourtDate(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString() != string.Empty)
                    {
                        DateTime courtDate = Convert.ToDateTime(e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString());
                        ra.Text = String.Format("{0}", courtDate.ToString("yyyy-MM-dd"));
                    }
                }
            }
            catch
            {
            }
        }

        public void ra_Total(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;

                ra.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                    {
                        //update by Rachel 20140819 for 5337
                        //ra.Text = String.Format("R {0:#0.00}", e.LayoutWriter.RecordSets.Current["Total"]);
                        ra.Text = String.Format(CultureInfo.InvariantCulture, "R {0:#0.00}", e.LayoutWriter.RecordSets.Current["Total"]);
                        //end update by Rachel 20140819 for 5337
                    }
                }
            }
            catch { }
        }

        // SD: 26.11.2008 Added noOfLines to count the number of Lines in Evidence pack of HistoryPrintAll stored Proc
        //protected int CheckNumberOfLines(int NotIntNo)
        //{
        //    OffenceDB lines = new OffenceDB(this.connectionString);
        //    int noOfLines = lines.GetNumberofLines(NotIntNo);
        //    return noOfLines;
        //}

        //Barry Dickson - to handle to check on receipt amount and if to show label.
        public void lbl_Amount(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblA.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (!string.IsNullOrEmpty(e.LayoutWriter.RecordSets.Current["Total"].ToString()) && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        lblA.Text = (string)GetLocalResourceObject("lblA.Text2");
                }
            }
            catch{ }
        }
        //Barry Dickson
        public void lbl_RctNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRN = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblRN.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        lblRN.Text = (string)GetLocalResourceObject("lblRN.Text");
                }
            }
            catch { }
        }

        //Jerry 2012-11-26 add
        public void ra_RctAgNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0 &&
                        e.LayoutWriter.RecordSets.Current["AGNumber"] != null && e.LayoutWriter.RecordSets.Current["AGNumber"].ToString() != string.Empty)
                    {
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["AGNumber"]);
                    }
                    else
                    {
                        ra.Text = "";
                    }
                }
            }
            catch { }
        }

        //Jerry 2012-11-26 add
        public void lbl_RctAgNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAGNum = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0 &&
                        e.LayoutWriter.RecordSets.Current["AGNumber"] != null && e.LayoutWriter.RecordSets.Current["AGNumber"].ToString() != string.Empty)
                    {
                        lblAGNum.Text = (string)GetLocalResourceObject("lblAGNum.Text");
                    }
                    else
                    {
                        lblAGNum.Text = "";
                    }
                }
            }
            catch { }
        }

        //Barry Dickson
        public void lbl_Easy(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblE = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;

                lblE.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"].ToString() != string.Empty)
                        lblE.Text = (string)GetLocalResourceObject("lblE.Text");
                }
            }
            catch { }
        }

        //Barry Dickson
        public void ra_RctNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;

                ra.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["RctNumber"]);
                }
            }
            catch { }
        }

        //Barry Dickson
        public void ra_Easy(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;

                ra.Text = string.Empty;

                if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"].ToString() != string.Empty)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"]);
                }
            }
            catch { }
        }

        private void imagePlaceHolder_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            try
            {
                //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];
                
                // david lin 20100324 - remove images from database
                //byte[] buffer = e.LayoutWriter.RecordSets.Current["Max_SI"] == System.DBNull.Value ? null : (byte[])e.LayoutWriter.RecordSets.Current["Max_Si"];                
                byte[] buffer = null;
                if (e.LayoutWriter.RecordSets.Current["Max_SI"] != System.DBNull.Value)
                {
                    ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                    ScanImageDetails imgMax = imgDB.GetImageFullPath(Convert.ToInt32(e.LayoutWriter.RecordSets.Current["Max_SI"]));
                    WebService webService = new WebService();
                    buffer = webService.GetImagesFromRemoteFileServer(imgMax);    
                }                 

                //MemoryStream ms = new MemoryStream(buffer, false);

                //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

                //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
                //float f = 200F / image.Width;
                //image.Width = image.Width * f;
                //image.Height = image.Height * f;

                //e.ContentArea.Add(image);
                if (buffer != null)
                {
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                    img.Height = 72.0F;            //12
                    img.Width = 90.0F;             //15
                    //dls 091126 - since we added the page breaks, the X & Y values of the images seem to be reset to 0
                    img.X = 438.0F;
                    img.Y = 6.0F;
                    e.ContentArea.Add(img);

                    int generation = System.GC.GetGeneration(buffer);
                    buffer = null;
                    System.GC.Collect(generation);
                }
            }
            catch { }
        }

        private void imagePlaceHolder2_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            try
            {
                //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];

                // david lin 20100324 - remove images from database
                //byte[] buffer = e.LayoutWriter.RecordSets.Current["Min_SI"] == System.DBNull.Value ? null : (byte[])e.LayoutWriter.RecordSets.Current["Min_Si"];
                byte[] buffer = null;
                if (e.LayoutWriter.RecordSets.Current["Min_SI"] != System.DBNull.Value)
                {
                    ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                    ScanImageDetails imgMin = imgDB.GetImageFullPath(Convert.ToInt32(e.LayoutWriter.RecordSets.Current["Min_SI"]));
                    WebService webService = new WebService();
                    buffer = webService.GetImagesFromRemoteFileServer(imgMin);
                }   
                //MemoryStream ms = new MemoryStream(buffer, false);

                //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

                //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
                //float f = 200F / image.Width;
                //image.Width = image.Width * f;
                //image.Height = image.Height * f;

                //e.ContentArea.Add(image);
                if (buffer != null)
                {
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                    img.Height = 72.0F;            //12
                    img.Width = 90.0F;             //15
                    //dls 091126 - since we added the page breaks, the X & Y values of the images seem to be reset to 0
                    img.X = 438.0F;
                    img.Y = 99.5F;
                    e.ContentArea.Add(img);

                    int generation = System.GC.GetGeneration(buffer);
                    buffer = null;
                    System.GC.Collect(generation);
                }
            }
            catch { }
        }
    }
}