<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.PostalCodes" Codebehind="PostalCodes.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 153px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                                    </td> 
                            </tr>
                            <tr>
                                <td align="center" style="width: 153px">
                                     </td>
                            </tr>
                        </table>
                                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    &nbsp;<asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label6"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="udp" runat="server">
                        <ContentTemplate>
                            <table border="0" width="568" height="482">
                                <tr>
                                    <td valign="top" height="47">
                                        <p align="center">
                                            <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                        <p>
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:Panel ID="Panel1" runat="server">
                                            <table id="Table1" height="30" cellspacing="1" cellpadding="1" width="542" border="0">
                                                <tr>
                                                    <td width="162">
                                                    </td>
                                                    <td width="7">
                                                    </td>
                                                    <td width="7">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="162">
                                                        <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label></td>
                                                    <td width="7">
                                                        <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="8"></asp:TextBox></td>
                                                    <td width="7">
                                                        <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                            OnClick="btnSearch_Click"></asp:Button></td>
                                                </tr>
                                                <tr>
                                                    <td width="162">
                                                    </td>
                                                    <td width="7">
                                                    </td>
                                                    <td width="7">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        &nbsp; &nbsp; &nbsp;
                                        <asp:Panel ID="Panel2" runat="server" Height="50px" Width="125px">
                                            <table>
                                                <tr>
                                                    <td>
                                    <asp:Button ID="btnOptAdd" runat="server" Width="125px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                                                    <td>
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                                                    <td style="width: 3px">
                                                        <asp:Button ID="btnOptAddAreaSuburb" runat="server" CssClass="NormalButton" OnClick="btnOptAddAreaSuburb_Click"
                                                            Text="<%$Resources:btnOptAddAreaSuburb.Text %>" Width="135px" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:DataGrid ID="dgArea" runat="server" BorderColor="Black" AllowPaging="True"
                                            GridLines="Vertical" CellPadding="4" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                            FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                            AutoGenerateColumns="False" OnItemCommand="dgArea_ItemCommand" OnPageIndexChanged="dgArea_PageIndexChanged"
                                            CssClass="Normal" PageSize="25" OnSelectedIndexChanged="dgArea_SelectedIndexChanged">
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle><PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                            <AlternatingItemStyle CssClass="CartListItemAlt" />
                                            <ItemStyle CssClass="CartListItem" />
                                            <Columns>
                                                <asp:BoundColumn DataField="AreaCode" HeaderText="<%$Resources:dgArea.HeaderText %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="AreaDescr" HeaderText="<%$Resources:dgArea.HeaderText1 %>"></asp:BoundColumn>
                                                <asp:ButtonColumn Text="<%$Resources:dgAreaItem.Text %>" CommandName="Edit"></asp:ButtonColumn>
                                                <asp:ButtonColumn CommandName="Select" Text="<%$Resources:dgAreaItem.Text1 %>"></asp:ButtonColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="CartListHead" />
                                        </asp:DataGrid>
                                        <asp:Panel ID="pnlUpdateArea" runat="server">
                                            <table id="Table3"  cellspacing="1" cellpadding="1" width="734" border="0"
                                                class="NormalBold">
                                                <tr>
                                                    <td width="157" valign="top" >
                                                        <asp:Label ID="Label19" runat="server" Width="158px" CssClass="ProductListHead" Text="<%$Resources:lblUpdSelectArea.Text %>"></asp:Label></td>
                                                    <td width="295">
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="2" valign="top" width="157">
                                                        <asp:Label ID="Label2" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAreaCode.Text %>"></asp:Label></td>
                                                    <td height="2" width="295">
                                                        <asp:TextBox ID="txtAreaCode" runat="server" CssClass="Normal" MaxLength="100" Width="289px"></asp:TextBox></td>
                                                    <td height="2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="157" >
                                                        <asp:Label ID="Label9" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAreadesc.Text %>"></asp:Label></td>
                                                    <td valign="top" width="295" style="height: 1px">
                                                        <asp:TextBox ID="txtAreaDescr" runat="server" Width="289px" CssClass="Normal"
                                                            MaxLength="100"></asp:TextBox>&nbsp;
                                                    </td>
                                                    <td height="2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                            <tr bgcolor='#FFFFFF'>
                                                                <td height="100">
                                                                    <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                                <td height="100">
                                                                    <uc1:UCLanguageLookup ID="ucLanguageLookupUpdateAreaCode" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                     <td style="height: 1px">
                                                        <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                            OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                                  </tr>
                                            </table>
                                        </asp:Panel>
                                        &nbsp; &nbsp; &nbsp;
                                        <asp:Panel ID="Panel3" runat="server" Height="50px" Width="125px">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnOptAddSuburb" runat="server" CssClass="NormalButton" OnClick="btnOptAddSuburb_Click"
                                                            Text="<%$Resources:btnOptAddSuburb.Text %>" Width="135px" /></td>
                                                    <td>
                                        <asp:Button ID="btnOptDeleteSuburb" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDeleteSuburb.Text %>" OnClick="btnOptDeleteSuburb_Click"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:DataGrid ID="dgSuburb" runat="server" BorderColor="Black" AllowPaging="True"
                                            GridLines="Vertical" CellPadding="4" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                            FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                            AutoGenerateColumns="False" OnItemCommand="dgSuburb_ItemCommand" OnPageIndexChanged="dgSuburb_PageIndexChanged"
                                            CssClass="Normal" PageSize="25" OnSelectedIndexChanged="dgSuburb_SelectedIndexChanged">
                            <FooterStyle CssClass="CartListFooter" />
                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                            <AlternatingItemStyle CssClass="CartListItemAlt" />
                            <ItemStyle CssClass="CartListItem" />
                            <Columns>
                                <asp:BoundColumn DataField="SubIntNo" HeaderText="SubIntNo" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AreaCode" HeaderText="<%$Resources:dgArea.HeaderText %>"></asp:BoundColumn>
                                <asp:BoundColumn DataField="SubDescr" HeaderText="<%$Resources:dgArea.HeaderText1 %>"></asp:BoundColumn>
                                <asp:ButtonColumn Text="<%$Resources:dgAreaItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                            </Columns>
                            <HeaderStyle CssClass="CartListHead" />
                        </asp:DataGrid>
                    <asp:Panel ID="pnlUpdateSuburb" runat="server">
                        <table id="Table2"  cellspacing="1" cellpadding="1" width="734" border="0"
                                                class="NormalBold">
                            <tr>
                                <td valign="top" style="width: 205px" >
                                    <asp:Label ID="Label3" runat="server" CssClass="ProductListHead" Width="200px" Text="<%$Resources:lblUpdSelectSuburb.Text %>"></asp:Label></td>
                                <td style="width: 291px">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td height="2" valign="top" style="width: 205px">
                                    <asp:Label ID="Label4" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAreaCode.Text %>"></asp:Label></td>
                                <td height="2" style="width: 291px">
                                    <asp:TextBox ID="txtSuburbCode" runat="server" CssClass="Normal" MaxLength="100" Width="289px"></asp:TextBox></td>
                                <td height="2">
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 205px" >
                                    <asp:Label ID="Label5" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblSuburbdescr.Text %>"></asp:Label></td>
                                <td valign="top" style="height: 1px; width: 291px;">
                                    <asp:TextBox ID="txtSubDescr" runat="server" CssClass="Normal" MaxLength="100" Width="289px"></asp:TextBox>&nbsp;
                                </td>
                                 <td height="2">
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2">
                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                        <tr bgcolor='#FFFFFF'>
                                            <td height="100">
                                                <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                            <td height="100">
                                                <uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                 <td style="height: 1px">
                                    <asp:Button ID="btnUpdateSuburb" runat="server" CssClass="NormalButton" OnClick="btnUpdateSuburb_Click" OnClientClick="return VerifytLookupRequired()"
                                        Text="<%$Resources:btnUpdateSuburb.Text %>" Width="108px" />
                                    <asp:Button ID="btnSuburbAdd" runat="server" CssClass="NormalButton"
                                        Text="<%$Resources:btnSuburbAdd.Text %>" Width="106px" OnClick="btnSuburbAdd_Click" OnClientClick="return VerifytLookupRequired()" /></td>
                             </tr>
                            </table>
                                    </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    &nbsp;
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
