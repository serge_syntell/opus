using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.IO;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class WOARegisterReport_Viewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;

        protected string sOrderBy = "CrtNo";
        //private string thisPage = "WOA Register Report Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "WOARegisterReport_Viewer.aspx";
        protected string loginUser = string.Empty;
        protected string woaPrintFile = string.Empty;

        //Document report = null;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.autIntNo = Request.QueryString["AutIntNo"] == null ? autIntNo : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString());
            this.sOrderBy = Request.QueryString["OrderBy"] == null ? "CrtNo" : Request.QueryString["OrderBy"].ToString();
            this.woaPrintFile = Request.QueryString["WOAPrintFile"] == null ? "" : Request.QueryString["woaPrintFile"].ToString();

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = string.Empty;
            string templatePath = string.Empty;
            string sTemplate = string.Empty;

            AuthorityDB authorityDB = new AuthorityDB(connectionString);
            AuthorityDetails authorityDetails = new AuthorityDetails();
            authorityDetails = authorityDB.GetAuthorityDetails(autIntNo);

            ////jerry 2011.2.24 add
            //if (authorityDetails.AutCode.Trim() == "MB")
            //{
            //    reportPage = arn.GetAuthReportName(autIntNo, "WOACourtRegister_MB");

            //    if (reportPage.Equals(string.Empty))
            //    {
            //        int arnIntNo = arn.AddAuthReportName(autIntNo, "WOACourtRegister_MB.dplx", "WOACourtRegister_MB", "system", "");
            //        reportPage = "WOACourtRegister_MB.dplx";
            //    }

            //    sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "WOACourtRegister_MB");
            //}
            //else
            //{
                if (woaPrintFile.Length > 0)
                {
                    reportPage = arn.GetAuthReportName(autIntNo, "WOACourtRegister");

                    if (reportPage.Equals(string.Empty))
                    {
                        int arnIntNo = arn.AddAuthReportName(autIntNo, "WOACourtRegister.dplx", "WOACourtRegister", "system", "");
                        reportPage = "WOACourtRegister.dplx";
                    }

                    sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "WOACourtRegister");
                }
                else
                {
                    reportPage = arn.GetAuthReportName(autIntNo, "WOARegister");

                    if (reportPage.Equals(string.Empty))
                    {
                        int arnIntNo = arn.AddAuthReportName(autIntNo, "WOARegister.dplx", "WOARegister", "system", "");
                        reportPage = "WOARegister.dplx";
                    }

                    sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "WOARegister");
                }
            //}
            

            string path = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            DocumentLayout doc = new DocumentLayout(path);

            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("AutIntNo", this.autIntNo);
            parameters.Add("OrderBy", this.sOrderBy.Trim());
            parameters.Add("WOAPrintFile", this.woaPrintFile);

            if (reportPage.IndexOf("_ST") < 0)
            {
                AuthorityDB db = new AuthorityDB(connectionString);
                AuthorityDetails authDet = new AuthorityDetails();
                authDet = db.GetAuthorityDetails(autIntNo);

                RecordArea recordAuthorityName = (RecordArea)doc.GetElementById("recordAuthorityName");
                recordAuthorityName.Text = authDet.AutName;
                //RecordArea recordAuthorityAddress = (RecordArea)doc.GetElementById("recordAuthorityAddress");
                //string sDetails = authDet.AutPhysAddr1.Trim() + " " + authDet.AutPhysAddr2.Trim() + " " + authDet.AutPhysAddr3.Trim() + " " + authDet.AutPhysAddr4.Trim()
                //+ " " + authDet.AutPhysCode.Trim() + " - " + authDet.AutPostAddr1.Trim() + " " + authDet.AutPostAddr2.Trim() + " " + authDet.AutPostAddr3.Trim() 
                //+ " " + authDet.AutPostCode.Trim() + " - TEL: " + authDet.AutTel.Trim() + " - FAX:" + authDet.AutFax.Trim() + " - EMAIL: " + authDet.AutEmail.Trim();
                //recordAuthorityAddress.Text = sDetails.ToUpper ();
            }

            Document report = doc.Run(parameters);
            byte[] buffer = report.Draw();

           if (Request.QueryString["printType"] != null && Request.QueryString["printType"] == "PrintWOAPrintCourtRegister")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, userDetails.UserLoginName, PunchStatisticsTranTypeList.PrintWOAPrintCourtRegister, PunchAction.Change);
            }

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(buffer);
            Response.End();

        }

    }
}