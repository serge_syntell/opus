using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Specialized;

namespace Stalberg.TMS
{
    public partial class Error : System.Web.UI.Page
    {
        // Fields
        private string connectionString;
        protected string styleSheet = string.Empty;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        //protected string thisPage = "An error has occurred!";
        protected string description = String.Empty;
        protected string thisPageURL = "Error.aspx";
        protected string title = string.Empty;
      
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get user info from session variable
            //if (Session["userDetails"] == null)
              //  Server.Transfer("Login.aspx?Login=invalid");

            //if (Session["userIntNo"] == null)
                //Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            //Stalberg.TMS.UserDB user = new UserDB(connectionString);
            //Stalberg.TMS.UserDetails userDetails = new UserDetails();

            //userDetails = (UserDetails)Session["userDetails"];

            //this.connectionString = Application["constr"].ToString();

            ////set domain specific variables
            //General gen = new General();
            //backgroundImage = gen.SetBackground(Session["drBackground"]);
            //styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            //title = gen.SetTitle(Session["drTitle"]);

            linkToHome.NavigateUrl = ConfigurationManager.AppSettings["AARTODomainURL"] + "/Home/Index";

            if (!this.Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                // 2014-01-08, Oscar moved this out of bottom.
                if (Application["ErrorMessage"] != null)
                {
                    lblError.Text = Application["ErrorMessage"].ToString();
                    lblErrorPage.Visible = false;
                    lblErrorPageURL.Visible = false;
                    Label1.Visible = false;
                    Application["ErrorMessage"] = null;
                    return;
                }

                //pick up query string values
                NameValueCollection lQString = Request.QueryString;

                // Get names of all keys into a string array.
                String[] lKeyArray = lQString.AllKeys;
                if (lKeyArray.Length > 0)
                {
                    if (Request.QueryString["error"] != null)
                    {
                        lblError.Text = Request.QueryString["error"].ToString();
                    }
                    // jerry 2010/10/14 add
                    //else if (Application["ErrorMessage"] != null)
                    //{
                    //    lblError.Text = Application["ErrorMessage"].ToString();
                    //    lblErrorPage.Visible = false;
                    //    lblErrorPageURL.Visible = false;
                    //    Label1.Visible = false;
                    //    Application["ErrorMessage"] = null;
                    //    return;
                    //}
                    else
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                    }

                    

                    if (Request.QueryString["errorPage"] != null)
                    {
                        lblErrorPage.Text = Request.QueryString["errorPage"].ToString();
                    }
                    else
                    {
                        lblErrorPage.Text = (string)GetLocalResourceObject("lblErrorPage.Text1");
                    }

                    if (Request.QueryString["errorPageURL"] != null)
                    {
                        lblErrorPageURL.Text = "(" + Request.QueryString["errorPageURL"].ToString() + ")";
                    }
                    else
                    {
                        lblErrorPageURL.Text = (string)GetLocalResourceObject("lblErrorPage.Text1");
                    }
                }
                
            }
        }

       
    }
}
