<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.CashReceiptTraffic_CashBoxRecon" Codebehind="CashReceiptTraffic_CashBoxRecon.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
        
     <script language="JavaScript" type="text/javascript">
         function addCash(txt, lbl, denomination) {
             var amount = parseInt(txt.value) * denomination;
             if (isNaN(amount))
                 amount = 0;
             lbl.innerHTML = (amount);

             addTotal();
         }

         function addOthers(txt, lbl) {
             var amount = parseFloat(txt.value);
             if (isNaN(amount))
                 amount = 0;
             //alert(amount);
             lbl.innerHTML = amount;

             addTotal();
         }

         function addTotal() {
             // Add up the cash totals
             var r500 = parseInt(document.getElementById('lbl500').innerHTML);
             if (isNaN(r500))
                 r500 = 0;
             var r200 = parseInt(document.getElementById('lbl200').innerHTML);
             if (isNaN(r200))
                 r200 = 0;
             var r100 = parseInt(document.getElementById('lbl100').innerHTML);
             if (isNaN(r100))
                 r100 = 0;
             var r50 = parseInt(document.getElementById('lbl50').innerHTML);
             if (isNaN(r50))
                 r50 = 0;
             var r20 = parseInt(document.getElementById('lbl20').innerHTML);
             if (isNaN(r20))
                 r20 = 0;
             var r10 = parseInt(document.getElementById('lbl10').innerHTML);
             if (isNaN(r10))
                 r10 = 0;
             var r5 = parseInt(document.getElementById('lbl5').innerHTML)
             if (isNaN(r5))
                 r5 = 0;
             var r2 = parseInt(document.getElementById('lbl2').innerHTML);
             if (isNaN(r2))
                 r2 = 0;
             var r1 = parseInt(document.getElementById('lbl1').innerHTML);
             if (isNaN(r1))
                 r1 = 0;
             var cash = r500 + r200 + r100 + r50 + r20 + r10 + r5 + r2 + r1;
             document.getElementById('lblCash').innerHTML = cash;

             var cheques = parseFloat(document.getElementById('lblCheques').innerHTML);
             if (isNaN(cheques))
                 cheques = 0;

             var cards = parseFloat(document.getElementById('lblCards').innerHTML);
             if (isNaN(cards))
                 cards = 0;


             var dcards = parseFloat(document.getElementById('lblDCards').innerHTML);
             if (isNaN(dcards))
                 dcards = 0;
             //  alert(dcards);

             var po = parseFloat(document.getElementById('lblPostalOrders').innerHTML);
             if (isNaN(po))
                 po = 0;

             var cashFloat = parseFloat(document.getElementById('lblFloat').innerHTML);
             if (isNaN(cashFloat))
                 cashFloat = 0;

             document.getElementById('lblTotalInBox').innerHTML = (cash + cheques + cards + dcards + po);
         }
        </script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center" style="width: 182px">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:UpdatePanel ID="udpCashboxRecon" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                                <p style="text-align: center;">
                                    <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;</p>
                            </asp:Panel>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                                <table border="0" class="NormalBold">
                                    <tr>
                                        <td colspan="6">
                                            <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblCash.Text %>"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label> 500</td>
                                        <td>
                                            <asp:Label ID="Label29" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt500" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right; width: 20px">
                                            <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl500" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label> 200</td>
                                        <td>
                                            <asp:Label ID="Label30" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt200" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl200" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label8" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label> 100</td>
                                        <td>
                                            <asp:Label ID="Label31" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt100" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label9" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl100" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label10" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label> 50</td>
                                        <td>
                                            <asp:Label ID="Label32" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt50" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label11" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl50" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label12" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label> 20</td>
                                        <td>
                                            <asp:Label ID="Label33" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt20" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label13" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl20" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label14" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label> 10</td>
                                        <td>
                                            <asp:Label ID="Label34" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt10" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label15" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl10" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label16" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label> 5</td>
                                        <td>
                                            <asp:Label ID="Label35" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt5" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label17" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl5" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label18" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label>2</td>
                                        <td>
                                            <asp:Label ID="Label36" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt2" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label19" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl2" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label20" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label>1</td>
                                        <td>
                                            <asp:Label ID="Label37" runat="server" Text="<%$Resources:lblX.Text %>"></asp:Label></td>
                                        <td style="width: 158px">
                                            <asp:TextBox ID="txt1" runat="server"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            =</td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label21" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lbl1" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="width: 158px">
                                            <asp:Label ID="Label38" runat="server" Text="<%$Resources:lblCashInBox.Text %>"></asp:Label></td>
                                        <td style="text-align: center">
                                        </td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label22" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td style="border-top: black 1px solid; border-bottom: black 1px double">
                                            <asp:Label ID="lblCash" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblNoCheques" runat="server" Text="<%$Resources:lblNoCheques.Text %>"></asp:Label></td>
                                        <td colspan="2">
                                            <asp:Label ID="Label39" runat="server" Text="<%$Resources:lblTotalCheques.Text %>"></asp:Label></td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtChequeCount" runat="server"></asp:TextBox></td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtChequeAmount" runat="server"></asp:TextBox></td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label23" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td style="border-top: black 1px solid; border-bottom: black 1px double">
                                            <asp:Label ID="lblCheques" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblNoCards" runat="server" Text="<%$Resources:lblNoCards.Text %>"></asp:Label></td>
                                        <td colspan="2">
                                            <asp:Label ID="Label40" runat="server" Text="<%$Resources:lblCreditCards.Text %>"></asp:Label></td>
                                        <td style="text-align: right">
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtCardCount" runat="server"></asp:TextBox></td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtCardAmount" runat="server"></asp:TextBox></td>
                                        <td style="text-align: right">
                                           <asp:Label ID="Label24" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td style="border-top: black 1px solid; border-bottom: black 1px double">
                                            <asp:Label ID="lblCards" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblNoDebitCards" runat="server" Text="<%$Resources:lblNoDebitCards.Text %>"></asp:Label></td>
                                        <td colspan="2">
                                            <asp:Label ID="Label41" runat="server" Text="<%$Resources:lblDebitCards.Text %>"></asp:Label></td>
                                        <td style="text-align: right">
                                        </td>
                                        <td style="border-top: black 1px solid; border-bottom: black 1px double">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtDebitCardCount" runat="server"></asp:TextBox></td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtDebitCardAmount" runat="server"></asp:TextBox></td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label25" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td style="border-top: black 1px solid; border-bottom: black 1px double">
                                           <asp:Label ID="lblDCards" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblNoPO" runat="server" Text="<%$Resources:lblNoPO.Text %>"></asp:Label></td>
                                        <td colspan="2">
                                            <asp:Label ID="Label42" runat="server" Text="<%$Resources:lblPostalOrders.Text %>"></asp:Label></td>
                                        <td style="text-align: right">
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtPostalOrderCount" runat="server"></asp:TextBox></td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtPostalOrderAmount" runat="server"></asp:TextBox></td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label26" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td style="border-top: black 1px solid; border-bottom: black 1px double">
                                            <asp:Label ID="lblPostalOrders" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                        </td>
                                        <td style="width: 158px">
                                        </td>
                                        <td style="text-align: center">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="2">
                                            <asp:Label ID="Label43" runat="server" Text="<%$Resources:lblAmountBox.Text %>"></asp:Label></td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label27" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td style="border-top: black 1px solid; border-bottom: black 1px double">
                                            <asp:Label ID="lblTotalInBox" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: right; height: 25px;">
                                            <input runat="server" id="btnRecelculate" class="NormalButton" type="button" value="<%$Resources:btnRecelculate.Text %>"
                                                onclick="addTotal();" />
                                            &nbsp; &nbsp;
                                            <asp:Button ID="btnRecord" runat="server" CssClass="NormalButton" OnClick="btnRecord_Click"
                                                Text="<%$Resources:btnRecord.Text %>" />
                                            &nbsp;&nbsp; &nbsp;
                                            <asp:Button ID="buttonRecon" runat="server" CssClass="NormalButton" Text="<%$Resources:buttonRecon.Text %>"
                                                OnClick="buttonRecon_Click" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="2">
                                        </td>
                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblEODBalancing.Text %>"></asp:Label><td
                                            style="text-align: right; height: 10px;">
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="2">
                                            <asp:Label ID="Label44" runat="server" Text="<%$Resources:lblCashBox.Text %>"></asp:Label></td>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label28" runat="server" Text="<%$Resources:lblR.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblFloat" runat="server" Text="0"></asp:Label></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label45"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center" style="width: 182px">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>

 
    </form>
  
</body>
</html>
