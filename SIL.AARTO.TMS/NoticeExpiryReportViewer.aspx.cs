﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using System.IO;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an NoticeExpiryReport
    /// </summary>
    public partial class NoticeExpiryReportViewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private int userIntNo = 0;
        //private string thisPage = "Notice Expiry Report Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "NoticeExpiryReportViewer.aspx";
        protected string loginUser = string.Empty;
        private string printFile = string.Empty;


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();
            //int nCount = 0;

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.loginUser = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticeExpiryReport");
            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeExpiryReport.dplx", "NoticeExpiryReport", "system", "");
                reportPage = "NoticeExpiryReport.dplx";
            }

            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            DocumentLayout doc = new DocumentLayout(reportPath);

            autIntNo = Request.QueryString["AutIntNo"] == null ? autIntNo : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString());

            int iWarningPeriod = Request.QueryString["WarningPeriod"] == null ? 30 : Convert.ToInt32(Request.QueryString["WarningPeriod"].ToString());
            int iExpiryDays = Request.QueryString["ExpiryDays"] == null ? 548 : Convert.ToInt32(Request.QueryString["ExpiryDays"].ToString());

            int iSort = Request.QueryString["Sort"] == null ? 0 : Convert.ToInt32(Request.QueryString["Sort"].ToString());

            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("AutIntNo", autIntNo);
            parameters.Add("WarningPeriod", iWarningPeriod);
            parameters.Add("ExpireDays", iExpiryDays);
            parameters.Add("Sort", iSort);


            ceTe.DynamicPDF.ReportWriter.ReportElements.Label Label1 = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblWarning");
            Label1.Text = string.Format((string)GetLocalResourceObject("lblDays.Text"), iWarningPeriod.ToString());

            ceTe.DynamicPDF.ReportWriter.ReportElements.Label LabelRule = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRule");

            int iRule =   Request.QueryString["Rule"] == null ? 0 : Convert.ToInt32(Request.QueryString["Rule"].ToString());
            if (iRule == 0)
                LabelRule.Text = string.Format((string)GetLocalResourceObject("LabelRule.Text"), iExpiryDays.ToString());
            else
                LabelRule.Text = string.Format((string)GetLocalResourceObject("LabelRule.Text1"), iExpiryDays.ToString());


            Document report = doc.Run(parameters);
            byte[] buffer = report.Draw();

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(buffer);
            Response.End();
        }
    }
}
