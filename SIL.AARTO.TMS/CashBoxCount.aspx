<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.CashBoxCount" Codebehind="CashBoxCount.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:UpdatePanel ID="udpCashboxCount" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                                <p style="text-align: center;">
                                    <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" 
                                        EnableViewState="false"></asp:Label></p>
                            </asp:Panel>
                            <asp:Panel ID="pnlCashBox" runat="server" Width="100%">
                                <fieldset>
                                    <legend>
                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblCashboxUsage.Text %>"></asp:Label></legend>
                                    <asp:RadioButtonList ID="rblCashboxType" runat="server" CssClass="Normal">
                                        <asp:ListItem Selected="True" Value="OverTheCounter" Text="<%$Resources:rblCashboxTypeItem.Text %>"></asp:ListItem>
                                        <asp:ListItem Value="PostalReceipts" Text="<%$Resources:rblCashboxTypeItem.Text1 %>"></asp:ListItem>
                                        <asp:ListItem Value="RoadblockReceipts" Text="<%$Resources:rblCashboxTypeItem.Text2 %>"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </fieldset>
                                <asp:GridView ID="grdCashBox" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                                    OnSelectedIndexChanged="grdCashBox_SelectedIndexChanged" ShowFooter="True">
                                    <FooterStyle CssClass="CartListHead" />
                                    <Columns>
                                        <asp:BoundField DataField="CBIntNo" HeaderText="CBIntNo" Visible="False" />
                                        <asp:BoundField DataField="CBName" HeaderText="<%$Resources:grdCashBox.HeaderText %>">
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CBStartAmount" HeaderText="<%$Resources:grdCashBox.HeaderText1 %>">
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:CommandField HeaderText="<%$Resources:grdCashBox.HeaderText2 %>" ShowSelectButton="True">
                                            <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                        </asp:CommandField>
                                    </Columns>
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                                <pager:AspNetPager id="grdCashBoxPager" runat="server" 
                                            showcustominfosection="Right" width="400px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"   
                                    PageSize="10" onpagechanged="grdCashBoxPager_PageChanged"  UpdatePanelId="udpCashboxCount"
                                    ></pager:AspNetPager>
                            </asp:Panel>
                            <asp:Panel ID="pnlFloat" runat="Server" Width="100%" CssClass="NormalBold">
                                <table border="0" class="NormalBold">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblNameNo.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblCashboxName" runat="server"></asp:Label></td>
                                        <td style="width: 170px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblR500Notes.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txt500" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox>
                                        </td>
                                        <td style="width: 170px">
                                            <asp:HiddenField ID="hid500" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblR200Notes.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txt200" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 170px">
                                            <asp:HiddenField ID="hid200" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblR100Notes.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txt100" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 170px">
                                            <asp:HiddenField ID="hid100" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblR50Notes.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txt50" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 170px">
                                            <asp:HiddenField ID="hid50" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px">
                                            <asp:Label ID="Label8" runat="server" Text="<%$Resources:lblR20 Notes.Text %>"></asp:Label></td>
                                        <td style="height: 26px">
                                            <asp:TextBox ID="txt20" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 170px; height: 26px">
                                            <asp:HiddenField ID="hid20" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label9" runat="server" Text="<%$Resources:lblR10Notes.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txt10" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 170px">
                                            <asp:HiddenField ID="hid10" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label10" runat="server" Text="<%$Resources:lblR5Coins.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txt5" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 170px">
                                            <asp:HiddenField ID="hid5" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label11" runat="server" Text="<%$Resources:lblR2Coins.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txt2" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 170px">
                                            <asp:HiddenField ID="hid2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label12" runat="server" Text="<%$Resources:lblR1Coins.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txt1" runat="server" MaxLength="4" Width="100px" CssClass="Normal"></asp:TextBox></td>
                                        <td style="width: 170px">
                                            <asp:HiddenField ID="hid1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label13" runat="server" Text="<%$Resources:lblStartingBalance.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblBalance" runat="server"></asp:Label></td>
                                        <td style="width: 170px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right;">
                                            <asp:Button ID="btnConfirm" runat="server" Text="<%$Resources:btnConfirm.Text %>" OnClick="btnConfirm_Click"
                                                CssClass="NormalButton" /></td>
                                        <td colspan="1" style="width: 170px; text-align: right">
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlConfirm" runat="server" Width="100%">
                                
                                <asp:Button ID="btnCashFloatConfirmed" runat="server" OnClick="btnCashFloatConfirmed_Click"
                                    Text="<%$Resources:btnCashFloatConfirmed.Text %>" CssClass="NormalButton" /></asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label14"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
