using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI.WebControls;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using System.Globalization;
using SIL.AARTO.BLL.Utility;

namespace Stalberg.TMS
{
    /// <summary>
    /// The starting point for ticket payment receipt
    /// </summary>
    public partial class AdminReceiptReversal : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string loginUser;
        private Cashier cashier = null;

        protected string styleSheet;
        protected string backgroundImage;
        protected double myTotal = 0;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Administrative Receipt Reversal";
        protected string thisPageURL = "AdminReceiptReversal.aspx";
        protected int autIntNo = 0;
        protected int userIntNo = 0;
        protected int rctIntNo = 0;
        protected string rctNumber = string.Empty;

        protected string alwaysUsePostalReceiptReport = "N";

        // Constants
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;

            userDetails = (UserDetails)Session["userDetails"];
            userIntNo = Int32.Parse(Session["userIntNo"].ToString());

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            BankAccountDB bankAccount = new BankAccountDB(connectionString);
            this.cashier = bankAccount.GetUserBankAccount(int.Parse(Session["userIntNo"].ToString()));

            this.pnlDetails.Visible = false;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.pnlDetails.Visible = true;

            if (autIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
            }

            if (ViewState["RctIntNo"] != null)
            {
                this.rctIntNo = Int32.Parse(ViewState["RctIntNo"].ToString());
            }

            if (ViewState["alwaysUsePostalReceiptReport"] == null)
            {
                this.alwaysUsePostalReceiptReport = GetPostalReceiptRule();
                ViewState["alwaysUsePostalReceiptReport"] = this.alwaysUsePostalReceiptReport;
            }
            else
            {
                this.alwaysUsePostalReceiptReport = ViewState["alwaysUsePostalReceiptReport"].ToString();
            }

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                this.TicketNumberSearch1.AutIntNo = this.autIntNo;
                this.pnlDetails.Visible = true;

                this.pnlDisplaySelectedTickets.Visible = false;

                this.chkAreYouSure.Visible = false;
                this.lblConfirmMessage.Visible = false;
                this.pnlConfirm.Visible = false;
                this.pnlReceiptDetails.Visible = false;

                this.pnlNotReApplied.Visible = false;
                this.pnlNext.Visible = false;

                ViewState["AreYouSure"] = null;
            }
            else
            {
                if (ViewState["AreYouSure"] != null)
                    lblConfirmMessage.Text = ViewState["AreYouSure"].ToString();
            }
            
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            if (this.txtTicketNo.Text.Trim().Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            //Jake 2015-04-15 commit out, no need to set index when binding data for grid
            //dgTicketDetailsPager.RecordCount = 0;
            //dgTicketDetailsPager.CurrentPageIndex = 1;
            LookForNotice();
        }

        protected void LookForNotice()
        {
            Session["editRctIntNo"] = null;

            this.pnlDisplaySelectedTickets.Visible = false;

            // Get the ticket details from the notice/charge tables for ticket / summons
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            //if (this.txtTicketNo.Text.Trim() == string.Empty)
            //{
            //    this.lblError.Text = "There are no receipts that match your search criteria.";
            //    return;
            //}

            //DataSet ds = db.ReceiptEnquiryOnTicketNumber(autIntNo, this.txtTicketNo.Text.Trim());
            int totalCount = 0;
            DataSet ds = db.ReceiptEnquiryOnTicketNumber(this.txtTicketNo.Text.Trim(), dgTicketDetailsPager.PageSize, dgTicketDetailsPager.CurrentPageIndex, out totalCount);

            if (ds.Tables[0].Rows.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            this.lblError.Text = string.Empty;

            dgTicketDetails.DataSource = ds;
            dgTicketDetails.DataKeyNames = new string[] { "RctIntNo" };
            dgTicketDetails.DataBind();
            dgTicketDetailsPager.RecordCount = totalCount;

            this.pnlDisplaySelectedTickets.Visible = true;
            this.pnlReceiptDetails.Visible = false;
            this.pnlConfirm.Visible = false;
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtTicketNo.Text = this.TicketNumberSearch1.TicketNumber;

            pnlConfirm.Visible = false;
            pnlDisplaySelectedTickets.Visible = false;
            pnlReceiptDetails.Visible = false;
        }

        protected void btnRePrint_Click(object sender, EventArgs e)
        {
            // Get the ticket details from the notice/charge tables for ticket / summons
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            if (this.txtTicketNo.Text.Trim() == string.Empty)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            //DataSet ds = db.ReceiptEnquiryOnTicketNumber(autIntNo, this.txtTicketNo.Text.Trim());
            DataSet ds = db.ReceiptEnquiryOnTicketNumber(this.txtTicketNo.Text.Trim());

            if (ds.Tables[0].Rows.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            else if (ds.Tables[0].Rows.Count == 1)
            {
                int rctIntNo = Convert.ToInt32(ds.Tables[0].Rows[0]["RctIntNo"].ToString());

                PageToOpen page = null;

                if (this.cashier == null || this.cashier.CashBoxType != CashboxType.PostalReceipts)
                {
                    if (alwaysUsePostalReceiptReport.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                    {
                        page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                    }
                    else
                    {
                        page = new PageToOpen(this, "ReceiptNoteViewer.aspx");
                    }
                }
                else
                {
                    page = new PageToOpen(this, "CashReceiptTraffic_PostalReceiptViewer.aspx");
                }

                if (alwaysUsePostalReceiptReport.Equals("Y", StringComparison.InvariantCultureIgnoreCase) || (this.cashier != null && this.cashier.CashBoxType == CashboxType.PostalReceipts))
                {
                    page.Parameters.Add(new QSParams("date", DateTime.Today.ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("CBIntNo", "0"));
                    page.Parameters.Add(new QSParams("RCtIntNo", rctIntNo.ToString()));
                }
                else
                {
                    page.Parameters.Add(new QSParams("receipt", rctIntNo.ToString()));
                }

                
                Helper_Web.BuildPopup(page);
                //Jake 2015-04-15 Bind data grid after populate a new page to show reprort
                LookForNotice();
            }
            else
            {
                PageToOpen page = new PageToOpen(this, "ReceiptEnquiry.aspx");
                page.Parameters.Add(new QSParams("NotTicketNo", this.txtTicketNo.Text.Trim()));
                Response.Redirect(page.ToString());
            }
            //PunchStats805806 enquiry AdminReceiptReversal
        }

        protected void dgTicketDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            int rctIntNo = 0;
            string reversed = "N";
            int parentRctIntNo = 0;

            DataRowView row = (DataRowView)e.Row.DataItem;
            if (row == null)
                return;

            rctIntNo = (int)row["RctIntNo"];

            reversed = row["Reversed"].ToString();
            parentRctIntNo = Convert.ToInt32(row["ParentRctIntNo"]);

            if (reversed.Equals("N") && parentRctIntNo == 0)
            {
                e.Row.Cells[10].Enabled = true;
            }
            else
            {
                e.Row.Cells[10].Enabled = false;
            }

            DateTime cbDateOpened = DateTime.MinValue;
            DateTime cbDateClosed = DateTime.MinValue;
            DateTime rctCaptureDate = DateTime.MinValue;

            if (row["CBDateOpened"] != null)
            {
                bool f = DateTime.TryParse(row["CBDateOpened"].ToString(), out cbDateOpened);
            }

            if (row["CBDateClosed"] != null)
            {
                bool f = DateTime.TryParse(row["CBDateClosed"].ToString(), out cbDateClosed);
            }

            if (row["CaptureDate"] != null)
            {
                bool c = DateTime.TryParse(row["CaptureDate"].ToString(), out rctCaptureDate);
            }

            bool isToday = false;
            bool cashboxClosed = false;

            if (rctCaptureDate != DateTime.MinValue)
            {
                //is the capture date of the original receipt today?
                isToday = (rctCaptureDate.Year == DateTime.Today.Year && rctCaptureDate.Month == DateTime.Today.Month && rctCaptureDate.Day == DateTime.Today.Day);

                //check if the cashbox was closed after the receipt was captured: cbCashBoxDate >= rctCaptureDate
                //  and prior to the attempted reversal: cbCashBoxDate <= DateTime.Now()

                //is the cashbox closed?
                if (cbDateClosed != DateTime.MinValue)
                {
                    cashboxClosed = true;
                }
                else
                {
                    //the cashbox is still open - was it opened after the receipt was captured?
                    if (cbDateOpened > rctCaptureDate)
                        cashboxClosed = true;
                }
            }

            //do not allow admin reversals if the same day and the cashbox has not been closed ==> they must use reveral screen
            if (isToday && !cashboxClosed)
            {
                e.Row.Cells[10].Enabled = false;
                e.Row.Cells[10].Text = (string)GetLocalResourceObject("dgTicketDetailsCell.Text");
            }
        }

        private void GetReceiptDetails()
        {
            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            SqlParameter totalCount;
            SqlDataReader reader = db.GetReceiptDetailsForReversal(this.rctIntNo, grdReceiptPager.PageSize, grdReceiptPager.CurrentPageIndex, out totalCount);

            if (!reader.HasRows)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                reader.Close();
                return;
            }

            this.lblError.Text = string.Empty;
            this.grdReceipt.DataSource = reader;
            this.grdReceipt.DataKeyNames = new string[] { "RctIntNo", "RTIntNo", "ChgIntNo", "NotIntNo", "CBIntNo", "RTAmount" };
            this.grdReceipt.DataBind();
            this.grdReceipt.Visible = true;
            grdReceiptPager.RecordCount = (int)(totalCount.Value == DBNull.Value ? 0 : totalCount.Value);

            this.pnlReceiptDetails.Visible = true;
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            if (this.grdReceipt.Rows.Count == 0)
                return;

            //dls 080506 - only allow bounced cheques for transactions that include cheque payments
            rdlReason.Items[1].Enabled = false;

            decimal total = 0;
            CashType cashType = CashType.Cash;
            string message = string.Empty;
            StringBuilder sb = new StringBuilder();

            string rctNumber = this.grdReceipt.Rows[0].Cells[1].Text;

            sb.Append((string)GetLocalResourceObject("lblConfirmMessage.Text") + " <ul>");

            foreach (GridViewRow row in this.grdReceipt.Rows)
            {
                string notTicketNo = ((Label)row.FindControl("lblTicketNo")).Text;

                sb.Append("<li>");
                sb.Append(notTicketNo);
                sb.Append("   [");

                string cashTypeString = ((Label)row.FindControl("lblCashType")).Text;
                if (cashTypeString.Length == 0)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                    return;
                }

                sb.Append(cashTypeString);
                sb.Append("]   R ");

                // tlp 20081201
                // Add test for null before Parse to avoid "Object not set to ..." error
                if (((Label)row.FindControl("lblAmount")).Text == null || ((Label)row.FindControl("lblAmount")).Text == string.Empty)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                    return;
                }

                // tlp 20081201 - test for valid decimal
                decimal amount;
                if (!decimal.TryParse(((Label)row.FindControl("lblAmount")).Text, out amount))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    return;
                }

                //calculate the amount based on the total of the amount columns
                amount = decimal.Parse(((Label)row.FindControl("lblAmount")).Text);
                total += amount;

                //update by Rachel 20140819 for 5337
                //sb.Append(string.Format("{0:0.00}", amount.ToString()));
                sb.Append(string.Format(CultureInfo.InvariantCulture, "{0:0.00}", amount));
                //end update by Rachel 20140819 for 5337

                sb.Append("</li>\n");

                cashType = (CashType)Enum.Parse(typeof(CashType), cashTypeString, true);

                if (cashType == CashType.Cheque)
                    rdlReason.Items[1].Enabled = true;
            }

            //display amount of receipt in text box

            //update by Rachel 20140819 for 5337
            //txtCashAmount.Text = total.ToString();
            txtCashAmount.Text = total.ToString(CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337
            txtCashAmount.ReadOnly = true;

            sb.Append("</ul>");

            ViewState.Add("AreYouSure", sb.ToString());

            this.btnConfirm.Visible = true;

            this.chkAreYouSure.Checked = false;
            this.chkAreYouSure.Visible = true;

            ViewState.Add("AreYouSure", (string)GetLocalResourceObject("lblConfirmMessage.Text1") + " # " + rctNumber.ToString() + "?<ul>" + sb.ToString());

            this.lblConfirmMessage.Text = ViewState["AreYouSure"].ToString();
            this.lblConfirmMessage.Visible = true;

            this.btnConfirm.Enabled = chkAreYouSure.Checked;

            this.pnlConfirm.Visible = true;
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            decimal amount;

            if (!decimal.TryParse(this.txtCashAmount.Text, out amount))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                return;
            }

            // SD: 20081204 - test for null value
            StringBuilder sb = new StringBuilder();
            if ((this.txtCashAmount.Text == null) && (this.txtCashAmount.Text.Trim().Length < 0))
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblConfirmMessage.Text2") + "</li>\n");
                sb.Append("</ul>");
                this.lblError.Text = sb.ToString();
            }

            if (amount < 1)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                return;
            }

            CashReceiptDB db = new CashReceiptDB(this.connectionString);

            string reason = string.Empty;

            switch (rdlReason.SelectedValue)
            {
                case "0":
                    reason = (string)GetLocalResourceObject("rdlReasonItem.Text");
                    break;
                case "1":
                    reason = (string)GetLocalResourceObject("rdlReasonItem.Text1");
                    break;
            }

            KeyValuePair<string, int> value = db.ProcessAdminReceiptReversal(this.rctIntNo, loginUser, reason, this.autIntNo);

            if (value.Key.Length > 0 && value.Value <= 0)
            {
                this.lblError.Text = value.Key;
                return;
            }
            // Jake 2013-12-02 add display error message from sp
            if (value.Key == "" && value.Value == -20)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text20");
                return;
            }
             
            //dls 090701 - moved the udpate of the SumComments to the SP ReceiptReversalAdmin
            //// 20090531 tf Update column SumComments in Summons
            //SummonsDB summonsDb = new SummonsDB(this.connectionString);
            //foreach (GridViewRow row in this.grdReceipt.Rows)
            //{
            //    summonsDb.UpdateSummonsComments(((Label)row.FindControl("lblTicketNo")).Text, "Case not finalized �C payment of R rrrr.cc was received on yyyy/mm/dd HH:MM (receipt # 999999)", loginUser);
            //}

            string newRctIntNo = value.Value.ToString();
            string newRctNumber = value.Key.ToString();

            ViewState.Add("NewRctIntNo", newRctIntNo);
            ViewState.Add("NewRctNumber", newRctNumber);

            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            //Linda 2012-9-12 add for list report19
            //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.loginUser, PunchStatisticsTranTypeList.PaymentTrafficCancel, PunchAction.Add);           
            
            this.pnlConfirm.Visible = false;
            this.pnlDisplaySelectedTickets.Visible = false;

            pnlNext.Visible = true;

            if (chkApply.Checked)
            {
                btnReApplyPayments.Visible = true;
            }
            else
            {
                btnReApplyPayments.Visible = false;
            }

        }

        protected void chkAreYouSure_CheckedChanged(object sender, EventArgs e)
        {
            btnConfirm.Enabled = chkAreYouSure.Checked;
        }

        protected void dgTicketDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int nRow = Convert.ToInt32(e.CommandArgument);

            this.rctIntNo = (int)this.dgTicketDetails.DataKeys[nRow].Values["RctIntNo"];

            ViewState.Add("RctIntNo", this.rctIntNo);

            lblReceiptDetails.Text = (string)GetLocalResourceObject("lblReceiptDetails.Text1") + " # " + txtTicketNo.Text;

            grdReceiptPager.RecordCount = 0;
            grdReceiptPager.CurrentPageIndex = 1;
            GetReceiptDetails();

            this.pnlDisplaySelectedTickets.Visible = false;
        }

        protected void rdlReason_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdlReason.SelectedValue.Equals("0"))
                chkApply.Visible = true;
            else
                chkApply.Visible = false;
        }

        protected void btnShowNotApplied_Click(object sender, EventArgs e)
        {
            this.pnlNotReApplied.Visible = true;
            // 2013-03-27 Henry add for pagination
            grvNotAppliedPager.RecordCount = 0;
            grvNotAppliedPager.CurrentPageIndex = 1;
            BindNotAppliedGrid();
        }

        private void BindNotAppliedGrid()
        {
            CashReceiptDB payment = new CashReceiptDB(this.connectionString);

            int totalCount = 0;
            grvNotApplied.DataSource = payment.GetAdminReversalNotAppliedDS(autIntNo, grvNotAppliedPager.PageSize, grvNotAppliedPager.CurrentPageIndex, out totalCount);
            grvNotApplied.DataKeyNames = new string[] { "ARRIntNo", "RctIntNo", "ARRAmount", "RctNumber" };
            grvNotApplied.DataBind();
            grvNotAppliedPager.RecordCount = totalCount;

            if (grvNotApplied.Rows.Count > 0)
            {
                grvNotApplied.Visible = true;
            }
            else
            {
                grvNotApplied.Visible = false;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                lblError.Visible = true;
            }
        }

        protected void grvNotApplied_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int nRow = Convert.ToInt32(e.CommandArgument);

            this.rctIntNo = (int)this.grvNotApplied.DataKeys[nRow].Values["RctIntNo"];
            decimal rctAmount = (decimal)this.grvNotApplied.DataKeys[nRow].Values["ARRAmount"];
            this.rctNumber = this.grvNotApplied.DataKeys[nRow].Values["RctNumber"].ToString();

            PageToOpen page = new PageToOpen(this, "AdminReceiptTraffic.aspx");
            Session.Add("AdminReceiptReversal", true);
            Session.Add("AdminRctIntNo", this.rctIntNo);
            Session.Add("AdminRctAmount", rctAmount);
            Session.Add("AdminRctNumber", this.rctNumber);

            //PunchStats805806 enqiry AdminReceiptReversal
            Response.Redirect(page.ToString());
        }

        protected void btnReApplyPayments_Click(object sender, EventArgs e)
        {
            string newRctIntNo = ViewState["NewRctIntNo"].ToString();
            string newRctNumber = ViewState["NewRctNumber"].ToString();

            PageToOpen page = new PageToOpen(this, "AdminReceiptTraffic.aspx");
            Session.Add("AdminReceiptReversal", true);
            Session.Add("AdminRctIntNo", newRctIntNo);
            Session.Add("AdminRctAmount", this.txtCashAmount.Text);
            Session.Add("AdminRctNumber", newRctNumber);
            Response.Redirect(page.ToString());
        }

        protected string GetPostalReceiptRule()
        {
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = this.autIntNo;
            ard.ARCode = "4510";
            ard.LastUser = this.loginUser;

            DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
            KeyValuePair<int, string> postalReceiptRule = authRule.SetDefaultAuthRule();
            return postalReceiptRule.Value;
        }

        protected void btnPrintReversalReceipt_Click(object sender, EventArgs e)
        {
            string newRctIntNo = ViewState["NewRctIntNo"].ToString();
            string newRctNumber = ViewState["NewRctNumber"].ToString();

            // Open a new window with the receipt report and print it 
            StringBuilder sb = new StringBuilder();
            //we will only ever reverse OnError receipt attribute a time
            sb.Append(newRctIntNo);
            sb.Append(',');

            if (alwaysUsePostalReceiptReport.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
            {
                Helper_Web.BuildPopup(this, "CashReceiptTraffic_PostalReceiptViewer.aspx", "date", DateTime.Now.ToString("yyyy-MM-dd"), "CBIntNo", "0", "RCtIntNo", sb.ToString());
            }
            else
            {
                Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", sb.ToString());
            }
            //2013-12-11 Heidi added for add all Punch Statistics Transaction(5084)
            //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            //punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.loginUser, PunchStatisticsTranTypeList.AdminReceiptReversal, PunchAction.Other);  
        }
        // 2013-03-27 Henry add for pagination
        protected void dgTicketDetailsPager_PageChanged(object sender, EventArgs e)
        {
            LookForNotice();
        }
        // 2013-03-27 Henry add for pagination
        protected void grvNotAppliedPager_PageChanged(object sender, EventArgs e)
        {
            BindNotAppliedGrid();
        }
        // 2013-03-27 Henry add for pagination
        protected void grdReceiptPager_PageChanged(object sender, EventArgs e)
        {
            GetReceiptDetails();
        }

        protected void linkToNoTrafficChargePymt_Click(object sender, EventArgs e)
        {
            string aartoDomain = WebUtility.GetAartoDomainName();
            Response.Redirect(aartoDomain + "/QuoteManagement/AdminReversalForNTC");
        }
    }
}
