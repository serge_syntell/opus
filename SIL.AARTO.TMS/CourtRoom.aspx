<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.CourtRoom" CodeBehind="CourtRoom.aspx.cs" %>

<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
    <style type="text/css">
        .style1 {
            width: 182px;
        }

        .style2 {
            height: 26px;
            width: 182px;
        }

        .style3 {
            width: 102px;
        }

        .style4 {
            width: 146px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img height="26" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" Text="<%$Resources:lblOptions.Text %>" CssClass="ProductListHead"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="100%" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="100%" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" Text="<%$Resources:lblPageName.Text %>" runat="server" Width="100%" CssClass="ContentHead"></asp:Label>
                        </p>
                        <p>
                            &nbsp;
                        </p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                                <table id="tblControls" border="0" class="NormalBold" style="width: 554px">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;         
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label ID="lblSelect" runat="server" Text="<%$Resources:lblSelect.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlCourt" runat="server" CssClass="Normal" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlCourt_SelectedIndexChanged" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label ID="lblCourtRoom" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCourtRoom.Text %>" Visible="true"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlCourtRoom" runat="server" CssClass="Normal" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlCourtRoom_SelectedIndexChanged" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label ID="lblRoomNo" runat="server" CssClass="NormalBold" Visible="false" Text="<%$Resources:lblRoomNo.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRoomNo" runat="server" CssClass="Normal" MaxLength="5" Width="200px" Visible="false"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regValTxtRoomNo" runat="server" ValidationExpression="[0-9,a-z,A-Z]{1,5}" Visible="false" ControlToValidate="txtRoomNo" CssClass="NormalRed" ErrorMessage="<%$Resources:CourtRoomError %>"></asp:RegularExpressionValidator></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label ID="lblRoomName" runat="server" CssClass="NormalBold" Visible="true" Text="<%$Resources:lblRoomName.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRoomName" runat="server" CssClass="Normal" MaxLength="50" Width="200px" Visible="true"></asp:TextBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table cellspacing="1" cellpadding="0" border="0" width="615" align="center"
                                                bgcolor="#000000" style="width: 613px">
                                                <tr bgcolor='#FFFFFF'>
                                                    <td height="100">
                                                        <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100">
                                                        <uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td height="25" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label runat="server" ID="lblCasePrefx" Visible="true" Text="<%$Resources:lblCasePrefx.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCasePrefix" runat="server" CssClass="Normal" MaxLength="5" Width="200px"></asp:TextBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label runat="server" ID="lblCaseNumber" Visible="true" Text="<%$Resources:lblCaseNumber.Text %>"></asp:Label>

                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNextCaseNo" runat="server" CssClass="Normal" MaxLength="6" Width="200px"></asp:TextBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label runat="server" ID="lblWOANumber" Visible="true" Text="<%$Resources:lblWOANumber.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNextWOANo" runat="server" CssClass="Normal" MaxLength="6" Width="200px"></asp:TextBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label runat="server" ID="lblOfficer" Visible="true" Text="<%$Resources:lblOfficer.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPresidingOfficer" runat="server" CssClass="Normal" MaxLength="50" Width="200px"></asp:TextBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label runat="server" ID="lblProsecutor" Visible="true" Text="<%$Resources:lblProsecutor.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPublicProsecutor" runat="server" CssClass="Normal" MaxLength="50" Width="200px"></asp:TextBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label runat="server" ID="lblClerk" Visible="true" Text="<%$Resources:lblClerk.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtClerk" runat="server" CssClass="Normal" MaxLength="50" Width="200px"></asp:TextBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label runat="server" ID="Label2" Visible="true" Text="<%$Resources:lblInterpreter.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtInterpreter" runat="server" CssClass="Normal" MaxLength="50" Width="200px"></asp:TextBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label runat="server" ID="Label3" Visible="true" Text="<%$Resources:lblActive.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkActive" runat="server" CssClass="Normal"></asp:CheckBox></td>
                                        <td align="center" class="style1"></td>
                                    </tr>
                                    <tr>
                                        <td class="style4">
                                            <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblCrtRStatusFlag.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlCrtRStatusFlag" runat="server" CssClass="Normal" Width="388px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="style4"></td>
                                        <td style="text-align: left">
                                            <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"
                                                Text=" <%$Resources:btnUpdate.Text %> " />&nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="NormalButton" OnClick="btnCancel_Click"
                                                    Text=" <%$Resources:btnCancel.Text %> " />
                                        </td>
                                        <td align="center" class="style2">&nbsp;&nbsp;
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center"></td>
                <td valign="top" align="left" width="100%"></td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
