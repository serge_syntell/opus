using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using System.IO;
using SIL.AARTO.BLL.Utility.PrintFile;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class CourtRollViewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string sRoomNo = string.Empty;
        private string sCrtName = string.Empty;
        private string sReportType = "P";
        RecordBox rb;
        ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPage = "Court Roll Viewer";
        protected string thisPageURL = "CourtRollViewer.aspx";

        #region Oscar 20120203 disabled for backup
        ///// <summary>
        ///// Handles the Load event of the Page control.
        ///// </summary>
        ///// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //protected override void OnLoad(EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    // Get user details
        //    Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
        //    Stalberg.TMS.UserDetails userDetails = new UserDetails();

        //    userDetails = (UserDetails)Session["userDetails"];
        //    autIntNo = Convert.ToInt32(Session["autIntNo"]);

        //    // Set domain specific variables
        //    General gen = new General();
        //    backgroundImage = gen.SetBackground(Session["drBackground"]);
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    // Setup the report
        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);
        //    string reportPage = arn.GetAuthReportName(autIntNo, "CourtRoll");
        //    if (reportPage.Equals(string.Empty))
        //    {
        //        int arnIntNo = arn.AddAuthReportName(autIntNo, "CourtRoll.dplx", "CourtRoll", "system", string.Empty);
        //        reportPage = "CourtRoll.dplx";
        //    }

        //    string path = Server.MapPath("reports/" + reportPage);

        //    //****************************************************
        //    //SD:  20081120 - check that report actually exists
        //    string templatePath = string.Empty;
        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "CourtRoll");

        //    if (!File.Exists(path))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        //dls 081117 - we can only check that the template path exists if there is actually a template!
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //****************************************************
        //    DocumentLayout doc = new DocumentLayout(path);
        //    sReportType = Request.QueryString["ReportType"] == null ? "P" : Request.QueryString["ReportType"].ToString();
        //    int nCrtIntNo = Request.QueryString["CrtIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["CrtIntNo"].ToString());
        //    int nCrtRIntNo = Request.QueryString["CrtRIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["CrtRIntNo"].ToString());
        //    DateTime dtCourtDate;
        //    DateTime dtFinalPrintDate;

        //    if (!DateTime.TryParse(Request.QueryString["CourtDate"], out dtCourtDate))
        //    {
        //        string error = "Please select a valid court date";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);
        //        Response.Redirect(errorURL);
        //        return;
        //    }

        //    if (!DateTime.TryParse(Request.QueryString["FinalPrintDate"], out dtFinalPrintDate))
        //    {
        //        string error = "Please select a valid final print date";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);
        //        Response.Redirect(errorURL);
        //        return;
        //    }

        //    //jerry 2011-11-04 add
        //    int courtRollType = Convert.ToInt32(Request.QueryString["CourtRollType"].ToString());

        //    rb = (RecordBox)doc.GetElementById("rbCaseNo");
        //    lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCaseNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblLAName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblLAName");

        //    //if (sReportType != "F")
        //    //{
        //    //    lblReportType.Text = "(Prep)";
        //    //    lbl.TextColor = ceTe.DynamicPDF.CmykColor.White;
        //    //}
        //    //else
        //    //    lblReportType.Text = "(Final)";

        //    // 2010/9/27 Paole changed for lblLAName value from CourtRoll_Main01.dplx
        //   // lblLAName.Text = Session["autName"].ToString();

        //    rb.LayingOut += new LayingOutEventHandler(rb_LayingOut);

        //    // We run 4 types of reports and then merge them into one using the ceTe Merge functionality

        //    // run the query for S54 and Personal service type reports
        //    Query query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;
        //    ParameterDictionary parameters = new ParameterDictionary();
        //    parameters.Add("AutIntNo", autIntNo);
        //    parameters.Add("CrtIntNo", nCrtIntNo);
        //    parameters.Add("CrtRIntNo", nCrtRIntNo);
        //    parameters.Add("Type", "S54");
        //    parameters.Add("SumServedStatus", 1);
        //    parameters.Add("SumCourtDate", dtCourtDate);
        //    parameters.Add("FinalPrintDate", dtFinalPrintDate);
        //    parameters.Add("FinalFlag", sReportType);
        //    parameters.Add("CourtRollType", courtRollType);

        //    Document reportA = doc.Run(parameters);

        //    // run the query for S54 and Impersonal service type reports
        //    query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;
        //    parameters = new ParameterDictionary();
        //    parameters.Add("AutIntNo", autIntNo);
        //    parameters.Add("CrtIntNo", nCrtIntNo);
        //    parameters.Add("CrtRIntNo", nCrtRIntNo);
        //    parameters.Add("Type", "S54");
        //    parameters.Add("SumServedStatus", 2);
        //    parameters.Add("SumCourtDate", dtCourtDate);
        //    parameters.Add("FinalPrintDate", dtFinalPrintDate);
        //    parameters.Add("FinalFlag", sReportType);
        //    parameters.Add("CourtRollType", courtRollType);

        //    Document reportB = doc.Run(parameters);

        //    // run the query for S56 and Personal service type reports
        //    query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;
        //    parameters = new ParameterDictionary();
        //    parameters.Add("AutIntNo", autIntNo);
        //    parameters.Add("CrtIntNo", nCrtIntNo);
        //    parameters.Add("CrtRIntNo", nCrtRIntNo);
        //    parameters.Add("Type", "S56");
        //    parameters.Add("SumServedStatus", 1);
        //    parameters.Add("SumCourtDate", dtCourtDate);
        //    parameters.Add("FinalPrintDate", dtFinalPrintDate);
        //    parameters.Add("FinalFlag", sReportType);
        //    parameters.Add("CourtRollType", courtRollType);

        //    Document reportC = doc.Run(parameters);
        //    byte[] bufferA = reportA.Draw();
        //    byte[] bufferB = reportB.Draw();
        //    byte[] bufferC = reportC.Draw();

        //    //20090514 tf set the pdf report with consecutive page numbers, and discard impersonal of S56 
        //    //MergeDocument document = new MergeDocument(new PdfDocument(bufferA));

        //    //dls 2010-11-16 - if A is blank, then we don't want to merge in a blank page
        //    byte[] bufferMain;

        //    if (bufferA.Length > 1488)
        //    {
        //        if (bufferB.Length > 1488)
        //            reportA.Pages.Append(reportB.Pages);
        //        if (bufferC.Length > 1488)
        //            reportA.Pages.Append(reportC.Pages);

        //        bufferMain = reportA.Draw();
        //    }
        //    else if (bufferB.Length > 1488)
        //    {
        //        if (bufferC.Length > 1488)
        //            reportB.Pages.Append(reportC.Pages);

        //        bufferMain = reportB.Draw();
        //    }
        //    else
        //    {
        //        bufferMain = reportC.Draw();
        //    }

        //    //byte[] bufferMain = new byte[bufferA.Length + bufferB.Length]; ;
        //    //bufferA.CopyTo(bufferMain,0);
        //    //bufferB.CopyTo(bufferMain, bufferA.Length);

        //    rb.LayingOut -= new LayingOutEventHandler(rb_LayingOut);

        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/pdf";
        //    Response.BinaryWrite(bufferMain);
        //    Response.End();
        //}

        //private void rb_LayingOut(object sender, LayingOutEventArgs e)
        //{
        //    if (sReportType == "P")
        //        rb.TextColor = ceTe.DynamicPDF.CmykColor.White;
        //}
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            this.backgroundImage = gen.SetBackground(Session["drBackground"]);
            this.styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            this.title = gen.SetTitle(Session["drTitle"]);

            PrintFileProcess process = new PrintFileProcess(this.connectionString);
            process.BuildPrintFile(
                new PrintFileModuleCourtRoll(CourtRollType.Summary, Request.QueryString["ReportType"], Request.QueryString["CrtIntNo"], Request.QueryString["CrtRIntNo"], Request.QueryString["CourtDate"], Request.QueryString["FinalPrintDate"], Request.QueryString["CourtRollType"]),
                this.autIntNo
            );
        }
    }
}