using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;
using System.Collections.Generic;
using SIL.AARTO.DAL.Services;


namespace Stalberg.TMS
{

    public partial class SysParam : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        protected string thisPageURL = "SysParam.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "System parameter maintenance (System Admin only)";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;

            //Heidi 2014-03-25 Commented out (5141)
            //may need to check user access level here....
            // if (userAccessLevel < 10)
            //Server.Transfer(Session["prevPage"].ToString());
            // Server.Transfer("Default.aspx");

            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                //pnlAddSysParam.Visible = false;
                pnlUpdateSysParam.Visible = false;

                //btnOptDelete.Visible = false;
                //btnOptAdd.Visible = true;
                btnOptHide.Visible = true;

                BindGrid();
            }
        }

        protected void BindGrid()
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.SysParamDB szList = new Stalberg.TMS.SysParamDB(connectionString);

            DataSet data = szList.GetSysParamListDS();
            dgSysParam.DataSource = data;
            dgSysParam.DataKeyField = "SPIntNo";
            dgSysParam.DataBind();

            if (dgSysParam.Items.Count == 0)
            {
                dgSysParam.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgSysParam.Visible = true;
            }

            data.Dispose();
            //pnlAddSysParam.Visible = false;
            pnlUpdateSysParam.Visible = false;
        }

        //protected void btnOptAdd_Click(object sender, System.EventArgs e)
        //{
        //    // allow transactions to be added to the database table
        //    pnlAddSysParam.Visible = true;
        //    pnlUpdateSysParam.Visible = false;

        //    btnOptDelete.Visible = false;
        //}

        //protected void btnAddSysParam_Click(object sender, System.EventArgs e)
        //{		
        //    // add the new transaction to the database table
        //    Stalberg.TMS.SysParamDB szAdd = new SysParamDB(connectionString);

        //    lblError.Visible = true;

        //    int intValue = 0;

        //    try
        //    {
        //        intValue = Convert.ToInt32(txtAddSPIntegerValue.Text);
        //    }
        //    catch 
        //    {
        //        lblError.Text = "Please enter a numeric value in teh integer value field, or 0 if not applicable";
        //        return;
        //    }

        //    int addSPIntNo = szAdd.AddSysParam(txtAddSPColumnName.Text, intValue, 
        //        txtAddSPStringValue.Text, txtAddSPDescr.Text, loginUser);

        //    if (addSPIntNo <= 0)
        //    {
        //        lblError.Text = "Unable to add this system parameter";
        //    }
        //    else
        //    {
        //        lblError.Text = "System parameter has been added";
        //    }


        //    BindGrid();
        //}

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            int spIntNo = Convert.ToInt32(Session["editSPIntNo"]);

            // add the new transaction to the database table
            Stalberg.TMS.SysParamDB szUpdate = new SysParamDB(connectionString);

            lblError.Visible = true;

            int intValue = 0;

            try
            {
                intValue = Convert.ToInt32(txtSPIntegerValue.Text);
            }
            catch
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            using (ConnectionScope.CreateTransaction())
            {
                int updSPIntNo = szUpdate.UpdateSysParam(spIntNo, txtSPColumnName.Text, intValue, txtSPStringValue.Text,
                    txtSPDescr.Text, loginUser);

                //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(spIntNo, lgEntityList, "SysParamLookup", "SPIntNo", "SPDescr", this.loginUser);

                if (updSPIntNo <= 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");

                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SystemParametersMaintenance, PunchAction.Change);

                }
                ConnectionScope.Complete();
            }

            lblError.Visible = true;

            BindGrid();
        }

        protected void dgSysParam_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgSysParam.SelectedIndex = e.Item.ItemIndex;

            if (dgSysParam.SelectedIndex > -1)
            {
                int editSPIntNo = Convert.ToInt32(dgSysParam.DataKeys[dgSysParam.SelectedIndex]);

                Session["editSPIntNo"] = editSPIntNo;
                Session["prevPage"] = thisPageURL;

                ShowSysParamDetails(editSPIntNo);

                //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editSPIntNo.ToString(), "SysParamLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);
            }
        }

        protected void ShowSysParamDetails(int editSPIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.SysParamDB sysParam = new Stalberg.TMS.SysParamDB(connectionString);

            Stalberg.TMS.SysParamDetails spDetails = sysParam.GetSysParamDetails(editSPIntNo);

            txtSPColumnName.Text = spDetails.SPColumnName;
            txtSPStringValue.Text = spDetails.SPStringValue;
            txtSPIntegerValue.Text = spDetails.SPIntegerValue.ToString();
            txtSPDescr.Text = spDetails.SPDescr;

            pnlUpdateSysParam.Visible = true;
            //pnlAddSysParam.Visible  = false;

            //btnOptDelete.Visible = true;
        }


        protected void dgSysParam_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgSysParam.CurrentPageIndex = e.NewPageIndex;

            BindGrid();
        }

        //protected void btnOptDelete_Click(object sender, System.EventArgs e)
        //{
        //    int spIntNo = Convert.ToInt32(Session["editSPIntNo"]);

        //    SysParamDB sysParam = new Stalberg.TMS.SysParamDB(connectionString);

        //    string delSPIntNo = sysParam.DeleteSysParam(spIntNo);

        //    if (delSPIntNo.Equals("0"))
        //    {
        //        lblError.Text = "Unable to delete this system parameter";
        //    }
        //    else if (delSPIntNo.Equals("-1"))
        //    {
        //        lblError.Text = "Unable to delete this system parameter - it has been linked to locations";
        //    }
        //    else
        //        lblError.Text = "System parameter has been deleted";		

        //    lblError.Visible = true;

        //    BindGrid();
        //}

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgSysParam.Visible.Equals(true))
            {
                //hide it
                dgSysParam.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("lblError.Text4");
            }
            else
            {
                //show it
                dgSysParam.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }



    }
}
