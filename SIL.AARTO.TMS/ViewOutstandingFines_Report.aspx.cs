using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using System.IO;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class ViewOutstandingFines_Report : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "View Outstanding Fines Report";
        protected string thisPageURL = "ViewOutstandingFines_Report.aspx";

        //Document report = null;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);


            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            NoticeQueryCriteria criteria = (NoticeQueryCriteria)Session["NoticeQueryCriteria"];
           
            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "OutstandingFines");
            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "OutstandingFines.dplx", "OutstandingFines", "system", string.Empty);
                reportPage = "OutstandingFines.dplx";
            }

            string path = Server.MapPath("reports/" + reportPage);

            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            DocumentLayout doc = new DocumentLayout(path);

            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("AutIntNo", criteria.AutIntNo  );
            parameters.Add("ColumnName", criteria.ColumnName.Trim() );
            parameters.Add("ColumnValue", criteria.Value.Trim() );
            parameters.Add("DateSince", criteria.DateSince);
            parameters.Add("NoAog", "N");


            //if (reportPage.IndexOf("_ST") < 0)
            //{
            //    AuthorityDB db = new AuthorityDB(connectionString);
            //    AuthorityDetails authDet = new AuthorityDetails();
            //    authDet = db.GetAuthorityDetails(autIntNo);

            //    RecordArea recordAuthorityName = (RecordArea)doc.GetElementById("recordAuthorityName");
            //    recordAuthorityName.Text = authDet.AutName;
            //    RecordArea recordAuthorityAddress = (RecordArea)doc.GetElementById("recordAuthorityAddress");
            //    string sDetails = authDet.AutPhysAddr1.Trim() + " " + authDet.AutPhysAddr2.Trim() + " " + authDet.AutPhysAddr3.Trim() + " " + authDet.AutPhysAddr4.Trim()
            //    + " " + authDet.AutPhysCode.Trim() + " - " + authDet.AutPostAddr1.Trim() + " " + authDet.AutPostAddr2.Trim() + " " + authDet.AutPostAddr3.Trim() 
            //    + " " + authDet.AutPostCode.Trim() + " - TEL: " + authDet.AutTel.Trim() + " - FAX:" + authDet.AutFax.Trim() + " - EMAIL: " + authDet.AutEmail.Trim();
            //    recordAuthorityAddress.Text = sDetails.ToUpper ();
            //}

            Document reportA = doc.Run(parameters);
            byte[] bufferA = reportA.Draw();

            query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            parameters = new ParameterDictionary();
            parameters.Add("AutIntNo", criteria.AutIntNo);
            parameters.Add("ColumnName", criteria.ColumnName.Trim());
            parameters.Add("ColumnValue", criteria.Value.Trim());
            parameters.Add("DateSince", criteria.DateSince);
            parameters.Add("NoAog", "Y");

            RecordBox rbTotal = (RecordBox)doc.GetElementById("rbTotal");
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblTotal = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblTotal");
            lblTotal.Text = (string)GetLocalResourceObject("lblTotal.Text");
            rbTotal.TextColor = ceTe.DynamicPDF.CmykColor.White;

           
            Document reportB = doc.Run(parameters);
            byte[] bufferB = reportB.Draw();

            MergeDocument document = new MergeDocument(new PdfDocument(bufferA));
            if (bufferB .Length > 1144)
                document.Append(new PdfDocument(bufferB));

            byte[] bufferMain = document.Draw();

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(bufferMain);
            Response.End();
            
        }

     

    }
}