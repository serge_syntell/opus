using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    public partial class ProcessLogReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "ProcessLogReport.aspx";
        //protected string thisPage = "Process Log Report";

        private const string DATE_FORMAT = "yyyy-MM-dd";


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.dtpAfter.Text = DateTime.Now.AddMonths(-3).ToString(DATE_FORMAT);
                this.dtpBefore.Text = DateTime.Now.ToString(DATE_FORMAT);
            }
        }

        

        protected void btnView_Click(object sender, EventArgs e)
        {
            this.lblError.Text = string.Empty;
            BindGrid();
            //PunchStats805806 enquiry ProcessLogReport
        }

        protected void BindGrid()
        {
            DateTime dtStart;
            DateTime dtEnd;
            String sSearch = string.Empty;

            if (!DateTime.TryParse(this.dtpAfter.Text, out dtStart))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                return;
            }

            if (!DateTime.TryParse(this.dtpBefore.Text, out dtEnd))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }

            if (Convert.ToDateTime(dtpAfter.Text) > Convert.ToDateTime(dtpBefore.Text))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            dtStart = DateTime.Parse(this.dtpAfter.Text);
            dtEnd = DateTime.Parse(this.dtpBefore.Text);

            ProcessLogDB db = new ProcessLogDB(connectionString);
            sSearch = this.ddlProcess.SelectedItem.Value;
            if (sSearch == "All")
                sSearch = string.Empty;

            DataSet data = db.GetProcessLogData(sSearch, dtStart, dtEnd);
            dgProcessLog.DataSource = data;
            dgProcessLog.DataKeyField = "PLIntNo";

            try
            {
                dgProcessLog.DataBind();
            }
            catch
            {
                dgProcessLog.CurrentPageIndex = 0;
                dgProcessLog.DataBind();
            }

            if (dgProcessLog.Items.Count == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                lblError.Visible = true;
                dgProcessLog.Visible = false;
                return;
            }

            data.Dispose();

            lblError.Visible = false;
            dgProcessLog.Visible = true;
        }

        protected void dgProcessLog_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgProcessLog.CurrentPageIndex = e.NewPageIndex;
            BindGrid();
        }

    }
}


