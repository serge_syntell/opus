using System;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.IO;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for Court Judgement(s)
    /// </summary>
    public partial class CourtJudgementRecordViewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        protected string title = string.Empty;
        private string sRoomNo = string.Empty ;
        private string sCrtName = string.Empty;
        protected string thisPageURL = "CourtJudgementRecordViewer.aspx";
        //protected string thisPage = "Court Judgement Record Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string description = String.Empty;
        protected string loginUser = string.Empty;
        
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "CourtJudgementRecord");
            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "CourtJudgementRecord.dplx", "CourtJudgementRecord", "system", string.Empty);
                reportPage = "CourtJudgementRecord.dplx";
            }

            string path = Server.MapPath("reports/" + reportPage);

            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            DocumentLayout doc = new DocumentLayout(path);

            int nCrtIntNo = Request.QueryString["CrtIntNo"] == null ? 0 : Convert.ToInt32( Request.QueryString["CrtIntNo"].ToString());
            int nCrtRIntNo = Request.QueryString["CrtRIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["CrtRIntNo"].ToString());
            DateTime dtCourtDate;
            DateTime.TryParse(Request.QueryString["CourtDate"].ToString(), out dtCourtDate);
            
            // run the query 
            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("AutIntNo", autIntNo);
            parameters.Add("CrtIntNo", nCrtIntNo);
            parameters.Add("CrtRIntNo", nCrtRIntNo);
            parameters.Add("SumCourtDate", dtCourtDate);
              
            Document reportA = doc.Run(parameters);
       
            byte[] bufferA = reportA.Draw();

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, userDetails.UserLoginName, PunchStatisticsTranTypeList.CourtJudgementRecord, PunchAction.Other); 

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(bufferA);
            Response.End();

            

        }
     
    }
}