﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Stalberg.TMS;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Util;
using System.Collections.Generic;
using Stalberg.TMS.Data;

public partial class Image : System.Web.UI.Page
{
    private string connectionString;
    private string imageFolder = "";

    protected override void OnInit(EventArgs e)
    {
        this.connectionString = Application["constr"].ToString();
        //this.useJp2 = (bool)Application["UseJP2Conversion"];

        //if (Session["CheckFileSystemForImages"] != null)
        //{
        //    checkFileSystemForImages = (bool)Session["CheckFileSystemForImages"];
        //}

        if (Application["imageFolder"] != null)
        {
            imageFolder = Application["imageFolder"].ToString();
        }
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Process();
    }

    private Bitmap NoticeImageProcess()
    {
        Bitmap image = null;
        try
        {
            string imageColumn = HttpUtility.UrlDecode(Request.QueryString["ImageColumn"]);
            //string sql = "SELECT " + imageColumn + " FROM Notice WHERE NotIntNo = " + Int32.Parse(HttpUtility.UrlDecode(Request.QueryString["NotIntNo"]));
            string sql = "SELECT " + imageColumn + ", HasData = CASE WHEN DATALENGTH(" + imageColumn + ") = 0 THEN 0 ELSE 1 END FROM Notice WHERE NotIntNo = "
                + Int32.Parse(HttpUtility.UrlDecode(Request.QueryString["NotIntNo"]));
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand myCommand = new SqlCommand(sql, myConnection);

            //dls 100713 - added for timing out of 3Mb images
            myCommand.CommandTimeout = 0;
            
            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            byte[] imageData;
            while (result.Read())
            {
                //if (!result[imageColumn].Equals(System.DBNull.Value))
                if (result["HasData"].ToString().Equals("1"))
                {
                    imageData = (byte[])result[imageColumn];
                    if (imageData != null)
                    {
                        image = Normalise(imageData);
                    }
                    else
                    {
                        Bitmap bitmap = new Bitmap(Server.MapPath("images/NoImage.jpg"));
                        MemoryStream ms = new MemoryStream();
                        bitmap.Save(ms, ImageFormat.Jpeg);

                        image = Normalise(imageData);
                    }
                }
                else
                {
                    Bitmap bitmap = new Bitmap(Server.MapPath("images/NoImage.jpg"));
                    MemoryStream ms = new MemoryStream();
                    bitmap.Save(ms, ImageFormat.Jpeg);

                    image = Normalise(ms.ToArray());
                }
            }
            result.Close();
        }
        catch
        {
        }
        return image;
    }

    private Bitmap ImagePreProcess()
    {
        Bitmap image = null;
        //if (!String.IsNullOrEmpty(HttpUtility.UrlDecode(Request.QueryString["NotIntNo"])))
        //{
        //    image = NoticeImageProcess();
        //}
        //else
        //{
            //if (!String.IsNullOrEmpty(Request.QueryString["JpegName"]) && File.Exists(Server.MapPath(imageFolder) + "\\" + HttpUtility.UrlDecode(Request.QueryString["FilmNo"]) + "\\" + HttpUtility.UrlDecode(Request.QueryString["JpegName"])))
            //{
            //    System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(Server.MapPath(imageFolder) + "\\" + HttpUtility.UrlDecode(Request.QueryString["FilmNo"]) + "\\" + HttpUtility.UrlDecode(Request.QueryString["JpegName"]));
            //    image = new Bitmap(imageConvert);
            //    imageConvert.Dispose();
            //}
            //else
            //{
                string jpegName = HttpUtility.UrlDecode(Request.QueryString["JpegName"]);
                byte[] data = null;
                WebService service = new WebService();
                if (!String.IsNullOrEmpty(Request.QueryString["IFSIntNo"]))
                {
                    int ifsIntNo = Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["IFSIntNo"]));
                    // get test image from ImageFileServer
                    ImageFileServerDB ifsDB = new ImageFileServerDB(connectionString);
                    ImageFileServerDetails testServer = ifsDB.GetImageFileServer(ifsIntNo);
                                        
                    //if (service.RemoteConnect(testServer.ImageMachineName, testServer.ImageShareName, testServer.RemoteUserName, testServer.RemotePassword))
                    //{
                        string strTestImagePath = string.Format(@"\\{0}\{1}\test.jpg", testServer.ImageMachineName, testServer.ImageShareName);

                        if (!File.Exists(strTestImagePath))
                            strTestImagePath = string.Format(@"\\{0}\{1}\test.jpg", testServer.ImageMachineName, testServer.ImageShareName);

                        System.Drawing.Image imageConvert = System.Drawing.Image.FromFile(strTestImagePath);
                        image = new Bitmap(imageConvert);
                        imageConvert.Dispose();
                    //}
                }
                else
                {
                    string strImgType = HttpUtility.UrlDecode(Request.QueryString["ImgType"]);
                    Dictionary<string, byte[]> imageSession = (Dictionary<string, byte[]>)Session["Image" + strImgType];
                    ImageProcesses img = new ImageProcesses(this.connectionString);
                    try
                    {                        
                        if (imageSession != null && imageSession.Keys.Contains(jpegName))
                        {
                            data = imageSession[jpegName];
                        }
                        else
                        {
                            //data = img.ProcessImage(Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["ScImIntNo"])), checkFileSystemForImages, Server.MapPath(imageFolder + "/" + HttpUtility.UrlDecode(Request.QueryString["FilmNo"])), jpegName, Server.MapPath("images/NoImage.jpg"));                        
                            ScanImageDB imageDB = new ScanImageDB(connectionString);
                            int intImageNo = Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["ScImIntNo"]));
                            ScanImageDetails imageDetail = imageDB.GetImageFullPath(intImageNo);
                            if (imageDetail.FileServer != null)
                            {
                                //if (service.RemoteConnect(imageDetail.FileServer.ImageMachineName, imageDetail.FileServer.ImageShareName, imageDetail.FileServer.RemoteUserName, imageDetail.FileServer.RemotePassword))
                                //{
                                    data = img.ProcessImage(imageDetail.ImageFullPath);
                                //}
                            }                            

                            if (jpegName != null)
                                imageSession.Add(jpegName, data);
                        }

                        MemoryStream ms = new MemoryStream(data);
                        System.Drawing.Image imageConvert = System.Drawing.Image.FromStream(ms);
                        image = new Bitmap(imageConvert);
                        imageConvert.Dispose();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.WriteLine(ex);
                        data = null;
                    }
                    finally
                    {

                        if (data == null || data.Length == 0)
                        {
                            string strNoImagePath = Server.MapPath("images/NoImage.jpg");
                            //data = img.ProcessImage(strNoImagePath);
                            System.Drawing.Image imgNoImage = System.Drawing.Image.FromFile(strNoImagePath);
                            image = new Bitmap(imgNoImage);
                            imgNoImage.Dispose();
                        }                        
                    }                                    

                    //if (imageSession == null || !imageSession.Keys.Contains(jpegName))
                    //{                        
                    //    ImageProcesses img = new ImageProcesses(this.connectionString);
                    //    //data = img.ProcessImage(Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["ScImIntNo"])), checkFileSystemForImages, Server.MapPath(imageFolder + "/" + HttpUtility.UrlDecode(Request.QueryString["FilmNo"])), jpegName, Server.MapPath("images/NoImage.jpg"));
                    //    string strNoImagePath = Server.MapPath("images/NoImage.jpg");

                    //    ScanImageDB imageDB = new ScanImageDB(connectionString);
                    //    int intImageNo = Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["ScImIntNo"]));
                    //    ScanImageDetails imageDetail = imageDB.GetImageFullPath(intImageNo);
                    //    if (imageDetail.FileServer != null)
                    //    {
                    //        if (service.RemoteConnect(imageDetail.FileServer.ImageMachineName, imageDetail.FileServer.ImageShareName, imageDetail.FileServer.RemoteUserName, imageDetail.FileServer.RemotePassword))
                    //        {
                    //            data = img.ProcessImage(imageDetail.ImageFullPath);                                
                    //        }
                    //    }

                    //    if (data == null || data.Length == 0)
                    //    {
                    //        data = img.ProcessImage(strNoImagePath);                                
                    //    }
                        
                    //    if (jpegName != null)
                    //        imageSession.Add(jpegName, data);
                    //}
                    //else
                    //{
                    //    data = imageSession[jpegName];
                    //}                   

                    //ImageProcesses img = new ImageProcesses(this.connectionString);
                    //data = img.ProcessImage(Convert.ToInt32(HttpUtility.UrlDecode(Request.QueryString["ScImIntNo"])), checkFileSystemForImages, Server.MapPath(imageFolder + "/" + HttpUtility.UrlDecode(Request.QueryString["FilmNo"])), jpegName, Server.MapPath("images/NoImage.jpg"));
                    //MemoryStream ms = new MemoryStream(data);
                    //System.Drawing.Image imageConvert = System.Drawing.Image.FromStream(ms);
                    //image = new Bitmap(imageConvert);
                    //imageConvert.Dispose();
                //}                
            //}
        }

        return image;
    }

    private void Process()
    {
        try
        {
            using (Bitmap bitmap = ImagePreProcess())
            {
                HandleImage(bitmap);

                if (!string.IsNullOrEmpty(HttpUtility.UrlDecode(Request.QueryString["Invert"])) && HttpUtility.UrlDecode(Request.QueryString["Invert"]) == "1")
                {
                    Invert(bitmap);
                }

                if (bitmap != null)
                {
                    Response.ContentType = "image/jpeg";
                    bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
                    bitmap.Dispose();
                }
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
        finally
        {
            GC.Collect();
        }
    }

    private void HandleImage(Bitmap bitmap)
    {
        bool result = false;
        //if (HttpUtility.UrlDecode(Request.QueryString["ImgType"]) == "A")
        //{
            float contrast;

            if (Request.QueryString["SavedContrast"] != null && !Request.QueryString["SavedContrast"].ToString().Equals("99"))
            {
                result = float.TryParse(HttpUtility.UrlDecode(Request.QueryString["SavedContrast"]), out contrast);
                Session["SavedContrast"] = HttpUtility.UrlDecode(Request.QueryString["SavedContrast"]);
            }
            else
                result = float.TryParse(HttpUtility.UrlDecode(Request.QueryString["Contrast"]), out contrast);

            if (result)
            {
                ImageProcesses.Contrast(bitmap, contrast);
                //Session["Contrast"] = HttpUtility.UrlDecode(Request.QueryString["Contrast"]);
                Session["Contrast"] = string.Format("{0:D1}", contrast.ToString());
            }

            float brightnessLevel;
            
            if (Request.QueryString["SavedBrightness"] != null && !Request.QueryString["SavedBrightness"].ToString().Equals("99"))
            {
                result = float.TryParse(HttpUtility.UrlDecode(Request.QueryString["SavedBrightness"]), out brightnessLevel);
                Session["SavedBrightness"] = HttpUtility.UrlDecode(Request.QueryString["SavedBrightness"]);
            }
            else
                result = float.TryParse(HttpUtility.UrlDecode(Request.QueryString["Brightness"]), out brightnessLevel);

            if (result)
            {
                Session["Brightness"] = string.Format("{0:D1}", brightnessLevel.ToString());
                brightnessLevel *= 0.1f;
                ImageProcesses.BrightNess(bitmap, brightnessLevel);
                //Session["Brightness"] = HttpUtility.UrlDecode(Request.QueryString["Brightness"]);
            }

        if ((Convert.ToBoolean(Session["DisplayCrossHair"]) || Convert.ToBoolean(Session["ShowCrossHairs"])) && HttpUtility.UrlDecode(Request.QueryString["CrossStyle"]) != string.Empty)
        {
            bool bShowCrossHairs;
            result = bool.TryParse(Session["ShowCrossHairs"].ToString(), out bShowCrossHairs);
            if (result && bShowCrossHairs)
            {
                DrawCrossHairs(bitmap, HttpUtility.UrlDecode(Request.QueryString["CrossStyle"]), HttpUtility.UrlDecode(Request.QueryString["XVal"]), HttpUtility.UrlDecode(Request.QueryString["YVal"]));
            }
        }
        //}
    }

    public void DrawCrossHairs(Bitmap bitmap, string style, string xVal, string yVal)
    {
        int X, Y;

        if (Int32.TryParse(xVal, out X) && Int32.TryParse(yVal, out Y))
        {
            Drawing draw = new Drawing();
            draw.CrossHairs(bitmap, style, X, Y);
        }
    }

    private void Invert(Bitmap bitmap)
    {
        try
        {
            ImageAttributes imageAttributes = new ImageAttributes();
            //ColorMatrix cm = new System.Drawing.Imaging.ColorMatrix(new float[][]
            //{
            //    new float[]{0.5f, 0.5f, 0.5f, 0, 0},
            //    new float[]{0.5f, 0.5f, 0.5f, 0, 0},
            //    new float[]{0.5f, 0.5f, 0.5f, 0, 0},
            //    new float[]{0, 0, 0, 1, 0, 0},
            //    new float[]{0, 0, 0, 0, 1, 0},
            //    new float[]{0, 0, 0, 0, 0, 1}
            //});


            ColorMatrix cm = new System.Drawing.Imaging.ColorMatrix(new float[][]
            {
                new float[]{-1, 1, 1, 0, 0},
                new float[]{1, -1, 1, 0, 0},
                new float[]{1, 1, -1, 0, 0},
                new float[]{0, 0, 0, 1, 0, 0},
                new float[]{0, 0, 0, 0, 1, 0},
                new float[]{0, 0, 0, 0, 0, 1}
            });
                

            imageAttributes.SetColorMatrix(cm);

            Graphics g = Graphics.FromImage(bitmap);
            g.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height,
                        GraphicsUnit.Pixel, imageAttributes);
            g.Dispose();
            imageAttributes.Dispose();
        }
        catch (Exception ex)
        {
            System.Diagnostics.Trace.WriteLine(ex);
        }
    }

    private Bitmap Normalise(byte[] bmpImage)
    {
        MemoryStream ms = new MemoryStream(bmpImage);
        System.Drawing.Image image = System.Drawing.Image.FromStream(ms);

        return new Bitmap(image);
    }

    public void Zoom(Bitmap bmpImage, string xPos, string yPos)
    {
        try
        {
            Bitmap tempImage = bmpImage;
            Graphics g = Graphics.FromImage(bmpImage);
            string delimStr = ",";
            char[] delimiter = delimStr.ToCharArray();

            if (Session["zoomXPos"] != null)
            {
                xPos = Convert.ToString(Session["zoomXPos"]) + "," + xPos;
                Session["zoomXPos"] = xPos;
            }
            else
            {
                Session["zoomXPos"] = xPos;
            }

            if (Session["zoomYPos"] != null)
            {
                yPos = Convert.ToString(Session["zoomYPos"]) + "," + yPos;
                Session["zoomYPos"] = yPos;
            }
            else
            {
                Session["zoomYPos"] = yPos;
            }
            string[] tempArrX = xPos.Split(delimiter);
            string[] tempArrY = yPos.Split(delimiter);
            for (int iIndex = 0; iIndex < tempArrX.Length; iIndex++)
            {
                //get the 60% area around the clik
                float iPortionWidth = (0.60f * bmpImage.Width);
                float iPortionHeight = (0.60f * bmpImage.Height);

                //calculate top x,y of the portion to enlarge.
                float iStartXpos = float.Parse(tempArrX[iIndex]) - (iPortionWidth / 2);
                float iStartYPos = float.Parse(tempArrY[iIndex]) - (iPortionHeight / 2);
                if (iStartXpos < 0) iStartXpos = 0;
                if (iStartYPos < 0) iStartYPos = 0;
                //set destination and source rectangle areas
                RectangleF desRect = new RectangleF(
                    0,
                    0,
                    bmpImage.Width,
                    bmpImage.Height);

                RectangleF sourceRect = new RectangleF(
                    iStartXpos,
                    iStartYPos,
                    iPortionWidth,
                    iPortionHeight);

                //get the portion of image(defined by sourceRect)
                //and enlarge it to desRect size...gives a zoom effect.
                g.DrawImage(bmpImage, desRect, sourceRect, GraphicsUnit.Pixel);

                IntPtr hBitmap = bmpImage.GetHbitmap(Color.Blue); //create pointer to bitmap
                bmpImage = System.Drawing.Image.FromHbitmap(hBitmap); //use pointer and load bitmap into oItemp

            }
            g.Dispose();

        }
        catch (Exception ex)
        {
            System.Diagnostics.Trace.WriteLine(ex);
        }
    }
    //Move to ImageProcesses.cs,  become common function
    //private void Contrast(Bitmap bitmap, float contrastFactor)
    //{
    //    try
    //    {
    //        Graphics g = Graphics.FromImage(bitmap);
    //        ImageAttributes imageAttributes = new ImageAttributes();

    //        ColorMatrix cm = new ColorMatrix(new float[][]
    //                                             {
    //                                                 new float[] {contrastFactor, 0f, 0f, 0f, 0f},
    //                                                 new float[] {0f, contrastFactor, 0f, 0f, 0f},
    //                                                 new float[] {0f, 0f, contrastFactor, 0f, 0f},
    //                                                 new float[] {0f, 0f, 0f, 1f, 0f},
    //                                                 //including the BLATANT FUDGE
    //                                                 new float[] {0.001f, 0.001f, 0.001f, 0f, 1f}
    //                                             });

    //        imageAttributes.SetColorMatrix(cm);
    //        g.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height,
    //                    GraphicsUnit.Pixel, imageAttributes);

    //        g.Dispose();
    //        imageAttributes.Dispose();
    //    }
    //    catch (Exception ex)
    //    {
    //        System.Diagnostics.Trace.WriteLine(ex);
    //    }
    //}

    //private void BrightNess(Bitmap bitmap, float brightnessLevel)
    //{
    //    try
    //    {
    //        Graphics g = Graphics.FromImage(bitmap);
    //        ImageAttributes imageAttributes = new ImageAttributes();

    //        ColorMatrix cm = new ColorMatrix(new float[][]{  
    //                          new float[]{1f,0f,0f,0f,0f},
    //                          new float[]{0f,1f,0f,0f,0f},
    //                          new float[]{0f,0f,1f,0f,0f},
    //                          new float[]{0f,0f,0f,1f,0f},
    //                          new float[]{brightnessLevel,
    //                              brightnessLevel,brightnessLevel,1f,1f}});

    //        imageAttributes.SetColorMatrix(cm);
    //        g.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, imageAttributes);

    //        g.Dispose();
    //        imageAttributes.Dispose();
    //    }
    //    catch (Exception ex)
    //    {
    //        System.Diagnostics.Trace.WriteLine(ex);
    //    }
    //}

}
