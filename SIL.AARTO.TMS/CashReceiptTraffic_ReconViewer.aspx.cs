using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;


namespace Stalberg.TMS
{
    /// <summary>
    /// The end of day Reconciliation report Viewer page
    /// </summary>
    public partial class CashReceiptTraffic_ReconViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "CashReceiptTraffic_ReconViewer.aspx";
        //protected string thisPage = "Cash Receipt Traffic - Recon Viewer";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (Request.QueryString["CBIntNo"] == null)
                Response.Redirect("Default.aspx");

            int cbIntNo = int.Parse(Request.QueryString["CBIntNo"]);

            CashboxDB db = new CashboxDB(this.connectionString);
            DataSet ds = db.GetReportReconData(cbIntNo, autIntNo);

            
            string reportPage = "CashboxRecon.rpt";
            string path = Server.MapPath("reports/" + reportPage);

            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            ReportDocument reportDoc = new ReportDocument();
            reportDoc.Load(Server.MapPath("reports/CashboxRecon.rpt"));

            reportDoc.SetDataSource(ds.Tables[0]);
            ds.Dispose();

            //set temp location for pdf export
            string tempFileLoc = string.Format("temp/{0}.pdf", Guid.NewGuid());

            //export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            reportDoc.Dispose();

            //stuff the PDF file into rendering stream
            //first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

    }
}
