using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// The Notice Correction Batch for CPI errors
    /// </summary>
    public partial class NoticeCorrectionBatch_CPI : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "Template.aspx";
        //protected string thisPage = "Batch Correction of Notice Detail";

        // Constants
        //private const string ALL_CAMERAS = "[ All Camera Units ]";
        private const int CIPRUS_ERROR_STATUS = 110;
        private const int CIPRUS_FIXED_STATUS = 120;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateAuthorities(this.autIntNo);
                this.PopulateCameraUnits(this.autIntNo);
                this.PopulateGrid(false);
            }
        }

        private void PopulateGrid(bool isRefresh)
        {
            int autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            string cameraUnitId = this.ddlCameraUnitID.SelectedValue;

            NoticeDB db = new NoticeDB(this.connectionString);

            if (isRefresh) //2013-04-09 add by Henry for pagination
            {
                grdExceptionsPager.RecordCount = 0;
                grdExceptionsPager.CurrentPageIndex = 1;
            }

            int totalCount = 0; //2013-04-09 add by Henry for pagination
            DataSet ds = db.GetCiprusNoticeBatchExceptions(autIntNo, CIPRUS_ERROR_STATUS, cameraUnitId,
                grdExceptionsPager.PageSize, grdExceptionsPager.CurrentPageIndex, out totalCount);
            grdExceptionsPager.RecordCount = totalCount;

            if (ds.Tables[0].Rows.Count == 0)
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text"),
                    this.ddlAuthority.SelectedItem.Text, this.ddlCameraUnitID.SelectedItem.Text);
            else
                this.lblError.Text = string.Empty;

            this.grdExceptions.DataSource = ds;
            this.grdExceptions.DataKeyNames = new string[] { "NotCameraID", "CiprusCode" };
            this.grdExceptions.DataBind();
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities( int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            this.ddlAuthority.SelectedIndex = this.ddlAuthority.Items.IndexOf(this.ddlAuthority.Items.FindByValue(autIntNo.ToString()));
        }

         

        protected void ddlCameraUnitID_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PopulateGrid(true);
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            int autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            this.PopulateCameraUnits(autIntNo);
        }

        private void PopulateCameraUnits(int autIntNo)
        {
            CameraUnitDB db = new CameraUnitDB(this.connectionString);
            SqlDataReader reader = db.GetCameraUnitList(autIntNo);

            this.ddlCameraUnitID.DataSource = reader;
            this.ddlCameraUnitID.DataTextField = "CamUnitId";
            this.ddlCameraUnitID.DataBind();

            this.ddlCameraUnitID.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ALL_CAMERAS"), "0"));
            this.ddlCameraUnitID.SelectedIndex = 0;
        }

        protected void grdExceptions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }

        protected void grdExceptions_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            string cameraUnitID = (string)this.grdExceptions.DataKeys[e.NewEditIndex].Values["NotCameraID"];
            int ciprusError = (int)this.grdExceptions.DataKeys[e.NewEditIndex].Values["CiprusCode"];

            NoticeDB db = new NoticeDB(this.connectionString);
            int recordsAffected = db.ResetCiprusNoticeExceptions(autIntNo, ciprusError, cameraUnitID, CIPRUS_FIXED_STATUS, CIPRUS_ERROR_STATUS);
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.BatchCorrectionOfNoticeDetail, PunchAction.Change);  

            this.PopulateGrid(true);
            this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text1"), recordsAffected > 3 ? recordsAffected / 4 : 0);
        }

        //2013-04-09 add by Henry for pagination
        protected void grvNoticesPager_PageChanged(object sender, EventArgs e)
        {
            PopulateGrid(false);
        }

    }
}
