using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using System.Transactions;
using SIL.AARTO.BLL.PostalManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represent a page that displays the print files that can be printed.
    /// </summary>
    public partial class ReprintNewOffender : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private string cType = "CAM";
        private int autIntNo = 0;
        private int statusLoaded = 10;
        private int statusPrinted = 250;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "ReprintNewOffender.aspx";
        //string csCode = "200"; // must have successfully been returned from NaTis before we touch it
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Reprint New Offender Notices";
        protected bool isIBMPrinter = false; //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)

        private const string RLV_DESCR = "Red Light Violation";
        private const string SPD_DESCR = "Speed Violations";
        private const string NAG_DESCR = "No Admission of Guilt";
        private const string RNO_DESCR = "Reprint New Offender-";
        private const string REG_DESCR = "Re-issue: Change of RegNo-";
        private const string FIRST = " (1st)";
        private const string SECOND = " (2nd)";
        private const int NAG_STATUS = 500;
        private const int NAG_PRINTED_STATUS = 510;
        

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;
            //int 

            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            // 20120726 Nick added for report engine
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = ddlSelectLA.SelectedIndex > -1 ? Convert.ToInt32(ddlSelectLA.SelectedValue) : autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)
            isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.pnlGeneral.Visible = true;
            this.dgPrintrun.Visible = true;

            //BD 20090128 - removing of auth rule to control what ticket processing being used, now using AutTicketProcessor
            //AuthorityRulesDB authRules = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails authRulesDet = authRules.GetAuthorityRulesDetailsByCode(autIntNo, "0715", "Format of External Interface", 0, "TMS_Notice", "TMS_Notice = Default; Ciprus = Civitas", login);
            //if (authRulesDet.ARString.Equals("Ciprus"))
            //{
            AuthorityDB authDB = new AuthorityDB(this.connectionString);
            AuthorityDetails authDetails =  authDB.GetAuthorityDetails(autIntNo);
            //Jerry 2012-12-28 changed
            //if (authDetails.AutTicketProcessor == "CiprusPI" || authDetails.AutTicketProcessor == "Cip_CofCT")
            //{
            //    //this.cType = authRulesDet.ARString;
            //    this.cType = authDetails.AutTicketProcessor;
            //    this.statusLoaded = 210;
            //    this.statusPrinted = 220;
            //}

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlUpdate.Visible = false;
                this.chkShowAll.Checked = false;
                this.lblInstruct.Visible = false;

                this.PopulateAuthorities(autIntNo);
                this.BindGrid(autIntNo, cType);
            }
            if (ddlSelectLA.SelectedIndex > -1)
            {
                autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                Session["printAutIntNo"] = autIntNo;
            }
            if (isIBMPrinter)
            {
                this.lblError.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("errorMsg_Print");
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorities( int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            
            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void BindGrid(int autIntNo, string cType)
        {
            int totalCount = 0; // 2013-03-15 add by henry for pagination
            Stalberg.TMS.NoticeDB db = new Stalberg.TMS.NoticeDB(connectionString);
            this.dgPrintrun.DataSource = db.GetNewOffenderFiles(autIntNo, chkShowAll.Checked, dgPrintrunPager.PageSize, dgPrintrunPager.CurrentPageIndex, out totalCount);
            this.dgPrintrun.DataKeyField = "NewOffenderPrintFileName";
            dgPrintrunPager.RecordCount = totalCount;

            try
            {
                this.dgPrintrun.DataBind();
            }
            catch
            {
                this.dgPrintrun.CurrentPageIndex = 0;
                this.dgPrintrun.DataBind();
            }

            if (this.dgPrintrun.Items.Count == 0)
            {
                this.dgPrintrun.Visible = false;
                this.lblError.Visible = true;
                if (isIBMPrinter)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("errorMsg_Print") + "<br />" + (string)GetLocalResourceObject("lblError.Text");
                }
                else
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                }
            }
            else
            {
                this.dgPrintrun.Visible = true;
                if (!isIBMPrinter)
                {
                    this.lblError.Visible = false;
                }
            }
            this.pnlUpdate.Visible = false;
        }

        protected void dgPrintrun_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            dgPrintrun.SelectedIndex = e.Item.ItemIndex;
            SetPrintLinkStatus();

            // Set printed date
            if (e.CommandName == "Select")
            {
                if (dgPrintrun.SelectedIndex > -1)
                {
                    pnlUpdate.Visible = true;
                    lblPrintFile.Text = dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString();

                    lblError.Visible = true;
                }
                return;
            }

            // Display the print File
            if (e.CommandName == "Edit")
            {
                string printFileName = e.Item.Cells[0].Text;

                // Add our JavaScript to the currently rendered page
                if (printFileName.IndexOf("2ND") > -1)
                    Helper_Web.BuildPopup(this, "SecondNoticeViewer.aspx", "PrintFile", printFileName);
                else
                    Helper_Web.BuildPopup(this, "FirstNoticeViewer.aspx", "PrintFile", printFileName);
            }
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string printFileName = txtPrint.Text.Trim();
            SetPrintLinkStatus();
            if (printFileName.Equals(string.Empty))
            {
                if (isIBMPrinter)
                {
                    lblError.Text = (string)GetLocalResourceObject("errorMsg_Print") + "<br />" + (string)GetLocalResourceObject("lblError.Text1");
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                }
                lblError.Visible = true;
                return;
            }

            Helper_Web.BuildPopup(this, "FirstNoticeViewer.aspx", "PrintFile", printFileName);
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ReprintNewOffenderNotices, PunchAction.Change);  

        }

        protected void SetPrintLinkStatus()
        {
            for (int i = 0; i < dgPrintrun.Items.Count; i++)
            {
                dgPrintrun.Items[i].Cells[3].Enabled = !isIBMPrinter;
                //this.lblError.Text = (string)GetLocalResourceObject("errorMsg_Print");
            }
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            Session["printAutIntNo"] = autIntNo;
            dgPrintrunPager.CurrentPageIndex = 1; // 2013-03-15 add by henry for pagination
            dgPrintrunPager.RecordCount = 0;
            BindGrid(autIntNo, cType);

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            //2014-05-15 Heidi rename "SecNotice" to "isIBMPrinter"(5283)
            isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

            if (isIBMPrinter)
            {
                this.lblError.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("errorMsg_Print");
            }
            else
            {
                this.lblError.Visible = false;
            }
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            this.Session["printAutIntNo"] = autIntNo;

            string printFileName = this.lblPrintFile.Text;            

            if (printFileName.Substring(0, 3).Equals("NAG", StringComparison.InvariantCultureIgnoreCase))
            {
                this.statusPrinted = NAG_PRINTED_STATUS;
                this.statusLoaded = NAG_STATUS;
            }

            int noOfRows = 0;
            string errMessage = string.Empty;
            using (TransactionScope scope = new TransactionScope())
            {
                NoticeDB db = new NoticeDB(connectionString);
                noOfRows = db.SetNewOffenderPrintedDate(printFileName, this.login, autIntNo);

                if (noOfRows > 0)
                {
                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "4200";
                    arDetails.LastUser = login;
                    DefaultAuthRules ar = new DefaultAuthRules(arDetails, connectionString);
                    bool isPostDate = ar.SetDefaultAuthRule().Value.Equals("Y");

                    if (isPostDate)
                    {
                        PostDateNotice postDate = new PostDateNotice(connectionString);
                        noOfRows = postDate.SetNoticePostedDate(printFileName, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), login, ref errMessage);
                    }
                }

                if (noOfRows > 0)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ReprintNewOffenderNotices, PunchAction.Change);  
                    scope.Complete();
                }
            }

            this.lblError.Visible = true;
            if (isIBMPrinter)
            {
                this.lblError.Text = (string)GetLocalResourceObject("errorMsg_Print") + "<br />" + string.Format((string)GetLocalResourceObject("lblError.Text2"), noOfRows);
            }
            else
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), noOfRows);
            }

            // 2013-09-12, Oscar removed, it reset wrong page
            //dgPrintrunPager.CurrentPageIndex = 1; // 2013-03-15 add by henry for pagination
            //dgPrintrunPager.RecordCount = 0;

            this.BindGrid(autIntNo, cType);
        }

        protected void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            dgPrintrunPager.CurrentPageIndex = 1; // 2013-03-15 add by henry for pagination
            dgPrintrunPager.RecordCount = 0;
            BindGrid(autIntNo, cType);

            if (chkShowAll.Checked)
                Session["showAllNotices"] = "Y";
            else
                Session["showAllNotices"] = "N";
        }

        protected void dgPrintrun_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.DataItem == null)
                return;

            //2014-01-22 Heidi changed for Violation Type Description cannot wrap(5103)
            Label lbl = (Label)e.Item.FindControl("lblType");
            //var lbl = (TextBox)e.Item.FindControl("lblType");

            DataRowView row = (DataRowView)e.Item.DataItem;
            string printFileName = (string)row["NewOffenderPrintFileName"];
            string prefix = printFileName.Substring(0, 3).ToUpper();
            string longPrefix = printFileName.Substring(0, 7).ToUpper();
            string reallyLongPrefix = printFileName.Substring(0, 11).ToUpper();
            switch (prefix)
            {
                case "SPD":
                    lbl.Text = SPD_DESCR;
                    break;
                case "RLV":
                    lbl.Text = RLV_DESCR;
                    break;
                case "NAG":
                    lbl.Text = NAG_DESCR;
                    break;
                case "RNO": 
                    switch (reallyLongPrefix)
                    {
                        case "RNO_RLV_2ND":
                            lbl.Text = RNO_DESCR + RLV_DESCR + SECOND;
                            break;
                        case "RNO_RLV_1ST":
                            lbl.Text = RNO_DESCR + RLV_DESCR + FIRST;
                            break;
                        case "RNO_SPD_2ND":
                            lbl.Text = RNO_DESCR + SPD_DESCR + SECOND;
                            break;
                        case "RNO_SPD_1ST":
                        default:
                            switch (longPrefix)
                            {
                                case "RNO_RLV":
                                    lbl.Text = lbl.Text = RNO_DESCR + RLV_DESCR + FIRST;
                                    break;
                                case "RNO_SPD":
                                default:
                                    lbl.Text = lbl.Text = RNO_DESCR + SPD_DESCR + FIRST;
                                    break;
                            }
                            break;
                    }
                    break;
                case "REG":
                    switch (reallyLongPrefix)
                    {
                        case "REG_RLV_2ND":
                            lbl.Text = REG_DESCR + RLV_DESCR + SECOND;
                            break;
                        case "REG_RLV_1ST":
                            lbl.Text = REG_DESCR + RLV_DESCR + FIRST;
                            break;
                        case "REG_SPD_2ND":
                            lbl.Text = REG_DESCR + SPD_DESCR + SECOND;
                            break;
                        case "REG_SPD_1ST":
                        default:
                            switch (longPrefix)
                            {
                                case "REG_RLV":
                                    lbl.Text = lbl.Text = REG_DESCR + RLV_DESCR + FIRST;
                                    break;
                                case "REG_SPD":
                                default:
                                    lbl.Text = lbl.Text = REG_DESCR + SPD_DESCR + FIRST;
                                    break;
                            }
                            break;
                    }
                    break;
                default:
                    lbl.Text = string.Format((string)GetLocalResourceObject("lblError.Text3"), prefix);
                    break;
            }

            e.Item.Cells[3].Enabled = !isIBMPrinter;
        }

        protected void dgPrintrunPager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, cType);
        }
    }
}
