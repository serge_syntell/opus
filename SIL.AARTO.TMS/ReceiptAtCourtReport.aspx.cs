﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Stalberg.TMS;
using System.Data.SqlClient;


namespace Stalberg.TMS
{
    public partial class ReceiptsAtCourtReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int userIntNo = 0;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "ReceiptsAtCourtReport.aspx";
        private const string DATE_FORMAT = "yyyy-MM-dd";
        protected string thisPage = "Receipts at Court Report";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.dtpStartDate.Text = DateTime.Today.ToString(DATE_FORMAT);
                this.dtpEndDate.Text = DateTime.Today.AddMonths(1).ToString(DATE_FORMAT);

                dtpCourtDate.Text = (string)GetLocalResourceObject("dtpCourtDate.Text");
                PopulateSortOrder();
                this.PopulateCourts();

            }
        }

        private void PopulateSortOrder()
        {
            this.ddlOrderBy.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlOrderBy.Items"), "CrtDate"));
            this.ddlOrderBy.Items.Insert(1, new ListItem((string)GetLocalResourceObject("ddlOrderBy.Items1"), "RptDate"));
            this.ddlOrderBy.SelectedIndex = 0;
        }

        private void PopulateCourts()
        {
            this.cboCourt.Items.Clear();

            CourtDB db = new CourtDB(this.connectionString);
            SqlDataReader reader = db.GetCourtList();

            ListItem item;
            while (reader.Read())
            {
                item = new ListItem();
                item.Value = reader["CrtIntNo"].ToString();
                item.Text = reader["CrtDetails"].ToString();
                this.cboCourt.Items.Add(item);
            }
            reader.Close();

            item = new ListItem();
            item.Text = (string)GetLocalResourceObject("cboCourtItem.Text");
            item.Value = "0";
            this.cboCourt.Items.Insert(0, item);

            this.cboCourt.SelectedIndex = 0;
        }

         

        protected void dtpStartDate_TextChanged(object sender, EventArgs e)
        {
            DateTime dtStart;
            if (!DateTime.TryParse(this.dtpStartDate.Text, out dtStart))
                return;

            DateTime dtEnd;
            if (!DateTime.TryParse(this.dtpEndDate.Text, out dtEnd))
            {
                this.dtpEndDate.Text = dtStart.AddDays(1).ToString(DATE_FORMAT);
                return;
            }

            if (dtEnd.Ticks < dtStart.Ticks)
                this.dtpEndDate.Text = dtStart.AddDays(1).ToString(DATE_FORMAT);
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            lblError.Visible = true;
            DateTime dtStart;
            if (!DateTime.TryParse(this.dtpStartDate.Text, out dtStart))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            DateTime dtEnd;
            if (!DateTime.TryParse(this.dtpEndDate.Text, out dtEnd))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            DateTime dtCourt = DateTime.MinValue;
            if (!DateTime.TryParse(this.dtpCourtDate.Text, out dtCourt) && this.dtpCourtDate.Text != (string)GetLocalResourceObject("dtpCourtDate.Text")) 
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            if (dtEnd.Ticks < dtStart.Ticks)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            lblError.Visible = false;

            int crtIntNo = int.Parse(this.cboCourt.SelectedValue);
            string crtText = this.cboCourt.SelectedItem.Text;
            string SortOrder = this.ddlOrderBy.SelectedValue;

            PageToOpen page = new PageToOpen(this, "ReceiptAtCourtViewer.aspx");

            page.Parameters.Add(new QSParams("rAutIntNo", autIntNo));
            page.Parameters.Add(new QSParams("rReceiptStartDate", dtStart.ToString("yyyy-M-dd")));
            page.Parameters.Add(new QSParams("rReceiptEndDate", dtEnd.ToString("yyyy-M-dd")));

            if (dtCourt > DateTime.MinValue)
            {
                page.Parameters.Add(new QSParams("rCourtDate", dtCourt.ToString("yyyy-M-dd")));
            }

            //BD now need to pass through extra parameters
            page.Parameters.Add(new QSParams("CrtIntNo", crtIntNo));
            //BD sort order can be: Notice, Receipt, Summons, AG
            page.Parameters.Add(new QSParams("SortOrder", SortOrder));
            page.Parameters.Add(new QSParams("crtText", crtText));

            Helper_Web.BuildPopup(page);
            //PunchStats805806 enquiry ReceiptsAtCourtReport
        }
}
}
