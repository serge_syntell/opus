<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ReprintNewOffender" Codebehind="ReprintNewOffender.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    &nbsp;
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table cellspacing="0" cellpadding="0" border="0" height="90%" width="100%">
                        <tr>
                            <td valign="top" style="height: 49px; width: 819px;">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="552px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p align="center">
                                    &nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="width: 819px">
                                <asp:UpdatePanel ID="udpGrid" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlGeneral" runat="server" DefaultButton="btnPrint">
                                            <table style="width: 877px">
                                                <tr>
                                                    <td style="width: 343px; height: 2px">
                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                                    <td style="width: 260px; height: 2px" valign="middle">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px">
                                                        </asp:DropDownList></td>
                                                    <td style="height: 2px" valign="middle">
                                                        <asp:CheckBox ID="chkShowAll" runat="server" AutoPostBack="True" CssClass="NormalBold"
                                                            OnCheckedChanged="chkShowAll_CheckedChanged" Text="<%$Resources:chkShowAll.Text %>" /></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 343px; height: 43px;">
                                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" EnableViewState="False"
                                                            Height="41px" Text="<%$Resources:lblPrint.Text %>"></asp:Label></td>
                                                    <td style="width: 260px; height: 43px;" valign="middle">
                                                        <asp:TextBox ID="txtPrint" runat="server" Width="233px"></asp:TextBox></td>
                                                    <td style="height: 43px" valign="middle">
                                                        <asp:Button ID="btnPrint" runat="server" Text="<%$Resources:btnPrint.Text %>" OnClick="btnPrint_Click"
                                                            CssClass="NormalButton" Width="155px" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label><br />
                                        <asp:DataGrid ID="dgPrintrun" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True" Width="90%"
                                            Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgPrintrun_ItemCommand"
                                            OnItemCreated="dgPrintrun_ItemCreated">
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="NewOffenderPrintFileName" HeaderText="<%$Resources:dgPrintrun.HeaderText %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoOFNotices" HeaderText="<%$Resources:dgPrintrun.HeaderText1 %>"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText2 %>">
                                                    <ItemTemplate>
                                                      <asp:Label ID="lblType" runat="server" style="word-wrap:break-word"></asp:Label>
                                                     <%-- <asp:TextBox ID="lblType" runat="server" BorderStyle="None" BorderWidth="0" style="background-color: transparent;" />--%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:HyperLinkColumn Text="<%$Resources:dgPrintrunItem.Text %>" Target="_blank" DataNavigateUrlField="NewOffenderPrintFileName"
                                                    DataNavigateUrlFormatString="FirstNoticeViewer.aspx?printfile={0}&printType=ReprintNewOffender" HeaderText="<%$Resources:dgPrintrun.HeaderText3 %>">
                                                </asp:HyperLinkColumn>
                                                <asp:ButtonColumn CommandName="Select" Text="<%$Resources:dgPrintrunItem.Text1 %>" HeaderText="<%$Resources:dgPrintrun.HeaderText4 %>">
                                                </asp:ButtonColumn>
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <pager:AspNetPager id="dgPrintrunPager" runat="server" 
                                            showcustominfosection="Right" width="895px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                            FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"
                                              onpagechanged="dgPrintrunPager_PageChanged"  UpdatePanelId="udpGrid"></pager:AspNetPager>

                                        <asp:Panel ID="pnlUpdate" runat="server" Height="50px" Width="125px">
                                            <table style="width: 495px">
                                                <tr>
                                                    <td style="height: 20px">
                                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblPrintFile.Text %>"></asp:Label></td>
                                                    <td style="height: 20px">
                                                        <asp:Label ID="lblPrintFile" runat="server" CssClass="NormalBold" Width="348px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnUpdateStatus" runat="server" Text="<%$Resources:btnUpdateStatus.Text %>" CssClass="NormalButton"
                                                            OnClick="btnUpdateStatus_Click" /></td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="lblInstruct" runat="server" Width="465px" BorderStyle="Inset" CssClass="CartListHead"
                                                Height="18px" EnableViewState="false" Text="<%$Resources:lblInstruct.Text %>"></asp:Label></asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="udp" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" 
                            /><asp:Label ID="Label3" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
