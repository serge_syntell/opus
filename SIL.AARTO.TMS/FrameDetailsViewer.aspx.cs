using System;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Stalberg.TMS.Data.Datasets;
using System.Collections;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Handles printing the first notice
    /// </summary>
    public partial class FrameDetailsViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        protected int _option = 1;              //1st Notice

        //protected string thisPage = "View Frame Details by Reason for Rejection";
        protected string thisPageURL = "FrameDetailsViewer.aspx";
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string showCrossHairs = "N";
        protected int crossHairStyle = 0;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //string autCode = GetAuthCode();
            string reportPath = string.Empty;
            autIntNo = Convert.ToInt32(Request.QueryString["AutIntNo"]);
            int rejIntNo = Convert.ToInt32(Request.QueryString["RejIntNo"]);

            string startDate = Request.QueryString["After"] == null ? string.Empty : Request.QueryString["After"].ToString();
            string endDate = Request.QueryString["Before"] == null ? string.Empty : Request.QueryString["Before"].ToString();

            DateTime startDt;
            if (!DateTime.TryParse(startDate, out startDt))
                startDt = new DateTime(2000, 1, 1);

            DateTime endDt;
            if (!DateTime.TryParse(endDate, out endDt))
                endDt = new DateTime(2050, 1, 1);

            AuthReportNameDB arn = new AuthReportNameDB(this.connectionString);

            string reportPage = arn.GetAuthReportName(autIntNo, "ViewFrameDetails");
            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "ViewFrameDetails.rpt", "ViewFrameDetails", "system", "");
                reportPage = "ViewFrameDetails.rpt";
            }

            reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "ViewFrameDetails");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            string tempFileLoc = string.Empty;
            SqlConnection con = null;
            SqlCommand com = null;
            SqlDataAdapter da = null;
            ReportDocument _reportDoc = null;

            //dls 100202 - move rules for showing cross-hairs out of the stored proc into the code
            AuthorityRulesDetails ar = new AuthorityRulesDetails();

            ar.AutIntNo = this.autIntNo;
            ar.ARCode = "3100";
            ar.LastUser = this.loginUser;

            DefaultAuthRules crossHairs = new DefaultAuthRules(ar, this.connectionString);

            KeyValuePair<int, string> crossHairRule = crossHairs.SetDefaultAuthRule();

            showCrossHairs = crossHairRule.Value;

            ar = new AuthorityRulesDetails();
            ar.AutIntNo = this.autIntNo;
            ar.ARCode = "3150";
            ar.LastUser = this.loginUser;

            crossHairs = new DefaultAuthRules(ar, this.connectionString);

            crossHairRule = crossHairs.SetDefaultAuthRule();

            crossHairStyle = crossHairRule.Key;

            try
            {
                _reportDoc = new ReportDocument();
                _reportDoc.Load(reportPath);

                // Fill the DataSet
                con = new SqlConnection(this.connectionString);
                com = new SqlCommand("FrameViewDetails", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
                com.Parameters.Add("@RejIntNo", SqlDbType.Int, 4).Value = rejIntNo;
                com.Parameters.Add("@StartDate", SqlDbType.SmallDateTime).Value = startDt;
                com.Parameters.Add("@EndDate", SqlDbType.SmallDateTime).Value = endDt;
                com.Parameters.Add("@ShowCrossHairs", SqlDbType.Char, 1).Value = showCrossHairs;
                com.Parameters.Add("@CrossHairStyle", SqlDbType.VarChar, 4).Value = crossHairStyle;

                //get data and populate dataset
                dsFrameViewDetails dsFrameViewDetails = new dsFrameViewDetails();
                da = new SqlDataAdapter(com);
                dsFrameViewDetails.DataSetName = "dsFrameViewDetails";
                da.Fill(dsFrameViewDetails);
                da.Dispose();

                if (dsFrameViewDetails.Tables[1].Rows.Count == 0)
                {
                    dsFrameViewDetails.Dispose();
                    Response.Write((string)GetLocalResourceObject("strWriteMsg"));
                    Response.End();
                    return;
                }

                // david lin 20100330 remove images from database
                ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                WebService service = new WebService();
                ScanImageDetails imageDetail = null;

                foreach (DataRow dr in dsFrameViewDetails.Tables[1].Rows)
                {
                    if (dr["ScImIntNo1"] != null && dr["ScImIntNo1"] != System.DBNull.Value)
                    {
                        imageDetail = imgDB.GetImageFullPath(Convert.ToInt32(dr["ScImIntNo1"]));
                        dr["ScanImage1"] = service.GetImagesFromRemoteFileServer(imageDetail);
                    }

                    if (dr["ScImIntNo2"] != null && dr["ScImIntNo2"] != System.DBNull.Value)
                    {
                        imageDetail = imgDB.GetImageFullPath(Convert.ToInt32(dr["ScImIntNo2"]));
                        dr["ScanImage2"] = service.GetImagesFromRemoteFileServer(imageDetail);
                    }

                    if (dr["ScImIntNo3"] != null && dr["ScImIntNo3"] != System.DBNull.Value)
                    {
                        imageDetail = imgDB.GetImageFullPath(Convert.ToInt32(dr["ScImIntNo3"]));
                        dr["ScanImage3"] = service.GetImagesFromRemoteFileServer(imageDetail);
                    }
                }                

                //set up new report based on .rpt report class
                _reportDoc.SetDataSource(dsFrameViewDetails.Tables[1]);
                dsFrameViewDetails.Dispose();

                //export the pdf file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
                _reportDoc.Dispose();

                // Stuff the PDF file into rendering stream first clear everything dynamically created and just send PDF file
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }
            finally
            {
                con.Dispose();
                con = null;

                com.Dispose();
                com = null;

                da.Dispose();
                da = null;
            }

        }

    }
}
