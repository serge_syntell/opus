using System;
using Stalberg.TMS.Data.Util;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Notice Post management page
    /// </summary>
    public partial class DataWashingReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = string.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "DataWashingReport.aspx";
        //protected string thisPage = "Data Washing Report";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            UserDetails userDetails  = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
        }

         

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            //ReportingUtil reportingUtil = new ReportingUtil(this.connectionString, this.login);
            //PageToOpen pto = new PageToOpen(this.Page, reportingUtil.GetReportUrl(Request.Url, "DataWashing"));
            //Helper_Web.BuildPopup(pto);

            PageToOpen pto = new PageToOpen(this.Page, "DataWashingReportViewer.aspx");
            Helper_Web.BuildPopup(pto);
        }

    }
}
