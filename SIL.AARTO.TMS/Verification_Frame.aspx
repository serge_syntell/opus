<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="ImageControl.ascx" TagName="ImageViewer" TagPrefix="iv1" %>
<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.Verification_Frame" Trace="false" Codebehind="Verification_Frame.aspx.cs" %>  
<%@ Register   Assembly="AjaxControlToolkit"  Namespace="AjaxControlToolkit"  TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=title %>
    </title>
<%--
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
        .style2
        {
            width: 274px;
        }
        .style3
        {
            width: 231px;
        }
        .style4
        {
            width: 228px;
        }
        .style5
        {
            width: 225px;
        }
        .style6
        {
            width: 223px;
        }
    </style>
--%>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0" rightmargin="0" language="javascript" onload="return window_onload()">
         
    <form id="Form1" runat="server">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
            <Services>
                <asp:ServiceReference Path="WebService.asmx" />
            </Services>
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/WebService.js" />
            </Scripts>
        </asp:ScriptManager>
        
<%--        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" colspan="2" valign="middle" style="width: 100%">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>  --%>
                                                          
        <asp:UpdatePanel ID="udpFrame" runat="server">
             <ContentTemplate>  
           
                <table border="0" width="100%">
                <tr>
                <td valign="top" width="100%" align="center">
                    <asp:Panel ID="pnlNavigation" Width="100%" runat="server">
                        <table border="0" style="width: 100%; border: solid 1px black;" class="Normal">
                            <tr>
                                <td style="width: 10%;">
                                    <asp:LinkButton ID="lnkPrevious" runat="server" CssClass="MenuItem" OnClick="lnkPrevious_Click"><< Previous</asp:LinkButton></td>
                                <td style="width: 80%; text-align: center;">
                                    <asp:Label ID="lblNavigation" runat="server" Font-Bold="True"></asp:Label></td>
                                <td style="width: 10%; text-align: right">
                                    <asp:LinkButton ID="lnkNext" runat="server" CssClass="MenuItem" OnClick="lnkNext_Click">Next >></asp:LinkButton></td>
                            </tr>
                        </table>
                    </asp:Panel>
                    
    
                    <table width="100%">
                        <tr>
                            <td>
                                &nbsp;<asp:Label ID="lblFilmNo" runat="server" CssClass="NormalBold" Width="107px"></asp:Label>&nbsp;
                                <asp:Label ID="Label19" runat="server" CssClass="NormalBold" Width="107px">Frame no / Seq:</asp:Label>&nbsp;
                                <asp:TextBox ID="txtFrameNo" runat="server" CssClass="Normal" MaxLength="10" ReadOnly="True"
                                    Width="71px"></asp:TextBox>&nbsp;
                                <asp:TextBox ID="txtSeq" runat="server" CssClass="Normal" MaxLength="10" ReadOnly="True"
                                    Width="29px"></asp:TextBox></td>
                            <td align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="388px" CssClass="ContentHead">Verification - Stage 2 (Frame)</asp:Label></td>
                            <td align="right" width="33%">
                                <span id="lblMessage" runat="server" class="NormalRed">&nbsp;</span>&nbsp;</td>
                        </tr>
                    </table>
                    &nbsp;
                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="False"
                        Width="376px" ForeColor="Red"></asp:Label> </asp:Label>
                    <asp:CheckBox ID="chkAllowContinue" runat="server" CssClass="NormalBold" Text="Registration Confirmed?" />
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtFirstSpeed"
                        CssClass="NormalRed" ErrorMessage="Please enter a valid speed" MaximumValue="240"
                        MinimumValue="0" Type="Integer"></asp:RangeValidator><asp:RangeValidator ID="RangeValidator4"
                            runat="server" ControlToValidate="txtSecondSpeed" CssClass="NormalRed" ErrorMessage="Please enter a valid speed"
                            MaximumValue="240" MinimumValue="0" Type="Integer"></asp:RangeValidator><asp:RangeValidator
                                ID="RangeValidator2" runat="server" ControlToValidate="txtOffenceDate" CssClass="NormalRed"
                                ErrorMessage="Please enter a valid date" MaximumValue="3000/12/31" MinimumValue="2005/01/01"
                                Type="Date"></asp:RangeValidator><asp:RangeValidator ID="RangeValidator1" runat="server"
                                    ControlToValidate="txtOffenceTime" CssClass="NormalRed" ErrorMessage="Please enter a valid time"
                                    MaximumValue="23:59" MinimumValue="00:00"></asp:RangeValidator>&nbsp;
                </td>
            </tr>
                <tr>
                <td valign="top" width="100%">
                    <table width="100%">
                        <tr>
                            <td width="100%" align="center">
                                    <table width="100%">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Width="94px">Speed:</asp:Label></td>
                                                    <td valign="top" style="width: 180px">
                                                        <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="161px">Offence time and date:</asp:Label></td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="137px" Height="19px">Vehicle make:</asp:Label></td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="138px">Vehicle type:</asp:Label></td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Width="137px" Height="19px">Registration no:</asp:Label></td>
                                                    <td valign="top" align="left">
                                                        <asp:Label ID="lblRejection" runat="server" CssClass="NormalBold" Width="137px">Reason for rejection:</asp:Label></td>
                                                    <td align="left" valign="top">
                                                        <input id="btnAccept" runat="server" class="NormalButton" style="width: 118px" type="button"
                                                            value="Accept" /></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:TextBox ID="txtFirstSpeed" runat="server" CssClass="NormalMand" MaxLength="10"
                                                            Width="36px"></asp:TextBox>
                                                        &nbsp;
                                                        <asp:TextBox ID="txtSecondSpeed" runat="server" CssClass="NormalMand" MaxLength="10"
                                                            Width="36px"></asp:TextBox></td>
                                                    <td valign="top">
                                                        <asp:TextBox ID="txtOffenceTime" runat="server" CssClass="NormalMand" MaxLength="10"
                                                            Width="59px"></asp:TextBox>&nbsp;
                                                        <asp:TextBox ID="txtOffenceDate" runat="server" CssClass="NormalMand" MaxLength="10"
                                                            Width="80px"></asp:TextBox></td>
                                                    <td valign="top">
                                                        <asp:DropDownList ID="ddlVehMake" runat="server" Width="169px" CssClass="Normal"
                                                            Height="15px">
                                                        </asp:DropDownList></td>
                                                    <td valign="top">
                                                        <asp:DropDownList ID="ddlVehType" runat="server" Width="169px" CssClass="Normal"
                                                            Height="15px">
                                                        </asp:DropDownList></td>
                                                    <td valign="top">
                                                        <asp:TextBox ID="txtRegNo" runat="server" CssClass="NormalMand" MaxLength="10" Width="134px"
                                                            AutoCompleteType="Disabled"></asp:TextBox></td>
                                                    <td valign="top" align="left">
                                                        <asp:DropDownList ID="ddlRejection" runat="server" Width="247px" CssClass="Normal"
                                                            Height="15px" AutoPostBack="False">
                                                        </asp:DropDownList></td>
                                                    <td align="left" valign="top">
                                                        <input id="btnChange" runat="server" class="NormalButton" style="width: 118px" type="button"
                                                            value="Change"  /></td>
                                                </tr>
                                                
                                                <tr>
                                                <td valign="top">&nbsp;</td>
                                                <td valign="top">
                                                    <asp:Label ID="lblNatis" runat="server" BorderColor="Red" CssClass="NormalRed" 
                                                        Width="207px">NATIS details (if different):</asp:Label>
                                                    </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtNatisVehicleMake" runat="server" CssClass="NormalRed" 
                                                        ReadOnly="True" Width="166px"></asp:TextBox>
                                                    </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtNatisVehicleType" runat="server" CssClass="NormalRed" 
                                                        ReadOnly="True" Width="166px"></asp:TextBox>
                                                    </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtNatisRegNo" runat="server" CssClass="NormalRed" 
                                                        ReadOnly="True" Width="132px"></asp:TextBox>
                                                    </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtNatisRejreason" runat="server" CssClass="NormalRed" 
                                                        ReadOnly="True" Width="247px"></asp:TextBox>
                                                    </td>
                                                <td valign="left">
                                                    <input ID="btnNatis" runat="server" class="NormalButton" style="width: 118px" 
                                                        type="button" value="Return to Natis" /></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                     </td>
                                                    <td valign="top" align="left">
                                                        <asp:Label ID="lblNatisReturnCode" runat="server" CssClass="NormalBold">Natis return code/error:</asp:Label>
                                                    </td>
                                                    <td valign="top" colspan="4">
                                                        <asp:TextBox ID="txtNatisreturncode" runat="server" CssClass="NormalRed" 
                                                            ReadOnly="True" Width="721px"></asp:TextBox>
                                                        <input id="hidUserName" runat="server" style="width: 24px" type="hidden" />
                                                        <input id="hidStatusVerified" runat="server" style="width: 24px" 
                                                            type="hidden" />
                                                        <input id="hidStatusRejected" runat="server" style="width: 24px" 
                                                            type="hidden" />
                                                        <input id="hidStatusNatis" runat="server" style="width: 24px" type="hidden" />
                                                        <input id="hidUpdateMainImage" runat="server" style="width: 24px" 
                                                            type="hidden" />
                                                        <input id="hidRowVersion" runat="server" style="width: 24px" type="hidden" />
                                                        <input id="hidPreUpdatedRejReason" runat="server" style="width: 24px" 
                                                            type="hidden" />
                                                        <input id="hidPreUpdatedAllowContinue" runat="server" style="width: 24px" 
                                                            type="hidden" />
                                                        <input id="hidFrameIntNo" runat="server" style="width: 24px" type="hidden" />
                                                        <input id="hidPreUpdatedRegNo" runat="server" style="width: 24px" 
                                                            type="hidden" />
                                                        <input id="hidProxyID" runat="server" style="width: 24px" type="hidden" />
                                                        <input id="hidOwnerID" runat="server" style="width: 24px" type="hidden" />
                                                        <input id="hidDriverID" runat="server" style="width: 24px" type="hidden" />
                                                    </td>
                                                    <td align="left" valign="top">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table >
        
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" 
            TargetControlID="btnShowOwner"
            PopupControlID="addressPanel" 
            BehaviorID="ModalPopupBehavior"
            BackgroundCssClass="modalBackground" 
            PopupDragHandleControlID="dragablePanel"
            CancelControlID="ImgbtnExit"
            DropShadow="false"
            X="10"
            Y="10"
            /> 
     
        <asp:Panel ID="addressPanel" runat="server" style="display:none;"   CssClass="modalPopup" >
            <asp:Panel ID="dragablePanel"  runat="server"  
                Style="cursor: move;background-color:#DDDDDD;border:solid 1px Gray;color:Black ;">
                <table width ="100%"><tr><td align="right" valign="middle">
                    <asp:ImageButton ID="ImgbtnExit" style="cursor:pointer;" runat="server"  OnClientClick ="return clearMessage();" ImageUrl="~/Images/exit1.jpg"/></td></tr></table>
                  </asp:Panel>   
   
               <div>
                                                                    <table ID="tblOwnerDetails" runat="server" >
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Width="100px">Owner 
                                                                        details:</asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="100px">Driver 
                                                                        details:</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="lblProxy" runat="server" CssClass="NormalBold" Width="202px">Proxy 
                                                                        details (if applicable):</asp:Label>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td >
                                                                        <asp:Label ID="lblOwnerIntNo" runat="server" CssClass="NormalBold" Width="100px" Visible=false>Owner IntNo:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerIntNo" runat="server" CssClass="NormalBold" Visible=false
                                                                            MaxLength="10" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:Label ID="lblDriverIntNo" runat="server" CssClass="NormalBold" Width="100px" Visible=false>Driver IntNo:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtDriverIntNo" runat="server" CssClass="NormalBold" Visible=false
                                                                            MaxLength="10" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:Label ID="lblProxyIntNo" runat="server" CssClass="NormalBold" Width="100px" Visible=false>Proxy IntNo:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyIntNo" runat="server" CssClass="NormalBold" Visible=false
                                                                            MaxLength="10" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="100px">Name:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerName" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="100" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Width="100px">Name:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtDriverName" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="100" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:Label ID="lblProxyName" runat="server" CssClass="NormalBold" Width="100px">Name:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyName" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="100" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label45" runat="server" CssClass="NormalBold" Width="100px">Initials:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerinitials" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="10" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:Label ID="Label46" runat="server" CssClass="NormalBold" Width="100px">Initials:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtDriverinitials" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="10" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:Label ID="lblProxtinitials" runat="server" CssClass="NormalBold" Width="100px">Initials:
                                                                        </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxtinitials" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="10" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="100px">ID:</asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerID" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="25" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="height: 27px">
                                                                        <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Width="100px">ID:</asp:Label>
                                                                    </td>
                                                                    <td style="height: 27px">
                                                                        <asp:TextBox ID="txtDriverID" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="25" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="height: 27px">
                                                                        <asp:Label ID="lblProxyID" runat="server" CssClass="NormalBold" Width="100px">ID:</asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyID" runat="server" CssClass="NormalBold" 
                                                                            MaxLength="25" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="100px">Postal address1: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerPostAddr" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Width="100px">Postal address1: </asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverPostAddr" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblProxyPostAddr" runat="server" CssClass="NormalBold" Width="100px">Postal address1: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyPostAddr" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label17" runat="server" CssClass="NormalBold" Width="100px">Postal address2: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerPostAddr2" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Width="100px">Postal address2: </asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverPostAddr2" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblProxyPostAddr2" runat="server" CssClass="NormalBold" Width="100px">Postal address2: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyPostAddr2" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label26" runat="server" CssClass="NormalBold" Width="100px">Postal address3: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerPostAddr3" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:Label ID="Label27" runat="server" CssClass="NormalBold" Width="100px">Postal address3: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtDriverPostAddr3" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td >
                                                                        <asp:Label ID="lblProxyPostAddr3" runat="server" CssClass="NormalBold" Width="100px">Postal address3: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyPostAddr3" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label29" runat="server" CssClass="NormalBold" Width="100px">Postal address4: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerPostAddr4" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblDriverPostAddr4" runat="server" CssClass="NormalBold" Width="102px">Postal address4: </asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverPostAddr4" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblProxyPostAddr4" runat="server" CssClass="NormalBold" Width="100px">Postal address4: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyPostAddr4" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label32" runat="server" CssClass="NormalBold" Width="100px">Postal address5: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerPostAddr5" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblDriverPostAddr5" runat="server" CssClass="NormalBold" Width="101px">Postal 
                                                                        address5: </asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverPostAddr5" runat="server" CssClass="NormalBold" MaxLength="100"
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td >
                                                                          
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Width="100px">Street 
                                                                        address1: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerStrAddr" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Width="98px">Street 
                                                                        address1: </asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverStrAddr" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblProxyStrAddr" runat="server" CssClass="NormalBold" Width="100px">Street 
                                                                        address1: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyStrAddr" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label34" runat="server" CssClass="NormalBold" Width="100px">Street 
                                                                        address2: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerStrAddr2" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label35" runat="server" CssClass="NormalBold" Width="98px">Street 
                                                                        address2: </asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverStrAddr2" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblProxyStrAddr2" runat="server" CssClass="NormalBold" Width="100px">Street 
                                                                        address2: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyStrAddr2" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label37" runat="server" CssClass="NormalBold" Width="100px">Street 
                                                                        address3: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerStrAddr3" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label38" runat="server" CssClass="NormalBold" Width="101px">Street 
                                                                        address3: </asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverStrAddr3" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblProxyStrAddr3" runat="server" CssClass="NormalBold" Width="100px">Street 
                                                                        address3: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyStrAddr3" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label40" runat="server" CssClass="NormalBold" Width="100px">Street 
                                                                        address4: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerStrAddr4" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label41" runat="server" CssClass="NormalBold" Width="99px">Street 
                                                                        address4: </asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverStrAddr4" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblProxyStrAddr4" runat="server" CssClass="NormalBold" Width="100px">Street 
                                                                        address4: </asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxyStrAddr4" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Width="100px">Postal 
                                                                        code:</asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtOwnerPostalcode" runat="server" CssClass="NormalBold" 
                                                                            Height="22px" Width="150px" MaxLength="10"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label21" runat="server" CssClass="NormalBold" Width="100px">Postal 
                                                                        code:</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDriverpostalcode" runat="server" CssClass="NormalBold" ReadOnly=true
                                                                            Height="22px" Width="150px" MaxLength="10"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblProxypostalcode" runat="server" CssClass="NormalBold" Width="100px">Postal 
                                                                        code:</asp:Label>
                                                                    </td>
                                                                    <td >
                                                                        <asp:TextBox ID="txtProxypostalcode" runat="server" CssClass="NormalBold" 
                                                                            Height="22px" Width="150px" MaxLength="10"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                 <tr><td colspan="6"><asp:Label ID="LabelMessage" runat="server" Text="" Font-Bold="true"></asp:Label></td></tr>
                                                                        <tr Height="30px"><td align="center" colspan="6">
                                                                           <asp:UpdatePanel ID="UpdatePanel1" runat="server"> <ContentTemplate>
                                                                            <asp:Button ID="btnUpdateOwner" runat="server" Text="Update Owner Details" Width="200px" 
                                                                                onclick="btnUpdateOwner_Click" Height="28px" /> 
                                                                            </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                            
                                                                            
                                                                        </td></tr>
                                                                        </table> 
                                                                                                                                            
                                                              </div>                                                                 
                                                    
        </asp:Panel>
        
        <table width="100%" runat="server" id="tableASD" visible="false">
        <tr>
            <td colspan="6" align="center">
            <label style="color:White; background-color:Blue; font-size:large ">AVERAGE SPEED OVER DISTANCE</label>
            </td>
        </tr>
        <tr>
        <td align="right" class="SubContentHead">Section Start:</td>
         <td style="width:10%; text-align:right" class="NormaBold">GPS Date & Time:</td>
        <td style="width:20%">
        <asp:TextBox runat="server" ID="tbASDGPSDatetime1" CssClass="NormalBold" 
                ReadOnly="True" Width="200px"></asp:TextBox>
        </td>
          
          <td align="right" class="SubContentHead">Section End:</td>
         <td style="width:10%; text-align:right" class="NormaBold">GPS Date & Time:</td>
        <td style="width:20%"><asp:TextBox runat="server" ID="tbASDGPSDatetime2" 
                CssClass="NormalBold" ReadOnly="True" Width="200px"></asp:TextBox></td>
        
        </tr>
        <tr>
        <td></td>
        <td style="width:10%; text-align:right" class="NormaBold">Lane Number:</td>
        <td><asp:TextBox runat="server" ID="tbASDLane1" CssClass="NormalBold" 
                ReadOnly="True" Width="67px"></asp:TextBox></td>
        <td></td>
         <td style="width:10%; text-align:right" class="NormaBold">Lane Number:</td>
        <td><asp:TextBox runat="server" ID="tbASDLane2" CssClass="NormalBold" 
                ReadOnly="True"></asp:TextBox></td>
        </tr>
        </table> 
        
        <table width="100%">
                    <tr>
                        <td align="left" valign="middle">
                             <asp:Button ID="btnShowOwner" runat="server" CssClass="NormalButton" OnClientClick="ShowOwnerClick();"
                                  Text="Show owner details" Width="200px"  />
       
                             <asp:Label ID="LabelMessageInfo" runat="server" Font-Bold="True" 
                                 ForeColor="Red"></asp:Label>
       
                        </td>
                            <td align="right"><input id="btnClose" class="NormalButtonRed" type="button" value="CLOSE" onclick="window.close();" />&nbsp;&nbsp;</td>
                    </tr>
                 </table>
         
        </ContentTemplate>
        </asp:UpdatePanel>
               <table width="100%" align="center">
               <tr>
              <td width="70%" >
                   <iv1:ImageViewer ID="imageViewer1" runat="server" />      
              </td></tr>
       
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="5%" width="100%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width: 100%" align="center">
                    <asp:UpdateProgress ID="udp" runat="server" AssociatedUpdatePanelID="udpFrame">
                        <ProgressTemplate>
                            <p class="NormalBold" style="text-align: center">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
                    

        </table>
             
    </form>

    <script language="javascript" type="text/javascript">
    <!--
	setTimeout(enableButtons,2000);
   
    function enableButtons() 
    {

	    // updated by LMZ 03-01-2007
	    if (document.getElementById("btnAccept") != null) 
	    {
	        document.getElementById("btnAccept").disabled = false;
	    }

	    if (document.getElementById("btnChange") != null && '<%= _ISASD%>' != 'True') {
	        document.getElementById("btnChange").disabled = false;
	    }
	    else
	        document.getElementById("btnChange").disabled = true; 
	    {
	    }
    }
    
    function AllowChange()
	{
	    document.getElementById("txtRegNo").readOnly = false;
	    document.getElementById("txtFirstSpeed").readOnly = false;
	    document.getElementById("txtSecondSpeed").readOnly = false;
	    document.getElementById("txtOffenceDate").readOnly = false;
	    document.getElementById("txtOffenceTime").readOnly = false;
	    
        document.getElementById("ddlRejection").disabled = false;
        document.getElementById("ddlRejection").style.display = "";
        document.getElementById("lblRejection").style.display = "";
        document.getElementById("ddlVehMake").disabled = false;
        document.getElementById("ddlVehType").disabled = false;        
	}
          
    function window_onload() {
        document.getElementById("lblMessage").style.display = "none";
    }

    /// LMZ 04-04-2007
    /// <summary>
    /// doChange - updates the Accept - Reject action 
    /// passing a JavaScript reference to the field that we want to set.
    /// </summary>
    /// <param name="strField">String. The JavaScript reference to the field that we want to set, in the format: FormName.FieldName
    /// Please note that JavaScript is case-sensitive.</param>
    function doChange() {
        var sValue = document.Form1.ddlRejection.options[document.Form1.ddlRejection.selectedIndex].text;
        var nRegNo = 0;

        if (sValue == "None") {
            document.Form1.btnAccept.value = "Accept";
            nRegNo = parseInt(document.Form1.txtRegNo.value);
            if (nRegNo == 0) {

                if (document.getElementById("lblMessage").firstChild == null) {
                    document.getElementById("lblMessage").appendChild(document.createTextNode("Registration may not be zeroes if it has been accepted"));
                }
                else {
                    document.getElementById("lblMessage").firstChild.nodeValue = "Registration may not be zeroes if it has been accepted";
                }
                return;
            }
        }
        else
            document.Form1.btnAccept.value = "Reject";
    }


    function ShowOwnerClick() {
        var name = navigator.appName;
        if (name == "Microsoft Internet Explorer") {
            var modalPopupBehavior = $find('ModalPopupBehavior');
            modalPopupBehavior.show();
            var bgElement = document.getElementById('ModalPopupBehavior_backgroundElement');

            var addressPanel = document.getElementById("<%= this.addressPanel.ClientID %>");
            //var dropShadow = document.getElementById('addressPanel_DropShadow')
            addressPanel.style.position = 'absolute';
            //dropShadow.style.position ='absolute';
            addressPanel.style.top = (document.body.offsetHeight - addressPanel.offsetHeight) / 2;
            addressPanel.style.left = (document.body.offsetWidth - addressPanel.offsetWidth) / 2;
            //dropShadow.style.top = (document.body.offsetHeight-addressPanel.offsetHeight)/2;
            //dropShadow.style.left =(document.body.offsetWidth-addressPanel.offsetWidth)/2;
            //alert(addressPanel.style.top);
            //alert(dropShadow.style.top);
            bgElement.style.position = 'absolute';
            bgElement.style.top = 0;
            bgElement.style.height = document.body.offsetHeight;

            //alert(parseFloat((document.body.offsetHeight-addressPanel.offsetHeight)/2));
        }

        return false;
    }

    function clearMessage() {
        var lbMessageControl = document.getElementById('<%= LabelMessage.ClientID %>');
        //alert(lbMessageControl);
        //lbMessageControl.value ='';
        return false;
    }
-->
    </script>

</body>
</html>
