using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class SpeedZone : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "SpeedZone.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "Speed zone maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();
            
            //get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
			autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddSpeedZone.Visible = false;
				pnlUpdateSpeedZone.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

				BindGrid();
			}		
		}

		protected void BindGrid()
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.SpeedZoneDB szList = new Stalberg.TMS.SpeedZoneDB(connectionString);

			DataSet data = szList.GetSpeedZoneListDS();
			dgSpeedZone.DataSource = data;
			dgSpeedZone.DataKeyField = "SZIntNo";
			dgSpeedZone.DataBind();

			if (dgSpeedZone.Items.Count == 0)
			{
				dgSpeedZone.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
				dgSpeedZone.Visible = true;
			}

			data.Dispose();
			pnlAddSpeedZone.Visible = false;
			pnlUpdateSpeedZone.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddSpeedZone.Visible = true;
			pnlUpdateSpeedZone.Visible = false;

			btnOptDelete.Visible = false;


            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
		}

		protected void btnAddSpeedZone_Click(object sender, System.EventArgs e)
		{		
			// add the new transaction to the database table
			Stalberg.TMS.SpeedZoneDB szAdd = new SpeedZoneDB(connectionString);
			
			int addSZIntNo = szAdd.AddSpeedZone(Convert.ToInt32(txtAddSZSpeed.Text), 
				txtAddSZDescr.Text, this.loginUser);

			if (addSZIntNo <= 0)
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
			else
			{

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(addSZIntNo, lgEntityList, "SpeedZoneLookup", "SZIntNo", "SZDescr", this.loginUser);

                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SpeedZonesMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			int szIntNo = Convert.ToInt32(Session["editSZIntNo"]);

			// add the new transaction to the database table
			Stalberg.TMS.SpeedZoneDB szUpdate = new SpeedZoneDB(connectionString);
			
			int updSZIntNo = szUpdate.UpdateSpeedZone(szIntNo, 
				Convert.ToInt32(txtSZSpeed.Text), txtSZDescr.Text, this.loginUser);

			if (updSZIntNo <= 0)
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
			}
			else
			{
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(updSZIntNo, lgEntityList, "SpeedZoneLookup", "SZIntNo", "SZDescr", this.loginUser);
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SpeedZonesMaintenance, PunchAction.Change);  
			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void dgSpeedZone_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgSpeedZone.SelectedIndex = e.Item.ItemIndex;

			if (dgSpeedZone.SelectedIndex > -1) 
			{			
				int editSZIntNo = Convert.ToInt32(dgSpeedZone.DataKeys[dgSpeedZone.SelectedIndex]);

				Session["editSZIntNo"] = editSZIntNo;
				Session["prevPage"] = thisPageURL;


                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editSZIntNo.ToString(), "SpeedZoneLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);


				ShowSpeedZoneDetails(editSZIntNo);
			}
		}

		protected void ShowSpeedZoneDetails(int editSZIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.SpeedZoneDB speedZone = new Stalberg.TMS.SpeedZoneDB(connectionString);
			
			Stalberg.TMS.SpeedZoneDetails szDetails = speedZone.GetSpeedZoneDetails(editSZIntNo);

			txtSZSpeed.Text = szDetails.SZSpeed.ToString();
			txtSZDescr.Text = szDetails.SZDescr;
			
			pnlUpdateSpeedZone.Visible = true;
			pnlAddSpeedZone.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

	
		protected void dgSpeedZone_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgSpeedZone.CurrentPageIndex = e.NewPageIndex;

			BindGrid();
				
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int szIntNo = Convert.ToInt32(Session["editSZIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "SpeedZoneLookup", "SZIntNo");

			SpeedZoneDB speedZone = new Stalberg.TMS.SpeedZoneDB(connectionString);

			string delSZIntNo = speedZone.DeleteSpeedZone(szIntNo);

			if (delSZIntNo.Equals("0"))
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
			}
            else if (delSZIntNo.Equals("-1"))
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            }
            else
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");

                //2014-01-21 Heidi fixed bug for When a page is only a data, to delete the data CurrentPageIndex is not correct (5103)
                if (dgSpeedZone.Items.Count == 1)
                {
                    dgSpeedZone.CurrentPageIndex--;
                }
                
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SpeedZonesMaintenance, PunchAction.Delete);  
            }
			lblError.Visible = true;

			BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgSpeedZone.Visible.Equals(true))
			{
				//hide it
				dgSpeedZone.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				//show it
				dgSpeedZone.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		 

	}
}
