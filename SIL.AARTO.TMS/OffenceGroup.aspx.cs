using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{

    public partial class OffenceGroup : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string thisPageURL = "OffenceGroup.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private int autIntNo = 0;
        //protected string thisPage = "Offence group maintenance";

        protected override void OnLoad(System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            //

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddOffence.Visible = false;
                pnlUpdateOffence.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptHide.Visible = true;

                PopulateRoadTypes(ddlRoadType);
                PopulateSpeedZone(ddlSpeedZone);
                PopulateVehicleTypeGroups(ddlVehicleTypeGroup);
                PopulateOffenceTypes(ddlOffenceType);

                txtSearch.Text = string.Empty;

                BindGrid();
            }
        }



        protected void BindGrid()
        {
            // Obtain and bind a list of all users

            Stalberg.TMS.OffenceGroupDB offList = new Stalberg.TMS.OffenceGroupDB(connectionString);

            DataSet data = offList.GetOffenceGroupListDS(txtSearch.Text);
            dgOffenceGroup.DataSource = data;
            dgOffenceGroup.DataKeyField = "OGIntNo";

            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgOffenceGroup.DataBind();
            }
            catch
            {
                dgOffenceGroup.CurrentPageIndex = 0;
                dgOffenceGroup.DataBind();
            }

            if (dgOffenceGroup.Items.Count == 0)
            {
                dgOffenceGroup.Visible = false;
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgOffenceGroup.Visible = true;
            }

            data.Dispose();
            pnlAddOffence.Visible = false;
            pnlUpdateOffence.Visible = false;

            //show it
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlAddOffence.Visible = true;
            pnlUpdateOffence.Visible = false;

            btnOptDelete.Visible = false;

            PopulateRoadTypes(ddlAddRoadType);
            //2012-3-7 linda modified into a multi-language
            ddlAddRoadType.Items.Insert(0, (string)GetLocalResourceObject("ddlAddRoadType.Items1"));

            PopulateSpeedZone(ddlAddSpeedZone);
            //2012-3-7 linda modified into a multi-language
            ddlAddSpeedZone.Items.Insert(0, (string)GetLocalResourceObject("ddlAddRoadType.Items2"));

            PopulateVehicleTypeGroups(ddlAddVehicleTypeGroup);
            //2012-3-7 linda modified into a multi-language
            ddlAddVehicleTypeGroup.Items.Insert(0, (string)GetLocalResourceObject("ddlAddRoadType.Items3"));

            PopulateOffenceTypes(ddlAddOffenceType);
            //2012-3-7 linda modified into a multi-language
            ddlAddOffenceType.Items.Insert(0, (string)GetLocalResourceObject("ddlAddRoadType.Items4"));

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);

            ResetControls();
        }

        protected void PopulateVehicleTypeGroups(DropDownList ddlSelVehicleTypeGroups)
        {
            VehicleTypeGroupDB group = new VehicleTypeGroupDB(connectionString);

            SqlDataReader reader = group.GetVehicleTypeGroupList("");

            Dictionary<int, string> lookups =
                VehicleTypeGroupLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int vTGIntNo = (int)reader["VTGIntNo"];
                if (lookups.ContainsKey(vTGIntNo))
                {
                    ddlSelVehicleTypeGroups.Items.Add(new ListItem(lookups[vTGIntNo], vTGIntNo.ToString()));
                }
            }
            //ddlSelVehicleTypeGroups.DataSource = reader;
            //ddlSelVehicleTypeGroups.DataValueField = "VTGIntNo";
            //ddlSelVehicleTypeGroups.DataTextField = "VTGDescr";
            //ddlSelVehicleTypeGroups.DataBind();
            reader.Close();
        }

        protected void PopulateRoadTypes(DropDownList ddlSelRoadType)
        {
            RoadTypeDB road = new RoadTypeDB(connectionString);

            SqlDataReader reader = road.GetRoadTypeList();

            Dictionary<int, string> lookups =
                RoadTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int rdTIntNo = (int)reader["RdTIntNo"];
                if (lookups.ContainsKey(rdTIntNo))
                {
                    ddlSelRoadType.Items.Add(new ListItem(lookups[rdTIntNo], rdTIntNo.ToString()));
                }
            }
            //ddlSelRoadType.DataSource = reader;
            //ddlSelRoadType.DataValueField = "RdTIntNo";
            //ddlSelRoadType.DataTextField = "RdTypeDescr";
            //ddlSelRoadType.DataBind();
            reader.Close();
        }

        protected void PopulateSpeedZone(DropDownList ddlSelSpeedZone)
        {
            SpeedZoneDB speed = new SpeedZoneDB(connectionString);

            SqlDataReader reader = speed.GetSpeedZoneList();

            Dictionary<int, string> lookups =
                SpeedZoneLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int sZIntNo = (int)reader["SZIntNo"];
                if (lookups.ContainsKey(sZIntNo))
                {
                    ddlSelSpeedZone.Items.Add(new ListItem(lookups[sZIntNo], sZIntNo.ToString()));
                }
            }
            //ddlSelSpeedZone.DataSource = reader;
            //ddlSelSpeedZone.DataValueField = "SZIntNo";
            //ddlSelSpeedZone.DataTextField = "SZSpeed";
            //ddlSelSpeedZone.DataBind();
            reader.Close();
        }

        protected void PopulateOffenceTypes(DropDownList ddlSelOffenceTypes)
        {
            OffenceTypeDB type = new OffenceTypeDB(connectionString);

            SqlDataReader reader = type.GetOffenceTypeList();

            Dictionary<int, string> lookups =
                OffenceTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int ocTIntNo = (int)reader["OcTIntNo"];
                if (lookups.ContainsKey(ocTIntNo))
                {
                    ddlSelOffenceTypes.Items.Add(new ListItem(lookups[ocTIntNo], ocTIntNo.ToString()));
                }
            }
            //ddlSelOffenceTypes.DataSource = reader;
            //ddlSelOffenceTypes.DataValueField = "OcTIntNo";
            //ddlSelOffenceTypes.DataTextField = "OcType";
            //ddlSelOffenceTypes.DataBind();
            reader.Close();
        }

        protected void btnAddOffence_Click(object sender, System.EventArgs e)
        {
            if (ddlAddRoadType.SelectedIndex < 0)
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            else if (ddlAddSpeedZone.SelectedIndex < 0)
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            else if (ddlAddVehicleTypeGroup.SelectedIndex < 0)
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }

            int vtgIntNo = Convert.ToInt32(ddlAddVehicleTypeGroup.SelectedValue);
            int szIntNo = Convert.ToInt32(ddlAddSpeedZone.SelectedValue);
            int rdtIntNo = Convert.ToInt32(ddlAddRoadType.SelectedValue);
            int ocTIntNo = Convert.ToInt32(ddlAddOffenceType.SelectedValue);

            // add the new transaction to the database table
            Stalberg.TMS.OffenceGroupDB toAdd = new OffenceGroupDB(connectionString);

            int addOGIntNo = toAdd.AddOffenceGroup(szIntNo, rdtIntNo, vtgIntNo, ocTIntNo,
                txtAddOGCode.Text, txtAddOGDescr.Text, txtAddOGStatutoryRef.Text, loginUser,
                txtAddOGStatutoryRefShort.Text, this.txtAddOffGroupS35StatRef.Text, this.txtAddOffGroupS35ShortStatRef.Text);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addOGIntNo, lgEntityList, "OffenceGroupLookup", "OGIntNo", "OGDescr", this.loginUser);

            if (addOGIntNo <= 0)
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            }
            else
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
				//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceGroupsMaintenance, PunchAction.Add);              
			}

            lblError.Visible = true;

            BindGrid();
        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            int offIntNo = Convert.ToInt32(Session["editOGIntNo"]);

            int vtgIntNo = Convert.ToInt32(ddlVehicleTypeGroup.SelectedValue);
            int szIntNo = Convert.ToInt32(ddlSpeedZone.SelectedValue);
            int rdtIntNo = Convert.ToInt32(ddlRoadType.SelectedValue);
            int ocTIntNo = Convert.ToInt32(ddlOffenceType.SelectedValue);

            // add the new transaction to the database table
            Stalberg.TMS.OffenceGroupDB ogUpdate = new OffenceGroupDB(connectionString);

            int updOGIntNo = ogUpdate.UpdateOffenceGroup(offIntNo, szIntNo, rdtIntNo,
                vtgIntNo, ocTIntNo, txtOGCode.Text, txtOGDescr.Text,
                txtOGStatutoryRef.Text, loginUser, txtOGStatutoryRefShort.Text,
                 this.txtOffGroupS35StatRef.Text, this.txtOffGroupS35ShortStatRef.Text);

            if (updOGIntNo <= 0)
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            }
            else
            {

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(updOGIntNo, lgEntityList, "OffenceGroupLookup", "OGIntNo", "OGDescr", this.loginUser);
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceGroupsMaintenance, PunchAction.Change); 
			}

            lblError.Visible = true;

            BindGrid();
        }

        private void ResetControls()
        {
            ddlAddVehicleTypeGroup.SelectedIndex = 0;
            ddlAddSpeedZone.SelectedIndex = 0;
            ddlAddRoadType.SelectedIndex = 0;
            ddlAddOffenceType.SelectedIndex = 0;
            txtAddOGCode.Text = "";
            txtAddOGDescr.Text = "";
            txtAddOGStatutoryRef.Text = "";
            txtAddOGStatutoryRefShort.Text = "";

            this.txtAddOffGroupS35StatRef.Text = "";
            this.txtAddOffGroupS35ShortStatRef.Text = "";


        }

        protected void ShowOffenceGroupDetails(int editOGIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.OffenceGroupDB group = new Stalberg.TMS.OffenceGroupDB(connectionString);

            Stalberg.TMS.OffenceGroupDetails ogDetails = group.GetOffenceGroupDetails(editOGIntNo);

            txtOGCode.Text = ogDetails.OGCode;
            txtOGDescr.Text = ogDetails.OGDescr;
            txtOGStatutoryRef.Text = ogDetails.OGStatutoryRef;
            txtOGStatutoryRefShort.Text = ogDetails.OGStatutoryRefShort;
            this.txtOffGroupS35ShortStatRef.Text = ogDetails.OffGroupS35StatRefShot;
            this.txtOffGroupS35StatRef.Text = ogDetails.OffGroupS35StatRef;

            ddlRoadType.SelectedIndex = ddlRoadType.Items.IndexOf(ddlRoadType.Items.FindByValue(ogDetails.RdTIntNo.ToString()));
            ddlSpeedZone.SelectedIndex = ddlSpeedZone.Items.IndexOf(ddlSpeedZone.Items.FindByValue(ogDetails.SZIntNo.ToString()));
            ddlVehicleTypeGroup.SelectedIndex = ddlVehicleTypeGroup.Items.IndexOf(ddlVehicleTypeGroup.Items.FindByValue(ogDetails.VTGIntNo.ToString()));
            ddlOffenceType.SelectedIndex = ddlOffenceType.Items.IndexOf(ddlOffenceType.Items.FindByValue(ogDetails.OcTIntNo.ToString()));

            pnlUpdateOffence.Visible = true;
            pnlAddOffence.Visible = false;

            btnOptDelete.Visible = true;
        }


        protected void dgOffenceGroup_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgOffenceGroup.CurrentPageIndex = e.NewPageIndex;

            BindGrid();

        }

        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
            int ogIntNo = Convert.ToInt32(Session["editOGIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "OffenceGroupLookup", "OGIntNo");

            OffenceGroupDB og = new Stalberg.TMS.OffenceGroupDB(connectionString);

            string delOGIntNo = og.DeleteOffenceGroup(ogIntNo);

            if (delOGIntNo.Equals("0"))
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            }
            else if (delOGIntNo.Equals("-1"))
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            }
            else
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceGroupsMaintenance, PunchAction.Delete); 
            }
            lblError.Visible = true;

            BindGrid();
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgOffenceGroup.Visible.Equals(true))
            {
                //hide it
                dgOffenceGroup.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
            }
            else
            {
                //show it
                dgOffenceGroup.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        //protected void btnOptUpdateOffenceFine_Click(object sender, System.EventArgs e)
        //{
        //    // allow transactions to be added to the database table
        //    pnlAddOffence.Visible = false;
        //    pnlUpdateOffence.Visible = false;
        //    btnOptDelete.Visible = false;
        //    pnlUpdateOffenceFine.Visible  = true;
        //    pnlSearch.Visible = false;
        //    dgOffenceGroup.Visible = false;
        //    //this.txtLA.Text = Session["autIntNo"].ToString(); 

        //}

        //protected void btnUpdateFine_Click(object sender, System.EventArgs e)
        //{
        //    OffenceFineDB of = new Stalberg.TMS.OffenceFineDB(connectionString);
        //    string sHold = String.Empty;
        //    try
        //    {
        //        sHold = this.ddlSelOffenceGroup.SelectedItem.Text;
        //        sHold = sHold.Substring(0, sHold.IndexOf('~') - 1);
        //    }
        //    catch (Exception ex)
        //    {
        //        this.lblError.Text = "Error in offence group selection";
        //        return;
        //    }
        //    this.lblError.Text = of.UpdateSpecial(this.ddlAuthority.SelectedItem.Value, this.txtEffectiveDate.Text, this.txtUpdateToDate.Text, sHold);   
        //}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindGrid();
            //PunchStats805806 enquiry OffenceGroups
		}


        protected void dgOffenceGroup_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgOffenceGroup.SelectedIndex = e.Item.ItemIndex;

            if (dgOffenceGroup.SelectedIndex > -1)
            {
                int editOGIntNo = Convert.ToInt32(dgOffenceGroup.DataKeys[dgOffenceGroup.SelectedIndex]);

                Session["editOGIntNo"] = editOGIntNo;
                Session["prevPage"] = thisPageURL;


                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editOGIntNo.ToString(), "OffenceGroupLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);


                ShowOffenceGroupDetails(editOGIntNo);
            }
        }




    }
}
