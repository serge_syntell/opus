<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>


<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.VehicleTypeGroup" Codebehind="VehicleTypeGroup.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="658px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table id="Table1" height="30" cellspacing="1" cellpadding="1" width="542" border="0">
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="10"></asp:TextBox></td>
                                            <td width="7">
                                                <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                    OnClick="btnSearch_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgVehicleTypeGroup" Width="495px" runat="server" BorderColor="Black"
                                    AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgVehicleTypeGroup_ItemCommand"
                                    >
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="VTGIntNo" HeaderText="VTGIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="VTGCode" HeaderText="<%$Resources:dgVehicleTypeGroup.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="VTGDescr" HeaderText="<%$Resources:dgVehicleTypeGroup.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgVehicleTypeGroupItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <pager:AspNetPager id="dgVehicleTypeGroupPager" runat="server" 
                                            showcustominfosection="Right" width="495px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                            FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"
                                            PageSize="10"
                                    onpagechanged="dgVehicleTypeGroupPager_PageChanged"></pager:AspNetPager>

                                <asp:Panel ID="pnlAddVehicleTypeGroup" runat="server" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAddVehicleTypeGroup" runat="server" Width="179px" CssClass="ProductListHead" Text="<%$Resources:lblAddVehicleTypeGroup.Text %>"></asp:Label></td>
                                            <td>
                                            </td>
                                            <td >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAddVTGCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddVTGCode.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtAddVTGCode" runat="server" Width="83px" CssClass="NormalMand"
                                                    MaxLength="5" Height="24px"></asp:TextBox></td>
                                            <td >
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtAddVTGCode" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                                      <tr>
                                                <td >
                                                    <asp:Label ID="Label26" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddVTGDescr.Text %>"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="txtAddVTGDescr" runat="server" CssClass="NormalMand" 
                                                    Height="24px" MaxLength="30" Width="228px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:Label ID="Label20" runat="server" CssClass="NormalBold" Text="<%$Resources:lblVTGMaxSpeed.Text %>"></asp:Label></td>
                                                <td  valign="top">
                                                    <asp:TextBox ID="txtAddVTGMaxSpeed" runat="server" CssClass="NormalMand" 
                                                        Height="24px" MaxLength="3" Width="83px"></asp:TextBox>
                                                </td>
                                                <td >
                                                    <asp:Label ID="Label22" runat="server" CssClass="NormalRed" Text="<%$Resources:lblRequired.Text %>"></asp:Label>
                                                </td>
                                            </tr>
                                     
                                        <tr>
                                            <td ID="Label21" runat="server" CssClass="NormalBold" >
                                           <asp:Label ID="lblOffenceType" runat="server" CssClass="NormalRed" Text="<%$Resources:lblOffenceType.Text %>"> </asp:label>
                                            </td>
                                            <td valign="top" >
                                                <asp:DropDownList ID="ddlAddOffenceType" runat="server" CssClass="Normal" 
                                                    Width="250px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td> 
                                                <asp:Button ID="btnAddVehicleTypeGroup" runat="server" CssClass="NormalButton" 
                                                    OnClick="btnAddVehicleTypeGroup_Click" Text="<%$Resources:btnAddVehicleTypeGroup.Text %>" OnClientClick="return VerifytLookupRequired()" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateVehicleTypeGroup" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label19" runat="server" Width="218px" CssClass="ProductListHead" Text="<%$Resources:lblUpdate.Text %>"></asp:Label></td>
                                            <td>
                                            </td>
                                            <td >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddVTGCode.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtVTGCode" runat="server" Width="83px" CssClass="NormalMand" MaxLength="5"
                                                    Height="24px"></asp:TextBox></td>
                                            <td >
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtVTGCode" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top"  height="25">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddVTGDescr.Text %>"></asp:Label></td>
                                            <td valign="top"  height="25">
                                                <asp:TextBox ID="txtVTGDescr" runat="server" Width="228px" CssClass="NormalMand"
                                                    MaxLength="30" Height="24px"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                          <tr>
                                                <td  valign="top" >
                                                    <asp:Label ID="Label23" runat="server" CssClass="NormalBold" Text="<%$Resources:lblVTGMaxSpeed.Text %>"></asp:Label>
                                                </td>
                                                <td  valign="top" >
                                                    <asp:TextBox ID="txtVTGMaxSpeed" runat="server" CssClass="NormalMand" 
                                                        Height="24px" MaxLength="3" Width="83px"></asp:TextBox>
                                                </td>
                                                <td >
                                                    <asp:Label ID="Label25" runat="server" CssClass="NormalRed" Text="<%$Resources:lblRequired.Text %>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td   valign="top">
                                                    <asp:Label ID="Label24" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOffenceType.Text %>"></asp:Label>
                                                </td>
                                                <td  valign="top">
                                                    <asp:DropDownList ID="ddlOffenceType" runat="server" 
                                                        CssClass="Normal" Width="250px">
                                                    </asp:DropDownList>
                                                   </td>
                                                <td >
                                                    &nbsp;</td>
                                            </tr>
                                        <tr>
                                            <td >
                                            </td>
                                            <td >
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
