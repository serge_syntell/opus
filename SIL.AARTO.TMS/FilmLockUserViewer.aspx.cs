using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
	/// <summary>
	/// Summary description for ApplicationObjectViewer
	/// </summary>
    public partial class FilmLockUserViewer : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string tempFileLoc = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = "FilmLockUserViewer";
        protected string description = String.Empty;
        protected string thisPageURL = "FilmLockUserViewer.aspx";
        protected int autIntNo = -1;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected string loginUser;
               
        protected override void OnLoad(System.EventArgs e)
        {
            
            connectionString = Application["constr"].ToString();
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;
            //int 

            if (!Page.IsPostBack)
            {
                if (ddlSelectLA.SelectedIndex > -1)
                {
                    this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                }

                this.PopulateAuthorities(autIntNo);

                pnlLockFilm.Visible = true;
                BindGrid();
            }
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities( int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void BindGrid()
        {
            FilmDB db = new FilmDB(connectionString);
            this.grdHeader.DataSource = db.GetFilmLockUserList(autIntNo);
            this.grdHeader.DataBind();
            if (grdHeader.Rows.Count > 0)
            {
                pnlGrid.Visible = true;
                btnRelease.Visible = true;
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                btnRelease.Visible = false;
                pnlGrid.Visible = false;
            }
        }
        
        protected void btnHideMenu_Click(object sender, System.EventArgs e)
        {
            if (pnlMainMenu.Visible.Equals(true))
            {
                pnlMainMenu.Visible = false;
                btnHideMenu.Text = (string)GetLocalResourceObject("btnHideMenu.Text1");
            }
            else
            {
                pnlMainMenu.Visible = true;
                btnHideMenu.Text = (string)GetLocalResourceObject("btnHideMenu.Text");
            }
        }

        protected void btnRelease_Click(object sender, EventArgs e)
        {
            FilmDB db = new FilmDB(connectionString);
            int nRes = db.ReleaseFilmLockUser(autIntNo, (Session["userDetails"] as UserDetails).UserLoginName);
            if (nRes == -1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                pnlGrid.Visible = false;
                btnRelease.Visible = false;
            }
            else
            {
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.FilmLocksViewAndRelease, PunchAction.Change);  

            }
            BindGrid();
            //Server.Transfer("FilmLockUserViewer.aspx");
        }

        protected void grdHeader_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        protected void grdHeader_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }

        protected void grdHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnLockFilm_Click(object sender, EventArgs e)
        {
            lblError.Visible = true;
            string errMessage = string.Empty;

            FilmDB film = new FilmDB(connectionString);

            int locFilm = film.LockFilm(txtFilmNo.Text, ref errMessage, (Session["userDetails"] as UserDetails).UserLoginName);

            if (locFilm > 0)
            {
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), txtFilmNo.Text);
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.FilmLocksViewAndRelease, PunchAction.Change); 
            }
            else if (locFilm == -1)
            {
                //2014-01-20 Heidi fixed bug for Prompt information is not correct.(5103)
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text3"), txtFilmNo.Text);
            }
            else if (locFilm == -2)
            {
                //2014-01-20 Heidi fixed bug for Prompt information is not correct.(5103)
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text4"), txtFilmNo.Text);
            }
            else if (locFilm == 0)
            {
                //2014-01-20 Heidi fixed bug for Prompt information is not correct.(5103)
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), txtFilmNo.Text);
            }
           
            BindGrid();
        }

}   

}
