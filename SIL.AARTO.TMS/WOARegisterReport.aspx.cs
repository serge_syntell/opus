using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    public partial class WOARegisterReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = "WOARegisterReport";
        protected string description = String.Empty;
        protected string thisPageURL = "WOARegisterReport.aspx";

        private const string DATE_FORMAT = "yyyy-MM-dd";


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.PopulateAuthorities();
                this.PopulateAuditReports();
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void PopulateAuditReports()
        {
            this.ddlOrderBy.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlOrderBy.Items"), "CrtNo"));
            this.ddlOrderBy.Items.Insert(1, new ListItem((string)GetLocalResourceObject("ddlOrderBy.Items1"), "CrtDate"));
            this.ddlOrderBy.Items.Insert(2, new ListItem((string)GetLocalResourceObject("ddlOrderBy.Items2"), "WoaNo"));
            this.ddlOrderBy.Items.Insert(3, new ListItem((string)GetLocalResourceObject("ddlOrderBy.Items3"), "AreaCode"));
            this.ddlOrderBy.SelectedIndex = 0;
        }

       

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (this.ddlAuthority.SelectedValue.Equals(string.Empty) || this.ddlAuthority.SelectedIndex < 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            this.lblError.Text = string.Empty;
            int autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            Helper_Web.BuildPopup(this, "WOARegisterReport_Viewer.aspx", "AutIntNo", autIntNo.ToString(), "OrderBy", ddlOrderBy.SelectedValue);
            //PunchStats805806 enquiry WOARegisterReport
        }

    }
}


