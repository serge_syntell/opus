using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
	/// <summary>
	/// </summary>
    public partial class ReceiptCashBoxSetup : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "ReceiptCashBoxSetup.aspx";
        //protected string thisPage = "Receipts: Cash box setup";

        // Fields
        private int autIntNo = 0;

        // Constants
        private const string LA_SELECT_TEXT = "Select an Authority";
        private const string CURRENCY_FORMAT = "####";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            btnOptAdd.Visible = true;
            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            int userAccessLevel = userDetails.UserAccessLevel;
            loginUser = userDetails.UserLoginName;

            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            //Session["editAutIntNo"] = this.autIntNo.ToString();
            // Set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlCashboxes.Visible = false;
                this.btnOptDelete.Visible = false;
                this.pnlEditCashbox.Visible = false;
                this.pnlAddCashbox.Visible = false;

                if (autIntNo > 0)
                {
                    this.BindGrid();
                    pnlCashboxes.Visible = true;
                    // PopulateCourt (ddlAuth_Court, autIntNo);
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    lblError.Visible = true;
                }
            }
        }

        //private void BindGrid(int autIntNo)
        //{
        //    Stalberg.TMS.CashboxDB cashbox = new Stalberg.TMS.CashboxDB(connectionString);
        //    DataSet ds = cashbox.GetCashboxListDS(autIntNo);
        //    this.grdCashbox.DataSource = ds;
        //    this.grdCashbox.DataBind();
        //}

        private void BindGrid()
        {
            Stalberg.TMS.CashboxDB cashbox = new Stalberg.TMS.CashboxDB(connectionString);
            //Jerry 2013-10-12 change
            //DataSet ds = cashbox.GetCashboxListDS();
            int totalCount = 0;
            DataSet ds = cashbox.GetCashboxListDS(this.autIntNo, grdCashBoxPager.PageSize, grdCashBoxPager.CurrentPageIndex, out totalCount);
            this.grdCashbox.DataSource = ds;
            this.grdCashbox.DataKeyField = "CBIntNo";
            this.grdCashbox.DataBind();
            grdCashBoxPager.RecordCount = totalCount;

            btnOptDelete.Visible = false;
        }

        protected void grdCashbox_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            //get cashbox detail and display it
            int cbIntNo = int.Parse(e.Item.Cells[0].Text);
            Session["editCBIntNo"] = Convert.ToInt32(cbIntNo);
            ShowCashboxDetails(cbIntNo);
        }

        protected void ShowCashboxDetails(int cbIntNo)
        {
            Stalberg.TMS.CashboxDB cashbox = new Stalberg.TMS.CashboxDB(connectionString);
            Stalberg.TMS.CashboxDetails cbDetails = cashbox.GetCashboxDetails(cbIntNo);
            txtCBName.Text = cbDetails.CBName;
            txtCBStartAmount.Text = cbDetails.CBStartAmount.ToString(CURRENCY_FORMAT);
            txtCBRand500s.Text = cbDetails.CBRand500s.ToString(CURRENCY_FORMAT);
            txtCBRand200s.Text = cbDetails.CBRand200s.ToString(CURRENCY_FORMAT);
            txtCBRand100s.Text = cbDetails.CBRand100s.ToString(CURRENCY_FORMAT);
            txtCBRand50s.Text = cbDetails.CBRand50s.ToString(CURRENCY_FORMAT);
            txtCBRand20s.Text = cbDetails.CBRand20s.ToString(CURRENCY_FORMAT);
            txtCBRand10s.Text = cbDetails.CBRand10s.ToString(CURRENCY_FORMAT);
            txtCBRand5s.Text = cbDetails.CBRand5s.ToString(CURRENCY_FORMAT);
            txtCBRand2s.Text = cbDetails.CBRand2s.ToString(CURRENCY_FORMAT);
            txtCBRand1s.Text = cbDetails.CBRand1s.ToString(CURRENCY_FORMAT);
            txtCBReceived.Text = cbDetails.CBReceived.ToString(CURRENCY_FORMAT);
            txtCBBanked.Text = cbDetails.CBBanked.ToString(CURRENCY_FORMAT);

            pnlEditCashbox.Visible = true;
            pnlCashboxes.Visible = false;
            pnlAddCashbox.Visible = false;
            btnOptDelete.Visible = true;
        }

        protected void btnOptAdd_Click(object sender, EventArgs e)
        {
            // display the add cashbox panel
            pnlAddCashbox.Visible = true;
            pnlEditCashbox.Visible = false;
            pnlCashboxes.Visible = false;
            btnOptDelete.Visible = false;
        }

        protected void btnAddCB_Click(object sender, EventArgs e)
        {
            //int autIntNo = Convert.ToInt32(Session["editAutIntNo"]);
            int cbIntNo = 0;
            // calculate notes and coins = start+received-banked
            double onhand = 0;
            int coinage = 0;
            double startAmount = 0.0;
            int rand500 = 0;
            int rand200 = 0;
            int rand100 = 0;
            int rand50 = 0;
            int rand20 = 0;
            int rand10 = 0;
            int rand5 = 0;
            int rand2 = 0;
            int rand1 = 0;
            double received = 0.0;
            double banked = 0.0;
            try
            {
                startAmount = Convert.ToDouble(txtAddCBStartAmount.Text);
                onhand += startAmount;
            }
            catch
            {
            }
            try
            {
                received = Convert.ToDouble(txtAddCBReceived.Text);
                onhand += received;
            }
            catch
            {
            }
            try
            {
                banked = Convert.ToDouble(txtAddCBBanked.Text);
                onhand -= banked;
            }
            catch
            {
            }
            try
            {
                rand500 = Convert.ToInt32(txtAddCBRand500s.Text);
                coinage += rand500 * 500;
            }
            catch
            {
            }
            try
            {
                rand200 = Convert.ToInt32(txtAddCBRand200s.Text);
                coinage += rand200 * 200;
            }
            catch
            {
            }
            try
            {
                rand100 = Convert.ToInt32(txtAddCBRand100s.Text);
                coinage += rand100 * 100;
            }
            catch
            {
            }
            try
            {
                rand50 = Convert.ToInt32(txtAddCBRand50s.Text);
                coinage += rand50 * 50;
            }
            catch
            {
            }
            try
            {
                rand20 = Convert.ToInt32(txtAddCBRand20s.Text);
                coinage += rand20 * 20;
            }
            catch
            {
            }
            try
            {
                rand10 += Convert.ToInt32(txtAddCBRand10s.Text);
                coinage += rand10 * 10;
            }
            catch
            {
            }
            try
            {
                rand5 = Convert.ToInt32(txtAddCBRand5s.Text);
                coinage += rand5 * 5;
            }
            catch
            {
            }
            try
            {
                rand2 = Convert.ToInt32(txtAddCBRand2s.Text);
                coinage += rand2 * 2;
            }
            catch
            {
            }
            try
            {
                rand1 = Convert.ToInt32(txtAddCBRand1s.Text);
                coinage += rand1 * 1;
            }
            catch
            {
            }
            if (Math.Abs(onhand - coinage) > 0.005)
            {
                // error
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
            }
            else
            {
                //continue with the add
                Stalberg.TMS.CashboxDB cbAdd = new Stalberg.TMS.CashboxDB(connectionString);
                cbIntNo = cbAdd.AddCashbox(txtAddCBName.Text, startAmount, rand500, rand200, rand100, rand50, rand20, rand10, 
                    rand5, rand2, rand1, received, banked, loginUser);
                if (cbIntNo > 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CashBoxSetUp, PunchAction.Add);  

                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                }
                lblError.Visible = true;
                pnlCashboxes.Visible = true;
                pnlEditCashbox.Visible = false;
                pnlAddCashbox.Visible = false;
                BindGrid();
            }
        }

        protected void btnUpdateCB_Click(object sender, EventArgs e)
        {
            // calculate notes and coins = start+received-banked
            double onhand = 0;
            int coinage = 0;
            int rand500 = 0;
            int rand200 = 0;
            int rand100 = 0;
            int rand50 = 0;
            int rand20 = 0;
            int rand10 = 0;
            int rand5 = 0;
            int rand2 = 0;
            int rand1 = 0;
            double startAmount = 0.0;
            double received = 0.0;
            double banked = 0.0;
            try
            {
                 startAmount = Convert.ToDouble(txtCBStartAmount.Text);
                 onhand += startAmount;
            }
            catch
            {
            }
            try
            {
                received = Convert.ToDouble(txtCBReceived.Text);
                onhand += received;
            }
            catch
            {
            }
            try
            {
                banked = Convert.ToDouble(txtCBBanked.Text);
                onhand -= banked;
            }
            catch
            {
            }
            try
            {
                rand500 = Convert.ToInt32(txtCBRand500s.Text);
                coinage += rand500 * 500;
            }
            catch
            {
            }
            try
            {
                rand200 = Convert.ToInt32(txtCBRand200s.Text);
                coinage += rand200 * 200;
            }
            catch
            { 
            }
            try
            {
                rand100 = Convert.ToInt32(txtCBRand100s.Text);
                coinage += rand100 * 100;
            }
            catch
            { 
            }
            try
            {
                rand50 = Convert.ToInt32(txtCBRand50s.Text);
                coinage += rand50 * 50;
            }
            catch
            { 
            }
            try
            {
                rand20 = Convert.ToInt32(txtCBRand20s.Text);
                coinage += rand20 * 20;
            }
            catch
            { 
            }
            try
            {
                rand10 += Convert.ToInt32(txtCBRand10s.Text);
                coinage += rand10 * 10;
            }
            catch
            { 
            }
            try
            {
                rand5= Convert.ToInt32(txtCBRand5s.Text);
                coinage += rand5 * 5;
            }
            catch
            { 
            }
            try
            {
                rand2= Convert.ToInt32(txtCBRand2s.Text);
                coinage += rand2 * 2;
            }
            catch
            { 
            }
            try
            {
                rand1= Convert.ToInt32(txtCBRand1s.Text);
                coinage += rand1 * 1;
            }
            catch
            { 
            }
            if (Math.Abs(onhand - coinage) > 0.005)
            {
                // error
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                lblError.Visible = true;
            }
            else
            {
                // continue with update
                // update the transaction to the database table
                Stalberg.TMS.CashboxDB cbUpdate = new CashboxDB(connectionString);
                //int autIntNo = Convert.ToInt32(Session["editAutIntNo"]);
                int cbIntNo = Convert.ToInt32(Session["editCBIntNo"]);
                int updCBIntNo = cbUpdate.UpdateCashbox(cbIntNo, txtCBName.Text, startAmount, rand500, rand200, rand100, rand50,
                    rand20, rand10, rand5, rand2, rand1, received, banked,loginUser);

                if (updCBIntNo <= 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CashBoxSetUp, PunchAction.Change);  
                }

                lblError.Visible = true;
                pnlCashboxes.Visible = true;
                pnlEditCashbox.Visible = false;
                BindGrid();
            }
        }

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            string errMessage = string.Empty;

            //int autIntNo = Convert.ToInt32(Session["editAutIntNo"]);
            int cbIntNo = Convert.ToInt32(Session["editCBIntNo"]);
            Stalberg.TMS.CashboxDB cbDelete = new Stalberg.TMS.CashboxDB(connectionString);
            int delCBIntNo = cbDelete.DeleteCashbox(cbIntNo, ref errMessage);

            if (delCBIntNo > 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CashBoxSetUp, PunchAction.Delete);  
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8") + errMessage;
            }

            lblError.Visible = true;
            pnlEditCashbox.Visible = false;
            pnlCashboxes.Visible = true;
            BindGrid();
        }

        protected void grdCashBoxPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

    }
}
