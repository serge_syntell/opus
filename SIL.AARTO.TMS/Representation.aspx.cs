using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.QueueLibrary;
using SIL.ServiceBase;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.BLL.Utility.Printing;
using System.Threading;
using Stalberg.TMS.Data;
using IsolationLevel = System.Transactions.IsolationLevel;
using SIL.AARTO.DAL.Data;
using System.Globalization;
using System.Configuration;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents an editor page to add, edit, and print Charge Representations.
    /// </summary>
    public partial class Representation : System.Web.UI.Page
    {
        [Serializable]
        class SelectListItem
        {
            public SelectListItem(string text, string value)
            {
                Text = text;
                Value = value;
            }

            public string Text { get; set; }
            public string Value { get; set; }
        }

        [Serializable]
        class KeyValue<TKey, TValue>
        {
            public KeyValue() { }
            public KeyValue(TKey key, TValue value)
            {
                Key = key;
                Value = value;
            }
            public TKey Key { get; set; }
            public TValue Value { get; set; }
        }

        [Serializable]
        class PageCache
        {
            public bool? AlwaysAllow { get; set; }
            public string SumCaseNo { get; set; }
            public bool? IsSummons { get; set; }
            public int? CurrentChargeStatus { get; set; }
            public List<SelectListItem> RepOfficialSource { get; set; }
            public DateTime? LastRecommendDate { get; set; }
            public List<KeyValue<int, bool>> GrdRep_Select { get; set; }
            public List<KeyValue<int, bool>> GrdChg_Select { get; set; }
            public DateTime? CourtDate { get; set; }
            public bool? AllowNoAOGOffence { get; set; }
            public bool? IsNoAOG { get; set; }
            public bool? IsSection35 { get; set; }
            public bool? IsNoAOGDefined { get; set; }
            public string TempRevFineAmount { get; set; }
            public bool? IsReadOnly { get; set; }    // 2014-02-12, Oscar added "IsReadOnly" for existing Charge Rep
        }
        PageCache cache;

        // Fields
        private string connectionString = string.Empty;
        private string repAction = string.Empty;
        private bool isNoAOG = false;
        private bool isS35NoAOG = false; // Jake 2013-12-04 added
        private int noOfReps = 0;
        private bool isSummons = false;
        private bool isNew = false;

        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string thisPageURL = "Representation.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Representation Register";

        private const int CODE_REP_LOGGED_CHARGE = 410;
        //private const int CODE_REP_PENDING = 420;
        private const int CODE_CASE_CANCELLED = 927;
        private const int CODE_SUMMONS_WITHDRAWN_CHARGE = 940;
        private const int CODE_SUMMONS_WITHDRAWN = 650;
        private const int CODE_SUMMONS_REISSUE = 600;
        private const int CODE_REP_LOGGED_SUMMONS = 660;
        private const int CODE_NOTICE_NEW_OFFENDER = 255;

        private const int REPCODE_NONE = 0;
        private const int REPCODE_WITHDRAW = 3;
        private const int REPCODE_REDUCTION = 4;
        private const int REPCODE_NOCHANGE = 5;
        private const int REPCODE_CHANGEOFFENDER = 9;
        private const int REPCODE_CANCELLED = 99;

        private const int SUMMONS_REPCODE_NONE = 10;
        private const int SUMMONS_REPCODE_WITHDRAW = 13;
        private const int SUMMONS_REPCODE_REDUCTION = 14;
        private const int SUMMONS_REPCODE_NOCHANGE = 15;
        private const int SUMMONS_REPCODE_CHANGEOFFENDER = 19;
        private const int SUMMONS_REPCODE_REISSUE = 30;
        private const int SUMMONS_REPCODE_ADDRESS = 40;

        private const string PAGE_URL_DECISION = "Representation.aspx?type=Decision";
        private const string PAGE_URL_REGISTER = "Representation.aspx?type=Register";
        private const string PAGE_URL_OFFENDER = "Representation.aspx?type=ChangeOfOffender";

        protected int _csCodeSummonsGenerated = 610;
        //protected int _csCodeSummonsReturned = 650;

        private const string DATE_FORMAT = "yyyy-MM-dd";

        private Int32 autIntNo = 0;
        private Int32 repIntNo = 0;
        private Int32 chgIntNo = 0;
        private int noOfDaysBeforeCourt = 0;

        private string defaultNationality = string.Empty;
        private bool allowAllInOne = false;
        private int maxStatusPaymentAllowed = 900;
        private bool bCheckID = false;
        private bool bCheckForenames = false;
        private int noOfDaysForPayment = 0;
        private string allowedRepForS35 = "N";
        // 2011-09-29 jerry add
        private bool isPrintBatchRepLetter = false;
        private string repPrintFileName = string.Empty;
        private int noOfDaysGracePeriod = 0;
        private string autCode = string.Empty;

        //jerry 2012-03-14 add
        int noOfDaysTo1stNotice;
        int noOfDaysToSummons;
        int noOfDaysToSummonsNoAOG;

        string dataWashingActived = null;
        string useRepAddress = null;
        int repAddrExpiryPeriod;

        //enum NoticeFilmType
        //{
        //    //S341,
        //    //S54,
        //    //S56,
        //    H,
        //    M,
        //    Others
        //}

        NoticeFilmType noticeFilmType = NoticeFilmType.Others;   //added by Oscar 20101014
        NoticeFilmTypes nft = new NoticeFilmTypes();

        // 2012.09.12 Nick add for report punch statistics
        PunchStatistics punchStat;

        //Modified by Henry 2012-3-21 to mulity language.
        string emptyMsg;
        string addMsg;

        /*
         * List out ViewState by Oscar 20101014
         * 
         * ViewState["allowAllInOne"]
         * ViewState["AutIntNo"]
         * ViewState["ChargeRowVersion"]
         * ViewState["ChargeStatus"]
         * ViewState["ChgIntNo"]
         * ViewState["defaultNationality"]
         * ViewState["DrvIntNo"]
         * ViewState["IsNoAOG"]
         * ViewState["IsSummons"]
         * ViewState["maxStatusPaymentAllowed"]
         * ViewState["noOfDaysBeforeCourt"]
         * ViewState["noOfDaysForPayment"]
         * ViewState["NotIntNo"]
         * ViewState["RCCode"]
         * ViewState["RepAction"]
         * ViewState["RepIntNo"]
         * ViewState["SumCaseNo"]
         * ViewState["SumIntNo"]
         * ViewState["SumServedStatus"]
         * ViewState["TicketNumberSearch_AutIntNo"]
         * ViewState["ChargeSource"]
         * 
         * 
         */

        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);
            if (ViewState["PageCache"] == null)
            {
                cache = new PageCache();
                ViewState["PageCache"] = cache;
            }
            else
                cache = (PageCache)ViewState["PageCache"];
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            emptyMsg = (string)this.GetLocalResourceObject("emptyMsg");
            addMsg = (string)this.GetLocalResourceObject("addMsg");
            // The database connection string
            this.connectionString = Application["constr"].ToString();

            punchStat = new PunchStatistics(connectionString);
            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            GetAutIntNo();    //added by Oscar 20101019

            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.SetAuthRuleDefaults();

            //jerry 2012-03-14 add
            this.SetDateRuleDefault();

            //if (this.ViewState["IsSummons"] != null)
            //    //this.isSummons = (bool)this.ViewState["IsSummons"];
            //    this.isSummons = Convert.ToBoolean(ViewState["IsSummons"]);   // changed by Oscar 20101021
            if (cache.IsSummons.HasValue)
                this.isSummons = cache.IsSummons.Value;

            if (this.ViewState["noOfDaysBeforeCourt"] != null)
                noOfDaysBeforeCourt = int.Parse(ViewState["noOfDaysBeforeCourt"].ToString());

            if (this.ViewState["noOfDaysForPayment"] != null)
                noOfDaysForPayment = int.Parse(ViewState["noOfDaysForPayment"].ToString());

            if (Request.QueryString["type"] != null)
            {
                if (Request.QueryString["type"] == "Register")
                {
                    repAction = "R";
                    //Modified by Henry 2012-3-21 to mulity language.
                    this.lblPageName.Text = (string)this.GetLocalResourceObject("lblPageName.Text3");
                    thisPageURL = PAGE_URL_REGISTER;
                }
                if (Request.QueryString["type"] == "Decision")
                {
                    thisPageURL = PAGE_URL_DECISION;
                    if (allowAllInOne)
                        btnAdd.Visible = true;
                    else
                        btnAdd.Visible = false;

                    //Modified by Henry 2012-3-21 to mulity language.
                    this.lblPageName.Text = (string)this.GetLocalResourceObject("lblPageName.Text1");
                    repAction = "D";
                    if (Request.QueryString["ticketno"] != null)
                    {
                        // Set for one step reps !!!!
                        this.txtSearch.Text = Request.QueryString["ticketno"];
                        GetRepresentationDetails();
                        pnlCharges.Visible = false;
                        pnlRepresentation.Visible = true;
                        pnlNoResults.Visible = false;
                        pnlRetrieve.Visible = false;
                        pnlDecide.Visible = true;


                    }
                }
                if (Request.QueryString["type"] == "ChangeOfOffender")
                {
                    thisPageURL = PAGE_URL_OFFENDER;
                    repAction = "O";
                    //Modified by Henry 2012-3-21 to mulity language.
                    this.lblPageName.Text = (string)this.GetLocalResourceObject("lblPageName.Text2");
                }

                this.ViewState.Add("RepAction", repAction);
            }
            else
            {
                repAction = "R";
                this.ViewState.Add("RepAction", repAction);
                //Modified by Henry 2012-3-21 to mulity language.
                this.lblPageName.Text = (string)this.GetLocalResourceObject("lblPageName.Text3");
            }

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            // Process the page if it is new
            if (!Page.IsPostBack)
            {
                this.GoBackToBeginning(true);

                this.PopulateVehicleMakes();
                this.PopulateVehicleTypes();

                if (ddlSelectLA.SelectedIndex > -1)
                    this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

                this.PopulateAuthorities(autIntNo);

                this.TicketNumberSearch1.AutIntNo = this.autIntNo;

                this.noOfDaysBeforeCourt = this.CheckNoOfDaysBeforeCourt();

                ViewState.Add("noOfDaysBeforeCourt", noOfDaysBeforeCourt);

                //this.SetupControls(); 2013-07-11 remove by Henry
                PopulateVehicleColours();
                this.noOfDaysForPayment = this.GetNoOfDaysForPayment(autIntNo);

                //2011-10-24 jerry add
                //string ShowBlockUI = Request.QueryString["ShowBlockUI"];
                //if (Session["ShowBlockUI"] != null && (bool)Session["ShowBlockUI"] && ShowBlockUI == "YES")
                // Oscar 20121015 changed
                var rlfpQuery = GetRepLetterForPrint();
                if (rlfpQuery.ShowBlockUI)
                {
                    pnlRepresentation.Visible = true;
                    ClientScript.RegisterStartupScript(this.GetType(), "Key", @"<script>$.blockUI({ message: $('#faceboxCopy')});</script>");

                    rlfpQuery.ShowBlockUI = false;
                }
            }
            //2011-10-13 jerry add
            else
            {
                string letterType = Request.QueryString["LetterType"];
                string repIntNo = Request.QueryString["RepIntNo"];
                string ticketNo = Request.QueryString["TicketNo"];
                string letterTo = Request.QueryString["LetterTo"];

                #region Oscar 20121016 disabled
                //if (hidIsSelectedYes.Value == "Yes")
                //{
                //    pnlChangeRegNoDecision.Visible = false;
                //    pnlRepresentation.Visible = false;
                //    lblPageName.Visible = false;
                //    PrintBatchSelectYes(letterType, repIntNo, ticketNo, letterTo);
                //}
                //else if (hidIsSelectedYes.Value == "No")
                //{
                //    pnlChangeRegNoDecision.Visible = false;
                //    pnlRepresentation.Visible = false;
                //    lblPageName.Visible = false; 
                //    PrintBatchSelectNo(letterType, repIntNo, ticketNo, letterTo);
                //}
                #endregion

                // Oscar 20121016 changed
                if (Session["RepLetterForPrint"] != null && !hidIsSelectedYes.Value.Equals(""))
                {
                    pnlChangeRegNoDecision.Visible = false;
                    pnlRepresentation.Visible = false;
                    lblPageName.Visible = false;

                    var selectedYes = hidIsSelectedYes.Value.Equals("Yes", StringComparison.OrdinalIgnoreCase);
                    var rlfpQuery = GetRepLetterForPrint();

                    if (!rlfpQuery.IsIBMPrinter)
                    {
                        if (selectedYes)
                            PrintBatchSelectYes(letterType, repIntNo, ticketNo, letterTo);
                        else
                            PrintBatchSelectNo(letterType, repIntNo, ticketNo, letterTo);
                    }
                    else
                    {
                        PopupTheRepresentationLetterViewer(letterType, repIntNo, ticketNo, letterTo);
                        PrintRepLetter(rlfpQuery.AutIntNo, rlfpQuery.PrintFileName, selectedYes);
                    }
                }

                Session.Remove("RepLetterForPrint");
                hidIsSelectedYes.Value = "";
            }
        }

        private bool IsIBMPrinter()
        {
            AuthorityRulesDetails arDetailsLoad = new AuthorityRulesDetails();
            arDetailsLoad.AutIntNo = autIntNo;
            arDetailsLoad.ARCode = "6209";
            arDetailsLoad.LastUser = this.loginUser;
            DefaultAuthRules arLoad = new DefaultAuthRules(arDetailsLoad, this.connectionString);
            return arLoad.SetDefaultAuthRule().Value.Equals("Y");
        }

        protected void PopulateVehicleColours()
        {
            VehicleColourDB vc = new VehicleColourDB(connectionString);

            SqlDataReader reader = vc.GetVehicleColours();
            Dictionary<int, string> lookups =
                VehicleColourLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int vCIntNo = int.Parse(reader["VCIntNo"].ToString());
                string vCCode = reader["VCCode"].ToString();
                if (lookups.ContainsKey(vCIntNo))
                {
                    ddlNotVehicleColour.Items.Add(new ListItem(lookups[vCIntNo], vCCode.ToString()));
                    ddlNewVehicleColourDescr.Items.Add(new ListItem(lookups[vCIntNo], vCCode.ToString()));
                }
            }
            this.ddlNotVehicleColour.DataSource = vc.GetVehicleColours();
            //this.ddlNotVehicleColour.DataValueField = "VCCode";
            //this.ddlNotVehicleColour.DataTextField = "VCDescr";
            //this.ddlNotVehicleColour.DataBind();

            //this.ddlNewVehicleColourDescr.DataSource = vc.GetVehicleColours();
            //this.ddlNewVehicleColourDescr.DataValueField = "VCCode";
            //this.ddlNewVehicleColourDescr.DataTextField = "VCDescr";
            //this.ddlNewVehicleColourDescr.DataBind();
        }


        private bool SetupControls()     // 2013-07-10 add bool return by Henry for withdraw
        {
            ListItem item;

            // Representation Code Radio Buttons
            this.rblCodes.Items.Clear();
            int withDrawType = CheckWithdrawType();
            bool notWithdrawCharge = withDrawType <= 1;
            if (this.isSummons)
            {
                //if all charges have been withdrawn, then we basically are not going to let them decide anything else
                if (withDrawType == 0)
                {
                    this.chkChangeOffender.Visible = false;
                }
                else
                {
                    //Jerry 2012-05-25 change
                    //item = new ListItem("None", SUMMONS_REPCODE_NONE.ToString());
                    item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem1.Text"), SUMMONS_REPCODE_NONE.ToString());
                    this.rblCodes.Items.Add(item);

                    //item = new ListItem("Reduce Fine", SUMMONS_REPCODE_REDUCTION.ToString());
                    item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem5.Text"), SUMMONS_REPCODE_REDUCTION.ToString());
                    this.rblCodes.Items.Add(item);

                    //item = new ListItem("No Change", SUMMONS_REPCODE_NOCHANGE.ToString());
                    item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem6.Text"), SUMMONS_REPCODE_NOCHANGE.ToString());
                    this.rblCodes.Items.Add(item);
                    //item = new ListItem(CheckWithdrawType() ? "Withdraw Summons" : "Withdraw Charge", SUMMONS_REPCODE_WITHDRAW.ToString());
                    //item = new ListItem(withDrawType <= 1 ? "Withdraw Summons" : "Withdraw Charge", SUMMONS_REPCODE_WITHDRAW.ToString());
                    item = new ListItem(notWithdrawCharge ? (string)this.GetLocalResourceObject("rblCodes.ListItem7.Text") : (string)this.GetLocalResourceObject("rblCodes.ListItem8.Text"), SUMMONS_REPCODE_WITHDRAW.ToString());
                    this.rblCodes.Items.Add(item);
                    //item = new ListItem("Withdraw Summons for Re-Issue", SUMMONS_REPCODE_REISSUE.ToString());
                    //this.rblCodes.Items.Add(item);
                    //item = new ListItem("Change of Address", SUMMONS_REPCODE_ADDRESS.ToString());
                    //this.rblCodes.Items.Add(item);
                    // Changed by Oscar 20101027
                    if (this.noticeFilmType != NoticeFilmType.M)
                    {
                        //item = new ListItem("Withdraw Summons for Re-Issue", SUMMONS_REPCODE_REISSUE.ToString());
                        item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem9.Text"), SUMMONS_REPCODE_REISSUE.ToString());
                        this.rblCodes.Items.Add(item);
                        //item = new ListItem("Change of Address", SUMMONS_REPCODE_ADDRESS.ToString());
                        item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem10.Text"), SUMMONS_REPCODE_ADDRESS.ToString());
                        this.rblCodes.Items.Add(item);
                    }
                    else
                        this.chkChangeOffender.Visible = false;
                }
            }
            else
            {
                //Jerry 2012-05-25 change
                //item = new ListItem("None", REPCODE_NONE.ToString());
                item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem1.Text"), REPCODE_NONE.ToString());
                this.rblCodes.Items.Add(item);
                //item = new ListItem("Nolle Proseeque", REPCODE_WITHDRAW.ToString());
                item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem2.Text"), REPCODE_WITHDRAW.ToString());
                this.rblCodes.Items.Add(item);

                //item = new ListItem("Fine Changed", REPCODE_REDUCTION.ToString());
                item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem3.Text"), REPCODE_REDUCTION.ToString());
                this.rblCodes.Items.Add(item);

                //item = new ListItem("No Action", REPCODE_NOCHANGE.ToString());
                item = new ListItem((string)this.GetLocalResourceObject("rblCodes.ListItem4.Text"), REPCODE_NOCHANGE.ToString());
                this.rblCodes.Items.Add(item);

                //item = new ListItem("", REPCODE_CHANGEOFFENDER.ToString());
                //this.rblCodes.Items.Add(item);
                //item = new ListItem("", REPCODE_CANCELLED.ToString());
                //this.rblCodes.Items.Add(item);
            }

            // Change of Offender radio buttons
            //this.rdlLetter.Visible = !this.isSummons;
            // Oscar 20101105 - changed, S56 is not allowed to change offender
            if (GetNoticeFilmType() != NoticeFilmType.M)
            {
                this.rdlLetter.Visible = !this.isSummons;
                //this.rdlLetter.Visible = true;
            }

            return notWithdrawCharge;
        }

        protected void PopulateVehicleMakes()
        {
            VehicleMakeDB make = new VehicleMakeDB(connectionString);

            SqlDataReader reader = make.GetVehicleMakeList("");

            Dictionary<int, string> lookups =
                VehicleMakeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int vMIntNo = (int)reader["VMIntNo"];
                if (lookups.ContainsKey(vMIntNo))
                {
                    ddlPrevVehMake.Items.Add(new ListItem(lookups[vMIntNo], vMIntNo.ToString()));
                    ddlNewVehMake.Items.Add(new ListItem(lookups[vMIntNo], vMIntNo.ToString()));
                }
            }
            //ddlPrevVehMake.DataSource = reader;
            //ddlPrevVehMake.DataValueField = "VMIntNo";
            //ddlPrevVehMake.DataTextField = "VMDescr";
            //ddlPrevVehMake.DataBind();

            //reader = make.GetVehicleMakeList("");
            //ddlNewVehMake.DataSource = reader;
            //ddlNewVehMake.DataValueField = "VMIntNo";
            //ddlNewVehMake.DataTextField = "VMDescr";
            //ddlNewVehMake.DataBind();
            ddlNewVehMake.Items.Insert(0, (string)GetLocalResourceObject("selectVehMake.Item.Text"));

            reader.Close();
        }

        protected void PopulateVehicleTypes()
        {
            VehicleTypeDB type = new VehicleTypeDB(connectionString);

            SqlDataReader reader = type.GetVehicleTypeList(0, "");

            Dictionary<int, string> lookups =
                VehicleTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int vTIntNo = (int)reader["VTIntNo"];
                if (lookups.ContainsKey(vTIntNo))
                {
                    ddlPrevVehType.Items.Add(new ListItem(lookups[vTIntNo], vTIntNo.ToString()));
                    ddlNewVehType.Items.Add(new ListItem(lookups[vTIntNo], vTIntNo.ToString()));
                }
            }
            //ddlPrevVehType.DataSource = reader;
            //ddlPrevVehType.DataValueField = "VTIntNo";
            //ddlPrevVehType.DataTextField = "VTDescr";
            //ddlPrevVehType.DataBind();

            //reader = type.GetVehicleTypeList(0, "");
            //ddlNewVehType.DataSource = reader;
            //ddlNewVehType.DataValueField = "VTIntNo";
            //ddlNewVehType.DataTextField = "VTDescr";
            //ddlNewVehType.DataBind();
            ddlNewVehType.Items.Insert(0, (string)GetLocalResourceObject("selectVehType.Item.Text"));

            reader.Close();
        }

        private void SetAuthRuleDefaults()
        {
            //AuthorityRulesDB ardb = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails ardet = null;

            AuthorityRulesDetails rule = new AuthorityRulesDetails();

            // get authrule for this Authority to check if we have one step reps enabled
            if (ViewState["allowAllInOne"] == null)
            {

                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = this.autIntNo;
                ard.ARCode = "4595";
                ard.LastUser = this.loginUser;

                DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
                KeyValuePair<int, string> allInOneRule = authRule.SetDefaultAuthRule();

                allowAllInOne = allInOneRule.Value == "Y" ? true : false;

                ViewState.Add("allowAllInOne", allowAllInOne);
            }
            else
            {
                allowAllInOne = (bool)ViewState["allowAllInOne"];
            }

            // get auth rule for this Authority to check if we have one step reps enabled
            if (ViewState["defaultNationality"] == null)
            {
                //ardet = ardb.GetAuthorityRulesDetailsByCode(this.autIntNo, "4560", "Rule to set citizenship default to RSA", 0, "RSA", "NA = Foreign; RSA (default)", loginUser);
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4560";
                rule.LastUser = this.loginUser;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> nationality = authRule.SetDefaultAuthRule();

                defaultNationality = nationality.Value;

                ViewState.Add("defaultNationality", defaultNationality);
            }
            else
            {
                defaultNationality = ViewState["defaultNationality"].ToString();
            }

            // get auth rule for the maximum status at which we allow payments or representations
            if (ViewState["maxStatusPaymentAllowed"] == null)
            {
                //ardet = ardb.GetAuthorityRulesDetailsByCode(this.autIntNo, "4570", "Rule to set maximum charge status at which payment/representations are allowed", 900, "", "900 (Default); See ChargeStatus table for allowable values (status must be 255 or greater)", loginUser);
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4570";
                rule.LastUser = this.loginUser;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> maxStatus = authRule.SetDefaultAuthRule();

                maxStatusPaymentAllowed = maxStatus.Key;

                ViewState.Add("maxStatusPaymentAllowed", maxStatusPaymentAllowed);
            }
            else
            {
                maxStatusPaymentAllowed = (int)ViewState["maxStatusPaymentAllowed"];
            }

            //ardet = ardb.GetAuthorityRulesDetailsByCode(this.autIntNo, "4531", "Rule to make the ID number optional", 0, "N", "Y = Yes; N= No(default) mandatory", loginUser);
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule = new AuthorityRulesDetails();
            rule.ARCode = "4531";
            rule.LastUser = this.loginUser;
            rule.AutIntNo = this.autIntNo;
            DefaultAuthRules ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> idRule = ar.SetDefaultAuthRule();

            bCheckID = idRule.Value == "N" ? true : false;

            // ardet = ardb.GetAuthorityRulesDetailsByCode(this.autIntNo, "4532", "Rule to make the Forenames optional", 0, "N", "Y = Yes; N= No(default) mandatory", loginUser);
            rule = new AuthorityRulesDetails();
            rule.ARCode = "4532";
            rule.LastUser = this.loginUser;
            rule.AutIntNo = this.autIntNo;

            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> foreNamesRule = ar.SetDefaultAuthRule();

            bCheckForenames = foreNamesRule.Value == "N" ? true : false;

            // 2011-09-29 jerry add
            rule = new AuthorityRulesDetails();
            rule.ARCode = "4120";
            rule.LastUser = this.loginUser;
            rule.AutIntNo = this.autIntNo;
            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> printBatchRepLetterRule = ar.SetDefaultAuthRule();
            isPrintBatchRepLetter = printBatchRepLetterRule.Value == "Y" ? true : false;

            // jerry 2011-11-30 add
            rule = new AuthorityRulesDetails();
            rule.ARCode = "4100";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.loginUser;
            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value4100 = ar.SetDefaultAuthRule();
            noOfDaysGracePeriod = value4100.Key;

            // 20120326 Nick add for data washing
            rule = new AuthorityRulesDetails();
            rule.ARCode = "9060";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.loginUser;
            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> dwActiveRule = ar.SetDefaultAuthRule();
            dataWashingActived = dwActiveRule.Value.ToUpper().Trim();

            rule = new AuthorityRulesDetails();
            rule.ARCode = "9120";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.loginUser;
            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> dwUseRepAddrRule = ar.SetDefaultAuthRule();
            useRepAddress = dwUseRepAddrRule.Value.ToUpper().Trim();

            rule = new AuthorityRulesDetails();
            rule.ARCode = "6610";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.loginUser;
            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> repAllowedForSection35 = ar.SetDefaultAuthRule();
            allowedRepForS35 = repAllowedForSection35.Value.ToUpper().Trim();

            rule = new AuthorityRulesDetails();
            rule.ARCode = "9130";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.loginUser;
            ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> dwRepAddrExpiryPeriodRule = ar.SetDefaultAuthRule();
            repAddrExpiryPeriod = dwRepAddrExpiryPeriodRule.Key;

            // Oscar 20121213 add new auth rule 4536
            if (!cache.AlwaysAllow.HasValue)
            {
                rule = new AuthorityRulesDetails
                {
                    ARCode = "4536",
                    AutIntNo = this.autIntNo,
                    LastUser = this.loginUser,
                };
                cache.AlwaysAllow = new DefaultAuthRules(rule, this.connectionString)
                    .SetDefaultAuthRule()
                    .Value.Equals("Y", StringComparison.OrdinalIgnoreCase);
            }
        }

        private void SetDateRuleDefault()
        {
            DateRulesDetails dateRule = new DateRulesDetails();
            dateRule.AutIntNo = autIntNo;
            dateRule.DtRStartDate = "NotOffenceDate";
            dateRule.DtREndDate = "NotPosted1stNoticeDate";
            dateRule.LastUser = this.loginUser;
            DefaultDateRules rule = new DefaultDateRules(dateRule, connectionString);
            noOfDaysTo1stNotice = rule.SetDefaultDateRule();

            dateRule = new DateRulesDetails();
            dateRule.AutIntNo = autIntNo;
            //dateRule.DtRStartDate = "NotOffenceDate";   --dls 2012-04-04 - this was the wrong rule!
            dateRule.DtRStartDate = "NotPosted1stNoticeDate";
            dateRule.DtREndDate = "NotIssueSummonsDate";
            dateRule.LastUser = this.loginUser;
            rule = new DefaultDateRules(dateRule, connectionString);
            noOfDaysToSummons = rule.SetDefaultDateRule();

            //Get number of days from NoAOG posted to summons generate date 
            dateRule = new DateRulesDetails();
            dateRule.AutIntNo = autIntNo;
            dateRule.DtRStartDate = "NotNoAOGPosted1stNoticeDate";
            dateRule.DtREndDate = "SumIssueDate";
            dateRule.LastUser = this.loginUser;
            rule = new DefaultDateRules(dateRule, connectionString);
            noOfDaysToSummonsNoAOG = rule.SetDefaultDateRule();


        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

        }

        protected void CalculateAge(object sender, EventArgs e)
        {
            //if (bCheckID)
            //{
            if (this.txtIDNumber.Text.Trim().Length == 13)
            {
                // work out the age
                try
                {
                    String sDOB = this.txtIDNumber.Text.Trim().Substring(0, 6);
                    DateTime dtDOB = new DateTime(Convert.ToInt32("19" + sDOB.Substring(0, 2)), Convert.ToInt32(sDOB.Substring(2, 2)), Convert.ToInt32(sDOB.Substring(4, 2)));
                    System.TimeSpan TS = new System.TimeSpan(DateTime.Now.Ticks - dtDOB.Ticks);
                    long lAge = TS.Days / 365;
                    txtAge.Text = lAge.ToString();
                    lblError.Text = string.Empty;
                }
                catch
                {
                    this.txtAge.Text = "0";
                    lblError.Text = (string)this.GetLocalResourceObject("lblError.Text1");
                    lblError.Visible = true;
                }
            }
            else if (!bCheckID && this.txtIDNumber.Text.Trim().Length == 0)
            {
                this.txtAge.Text = "0";
            }
            else
            {
                this.txtAge.Text = "0";
                lblError.Text = (string)this.GetLocalResourceObject("lblError.Text2");
                lblError.Visible = true;
            }
            //}
        }

        // Oscar 20101022 - Rewrite the method
        private void GetRepresentationDetails()
        {
            BindDDLRepOfficial();

            this.lblRepOfficial.Text = string.Empty;
            this.lblRepOfficial.Visible = false;

            this.txtSurname.ReadOnly = false;
            this.txtInitials.ReadOnly = false;
            this.txtForenames.ReadOnly = false;

            // set controls
            this.lblError.Visible = true;
            this.pnlRepresentation.Visible = false;
            this.pnlNoResults.Visible = false;
            this.pnlCharges.Visible = false;
            this.pnlChangeRegNoDecision.Enabled = true;
            this.grdRepresentation.Visible = false;
            this.lblEmptyData.Visible = false;

            this.repIntNo = ViewState["RepIntNo"] == null ? 0 : Convert.ToInt32(ViewState["RepIntNo"]);
            this.repAction = ViewState["RepAction"] == null ? "R" : ViewState["RepAction"].ToString();
            this.chgIntNo = ViewState["ChgIntNo"] == null ? 0 : Convert.ToInt32(ViewState["ChgIntNo"]);

            // Oscar 20120813 added for authorise 2nd rep
            this.btnDecide.Enabled = true;
            this.btnReverse.Enabled = true;

            if (this.isSummons)
            {
                this.hdnType_rep.Value = "0";
                this.hdnType_srep.Value = this.repIntNo.ToString();
            }
            else
            {
                this.hdnType_rep.Value = this.repIntNo.ToString();
                this.hdnType_srep.Value = "0";
            }

            object recommendDate = null;
            DataView dvSource = GetRepresentationSource(GetSearchRepresentationsSource(), this.chgIntNo, this.repIntNo);

            if (dvSource.Count > 0)
            {
                DataRowView row = dvSource[0];

                recommendDate = row["RepRecommendDate"];

                ViewState["ChgIntNo"] = row["ChgIntNo"];
                ViewState["SumIntNo"] = row["SumIntNo"];
                int status = Convert.ToInt32(row["ChargeStatus"]);
                this.isS35NoAOG = Convert.ToBoolean(row["IsSection35"]);
                this.ViewState.Add("IsSection35", this.isS35NoAOG);

                if (status < maxStatusPaymentAllowed || status == CODE_CASE_CANCELLED || status == CODE_SUMMONS_WITHDRAWN_CHARGE)
                {
                    int repCode = REPCODE_NONE;
                    if (!row["RCCode"].Equals(DBNull.Value))
                        repCode = Convert.ToInt32(row["RCCode"]);

                    this.isSummons = Convert.ToBoolean(row["IsSummons"]);
                    //ViewState["IsSummons"] = this.isSummons;
                    cache.IsSummons = this.isSummons;
                    ViewState["ChargeRowVersion"] = row["ChargeRowVersion"];
                    ViewState["NotIntNo"] = row["NotIntNo"];
                    ViewState["RCCode"] = repCode;

                    // fill items' data
                    this.txtTicketNo.Text = Convert.ToString(row["NotTicketNo"]);
                    this.hdnTicketNo.Value = this.txtTicketNo.Text;
                    this.txtRegNo.Text = Convert.ToString(row["OrigRegNo"]);
                    this.txtRepOffenderName.Text = Convert.ToString(row["RepOffenderName"]);
                    this.txtRepOffenderAddress.Text = Convert.ToString(row["RepOffenderAddress"]);
                    this.txtOffenceDescr.Text = Convert.ToString(row["ChgOffenceDescr"]);
                    this.wdcRepDate.Text = row["RepDate"] == DBNull.Value ? default(DateTime).ToString(DATE_FORMAT) : Convert.ToDateTime(row["RepDate"]).ToString(DATE_FORMAT);
                    if (this.isNew)
                    {
                        this.txtRepDetails.Text = string.Empty;
                        this.txtReversalReason.Text = string.Empty;
                        this.txtRepOfficial.Text = string.Empty;
                    }
                    else
                    {
                        this.txtRepDetails.Text = Convert.ToString(row["RepDetails"]);
                        this.txtReversalReason.Text = Convert.ToString(row["RepReverseReason"]);
                        this.txtRepOfficial.Text = Convert.ToString(row["RepOfficial"]);
                    }

                    // Oscar 2013-05-17 added
                    //this.ddlRepOfficial.SelectedValue = this.txtRepOfficial.Text;
                    FindRepOfficial(repCode, this.txtRepOfficial.Text);

                    this.txtChargeStatus.Text = Convert.ToString(row["RepCSDescr"]);

                    //Rachel 2014-08-07 updated-5337
                    //this.txtOriginalAmount.Text = Convert.ToString(row["ChgFineAmount"]);

                    //if (Convert.ToString(row["ChgRevFineAmount"]).Equals("0") && Convert.ToString(row["ChgNoAOG"]).Equals("Y"))
                    //    this.txtRevAmount.Text = "999999";
                    //else
                    //    this.txtRevAmount.Text = Convert.ToString(row["ChgRevFineAmount"]);

                    this.txtOriginalAmount.Text = Convert.ToString(row["ChgFineAmount"],CultureInfo.InvariantCulture);

                    if (Convert.ToString(row["ChgRevFineAmount"]).Equals("0") && Convert.ToString(row["ChgNoAOG"]).Equals("Y"))
                        this.txtRevAmount.Text = "999999";
                    else
                        this.txtRevAmount.Text = Convert.ToString(row["ChgRevFineAmount"], CultureInfo.InvariantCulture);

                    //end by Rachel 2014-08-07 updated-5337

                    this.chkChangeOffender.Enabled = true;   //444
                    this.chkChangeRegistration.Enabled = true;
                    this.txtRecommend.Visible = false;
                    this.lblRecommend.Visible = false;

                    // Oscar 20101014 - added checking for S56
                    this.noticeFilmType = nft.GetNoticeFilmType(row["NotFilmType"]);
                    ViewState["NoticeFilmType"] = this.noticeFilmType;

                    //2013-07-10 add by Henry for popup message of withdraw
                    bool notWithdrawCharge = this.SetupControls();
                    string withdrawKey = this.isSummons ?
                        notWithdrawCharge ? "withdrawSumMsg" : "withdrawSumChgMsg" :
                        notWithdrawCharge ? "withdrawNotMsg" : "withdrawNotChgMsg";
                    string witMsg = (string)GetLocalResourceObject(withdrawKey);
                    WithdrawMsg.Value = notWithdrawCharge ? string.Format(witMsg, txtTicketNo.Text) :
                            string.Format(witMsg, row["ChgOffenceCode"], txtTicketNo.Text);

                    //Jerry 2012-05-25 change
                    //if (btnUpdate.Text == " Add ")
                    if (btnUpdate.Text == ((string)this.GetLocalResourceObject("btnAdd.Text")).Trim())
                    {
                        //this.txtTicketNo.Text = (string)reader["NotTicketNo"];
                        //this.txtRegNo.Text = (string)reader["NotRegNo"];
                        if (!row["Name"].Equals(DBNull.Value))
                            this.txtRepOffenderName.Text = Convert.ToString(row["Name"]).Trim();
                        StringBuilder sb = new StringBuilder();
                        if (!row["DrvPoAdd1"].Equals(DBNull.Value))
                            sb.Append(row["DrvPoAdd1"] + "\n");
                        if (!row["DrvPoAdd2"].Equals(DBNull.Value))
                            sb.Append(row["DrvPoAdd2"] + "\n");
                        if (!row["DrvPoAdd3"].Equals(DBNull.Value))
                            sb.Append(row["DrvPoAdd3"] + "\n");
                        if (!row["DrvPoAdd4"].Equals(DBNull.Value))
                            sb.Append(row["DrvPoAdd4"] + "\n");
                        if (!row["DrvPoAdd5"].Equals(DBNull.Value))
                            sb.Append(row["DrvPoAdd5"] + "\n");
                        if (!row["DrvPoCode"].Equals(DBNull.Value))
                            sb.Append(row["DrvPoCode"]);
                        this.txtRepOffenderAddress.Text = sb.ToString().Trim();
                        //this.txtOffenceDescr.Text = (string)reader["ChgOffenceDescr"];

                        this.wdcRepDate.Text = DateTime.Today.ToString(DATE_FORMAT);
                        //if (this.isNew)
                        //    this.txtRepDetails.Text = string.Empty;
                        //else
                        //    this.txtRepDetails.Text = reader["RepDetails"].ToString();
                        //this.txtChargeStatus.Text = (string)reader["RepCSDescr"];
                        //this.txtOriginalAmount.Text = reader["ChgFineAmount"].ToString();

                        //dls 071218 - they want to leave the amount as 999999 for a NoAOG
                        
                        //update by rachel 20140808 for 5337
                        //if (row["ChgRevFineAmount"].ToString().Equals("0"))
                        //    this.txtRevAmount.Text = "999999";
                        //else
                        //    this.txtRevAmount.Text = row["ChgRevFineAmount"].ToString();

                        if (row["ChgRevFineAmount"].ToString().Equals("0"))
                            this.txtRevAmount.Text = "999999";
                        else
                            this.txtRevAmount.Text =Convert.ToString(row["ChgRevFineAmount"],CultureInfo.InvariantCulture);

                        //end update by rachel 20140808 for 5337

                        if (this.isNew)
                            this.txtRecommend.Text = string.Empty;
                        else
                            this.txtRecommend.Text = row["RepRecommend"].ToString();
                        this.txtOffenceDescr.Enabled = true;
                        this.txtOffenceDescr.ReadOnly = false;
                        this.btnReverse.Visible = false;
                        this.txtReversalReason.Visible = false;
                        this.txtRecommend.Visible = false;
                        this.lblReversalReason.Visible = false;
                        this.lblRecommend.Visible = false;
                    }

                    //Jerry 2012-05-25 change
                    //if (repAction == "R" && btnUpdate.Text == "Update")
                    if (repAction == "R" && btnUpdate.Text == ((string)this.GetLocalResourceObject("btnUpdate.Text1")).Trim())
                    {
                        //dls 080611 - no longer allowed to reverse a summons rep - they need to process a new one! or create via a single summons
                        this.btnDecide.Enabled = true;   //?

                        //if (reader["ChargeStatus"].Equals(410) || reader["ChargeStatus"].Equals(420) || this.isSummons)
                        if (row["ChargeStatus"].Equals(410) || row["ChargeStatus"].Equals(420))
                        {
                            this.btnReverse.Visible = true;
                            this.txtReversalReason.Visible = true;
                            this.lblReversalReason.Visible = true;
                        }
                        else
                        {
                            this.btnReverse.Visible = false;
                            this.txtReversalReason.Visible = false;
                            this.txtRecommend.Visible = false;
                            this.lblReversalReason.Visible = false;
                            this.lblRecommend.Visible = false;

                            if (this.isSummons)   //?
                            {
                                this.btnDecide.Enabled = false;
                            }
                        }
                    }

                    this.pnlRepresentation.Visible = true;
                    this.lblError.Visible = false;

                    if (repAction == "D" || repAction == "O")
                    {
                        // We want to decide a representation
                        this.pnlDecide.Visible = true;
                        this.txtRecommend.Visible = true;
                        this.lblRecommend.Visible = true;

                        //this.chkChangeOffender.Visible = true; 
                        // Changed by Oscar 20101027
                        if (this.noticeFilmType != NoticeFilmType.M)
                            this.chkChangeOffender.Visible = true;

                        this.pnlChangeOfOffender.Visible = false;

                        //if (row["ChargeStatus"].Equals(450))
                        //    btnDecide.Visible = false;
                        //else
                        //    btnDecide.Visible = true;
                        // Oscar 20101022 - status 450 is not in use, so these codes should be removed.                        
                        btnDecide.Visible = true;

                        btnUpdate.Visible = false;

                        if (repAction == "O")
                        {
                            chkChangeOffender.Checked = true;
                            pnlChangeOfOffender.Visible = true;
                            pnlChangeOfOffender.Enabled = true;
                            btnDecide.Visible = true;
                            btnUpdate.Visible = false;
                        }

                        if (repCode == REPCODE_CHANGEOFFENDER || repCode == SUMMONS_REPCODE_CHANGEOFFENDER)
                        {
                            chkChangeOffender.Checked = true;
                            pnlChangeOfOffender.Visible = true;
                            pnlChangeOfOffender.Enabled = true;
                            //repCode = this.isSummons ? SUMMONS_REPCODE_NONE : REPCODE_NONE;

                            if (repCode == REPCODE_CHANGEOFFENDER)
                            {
                                repCode = REPCODE_NONE;
                            }
                        }

                        else if (repCode == REPCODE_CANCELLED)
                        {
                            chkChangeOffender.Checked = false;
                            pnlChangeOfOffender.Enabled = false;
                            repCode = this.isSummons ? SUMMONS_REPCODE_NONE : REPCODE_NONE;
                            chkChangeRegistration.Checked = false;
                            pnlChangeRegNoDecision.Enabled = false;
                            pnlRegistration.Enabled = false;
                        }

                        if (!this.isNew)     // Oscar - added "if condition" for IsNew
                        {
                            if (this.isSummons && (repCode == REPCODE_NONE || repCode == SUMMONS_REPCODE_NONE || repCode == SUMMONS_REPCODE_CHANGEOFFENDER))
                            {
                                this.rblCodes.SelectedValue = SUMMONS_REPCODE_NONE.ToString();
                            }
                            else if (this.rblCodes.Items.FindByValue(repCode.ToString()) != null)   // Oscar 20120401 add this check, if repCode's value not in the rblCodes's list, will cause an error.
                                this.rblCodes.SelectedValue = repCode.ToString();
                        }
                        else
                            if (this.rblCodes.Items.Count > 0)
                                this.rblCodes.SelectedIndex = 0;

                        Stalberg.TMS.RepresentationDB subrepList = new Stalberg.TMS.RepresentationDB(this.connectionString);

                        if (this.repIntNo == 0)
                        {
                            this.txtReversalReason.Visible = false;
                            this.lblReversalReason.Visible = false;
                            this.btnReverse.Visible = false;

                            decimal originalAmount = decimal.Parse(this.txtOriginalAmount.Text);
                            string offenderName = this.txtRepOffenderName.Text.Trim();
                            string changeOfOffencer = "N";

                            if (repAction == "O")
                                changeOfOffencer = "Y";

                            //insert it first
                            string errMsg = string.Empty;

                            chgIntNo = (int)ViewState["ChgIntNo"];
                            //repIntNo = subrepList.NewUpdateRepresentation(repIntNo, chgIntNo, "", DateTime.Now, offenderName, txtRepOffenderAddress.Text, "", "", loginUser, this.autIntNo, CODE_REP_LOGGED_CHARGE, originalAmount, changeOfOffencer, "A", ref errMsg, this.isSummons);
                            // Oscar changed
                            // jerry 2011-11-30 add push queue code
                            QueueItem item;
                            QueueItemProcessor queProcessor = new QueueItemProcessor();
                            using (TransactionScope scope = new TransactionScope())
                            {
                                repIntNo = subrepList.NewUpdateRepresentation(repIntNo, chgIntNo, "", DateTime.Now, offenderName, txtRepOffenderAddress.Text, "", "", loginUser, this.autIntNo, this.isSummons ? CODE_REP_LOGGED_SUMMONS : CODE_REP_LOGGED_CHARGE, originalAmount, changeOfOffencer, "A", ref errMsg, this.isSummons);

                                // Indicate that the representation has been updated
                                if (repIntNo == -1)
                                {
                                    lblError.Text = (string)this.GetLocalResourceObject("lblError.Text3");
                                    lblError.Visible = true;
                                    return;
                                }
                                else if (repIntNo == -2)
                                {
                                    lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text4"), errMsg);
                                    lblError.Visible = true;
                                    return;
                                }
                                //Jerry 2012-11-01 add
                                else if (repIntNo == -11)
                                {
                                    lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text21"), isSummons ? "Summons" : "Notice");
                                    lblError.Visible = true;
                                    return;
                                }
                                //jerry 2011-12-01 add push queue
                                else if (repIntNo > 0)
                                {
                                    item = new QueueItem();
                                    item.Body = this.repIntNo.ToString();
                                    item.QueueType = ServiceQueueTypeList.RepresentationGracePeriod_Notice;
                                    item.ActDate = DateTime.Now.AddDays(noOfDaysGracePeriod);
                                    item.Group = this.autCode;
                                    queProcessor.Send(item);
                                }
                                scope.Complete();
                            }

                            //***** this is where we add the new repintno to the view state.
                            this.ViewState.Add("RepIntNo", repIntNo);

                            if (this.isSummons)
                            {
                                this.hdnType_rep.Value = "0";
                                this.hdnType_srep.Value = this.repIntNo.ToString();
                            }
                            else
                            {
                                this.hdnType_rep.Value = this.repIntNo.ToString();
                                this.hdnType_srep.Value = "0";
                            }

                            this.hdnTicketNo.Value = this.txtTicketNo.Text;
                        }
                        else
                        {
                            //Jerry 2013-04-15 if the status is withdrawn, the user should not be able to add a new rep
                            //but they must be allowed to select and reverse an existing rep where the rep was to withdraw the notice/summons 
                            //if (this.isSummons && repCode != SUMMONS_REPCODE_REDUCTION && this.noticeFilmType != NoticeFilmType.M)
                            if (this.isSummons && repCode != SUMMONS_REPCODE_REDUCTION && this.noticeFilmType != NoticeFilmType.M
                                && repCode != SUMMONS_REPCODE_WITHDRAW && repCode != REPCODE_WITHDRAW
                                && (status != CODE_REP_LOGGED_SUMMONS || repCode != SUMMONS_REPCODE_NONE))    // Oscar added, We also need to allow the users to reverse a logged summons rep (Status = 660 and RepCode = 10)
                            {
                                this.txtReversalReason.Visible = false;
                                this.lblReversalReason.Visible = false;
                                this.btnReverse.Visible = false;
                            }
                            else
                            {
                                this.txtReversalReason.Visible = true;
                                this.lblReversalReason.Visible = true;
                                this.btnReverse.Visible = true;
                                //dls 2011-01-18 - actually this is not true in all cases
                                //if the rep has been created and then the user exits and comes back later, he must be allowed to decode OR Reverse the rep
                                //removed
                                //this.btnDecide.Visible = false; //Oscar 20101203 - added, they can't been displayed at the same time.
                                if (CheckWithdrawType() == 0)
                                {
                                    this.btnDecide.Visible = false;
                                    this.rblCodes.Items.Clear();
                                }

                            }

                        }

                        SqlDataReader subreader = subrepList.RetrieveRepresentation(this.repIntNo, this.isSummons);
                        if (subreader.Read())
                        {
                            recommendDate = subreader["RepRecommendDate"];

                            decimal amount = Convert.ToDecimal(subreader["ChgFineAmount"]);
                            this.isNoAOG = (amount == 999999);
                            this.ViewState.Add("IsNoAOG", this.isNoAOG);

                            if (subreader["RepOfficial"] != DBNull.Value && !this.isNew)
                                this.txtRepOfficial.Text = subreader["RepOfficial"].ToString();

                            // Oscar 2013-05-17 added
                            //this.ddlRepOfficial.SelectedValue = this.txtRepOfficial.Text;
                            FindRepOfficial(Convert.ToInt32(ViewState["RCCode"]), this.txtRepOfficial.Text);

                            if (subreader["RepDetails"] != DBNull.Value && !this.isNew)
                                this.txtRepDetails.Text = subreader["RepDetails"].ToString();

                            if (subreader["RepRecommend"] != DBNull.Value & !this.isNew)
                                this.txtRecommend.Text = subreader["RepRecommend"].ToString();

                            if (subreader["RepReverseReason"] != DBNull.Value & !this.isNew)
                                this.txtReversalReason.Text = subreader["RepReverseReason"].ToString();

                            if (subreader["ChangeOfOffender"] != DBNull.Value && !this.isNew)
                                chkChangeOffender.Checked = subreader["ChangeOfOffender"].ToString().Equals("Y") ? true : false;

                            if (subreader["LetterTo"] != DBNull.Value)
                            {
                                if (subreader["LetterTo"].ToString().Equals("A"))
                                    this.rdlLetter.SelectedValue = "D";
                                else
                                    this.rdlLetter.SelectedValue = subreader["LetterTo"].ToString();
                            }

                            this.ShowChangeOfOffender();

                            if (subreader["ChangeOfRegNo"] != DBNull.Value)
                                chkChangeRegistration.Checked = subreader["ChangeOfRegNo"].ToString().Equals("Y") ? true : false;

                            if (subreader["ChangeOfRegNoType"] != DBNull.Value)
                                rdlRegDecision.SelectedValue = subreader["ChangeOfRegNoType"].ToString();

                            if (subreader["NewRegNoDetails"] != DBNull.Value)
                                chkNoFurtherDetails.Checked = subreader["NewRegNoDetails"].ToString().Equals("Y") ? false : true;

                            ShowChangeOfRegNo(false);

                            txtPrevRegNo.Text = subreader["OrigRegNo"].ToString();
                            ddlNotVehicleColour.SelectedIndex = ddlNotVehicleColour.Items.IndexOf(ddlNotVehicleColour.Items.FindByValue(subreader["NotVehicleColour"].ToString()));
                            ddlPrevVehMake.SelectedIndex = ddlPrevVehMake.Items.IndexOf(ddlPrevVehMake.Items.FindByValue(subreader["VMIntNo"].ToString()));
                            ddlPrevVehType.SelectedIndex = ddlPrevVehType.Items.IndexOf(ddlPrevVehType.Items.FindByValue(subreader["VTIntNo"].ToString()));

                            txtNewRegNo.Text = subreader["NotRegNo"].ToString();
                            ddlNewVehicleColourDescr.SelectedIndex = ddlNewVehicleColourDescr.Items.IndexOf(ddlNewVehicleColourDescr.Items.FindByValue(subreader["NewVehicleColourDescr"].ToString()));
                            ddlNewVehMake.SelectedIndex = ddlNewVehMake.Items.IndexOf(ddlNewVehMake.Items.FindByValue(subreader["NewVMIntNo"].ToString()));
                            ddlNewVehType.SelectedIndex = ddlNewVehType.Items.IndexOf(ddlNewVehType.Items.FindByValue(subreader["NewVTIntNo"].ToString()));

                            //}   // Oscar 20120401, find this, why add an "}" here? if reader.Read() is false, we can't run the stuff at back.

                            this.ViewState.Add("ChargeStatus", Helper.GetReaderValue<int>(subreader, "CSCode"));
                            this.ViewState.Add("DrvIntNo", Helper.GetReaderValue<int>(subreader, "DrvIntNo"));
                            this.ViewState.Add("ChgIntNo", Helper.GetReaderValue<int>(subreader, "ChgIntNo"));
                            this.ViewState.Add("ChargeRowVersion", Helper.GetReaderValue<Int64>(subreader, "ChargeRowVersion"));

                            this.txtSurname.Text = Helper.GetReaderValue<string>(subreader, "DrvSurname");
                            this.txtForenames.Text = Helper.GetReaderValue<string>(subreader, "DrvForenames");
                            this.txtInitials.Text = Helper.GetReaderValue<string>(subreader, "DrvInitials");
                            this.txtAge.Text = Helper.GetReaderValue<string>(subreader, "DrvAge");
                            this.txtIDNumber.Text = Helper.GetReaderValue<string>(subreader, "DrvIDNumber");
                            this.txtNationality.Text = Helper.GetReaderValue<string>(subreader, "DrvNationality");

                            if (this.txtNationality.Text.Length == 0)
                            {
                                this.txtNationality.Text = defaultNationality;
                            }

                            string idType = Helper.GetReaderValue<string>(subreader, "DrvIDType");
                            this.txtPO1.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd1");
                            this.txtPo2.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd2");
                            this.txtPo3.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd3");
                            this.txtPo4.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd4");
                            this.txtPo5.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd5");
                            this.txtPoArea.Text = Helper.GetReaderValue<string>(subreader, "DrvPoCode");
                            this.txtStreet1.Text = Helper.GetReaderValue<string>(subreader, "DrvStAdd1");
                            this.txtStreet2.Text = Helper.GetReaderValue<string>(subreader, "DrvStAdd2");
                            this.txtStreet3.Text = Helper.GetReaderValue<string>(subreader, "DrvStAdd3");
                            this.txtStreet4.Text = Helper.GetReaderValue<string>(subreader, "DrvStAdd4");
                            this.txtStreetArea.Text = Helper.GetReaderValue<string>(subreader, "DrvStCode");
                            this.txtHomeNo.Text = Helper.GetReaderValue<string>(subreader, "DrvHomeNo");
                            this.txtCellNo.Text = Helper.GetReaderValue<string>(subreader, "DrvCellNo");
                            this.txtWorkNo.Text = Helper.GetReaderValue<string>(subreader, "DrvWorkNo");

                            if (subreader["LockedFlag"].ToString().Equals("Y"))
                            {
                                btnReverse.Enabled = false;
                                btnDecide.Enabled = false;
                                btnUpdate.Enabled = false;
                                chkChangeOffender.Enabled = false;
                                chkChangeRegistration.Enabled = false;

                            }
                            else
                            {
                                //if (this.isSummons && repCode != SUMMONS_REPCODE_NONE && repCode != REPCODE_NONE && !this.isNew)
                                //Oscar 20110324 changed, don't need "this.isSummons"
                                if (repCode != SUMMONS_REPCODE_NONE && repCode != REPCODE_NONE && !this.isNew)
                                {
                                    //this.btnDecide.Enabled = false;
                                    this.btnDecide.Visible = false;
                                    this.rblCodes.Items.Clear();
                                }
                                else
                                {
                                    //this.btnDecide.Enabled = true;
                                    this.btnDecide.Visible = true;
                                }

                                btnReverse.Enabled = true;
                                btnUpdate.Enabled = true;
                                chkChangeOffender.Enabled = true; //333
                                chkChangeRegistration.Enabled = true;
                            }

                        }   // Oscar 2012 add an "}" here to replace the above "}" (line 1033)
                        else
                        {
                            // Oscar 20121211 add for missing IsNoAOG
                            var amount = Convert.ToDecimal(row["ChgFineAmount"]);
                            var noAog = row["ChgNoAOG"].ToString();
                            this.isNoAOG = noAog.Equals("Y", StringComparison.OrdinalIgnoreCase) || amount == 999999;
                            ViewState.Add("IsNoAOG", this.isNoAOG);
                        }
                        subreader.Close();
                    }
                }
            }

            // Oscar 20120813 added for authorise 2nd rep
            if ((this.btnDecide.Visible || this.btnReverse.Visible)
                && !CheckManagementOverridePermission()
                && GetDecidedRepresentationCount(chgIntNo) != 0
                && !CheckRecommendationDateIn24Hours(recommendDate)
                )
            {
                this.btnDecide.Enabled = false;
                this.btnReverse.Enabled = false;
            }

            // Oscar 20130208 added for status check.
            if (this.btnReverse.Visible)
            {
                this.btnReverse.Visible = CheckStatus(cache.CurrentChargeStatus.GetValueOrDefault(), CheckType.Reverse);
            }

            // Oscar 2013-06-05 added
            BlockNoAOGOffence();

            //Jake 2013-12-06 added ,we need to unlock txtRevFineAmmount if rep code =none
            rblCodes_OnSelectedIndexChanged(this, null);

            // 2014-02-12, Oscar added "IsReadOnly" for existing Charge Rep
            if (cache.IsReadOnly.GetValueOrDefault()
                && (this.btnUpdate.Visible
                    || this.btnDecide.Visible
                    || this.btnReverse.Visible))
            {
                this.btnUpdate.Enabled = false;
                this.btnDecide.Enabled = false;
                this.btnReverse.Enabled = false;
            }
        }

        #region Oscar 20101021 - disable for backup
        //private void GetRepresentationDetails1()
        //{
        //    this.repIntNo = 0;
        //    //this.btnOptReport.Visible = false;
        //    this.lblError.Visible = true;
        //    this.pnlRepresentation.Visible = false;
        //    pnlNoResults.Visible = false;
        //    this.pnlCharges.Visible = false;
        //    SqlDataReader reader = null;

        //    //dls 071010 - added this, as the 99 repcode disables it and never re-enables it for the other codes
        //    pnlChangeRegNoDecision.Enabled = true;

        //    string search = txtSearch.Text.Trim().Replace(@" ", "");
        //    this.repIntNo = ViewState["RepIntNo"] == null ? 0 : Convert.ToInt32(ViewState["RepIntNo"].ToString());

        //    Stalberg.TMS.RepresentationDB repList = new Stalberg.TMS.RepresentationDB(connectionString);

        //    repAction = ViewState["RepAction"] == null ? "R" : ViewState["RepAction"].ToString();

        //    if (this.isSummons)
        //    {
        //        this.hdnType_rep.Value = "0";
        //        this.hdnType_srep.Value = this.repIntNo.ToString();
        //    }
        //    else
        //    {
        //        this.hdnType_rep.Value = this.repIntNo.ToString();
        //        this.hdnType_srep.Value = "0";
        //    }

        //    // Check the rule for which date to use as the representation cutoff date
        //    string columnName = this.CheckLastRepresentationDateRule();

        //    reader = repList.NewSearchRepresentations(autIntNo, search, this.repIntNo, repAction, columnName);

        //    //dls 070825 - no longer doing an extra read inside the recordset, so can remove this
        //    //reader.NextResult();

        //    if (reader.HasRows)
        //    {
        //        while (reader.Read())
        //        {
        //            this.ViewState.Add("ChgIntNo", reader["ChgIntNo"]);
        //            int status = (int)reader["ChargeStatus"];
        //            this.ViewState.Add("SumIntNo", reader["SumIntNo"]);

        //            //use auth rule setting for maximum status allowed - default is 900
        //            //if (status < _csCodeSummonsReturned)
        //            //if (status < maxStatusPaymentAllowed || status == CODE_CASE_CANCELLED)
        //            //if (status != CODE_CASE_CANCELLED)

        //            //dls 071010 - there is nothing wrong with this check this is what it must be leave it here, please!!!!   
        //            //we have to allow them to reverse a 927!!!

        //            //dls 080611 - all the rules have changed - they are no longer allowed to reverse a summons rep, as it has too many implications

        //            if (status < maxStatusPaymentAllowed || status == CODE_CASE_CANCELLED || status == CODE_SUMMONS_WITHDRAWN_CHARGE)  //there is nothing wrong with this check this is what it must be leave it here, please!!!!            
        //            {
        //                int repCode = REPCODE_NONE;
        //                if (!reader["RCCode"].Equals(DBNull.Value))
        //                    //repCode = (byte)reader["RCCode"];   // Oscar 20100926 - replace this as it cause a cast exception
        //                    repCode = (int)reader["RCCode"];

        //                // Set whether this is a summons or not
        //                this.isSummons = Convert.ToBoolean(reader["IsSummons"]);

        //                // Update an existing representation 
        //                this.ViewState.Add("IsSummons", this.isSummons);
        //                //cannot use this here if there are multiple reps and we are trying to add a new one - it gets done later when the new rep is added! See *****
        //                //this.ViewState.Add("RepIntNo", reader["RepIntNo"]);
        //                //cannot use this here if there are multiple reps and we are trying to add a new one
        //                //this.hdnType.Value = reader["RepIntNo"] == System.DBNull.Value ? "0" : reader["RepIntNo"].ToString();
        //                this.ViewState.Add("ChargeRowVersion", reader["ChargeRowVersion"]);
        //                this.ViewState.Add("NotIntNo", reader["NotIntNo"]);
        //                this.ViewState.Add("RCCode", repCode);

        //                this.txtTicketNo.Text = (string)reader["NotTicketNo"];
        //                this.hdnTicketNo.Value = this.txtTicketNo.Text;
        //                this.txtRegNo.Text = (string)reader["OrigRegNo"];
        //                this.txtRepOffenderName.Text = Helper.GetReaderValue<string>(reader, "RepOffenderName");
        //                this.txtRepOffenderAddress.Text = Helper.GetReaderValue<string>(reader, "RepOffenderAddress");
        //                this.txtOffenceDescr.Text = (string)reader["ChgOffenceDescr"];

        //                this.wdcRepDate.Text = Helper.GetReaderValue<DateTime>(reader, "RepDate").ToString(DATE_FORMAT);
        //                if (this.isNew)
        //                    this.txtRepDetails.Text = string.Empty;
        //                else
        //                    this.txtRepDetails.Text = Helper.GetReaderValue<string>(reader, "RepDetails");
        //                this.txtChargeStatus.Text = Helper.GetReaderValue<string>(reader, "RepCSDescr");
        //                this.txtOriginalAmount.Text = reader["ChgFineAmount"].ToString();

        //                //dls 071218 - they want to leave the amount as 999999 for a NoAOG
        //                if (reader["ChgRevFineAmount"].ToString().Equals("0") && reader["ChgNoAOG"].ToString().Equals("Y"))
        //                    this.txtRevAmount.Text = "999999";
        //                else
        //                    this.txtRevAmount.Text = reader["ChgRevFineAmount"].ToString();

        //                if (this.isNew)
        //                    this.txtReversalReason.Text = string.Empty;
        //                else
        //                    this.txtReversalReason.Text = reader["RepReverseReason"].ToString();
        //                this.chkChangeOffender.Enabled = true;
        //                this.chkChangeRegistration.Enabled = true;
        //                this.txtRecommend.Visible = false;
        //                this.lblRecommend.Visible = false;
        //                if (this.isNew)
        //                    this.txtRepOfficial.Text = string.Empty;
        //                else
        //                    this.txtRepOfficial.Text = reader["RepOfficial"].ToString();

        //                // Oscar 20101014 - added checking for S56
        //                this.noticeType = Helper.GetReaderValue<char>(reader, "NotFilmType") == 'M' ? NoticeType.S56 : NoticeType.Others;

        //                this.SetupControls();

        //                //if (repAction == "R" && btnUpdate.Text == " Add ")
        //                if (btnUpdate.Text == " Add ")
        //                {
        //                    // Register a new representation
        //                    ////once again, we can't do this here, because we need to set the RepIntNo when we add the rep later:  see ****
        //                    //this.ViewState.Add("RepIntNo", 0);
        //                    this.txtTicketNo.Text = (string)reader["NotTicketNo"];
        //                    this.txtRegNo.Text = (string)reader["NotRegNo"];
        //                    if (!reader["Name"].Equals(DBNull.Value))
        //                        this.txtRepOffenderName.Text = ((string)reader["Name"]).Trim();
        //                    StringBuilder sb = new StringBuilder();
        //                    if (!reader["DrvPoAdd1"].Equals(DBNull.Value))
        //                        sb.Append(reader["DrvPoAdd1"] + "\n");
        //                    if (!reader["DrvPoAdd2"].Equals(DBNull.Value))
        //                        sb.Append(reader["DrvPoAdd2"] + "\n");
        //                    if (!reader["DrvPoAdd3"].Equals(DBNull.Value))
        //                        sb.Append(reader["DrvPoAdd3"] + "\n");
        //                    if (!reader["DrvPoAdd4"].Equals(DBNull.Value))
        //                        sb.Append(reader["DrvPoAdd4"] + "\n");
        //                    if (!reader["DrvPoAdd5"].Equals(DBNull.Value))
        //                        sb.Append(reader["DrvPoAdd5"] + "\n");
        //                    if (!reader["DrvPoCode"].Equals(DBNull.Value))
        //                        sb.Append(reader["DrvPoCode"]);
        //                    this.txtRepOffenderAddress.Text = sb.ToString().Trim();
        //                    this.txtOffenceDescr.Text = (string)reader["ChgOffenceDescr"];

        //                    this.wdcRepDate.Text = DateTime.Today.ToString(DATE_FORMAT);
        //                    if (this.isNew)
        //                        this.txtRepDetails.Text = string.Empty;
        //                    else
        //                        this.txtRepDetails.Text = reader["RepDetails"].ToString();
        //                    this.txtChargeStatus.Text = (string)reader["RepCSDescr"];
        //                    this.txtOriginalAmount.Text = reader["ChgFineAmount"].ToString();

        //                    //dls 071218 - they want to leave the amount as 999999 for a NoAOG
        //                    if (reader["ChgRevFineAmount"].ToString().Equals("0"))
        //                        this.txtRevAmount.Text = "999999";
        //                    else
        //                        this.txtRevAmount.Text = reader["ChgRevFineAmount"].ToString();

        //                    if (this.isNew)
        //                        this.txtRecommend.Text = string.Empty;
        //                    else
        //                        this.txtRecommend.Text = reader["RepRecommend"].ToString();
        //                    this.txtOffenceDescr.Enabled = true;
        //                    this.txtOffenceDescr.ReadOnly = false;
        //                    this.btnReverse.Visible = false;
        //                    this.txtReversalReason.Visible = false;
        //                    this.txtRecommend.Visible = false;
        //                    this.lblReversalReason.Visible = false;
        //                    this.lblRecommend.Visible = false;
        //                }

        //                if (repAction == "R" && btnUpdate.Text == "Update")
        //                {
        //                    //dls 080611 - no longer allowed to reverse a summons rep - they need to process a new one! or create via a single summons
        //                    this.btnDecide.Enabled = true;

        //                    //if (reader["ChargeStatus"].Equals(410) || reader["ChargeStatus"].Equals(420) || this.isSummons)
        //                    if (reader["ChargeStatus"].Equals(410) || reader["ChargeStatus"].Equals(420))       // || this.isSummons)
        //                    {
        //                        this.btnReverse.Visible = true;
        //                        this.txtReversalReason.Visible = true;
        //                        this.lblReversalReason.Visible = true;
        //                    }
        //                    else
        //                    {
        //                        this.btnReverse.Visible = false;
        //                        this.txtReversalReason.Visible = false;
        //                        this.txtRecommend.Visible = false;
        //                        this.lblReversalReason.Visible = false;
        //                        this.lblRecommend.Visible = false;

        //                        if (this.isSummons)
        //                        {
        //                            this.btnDecide.Enabled = false;
        //                        }
        //                    }
        //                }

        //                this.pnlRepresentation.Visible = true;
        //                this.lblError.Visible = false;

        //                if (repAction == "D" || repAction == "O")
        //                {
        //                    // We want to decide a representation
        //                    this.pnlDecide.Visible = true;
        //                    this.txtRecommend.Visible = true;
        //                    this.lblRecommend.Visible = true;
        //                    this.chkChangeOffender.Visible = true;
        //                    this.pnlChangeOfOffender.Visible = false;

        //                    if (reader["ChargeStatus"].Equals(450))
        //                        btnDecide.Visible = false;
        //                    else
        //                        btnDecide.Visible = true;

        //                    btnUpdate.Visible = false;

        //                    if (repAction == "O")
        //                    {
        //                        chkChangeOffender.Checked = true;
        //                        pnlChangeOfOffender.Visible = true;
        //                        pnlChangeOfOffender.Enabled = true;
        //                        btnDecide.Visible = true;
        //                        btnUpdate.Visible = false;
        //                    }

        //                    if (repCode == REPCODE_CHANGEOFFENDER || repCode == SUMMONS_REPCODE_CHANGEOFFENDER)
        //                    {
        //                        chkChangeOffender.Checked = true;
        //                        pnlChangeOfOffender.Visible = true;
        //                        pnlChangeOfOffender.Enabled = true;
        //                        //repCode = this.isSummons ? SUMMONS_REPCODE_NONE : REPCODE_NONE;

        //                        if (repCode == REPCODE_CHANGEOFFENDER)
        //                        {
        //                            repCode = REPCODE_NONE;
        //                        }
        //                    }

        //                    else if (repCode == REPCODE_CANCELLED)
        //                    {
        //                        chkChangeOffender.Checked = false;
        //                        pnlChangeOfOffender.Enabled = false;
        //                        repCode = this.isSummons ? SUMMONS_REPCODE_NONE : REPCODE_NONE;
        //                        chkChangeRegistration.Checked = false;
        //                        pnlChangeRegNoDecision.Enabled = false;
        //                        pnlRegistration.Enabled = false;
        //                    }

        //                    if (this.isSummons && (repCode == REPCODE_NONE || repCode == SUMMONS_REPCODE_NONE || repCode == SUMMONS_REPCODE_CHANGEOFFENDER))
        //                        this.rblCodes.SelectedValue = SUMMONS_REPCODE_NONE.ToString();
        //                    else
        //                        this.rblCodes.SelectedValue = repCode.ToString();

        //                    Stalberg.TMS.RepresentationDB subrepList = new Stalberg.TMS.RepresentationDB(this.connectionString);

        //                    if (this.repIntNo == 0)
        //                    {
        //                        this.txtReversalReason.Visible = false;
        //                        this.lblReversalReason.Visible = false;
        //                        this.btnReverse.Visible = false;

        //                        decimal originalAmount = decimal.Parse(this.txtOriginalAmount.Text);
        //                        string offenderName = this.txtRepOffenderName.Text.Trim();
        //                        string changeOfOffencer = "N";

        //                        if (repAction == "O")
        //                            changeOfOffencer = "Y";

        //                        //insert it first
        //                        string errMsg = string.Empty;

        //                        chgIntNo = (int)ViewState["ChgIntNo"];
        //                        repIntNo = subrepList.NewUpdateRepresentation(repIntNo, chgIntNo, "",
        //                            DateTime.Now, offenderName, txtRepOffenderAddress.Text, "", "",
        //                            loginUser, this.autIntNo, CODE_REP_LOGGED_CHARGE, originalAmount, changeOfOffencer, "A", ref errMsg, this.isSummons);

        //                        // Indicate that the representation has been updated
        //                        if (repIntNo == -1)
        //                        {
        //                            lblError.Text = "Unable to update/add representation - representation with the same representation code already exists";
        //                            lblError.Visible = true;
        //                            return;
        //                        }
        //                        else if (repIntNo == -2)
        //                        {
        //                            lblError.Text = "Error could not update/add representation: " + errMsg;
        //                            lblError.Visible = true;
        //                            return;
        //                        }
        //                        //***** this is where we add the new repintno to the view state.
        //                        this.ViewState.Add("RepIntNo", repIntNo);

        //                        if (this.isSummons)
        //                        {
        //                            this.hdnType_rep.Value = "0";
        //                            this.hdnType_srep.Value = this.repIntNo.ToString();
        //                        }
        //                        else
        //                        {
        //                            this.hdnType_rep.Value = this.repIntNo.ToString();
        //                            this.hdnType_srep.Value = "0";
        //                        }

        //                        this.hdnTicketNo.Value = this.txtTicketNo.Text;
        //                    }
        //                    else
        //                    {
        //                        if (this.isSummons && repCode != SUMMONS_REPCODE_REDUCTION)
        //                        {
        //                            this.txtReversalReason.Visible = false;
        //                            this.lblReversalReason.Visible = false;
        //                            this.btnReverse.Visible = false;
        //                        }
        //                        else
        //                        {
        //                            this.txtReversalReason.Visible = true;
        //                            this.lblReversalReason.Visible = true;
        //                            this.btnReverse.Visible = true;
        //                        }

        //                    }

        //                    SqlDataReader subreader = subrepList.RetrieveRepresentation(this.repIntNo, this.isSummons);
        //                    if (subreader.Read())
        //                    {
        //                        decimal amount = Convert.ToDecimal(subreader["ChgFineAmount"]);
        //                        this.isNoAOG = (amount == 999999);
        //                        this.ViewState.Add("IsNoAOG", this.isNoAOG);

        //                        if (subreader["RepOfficial"] != DBNull.Value && !this.isNew)
        //                            this.txtRepOfficial.Text = subreader["RepOfficial"].ToString();

        //                        if (subreader["RepDetails"] != DBNull.Value && !this.isNew)
        //                            this.txtRepDetails.Text = subreader["RepDetails"].ToString();

        //                        if (subreader["RepRecommend"] != DBNull.Value & !this.isNew)
        //                            this.txtRecommend.Text = subreader["RepRecommend"].ToString();

        //                        if (subreader["RepReverseReason"] != DBNull.Value & !this.isNew)
        //                            this.txtReversalReason.Text = subreader["RepReverseReason"].ToString();

        //                        if (subreader["ChangeOfOffender"] != DBNull.Value && !this.isNew)
        //                            chkChangeOffender.Checked = subreader["ChangeOfOffender"].ToString().Equals("Y") ? true : false;

        //                        if (subreader["LetterTo"] != DBNull.Value)
        //                        {
        //                            if (subreader["LetterTo"].ToString().Equals("A"))
        //                                this.rdlLetter.SelectedValue = "D";
        //                            else
        //                                this.rdlLetter.SelectedValue = subreader["LetterTo"].ToString();
        //                        }

        //                        this.ShowChangeOfOffender();

        //                        if (subreader["ChangeOfRegNo"] != DBNull.Value)
        //                            chkChangeRegistration.Checked = subreader["ChangeOfRegNo"].ToString().Equals("Y") ? true : false;

        //                        if (subreader["ChangeOfRegNoType"] != DBNull.Value)
        //                            rdlRegDecision.SelectedValue = subreader["ChangeOfRegNoType"].ToString();

        //                        if (subreader["NewRegNoDetails"] != DBNull.Value)
        //                            chkNoFurtherDetails.Checked = subreader["NewRegNoDetails"].ToString().Equals("Y") ? false : true;

        //                        ShowChangeOfRegNo(false);

        //                        txtPrevRegNo.Text = subreader["OrigRegNo"].ToString();
        //                        ddlNotVehicleColour.SelectedIndex = ddlNotVehicleColour.Items.IndexOf(ddlNotVehicleColour.Items.FindByValue(subreader["NotVehicleColour"].ToString()));
        //                        ddlPrevVehMake.SelectedIndex = ddlPrevVehMake.Items.IndexOf(ddlPrevVehMake.Items.FindByValue(subreader["VMIntNo"].ToString()));
        //                        ddlPrevVehType.SelectedIndex = ddlPrevVehType.Items.IndexOf(ddlPrevVehType.Items.FindByValue(subreader["VTIntNo"].ToString()));

        //                        txtNewRegNo.Text = subreader["NotRegNo"].ToString();
        //                        ddlNewVehicleColourDescr.SelectedIndex = ddlNewVehicleColourDescr.Items.IndexOf(ddlNewVehicleColourDescr.Items.FindByValue(subreader["NewVehicleColourDescr"].ToString()));
        //                        ddlNewVehMake.SelectedIndex = ddlNewVehMake.Items.IndexOf(ddlNewVehMake.Items.FindByValue(subreader["NewVMIntNo"].ToString()));
        //                        ddlNewVehType.SelectedIndex = ddlNewVehType.Items.IndexOf(ddlNewVehType.Items.FindByValue(subreader["NewVTIntNo"].ToString()));

        //                    }

        //                    this.ViewState.Add("ChargeStatus", Helper.GetReaderValue<int>(subreader, "CSCode"));
        //                    this.ViewState.Add("DrvIntNo", Helper.GetReaderValue<int>(subreader, "DrvIntNo"));
        //                    this.ViewState.Add("ChgIntNo", Helper.GetReaderValue<int>(subreader, "ChgIntNo"));
        //                    this.ViewState.Add("ChargeRowVersion", Helper.GetReaderValue<Int64>(subreader, "ChargeRowVersion"));

        //                    this.txtSurname.Text = Helper.GetReaderValue<string>(subreader, "DrvSurname");
        //                    this.txtForenames.Text = Helper.GetReaderValue<string>(subreader, "DrvForenames");
        //                    this.txtInitials.Text = Helper.GetReaderValue<string>(subreader, "DrvInitials");
        //                    this.txtAge.Text = Helper.GetReaderValue<string>(subreader, "DrvAge");
        //                    this.txtIDNumber.Text = Helper.GetReaderValue<string>(subreader, "DrvIDNumber");
        //                    this.txtNationality.Text = Helper.GetReaderValue<string>(subreader, "DrvNationality");

        //                    if (this.txtNationality.Text.Length == 0)
        //                    {
        //                        this.txtNationality.Text = defaultNationality;
        //                    }

        //                    string idType = Helper.GetReaderValue<string>(subreader, "DrvIDType");
        //                    this.txtPO1.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd1");
        //                    this.txtPo2.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd2");
        //                    this.txtPo3.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd3");
        //                    this.txtPo4.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd4");
        //                    this.txtPo5.Text = Helper.GetReaderValue<string>(subreader, "DrvPoAdd5");
        //                    this.txtPoArea.Text = Helper.GetReaderValue<string>(subreader, "DrvPoCode");
        //                    this.txtStreet1.Text = Helper.GetReaderValue<string>(subreader, "DrvStAdd1");
        //                    this.txtStreet2.Text = Helper.GetReaderValue<string>(subreader, "DrvStAdd2");
        //                    this.txtStreet3.Text = Helper.GetReaderValue<string>(subreader, "DrvStAdd3");
        //                    this.txtStreet4.Text = Helper.GetReaderValue<string>(subreader, "DrvStAdd4");
        //                    this.txtStreetArea.Text = Helper.GetReaderValue<string>(subreader, "DrvStCode");
        //                    this.txtHomeNo.Text = Helper.GetReaderValue<string>(subreader, "DrvHomeNo");
        //                    this.txtCellNo.Text = Helper.GetReaderValue<string>(subreader, "DrvCellNo");
        //                    this.txtWorkNo.Text = Helper.GetReaderValue<string>(subreader, "DrvWorkNo");

        //                    if (subreader["LockedFlag"].ToString().Equals("Y"))
        //                    {
        //                        btnReverse.Enabled = false;
        //                        btnDecide.Enabled = false;
        //                        btnUpdate.Enabled = false;
        //                        chkChangeOffender.Enabled = false;
        //                        chkChangeRegistration.Enabled = false;

        //                    }
        //                    else
        //                    {
        //                        if (this.isSummons && repCode != SUMMONS_REPCODE_NONE && repCode != REPCODE_NONE)
        //                        {
        //                            this.btnDecide.Enabled = false;
        //                        }
        //                        else
        //                        {
        //                            this.btnDecide.Enabled = true;
        //                        }

        //                        btnReverse.Enabled = true;
        //                        btnUpdate.Enabled = true;
        //                        chkChangeOffender.Enabled = true;
        //                        chkChangeRegistration.Enabled = true;
        //                    }

        //                    subreader.Close();
        //                }
        //            }
        //            else
        //            {
        //                // The ticket is not valid for representation
        //                this.lblError.Text = string.Format("The Status of the Ticket is: '{0}'", (string)reader["CSDescr"]);
        //                reader.Close();
        //                pnlRetrieve.Visible = true;
        //                pnlCharges.Visible = true;
        //                return;
        //            }
        //            //dls 080617 - added this here so that it only reads the first rep in the list
        //            break;
        //        }
        //        reader.Close();

        //        //if (this.isSummons)
        //        //{
        //        //    //Barry Dickson: taken out as they can change the RegNo details as the summons will be withdrawn
        //        //    //this.chkChangeRegistration.Visible = false;
        //        //}
        //    }
        //    else
        //    {
        //        // There was no ticket found
        //        this.lblError.Text = "The ticket was not found.";
        //        btnReport.Visible = false;
        //    }
        //    this.txtRepOffenderName.Text = "Oscar"; //test
        //}
        #endregion

        // Oscar 20101019 - rewrite this method
        private void SearchRepresentations(bool bIgnore)
        {
            // Oscar 2013-05-24 added
            cache.AllowNoAOGOffence = new AuthorityRulesDB(this.connectionString).GetAuthorityRule(GetAutIntNo(), "4537").ARString.Equals("Y", StringComparison.OrdinalIgnoreCase);
            cache.IsNoAOG = null;
            cache.IsNoAOGDefined = null;
            this.lblError.Text = string.Empty;

            //added by oscar 20101019
            this.grdRepresentation.Visible = false;
            this.lblEmptyData.Visible = false;

            this.repIntNo = 0;
            //this.btnOptReport.Visible = false;
            this.lblError.Visible = true;
            this.pnlRepresentation.Visible = false;
            this.pnlNoResults.Visible = true;
            this.pnlCharges.Visible = false;

            // Check the Authority
            if (ddlSelectLA.SelectedIndex < 0 && !bIgnore)
            {
                lblError.Text = (string)this.GetLocalResourceObject("lblError.Text5");
                return;
            }
            if (!bIgnore)
                this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            // Make sure there's something in the ticket text box
            if (txtSearch.Text.Equals(string.Empty))
            {
                lblError.Text = (string)this.GetLocalResourceObject("lblError.Text6");
                return;
            }

            string columnName;
            DataTable dtSource = GetSearchRepresentationsSourceRefresh(out columnName);
            ViewState.Remove("ChargeSource");

            if (dtSource.Rows.Count > 0)
            {
                // Oscar 20101018
                ViewState["ChargeSource"] = dtSource;
                this.divNoticeTitle.Visible = true;
                this.lblSummonsNo.Text = Helper.GetDataRowValue<string>(dtSource.Rows[0], "SummonsNo");
                if (string.IsNullOrEmpty(this.lblSummonsNo.Text.Trim()))
                    this.lblSummonsNo.Visible = false;
                else
                    this.lblSummonsNo.Visible = true;
                //this.lblNoticeNo.Text = Helper.GetDataRowValue<string>(dtSource.Rows[0], "NoticeNo");
                //this.lblNoticeDescr.Text = "Location: " + Helper.GetDataRowValue<string>(dtSource.Rows[0], "NotLocDescr");
                this.lblNoticeNo.Text = Helper.GetDataRowValue<string>(dtSource.Rows[0], "NoticeNo");
                this.lblNoticeDescr.Text = (string)this.GetLocalResourceObject("lblLocation.Text") + Helper.GetDataRowValue<string>(dtSource.Rows[0], "NotLocDescr");

                //Jerry 2013-04-16 add
                cache.GrdChg_Select = null;

                //noOfReps = 0;   // Oscar 20101021 - move this into new place

                //this.grdCharges.DataSource = reader;
                this.grdCharges.DataSource = GetChargeSource(dtSource);     //changed by oscar 20101018
                this.grdCharges.DataKeyNames = new string[] { "ChgIntNo", "CanAutoDoc", "IsReadOnly" };
                this.grdCharges.DataBind();
                this.grdCharges.Visible = true;
                this.pnlCharges.Visible = true;

                // Oscar 20101021 - move this into new place
                //if (noOfReps > 0)
                //{
                //    btnReport.Visible = true;
                //}
                //else
                //{
                //    btnReport.Visible = false;
                //}
            }
            else
            {
                // added by Oscar 20101019
                this.divNoticeTitle.Visible = false;

                string msg = (string)this.GetLocalResourceObject("errorMsgToLblError.Text1");
                switch (repAction)
                {
                    case "R":
                        msg += (string)this.GetLocalResourceObject("errorMsgToLblError.Text2");
                        break;
                    case "D":
                        msg += (string)this.GetLocalResourceObject("errorMsgToLblError.Text3");
                        break;
                    case "O":
                        msg += (string)this.GetLocalResourceObject("errorMsgToLblError.Text4");
                        break;
                }
                msg += (string)this.GetLocalResourceObject("errorMsgToLblError.Text5");
                if (columnName.Equals("NotPaymentDate"))
                    msg += (string)this.GetLocalResourceObject("errorMsgToLblError.Text6");
                if (columnName.Equals("CDCourtRollCreatedDate"))
                    msg += (string)this.GetLocalResourceObject("errorMsgToLblError.Text7");
                this.lblError.Text = msg;
                this.lblError.Visible = true;
                this.grdCharges.Visible = false;
                this.pnlCharges.Visible = false;
                btnReport.Visible = false;
            }
            //added by Oscar 20101019
            this.btnAdd.Visible = false;

            // Oscar 2013-05-24 added
            if (RequireBlockNoAOGOffence())
            {
                this.lblError.Text += (string)GetLocalResourceObject("NoAOG_RepresentationNotAllowed");
                this.lblError.Visible = true;
            }
        }

        #region Oscar 20101019 - disable for backup
        //private void SearchRepresentations(bool bIgnore)
        //{
        //    this.repIntNo = 0;
        //    //this.btnOptReport.Visible = false;
        //    this.lblError.Visible = true;
        //    this.pnlRepresentation.Visible = false;
        //    pnlNoResults.Visible = true;
        //    this.pnlCharges.Visible = false;
        //    SqlDataReader reader = null;

        //    // Check the Authority
        //    if (ddlSelectLA.SelectedIndex < 0 && !bIgnore)
        //    {
        //        lblError.Text = "Please select a valid authority";
        //        return;
        //    }
        //    if (!bIgnore)
        //        this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

        //    // Make sure there's something in the ticket text box
        //    if (txtSearch.Text.Equals(string.Empty))
        //    {
        //        lblError.Text = "Ticket no. may not be blank";
        //        return;
        //    }

        //    // Check the last date for representation rule for this LA
        //    string columnName = this.CheckLastRepresentationDateRule();

        //    // Get the minimum charge status to return
        //    int minStatus = this.CheckMinimumStatusRule();

        //    string search = txtSearch.Text.Trim().Replace(" ", "");
        //    Stalberg.TMS.RepresentationDB repList = new Stalberg.TMS.RepresentationDB(this.connectionString);
        //    repAction = ViewState["RepAction"] == null ? "R" : ViewState["RepAction"].ToString();
        //    reader = repList.NewSearchRepresentations(autIntNo, search, this.repIntNo, repAction, columnName, minStatus);

        //    //dls 070825 - no longer doing an extra read inside the recordset, so can remove this
        //    //reader.NextResult();

        //    if (reader.HasRows)
        //    {
        //        noOfReps = 0;

        //        this.grdCharges.DataSource = reader;
        //        this.grdCharges.DataBind();
        //        this.grdCharges.Visible = true;
        //        this.pnlCharges.Visible = true;

        //        if (noOfReps > 0)
        //        {
        //            btnReport.Visible = true;
        //        }
        //        else
        //        {
        //            btnReport.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        string msg = "This notice is not available for ";
        //        switch (repAction)
        //        {
        //            case "R":
        //                msg += " registering a representation.";
        //                break;
        //            case "D":
        //                msg += " making a decision on a representation.";
        //                break;
        //            case "O":
        //                msg += " changing an offender.";
        //                break;
        //        }
        //        msg += " This notice has either been paid, or is at some other status that makes it unavailable for this action";
        //        if (columnName.Equals("NotPaymentDate"))
        //            msg += "<br>Notices that have passed their payment date cannot have representations made against them";
        //        if (columnName.Equals("CDCourtRollCreatedDate"))
        //            msg += "<br>Summons for which a final court roll has been printed cannot have representations made against them";
        //        this.lblError.Text = msg;
        //        this.lblError.Visible = true;
        //        this.grdCharges.Visible = false;
        //        this.pnlCharges.Visible = false;
        //        btnReport.Visible = false;
        //    }

        //    return;
        //}
        #endregion

        // Oscar 20101019 added - get data of charge + representation
        private DataTable GetSearchRepresentationsSource()
        {
            DataTable result = new DataTable();
            if (ViewState["ChargeSource"] != null)
            {
                result = (DataTable)ViewState["ChargeSource"];
            }
            if (result.Rows.Count == 0)
            {
                string columnName;
                result = GetSearchRepresentationsSourceRefresh(out columnName);
            }

            return result;
        }
        // Oscar 20101019 added - get refresh data of charge + representation
        private DataTable GetSearchRepresentationsSourceRefresh(out string columnName)
        {
            DataTable result = new DataTable();

            GetAutIntNo();

            // Check the last date for representation rule for this LA
            columnName = CheckLastRepresentationDateRule();

            // Get the minimum charge status to return
            int minStatus = CheckMinimumStatusRule();

            string search = txtSearch.Text.Trim().Replace(" ", "");
            if (string.IsNullOrEmpty(search)) return result;

            Stalberg.TMS.RepresentationDB repList = new Stalberg.TMS.RepresentationDB(this.connectionString);
            this.repAction = ViewState["RepAction"] == null ? "R" : ViewState["RepAction"].ToString();
            using (SqlDataReader reader = repList.NewSearchRepresentations(this.autIntNo, search, this.repIntNo, repAction, columnName, minStatus))
            {
                result.Load(reader);

                if (result.Rows.Count > 0)
                {
                    ViewState["NotIntNo"] = result.Rows[0]["NotIntNo"];
                    //ViewState["IsSummons"] = result.Rows[0]["IsSummons"];
                    //ViewState["SumServedStatus"] = result.Rows[0]["SumServedStatus"];
                    //ViewState["SumCaseNo"] = result.Rows[0]["SumCaseNo"];
                    cache.IsSummons = Convert.ToBoolean(result.Rows[0]["IsSummons"]);
                    cache.SumCaseNo = result.Rows[0]["SumCaseNo"].ToString();
                }
                else
                {
                    ViewState.Remove("NotIntNo");
                    //ViewState.Remove("IsSummons");
                    //ViewState.Remove("SumServedStatus");
                    //ViewState.Remove("SumCaseNo");
                    cache.IsSummons = null;
                    cache.SumCaseNo = null;
                }

                return result;
            }
        }

        // Oscar 20101018 added - get data of charge
        private DataTable GetChargeSource(DataTable dtSource)
        {
            DataView dv = new DataView(dtSource);
            dv.Sort = "ChgIntNo asc";
            DataTable dt = dv.ToTable(true, "ChgIntNo", "ChgOffenceDescr", "IsMainCharge", "CSDescr", "SumCourtDate", "IsSummons", "MainChargeID", "ChargeNo", "ChargeStatus", "HasRep", "ChargeRowVersion", "NotIntNo", "ChgOffenceCode", "ChgNoAOG", "IsSection35", "IsReadOnly");   //Oscar 20120801 added for presentation of document
            //dt = GetChargeSourceByMainChargeID(dt);
            //return dt;
            return GetChargeSourceByMainChargeID(dt);
        }

        // Oscar 20101112 - added
        private DataTable GetChargeSourceByMainChargeID(DataTable source)
        {
            DataTable tmpSource = source.Copy();
            DataTable changedSource = source.Clone();
            foreach (DataColumn dc in changedSource.Columns)
            {
                if (dc.ReadOnly == true)
                    dc.ReadOnly = false;
            }
            tmpSource.Columns.Add("IsAdded", typeof(int));

            //Oscar 20120801 added for presentation of document
            changedSource.Columns.Add("CanAutoDoc", typeof(bool));
            var arDB = new AuthorityRulesDB(this.connectionString);
            var autoDocRule = arDB.GetAuthorityRule(GetAutIntNo(), "4534").ARString.Equals("Y", StringComparison.OrdinalIgnoreCase) && this.repAction.Equals("D", StringComparison.OrdinalIgnoreCase);
            var offCodeList = GetOffenceCodeListByIsPresentationOfDocument();
            var checkMop = CheckManagementOverridePermission();

            DataRow newRow;
            DataRow[] drs;
            int index = 1;
            foreach (DataRow dr in tmpSource.Rows)
            {
                if (dr["IsAdded"] == DBNull.Value && (dr["MainChargeID"] == DBNull.Value || dr["IsMainCharge"].ToString().ToLower().Equals("main")))
                {
                    // Oscar 2013-05-24 added
                    var currentNoAOG = false;
                    if (!cache.IsNoAOGDefined.GetValueOrDefault())
                        currentNoAOG = dr["ChgNoAOG"].ToString().Equals("Y", StringComparison.OrdinalIgnoreCase);

                    //Oscar 20120801 added for presentation of document
                    var canAutoDoc = !Convert.ToBoolean(dr["HasRep"]) && (checkMop || GetDecidedRepresentationCount(Convert.ToInt32(dr["ChgIntNo"])) == 0);
                    var hasAlternate = false;

                    newRow = changedSource.NewRow();
                    DataRowCopy(dr, newRow);
                    changedSource.Rows.Add(newRow);
                    drs = tmpSource.Select(string.Format("MainChargeID = {0} and IsAdded is NULL", dr["ChgIntNo"]));
                    foreach (DataRow dr2 in drs)
                    {
                        var newRow2 = changedSource.NewRow();
                        DataRowCopy(dr2, newRow2);
                        newRow2["CanAutoDoc"] = false;
                        changedSource.Rows.Add(newRow2);
                        dr2["IsAdded"] = 1;

                        //Oscar 20120801 added for presentation of document
                        var isMainCharge = dr2["IsMainCharge"].ToString().Equals("main", StringComparison.OrdinalIgnoreCase);
                        hasAlternate = !isMainCharge;

                        // Oscar 2013-05-24 added
                        if (isMainCharge)
                            currentNoAOG = dr2["ChgNoAOG"].ToString().Equals("Y", StringComparison.OrdinalIgnoreCase);
                    }
                    dr["IsAdded"] = 1;

                    //Oscar 20120801 added for presentation of document
                    var offCode = dr["ChgOffenceCode"].ToString().Trim();
                    newRow["CanAutoDoc"] = autoDocRule && canAutoDoc && hasAlternate && !string.IsNullOrWhiteSpace(offCode) && offCodeList.Contains(offCode);

                    // Oscar 2013-05-24 added
                    if (!cache.IsNoAOGDefined.GetValueOrDefault() && currentNoAOG)
                    {
                        cache.IsNoAOG = true;
                        cache.IsNoAOGDefined = true;
                    }

                    index++;
                }

                if (cache.IsSection35.GetValueOrDefault() == false)
                {
                    cache.IsSection35 = Convert.ToBoolean(dr["IsSection35"]);
                }
            }



            return changedSource;
        }

        // Oscar 20101112 - added
        private void DataRowCopy(DataRow originalRow, DataRow targetRow)
        {
            targetRow["ChgIntNo"] = originalRow["ChgIntNo"];
            targetRow["ChgOffenceDescr"] = originalRow["ChgOffenceDescr"];
            targetRow["IsMainCharge"] = originalRow["IsMainCharge"];
            targetRow["CSDescr"] = originalRow["CSDescr"];
            targetRow["SumCourtDate"] = originalRow["SumCourtDate"];
            targetRow["IsSummons"] = originalRow["IsSummons"];
            targetRow["MainChargeID"] = originalRow["MainChargeID"];
            targetRow["ChargeNo"] = originalRow["ChargeNo"];
            targetRow["ChargeStatus"] = originalRow["ChargeStatus"];
            //targetRow["SumServedStatus"] = originalRow["SumServedStatus"];

            //Oscar 20120801 added for presentation of document
            targetRow["NotIntNo"] = originalRow["NotIntNo"];
            targetRow["ChargeRowVersion"] = originalRow["ChargeRowVersion"];
            targetRow["ChgOffenceCode"] = originalRow["ChgOffenceCode"];

            targetRow["ChgNoAOG"] = originalRow["ChgNoAOG"];

            // 2014-02-12, Oscar added "IsReadOnly" for existing Charge Rep
            targetRow["IsReadOnly"] = originalRow["IsReadOnly"];
        }

        // Oscar 20101018 added - get data of representation
        private DataView GetRepresentationSource(DataTable dtSource, int? chgIntNo, int? repIntNo)
        {
            DataView dv = new DataView(dtSource);

            if (chgIntNo != null && chgIntNo > 0)
            {
                if (repIntNo == null)
                    dv.RowFilter = string.Format("ChgIntNo = {0} and RepIntNo > 0", chgIntNo);
                else
                    dv.RowFilter = string.Format("ChgIntNo = {0}", chgIntNo);
            }
            if (repIntNo != null && repIntNo > 0)
                dv.RowFilter = string.Format("RepIntNo = {0}", repIntNo);

            dv.Sort = "RepIntNo desc";

            return dv;
        }

        // Oscar 20101019 added - the rule of being able to add or edit a representation
        //private bool IsAbleToEditRep(IsAbleToEditRepPara para, out bool isAbleToAdd, out string errorMsg)
        //{
        //    bool isAbleToEdit = true;
        //    isAbleToAdd = true;
        //    bool isAbleToAddTmp = true;
        //    errorMsg = string.Empty;

        //    // validate the ChgType 
        //    if (!string.IsNullOrEmpty(para.ChgType))
        //    {
        //        switch (para.ChgType.ToLower())
        //        {
        //            case "alternate":
        //            case "0":
        //            case "false":
        //                isAbleToAdd = false;
        //                break;
        //        }
        //    }

        //    string sumCaseNo = ViewState["SumCaseNo"].ToString();
        //    bool hasCaseNo = (sumCaseNo.Length == 0 || sumCaseNo.ToLower().Equals("none")) ? false : true;

        //    // validate the CourtDate
        //    if (para.CourtDate != null && para.IsSummons != null)
        //    {
        //        //dls 2011-04-05 - they are allowed to rep this if it is past court date, but was not served
        //        if (para.IsSummons == true && DateTime.Today.AddDays(noOfDaysBeforeCourt) > para.CourtDate && hasCaseNo)
        //        {
        //            isAbleToAdd = false;
        //            errorMsg = string.Format((string)GetLocalResourceObject("ErrorMsgInFunction.Text"), Convert.ToDateTime(para.CourtDate).ToString(DATE_FORMAT), noOfDaysBeforeCourt.ToString());
        //        }
        //    }

        //    // validate the data row
        //    if (para.Row != null)
        //    {
        //        int? rcCode;
        //        bool isLocked = !para.Row["LockedFlag"].Equals("N");

        //        if (para.Row["RCCode"] != DBNull.Value)
        //        {
        //            rcCode = Convert.ToInt32(para.Row["RCCode"]);

        //            if (this.repAction == "R")
        //            {
        //                if (rcCode == REPCODE_NONE)
        //                    isAbleToAddTmp = false;
        //            }

        //            if (!isLocked)
        //            {
        //                if (this.repAction == "R")
        //                {
        //                    if (rcCode != REPCODE_NONE && rcCode != SUMMONS_REPCODE_NONE)
        //                        isAbleToEdit = false;
        //                }
        //                else if (this.repAction == "D" || this.repAction == "O")
        //                {
        //                    if (rcCode == REPCODE_NONE || rcCode == SUMMONS_REPCODE_NONE)
        //                        isAbleToAddTmp = false;
        //                    else if (rcCode == REPCODE_CANCELLED)
        //                        isAbleToEdit = false;
        //                }
        //            }
        //            else
        //                isAbleToEdit = false;

        //            if (rcCode == REPCODE_WITHDRAW || rcCode == SUMMONS_REPCODE_WITHDRAW)
        //                isAbleToAdd = false;
        //        }
        //        else
        //            isAbleToEdit = false;

        //        DateTime courtDate = DateTime.MaxValue;
        //        DateTime dt;
        //        if (DateTime.TryParse(Convert.ToString(para.Row["SumCourtDate"]), out dt))
        //            courtDate = dt;

        //        //int sumServed = Convert.ToInt32(para.Row["SumServedStatus"]);
        //        //string sumCaseNo = para.Row["SumCaseNo"].ToString();
        //        bool isSummons = Convert.ToBoolean(para.Row["IsSummons"]);

        //        //bool served = false;

        //        //if (sumServed == 1 || sumServed == 2)
        //        //    served = true;

        //        if (isSummons && DateTime.Today.AddDays(noOfDaysBeforeCourt) > courtDate)
        //        {
        //            //if (served || sumCaseNo != "None"))
        //            if (hasCaseNo)
        //            {
        //                isAbleToAdd = false;
        //                if (DateTime.Today > courtDate)
        //                {
        //                    isAbleToEdit = false; // disable for testing
        //                    errorMsg = "A representation cannot be raised on this notice as the court date " + courtDate.ToString(DATE_FORMAT) + " has already passed";
        //                }
        //                else
        //                    errorMsg = "A representation cannot be raised on this notice due to the court date " + courtDate.ToString(DATE_FORMAT) + " being less than " + noOfDaysBeforeCourt.ToString() + " days from today";
        //            }
        //        }

        //        if ((this.repAction == "R" || this.allowAllInOne) && !isAbleToAddTmp)
        //            isAbleToAdd = isAbleToAddTmp;
        //    }

        //    return isAbleToEdit;
        //}

        bool CanChargeSelect(string isMainCharge)
        {
            if (!string.IsNullOrWhiteSpace(isMainCharge)
                && (isMainCharge.Equals("alternate", StringComparison.OrdinalIgnoreCase)
                    || isMainCharge.Equals("0")
                    || isMainCharge.Equals("false")
                   ))
            {
                return false;
            }
            return true;
        }

        bool CanRepAdd(DateTime courtDate, bool isSummons, out string errorMsg)
        {
            errorMsg = string.Empty;
            var canAdd = true;
            var hasCaseNo = !string.IsNullOrWhiteSpace(cache.SumCaseNo)
                            && !cache.SumCaseNo.Equals("none", StringComparison.OrdinalIgnoreCase);

            do
            {
                // 2014-02-12, Oscar added "IsReadOnly" for existing Charge Rep
                if (cache.IsReadOnly.GetValueOrDefault())
                {
                    canAdd = false;
                    break;
                }

                if (!cache.AlwaysAllow.GetValueOrDefault()
                    && isSummons
                    && hasCaseNo
                    && DateTime.Today.AddDays(this.noOfDaysBeforeCourt) > courtDate
                    )
                {
                    errorMsg = string.Format(Convert.ToString(GetLocalResourceObject("ErrorMsgInFunction.Text")), courtDate.ToString(DATE_FORMAT), this.noOfDaysBeforeCourt);
                    canAdd = false;
                    break;
                }

                if (this.grdRepresentation.Rows.Count > 0)
                {
                    var source = this.grdRepresentation.DataSource as DataView;
                    foreach (DataRowView row in source)
                    {
                        int? rcCode = null;
                        if (!row.Row.IsNull("RCCode"))
                            rcCode = Convert.ToInt32(row["RCCode"]);
                        var isLocked = !"N".Equals(row["LockedFlag"].ToString(), StringComparison.OrdinalIgnoreCase);
                        if (rcCode.HasValue)
                        {
                            if (this.repAction.Equals("R", StringComparison.OrdinalIgnoreCase)
                                && rcCode == REPCODE_NONE)
                            {
                                canAdd = false;
                                break;
                            }

                            if (this.allowAllInOne
                                && !isLocked
                                && (this.repAction.Equals("D", StringComparison.OrdinalIgnoreCase)
                                    || this.repAction.Equals("O", StringComparison.OrdinalIgnoreCase))
                                && (rcCode == REPCODE_NONE
                                    || rcCode == SUMMONS_REPCODE_NONE))
                            {
                                canAdd = false;
                                break;
                            }
                        }
                    }
                }


            } while (false);

            // Oscar 20130208 added for status check.
            return canAdd && CheckStatus(cache.CurrentChargeStatus.GetValueOrDefault(), CheckType.Add);
        }

        bool CanRepEdit(DataRowView row, out string errorMsg)
        {
            errorMsg = string.Empty;
            var canEdit = true;

            int? rcCode = null;
            if (!row.Row.IsNull("RCCode"))
                rcCode = Convert.ToInt32(row["RCCode"]);

            var isLocked = !"N".Equals(row["LockedFlag"].ToString(), StringComparison.OrdinalIgnoreCase);

            do
            {
                if (!rcCode.HasValue || isLocked)
                {
                    canEdit = false;
                    break;
                }

                if (this.repAction.Equals("R", StringComparison.OrdinalIgnoreCase)
                    && rcCode != REPCODE_NONE
                    && rcCode != SUMMONS_REPCODE_NONE)
                {
                    canEdit = false;
                    break;
                }

                if ((this.repAction.Equals("D", StringComparison.OrdinalIgnoreCase)
                     || this.repAction.Equals("O", StringComparison.OrdinalIgnoreCase))
                    && rcCode == REPCODE_CANCELLED)
                {
                    canEdit = false;
                    break;
                }

                var isSummons = Convert.ToBoolean(row["IsSummons"]);
                var hasCaseNo = !string.IsNullOrWhiteSpace(cache.SumCaseNo)
                                && !cache.SumCaseNo.Equals("none", StringComparison.OrdinalIgnoreCase);
                DateTime courtDate = DateTime.MaxValue, dt;
                if (DateTime.TryParse(row["SumCourtDate"].ToString(), out dt))
                    courtDate = dt;

                if (!cache.AlwaysAllow.GetValueOrDefault()
                    && isSummons
                    && hasCaseNo
                    && DateTime.Today.AddDays(this.noOfDaysBeforeCourt) > courtDate
                    && DateTime.Today > courtDate
                    )
                {
                    errorMsg = "A representation cannot be raised on this notice as the court date " + courtDate.ToString(DATE_FORMAT) + " has already passed";
                    canEdit = false;
                    break;
                }

            } while (false);

            return canEdit;
        }

        private int CheckMinimumStatusRule()
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4605";
            //rule.ARComment = "Default = 255 (posted).";
            //rule.ARDescr = "Status to start showing notices on enquiry screen";
            //rule.ARNumeric = 255;
            //rule.ARString = string.Empty;
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.loginUser;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "4605";
            rule.LastUser = this.loginUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            return value.Key; //rule.ARNumeric;
        }

        private string CheckLastRepresentationDateRule()
        {
            // Populate the rule with default values
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4650";
            ////rule.ARComment = "Either: NotPaymentDate (default) = the AOG date on the Notice table ; or, CDCourtRollCreatedDate = the date that the court roll is created";
            //rule.ARComment = "Either: NotPaymentDate (default) = the AOG date on the Notice table;  CDCourtRollCreatedDate = the date that the court roll is created; WithdrawSummonsRegardless = do not check for case number, etc";
            //rule.ARDescr = "Rule to determine which date rule to use as the cutoff date for representations.";
            //rule.ARString = "NotPaymentDate";
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.loginUser;

            //// Check the rule
            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);


            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "4650";
            rule.LastUser = this.loginUser;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            // Return the column
            return value.Value; //rule.ARString;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.SearchRepresentations(false);
            //PunchStats805806 enquiry Representation
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //Jerry 2013-04-03 check is paid
            bool isPaid = CheckNoticeOrSummonsIsPaid(Convert.ToInt32(ViewState["ChgIntNo"]), this.isSummons);
            if (isPaid)
            {
                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text22"), isSummons ? "Summons" : "Notice");
                lblError.Visible = true;
                return;
            }

            bool isValid = true;
            //int csCode = CODE_REP_LOGGED_CHARGE;
            // Oscar - changed
            int csCode = this.isSummons ? CODE_REP_LOGGED_SUMMONS : CODE_REP_LOGGED_CHARGE;
            decimal originalAmount = decimal.Parse(this.txtOriginalAmount.Text);
            string offenderName = this.txtRepOffenderName.Text.Trim();

            //Oscar 2013-05-24 added
            if (InvalidNoAOGOffenceInput(true)) return;

            // we first check if we are doing a straight forward representation register or a decision
            if (repAction == "R")
            {
                StringBuilder sb = new StringBuilder((string)this.GetLocalResourceObject("ErrorRepAction.Text1"));

                if (offenderName.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorRepAction.Text2"));
                }

                DateTime dtRepDate;
                //Jerry 2013-10-28 add time portion for EvidencePack.EPItemDate
                //if (!DateTime.TryParse(this.wdcRepDate.Text.Trim(), out dtRepDate))
                if (!DateTime.TryParse(this.wdcRepDate.Text.Trim() + " " + DateTime.Now.ToString("HH:mm:ss"), out dtRepDate))
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorRepAction.Text3"));
                }

                string details = this.txtRepDetails.Text.Trim();
                if (details.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorRepAction.Text4"));
                }

                if (!isValid)
                {
                    sb.Append("</ul>\n");
                    this.lblError.Text = sb.ToString();
                    this.lblError.Visible = true;
                    return;
                }
                this.lblError.Visible = false;

                this.repIntNo = this.ViewState["RepIntNo"] == null ? 0 : (int)this.ViewState["RepIntNo"];
                this.autIntNo = int.Parse(ddlSelectLA.SelectedValue);
                this.chgIntNo = (int)this.ViewState["ChgIntNo"];

                RepresentationDB represent = new RepresentationDB(connectionString);

                string errMsg = string.Empty;

                // jerry 2011-11-30 add push queue code
                QueueItem item;
                QueueItemProcessor queProcessor = new QueueItemProcessor();
                try
                {
                    using (TransactionScope scope = ServiceUtility.CreateTransactionScope())
                    {
                        int newID = represent.NewUpdateRepresentation(repIntNo, chgIntNo, txtRepDetails.Text, dtRepDate, offenderName, txtRepOffenderAddress.Text,
                            "", txtRepOfficial.Text, loginUser, this.autIntNo, csCode, originalAmount, "N", "N", ref errMsg, this.isSummons);

                        int result;
                        lblError.Visible = true;

                        if (this.repIntNo == 0 && newID > 0)
                        {
                            lblError.Text = (string)this.GetLocalResourceObject("lblError.Text7");
                            //Jerry 2012-05-25 change
                            //btnUpdate.Text = "Update";
                            btnUpdate.Text = ((string)this.GetLocalResourceObject("btnUpdate.Text1")).Trim();
                            this.repIntNo = newID;
                            ViewState["RepIntNo"] = newID;
                            // jerry 2011-11-30 add
                            item = new QueueItem();
                            item.Body = this.repIntNo.ToString();
                            item.QueueType = ServiceQueueTypeList.RepresentationGracePeriod_Notice;
                            item.ActDate = DateTime.Now.AddDays(noOfDaysGracePeriod);
                            item.Group = this.autCode;
                            queProcessor.Send(item);

                            // 20120807 Nick Add rule check for report engine
                            if (IsIBMPrinter())
                            {
                                result = represent.UpdateRepresentationForPrint(this.autIntNo, repIntNo, loginUser, repAction, this.isSummons, out errMsg);
                                if (result <= 0)
                                {
                                    lblError.Text = string.Format((string)GetLocalResourceObject("FailedInUpdatingRepresentationForPrint"), result, errMsg);
                                    return;
                                }
                            }

                            // 2012.09.12 Nick add for report punch statistics
                            //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                            result = punchStat.PunchStatisticsTransactionAdd(GetAutIntNo(), loginUser, PunchStatisticsTranTypeList.RepresentationRegister, PunchAction.Add);
                            // 2014-02-12, Oscar added returned value handling
                            if (result <= 0)
                            {
                                lblError.Text = string.Format(punchStat.ErrorMessage);
                                return;
                            }
                        }
                        else if (newID > 0)
                        // Indicate that the representation has been updated
                        {
                            lblError.Text = (string)this.GetLocalResourceObject("lblError.Text8");
                            //Response.Redirect(PAGE_URL_REGISTER);   

                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            PunchStatistics punchStatistics = new PunchStatistics(this.connectionString);
                            result = punchStatistics.PunchStatisticsTransactionAdd(GetAutIntNo(), this.loginUser, PunchStatisticsTranTypeList.Representation, PunchAction.Change);
                            // 2014-02-12, Oscar added returned value handling
                            if (result <= 0)
                            {
                                lblError.Text = string.Format(punchStatistics.ErrorMessage);
                                return;
                            }
                        }
                        // 2014-02-12, Oscar added "ELSE" for returned minus value
                        else
                        {
                            if (newID == -1)
                                lblError.Text = (string)this.GetLocalResourceObject("lblError.Text9");
                            else if (newID == -2)
                                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text10"), errMsg);
                            //Jerry 2012-11-01 add
                            else if (newID == -11)
                            {
                                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text21"), isSummons ? "Summons" : "Notice");
                            }
                            // 2014-02-12, Oscar added returned value handling
                            else if (newID == -3)
                            {
                                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text10"), GetLocalResourceObject("FailedInUpdatingNoticeChargeStatus"));
                            }
                            else
                            {
                                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text10"), newID);
                            }

                            return; // 2014-02-12, Oscar added returned value handling
                        }
                        //lblError.Visible = true;

                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                    lblError.Visible = true;
                    return;
                }
            }

            if (repAction == "D" || repAction == "O")
            {
                StringBuilder sb = new StringBuilder((string)this.GetLocalResourceObject("ErrorRepAction.Text1"));

                if (offenderName.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorRepAction.Text2"));
                }

                DateTime dtRepDate;
                //Jerry 2013-10-28 add time portion for EvidencePack.EPItemDate
                //if (!DateTime.TryParse(this.wdcRepDate.Text.Trim(), out dtRepDate))
                if (!DateTime.TryParse(this.wdcRepDate.Text.Trim() + " " + DateTime.Now.ToString("HH:mm:ss"), out dtRepDate))
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorRepAction.Text3"));
                }

                string details = this.txtRepDetails.Text.Trim();
                if (details.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorRepAction.Text4"));
                }

                if (!isValid)
                {
                    sb.Append("</ul>\n");
                    this.lblError.Text = sb.ToString();
                    this.lblError.Visible = true;
                    return;
                }
                this.lblError.Visible = false;

                this.repIntNo = (int)this.ViewState["RepIntNo"];
                this.autIntNo = int.Parse(ddlSelectLA.SelectedValue);
                this.chgIntNo = (int)this.ViewState["ChgIntNo"];
                string sChangeOfOffender = "N";

                RepresentationDB represent = new RepresentationDB(connectionString);
                if (chkChangeOffender.Checked)
                    sChangeOfOffender = "Y";
                string errMsg = string.Empty;

                //****************************************************************************************************************** Only affects Charge Status
                int newID = represent.NewUpdateRepresentation(repIntNo, chgIntNo, txtRepDetails.Text,
                    dtRepDate, offenderName, txtRepOffenderAddress.Text, "", txtRepOfficial.Text, loginUser,
                    this.autIntNo, csCode, originalAmount, sChangeOfOffender, "N", ref errMsg, this.isSummons);

                // Indicate that the representation has been updated
                //if (this.repIntNo > 0)
                if (newID > 0)
                {
                    lblError.Text = (string)this.GetLocalResourceObject("lblError.Text11");
                    //Response.Redirect("Representation.aspx?type=Decide");
                    //Jerry 2012-05-25 change
                    //btnUpdate.Text = "Update";
                    btnUpdate.Text = ((string)this.GetLocalResourceObject("btnUpdate.Text1")).Trim();
                }
                else if (newID == 0)
                    lblError.Text = (string)this.GetLocalResourceObject("lblError.Text12");
                else if (newID == -1)
                    lblError.Text = (string)this.GetLocalResourceObject("lblError.Text13");
                else if (newID == -2)
                    lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text14"), errMsg);
                //Jerry 2012-11-01 add
                else if (newID == -11)
                {
                    lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text21"), isSummons ? "Summons" : "Notice");
                }

                lblError.Visible = true;
            }
        }

        #region Removed by Oscar 20101021 - being used in nowhere
        //protected void grdCharges_ItemCommand(object source, DataGridCommandEventArgs e)
        //{
        //    if (e.CommandName == "Edit")
        //    {
        //        this.btnUpdate.Text = "Update";
        //        this.repIntNo = int.Parse(e.Item.Cells[0].Text);
        //        this.ViewState.Add("RepIntNo", this.repIntNo);
        //        this.txtReversalReason.Enabled = true;
        //    }


        //    this.chgIntNo = int.Parse(e.Item.Cells[1].Text);
        //    this.ViewState.Add("ChgIntNo", this.chgIntNo);
        //    GetRepresentationDetails();
        //    this.pnlRetrieve.Visible = false;
        //    this.pnlCharges.Visible = false;
        //}
        #endregion

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            this.hdnType_rep.Value = "0";
            this.hdnType_srep.Value = "0";
            this.hdnTicketNo.Value = string.Empty;
            //Jerry 2012-05-25 change
            //this.btnUpdate.Text = " Add ";
            this.btnUpdate.Text = ((string)this.GetLocalResourceObject("btnAdd.Text")).Trim();
            this.btnUpdate.Visible = true;

            //this.ViewState.Remove("IsSummons");
            this.ViewState.Remove("RepIntNo");
            //this.ViewState.Remove("ChgIntNo");
            this.ViewState.Remove("ChargeRowVersion");
            this.ViewState.Remove("ChargeStatus");

            this.txtReversalReason.Enabled = false;
            this.pnlRetrieve.Visible = false;
            this.pnlCharges.Visible = false;

            this.isNew = true;

            //Jerry 2012-11-01 check is Handwritten Correction
            bool isHandwrittenCorrection = CheckIsHandwrittenCorrection(Convert.ToInt32(ViewState["ChgIntNo"]), this.isSummons);
            if (isHandwrittenCorrection)
            {
                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text21"), isSummons ? "Summons" : "Notice");
                lblError.Visible = true;
                return;
            }

            //Jerry 2013-04-03 check is paid
            bool isPaid = CheckNoticeOrSummonsIsPaid(Convert.ToInt32(ViewState["ChgIntNo"]), this.isSummons);
            if (isPaid)
            {
                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text22"), isSummons ? "Summons" : "Notice");
                lblError.Visible = true;
                return;
            }

            this.GetRepresentationDetails();
        }

        protected void btnOptReport_Click(object sender, EventArgs e)
        {
            Helper_Web.BuildPopup(this, "Representation_ReportViewer.aspx", "RepIntNo", this.ViewState["RepIntNo"].ToString());
        }

        #region disabled by Oscar 20101018 for no longer use
        //protected void grdCharges_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (this.ViewState["RepIntNo"] == null)
        //        return;
        //    this.repIntNo = (int)this.ViewState["RepIntNo"];
        //}
        #endregion

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtSearch.Text = TicketNumberSearch1.TicketNumber;
            SearchRepresentations(false);
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int autIntNo = GetAutIntNo();    // changed by oscar 20101019
            this.TicketNumberSearch1.AutIntNo = autIntNo;

            this.noOfDaysBeforeCourt = this.CheckNoOfDaysBeforeCourt();

            ViewState.Add("noOfDaysBeforeCourt", noOfDaysBeforeCourt);
            ViewState.Add("AutIntNo", autIntNo);

            this.noOfDaysForPayment = this.GetNoOfDaysForPayment(autIntNo);
        }

        private int GetNoOfDaysForPayment(int autIntNo)
        {
            AuthorityRulesDetails arule = new AuthorityRulesDetails();
            arule.AutIntNo = autIntNo;
            arule.ARCode = "4660";
            arule.LastUser = this.loginUser;

            DefaultAuthRules paymentRule = new DefaultAuthRules(arule, this.connectionString);
            KeyValuePair<int, string> payment = paymentRule.SetDefaultAuthRule();

            ViewState["noOfDaysForPayment"] = payment.Key;
            return payment.Key;
        }

        private int CheckNoOfDaysBeforeCourt()
        {
            //AuthorityRulesDB authRules = new AuthorityRulesDB(connectionString);
            //AuthorityRulesDetails ardet = authRules.GetAuthorityRulesDetailsByCode(this.autIntNo, "4050", "Rule to indicate no. of days before court date that a representation may be logged", 10, "N", "Representations may not be logged after a certain no. of days before court date", "TPExInt");
            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here

            AuthorityRulesDetails ardet = new AuthorityRulesDetails();
            ardet.AutIntNo = this.autIntNo;
            ardet.ARCode = "4050";
            ardet.LastUser = this.loginUser;

            DefaultAuthRules authRule = new DefaultAuthRules(ardet, this.connectionString);
            KeyValuePair<int, string> courtDateRule = authRule.SetDefaultAuthRule();

            return courtDateRule.Key;
        }

        protected void ShowChangeOfOffender()
        {
            this.pnlChangeOfOffender.Visible = this.chkChangeOffender.Checked;
            pnlLetterTo.Visible = this.chkChangeOffender.Checked;

            // Oscar 20101104 - added, S56 is not allowed to change offender.
            if (GetNoticeFilmType() == NoticeFilmType.M)
            {
                this.chkChangeOffender.Checked = false;
                this.pnlLetterTo.Visible = false;
            }
        }

        protected void chkChangeOffender_onChecked(object sender, EventArgs e)
        {
            this.ShowChangeOfOffender();

            if (chkChangeOffender.Checked)
                pnlChangeOfOffender.Enabled = true;

            // Oscar 2013-06-05 added
            LockRevAmountForWithdrawn();
        }

        protected void ReverseRep(bool insufficientDetails)
        {
            //Jerry 2012-11-01 check is Handwritten Correction
            bool isHandwrittenCorrection = CheckIsHandwrittenCorrection(Convert.ToInt32(ViewState["ChgIntNo"]), this.isSummons);
            if (isHandwrittenCorrection)
            {
                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text21"), isSummons ? "Summons" : "Notice");
                lblError.Visible = true;
                return;
            }

            //Jerry 2013-04-03 check is paid
            bool isPaid = CheckNoticeOrSummonsIsPaid(Convert.ToInt32(ViewState["ChgIntNo"]), this.isSummons);
            if (isPaid)
            {
                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text22"), isSummons ? "Summons" : "Notice");
                lblError.Visible = true;
                return;
            }

            bool isValid = true;
            //int csCode = CODE_REP_PENDING;         //use for updates
            StringBuilder sb = new StringBuilder((string)this.GetLocalResourceObject("ErrorRepAction.Text1"));

            string reverseReason = string.Empty;

            if (insufficientDetails)
                reverseReason = (string)this.GetLocalResourceObject("ErrorRepAction.Text6");
            else
                reverseReason = this.txtReversalReason.Text.Trim();

            if (reverseReason.Length == 0)
            {
                isValid = false;
                sb.Append((string)this.GetLocalResourceObject("ErrorRepAction.Text7"));
            }

            if (!isValid)
            {
                sb.Append("</ul>\n");
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return;
            }

            this.repIntNo = (int)this.ViewState["RepIntNo"];
            this.autIntNo = int.Parse(ddlSelectLA.SelectedValue);
            this.chgIntNo = (int)this.ViewState["ChgIntNo"];
            int rcCode = (int)this.ViewState["RCCode"];

            string drvIDNumber = string.Empty;

            //Barry Dickson 20080313 Need to move updating of Driver and persona strored procs into the reversal stored proc below so that it can all be handled
            //in a transaction. Will need to pass the if statement variables into the stored proc so that the checks can be done to if the driver needs updating.

            int sumIntNo = 0;
            int srIntNo = 0;
            int notIntNo = (int)this.ViewState["NotIntNo"];
            int drvIntNo = 0;

            DriverDetails driver = null;
            OwnerDetails owner = null;

            repIntNo = (int)this.ViewState["RepIntNo"];

            string errMessage = string.Empty;

            //they want to roll back the rep if it is a summons
            RepresentationDB rep = new RepresentationDB(connectionString);

            if (this.isSummons && insufficientDetails)
            {
                srIntNo = rep.ReverseSummonsLoggedRep(repIntNo, loginUser, ref errMessage);

                if (srIntNo < 1)
                {
                    lblError.Visible = true;
                    lblError.Text = (string)this.GetLocalResourceObject("ErrorMsgReverseSummon.Text1");

                    switch (srIntNo)
                    {
                        case -1:
                            errMessage = (string)this.GetLocalResourceObject("ErrorMsgReverseSummon.Text2");
                            break;
                        case -2:
                            errMessage = (string)this.GetLocalResourceObject("ErrorMsgReverseSummon.Text3");
                            break;
                        case -3:
                            errMessage = (string)this.GetLocalResourceObject("ErrorMsgReverseSummon.Text4");
                            break;
                        case -4:
                            errMessage = (string)this.GetLocalResourceObject("ErrorMsgReverseSummon.Text5");
                            break;
                    }

                    lblError.Text += errMessage;
                    return;
                }
                else
                {
                    #region Oscar 20121016 disabled
                    //if (IsIBMPrinter())
                    //{
                    //    rep.UpdateRepresentationForPrint(this.autIntNo, srIntNo, loginUser, "I", this.isSummons);
                    //    this.lblError.Visible = true;
                    //    this.lblError.Text = (string)GetLocalResourceObject("errorMsg");
                    //    GoBackToBeginning(true);
                    //}
                    //else
                    //{
                    //    // 2011-09-30 jerry add
                    //    if (isPrintBatchRepLetter)
                    //    {
                    //        //set the RepPrintFileName
                    //        // jerry 2012-03-08 set PrintFileName = RepInsufficentDecision_AutCode_DateTime
                    //        //repPrintFileName = "RepInsufficentDecision_" + DateTime.Now.ToString("yyyy-MM-dd");
                    //        repPrintFileName = "RepInsufficentDecision_" + this.autCode + "_" + DateTime.Now.ToString("yyyy-MM-dd");
                    //        rep.RepresentationUpdateForPrintBatch(true, this.isSummons, srIntNo, this.autIntNo, false, false, this.loginUser, repPrintFileName);

                    //        Session["ShowBlockUI"] = true;
                    //        string tempLetterTo = this.isSummons ? "S" : rdlLetter.SelectedValue;
                    //        this.thisPageURL = this.thisPageURL + "&ShowBlockUI=YES&LetterType=InsufficientDetails&RepIntNo=" + srIntNo.ToString() + "&TicketNo=" + this.txtTicketNo.Text.Trim() + "&LetterTo=" + tempLetterTo;

                    //        Response.Redirect(this.thisPageURL, true);
                    //    }
                    //    else
                    //    {
                    //        //2011-09-30 jerry print one file at a time.
                    //        rep.RepresentationUpdateForPrintBatch(false, this.isSummons, srIntNo, this.autIntNo, true, true, this.loginUser, null);

                    //        PopupTheRepresentationLetterViewer("InsufficientDetails", this.repIntNo.ToString(), txtTicketNo.Text.ToString(), this.isSummons ? "S" : rdlLetter.SelectedValue);
                    //    }
                    //}
                    #endregion

                    //Oscar 20131016 changed
                    PostRepLetter(false);

                    return;
                }
            }
            else if (chkChangeOffender.Checked)
            {
                if (this.isSummons)
                {
                    sumIntNo = (int)this.ViewState["SumIntNo"];
                    srIntNo = (int)this.ViewState["RepIntNo"];

                    // Get the current accused
                    AccusedDB db = new AccusedDB(this.connectionString);
                    driver = db.GetAccusedBySumIntNo(sumIntNo);
                }
                else
                {
                    //int notIntNo = (int)this.ViewState["NotIntNo"];

                    // dls 080326 - we need to check if there is a proxy for this notice, in which case, we will need to change the NotSendTo flag to 'P' instead of 'D'
                    //            - we will still copy the owner details back over the driver, as that is the way it was before.

                    // Get the owner
                    OwnerDB ownerdb = new OwnerDB(connectionString);
                    owner = ownerdb.GetOwnerDetailsByNotice(notIntNo);

                    // Get the current driver
                    DriverDB driverDb = new DriverDB(connectionString);
                    driver = driverDb.GetDriverDetailsByNotice(notIntNo);
                    drvIntNo = driver.DrvIntNo;
                    drvIDNumber = driver.DrvIDNumber;
                }
            }

            RepresentationDB represent = new RepresentationDB(connectionString);

            List<Int32> result = represent.ReverseRepresentation(repIntNo, chgIntNo, reverseReason, loginUser, repAction,
                this.isSummons, owner, drvIntNo, notIntNo, drvIDNumber, chkChangeOffender.Checked, ref errMessage, dataWashingActived, useRepAddress, repAddrExpiryPeriod);

            this.lblError.Visible = true;

            // Indicate that the representation has been updated
            if (result[0] <= 0)
            {
                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text15"), errMessage);
            }
            else
            {
                if (!this.isSummons)
                {
                    SIL.AARTO.DAL.Entities.Notice notice = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(notIntNo);
                    if (notice.NoticeStatus != 410)
                    {
                        PushGenerateSummonsQueue(repIntNo, notIntNo, true);
                    }
                }

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(GetAutIntNo(), this.loginUser, PunchStatisticsTranTypeList.Representation, PunchAction.Change);
                if (insufficientDetails)
                {
                    #region Oscar 20121016 disabled
                    //if (IsIBMPrinter())
                    //{
                    //    rep.UpdateRepresentationForPrint(this.autIntNo, repIntNo, loginUser, "I", this.isSummons);
                    //    this.lblError.Visible = true;
                    //    this.lblError.Text = (string)GetLocalResourceObject("errorMsg");
                    //}
                    //else
                    //{
                    //    // 2011-09-30 jerry add
                    //    if (isPrintBatchRepLetter)
                    //    {
                    //        //set the RepPrintFileName
                    //        // jerry 2012-03-08 set PrintFileName = RepInsufficentDecision_AutCode_DateTime
                    //        //repPrintFileName = "RepInsufficentDecision_" + DateTime.Now.ToString("yyyy-MM-dd");
                    //        repPrintFileName = "RepInsufficentDecision_" + this.autCode + "_" + DateTime.Now.ToString("yyyy-MM-dd");
                    //        rep.RepresentationUpdateForPrintBatch(true, this.isSummons, this.repIntNo, this.autIntNo, false, false, this.loginUser, repPrintFileName);

                    //        Session["ShowBlockUI"] = true;
                    //        string tempLetterTo = this.isSummons ? "S" : rdlLetter.SelectedValue;
                    //        this.thisPageURL = this.thisPageURL + "&ShowBlockUI=YES&LetterType=InsufficientDetails&RepIntNo=" + this.repIntNo.ToString() + "&TicketNo=" + this.txtTicketNo.Text.Trim() + "&LetterTo=" + tempLetterTo;

                    //        Response.Redirect(this.thisPageURL, true);
                    //    }
                    //    else
                    //    {
                    //        //2011-09-30 jerry print one file at a time.
                    //        rep.RepresentationUpdateForPrintBatch(false, this.isSummons, this.repIntNo, this.autIntNo, true, true, this.loginUser, null);

                    //        PopupTheRepresentationLetterViewer("InsufficientDetails", this.repIntNo.ToString(), txtTicketNo.Text.ToString(), this.isSummons ? "S" : rdlLetter.SelectedValue);
                    //    }
                    //}
                    #endregion

                    // Oscar 20121016 changed
                    PostRepLetter(false);

                }
                else
                    lblError.Text = (string)this.GetLocalResourceObject("lblError.Text16");

                GoBackToBeginning(true);
            }
        }

        protected void btnReverse_Click(object sender, EventArgs e)
        {
            ReverseRep(false);
        }

        // Oscar 20101019 - rewrite
        protected void grdCharges_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;
            DataRowView row = (DataRowView)e.Row.DataItem;
            if (row == null)
            {
                //Jerry 2013-04-16 add
                KeyValue<int, bool> cell;
                if (ViewState["PageCache"] != null
                    && (cache = (PageCache)ViewState["PageCache"]).GrdChg_Select != null
                    && (cell = cache.GrdChg_Select.FirstOrDefault(r => r.Key == e.Row.RowIndex)) != null)
                {
                    e.Row.Cells[6].Enabled = cell.Value;
                }
                return;
            }

            #region
            //bool isAbleToSelect;
            //string msg;
            //IsAbleToEditRepPara para = new IsAbleToEditRepPara();
            //para.ChgType = Convert.ToString(row["IsMainCharge"]);

            //////dls 2011-04-05 - need to check the served status if the court date has already past
            ////if (int.Parse(row["SumServedStatus"].ToString()) == 1 || int.Parse(row["SumServedStatus"].ToString()) == 2)
            ////    para.IsServed = true;
            ////else
            ////    para.IsServed = false;

            //IsAbleToEditRep(para, out isAbleToSelect, out msg);

            //if (!isAbleToSelect)
            //{
            //    //LinkButton lnkbtn = (LinkButton)e.Row.Cells[6].FindControl("btnSelect");
            //    //lnkbtn.Enabled = false;
            //    e.Row.Cells[6].Enabled = false;
            //}
            #endregion

            // Oscar 20121224 added
            if (!CanChargeSelect(row["IsMainCharge"].ToString()))
                e.Row.Cells[6].Enabled = false;

            //Jerry 2013-04-16 add
            if (cache.GrdChg_Select == null)
                cache.GrdChg_Select = new List<KeyValue<int, bool>>();

            var currentCell = cache.GrdChg_Select.FirstOrDefault(r => r.Key == e.Row.RowIndex);
            if (currentCell == null)
                cache.GrdChg_Select.Add(new KeyValue<int, bool>(e.Row.RowIndex, e.Row.Cells[6].Enabled));
            else
                currentCell.Value = e.Row.Cells[6].Enabled;
        }

        #region Oscar 20101019 - disable for backup
        //protected void grdCharges_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    System.Data.Common.DbDataRecord s = (System.Data.Common.DbDataRecord)e.Row.DataItem;
        //    bool openRepExists = false;
        //    bool isSummons = false;
        //    DateTime courtDate = DateTime.MaxValue;
        //    DateTime dt;

        //    //have removed contents of cell 6 (used to be documentation)
        //    if (s != null)
        //    {
        //        int repIntNo = Convert.ToInt32(s["RepIntNo"].ToString());
        //        isSummons = Convert.ToBoolean(s["IsSummons"]);

        //        object dtObject = s["SumCourtDate"];
        //        if (dtObject != null && DateTime.TryParse(dtObject.ToString(), out dt))
        //            courtDate = dt;

        //        // check if we are in Register or Decide mode
        //        if (this.repAction == "R")
        //        {
        //            if (s["RCCode"] != DBNull.Value)
        //            {
        //                // This indicates that we can capture a new Representation
        //                int rcCode = Convert.ToInt32(s["RCCode"].ToString());
        //                bool isLocked = !s["LockedFlag"].Equals("N");

        //                if (rcCode > REPCODE_NONE)
        //                {
        //                    e.Row.Cells[4].Text = s["RCDescr"].ToString();
        //                }
        //                else
        //                {
        //                    e.Row.Cells[4].Text = s["CSDescr"].ToString();
        //                    openRepExists = true;
        //                }

        //                if (!isLocked && repIntNo > 0)
        //                {
        //                    switch (rcCode)
        //                    {
        //                        case REPCODE_NONE:
        //                        case SUMMONS_REPCODE_NONE:
        //                            e.Row.Cells[6].Enabled = true;
        //                            break;
        //                        default:
        //                            e.Row.Cells[6].Enabled = false;
        //                            break;
        //                    }
        //                }
        //                else
        //                    e.Row.Cells[6].Enabled = false;
        //            }
        //            else
        //            {
        //                e.Row.Cells[4].Text = s["CSDescr"].ToString();
        //                e.Row.Cells[6].Enabled = false;
        //            }

        //            if (repIntNo != 0)
        //                noOfReps += 1;

        //        }

        //        if (repAction == "D" || repAction == "O")
        //        {
        //            if (s["RCCode"] != DBNull.Value)
        //            {
        //                int rcCode = Convert.ToInt32(s["RCCode"].ToString());
        //                bool isLocked = s["LockedFlag"].Equals("Y");
        //                isSummons = Convert.ToBoolean(s["IsSummons"]);

        //                //mrs 2008-07-07 disabled the insufficient detail button for summons rep
        //                if (isSummons && repAction == "D")
        //                {
        //                    btnInsufficientDetails.Enabled = false;
        //                }

        //                dtObject = s["SumCourtDate"];
        //                if (dtObject != null && DateTime.TryParse(dtObject.ToString(), out dt))
        //                    courtDate = dt;

        //                if (!isLocked && repIntNo > 0)
        //                {
        //                    if (rcCode == REPCODE_NONE)
        //                    {
        //                        e.Row.Cells[6].Enabled = true;
        //                        openRepExists = true;
        //                        e.Row.Cells[4].Text = s["CSDescr"].ToString();
        //                    }
        //                    else if (rcCode == SUMMONS_REPCODE_NONE)
        //                    {
        //                        e.Row.Cells[6].Enabled = true;
        //                        openRepExists = true;
        //                        e.Row.Cells[4].Text = s["RCDescr"].ToString();
        //                    }
        //                    else
        //                    {
        //                        e.Row.Cells[6].Enabled = (rcCode != REPCODE_CANCELLED);
        //                        e.Row.Cells[4].Text = s["RCDescr"].ToString();
        //                    }
        //                }
        //                else
        //                {
        //                    e.Row.Cells[6].Enabled = false;
        //                    e.Row.Cells[4].Text = s["RCDescr"].ToString();
        //                }
        //            }
        //            else
        //            {
        //                e.Row.Cells[4].Text = s["CSDescr"].ToString();
        //                e.Row.Cells[6].Enabled = false;
        //            }

        //            if (repIntNo != 0)
        //                noOfReps += 1;

        //        }

        //        //mrs 20080728 added columns to dataset
        //        int sumServed = Convert.ToInt16(s["SumServedStatus"].ToString());
        //        string sumCaseNo = s["SumCaseNo"].ToString();

        //        ViewState.Add("RepIntNo", repIntNo);
        //        ViewState.Add("ChargeStatus", Convert.ToInt32(s["ChargeStatus"].ToString()));
        //        ViewState.Add("ChargeRowVersion", Convert.ToInt64(s["ChargeRowVersion"].ToString()));
        //        ViewState.Add("NotIntNo", Convert.ToInt32(s["NotIntNo"].ToString()));
        //        ViewState.Add("ChgIntNo", Convert.ToInt32(s["ChgIntNo"].ToString()));
        //        ViewState.Add("IsSummons", isSummons);
        //        ViewState.Add("SumServedStatus", sumServed);
        //        ViewState.Add("SumCaseNo", sumCaseNo);

        //        bool served = false;
        //        if (sumServed == 1 || sumServed == 2)
        //        {
        //            //served
        //            served = true;
        //        }

        //        //dls 080617 - also need to check the no. of days before court date
        //        //mrs 20080728 if the summons has not been served or there is no case number it is pointless testing for no of days before court
        //        if (served || sumCaseNo != "None")
        //        {
        //            if (isSummons && DateTime.Today.AddDays(noOfDaysBeforeCourt) > courtDate)
        //            {
        //                if (DateTime.Today > courtDate)
        //                {
        //                    btnAdd.Visible = false;
        //                    //mrs 2008-07-07 added message
        //                    lblError.Text = "A representation cannot be raised on this notice as the court date " + courtDate.ToString(DATE_FORMAT) + " has already passed";
        //                    lblError.Visible = true;
        //                }
        //                else
        //                {
        //                    btnAdd.Visible = false;
        //                    //mrs 2008-07-07 added message
        //                    lblError.Text = "A representation cannot be raised on this notice due to the court date " + courtDate.ToString(DATE_FORMAT) + " being less than " + noOfDaysBeforeCourt.ToString() + " days from today";
        //                    lblError.Visible = true;
        //                }
        //            }
        //        }

        //        //only show the add button if there are no open reps
        //        if (repAction == "R" || allowAllInOne)
        //            btnAdd.Visible = !openRepExists;
        //    }
        //}
        #endregion

        // Oscar 20101019 - added
        //protected void grdCharges_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    this.grdCharges.SelectedIndex = e.NewEditIndex;
        //    GridViewRow row = this.grdCharges.Rows[e.NewEditIndex];
        //    string courtDate = row.Cells[5].Text;
        //    //string chgIntNo = row.Cells[0].Text;
        //    string chgIntNo = this.grdCharges.DataKeys[e.NewEditIndex][0].ToString();
        //    Int32.TryParse(chgIntNo, out this.chgIntNo);
        //    ViewState["ChgIntNo"] = this.chgIntNo;

        //    DateTime dtCourtDate;
        //    DateTime.TryParse(courtDate, out dtCourtDate);
        //    if (dtCourtDate == DateTime.MinValue) dtCourtDate = DateTime.MaxValue;
        //    //ViewState["CourtDate"] = dtCourtDate;

        //    grdRepresentationBind(this.chgIntNo, dtCourtDate);
        //}
        protected void grdCharges_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            this.grdCharges.SelectedIndex = e.NewSelectedIndex;
            GridViewRow row = this.grdCharges.Rows[e.NewSelectedIndex];
            string courtDate = row.Cells[5].Text;
            //string chgIntNo = row.Cells[0].Text;
            string chgIntNo = this.grdCharges.DataKeys[e.NewSelectedIndex]["ChgIntNo"].ToString();
            Int32.TryParse(chgIntNo, out this.chgIntNo);
            ViewState["ChgIntNo"] = this.chgIntNo;

            // 2014-02-12, Oscar added "IsReadOnly" for existing Charge Rep
            cache.IsReadOnly = Convert.ToBoolean(this.grdCharges.DataKeys[e.NewSelectedIndex]["IsReadOnly"]);

            DateTime dtCourtDate;
            DateTime.TryParse(courtDate, out dtCourtDate);
            if (dtCourtDate == DateTime.MinValue) dtCourtDate = DateTime.MaxValue;
            //ViewState["CourtDate"] = dtCourtDate;

            // Oscar 2013-05-23 added checking RequireNoAOGAlert
            cache.CourtDate = dtCourtDate;
            if (!ShowNoAOGAlert())
                grdRepresentationBind(this.chgIntNo, dtCourtDate);
        }


        #region Oscar 20101019 - disable for backup
        //protected void grdCharges_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    this.grdCharges.SelectedIndex = e.NewEditIndex;
        //    GridViewRow row = this.grdCharges.Rows[e.NewEditIndex];
        //    this.btnUpdate.Text = "Update";

        //    this.repIntNo = int.Parse(row.Cells[0].Text);
        //    this.chgIntNo = int.Parse(row.Cells[1].Text);

        //    this.txtReversalReason.Enabled = true;

        //    this.ViewState.Add("RepIntNo", this.repIntNo);
        //    this.ViewState.Add("ChgIntNo", this.chgIntNo);

        //    this.GetRepresentationDetails();

        //    this.pnlRetrieve.Visible = false;
        //    this.pnlCharges.Visible = false;
        //}
        #endregion

        // Oscar 20101103 - added
        private void grdRepresentationBind(int chgIntNo, DateTime courtDate)
        {
            // Oscar 20120813 added for authorise 2nd rep
            //this.btnAdd.Enabled = true;

            //this.grdCharges.Dispose();
            this.lblError.Visible = false;
            this.grdCharges.Visible = false;
            this.grdRepresentation.Visible = true;

            DataView source = GetRepresentationSource(GetSearchRepresentationsSource(), Convert.ToInt32(chgIntNo), null);

            // Oscar 20130208 added for status check.
            cache.CurrentChargeStatus = null;
            cache.LastRecommendDate = null;
            if (source.Count > 0)
            {
                var objChargeStatus = source[0]["ChargeStatus"];
                int chargeStatus;
                if (objChargeStatus != null && int.TryParse(objChargeStatus.ToString(), out chargeStatus))
                {
                    cache.CurrentChargeStatus = chargeStatus;
                }

                foreach (DataRowView dv in source)
                {
                    var objRecommendDate = dv["RepRecommendDate"];
                    DateTime recommendDate;
                    if (objRecommendDate != null
                        && objRecommendDate != DBNull.Value
                        && DateTime.TryParse(objRecommendDate.ToString(), out recommendDate))
                    {
                        cache.LastRecommendDate = recommendDate;
                        break;
                    }
                }
            }

            //Jerry 2013-04-16 add
            cache.GrdRep_Select = null;

            this.grdRepresentation.DataSource = source;
            this.grdRepresentation.DataKeyNames = new string[] { "ChgIntNo" };
            this.grdRepresentation.DataBind();

            if (source.Count > 0)
            {
                this.lblEmptyData.Visible = false;
                this.btnReport.Visible = true;
            }
            else
            {
                this.lblEmptyData.Text = emptyMsg;
                this.lblEmptyData.Visible = true;
                this.btnReport.Visible = false;
            }

            #region
            //bool isAbleToAdd;
            //string msg;

            //IsAbleToEditRepPara para = new IsAbleToEditRepPara();
            //para.CourtDate = courtDate;
            //para.IsSummons = Convert.ToBoolean(ViewState["IsSummons"]);

            //////dls 2011-04-05 - need to check the served status if the court date has already past
            ////if (int.Parse(ViewState["SumServedStatus"].ToString()) == 1 || int.Parse(ViewState["SumServedStatus"].ToString()) == 2)
            ////    para.IsServed = true;
            ////else
            ////    para.IsServed = false;

            //IsAbleToEditRep(para, out isAbleToAdd, out msg);
            //if (!isAbleToAdd)
            //{
            //    this.btnAdd.Visible = false;
            //    if (source.Count == 0)
            //    {
            //        this.lblError.Visible = true;
            //        this.lblError.Text = string.IsNullOrEmpty(msg) ? string.Format((string)this.GetLocalResourceObject("lblError.Text17"),Convert.ToDateTime(courtDate).ToString(DATE_FORMAT)) : msg;
            //    }
            //}
            //else
            //{
            //    if (source.Count == 0)
            //        this.btnAdd.Visible = true;
            //    if (this.lblEmptyData.Visible && source.Count == 0)
            //        this.lblEmptyData.Text += addMsg;
            //}

            //// Oscar 20120813 added for authorise 2nd rep 
            //if (this.btnAdd.Visible
            //    && !CheckManagementOverridePermission()
            //    && GetDecidedRepresentationCount(chgIntNo) != 0)
            //    this.btnAdd.Enabled = false;
            #endregion

            // Oscar 20121214 added
            this.btnAdd.Visible = true;


            string msg;
            if (!CanRepAdd(courtDate, cache.IsSummons.Value, out msg))
            {
                this.btnAdd.Visible = false;

                if (this.grdRepresentation.Rows.Count == 0)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = string.IsNullOrEmpty(msg) ? string.Format((string)this.GetLocalResourceObject("lblError.Text17"), Convert.ToDateTime(courtDate).ToString(DATE_FORMAT)) : msg;
                }
            }
            else if (this.lblEmptyData.Visible && source.Count == 0)
                this.lblEmptyData.Text += addMsg;

            if (this.btnAdd.Visible
                && !CheckManagementOverridePermission()
                && GetDecidedRepresentationCount(chgIntNo) != 0
                && !CheckRecommendationDateIn24Hours(cache.LastRecommendDate)
                )
            {
                this.btnAdd.Enabled = false;
            }
        }

        // Oscar 20101020 - added
        protected void grdRepresentation_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;
            DataRowView row = (DataRowView)e.Row.DataItem;
            if (row == null)
            {
                //Jerry 2013-04-16 add
                KeyValue<int, bool> cell;
                if (ViewState["PageCache"] != null
                    && (cache = (PageCache)ViewState["PageCache"]).GrdRep_Select != null
                    && (cell = cache.GrdRep_Select.FirstOrDefault(r => r.Key == e.Row.RowIndex)) != null)
                {
                    e.Row.Cells[6].Enabled = cell.Value;
                }
                return;
            }

            #region
            //// Oscar 20120813 added for authorise 2nd rep
            //this.btnAdd.Enabled = true;

            //bool isAbleToAdd;
            //string msg;
            //IsAbleToEditRepPara para = new IsAbleToEditRepPara();
            //para.Row = row;

            //if (!IsAbleToEditRep(para, out isAbleToAdd, out msg))
            //{
            //    e.Row.Cells[6].Enabled = false;
            //}
            //if (e.Row.DataItemIndex == 0)
            //{
            //    if (!isAbleToAdd)
            //    {
            //        this.btnAdd.Visible = false;
            //        this.lblError.Visible = true;
            //        this.lblError.Text = msg;
            //    }
            //    else
            //    {
            //        this.btnAdd.Visible = true;
            //        this.lblError.Visible = false;
            //    }
            //}

            //// Oscar 20120813 added for authorise 2nd rep
            //if (this.btnAdd.Visible
            //    && !CheckManagementOverridePermission()
            //    && GetDecidedRepresentationCount(chgIntNo) != 0)
            //    this.btnAdd.Enabled = false;
            #endregion

            // Oscar 20121214 added
            this.lblError.Visible = false;

            string msg;
            if (!CanRepEdit(row, out msg))
            {
                e.Row.Cells[6].Enabled = false;
                if (!string.IsNullOrWhiteSpace(msg))
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = msg;
                }
            }

            //Jerry 2013-04-16 add
            if (cache.GrdRep_Select == null)
                cache.GrdRep_Select = new List<KeyValue<int, bool>>();

            var currentCell = cache.GrdRep_Select.FirstOrDefault(r => r.Key == e.Row.RowIndex);
            if (currentCell == null)
            {
                cache.GrdRep_Select.Add(new KeyValue<int, bool>(e.Row.RowIndex, e.Row.Cells[6].Enabled));
            }
            else
                currentCell.Value = e.Row.Cells[6].Enabled;
        }

        // Oscar 20101021 - added
        //protected void grdRepresentation_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    this.grdRepresentation.SelectedIndex = e.NewEditIndex;
        //    GridViewRow row = this.grdRepresentation.Rows[e.NewEditIndex];
        //    this.btnUpdate.Text = "Update";

        //    this.repIntNo = Convert.ToInt32(row.Cells[1].Text);
        //    //this.chgIntNo = Convert.ToInt32(row.Cells[1].Text);
        //    this.chgIntNo = Convert.ToInt32(this.grdRepresentation.DataKeys[e.NewEditIndex][0]);

        //    this.txtReversalReason.Enabled = true;

        //    // ViewStateAdd
        //    ViewState["RepIntNo"] = this.repIntNo;
        //    ViewState["ChgIntNo"] = this.chgIntNo;

        //    GetRepresentationDetails();

        //    this.pnlRetrieve.Visible = false;
        //    this.pnlCharges.Visible = false;

        //    if (isSummons && repAction == "D")
        //    {
        //        btnInsufficientDetails.Enabled = false;
        //    }

        //}
        protected void grdRepresentation_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            this.grdRepresentation.SelectedIndex = e.NewSelectedIndex;
            GridViewRow row = this.grdRepresentation.Rows[e.NewSelectedIndex];
            //Jerry 2012-05-25 change
            //this.btnUpdate.Text = "Update";
            this.btnUpdate.Text = ((string)this.GetLocalResourceObject("btnUpdate.Text1")).Trim();

            this.repIntNo = Convert.ToInt32(row.Cells[1].Text);
            //this.chgIntNo = Convert.ToInt32(row.Cells[1].Text);
            this.chgIntNo = Convert.ToInt32(this.grdRepresentation.DataKeys[e.NewSelectedIndex][0]);

            this.txtReversalReason.Enabled = true;

            // ViewStateAdd
            ViewState["RepIntNo"] = this.repIntNo;
            ViewState["ChgIntNo"] = this.chgIntNo;

            GetRepresentationDetails();

            this.pnlRetrieve.Visible = false;
            this.pnlCharges.Visible = false;

            if (isSummons && repAction == "D")
            {
                btnInsufficientDetails.Enabled = false;
            }
        }


        protected void btnInsufficientDetails_Click(object sender, EventArgs e)
        {
            ReverseRep(true);
        }

        protected void btnDecide_Click(object sender, EventArgs e)
        {
            //Jerry 2012-11-01 check is Handwritten Correction
            bool isHandwrittenCorrection = CheckIsHandwrittenCorrection(Convert.ToInt32(ViewState["ChgIntNo"]), this.isSummons);
            if (isHandwrittenCorrection)
            {
                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text21"), isSummons ? "Summons" : "Notice");
                lblError.Visible = true;
                return;
            }

            //Jerry 2013-04-03 check is paid
            bool isPaid = CheckNoticeOrSummonsIsPaid(Convert.ToInt32(ViewState["ChgIntNo"]), this.isSummons);
            if (isPaid)
            {
                lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text22"), isSummons ? "Summons" : "Notice");
                lblError.Visible = true;
                return;
            }

            //Oscar 2013-05-24 added
            if (InvalidNoAOGOffenceInput(false)) return;

            //Barry Dickson 20080312 for the upgrade of reprinting notices, going through code and stored procs to check which functions need to be moved into the 
            //representationdecide stored proc. This will be based on what stored procs are updating the fields needed for reprinting of notices.
            //Have indicated my comments and changes with ********
            bool isValid = true;
            //int chargeStatus = 450;    
            int chargeStatus = 410;    // Oscar 20101027 - changed, 450 no longer used

            StringBuilder sb = new StringBuilder((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text1"));
            int rcCode = Convert.ToInt32(this.rblCodes.SelectedValue);

            if (chkChangeOffender.Checked)
            {
                if (rcCode == REPCODE_NONE)
                    rcCode = REPCODE_CHANGEOFFENDER;
                else if (rcCode == SUMMONS_REPCODE_NONE)      //dls 071009 - to cater for where they are only changing the offender for a summons
                    rcCode = SUMMONS_REPCODE_CHANGEOFFENDER;
            }

            DateTime dtRepDate;

            if (!DateTime.TryParse(wdcRepDate.Text, out dtRepDate))
            {
                sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text2"));
                isValid = false;
            }

            this.repIntNo = (int)this.ViewState["RepIntNo"];
            this.autIntNo = int.Parse(ddlSelectLA.SelectedValue);
            this.chgIntNo = (int)this.ViewState["ChgIntNo"];
            //this.isNoAOG = (bool)this.ViewState["IsNoAOG"];
            this.isNoAOG = Convert.ToBoolean(ViewState["IsNoAOG"]);

            //BD: Need to set the ticket no so that we can pass it through to the letter
            string ticketNo = this.txtTicketNo.Text.Trim();

            Int64 chargeRowVersion = Convert.ToInt64(this.ViewState["ChargeRowVersion"]);

            string official = this.txtRepOfficial.Text.Trim();
            decimal originalAmount = decimal.Parse(this.txtOriginalAmount.Text);
            string offenderName = this.txtRepOffenderName.Text.Trim();
            // 2012.09.12 Nick add for report punch statistics
            PunchStatisticsTranTypeList punchStatType = PunchStatisticsTranTypeList.RepresentationResultFineReduced;

            switch (rcCode)
            {
                case 1:
                case REPCODE_WITHDRAW:
                    //mrs 20080910 Nilesh found error, sending 99 to chargeStatus
                    //chargeStatus = REPCODE_CANCELLED;
                    chargeStatus = CODE_CASE_CANCELLED;
                    // 2012.09.12 Nick add for report punch statistics
                    punchStatType = PunchStatisticsTranTypeList.RepresentationResultWithdrawn;
                    break;
                case REPCODE_REDUCTION:
                    punchStatType = PunchStatisticsTranTypeList.RepresentationResultFineReduced;
                    break;
                case REPCODE_NOCHANGE:
                    punchStatType = PunchStatisticsTranTypeList.RepresentationResultProceed;
                    break;
                case REPCODE_CHANGEOFFENDER:
                    punchStatType = PunchStatisticsTranTypeList.ChangeOfOffender;
                    break;
                case SUMMONS_REPCODE_REDUCTION:
                    punchStatType = PunchStatisticsTranTypeList.RepresentationResultFineReduced;
                    break;
                case SUMMONS_REPCODE_NOCHANGE:
                    //case SUMMONS_REPCODE_CHANGEOFFENDER:
                    chargeStatus = (int)ViewState["ChargeStatus"];
                    punchStatType = PunchStatisticsTranTypeList.RepresentationResultProceed;
                    break;
                //dls 070929 - added new status values for summons rep
                case SUMMONS_REPCODE_WITHDRAW:
                    chargeStatus = CODE_SUMMONS_WITHDRAWN_CHARGE;
                    punchStatType = PunchStatisticsTranTypeList.RepresentationResultWithdrawn;
                    break;
                case SUMMONS_REPCODE_REISSUE:
                    punchStatType = PunchStatisticsTranTypeList.RepresentationReIssue;
                    break;
                case SUMMONS_REPCODE_ADDRESS:
                    punchStatType = PunchStatisticsTranTypeList.ChangeOfOffender;
                    break;
                case SUMMONS_REPCODE_CHANGEOFFENDER:
                    chargeStatus = CODE_SUMMONS_REISSUE;
                    punchStatType = PunchStatisticsTranTypeList.ChangeOfOffender;
                    break;
            }

            string changeOfRegNo = chkChangeRegistration.Checked ? "Y" : "N";

            //if there are no new registration details, need to withdraw the notice
            if (chkChangeRegistration.Checked)
            {
                if (rdlRegDecision.SelectedValue.Equals("N"))
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text3"));
                }

                if (chkNoFurtherDetails.Checked)
                {
                    if (this.isSummons)
                    {
                        rcCode = SUMMONS_REPCODE_WITHDRAW;
                        chargeStatus = CODE_SUMMONS_WITHDRAWN_CHARGE;
                    }
                    else
                    {
                        rcCode = REPCODE_WITHDRAW;         //Nolle
                        chargeStatus = CODE_CASE_CANCELLED;
                    }
                }
                else
                {
                    if (this.isSummons)
                    {
                        rcCode = SUMMONS_REPCODE_WITHDRAW;
                        chargeStatus = CODE_NOTICE_NEW_OFFENDER;        // CODE_SUMMONS_WITHDRAWN_CHARGE;
                    }
                    else
                    {
                        rcCode = REPCODE_NOCHANGE;
                        chargeStatus = (int)ViewState["ChargeStatus"];  //we continue with the case
                    }
                }
            }

            //dls 070929 - need to check that summons reps also have seleted a valid decision
            if (rcCode == REPCODE_NONE || rcCode == SUMMONS_REPCODE_NONE)
            {
                isValid = false;
                sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text4"));
            }

            // Oscar 20120724 add optional details and recommendation
            var arDB = new AuthorityRulesDB(this.connectionString);
            var optionalDetailsAndRecommendation = arDB.GetAuthorityRule(this.autIntNo, "4533").ARString.Equals("Y", StringComparison.OrdinalIgnoreCase);

            if (txtRepDetails.Text.Trim().Length == 0 && !optionalDetailsAndRecommendation)
            {
                isValid = false;
                sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text5"));
            }

            if ((official.Length == 0 || official.Equals("[ None ]")) && RepOfficialIsMandatory(rcCode, optionalDetailsAndRecommendation))
            {
                isValid = false;
                sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text6"));
            }

            string recommendation = this.txtRecommend.Text.Trim();
            if (recommendation.Length == 0 && !optionalDetailsAndRecommendation)
            {
                isValid = false;
                sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text7"));
            }

            decimal amount;
            if (!decimal.TryParse(this.txtRevAmount.Text.Trim(), out amount))
            {
                isValid = false;
                sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text8"));
            }
            else
            {
                // SD: 20081208 : Test for null amount.
                if (amount == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text9"));
                }


                //dls 071218 - this now applies to NoAOG's as well as std notices
                //if (!this.isNoAOG)
                if (amount > originalAmount)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text10"));
                }
                if ((rcCode == REPCODE_REDUCTION || rcCode == SUMMONS_REPCODE_REDUCTION) && amount == originalAmount)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text11"));
                }
            }

            // Set the new amount to zero if it is a no re-summons
            if (rcCode == 1 || rcCode == REPCODE_WITHDRAW || rcCode == SUMMONS_REPCODE_WITHDRAW)
                amount = 0;
            else if (amount <= 0)
            {
                isValid = false;
                sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text12"));
            }

            //dls 071218 - even tho we are displaying 999999 on the screen, the ChgRevAmount in the database must remain as 0
            if (this.isNoAOG && amount == 999999)
            {
                amount = 0;
            }
            // Check the new offender details if the panel is visible    
            DriverDetails driver = null;
            string sChangeOfOffender = "N";

            if (chkChangeOffender.Checked || rcCode == SUMMONS_REPCODE_ADDRESS)
            {
                driver = new DriverDetails();
                driver.LastUser = loginUser;
                sChangeOfOffender = "Y";
                driver.DrvSurname = this.txtSurname.Text;
                if (driver.DrvSurname.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text13"));
                }

                driver.DrvForeNames = this.txtForenames.Text.Trim();
                string[] sForename = driver.DrvForeNames.Split(' ');
                bool bForename = true;

                if (bCheckForenames)
                {
                    // first check if Initials text value equals Forenames text value
                    if (this.txtForenames.Text.Trim() == this.txtInitials.Text.Trim())
                        bForename = false;

                    if (bForename)
                    {
                        for (int n = 0; n < sForename.Length; n++)
                        {
                            if (sForename[n].Length < 3)
                            {
                                bForename = false;
                            }
                        }

                        string sPattern = @"^[a-zA-Z- ]+$";
                        Regex regex = new Regex(sPattern);
                        //Match match = regex.Match(driver.DrvForeNames);
                        if (!regex.IsMatch(driver.DrvForeNames) || !bForename)
                        {
                            isValid = false;
                            // mrs 2008-07-07 made visible
                            txtForenames.ReadOnly = false;
                            sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text14"));
                        }
                    }
                    else
                    {
                        isValid = false;
                        // mrs 2008-07-07 made visible
                        txtForenames.ReadOnly = false;
                        sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text15"));
                    }
                }

                driver.DrvInitials = this.txtInitials.Text.Trim();
                if (driver.DrvInitials.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text16"));
                }

                driver.DrvAge = this.txtAge.Text.Trim();

                if (bCheckID)
                {
                    if (this.txtIDNumber.Text.Trim().Length == 0 && txtPassport.Text.Trim().Length == 0)
                    {
                        //Jerry 2012-12-14 change
                        //pnlChangeOfOffender.Visible = false;
                        //lblError.Visible = false;
                        //return;
                        isValid = false;
                        sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text17"));
                    }

                    if (this.txtIDNumber.Text.Trim().Length > 0)
                    {
                        // check for length 13
                        if (this.txtIDNumber.Text.Trim().Length != 13)
                        {
                            isValid = false;
                            sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text17"));
                        }
                        else
                        {
                            int nRes = -1;
                            int.TryParse(this.txtIDNumber.Text.Trim(), out nRes);
                            if (nRes == -1)
                            {
                                isValid = false;
                                sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text18"));
                            }


                        }


                    }

                    if (driver.DrvAge.Length == 0)
                    {
                        isValid = false;
                        sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text19"));
                    }
                }

                // work out the type
                if (this.txtIDNumber.Text.Trim().Length > 0)
                {
                    driver.DrvIDNumber = this.txtIDNumber.Text.Trim();
                    driver.DrvIDType = "02";
                }
                else
                {
                    driver.DrvIDNumber = txtPassport.Text.Trim();
                    driver.DrvIDType = "01";
                }

                driver.DrvNationality = this.txtNationality.Text;
                if (driver.DrvNationality.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text20"));
                }

                driver.DrvPOAdd1 = this.txtPO1.Text.Trim();
                if (driver.DrvPOAdd1.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text21"));
                }

                driver.DrvPOAdd2 = this.txtPo2.Text.Trim();
                driver.DrvPOAdd3 = this.txtPo3.Text.Trim();
                driver.DrvPOAdd4 = this.txtPo4.Text.Trim();
                driver.DrvPOAdd5 = this.txtPo5.Text.Trim();

                if (driver.DrvPOAdd2.Length == 0 && driver.DrvPOAdd3.Length == 0 && driver.DrvPOAdd4.Length == 0 && driver.DrvPOAdd5.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text22"));
                }

                driver.DrvPOCode = this.txtPoArea.Text.Trim();
                if (driver.DrvPOCode.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text23"));
                }

                driver.DrvStAdd1 = this.txtStreet1.Text.Trim();
                if (driver.DrvStAdd1.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text24"));
                }

                driver.DrvStAdd2 = this.txtStreet2.Text.Trim();
                driver.DrvStAdd3 = this.txtStreet3.Text.Trim();
                driver.DrvStAdd4 = this.txtStreet4.Text.Trim();

                if (driver.DrvStAdd2.Length == 0 && driver.DrvStAdd3.Length == 0 && driver.DrvStAdd4.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text25"));
                }

                driver.DrvStCode = this.txtStreetArea.Text.Trim();
                if (driver.DrvStCode.Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgBtnDecide.Text26"));
                }

                driver.DrvIntNo = (int)this.ViewState["DrvIntNo"];
                driver.DrvWorkNo = txtWorkNo.Text;
                driver.DrvHomeNo = txtHomeNo.Text;
                driver.DrvCellNo = txtCellNo.Text;
            }

            // End here if something needs correcting
            if (!isValid)
            {
                sb.Append("</ul>\n");
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return;
            }

            string letterTo = rdlLetter.SelectedValue;
            string changeRegNo = "N";

            if (chkChangeRegistration.Checked)
            {
                changeRegNo = "Y";
                isValid = SaveRegNoDetails(); //********************************************************************************** Needs to be inserted into 
                //representationDecide stored proc, need to still be called so that the checks on the data can be done.
                if (!isValid)
                {
                    return;
                }
            }

            int notIntNo = (int)this.ViewState["NotIntNo"];

            // Decide the Representation
            //******************************************************************************************************************** Need to add the check to see if it
            //is a change of offender, if so then need to pass driver object fields into NewDecideRepresentation to update the driver at the same time, if it is not
            //a change in offender then must not update the fields

            //dls 080322 - removed code and simplified and combined all into single transaction

            RepresentationDB represent = new RepresentationDB(this.connectionString);
            NoticeDB noticeDB = new NoticeDB(this.connectionString);

            this.repIntNo = (int)this.ViewState["RepIntNo"];
            string errMessage = string.Empty;

            //variables for Change of registration
            int newVMIntNo = 0;
            int newVTIntNo = 0;

            string newVehColour = string.Empty;
            string newRegNo = string.Empty;
            string newRegDecision = "N";

            if (chkChangeRegistration.Checked)
            {
                txtNewRegNo.Text = txtNewRegNo.Text.ToUpper();

                if (!chkNoFurtherDetails.Checked)
                {
                    newVMIntNo = Convert.ToInt32(ddlNewVehMake.SelectedValue);
                    newVTIntNo = Convert.ToInt32(ddlNewVehType.SelectedValue);

                    newVehColour = ddlNewVehicleColourDescr.SelectedValue;
                    newRegDecision = rdlRegDecision.SelectedValue;
                    newRegNo = txtNewRegNo.Text;
                }
            }

            //no need to call EASYPay code - Charge Update trigger will take care of it
            ////dls 071218 - if the amount is 0 (NoAOG) bypass the EasyPay tran 
            bool easyPayDelete = false;
            int noOfDaysForExpiry = 0;

            //if (!(this.isNoAOG && amount == 0))
            //{
            //    // Update the EasyPay Transaction
            //    AuthorityDB authDb = new AuthorityDB(this.connectionString);

            //    DateRulesDetails dr = new DateRulesDetails();
            //    dr.AutIntNo = this.autIntNo;
            //    dr.DtRStartDate = "EasyPayDate";
            //    dr.DtREndDate = "NotPaymentDate";
            //    dr.LastUser = this.loginUser;

            //    DefaultDateRules rule = new DefaultDateRules(dr, this.connectionString);
            //    int noDaysForExpiry = rule.SetDefaultDateRule();

            //    EasyPayAuthority authority = authDb.VerifyAuthorityForEasyPay(this.autIntNo, noDaysForExpiry);
            //    if (authority.IsValid)
            //    {
            //        //EasyPayDB epDb = new EasyPayDB(this.connectionString);
            //        //epDb.EasyPayTransactionVerification(authority, notIntNo, loginUser);
            //        easyPayDelete = true;
            //        noOfDaysForExpiry = authority.NumberOfDaysForExpiry;
            //    }
            //}

            // Decide if there's anything else to update

            bool updatePersona = false;
            string personaSource = string.Empty;

            if (rcCode == REPCODE_CHANGEOFFENDER || rcCode == SUMMONS_REPCODE_CHANGEOFFENDER || sChangeOfOffender.Equals("Y") || rcCode == SUMMONS_REPCODE_ADDRESS)
            {
                if (chkChangeRegistration.Checked)
                    personaSource = "RegNoRep"; // Max length 10
                else
                    personaSource = "Represent"; // Max length 10

                updatePersona = true;
            }

            // Summons case withdrawn
            bool withdrawSummons = false;

            if (rcCode == SUMMONS_REPCODE_WITHDRAW)
            {
                withdrawSummons = true;
            }

            // Oscar 20111125 add transaction for push queue
            List<int> result = new List<int>();
            QueueItem item = new QueueItem();
            QueueItemProcessor queProcessor = new QueueItemProcessor();
            using (TransactionScope scope = new TransactionScope())
            {
                result = represent.DecideRepresentation(repIntNo, rcCode,null, official, recommendation,
                    DateTime.Now, loginUser, chgIntNo, amount, chargeStatus, chargeRowVersion, originalAmount,
                    sChangeOfOffender, txtRepDetails.Text.Trim(), letterTo, this.isSummons, changeRegNo,
                    notIntNo, newRegDecision, newRegNo, newVMIntNo, newVTIntNo, newVehColour, driver,
                    easyPayDelete, noOfDaysForExpiry, updatePersona, personaSource,
                    withdrawSummons, ref errMessage, this.noOfDaysForPayment, dataWashingActived, useRepAddress, repAddrExpiryPeriod);

                if (result == null || result.Count <= 0 || result[0] <= 0)
                {
                    this.lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text18"), errMessage);
                    return;
                }

                // 2012.09.12 Nick add for report punch statistics
                if (rcCode == REPCODE_REDUCTION || rcCode == SUMMONS_REPCODE_REDUCTION ||
                    rcCode == REPCODE_WITHDRAW || rcCode == SUMMONS_REPCODE_WITHDRAW ||
                    rcCode == REPCODE_NOCHANGE || rcCode == SUMMONS_REPCODE_NOCHANGE ||
                    rcCode == REPCODE_CHANGEOFFENDER || rcCode == SUMMONS_REPCODE_CHANGEOFFENDER ||
                    rcCode == SUMMONS_REPCODE_ADDRESS || rcCode == SUMMONS_REPCODE_REISSUE)
                {
                    //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                    punchStat.PunchStatisticsTransactionAdd(GetAutIntNo(), loginUser, punchStatType, PunchAction.Add);
                }

                // Oscar 20111116 add push queue
                item = new QueueItem();
                item.Body = notIntNo.ToString();
                item.QueueType = ServiceQueueTypeList.SearchNameIDUpdate;
                queProcessor.Send(item);

                //Jerry 2013-04-23 add rcCode == SUMMONS_REPCODE_REISSUE
                // jerry 2012-02-15 add push queue
                //if (this.chkChangeRegistration.Checked || this.chkChangeOffender.Checked)
                if (this.chkChangeRegistration.Checked || this.chkChangeOffender.Checked || rcCode == SUMMONS_REPCODE_REISSUE)
                {

                    //Jerry 2013-04-23 add if condition, the rcCode == SUMMONS_REPCODE_REISSUE don't need to push ProcessNewOffenderNotices
                    if (this.chkChangeRegistration.Checked || this.chkChangeOffender.Checked)
                    {
                        //Jake 2014-02-28 comment out, when push new offender notice , we need to set action date = now
                        //int numOfDays = 0;
                        //if (this.isS35NoAOG || this.isNoAOG)
                        //{
                        //    numOfDays = this.noOfDaysToSummonsNoAOG;
                        //}
                        //else
                        //{
                        //    numOfDays = this.noOfDaysToSummons;
                        //}
                        item = new QueueItem();
                        item.Body = notIntNo.ToString();
                        item.QueueType = ServiceQueueTypeList.ProcessNewOffenderNotices;
                        item.Group = this.autCode;
                        // jake 2013-12-06 added 
                        item.ActDate = DateTime.Now;// DateTime.Now.AddDays(numOfDays);
                        queProcessor.Send(item);
                    }

                    //jerry 2012-03-14 add to push Generate summons queue
                    if (this.isSummons)
                    {
                      
                        // Oscar 20120315 changed the Group
                        NoticeDetails notice = noticeDB.GetNoticeDetails(notIntNo);
                        string[] group = new string[3];
                        group[0] = this.autCode.Trim();
                        //Jerry 2013-03-07 change
                        //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                        group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                        group[2] = Convert.ToString(notice.NotCourtName);

                        item = new QueueItem();
                        item.Body = notIntNo.ToString();
                        //item.Group = this.autCode;
                        // Oscar 20120315 changed the Group
                        item.Group = string.Join("|", group);
                        item.QueueType = ServiceQueueTypeList.GenerateSummons;
                        //item.ActDate = DateTime.Now.AddDays(this.noOfDaysToSummons - this.noOfDaysTo1stNotice);
                        int delayHours = new SIL.AARTO.DAL.Services.SysParamService().GetBySpColumnName(SIL.AARTO.DAL.Entities.SysParamList.DelayActionDate_InHours.ToString()).SpIntegerValue;
                        item.ActDate = DateTime.Now.AddHours(delayHours);
                        //jerry 2012-03-31 add Priority
                        item.Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days;
                        //Jerry 2012-05-14 add last user
                        item.LastUser = this.loginUser;
                        queProcessor.Send(item);

                        //Henry 2013-03-27 add
                        AuthorityRulesDetails arDetails = new AuthorityRulesDetails
                        {
                            AutIntNo = this.autIntNo,
                            ARCode = "5050",
                            LastUser = this.loginUser
                        };
                        KeyValuePair<int, string> aR_5050 = (new DefaultAuthRules(arDetails, this.connectionString)).SetDefaultAuthRule();

                        if (aR_5050.Value.Trim().ToUpper() == "Y")
                        {
                            item = new QueueItem();
                            item.Body = notIntNo.ToString();
                            item.Group = string.Join("|", group);
                            item.QueueType = ServiceQueueTypeList.GenerateSummons;
                            item.ActDate = notice.NotOffenceDate.AddMonths(aR_5050.Key).AddDays(-5);
                            item.Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days;
                            item.LastUser = this.loginUser;
                            queProcessor.Send(item);
                        }
                    }
                }

                if (!this.isSummons)
                {
                    if (rcCode == REPCODE_REDUCTION || rcCode == REPCODE_NOCHANGE)
                    {
                        PushGenerateSummonsQueue(repIntNo, notIntNo, false);
                    }

                    if (rcCode == REPCODE_WITHDRAW)
                    {
                        SIL.AARTO.DAL.Entities.Notice notice = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(notIntNo);

                        if (notice.NoticeStatus != 927)
                        {
                            PushGenerateSummonsQueue(repIntNo, notIntNo, false);
                        }
                    }
                }

                scope.Complete();
            }

            // TODO: Summons representation post-decision processing

            //dls 070829 - they do not want a letter for a plain change of offender
            //dls 070825 - everyone should get a letter! Here's my thinking:
            //If rcCode = 9, it means they changed offender and are proceeding with the case. This is equivalent to a 5 = NoAction, proceed with case
            //so they still need to select owner/ driver and decide who should get the letter.
            //if ((rcCode != REPCODE_CHANGEOFFENDER || this.isSummons) && rcCode != SUMMONS_REPCODE_CHANGEOFFENDER)
            //{
            //    Helper_Web.BuildPopup(PAGE_URL_DECISION, this, "Representation_LetterViewer.aspx", "RepIntNo", repIntNo.ToString(), "LetterTo",
            //        this.isSummons ? "S" : rdlLetter.SelectedValue, "TicketNo", ticketNo);
            //}
            //else
            //{
            //    this.lblError.Text = "The new offender's details have been captured";
            //    this.Response.Redirect(thisPageURL);
            //}

            switch (rcCode)
            {
                case REPCODE_CHANGEOFFENDER:
                case SUMMONS_REPCODE_CHANGEOFFENDER:
                case SUMMONS_REPCODE_ADDRESS:
                    lblError.Text = (string)this.GetLocalResourceObject("lblError.Text19");
                    lblError.Visible = true;
                    GoBackToBeginning(true);
                    break;

                case SUMMONS_REPCODE_REISSUE:
                    lblError.Text = (string)this.GetLocalResourceObject("lblError.Text20");
                    lblError.Visible = true;
                    GoBackToBeginning(true);
                    break;

                default:
                    #region Oscar 20121015 disabled
                    //// 20120809 Nick add for report engine
                    //if (IsIBMPrinter())
                    //{
                    //    represent.UpdateRepresentationForPrint(this.autIntNo, repIntNo, loginUser, "D", this.isSummons);
                    //    this.lblError.Visible = true;
                    //    this.lblError.Text = (string)GetLocalResourceObject("errorMsg");
                    //    GoBackToBeginning(true);
                    //}
                    //else
                    //{
                    //    // 2011-09-30 jerry add
                    //    if (isPrintBatchRepLetter)
                    //    {
                    //        //set the RepPrintFileName
                    //        // jerry 2012-03-08 set PrintFileName = RepDecision_AutCode_DateTime
                    //        //repPrintFileName = "RepDecision_" + DateTime.Now.ToString("yyyy-MM-dd");
                    //        repPrintFileName = "RepDecision_" + this.autCode + "_" + DateTime.Now.ToString("yyyy-MM-dd");
                    //        represent.RepresentationUpdateForPrintBatch(true, this.isSummons, this.repIntNo, this.autIntNo, false, false, this.loginUser, repPrintFileName);

                    //        Session["ShowBlockUI"] = true;
                    //        string tempLetterTo = this.isSummons ? "S" : rdlLetter.SelectedValue;
                    //        this.thisPageURL = this.thisPageURL + "&ShowBlockUI=YES&LetterType=RepDecision&RepIntNo=" + this.repIntNo.ToString() + "&TicketNo=" + this.txtTicketNo.Text.Trim() + "&LetterTo=" + tempLetterTo;
                    //        Response.Redirect(this.thisPageURL, true);
                    //    }
                    //    else
                    //    {
                    //        //2011-09-30 jerry print one file at a time.
                    //        represent.RepresentationUpdateForPrintBatch(false, this.isSummons, this.repIntNo, this.autIntNo, true, true, this.loginUser, null);

                    //        PopupTheRepresentationLetterViewer("RepDecision", this.repIntNo.ToString(), txtTicketNo.Text.ToString(), this.isSummons ? "S" : rdlLetter.SelectedValue);
                    //    }
                    //}
                    #endregion

                    // Oscar 20121015 changed
                    PostRepLetter();

                    break;
            }

        }

        void PushGenerateSummonsQueue(int repIntNo, int notIntNo, bool isReverseOrCancel)
        {
            SIL.AARTO.DAL.Entities.Notice notice = new NoticeService().GetByNotIntNo(notIntNo);

            string NotFilmType = notice.NotFilmType == null ? "" : notice.NotFilmType;
            string[] group = new string[3];
            group[0] = this.autCode.Trim();
            group[1] = (Convert.ToString(NotFilmType).Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
            group[2] = Convert.ToString(notice.NotCourtName);

            DateTime actDate = DateTime.Now;
            DateTime postDate = notice.NotPosted1stNoticeDate.HasValue ? notice.NotPosted1stNoticeDate.Value : DateTime.Now;

            NoAOGDB noAogDB = new NoAOGDB(this.connectionString);
            bool isNoAog = noAogDB.IsNoAOG(Convert.ToInt32(notIntNo), NoAOGQueryType.Notice);

            if (isReverseOrCancel)
            {
                actDate = DateTime.Now;
            }
            else if (isNoAog)
            {
                actDate = noAogDB.GetNoAOGGenerateSummonsActionDate(notice.AutIntNo, postDate);
            }
            else
            {
                DateRulesDetails dateRuleIssueSum = new DateRulesDetails();
                dateRuleIssueSum.AutIntNo = autIntNo;
                dateRuleIssueSum.DtRStartDate = "RepRecommendDate";
                dateRuleIssueSum.DtREndDate = "NotIssueSummonsDate";
                dateRuleIssueSum.LastUser = this.loginUser;
                DefaultDateRules ruleIssueSum = new DefaultDateRules(dateRuleIssueSum, connectionString);
                int noOfDaysToSummons = ruleIssueSum.SetDefaultDateRule();

                SIL.AARTO.DAL.Entities.Representation rep = new RepresentationService().GetByRepIntNo(repIntNo);

                actDate = rep.RepRecommendDate.Value.AddDays(noOfDaysToSummons);
            }

            QueueItem item = new QueueItem();
            QueueItemProcessor queProcessor = new QueueItemProcessor();

            item = new QueueItem();
            item.Body = notIntNo.ToString();
            item.Group = string.Join("|", group);
            item.QueueType = ServiceQueueTypeList.GenerateSummons;
            item.ActDate = actDate;
            item.Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days;   // 2013-09-06, Oscar added
            item.LastUser = this.loginUser;
            queProcessor.Send(item);
        }

        protected void GoBackToBeginning(bool bIgnore)
        {
            pnlRetrieve.Visible = true;
            pnlCharges.Visible = true;
            this.divNoticeTitle.Visible = false;    //Oscar 20101202 - added
            pnlRepresentation.Visible = false;
            pnlChangeOfOffender.Visible = false;
            pnlNoResults.Visible = true;
            pnlDecide.Visible = false;
            btnDecide.Visible = false;
            btnUpdate.Visible = true;
            btnReverse.Visible = false;
            this.pnlLetterTo.Visible = false;
            this.pnlChangeRegNoDecision.Visible = false;

            this.btnReport.Visible = false;
            this.btnAdd.Visible = false;

            if (!bIgnore)
                SearchRepresentations(bIgnore);
            // Oscar 20101103 - changed, display grdRepresentation.
            //this.chgIntNo = ViewState["ChgIntNo"] == null ? 0 : Convert.ToInt32(ViewState["ChgIntNo"]);
            //DateTime courtDate = ViewState["CourtDate"] == null ? DateTime.MinValue : Convert.ToDateTime(ViewState["CourtDate"]);
            //grdRepresentationBind(this.chgIntNo, courtDate);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.GoBackToBeginning(false);
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            Helper_Web.BuildPopup(this, "Representation_ReportViewer.aspx", "ChgIntNo", this.ViewState["ChgIntNo"].ToString());
            //PunchStats805806 enquiry Representation
        }

        protected void btnNoResults_Click(object sender, EventArgs e)
        {
            //Helper_Web.BuildPopup(this, "Representation_ReportNoResults.aspx", "AutIntNo", ddlSelectLA.SelectedValue.ToString());
            //2014-09-23 Heidi changed for adding a jump page for RepresentationWithNoResult report. (5360)
            string tmsDomain = ConfigurationManager.AppSettings["AARTODomainURL"];
            if (!string.IsNullOrWhiteSpace(tmsDomain) && tmsDomain.EndsWith("/"))
            {
                tmsDomain = tmsDomain.Remove(tmsDomain.Length - 1, 1);
            }
            string url = tmsDomain + "/Representation/RepresentationWithNoResult?AutIntNo=" + ddlSelectLA.SelectedValue.ToString();
            Response.Redirect(url);
            //PunchStats805806 enquiry Representation
        }

        protected void btnPostalCodesA_Click(object sender, EventArgs e)
        {
            this.PopulateAreaCodes(this.txtPo4.Text, "A");
        }

        protected void ddlPostalCodesA_SelectedIndexChanged(object sender, EventArgs e)
        {
            SuburbDB acdb = new SuburbDB(connectionString);
            this.txtPoArea.Text = ddlPostalCodesA.SelectedValue;
        }

        protected void btnPostalCodesB_Click(object sender, EventArgs e)
        {
            this.PopulateAreaCodes(this.txtStreet4.Text, "B");
        }

        protected void ddlPostalCodesB_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtStreetArea.Text = ddlPostalCodesB.SelectedValue;
        }

        private void PopulateAreaCodes(string sTown, string sType)
        {
            SuburbDB acdb = new SuburbDB(connectionString); //changed from areadb to suburbdb
            DataSet ds = acdb.GetFilteredSuburbCodes("0", sTown);
            if (sType == "A")
            {
                //Dictionary<int, string> lookups =
                //    SuburbLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
                //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //{
                //    int subIntNo =int.Parse(ds.Tables[0].Rows[i]["SubIntNo"].ToString());
                //    string areaCode = ds.Tables[0].Rows[i]["AreaCode"].ToString();
                //    if (lookups.ContainsKey(subIntNo))
                //    {
                //        ddlPostalCodesA.Items.Add(new ListItem(lookups[subIntNo], areaCode.ToString()));
                //    }
                //}
                Dictionary<string, string> lookups =
                  AreaCodeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string areaCode = ds.Tables[0].Rows[i]["AreaCode"].ToString();
                    if (lookups.ContainsKey(areaCode))
                    {
                        ddlPostalCodesA.Items.Add(new ListItem(lookups[areaCode], areaCode.ToString()));
                    }
                }
                //ddlPostalCodesA.DataSource = ds;
                //ddlPostalCodesA.DataValueField = "AreaCode";
                //ddlPostalCodesA.DataTextField = "Descr";
                //ddlPostalCodesA.DataBind();
                if (ds.Tables[0].Rows.Count == 1)
                    this.txtPoArea.Text = ddlPostalCodesA.Items[0].Value;
            }
            else
            {
                //Dictionary<int, string> lookups =
                //    SuburbLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
                //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //{
                //    int subIntNo = int.Parse(ds.Tables[0].Rows[i]["SubIntNo"].ToString());
                //    string areaCode = ds.Tables[0].Rows[i]["AreaCode"].ToString();
                //    if (lookups.ContainsKey(subIntNo))
                //    {
                //        ddlPostalCodesB.Items.Add(new ListItem(lookups[subIntNo], areaCode.ToString()));
                //    }
                //}
                Dictionary<string, string> lookups =
                  AreaCodeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string areaCode = ds.Tables[0].Rows[i]["AreaCode"].ToString();
                    if (lookups.ContainsKey(areaCode))
                    {
                        ddlPostalCodesB.Items.Add(new ListItem(lookups[areaCode], areaCode.ToString()));
                    }
                }
                //ddlPostalCodesB.DataSource = ds;
                //ddlPostalCodesB.DataValueField = "AreaCode";
                //ddlPostalCodesB.DataTextField = "Descr";
                //ddlPostalCodesB.DataBind();
                if (ds.Tables[0].Rows.Count == 1)
                    this.txtStreetArea.Text = ddlPostalCodesB.Items[0].Value;
            }
            ds.Dispose();
        }

        protected void chkChangeRegistration_CheckedChanged(object sender, EventArgs e)
        {
            ShowChangeOfRegNo(true);
        }

        private void ShowChangeOfRegNo(bool force)
        {
            pnlChangeRegNoDecision.Visible = this.chkChangeRegistration.Checked;

            if (this.chkChangeRegistration.Checked)
            {
                //force the letter to go to the owner for Change of Registration
                rdlLetter.SelectedValue = "O";
                rdlLetter.Enabled = false;

                if (force)
                    chkNoFurtherDetails.Checked = false;

                if (!this.chkNoFurtherDetails.Checked)
                {
                    chkChangeOffender.Checked = true;

                    // Oscar 20101105 - added, S56 is not allowed to change offender
                    if (GetNoticeFilmType() == NoticeFilmType.M)
                        chkChangeOffender.Checked = false;
                }
                else
                {
                    chkChangeOffender.Checked = false;
                }

                ShowChangeOfOffender();
            }
            else
            {
                rdlLetter.Enabled = true;   //222
            }

            ShowRegNoDetails();

            // Oscar 20101104 - added, S56 is not allowed to change offender
            if (GetNoticeFilmType() == NoticeFilmType.M)
            {
                this.pnlChangeOfOffender.Visible = false;
                this.chkChangeOffender.Checked = false;
                this.pnlLetterTo.Visible = false;
            }

            // added by Oscar 20101027 - to avoid if pnlChangeRegNoDecision is closed, but the pnlChangeOfOffender and pnlLetterTo is still display.
            if (!pnlChangeRegNoDecision.Visible)
            {
                this.pnlChangeOfOffender.Visible = this.chkChangeOffender.Checked;
                this.pnlLetterTo.Visible = this.chkChangeOffender.Checked;
            }

        }

        private void ShowRegNoDetails()
        {
            if (pnlChangeRegNoDecision.Visible)
                pnlRegistration.Visible = !chkNoFurtherDetails.Checked;
            else
                pnlRegistration.Visible = false;

            if (pnlChangeRegNoDecision.Visible && chkNoFurtherDetails.Checked)
            {
                //force this to be Nolle
                if (this.isSummons)
                    rblCodes.SelectedValue = SUMMONS_REPCODE_WITHDRAW.ToString();
                else
                    rblCodes.SelectedValue = REPCODE_WITHDRAW.ToString();

                rblCodes.Enabled = false;
            }
            else
            {
                rblCodes.Enabled = true;
            }
        }

        protected void chkNoFurtherDetails_CheckedChanged(object sender, EventArgs e)
        {
            this.ShowRegNoDetails();
        }

        protected bool SaveRegNoDetails()
        {
            //Barry Dickson 20080317 Need to allow the checks for save reg no but need to take the actual updating out of here as it must be grouped in a
            //transaction in the representation decide.
            StringBuilder sb = new StringBuilder((string)this.GetLocalResourceObject("ErrorMsgSabeReg.Text1"));
            bool isValid = true;
            string changeOfRegNoType = rdlRegDecision.SelectedValue;

            if (rdlRegDecision.SelectedValue.Equals("N"))
            {
                isValid = false;
                sb.Append((string)this.GetLocalResourceObject("ErrorMsgSabeReg.Text2"));
            }

            if (!chkNoFurtherDetails.Checked)
            {
                //only check these if there are details to update
                if (ddlNewVehMake.SelectedIndex < 1)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgSabeReg.Text3"));
                }

                if (ddlNewVehType.SelectedIndex < 1)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgSabeReg.Text4"));
                }

                if (txtNewRegNo.Text.Trim().Length == 0)
                {
                    isValid = false;
                    sb.Append((string)this.GetLocalResourceObject("ErrorMsgSabeReg.Text5"));
                }
            }

            if (!isValid)
            {
                sb.Append("</ul>\n");
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return isValid;
            }

            this.lblError.Visible = true;
            return isValid;
        }

        public void rblCodes_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtSurname.ReadOnly = false;
            this.txtInitials.ReadOnly = false;
            this.txtForenames.ReadOnly = false;

            int value;
            if (int.TryParse(this.rblCodes.SelectedValue, out value))
            {
                switch (value)
                {
                    case SUMMONS_REPCODE_WITHDRAW:
                    case SUMMONS_REPCODE_REISSUE:
                        this.chkChangeOffender.Checked = false;
                        this.pnlChangeOfOffender.Visible = false;
                        this.chkChangeOffender.Visible = false;
                        break;
                    case SUMMONS_REPCODE_REDUCTION:
                    case SUMMONS_REPCODE_NONE:
                    case SUMMONS_REPCODE_NOCHANGE:
                    case SUMMONS_REPCODE_CHANGEOFFENDER:
                        //this.chkChangeOffender.Visible = true;
                        // Changed by Oscar 20101027
                        if (GetNoticeFilmType() != NoticeFilmType.M)
                            this.chkChangeOffender.Visible = true;
                        break;
                    case SUMMONS_REPCODE_ADDRESS:
                        this.chkChangeOffender.Checked = false;
                        this.pnlChangeOfOffender.Visible = true;
                        this.chkChangeOffender.Visible = false;
                        this.txtSurname.ReadOnly = true;
                        this.txtInitials.ReadOnly = true;
                        //this.txtForenames.ReadOnly = txtForenames.Text.Trim().Length == 0 ? false : true;
                        this.txtForenames.ReadOnly = txtForenames.Text.Trim().Length > 3 ? false : true;
                        break;
                }
            }

            // Oscar 2013-06-05 added
            LockRevAmountForWithdrawn();
        }

        // Oscar 20101019 - added
        private int GetAutIntNo()
        {
            if (!int.TryParse(this.ddlSelectLA.SelectedValue, out this.autIntNo)
                && !int.TryParse(Convert.ToString(ViewState["AutIntNo"]), out this.autIntNo)
                && !int.TryParse(Convert.ToString(Session["autIntNo"]), out this.autIntNo))
                this.autIntNo = 0;
            ViewState["AutIntNo"] = this.autIntNo;

            //jerry 2011-12-01 add autCode for push queue
            this.autCode = new AuthorityDB(this.connectionString).GetAuthorityDetails(this.autIntNo).AutCode.Trim();

            return this.autIntNo;
        }

        // Oscar 20101027 - added
        private NoticeFilmType GetNoticeFilmType()
        {
            return nft.GetNoticeFilmType(ViewState["NoticeFilmType"]);
        }

        //private NoticeFilmType GetNoticeFilmType(object typeSource)
        //{
        //    if (typeSource != null && typeSource != DBNull.Value)
        //    {
        //        string chargeType = Convert.ToString(typeSource).ToUpper();
        //        switch (chargeType)
        //        {
        //            case "M":
        //            //case "S56":
        //                return NoticeFilmType.M;
        //            case "H":
        //            //case "S341":
        //                return NoticeFilmType.H;
        //            default:
        //                return NoticeFilmType.Others;
        //        }
        //    }
        //    return NoticeFilmType.Others;
        //}

        private int CheckWithdrawType()
        {
            int repIntNo = this.repIntNo;
            this.repIntNo = 0;
            string columnName;
            DataTable dt = GetChargeSource(GetSearchRepresentationsSourceRefresh(out columnName));
            DataView dv = dt.DefaultView;
            dv.RowFilter = "ChargeStatus not in (927, 940)";
            this.repIntNo = repIntNo;
            //if (dv.Count > 1)
            //    return false;    // withdraw charge
            //else
            //    return true;    // withdraw summons

            // > 1 ==> withdraw charge
            // = 1 ==> withdraw summons
            // = 0 ==> not allowed to make a decision

            return dv.Count;    // withdraw charge
        }

        /// <summary>
        /// 2011-10-13 jerry add, when the user click yes button
        /// </summary>
        private void PrintBatchSelectYes(string letterType, string repIntNo, string ticketNo, string letterTo)
        {
            RepresentationDB represent = new RepresentationDB(this.connectionString);

            //// 2012-02-20 add push queue
            //QueueItem item = new QueueItem();
            //QueueItemProcessor queProcessor = new QueueItemProcessor();
            //using (TransactionScope scope = new TransactionScope())
            //{
            int temRepIntNo = Convert.ToInt32(repIntNo);
            //    int temNotIntNo = 0;

            represent.RepresentationUpdateForPrintBatch(true, letterTo == "S" ? true : false, temRepIntNo, this.autIntNo, true, true, this.loginUser, null);

            //    // get repPrintFileName
            //    SqlDataReader reader;
            //    // is a summons
            //    if (letterTo == "S")
            //    {
            //        reader = represent.GetRepPrintFileNameByRepIntNo(temRepIntNo, true);
            //        if (reader.Read())
            //        {
            //            repPrintFileName = reader["SRPrintFileName"].ToString();
            //            temNotIntNo = Convert.ToInt32(reader["NotIntNo"].ToString());
            //        }
            //        reader.Dispose();
            //    }
            //    else
            //    {
            //        reader = represent.GetRepPrintFileNameByRepIntNo(temRepIntNo, false);
            //        if (reader.Read())
            //        {
            //            repPrintFileName = reader["RepPrintFileName"].ToString();
            //            temNotIntNo = Convert.ToInt32(reader["NotIntNo"].ToString());
            //        }
            //        reader.Dispose();
            //    }

            //    // Save PrintFileName
            //    SIL.AARTO.DAL.Entities.PrintFileName printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().GetByPrintFileName(repPrintFileName);
            //    if (printFileName == null)
            //    {
            //        printFileName = new SIL.AARTO.DAL.Entities.PrintFileName();
            //        printFileName.PrintFileName = repPrintFileName;
            //        printFileName.AutIntNo = this.autIntNo;
            //        printFileName.DateAdded = DateTime.Now;
            //        printFileName.LastUser = this.loginUser;
            //        printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().Save(printFileName);
            //    }

            //    // Save PrintFileName_Notice
            //    SIL.AARTO.DAL.Entities.PrintFileNameNotice printFileNameNotice = new SIL.AARTO.DAL.Services.PrintFileNameNoticeService().GetByNotIntNoPfnIntNo(temNotIntNo, printFileName.PfnIntNo);
            //    if (printFileNameNotice == null)
            //    {
            //        printFileNameNotice = new SIL.AARTO.DAL.Entities.PrintFileNameNotice();
            //        printFileNameNotice.PfnIntNo = printFileName.PfnIntNo;
            //        printFileNameNotice.NotIntNo = temNotIntNo;
            //        printFileNameNotice.LastUser = this.loginUser;
            //        new SIL.AARTO.DAL.Services.PrintFileNameNoticeService().Save(printFileNameNotice);
            //    }

            //    //push queue
            //    item.Body = printFileName.PfnIntNo.ToString();
            //    if (letterType == "RepDecision")
            //    {
            //        item.QueueType = ServiceQueueTypeList.PrintRepresentationResult;
            //    }
            //    else if (letterType == "InsufficientDetails")
            //    {
            //        item.QueueType = ServiceQueueTypeList.PrintRepresentationInsufficientDetails;
            //    }
            //    item.Group = this.autCode;
            //    int delayHours = new SIL.AARTO.DAL.Services.SysParamService().GetBySpColumnName(SIL.AARTO.DAL.Entities.SysParamList.DelayActionDate_InHours.ToString()).SpIntegerValue;
            //    item.ActDate = DateTime.Now.AddHours(delayHours);

            //    queProcessor.Send(item);
            //    scope.Complete();
            //}

            PopupTheRepresentationLetterViewer(letterType, repIntNo, ticketNo, letterTo);
        }

        /// <summary>
        /// 2011-10-13 jerry add, when the user click no button
        /// </summary>
        private void PrintBatchSelectNo(string letterType, string repIntNo, string ticketNo, string letterTo)
        {
            RepresentationDB represent = new RepresentationDB(this.connectionString);

            // 2012-02-20 add push queue
            QueueItem item = new QueueItem();
            QueueItemProcessor queProcessor = new QueueItemProcessor();
            using (TransactionScope scope = new TransactionScope())
            {
                int temRepIntNo = Convert.ToInt32(repIntNo);
                int temNotIntNo = 0;

                // get repPrintFileName
                SqlDataReader reader;
                // is a summons
                if (letterTo == "S")
                {
                    reader = represent.GetRepPrintFileNameByRepIntNo(temRepIntNo, true);
                    if (reader.Read())
                    {
                        repPrintFileName = reader["SRPrintFileName"].ToString();
                        temNotIntNo = Convert.ToInt32(reader["NotIntNo"].ToString());
                    }
                    reader.Dispose();
                }
                else
                {
                    reader = represent.GetRepPrintFileNameByRepIntNo(temRepIntNo, false);
                    if (reader.Read())
                    {
                        repPrintFileName = reader["RepPrintFileName"].ToString();
                        temNotIntNo = Convert.ToInt32(reader["NotIntNo"].ToString());
                    }
                    reader.Dispose();
                }

                //Jerry 2012-06-20 just new printFileName to push queue
                bool isNeedPush = false;

                // Save PrintFileName
                SIL.AARTO.DAL.Entities.PrintFileName printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().GetByPrintFileName(repPrintFileName);
                if (printFileName == null)
                {
                    printFileName = new SIL.AARTO.DAL.Entities.PrintFileName();
                    printFileName.PrintFileName = repPrintFileName;
                    printFileName.AutIntNo = this.autIntNo;
                    printFileName.DateAdded = DateTime.Now;
                    printFileName.LastUser = this.loginUser;
                    printFileName = new SIL.AARTO.DAL.Services.PrintFileNameService().Save(printFileName);

                    //need push queue
                    isNeedPush = true;
                }

                // Save PrintFileName_Notice
                SIL.AARTO.DAL.Entities.PrintFileNameNotice printFileNameNotice = new SIL.AARTO.DAL.Services.PrintFileNameNoticeService().GetByNotIntNoPfnIntNo(temNotIntNo, printFileName.PfnIntNo);
                if (printFileNameNotice == null)
                {
                    printFileNameNotice = new SIL.AARTO.DAL.Entities.PrintFileNameNotice();
                    printFileNameNotice.PfnIntNo = printFileName.PfnIntNo;
                    printFileNameNotice.NotIntNo = temNotIntNo;
                    printFileNameNotice.LastUser = this.loginUser;
                    new SIL.AARTO.DAL.Services.PrintFileNameNoticeService().Save(printFileNameNotice);
                }

                //if new print file name to push queue
                if (isNeedPush)
                {
                    //push queue
                    item.Body = printFileName.PfnIntNo.ToString();
                    if (letterType == "RepDecision")
                    {
                        item.QueueType = ServiceQueueTypeList.PrintRepresentationResult;
                    }
                    else if (letterType == "InsufficientDetails")
                    {
                        item.QueueType = ServiceQueueTypeList.PrintRepresentationInsufficientDetails;
                    }
                    item.Group = this.autCode;
                    int delayHours = new SIL.AARTO.DAL.Services.SysParamService().GetBySpColumnName(SIL.AARTO.DAL.Entities.SysParamList.DelayActionDate_InHours.ToString()).SpIntegerValue;
                    item.ActDate = DateTime.Now.AddHours(delayHours);

                    queProcessor.Send(item);
                }

                scope.Complete();
            }

            PopupTheRepresentationLetterViewer(letterType, repIntNo, ticketNo, letterTo);
        }

        /// <summary>
        /// 2011-10-13 jerry add
        /// </summary>
        private void PopupTheRepresentationLetterViewer(string letterType, string repIntNo, string ticketNo, string letterTo)
        {
            if (letterType == "RepDecision")
            {
                Helper_Web.BuildPopup(PAGE_URL_DECISION, this, "Representation_LetterViewer.aspx", "RepIntNo", repIntNo, "LetterTo", letterTo, "TicketNo", ticketNo);
            }
            else if (letterType == "InsufficientDetails")
            {

                Helper_Web.BuildPopup(thisPageURL, this, "RepresentationInsufficientDetails_LetterViewer.aspx", "RepIntNo", repIntNo, "IsSummons", letterTo == "S" ? "1" : "0");
            }
        }

        #region Oscar 20120801 added for presentation of document

        protected void grdCharges_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Document", StringComparison.OrdinalIgnoreCase))
            {
                var lnkbtn = (LinkButton)e.CommandSource;
                lnkbtn.Enabled = false;
                var args = e.CommandArgument.ToString().Split(',');

                var notIntNo = Convert.ToInt32(args[0]);
                var chgIntNo = Convert.ToInt32(args[1]);
                var isSummons = Convert.ToBoolean(args[2]);

                var success = AutoDocument(notIntNo, chgIntNo, isSummons);
                lnkbtn.Enabled = !success;
                if (success)
                {
                    SearchRepresentations(false);
                    // 2012.09.12 Nick add for report punch statistics
                    //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
                    punchStat.PunchStatisticsTransactionAdd(GetAutIntNo(), loginUser, PunchStatisticsTranTypeList.PresentationOfDocument, PunchAction.Add);
                }
            }
        }

        protected void grdCharges_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var currentGrid = (GridView)sender;
            if (e.Row.RowType == DataControlRowType.Header)
                currentGrid.Columns[7].Visible = false;
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[7].FindControl("lnkbtnDocument").Visible = false;

                return;     // 2013-08-13�� Oscar disabled pod
                if (Convert.ToBoolean(currentGrid.DataKeys[e.Row.RowIndex]["CanAutoDoc"]))
                {
                    e.Row.Cells[7].FindControl("lnkbtnDocument").Visible = true;
                    currentGrid.Columns[7].Visible = true;
                }
            }
        }

        bool AutoDocument(int notIntNo, int chgIntNo, bool isSummons)
        {
            var options = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.MaxValue
            };
            using (var scope = new TransactionScope(TransactionScopeOption.Required, options))
            {
                var repDB = new RepresentationDB(this.connectionString);

                var mandatoryValue = "unknown";
                var msg = string.Empty;
                var autIntNo = GetAutIntNo();
                var repIntNo = 0;
                var repDetails = Convert.ToString(GetLocalResourceObject("PresentationOfDocument"));
                var recommend = Convert.ToString(GetLocalResourceObject("PresentationOfDocument"));
                var repDate = DateTime.Now;
                var csCode = !isSummons ? CODE_REP_LOGGED_CHARGE : CODE_REP_LOGGED_SUMMONS;

                this.lblError.Text = string.Empty;
                this.lblError.Visible = false;

                var dvSource = GetRepresentationSource(GetSearchRepresentationsSource(), chgIntNo, 0);
                if (dvSource.Count > 0)
                {
                    var dr = dvSource[0];

                    var offenderName = GetMandatoryValue(dr["Name"], mandatoryValue);

                    var sb = new StringBuilder();
                    string tmpAddress;
                    sb.Append(dr["DrvPoAdd1"] != DBNull.Value && !string.IsNullOrWhiteSpace(tmpAddress = dr["DrvPoAdd1"].ToString())
                                  ? tmpAddress + "\n" : string.Empty);
                    sb.Append(dr["DrvPoAdd2"] != DBNull.Value && !string.IsNullOrWhiteSpace(tmpAddress = dr["DrvPoAdd2"].ToString())
                                  ? tmpAddress + "\n" : string.Empty);
                    sb.Append(dr["DrvPoAdd3"] != DBNull.Value && !string.IsNullOrWhiteSpace(tmpAddress = dr["DrvPoAdd3"].ToString())
                                  ? tmpAddress + "\n" : string.Empty);
                    sb.Append(dr["DrvPoAdd4"] != DBNull.Value && !string.IsNullOrWhiteSpace(tmpAddress = dr["DrvPoAdd4"].ToString())
                                  ? tmpAddress + "\n" : string.Empty);
                    sb.Append(dr["DrvPoAdd5"] != DBNull.Value && !string.IsNullOrWhiteSpace(tmpAddress = dr["DrvPoAdd5"].ToString())
                                  ? tmpAddress + "\n" : string.Empty);
                    sb.Append(dr["DrvPoCode"] != DBNull.Value && !string.IsNullOrWhiteSpace(tmpAddress = dr["DrvPoCode"].ToString())
                                  ? tmpAddress : string.Empty);
                    var offenderAddress = GetMandatoryValue(sb.ToString(), mandatoryValue);

                    var repOfficial = GetMandatoryValue(dr["Repofficial"], mandatoryValue);

                    decimal amount;
                    decimal.TryParse(dr["ChgFineAmount"].ToString(), out amount);

                    repIntNo = repDB.NewUpdateRepresentation(repIntNo, chgIntNo, repDetails, repDate, offenderName, offenderAddress, mandatoryValue, repOfficial, this.loginUser, autIntNo, csCode, amount, "N", "N", ref msg, isSummons);

                    if (repIntNo <= 0)
                    {
                        if (repIntNo == -1)
                        {
                            this.lblError.Text = (string)this.GetLocalResourceObject("lblError.Text3");
                            this.lblError.Visible = true;
                        }
                        else if (repIntNo == -2)
                        {
                            this.lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text4"), msg);
                            this.lblError.Visible = true;
                        }
                        //Jerry 2012-11-01 add
                        else if (repIntNo == -11)
                        {
                            lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text21"), isSummons ? "Summons" : "Notice");
                            lblError.Visible = true;
                        }

                        return false;
                    }

                    int repCode;
                    var withdrawSummons = false;
                    var rowVersionStr = "0";
                    if (!isSummons)
                    {
                        repCode = REPCODE_WITHDRAW;
                        csCode = CODE_CASE_CANCELLED;
                        var chgEntity = new ChargeService().GetByChgIntNo(chgIntNo);
                        if (chgEntity != null)
                            rowVersionStr = BitConverter.ToString(chgEntity.RowVersion).Replace("-", string.Empty);
                    }
                    else
                    {
                        repCode = SUMMONS_REPCODE_WITHDRAW;
                        csCode = CODE_SUMMONS_WITHDRAWN_CHARGE;
                        withdrawSummons = true;
                        var sumChgEntity = new SumChargeService().GetBySchIntNo(chgIntNo);
                        if (sumChgEntity != null)
                            rowVersionStr = BitConverter.ToString(sumChgEntity.RowVersion).Replace("-", string.Empty);
                    }

                    var rowVersion = Convert.ToInt64(rowVersionStr, 16);

                    var result = repDB.DecideRepresentation(repIntNo, repCode, null, repOfficial, recommend, repDate, this.loginUser, chgIntNo, amount, csCode, rowVersion, amount, "N", repDetails, "O", isSummons, "N", notIntNo, "N", "", 0, 0, "", null, false, 0, false, "", withdrawSummons, ref msg, GetNoOfDaysForPayment(autIntNo), this.dataWashingActived, useRepAddress, repAddrExpiryPeriod, true);
                    if (result == null || result.Count <= 0 || result[0] <= 0)
                    {
                        this.lblError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text18"), msg);
                        this.lblError.Visible = true;
                        return false;
                    }

                    if (!isSummons)
                    {
                        SIL.AARTO.DAL.Entities.Notice notice = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(notIntNo);
                        if (notice.NoticeStatus != 410)
                        {
                            PushGenerateSummonsQueue(repIntNo, notIntNo, false);
                        }
                    }

                    if (Transaction.Current != null && Transaction.Current.TransactionInformation.Status == TransactionStatus.Active)
                        scope.Complete();
                    else
                        return false;
                }
            }
            return true;
        }

        T GetMandatoryValue<T>(object value, T mandatoryValue = default(T))
        {
            var result = mandatoryValue;
            if (value == null || value == DBNull.Value) return result;
            try
            {
                result = (T)Convert.ChangeType(value, typeof(T));
            }
            catch { }
            return result;
        }

        List<string> GetOffenceCodeListByIsPresentationOfDocument()
        {
            var offCodeList = new List<string>();
            var paras = new[]
            {
                new SqlParameter("@IsPOD", true)
            };
            new ServiceDB(this.connectionString).ExecuteReader("GetOffenceCodeListByIsPresentationOfDocument", paras, dr =>
            {
                while (dr.Read())
                {
                    offCodeList.Add(dr["OffCode"].ToString().Trim());
                }
            });
            return offCodeList;
        }

        #endregion

        #region Oscar 20120810 added for authorise 2nd rep

        bool CheckManagementOverridePermission(int autIntNo = 0)
        {
            int userIntNo;
            if (Session["userIntNo"] == null
                || !int.TryParse(Session["userIntNo"].ToString(), out userIntNo)
                || userIntNo <= 0
                || (autIntNo <= 0 && (autIntNo = GetAutIntNo()) <= 0)
                )
                return false;

            // Oscar 20121109 ignore when ChangeOfOffender
            if (this.repAction.Equals("O", StringComparison.OrdinalIgnoreCase))
                return true;

            var authRule = new AuthorityRulesDB(this.connectionString).GetAuthorityRule(autIntNo, "4535");
            if (authRule == null || authRule.ARString.Equals("N", StringComparison.OrdinalIgnoreCase)) return true;

            var roleList = new AARTOUserRoleDB(this.connectionString).GetAARTOUserRoleNameByUserID(userIntNo);
            return roleList.Contains("ManagementOverride");
        }

        // only when return 0, can add, decide or reverse
        int GetDecidedRepresentationCount(int chgIntNo, int autIntNo = 0)
        {
            string ticketNo;
            if ((autIntNo <= 0 && (autIntNo = GetAutIntNo()) <= 0)
                || string.IsNullOrWhiteSpace(ticketNo = txtSearch.Text.Trim().Replace(" ", ""))
                )
                return -1;

            // Oscar 20121109 ignore when ChangeOfOffender
            if (this.repAction.Equals("O", StringComparison.OrdinalIgnoreCase))
                return 0;

            var paras = new[]
            {
                new SqlParameter("@AutIntNo", autIntNo),
                new SqlParameter("@TicketNo", ticketNo),
                new SqlParameter("@ChargeIntNo", chgIntNo)
            };
            var obj = new ServiceDB(this.connectionString).ExecuteScalar("GetDecidedRepresentationCount", paras);
            return obj != null ? Convert.ToInt32(obj) : -1;
        }

        // Oscar 2013-02-19 add for 24hours checking
        bool CheckRecommendationDateIn24Hours(object rowDate)
        {
            DateTime recommendDate;
            return rowDate == null
                || rowDate == DBNull.Value
                || !DateTime.TryParse(Convert.ToString(rowDate), out recommendDate)
                || recommendDate.Date.AddDays(1) >= DateTime.Now.Date;
        }

        #endregion

        #region Oscar 20121015 added for Rep Letter Print
        void PostRepLetter(bool isDecision = true)
        {
            // "Insufficent" is misspelled, but it's too late to fix.
            var fileNamePrefix = isDecision ? "RepDecision_" : "RepInsufficentDecision_";
            var letterType = isDecision ? "RepDecision" : "InsufficientDetails";
            var repType = isDecision ? "D" : "I";
            var letterTo = this.isSummons ? "S" : this.rdlLetter.SelectedValue;

            var represent = new RepresentationDB(this.connectionString);
            var isIBMPrinter = IsIBMPrinter();
            if (!isIBMPrinter && !isPrintBatchRepLetter)
            {
                //2011-09-30 jerry print one file at a time.
                represent.RepresentationUpdateForPrintBatch(false, this.isSummons, this.repIntNo, this.autIntNo, true, true, this.loginUser);

                PopupTheRepresentationLetterViewer(letterType, this.repIntNo.ToString(), this.txtTicketNo.Text, letterTo);
            }
            else
            {
                var rlfpQuery = new RepLetterForPrint
                {
                    ShowBlockUI = true,
                    IsIBMPrinter = isIBMPrinter,
                    IsSummons = this.isSummons,
                    AutIntNo = this.autIntNo
                };
                if (!isIBMPrinter)
                {
                    rlfpQuery.PrintFileName = fileNamePrefix + this.autCode + "_" + DateTime.Now.ToString("yyyy-MM-dd");
                    //Jerry 2013-06-18 change it, the repPrintFileName value is "", so it is wrong
                    //represent.RepresentationUpdateForPrintBatch(true, this.isSummons, this.repIntNo, this.autIntNo, false, false, this.loginUser, repPrintFileName);
                    represent.RepresentationUpdateForPrintBatch(true, this.isSummons, this.repIntNo, this.autIntNo, false, false, this.loginUser, rlfpQuery.PrintFileName);
                }
                else
                {
                    string error;
                    represent.UpdateRepresentationForPrint(this.autIntNo, repIntNo, loginUser, repType, this.isSummons, out error);
                    if (this.isSummons)
                    {
                        var spEntity = new SummonsRepresentationService().GetBySrIntNo(this.repIntNo);
                        if (spEntity != null)
                            rlfpQuery.PrintFileName = spEntity.SrPrintFileName;
                    }
                    else
                    {
                        var repEntity = new RepresentationService().GetByRepIntNo(this.repIntNo);
                        if (repEntity != null)
                            rlfpQuery.PrintFileName = repEntity.RepPrintFileName;
                    }
                }
                Session["RepLetterForPrint"] = rlfpQuery;

                var url = string.Format("{0}&LetterType={1}&RepIntNo={2}&TicketNo={3}&LetterTo={4}", this.thisPageURL, letterType, this.repIntNo, this.txtTicketNo.Text.Trim(), letterTo);
                Response.Redirect(url, true);
            }
        }

        void PrintRepLetter(int autIntNo, string fileName, bool onlyUpdateStatus = false)
        {
            //var engine = new FreeStylePrintEngineForGenerateLatters(this.connectionString);
            //var data = engine.CurrentReportDataService.LoadReportDataByPrintFiles(new List<string> { fileName }, autIntNo);
            //var where = string.Format("AutIntNo = {0} AND ReportCode = '{1}'", autIntNo, (int) ReportConfigCodeList.GenerateLetters);
            //var rcList = new ReportConfigService().Find(where);
            //if (rcList != null && rcList.Count > 0)
            //{
            //    if (!onlyUpdateStatus)
            //    {
            //        engine.CurrentReportDataService.InitializeReportConfig(rcList[0].RcIntNo, this.connectionString);
            //        engine.CreateEngine(rcList[0].RcIntNo);
            //        engine.PrintReportWithData(data, true, false);
            //    }               
            //}
            if (onlyUpdateStatus)
            {
                // 2013-10-24, Oscar has moved this into IF condition.
                var engine = new FreeStylePrintEngineForGenerateLatters(this.connectionString);
                var data = engine.CurrentReportDataService.LoadReportDataByPrintFiles(new List<string> { fileName }, autIntNo);

                var userIsNull = GlobalVariates<User>.CurrentUser == null;
                if (userIsNull)
                    GlobalVariates<User>.CurrentUser = new User { UserLoginName = (Session["userDetails"] as UserDetails).UserLoginName };
                engine.CurrentReportDataService.LastUser = this.loginUser; // 2013-07-23 add by Henry
                engine.CurrentReportDataService.ModifyPrintDate(data);
                if (userIsNull)
                    GlobalVariates<User>.CurrentUser = null;
            }
        }

        [Serializable]
        class RepLetterForPrint
        {
            public bool ShowBlockUI { get; set; }
            public bool IsIBMPrinter { get; set; }
            public string PrintFileName { get; set; }
            public bool IsSummons { get; set; }
            public int AutIntNo { get; set; }
        }

        RepLetterForPrint GetRepLetterForPrint()
        {
            return (Session["RepLetterForPrint"] as RepLetterForPrint) ?? new RepLetterForPrint();
        }
        #endregion

        private bool CheckIsHandwrittenCorrection(int chgIntNo, bool isSummons)
        {
            //Jerry 2012-11-01 check is Handwritten Correction
            RepresentationDB repDB = new RepresentationDB(this.connectionString);
            int isHandwritten = repDB.CheckIsHandwrittenCorrrection(chgIntNo, isSummons);
            if (isHandwritten == -11)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 2013-04-03 check Notice or Summons is paid
        /// </summary>
        /// <param name="chgIntNo"></param>
        /// <param name="isSummons"></param>
        /// <returns></returns>
        private bool CheckNoticeOrSummonsIsPaid(int chgIntNo, bool isSummons)
        {
            RepresentationDB repDB = new RepresentationDB(this.connectionString);
            int isHandwritten = repDB.CheckNoticeOrSummonsIsPaid(chgIntNo, isSummons);
            if (isHandwritten == -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Oscar 20130208 added for status check.
        bool CheckStatus(int status, CheckType type)
        {
            if (status > 900)
            {
                switch (type)
                {
                    case CheckType.Add:
                        return false;
                    case CheckType.Reverse:
                        return status == 927 || status == 940;
                }
            }
            return true;
        }

        enum CheckType
        {
            Add,
            Decide,
            Reverse
        }

        // Oscar 2013-02-19 add RepOfficialIsMandatory for change of offender
        bool RepOfficialIsMandatory(int rcCode, bool rule4533)
        {
            return (rcCode != REPCODE_CHANGEOFFENDER && rcCode != SUMMONS_REPCODE_CHANGEOFFENDER) || !rule4533;
        }

        // Oscar 2013-02-19 add drop down list for offical name
        void BindDDLRepOfficial()
        {
            if (cache.RepOfficialSource == null || cache.RepOfficialSource.Count <= 0)
            {
                cache.RepOfficialSource = new RepresentationAuthorisationUserService()
                    .GetAll().OrderBy(r => r.RauName).ThenBy(r => r.RauForceNumber)
                    .Select(r => new SelectListItem(r.RauName, r.RauName)).ToList();
            }

            if (cache.RepOfficialSource.FirstOrDefault(r => string.IsNullOrWhiteSpace(r.Value)) == null)
                cache.RepOfficialSource.Insert(0, new SelectListItem(Convert.ToString(GetLocalResourceObject("SelectOfficialName")), ""));

            this.ddlRepOfficial.DataSource = cache.RepOfficialSource;
            this.ddlRepOfficial.DataTextField = "Text";
            this.ddlRepOfficial.DataValueField = "Value";
            this.ddlRepOfficial.DataBind();

            this.ddlRepOfficial.SelectedIndex = 0;
            this.ddlRepOfficial.Enabled = true;
        }

        protected void ddlRepOfficial_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (this.ddlRepOfficial.SelectedIndex <= 0) return;

            this.txtRepOfficial.Text = this.ddlRepOfficial.SelectedValue;
        }

        // 2013-08-29, Oscar added for unmatched official
        void FindRepOfficial(int repCode, string official)
        {
            // 2013-12-06, Oscar added "&& !this.isNew"
            var requireLock = !repCode.In(REPCODE_NONE, SUMMONS_REPCODE_NONE) && !this.isNew;
            this.ddlRepOfficial.Enabled = !requireLock;

            if (this.cache.RepOfficialSource != null
                && this.cache.RepOfficialSource.Count > 0)
            {
                int index;
                if ((index = this.cache.RepOfficialSource.FindIndex(o => o.Value.Equals2(official))) > 0)
                {
                    this.ddlRepOfficial.SelectedIndex = index;
                }
                else
                {
                    this.ddlRepOfficial.SelectedIndex = requireLock ? -1 : 0;

                    if (!string.IsNullOrWhiteSpace(official))
                    {
                        this.lblRepOfficial.Text = official;
                        this.lblRepOfficial.Visible = true;
                    }
                }
            }
            else
            {
                this.ddlRepOfficial.SelectedIndex = -1;
            }
        }

        #region Oscar 2013-05-23, NoAOG checking

        bool RequireBlockNoAOGOffence()
        {
            if (cache.IsSection35.GetValueOrDefault())
            {
                return allowedRepForS35 == "N";
            }
            else
            {
                return cache.IsNoAOG.GetValueOrDefault() && !cache.AllowNoAOGOffence.GetValueOrDefault();
            }
        }

        bool ShowNoAOGAlert()
        {
            this.blockNoAOG.Visible = false;
            if (RequireBlockNoAOGOffence())
            {
                this.blockNoAOG.Visible = true;
                return true;
            }
            return false;
        }

        void BlockNoAOGOffence()
        {
            cache.TempRevFineAmount = this.txtRevAmount.Text;

            var block = RequireBlockNoAOGOffence();
            this.txtRevAmount.Enabled = !block;

            for (var i = 0; i < this.rblCodes.Items.Count; i++)
            {
                var item = this.rblCodes.Items[i];
                if ((item.Value.Equals(REPCODE_REDUCTION.ToString())
                    || item.Value.Equals(SUMMONS_REPCODE_REDUCTION.ToString()))
                    && cache.IsSection35.GetValueOrDefault() == false)
                    item.Enabled = !block;
            }
        }

        bool InvalidNoAOGOffenceInput(bool onlyCheckNoAog)
        {
            if (onlyCheckNoAog)
            {
                if (RequireBlockNoAOGOffence())
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = String.Format((string)GetLocalResourceObject("NoAOG_RepresentationNotAllowed_1"), this.txtTicketNo.Text);
                    return true;
                }
            }
            else
            {
                if (RequireBlockNoAOGOffence()
                    && (this.txtRevAmount.Text != cache.TempRevFineAmount
                        || this.rblCodes.SelectedValue == REPCODE_REDUCTION.ToString()
                        || this.rblCodes.SelectedValue == SUMMONS_REPCODE_REDUCTION.ToString()))
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = (string)GetLocalResourceObject("NoAOG_RepresentationNotAllowed");
                    return true;
                }
            }
            return false;
        }

        void ContinueShowRepresentations(bool isContinue)
        {
            this.blockNoAOG.Visible = false;
            if (isContinue)
            {
                var chgIntNo = Convert.ToInt32(ViewState["ChgIntNo"]);
                var dtCourtDate = cache.CourtDate ?? DateTime.MaxValue;
                grdRepresentationBind(chgIntNo, dtCourtDate);
            }
        }

        protected void btnBlockNoAOGYes_Click(object sender, EventArgs e)
        {
            ContinueShowRepresentations(true);
        }

        protected void btnBlockNoAOGNo_Click(object sender, EventArgs e)
        {
            ContinueShowRepresentations(false);
        }

        #endregion

        #region Oscar 2013-06-05, lock RevAmount when withdrawn

        int GetRepCode()
        {
            int code;
            int.TryParse(this.rblCodes.SelectedValue, out code);
            if (this.chkChangeOffender.Checked)
            {
                switch (code)
                {
                    case REPCODE_NONE:
                        code = REPCODE_CHANGEOFFENDER;
                        break;
                    case SUMMONS_REPCODE_NONE:
                        code = SUMMONS_REPCODE_CHANGEOFFENDER;
                        break;
                }
            }
            return code;
        }

        void LockRevAmountForWithdrawn()
        {
            var code = GetRepCode();
            var lockRevAmout = !(code == REPCODE_REDUCTION
                || code == REPCODE_CHANGEOFFENDER
                || code == SUMMONS_REPCODE_REDUCTION
                || code == SUMMONS_REPCODE_CHANGEOFFENDER);
            this.txtRevAmount.Enabled = !lockRevAmout && !RequireBlockNoAOGOffence();
            if (lockRevAmout)
                this.txtRevAmount.Text = cache.TempRevFineAmount;
        }

        #endregion

    }
}