﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page where a synopsis of offences can be displayed as the result of a search
    /// 20090420 tf  ---modify enquiry function, add SummonsNO,  CASENO,  WOANO search,  and modify the page of the enquiry.
    /// </summary>
    public partial class ViewHistoricalSummons_Search : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string login;

        protected string _styleSheet;
        protected string _background;
        protected string _thisPage = "ViewHistoricalSummons_Search.aspx";
        protected string _keywords = string.Empty;
        protected string _title = string.Empty;
        protected string _description = string.Empty;
        protected string _strEnquiryCondition = "";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        //protected DateTime dtpSince = Convert.ToDateTime("2005-01-01");

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            _background = gen.SetBackground(Session["drBackground"]);
            _styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            _title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = _styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.pnlResults.Visible = false;
                this.noticeNoLookup.AutIntNo = autIntNo;
                txtRegNo.Focus();
            }
        }

        private void BindGrid()
        {
            // dls 080314 - need to clear the previous list of notices
            Session["NoticeList"] = null;

            SummonsDB sumWithdrawnReissue = new SummonsDB(this.connectionString);
            this.pnlResults.Visible = false;

            // Check the authority rule for minimum charge status
            int minStatus = this.CheckMinimumStatusRule();

            // Select which data set to generate
            NoticeQueryCriteria criteria = this.GetQueryCriteria();
            if (criteria == null)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            //remove slashes and dashes from input value.
            criteria.Value = criteria.Value.Replace("/", "").Replace("-", "");

            // Bind the data
            //dls 090601 - seearch for all notices regardless of the LA into which the user has logged
            //DataSet ds = notice.GetNoticeSearchDS(criteria.AutIntNo, criteria.ColumnName, criteria.Value, criteria.DateSince, minStatus);
            DataSet ds = sumWithdrawnReissue.GetSumWithdrawnReissueSearchDS(0, criteria.ColumnName, criteria.Value, criteria.DateSince, minStatus);
            grvOffenceList.DataSource = ds.Tables[1];
            grvOffenceList.DataKeyNames = new string[] { "NotIntNo", "Representation" };
            grvOffenceList.DataBind();

            // Check if there are any results
            if (grvOffenceList.Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
                this.pnlResults.Visible = true;

            FocusControl();
        }

        private void FocusControl()
        {
            switch (SearchCondition.Value.Replace("mypanel1", ""))
            {
                case "RegNo":
                    txtRegNo.Focus();
                    break;
                case "IDNO":
                    txtIDNO.Focus();
                    break;
                case "NoticeNO":
                    txtNoticeNO.Focus();
                    break;
                case "SummonsNO":
                    txtSummonsNO.Focus();
                    break;
                case "CASENO":
                    txtCASENO.Focus();
                    break;
                case "WOANO":
                    txtWOANO.Focus();
                    break;
                case "Name":
                    txtName.Focus();
                    break;
            }
        }

        private int CheckMinimumStatusRule()
        {
            #region Jerry 2012-06-28 change
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4605";
            //rule.ARComment = "Default = 255 (posted).";
            //rule.ARDescr = "Status to start showing notices on enquiry screen";
            //rule.ARNumeric = 255;
            //rule.ARString = string.Empty;
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);

            //return rule.ARNumeric;
            #endregion

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.ARCode = "4605";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.login;

            DefaultAuthRules ar = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> idRule = ar.SetDefaultAuthRule();

            return idRule.Key;
        }

        /// <summary>
        /// Get query parameters for the stored procdure NoticeSearch.
        /// 20090417 tf --Add SummonsNO, CASENO, WOANO for search, and replace if else statement by switch statement
        /// 20090421 tf --Replace dojo calendar with ajaxtoolkit calendar. 
        /// </summary>
        /// <returns></returns>
        private NoticeQueryCriteria GetQueryCriteria()
        {
            // Check the authority rule for minimum charge status
            int minStatus = this.CheckMinimumStatusRule();

            //dls 090612 - replace all autintno with 0 so that we can search across all LA's
            switch (SearchCondition.Value.Replace("mypanel1", ""))
            {
                case "RegNo":
                    //return new NoticeQueryCriteria(this.autIntNo, "RegNo", txtRegNo.Text.Replace(" ", string.Empty), this.dtpSince.Text);
                    return new NoticeQueryCriteria(0, "RegNo", txtRegNo.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "IDNO":
                    return new NoticeQueryCriteria(0, "IDNo", txtIDNO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "NoticeNO":

                    return new NoticeQueryCriteria(0, "TicketNo", txtNoticeNO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "SummonsNO":

                    return new NoticeQueryCriteria(0, "SummonsNO", txtSummonsNO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "CASENO":

                    return new NoticeQueryCriteria(0, "CaseNO", txtCASENO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "WOANO":

                    return new NoticeQueryCriteria(0, "WOANO", txtWOANO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "Name":
                    return new NoticeQueryCriteria(0, "Names", txtInitials.Text + "*" + txtName.Text, this.dtpSince.Text, minStatus);
            }
            return null;


            //if (!txtRegNo.Text.Trim().Equals(string.Empty))
            //    return new NoticeQueryCriteria(this.autIntNo, "RegNo", txtRegNo.Text.Replace(" ", string.Empty), this.dtpSince == null ? string.Empty : this.dtpSince.ToString(DATE_FORMAT));
            //else if (!txtTicketNo.Text.Trim().Equals(string.Empty))
            //    return new NoticeQueryCriteria(this.autIntNo, "TicketNo", txtTicketNo.Text.Replace(" ", string.Empty), this.dtpSince == null ? string.Empty : this.dtpSince.ToString(DATE_FORMAT));
            //else if (!txtIDNo.Text.Trim().Equals(string.Empty))
            //    return new NoticeQueryCriteria(this.autIntNo, "IDNo", txtIDNo.Text.Replace(" ", string.Empty), this.dtpSince == null ? string.Empty : this.dtpSince.ToString(DATE_FORMAT));
            //else if (!txtSurname.Text.Equals(string.Empty) || !txtInitials.Text.Equals(string.Empty))
            //    return new NoticeQueryCriteria(this.autIntNo, "Names", txtInitials.Text + "*" + txtSurname.Text, this.dtpSince == null ? string.Empty : this.dtpSince.ToString(DATE_FORMAT));
            //else
            //    return null;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindGrid();
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            NoticeQueryCriteria criteria = this.GetQueryCriteria();
            if (criteria == null)
                return;

            Session.Add("NoticeQueryCriteria", criteria);
            Helper_Web.BuildPopup(this, "ViewHistoricalSummons_Report.aspx", null);
            BindGrid();
        }



        /// <summary>
        /// Clicked the lookup button of noticeLookup control .
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void NoticeLookup(object sender, EventArgs e)
        {
            this.txtNoticeNO.Text = noticeNoLookup.Return;
            SearchCondition.Value = "mypanel1NoticeNO";
            BindGrid();
        }

    }
}

