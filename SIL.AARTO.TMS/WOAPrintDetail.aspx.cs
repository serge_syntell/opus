﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stalberg.TMS;
using System.Text;

public partial class WOAPrintDetail : System.Web.UI.Page
{

    private string connectionString = string.Empty;
    private string login;
    private int autIntNo = 0;
    private string cType = "SUM";

    protected string styleSheet;
    protected string backgroundImage;

    protected string keywords = string.Empty;
    protected string title = string.Empty;
    protected string description = string.Empty;
    //protected string thisPage = "Print file detail: Separate WOA for Authorisation";
    protected string printFileName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.connectionString = Application["constr"].ToString();

        //get user info from session variable
        if (Session["userDetails"] == null)
            Server.Transfer("Login.aspx?Login=invalid");
        if (Session["userIntNo"] == null)
            Server.Transfer("Login.aspx?Login=invalid");

        //get user details
        Stalberg.TMS.UserDB user = new UserDB(connectionString);
        Stalberg.TMS.UserDetails userDetails = new UserDetails();

        userDetails = (UserDetails)Session["userDetails"];
        this.login = userDetails.UserLoginName;

        //int 
        autIntNo = Convert.ToInt32(Session["autIntNo"]);

        Session["userLoginName"] = userDetails.UserLoginName.ToString();
        int userAccessLevel = userDetails.UserAccessLevel;

        // Set domain specific variables
        General gen = new General();
        backgroundImage = gen.SetBackground(Session["drBackground"]);
        styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        title = gen.SetTitle(Session["drTitle"]);

        printFileName = Request.QueryString["woaPrintFile"];

        if (!Page.IsPostBack)
        {

            if (!String.IsNullOrEmpty(printFileName))
            {
                lblPrintFileName.Text = printFileName;
                this.BindGrid(printFileName);
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
        }
    }

    private void BindGrid(string printFileName)
    {
        this.btnUpdatePrintFileName.Visible = true;
        Stalberg.TMS.WOADB printList = new Stalberg.TMS.WOADB(connectionString);

        try
        {
            int totalCount = 0;//2013-03-27 add by Henry for pagination
            dgWoaPrintDetail.DataSource = printList.GetWOAPrintFileDetailSelect(printFileName, dgWoaPrintDetailPager.PageSize, dgWoaPrintDetailPager.CurrentPageIndex, out totalCount);
            dgWoaPrintDetail.DataKeyField = "WOAIntNo";
            dgWoaPrintDetail.DataBind();
            dgWoaPrintDetailPager.RecordCount = totalCount;
        }
        catch(Exception ex)
        {
            dgWoaPrintDetail.CurrentPageIndex = 0;
            dgWoaPrintDetail.DataBind();
            throw ex;
        }

        if (dgWoaPrintDetail.Items.Count == 0)
        {
            dgWoaPrintDetail.Visible = false;
            //lblError.Visible = true;
            //lblError.Text = "There are no print files available for printing";
        }
        else if (dgWoaPrintDetail.Items.Count == 1)
        {
            foreach (DataGridItem item in dgWoaPrintDetail.Items)
            {
                ((CheckBox)item.FindControl("chbWoa")).Visible = false;
            }
            this.btnUpdatePrintFileName.Visible = false;
        }
        else
        {
            foreach (DataGridItem item in dgWoaPrintDetail.Items)
            {
                if (item.Cells[11].Text == "1")
                {
                    ((CheckBox)item.FindControl("chbWoa")).Visible = false;
                    this.btnUpdatePrintFileName.Visible = false;
                }
            }

            dgWoaPrintDetail.Visible = true;
            lblError.Visible = false;
        }

    }

    //private string GetCheckedWoaIntNo()
    //{
    //    string woaIntNoStr = string.Empty;


    //}
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("PrintWOA.aspx?authIntNo={0}", Request.QueryString["authIntNo"]));
    }
    protected void btnUpdatePrintFileName_Click(object sender, EventArgs e)
    {
        StringBuilder woaIntNoStr = new StringBuilder();
        foreach (DataGridItem item in dgWoaPrintDetail.Items)
        {
            if (((CheckBox)item.FindControl("chbWoa")).Checked)
            {
                woaIntNoStr.Append(item.Cells[0].Text).Append(",");
            }
        }

        if (woaIntNoStr.Length <= 0)
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
        }
        if (woaIntNoStr.Length > 0 && !String.IsNullOrEmpty(printFileName))
        {
            Stalberg.TMS.WOADB printList = new Stalberg.TMS.WOADB(connectionString);

            if (printList.UpdateWOAPrintFileName(printFileName, woaIntNoStr.ToString(), this.login))
            {
                //lblError.Text = "Update Woa print file name successfully";
                Response.Redirect(String.Format("PrintWOA.aspx?authIntNo={0}", Request.QueryString["authIntNo"]));
            }
        }

        //Jerry 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
        //2013-03-27 add by Henry for pagination
        //dgWoaPrintDetailPager.RecordCount = 0;
        //dgWoaPrintDetailPager.CurrentPageIndex = 1;
        this.BindGrid(printFileName);
    }

    //2013-03-27 add by Henry for pagination
    protected void dgWoaPrintDetailPager_PageChanged(object sender, EventArgs e)
    {
        this.BindGrid(printFileName);
    }
}