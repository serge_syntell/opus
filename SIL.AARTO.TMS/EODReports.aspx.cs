using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class EODReports : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = "Template";
        protected string description = String.Empty;
        protected string thisPageURL = "EODReports.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                //dls 080111 - the report selection has changed to select by cashbox instead of user/cashier
                //PopulateCashiers();
                PopulateAuthorites();
                PopulateCashboxes();
                this.calEODReportDate.SelectedDate = DateTime.Today;
            }
        }

        protected void PopulateAuthorites()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);
            
            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");
            
            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count;i++ )
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(this.autIntNo.ToString()));
        }

        public void PopulateCashboxes()
        {
            CashboxDB cashbox = new CashboxDB(connectionString);
            
            this.ddlCashBox.DataSource = cashbox.GetCashboxListDS();
            this.ddlCashBox.DataValueField = "CBIntNo";
            this.ddlCashBox.DataTextField = "CBName";
            this.ddlCashBox.DataBind();
            this.ddlCashBox.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCashBoxItem.Text"), "0"));
        }

         

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DateTime dt = this.calEODReportDate.SelectedDate;
            //dls 080111 - the report selection has changed to select by cashbox instead of user/cashier
            Helper_Web.BuildPopup(this, "ReceiptEODBalancingViewer.aspx", "Date", dt.ToString("yyyy-MM-dd"), "cbIntNo", ddlCashBox.SelectedValue, "cbName", ddlCashBox.SelectedItem.Text,
                "AutIntNo", ddlSelectLA.SelectedValue);
        }

    }
}
