using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;


namespace Stalberg.TMS
{
    /// <summary>
    /// The starting point for ticket payment receipt
    /// </summary>
    public partial class SummonsErrorViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsErrorViewer.aspx";
        //protected string thisPage = "Summons Error Viewer";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;
            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Make sure the SQ is right
            if (Request.QueryString["Status"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Request.QueryString["AutIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            int state = int.Parse(Request.QueryString["Status"].ToString());
            this.autIntNo = int.Parse(Request.QueryString["AutIntNo"].ToString());

            if (!Page.IsPostBack)
            {
                NoticeReportDB notice = new NoticeReportDB(this.connectionString);

                // Create a report object
                ReportDocument report = new ReportDocument();
                DataSet ds = new DataSet();
                string reportFile = string.Empty;
                string reportPage = string.Empty;

                switch (state)
                {
                    case 603:
                        ds = notice.GetSummonsServerErrorList(this.autIntNo);

                        reportPage = "SummonsServerErrors.rpt";
                        reportFile = Server.MapPath("Reports/" + reportPage);
                        break;

                    case 606:
                        //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
                        //DateRulesDetails rule = new DateRulesDetails();
                        //rule.AutIntNo = autIntNo;
                        //rule.DtRDescr = "Min no. of days from FIRST NOTICE POST DATE to SUMMONS ISSUE DATE";
                        //rule.DtREndDate = "NotIssueSummonsDate";
                        //rule.DtRNoOfDays = 90;
                        //rule.DtRStartDate = "NotPosted1stNoticeDate";
                        //rule.LastUser = "TMS";
                        //rule = dateRule.GetDefaultDateRule(rule);

                        //AutIntNo, StartDate, EndDate and LastUser must be set up before the default rule is called
                        DateRulesDetails rule = new DateRulesDetails();
                        rule.AutIntNo = autIntNo;
                        rule.LastUser = this.login;
                        rule.DtRStartDate = "NotPosted1stNoticeDate";
                        rule.DtREndDate = "NotIssueSummonsDate";

                        DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
                        int noOfDays = dateRule.SetDefaultDateRule();

                        //ds = notice.GetSummonsNoCourtDates(this.autIntNo, rule.DtRNoOfDays);
                        ds = notice.GetSummonsNoCourtDates(this.autIntNo, noOfDays);

                        reportPage = "SummonsCourtDateErrors.rpt";
                        reportFile = Server.MapPath("Reports/" + reportPage);
                        break;
                }

                if (!File.Exists(reportFile))
                {
                    string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }

                // Set the report's data source
                report.Load(reportFile);

                report.SetDataSource(ds.Tables[1]);
                ds.Dispose();

                // Export the PDF file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat);
                report.Dispose();

                //stuff the PDF file into rendering stream
                //first clear everything dynamically created and just send PDF file
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }
        }

    }
}
