using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Stalberg.TMS;
using System.Data;
using SIL.AARTO.BLL.Utility.Cache;

/// <summary>
/// Represents an editor for processing on-the-fly representations
/// </summary>
public partial class RepresentationOnTheFly : System.Web.UI.UserControl
{
    // Fields
    private string connectionString;
    private string login;
    private int autIntNo;
    private bool mandatoryPhoneNo;
    private decimal originalAmount;
    private decimal newAmount;
    private string ticketNumber;
    private bool isSummons;
    private int noOfDaysForPayment = 0;
    private int maxStatus = 900;

    // Constants
    private const int CHANGE_FINE_REP_CODE = 4;
    //private const int SUMMONS_REP_FINECHANGED = 210;
    private const int SUMMONS_REPCODE_REDUCTION = 14;

    // Events
    public event RepresentationOnTheFlyEventHandler RepresentationSuccessful;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.connectionString = Application["constr"].ToString();

        UserDetails userDetails = new UserDetails();
        userDetails = (UserDetails)Session["userDetails"];
        login = userDetails.UserLoginName;

        this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

        // Original fine amount
        if (this.ViewState["RepresentationOnTheFlyOriginalAmount"] != null)
            this.originalAmount = (decimal)this.ViewState["RepresentationOnTheFlyOriginalAmount"];

        if (this.ViewState["RepresentationOnTheFlyAutIntNo"] != null)
            this.autIntNo = (int)this.ViewState["RepresentationOnTheFlyAutIntNo"];

        if (this.ViewState["RepresentationOnTheFlyNewAmount"] != null)
            this.newAmount = (decimal)this.ViewState["RepresentationOnTheFlyNewAmount"];

        if (this.ViewState["RepresentationOnTheFlyTicketNumber"] != null)
            this.ticketNumber = (string)this.ViewState["RepresentationOnTheFlyTicketNumber"];

        if (this.ViewState["RepresentationOnTheFlyIsSummons"] != null)
            this.isSummons = (bool)this.ViewState["RepresentationOnTheFlyIsSummons"];

        // Check maximum payable status
        this.CheckMaximumStatusRule();

        if (!this.Page.IsPostBack)
        {
            this.noOfDaysForPayment = this.GetNoOfDaysForPayment(autIntNo);
        }
    }

    private void CheckMaximumStatusRule()
    {
        if (this.ViewState["MaxStatus"] != null)
            this.maxStatus = (int)ViewState["MaxStatus"];
        else
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4570";
            //rule.ARComment = "900 (Default); See ChargeStatus table for allowable values (status must be 255 or greater)";
            //rule.ARDescr = "Rule to set maximum charge status at which payment/representations are allowed";
            //rule.ARNumeric = 900;
            //rule.ARString = string.Empty;
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "4570";
            rule.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();


            this.maxStatus = value.Key; //rule.ARNumeric;
            this.ViewState.Add("MaxStatus", this.maxStatus);
        }
    }

    private int GetNoOfDaysForPayment(int autIntNo)
    {
        AuthorityRulesDetails arule = new AuthorityRulesDetails();
        arule.AutIntNo = autIntNo;
        arule.ARCode = "4660";
        arule.LastUser = this.login;

        DefaultAuthRules paymentRule = new DefaultAuthRules(arule, this.connectionString);
        KeyValuePair<int, string> payment = paymentRule.SetDefaultAuthRule();

        return payment.Key;
    }
    /// <summary>
    /// Binds the data to the control.
    /// </summary>
    public void BindData()
    {
        if (string.IsNullOrEmpty(this.ticketNumber))
            throw new ApplicationException((string)GetLocalResourceObject("applicationException"));

        if (this.autIntNo < 1)
            throw new ApplicationException((string)GetLocalResourceObject("applicationException1"));

        if (this.originalAmount < 1)
            throw new ApplicationException((string)GetLocalResourceObject("applicationException2"));

        // Check if the phone number is mandatory
        //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
        if (this.ViewState["mandatoryPhoneNo"] == null)
        {
            //AuthorityRulesDetails ard = db.GetAuthorityRulesDetailsByCode(this.autIntNo, "4520", "Rule to make the receipt telephone number optional", 0, "N", "Y = Yes; N= No(default) mandatory", this.login);
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "4520";
            rule.LastUser = login;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
            
            if (value.Value == "Y")
                this.mandatoryPhoneNo = false;
            this.ViewState["mandatoryPhoneNo"] = this.mandatoryPhoneNo;
        }
        else
            this.mandatoryPhoneNo = (bool)this.ViewState["mandatoryPhoneNo"];

        // Setup controls
        this.PopulateOfficerCombo();

        // Populate the offender details
        this.GetRepresentationDetails();
    }

    /// <summary>
    /// Gets or sets the new amount.
    /// </summary>
    /// <value>The new amount.</value>
    public decimal OriginalAmount
    {
        get { return this.originalAmount; }
        set
        {
            this.originalAmount = value;
            this.ViewState.Add("RepresentationOnTheFlyOriginalAmount", value);
        }
    }

    /// <summary>
    /// Gets or sets the new amount.
    /// </summary>
    /// <value>The new amount.</value>
    public decimal NewAmount
    {
        get { return this.newAmount; }
        set
        {
            this.newAmount = value;
            this.ViewState.Add("RepresentationOnTheFlyNewAmount", value);
            this.lblAmount.Text = "R " + value.ToString("#,##0");
            this.txtRepresentationDetails.Focus();
        }
    }

    /// <summary>
    /// Gets or sets the ticket number to make representation against.
    /// </summary>
    /// <value>The ticket number.</value>
    public string TicketNumber
    {
        get { return this.ticketNumber; }
        set
        {
            this.ticketNumber = value;
            this.ViewState.Add("RepresentationOnTheFlyTicketNumber", value);
        }
    }

    /// <summary>
    /// Gets or sets the authority int no.
    /// </summary>
    /// <value>The aut int no.</value>
    public int AutIntNo
    {
        get { return this.autIntNo; }
        set
        {
            this.autIntNo = value;
            this.ViewState.Add("RepresentationOnTheFlyAutIntNo", value);
        }
    }

    private void GetRepresentationDetails()
    {
        this.txtRepresentationDetails.Text = string.Empty;
        this.txtRepresentationPhone.Text = string.Empty;
        this.txtRepresentationName.Text = string.Empty;
        this.txtRepresentationAddress.Text = string.Empty;
        this.isSummons = false;

        NoticeDB db = new NoticeDB(this.connectionString);
        //SqlDataReader reader = db.FindNoticeForElectronicPayment(this.ticketNumber, this.maxStatus);

        //if (reader.NextResult())
        //{
        //    if (reader.Read())
        //    {
        //        this.txtRepresentationName.Text = Helper.GetReaderValue<string>(reader, "FullName");
        //        StringBuilder sb = new StringBuilder();
        //        if (reader["Address1"] != DBNull.Value && reader["Address1"].ToString().Trim().Length > 0)
        //            sb.Append((string)reader["Address1"] + "\n");
        //        if (reader["Address2"] != DBNull.Value && reader["Address2"].ToString().Trim().Length > 0)
        //            sb.Append((string)reader["Address2"] + "\n");
        //        if (reader["Address3"] != DBNull.Value && reader["Address3"].ToString().Trim().Length > 0)
        //            sb.Append((string)reader["Address3"] + "\n");
        //        if (reader["Address4"] != DBNull.Value && reader["Address4"].ToString().Trim().Length > 0)
        //            sb.Append((string)reader["Address4"] + "\n");
        //        if (reader["Address5"] != DBNull.Value && reader["Address5"].ToString().Trim().Length > 0)
        //            sb.Append((string)reader["Address5"] + "\n");
        //        if (reader["AddressCode"] != DBNull.Value && reader["AddressCode"].ToString().Trim().Length > 0)
        //            sb.Append((string)reader["AddressCode"]);

        //        this.txtRepresentationAddress.Text = sb.ToString();

        //        this.isSummons = Helper.GetReaderValue<bool>(reader, "IsSummons");
        //        this.ViewState.Add("RepresentationOnTheFlyIsSummons", this.isSummons);
        //    }
        //}
        //reader.Close();

        //dls 100202 - Authrule removed from SP

        DataSet ds = db.FindNoticeForElectronicPayment(this.ticketNumber, this.maxStatus);

        int tableID = 0;

        if (ds.Tables.Count > 1)
            tableID = 1;

        if (ds.Tables[tableID].Rows.Count > 0)
        {
            DataRow dr = ds.Tables[tableID].Rows[0];
            this.txtRepresentationName.Text = Helper.GetDataRowValue<string>(dr, "FullName");
            StringBuilder sb = new StringBuilder();
            if (dr["Address1"] != DBNull.Value && dr["Address1"].ToString().Trim().Length > 0)
                sb.Append((string)dr["Address1"] + "\n");
            if (dr["Address2"] != DBNull.Value && dr["Address2"].ToString().Trim().Length > 0)
                sb.Append((string)dr["Address2"] + "\n");
            if (dr["Address3"] != DBNull.Value && dr["Address3"].ToString().Trim().Length > 0)
                sb.Append((string)dr["Address3"] + "\n");
            if (dr["Address4"] != DBNull.Value && dr["Address4"].ToString().Trim().Length > 0)
                sb.Append((string)dr["Address4"] + "\n");
            if (dr["Address5"] != DBNull.Value && dr["Address5"].ToString().Trim().Length > 0)
                sb.Append((string)dr["Address5"] + "\n");
            if (dr["AddressCode"] != DBNull.Value && dr["AddressCode"].ToString().Trim().Length > 0)
                sb.Append((string)dr["AddressCode"]);

            this.txtRepresentationAddress.Text = sb.ToString();

            this.isSummons = Helper.GetDataRowValue<bool>(dr, "IsSummons");
            this.ViewState.Add("RepresentationOnTheFlyIsSummons", this.isSummons);
        }
        
        ds.Dispose();
    }

    private void PopulateOfficerCombo()
    {
        //Jerry 2014-04-10 clear items firstly by Paole required
        this.cboRepresentationOfficial.Items.Clear();

        TrafficOfficerDB db = new TrafficOfficerDB(this.connectionString);
        SqlDataReader reader = db.GetTrafficOfficerListByAuthority(this.autIntNo, OfficerSeniority.Senior);
        this.cboRepresentationOfficial.DataSource = reader;
        while (reader.Read())
            this.cboRepresentationOfficial.Items.Add(Helper.GetReaderValue<string>(reader, "TODescr"));
        reader.Close();

        this.cboRepresentationOfficial.Items.Insert(0, "Public Prosecutor");
    }

    protected void btnRepresentation_Click(object sender, EventArgs e)
    {
        bool response = true;
        StringBuilder sb = new StringBuilder();
        sb.Append("<ul>"+(string)GetLocalResourceObject("lblRepresentationError.Text")+"\n");

        string details = this.txtRepresentationDetails.Text.Trim();
        if (details.Length == 0)
        {
            response = false;
            sb.Append("<li>"+(string)GetLocalResourceObject("lblRepresentationError.Text1")+"</li>\n");
        }
        string name = this.txtRepresentationName.Text.Trim();
        if (name.Length == 0)
        {
            response = false;
            sb.Append("<li>"+(string)GetLocalResourceObject("lblRepresentationError.Text2")+"</li>\n");
        }
        string address = this.txtRepresentationAddress.Text.Trim();
        if (address.Length == 0)
        {
            response = false;
            sb.Append("<li>"+(string)GetLocalResourceObject("lblRepresentationError.Text3")+"</li>\n");
        }
        string phone = this.txtRepresentationPhone.Text.Trim();
        if (mandatoryPhoneNo)
        {
            if (phone.Length == 0)
            {
                response = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("lblRepresentationError.Text4")+"</li>\n");
            }
        }
        string official = string.Empty;
        if (this.cboRepresentationOfficial.SelectedIndex == -1)
        {
            response = false;
            sb.Append("<li>"+(string)GetLocalResourceObject("lblRepresentationError.Text5")+"</li>\n");
        }
        else
            official = this.cboRepresentationOfficial.SelectedValue;
        if (this.newAmount <= 0)
        {
            response = false;
            sb.Append("<li>" + (string)GetLocalResourceObject("lblRepresentationError.Text6") + "</li>\n");
        }

        sb.Append("</ul>\n");
        if (!response)
        {
            this.lblRepresentationError.Text = sb.ToString();
            return;
        }

        RepresentationDB db = new RepresentationDB(this.connectionString);
        SqlDataReader reader = db.NewSearchRepresentations(this.autIntNo, this.ticketNumber, 0, "R", "CDCourtRollCreatedDate");

        // The details are in the second result set
        //reader.NextResult();

        if (!reader.HasRows)
        {
            this.lblRepresentationError.Text = (string)GetLocalResourceObject("lblRepresentationError.Text7");
            return;
        }
        reader.Read();
        int chgIntNo = (int)reader["ChgIntNo"];
        int csCode = (int)reader["RepCSCode"];
        reader.Close();

        // Make the Representation
        string errMsg = string.Empty;
        // need extra code in here to tell the proc that this is RepType = 'R' (On-the-fly representation)
        int repIntNo = db.NewUpdateRepresentation(0, chgIntNo, details, DateTime.Now, name, address, phone, official, login,
            this.autIntNo, csCode, this.originalAmount, "N", "R", ref errMsg, this.isSummons);
        if (repIntNo == -2)
        {
            this.lblRepresentationError.Text = (string)GetLocalResourceObject("lblRepresentationError.Text8") +
                errMsg;
            return;
        }
        //Jerry 2012-11-01 add
        else if (repIntNo == -11)
        {
            this.lblRepresentationError.Text = string.Format((string)this.GetLocalResourceObject("lblError.Text21"), isSummons ? "Summons" : "Notice");
            return;
        }

        // Get the Representation details with the new Charge RowVersion
        ChargeDB chgDb = new ChargeDB(connectionString);
        ChargeDetails charge = chgDb.GetChargeDetails(chgIntNo);

        string errMessage = string.Empty;

        // Make the representation decision
        Int32 result = db.DecideRepresentation(repIntNo, this.isSummons ? SUMMONS_REPCODE_REDUCTION : CHANGE_FINE_REP_CODE, null, official, details, DateTime.Now,
            login, chgIntNo, this.newAmount, csCode, charge.RowVersion, this.originalAmount, "N", details, "O", this.isSummons, "N", ref errMessage, this.noOfDaysForPayment);

        if (result < 0)
        {
            this.lblRepresentationError.Text = (string)GetLocalResourceObject("lblRepresentationError.Text9");
            return;
        }

        // Fire the successful event if has got this far
        if (RepresentationSuccessful != null)
            RepresentationSuccessful(this, new RepresentationOnTheFlyEventArgs(repIntNo, true, this.ticketNumber, this.newAmount));
    }

}

public class RepresentationOnTheFlyEventArgs : EventArgs
{
    // Fields
    private int repIntNo;
    private bool success;
    private string ticketNo;
    private decimal newAmount;

    /// <summary>
    /// Initializes a new instance of the <see cref="RepresentationOnTheFlyEventArgs"/> class.
    /// </summary>
    /// <param name="repIntNo">The rep int no.</param>
    /// <param name="success">if set to <c>true</c> the representation was a success.</param>
    public RepresentationOnTheFlyEventArgs(int repIntNo, bool success, string ticketNo, decimal newAmount)
    {
        this.repIntNo = repIntNo;
        this.success = success;
        this.ticketNo = ticketNo;
        this.newAmount = newAmount;
    }

    /// <summary>
    /// Gets the new charge amount.
    /// </summary>
    /// <value>The new amount.</value>
    public decimal NewAmount
    {
        get { return this.newAmount; }
    }

    /// <summary>
    /// Gets the ticket number.
    /// </summary>
    /// <value>The ticket number.</value>
    public string TicketNumber
    {
        get { return this.ticketNo; }
    }

    /// <summary>
    /// Gets the representation int no.
    /// </summary>
    /// <value>The representation int no.</value>
    public int RepresentationIntNo
    {
        get { return this.repIntNo; }
    }

    /// <summary>
    /// Gets a value indicating whether the representation was a success.
    /// </summary>
    /// <value><c>true</c> if success; otherwise, <c>false</c>.</value>
    public bool Success
    {
        get { return this.success; }
    }

}

/// <summary>
/// The delegate that is used for the RepresentationOnTheFly success event
/// </summary>
public delegate void RepresentationOnTheFlyEventHandler(object sender, RepresentationOnTheFlyEventArgs e);
