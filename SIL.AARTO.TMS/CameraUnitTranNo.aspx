<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.CameraUnitTranNo" Codebehind="CameraUnitTranNo.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>" 
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptCopy" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptCopy.Text %>"  Height="24px"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="626px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <table id="Table1" cellspacing="1" cellpadding="1" width="300" border="0">
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelAuthority" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:DropDownList ID="ddlSelectLA" runat="server" Width="217px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgTranNo" Width="495px" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                    AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgTranNo_ItemCommand"
                                    OnPageIndexChanged="dgTranNo_PageIndexChanged">
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="CUTIntNo" HeaderText="CUTIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="AutNo" HeaderText="<%$Resources:dgTranNo.HeaderText %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="CamUnitID" HeaderText="<%$Resources:dgTranNo.HeaderText1 %> "></asp:BoundColumn>
                                        <asp:BoundColumn DataField="CUTNextNo" HeaderText="<%$Resources:dgTranNo.HeaderText2 %> "></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgTranNoItem.Text %> " CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <asp:Panel ID="pnlAddTranNo" runat="server" Height="127px">
                                    <table id="Table2" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td height="2">
                                                <asp:Label ID="lblAddTrans" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddTrans.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" height="25">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblAuthorityno.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddAutNo" runat="server" CssClass="Normal" Width="62px" ReadOnly="True"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <asp:Label ID="lblAddTNDescr" runat="server" Width="214px" CssClass="NormalBold" Text="<%$Resources:lblAddTNDescr.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:DropDownList ID="ddlAddCUTCamUnitID" runat="server" Width="155px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>
                                                    <asp:Label ID="lblAddTNumber" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddTNumber.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddTNumber" runat="server" Width="62px" CssClass="Normal" MaxLength="3"></asp:TextBox></td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revNumber" runat="server" ForeColor=" " ControlToValidate="txtAddTNumber"
                                                    ErrorMessage="<%$Resources:revNumber.Text %>" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTranNo" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddTranNo.Text %>"
                                                    OnClick="btnAddTranNo_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlEditTranNo" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td height="2">
                                                <asp:Label ID="lblEditTranNo" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblEditTranNo.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" height="25">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblAuthorityno.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAutNo" runat="server" CssClass="Normal" MaxLength="3" Width="62px"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 25px">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="214px" Text="<%$Resources:lblAddTNDescr.Text %>"></asp:Label></td>
                                            <td width="248" style="height: 25px">
                                                <asp:DropDownList ID="ddlCUTCamUnitID" runat="server" Width="155px" CssClass="Normal"
                                                    Enabled="False">
                                                </asp:DropDownList></td>
                                            <td style="height: 25px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>
                                                    <asp:Label ID="lblTNumber" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddTNumber.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248">
                                                <asp:TextBox ID="txtTNumber" runat="server" Width="62px" CssClass="Normal" MaxLength="3"></asp:TextBox></td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revTNumber" runat="server" ForeColor=" " ControlToValidate="txtTNumber"
                                                    ErrorMessage="<%$Resources:revNumber.Text %>" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdateTranNo" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdateTranNo.Text %>"
                                                    OnClick="btnUpdateTranNo_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
