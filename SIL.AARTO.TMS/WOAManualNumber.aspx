﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.WOAManualNumber" CodeBehind="WOAManualNumber.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script src="Scripts/Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        function LoadingBlocker(text) {
            var content = '<div style="text-align:center; width:150px; margin: auto"><img alt="loading" src="Images/ig_progressIndicator.gif" style="display:block; float: left" />&nbsp;' + text + '</div>';
            $.blockUI({
                message: content
            });
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 5%">
            <tr>
                <td class="HomeHead" align="center" style="width: 100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167" alt="images/1x1.gif" />
                </td>
                <td valign="top" align="left" colspan="1" style="height: 90%" width="100%">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                                <p style="text-align: center;">
                                    <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead"></asp:Label>
                                </p>
                                <p>
                                    &nbsp;
                                </p>
                            </asp:Panel>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                                <asp:Label ID="lblError" runat="server" CssClass="NormalRed" /><br />
                                <br />
                                <asp:Panel ID="pnlCourt" runat="server" Width="125px">
                                    <table id="tblControls" border="0" class="NormalBold" style="width: 554px">
                                        <tr>
                                            <td style="width: 147px">
                                                <asp:Label ID="Label1" runat="server" Text="<%$Resources:lblCourt.Text %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCourt" runat="server" CssClass="Normal" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlCourt_SelectedIndexChanged" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 147px">
                                                <asp:Label ID="lblCourtRoom" runat="server" CssClass="NormalBold" Visible="true" Text="<%$Resources:lblCourtRoom.Text %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCourtRoom" runat="server" CssClass="Normal" AutoPostBack="True"
                                                    Width="200px" OnSelectedIndexChanged="ddlCourtRoom_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 147px">
                                                <asp:Label ID="lblCourtDates" runat="server" CssClass="NormalBold" Visible="true" Text="<%$Resources:lblCourtDates.Text %>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCourtDates" runat="server" CssClass="Normal" AutoPostBack="True"
                                                    Width="200px" OnSelectedIndexChanged="ddlCourtDates_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <br />
                                <!--Ask user to decide how to handle generate WOA numbers-->
                                <asp:Panel ID="pnlDecision" runat="server" Height="80px" Width="429px" HorizontalAlign="Center">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDecision" runat="server" Height="40px" Text="<%$Resources:lblDecision.Text %>"
                                                    Width="385px" CssClass="NormalRed"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnYes" runat="server" Text="<%$Resources:btnYes.Text %>" CssClass="NormalButton" Width="76px"
                                                    OnClick="btnYes_Click" />
                                                &nbsp; &nbsp; &nbsp;
                                            <asp:Button ID="btnNo" runat="server" Text="<%$Resources:btnNo.Text %>" Width="76px" CssClass="NormalButton"
                                                Height="24px" OnClick="btnNo_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="pnlWOANos" runat="server" HorizontalAlign="Center" Width="550px"
                                Style="margin-bottom: 0px">
                                &nbsp;
                            <table width="100%" id="TABLE1">
                                <tr>
                                    <td style="width: 164px; text-align:right;">
                                        <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSetAutofill.Text %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 164px; text-align:right;">
                                        <asp:Label ID="lblPrefix" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPrefix.Text %>"></asp:Label>
                                    </td>
                                    <td style="width: 117px">
                                        <asp:TextBox ID="txtPrefix" runat="server" Width="121px"></asp:TextBox>
                                    </td>
                                    <td style="width: 103px"></td>
                                </tr>
                                <tr>
                                    <td style="width: 164px; text-align:right;">
                                        <asp:Label ID="lblNext" runat="server" CssClass="NormalBold" Text="<%$Resources:lblNext.Text %>"></asp:Label>
                                    </td>
                                    <td style="width: 117px;">
                                        <asp:TextBox ID="txtNext" runat="server" Width="119px" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td style="width: 103px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 164px; text-align:right;">
                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSuffix.Text %>"></asp:Label>
                                    </td>
                                    <td style="width: 117px">
                                        <asp:TextBox ID="txtSuffix" runat="server" Width="119px"></asp:TextBox>
                                    </td>
                                    <td style="width: 103px"></td>
                                </tr>
                            </table>
                                <br />
                                <asp:GridView ID="grdEnterWOANo" runat="server" Width="650px" AutoGenerateColumns="False" CellPadding="3"
                                    CssClass="Normal" GridLines="Horizontal" ShowFooter="True" AllowPaging="False"
                                    OnRowEditing="grdEnterWOANo_RowEditing" OnRowUpdating="grdEnterWOANo_RowUpdating"
                                    OnRowCancelingEdit="grdEnterWOANo_RowCancelingEdit"
                                    OnSelectedIndexChanged="grdEnterWOANo_SelectedIndexChanged" OnRowDataBound="grdEnterWOANo_RowDataBound" OnRowCreated="grdEnterWOANo_RowCreated">
                                    <FooterStyle CssClass="CartListHead" />
                                    <Columns>
                                        <asp:BoundField HeaderText="<%$Resources:grdEnterWOANo.HeaderText %>" DataField="SummonsNo" ReadOnly="True" ItemStyle-Width="200px">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="<%$Resources:grdEnterWOANo.HeaderText1 %>" DataField="CrtRoomNo" ReadOnly="True" />
                                        <asp:BoundField DataField="SumCourtDate" HeaderText="<%$Resources:grdEnterWOANo.HeaderText2 %>" DataFormatString="{0:yyyy-MM-dd}"
                                            ReadOnly="True" />
                                        <asp:TemplateField ItemStyle-Width="200px" HeaderText="<%$Resources:grdEnterWOANo.HeaderText3 %>">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtWOANumber" runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblWOANumber" Text='<%# Bind("WOANumber") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ShowEditButton="True" EditText="<%$Resources:edit.Text %>" ItemStyle-HorizontalAlign="Right" SelectText="<%$Resources:edit.Text %>" />
                                        <asp:CommandField SelectText="<%$Resources:grdEnterWOANo.SelectText %>" ItemStyle-HorizontalAlign="Right" ShowSelectButton="True" />
                                    </Columns>
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                </asp:GridView>
                                <pager:AspNetPager ID="grdEnterWOANoPager" runat="server"
                                    ShowCustomInfoSection="Right" Width="400px"
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                    FirstPageText="|&amp;lt;"
                                    LastPageText="&amp;gt;|"
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                    CustomInfoStyle="float:right;" PageSize="15" OnPageChanged="grdEnterWOANoPager_PageChanged" UpdatePanelId="UpdatePanel1">
                                </pager:AspNetPager>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width: 100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
