﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stalberg.TMS;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class CourtRoom_Tran : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        protected int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPage = "Court Room Transaction Number Maintenance";
        protected string thisPageURL = "CourtRoom_Tran.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        // Constants
        private const string COPY_TEXT = "Copy";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName;
            int userAccessLevel = userDetails.UserAccessLevel;

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = thisPage;
                pnlEditTranNo.Visible = false;
                btnOptAdd.Visible = true;
                btnOptCopy.Visible = false;
                this.btnOptCopy.Text = COPY_TEXT;
                this.pnlTransactionList.Visible = false;
                this.pnlEditTranNo.Visible = false;

                this.PopulateCourts();
            }
        }

        protected void PopulateCourts()
        {
            CourtDB courts = new CourtDB(this.connectionString);

            ddlCourt.DataSource = courts.GetCourtList();
            ddlCourt.DataValueField = "CrtIntNo";
            ddlCourt.DataTextField = "CrtDetails";
            ddlCourt.DataBind();
            ddlCourt.Items.Insert(0, new ListItem("[ Select a Court ]", string.Empty));
        }

        protected void PopulateCourtRooms(int nCrtIntNo)
        {
            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            SqlDataReader reader = room.GetCourtRoomListByCrtRStatusFlag(nCrtIntNo);//room.GetCourtRoomList(nCrtIntNo);
            this.ddlCourtRoom.DataSource = reader;
            this.ddlCourtRoom.DataValueField = "CrtRIntNo";
            this.ddlCourtRoom.DataTextField = "CrtRoomDetails";
            this.ddlCourtRoom.DataBind();
            this.ddlCourtRoom.Items.Insert(0, new ListItem("[Select court room]", "0"));
            reader.Close();
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ddlCourt.SelectedIndex > 0)
            {
                pnlTransactionList.Visible = false;
                PopulateCourtRooms(Convert.ToInt32(ddlCourt.SelectedValue));
            }
            else
            {
                ddlCourtRoom.Items.Clear();
            }
        }

        protected void ddlCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlCourt.SelectedValue.Length == 0)
            {
                this.pnlEditTranNo.Visible = false;
                this.pnlTransactionList.Visible = false;
                this.btnOptAdd.Visible = false;
                if (this.btnOptCopy.Text.Equals(COPY_TEXT))
                    this.btnOptCopy.Visible = false;

                return;
            }

            // 2013-04-07 add by Henry for pagination
            gvCourtRoomTranPager.RecordCount = 0;
            gvCourtRoomTranPager.CurrentPageIndex = 1;
            this.BindGrid(Convert.ToInt32(ddlCourtRoom.SelectedValue));
        }

        protected void BindGrid(int crtRIntNo)
        {
            Stalberg.TMS.CourtRoomTranDB ctRTranNo = new Stalberg.TMS.CourtRoomTranDB(this.connectionString);
            int totalCount = 0;// 2013-04-07 add by Henry for pagination
            DataSet ds = ctRTranNo.GetCourtRoomTranListDS(crtRIntNo, gvCourtRoomTranPager.PageSize, gvCourtRoomTranPager.CurrentPageIndex, out totalCount);
            this.gvCourtRoomTran.DataSource = ds;
            this.gvCourtRoomTran.DataKeyNames = new string[] { "CRTranIntNo" };
            this.gvCourtRoomTran.DataBind();
            gvCourtRoomTranPager.RecordCount = totalCount;

            if (ds.Tables[0].Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = "There are no transaction numbers matching your search";
            }

            this.pnlTransactionList.Visible = true;
            this.btnOptAdd.Visible = true;
            if (this.btnOptCopy.Text.Equals(COPY_TEXT))
                this.btnOptCopy.Visible = false;
            this.pnlEditTranNo.Visible = false;
        }

        private void BindDetails(CourtRoomTranDetails detail)
        {
            this.txtCRTranType.Text = detail.CRTranType;
            this.txtCRTranDescr.Text = detail.CRTranDescr;
            this.txtCRTranNumber.Text = detail.CRTranNumber.ToString();
            this.hidCRTranIntNo.Value = detail.CRTranIntNo.ToString();
            this.txtCRTranYear.Text = detail.CRTranYear.ToString();
        }

        protected void btnUpdateTranNo_Click(object sender, EventArgs e)
        {
            bool isValid = true;
            StringBuilder sb = new StringBuilder("<p>There were problems saving the Court Room Transaction details:\n<ul>");

            if (string.IsNullOrEmpty(this.ddlCourt.SelectedValue) )
            {
                isValid = false;
                sb.Append("<li>You need to select court for the transaction.</li>\n");
            }

            if (string.IsNullOrEmpty(this.ddlCourtRoom.SelectedValue))
            {
                isValid = false;
                sb.Append("<li>You need to select court room for the transaction.</li>\n");
            }

            string type = this.txtCRTranType.Text.Trim();
            if (type.Length == 0 || type.Length > 3)
            {
                isValid = false;
                sb.Append("<li>You need to fill in a one to three letter type code for the transaction.</li>\n");
            }

            int currentNo = 0;
            if (!int.TryParse(this.txtCRTranNumber.Text, out currentNo) || currentNo < 0)
            {
                isValid = false;
                sb.Append("<li>You need to supply the current transaction number to begin incrementing from.</li>\n");
            }

            if (!isValid)
            {
                sb.Append("</ul>\n");
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return;
            }

            CourtRoomTranDetails details = new CourtRoomTranDetails();
            details.CRTranIntNo = Convert.ToInt32(hidCRTranIntNo.Value);
            details.CRTranDescr = this.txtCRTranDescr.Text.Trim();
            details.CRTranNumber = currentNo;
            details.CRTranType = type;
            details.CrtRIntNo = Convert.ToInt32(this.ddlCourtRoom.SelectedValue);
            details.CRTranYear = Convert.ToInt32(txtCRTranYear.Text.Trim());

            Stalberg.TMS.CourtRoomTranDB db = new CourtRoomTranDB(this.connectionString);
            if (!db.UpdateCourtRoomTranDetails(details, this.login))
            {
                lblError.Text = "Unable to update transaction";
            }
            else
            {
                lblError.Text = "Transaction details have been updated";
                if (details.CRTranIntNo<=0)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtRoomTransactionNumberMaintenance, PunchAction.Add);  

                }
                if (details.CRTranIntNo > 0)
                {
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.CourtRoomTransactionNumberMaintenance, PunchAction.Change);  
                }
                // 2013-04-07 add by Henry for pagination
                //Edge 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                //gvCourtRoomTranPager.CurrentPageIndex = 1;
                //gvCourtRoomTranPager.RecordCount = 0;
                this.BindGrid(Convert.ToInt32(this.ddlCourtRoom.SelectedValue));
            }

            lblError.Visible = true;
        }

        protected void btnOptAdd_Click(object sender, EventArgs e)
        {
            this.AddNewCourtTransaction();
            
            txtCRTranYear.Enabled = true;
            btnUpdateTranNo.Text = "Add Transaction";
        }

        private void AddNewCourtTransaction()
        {
            CourtRoomTranDetails detail = new CourtRoomTranDetails();
            detail.CRTranYear = DateTime.Now.Year;
            detail.CRTranNumber = 1;
            this.BindDetails(detail);
            this.lblEditTranNo.Text = "New Court Room Transaction";
            this.pnlEditTranNo.Visible = true;
        }

        protected void btnOptCopy_Click(object sender, EventArgs e)
        {
            if (this.btnOptCopy.Text.Equals(COPY_TEXT))
            {
                this.ViewState.Add("CRTranType", this.txtCRTranType.Text);
                this.ViewState.Add("CRTranDescr", this.txtCRTranDescr.Text);
                //this.ddlCourt.SelectedIndex = 0;
                this.btnOptCopy.Text = "Paste";
                //this.pnlTransactionList.Visible = false;
                //this.pnlEditTranNo.Visible = false;
            }
            else
            {
                this.AddNewCourtTransaction();
                this.txtCRTranType.Text = this.ViewState["CRTranType"].ToString();
                this.txtCRTranDescr.Text = this.ViewState["CRTranDescr"].ToString();

                this.ViewState.Remove("CRTranType");
                this.ViewState.Remove("CRTranDescr");
                this.btnOptCopy.Text = COPY_TEXT;
            }
        }

        protected void gvCourtRoomTran_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int cRTranIntNo = (int)this.gvCourtRoomTran.DataKeys[e.NewSelectedIndex]["CRTranIntNo"];

            CourtRoomTranDB db = new CourtRoomTranDB(this.connectionString);
            CourtRoomTranDetails details = db.GetDetails(cRTranIntNo);
            this.BindDetails(details);

            this.lblEditTranNo.Text = "Edit Court Room Transaction";
            this.pnlEditTranNo.Visible = true;
            this.btnOptCopy.Visible = true;
            this.txtCRTranYear.Enabled = false;
            btnUpdateTranNo.Text = "Update Transaction";
        }

        // 2013-04-07 add by Henry for pagination
        protected void gvCourtRoomTranPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid(Convert.ToInt32(this.ddlCourtRoom.SelectedValue));
        }

    }
}