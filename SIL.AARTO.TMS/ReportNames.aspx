<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>


<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.ReportNamesPage" CodeBehind="ReportNames.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <style type="text/css">
        .ContentHead
        {
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Button ID="btnOptCopy" runat="server" CssClass="NormalButton" Height="24px"
                                        OnClick="btnOptCopy_Click" Text="<%$Resources:btnOptCopy.Text %>" Width="135px" /></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="634px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <table id="Table2" cellspacing="1" cellpadding="1" width="300" border="0">
                                        <tr>
                                            <td width="162"></td>
                                            <td width="7"></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelAuthority" runat="server" Width="136px" Text="<%$Resources:lblSelAuthority.Text %>" CssClass="NormalBold"></asp:Label></td>
                                            <td width="7">
                                                <asp:DropDownList ID="ddlSelectLA" runat="server" Width="217px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgReportNames" Width="495px" runat="server" BorderColor="Black"
                                    AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgReportNames_ItemCommand">
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="ARNIntNo" HeaderText="ARNIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ARNFunction" HeaderText="<%$Resources:dgReportNames.ARNFunction.HeaderText %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ARNReportPage" HeaderText="<%$Resources:dgReportNames.ARNReportPage.HeaderText %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgReportNames.Command.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <pager:AspNetPager ID="dgReportNamesPager" runat="server"
                                    ShowCustomInfoSection="Right" Width="495px"
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                      FirstPageText="|&amp;lt;"
                                    LastPageText="&amp;gt;|"
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                    CustomInfoStyle="float:right;"
                                    OnPageChanged="dgReportNamesPager_PageChanged">
                                </pager:AspNetPager>
                                <asp:Panel ID="pnlAddReportName" runat="server" Height="127px">
                                    <table id="Table4" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAddAccount" runat="server" CssClass="ProductListHead" Width="232px" Text="<%$Resources:lblAddAccount.Text %>"></asp:Label></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="lblAddAccountNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddAccountNo.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddARNFunction" runat="server" Width="237px" CssClass="NormalMand"
                                                    MaxLength="30" Height="22px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>
                                                    <asp:Label ID="lblAddAccName" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddAccName.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddARNReportPage" runat="server" Width="372px" CssClass="Normal"
                                                    MaxLength="100" Height="22px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>
                                                    <asp:Label ID="lbl_temp" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lbl_temp.Text %>"></asp:Label>
                                                </p>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="ArnTemple" runat="server" Width="372px" CssClass="Normal"
                                                    MaxLength="100" Height="22px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnAddReport" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddReport.Text %>"
                                                    OnClick="btnAddReportNames_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateReportName" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label19" runat="server" Text="<%$Resources:lblSelectAuthority.Text %>" CssClass="ProductListHead" Width="232px"></asp:Label></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblFunction.Text %>" CssClass="NormalBold"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtARNFunction" runat="server" Width="237px" CssClass="NormalMand"
                                                    MaxLength="30" Height="22px"></asp:TextBox></td>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>
                                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblReportPage.Text %>" CssClass="NormalBold" Width="94px"></asp:Label>&nbsp;
                                                </p>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtARNReportPage" runat="server" Width="372px" CssClass="Normal"
                                                    MaxLength="100" Height="22px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>
                                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="94px" Text="<%$Resources:lblTemplate.Text %>"></asp:Label>&nbsp;
                                                </p>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="ARNTemp" runat="server" Width="372px" CssClass="Normal"
                                                    MaxLength="100" Height="22px"></asp:TextBox></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="text-align: right;">
                                                <asp:Button ID="btnOptDelete" runat="server" CssClass="NormalButton" OnClick="btnOptDelete_Click" Text="<%$Resources:btnOptDelete.Text %>" Width="135px" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdateReport" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdateReport.Text %>"
                                                    OnClick="btnUpdateReportNames_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlCopy" runat="server">
                                    <table id="Table1" border="0" cellpadding="1" cellspacing="1" width="300">
                                        <tr>
                                            <td height="19" width="162">
                                                <asp:Label ID="Label6" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblCopyReports.Text %>"></asp:Label></td>
                                            <td height="19" width="7"></td>
                                            <td height="19"></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblFrom.Text %>">FROM:</asp:Label></td>
                                            <td valign="top" width="7">
                                                <asp:DropDownList ID="ddlAutFrom" runat="server" CssClass="Normal" Width="217px">
                                                </asp:DropDownList></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblTo.Text %>"></asp:Label></td>
                                            <td valign="top" width="7">
                                                <asp:DropDownList ID="ddlAutTo" runat="server" CssClass="Normal" Width="217px">
                                                </asp:DropDownList></td>
                                            <td>
                                                <asp:Button ID="btnCopyReports" runat="server" CssClass="NormalButton" OnClick="btnCopyReports_Click"
                                                    Text="<%$Resources:btnCopyReports.Text %>" /></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
