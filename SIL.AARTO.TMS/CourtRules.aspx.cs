﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class CourtRules : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected int userAccessLevel;
        protected string thisPageURL = "CourtRules.aspx";
        //protected string thisPage = "Court Rules Maintenance";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected int autIntNo = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName;
            userAccessLevel = userDetails.UserAccessLevel;

            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddCourtRule.Visible = false;
                pnlEditCourtRule.Visible = false;
                pnlCopy.Visible = false;
                btnOptHide.Visible = true;

                PopulateCourtRule();

                PopulateCourt(ddlSelectCourt);
            }
        }

        protected void PopulateCourt(DropDownList ddlCourt)
        {
            CourtDB court = new CourtDB(this.connectionString);
            DataSet data = court.GetCourtListDS();
            ddlCourt.DataSource = data;
            ddlCourt.DataValueField = "CrtIntNo";
            ddlCourt.DataTextField = "CrtDetails";
            ddlCourt.DataBind();
            ddlCourt.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourt.Items"), "0"));
        }

        protected void PopulateCourtRule()
        {

            this.ddlAddCRCode.DataSource = WorkFlowRuleForCourtCache.GetAll();
            this.ddlAddCRCode.DataValueField = "WFRFCID";
            this.ddlAddCRCode.DataTextField = "WFRFCName";
            this.ddlAddCRCode.DataBind();
        }

        protected void BindGrid(int crtIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.CourtRulesDB courtRuleDB = new Stalberg.TMS.CourtRulesDB(connectionString);

            DataSet data = courtRuleDB.GetCourtRulesListDS(crtIntNo);
            dgCourtRule.DataSource = data;
            dgCourtRule.DataKeyField = "CRID";
            dgCourtRule.DataBind();

            if (dgCourtRule.Items.Count == 0)
            {
                dgCourtRule.Visible = false;
                lblError.Visible = true;
                //Modefied By Henry 2012-3-7
                //Display multi language error message lblError.Text1 - lblError.Text13
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
            {
                dgCourtRule.Visible = true;
            }

            data.Dispose();

            pnlAddCourtRule.Visible = false;
            pnlEditCourtRule.Visible = false;
            pnlCopy.Visible = false;
        }

        protected void ddlSelectCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgCourtRule.Visible = true;
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            BindGrid(Convert.ToInt32(ddlSelectCourt.SelectedValue));
        }

        protected void btnOptAdd_Click(object sender, EventArgs e)
        {
            pnlAddCourtRule.Visible = true;
            pnlEditCourtRule.Visible = false;

            txtAddCRNumeric.Text = "0";

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
            this.ucLanguageLookupAddForComment.DataBind(entityList);

            ddlAddCRCode_SelectedIndexChanged(sender, e);
        }

        protected void btnOptCopy_Click(object sender, EventArgs e)
        {
            pnlAddCourtRule.Visible = false;
            pnlEditCourtRule.Visible = false;
            pnlCopy.Visible = true;

            PopulateCourt(ddlCourtFrom);
            PopulateCourt(ddlCourtTo);
        }

        protected void btnOptHide_Click(object sender, EventArgs e)
        {
            if (dgCourtRule.Visible.Equals(true))
            {
                //hide it
                dgCourtRule.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptShow.Text");
            }
            else
            {
                //show it
                dgCourtRule.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }


        protected void btnAddCourtRule_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelectCourt.SelectedValue) == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            if (String.IsNullOrEmpty(this.ddlAddCRCode.SelectedValue) || this.ddlAddCRCode.SelectedValue == "")
            {
                lblError.Text = (string)GetLocalResourceObject("RegularExpressionValidator1.ErrorMessage");
                return;
            }

            // add the new court rule to the database table
            CourtRulesDB courtRuleAdd = new CourtRulesDB(connectionString);

            int addCRIntNo = courtRuleAdd.AddCourtRules(Convert.ToInt32(ddlSelectCourt.SelectedValue), Convert.ToInt32(this.ddlAddCRCode.SelectedValue),
                txtAddCRDescr.Text.Trim(), Convert.ToInt32(txtAddCRNumeric.Text.Trim()), txtAddCRString.Text.Trim(), txtAddCRComment.Text.Trim(),
                loginUser);

            if (addCRIntNo <= 0)
            {
                if (addCRIntNo == -1)
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                }
            }
            else
            {
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(addCRIntNo, lgEntityList, "WorkFlowRuleForCourtDescrLookup", "WFRFCID", "WFRFCDescr", this.loginUser);

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgForCommentEntityList = this.ucLanguageLookupAddForComment.Save();
                rUMethod.UpdateIntoLookup(addCRIntNo, lgForCommentEntityList, "WorkFlowRuleForCourtCommentLookup", "WFRFCID", "WFRFCComment", this.loginUser);

                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                //txtAddCRCode.Text = "";
                txtAddCRDescr.Text = "";
                txtAddCRNumeric.Text = "0";
                txtAddCRString.Text = "";
                txtAddCRComment.Text = "";
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtRulesMaintenance, PunchAction.Add);  

            }

            lblError.Visible = true;

            int crtIntNo = Convert.ToInt32(ddlSelectCourt.SelectedValue);

            BindGrid(crtIntNo);
            if (addCRIntNo <= 0)
            {
                this.pnlAddCourtRule.Visible = true;
            }
        }

        protected void btnUpdateCourtRule_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelectCourt.SelectedValue) == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            if (String.IsNullOrEmpty(this.ddlAddCRCode.SelectedValue) || this.ddlAddCRCode.SelectedValue == "")
            {
                lblError.Text = (string)GetLocalResourceObject("RegularExpressionValidator1.ErrorMessage");
                return;
            }

            int crtIntNo = Convert.ToInt32(ddlSelectCourt.SelectedValue);

            CourtRulesDB courtRuleUpdate = new CourtRulesDB(connectionString);

            int updCourtRuleIntNo = courtRuleUpdate.UpdateCourtRules(Convert.ToInt32(ddlSelectCourt.SelectedValue),
                    txtCRDescr.Text.Trim(), Convert.ToInt32(txtCRNumeric.Text.Trim()), txtCRString.Text.Trim(), txtCRComment.Text.Trim(),
                    loginUser, Convert.ToInt32(Session["editCRIntNo"]));

            if (updCourtRuleIntNo.Equals("-1"))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
            }
            else
            {
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(updCourtRuleIntNo, lgEntityList, "WorkFlowRuleForCourtDescrLookup", "WFRFCID", "WFRFCDescr", this.loginUser);

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityCommentList = this.ucLanguageLookupUpdateForComment.Save();
                rUMethod.UpdateIntoLookup(updCourtRuleIntNo, lgEntityCommentList, "WorkFlowRuleForCourtCommentLookup", "WFRFCID", "WFRFCComment", this.loginUser);

                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtRulesMaintenance, PunchAction.Change);  
            }

            lblError.Visible = true;

            BindGrid(crtIntNo);
        }

        protected void btnCopyCourtRules_Click(object sender, EventArgs e)
        {
            int fromCrtIntNo = Convert.ToInt32(ddlCourtFrom.SelectedValue);
            int toCrtIntNo = Convert.ToInt32(ddlCourtTo.SelectedValue);

            if (fromCrtIntNo == toCrtIntNo)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                return;
            }

            #region Jerry 2013-10-16 change the logic, allow the user to copy all authority rules that do not exist for the “to” authority
            //CourtRulesDB courtRule = new CourtRulesDB(connectionString);

            //int copyCrtIntNo = courtRule.CopyCourtRules(fromCrtIntNo, toCrtIntNo, loginUser);

            //if (copyCrtIntNo == -1)
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            //else if (copyCrtIntNo == -2)
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            //else if (copyCrtIntNo == 0)
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            //else
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            #endregion
            
            CourtRuleService service = new CourtRuleService();
            TList<CourtRule> fromCourtRuleList = service.GetByCrtIntNo(fromCrtIntNo);
            if (fromCourtRuleList.Count == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            }

            foreach (CourtRule fromCourtRule in fromCourtRuleList)
            {
                CourtRule toCourtRule = service.GetByWfrfcidCrtIntNo(fromCourtRule.Wfrfcid, toCrtIntNo);
                if (toCourtRule == null)
                {
                    toCourtRule = new CourtRule();
                    toCourtRule.Wfrfcid = fromCourtRule.Wfrfcid;
                    toCourtRule.CrtIntNo = toCrtIntNo;
                    toCourtRule.CrNumeric = fromCourtRule.CrNumeric;
                    toCourtRule.CrString = fromCourtRule.CrString;
                    toCourtRule.LastUser = this.loginUser;

                    service.Save(toCourtRule);
                }
            }
            lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CourtRulesMaintenance, PunchAction.Add);  

            // Iris 20140310 changed for showing the selected court list in current page when complete 'copy'.
            toCrtIntNo = Convert.ToInt32(ddlSelectCourt.SelectedValue);
            BindGrid(toCrtIntNo);
        }

        protected void btnPrintCourtRules_Click(object sender, EventArgs e)
        {
            if (this.ddlSelectCourt.SelectedValue.Equals(string.Empty) || this.ddlSelectCourt.SelectedIndex <= 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                return;
            }

            this.lblError.Text = string.Empty;

            int crtIntNo = int.Parse(this.ddlSelectCourt.SelectedValue);
            Helper_Web.BuildPopup(this, "ViewCourtRules_Report.aspx", "CrtIntNo", this.ddlSelectCourt.SelectedValue);
            
        }

        protected void dgCourtRule_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgCourtRule.CurrentPageIndex = e.NewPageIndex;

            int crtIntNo = Convert.ToInt32(ddlSelectCourt.SelectedValue);

            BindGrid(crtIntNo);
        }

        protected void dgCourtRule_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dgCourtRule.SelectedIndex = e.Item.ItemIndex;

            if (dgCourtRule.SelectedIndex > -1)
            {
                pnlAddCourtRule.Visible = false;
                Session["editCRIntNo"] = Convert.ToInt32(dgCourtRule.DataKeys[dgCourtRule.SelectedIndex]);

                int wfrcId = Convert.ToInt32(e.Item.Cells[1].Text);

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(wfrcId.ToString(), "WorkFlowRuleForCourtDescrLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityCommentList = rUMethod.BindUCLanguageLookupByID(wfrcId.ToString(), "WorkFlowRuleForCourtCommentLookup");
                this.ucLanguageLookupUpdateForComment.DataBind(entityCommentList);

                EditCourtRule(Convert.ToInt32(Session["editCRIntNo"]));
            }
        }

        protected void EditCourtRule(int crIntNo)
        {
            CourtRulesDB courtRules = new CourtRulesDB(connectionString);
            CourtRulesDetails courtRulesDetails = courtRules.GetCourtRulesDetails(crIntNo);
            if (crIntNo > 0)
            {
                txtCRCode.Text = courtRulesDetails.CRCode;
                txtCRString.Text = courtRulesDetails.CRString;
                txtCRNumeric.Text = courtRulesDetails.CRNumeric.ToString();
                txtCRDescr.Text = courtRulesDetails.CRDescr;
                txtCRComment.Text = courtRulesDetails.CRComment;

                pnlEditCourtRule.Visible = true;
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                lblError.Visible = true;
                pnlEditCourtRule.Visible = false;
            }
        }

        protected void ddlAddCRCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.ddlAddCRCode.SelectedValue) && this.ddlAddCRCode.SelectedValue != "0")
            {
                WorkFlowRuleForCourt court = WorkFlowRuleForCourtCache.GetAll().Where(c => c.Wfrfcid == Convert.ToInt32(this.ddlAddCRCode.SelectedValue)).FirstOrDefault();
                if (court != null)
                {
                    int wffcId = Convert.ToInt32(this.ddlAddCRCode.SelectedValue);
                    txtAddCRDescr.Text = court.WfrfcDescr;
                    txtAddCRComment.Text = court.WfrfcComment;

                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();

                    List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(wffcId.ToString(), "WorkFlowRuleForCourtDescrLookup");
                    this.ucLanguageLookupAdd.DataBind(entityList);
                    List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityCommentList = rUMethod.BindUCLanguageLookupByID(wffcId.ToString(), "WorkFlowRuleForCourtCommentLookup");
                    this.ucLanguageLookupAddForComment.DataBind(entityCommentList);
                }
            }
        }

    }
}