using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
//using BarcodeNETWorkShop;

namespace Stalberg.TMS.Reports
{
    /// <summary>
    /// Represents a page that crates a PDF of a cash receipt note.
    /// </summary>
    public partial class SuspenseAccountPaymentReportViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private ReportDocument _reportDoc = new ReportDocument();
        private ExportOptions _exportOpt;
        private DiskFileDestinationOptions _diskFileOpt;
        private int autIntNo = 0;

        //protected string thisPage = "Suspense Payment Report Viewer";
        protected string thisPageURL = "SuspenseAccountPaymentReportViewer.aspx";
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            string tempFileLoc;
            string reportPath = string.Empty;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "SuspenseAccountPayments");

            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "SuspenseAccountPayments.rpt", "SuspenseAccountPayments", "system", "");
                reportPage = "SuspenseAccountPayments.rpt";
            }

            reportPath = Server.MapPath("reports/" + reportPage);


            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "SuspenseAccountPayments");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {

                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            
                //DateTime dt = DateTime.Parse(Request.QueryString["Date"]);
                //DateTime endDt = DateTime.Parse(Request.QueryString["EndDate"]);

            DateTime dt;
            DateTime endDt;
            if (Request.QueryString["Date"] == null || Request.QueryString["EndDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error2");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }
            else if (!DateTime.TryParse(Request.QueryString["Date"], out dt) || !DateTime.TryParse(Request.QueryString["EndDate"], out endDt))
            {
                string error = (string)GetLocalResourceObject("error2");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            _reportDoc.Load(reportPath);

            // tlp - 20081023 - new parameter to report - set to 1 if user wants to report on Easypay suspense payments only,
            int easyPayOnly = Int32.Parse(Request.QueryString["EasyPayOnly"]);

            SuspenseAccount db = new SuspenseAccount(this.connectionString);
            DataSet ds = db.PaymentReport(dt, endDt, easyPayOnly, this.autIntNo);
            _reportDoc.SetDataSource(ds.Tables[0]);

            //set temp location for pdf export
            tempFileLoc = Server.MapPath("Temp/" + Guid.NewGuid().ToString() + ".pdf");

            //set up export options
            _diskFileOpt = new DiskFileDestinationOptions();
            _diskFileOpt.DiskFileName = tempFileLoc;

            _exportOpt = _reportDoc.ExportOptions;
            _exportOpt.DestinationOptions = _diskFileOpt;
            _exportOpt.ExportDestinationType = ExportDestinationType.DiskFile;
            _exportOpt.ExportFormatType = ExportFormatType.PortableDocFormat;

            //export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            _reportDoc.Dispose();

            // Stuff the PDF file into rendering stream first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

    }
}