﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Drawing;
using System.Data.SqlClient;

using Stalberg.TMS;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

    public partial class NatisReturnCodeList : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;

        //protected string thisPage = "Natis Return Code Maintenance";
        protected string thisPageURL = "NatisReturnCodeList.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }


        protected void Page_Load(object sender, EventArgs e)
        {


            connectionString = Application["constr"].ToString();

            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddNRC.Visible = false;
                pnlUpdateNRC.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptHide.Visible = true;

                BindGrid();
            }		
        }

        protected void BindGrid()
        {
            // Obtain and bind a list of all users

            Stalberg.TMS.FrameDB natisReturnCodeList = new Stalberg.TMS.FrameDB(connectionString);
            DataSet data = natisReturnCodeList.GetNatisReturnCodeList();
            dgNatisRCList.DataSource = data;
            dgNatisRCList.DataKeyField = "NRCIntNo";
            dgNatisRCList.DataBind();


            if (dgNatisRCList.Items.Count == 0)
            {
                dgNatisRCList.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgNatisRCList.Visible = true;
            }

            data.Dispose();
            pnlAddNRC.Visible = false;
            pnlUpdateNRC.Visible = false;
        }

        //***************************
        // Update by Jake 04/07/2009
        //***************************
        protected void ShowRejectionDetails(int editNRCIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.FrameDB  frameDB = new Stalberg.TMS.FrameDB (connectionString);

            Stalberg.TMS.NatisReturnCodeDetails natisRCDetails = frameDB.GetNRCDetailsByIntNo (editNRCIntNo);

            txtNRCDescr.Text = natisRCDetails.NRCDescr;
            txtNRCode.Text = natisRCDetails.NRCode;
            txtNRCPriority.Text = natisRCDetails.NRCPriority.ToString();

            pnlUpdateNRC.Visible = true;
            pnlAddNRC.Visible = false;

            btnOptDelete.Visible = true;
        }


        protected void dgNatisRCList_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            try
            {
                this.dgNatisRCList.CurrentPageIndex = e.NewPageIndex;
                this.BindGrid();
            }
            catch(Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }
        }

        protected void dgNatisRCList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            dgNatisRCList.SelectedIndex = e.Item.ItemIndex;

            if (dgNatisRCList.SelectedIndex > -1)
            {
                int editNRCIntNo = Convert.ToInt32(dgNatisRCList.DataKeys[dgNatisRCList.SelectedIndex]);

                Session["editNRCIntNo"] = editNRCIntNo;
                Session["prevPage"] = thisPageURL;

                ShowRejectionDetails(editNRCIntNo);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            
            // add the new transaction to the database table
            Stalberg.TMS.FrameDB  frameDB = new FrameDB (connectionString);

            //Test//
            loginUser = "Michael";


            //int addRejIntNo = toAdd.AddRejection(txtAddRejReason.Text, rejSkip, loginUser);
            int addNRCIntNo = frameDB.AddNatisReturnCode(txtAddNRCDescr.Text,txtAddNRCode.Text, loginUser, Convert.ToInt32(txtAddNRCPriority.Text) );

            if (addNRCIntNo <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.NatisReturnCodeMaintenance, PunchAction.Add);  

            }

            lblError.Visible = true;

            BindGrid();
        }


        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int editNRCIntNo = Convert.ToInt32(Session["editNRCIntNo"]);

            // add the new transaction to the database table
            Stalberg.TMS.FrameDB frameDB = new FrameDB(connectionString);

            //int updRejIntNo = conUpdate.UpdateRejection(rejIntNo, txtRejReason.Text, rejSkip, loginUser);
           
            //Test//
            //2013-12-02 Heidi Commented out for use current user(5084)
           // loginUser = "Michael";

            int updNRCIntNo = frameDB.UpdateNatisReturnCode(editNRCIntNo, txtNRCDescr.Text, 
                txtNRCode.Text, loginUser, Convert.ToInt32( txtNRCPriority.Text));

            if (updNRCIntNo == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            }
            else if (updNRCIntNo == -1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.NatisReturnCodeMaintenance, PunchAction.Change); 
            }

            lblError.Visible = true;

            BindGrid();
        }

        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
            int nrcIntNo = Convert.ToInt32(Session["editNRCIntNo"]);

            Stalberg.TMS.FrameDB frameDB = new FrameDB(connectionString);

            int delNRCIntNo = frameDB.DeleteNatisReturnCode(nrcIntNo);

            if (delNRCIntNo <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            }
            //else if (delRejIntNo.Equals("-1"))
            //{
            //    lblError.Text = "Unable to delete this rejection reason - it has been linked to submitted films";
            //}
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.NatisReturnCodeMaintenance, PunchAction.Delete); 
            }

            lblError.Visible = true;

            BindGrid();
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgNatisRCList .Visible.Equals(true))
            {
                //hide it
                dgNatisRCList.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptShow.Text");
            }
            else
            {
                //show it
                dgNatisRCList.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        
        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlAddNRC.Visible = true;
            pnlUpdateNRC.Visible = false;

            btnOptDelete.Visible = false;
        }

}


