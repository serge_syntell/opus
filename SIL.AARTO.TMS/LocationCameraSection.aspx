﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.LocationCameraSection" Codebehind="LocationCameraSection.aspx.cs" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=_title%>
    </title>
    <link href="<%= _styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= _description %>" name="Description" />
    <meta content="<%= _keywords %>" name="Keywords" />
</head>
<body style="margin: 0px 0px 0px 0px; background: <%=_background %>;">
    <form id="Form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="height: 10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" style="width: 100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table style="height: 85%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center">
                <img style="height: 1px" src="images/1x1.gif" alt="" width="167" />
                <asp:Panel ID="pnlMainMenu" runat="server">
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label1" runat="server" Width="118px" Text="<%$Resources:lblOptions.Text %>" CssClass="ProductListHead"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                <asp:Button ID="buttonAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:buttonAdd.Text %>"
                                    OnClick="buttonAdd_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                <asp:Button ID="buttonList" runat="server" Width="135px" 
                                    CssClass="NormalButton" Text="<%$Resources:buttonList.Text %>" onclick="buttonList_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                <asp:Button ID="btnHideMenu" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$Resources:btnHideMenu.Text %>" OnClick="btnHideMenu_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" colspan="1" style="width: 100%; text-align: center">
                                <asp:UpdatePanel ID="upd" runat="server">
    <ContentTemplate>
                <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                    <p style="text-align: center;">
                        <asp:Label ID="lblPageName" runat="server" Width="100%" Text="<%$Resources:lblPageName.Text %>" CssClass="ContentHead"></asp:Label></p>
                    <p>
                        &nbsp;</p>
                </asp:Panel>
                        <asp:Label ID="lblError" CssClass="NormalRed" runat="server" />
                        <table>
                         <tr>
                                                <td width="162">
                                                    <asp:Label ID="lblSelAuthority" runat="server" Width="136px" Text="<%$Resources:lblSelAuthority.Text %>" CssClass="NormalBold"></asp:Label>
                                                </td>
                                                <td width="7">
                                                    <asp:DropDownList ID="ddlSelectLA" runat="server" Width="217px" CssClass="Normal"
                                                        AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                </td>
                                            </tr></table>
                        <asp:Panel ID="pnlLocation" runat="server" Width="100%" CssClass="Normal">
                            <asp:DataGrid ID="gridLocation" Width="900px" runat="server" BorderColor="Black"
                                AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True" AllowPaging="true" PageSize="10"
                                Font-Size="8pt" CellPadding="4" GridLines="Vertical" OnItemCommand="gridLocation_ItemCommand" OnPageIndexChanged="gridLocation_PageIndexChanged">
                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="LocIntNo" HeaderText="LocIntNo" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LocCode" HeaderText="<%$Resources:gridLocation.LocCode.HeaderText %>"></asp:BoundColumn>
                                    <%--<asp:BoundColumn DataField="LocStreetCode" HeaderText="StreetCode"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LocStreetName" HeaderText="StreetName"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LocTravelDirection" HeaderText="TravelDirection"></asp:BoundColumn>--%>
                                    <asp:BoundColumn DataField="LocDescr" HeaderText="<%$Resources:gridLocation.LocDescr.HeaderText %>"></asp:BoundColumn>
                                    <asp:ButtonColumn CommandName="Select" Text="<%$Resources:gridLocation.ComandSelect.Text %>"></asp:ButtonColumn>
                                </Columns>
                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                <PagerStyle Font-Size="Small" PageButtonCount="10" Mode="NumericPages"/>
                            </asp:DataGrid>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="PanelLcs" runat="server" Width="100%" CssClass="Normal">
                            <asp:DataGrid ID="DataGridLcs" Width="900px" runat="server" BorderColor="Black"
                                AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                Font-Size="8pt" CellPadding="4" GridLines="Vertical" OnItemCommand="gridLcs_ItemCommand">
                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="LCSIntNo" HeaderText="LCSIntNo" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LocIntNo" HeaderText="LocIntNo" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CamSerialNo1" HeaderText="<%$Resources:DataGridLcs.CamSerialNo1.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CamSerialNo2" HeaderText="<%$Resources:DataGridLcs.CamSerialNo2.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LCSSectionCode" HeaderText="<%$Resources:DataGridLcs.LCSSectionCode.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LCSSectionName" HeaderText="<%$Resources:DataGridLcs.LCSSectionName.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LCSSectionDistance" HeaderText="<%$Resources:DataGridLcs.LCSSectionDistance.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LCSActive" HeaderText="<%$Resources:DataGridLcs.LCSActive.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LCSStartDate" HeaderText="<%$Resources:DataGridLcs.LCSStartDate.HeaderText %>" DataFormatString="{0:yyyy-MM-dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LCSEndDate" HeaderText="<%$Resources:DataGridLcs.LCSEndDate.HeaderText %>" DataFormatString="{0:yyyy-MM-dd}"></asp:BoundColumn>
                                    
                                    <asp:ButtonColumn CommandName="Select" Text="<%$Resources:DataGridLcs.CommandSelect.Text %>"></asp:ButtonColumn>
                                    <asp:ButtonColumn CommandName="Delete" Text="<%$Resources:DataGridLcs.CommandDelete.Text %>"></asp:ButtonColumn>
                                </Columns>
                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                            </asp:DataGrid>
                        </asp:Panel>
                        <asp:Panel ID="panelEdit" runat="server" Width="100%" CssClass="Normal">
                            <table>
                                    </tr>
                            <tr><td><asp:Label ID="lblLocation" runat="server"  CssClass="SubContentHead" /></td><td></td></tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbLCSSectionCode" runat="server"  CssClass="NormalBold" Text="<%$Resources:lbLCSSectionCode.Text %>" />
                                        </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtLCSSectionCode" MaxLength="5" CssClass="Normal"></asp:TextBox>
                                        <asp:HiddenField runat="server" ID="hiddenID" />
                                    </td>
                                    </tr>
                                    
                                    <tr>                            
                                    <td>
                                        <asp:Label ID="lbLCSSectionName" runat="server" CssClass="NormalBold" Text="<%$Resources:lbLCSSectionName.Text %>" />
                                        </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtLCSSectionName" MaxLength="30" 
                                            CssClass="Normal" Width="355px"></asp:TextBox>
                                    </td>
                                     </tr>
                                     
                                     <tr>
                                    <td>
                                        <asp:Label ID="lbLCSSectionDistance" runat="server" CssClass="NormalBold" Text="<%$Resources:lbLCSSectionDistance.Text %>" />
                                        </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtLCSSectionDistance" MaxLength="10" 
                                            CssClass="Normal" Width="57px"></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                            ControlToValidate="txtLCSSectionDistance" CssClass="NormalRed" 
                                                            ErrorMessage="<%$Resources:RegularExpressionValidator3.ErrorMessage%>" 
                                                            ValidationExpression="\d*"></asp:RegularExpressionValidator>

                                    </td>
                                       </tr>
                                    
                                      <tr> 
                                      <td>
                                        <asp:Label ID="lbLCSActive" runat="server" CssClass="NormalBold" Text="<%$Resources:lbLCSActive.Text %>" />
                                    </td>
                                     <td>
                                        <asp:CheckBox runat="server" ID="chkLCSActive" CssClass="NormalBold"/>
                                    </td>
                                    </tr>
                                    
                                    <tr>
                                     <td>
                                        <asp:Label ID="lbLCSStartDate" runat="server" CssClass="NormalBold" Text="<%$Resources:lbLCSStartDate.Text %>" />
                                         </td>
                                    <td>
                                         <asp:TextBox runat="server" ID="txtLCSStartDate" CssClass="Normal" Height="20px" autocomplete="off" UseSubmitBehavior="False"/>
                                         <cc1:CalendarExtender ID="DateCalendar1" runat="server" TargetControlID="txtLCSStartDate" Format="yyyy-MM-dd">
                                         </cc1:CalendarExtender>
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                            ControlToValidate="txtLCSStartDate" CssClass="NormalRed" 
                                                            ErrorMessage="<%$Resources:RegularExpressionValidator1.ErrorMessage %>" 
                                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                    </td>
                                    </tr>
                                    
                                    <tr>
                                     <td>
                                        <asp:Label ID="lbLCSEndDate" runat="server" CssClass="NormalBold" Text="<%$Resources:lbLCSEndDate.Text %>" />
                                         </td>
                                   <td>
                                                                                                <asp:TextBox runat="server" ID="txtLCSEndDate" CssClass="Normal" Height="20px" autocomplete="off" UseSubmitBehavior="False"/>
                                                        <cc1:CalendarExtender ID="DateCalendar2" runat="server" TargetControlID="txtLCSEndDate" Format="yyyy-MM-dd">
                                                        </cc1:CalendarExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                            ControlToValidate="txtLCSEndDate" CssClass="NormalRed" 
                                                            ErrorMessage="<%$Resources:RegularExpressionValidator2.ErrorMessage %>" 
                                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                    </td>
                                    </tr>
                                    
                                    <tr>
                                     <td>
                                        <asp:Label ID="lbCameraIntNo1" runat="server" CssClass="NormalBold" Text="<%$Resources:lbCameraIntNo1.Text %>" />
                                         </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlCamera1"  CssClass="Normal" ></asp:DropDownList>
                                    </td>
                                    </tr>
                                    
                                <tr>
                                <td>
                                    <asp:Label ID="lbCameraIntNo2" runat="server" CssClass="NormalBold" 
                                        Text="<%$Resources:lbCameraIntNo2.Text %>" />
                                    </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlCamera2" runat="server" CssClass="Normal">
                                                                </asp:DropDownList>
                                </td>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="buttonSubmit" runat="server" CssClass="NormalButton" 
                                                OnClick="buttonSubmit_Click" Text="<%$Resources:buttonSubmit.Text %>" />
                                        </td>
                                    </tr>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="panelDelete" runat="server" Width="100%" CssClass="Normal">
                            <asp:Label runat="server" ID="labelConfirmDelete" ForeColor="Red" />
                            <asp:Button ID="buttonDelete" runat="server" Text="<%$Resources:buttonDelete.Text %>" 
                                onclick="buttonDelete_Click1" />
                        </asp:Panel>
                        
                            </ContentTemplate>
                </asp:UpdatePanel>
    <asp:UpdateProgress ID="udp" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;Loading...</p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
            </td>
            <td valign="top" align="left" style="width: 100%">
            </td>
        </tr>
    </table>
    <table style="height: 5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" style="width: 100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
