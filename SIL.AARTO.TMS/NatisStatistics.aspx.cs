using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;


namespace Stalberg.TMS 
{
    /// <summary>
    /// A page that provides statistical information about data sent to NATIS
    /// </summary>
    public partial class NatisStatistics : System.Web.UI.Page 
	{
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo;
        private string loginUser;

        protected string styleSheet;
        protected string backgroundImage;
        //protected string thisPage = "NaTIS Statistics";
        protected string thisPageURL = "NatisStatistics.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];

			loginUser = userDetails.UserLoginName;

			//int 
			autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

			if (!Page.IsPostBack)
			{
                //Jerry 2014-12-04 add space
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text") + " " + (string)GetLocalResourceObject("Stringfor.Value") + " " + Session["autName"].ToString();
                rdoCurrentLAOnly.Text = Session["autName"].ToString() + " " + (string)GetLocalResourceObject("stringOnly.Value");

                dtpFrom.Text = DateTime.Today.ToString(DATE_FORMAT);
                dtpTo.Text = DateTime.Today.ToString(DATE_FORMAT);

				BindGridSchedule();
                dgNaTISStatistics.Visible = false;
                pnlSummary.Visible = true;
                BindGridStats();

			}		
		}

        private void BindGridStats()
        {
            Stalberg.TMS.NaTISFilesDB natis = new Stalberg.TMS.NaTISFilesDB(connectionString);

            int natisAutIntNo = 0;
            int noOFNotFound = 0;
            int noOfFailed = 0;
            
            if (rdoCurrentLAOnly.Checked)
                natisAutIntNo = autIntNo;

            DataSet data = natis.GetNaTISStatsDS(natisAutIntNo, ref noOFNotFound, ref noOfFailed);
            dgNatisSummary.DataSource = data;
            try
            {
                dgNatisSummary.DataBind();
            }
            catch
            {
                dgNatisSummary.CurrentPageIndex = 0;
                dgNatisSummary.DataBind();
            }

            if (dgNatisSummary.Items.Count == 0)
            {
                dgNatisSummary.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgNatisSummary.Visible = true;
                lblSummary.Visible = true;
            }

            lblNoOfFailed.Text = noOfFailed.ToString();
            lblNoOfNotFound.Text = noOFNotFound.ToString();
            data.Dispose();
        }

		protected void BindGridSchedule()
		{
			//int regIntNo = 0;

            DateTime dateFrom = DateTime.Today;
            DateTime dateTo = DateTime.Today;

            if (dtpFrom.Text.Trim().Equals(string.Empty))
                dateFrom = Convert.ToDateTime("1900/01/01");
            else if (!DateTime.TryParse(dtpFrom.Text, out dateFrom))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            if (dtpTo.Text.Trim().Equals(string.Empty))
                dateTo = Convert.ToDateTime("1900/01/01");
            else if (!DateTime.TryParse(dtpTo.Text, out dateTo))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            Stalberg.TMS.NaTISFilesDB natis = new Stalberg.TMS.NaTISFilesDB(connectionString);

            int natisAutIntNo = 0;

            if (rdoCurrentLAOnly.Checked)
                natisAutIntNo = autIntNo;

            DataSet data = natis.GetNaTISFilesListDS(natisAutIntNo, dateFrom, dateTo);
			dgNaTISStatistics.DataSource = data;
			dgNaTISStatistics.DataKeyField = "NFIntNo";

            try
            {
                dgNaTISStatistics.DataBind();
            }
            catch
            {
                dgNaTISStatistics.CurrentPageIndex = 0;
                dgNaTISStatistics.DataBind();
            }

			if (dgNaTISStatistics.Items.Count == 0)
			{
				dgNaTISStatistics.Visible = false;
				lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
			}
			else
			{
				dgNaTISStatistics.Visible = true;
			}

			data.Dispose();

		}

		 

        protected void rdoCurrentLAOnly_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoCurrentLAOnly.Checked.Equals(true))
            {
                rdoAll.Checked = false;
                RefreshGrids();
            }
        }

        protected void rdoAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoAll.Checked.Equals(true))
            {
                rdoCurrentLAOnly.Checked = false;
                RefreshGrids();

            }
        }
        protected void btnOptHideSummary_Click(object sender, EventArgs e)
        {
            if (dgNatisSummary.Visible.Equals(true))
            {
                //hide it
                pnlSummary.Visible = false;
                btnOptHideSummary.Text = (string)GetLocalResourceObject("btnOptHideSummary.Text1");
            }
            else
            {
                //show it
                pnlSummary.Visible = true;
                btnOptHideSummary.Text = (string)GetLocalResourceObject("btnOptHideSummary.Text");
            }
        }

        protected void btnOptHideSched_Click(object sender, EventArgs e)
        {
            if (dgNaTISStatistics.Visible.Equals(true))
            {
                //hide it
                dgNaTISStatistics.Visible = false;
                btnOptHideSched.Text = (string)GetLocalResourceObject("btnOptHideSummary.Text2");
            }
            else
            {
                //show it
                dgNaTISStatistics.Visible = true;
                btnOptHideSched.Text = (string)GetLocalResourceObject("btnOptHideSummary.Text3");
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshGrids();
        }

        private void RefreshGrids()
        {
            if (dgNaTISStatistics.Visible.Equals(true))
                BindGridSchedule();

            if (dgNatisSummary.Visible.Equals(true))
                BindGridStats();

        }
        protected void dgNaTISStatistics_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgNaTISStatistics.CurrentPageIndex = e.NewPageIndex;

            BindGridSchedule();		
        }
        protected void dgNatisSummary_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgNatisSummary.CurrentPageIndex = e.NewPageIndex;

            BindGridStats();		
        }
}
}
