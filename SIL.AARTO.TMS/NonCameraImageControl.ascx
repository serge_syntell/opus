﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NonCameraImageControl.ascx.cs" Inherits="SIL.AARTO.TMS.NonCameraImageControl" %>
   <script type="text/javascript" src="Scripts/Jquery/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/ui.core.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/ui.draggable.js"></script>
    <script type="text/javascript" src="Scripts/Jquery/jquery.rightClick.js"></script>
    <link href="stylesheets/ST_Odyssey.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/Jquery/jquery-ui.js" type="text/javascript"></script>
       <%-- <div style="width: 700px; margin-left: auto; margin-right: auto; border: 1px solid #ccc">
        <table width="100%">
            <tr style="">
                <td style="width: 700px; border-bottom: solid 1px #ccc; padding-bottom: 4px;">
                    <span>Document preview </span>
                </td>
            </tr>
        </table>--%>
        <div id="div_message" style="display:none; height:30px; line-height:30px;padding-top:10px;">No data</div>
        <div id="div_show">
        <table width="100%">
         
            <tr>
                <td colspan="3">
                </td>
                <div id="mainImageDiv" style="width: 630px; height: 800px; line-height: 800px; overflow: auto;
                    position: relative; margin-left: auto; margin-right: auto;">
                    <div id="rect" class="Rect" style="display: none">
                    </div>
                    <img alt="" id="mainImage" src="" style="position: absolute" />
                </div>
            </tr>
               <tr>
                <td width="120">
                    <input id="btnPrev" onclick="prev()" class="NormalButton" value="<<" type="button" />
                    <input id="btnNext" class="NormalButton" onclick="next()" value=">>" type="button" />
                    <span id="span_num" ></span>
                </td>
                <td width="100">
                    <input id="btnZoom" class="NormalButton" type="button" clientidmode="Static" runat="server"
                        value="Zoom" style="width: 150px" />
                </td>
                <td width="450" align="left">
                   
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span id="lblFeedback" runat="server" style="color: Red; border: solid 1px #ccc">
                    </span>
                </td>
            </tr>
        
        </table>
        </div>
 <%--   </div>--%>
    <input id="hd_notIntNO" type="hidden" runat="server" value="0" />
<%--    <input runat="server" id="ljj" type="text" />--%>
    <script type="text/javascript">
        var mainImageID = "mainImage";
        //  var MainSrc = "/Images/Image.aspx.jpg";
        //var imgArr = ["Images/111.JPG", "Images/Bluehills.jpg", "Images/111.JPG"];

        var imgstr ='<%=imageUrls %>'
      
        var imgArr = imgstr.split(',');

        var i = 0;
        var arrlenth = 0;
        arrlenth = imgArr.length;
        function prev() {
           
            $("#btnNext").removeAttr("disabled");

            if (arrlenth > 0) {
                if (i > 0) {
                    i = i - 1;
                    $("#mainImage").attr("src", imgArr[i]);
                }
                if (i == 0) {
                    $("#btnPrev").attr("disabled", "disabled");
                }
            }
            $("#span_num").html((i + 1) + "/" + arrlenth);

        }

        function next() {

           
            $("#btnPrev").removeAttr("disabled");
            if (arrlenth > 0) {
                if (i < arrlenth - 1) {
                    i = i + 1;
                    $("#mainImage").attr("src", imgArr[i]);
                }
                if (i == arrlenth - 1)
                { $("#btnNext").attr("disabled", "disabled"); }
            }
            $("#span_num").html((i + 1) + "/" + arrlenth);

        }


        var lblFeedbackID = "<%=lblFeedback.ClientID %>";

        $(function () {
            if (arrlenth > 0) {
                $("#mainImage").attr("src", imgArr[0]);
                $("#span_num").html((i + 1) + "/" + arrlenth);

            }
            if (imgstr.length == 0) {
                $("#div_show").hide();
                $("#div_message").show();
            }
            if (arrlenth == 1) {
                $("#btnNext").attr("disabled", "disabled");
                $("#btnPrev").attr("disabled", "disabled");

            }
            pageLoad();
            $("#mainImage").draggable();
        })
    
  
    </script>
<script src="Scripts/nonCameraZoom.js" type="text/javascript"></script>
