<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.SystemOverViewReport" Codebehind="SystemOverViewReport.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    <asp:Button ID="reclaculate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnReclaculate %>" 
                                onclick="reclaculate_Click" />
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;</p>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                        <asp:GridView ID="grdHeader" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="Normal" GridLines="Horizontal" OnSelectedIndexChanged="grdHeader_SelectedIndexChanged"
                            ShowFooter="True" OnPageIndexChanging="grdHeader_PageIndexChanging" OnRowCreated="grdHeader_RowCreated">
                            <FooterStyle CssClass="CartListHead" />
                            <Columns>
                                <asp:BoundField DataField="Description" HeaderText="<%$Resources:grdHeader.HeaderText %>" />
                                <asp:BoundField DataField="Quantity" HeaderText="<%$Resources:grdHeader.HeaderText1 %>">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$Resources:grdHeader.HeaderText2 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NoDaysLeft") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("NoDaysLeft") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdHeader.HeaderText4 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("OldestDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("OldestDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdHeader.HeaderText5 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DateLastRun") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("DateLastRun") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField SelectText="<%$Resources:grdHeader.SelectText %>" ShowSelectButton="True" />
                            </Columns>
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                        </asp:GridView>
                    </asp:Panel>
                    <span style="color:Red; height:13px; line-height:13px; margin-top:4px;">
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                    </span>
                    <asp:Panel ID="pnlInfo" Visible="false" runat="server" Width="100%">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDrillDownHeader" runat="server" CssClass="NormalBold"></asp:Label></td>
                            </tr>
                        </table>
                        <asp:GridView ID="grdDrillDown" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="Normal" GridLines="Horizontal" OnSelectedIndexChanged="grdDrillDown_SelectedIndexChanged"
                            ShowFooter="True" OnPageIndexChanging="grdDrillDown_PageIndexChanging" OnRowCreated="grdDrillDown_RowCreated"
                            AllowPaging="True" PageSize="20">
                            <FooterStyle CssClass="CartListHead" />
                            <Columns>
                                <asp:BoundField DataField="Description" HeaderText="<%$Resources:grdDrillDown.HeaderText %>" />
                                <asp:BoundField DataField="AutCode" HeaderText="<%$Resources:grdDrillDown.HeaderText1 %>" />
                                <asp:BoundField DataField="Quantity" HeaderText="<%$Resources:grdDrillDown.HeaderText2 %>">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$Resources:grdDrillDown.HeaderText3 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NoDaysLeft") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("NoDaysLeft") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdDrillDown.HeaderText4 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("FirstOffenceDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("FirstOffenceDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                        </asp:GridView>
                        <asp:GridView ID="grdDrillDownUpload" runat="server" AutoGenerateColumns="False"
                            CellPadding="3" CssClass="Normal" GridLines="Horizontal" OnSelectedIndexChanged="grdDrillDownUpload_SelectedIndexChanged"
                            ShowFooter="True" OnPageIndexChanging="grdDrillDownUpload_PageIndexChanging"
                            OnRowCreated="grdDrillDownUpload_RowCreated" AllowPaging="True" PageSize="20">
                            <FooterStyle CssClass="CartListHead" />
                            <Columns>
                                <asp:BoundField DataField="Description" HeaderText="<%$Resources:grdDrillDownUpload.HeaderText %>" />
                                <asp:BoundField DataField="AutCode" HeaderText="<%$Resources:grdDrillDownUpload.HeaderText1 %>" />
                                <asp:BoundField DataField="Quantity" HeaderText="<%$Resources:grdDrillDownUpload.HeaderText2 %>">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$Resources:grdDrillDownUpload.HeaderText3 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NoDaysLeft") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("NoDaysLeft") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdDrillDownUpload.HeaderText4 %>">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("FirstOffenceDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("FirstOffenceDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdDrillDownUpload.HeaderText5 %>" Visible="true">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtResponseFile" runat="server" Text='<%# Bind("CUBResponseDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblResponseFile" runat="server" Text='<%# Bind("CUBResponseDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$Resources:grdDrillDownUpload.HeaderText6 %>" Visible="true">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtAllocFile" runat="server" Text='<%# Bind("CUBAllocDate") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAllocFile" runat="server" Text='<%# Bind("CUBAllocDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
