﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS 
{
    public partial class ImageServiceUsers : System.Web.UI.Page 
	{
        // Fields
        private string connectionString = "";
		
        protected string _styleSheet;
		protected string _login;
		protected string _background;
        protected string _thisPage = "ImageServiceUsers.aspx";
		protected string _keywords = "";
		protected string _title = "";
		protected string _description = "";
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int _autIntNo = 0;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnLoad(System.EventArgs e) 
		{
            this.connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];

			_login = userDetails.UserLoginName;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            _autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

            if (userAccessLevel < 7)
                Server.Transfer("Default.aspx");
			//set domain specific variables
			General gen = new General();

			_background = gen.SetBackground(Session["drBackground"]);
			_styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            _title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
				pnlAddUser.Visible = false;
				pnlUserDetails.Visible = false;
				pnlSearch.Visible = true;
				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptSearch.Visible = true;

				if (Session["searchCriteria"] != null)
				{
					//find original search criteria to refresh grid display with changed status
					ArrayList searchCriteria = new ArrayList();
					searchCriteria = (ArrayList)Session["searchCriteria"];

					DisplayUserList(searchCriteria);
				}
				else
					BindGrid(0);

			}
		}

		protected void DisplayUserList(ArrayList searchCriteria)
		{
			BindGrid(Convert.ToInt32(searchCriteria[0]));
		}

		protected void BindGrid(int colValue)
		{
			//set session variables to show what the selection criteria has been
			ArrayList searchCriteria = new ArrayList();
			searchCriteria.Add(colValue);
			Session["searchCriteria"] = searchCriteria;

			// Obtain and bind a list of all users
            Stalberg.TMS.ImageServiceUserDB userList = new Stalberg.TMS.ImageServiceUserDB(connectionString);

			DataSet data = new DataSet();
			data = userList.SearchUserListDS(txtSearch.Text);

			
			dgUsers.DataSource = data;
            dgUsers.DataKeyField = "ISUIntNo";
			dgUsers.DataBind();

			// Hide the list and display a message if no users exist
			if (dgUsers.Items.Count == 0) 
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
				dgUsers.Visible = false;
				lblError.Visible = true;
			}
			else
			{
				lblError.Visible = false;
				dgUsers.Visible = true;
			}

			data.Dispose();

			pnlAddUser.Visible = false;
			btnOptDelete.Visible = false;
			pnlUserDetails.Visible = false;
		}

		protected void dgUsers_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgUsers.SelectedIndex = e.Item.ItemIndex;

			if (dgUsers.SelectedIndex > -1) 
			{			
				int editUserIntNo = Convert.ToInt32(dgUsers.DataKeys[dgUsers.SelectedIndex]);
                string userName = e.Item.Cells[1].Text;

				Session["editUserIntNo"] = editUserIntNo;
				Session["prevPage"] = _thisPage;

                ShowUserDetails(editUserIntNo, userName);
			}
		}

        protected void ShowUserDetails(int editUserIntNo, string userName)
        {
            txtUserName.Text = userName;

            pnlUserDetails.Visible = true;
            pnlAddUser.Visible = false;
            pnlSearch.Visible = false;
            dgUsers.Visible = false;

            btnUpdate.Enabled = true;
            btnOptDelete.Visible = true;
        }

        private static string GetMd5StringMethod(string s)
        {
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(s);
            bytes = md5.ComputeHash(bytes);
            md5.Clear();

            string ret = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                ret += Convert.ToString(bytes[i], 16).PadLeft(2, '0');
            }

            ret = ret.PadLeft(32, '0');
            return ret;
        }

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
            Stalberg.TMS.ImageServiceUserDB user = new ImageServiceUserDB(connectionString);
	
			int editUserIntNo = Convert.ToInt32(Session["editUserIntNo"]);
            //set up encryption class
            //string PassWord = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassWord.Text, "MD5");
            string PassWord = GetMd5StringMethod(txtPassWord.Text);

            int iUpdate = user.UpdateUser(editUserIntNo, txtUserName.Text, PassWord, _login);

            if (iUpdate > 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this._login, PunchStatisticsTranTypeList.ImageServiceUsers, PunchAction.Change);  

            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            }

			

			//find original search criteria to refresh grid display with changed status
			ArrayList searchCriteria = new ArrayList();
			searchCriteria = (ArrayList)Session["searchCriteria"];

			BindGrid(Convert.ToInt32(searchCriteria[0]));


            lblError.Visible = true;
			pnlUserDetails.Visible = false;
			btnOptDelete.Visible = false;
		}

        protected void btnBack_Click(object sender, System.EventArgs e)
        {
            BindGrid(0);

            dgUsers.Visible = true;
            pnlUserDetails.Visible = false;
            btnOptDelete.Visible = false;
        }
	
		protected void btnAddUser_Click(object sender, System.EventArgs e)
		{
            Stalberg.TMS.ImageServiceUserDB userAdd = new ImageServiceUserDB(connectionString);

            int addUserIntNo = userAdd.AddUser(txtAddUserName.Text, "", _login);

            if (addUserIntNo <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this._login, PunchStatisticsTranTypeList.ImageServiceUsers, PunchAction.Add);  
            }

			//find original search criteria to refresh grid display with changed status
			ArrayList searchCriteria = new ArrayList();
			searchCriteria = (ArrayList)Session["searchCriteria"];

			BindGrid(Convert.ToInt32(searchCriteria[0]));
            lblError.Visible = true;
		}

		protected void dgUsers_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgUsers.CurrentPageIndex = e.NewPageIndex;
			
			//find original search criteria to refresh grid display with changed status
			ArrayList searchCriteria = new ArrayList();
			searchCriteria = (ArrayList)Session["searchCriteria"];

			BindGrid(Convert.ToInt32(searchCriteria[0]));
		
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			BindGrid(0);
		}

		protected void btnOptSearch_Click(object sender, System.EventArgs e)
		{
			pnlSearch.Visible = true;
			pnlAddUser.Visible = false;
			pnlUserDetails.Visible = false;
			btnOptDelete.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			pnlAddUser.Visible = true;
			//ddlAddUSGName.Enabled = false;
			pnlUserDetails.Visible = false;		
			pnlSearch.Visible = false;
			btnOptDelete.Visible = false;
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int userIntNo = Convert.ToInt32(Session["editUserIntNo"]);

            Stalberg.TMS.ImageServiceUserDB userDelete = new ImageServiceUserDB(connectionString);
			
			string delUserIntNo = userDelete.DeleteUser(userIntNo);

			if (delUserIntNo.Equals("-1"))
			{
				lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");

                //2014-01-21 Heidi fixed bug for When a page is only a data, to delete the data CurrentPageIndex is not correct (5103)
                if (dgUsers.Items.Count == 1)
                {
                    dgUsers.CurrentPageIndex--;
                }
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this._login, PunchStatisticsTranTypeList.ImageServiceUsers, PunchAction.Delete);  
			}

			//find original search criteria to refresh grid display with changed status
			ArrayList searchCriteria = new ArrayList();
			searchCriteria = (ArrayList)Session["searchCriteria"];

			BindGrid(Convert.ToInt32(searchCriteria[0]));
            lblError.Visible = true;
		}

		protected void btnOptMenu_Click(object sender, System.EventArgs e)
		{
			pnlAddUser.Visible = false;
			pnlUserDetails.Visible = false;
			dgUsers.Visible = false;
		}

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

	}
}

