﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.PaymentHistory" Codebehind="PaymentHistory.aspx.cs" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <link href="stylesheets/webSample_style.css" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />

  
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0" language="javascript" >
    <form id="Form2" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                        <table id="tblControls" border="0" class="NormalBold">
                            <tr>
                                <td>
                                    <asp:Label ID="lblAuthority" runat="server" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" Width="197px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblYear" runat="server" Text="<%$Resources:lblYear.Text %>"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtYear" runat="server" CssClass="Normal"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right">
                                    <asp:Button ID="btnView" runat="server" CssClass="NormalButton" OnClick="btnView_Click"
                                        Text="<%$Resources:btnView.Text %>" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                                </td>
                            </tr>
                        </table>
             
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
