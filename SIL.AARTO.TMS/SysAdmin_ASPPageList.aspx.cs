using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Stalberg.TMS 
{

	public partial class SysAdmin_ASPPageList : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected string thisPageURL = "SysAdmin_ASPPageList.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "System admin: ASPX page list";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
			loginUser = userDetails.UserLoginName;

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			if (userAccessLevel<10)
				//Server.Transfer(Session["prevPage"].ToString());
                Server.Transfer("Default.aspx");

			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				LoadSelector();
				BindGrid();
				
			}
		}

		protected void LoadSelector()
		{
			ddlSelect.Items.Clear();
            ddlSelect.Items.Insert(0, (string)GetLocalResourceObject("ddlSelect.Items"));
			ddlSelect.Items.Insert(1, (string)GetLocalResourceObject("ddlSelect.Items1"));
            ddlSelect.Items.Insert(2, (string)GetLocalResourceObject("ddlSelect.Items2"));
			ddlSelect.SelectedIndex = 0;
		}

		public string SetValue (object sysAdminOnly)
		{
			string val = "No";

			if (sysAdminOnly.Equals("Y"))
				val = "Yes";
		
			return val;
		}

		protected void BindGrid()
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.ASPPageListDB pageList = new Stalberg.TMS.ASPPageListDB(connectionString);
        
			DataSet data = pageList.GetASPPageListListDS(ddlSelect.SelectedIndex);
			dgPageList.DataSource = data;
			dgPageList.DataKeyField = "APLIntNo";
			dgPageList.DataBind();

			// Hide the list and display a message if no users exist
			if (dgPageList.Items.Count == 0) 
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
				dgPageList.Visible = false;
				lblError.Visible = true;
			}
			else
			{
				lblError.Visible = false;
				dgPageList.Visible = true;
			}

			data.Dispose();

			pnlAddPage.Visible = false;
			pnlPageDetails.Visible = false;
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			int aplIntNo = Convert.ToInt32(Session["editAPLIntNo"]);

			string sysAdminOnly = "N";
			if (chkAPLSysAdminOnly.Checked) 
				sysAdminOnly = "Y";

			Stalberg.TMS.ASPPageListDB pageList = new ASPPageListDB(connectionString);
			
			int updAPLIntNo = pageList.UpdateASPPageList(aplIntNo, txtAPLPageName.Text,
				txtAPLPageURL.Text, txtAPLPageDescr.Text, sysAdminOnly, loginUser);

			if (updAPLIntNo.Equals("-1"))
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
				lblError.Visible = true;
			}
			else
			{
				lblError.Text = string.Empty;
				lblError.Visible = false;
			}

			//populate grid
			BindGrid();

            lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
			lblError.Visible = true;
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			string sysAdminOnly = "N";
			if (chkAddAPLSysAdminOnly.Checked) 
				sysAdminOnly = "Y";

			Stalberg.TMS.ASPPageListDB pageList = new ASPPageListDB(connectionString);
			
			int addAPLIntNo = pageList.AddASPPageList(txtAddAPLPageName.Text, txtAddAPLPageURL.Text,
				txtAddAPLPageDescr.Text, sysAdminOnly, loginUser);

			if (addAPLIntNo.Equals("-1"))
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
				lblError.Visible = true;
			}
			else
			{
				lblError.Text = string.Empty;
				lblError.Visible = false;
			}

			//populate grid
			BindGrid();

            lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
			lblError.Visible = true;

		}

		protected void dgPageList_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgPageList.SelectedIndex = e.Item.ItemIndex;

			if (dgPageList.SelectedIndex > -1) 
			{			
				int editAPLIntNo = Convert.ToInt32(dgPageList.DataKeys[dgPageList.SelectedIndex]);

				Session["editAPLIntNo"] = editAPLIntNo;
				Session["prevPage"] = thisPageURL;

				ShowPageDetails(editAPLIntNo);
			}
		}

		protected void ShowPageDetails(int editAPLIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.ASPPageListDB page = new Stalberg.TMS.ASPPageListDB(connectionString);
			
			Stalberg.TMS.ASPPageListDetails pageDetails = page.GetASPPageListDetails(editAPLIntNo);

			txtAPLPageName.Text = pageDetails.APLPageName;
			txtAPLPageDescr.Text = pageDetails.APLPageDescr;
			txtAPLPageURL.Text = pageDetails.APLPageURL;
			
			if (pageDetails.APLSysAdminOnly.Equals("Y"))
				chkAPLSysAdminOnly.Checked = true;
			else
				chkAPLSysAdminOnly.Checked = false;

			pnlPageDetails.Visible = true;
			pnlAddPage.Visible  = false;
		}

		protected void dgPageList_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			int aplIntNo = Convert.ToInt32(Session["editAPLIntNo"]);

			Stalberg.TMS.ASPPageListDB page = new ASPPageListDB(connectionString);
			
			string delAPLIntNo = page.DeleteASPPageList(aplIntNo);

			if (delAPLIntNo.Equals("-1"))
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
				lblError.Visible = true;
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
				lblError.Visible = true;
			}

			//populate grid
			BindGrid(); 
		
		}

		protected void dgPageList_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgPageList.CurrentPageIndex = e.NewPageIndex;
			BindGrid();
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			pnlAddPage.Visible = true;
			pnlPageDetails.Visible = false;	
			txtAddAPLPageDescr.Text = string.Empty;
			txtAddAPLPageName.Text = string.Empty;
			txtAddAPLPageURL.Text = string.Empty;
			chkAddAPLSysAdminOnly.Checked = false;
		}

		protected void ddlSelect_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindGrid();
		}

		protected void btnHideMenu_Click(object sender, System.EventArgs e)
		{
			if (pnlMainMenu.Visible.Equals(true))
			{
				pnlMainMenu.Visible = false;
                btnHideMenu.Text = (string)GetLocalResourceObject("btnShowMenu.Text");
			}
			else
			{
				pnlMainMenu.Visible = true;
                btnHideMenu.Text = (string)GetLocalResourceObject("btnHideMenu.Text");
			}
		}

	}
}
