<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup"
    TagPrefix="uc1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Rejection"
    CodeBehind="Rejection.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
 <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 10%;">
        <tr>
            <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <!-- place update panel here -->
    <asp:UpdatePanel ID="UPD" runat="server">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" border="0" height="85%">
                <tr>
                    <td align="center" valign="top" style="height: 1249px">
                        <img style="height: 1px" src="images/1x1.gif" width="167">
                        <asp:Panel ID="pnlMainMenu" runat="server">
                        </asp:Panel>
                        <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                            BorderColor="#000000">
                            <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                                <tr>
                                    <td align="center" style="width: 83px">
                                        <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 83px">
                                        <asp:Button ID="btnOptAdd" runat="server" CssClass="NormalButton" OnClick="btnOptAdd_Click"
                                            Text="<%$Resources:btnOptAdd.Text %>" Width="135px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 83px">
                                        <asp:Button ID="btnOptAddRejectionReason" runat="server" Width="135px" CssClass="NormalButton"
                                            Text="<%$Resources:btnOptAddRejectionReason.Text %>" OnClick="btnOptAddRejectionReason_Click">
                                        </asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 83px">
                                        <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                            Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 83px">
                                        <asp:Button ID="btnOptDeleteReason" runat="server" Width="135px" CssClass="NormalButton"
                                            Text="<%$Resources:btnOptDeleteReason.Text %>" OnClick="btnOptDeleteReason_Click">
                                        </asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 83px">
                                        <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                            Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 83px">
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td valign="top" align="left" width="100%" colspan="1" style="height: 1249px">
                        <table border="0" width="568" height="482">
                            <tr>
                                <td valign="top" height="47">
                                    <p align="center">
                                        <asp:Label ID="lbPageHeader" runat="server" Width="379px" CssClass="ContentHead"
                                            Text="<%$Resources:lbPageHeader.Text %>"></asp:Label>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>&nbsp;</p>
                                    <p>
                                        <asp:DataGrid ID="dgRejCategory" runat="server" AllowPaging="True" AlternatingItemStyle-CssClass="CartListItemAlt"
                                            AutoGenerateColumns="False" BorderColor="Black" CellPadding="4" Font-Size="8pt"
                                            FooterStyle-CssClass="cartlistfooter" GridLines="Vertical" HeaderStyle-CssClass="CartListHead"
                                            ItemStyle-CssClass="CartListItem" ShowFooter="True" Width="495px" OnSelectedIndexChanged="dgRejCategory_SelectedIndexChanged"
                                            OnItemCommand="dgRejCategory_ItemCommand">
                                            <Columns>
                                                <asp:BoundColumn DataField="RejCIntNo" HeaderText="RejIntNo" Visible="False"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="RejCCategory" HeaderText="<%$Resources:dgRejCategory.HeaderText %>">
                                                </asp:BoundColumn>
                                                <asp:ButtonColumn Text="<%$Resources:dgRejCategoryItem.Text %>" CommandName="Select">
                                                </asp:ButtonColumn>
                                                <asp:ButtonColumn Text="<%$Resources:dgRejCategoryItem.Text1 %>" CommandName="Edit">
                                                </asp:ButtonColumn>
                                            </Columns>
                                            <FooterStyle CssClass="cartlistfooter" />
                                            <AlternatingItemStyle CssClass="CartListItemAlt" />
                                            <ItemStyle CssClass="CartListItem" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <PagerStyle Mode="NumericPages" />
                                        </asp:DataGrid>&nbsp;</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="height: 47px">
                                    <asp:Panel ID="pnlAddCategory" runat="server" Height="127px" Visible="False">
                                        <table id="Table1" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td width="157" height="2">
                                                    <asp:Label ID="lbRejCategory" runat="server" CssClass="ProductListHead" Width="192px"
                                                        Text="<%$Resources:lbRejCategory.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2">
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" height="2" valign="top">
                                                    <asp:Label ID="lblRejectCategory" runat="server" CssClass="NormalBold" Text="<%$Resources:lblRejectCategory.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2" valign="top">
                                                    <asp:TextBox ID="txtAddRejCategory" runat="server" CssClass="NormalMand" Height="24px"
                                                        MaxLength="50" Width="204px"></asp:TextBox>
                                                </td>
                                                <td height="2">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddRejCategory"
                                                        CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCategory.ErrorMessage %>"
                                                        ForeColor=" " Width="210px" ValidationGroup="AddRegCategory"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                        <tr bgcolor='#FFFFFF'>
                                                            <td height="100">
                                                                <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>"
                                                                    Width="265"></asp:Label>
                                                            </td>
                                                            <td height="100">
                                                                <uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                </td>
                                                <td width="248">
                                                    <asp:CheckBox ID="chkRejCategory" runat="server" CssClass="NormalBold" Text="<%$Resources:chkRejCategory.Text %>"
                                                        Width="277px" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddRejCat" runat="server" CssClass="NormalButton" Text="<%$Resources:lblAddRejCategory.Text %>"
                                                        OnClick="btnAddRejCat_Click" OnClientClick="return VerifytLookupRequired()"  ValidationGroup="AddRegCategory" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td height="190" valign="top">
                                    <asp:Panel ID="pnlUpdateRejCategory" runat="server" Height="127px" Visible="False">
                                        <table id="Table3" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td width="157">
                                                    <asp:Label ID="lbRejCatUpdate" runat="server" Width="224px" CssClass="ProductListHead"
                                                        Text="<%$Resources:lbRejCatUpdate.Text %>"></asp:Label>
                                                </td>
                                                <td width="248">
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="157">
                                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblRejectCategory.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="248">
                                                    <asp:TextBox ID="txtRejectUpdate" runat="server" Width="204px" CssClass="NormalMand"
                                                        Height="24px" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td valign="top">
                                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                        CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCategory.ErrorMessage %>"
                                                        ControlToValidate="txtRejectUpdate" ForeColor=" " ValidationGroup="EditRegCategory"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                        <tr bgcolor='#FFFFFF'>
                                                            <td height="100">
                                                                <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>"
                                                                    Width="265"> </asp:Label>
                                                            </td>
                                                            <td height="100">
                                                                <uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                </td>
                                                <td width="248">
                                                    <asp:CheckBox ID="chkRejSkip" runat="server" CssClass="NormalBold" Text="<%$Resources:chkRejCategory.Text %>"
                                                        Width="247px" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                        OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()" ValidationGroup="EditRegCategory"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td height="47" valign="top">
                                    <asp:DataGrid ID="dgRejectionReason" Width="495px" runat="server" BorderColor="Black"
                                        AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                        FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                        Font-Size="8pt" CellPadding="4" GridLines="Vertical" OnItemCommand="dgRejectionReason_ItemCommand"
                                        AllowPaging="True" OnPageIndexChanged="dgRejectionReason_PageIndexChanged">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <SelectedItemStyle CssClass="RowHighlighted" />
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="RejCIntNo" HeaderText="RejCIntNo"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="RejReason" HeaderText="<%$Resources:dgRejectionReason.HeaderText %>">
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgRejCategoryItem.Text1 %>" CommandName="Edit">
                                            </asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td height="47" valign="top">
                                    <br />
                                    <asp:Panel ID="pnlAddRejectionReason" runat="server" Height="127px" Visible="False">
                                        <table id="Table4" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td width="157" height="2">
                                                    <asp:Label ID="lbRejectionReason" runat="server" CssClass="ProductListHead" Width="192px"
                                                        Text="<%$Resources:lbRejectionReason.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2">
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" height="2" valign="top">
                                                    <asp:Label ID="lbRejCategoryreason" runat="server" CssClass="NormalBold" Text="<%$Resources:lblRejectCategory.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2" valign="top">
                                                    <asp:DropDownList ID="ddlRejectionCategory" runat="server" CssClass="NormalMand"
                                                        Height="24px" Width="204px" />
                                                </td>
                                                <td height="2">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlRejectionCategory"
                                                        CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCategory.ErrorMessage %>"
                                                        ForeColor=" " Width="210px"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                    <asp:Label ID="lbRejectReason" runat="server" CssClass="NormalBold" Text="<%$Resources:lbRejectReason.Text %>"></asp:Label>
                                                </td>
                                                <td width="248">
                                                    <asp:TextBox ID="txtRejectReason" runat="server" CssClass="NormalMand" Height="24px"
                                                        MaxLength="50" Width="204px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtRejectReason"
                                                        CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqReason.ErrorMessage %>"
                                                        ForeColor=" " Width="210px" ValidationGroup="AddRejection"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                        <tr bgcolor='#FFFFFF'>
                                                            <td height="100">
                                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>"
                                                                    Width="265"></asp:Label>
                                                            </td>
                                                            <td height="100">
                                                                <uc1:UCLanguageLookup ID="ucLanguageLookupAdd1" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" style="height: 26px">
                                                </td>
                                                <td width="248" style="height: 26px">
                                                    <asp:CheckBox ID="chkAddRejectionReason" runat="server" CssClass="NormalBold" Text="<%$Resources:chkRejCategory.Text %>"
                                                        Width="277px" />
                                                </td>
                                                <td style="height: 26px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                </td>
                                                <td width="248">
                                                    <asp:CheckBox ID="chkAddNatisBSkip" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddNatisBSkip.Text %>"
                                                        Width="277px" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                    &nbsp;
                                                </td>
                                                <td width="248">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddRejectionReason" runat="server" CssClass="NormalButton" OnClick="btnAddRejectionReason_Click"
                                                        Text="<%$Resources:btnAddRejectionReason.Text %>" OnClientClick="return VerifytLookupRequired()"  ValidationGroup="AddRejection" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    &nbsp; &nbsp;&nbsp;
                                    <asp:Panel ID="pnlRejReasUpdate" runat="server" Height="127px" Visible="False">
                                        <table id="Table2" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td width="157" style="height: 21px">
                                                    <asp:Label ID="lbUpdateReason" runat="server" CssClass="ProductListHead" Width="224px"
                                                        Text="<%$Resources:lbUpdateReason.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" style="height: 21px">
                                                </td>
                                                <td style="height: 21px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 21px" width="157">
                                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblRejectCategory.Text %>"></asp:Label>
                                                </td>
                                                <td style="height: 21px" width="248">
                                                    <asp:DropDownList ID="ddlUpdateRejectionCategory" runat="server" CssClass="NormalMand"
                                                        Height="24px" Width="204px" AutoPostBack="True" OnSelectedIndexChanged="ddlUpdateRejectionCategory_SelectedIndexChanged" />
                                                </td>
                                                <td style="height: 21px">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlUpdateRejectionCategory"
                                                        CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCategory.ErrorMessage %>"
                                                        ForeColor=" " Width="210px"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="157">
                                                    <asp:Label ID="lbRejection" runat="server" CssClass="NormalBold" Text="<%$Resources:lbRejection.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="248">
                                                    <asp:TextBox ID="txtUpdateRejection" runat="server" CssClass="NormalMand" Height="24px"
                                                        MaxLength="50" Width="204px"></asp:TextBox>
                                                </td>
                                                <td valign="top">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUpdateRejection"
                                                        CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCategory.ErrorMessage %>"
                                                        ForeColor=" " Width="210px" ValidationGroup="EditRejection"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                        <tr bgcolor='#FFFFFF'>
                                                            <td height="100">
                                                                <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>"
                                                                    Width="265"> </asp:Label>
                                                            </td>
                                                            <td height="100">
                                                                <uc1:UCLanguageLookup ID="ucLanguageLookupUpdate1" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                    <asp:Button ID="btnDeleteRejection" runat="server" CssClass="NormalButton" Text="<%$Resources:btnDeleteRejection.Text %>"
                                                        OnClick="btnDeleteRejection_Click" CausesValidation="False" />
                                                </td>
                                                <td width="248">
                                                    <asp:CheckBox ID="chkUpdateCSkip" runat="server" CssClass="NormalBold" Text="<%$Resources:chkRejCategory.Text %>"
                                                        Width="247px" />
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                </td>
                                                <td width="248">
                                                    <asp:CheckBox ID="chkupdateNatisBSkip" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddNatisBSkip.Text %>"
                                                        Width="247px" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                    &nbsp;
                                                </td>
                                                <td width="248">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnUpdateRejection" runat="server" CssClass="NormalButton" OnClick="btnUpdateRejection_Click"
                                                        Text="<%$Resources:btnUpdateRejection.Text %>" OnClientClick="return VerifytLookupRequired()"  ValidationGroup="EditRejection" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
