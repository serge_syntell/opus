using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.Data.Datasets;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for FirstNotice1.
    /// </summary>
    public partial class WOAViewer : System.Web.UI.Page
    {
        // Fields
        private string login = string.Empty;
        private string connectionString = string.Empty;
        private Int32 autIntNo = 0;
        private string thisPage = "WOA Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "WOAViewer.aspx";
        protected string loginUser = string.Empty;

        #region 2012-01-20 jerry disabled for backup
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        //protected override void OnLoad(System.EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    // Get user details
        //    Stalberg.TMS.UserDB user = new UserDB(connectionString);
        //    Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
        //    this.login = userDetails.UserLoginName;

        //    if (Request.QueryString["printfile"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    autIntNo = (int)Session["printAutIntNo"];

        //    // Set domain specific variables
        //    General gen = new General();
        //    backgroundImage = gen.SetBackground(Session["drBackground"]);
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    // Generate the report details
        //    ReportDocument reportDoc = new ReportDocument();

        //    //// Check the QueryString for the 'Summary' flag
        //    //bool isSummary = (Request.QueryString["Summary"] != null);
        //    string printFile = Request.QueryString["printfile"].ToString().Trim();
        //    //string sMode = Request.QueryString["mode"] == null ? "N" : Request.QueryString["mode"].ToString().Trim();

        //    AuthReportNameDB arn = new AuthReportNameDB(this.connectionString);
        //    string reportPage = arn.GetAuthReportName(autIntNo, "WOAPrint");

        //    //if (!isSummary)
        //    //{
        //    if (reportPage.Equals(string.Empty))
        //    {
        //        reportPage = "WOA_Personal_DotMatrix.rpt";
        //        arn.AddAuthReportName(autIntNo, reportPage, "WOAPrint", "System", "");
        //    }
        //    // 2011-10-09 jerry changed
        //    //else
        //    //{
        //    //    reportPage = "WOA_Personal_DotMatrix.rpt";
        //    //}

        //    string reportPath = Server.MapPath("reports/" + reportPage);

        //    //****************************************************
        //    //SD:  20081120 - check that report actually exists
        //    string templatePath = string.Empty;
        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "WOAPrint");

        //    if (!File.Exists(reportPath))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //****************************************************

        //    reportDoc.Load(reportPath);

        //    // Get the PageMargins structure and set the margins for the report.
        //    PageMargins margins = reportDoc.PrintOptions.PageMargins;
        //    margins.leftMargin = 0;
        //    margins.topMargin = 0;
        //    margins.rightMargin = 0;
        //    margins.bottomMargin = 0;

        //    // Apply the page margins.
        //    reportDoc.PrintOptions.ApplyPageMargins(margins);

        //    // Retrieve the data source
        //    string tempFile = printFile;
        //    SqlConnection con = new SqlConnection(connectionString);

        //    //2011-10-08 jerry changed
        //    SqlCommand com;
        //    if (reportPage.ToUpper().IndexOf("MB") > 1)
        //    {
        //        com = new SqlCommand("WOAPrintWOA_MM", con);
        //    }
        //    else
        //    {
        //        com = new SqlCommand("WOAPrintWOA", con);
        //    }

        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = tempFile;
        //    //com.Parameters.Add("@NoDays", SqlDbType.Int, 4).Value = rule.DtRNoOfDays;
        //    //com.Parameters.Add("@Mode", SqlDbType.VarChar, 1).Value = sMode;
        //    //mrs 20081110 added output parameter to test for rollback conditions
        //    //  parameter contains null - no good
        //    //com.Parameters.Add("@ErrorReturn", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
        //    SqlDataAdapter da = new SqlDataAdapter(com);

        //    //int errorReturn = 0;
        //    //errorReturn = int.Parse(com.Parameters["@ErrorReturn"].Value.ToString());

        //    dsPrintWOA ds = new dsPrintWOA();
        //    da.Fill(ds);
        //    da.Dispose();

        //    //if (errorReturn < 0)
        //    //{
        //    //    switch (errorReturn)
        //    //    {
        //    //        case -2:
        //    //            Response.Write("There was a problem updating the print date on the print file");
        //    //            Response.End();
        //    //            return;
        //    //        case -3:
        //    //            Response.Write("There was a problem inserting the evidence pack row");
        //    //            Response.End();
        //    //            return;
        //    //    }
        //    //}
        //    //else

        //    if (ds.Tables[1].Rows.Count == 0)
        //    {
        //        ds.Dispose();
        //        Response.Write("There are no WOA's matching your search");
        //        Response.End();
        //        return;
        //    }

        //    reportDoc.PrintOptions.PaperSize = PaperSize.PaperFanfoldStdGerman; // 8.5" x 12"

        //    // Bind the report to the data
        //    reportDoc.SetDataSource(ds.Tables[1]);
        //    ds.Dispose();

        //    // Perform the export
        //    MemoryStream ms = new MemoryStream();
        //    ms = (MemoryStream)reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
        //    reportDoc.Dispose();

        //    // Insert the PDF file as the only content in the Web stream
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/pdf";
        //    Response.BinaryWrite(ms.ToArray());
        //    Response.End();

        //}
        #endregion

        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            if (Request.QueryString["printfile"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //autIntNo = (int)Session["printAutIntNo"];
            if (Session["printAutIntNo"] != null)
                int.TryParse(Session["printAutIntNo"].ToString(), out this.autIntNo);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //jerry 2012-01-20, use module to create print pdf file
            PrintFileProcess process = new PrintFileProcess(this.connectionString, this.login);
            process.BuildPrintFile(new PrintFileModuleWOA(this.login), this.autIntNo, Request.QueryString["printfile"]);

            if (Request.QueryString["printType"] != null && Request.QueryString["printType"] == "PrintWOA")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintWOA, PunchAction.Other);
            }
        }
    }
}
