using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
//using BarcodeNETWorkShop;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF.Merger;
using System.Text.RegularExpressions;
using Stalberg.TMS.Data.Util;
using System.Collections.Generic;
using System.Drawing;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for Second Notices.
    /// </summary>
    public partial class SecondNoticeViewer : DplxWebForm               //System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int _status = 260;
        private string _showAll = "N";
        private string printFile = string.Empty;
        private int autIntNo = 0;
        private string loginUser;
        private int _option = 2;              //2nd Notice

        private const int FIRST_NOTICE = 1;
        private const int SECOND_NOTICE = 2;

        protected string styleSheet;
        protected string backgroundImage;

        protected string thisPageURL = "SecondNoticeViewer.aspx";
        protected string thisPage = "View Second Notices";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;


        // Remove BarcodeNewImage instead of SIL.AARTO.BLL.BarCode  Jake 2011-02-25
        //BarcodeNETImage barcode = null;
        ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phBarCode;
        System.Drawing.Image barCodeImage = null;

        //byte[] b = null;

        //private ConnectionInfo crConnectionInfo = new ConnectionInfo();

        #region 20120301 Oscar disabled for backup
        ///// <summary>
        ///// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        ///// </summary>
        ///// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        //protected override void OnLoad(EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    //get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    //get user details
        //    Stalberg.TMS.UserDB user = new UserDB(connectionString);
        //    Stalberg.TMS.UserDetails userDetails = new UserDetails();

        //    userDetails = (UserDetails)Session["userDetails"];
        //    loginUser = userDetails.UserLoginName;

        //    //string autCode = GetAuthCode();
        //    //string reportPath = string.Empty;
        //    //autIntNo = Convert.ToInt32(Session["AutIntNo"]);
        //    autIntNo = Convert.ToInt32(Session["printAutIntNo"]);

        //    if (Request.QueryString["printfile"].ToString() == null)
        //    {
        //        Response.Write("There was no print file supplied!");
        //        Response.End();
        //        return;
        //    }

        //    printFile = Request.QueryString["printfile"].ToString(); // Ciprus print request file name or none is the default
        //    title = printFile;

        //    //Modify by Tod Zhang on 090927
        //    string prefix = string.Empty;
        //    string longPrefix = string.Empty;
        //    string extendedPrefix = string.Empty;

        //    string sTempEng = string.Empty;
        //    string sTempAfr = string.Empty;
        //    DocumentLayout doc;

        //    int noticeStage = SECOND_NOTICE;

        //    AuthorityRulesDB authRules = new AuthorityRulesDB(connectionString);
        //    NoticeDB notice = new NoticeDB(this.connectionString);
        //    DataSet ds = null;

        //    if (printFile != "-1")
        //    {
        //        prefix = this.printFile.Substring(0, 3).ToUpper();
        //        longPrefix = printFile.Substring(0, 7).ToUpper();
        //        extendedPrefix = printFile.Substring(0, 11).ToUpper();

        //        // See if its a single notice
        //        string pattern = @"^\w{2,}/\d{2,}/\d{2,}/\d{3,}$";
        //        Regex regex = new Regex(pattern, RegexOptions.Singleline);
        //        if (regex.IsMatch(this.printFile))
        //        {
        //            if (!this.printFile[0].Equals('*'))
        //                this.printFile = "*" + this.printFile;

        //            ds = notice.GetNoticeCheckRLVDS(this.printFile.Substring(1, this.printFile.Length - 1));
        //        }
        //    }

        //    if (ds != null)
        //    {
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            DataRow dr = ds.Tables[0].Rows[0];
        //            prefix = dr["PrintFileName"].ToString();
        //            //add the check for the status of the notice here
        //            noticeStage = Convert.ToInt16(dr["NoticeStage"]);
        //        }

        //        ds.Dispose();
        //    }

        //    // AuthorityRulesDetails arDet1 = authRules.GetAuthorityRulesDetailsByCode(autIntNo, "2550", "Rule for re-printing a notice using the format as determined by the current status of the notice", 0, "Y", "Y = Notice will be printed based on ChargeStatus : < 260 -> 1st notice, >= 260 -> 2nd Notice (default); N = Notice will be printed based on which Notice Print viewer screen is used", loginUser);
        //    //AuthorityRulesDetails arDet2 = authRules.GetAuthorityRulesDetailsByCode(autIntNo, "4250", "Rule for Whether to Print Second Notices", 1, "Y", "1 or Y = true, second notices should be printed for authority (default).", loginUser);

        //    //20090113 SD	
        //    //AutIntNo, ARCode and LastUser need to be set from here
        //    AuthorityRulesDetails arDet1 = new AuthorityRulesDetails();
        //    arDet1.AutIntNo = autIntNo;
        //    arDet1.ARCode = "2550";
        //    arDet1.LastUser = this.loginUser;

        //    DefaultAuthRules authRule = new DefaultAuthRules(arDet1, this.connectionString);
        //    KeyValuePair<int, string> valueArDet1 = authRule.SetDefaultAuthRule();

        //    //20090113 SD	
        //    //AutIntNo, ARCode and LastUser need to be set from here
        //    AuthorityRulesDetails arDet2 = new AuthorityRulesDetails();
        //    arDet2.AutIntNo = this.autIntNo;
        //    arDet2.ARCode = "4250";
        //    arDet2.LastUser = this.loginUser;

        //    DefaultAuthRules authRule1 = new DefaultAuthRules(arDet2, this.connectionString);
        //    KeyValuePair<int, string> valueArDet2 = authRule1.SetDefaultAuthRule();

        //    ////if the rule for printing 2nd notice is turned off or they want see the notice based on the selected viewer, regardless of status
        //    //if (arDet1.ARString.Equals("N") || arDet2.ARString.Equals("N") || arDet2.ARNumeric == 0)
        //    //    noticeStage = SECOND_NOTICE;

        //    //if the rule for printing 2nd notice is turned off or they want see the notice based on the selected viewer, regardless of status
        //    if (valueArDet1.Value.Equals("N") || valueArDet2.Value.Equals("N") || valueArDet2.Key == 0)
        //        noticeStage = SECOND_NOTICE;

        //    if (noticeStage == FIRST_NOTICE)
        //    //have to place this here to cater for printing 1st notices for new offenders/changed regno's
        //    {
        //        Response.Redirect("FirstNoticeViewer.aspx?printfile=" + printFile);
        //    }

        //    //// See if its a single notice
        //    //string pattern = @"^\w{2,}/\d{2,}/\d{2,}/\d{3,}$";
        //    //Regex regex = new Regex(pattern, RegexOptions.Singleline);
        //    //if (regex.IsMatch(this.printFile))
        //    //{
        //    //    if (!this.printFile[0].Equals('*'))
        //    //        this.printFile = "*" + this.printFile;

        //    //    NoticeDB notice = new NoticeDB(this.connectionString);
        //    //    DataSet ds = notice.GetNoticeCheckRLVDS(this.printFile.Substring(1, this.printFile.Length - 1));
        //    //    if (ds.Tables[0].Rows.Count > 0)
        //    //    {
        //    //        DataRow dr = ds.Tables[0].Rows[0];
        //    //        prefix = dr["PrintFileName"].ToString();
        //    //        //add the check for the status of the notice here
        //    //        noticeStage = Convert.ToInt16(dr["NoticeStage"]);
        //    //    }

        //    //    ds.Dispose();

        //    //    AuthorityRulesDetails arDet1 = authRules.GetAuthorityRulesDetailsByCode(autIntNo, "2550", "Rule for re-printing a notice using the format as determined by the current status of the notice", 0, "Y", "Y = Notice will be printed based on ChargeStatus : < 260 -> 1st notice, >= 260 -> 2nd Notice (default); N = Notice will be printed based on which Notice Print viewer screen is used", loginUser);
        //    //    AuthorityRulesDetails arDet2 = authRules.GetAuthorityRulesDetailsByCode(autIntNo, "4250", "Rule for Whether to Print Second Notices", 1, "Y", "1 or Y = true, second notices should be printed for authority (default).", loginUser);

        //    //    //if the rule for printing 2nd notice is turned off or they want see the notice based on the selected viewer, regardless of status
        //    //    if (arDet1.ARString.Equals("N") || arDet2.ARString.Equals("N") || arDet2.ARNumeric == 0)
        //    //        noticeStage = SECOND_NOTICE;

        //    //    if (noticeStage == FIRST_NOTICE)
        //    //    //have to place this here to cater for printing 1st notices for new offenders/changed regno's
        //    //    {
        //    //        Response.Redirect("FirstNoticeViewer.aspx?printfile=" + printFile);
        //    //    }

        //    //}

        //    //BD 20090128 - removing of auth rule to control what ticket processing being used, now using AutTicketProcessor
        //    AuthorityDB authDB = new AuthorityDB(this.connectionString);
        //    AuthorityDetails authDetails = authDB.GetAuthorityDetails(autIntNo);
        //    string format = "TMS";
        //    if (authDetails.AutTicketProcessor == "CiprusPI" || authDetails.AutTicketProcessor == "Cip_CofCT")
        //        format = authDetails.AutTicketProcessor;

        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);
        //    string reportPage = arn.GetAuthReportName(autIntNo, "SecondNotice");
        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "SecondNotice");

        //    if (reportPage.Equals(string.Empty))
        //    {
        //        int arnIntNo = arn.AddAuthReportName(autIntNo, "SecondNotice.rpt", "SecondNotice", "system", "SecondNoticeTemplate_PL.pdf");
        //        reportPage = "SecondNotice_PL.dplx";
        //    }

        //    //****************************************************

        //    //dls 090710 - need to make all the string checks ToUpper - user doesn't necessarily use the same case
        //    // Check the print file prefix
        //    //switch (prefix)
        //    switch (prefix.ToUpper())
        //    {
        //        case "ASD": // Average Speed Over Distance
        //            reportPage = arn.GetAuthReportName(autIntNo, "ASDSecondNotice");

        //            if (reportPage.Equals(string.Empty))
        //            {
        //                reportPage = "SecondNotice_CofCT_ASD.dplx";
        //                arn.AddAuthReportName(autIntNo, reportPage, "ASDSecondNotice", "System", "SecondNoticeTemplate_CofCT_ASD.pdf");
        //            }

        //            sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "ASDSecondNotice");
        //            break;

        //        case "REG": // Change of registration
        //            switch (longPrefix)
        //            {
        //                //at the moment everyone except JMPD uses the same report for RLV as for SPD, e.g. FirstNotice_ST.rpt
        //                case "REG_RLV":
        //                case "REG_SPD":
        //                default:
        //                    Response.Redirect("ChangeRegNo_Viewer.aspx?printfile=" + printFile);
        //                    //reportPage = arn.GetAuthReportName(autIntNo, "ChangeRegNo2ndNotice");
        //                    //sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "ChangeRegNo2ndNotice");
        //                    //if (reportPage.Equals(string.Empty))
        //                    //{
        //                    //    reportPage = "ChangeRegNo2ndNotice.dplx";
        //                    //    //arn.AddAuthReportName(autIntNo, reportPage, "ChangeRegNo2ndNotice", "System");
        //                    //    arn.AddAuthReportName(autIntNo, reportPage, "ChangeRegNo2ndNotice", "System", "SecondNoticeTemplate_ST.pdf");
        //                    //}
        //                    break;
        //            }
        //            break;
        //        case "HWO": //jerry 2011-3-2 add
        //            Response.Redirect("SecondNotice_HWO_Viewer.aspx?printfile=" + printFile);
        //            break;
        //        default:
        //            if (extendedPrefix.IndexOf("EPR") > 0)
        //            {
        //                //dls 080118 - add extra option for printing these EasyPay Retrofitted notices - their status values will be all over the place
        //                _option = 3;
        //            }
        //            break;
        //    }

        //    //if (reportPage.IndexOf("ChangeRegNo") >= 0)
        //    //{
        //    //    //Response.Redirect("ChangeRegNo_Viewer.aspx?printfile=" + printFile);
        //    //    PrintChangeOfRegNoNotice(sTemplate, reportPage);
        //    //    return;
        //    //}

        //    ////dls 071222 - had to add this to handle the 2nd notice change of regno reports
        //    //if (reportPage.IndexOf("ChangeRegNo") >= 0)
        //    //    Response.Redirect("ChangeRegNo_Viewer.aspx?printfile=" + printFile);

        //    //****************************************************
        //    //SD:  20081120 - check that report actually exists
        //    string templatePath = string.Empty;
        //    string reportPath = Server.MapPath("Reports/" + reportPage);

        //    if (!File.Exists(reportPath))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    doc = new DocumentLayout(reportPath);
        //    Query query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;
        //    ParameterDictionary parameters = new ParameterDictionary();

        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblId = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblId");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReference = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReference");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReferenceB = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReferenceB");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFormattedNotice = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFormattedNotice");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblForAttention = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblForAttention");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblNoticeNumber = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblNoticeNumber");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPaymentInfo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPaymentInfo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblStatRef = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblStatRef");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblTime = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblTime");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblLocDescr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblLocDescr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDescrEng = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDescrEng");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDescrAfr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDescrAfr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCode = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCode");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCameraNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCamera");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOfficerNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOfficerNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFineAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAmount");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPrintDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPrintDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceTime = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceTime");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAut = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblText");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblLocation = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblLocation");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSpeedLimit = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblSpeedLimit");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSpeed = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblSpeed");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOfficer = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOfficer");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRegNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRegNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblIssuedBy = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblIssuedBy");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPaymentDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPaymentDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCourtName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCourtName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblEasyPay");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFilmNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFilmNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFrameNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFrameNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblVehicle = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblVehicle");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAuthorityAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAuthorityAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutTel = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAutTel");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutFax = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAutFax");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblDisclaimer = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblDisclaimer");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReprintDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReprintDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReprintDateText = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReprintDateText");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblMtrName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_MtrName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblMtrDepart = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_MtrDepart");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblMtrPostAddr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_MtrPostAddr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAuthName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AuthName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutDepart = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AutDepart");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutPhysAddr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AutPhysAddr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutPostAddr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AutPostAddr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCourt = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCourt");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblpaypoint = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_paypoint");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCameraA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCameraA");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCameraB = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCameraB");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblMeasurement = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblMeasurement");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblTimeDifference = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblTimeDifference");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl_AuthName2 = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AuthName2");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl_CourtName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCourtName");

        //    phBarCode = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phBarCode");
        //    phBarCode.LaidOut += new PlaceHolderLaidOutEventHandler(ph_BarCode);
        //    //ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phImage = null;

        //    //Tod Zhang 090927 - added file name for CoCT
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPrintFileName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPrintFileName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.PageNumberingLabel PageNumberingLabel1 = (ceTe.DynamicPDF.ReportWriter.ReportElements.PageNumberingLabel)doc.GetElementById("PageNumberingLabel1");


        //    // only do this is its a noaog or first notice
        //    //if (reportPage.IndexOf("_RLV") < 0 && reportPage.IndexOf("SecondNotice") < 0)
        //    //{
        //    //    phImage = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phImage");
        //    //    phImage.LaidOut += new PlaceHolderLaidOutEventHandler(ph_Image);
        //    //}

        //    if (Session["showAllNotices"] != null)
        //        if (Session["showAllNotices"].ToString().Equals("Y")) _showAll = "Y";

        //    //get additional parameters for date rules and sysparam setting
        //    SysParamDB sp = new SysParamDB(this.connectionString);

        //    string violationCutOff = "N";
        //    int spValue = 0;

        //    bool found = sp.CheckSysParam("ViolationCutOff", ref spValue, ref violationCutOff);

        //    DateRulesDetails dateRule = new DateRulesDetails();
        //    dateRule.AutIntNo = autIntNo;
        //    dateRule.DtRStartDate = "NotOffenceDate";
        //    dateRule.DtREndDate = "NotIssue1stNoticeDate";
        //    dateRule.LastUser = this.loginUser;

        //    DefaultDateRules rule = new DefaultDateRules(dateRule, this.connectionString);
        //    int noOfDaysIssue = rule.SetDefaultDateRule();

        //    if (Request.QueryString["printfile"] != null)
        //    {
        //        //autIntNo = Convert.ToInt32(Session["autIntNo"]);
        //        autIntNo = Convert.ToInt32(Session["printAutIntNo"]);

        //        string tempFileLoc = string.Empty;
        //        SqlConnection con = null;
        //        SqlCommand com = null;
        //        SqlDataReader result = null;

        //        try
        //        {
        //            // Fill the DataSet
        //            con = new SqlConnection(this.connectionString);
        //            com = new SqlCommand("NoticePrint", con);
        //            com.CommandType = CommandType.StoredProcedure;
        //            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //            com.Parameters.Add("@PrintFile", SqlDbType.VarChar, 50).Value = printFile;
        //            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = _status;
        //            com.Parameters.Add("@ShowAll", SqlDbType.Char, 1).Value = _showAll;
        //            com.Parameters.Add("@Format", SqlDbType.VarChar, 10).Value = format;
        //            com.Parameters.Add("@Option", SqlDbType.Int, 4).Value = _option;
        //            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = loginUser;
        //            com.Parameters.Add("@ViolationCutOff", SqlDbType.Char, 1).Value = violationCutOff;
        //            com.Parameters.Add("@NoOfDaysIssue", SqlDbType.Int, 4).Value = noOfDaysIssue;

        //            con.Open();
        //            result = com.ExecuteReader(CommandBehavior.CloseConnection);

        //            MergeDocument merge = new MergeDocument();
        //            byte[] buffer;
        //            Document report = null;
        //            int noOfNotices = 0;

        //            char pad0Char = Convert.ToChar("0");

        //            while (result.Read())
        //            {
        //                try
        //                {
        //                    //dls 090710 - need to make all the string checks ToUpper - user doesn't necessarily use the same case
        //                    if (reportPage.ToUpper().IndexOf("_CPI") >= 0)
        //                    {
        //                        lblFormattedNotice.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblReference.Text = "VERWYSING/REFERENCE: " + result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblReferenceB.Text = "VERWYSING/REFERENCE: " + result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim();
        //                        lblPaymentInfo.Text = result["AutNoticePaymentInfo"].ToString().Trim();
        //                    }

        //                    if (reportPage.ToUpper().IndexOf("_PL") >= 0)
        //                    {
        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblOfficer.Text = result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();
        //                        //lblFineAmount.Text = (result["NoticeOption"].ToString().Equals("1") ? result["ChgFineAmount"].ToString() : result["ChgRevFineAmount"].ToString());
        //                        lblFineAmount.Text = string.Format("{0:0.00}", Decimal.Parse(result["ChgRevFineAmount"].ToString()));
        //                        // mrs 20080627 added easypaynumber
        //                        //mrs 20081217 easypay number formatted in stored proc
        //                        //lblEasyPay.Text = ">>>>>" + result["NotEasyPayNumber"].ToString().Trim();
        //                        //mrs 20090109 still need to fill the label
        //                        lblEasyPay.Text = result["NotEasyPayNumber"].ToString().Trim();
        //                    }

        //                    if (reportPage.ToUpper().IndexOf("_ST") >= 0)
        //                    {
        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblEasyPay.Text = result["NotEasyPayNumber"].ToString().Trim();
        //                        lblFilmNo.Text = result["NotFilmNo"].ToString().Trim();
        //                        lblFrameNo.Text = result["NotFrameNo"].ToString().Trim();
        //                        lblVehicle.Text = result["NotVehicleMake"].ToString().Trim();
        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                            lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                        else
        //                            lblId.Text = result["DrvIDNUmber"].ToString().Trim();

        //                        lblOfficer.Text = result["NotOfficerNo"].ToString();
        //                        //lblFineAmount.Text = "R " + (result["NoticeOption"].ToString().Equals("1") ? result["ChgFineAmount"].ToString() : result["ChgRevFineAmount"].ToString());
        //                        lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(result["ChgRevFineAmount"].ToString()));
        //                    }

        //                    if (reportPage.ToUpper().IndexOf("_FS") >= 0)
        //                    {
        //                        lblMtrName.Text = result["MtrName"].ToString().Trim();
        //                        lblMtrDepart.Text = result["MtrDepartName"].ToString().Trim();
        //                        lblMtrPostAddr.Text = (result["MtrPostAddr1"].ToString().Trim().Equals("") ? "" : result["MtrPostAddr1"].ToString().Trim() + "\n" )
        //                            +  (result["MtrPostAddr2"].ToString().Trim().Equals("") ? "" : result["MtrPostAddr2"].ToString().Trim() + "\n" )
        //                            +  (result["MtrPostAddr3"].ToString().Trim().Equals("") ? "" : result["MtrPostAddr3"].ToString().Trim() + "\n" )
        //                            +  (result["MtrPostCode"].ToString().Trim().Equals("") ? "" : result["MtrPostCode"].ToString().Trim());

        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblEasyPay.Text = result["NotEasyPayNumber"].ToString().Trim();
        //                        lblFilmNo.Text = result["NotFilmNo"].ToString().Trim();
        //                        lblFrameNo.Text = result["NotFrameNo"].ToString().Trim();
        //                        lblVehicle.Text = result["NotVehicleMake"].ToString().Trim();
        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                            lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                        else
        //                            lblId.Text = result["DrvIDNUmber"].ToString().Trim();

        //                        lblOfficer.Text = result["NotOfficerNo"].ToString();
        //                        lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(result["ChgRevFineAmount"].ToString()));
        //                    }

        //                    if (reportPage.ToUpper().IndexOf("_RM") >= 0)
        //                    {
        //                        lblAuthName.Text = result["AutName"].ToString().Trim();
        //                        lblAutDepart.Text = result["AutDepartName"].ToString().Trim();

        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblEasyPay.Text = result["NotEasyPayNumber"].ToString().Trim();
        //                        lblFilmNo.Text = result["NotFilmNo"].ToString().Trim();
        //                        lblFrameNo.Text = result["NotFrameNo"].ToString().Trim();
        //                        lblVehicle.Text = result["NotVehicleMake"].ToString().Trim();
        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                            lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                        else
        //                            lblId.Text = result["DrvIDNUmber"].ToString().Trim();

        //                        lblOfficer.Text = result["NotOfficerNo"].ToString();
        //                        lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(result["ChgRevFineAmount"].ToString()));
        //                    }

        //                    if (reportPage.ToUpper().IndexOf("_SW") >= 0)
        //                    {
        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblFilmNo.Text = result["NotFilmNo"].ToString().Trim();
        //                        lblFrameNo.Text = result["NotFrameNo"].ToString().Trim();
        //                        lblVehicle.Text = result["NotVehicleMake"].ToString().Trim();
        //                        lblPaymentInfo.Text = result["AutNoticePaymentInfo"].ToString().Trim();
        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                            lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                        else
        //                            lblId.Text = result["DrvIDNUmber"].ToString().Trim();
        //                    }

        //                    //Modify by Tod Zhang on 090927 
        //                    if (reportPage.ToUpper().IndexOf("_COFCT") >= 0)
        //                    {
        //                        if (reportPage.ToUpper().IndexOf("AOG") < 0)
        //                        {
        //                            lblFormattedNotice.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                            lblOfficer.Text = result["NotOfficerNo"].ToString();
        //                            lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(result["ChgRevFineAmount"].ToString()));

        //                            lblpaypoint.Text = (result["MtrPayPoint1Add1"].ToString().Trim().Equals("") ? "" : result["MtrPayPoint1Add1"].ToString().Trim() + "\n")
        //                                       + (result["MtrPayPoint1Add2"].ToString().Trim().Equals("") ? "" : result["MtrPayPoint1Add2"].ToString().Trim() + "\n")
        //                                       + (result["MtrPayPoint1Add3"].ToString().Trim().Equals("") ? "" : result["MtrPayPoint1Add3"].ToString().Trim() + "\n")
        //                                       + (result["MtrPayPoint1Add4"].ToString().Trim().Equals("") ? "" : result["MtrPayPoint1Add4"].ToString().Trim() + "\n")
        //                                       + result["MtrPayPoint1Code"].ToString();

        //                        }

        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                            lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                        else
        //                            lblId.Text = result["DrvIDNUmber"].ToString().Trim();

        //                        lblPrintFileName.Text = "Batch Number: " + result["Not2ndNoticePrintFile"].ToString();

        //                        int page = noOfNotices + 1;
        //                        PageNumberingLabel1.Text = "Sequential Number: " + page.ToString().PadLeft(4, pad0Char);
        //                    }

        //                    if (reportPage.ToUpper().IndexOf("_RLV_JMPD") >= 0)
        //                    {
        //                        //Jake 2011-02-25 Removed BarcodeNETImage 
        //                        //barcode = new BarcodeNETImage();
        //                        //barcode.BarcodeText = result["NotTicketNo"].ToString();
        //                        //barcode.ShowBarcodeText = false;
        //                        barCodeImage = Code128Rendering.MakeBarcodeImage(result["NotTicketNo"].ToString(), 1, 25, true);

        //                        lblAddress.Text = result["DrvPOAdd1"].ToString().Trim() + "\n" + (result["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : result["DrvPOAdd2"].ToString().Trim()) + "\n"
        //                            + (result["DrvPOAdd3"].ToString().Trim().Equals("") ? "\n" : (result["DrvPOAdd3"].ToString().Trim()) + "\n")
        //                            + (result["DrvPOAdd4"].ToString().Trim().Equals("") ? "\n" : (result["DrvPOAdd4"].ToString().Trim()) + "\n")
        //                            + (result["DrvPOAdd5"].ToString().Trim().Equals("") ? "\n" : (result["DrvPOAdd5"].ToString().Trim()) + "\n")
        //                            + result["DrvPOCode"].ToString().Trim();

        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                            lblForAttention.Text = result["PrxInitials"].ToString() + " " + result["PrxSurname"].ToString() + " as Representative of " + result["DrvSurname"].ToString();
        //                        else
        //                            lblForAttention.Text = result["DrvInitials"].ToString() + " " + result["DrvSurname"].ToString();

        //                        lblCode.Text = result["ChgOffenceCode"].ToString();
        //                        lblFormattedNotice.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        //                        lblAuthorityAddress.Text = (result["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr1"].ToString().Trim() + "\n")
        //                                    + (result["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr2"].ToString().Trim() + "\n")
        //                                    + (result["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr3"].ToString().Trim() + "\n")
        //                                    + (result["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr4"].ToString().Trim() + "\n")
        //                                    + result["AutPhysCode"].ToString();
        //                        lblAutTel.Text = result["AutTel"].ToString();
        //                        lblAutFax.Text = result["AutFax"].ToString();
        //                        lblOffenceDescrAfr.Text = "DEURDAT die bestuurder van voertuig " + result["NotVehicleMake"].ToString().Trim() +
        //                            " met registrasienommer " + result["NotRegNo"].ToString().Trim() + " op " + string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]) +
        //                            " om " + string.Format("{0:HH:mm}", result["NotOffenceDate"]) + " en te \n" +
        //                        result["NotLocDescr"].ToString() + " n openbare pad in die distrik van " + result["AutName"].ToString() + " , n rooi verkeerslig verontagsaam";
        //                        lblOffenceDescrEng.Text = "IN THAT the driver of motor vehicle " + result["NotVehicleMake"].ToString().Trim() +
        //                            " with registration number " + result["NotRegNo"].ToString().Trim() + " on " + string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]) +
        //                            " at " + string.Format("{0:HH:mm}", result["NotOffenceDate"]) + " and at \n" +
        //                        result["NotLocDescr"].ToString() + " a public road in the district of " + result["AutName"].ToString() + " , failed to obey a red light";
        //                        lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint1stNoticeDate"]);
        //                        //dls 071221 - added reprint date
        //                        lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint2ndNoticeDate"]);
        //                    }

        //                    if (reportPage.ToUpper().IndexOf("_GE") >= 0 || reportPage.ToUpper().IndexOf("_BV") >= 0)
        //                    {
        //                        //barcode = new BarcodeNETImage();
        //                        //barcode.BarcodeText = result["NotTicketNo"].ToString();
        //                        //barcode.ShowBarcodeText = false;
        //                        // add by richard 2011-06-03

        //                        lblAuthName.Text = result["AutName"].ToString().Trim();

        //                        //client has changed there mind about the Court Name on the BV notice
        //                        if (lblCourt != null)
        //                            lblCourt.Text = result["NotCourtName"].ToString();

        //                        if (lbl_AuthName2 != null)
        //                            lbl_AuthName2.Text = result["AutName"].ToString();
                                        
        //                        lblAutPhysAddr.Text = (result["AutPhysAddr1"].ToString().Trim().Equals("")?"": result["AutPhysAddr1"].ToString().Trim()+ "\n")
        //                            + (result["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr2"].ToString().Trim() + "\n")
        //                            + (result["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr3"].ToString().Trim()+ "\n") 
        //                            + (result["AutPhysAddr4"].ToString().Trim().Equals("")?"":result["AutPhysAddr4"].ToString().Trim() + "\n" )
        //                            + result["AutPhysCode"].ToString();

        //                        // 2011-7-21 add
        //                        if (reportPage.ToUpper().IndexOf("_GE") >= 0)
        //                        {
        //                            lblAutPostAddr.Text = (result["AutPostAddr1"].ToString().Trim().Equals("") ? "" : result["AutPostAddr1"].ToString().Trim() + "\n")
        //                                + (result["AutPostAddr2"].ToString().Trim().Equals("") ? "" : result["AutPostAddr2"].ToString().Trim() + "\n")
        //                                + (result["AutPostAddr3"].ToString().Trim().Equals("") ? "" : result["AutPostAddr3"].ToString().Trim() + "\n")
        //                                + result["AutPostCode"].ToString().Trim();
                                

        //                        lblpaypoint.Text = (result["MtrPayPoint1Add1"].ToString().Trim().Equals("") ? "" : result["MtrPayPoint1Add1"].ToString().Trim() + "\n")
        //                                        + (result["MtrPayPoint1Add2"].ToString().Trim().Equals("") ? "" : result["MtrPayPoint1Add2"].ToString().Trim() + "\n")
        //                                        + (result["MtrPayPoint1Add3"].ToString().Trim().Equals("") ? "" : result["MtrPayPoint1Add3"].ToString().Trim() + "\n")
        //                                        + (result["MtrPayPoint1Add4"].ToString().Trim().Equals("") ? "" : result["MtrPayPoint1Add4"].ToString().Trim() + "\n")
        //                                        + result["MtrPayPoint1Code"].ToString();
        //                        }



        //                        barCodeImage = Code128Rendering.MakeBarcodeImage(result["NotTicketNo"].ToString(), 1, 25, true);

        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                        {
        //                            lblForAttention.Text = result["PrxInitials"].ToString() + " " + result["PrxSurname"].ToString() + " as Representative of " + result["DrvSurname"].ToString();
        //                            lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                        }
        //                        else
        //                        {
        //                            lblForAttention.Text = result["DrvInitials"].ToString() + " " + result["DrvSurname"].ToString();
        //                            lblId.Text = result["DrvIDNUmber"].ToString().Trim();
        //                        }

        //                        lblAddress.Text = (result["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd1"].ToString().Trim()) + "\n")
        //                             + (result["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd2"].ToString().Trim()) + "\n")
        //                             + (result["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd3"].ToString().Trim()) + "\n")
        //                             + (result["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd4"].ToString().Trim()) + "\n")
        //                             + (result["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd5"].ToString().Trim()) + "\n")
        //                             + result["DrvPOCode"].ToString().Trim();

        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");

        //                        lblStatRef.Text = (result["ChgStatRefLine1"] == DBNull.Value ? " " : result["ChgStatRefLine1"].ToString().Trim())
        //                            + (result["ChgStatRefLine2"] == DBNull.Value ? " " : result["ChgStatRefLine2"].ToString().Trim())
        //                            + (result["ChgStatRefLine3"] == DBNull.Value ? " " : result["ChgStatRefLine3"].ToString().Trim())
        //                            + (result["ChgStatRefLine4"] == DBNull.Value ? " " : result["ChgStatRefLine4"].ToString().Trim());

        //                        lblDate.Text = string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]);
        //                        lblTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                        lblLocDescr.Text = result["NotLocDescr"].ToString();
        //                        lblFilmNo.Text = result["NotFilmNo"].ToString().Trim();
        //                        lblFrameNo.Text = result["NotFrameNo"].ToString().Trim();

        //                        sTempEng = result["OcTDescr"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());
        //                        sTempAfr = result["OcTDescr1"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());

        //                        if (result["NotNewOffender"].ToString().Equals("Y"))
        //                        {
        //                            lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
        //                            lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
        //                        }
        //                        else
        //                        {
        //                            lblOffenceDescrEng.Text = sTempEng;
        //                            lblOffenceDescrAfr.Text = sTempAfr;
        //                        }

        //                        lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", result["Not2ndPaymentDate"]);
        //                        lblCourtName.Text = result["NotCourtName"].ToString().ToUpper();
        //                        lblCode.Text = result["ChgOffenceCode"].ToString();

        //                        //lblCameraNo.Text = result["NotCamSerialNo"].ToString();
        //                        // David Lin 20100812 change layout for ASD 2nd Notice
        //                        if (reportPage.ToUpper().IndexOf("ASD") >= 0)
        //                        {                          
        //                            lblCameraA.Text = result["CameraNoA"].ToString();
        //                            lblCameraB.Text = result["CameraNoB"].ToString();    
        //                            lblMeasurement.Text = result["MeasurementDistance"].ToString() + " metres";                             
        //                            lblTimeDifference.Text = result["TimeDifference"].ToString() + " seconds";
        //                        }
        //                        else
        //                        {
        //                            lblCameraNo.Text = result["NotCamSerialNo"].ToString();
        //                        }

        //                        lblOfficerNo.Text = result["NotOfficerNo"].ToString();
        //                        lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(result["ChgRevFineAmount"].ToString()));

        //                        lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint1stNoticeDate"]);
        //                        lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint2ndNoticeDate"]);

        //                        lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]);
        //                        lblOffenceTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                        lblLocation.Text = result["NotLocDescr"].ToString();
        //                        lblSpeedLimit.Text = result["NotSpeedLimit"].ToString();
        //                        lblSpeed.Text = result["NotSpeed1"].ToString();
        //                        //lblOfficer.Text = result["NotOfficerNo"].ToString();  //result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();
        //                        lblRegNo.Text = result["NotRegNo"].ToString();
        //                        lblDisclaimer.Text = result["Disclaimer"].ToString();

        //                        lblOfficer.Text = result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();
        //                    }

        //                    else
        //                    {
        //                        //2010/9/14 jerry changed for MB
        //                        if (reportPage.ToUpper().IndexOf("_MB") >= 0)
        //                        {
        //                            //barcode = new BarcodeNETImage();
        //                            //barcode.BarcodeText = result["NotTicketNo"].ToString();
        //                            //barcode.ShowBarcodeText = false;
        //                            // add by richard 2011-06-03

        //                            lblAuthName.Text = result["AutName"].ToString().Trim();
        //                            lbl_AuthName2.Text = result["AutName"].ToString();
        //                            lblCourt.Text = result["NotCourtName"].ToString().Trim();
        //                            lbl_CourtName.Text = result["NotCourtName"].ToString().Trim();
        //                            lblAutPhysAddr.Text = (result["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr1"].ToString().Trim() + "\n")
        //                                    + (result["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr2"].ToString().Trim() + "\n")
        //                                    + (result["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr3"].ToString().Trim() + "\n")
        //                                    + (result["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr4"].ToString().Trim() + "\n")
        //                                    + result["AutPhysCode"].ToString();
        //                            lblAutPostAddr.Text = (result["AutPostAddr1"].ToString().Trim().Equals("") ? "" : result["AutPostAddr1"].ToString().Trim() + "\n")
        //                                    + (result["AutPostAddr2"].ToString().Trim().Equals("") ? "" : result["AutPostAddr2"].ToString().Trim() + "\n")
        //                                    + (result["AutPostAddr3"].ToString().Trim().Equals("") ? "" : result["AutPostAddr3"].ToString().Trim() + "\n")
        //                                    + result["AutPostCode"].ToString();


        //                            barCodeImage = Code128Rendering.MakeBarcodeImage(result["NotTicketNo"].ToString(), 1, 25, true);


        //                            if (result["NotSendTo"].ToString().Equals("P"))
        //                            {
        //                                lblForAttention.Text = result["PrxInitials"].ToString() + " " + result["PrxSurname"].ToString() + " as Representative of " + result["DrvSurname"].ToString();
        //                                lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                            }
        //                            else
        //                            {
        //                                lblForAttention.Text = result["DrvInitials"].ToString() + " " + result["DrvSurname"].ToString();
        //                                lblId.Text = result["DrvIDNUmber"].ToString().Trim();
        //                            }

        //                            lblAddress.Text = (result["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd1"].ToString().Trim()) + "\n")
        //                                 + (result["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd2"].ToString().Trim()) + "\n")
        //                                 + (result["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd3"].ToString().Trim()) + "\n")
        //                                 + (result["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd4"].ToString().Trim()) + "\n")
        //                                 + (result["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd5"].ToString().Trim()) + "\n")
        //                                 + result["DrvPOCode"].ToString().Trim();

        //                            lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");

        //                            lblStatRef.Text = (result["ChgStatRefLine1"] == DBNull.Value ? " " : result["ChgStatRefLine1"].ToString().Trim())
        //                                + (result["ChgStatRefLine2"] == DBNull.Value ? " " : result["ChgStatRefLine2"].ToString().Trim())
        //                                + (result["ChgStatRefLine3"] == DBNull.Value ? " " : result["ChgStatRefLine3"].ToString().Trim())
        //                                + (result["ChgStatRefLine4"] == DBNull.Value ? " " : result["ChgStatRefLine4"].ToString().Trim());

        //                            lblDate.Text = string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]);
        //                            lblTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                            lblLocDescr.Text = result["NotLocDescr"].ToString();
        //                            lblFilmNo.Text = result["NotFilmNo"].ToString().Trim();
        //                            lblFrameNo.Text = result["NotFrameNo"].ToString().Trim();

        //                            sTempEng = result["OcTDescr"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());
        //                            sTempAfr = result["OcTDescr1"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());

        //                            if (result["NotNewOffender"].ToString().Equals("Y"))
        //                            {
        //                                lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
        //                                lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
        //                            }
        //                            else
        //                            {
        //                                lblOffenceDescrEng.Text = sTempEng;
        //                                lblOffenceDescrAfr.Text = sTempAfr;
        //                            }

        //                            lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", result["Not2ndPaymentDate"]);
        //                            //lblCourtName.Text = result["NotCourtName"].ToString().ToUpper();
        //                            lblCode.Text = result["ChgOffenceCode"].ToString();

        //                            //lblCameraNo.Text = result["NotCamSerialNo"].ToString();
        //                            // David Lin 20100812 change layout for ASD 2nd Notice
        //                            if (reportPage.ToUpper().IndexOf("ASD") >= 0)
        //                            {
        //                                lblCameraA.Text = result["CameraNoA"].ToString();
        //                                lblCameraB.Text = result["CameraNoB"].ToString();
        //                                lblMeasurement.Text = result["MeasurementDistance"].ToString() + " metres";
        //                                lblTimeDifference.Text = result["TimeDifference"].ToString() + " seconds";
        //                            }
        //                            else
        //                            {
        //                                lblCameraNo.Text = result["NotCamSerialNo"].ToString();
        //                            }

        //                            lblOfficerNo.Text = result["NotOfficerNo"].ToString();
        //                            lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(result["ChgRevFineAmount"].ToString()));

        //                            lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint1stNoticeDate"]);
        //                            lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint2ndNoticeDate"]);

        //                            lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]);
        //                            lblOffenceTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                            lblLocation.Text = result["NotLocDescr"].ToString();
        //                            lblSpeedLimit.Text = result["NotSpeedLimit"].ToString();
        //                            lblSpeed.Text = result["NotSpeed1"].ToString();
        //                            //lblOfficer.Text = result["NotOfficerNo"].ToString();  //result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();
        //                            lblRegNo.Text = result["NotRegNo"].ToString();
        //                            lblDisclaimer.Text = result["Disclaimer"].ToString();

        //                            lblOfficer.Text = result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();

        //                        }
        //                        else
        //                        {

        //                            if (result["NotSendTo"].ToString().Equals("P"))
        //                                lblForAttention.Text = result["PrxInitials"].ToString() + " " + result["PrxSurname"].ToString() + " as Representative of " + result["DrvSurname"].ToString();
        //                            else
        //                                lblForAttention.Text = result["DrvInitials"].ToString() + " " + result["DrvSurname"].ToString();

        //                            lblAddress.Text = (result["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd1"].ToString().Trim()) + "\n")
        //                                 + (result["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd2"].ToString().Trim()) + "\n")
        //                                 + (result["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd3"].ToString().Trim()) + "\n")
        //                                 + (result["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd4"].ToString().Trim()) + "\n")
        //                                 + (result["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd5"].ToString().Trim()) + "\n")
        //                                 + result["DrvPOCode"].ToString().Trim();

        //                            lblStatRef.Text = (result["ChgStatRefLine1"] == DBNull.Value ? " " : result["ChgStatRefLine1"].ToString().Trim())
        //                                + (result["ChgStatRefLine2"] == DBNull.Value ? " " : result["ChgStatRefLine2"].ToString().Trim())
        //                                + (result["ChgStatRefLine3"] == DBNull.Value ? " " : result["ChgStatRefLine3"].ToString().Trim())
        //                                + (result["ChgStatRefLine4"] == DBNull.Value ? " " : result["ChgStatRefLine4"].ToString().Trim());

        //                            //barcode = new BarcodeNETImage();
        //                            //barcode.BarcodeText = result["NotTicketNo"].ToString();
        //                            //barcode.ShowBarcodeText = false;
        //                            barCodeImage = Code128Rendering.MakeBarcodeImage(result["NotTicketNo"].ToString(), 1, 25, true);


        //                            lblDate.Text = string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]);
        //                            lblTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                            lblLocDescr.Text = result["NotLocDescr"].ToString();
        //                            sTempEng = result["OcTDescr"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());
        //                            sTempAfr = result["OcTDescr1"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());

        //                            if (result["NotNewOffender"].ToString().Equals("Y"))
        //                            {
        //                                lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
        //                                lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
        //                            }
        //                            else
        //                            {
        //                                lblOffenceDescrEng.Text = sTempEng;
        //                                lblOffenceDescrAfr.Text = sTempAfr;
        //                            }

        //                            //lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPaymentDate"]);
        //                            lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", result["Not2ndPaymentDate"]);
        //                            lblCourtName.Text = result["NotCourtName"].ToString().ToUpper();
        //                            lblCode.Text = result["ChgOffenceCode"].ToString();

        //                            //lblCameraNo.Text = result["NotCamSerialNo"].ToString();
        //                            // David Lin 20100812 change layout for ASD 2nd Notice
        //                            if (reportPage.ToUpper().IndexOf("ASD") >= 0)
        //                            {
        //                                lblCameraA.Text = result["CameraNoA"].ToString();
        //                                lblCameraB.Text = result["CameraNoB"].ToString();
        //                                lblMeasurement.Text = result["MeasurementDistance"].ToString() + " metres";
        //                                lblTimeDifference.Text = result["TimeDifference"].ToString() + " seconds";
        //                            }
        //                            else
        //                            {
        //                                lblCameraNo.Text = result["NotCamSerialNo"].ToString();
        //                            }

        //                            lblOfficerNo.Text = result["NotOfficerNo"].ToString();
        //                            //lblFineAmount.Text = "R " + (result["NoticeOption"].ToString().Equals("1") ? result["ChgFineAmount"].ToString() : result["ChgRevFineAmount"].ToString());
        //                            lblIssuedBy.Text = result["AutNoticeIssuedByInfo"].ToString();

        //                            if (reportPage.ToUpper().IndexOf("_ST") < 0)
        //                            {
        //                                lblAut.Text = (result["AutName"].ToString().ToUpper().Trim().Equals("") ? "" : result["AutName"].ToString().ToUpper().Trim() + "\n")+
        //                                (result["AutPostAddr1"].ToString().Trim().Equals("") ? "" : result["AutPostAddr1"].ToString().Trim() + "\n")
        //                                    + (result["AutPostAddr2"].ToString().Trim().Equals("") ? "" : result["AutPostAddr2"].ToString().Trim() + "\n")
        //                                    + (result["AutPostAddr3"].ToString().Trim().Equals("") ? "" : result["AutPostAddr3"].ToString().Trim() + "\n")
        //                                    + result["AutPostCode"].ToString();
        //                            }

        //                            lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint1stNoticeDate"]);
        //                            //dls 071221 - added reprint date
        //                            lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint2ndNoticeDate"]);
        //                            lblReprintDateText.Text = "Reprint date:";
        //                            lblOffenceDate.Text = string.Format("{0:yyyy-MM-dd}", result["NotOffenceDate"]);
        //                            lblOffenceTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                            lblLocation.Text = result["NotLocDescr"].ToString();
        //                            lblSpeedLimit.Text = result["NotSpeedLimit"].ToString();
        //                            lblSpeed.Text = result["NotSpeed1"].ToString();
        //                            //lblOfficer.Text = result["NotOfficerNo"].ToString();  //result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();
        //                            lblRegNo.Text = result["NotRegNo"].ToString();
        //                            //mrs 20080212 added disclaimer
        //                            lblDisclaimer.Text = result["Disclaimer"].ToString();

        //                            //b = (byte[])result["ScanImage1"];
        //                        }
        //                    }

        //                    report = doc.Run(parameters);

        //                    ImportedPageArea importedPage;
        //                    byte[] bufferTemplate;
        //                    if (!sTemplate.Equals(""))
        //                    {
        //                        if (sTemplate.ToLower().IndexOf(".dplx") > 0)
        //                        {
        //                            DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
        //                            Query queryTemplate = (Query)template.GetQueryById("Query");
        //                            queryTemplate.ConnectionString = this.connectionString;
        //                            ParameterDictionary parametersTemplate = new ParameterDictionary();
        //                            Document reportTemplate = template.Run(parametersTemplate);
        //                            bufferTemplate = reportTemplate.Draw();
        //                            PdfDocument pdf = new PdfDocument(bufferTemplate);
        //                            PdfPage page = pdf.Pages[0];
        //                            importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
        //                        }
        //                        else
        //                        {
        //                            //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                            importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                        }

        //                        ceTe.DynamicPDF.Page rptPage = report.Pages[0];
        //                        rptPage.Elements.Insert(0, importedPage);
        //                    }
        //                    buffer = report.Draw();
        //                    merge.Append(new PdfDocument(buffer));

        //                    buffer = null;
        //                    bufferTemplate = null;
        //                }
        //                catch (Exception ex)
        //                {
        //                    String sError = ex.Message;
        //                }

        //                noOfNotices++;
        //            }
        //            result.Close();
        //            con.Dispose();

        //            if (!printFile.Substring(0, 1).Equals("*") && !printFile.Equals("-1"))
        //            {
        //                //dls 071228 - add summary report page and append to end of PDF document
        //                reportPage = "NoticeSummary.dplx";
        //                string path = Server.MapPath("reports/" + reportPage);

        //                if (!File.Exists(path))
        //                {
        //                    string error = "Report " + reportPage + " does not exist";
        //                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //                    Response.Redirect(errorURL);
        //                    return;
        //                }

        //                NoticeSummary summary = new NoticeSummary(this.connectionString, autIntNo, printFile, format, _showAll, _status, _option, path, reportPage, noOfNotices);

        //                byte[] sumReport = summary.CreateSummary();

        //                merge.Append(new PdfDocument(sumReport));

        //                sumReport = null;
        //            }

        //            byte[] buf = merge.Draw();

        //            phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);

        //            //if (reportPage.IndexOf("_RLV_JMPD") < 0 && reportPage.IndexOf("SecondNotice") < 0)
        //            //{
        //            //    phImage.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_Image);
        //            //}

        //            Response.ClearContent();
        //            Response.ClearHeaders();
        //            Response.ContentType = "application/pdf";
        //            Response.BinaryWrite(buf);
        //            Response.End();

        //            buf = null;
        //        }
        //        catch
        //        {
        //        }
        //        finally
        //        {
        //            GC.Collect();
        //        }
        //    }
        //}

        //public void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        //{
        //    if (barCodeImage != null)
        //    {
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //            ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
        //            img.Height = 25.0F;
        //            e.ContentArea.Add(img);
        //        }

        //        int generation = System.GC.GetGeneration(barCodeImage);
        //        barCodeImage = null;
        //        System.GC.Collect(generation);
        //    }
        //}

        ////void ph_Image(object sender, PlaceHolderLaidOutEventArgs e)
        ////{
        ////    if (b != null)
        ////    {
        ////        ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(b), 0, 0);
        ////        img.Height = 120.0F;
        ////        img.Width = 150.0F;
        ////        e.ContentArea.Add(img);
        ////    }
        ////}
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            autIntNo = Convert.ToInt32(Session["printAutIntNo"]);

            if (Request.QueryString["printfile"] != null)
                title = Request.QueryString["printfile"].ToString();
            
           PrintFileProcess process = new PrintFileProcess(this.connectionString, this.loginUser);
           process.BuildPrintFile(new PrintFileModule2ndNotice(), autIntNo, Request.QueryString["printfile"]);
           string printType = Request.QueryString["printType"] == null ? "" : Request.QueryString["printType"].ToString().Trim();
           if (printType == "ReprintNewOffender")
           {
               //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
               SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
               punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ReprintNewOffenderNotices, PunchAction.Change);  

           }
           else //if (printType == "PrintSecondNotice")
           {
               //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
               SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
               punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.PrintSecondNotice, PunchAction.Change);  
           }
        }

    }
}
