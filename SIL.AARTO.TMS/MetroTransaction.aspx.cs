﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Stalberg.TMS;
using SIL.AARTO.BLL.Utility.Cache;
using System.Collections.Generic;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

public partial class MetroTransaction : System.Web.UI.Page
{
    protected string _connStr = "";
    protected string _styleSheet;
    protected string _background;
    protected string _login;
    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
    protected int autIntNo = 0;
    protected string _thisPage = "MetroTransaction.aspx";
    protected string _keywords = "";
    protected string _title = "";
    protected string _description = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        _connStr = Application["constr"].ToString();

        //get user info from session variable
        if (Session["userDetails"] == null)
            Server.Transfer("Login.aspx?Login=invalid");

        if (Session["userIntNo"] == null)
            Server.Transfer("Login.aspx?Login=invalid");

        //get user details
        Stalberg.TMS.UserDB user = new UserDB(_connStr);
        Stalberg.TMS.UserDetails userDetails = new UserDetails();

        userDetails = (UserDetails)Session["userDetails"];

        _login = userDetails.UserLoginName;

        autIntNo = Convert.ToInt32(Session["autIntNo"]);
        Session["userLoginName"] = userDetails.UserLoginName;
        int userAccessLevel = userDetails.UserAccessLevel;

        //may need to check user access level here....
        //			if (userAccessLevel<7)
        //				Server.Transfer(Session["prevPage"].ToString());


        //set domain specific variables
        General gen = new General();

        _background = gen.SetBackground(Session["drBackground"]);
        _styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        _title = gen.SetTitle(Session["drTitle"]);


        if (!Page.IsPostBack)
        {
            pnlAddMetroTran.Visible = false;
            pnlEditMetroTran.Visible = false;
            btnOptDelete.Visible = false;
            btnOptAdd.Visible = true;
            btnOptHide.Visible = true;

            PopulateMetros();
            PopulateTT();
            BindGrid();
        }

    }
    protected void BindGrid()
    {
        int metroIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
        // Obtain and bind a list of all users
        MetroTransactionDB tranNoList = new MetroTransactionDB(_connStr);

        DataSet data = tranNoList.GetMetroTransactionListDS(metroIntNo);
        dgMetroTran.DataSource = data;
        dgMetroTran.DataKeyField = "MTIntNo";
        dgMetroTran.DataBind();

        if (dgMetroTran.Items.Count == 0)
        {
            dgMetroTran.Visible = false;
            lblError.Visible = true;

            //Modefied By Henry 2012-03-06
            //Desc: Mulitiple Language use resource file lblError.Text1 - lblError.Text9
            lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
        }
        else
        {
            dgMetroTran.Visible = true;
        }

        data.Dispose();

        pnlAddMetroTran.Visible = false;
        pnlEditMetroTran.Visible = false;
    }
    protected void PopulateMetros()
    {
        MetroDB metros = new MetroDB(_connStr);
        
        SqlDataReader reader = metros.GetMetroList("MtrName");

        Dictionary<int, string> lookups =
                MetroLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int mtroIntNo = (int)reader["MtrIntNo"];
                if (lookups.ContainsKey(mtroIntNo))
	            {
                    ddlSelectLA.Items.Add(new ListItem(lookups[mtroIntNo], mtroIntNo.ToString()));
	            }
            }
        //ddlSelectLA.DataSource = reader;
        //ddlSelectLA.DataValueField = "MtrIntNo";
        //ddlSelectLA.DataTextField = "MtrName";
        //ddlSelectLA.DataBind();
        //ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

        reader.Close();
    }
    protected void PopulateTT()
    {
        TransactionTypeDB tts = new TransactionTypeDB(_connStr);
        
        DataSet ds = tts.GetTransactionTypeListDS();
        Dictionary<int, string> lookups =
            TransactionTypeLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            int tTIntNo = (int)ds.Tables[0].Rows[i]["TTIntNo"];
            if (lookups.ContainsKey(tTIntNo))
            {
                ddlSelectTT.Items.Add(new ListItem(lookups[tTIntNo], tTIntNo.ToString()));
                ddlAddSelectTT.Items.Add(new ListItem(lookups[tTIntNo], tTIntNo.ToString()));
            }
        }
        //ddlSelectTT.DataSource = ds;
        //ddlSelectTT.DataValueField = "TTIntNo";
        //ddlSelectTT.DataTextField = "TTCode";
        //ddlSelectTT.DataBind();
        //ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

        //ddlAddSelectTT.DataSource = ds;
        //ddlAddSelectTT.DataValueField = "TTIntNo";
        //ddlAddSelectTT.DataTextField = "TTCode";
        //ddlAddSelectTT.DataBind();
    }
    protected void btnAddMetroTran_Click(object sender, EventArgs e)
    {
        // add the new transaction to the database table
        MetroTransactionDB MetroTransactionAdd = new MetroTransactionDB(_connStr);

        int addMTIntNo = MetroTransactionAdd.AddMetroTransaction(Convert.ToInt32(ddlSelectLA.SelectedValue), ddlAddSelectTT.SelectedValue, Convert.ToInt32(txtAddMTNumber.Text),
           Session["userLoginName"].ToString());

        if (addMTIntNo <= 0)
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
        }
        else
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this._connStr);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this._login, PunchStatisticsTranTypeList.MetroTransactionNumbersMaintenance, PunchAction.Add);  

        }

        lblError.Visible = true;

        BindGrid();

    }
    protected void btnUpdateMetroTran_Click(object sender, EventArgs e)
    {
        int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

        MetroTransactionDB metroTranUpdate = new MetroTransactionDB(_connStr);

        int updTranNoIntNo = metroTranUpdate.UpdateMetroTransaction(Convert.ToInt32(Session["editTNIntNo"]), ddlSelectLA.SelectedValue, ddlSelectTT.SelectedValue, txtMTNumber.Text, Session["userLoginName"].ToString());

        if (updTranNoIntNo.Equals("-1"))
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
        }
        else
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this._connStr);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this._login, PunchStatisticsTranTypeList.MetroTransactionNumbersMaintenance, PunchAction.Change);  
        }

        lblError.Visible = true;
        BindGrid();
    }
    protected void btnOptAdd_Click(object sender, EventArgs e)
    {
        // allow transactions to be added to the database table
        pnlAddMetroTran.Visible = true;
        pnlEditMetroTran.Visible = false;
        btnOptDelete.Visible = false;
        PopulateTT();
    }

    protected void btnOptDelete_Click(object sender, EventArgs e)
    {
        int tranIntNo = Convert.ToInt32(Session["editTNIntNo"]);

        MetroTransactionDB tran = new Stalberg.TMS.MetroTransactionDB(_connStr);

        string delTNIntNo = tran.DeleteMetroTransaction(tranIntNo);

        if (delTNIntNo.Equals("0"))
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
        }
        else if (delTNIntNo.Equals("-1"))
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
        }
        else
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this._connStr);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this._login, PunchStatisticsTranTypeList.MetroTransactionNumbersMaintenance, PunchAction.Delete);  
        }

        lblError.Visible = true;

        BindGrid();

    }
    protected void btnOptHide_Click(object sender, EventArgs e)
    {
        if (dgMetroTran.Visible.Equals(true))
        {
            //hide it
            dgMetroTran.Visible = false;
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
        }
        else
        {
            //show it
            dgMetroTran.Visible = true;
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
        }

    }
    protected void btnHideMenu_Click(object sender, EventArgs e)
    {
        if (pnlMainMenu.Visible.Equals(true))
        {
            pnlMainMenu.Visible = false;
            btnHideMenu.Text = (string)GetLocalResourceObject("btnHideMenu.Text1");
        }
        else
        {
            pnlMainMenu.Visible = true;
            btnHideMenu.Text = (string)GetLocalResourceObject("btnHideMenu.Text");
        }

    }
    protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
    {
        dgMetroTran.Visible = true;
        btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
        BindGrid();
    }
    protected void dgMetroTran_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dgMetroTran.SelectedIndex = e.Item.ItemIndex;

        if (dgMetroTran.SelectedIndex > -1)
        {
            pnlAddMetroTran.Visible = false;
            Session["editTNIntNo"] = Convert.ToInt32(dgMetroTran.DataKeys[dgMetroTran.SelectedIndex]);

            // Obtain and bind a list of all users
            MetroTransactionDB metroTran = new MetroTransactionDB(_connStr);
            int mtIntNo = Convert.ToInt32(Session["editTNIntNo"]);
            MetroTransactionDetails metroTranDetails = metroTran.GetMetroTransactionDetails(mtIntNo);
            if (mtIntNo > 0)
            {
                ddlSelectTT.SelectedValue = metroTranDetails.TTIntNo.ToString();
                txtMTNumber.Text = metroTranDetails.MTNumber.ToString(); 

                pnlEditMetroTran.Visible = true;
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                lblError.Visible = true;
                pnlEditMetroTran.Visible = false;
            }
        }
    }
    protected void dgMetroTran_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        dgMetroTran.CurrentPageIndex = e.NewPageIndex;
        BindGrid();

    }
}
