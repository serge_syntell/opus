using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using Stalberg.TMS.Data;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class DocumentType : Page
    {
        private enum Display
        {
            List,
            Edit,
            Delete
        }
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        // Protected
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        //protected string thisPage = "Document type Management";
        protected string description = String.Empty;
        protected string thisPageURL = "DocumentType.aspx";
        protected string title = string.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            //UserDB user = new UserDB(this.connectionString);
            UserDetails userDetails = new UserDetails();

            //int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            ////int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.bindData();
            }
        }

        private void bindData()
        {
            this.setPanelVisibility(Display.List);

            // Get the list of document types and bind it to the grid
            DocumentTypeDB db = new DocumentTypeDB(this.connectionString);
            List<Data.DocumentType> data = db.ListDocumentTypes();
            this.gridDocumentTypes.DataSource = data;
            this.gridDocumentTypes.DataKeyField = "ID";
            this.gridDocumentTypes.DataBind();

            if (data.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                this.lblError.Text = string.Empty;
            }
        }

        private void setPanelVisibility(Display display)
        {
            this.lblError.Text = string.Empty;

            this.panelEdit.Visible = false;
            this.panelDelete.Visible = false;
            this.pnlDetails.Visible = false;

            switch (display)
            {
                case Display.List:
                    this.pnlDetails.Visible = true;
                    break;
                case Display.Edit:
                    this.panelEdit.Visible = true;
                    break;
                case Display.Delete:
                    this.panelDelete.Visible = true;
                    break;
            }
        }

         

        protected void buttonAdd_Click(object sender, EventArgs e)
        {
            this.setPanelVisibility(Display.Edit);

            this.textCode.Text = string.Empty;
            this.textDescription.Text = string.Empty;
            this.hiddenID.Value = "0";

            //Heidi 2014-03-25 added for maintaining multiple languages(5141)
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupUpdate.DataBind(entityList);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            this.setPanelVisibility(Display.Delete);
        }

        protected void buttonSubmit_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.hiddenID.Value);
            Data.DocumentType doc = new Data.DocumentType();
            doc.ID = id;
            doc.Code = this.textCode.Text.Trim();
            doc.Description = this.textDescription.Text.Trim();

            if (string.IsNullOrEmpty(doc.Code))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            if (string.IsNullOrEmpty(doc.Description))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            if (id == 0)
            {
                //Heidi 2014-03-25 fixed a problem that add document code repeatedly will throw an exception(5141)
                DocumentTypeService dtService = new DocumentTypeService();
                SIL.AARTO.DAL.Entities.DocumentType dtEntity = dtService.GetByDocCode(doc.Code);
                if (dtEntity != null)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    return;
                }
            }

            this.lblError.Text = string.Empty;

            DocumentTypeDB db = new DocumentTypeDB(this.connectionString);

            using (ConnectionScope.CreateTransaction())
            {
                if (id == 0)
                {
                    db.InsertDocumentType(doc, this.login);

                    //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                    List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                    rUMethod.UpdateIntoLookup(doc.ID, lgEntityList, "DocumentTypeLookup", "DocTypeID", "DocDescr",this.login);

                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.DocumentTypeMaintenance, PunchAction.Add);

                }
                else
                {
                    db.UpdateDocumentType(doc, this.login);

                    //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                    List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                    rUMethod.UpdateIntoLookup(id, lgEntityList, "DocumentTypeLookup", "DocTypeID", "DocDescr",this.login);

                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.DocumentTypeMaintenance, PunchAction.Change);
                }
                ConnectionScope.Complete();
            }

            this.bindData();
        }

        protected void gridDocumentTypes_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            this.hiddenID.Value = e.Item.Cells[0].Text;

            if (e.CommandName.Equals("Select"))
            {
                this.setPanelVisibility(Display.Edit);

                this.textCode.Text = e.Item.Cells[1].Text;
                this.textDescription.Text = e.Item.Cells[2].Text;

                //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(this.hiddenID.Value, "DocumentTypeLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);
            }
            else if (e.CommandName.Equals("Delete"))
            {
                this.setPanelVisibility(Display.Delete);
                this.labelConfirmDelete.Text = string.Format((string)GetLocalResourceObject("labelConfirmDelete.Text"), e.Item.Cells[2].Text);
               
            }
        }

        protected void buttonDelete_Click1(object sender, EventArgs e)
        {
            int id = int.Parse(this.hiddenID.Value);
            SIL.AARTO.DAL.Services.DocumentTypeLookupService dtLookUpService = new DocumentTypeLookupService();
            SIL.AARTO.DAL.Services.DocumentTypeService dtService = new DocumentTypeService();
           // DocumentTypeDB db = new DocumentTypeDB(this.connectionString);

            using (ConnectionScope.CreateTransaction())
            {
                //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                dtLookUpService.Delete(dtLookUpService.GetByDocTypeId(id));
                dtService.Delete(dtService.GetByDocTypeId(id));;

                //db.DeleteDocumentType(id);

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.DocumentTypeMaintenance, PunchAction.Delete);

                ConnectionScope.Complete();
            }

            this.bindData();
        }

        protected void buttonList_Click(object sender, EventArgs e)
        {
            this.bindData();
        }
    }
}
