﻿<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SummonsServer_CaptureServed"
    CodeBehind="SummonsServer_CaptureServed.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script type="text/javascript">
        // xBroswer event subscription
        function addListener(a, b, c, d) { if (a.addEventListener) { a.addEventListener(b, c, d); return true; } else if (a.attachEvent) { var e = a.attachEvent("on" + b, c); return e; } else { alert("Handler could not be attached"); } }
        function bind(a, b, c, d) { return window.addListener(a, b, function () { d.apply(c, arguments) }); }
        //

        // Generic dispatcher for keystrokes
        function handleKeystroke(evt) {
            evt = getEvent(evt);
            var asc = getKeyCode(evt);
            var chr = getCharacter(asc);

            for (var i in this) {
                if (asc == i) {
                    this[i](evt);
                    break;
                }
            }
        }
        //

        // xBrowser event utilities
        function cancelEvent(evt) {
            evt.cancelBubble = true;
            evt.returnValue = false;
            if (evt.preventDefault) evt.preventDefault();
            if (evt.stopPropagation) evt.stopPropagation();
            return false;
        }
        function getEvent(evt) {
            if (!evt) evt = window.event;
            return evt;
        }
        function getTarget(evt) {
            var target = evt.srcElement ? evt.srcElement : evt.target;
            return target;
        }
        function getKeyCode(evt) {
            var asc = !evt.keyCode ? (!evt.which ? evt.charCode : evt.which) : evt.keyCode;
            return asc;
        }
        function getCharacter(asc) {
            var chr = String.fromCharCode(asc).toLowerCase();
            return chr;
        }
        //

        function handleEnterKey(evt) {
            var target = getTarget(evt);
            var targetTabIndex = target.tabIndex;
            var nextTabIndex = targetTabIndex + 1;

            var nextElement = getElementByTabIndex(nextTabIndex);
            if (nextElement) {
                //handle the click of the search and the fix error
                if (targetTabIndex != 2 && targetTabIndex != 8 && targetTabIndex != 3) {
                    //handle the enter of the update
                    if (targetTabIndex != 6) {
                        nextElement.focus();
                        return cancelEvent(evt);
                    }
                }
            }

            return true;
        }

        function getElementByTabIndex(tabIndex) {
            var form = document.forms[0];
            for (var i = 0; i < form.elements.length; i++) {
                var el = form.elements[i];
                if (el.tabIndex && el.tabIndex == tabIndex) {
                    return el;
                }
            }

            return null;
        }

        // Setup our Key Commands
        var keyMap = new Array();
        var ENTER = 13 // ASCII code
        keyMap[ENTER] = handleEnterKey;

        // Add the keypress listner to the document object for global capture
        bind(document, 'keypress', keyMap, handleKeystroke);
    </script>
    <script type="text/javascript">
        //handle setting focus of controls	
        function test_Clicked() {
            Form1.txtDateServed.focus();
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table height="85%" cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td valign="top" align="center">
                <img style="height: 1px" src="images/1x1.gif" width="167" />
                <asp:Panel ID="pnlMainMenu" runat="server">
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead"  Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" colspan="1" style="width: 100%">
                <asp:Panel ID="pnlTitle" runat="Server" Width="447px">
                    <p style="text-align: center;">
                        <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead"  Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                    </p>
                    <p>
                        <table border="0" class="Normal">
                            <tr>
                                <td valign="top">
                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="221px"  Text="<%$Resources:lblSummonsNum.Text %>"></asp:Label>
                                </td>
                                <td style="height: 26px" class="Normal" valign="top">
                                    <asp:TextBox ID="txtSummonsNo" runat="server" TabIndex="1"></asp:TextBox><br />
                                </td>
                                <td style="height: 26px; text-align: right" valign="top">
                                    <asp:Button ID="btnSearch" runat="server" Text="<%$Resources:btnSearch.Text %>" OnClick="btnSearch_Click"
                                        CssClass="NormalButton" TabIndex="2" />
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p>
                        <asp:Label ID="lblWarning" runat="server" CssClass="NormalRed" Text="<%$Resources:lblWarning.Text %>"></asp:Label>
                        <table border="0" class="Normal">
                            <tr>
                                <td>
                                    <asp:Label ID="lblProcessed" runat="server" CssClass="NormalBold" Width="221px" Visible="False" Text="<%$Resources:lblProcessed.Text %>"></asp:Label>
                                </td>
                                <td style="height: 26px">
                                    <asp:Button ID="btnContinue" runat="server" Text="<%$Resources:btnContinue.Text %>" OnClick="btnContinue_Click"
                                        CssClass="NormalButton" TabIndex="3" Visible="false" />
                                    <asp:Button ID="btnCancelSearch" runat="server" Text="<%$Resources:btnCancelSearch.Text %>" OnClick="btnCancelSearch_Click"
                                        CssClass="NormalButton" TabIndex="4" Visible="false" />
                                </td>
                            </tr>
                        </table>
                    </p>
                    <p>
                        <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>
                        <table border="0" class="Normal" width="100%" align="left">
                            <!-- <tr>
                                        <td class="NormalBold" style="width: 108px">
                                            Summons Server:
                                        </td>
                                        <td style="width: 173px" class="Normal">
                                            <asp:Label ID="lblSummonsServer" runat="server" CssClass="Normal"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="NormalBold" style="width: 108px">
                                            Court:</td>
                                        <td style="width: 173px" class="Normal">
                                            <asp:Label ID="lblCourtName" runat="server" Width="235px" CssClass="Normal"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="NormalBold" style="width: 108px">
                                            Court Number:</td>
                                        <td style="width: 173px" class="Normal">
                                            <asp:Label ID="lblCourtNo" runat="server" Width="233px" CssClass="Normal"></asp:Label></td>
                                    </tr> -->
                            <tr>
                                <td class="NormalBold" style="width: 108px">
                                    <asp:Label ID="lblServedBy" runat="server" CssClass="NormalBold" Text="<%$Resources:lblServedBy.Text %>"></asp:Label>
                                </td>
                                <td style="width: 173px" class="Normal">
                                    <asp:DropDownList ID="cmbServedBy" runat="server" CssClass="Normal" Width="236px"
                                        TabIndex="5">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="NormalBold" style="width: 108px">
                                    <asp:Label ID="lblDateServed" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateServed.Text %>"></asp:Label>
                                </td>
                                <td valign="top" class="Normal">
                                    <asp:TextBox ID="txtDateServed" runat="server" Width="232px" AutoPostBack="True"
                                        CausesValidation="True" TabIndex="6"></asp:TextBox>
                                    <asp:Label ID="lblSumSSToDate" runat="server" Visible="False" Width="82px"></asp:Label>
                                    <asp:Label ID="lblSumServeByDate" runat="server" Visible="False" Width="82px"></asp:Label><br />
                                    <asp:Label ID="lblComment" runat="server" CssClass="NormalBold" Text="<%$Resources:lblComment.Text %>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="NormalBold" style="width: 108px">
                                </td>
                                <td valign="top" class="Normal">
                                    <asp:CustomValidator ID="cvDateServed" runat="server" ControlToValidate="txtDateServed"
                                         ErrorMessage="<%$Resources:cvDateServed.ErrorMsg %>"
                                        OnServerValidate="cvDateServed_ServerValidate" CssClass="NormalRed"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="NormalBold" style="width: 108px">
                                    <asp:Label ID="lblDelivery" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDelivery.Text %>"></asp:Label>
                                </td>
                                <td style="width: 173px" class="Normal">
                                    <asp:TextBox ID="txtMethodServed" runat="server" Width="33px" OnTextChanged="txtMethodServed_TextChanged"
                                        AutoPostBack="True" TabIndex="7" MaxLength="2"></asp:TextBox>&nbsp;
                                    <asp:Label ID="lblMethodServed" runat="server" Width="196px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right;" class="NormalBold">
                                    <asp:Button ID="btnCapture" runat="Server" Text="<%$Resources:btnCapture.Text %>" CssClass="NormalButton"
                                        OnClick="btnCapture_Click" TabIndex="8" />
                                </td>
                            </tr>
                        </table>
                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlCapture" runat="server" Visible="False" Width="447px">
                </asp:Panel>
                <asp:Panel ID="pnlDecision" runat="server" Height="80px" Width="429px" HorizontalAlign="Center" Visible="false">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblDecision" runat="server" Height="40px" Text="<%$Resources:lblDecision.Text %>"
                                    Width="385px" CssClass="NormalRed"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnYes" runat="server" Text="<%$Resources:btnYes.Text %>" CssClass="NormalButton" 
                                    Width="76px" onclick="btnYes_Click"/>
                                &nbsp; &nbsp; &nbsp;
                                <asp:Button ID="btnNo" runat="server" Text="<%$Resources:btnNo.Text %>" Width="76px" 
                                    CssClass="NormalButton" Height="24px" onclick="btnNo_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlDetails" runat="server" Width="100%" HorizontalAlign="Left">
                    <br />
                    <asp:Label ID="lblResult" runat="server" CssClass="NormalBold"></asp:Label><br />
                    <asp:DetailsView ID="dvSummons" runat="server" AllowPaging="True" AutoGenerateRows="False"
                        BorderStyle="None" BorderWidth="0px" CellPadding="3" CssClass="Normal" Height="50px"
                        OnItemCommand="dvSummons_ItemCommand" OnPageIndexChanging="dvSummons_PageIndexChanging"
                        Width="452px">
                        <PagerStyle CssClass="NormalBold" />
                        <RowStyle CssClass="Normal" />
                        <AlternatingRowStyle CssClass="CartListItemAlt" />
                        <Fields>
                            <asp:BoundField DataField="CrtName" HeaderText="<%$Resources:dvSummons.HeaderText %>" />
                            <asp:BoundField DataField="CrtNo" HeaderText="<%$Resources:dvSummons.HeaderText1 %>" />
                            <asp:BoundField DataField="SummonsNo" HeaderText="<%$Resources:dvSummons.HeaderText2 %>">
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Surname" HeaderText="<%$Resources:dvSummons.HeaderText3 %>">
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ForeNames" HeaderText="<%$Resources:dvSummons.HeaderText4 %>">
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="IDNumber" HeaderText="<%$Resources:dvSummons.HeaderText5 %>">
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="StreetAddress" HeaderText="<%$Resources:dvSummons.HeaderText6 %>">
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="StreetCode" HeaderText="<%$Resources:dvSummons.HeaderText7 %>" />
                            <asp:BoundField DataField="PostalAddress" HeaderText="<%$Resources:dvSummons.HeaderText8 %>">
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PostCode" HeaderText="<%$Resources:dvSummons.HeaderText9 %>" />
                            <asp:BoundField DataField="SumSSCompany" HeaderText="<%$Resources:dvSummons.HeaderText10 %>">
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SSFullName" HeaderText="<%$Resources:dvSummons.HeaderText11 %>">
                                <ItemStyle Wrap="False" />
                            </asp:BoundField>
                        </Fields>
                        <FieldHeaderStyle CssClass="NormalBold" />
                        <HeaderStyle CssClass="NormalBold" />
                    </asp:DetailsView>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
