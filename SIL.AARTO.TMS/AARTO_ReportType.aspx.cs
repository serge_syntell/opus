﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using Stalberg.TMS.Data;

namespace Stalberg.TMS
{
    public partial class AARTO_ReportType : System.Web.UI.Page
    {
        private enum Display
        {
            List,
            Edit,
            Delete
        }
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        // Protected
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;

        //protected string thisPage = "AARTO Report Type Management";
        protected string thisPageURL = "AARTO_ReportType.aspx";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            //UserDB user = new UserDB(this.connectionString);
            UserDetails userDetails = new UserDetails();

            //int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.bindData();
            }
        }

        private void bindData()
        {
            this.setPanelVisibility(Display.List);

            // Get the list of AARTO_Report types and bind it to the grid
            AARTO_ReportTypeDB db = new AARTO_ReportTypeDB(this.connectionString);
            List<Data.AARTO_ReportType> data = db.ListAARTO_ReportTypes();
            this.gridReportTypes.DataSource = data;
            this.gridReportTypes.DataKeyField = "ID";
            this.gridReportTypes.DataBind();

            if (data.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                this.lblError.Text = string.Empty;
            }
        }

        private void setPanelVisibility(Display display)
        {
            this.lblError.Text = string.Empty;

            this.panelEdit.Visible = false;
            this.panelDelete.Visible = false;
            this.pnlDetails.Visible = false;

            switch (display)
            {
                case Display.List:
                    this.pnlDetails.Visible = true;
                    break;
                case Display.Edit:
                    this.panelEdit.Visible = true;
                    break;
                case Display.Delete:
                    this.panelDelete.Visible = true;
                    break;
            }
        }

        //protected void btnHideMenu_Click(object sender, EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void buttonAdd_Click(object sender, EventArgs e)
        {
            this.setPanelVisibility(Display.Edit);

            this.textCode.Text = string.Empty;
            this.textDescription.Text = string.Empty;
            this.hiddenID.Value = "0";
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            this.setPanelVisibility(Display.Delete);
        }

        protected void buttonSubmit_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.hiddenID.Value);
            Data.AARTO_ReportType doc = new Data.AARTO_ReportType();
            doc.ID = id;
            doc.Code = this.textCode.Text.Trim();
            doc.Description = this.textDescription.Text.Trim();

            if (string.IsNullOrEmpty(doc.Code))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            if (string.IsNullOrEmpty(doc.Description))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            this.lblError.Text = string.Empty;

            AARTO_ReportTypeDB db = new AARTO_ReportTypeDB(this.connectionString);
            if (id == 0)
            {
                if (db.InsertAARTO_ReportType(doc, this.login) == -1)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                    return;
                }
                else
                {
                    //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ReportTypes, PunchAction.Add);  

                }
            }

            else
            {
                db.UpdateAARTO_ReportType(doc, this.login);
                //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ReportTypes, PunchAction.Change);  
            }

            this.bindData();
        }

        protected void gridReportTypes_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            this.hiddenID.Value = e.Item.Cells[0].Text;

            if (e.CommandName.Equals("Select"))
            {
                this.setPanelVisibility(Display.Edit);

                this.textCode.Text = e.Item.Cells[1].Text;
                this.textDescription.Text = e.Item.Cells[2].Text;
            }
            else if (e.CommandName.Equals("Delete"))
            {
                this.setPanelVisibility(Display.Delete);
                this.labelConfirmDelete.Text = string.Format((string)GetLocalResourceObject("lblError.Text3"), e.Item.Cells[1].Text);
            }
        }

        protected void buttonDelete_Click1(object sender, EventArgs e)
        {
            int id = int.Parse(this.hiddenID.Value);
            AARTO_ReportTypeDB db = new AARTO_ReportTypeDB(this.connectionString);
            try
            {
                db.DeleteAARTO_ReportType(id);
                //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ReportTypes, PunchAction.Delete);  
            }
            catch (Exception ex)
            {
                this.lblError.Text = ex.Message;
            }
            this.bindData();
        }

        protected void buttonList_Click(object sender, EventArgs e)
        {
            this.bindData();
        }
    }
}
