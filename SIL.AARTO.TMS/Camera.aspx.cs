using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;

//dls 061121 - remove MtrIntNo and CameraID (becomes separate CamUnitID table) from Camera table 
namespace Stalberg.TMS 
{

	public partial  class Camera : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;

        //protected string thisPage = "Camera Maintenance";
        protected string thisPageURL = "Camera.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        CameraService cameraService = new CameraService();

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];

			loginUser = userDetails.UserLoginName;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

			//int 
			//int	autIntNo = Convert.ToInt32(Session["autIntNo"]);
            //int mtrIntNo = Convert.ToInt32(Session["mtrIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                //2012-3-2 Linda Modified into a multi- language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddCamera.Visible = false;
				pnlUpdateCamera.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

                BindGrid();

				//PopulateMetro(ugIntNo, mtrIntNo);

                //if (mtrIntNo>0)
                //{

                //    BindGrid(mtrIntNo);
                //}
                //else
                //{
                //    lblError.Text = "Camera for unknown metro not allowed. Please select metro/authority first";
                //    lblError.Visible = true;
                //}
			}		
		}

        //protected void PopulateMetro(int ugIntNo, int mtrIntNo)
        //{
        //    UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

        //    SqlDataReader reader = authorities.GetUserGroup_MetroListByUserGroup(ugIntNo);
        //    ddlSelectLA.DataSource = reader;
        //    ddlSelectLA.DataValueField = "MtrIntNo";
        //    ddlSelectLA.DataTextField = "MtrName";
        //    ddlSelectLA.DataBind();
        //    ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(mtrIntNo.ToString()));
        //    reader.Close();
        //}

		//protected void BindGrid(int mtrIntNo)
        protected void BindGrid()
		{
			// Obtain and bind a list of all users

			Stalberg.TMS.CameraDB camList = new Stalberg.TMS.CameraDB(connectionString);

			//DataSet data = camList.GetCameraListDS(mtrIntNo);
            int totalCount = 0;
            DataSet data = camList.GetCameraListDS(dgCameraPager.PageSize, dgCameraPager.CurrentPageIndex - 1, out totalCount);
			dgCamera.DataSource = data;
			dgCamera.DataKeyField = "CamIntNo";
			dgCamera.DataBind();
            dgCameraPager.RecordCount = totalCount;
			
			if (dgCamera.Items.Count == 0)
			{
				dgCamera.Visible = false;
				lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text"); 
			}
			else
			{
				dgCamera.Visible = true;
			}

			data.Dispose();
			pnlAddCamera.Visible = false;
			pnlUpdateCamera.Visible = false;

            //Seawen 2013-09-13 pager visible equal to grid
            dgCameraPager.Visible = dgCamera.Visible;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddCamera.Visible = true;
			pnlUpdateCamera.Visible = false;

			btnOptDelete.Visible = false;
		}

		protected void btnAddCamera_Click(object sender, System.EventArgs e)
		{	
            //if (ddlSelectLA.SelectedIndex < 0)
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "Please select a valid metro";
            //    return;
            //}

			//int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            //int mtrIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            //jerry 2012-1-13 add validation
            if (txtAddCamSerialNo.Text.Trim().Length == 0)
            {
                lblError.Text = "Serial no can not be blank.";
                lblError.Visible = true;
                return;
            }

			// add the new transaction to the database table
			Stalberg.TMS.CameraDB toAdd = new CameraDB(connectionString);

            //2015-03-11 Heidi added checked UK constraint(bontq1913).
            IList<SIL.AARTO.DAL.Entities.Camera> cameraList = cameraService.GetByCamSerialNo(txtAddCamSerialNo.Text.Trim());
            if (cameraList != null && cameraList.Count>0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7") +
                string.Format((string)GetLocalResourceObject("lblError.Text8"), txtAddCamSerialNo.Text.Trim());
                lblError.Visible = true;
                return;
            }
			
			//int addCamIntNo = toAdd.AddCamera(mtrIntNo, txtAddCamID.Text, txtAddCamSerialNo.Text, loginUser);
            int addCamIntNo = toAdd.AddCamera(txtAddCamSerialNo.Text, int.Parse(txtAddMaxSupportingImages.Text), loginUser);

			if (addCamIntNo <= 0)
			{
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1"); 
			}
			else
			{
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;

            //Seawen 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
			//BindGrid(mtrIntNo);
            //dgCameraPager.RecordCount = 0;
            //dgCameraPager.CurrentPageIndex = 1;
            BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
            //if (ddlSelectLA.SelectedIndex < 0)
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "Please select a valid metro";
            //    return;
            //}

            //jerry 2012-1-13 add validation
            if (txtCamSerialNo.Text.Trim().Length == 0)
            {
                lblError.Text = "Serial no can not be blank.";
                lblError.Visible = true;
                return;
            }

			//int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            //int mtrIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int camIntNo = Convert.ToInt32(Session["editCamIntNo"]);

			// add the new transaction to the database table
			Stalberg.TMS.CameraDB camUpdate = new CameraDB(connectionString);

            //2015-03-11 Heidi added checked UK constraint(bontq1913).
            IList<SIL.AARTO.DAL.Entities.Camera> cameraList = cameraService.GetByCamSerialNo(txtCamSerialNo.Text.Trim());
            if (cameraList != null && cameraList.Count > 0 && camIntNo > 0 && cameraList[0].CamIntNo != camIntNo)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7") +
                string.Format((string)GetLocalResourceObject("lblError.Text8"), txtCamSerialNo.Text.Trim());
                lblError.Visible = true;
                return;
            }
			
			//int updCamIntNo = camUpdate.UpdateCamera(mtrIntNo, txtCamID.Text, txtCamSerialNo.Text, loginUser, camIntNo);
            int updCamIntNo = camUpdate.UpdateCamera(txtCamSerialNo.Text, int.Parse(txtMaxSupportingImages.Text), loginUser, camIntNo);

			if (updCamIntNo <= 0)
			{
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3"); 
			}
			else
			{
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
             
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraMaintenance, PunchAction.Change); 
			}

			lblError.Visible = true;

			//BindGrid(mtrIntNo);
            //dgCameraPager.RecordCount = 0;
            //dgCameraPager.CurrentPageIndex = 1;
            BindGrid();
        }

		protected void dgCamera_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgCamera.SelectedIndex = e.Item.ItemIndex;

			if (dgCamera.SelectedIndex > -1) 
			{			
				int editCamIntNo = Convert.ToInt32(dgCamera.DataKeys[dgCamera.SelectedIndex]);

				Session["editCamIntNo"] = editCamIntNo;
				Session["prevPage"] = thisPageURL;

				ShowCameraDetails(editCamIntNo);
			}
		}

		protected void ShowCameraDetails(int editCamIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.CameraDB camera = new Stalberg.TMS.CameraDB(connectionString);
			
			Stalberg.TMS.CameraDetails camDetails = camera.GetCameraDetails(editCamIntNo);

			//txtCamID.Text = camDetails.CamID;
			txtCamSerialNo.Text = camDetails.CamSerialNo;
            txtMaxSupportingImages.Text = camDetails.MaxSupportingImages;
			
			pnlUpdateCamera.Visible = true;
			pnlAddCamera.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

	
		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int camIntNo = Convert.ToInt32(Session["editCamIntNo"]);
            //int mtrIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

			CameraDB camera = new Stalberg.TMS.CameraDB(connectionString);

			string delCamIntNo = camera.DeleteCamera(camIntNo);

            if (string.IsNullOrEmpty(delCamIntNo) || delCamIntNo.Equals("0"))
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
            }
            //			else if (delCamIntNo.Equals("-1"))
            //			{
            //				lblError.Text = "Unable to delete this camera - it has been linked to locations";
            //			}
            else
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                //Seawen 2013-09-13 last page last row
                if (dgCamera.Items.Count == 1)
                {
                    dgCameraPager.CurrentPageIndex--;
                }
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraMaintenance, PunchAction.Delete); 
            }

			lblError.Visible = true;

			//int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            //int mtrIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

			//BindGrid(mtrIntNo);
            //dgCameraPager.RecordCount = 0;
            //dgCameraPager.CurrentPageIndex = 1;

            BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgCamera.Visible.Equals(true))
			{
				//hide it
				dgCamera.Visible = false;
                //2012-3-2 Linda Modified into a multi- language
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				//show it
				dgCamera.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}

            //Seawen 2013-09-13 pager visible equal to grid
            dgCameraPager.Visible = dgCamera.Visible;
		}

        protected void dgCameraPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        //protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        //{
        //    dgCamera.Visible = true;
        //    btnOptHide.Text = "Hide list";

        //    //BindGrid(Convert.ToInt32(ddlSelectLA.SelectedValue));
        //}

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

	}
}
