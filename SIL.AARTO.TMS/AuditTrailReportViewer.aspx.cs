﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SIL.AARTO.BLL.Report;


namespace Stalberg.TMS
{
    public partial class AuditTrailReportViewer : System.Web.UI.Page
    {
        private string connectionString = String.Empty;

        protected string title = String.Empty;

        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            General gen = new General();
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                DateTime before = DateTime.Parse("2000-01-01");
                DateTime after = DateTime.Parse("2050-01-01");

                string TicketNo = "";
                string RegNo = "";
                string DBAudit = "";
                int autIntNo = 0;
                
                if (Request["AutIntNo"] != null)
                {
                    int.TryParse(Request["AutIntNo"].ToString(), out autIntNo);
                }
                if (Request["TicketNo"] != null)
                {
                    TicketNo = Request["TicketNo"].ToString();
                }
                if (Request["RegNo"] != null)
                {
                    RegNo = Request["RegNo"].ToString();
                }
                if (Request["DBAudit"] != null)
                {
                    DBAudit = Request["DBAudit"].ToString();
                }
                if (Request["Before"] != null)
                {
                    DateTime.TryParse(Request["After"].ToString(), out before);
                }
                if (Request["After"] != null)
                {
                    DateTime.TryParse(Request["Before"].ToString(), out after);
                }

                string filename = "AuditTrailStatistics " + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();

                SIL.AARTO.BLL.Report.AuditTrailReport report = new SIL.AARTO.BLL.Report.AuditTrailReport(connectionString);

                MemoryStream stream = report.ExportToExcel(DBAudit, TicketNo, RegNo, autIntNo, before, after);

                Response.BinaryWrite(stream.GetBuffer());
                Response.End();
            }
        }
    }
}