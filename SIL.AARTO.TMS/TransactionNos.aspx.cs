using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class TransactionNos : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "TransactionNos.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "Transaction number maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
           
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
			autIntNo = Convert.ToInt32(Session["autIntNo"]);
			Session["userLoginName"] = userDetails.UserLoginName;
			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddTranNo.Visible = false;
				pnlEditTranNo.Visible = false;
				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptCopy.Visible = false;
				btnOptHide.Visible = true;

				PopulateAuthorites( autIntNo);

				if (autIntNo>0)
				{
					BindGrid(autIntNo);
				}
				else
				{
                    //Modefied By Linda 2012-2-24
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (String)GetLocalResourceObject("lblError.Text");
                    
					lblError.Visible = true;
				}
			}		
		}

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
		protected void PopulateAuthorites( int autIntNo)
		{
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");


            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data ;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
			ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

			//reader.Close();
		}

		protected void BindGrid(int autIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.TranNoDB tranNoList = new Stalberg.TMS.TranNoDB(connectionString);

			DataSet data = tranNoList.GetTranNoListDS(autIntNo);
			dgTranNo.DataSource = data;
			dgTranNo.DataKeyField = "TNIntNo";
			dgTranNo.DataBind();

			if (dgTranNo.Items.Count == 0)
			{
				dgTranNo.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text1");
			}
			else
			{
				dgTranNo.Visible = true;
			}

			data.Dispose();

			pnlAddTranNo.Visible = false;
			pnlEditTranNo.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddTranNo.Visible = true;
			pnlEditTranNo.Visible = false;
			btnOptDelete.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
		}

		protected void btnAddTranNo_Click(object sender, System.EventArgs e)
		{		
			// add the new transaction to the database table
			Stalberg.TMS.TranNoDB tranNoAdd = new TranNoDB(connectionString);
            //2013-12-02 Heidi moved for add all Punch Statistics Transaction(5084)
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
			
			int addTNIntNo = tranNoAdd.AddTranNo(Convert.ToInt32(ddlSelectLA.SelectedValue), txtAddTNType.Text, Convert.ToInt32(txtAddTNumber.Text), 
				txtAddTNDescr.Text, Session["userLoginName"].ToString());

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addTNIntNo, lgEntityList, "TranNoLookup", "TNIntNo", "TNDescr", this.loginUser);

			if (addTNIntNo <= 0)
			{
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text2");
			}
			else
			{
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text3");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.TransactionNumbers, PunchAction.Add);  

			}

			lblError.Visible = true;

			

			BindGrid(autIntNo);
		}

		protected void btnUpdateTranNo_Click(object sender, System.EventArgs e)
		{

			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

			Stalberg.TMS.TranNoDB tranNoUpdate = new TranNoDB(connectionString);
			
			int updTranNoIntNo = tranNoUpdate.UpdateTranNo(autIntNo, txtTNType.Text, Convert.ToInt32(txtTNumber.Text), txtTNDescr.Text, 
				Session["userLoginName"].ToString(), Convert.ToInt32(Session["editTNIntNo"]));

			if (updTranNoIntNo.Equals("-1"))
			{
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text4");
			}
			else
			{

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(updTranNoIntNo, lgEntityList, "TranNoLookup", "TNIntNo", "TNDescr", this.loginUser);
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text5");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.TransactionNumbers, PunchAction.Change); 
			}

			lblError.Visible = true;

			BindGrid(autIntNo);
		}

		protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgTranNo.Visible = true;
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			BindGrid(Convert.ToInt32(ddlSelectLA.SelectedValue));
		}

		protected void PopulateTranNoTextBoxes(int tnIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.TranNoDB tranNo = new Stalberg.TMS.TranNoDB(connectionString);
	
			Stalberg.TMS.TranNoDetails tranNoDetails = tranNo.GetTranNoDetails(tnIntNo);
			if (tnIntNo >0)
			{
				txtTNType.Text = tranNoDetails.TNType;
				txtTNumber.Text = tranNoDetails.TNumber.ToString();
				txtTNDescr.Text = tranNoDetails.TNDescr;

				pnlEditTranNo.Visible = true;
			}
			else
			{
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text6");
				lblError.Visible = true;
				pnlEditTranNo.Visible = false;
			}
		}

		protected void dgTranNo_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			dgTranNo.SelectedIndex = e.Item.ItemIndex;

			if (dgTranNo.SelectedIndex > -1) 
			{			
				pnlAddTranNo.Visible = false;
				Session["editTNIntNo"] = Convert.ToInt32(dgTranNo.DataKeys[dgTranNo.SelectedIndex]);
				PopulateTranNoTextBoxes(Convert.ToInt32(Session["editTNIntNo"]));

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID((Session["editTNIntNo"]).ToString(), "TranNoLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

			}
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgTranNo.Visible.Equals(true))
			{
				//hide it
				dgTranNo.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				//show it
				dgTranNo.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		 

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int tranIntNo = Convert.ToInt32(Session["editTNIntNo"]);

            TranNoDB tran = new Stalberg.TMS.TranNoDB(connectionString);

            string delTNIntNo = tran.DeleteTranNo(tranIntNo);
            //2013-12-02 Heidi moved for add all Punch Statistics Transaction(5084)
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            if (delTNIntNo.Equals("0"))
            {
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text7");
            }
            else if (delTNIntNo.Equals("-1"))
            {
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text8");
            }
            else
            {

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.DeleteLookup(lgEntityList, "TranNoLookup", "TNIntNo");
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text9");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.TransactionNumbers, PunchAction.Delete);
            }
            lblError.Visible = true;

            

            BindGrid(autIntNo);
        }
        protected void dgTranNo_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgTranNo.CurrentPageIndex = e.NewPageIndex;

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo);
        }

        protected void btnOptCopy_Click(object sender, EventArgs e)
        {

        }
}
}
