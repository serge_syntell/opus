using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class OffenceType : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string thisPageURL = "OffenceType.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        protected int userAccessLevel = 0;
        //protected string thisPage = "Offence type maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
			autIntNo = Convert.ToInt32(Session["autIntNo"]);

			this.userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....

			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddOffenceType.Visible = false;
				pnlUpdateOffenceType.Visible = false;

				btnOptDelete.Visible = false;
				//btnOptAdd.Visible = true;
                //dls 081003 - only allow system admin access to the add and delete functions
                if (this.userAccessLevel < 10)
                    btnOptAdd.Visible = false;
                else
                    btnOptAdd.Visible = true;

				btnOptHide.Visible = true;

				BindGrid();
			}		
		}

		protected void BindGrid()
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.OffenceTypeDB octList = new Stalberg.TMS.OffenceTypeDB(connectionString);

			DataSet data = octList.GetOffenceTypeListDS();
			dgOffenceType.DataSource = data;
			dgOffenceType.DataKeyField = "OcTIntNo";
			dgOffenceType.DataBind();

			if (dgOffenceType.Items.Count == 0)
			{
				dgOffenceType.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
				dgOffenceType.Visible = true;
			}

			data.Dispose();

			pnlAddOffenceType.Visible = false;
			pnlUpdateOffenceType.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddOffenceType.Visible = true;
			pnlUpdateOffenceType.Visible = false;

			btnOptDelete.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
		}

		protected void btnAddOffenceType_Click(object sender, System.EventArgs e)
		{
            //dls 081003 - only allow system admin access to the add and delete functions
            if (this.userAccessLevel < 10)
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

			// add the new transaction to the database table
			Stalberg.TMS.OffenceTypeDB octAdd = new OffenceTypeDB(connectionString);

            //dls 090701 - added English & Afrikaans short descriptions for display on court roll
			int addOcTIntNo = octAdd.AddOffenceType(txtAddOcTCode.Text,
				txtAddOcType.Text, txtAddOcTDescr.Text, txtAddOcTDescr1.Text, txtAddOcTDescriptionEng.Text, txtAddOcTDescriptionAfr.Text, loginUser, txtAddOcTEngShortDescr.Text, txtAddOcTAfrShortDescr.Text);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addOcTIntNo, lgEntityList, "OffenceTypeLookup", "OcTIntNo", "OcTDescr", this.loginUser);

			if (addOcTIntNo <= 0)
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
			}
			else
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceTypesMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			int ocTIntNo = Convert.ToInt32(Session["editOcTIntNo"]);

			// add the new transaction to the database table
			Stalberg.TMS.OffenceTypeDB octUpdate = new OffenceTypeDB(connectionString);
			
            //dls 081003 - offence type code is now read-only in the update 
            //dls 090701 - added English & Afrikaans short descriptions for display on court roll
			int updOcTIntNo = octUpdate.UpdateOffenceType(ocTIntNo, txtOcTCode.Text, txtOcType.Text, txtOcTDescr.Text,
				txtOcTDescr1.Text, txtOcTDescriptionEng.Text, txtOcTDescriptionAfr.Text, loginUser, txtOctEngShortDescr.Text, txtOctAfrShortDescr.Text);

			if (updOcTIntNo <= 0)
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
			}
			else
			{
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(updOcTIntNo, lgEntityList, "OffenceTypeLookup", "OcTIntNo", "OcTDescr", this.loginUser);
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceTypesMaintenance, PunchAction.Change); 
			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void dgOffenceType_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgOffenceType.SelectedIndex = e.Item.ItemIndex;

			if (dgOffenceType.SelectedIndex > -1) 
			{			
				int editOcTIntNo = Convert.ToInt32(dgOffenceType.DataKeys[dgOffenceType.SelectedIndex]);

				Session["editOcTIntNo"] = editOcTIntNo;
				Session["prevPage"] = thisPageURL;

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editOcTIntNo.ToString(), "OffenceTypeLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

				ShowOffenceTypeDetails(editOcTIntNo);
			}
		}

		protected void ShowOffenceTypeDetails(int editOcTIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.OffenceTypeDB offenceType = new Stalberg.TMS.OffenceTypeDB(connectionString);
			
			Stalberg.TMS.OffenceTypeDetails octDetails = offenceType.GetOffenceTypeDetails(editOcTIntNo);

			txtOcTCode.Text = octDetails.OcTCode;
			txtOcType.Text = octDetails.OcType;
			txtOcTDescr.Text = octDetails.OcTDescr;
			txtOcTDescr1.Text = octDetails.OcTDescr1;
            txtOcTDescriptionEng.Text = octDetails.OcTDescriptionEng;
            txtOcTDescriptionAfr.Text = octDetails.OcTDescriptionAfr;

            //dls 090701 - added English & Afrikaans short descriptions for display on court roll
            txtOctEngShortDescr.Text = octDetails.OcTEngShortDescr;
            txtOctAfrShortDescr.Text = octDetails.OcTAfrShortDescr;

			pnlUpdateOffenceType.Visible = true;
			pnlAddOffenceType.Visible  = false;
			
            //btnOptDelete.Visible = true;
            //dls 081003 - only allow system admin access to the add and delete functions
            if (this.userAccessLevel < 10)
                btnOptDelete.Visible = false;
            else
                btnOptDelete.Visible = true;
		}
	
		protected void dgOffenceType_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgOffenceType.CurrentPageIndex = e.NewPageIndex;

			BindGrid();
				
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
            //dls 081003 - only allow system admin access to the add and delete functions
            if (this.userAccessLevel < 10)
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                return;
            }

			int ocTIntNo = Convert.ToInt32(Session["editOcTIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "OffenceTypeLookup", "OcTIntNo");

			OffenceTypeDB offenceType = new Stalberg.TMS.OffenceTypeDB(connectionString);

			string delOcTIntNo = offenceType.DeleteOffenceType(ocTIntNo);

			if (delOcTIntNo.Equals("0"))
			{
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
			}
            else if (delOcTIntNo.Equals("-1"))
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            }
            else
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffenceTypesMaintenance, PunchAction.Delete); 
            }
			lblError.Visible = true;

			BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgOffenceType.Visible.Equals(true))
			{
				//hide it
				dgOffenceType.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				//show it
				dgOffenceType.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		 
	}
}
