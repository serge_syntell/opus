using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS.Reports
{
    /// <summary>
    /// Summary description for FirstNotice1.
    /// </summary>
    public partial class InvalidNoticeViewer : System.Web.UI.Page
    {
        private System.Data.SqlClient.SqlDataAdapter sqlDANotice;
        private System.Data.SqlClient.SqlConnection cn;
        private System.Data.SqlClient.SqlParameter paramAutIntNo;
        private System.Data.SqlClient.SqlParameter paramCSCode;
        private ReportDocument _reportDoc = new ReportDocument();
        private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        private dsPrintNotice dsInvalidNoticeTicket = null;
        //private string thisPage = "Invalid Notice Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "InvalidNoticeViewer.aspx";
        protected string loginUser = string.Empty;
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            int autIntNo = 0;
            string csCode = string.Empty;

            string reportPage = "InvalidNoticeTickets.rpt";
            string reportPath = Server.MapPath("reports/" + reportPage);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            //if (File.Exists(reportPath))
            //{
            _reportDoc.Load(reportPath);

            if (Request.QueryString["Authority"] != null
                && Request.QueryString["Code"] != null)
            {
                autIntNo = Convert.ToInt32(Request.QueryString["Authority"]);
                csCode = Request.QueryString["Code"].ToString();

                //set filter for batch to print
                paramAutIntNo.Value = autIntNo;
                paramCSCode.Value = csCode;

                //get data and populate dataset
                sqlDANotice.SelectCommand.Connection.ConnectionString = Application["constr"].ToString();
                sqlDANotice.Fill(dsInvalidNoticeTicket);
                sqlDANotice.Dispose();

                //set up new report based on .rpt report class
                _reportDoc.SetDataSource(dsInvalidNoticeTicket);
                dsInvalidNoticeTicket.Dispose();

                SetMargins(2, 1, 0, 1);

                //export the pdf file
                MemoryStream ms = new MemoryStream();
                ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
                _reportDoc.Dispose();

                //stuff the PDF file into rendering stream
                //first clear everything dynamically created and just send PDF file
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(ms.ToArray());
                Response.End();
            }
            //}

            //else
            //{
            //    // SD: 20.11.2008 : Adding  Error.aspx page

            //    //Response.Write("<script language='javascript'> { self.close() }</script>");
            //    PageToOpen page = new PageToOpen(this, "Error.aspx");
            //    page.Parameters.Add(new QSParams("error", "Report page not available"));
            //    page.Parameters.Add(new QSParams("errorPage", "Invalid NoticeViewer"));
            //    Helper_Web.BuildPopup(page); 


            //}
        }

        protected void SetMargins(int left, int top, int right, int bottom)
        {
            PageMargins margins;

            // Get the PageMargins structure and set the 
            // margins for the report.
            margins = _reportDoc.PrintOptions.PageMargins;
            margins.leftMargin = left;
            margins.topMargin = top;
            margins.rightMargin = right;
            margins.bottomMargin = bottom;

            // Apply the page margins.
            _reportDoc.PrintOptions.ApplyPageMargins(margins);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        protected void InitializeComponent()
        {
            this.sqlDANotice = new System.Data.SqlClient.SqlDataAdapter();
            this.cn = new System.Data.SqlClient.SqlConnection();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            ((System.ComponentModel.ISupportInitialize)(this.dsInvalidNoticeTicket)).BeginInit();
            // 
            // sqlDANotice
            // 
            this.sqlDANotice.SelectCommand = this.sqlSelectCommand1;
            this.sqlDANotice.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																								  new System.Data.Common.DataTableMapping("Table", "NoticeInvalidTickets", new System.Data.Common.DataColumnMapping[] {
																																																						  new System.Data.Common.DataColumnMapping("AutName", "AutName"),
																																																						  new System.Data.Common.DataColumnMapping("AutCode", "AutCode"),
																																																						  new System.Data.Common.DataColumnMapping("BatNo", "BatNo"),
																																																						  new System.Data.Common.DataColumnMapping("ChgOffenceCode", "ChgOffenceCode"),
																																																						  new System.Data.Common.DataColumnMapping("ChgOffenceDescr", "ChgOffenceDescr"),
																																																						  new System.Data.Common.DataColumnMapping("NotSpeedLimit", "NotSpeedLimit"),
																																																						  new System.Data.Common.DataColumnMapping("NotVehicleTypeCode", "NotVehicleTypeCode"),
																																																						  new System.Data.Common.DataColumnMapping("NotVehicleType", "NotVehicleType"),
																																																						  new System.Data.Common.DataColumnMapping("NotFilmNo", "NotFilmNo"),
																																																						  new System.Data.Common.DataColumnMapping("NotFrameNo", "NotFrameNo"),
																																																						  new System.Data.Common.DataColumnMapping("NotOffenceDate", "NotOffenceDate"),
																																																						  new System.Data.Common.DataColumnMapping("NotRdTypeDescr", "NotRdTypeDescr"),
																																																						  new System.Data.Common.DataColumnMapping("NotRdTypeCode", "NotRdTypeCode"),
																																																						  new System.Data.Common.DataColumnMapping("CSCode", "CSCode"),
																																																						  new System.Data.Common.DataColumnMapping("CSDescr", "CSDescr")})});
            // 
            // cn
            // 
            this.cn.ConnectionString = "workstation id=XE4500;packet size=4096;integrated security=SSPI;data source=SERVE" +
                "R2;persist security info=False;initial catalog=TMS";
            // 
            // dsInvalidNoticeTicket
            // 
            this.dsInvalidNoticeTicket.DataSetName = "dsInvalidNoticeTicket";
            this.dsInvalidNoticeTicket.Locale = new System.Globalization.CultureInfo("en-US");
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "[NoticeInvalidTickets]";
            this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand1.Connection = this.cn;
            this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((System.Byte)(0)), ((System.Byte)(0)), "", System.Data.DataRowVersion.Current, null));
            paramAutIntNo = this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AutIntNo", System.Data.SqlDbType.Int, 4));
            paramCSCode = this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CSCode", System.Data.SqlDbType.VarChar, 3));
            ((System.ComponentModel.ISupportInitialize)(this.dsInvalidNoticeTicket)).EndInit();

        }
        #endregion
    }
}
