// 20050714 mrs apparently ~ is outside the acceptable range of ascii - changed to = 
// 20060103 mrs removed batch
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS 
{

    public partial class SendNaTIS : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected string thisPageURL = "SendNaTIS.aspx";
		protected int frameStatus = 100;        //pick up all frames with status < 100 and > 0
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        protected int autIntNo = 0;
        private int _noOfNatisRecords = 600;   //default no of records to place in Natis file
        //protected string thisPage = "Create file to send to NATIS";

        protected bool _natisLast = false;
        protected string _showDifferencesOnly = "Y";
        protected int _statusLoaded = 0;
        protected int _statusSent = 0;

        // Constants
        //natis first
        private const int STATUS_LOADED_FIRST = 5;
        private const int STATUS_START_SEND_NATIS_FRAME_FIRST = 11;
        private const int STATUS_SENT_TO_NATIS_FIRST = 100;
        private const int STATUS_NOT_YET_RECEIVED_FROM_NATIS_FIRST = 150;

        //natis last
        private const int STATUS_LOADED_LAST = 710;                     //post adjudication
        private const int STATUS_START_SEND_NATIS_FRAME_LAST = 711;     //started send to natis
        private const int STATUS_SENT_TO_NATIS_LAST = 730;
        private const int STATUS_NOT_YET_RECEIVED_FROM_NATIS_LAST = 750;

        private const int NO_OF_NATIS_RECORDS = 600;            //default no of records to place in Natis file
       
        protected override void OnLoad(System.EventArgs e)
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
			
			

			loginUser = userDetails.UserLoginName.ToString();
			
			int userAccessLevel = userDetails.UserAccessLevel;

		    //set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			pnlGeneral.Visible = true;
			if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                PopulateAuthorities();
			}
		}

       
        protected void btnSendToNatis_Click(object sender, EventArgs e)
        {
            lblError.Visible = true;
            if (ddlFilmNo.SelectedIndex < 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            bool failed = GetFrameNatisData( Convert.ToInt32(ddlFilmNo.SelectedValue), Convert.ToInt32(ddlAuthority.SelectedValue));

            if (!failed)
            {
                lblError.Text += (string)GetLocalResourceObject("lblError.Text1") + ddlFilmNo.SelectedItem.Text;
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SendFilmToNatis, PunchAction.Change);  

            }
            else
            {
                lblError.Text += (string)GetLocalResourceObject("lblError.Text2") + ddlFilmNo.SelectedItem.Text;
            }

            PopulateFilms();
           
        }
     
        /// <summary>
        /// Updates the Frame status.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <returns></returns>
        private bool UpdateStatus(int autIntNo)
        {
            // update all the tables with the appropriate codes
            // charge status from 010 to 100 after send to Natis is complete
            // dls 060516 - now sending for Natis infor from the frame table

            bool failed = false;
            string errMessage = string.Empty;

            // without batch we cannot be sure that the rows returned for update are the same as those sent to Natis
            Stalberg.TMS.FrameDB statusUpdate = new Stalberg.TMS.FrameDB(connectionString);

            int statusStart = 0;
            int statusSent = 0;

            if (_natisLast)
            {
                statusStart = STATUS_START_SEND_NATIS_FRAME_LAST;
                statusSent = STATUS_SENT_TO_NATIS_LAST;
            }
            else
            {
                statusStart = STATUS_START_SEND_NATIS_FRAME_FIRST;
                statusSent = STATUS_SENT_TO_NATIS_FIRST;
            }

            int xxIntNo = statusUpdate.FrameStatusChange(autIntNo, statusStart, statusSent, "SendNaTIS", ref errMessage);
            if (xxIntNo == autIntNo)
            {
                // finished successfully
            }
            else
            {
                // some form of disaster
                failed = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3") + errMessage;
            }

            return failed;
        }

        /// <summary>
        /// Gets the frame data from Natis.
        /// </summary>
        /// <param name="autIntNo">The aut int no.</param>
        /// <param name="writer">The writer.</param>
        /// <param name="connStr">The conn STR.</param>
        /// <param name="procStr">The proc STR.</param>
        /// <param name="localFtpParam">The local FTP param.</param>
        /// <param name="autFolder">The aut folder.</param>
        /// <param name="showDifferencesOnly">The show differences only.</param>
        /// <returns></returns>
        public bool GetFrameNatisData(int nFilmNo, int autIntNo)
        {
            bool failed = false;
            string folder = Server.MapPath("Natis") + @"\INP";
            //string exportPath = autFolder + folder;
            string exportPath = folder;
            int seqNoLength = 3;
            int autNoLength = 4;
            string sNumber = "0";
            string tnType = "kk";
            int frameStatus = 100;        //pick up all frames with status < 100 and > 0
            // this currently includes all the 40's = Natis recycle bin

            char cTemp = Convert.ToChar("0");
            string autCode = "SP";
            string autNo = "1234";
            string autNaTISCode = "1234";
            string autEmail = string.Empty;
            string autSendEmail = string.Empty;
            string autName = string.Empty;
            ArrayList fileList = new ArrayList();

            Stalberg.TMS.AuthorityDB authCode = new Stalberg.TMS.AuthorityDB(connectionString);
            Stalberg.TMS.AuthorityDetails acDetails = authCode.GetAuthorityDetails(autIntNo);

            // DLS 2007-05-21 added the lookup for some values in the NatisParams table - setup via OPUS
            NatisParamsDB npdb = new NatisParamsDB(connectionString);
            NatisParamsDB.NatisParamDetails np = npdb.GetNatisParamsDetails(autIntNo);

            if (np.nNpIntNo == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4") + autName + ")";
               failed = true;
               return failed;
            }

            autNaTISCode = np.sTerminalID;
            autSendEmail = np.sSendNatisEmail;

            //check folder exists
            bool folderNotExists =  CheckFolder(exportPath);

            if (folderNotExists)
            {
                //Console.WriteLine("GetFrameNatisData: Folder does not exist or is inaccessible: " + exportPath);
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), exportPath);
                failed = true;
                return failed;
            }
           
            try
            {
                Stalberg.TMS.FrameDB sendNaTIS = new Stalberg.TMS.FrameDB(connectionString);

                frameStatus = _natisLast ? STATUS_SENT_TO_NATIS_LAST : STATUS_SENT_TO_NATIS_FIRST;

                string sForce = btnSendToNatis.Text.Equals("Force re-send") ? "Y" : "N";

                SqlDataReader send = sendNaTIS.GetNaTISSendDataByFilm(nFilmNo, autIntNo, loginUser, frameStatus, _showDifferencesOnly, _noOfNatisRecords, sForce);

                // potential size problem - can only communicate with Natis machines via stiffy disk!!!
                // the stored proc will only release a specific no. of rows at a time
                if (send.HasRows)
                {                   
                    // make file name - NaTIS requires a numeric file name . INP
                    // first three digits are the authority code
                    // wrong! each traffic department has a unique Natis number
                    
                    int xxIntNo = acDetails.AutIntNo;
                    if (autIntNo == xxIntNo)
                    {
                        autCode = acDetails.AutCode;

                        //must be re-initialised for each file
                        exportPath = folder;

                        autEmail = acDetails.AutEmail;
                        //autSendEmail = acDetails.AutSendNatisEmail;
                        autNo = acDetails.AutNo.Substring(0, autNoLength);
                        //autNaTISCode = acDetails.AutNaTISCode;
                        autName = acDetails.AutName.ToUpper();

                        try
                        {
                            int code = Convert.ToInt32(autNaTISCode);
                        }
                        catch
                        {
                            //Console.WriteLine("GetFrameNatisData: natis code for authority " + autCode + " (" + autNaTISCode + ") is not valid");
                            lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Tex6"), autCode, autNaTISCode);
                            send.Close();
                            failed = true;
                            return failed;
                        }

                        if (exportPath.IndexOf(@"\" + autCode.Trim()) < 0)
                            exportPath += @"\" + autCode.Trim();

                        folderNotExists = CheckFolder(exportPath);

                        if (folderNotExists)
                        {
                            //Console.WriteLine("GetFrameNatisData: Folder does not exist or is inaccessible: " + exportPath);
                            lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text7"), exportPath);
                            failed = true;
                            send.Close();
                            return failed;
                        }
                    }
                    else
                    {
                        //Console.WriteLine("GetFrameNatisData: unable to find Authority (" + autIntNo.ToString());
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text8") + autIntNo.ToString() + ")";
                        failed = true;
                        send.Close();
                        return failed;
                    }
                    // get the next number from tranNo and increment (in stored proc)
                    tnType = "NIN";	// means NaTIS INP!

                    //dls 060522 - the stored proc requires a Max No when auto-generating no's
                    int maxNo = 999;
                    Stalberg.TMS.TranNoDB number = new Stalberg.TMS.TranNoDB(connectionString);
                    int tNumber = number.GetNextTranNoByType(tnType, autIntNo, "SendNaTIS", maxNo);
                    if (tNumber > 0)
                        sNumber = tNumber.ToString().PadLeft(seqNoLength, cTemp);
                    else
                    {
                        //Console.WriteLine("GetFrameNatisData: no transaction number for type 'NIN' for Authority (" + autIntNo.ToString());
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text9") + autIntNo.ToString() + ")";
                        failed = true;
                        send.Close();
                        return failed;
                    }

                    sNumber = autNaTISCode.Trim() + sNumber.Trim();	// concat auth NaTIS code and sequential number together
                    string fileName = sNumber + @".INP";

                    string exportFullPath = exportPath + @"\" + fileName;
                    string sLenRegNo = string.Empty;
                    string sLenOffDate = string.Empty;
                    string sLenIDNo = string.Empty;

                    //check that folder exists
                    if (!Directory.Exists(exportPath))
                        Directory.CreateDirectory(exportPath);

                    if (File.Exists(exportFullPath))
                    {
                        //Console.WriteLine("GetFrameNatisData: Export path (" + exportFullPath + " already exists");
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text10"), exportFullPath);
                        failed = true;
                        send.Close();
                        return failed;
                    }

                    // create the file
                    StreamWriter sw = File.CreateText(exportFullPath);

                    // loop through send to write out lines to file
                    while (send.Read())
                    {
                        //dls 060522 - the data for NATIS is now coming from the Frame table, not Notice anymore
                        sLenRegNo = send["RegNo"].ToString().Length.ToString().PadLeft(2, cTemp);
                        sLenOffDate = send["OffenceDate"].ToString().Length.ToString().PadLeft(2, cTemp);
                        sLenIDNo = send["IdNo"].ToString().Length.ToString().PadLeft(2, cTemp);
                        //sw.WriteLine ("5040" + sNumber + "003" + sLenRegNo + send["NotRegNo"] + "060" + sLenOffDate + send["OffenceDate"].ToString() + "065" + sLenIDNo + send["IDNo"].ToString() + "99901");
                        sw.WriteLine("5040" + sNumber + "003" + sLenRegNo + send["RegNo"] + "060" + sLenOffDate + send["OffenceDate"].ToString() + "065" + sLenIDNo + send["IDNo"].ToString() + "99901");
                    }
                    // write the last lines
                    //5040 1148413 003 08 CA629722 060 08 20050521 065 16 9486992760174838 99901
                    //5040 1148413 065 23 99999999999999999999999 99901
                    //5040 1148413 065 29 99999999999999999999999999999 99901
                    //5990 1148413 900 07 1148413 99901
                    string sLenNumber = sNumber.ToString().Length.ToString().PadLeft(2, cTemp);
                    sw.WriteLine("5040" + sNumber + "065" + "23" + "99999999999999999999999" + "99901");
                    sw.WriteLine("5040" + sNumber + "065" + "29" + "99999999999999999999999999999" + "99901");
                    sw.WriteLine("5990" + sNumber + "900" + sLenNumber + sNumber + "99901");
                    // finished with the file
                    sw.Close();
                    // explicitly close the result from the data reader
                    send.Close();

                    //dls 060522 - this is now done at the Frame level
                    //dls 060804 - this proc also adds row to NatisFiles table with filename and create date anda no. of frames sent
                    // update the notice table with the inp used to send the rows to natis
                    FrameDB updateINP = new FrameDB(connectionString);

                    int statusSend = 0;

                    if (_natisLast)
                        statusSend = STATUS_START_SEND_NATIS_FRAME_LAST;
                    else
                        statusSend = STATUS_START_SEND_NATIS_FRAME_FIRST;

                    int updIntNo = updateINP.UpdateFrameINP(statusSend, fileName, "SendNaTIS", autIntNo);
                    if (updIntNo <= 0)
                    {
                        //Console.WriteLine("GetFrameNatisData: Unable to update Frame table with INP filename (" + fileName + ")");
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text11") + fileName + ")";
                        failed = true;
                        return failed;
                    }

                    bool updateFailed = this.UpdateStatus(autIntNo);

                    if (updateFailed)
                    {
                        failed = true;
                        return failed;
                    }

                    //add file to arraylist so that all files can be sent in one email
                    fileList.Add(exportFullPath);
                }
                else
                {
                    //Console.WriteLine("GetFrameNatisData No Data to Send ");
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                    failed = true;
                    return failed;
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("GetFrameNatisData (framesForNatis): Error " + ex.Message);
                lblError.Text = (string)GetLocalResourceObject("lblError.Text13") + ex.Message;
                failed = true;
                return failed;
            }
            lblError.Visible = true;
      
            try
            {
                if (fileList.Count > 0)
                {
                    //save the file to the relavant local folder

                    // took a decision to use email to send the text file to the user. Avoids potential complications with access rights on local machines
                    // send the file as an attachment to the la email address
                    System.DateTime dateCreated = DateTime.Now;

                    string message =string.Format( (string)GetLocalResourceObject("lblError.Text14"),dateCreated);

                    for (int i = 0; i < fileList.Count; i++)
                    {
                        message += Path.GetFileName(fileList[i].ToString()) + ";";
                    }

                    message += (string)GetLocalResourceObject("lblError.Text15");

                    MailAddress from = new MailAddress(autEmail);
                    MailAddress to = new MailAddress(autSendEmail);
                    
                    MailMessage mail = new MailMessage(from, to);
                    mail.Subject = autName + " NaTIS Send file";
                    mail.Body = message;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;

                    for (int i = 0; i < fileList.Count; i++)
                    {
                        Attachment myAttachment = new Attachment(fileList[i].ToString());
                        mail.Attachments.Add(myAttachment);
                    }
                    try
                    {
                        SmtpClient mailClient = new SmtpClient();
                        mailClient.Host = Application["smtp"].ToString();
                        mailClient.UseDefaultCredentials = true;
                        mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                        //Send delivers the message to the mail server
                        mailClient.Send(mail);
                    }
                    catch (Exception smtpEx)
                    {
                        failed = true;
                        //Console.WriteLine("GetFrameNatisData: Failed to send email " + smtpEx.Message);
                        lblError.Text =string.Format((string)GetLocalResourceObject("lblError.Text16") ,smtpEx.Message);
                        return failed;
                    }
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("GetFrameNatisData (email) : Error " + e.Message);
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text17"), e.Message);
                return failed;
            }
            lblError.Visible = true;
            return failed;
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString );
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            //this.ddlAuthority.Items.Insert(0, new ListItem("[ Select an Authority ]", string.Empty));
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));
            //reader.Close();

            SetAuthRules();

            PopulateFilms();
            
        }

        protected void PopulateFilms()
        {
            int startStatus = 0;
            int endStatus = 0;

            if (_natisLast)
            {
                startStatus = STATUS_LOADED_LAST;
                if (btnSendToNatis.Text.Equals((string)GetLocalResourceObject("btnOptForce.Text")))
                    endStatus = STATUS_NOT_YET_RECEIVED_FROM_NATIS_LAST;
                else
                    endStatus = STATUS_SENT_TO_NATIS_LAST;
            }
            else
            {
                startStatus = STATUS_LOADED_FIRST;
                if (btnSendToNatis.Text.Equals((string)GetLocalResourceObject("btnOptForce.Text")))
                    endStatus = STATUS_NOT_YET_RECEIVED_FROM_NATIS_FIRST;
                else
                    endStatus = STATUS_SENT_TO_NATIS_FIRST;
                
            }

            //populate list of films for selected authority
            FilmDB fdb = new FilmDB(connectionString);
            SqlDataReader rd = fdb.GetFilmNoByAuthorityForNatis(Convert.ToInt32(ddlAuthority.SelectedValue), startStatus, endStatus);
            this.ddlFilmNo.DataSource = rd;
            this.ddlFilmNo.DataValueField = "FilmIntNo";
            this.ddlFilmNo.DataTextField = "FilmNo";
            this.ddlFilmNo.DataBind();
        }

        protected void SetAuthRules()
        {
            //get list of all authorities
            //AuthorityRulesDB authRules = new AuthorityRulesDB(connectionString);

            int arNumeric = 0;
            //int arIntNo = authRules.GetAuthorityRulesByCode(autIntNo, "0550", "Rule to indicate whether to just show NaTIS discrepancy frames or all frames for verification.", ref arNumeric, ref _showDifferencesOnly, "Y = Yes, only show NaTIS discrepant frames; N = No, shows all frames of the film.", "system");

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "0550";
            rule.LastUser = this.loginUser;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
            arNumeric = value.Key;
            _showDifferencesOnly = value.Value;

            //get the rule for working out whether Natis must be completed 1st/last
            // Check if this authority processes NaTIS data last
            //AuthorityRulesDetails details = authRules.GetAuthorityRulesDetailsByCode(autIntNo, "0400",
            //        "Rule that indicates whether to process NaTIS data last, i.e. after Adjudication.", 0,
            //        "Y", "Y = Default, process NaTIS data after Adjudication, N = Process NaTIS data before verification and before Adjudication",
            //        "SendNaTIS");

            AuthorityRulesDetails rule2 = new AuthorityRulesDetails();
            rule2.AutIntNo = this.autIntNo;
            rule2.ARCode = "0400";
            rule2.LastUser = this.loginUser;

            DefaultAuthRules authRule2 = new DefaultAuthRules(rule2, connectionString);
            KeyValuePair<int, string> value2 = authRule2.SetDefaultAuthRule();

            _natisLast = value2.Value.Equals("Y") ? true : false; //details.ARString.Equals("Y") ? true : false;
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            SetAuthRules();

            PopulateFilms();       
        }

        protected void btnOptForce_Click(object sender, System.EventArgs e)
        {
            if (btnSendToNatis.Text.Equals((string)GetLocalResourceObject("btnSendToNatis.Text")))
            {
                btnSendToNatis.Text = (string)GetLocalResourceObject("btnOptForce.Text");
                btnOptForce.Text = (string)GetLocalResourceObject("btnSendToNatis.Text");
            }
            else
            {
                btnSendToNatis.Text = (string)GetLocalResourceObject("btnSendToNatis.Text");
                btnOptForce.Text = (string)GetLocalResourceObject("btnOptForce.Text");
            }

            PopulateFilms();
        }

        /// <summary>
        /// Checks the folder.
        /// </summary>
        /// <param name="destinationPath">The destination path.</param>
        /// <returns></returns>
        public bool CheckFolder(string destinationPath)
        {
            bool failed = false;
            try
            {
                // must first check whether the destination directory is valid
                if (Directory.Exists(destinationPath))
                {
                    // valid path to directory
                }
                else
                {
                    // make a directory
                    DirectoryInfo di = new DirectoryInfo(destinationPath);
                    if (di.Exists == false)
                        di.Create();
                }
                return failed;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                failed = true;
                return failed;
            }
        }

     
    }
}
