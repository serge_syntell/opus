using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Stalberg.TMS
{
    /// <summary>
    /// The default, catch-all page of the application
    /// </summary>
	public partial class _Default : System.Web.UI.Page
	{
        // Fields
		protected string styleSheet;
		protected string backgroundImage;
		//protected System.Web.UI.WebControls.ImageButton ImageButton1;
		protected string thisPageURL = "Default.aspx";
        //protected string thisPage = "Default";
        protected string title = string.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnLoad(System.EventArgs e)
		{
			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//may need to check user access level here....

			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);
			
			if (!Page.IsPostBack)
			{
                Session["system"] = "TMS";
				try
				{
					imgMain.ImageUrl = gen.SetMainImage(Session["drMainImage"]);
				}
				catch
				{
					imgMain.Visible = false;
				}

			}
		}

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            imgMain.Visible = !imgMain.Visible;
        }

        protected void btnSystemOverview_Click(object sender, EventArgs e)
        {
            Response.Redirect("SystemOverviewReport.aspx");
        }

        //dls 090327 - These buttons will be hidden until the graphs and charts are re-written - only wen the users ask for it!
        //protected void btnCameraMgt_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("Chart_CameraMgt.aspx");
        //}

        //protected void btnPaymentStats_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("Chart_PaymentTotals.aspx");
        //}
}

}
