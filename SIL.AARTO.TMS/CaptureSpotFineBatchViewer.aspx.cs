using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
//using BarcodeNETWorkShop;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.PageElements;
using Stalberg.TMS.Data;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class CaptureSpotFineBatchViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private int userIntNo = 0;
        private string printName;
        private DataSet ds = null;
        private DocumentLayout layout = null;
        //private BarcodeNETImage barcode;
        //System.Drawing.Image barCodeImage = null;
        private DateTime userCompleteDate;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Notice and Receipt Batch Print Viewer";
        protected string thisPageURL = "CaptureSpotFineBatchViewer.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Call the base class
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Make sure there is a batch ID in the query string and parse it into the field variable
            if (Request.QueryString["Batch"] == null || Request.QueryString["UserCompleteDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.printName = this.Request.QueryString["Batch"].ToString();
            this.userCompleteDate = Convert.ToDateTime(this.Request.QueryString["UserCompleteDate"].ToString());

            // Check if the batch is valid
            List<SpotFineReceipt> receipts;
            if (!this.CheckBatchIsValid(out receipts))
            {
                string error = (string)GetLocalResourceObject("error1");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            if (receipts.Count == 0)
            {
                string error = (string)GetLocalResourceObject("error2");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            try
            {
                // Create common variables
                byte[] buffer;
                //this.barcode = new BarcodeNETImage();
                MergeDocument document = new MergeDocument();

                // Get the template name from the DB
                AuthReportNameDB db = new AuthReportNameDB(this.connectionString);

                string spdTemplateName = string.Empty;
                string rlvTemplateName = string.Empty;

                int autIntNo = 0;

                string path = string.Empty;
                string templatePath = string.Empty;

                string spdReport = string.Empty;
                string rlvReport = string.Empty;

                string receiptReport = "Receipt_CPI.dplx";
                string receiptPath = Server.MapPath("Reports\\" + receiptReport);

                if (!File.Exists(receiptPath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error3"), receiptReport);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }

                // Loop the data
                foreach (SpotFineReceipt receipt in receipts)
                {
                    if (autIntNo == 0)
                    {
                        //should only need to find these templates once, as all notices for a bactch will be in the same authority
                        spdTemplateName = Server.MapPath("Templates\\" + db.GetAuthReportNameTemplate(receipt.AutIntNo, "FirstNotice"));
                        rlvTemplateName = Server.MapPath("Templates\\" + db.GetAuthReportNameTemplate(receipt.AutIntNo, "FirstNotice RLV"));

                        autIntNo = receipt.AutIntNo;

                        rlvReport = this.GetReportName(receipt, "FirstNotice RLV", "FirstNotice_RLV_JMPD.dplx");
                        spdReport = this.GetReportName(receipt, "FirstNotice", "FirstNotice.dplx");
                    }

                    // Create the Notice
                    if (receipt.IsRedLightViolation)
                    {
                        //****************************************************
                        path = Server.MapPath("reports/" + rlvReport);
                        if (!File.Exists(path))
                        {
                            string error = string.Format((string)GetLocalResourceObject("error3"), rlvReport);
                            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                            Response.Redirect(errorURL);
                            return;
                        }
                        else if (!rlvTemplateName.Equals(""))
                        {
                            //dls 081117 - we can only check that the template path exists if there is actually a template!
                            templatePath = Server.MapPath("Templates/" + rlvTemplateName);

                            if (!File.Exists(templatePath))
                            {
                                string error = string.Format((string)GetLocalResourceObject("error4"), rlvTemplateName);
                                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                                Response.Redirect(errorURL);
                                return;
                            }
                        }
                        //****************************************************

                        buffer = this.CreateNotice_RLV(receipt, path);
                        // Overlay the notice with the background
                        buffer = this.MergeNoticeWithBackground(buffer, receipt, rlvTemplateName);
                    }
                    else
                    {
                        //****************************************************
                        path = Server.MapPath("reports/" + spdReport);
                        if (!File.Exists(path))
                        {
                            string error = string.Format((string)GetLocalResourceObject("error3"), spdReport);
                            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                            Response.Redirect(errorURL);
                            return;
                        }
                        else if (!rlvTemplateName.Equals(""))
                        {
                            //dls 081117 - we can only check that the template path exists if there is actually a template!
                            templatePath = Server.MapPath("Templates/" + spdTemplateName);

                            if (!File.Exists(templatePath))
                            {
                                string error = string.Format((string)GetLocalResourceObject("error4"), spdTemplateName);
                                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                                Response.Redirect(errorURL);
                                return;
                            }
                        }
                        //****************************************************

                        buffer = this.CreateNotice_Speed(receipt, path);
                        // Overlay the notice with the background
                        buffer = this.MergeNoticeWithBackground(buffer, receipt, spdTemplateName);
                    }

                    // Overlay the notice with the background
                    //buffer = this.MergeNoticeWithBackground(buffer, receipt, templateName);

                    //BD 3858 need to check whether the notice must be printed or not
                    if (receipt.PrintNotice)
                    {
                        // Stamp the receipt
                        //buffer = this.StampCopy(buffer);
                        document.Append(new PdfDocument(buffer));
                    }


                    //BD 3858 need to check if we must print the receipt or not
                    if (receipt.PrintReceipt)
                    {
                        // Create the receipt
                        buffer = this.CreateReceipt(receipt, receiptPath);
                        if (buffer.Length > 0)
                            document.Append(new PdfDocument(buffer));
                    }
                }

                // Output the PDF to the page
                //Helper_Web.OutputByteArray(this, document.Draw(), BufferType.PDF);
                byte[] buf = document.Draw();

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(buf);
                Response.End();

                buffer = null;
                buf = null;
            }
            catch
            {
            }
            finally
            {
                GC.Collect();
            }

        }

        #region RLV Notice

        private byte[] CreateNotice_RLV(SpotFineReceipt receipt, string reportPath)
        {
            // Check for the report name
            // TODO: Check if its an RLV and ret the RLV report if it is
            //string reportName = this.GetReportName(receipt, "FirstNotice RLV", "FirstNotice_RLV_JMPD.dplx");
            //reportName = Server.MapPath("Reports\\" + reportName);

            // Update the data set
            this.GetNoticeData(receipt);

            // Check if this is a Proxy
            bool isProxy = this.ds.Tables[0].Rows[0]["NotSendTo"].ToString().Equals("P");

            // Setup the DPLX
            this.layout = new DocumentLayout(reportPath);
            StoredProcedureQuery query = (StoredProcedureQuery)this.layout.GetQueryById("Query");
            query.ConnectionString = this.connectionString;

            // Setup the image events
            PlaceHolder barcodePlaaceholder = (PlaceHolder)this.layout.GetElementById("phBarCode");
            barcodePlaaceholder.LaidOut += new PlaceHolderLaidOutEventHandler(barcodePlaaceholder_LaidOut);

            // Get the controls & set their values
            this.SetOffenderLabel(isProxy);
            this.SetLabelMultilineValue("lblAddress", "DrvPOAdd1", "DrvPOAdd2", "DrvPOAdd3", "DrvPOAdd4", "DrvPOAdd5", "DrvPOCode");
            this.SetLabelValue("lblCode", "NotLocCode");
            this.SetLabelSpacedTicketNo("lblFormattedNotice");
            this.SetLabelMultilineValue("lblAuthorityAddress", "AutName", "AutPhysAddr1", "AutPhysAddr2", "AutPhysAddr3", "AutPhysCode");
            this.SetLabelValue("lblAutTel", "AutTel");
            this.SetLabelValue("lblAutFax", "AutFax");
            this.SetLabelValueOffenceEngDescr_RLV("lblOffenceDescrEng");
            this.SetLabelValueOffenceAfrDescr_RLV("lblOffenceDescrAfr");
            this.SetLabelDate("lblPrintDate", "NotPrint1stNoticeDate", "d MMMM yyyy");

            Document document = layout.Run();
            byte[] buffer = document.Draw();

            // Unhook the events
            barcodePlaaceholder.LaidOut -= new PlaceHolderLaidOutEventHandler(barcodePlaaceholder_LaidOut);

            return buffer;

        }

        #endregion

        #region Copy Stamp

        private byte[] StampCopy(byte[] buffer)
        {
            MergeDocument merge = new MergeDocument(new PdfDocument(buffer));

            ceTe.DynamicPDF.PageElements.Label label = new ceTe.DynamicPDF.PageElements.Label("COPY", 0, 300, 500, 100, Font.HelveticaBold, 64, TextAlign.Center, RgbColor.Silver);
            label.Angle = -20;

            // Create an anchor group to keep the label anchored regardless of page size
            AnchorGroup anchorGroup = new AnchorGroup(500, 100, Align.Center, VAlign.Top);
            anchorGroup.Add(label);

            ceTe.DynamicPDF.Template template = new ceTe.DynamicPDF.Template();
            merge.Template = template;
            template.Elements.Add(anchorGroup);

            return merge.Draw();
        }

        #endregion

        #region Receipt Generation

        private byte[] CreateReceipt(SpotFineReceipt receipt, string reportPath)
        {
            // Load the receipt DPLX
            //this.layout = new DocumentLayout(Server.MapPath("Reports/Receipt_CPI.dplx"));

            this.layout = new DocumentLayout(reportPath);

            StoredProcedureQuery query = (StoredProcedureQuery)this.layout.GetQueryById("Query");
            query.ConnectionString = this.connectionString;

            // Set the receipt data
            this.GetReceiptData(receipt);
            //BD 3858 check to see if there is receipt data - more for a testing purpose
            if (ds.Tables[0].Rows.Count != 0)
            {
                this.LayoutReceipt();

                // Create a document from the DPLX layout
                Document document = this.layout.Run();

                // Return the report document
                return document.Draw();
            }
            else
            {
                Byte[] document = new Byte[0];
                return document;
            }

        }

        private void GetReceiptData(SpotFineReceipt receipt)
        {
            SpotFineBatchDB db = new SpotFineBatchDB(this.connectionString);
            this.ds = db.GetReceiptData(receipt.RctIntNo);
        }

        private void LayoutReceipt()
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblAuthority");
            lbl.Text += this.ds.Tables[0].Rows[0]["AutName"].ToString().ToUpper();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblAuthority2");
            lbl.Text += this.ds.Tables[0].Rows[0]["AutName"].ToString().ToUpper();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblRctNumber");
            lbl.Text = this.ds.Tables[0].Rows[0]["SFBNRctNumber"].ToString().ToUpper();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblRctDate");
            //lbl.Text = ((DateTime)this.ds.Tables[0].Rows[0]["RctDate"]).ToString("yyyy-MM-dd");
            DateTime dt;
            if (DateTime.TryParse(this.ds.Tables[0].Rows[0]["RctDate"].ToString(), out dt))
                lbl.Text = dt.ToString("yyyy-MM-dd");
            else
                lbl.Text = string.Empty;
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblTicketNo");
            lbl.Text = this.ds.Tables[0].Rows[0]["NotTicketNo"].ToString();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblAmount");

            //update by Rachel 20140820 for 5337
            //lbl.Text = string.Format("R {0}", this.ds.Tables[0].Rows[0]["RTAmount"]);
            lbl.Text = string.Format(CultureInfo.InvariantCulture,"R {0}", this.ds.Tables[0].Rows[0]["RTAmount"]);
            //end update by Rachel 20140820 for 5337

            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblAmount2");

            //update by Rachel 20140820 for 5337
            //lbl.Text = string.Format("R {0}", this.ds.Tables[0].Rows[0]["RTAmount"]);
            lbl.Text = string.Format(CultureInfo.InvariantCulture,"R {0}", this.ds.Tables[0].Rows[0]["RTAmount"]);
            //end update by Rachel 20140820 for 5337

            CashType cashType = (CashType)((int)this.ds.Tables[0].Rows[0]["CashType"].ToString()[0]);
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblCashType");
            lbl.Text = cashType.ToString().ToUpper();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblCashier");
            lbl.Text += this.ds.Tables[0].Rows[0]["SFBNReceivedFrom"].ToString().ToUpper();
            lbl = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)this.layout.GetElementById("lblOffender");
            lbl.Text = this.ds.Tables[0].Rows[0]["Offender"].ToString().ToUpper();
        }

        #endregion

        #region Notice Background Overlay

        private byte[] MergeNoticeWithBackground(byte[] buffer, SpotFineReceipt receipt, string templateName)
        {
            // Create a merge document
            MergeDocument document = new MergeDocument(new PdfDocument(buffer));

            // Add the background
            ImportedPageContents contents = new ImportedPageContents(templateName, 1);
            ImportedPageData data;
            if (templateName.IndexOf("RLV") > 0)
                data = new ImportedPageData(contents, 0.0F, -10.0F);
            else
                data = new ImportedPageData(contents, 0.0F, 10.0F);

            ceTe.DynamicPDF.Template template = new ceTe.DynamicPDF.Template();
            document.Template = template;
            template.Elements.Add(data);

            return document.Draw();
        }

        #endregion

        #region Notice Generation

        private byte[] CreateNotice_Speed(SpotFineReceipt receipt, string reportPath)
        {
            // Check for the report name
            //string reportName = this.GetReportName(receipt, "FirstNotice", "FirstNotice.dplx");
            //reportName = Server.MapPath("Reports\\" + reportName);

            // Update the data set
            this.GetNoticeData(receipt);

            // Check if this is a Proxy
            bool isProxy = this.ds.Tables[0].Rows[0]["NotSendTo"].ToString().Equals("P");

            // Setup the DPLX
            this.layout = new DocumentLayout(reportPath);
            StoredProcedureQuery query = (StoredProcedureQuery)this.layout.GetQueryById("Query");
            query.ConnectionString = this.connectionString;

            // Setup the image events
            PlaceHolder imagePlaceHolder = (PlaceHolder)this.layout.GetElementById("phImage");
            imagePlaceHolder.LaidOut += new PlaceHolderLaidOutEventHandler(imagePlaceHolder_LaidOut);
            PlaceHolder barcodePlaaceholder = (PlaceHolder)this.layout.GetElementById("phBarCode");
            barcodePlaaceholder.LaidOut += new PlaceHolderLaidOutEventHandler(barcodePlaaceholder_LaidOut);

            // Get the controls & set their values
            //this.SetLabelValue("lblReference", "NotRefNo");
            // BD - not on report - this.SetLabelReference("lblReference", "NotRefNo");
            this.SetLabelReference("lblReferenceB", "NotRefNo");
            this.SetOffenderLabel(isProxy);
            this.SetLabelMultilineValue("lblAddress", "DrvPOAdd1", "DrvPOAdd2", "DrvPOAdd3", "DrvPOAdd4", "DrvPOAdd5", "DrvPOCode");
            this.SetLabelValue("lblNoticeNumber", "NotTicketNo");
            // BD - not on report - this.SetLabelSpacedTicketNo("lblFormattedNotice");
            //this.SetLabelMultilineValue("lblStatRef", "ChgStatRefline1", "ChgStatRefline1", "ChgStatRefline3", "ChgStatRefline4");
            this.SetLabelValue("lblStatRef", "ChgStatutoryRef");
            this.SetLabelDate("lblDate", "NotOffenceDate", "d MMMM yyyy");
            this.SetLabelDate("lblTime", "NotOffenceDate", "HH:mm");
            this.SetLabelValue("lblLocDescr", "NotLocDescr");
            //this.SetLabelValue("lblOffenceDescrEng", "OcTDescr");
            this.SetLabelValueOffenceDescr("lblOffenceDescrEng", "OcTDescr1");
            //this.SetLabelValue("lblOffenceDescrAfr", "OcTDescr1");
            this.SetLabelValueOffenceDescr("lblOffenceDescrAfr", "OcTDescr");
            this.SetLabelValue("lblCode", "NotLocCode");
            this.SetLabelValue("lblOfficerNo", "NotOfficerNo");
            this.SetLabelValue("lblCamera", "NotCamSerialNo");
            //BD - not in report - this.SetLabelReceipt();
            this.SetLabelDate("lblPaymentDate", "NotPaymentDate", "d MMMM yyyy");
            this.SetLabelValue("lblCourtName", "NotCourtName");
            //this.SetLabelValue("lblAmount", "ChgRevFineAmount");
            this.SetLabelPaymentAmountValue("lblAmount", "ChgRevFineAmount");
            this.SetLabelMultilineValue("lblText", "AutName", "AutPostAddr1", "AutPostAddr2", "AutPostAddr3", "AutPostCode");
            this.SetLabelValue("lblIssuedBy", "AutNoticeIssuedByInfo");
            this.SetLabelValue("lblPaymentInfo", "AutNoticePaymentInfo");
            this.SetLabelDate("lblPrintDate", "NotPrint1stNoticeDate", "d MMMM yyyy");
            //this.SetLabelValue("lblReferenceB", "");
            this.SetLabelDate("lblOffenceDate", "NotOffenceDate", "d MMMM yyyy");
            this.SetLabelDate("lblOffenceTime", "NotOffenceDate", "HH:mm");
            this.SetLabelValue("lblLocation", "NotLocDescr");
            this.SetLabelValue("lblSpeedLimit", "NotSpeedLimit");
            this.SetLabelValue("lblSpeed", "NotSpeed1");
            this.SetLabelValue("lblOfficer", "NotOfficerSName");
            this.SetLabelValue("lblRegNo", "NotRegNo");

            Document document = layout.Run();
            byte[] buffer = document.Draw();

            // Unhook the events
            imagePlaceHolder.LaidOut -= new PlaceHolderLaidOutEventHandler(imagePlaceHolder_LaidOut);
            barcodePlaaceholder.LaidOut -= new PlaceHolderLaidOutEventHandler(barcodePlaaceholder_LaidOut);

            return buffer;
        }

        private void SetLabelReference(string labelName, string columnName)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);
            label.Text = "VERWYSING/REFERENCE: " + this.ds.Tables[0].Rows[0]["NotTicketNo"].ToString().Trim().Replace("/", " / ");
        }

        private void SetLabelPaymentAmountValue(string labelName, string columnName)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);
            
            //update by Rachel 20140811 for 5337
            //label.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(this.ds.Tables[0].Rows[0][columnName].ToString())); 
            label.Text = "R " + string.Format(CultureInfo.InvariantCulture, "{0:0.00}",this.ds.Tables[0].Rows[0][columnName]); 
            //update by Rachel 20140811 for 5337
        }

        private void SetLabelValueOffenceDescr(string labelName, string columnName)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);
            label.Text = this.ds.Tables[0].Rows[0][columnName].ToString().Replace("*001      ", this.ds.Tables[0].Rows[0]["NotRegNo"].ToString()).Replace("*002", this.ds.Tables[0].Rows[0]["MinSpeed"].ToString());
        }

        private void SetLabelValueOffenceAfrDescr_RLV(string labelName)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);
            label.Text =string.Format( (string)GetLocalResourceObject("label.Text1"),
                this.ds.Tables[0].Rows[0]["NotVehicleMake"].ToString().Trim(),
                this.ds.Tables[0].Rows[0]["NotRegNo"].ToString().Trim(),
                string.Format("{0:d MMMM yyyy}", this.ds.Tables[0].Rows[0]["NotOffenceDate"]),
                string.Format("{0:HH:mm}", this.ds.Tables[0].Rows[0]["NotOffenceDate"]),
                this.ds.Tables[0].Rows[0]["NotLocDescr"].ToString(),
                this.ds.Tables[0].Rows[0]["AutName"].ToString());
        }

        private void SetLabelValueOffenceEngDescr_RLV(string labelName)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);
            label.Text = string.Format((string)GetLocalResourceObject("label.Text"),
                this.ds.Tables[0].Rows[0]["NotVehicleMake"].ToString().Trim(),
                this.ds.Tables[0].Rows[0]["NotRegNo"].ToString().Trim(),
                string.Format("{0:d MMMM yyyy}",this.ds.Tables[0].Rows[0]["NotOffenceDate"]),
                string.Format("{0:HH:mm}", this.ds.Tables[0].Rows[0]["NotOffenceDate"]),
                this.ds.Tables[0].Rows[0]["NotLocDescr"].ToString(),
                this.ds.Tables[0].Rows[0]["AutName"].ToString());
        }

        private void barcodePlaaceholder_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            // Jake 2011-02-25 Removed BarcodeNETImage
            //this.barcode.BarcodeText = this.ds.Tables[0].Rows[0]["NotTicketNo"].ToString();
            //this.barcode.ShowBarcodeText = false;
            //byte[] buffer = this.barcode.GetBarcodeBitmap(FILE_FORMAT.BMP);

            using (MemoryStream ms = new MemoryStream())
            {
                Code128Rendering.MakeBarcodeImage(this.ds.Tables[0].Rows[0]["NotTicketNo"].ToString(), 1, 25, true).Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                img.Height = 25.0F;
                e.ContentArea.Add(img);
            }
            //MemoryStream ms = new MemoryStream(buffer, false);
            //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

            //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
            //float f = 200F / image.Width;
            //image.Width = f * image.Width;
            //f = 150F / image.Width;
            //image.Height = f * image.Height;

            //e.ContentArea.Add(image);


        }

        private void imagePlaceHolder_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];
            // David Lin 20100331 remove images from database, get images data from remote file server
            byte[] buffer = null;
            if (this.ds.Tables[0].Rows[0]["ScanImage1"] != System.DBNull.Value)
            {
                WebService service = new WebService();
                ScanImageDB imageDB = new ScanImageDB(connectionString);
                int intImageNo = Convert.ToInt32(this.ds.Tables[0].Rows[0]["ScanImage1"]);
                ScanImageDetails imageDetail = imageDB.GetImageFullPath(intImageNo);
                buffer = service.GetImagesFromRemoteFileServer(imageDetail);
            }
            //byte[] buffer = this.ds.Tables[0].Rows[0]["ScanImage1"] == System.DBNull.Value ? null : (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];
            //MemoryStream ms = new MemoryStream(buffer, false);

            //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

            //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
            //float f = 200F / image.Width;
            //image.Width = image.Width * f;
            //image.Height = image.Height * f;

            //e.ContentArea.Add(image);
            if(buffer != null)
            {
                ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                img.Height = 160.0F;            //12
                img.Width = 210.0F;             //15
                e.ContentArea.Add(img);

                int generation = System.GC.GetGeneration(buffer);
                buffer = null;
                System.GC.Collect(generation);
            }
        }

        private void SetLabelReceipt()
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById("lblReceipt");

            this.SetLabelDate("lblReceipt", "RctDate", "yyyy-MM-dd");
            string date = label.Text;

            //update by Rachel 20140820 for 5337
            //label.Text = this.ds.Tables[0].Rows[0]["RctNumber"].ToString().Trim() + " " + date + " R" + this.ds.Tables[0].Rows[0]["RTAmount"].ToString();
            label.Text = this.ds.Tables[0].Rows[0]["RctNumber"].ToString().Trim() + " " + date + " R" + Convert.ToString(this.ds.Tables[0].Rows[0]["RTAmount"],CultureInfo.InvariantCulture);
            //end update by Rachel 20140820 for 5337
        }

        private void SetOffenderLabel(bool isProxy)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById("lblForAttention");

            StringBuilder sb = new StringBuilder();
            string temp;

            if (isProxy)
            {
                // [Proxy] as representative of [Driver]
                temp = this.ds.Tables[0].Rows[0]["PrxInitials"].ToString().Trim();
                if (temp.Length > 0)
                    sb.Append(temp + " ");
                sb.Append(this.ds.Tables[0].Rows[0]["PrxSurname"].ToString().Trim());

                sb.Append(" as Representative of ");

                temp = this.ds.Tables[0].Rows[0]["DrvInitials"].ToString().Trim();
                if (temp.Length > 0)
                    sb.Append(temp + " ");
                sb.Append(this.ds.Tables[0].Rows[0]["DrvSurname"].ToString().Trim());
            }
            else
            {
                temp = this.ds.Tables[0].Rows[0]["DrvInitials"].ToString().Trim();
                if (temp.Length > 0)
                    sb.Append(temp + " ");
                sb.Append(this.ds.Tables[0].Rows[0]["DrvSurname"].ToString().Trim());
            }

            label.Text = sb.ToString();
        }

        private void SetLabelSpacedTicketNo(string labelName)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);
            string temp = this.ds.Tables[0].Rows[0]["NotTicketNo"].ToString();
            label.Text = temp.Replace("/", " / ");
        }

        private void SetLabelMultiItemValue(string labelName, params string[] fields)
        {
            this.SetLabelMultiValues(labelName, " ", fields);
        }

        private void SetLabelMultilineValue(string labelName, params string[] fields)
        {
            this.SetLabelMultiValues(labelName, "\r\n", fields);
        }

        private void SetLabelMultiValues(string labelName, string separator, params string[] fields)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);

            StringBuilder sb = new StringBuilder();
            string temp;
            foreach (string s in fields)
            {
                temp = this.ds.Tables[0].Rows[0][s].ToString().Trim();
                if (temp.Length > 0)
                    sb.Append(temp + separator);
            }

            label.Text = sb.ToString();
        }

        private void SetLabelDate(string labelName, string columnName, string format)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);
            DateTime dt;
            if (DateTime.TryParse(this.ds.Tables[0].Rows[0][columnName].ToString(), out dt))
                label.Text = dt.ToString(format);
            else
                label.Text = string.Empty;
        }

        private void SetLabelValue(string labelName, string columnName)
        {
            ceTe.DynamicPDF.ReportWriter.ReportElements.Label label = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)layout.GetElementById(labelName);
            label.Text = this.ds.Tables[0].Rows[0][columnName].ToString();
        }

        private void GetNoticeData(SpotFineReceipt receipt)
        {
            NoticeReportDB db = new NoticeReportDB(this.connectionString);
            this.ds = db.GetCiprusNoticeRePrint(receipt.NotIntNo);
        }

        private string GetReportName(SpotFineReceipt receipt, string reportType, string defaultName)
        {
            AuthReportNameDB db = new AuthReportNameDB(this.connectionString);

            string reportName = db.GetAuthReportName(receipt.AutIntNo, reportType);
            if (reportName.Equals(string.Empty))
            {
                reportName = defaultName;
                db.AddAuthReportName(receipt.AutIntNo, reportName, reportType, "TMS", string.Empty);
            }

            return reportName;
        }

        private bool CheckBatchIsValid(out List<SpotFineReceipt> receipts)
        {
            receipts = new List<SpotFineReceipt>();
            CiprusData db = new CiprusData(this.connectionString);
            return db.SpotFineBatchPrintData(this.printName, this.userCompleteDate, receipts, (Session["userDetails"] as UserDetails).UserLoginName);
        }

        #endregion

    }
}
