<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.EODChequeReport" Codebehind="EODChequeReport.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
  </head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" style="text-align: center; width: 500px">
                    <asp:UpdatePanel ID="upd" runat="server">
                        <ContentTemplate>
                            <table style="border-style: none; width: 492px;" class="Normal">
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:Panel ID="pnlTitle" runat="Server" HorizontalAlign="Center">
                                            <asp:Label ID="lblPageName" runat="server" Width="500px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label><p>
                                            </p>
                                        </asp:Panel>
                                        
                                    </td>
                                </tr>
                               <tr>
                                    <td align="left" colspan="1">
                                            <asp:Label ID="lblError" CssClass="NormalRed" runat="server" />                                   
                                    </td>
                                </tr> 
                                <tr>
                                    <td align="left" style="vertical-align: top; width: 200px">
                                        <asp:Label ID="Label3" runat="server" Width="200px" CssClass="NormalBold" Text="<%$Resources:lblStartdate.Text %>"></asp:Label></td>
                                    <td style="vertical-align: top; " valign="top" class="style1">
                                        <asp:TextBox runat="server" ID="dtpStartDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpStartDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="dtpStartDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqStartDate.ErrorMsg %>" 
                                            
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])" 
                                            Width="150px"></asp:RegularExpressionValidator>
                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                            ControlToValidate="dtpStartDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqStartDate.ErrorMsg %>"></asp:RequiredFieldValidator>
                                </tr>
                                <tr>
                                    <td align="left" style="vertical-align: top; width: 200px; height: 43px;">
                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="200px" Text="<%$Resources:lblEndDate.Text %>"></asp:Label></td>
                                    <td style="vertical-align: top; " class="style2">
                                       
                                        <asp:TextBox runat="server" ID="dtpEndDate" CssClass="Normal" Height="20px" autocomplete="off" UseSubmitBehavior="False" />
                                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="dtpEndDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                            ControlToValidate="dtpEndDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqStartDate.ErrorMsg %>" Width="150px"></asp:RequiredFieldValidator>
                                        &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                            ControlToValidate="dtpEndDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqStartDate.ErrorMsg %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator> </td>
                                </tr>
                                <tr>
                                    <td align="center" style="vertical-align: top; " colspan="2">
                                   
                                        <fieldset>
                                            <legend>
                                                <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblOption.Text %>"></asp:Label></legend>
                                            <asp:RadioButtonList ID="rdlOption" runat="server" CssClass="Normal" 
                                                RepeatColumns="2" RepeatLayout="Table" Width="251px">
                                                <asp:ListItem Selected="True" Value="0" Text="<%$Resources:rdlOptionItem.Text %>"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="<%$Resources:rdlOptionItem.Text1 %>"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </fieldset>
                                   
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 26px; " align="center">
                                    <asp:Button ID="btnSubmit" runat="server" Text="<%$Resources:btnSubmit.Text %>" CssClass="NormalButton" OnClick="btnSubmit_Click"
                                            Width="120px" Height="25px" /></td>
                                    <td align="center" class="style3">
                                        </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label5"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
