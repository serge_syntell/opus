<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.WOACancellation" CodeBehind="WOACancellation.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <style type="text/css">
        #Table2
        {
            width: 702px;
        }
    </style>
</head>
<script type="text/javascript">
    function showConfirm() {
        var woaNumber = document.getElementById("txtWOANumb").value;
        if (woaNumber == "" || woaNumber == null) return true;
        if (window.confirm("Are you sure you want to rollback WOA [" + woaNumber + "] to the previous stage?") == false) {
            return false;
        }

        return true;
    }
    function showReIssueConfirm() {

        var woaNumber = document.getElementById("txtWOANumb").value;

        if (woaNumber == "" || woaNumber == null) return false;
        var msg = "Are you sure you want to cancel this WOA [" + woaNumber + "] and withdraw summons for reissue?";

        if (window.confirm(msg) == false) {
            return false;
        }

        return true;
    }
</script>
<body style="margin: 0px" background="<%=backgroundImage %>">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%" style="margin-right: 0px">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">

                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" height="482" style="margin-right: 54px;" width="950px">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                </p>
                                <p>
                                    &nbsp;
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:UpdatePanel ID="udp" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlGeneral" runat="server">
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="False"></asp:Label>
                                            <br />
                                            <br />
                                            <table id="Table2" cellspacing="1" cellpadding="1" border="0">
                                                <tr>
                                                    <td width="162">
                                                        <asp:Label ID="lblSelAuthority" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="7">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" Width="217px" CssClass="Normal"
                                                            AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="162">
                                                        <asp:Label ID="lblwoaNumb" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblwoaNumb.Text %>"></asp:Label>
                                                    </td>
                                                    <td width="7">
                                                        <asp:TextBox ID="txtWOANumb" ClientIDMode="Static" runat="server" CssClass="NormalMand" Width="215px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                            OnClick="btnSearch_Click"></asp:Button>&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <br />

                                        <asp:DataGrid ID="dgWOA" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" Font-Size="8pt"
                                            CellPadding="4" Width="100%" GridLines="Vertical" OnItemCreated="dgWOA_ItemCreated" OnItemCommand="dgWOA_ItemCommand" OnItemDataBound="dgWOA_ItemDataBound">
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="SummonsNo" HeaderText="<%$Resources:dgWOA.HeaderText %>" ItemStyle-Width="160px"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SumIssueDate" HeaderText=" <%$Resources:dgWOA.HeaderText1 %> " ItemStyle-Width="120px"
                                                    DataFormatString="{0:yyyy-MM-dd}"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NotTicketNo" HeaderText=" <%$Resources:dgWOA.HeaderText2 %> "></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NotOffenceDate" HeaderText=" <%$Resources:dgWOA.HeaderText3 %> " DataFormatString="{0:yyyy-MM-dd}"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="WOANumber" HeaderText=" <%$Resources:dgWOA.HeaderText4 %> "></asp:BoundColumn>
                                                <asp:BoundColumn DataField="WOAIssueDate" HeaderText=" <%$Resources:dgWOA.HeaderText5 %> " DataFormatString="{0:yyyy-MM-dd}" ItemStyle-Width="100px"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="WOAFineAmount" HeaderText=" <%$Resources:dgWOA.HeaderText6 %> " DataFormatString="{0:f}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="80px"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="WOAAddAmount" HeaderText=" <%$Resources:dgWOA.HeaderText7 %> " DataFormatString="{0:f}" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120px"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NotFilmType" HeaderText="NotFilmType" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SumIntNo" HeaderText="SumIntNo" Visible="false"></asp:BoundColumn>
                                                <%--                                                    <asp:BoundColumn DataField="SchIntNo" HeaderText="SchIntNo" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ChgRevFineAmount" HeaderText="<%$Resources:dgWOA.HeaderText8 %>" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="SChContemptAmount" HeaderText="<%$Resources:dgWOA.HeaderText7 %>"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="SChOtherAmount" HeaderText="SChOtherAmount" Visible="false"></asp:BoundColumn>--%>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkCmdSelect" Text="Select" CommandName="Select" CommandArgument='<%#Bind("WoaIntNo")%>' runat="server" />
                                                        <%--<br /><asp:Label ID="lblReverse" runat="server" Text="<%$Resources:btnReverse.Text %>"></asp:Label>--%>
                                                        <asp:LinkButton ID="lnkCmdReverse" Text="<%$Resources:btnReverse.Text %>" OnClientClick="return showConfirm();" CommandName="WOACancelReverse" CommandArgument='<%#Bind("WoaIntNo")%>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                        <br />

                                        <asp:Panel ID="panelDetails" runat="server">

                                            <table id="Table4" cellspacing="1" cellpadding="1" border="0">
                                                <tr>
                                                    <td colspan="2" style="height: 30px;">
                                                        <asp:Label ID="lblCurrentMsg" runat="server" CssClass="NormalBold"></asp:Label>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 150px;">
                                                        <asp:Label ID="lblPublicProsecutor" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblPublicProsecutor.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlMagistrate" runat="server" AutoPostBack="false" CssClass="NormalMand"></asp:DropDownList>
                                                        <!-- <asp:TextBox ID="txtPProsecutor" runat="server" CssClass="NormalMand" Width="300px"></asp:TextBox>-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbReasonCancelled" runat="server" CssClass="ProductListHead" Text="<%$Resources:lbReasonCancelled.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtReasonCancel" runat="server" TextMode="MultiLine" Width="300px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:CheckBox ID="chkWithdrawCAmount" runat="server" CssClass="Normal"
                                                            Text="<%$Resources:chkWithdrawCAmount.Text %>"
                                                            OnCheckedChanged="chkWithdrawCAmount_CheckedChanged" AutoPostBack="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label2" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblDateCancelled.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="dtpWOADate" CssClass="Normal" Height="20px" autocomplete="off"
                                                            UseSubmitBehavior="False" />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" TargetControlID="dtpWOADate"
                                                            Format="yyyy-MM-dd">
                                                        </cc1:CalendarExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="dtpWOADate"
                                                            CssClass="NormalRed" Display="Dynamic" ErrorMessage="<%$Resources:reqWOADate.ErrorMsg %>"
                                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <%--
                                                <tr>
                                                    <td style="text-align: right;">
                                                    <asp:Button ID="btnCancelWarrant" runat="server" CssClass="NormalButton" Text="<%$Resources:btnCancelWarrant.Text %>"
                                                        OnClick="btnCancelWarrant_Click" Width="330px" />
                                                </td>
                                                    <td>
                                                        
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Button ID="btnWithdrawSummonsPermanently" runat="server" CssClass="NormalButton" OnClick="btnWithdrawSummonsPermanently_Click" Text="<%$Resources:btnWithdrawSummonsPermanently.Text %>" />
                                                        &nbsp;&nbsp;
                                                        <asp:Button ID="btnWithdrawSummonsReissue" runat="server" CssClass="NormalButton" OnClientClick="return showReIssueConfirm();" OnClick="btnWithdrawSummonsReissue_Click" Text="<%$Resources:btnWithdrawSummonsReissue.Text %>" />
                                                        <%--<asp:Button ID="btnWOAReversal" runat="server" CssClass="NormalButton" OnClick="btnWOAReversal_Click" OnClientClick="return showConfirm();" Text="<%$Resources:btnReverse.Text %>" />--%>
                                                    </td>
                                                </tr>
                                            </table>

                                        </asp:Panel>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label9"
                                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label>
                                        </p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
