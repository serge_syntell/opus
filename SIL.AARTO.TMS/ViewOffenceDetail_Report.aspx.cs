using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using System.IO;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class ViewOffenceDetail_Report : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Notice Detail Enquiry";
        protected string thisPageURL = "ViewOffenceDetail_Report.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticeEnquiry");
            if (reportPage.Equals(string.Empty))
            {
                //int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiry.dplx", "NoticeEnquiry", "system");
                int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiry_ST.dplx", "NoticeEnquiry", "system", "");
                reportPage = "NoticeEnquiry_ST.dplx";
            }

            string path = Server.MapPath("reports/" + reportPage);
            DocumentLayout doc = new DocumentLayout(path);


            if (Request.QueryString["NotIntNo"] == null)
            {
                Response.Write((string)GetLocalResourceObject("strWriteMsg"));
                Response.Flush();
                Response.End();
                return;
            }

            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea raTotal = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("raTotal");
                raTotal.LayingOut += new LayingOutEventHandler(ra_Total);
                //Barry Dickson - add control in to not show receipt fields if there has been no payment made or there was a reversal
                //label for raTotal
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRctAmount");
                lblRctAmount.LayingOut += new LayingOutEventHandler(lbl_Amount);
                //Receipt Number
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbRctNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbRctNo");
                rcbRctNo.LayingOut += new LayingOutEventHandler(ra_RctNo);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRctNo");
                lblRctNo.LayingOut += new LayingOutEventHandler(lbl_RctNo);
                ////EasyPay Number
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcbEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rcbEasyPay");
                rcbEasyPay.LayingOut += new LayingOutEventHandler(ra_Easy);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblEasyPay");
                lblEasyPay.LayingOut += new LayingOutEventHandler(lbl_Easy);

                //Jerry 2012-11-26 Receipt AGNumber
                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea raRctAGNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("raAGNumber");
                raRctAGNo.LayingOut += new LayingOutEventHandler(ra_RctAgNo);
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRctAgNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAGNumber");
                lblRctAgNo.LayingOut += new LayingOutEventHandler(lbl_RctAgNo);

                //Remanded from case number and court date

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcdRemandedFromCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rdaSumRemandedFromCaseNumber");
                rcdRemandedFromCaseNo.LayingOut += new LayingOutEventHandler(ra_RemandedFromCaseNo);

                ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea rcdRemandedFromCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.RecordArea)doc.GetElementById("rdaSumRemandedFromCourtDate");
                rcdRemandedFromCourtDate.LayingOut += new LayingOutEventHandler(ra_RemandedFromCourtDate);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRemandedFromCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRemandedFromCourtDate");
                lblRemandedFromCourtDate.LayingOut += new LayingOutEventHandler(lbl_RemandedFromCourtDate);

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRemandedFromCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRemandedFromCaseNumber");
                lblRemandedFromCaseNo.LayingOut += new LayingOutEventHandler(lbl_RemandedFromCaseNo);

                int nNotIntNo = Convert.ToInt32(Request.QueryString["NotIntNo"].ToString());

                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                StoredProcedureQuery query2 = (StoredProcedureQuery)doc.GetQueryById("Query2");
                StoredProcedureQuery query3 = (StoredProcedureQuery)doc.GetQueryById("Query3");
                query.ConnectionString = this.connectionString;
                query2.ConnectionString = this.connectionString;
                query3.ConnectionString = this.connectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("NotIntNo", nNotIntNo);

                ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder imagePlaceHolder = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("Img1");
                imagePlaceHolder.LaidOut += new PlaceHolderLaidOutEventHandler(imagePlaceHolder_LaidOut);
                ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder imagePlaceHolder2 = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("Img2");
                imagePlaceHolder2.LaidOut += new PlaceHolderLaidOutEventHandler(imagePlaceHolder2_LaidOut);

                Document report = doc.Run(parameters);

                byte[] buffer = report.Draw();

                imagePlaceHolder.LaidOut -= new PlaceHolderLaidOutEventHandler(imagePlaceHolder_LaidOut);
                imagePlaceHolder2.LaidOut -= new PlaceHolderLaidOutEventHandler(imagePlaceHolder2_LaidOut);

                raTotal.LayingOut -= new LayingOutEventHandler(ra_Total);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(lbl_Amount);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(lbl_Easy);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(lbl_RctNo);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(ra_RctNo);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(ra_Easy);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(ra_RctAgNo);
                lblRctAmount.LayingOut -= new LayingOutEventHandler(lbl_RctAgNo);

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(buffer);
                Response.End();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Response.End();
            }
        }

        public void ra_Total(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                    {
                        //update by Rachel 20140819 for 5337
                        //ra.Text = String.Format("R {0:#0.00}", e.LayoutWriter.RecordSets.Current["Total"]);
                        ra.Text = String.Format(CultureInfo.InvariantCulture,"R {0:#0.00}", e.LayoutWriter.RecordSets.Current["Total"]);
                        //end update by Rachel 20140819 for 5337
                    }

                }
            }
            catch { }
        }
        //Barry Dickson - to handle to check on receipt amount and if to show label.
        public void lbl_Amount(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty)
                        lblA.Text = (string)GetLocalResourceObject("lblA.Text");

                }
            }
            catch { }

        }
        //Barry Dickson
        public void lbl_RctNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRN = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        lblRN.Text = (string)GetLocalResourceObject("lblRN.Text");
                }
            }
            catch { }
        }

        //Barry Dickson
        public void lbl_Easy(object sender, LayingOutEventArgs e)
        {
            try
            {
                //dls 080326 - always show easypay number, if there is one

                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblE = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                //if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                //{
                //    if (e.LayoutWriter.RecordSets.Current["Total"] != string.Empty)
                //        lblE.Text = "Easy Pay No:";

                //}

                if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"].ToString() != string.Empty)
                        lblE.Text = (string)GetLocalResourceObject("lblE.Text");

                }
            }
            catch { }
        }

        //Barry Dickson
        public void ra_RctNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["RctNumber"]);
                }
            }
            catch { }
        }

        //Jerry 2012-11-26 add
        public void ra_RctAgNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["AGNumber"]);
                }
            }
            catch { }
        }

        //Jerry 2012-11-26 add
        public void lbl_RctAgNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAGNum = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["Total"].ToString() != string.Empty && int.Parse(e.LayoutWriter.RecordSets.Current["Total"].ToString()) != 0 &&
                        e.LayoutWriter.RecordSets.Current["AGNumber"] != null && e.LayoutWriter.RecordSets.Current["AGNumber"].ToString() != string.Empty)
                        lblAGNum.Text = (string)GetLocalResourceObject("lblAGNum.Text");
                }
            }
            catch { }
        }

        public void lbl_RemandedFromCourtDate(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString() != string.Empty)
                        lblA.Text = (string)GetLocalResourceObject("lblA.Text1");

                }
            }
            catch { }
        }

        public void lbl_RemandedFromCaseNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblA = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"].ToString() != string.Empty)
                        lblA.Text = (string)GetLocalResourceObject("lblA.Text2");

                }
            }
            catch { }
        }

        public void ra_RemandedFromCaseNo(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"].ToString() != string.Empty)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["SumRemandedFromCaseNumber"]);
                }
            }
            catch
            {
            }
        }

        public void ra_RemandedFromCourtDate(object sender, LayingOutEventArgs e)
        {
            try
            {
                RecordArea ra = (RecordArea)sender;
                if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString() != string.Empty)
                    {
                        DateTime courtDate = Convert.ToDateTime(e.LayoutWriter.RecordSets.Current["SumRemandedFromCourtDate"].ToString());
                        ra.Text = String.Format("{0}", courtDate.ToString("yyyy-MM-dd"));
                    }
                }
            }
            catch
            {
            }
        }

        //Barry Dickson
        public void ra_Easy(object sender, LayingOutEventArgs e)
        {
            try
            {
                //dls 080326 - always show easypay number, if there is one

                RecordArea ra = (RecordArea)sender;
                //if (e.LayoutWriter.RecordSets.Current["Total"] != DBNull.Value)
                //{
                //    if (e.LayoutWriter.RecordSets.Current["Total"] != string.Empty)
                //        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"]);

                //}

                if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"] != DBNull.Value)
                {
                    if (e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"].ToString() != string.Empty)
                        ra.Text = Convert.ToString(e.LayoutWriter.RecordSets.Current["NotEasyPayNumber"]);
                }
            }
            catch { }
        }

        private void imagePlaceHolder_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            try
            {
                //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];

                // david lin 20100324 - remove images from database
                //byte[] buffer = e.LayoutWriter.RecordSets.Current["Max_SI"] == System.DBNull.Value ? null : (byte[])e.LayoutWriter.RecordSets.Current["Max_Si"];                
                byte[] buffer = null;
                if (e.LayoutWriter.RecordSets.Current["Max_SI"] != System.DBNull.Value)
                {
                    ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                    ScanImageDetails imgMax = imgDB.GetImageFullPath(Convert.ToInt32(e.LayoutWriter.RecordSets.Current["Max_SI"]));
                    WebService webService = new WebService();
                    buffer = webService.GetImagesFromRemoteFileServer(imgMax);
                }

                //MemoryStream ms = new MemoryStream(buffer, false);
                //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);
                //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
                //float f = 200F / image.Width;
                //image.Width = image.Width * f;
                //image.Height = image.Height * f;
                //e.ContentArea.Add(image);
                if (buffer != null)
                {
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                    img.Height = 72.0F;            //12
                    img.Width = 90.0F;             //15
                    //dls 091126 - since we added the page breaks, the X & Y values of the images seem to be reset to 0
                    img.X = 438.0F;
                    img.Y = 6.0F;
                    e.ContentArea.Add(img);
                }
            }
            catch { }
        }

        private void imagePlaceHolder2_LaidOut(object sender, PlaceHolderLaidOutEventArgs e)
        {
            try
            {
                //byte[] buffer = (byte[])this.ds.Tables[0].Rows[0]["ScanImage1"];               

                // david lin 20100324 - remove images from database
                //byte[] buffer = e.LayoutWriter.RecordSets.Current["Min_SI"] == System.DBNull.Value ? null : (byte[])e.LayoutWriter.RecordSets.Current["Min_Si"];
                byte[] buffer = null;
                if (e.LayoutWriter.RecordSets.Current["Min_SI"] != System.DBNull.Value)
                {
                    ScanImageDB imgDB = new ScanImageDB(this.connectionString);
                    ScanImageDetails imgMin = imgDB.GetImageFullPath(Convert.ToInt32(e.LayoutWriter.RecordSets.Current["Min_SI"]));
                    WebService webService = new WebService();
                    buffer = webService.GetImagesFromRemoteFileServer(imgMin);
                }

                //MemoryStream ms = new MemoryStream(buffer, false);

                //System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(ms);

                //ceTe.DynamicPDF.PageElements.Image image = new ceTe.DynamicPDF.PageElements.Image(bmp, 0, 0);
                //float f = 200F / image.Width;
                //image.Width = image.Width * f;
                //image.Height = image.Height * f;

                //e.ContentArea.Add(image);
                if (buffer != null)
                {
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(buffer), 0, 0);
                    img.Height = 72.0F;            //12
                    img.Width = 90.0F;             //15
                    //dls 091126 - since we added the page breaks, the X & Y values of the images seem to be reset to 0
                    img.X = 438.0F;
                    img.Y = 99.5F;
                    e.ContentArea.Add(img);
                }
            }
            catch { }
        }

    }
}