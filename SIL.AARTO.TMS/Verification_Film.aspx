<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Register Src="ImageControl.ascx" TagName="ImageViewer" TagPrefix="iv1" %>
<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.Verification_Film" EnableSessionState="True" Codebehind="Verification_Film.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= title %>
    </title>
   <%-- <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">--%>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Services>
                <asp:ServiceReference Path="WebService.asmx" />
            </Services>
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/WebService.js" />
            </Scripts>
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle" style="height: 173px">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
       
       
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td align="center" valign="top" >
                </td>
                <td valign="top" align="left" colspan="1" width="100%">
                    <table border="0" width="100%">
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="592px" CssClass="ContentHead">Verification - Stage 1 (Validate Film data)</asp:Label></td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" width="100%" style="height: 316px">
                                <table width="100%">
                                    <tr>
                                        <td align="center" width="40%">
                                          <asp:UpdatePanel ID="udpFrame" runat="server">
                        <ContentTemplate>
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                            <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Yes"
                                                CssClass="NormalButton" />
                                            <asp:Button ID="btnNo" runat="server" OnClick="btnNo_Click" Text="No" CssClass="NormalButton" />
                                            <asp:Panel ID="pnlFilm" runat="server" >
                                                <table >
                                                     <tr style="display:<%=_ISASD%>">
                                                        <td align="left" valign="middle">
                                                            <asp:Label ID="lbVIOLATIONTYPE" runat="server" CssClass="SubContentHead" Width="100%">VIOLATION TYPE:</asp:Label>
                                                         </td>
                                                        <td colspan="2" align="left" valign="middle">
                                                            <asp:Label ID="lbASD" runat="server" CssClass="SubContentHead" Width="100%">AVERAGE SPEED OVER DISTANCE</asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" 
                                                                Width="150px" >Local authority:</asp:Label><br />
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblAuthority" runat="server" CssClass="SubContentHead" ></asp:Label><br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <asp:Label ID="Label8" runat="server" CssClass="NormalBold" >Film:</asp:Label></td>
                                                        <td colspan="2" >
                                                            <asp:Label ID="lblFilmNo" runat="server" CssClass="NormalBold" Width="136px"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <asp:Label ID="Label4" runat="server" CssClass="NormalBold">Location:</asp:Label></td>
                                                        <td colspan="2" >
                                                            <asp:DropDownList ID="ddlLocation" runat="server" Width="333px" CssClass="Normal"
                                                                Height="15px" AutoPostBack="True" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <asp:Label ID="Label7" runat="server" CssClass="NormalBold" >Camera:</asp:Label></td>
                                                        <td colspan="2" >
                                                            <asp:DropDownList ID="ddlCamera" runat="server" Width="169px" CssClass="Normal" Height="15px">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr style="display:<%=_ISASD%>">
                                                        <td>
                                                            <asp:Label ID="lbCamera2" runat="server" CssClass="NormalBold" Width="100%">2nd Camera:</asp:Label>
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="ddlCamera2" runat="server" Width="169px" CssClass="Normal" Height="15px" Enabled="false">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr style="display:<%=_ISASD%>">
                                                        <td>
                                                            <asp:Label ID="lbSectionName" runat="server" CssClass="NormalBold" Width="100%">Section Name:</asp:Label>
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="tbSectionName" runat="server" CssClass="NormalBold" Width="169px" Enabled="false"></asp:TextBox></td>
                                                    </tr>
                                                    <tr style="display:<%=_ISASD%>">
                                                        <td>
                                                            <asp:Label ID="lbSectionCode" runat="server" CssClass="NormalBold" Width="100%">Section Code:</asp:Label>
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="tbSectionCode" runat="server" CssClass="NormalBold" Width="169px" Enabled="false"></asp:TextBox></td>
                                                    </tr>
                                                    <tr style="display:<%=_ISASD%>">
                                                        <td>
                                                            <asp:Label ID="lbDistance" runat="server" CssClass="NormalBold" Width="100%">Section Distance:</asp:Label>
                                                         </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="tbDistance" runat="server" CssClass="NormalBold" Width="169px" Enabled="false"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="94px">Traffic officer:</asp:Label></td>
                                                        <td colspan="2" >
                                                            <asp:DropDownList ID="ddlOfficer" runat="server" Width="329px" CssClass="NormalBold"
                                                                Height="15px" Enabled="False">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Width="94px">Road type:</asp:Label></td>
                                                        <td colspan="2" >
                                                            <asp:DropDownList ID="ddlRoadType" runat="server" Width="169px" CssClass="NormalBold"
                                                                Height="15px" Enabled="False">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Width="94px">Court:</asp:Label></td>
                                                        <td colspan="2" >
                                                            <asp:DropDownList ID="ddlCourt" runat="server" Width="333px" CssClass="NormalBold" Height="15px" Enabled="False">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Width="94px">Speed zone:</asp:Label></td>
                                                        <td colspan="2" >
                                                            <asp:DropDownList ID="ddlSpeed" runat="server" Width="169px" CssClass="NormalBold" Height="15px" Enabled="False">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td>
                                                            <asp:Button ID="btnAccept" runat="server" CssClass="NormalButton" 
                                                                OnClick="btnAccept_Click" Text="Accept " />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnChange" runat="server" CssClass="NormalButton" 
                                                                OnClick="btnChange_Click" Text="Change" />
                                                            <asp:Button ID="btnReject" runat="server" CssClass="NormalButton" style="display:<%=_ISASD%>" 
                                                                OnClick="btnReject_Click" Text="Reject"/>
                                                                </td>
                                              <td>  <input id="btnClose" class="NormalButtonRed" type="button" value="CLOSE" onclick="window.close();" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            &nbsp;&nbsp; &nbsp;       </ContentTemplate>
        </asp:UpdatePanel>
                                <asp:UpdateProgress ID="udp" runat="server" AssociatedUpdatePanelID="udpFrame">
                                    <ProgressTemplate>
                                        <p class="NormalBold" style="text-align: center">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                                        <td align="center" width="60%">
                                            <iv1:ImageViewer ID="imageViewer1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
        </table>
 
        
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
                        <tr>
                            <td class="SubContentHeadSmall" valign="top" width="100%" align="center">
                                &nbsp;</td>
                        </tr>
                    </table>
    </form>
</body>
</html>
