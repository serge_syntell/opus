using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Util;
using System.Data;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    public partial class ManagementStatistics : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private int autIntNo;
        private string login;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        //protected string thisPage = "Management Report";
        protected string description = String.Empty;
        protected string thisPageURL = "ManagementStatistics.aspx";

        public ManagementStatistics()
        {
            autIntNo = 0;
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                //2012-3-6 linda modified into a multi-language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateAuthorities();
                this.PopulateMonth();
                txtYear.Text = DateTime.Today.Year.ToString();
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void PopulateMonth()
        {
            //2012-3-6 linda modified into a multi-language
            this.ddlMonth.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlMonth.Items1"), "0"));
            this.ddlMonth.Items.Insert(1, new ListItem((string)GetLocalResourceObject("ddlMonth.Items2"), "1"));
            this.ddlMonth.Items.Insert(2, new ListItem((string)GetLocalResourceObject("ddlMonth.Items3"), "2"));
            this.ddlMonth.Items.Insert(3, new ListItem((string)GetLocalResourceObject("ddlMonth.Items4"), "3"));
            this.ddlMonth.Items.Insert(4, new ListItem((string)GetLocalResourceObject("ddlMonth.Items5"), "4"));
            this.ddlMonth.Items.Insert(5, new ListItem((string)GetLocalResourceObject("ddlMonth.Items6"), "5"));
            this.ddlMonth.Items.Insert(6, new ListItem((string)GetLocalResourceObject("ddlMonth.Items7"), "6"));
            this.ddlMonth.Items.Insert(7, new ListItem((string)GetLocalResourceObject("ddlMonth.Items8"), "7"));
            this.ddlMonth.Items.Insert(8, new ListItem((string)GetLocalResourceObject("ddlMonth.Items9"), "8"));
            this.ddlMonth.Items.Insert(9, new ListItem((string)GetLocalResourceObject("ddlMonth.Items10"), "9"));
            this.ddlMonth.Items.Insert(10, new ListItem((string)GetLocalResourceObject("ddlMonth.Items11"), "10"));
            this.ddlMonth.Items.Insert(11, new ListItem((string)GetLocalResourceObject("ddlMonth.Items12"), "11"));
            this.ddlMonth.Items.Insert(12, new ListItem((string)GetLocalResourceObject("ddlMonth.Items13"), "12"));
            this.ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(DateTime.Today.Month.ToString()));
            return;
        }

       
        protected void btnView_Click(object sender, EventArgs e)
        {
            if (this.ddlAuthority.SelectedValue.Equals(string.Empty))
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            if ((this.ddlMonth.SelectedIndex > 0) && this.txtYear.Text == string.Empty)
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            int year;
            if(!int.TryParse(this.txtYear.Text, out year))
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            this.lblError.Text = string.Empty;

            int selectedAutIntno = int.Parse(this.ddlAuthority.SelectedValue);
            //Helper_Web.BuildPopup(this, "ManagementStatistics_Viewer.aspx", "AutIntNo", autIntNo.ToString(), "Year", this.txtYear.Text, "Month", this.ddlMonth.Text);

            //ReportingUtil util = new ReportingUtil(this.connectionString, this.login);
            //PageToOpen pto = new PageToOpen(this, util.GetReportUrl(Request.Url, "ManagementStatistics"));

            //Edge 2012-05-24 added
            PageToOpen pto = new PageToOpen(this.Page,"ManagementStatisticsViewer.aspx");
            pto.AddParameter("AutIntNo", selectedAutIntno);
            pto.AddParameter("Year", this.txtYear.Text.Trim());
            pto.AddParameter("Month", this.ddlMonth.SelectedValue);
            Helper_Web.BuildPopup(pto);
        }
    }
}


