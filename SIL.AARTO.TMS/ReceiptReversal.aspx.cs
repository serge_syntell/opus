using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// Defines a page for setting up the relationships between ledger accounts and charge types, and between users and 
    /// bank accounts so that they can act as cashiers
    /// </summary>
    public partial class ReceiptReversal : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private int rctIntNo = 0;
        private Cashier cashier = null;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "ReceiptReversal.aspx";
        //protected string thisPage = "Receipts: Reversal of receipt transaction";
        protected string rctNumber = string.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            int userAccessLevel = userDetails.UserAccessLevel;
            //int 
            this.login = userDetails.UserLoginName;
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            BankAccountDB bankAccount = new BankAccountDB(connectionString);
            this.cashier = bankAccount.GetUserBankAccount(int.Parse(Session["userIntNo"].ToString()));

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (this.Session["RctIntNo"] == null)
            {
                string error = (string)GetLocalResourceObject("strError");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("lblPageName.Text"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            this.rctIntNo = (int)this.Session["rctIntNo"];

            this.autIntNo = Convert.ToInt32(this.Session["AutIntNo"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                this.chkAreYouSure.Visible = false;
                this.lblSelectedCashbox.Visible = false;
                this.panelGeneral.Visible = true;
                this.pnlConfirm.Visible = false;
                this.pnlConfirmCashbox.Visible = false;
                this.GetReceiptDetails();

                ViewState["AreYouSure"] = null;
            }
            else
            {
                if (ViewState["AreYouSure"] != null)
                    lblSelectedCashbox.Text = ViewState["AreYouSure"].ToString();
            }
        }

        private void GetReceiptDetails()
        {
            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            SqlParameter totalCount;    //2013-04-09 add by Henry for pagination
            SqlDataReader reader = db.GetReceiptDetailsForReversal(this.rctIntNo, grdReceiptPager.PageSize, grdReceiptPager.CurrentPageIndex, out totalCount);

            if (!reader.HasRows)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                reader.Close();
                return;
            }

            this.lblError.Text = string.Empty;
            this.grdReceipt.DataSource = reader;

            this.grdReceipt.DataKeyNames = new string[] { "RctIntNo", "RTIntNo", "ChgIntNo", "NotIntNo", "CBIntNo", "RTAmount"};
            this.grdReceipt.DataBind();
            this.grdReceipt.Visible = true;
            grdReceiptPager.RecordCount = (int)(totalCount.Value == DBNull.Value ? 0 : totalCount.Value);

            this.panelGeneral.Visible = true;
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    if (this.pnlMainMenu.Visible)
        //    {
        //        this.btnOptionHide.Text = "Show Menu";
        //        this.pnlMainMenu.Visible = false;
        //    }
        //    else
        //    {
        //        this.btnOptionHide.Text = "Hide Menu";
        //        this.pnlMainMenu.Visible = true;
        //    }
        //}

        protected void grdReceipt_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void grdCashBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //dls 080214 - new complexity with mutliple payment types on the same receipt - have to check that rules apply to all receipt tran cash types
            
            this.lblError.Visible = false;
            int cbIntNo = (int)this.grdCashBox.SelectedDataKey[0];

            //CashType cashType = (CashType)Enum.Parse(typeof(CashType), ((Label)this.grdReceipt.Rows[row].FindControl("lblCashType")).Text, true);

            //dls 071213 - if the receipt being reversed was cashtype=Cash and (it captured today or the cashbox has not been closed since the receipt was captured),
            //             it must be reversed against the same cash box

            string rctNo = string.Empty;

            foreach (GridViewRow row in grdReceipt.Rows)
            {
                rctNo = ((Label)row.FindControl("lblRctNumber")).Text;

                CashType cashType = (CashType)Enum.Parse(typeof(CashType), ((Label)row.FindControl("lblCashType")).Text, true);

                if (cashType.Equals(CashType.Cash))
                {
                    int rctCBIntNo = 0;
                    DateTime cbDateOpened = DateTime.MinValue;
                    DateTime cbDateClosed = DateTime.MinValue;
                    DateTime rctCaptureDate = DateTime.MinValue;

                    if (ViewState["rctCBIntNo"] != null)
                        rctCBIntNo = Convert.ToInt32(ViewState["rctCBIntNo"]);

                    if (ViewState["cbDateOpened"] != null)
                        cbDateOpened = Convert.ToDateTime(ViewState["cbDateOpened"]);

                    if (ViewState["cbDateClosed"] != null)
                        cbDateClosed = Convert.ToDateTime(ViewState["cbDateClosed"]);

                    if (ViewState["rctCaptureDate"] != null)
                        rctCaptureDate = Convert.ToDateTime(ViewState["rctCaptureDate"]);

                    bool isToday = false;
                    bool cashboxClosed = false;

                    if (rctCaptureDate != DateTime.MinValue)
                    {
                        //is the capture date of the original receipt today?
                        isToday = (rctCaptureDate.Year == DateTime.Today.Year && rctCaptureDate.Month == DateTime.Today.Month && rctCaptureDate.Day == DateTime.Today.Day);

                        //check if the cashbox was closed after the receipt was captured: cbCashBoxDate >= rctCaptureDate
                        //  and prior to the attempted reversal: cbCashBoxDate <= DateTime.Now()

                        //is the cashbox closed?
                        if (cbDateClosed != DateTime.MinValue)
                        {
                            cashboxClosed = true;
                        }
                        else
                        {
                            //the cashbox is still open - was it opened after the receipt was captured?
                            if (cbDateOpened > rctCaptureDate)
                                cashboxClosed = true;
                        }
                    }

                    //reversal is either on the same day OR the cashbox has not been closed ==> they have to use the same cashbox
                    if (isToday || !cashboxClosed)
                    {
                        if (rctCBIntNo == 0)
                        {
                            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                            this.lblError.Visible = true;
                            return;
                        }
                        else if (rctCBIntNo != cbIntNo)
                        {
                            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                            this.lblError.Visible = true;
                            return;
                        }
                    }
                }
            }

            this.lblSelectedCashbox.Text = string.Format((string)GetLocalResourceObject("lblError.Text3") + "</br>",
                rctNo,          //this.grdReceipt.Rows[0].Cells[1].Text,
                this.grdCashBox.SelectedRow.Cells[0].Text);

            this.pnlConfirmCashbox.Visible = true;
            this.chkAreYouSure.Visible = true;
            this.chkAreYouSure.Checked = false;
            this.lblSelectedCashbox.Visible = true;

            this.lblSelectedCashbox.Text += ViewState["AreYouSure"].ToString();

            //ViewState["AreYouSure"] = this.lblSelectedCashbox.Text;

            this.btnConfirm.Visible = true;
            this.btnConfirm.Enabled = chkAreYouSure.Checked;
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            decimal amount;
            if (!decimal.TryParse(this.txtCashAmount.Text, out amount))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                return;
            }
            if (amount < 1)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                return;
            }

            //this.ProcessReversal(amount, 0, string.Empty, string.Empty, null);
            this.ProcessReversal();
           
        }

        //private void ProcessReversal(decimal amount, int number, string person, string institution, DateTime? date)
        private void ProcessReversal()
        {
            //dls 080318 - they are not allowed to change the amount, so what's the point of checking it?

            //decimal total = 0;
            //foreach (DataKey key in this.grdReceipt.DataKeys)
            //    total += decimal.Parse(key[5].ToString());

            //if (amount != total)
            //{
            //    this.lblError.Text = "The amount you have entered for the reversal is not the same as the original receipt.";
            //    return;
            //}

            int cbIntNo = 0;
            string errMessage = string.Empty;
            string receiptList = string.Empty;
            StringBuilder sb = new StringBuilder(string.Empty);

            sb = new StringBuilder("<root>");

            foreach (GridViewRow row in grdReceipt.Rows)
            {
                string cashTypeString = ((Label)row.FindControl("lblCashType")).Text;
                if (cashTypeString.Length == 0)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    lblError.Visible = true;
                    return;
                }

                //dls 070719 - allow for reversal of electronic payments
                CashType cashType = (CashType)Enum.Parse(typeof(CashType), cashTypeString, true);

                if (cashType != CashType.ElectronicPayment && cashType != CashType.DirectDeposit && cashType != CashType.BankPayment)
                    cbIntNo = (int)this.grdCashBox.SelectedDataKey[0];

                int rctIntNo = Int32.Parse(((Label)row.FindControl("lblRctIntNo")).Text);
                sb.Append("<receipt rctintno=\"");
                sb.Append(rctIntNo);
                sb.Append("\"");
                sb.Append(" />");
            }

            sb.Append("</root>");
            receiptList = sb.ToString();

            CashReceiptDB db = new CashReceiptDB(this.connectionString);
            //KeyValuePair<string, int> value = db.ProcessReceiptReversal(this.rctIntNo, cbIntNo, login, amount, number, person, institution, date);

            List<Int32> receipts = new List<Int32>();

            receipts = db.ProcessReceiptReversal(receiptList, cbIntNo, login, this.autIntNo, DateTime.Now, ref errMessage);

            if (receipts[0] <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7") + errMessage;
                lblError.Visible = true;
                return;
            }

            //dls 090701 - moved the udpate of the SumComments to the SP ReceiptReversal
            ////20090531 tf Update column SumComments in Summons
            //SummonsDB summonsDb = new SummonsDB(this.connectionString);
            //foreach (GridViewRow row in grdReceipt.Rows)
            //{
            //    summonsDb.UpdateSummonsComments(Convert.ToInt32(((Label)row.FindControl("lblNotIntNo")).Text), "Case not finalized �C payment of R rrrr.cc was received on yyyy/mm/dd HH:MM (receipt # 999999)", this.login);
            //}

            //dls 070711 -To check for Authority Rule - print all receipts to Postal Receipt
            //Helper.BuildPopup(this, "ReceiptNoteViewer.aspx", "Receipt", value.Value.ToString());

            //AuthorityRulesDB ar = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails ard = ar.GetAuthorityRulesDetailsByCode(this.autIntNo, "4510", "Rule to indicate using of postal receipt for all receipts", 0, "N", "Y = Yes; N= No(default)", this.login);

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails ard = new AuthorityRulesDetails();
            ard.AutIntNo = this.autIntNo;
            ard.ARCode = "4510";
            ard.LastUser = this.login;

            DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();


            // Open a new window with the receipt report and print it
            sb  = new StringBuilder();
            foreach (int rctIntNo in receipts)
            {
                sb.Append(rctIntNo);
                sb.Append(',');
            }

            if (this.cashier == null || this.cashier.CashBoxType != CashboxType.PostalReceipts)
            {
                if (value.Value.Equals("Y"))
                    Helper_Web.BuildPopup(this, "CashReceiptTraffic_PostalReceiptViewer.aspx", "date",
                                          DateTime.Now.ToString("yyyy-MM-dd"), "CBIntNo", "0", "RCtIntNo", sb.ToString());
                else
                    Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", sb.ToString());
            }
            else
            {
                Helper_Web.BuildPopup(this, "CashReceiptTraffic_PostalReceiptViewer.aspx", "date",
                                         DateTime.Now.ToString("yyyy-MM-dd"), "CBIntNo", "0", "RCtIntNo", sb.ToString());
            }

            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");

            //Linda 2012-9-12 add for list report19
            //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.login, PunchStatisticsTranTypeList.PaymentTrafficCancel,SIL.AARTO.BLL.Utility.Printing.PunchAction.Add);           
            
            this.pnlConfirm2.Visible = false;
            this.pnlConfirm.Visible = false;
            this.pnlConfirmCashbox.Visible = false;
            
            this.panelGeneral.Visible = false;
        }
             
        protected void btnProceed_Click(object sender, EventArgs e)
        {
            if (this.grdReceipt.Rows.Count == 0)
                return;

            decimal total = 0;
            CashType cashType = CashType.Cash;
            string message = string.Empty;
            StringBuilder sb = new StringBuilder();

            string rctNumber = this.grdReceipt.Rows[0].Cells[1].Text;

            sb.Append((string)GetLocalResourceObject("lblError.Text9") + "<ul>");

            foreach (GridViewRow row in this.grdReceipt.Rows)
            {
                string notTicketNo = ((Label)row.FindControl("lblTicketNo")).Text;
                string rctNo = ((Label)row.FindControl("lblRctNumber")).Text;

                sb.Append("<li>");
                sb.Append(notTicketNo);
                sb.Append("   Rct # ");
                sb.Append(rctNo);
                sb.Append("   [");

                string cashTypeString = ((Label)row.FindControl("lblCashType")).Text;
                if (cashTypeString.Length == 0)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                    return;
                }

                sb.Append(cashTypeString);
                sb.Append("]   R ");
                
                //calculate the amount based on the total of the amount columns
                decimal amount = decimal.Parse(((Label)row.FindControl("lblAmount")).Text);
                total += amount;

                //update by Rachel 20140819 for 5337
                //sb.Append(string.Format("{0:0.00}", amount.ToString()));
                sb.Append(string.Format(CultureInfo.InvariantCulture,"{0:0.00}",amount));
                //end update by Rachel 20140819 for 5337
                sb.Append("</li>\n");

                cashType = (CashType)Enum.Parse(typeof(CashType), cashTypeString, true);

                DateTime dt;

                //dls 070719 - allow for reversal of electronic payments - go straight to entering of amount
                if (cashType != CashType.ElectronicPayment && cashType != CashType.DirectDeposit && cashType != CashType.BankPayment)
                {
                    string strReceiptDate = ((Label)row.FindControl("lblReceiptDate")).Text;

                    if (!DateTime.TryParse(strReceiptDate, out dt))
                    {
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                        return;
                    }

                    bool isToday = (dt.Year == DateTime.Today.Year && dt.Month == DateTime.Today.Month && dt.Day == DateTime.Today.Day);

                    if (!isToday && cashType != CashType.Cash)
                    {
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                        return;
                    }
                }
            }

            //display amount of receipt in text box

            //update by Rachel 20140819 for 5337
            //txtCashAmount.Text = total.ToString();
            txtCashAmount.Text = total.ToString(CultureInfo.InvariantCulture);
            //end update by Rachel 20140819 for 5337
            txtCashAmount.ReadOnly = true;

            sb.Append("</ul>");

            ViewState.Add("AreYouSure", sb.ToString());

            if (cashType != CashType.ElectronicPayment && cashType != CashType.DirectDeposit && cashType != CashType.BankPayment)
            {
                //CashboxDB db = new CashboxDB(this.connectionString);
                //DataSet ds = db.GetCashboxListDS(this.autIntNo);
                //this.grdCashBox.DataSource = ds;
                //this.grdCashBox.DataKeyNames = new string[] { "CBIntNo" };
                //this.grdCashBox.DataBind();
                BindGrdCashBox(true);   // 2013-04-09 add by Henry for pagination

                this.pnlConfirm.Visible = true;
            }
            else
            {
                this.pnlConfirmCashbox.Visible = true;
                this.btnConfirm.Visible = true;

                this.chkAreYouSure.Checked = false;
                this.chkAreYouSure.Visible = true;

                ViewState.Add("AreYouSure", (string)GetLocalResourceObject("ViewStateValue") + rctNumber.ToString() + "?<ul>" + sb.ToString());
                this.lblSelectedCashbox.Text = ViewState["AreYouSure"].ToString();
                this.lblSelectedCashbox.Visible = true;

                this.btnConfirm.Enabled = chkAreYouSure.Checked;
            }
            
        }

        // 2013-04-09 add by Henry for pagination
        private void BindGrdCashBox(bool isReload)
        {
            if (isReload)
            {
                grdCashBoxPager.RecordCount = 0;
                grdCashBoxPager.CurrentPageIndex = 1;
            }
            CashboxDB db = new CashboxDB(this.connectionString);
            int totalCount = 0;
            DataSet ds = db.GetCashboxListDS(this.autIntNo, grdCashBoxPager.PageSize, grdCashBoxPager.CurrentPageIndex, out totalCount);
            this.grdCashBox.DataSource = ds;
            this.grdCashBox.DataKeyNames = new string[] { "CBIntNo" };
            this.grdCashBox.DataBind();
            grdCashBoxPager.RecordCount = totalCount;
        }

        protected void grdReceipt_RowCreated(object sender, GridViewRowEventArgs e)
        {
            System.Data.Common.DbDataRecord s = (System.Data.Common.DbDataRecord)e.Row.DataItem;
            decimal total = 0;
                
            if (s != null)
            {
                //store the receipt cashbox, the capture date and the last available cashbox recon date
                int rctCBIntNo = Convert.ToInt32(s["CBIntNo"].ToString());
                ViewState.Add("rctCBIntNo", rctCBIntNo.ToString());

                DateTime cbDateOpened = DateTime.MinValue;

                if (s["CBDateOpened"] != null)
                {
                    bool f = DateTime.TryParse(s["CBDateOpened"].ToString(), out cbDateOpened);
                }

                ViewState.Add("cbDateOpened", cbDateOpened);

                DateTime cbDateClosed = DateTime.MinValue;

                if (s["CBDateClosed"] != null)
                {
                    bool f = DateTime.TryParse(s["CBDateClosed"].ToString(), out cbDateClosed);
                }

                ViewState.Add("cbDateClosed", cbDateClosed);

                DateTime rctCaptureDate = DateTime.MinValue;

                if (s["CaptureDate"] != null)
                {
                    bool c = DateTime.TryParse(s["CaptureDate"].ToString(), out rctCaptureDate);
                }

                ViewState.Add("rctCaptureDate", rctCaptureDate);

                total += decimal.Parse(s["Total"].ToString());
            }

            //update by Rachel 20140820 for 5337
            //txtCashAmount.Text = total.ToString();
            txtCashAmount.Text = total.ToString(CultureInfo.InvariantCulture);
            //end update by Rachel 20140820 for 5337
       }

        protected void chkAreYouSure_CheckedChanged(object sender, EventArgs e)
        {
            btnConfirm.Enabled = chkAreYouSure.Checked;

        }

        //2013-04-09 add by Henry for pagination
        protected void grdReceiptPager_PageChanged(object sender, EventArgs e)
        {
            GetReceiptDetails();
        }

        //2013-04-09 add by Henry for pagination
        protected void grdCashBoxPager_PageChanged(object sender, EventArgs e)
        {
            BindGrdCashBox(false);
        }
}
}
