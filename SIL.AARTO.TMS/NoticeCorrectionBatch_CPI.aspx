<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.NoticeCorrectionBatch_CPI" Codebehind="NoticeCorrectionBatch_CPI.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="udpBatchCorrection" runat="server">
                        <ContentTemplate>
                            <p>
                                <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>
                                <br />
                                &nbsp;</p>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                                <table border="0" class="Normal">
                                    <tr>
                                        <td style="width: 103px">
                                            <asp:Label ID="lblAuthority" runat="server" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlAuthority" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged"
                                                Width="200px">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 103px">
                                            <asp:Label ID="lblCameraUnit" runat="server" Text="<%$Resources:lblCameraUnit.Text %>"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlCameraUnitID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCameraUnitID_SelectedIndexChanged"
                                                Width="200px">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                                <br />
                                <asp:GridView ID="grdExceptions" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                                    CellPadding="2" CssClass="Normal"
                                    OnRowEditing="grdExceptions_RowEditing" ShowFooter="True">
                                    <FooterStyle CssClass="CartListHead" />
                                    <Columns>
                                        <asp:BoundField DataField="NotCameraID" HeaderText="<%$Resources:grdExceptions.HeaderText %>" />
                                        <asp:BoundField DataField="Description" HeaderText="<%$Resources:grdExceptions.HeaderText1 %>" />
                                        <asp:TemplateField HeaderText="<%$Resources:grdExceptions.HeaderText2 %>">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("OffenceDate" , "{0:yyyy-MM-dd HH:mm}") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("OffenceDate" , "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Number" HeaderText="<%$Resources:grdExceptions.HeaderText3 %>">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:CommandField EditText="<%$Resources:grdExceptions.EditText %>" HeaderText="<%$Resources:grdExceptions.HeaderText4 %>" ShowEditButton="True">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:CommandField>
                                    </Columns>
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListAlt" />
                                </asp:GridView>
                                <pager:AspNetPager id="grdExceptionsPager" runat="server" 
                                    showcustominfosection="Right" width="400px" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                      FirstPageText="|&amp;lt;" 
                                    LastPageText="&amp;gt;|" 
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                    CustomInfoStyle="float:right;"   PageSize="20" 
                                        onpagechanged="grvNoticesPager_PageChanged"  UpdatePanelId="udpBatchCorrection"></pager:AspNetPager>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label2"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
