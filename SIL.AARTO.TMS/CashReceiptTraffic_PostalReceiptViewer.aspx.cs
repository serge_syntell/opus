using System;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
//using BarcodeNETWorkShop;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF.Merger;
using Stalberg.TMS.Data.Datasets;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// represents a page that a cahier uses to reconcile their cashbox at the end of the day
    /// </summary>
    public partial class CashReceiptTraffic_PostalReceiptViewer : DplxWebForm
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

       // BarcodeNETImage barcode = null;
        ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phBarCode;
        byte[] buf;
        System.Drawing.Image barCodeImage = null;
        protected string thisPageURL = "CashReceiptTraffic_PostalReceiptViewer.aspx";
        //protected string thisPage = "Postal Receipts Viewer";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (Request.QueryString["Date"] == null || Request.QueryString["CBIntNo"] == null)
                Response.Redirect("Login.aspx");

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "PostalReceipts");
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "PostalReceipts");

            if (reportPage.Equals(""))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "PostalReceipts.rpt", "PostalReceipts", "system", "");
                reportPage = "PostalReceipts.dplx";
            }

            string path = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;

            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                //dls 081117 - we can only check that the template path exists if there is actually a template!
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            try
            {
                //all non-ST DotMatrix receipts go through here
                if (reportPage.IndexOf("DotMatrix") == -1)
                {
                    string error = string.Format((string)GetLocalResourceObject("error2"), reportPage);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }

                //    buf = merge.Draw();

                //    phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);

                //}
                else
                {
                    //ST Postal receipts goes thru here
                    ReportDocument report = new ReportDocument();
                    report.Load(path);

                    // Fill the DataSet
                    DateTime date;
                    if (!(DateTime.TryParse(Request.QueryString["Date"].ToString(), out date)))
                    {
                        string error = (string)GetLocalResourceObject("error3");
                        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                        Response.Redirect(errorURL);
                        return;
                    }

                    int cbIntNo;
                    if (!int.TryParse(Request.QueryString["CBIntNo"], out cbIntNo))
                    {
                        string error = (string)GetLocalResourceObject("error4");
                        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                        Response.Redirect(errorURL);
                        return;
                    }

                    string receipts = string.Empty;
                    if (Request.QueryString["RCtIntNo"] != null)
                        receipts = Request.QueryString["RCtIntNo"].ToString();

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = new SqlCommand("PostalReceipts_ST", new SqlConnection(this.connectionString));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    if (receipts.Length > 0 && !receipts.Equals("0"))
                        da.SelectCommand.Parameters.Add("@Receipts", SqlDbType.VarChar, 100).Value = receipts;
                    else if (cbIntNo > 0)
                        da.SelectCommand.Parameters.Add("@CBIntNo", SqlDbType.Int, 4).Value = cbIntNo;
                    da.SelectCommand.Parameters.Add("@Date", SqlDbType.DateTime, 8).Value = date;
                    da.SelectCommand.Parameters.Add("@Authority", SqlDbType.Int, 4).Value = autIntNo;

                    dsPostalReceipts_ST ds = new dsPostalReceipts_ST();
                    da.Fill(ds);
                    da.Dispose();

                    if (ds.Tables[1].Rows.Count == 0)
                    {
                        Response.Write((string)GetLocalResourceObject("strMsg"));
                        Response.End();
                        return;
                    }

                    //dls 2010-04-01 - have added it back into the SQL dataset 
                    //// Create DataColumn objects of data types.
                    //DataColumn BarcodeImages = new DataColumn("BarcodeImages");
                    //BarcodeImages.DataType = System.Type.GetType("System.Byte[]");
                    //BarcodeImages.AllowDBNull = true;
                    //ds.Tables[1].Columns.Add(BarcodeImages); 

                    // Populate barcodes
                    //BarcodeNETImage barcode = new BarcodeNETImage();
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {

                        // Jake 2011-02-25 Removed BarcodeNETImage
                        //barcode.BarcodeText = row["RctNumber"].ToString() + ":" + row["RctIntNo"].ToString();
                        //barcode.ShowBarcodeText = false;
                        //row["BarcodeImages"] = barcode.GetBarcodeBitmap(FILE_FORMAT.JPG);
                        using (MemoryStream mstr = new MemoryStream())
                        {
                            Code128Rendering.MakeBarcodeImage(row["RctNumber"].ToString() + ":" + row["RctIntNo"].ToString(), 1, 30, true).Save(mstr, System.Drawing.Imaging.ImageFormat.Jpeg);
                            row["BarcodeImages"] = mstr.GetBuffer();
                        }

                    }

                    report.SetDataSource(ds.Tables[1]);
                    ds.Dispose();

                    // Export the pdf file
                    MemoryStream ms = new MemoryStream();
                    ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat);

                    // here's a dinkum wangle !!!! 
                    // LMZ Added (2007-06-15): To check for Authority Rule - get the dot matrix printer if set
                    //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                    ////dls 070711 - changed wording on comment to make it clearer
                    //AuthorityRulesDetails ard = db.GetAuthorityRulesDetailsByCode(this.autIntNo, "4540", "Rule to get dot matrix printer for postal receipts", 0, "", "0 - no default Dot Matrix Printer (Default); 1 - Set Default Dot Matrix Printer (Enter the Printer Name in the ARString column)", this.login);

                    //20090113 SD	
                    //AutIntNo, ARCode and LastUser need to be set from here
                    AuthorityRulesDetails ard = new AuthorityRulesDetails();
                    ard.AutIntNo = this.autIntNo;
                    ard.ARCode = "4540";
                    ard.LastUser = this.login;

                    DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
                    KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                    // A value of 1 indicates its set
                    if (value.Key == 1)
                    {
                        report.PrintOptions.PrinterName = ard.ARString;
                        report.PrintToPrinter(2, true, 1, 1);
                    }
                    report.Dispose();

                    buf = ms.ToArray();
                }


                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(buf);
                Response.End();
                buf = null;
            }
            catch(Exception error)
            {
                throw error;
            }
            finally
            {
                GC.Collect();
            }
        }



        //// Jake 2011-02-25 Removed BarcodeNETImage
        public void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        {
            //ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(barcode.GetBarcodeBitmap(FILE_FORMAT.JPG)), 0, 0);
            //img.Height = 30.0F;
            //img.Width = 150.0F;
            //e.ContentArea.Add(img);

            if (barCodeImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
                    img.Height = 30.0F;
                    img.Width = 150.0F;
                    e.ContentArea.Add(img);
                }

                int generation = System.GC.GetGeneration(barCodeImage);
                barCodeImage = null;
                System.GC.Collect(generation);
            }
        }

    }
}
