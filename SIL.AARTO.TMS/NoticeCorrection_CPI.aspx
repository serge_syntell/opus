<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.NoticeCorrection_CPI" Codebehind="NoticeCorrection_CPI.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <style type="text/css">
        .CssImage
        {
            width:194px;
            height:145px;
            margin-top:20px;
            font-size:large;
            color:red;
            font-family:Verdana, Geneva, Arial, Helvetica, sans-serif;
            margin-top:5px;
            align-items:center;
        }
        .myPromptCss
        {
            font-family:Verdana, Geneva, Arial, Helvetica, sans-serif;
            font-size:11px;
            line-height:12px;
            color:gray;
            margin-top:5px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <IMG style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptReport" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptReport.Text %>"
                                        OnClick="btnOptReport_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnBatchCorrection" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnBatchCorrection.Text %>" OnClick="btnBatchCorrection_Click" />
                                   </td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                <asp:UpdatePanel ID="upd" runat="server">
                <ContentTemplate>
                    <table border="0" width="100%">
                        <tr>
                            <td valign="top" style="height: 47px; width: 688px;">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="626px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="height: 481px; width: 688px;">

                                <asp:Panel ID="pnlGeneral" runat="server" Width="100%">
                                    <table border="0">
                                        <tr>
                                            <td style="width: 100px">
                                            </td>
                                            <td style="width: 220px">
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100px">
                                                <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="190px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                    OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="220px">
                                                </asp:DropDownList></td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlNoticeList" runat="server" Width="100%">
                                    <asp:GridView ID="grvNotices" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                        CssClass="Normal" AllowPaging="False" ShowFooter="true" 
                                        onselectedindexchanging="grvNotices_SelectedIndexChanging" >
                                        <FooterStyle CssClass="CartListHead" />
                                        <Columns>
                                            <asp:BoundField DataField="NotIntNo" HeaderText="NotIntNo" Visible="False" />
                                            <asp:BoundField DataField="NotFilmNoFrameNo" HeaderText="<%$Resources:grvNotices.HeaderText %>" />
                                            <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:grvNotices.HeaderText1 %>"   />
                                            <asp:TemplateField HeaderText="<%$Resources:grvNotices.HeaderText2 %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NotiCiprusPIProcessCode" HeaderText="NotiCiprusPIProcessCode"
                                                Visible="False" />
                                            <asp:BoundField DataField="NotCiprusPIProcessDescr" HeaderText="<%$Resources:grvNotices.HeaderText3 %>" />
                                            <%--<asp:CommandField HeaderText="Edit" ShowEditButton="True">--%>
                                            <asp:CommandField HeaderText="<%$Resources:grvNotices.HeaderText4 %>" SelectText="<%$Resources:grvNotices.HeaderText4 %>" ShowSelectButton="True">
                                                <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                            </asp:CommandField>
                                        </Columns>
                                        <HeaderStyle CssClass="CartListHead" />
                                        <AlternatingRowStyle CssClass="CartListAlt" />
                                    </asp:GridView>
                                    <pager:AspNetPager id="grvNoticesPager" runat="server" 
                                    showcustominfosection="Right" width="400px" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                      FirstPageText="|&amp;lt;" 
                                    LastPageText="&amp;gt;|" 
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                    CustomInfoStyle="float:right;"   PageSize="15" 
                                        onpagechanged="grvNoticesPager_PageChanged" UpdatePanelId="upd"></pager:AspNetPager>
                                </asp:Panel>
                                <asp:Panel ID="pnlEditNotice" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td class="NormalBold">
                                            &nbsp;</td>
                                    </tr>
                                    <tr><td class="NormalBold">
                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblNoticefor.Text %>"></asp:Label> &nbsp;
                                                <asp:Label ID="lblNotFilmFrame" runat="server" CssClass="NormalBold" EnableViewState="False"></asp:Label>
                                                &nbsp; &nbsp;
                                                <asp:Label ID="lblRegNo" runat="server" CssClass="NormalBold" EnableViewState="False"></asp:Label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lblOffenceDate" runat="server" CssClass="NormalBold" EnableViewState="False"></asp:Label></td></tr>
                                    <tr>
                                        <td class="NormalBold">
                                            &nbsp;</td>
                                    </tr>
                                    <tr><td class="NormalBold">
                                        <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblErrordesc.Text %>"></asp:Label>
                                                <asp:Label ID="lblErrDescr" runat="server" Text="<%$Resources:lblErrDescr.Text %>" CssClass="NormalBold"></asp:Label>&nbsp;
                                                <asp:Label ID="lblInstruction" runat="server" CssClass="NormalRed" EnableViewState="False"></asp:Label></td>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                                
                                    <table  width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtErrField" runat="server" Width="344px" CssClass="Normal"></asp:TextBox>
                                                            <asp:Button ID="checkImage" runat="server" Text="<%$Resources:viewImage.Text %>" CssClass="NormalButton" Width="120px" Height="24px" OnClick="checkImage_Click" Visible="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label runat="server" ID="prompting" CssClass="myPromptCss" Visible="false"></asp:Label>
                                                            <asp:Label runat="server" ID="imageFullPath" CssClass="myPromptCss" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label runat="server" ID="imageErrorInfo" CssClass="CssImage" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtErrField1" runat="server" CssClass="Normal" Width="344px"></asp:TextBox>
                                                            <asp:TextBox ID="txtErrField2" runat="server" CssClass="Normal" Width="344px"></asp:TextBox>
                                                            <asp:TextBox ID="txtErrField3" runat="server" CssClass="Normal" Width="344px"></asp:TextBox><br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Image runat="server" Visible="false" CssClass="CssImage" ID="myImage" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="width: 512px; height: 79px">
                                                    <tr>
                                                        <td colspan="2" style="vertical-align: bottom; height: 32px">
                                                            <asp:Label ID="lblPostalCode1" Visible="false" runat="server" CssClass="Normal" Text="<%$Resources:lblPostalCode1.Text %>"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblPostalCode2" Visible="false" runat="Server" CssClass="NormalRed">*</asp:Label>
                                                            <asp:TextBox ID="txtAddress4" Visible="false" runat="server" CssClass="Normal" MaxLength="100" Width="70%"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="vertical-align: top; height: 36px">
                                                            <asp:Label ID="lblPostalCode3" Visible="false" runat="server" CssClass="NormalRed" Width="474px" Text="<%$Resources:lblPostalCode3 %>"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 118px">
                                                            <asp:Label ID="lblPostalCode4" Visible="false" runat="server" CssClass="Normal" Text="<%$Resources:lblPostalCode4.Text %>"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnPostalCodesA" Visible="false" runat="server" class="NormalButton" OnClick="btnPostalCodesA_Click"
                                                                Text="..." Width="18px" />&nbsp;
                                                            <asp:DropDownList ID="ddlPostalCodesA" Visible="false" runat="server" AutoPostBack="true"
                                                                    CssClass="Normal" OnSelectedIndexChanged="ddlPostalCodesA_SelectedIndexChanged"
                                                                    Width="222px">
                                                                </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 118px">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 3px" align="center">
                                                <asp:Button ID="btnProcess" runat="server" Text="<%$Resources:btnProcess.Text %>" CssClass="NormalButton" OnClick="btnProcess_Click" Width="200px" Height="24px" />
                                                <asp:Button ID="btnCancel" runat="server" Text="<%$Resources:btnCancel.Text %>" CssClass="NormalButton" OnClick="btnCancel_Click" Height="24px" Width="200px" />
                                                <asp:Button ID="btnSkip" runat="server" Text="<%$Resources:btnSkip.Text %>" CssClass="NormalButton" OnClick="btnSkip_Click" Width="200px" />&nbsp;</td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAdditionalInfo" runat="server" CssClass="NormalBold" EnableViewState="False" Width="173px" Text="<%$Resources:lblAdditionalInfo.Text %>"></asp:Label><br />
                                                <asp:TextBox ID="txtOtherDetails" runat="server" CssClass="Normal" Height="76px"
                                                    ReadOnly="True" TextMode="MultiLine" Width="344px"></asp:TextBox></td>
                                            <td align="center" style="width: 3px">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                &nbsp;
                                </ContentTemplate>
                                </asp:UpdatePanel>
                                 <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;<asp:Label
                ID="Label4" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </ProgressTemplate>
            </asp:UpdateProgress>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
