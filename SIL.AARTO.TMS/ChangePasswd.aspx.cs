using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;
using System.Web.Security;
using System.Security.Cryptography;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class ChangePasswd : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected System.Data.SqlClient.SqlDataAdapter sqlDARegions;
        protected System.Data.SqlClient.SqlConnection sqlCn;
        protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;
        protected System.Data.DataSet dsRegions;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "ChangePasswd.aspx";
        //protected string thisPage = "Change user password";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userIntNo = Convert.ToInt32(Session["userIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;

            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                txtError.Visible = false;
                pnlUserDetails.Visible = true;

                int editUserIntNo = 0;

                if (Session["editUserIntNo"] == null)
                {
                    Session["editUserIntNo"] = userIntNo;
                    editUserIntNo = userIntNo;
                }
                else
                {
                    //try
                    //{
                    if (Request.QueryString["user"] != null &&
                        Request.QueryString["user"].ToString().Equals("own"))
                    {
                        Session["editUserIntNo"] = userIntNo;
                        editUserIntNo = userIntNo;
                    }
                    else
                    {
                        editUserIntNo = Convert.ToInt32(Session["editUserIntNo"]);
                    }
                    //}
                    //catch
                    //{
                    //    editUserIntNo = Convert.ToInt32(Session["editUserIntNo"]);
                    //}
                }

                //user must have admin level access or the user may change his/her own password
                if (userAccessLevel >= 9 || userIntNo == editUserIntNo)
                    //show user details
                    LoadUserDetails(editUserIntNo, userAccessLevel);
                else
                {
                    Application["ErrorMessage"] = (string)GetLocalResourceObject("ApplicationErrorMsg");
                    Server.Transfer("Error.aspx");
                    //Server.Transfer(Session["prevPage"].ToString());
                }
            }
        }

        protected void LoadUserDetails(int editUserIntNo, int userAccessLevel)
        {
            // Obtain user details
            UserDB user = new UserDB(connectionString);

            UserDetails userDet = user.GetUserDetails(editUserIntNo);

            lblUserName.Text = userDet.UserFName + " " + userDet.UserSName;

            //session variables from DomainRules table
            if (Session["drLoginReq"] == null && Session["drEmailReq"] == null)
            {
                //Dawn 2014-01-31
                txtError.Visible = true;
                txtError.Text = txtError.Text + (string)GetLocalResourceObject("txtError.Text5");
                return;
            }

            if (Session["drLoginReq"] != null)
            {
                if (Session["drLoginReq"].ToString().Equals("Y"))
                {
                    lblUserLogin.Text = userDet.UserLoginName;
                    lblShowUserLogin.Visible = true;
                    lblUserLogin.Visible = true;
                }
                else
                {
                    lblShowUserLogin.Visible = false;
                    lblUserLogin.Visible = false;
                }
            }

            if (Session["drEmailReq"] != null)
            {
                if (Session["drEmailReq"].ToString().Equals("Y"))
                {
                    lblShowUserEmail.Visible = true;
                    lblUserEmail.Visible = true;
                    lblUserEmail.Text = userDet.UserEmail;
                }
                else
                {
                    lblShowUserEmail.Visible = false;
                    lblUserEmail.Visible = false;
                }
            }
        }

        protected void btnUpdPasswd_Click(object sender, System.EventArgs e)
        {
            //check the user password has been entered
            if (txtAddUserPasswd.Text.Trim ( ).Equals ( "" ) || textNewPassWord.Text.Trim ( ).Equals ( "" ) || txtOldPassWord.Text.Trim().Equals(""))
            {
                txtError.Visible = true;
                txtError.Text = (string)GetLocalResourceObject("txtError.Text");
                txtPasswd.Value = "0";
            }
            else
            {
                //validate password
                string text = txtAddUserPasswd.Text;
                string tetxOld = txtOldPassWord.Text;
                string textNew = textNewPassWord.Text;
                string pat = @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,10}$";

                // Compile the regular expression.
                Regex r = new Regex(pat);

                // Match the regular expression pattern against a text string.
                Match m = r.Match(text);
                Match mOldText = r.Match(tetxOld);
                Match mNewText = r.Match (textNew);

                if (!m.Success || !mOldText.Success || !mNewText.Success)
                {
                    txtError.Visible = true;
                    txtError.Text = (string)GetLocalResourceObject("txtError.Text1");
                    return;
                }
                else
                    txtError.Visible = false;

                       //call procedure to update password
                     int editUserIntNo = Convert.ToInt32 ( Session["editUserIntNo"] );
                     UserDB user = new UserDB(connectionString);
                     Stalberg.TMS.UserDetails userDetail = user.GetUserDetails(editUserIntNo);
                    //check that the re-entered password matches the first one
                    if (txtAddUserPasswd.Text.Equals(textNewPassWord.Text ) && HashPassword(txtOldPassWord.Text).Equals(userDetail.UserPassword) )
                    {                                                  
                        //reset labels
                        //txtError.Visible = false;
                        //lblUserPasswd.Text = (string)GetLocalResourceObject("lblUserPasswd.Text2");
                        ////session variables from DomainRules table
                        string loginReq = "N";
                        string emailReq = "N";

                        if (Session["drLoginReq"] != null)
                        {
                            if (Session["drLoginReq"].ToString ( ).Equals ( "Y" ))
                            {
                                loginReq = "Y";
                            }
                        }

                        if (Session["drEmailReq"] != null)
                        {
                            if (Session["drEmailReq"].ToString ( ).Equals ( "Y" ))
                            {
                                emailReq = "Y";
                            }
                        }
                        //set up encryption class
                        //Rc4Encrypt.clsRc4Encrypt rc4 = new clsRc4Encrypt();
                        //rc4.Password = "sacompconsult";
                        //rc4.PlainText = txtAddUserPasswd.Text;

                        int expiryDays = new SIL.AARTO.DAL.Services.SysParamService().GetBySpColumnName(SIL.AARTO.DAL.Entities.SysParamList.UserPasswordExpiryDays.ToString()).SpIntegerValue;

                        bool setPasswd = user.UpdateUserPasswd(editUserIntNo, lblUserLogin.Text,
                            lblUserEmail.Text,
                            HashPassword(txtAddUserPasswd.Text), loginReq, emailReq, this.loginUser, expiryDays);

                        if (setPasswd)
                        {
                            Session["editUserIntNo"] = null;

                            txtError.Visible = true;
                            txtError.Text = (string)GetLocalResourceObject("txtError.Text2");
                           
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ChangePassword, PunchAction.Change);  

                            return;

                        }
                        else
                        {
                            txtError.Visible = true;
                            txtError.Text = (string)GetLocalResourceObject("txtError.Text3");
                            //Jerry 2014-01-08 add
                            //if (string.IsNullOrEmpty(lblUserLogin.Text.Trim()) || string.IsNullOrEmpty(lblUserEmail.Text.Trim()))  -- only one of these is required
                            if (string.IsNullOrEmpty(lblUserLogin.Text.Trim()) && string.IsNullOrEmpty(lblUserEmail.Text.Trim()))
                                txtError.Text = txtError.Text + (string)GetLocalResourceObject("txtError.Text5");
                        }
                    }
                    else
                    {
                        //password has not been re-entered correctly - cancel
                        txtError.Visible = true;
                        txtError.Text = (string)GetLocalResourceObject("txtError.Text4");
                        txtAddUserPasswd.Text = string.Empty;
                        txtPasswd.Value = "0";
                        lblUserPasswd.Text = (string)GetLocalResourceObject("lblUserPasswd.Text2");
                    }
             
            }
        }

        protected string HashPassword(string plainMessage)
        {
            byte[] data = Encoding.UTF8.GetBytes(plainMessage);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                byte[] encryptedBytes = sha.TransformFinalBlock(data, 0, data.Length);
                return Convert.ToBase64String(sha.Hash);
            }
        }

        protected void txtAddUserPasswd_TextChanged ( object sender, EventArgs e )
        {

        }

        //protected void btnCancel_Click(object sender, System.EventArgs e)
        //{
        //    Server.Transfer(Session["prevPage"].ToString());
        //}

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}
    }
}
