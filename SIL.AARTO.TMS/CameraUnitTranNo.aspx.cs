using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility.Cache;
using System.Collections.Generic;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{

    public partial class CameraUnitTranNo : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;

        protected string thisPageURL = "CameraUnitTranNo.aspx";
        //protected string thisPage = "Camera Unit Transaction Number mMaintenance";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            //int 
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName;
            int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddTranNo.Visible = false;
                pnlEditTranNo.Visible = false;
                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptCopy.Visible = false;
                btnOptHide.Visible = true;

                PopulateAuthorites(autIntNo);
                PopulateCameras(autIntNo);

                if (autIntNo > 0)
                {
                    BindGrid();
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    lblError.Visible = true;
                }
            }
        }


        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorites(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        protected void BindGrid()
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.CameraUnitTranNoDB tranNoList = new Stalberg.TMS.CameraUnitTranNoDB(connectionString);

            DataSet data = tranNoList.GetCameraUnitTranNoListDS(GetAuthorityNo());
            dgTranNo.DataSource = data;
            dgTranNo.DataKeyField = "CUTIntNo";
            dgTranNo.DataBind();

            if (dgTranNo.Items.Count == 0)
            {
                dgTranNo.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
            {
                dgTranNo.Visible = true;
            }

            data.Dispose();

            pnlAddTranNo.Visible = false;
            pnlEditTranNo.Visible = false;
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlAddTranNo.Visible = true;

            txtAddAutNo.Text = GetAuthorityNo();
            txtAddTNumber.Text = "1";

            pnlEditTranNo.Visible = false;
            btnOptDelete.Visible = false;
        }

        protected void btnAddTranNo_Click(object sender, System.EventArgs e)
        {
            if (ddlAddCUTCamUnitID.SelectedIndex < 1)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            // add the new transaction to the database table
            Stalberg.TMS.CameraUnitTranNoDB tranNoAdd = new CameraUnitTranNoDB(connectionString);
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            int addCUTIntNo = tranNoAdd.AddCameraUnitTranNo(txtAddAutNo.Text,
                ddlAddCUTCamUnitID.SelectedValue.ToString(), Convert.ToInt32(txtAddTNumber.Text), loginUser);

            if (addCUTIntNo <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraUnitTransactions, PunchAction.Add);  
            }

            lblError.Visible = true;

      

            BindGrid();
        }

        protected void btnUpdateTranNo_Click(object sender, System.EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            Stalberg.TMS.CameraUnitTranNoDB tranNoUpdate = new CameraUnitTranNoDB(connectionString);

            int updTranNoIntNo = tranNoUpdate.UpdateCameraUnitTranNo(txtAutNo.Text, ddlCUTCamUnitID.SelectedValue.ToString(),
                Convert.ToInt32(txtTNumber.Text), loginUser);

            if (updTranNoIntNo.Equals("-1"))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraUnitTransactions, PunchAction.Change);  

            }

            lblError.Visible = true;

            BindGrid();
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dgTranNo.Visible = true;
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid();

            PopulateCameras(autIntNo);
        }

        private void PopulateCameras(int autIntNo)
        {
            CameraUnitDB camera = new CameraUnitDB(connectionString);
            
            SqlDataReader reader = camera.GetCameraUnitList(autIntNo);
            ddlAddCUTCamUnitID.DataSource = reader;
            ddlAddCUTCamUnitID.DataValueField = "CamUnitID";
            ddlAddCUTCamUnitID.DataTextField = "CamUnitID";
            ddlAddCUTCamUnitID.DataBind();
            ddlAddCUTCamUnitID.Items.Insert(0, (string)GetLocalResourceObject("ddlAddCUTCamUnitID.Items"));

            reader = camera.GetCameraUnitList(autIntNo);
            ddlCUTCamUnitID.DataSource = reader;
            ddlCUTCamUnitID.DataValueField = "CamUnitID";
            ddlCUTCamUnitID.DataTextField = "CamUnitID";
            ddlCUTCamUnitID.DataBind();

            reader.Close();
        }

        protected string GetAuthorityNo()
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            AuthorityDB auth = new AuthorityDB(connectionString);
            AuthorityDetails authDet = auth.GetAuthorityDetails(autIntNo);

            string autNo = authDet.AutNo.Trim();

            return autNo;
        }

        protected void PopulateTranNoTextBoxes(int CUTIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.CameraUnitTranNoDB tranNo = new Stalberg.TMS.CameraUnitTranNoDB(connectionString);

            Stalberg.TMS.CameraUnitTranNoDetails tranNoDetails = tranNo.GetCameraUnitTranNoDetails(CUTIntNo);
            if (CUTIntNo > 0)
            {
                txtTNumber.Text = tranNoDetails.CUTNextNo.ToString();
                txtAutNo.Text = GetAuthorityNo();
                pnlEditTranNo.Visible = true;
                ddlCUTCamUnitID.SelectedIndex = ddlCUTCamUnitID.Items.IndexOf(ddlCUTCamUnitID.Items.FindByValue(tranNoDetails.CamUnitID));
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                lblError.Visible = true;
                pnlEditTranNo.Visible = false;
            }
        }

        protected void dgTranNo_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dgTranNo.SelectedIndex = e.Item.ItemIndex;

            if (dgTranNo.SelectedIndex > -1)
            {
                pnlAddTranNo.Visible = false;
                Session["editCUTIntNo"] = Convert.ToInt32(dgTranNo.DataKeys[dgTranNo.SelectedIndex]);
                PopulateTranNoTextBoxes(Convert.ToInt32(Session["editCUTIntNo"]));
            }
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgTranNo.Visible.Equals(true))
            {
                //hide it
                dgTranNo.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptShow.Text");
            }
            else
            {
                //show it
                dgTranNo.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int tranIntNo = Convert.ToInt32(Session["editCUTIntNo"]);

            TranNoDB tran = new Stalberg.TMS.TranNoDB(connectionString);

            string delCUTIntNo = tran.DeleteTranNo(tranIntNo);

            if (delCUTIntNo.Equals("0"))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            }
            else if (delCUTIntNo.Equals("-1"))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            }
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraUnitTransactions, PunchAction.Delete);

            lblError.Visible = true;

            BindGrid();
        }

        protected void dgTranNo_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgTranNo.CurrentPageIndex = e.NewPageIndex;

            BindGrid();
        }

        protected void btnOptCopy_Click(object sender, EventArgs e)
        {

        }
    }
}
