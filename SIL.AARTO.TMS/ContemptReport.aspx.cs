using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    public partial class ContemptReport : Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string title = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        //protected string thisPage = "Consolidated Contempt Receipt Report";
        protected string description = String.Empty;
        protected string thisPageURL = "ContemptReport.aspx";

        private const string DATE_FORMAT = "yyyy-MM-dd";


        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            this.userIntNo = Convert.ToInt32(Session["userIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateAuthorities();
                //this.PopulateCashiers(int.Parse(this.ddlAuthority.SelectedValue));
                this.PopulateCourts();
                this.PopulateMonth();
                txtYear.Text = DateTime.Today.Year.ToString();
                chkDateRange.Checked = false;

                wdcDateTo.Enabled = false;
                wdcDateFrom.Enabled = false;
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            this.ddlAuthority.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlAuthority.Items"), "0"));
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));
            //reader.Close();
        }

        public void PopulateCourts()
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("CourtList", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //SqlParameter param = new SqlParameter("@AutIntNo", SqlDbType.Int, 4);
            //param.Value = autIntNo;
            //cmd.Parameters.Add(param);

            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            this.ddlCourt.DataSource = reader;
            this.ddlCourt.DataValueField = "CrtIntNo";
            this.ddlCourt.DataTextField = "CrtName";
            this.ddlCourt.DataBind();
            //this.ddlCashier.Items.Insert(0, new ListItem("[ All ]", "0"));

            reader.Dispose();
        }

        private void PopulateMonth()
        {
            this.ddlMonth.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlAuthority.Items"), "13"));
            this.ddlMonth.Items.Insert(1, new ListItem((string)GetLocalResourceObject("ddlMonth.Items"), "01"));
            this.ddlMonth.Items.Insert(2, new ListItem((string)GetLocalResourceObject("ddlMonth.Items1"), "02"));
            this.ddlMonth.Items.Insert(3, new ListItem((string)GetLocalResourceObject("ddlMonth.Items2"), "03"));
            this.ddlMonth.Items.Insert(4, new ListItem((string)GetLocalResourceObject("ddlMonth.Items3"), "04"));
            this.ddlMonth.Items.Insert(5, new ListItem((string)GetLocalResourceObject("ddlMonth.Items4"), "05"));
            this.ddlMonth.Items.Insert(6, new ListItem((string)GetLocalResourceObject("ddlMonth.Items5"), "06"));
            this.ddlMonth.Items.Insert(7, new ListItem((string)GetLocalResourceObject("ddlMonth.Items6"), "07"));
            this.ddlMonth.Items.Insert(8, new ListItem((string)GetLocalResourceObject("ddlMonth.Items7"), "08"));
            this.ddlMonth.Items.Insert(9, new ListItem((string)GetLocalResourceObject("ddlMonth.Items8"), "09"));
            this.ddlMonth.Items.Insert(10, new ListItem((string)GetLocalResourceObject("ddlMonth.Items9"), "10"));
            this.ddlMonth.Items.Insert(11, new ListItem((string)GetLocalResourceObject("ddlMonth.Items10"), "11"));
            this.ddlMonth.Items.Insert(12, new ListItem((string)GetLocalResourceObject("ddlMonth.Items11"), "12"));
            this.ddlMonth.SelectedIndex = 0;
            return;
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (chkDateRange.Checked)
            {
                DateTime dt;

                if (!DateTime.TryParse(wdcDateFrom.Text, out dt))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    return;
                }

                if (!DateTime.TryParse(wdcDateTo.Text, out dt))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                    return;
                }
            }
            else
            {
                if (this.ddlAuthority.SelectedValue.Equals(string.Empty))
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    return;
                }
                if ((this.ddlMonth.SelectedIndex > 0) && this.txtYear.Text == string.Empty)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    return;
                }
            }
            this.lblError.Text = string.Empty;

            int autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            int crtIntNo = int.Parse(this.ddlCourt.SelectedValue);
            if (chkDateRange.Checked)
                Helper_Web.BuildPopup(this, "ContemptReport_Viewer.aspx", "AutIntNo", ddlAuthority.SelectedValue, "CrtIntNo", crtIntNo.ToString(), "From", wdcDateFrom.Text, "To", wdcDateTo.Text);
            else
                Helper_Web.BuildPopup(this, "ContemptReport_Viewer.aspx", "AutIntNo", ddlAuthority.SelectedValue, "CrtIntNo", crtIntNo.ToString(), "Year", this.txtYear.Text, "Month", this.ddlMonth.SelectedItem.Value.ToString());
        }

        protected void chkDateRange_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkDateRange.Checked)
            {
                wdcDateFrom.Enabled = true;
                wdcDateTo.Enabled = true;
                txtYear.Enabled = false;
                ddlMonth.Enabled = false;
            }
            else
            {
                wdcDateFrom.Enabled = false;
                wdcDateTo.Enabled = false;
                txtYear.Enabled = true;
                ddlMonth.Enabled = true;

                wdcDateFrom.Text = string.Empty;
                wdcDateTo.Text = string.Empty;
            }
        }
    }
}


