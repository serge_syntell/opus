<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.ContempReport_Viewer" Codebehind="ContemptReport_Viewer.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
</head>
<body onload="self.focus();" style="margin: 0px" background="<%=backgroundImage %>" >
    <form id="form" runat="server">
    </form>
</body>
</html>
