﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stalberg.TMS.Data;
using System.Text.RegularExpressions;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class AARTO_SAPO_Recipients : System.Web.UI.Page
    {
        private enum Display
        {
            List,
            Edit,
            Delete,
            Copy
        }
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        // Protected
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "AARTO SAPO Recipients Management";
        protected string thisPageURL = "AARTO_SAPO_Recipients.aspx";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            //UserDB user = new UserDB(this.connectionString);
            UserDetails userDetails = new UserDetails();

            //int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                BindDDLReportType();
                this.bindData();
            }
        }

        private void BindDDLReportType()
        {
            AARTO_ReportTypeDB db = new AARTO_ReportTypeDB(this.connectionString);
            List<Data.AARTO_ReportType> data = db.ListAARTO_ReportTypes();
            ddlReportType.DataSource = data;
            ddlReportType.DataTextField = "Code";
            ddlReportType.DataValueField = "ID";
            ddlReportType.DataBind();

            if (this.ddlReportType.Items.Count > 0)
                this.ddlReportType.SelectedIndex = 0;
        }

        private void bindData()
        {
            this.setPanelVisibility(Display.List);

            // Get the list of AARTO_Report types and bind it to the grid
            AARTO_SAPO_RecipientDB db = new AARTO_SAPO_RecipientDB(this.connectionString);
            List<Data.AARTO_SAPO_Recipient> data = db.ListAARTO_SAPO_Recipients();
            this.gridRecipients.DataSource = data;
            this.gridRecipients.DataKeyField = "ID";
            this.gridRecipients.DataBind();

            if (data.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                this.lblError.Text = string.Empty;
            }
        }

        private void setPanelVisibility(Display display)
        {
            this.lblError.Text = string.Empty;

            this.panelEdit.Visible = false;
            this.panelDelete.Visible = false;
            this.pnlDetails.Visible = false;

            switch (display)
            {
                case Display.List:
                    this.pnlDetails.Visible = true;
                    break;
                case Display.Edit:
                    this.panelEdit.Visible = true;
                    break;
                case Display.Delete:
                    this.panelDelete.Visible = true;
                    break;
                case Display.Copy:
                    this.panelEdit.Visible = true;
                    break;
            }
        }

        //protected void btnHideMenu_Click(object sender, EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void buttonAdd_Click(object sender, EventArgs e)
        {
            this.setPanelVisibility(Display.Edit);

            this.textEmail.Text = string.Empty;
            this.ddlFunction.SelectedIndex = 0;
            this.textName.Text = string.Empty;
            this.hiddenID.Value = "0";
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            this.setPanelVisibility(Display.Delete);
        }

        protected void buttonSubmit_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.hiddenID.Value);
            Data.AARTO_SAPO_Recipient doc = new Data.AARTO_SAPO_Recipient();
            doc.ID = id;
            doc.Function = this.ddlFunction.Text;
            doc.Name = this.textName.Text.Trim();
            doc.Email = this.textEmail.Text.Trim();

            if (this.ddlReportType.SelectedIndex < 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            doc.ReportTypeID = Convert.ToInt32(this.ddlReportType.SelectedValue);

            if (string.IsNullOrEmpty(doc.Function))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            if (string.IsNullOrEmpty(doc.Name))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }
            if (string.IsNullOrEmpty(doc.Email))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                return;
            }
            else if(!Regex.IsMatch(doc.Email, @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                return;
            }

            this.lblError.Text = string.Empty;

            AARTO_SAPO_RecipientDB db = new AARTO_SAPO_RecipientDB(this.connectionString);
            if (id == 0)
            {
                if (db.InsertAARTO_SAPO_Recipient(doc, this.login) == -1)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    return;
                }
                else
                {
                    //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SAPORecipients, PunchAction.Add);  
                }
            }
            else
            {
                db.UpdateAARTO_SAPO_Recipient(doc, this.login);
                //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SAPORecipients, PunchAction.Change);  
            }

            this.bindData();
        }

        protected void gridRecipients_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            this.hiddenID.Value = e.Item.Cells[0].Text;

            if (e.CommandName.Equals("Select"))
            {
                this.setPanelVisibility(Display.Edit);

                this.ddlReportType.SelectedIndex = 0;
                this.ddlFunction.Text = e.Item.Cells[3].Text; ;
                this.textName.Text = e.Item.Cells[4].Text;
                this.textEmail.Text = e.Item.Cells[5].Text;
                this.ddlReportType.SelectedValue = e.Item.Cells[1].Text;
            }
            else if (e.CommandName.Equals("Delete"))
            {
                this.setPanelVisibility(Display.Delete);
                this.labelConfirmDelete.Text = string.Format((string)GetLocalResourceObject("lblError.Text7"), e.Item.Cells[4].Text, e.Item.Cells[2].Text);
            }
            else if (e.CommandName.Equals("Copy"))
            {
                this.setPanelVisibility(Display.Copy);
                this.ddlReportType.SelectedIndex = 0;
                this.ddlFunction.Text = e.Item.Cells[3].Text;
                this.textName.Text = e.Item.Cells[4].Text;
                this.textEmail.Text = e.Item.Cells[5].Text;
                this.ddlReportType.SelectedValue = e.Item.Cells[1].Text;
                this.hiddenID.Value = "0";
            }
        }

        protected void buttonDelete_Click1(object sender, EventArgs e)
        {
            int id = int.Parse(this.hiddenID.Value);
            AARTO_SAPO_RecipientDB db = new AARTO_SAPO_RecipientDB(this.connectionString);
            db.DeleteAARTO_SAPO_Recipient(id);
            //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SAPORecipients, PunchAction.Delete);  
            this.bindData();
        }

        protected void buttonList_Click(object sender, EventArgs e)
        {
            this.bindData();
        }
    }
}
