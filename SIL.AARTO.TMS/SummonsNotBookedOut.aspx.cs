﻿using System;
using System.Web;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;

namespace Stalberg.TMS
{
    public partial class SummonsNotBookedOut : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        byte[] buffer;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsNotBookedOut.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Retrieve the database connection string
                this.connectionString = Application["constr"].ToString();

                string SumPrintFileName = Session["SumPrintFileName"].ToString();
                Session.Remove("SumPrintFileName");
                if (string.IsNullOrEmpty(SumPrintFileName))
                    return;

                // Set domain specific variables
                General gen = new General();
                backgroundImage = gen.SetBackground(Session["drBackground"]);
                styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
                title = gen.SetTitle(Session["drTitle"]);

                string path = Server.MapPath("reports/SummonsNotBookedOut.dplx");

                DocumentLayout report = new DocumentLayout(path);
                StoredProcedureQuery query = (StoredProcedureQuery)report.GetQueryById("Query");
                query.ConnectionString = this.connectionString;
                ParameterDictionary parameters = new ParameterDictionary();

                parameters.Add("SumPrintFileName", SumPrintFileName);

                Document document = report.Run(parameters);
                buffer = document.Draw();

                Response.ClearContent();
                Response.ClearHeaders();

                Response.ContentType = "application/pdf";
                Response.BinaryWrite(buffer);

                Response.End();
            }
        }
    }
}