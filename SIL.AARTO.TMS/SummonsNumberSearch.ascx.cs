using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace Stalberg.TMS
{

    public partial class SummonsNumberSearch : System.Web.UI.UserControl
    {
        private string connectionString = String.Empty;
        private string login;
        private int userIntNo = 0;
        private string sSummonsNo = String.Empty;
        private int nAutIntNo;
  
        public event EventHandler SummonsSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();
            this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.nAutIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (this.ViewState["SummonsNumberSearch_AutIntNo"] != null)
            {
                this.nAutIntNo = (int)this.ViewState["SummonsNumberSearch_AutIntNo"];
            }
           
            if (!Page.IsPostBack)
            {
                PopulatePrefix();
                txtYear.Text = DateTime.Now.Year.ToString().Substring(2, 2);
            }
        }

        public void PopulatePrefix()
        {
            Stalberg.TMS.CourtDB cdb = new Stalberg.TMS.CourtDB(connectionString);
            SqlDataReader reader = cdb.GetCourtPrefixByAuthority(this.nAutIntNo);
            this.ddlPrefix.DataSource = reader;
            this.ddlPrefix.DataValueField = "CrtPrefix";
            this.ddlPrefix.DataTextField = "CrtPrefix";
            this.ddlPrefix.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            char cTemp = Convert.ToChar("0");

            if (txtNumber.Text.Trim().Length < 1)
                return;

            this.lblError.Text = string.Empty;

            int a1 = 0;
            int aa = 0;
            if (int.TryParse(txtNumber.Text.Trim(), out aa))
            {
                a1 = Convert.ToInt32(txtNumber.Text.Trim());
            }
            else
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                this.lblError.Visible = true;
                return;
            }

            string sHold = ddlPrefix.SelectedValue.ToString().Trim() + "/" + a1.ToString().PadLeft(6,cTemp) + "/" + txtYear.Text;

            Stalberg.TMS.SummonsDB sdb = new Stalberg.TMS.SummonsDB(connectionString);
            SqlDataReader reader = sdb.SearchSummonsNo( sHold);

            this.grdHeader.DataSource = reader;
            this.grdHeader.DataBind();
            if (grdHeader.Rows.Count >= 1)
            {
                //grdHeader.SelectedIndex = 0;
                //this.sSummonsNo = grdHeader.SelectedRow.Cells[1].Text;
                //if (this.SummonsSelected != null)
                //{
                //    this.SummonsSelected(this.sSummonsNo, EventArgs.Empty);
                //}
                this.pnlGrid.Visible = true;
            }
            else
            {
                this.pnlGrid.Visible = false;
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text1"), sHold);
                this.lblError.Visible = true;
                this.txtNumber.Text = string.Empty;
                this.SummonsSelected("", EventArgs.Empty);
            }
        }

        protected void grdHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void grdHeader_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            grdHeader.SelectedIndex = e.NewSelectedIndex;
            this.sSummonsNo = grdHeader.SelectedRow.Cells[1].Text;
            if (this.SummonsSelected != null)
            {
                this.SummonsSelected(this.sSummonsNo, EventArgs.Empty);
            }
            this.pnlGrid.Visible = false;
        }

        public string SummonsNumber
        {
            get { return sSummonsNo; }
        }

        public int AutIntNo
        {
            get { return nAutIntNo; }
            set
            {
                this.nAutIntNo = value;
                this.ViewState.Add("SummonsNumberSearch_AutIntNo", value);
                DataBind();
            }
        }

        public override void DataBind()
        {
            this.connectionString = Application["constr"].ToString();
            PopulatePrefix();
        }
    }
}
