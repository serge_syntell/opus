using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Stalberg.TMS
{

    public partial class C_Tags : System.Web.UI.UserControl {
    
		protected string title = string.Empty;
		protected string description = string.Empty;
		protected string keywords = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Page_Load(object sender, EventArgs e) 
		{
	
			General gen = new General();

			title = gen.SetTitle(Session["drTitle"]);
			description = gen.SetDescription(Session["drDescription"]);
			keywords = gen.SetKeywords(Session["drKeywords"]);
        }
    }
}
