<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.NatisStatistics" Codebehind="NatisStatistics.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">

</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">

    </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHideSched" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHideSched.Text %>" OnClick="btnOptHideSched_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHideSummary" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHideSummary.Text %>" OnClick="btnOptHideSummary_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" colspan="1" style="width: 100%">
                   
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                 <table border="0" width="568" >
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="543px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table id="Table1"  cellspacing="1" cellpadding="1" width="542" border="0">
                                        <tr>
                                            <td style="height: 21px; width: 166px;">
                                                &nbsp;
                                                <asp:RadioButton ID="rdoCurrentLAOnly" runat="server" AutoPostBack="True" Checked="True"
                                                    CssClass="NormalBold" OnCheckedChanged="rdoCurrentLAOnly_CheckedChanged" />
                                            </td>
                                            <td width="7" style="height: 21px">
                                                <asp:RadioButton ID="rdoAll" runat="server" Text="<%$Resources:rdoAll.Text %>" Width="186px"
                                                    AutoPostBack="True" CssClass="NormalBold" OnCheckedChanged="rdoAll_CheckedChanged" /></td>
                                            <td style="width: 34px; height: 21px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 21px; width: 166px;">
                                                &nbsp;</td>
                                            <td style="height: 21px" width="7">
                                            </td>
                                            <td style="width: 34px; height: 21px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="lblDateFrom" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateFrom.Text %>" Width="47px"></asp:Label>
                                               
                                                  <asp:TextBox runat="server" ID="dtpFrom" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpFrom" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="dtpFrom" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqFrom.Text %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>  </td>
                                            <td valign="top">
                                                <asp:Label ID="lblDateTo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateTo.Text %>" Width="54px"></asp:Label>
                                               
                                                <asp:TextBox runat="server" ID="dtpTo" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                TargetControlID="dtpTo" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                                &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                            ControlToValidate="dtpTo" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqFrom.Text %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>  </td>
                                            <td style="width: 34px">
                                                <asp:Button ID="btnRefresh" runat="server" CssClass="NormalButton" Font-Bold="True"
                                                    OnClick="btnRefresh_Click" Text="<%$Resources:btnRefresh.Text %>" Width="86px" /></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px; width: 166px;" valign="top">
                                                &nbsp;
                                            </td>
                                            <td style="height: 20px" width="7">
                                                &nbsp;
                                            </td>
                                            <td style="width: 34px; height: 20px">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgNaTISStatistics" Width="495px" runat="server" BorderColor="Black"
                                    GridLines="Vertical" CellPadding="4" Font-Size="8pt" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                    FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                    AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanged="dgNaTISStatistics_PageIndexChanged"
                                    PageSize="15">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="NFIntNo" HeaderText="NFIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="AutCode" HeaderText="<%$Resources:dgNaTISStatistics.HeaderText %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NFFileName" HeaderText="<%$Resources:dgNaTISStatistics.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NFSendDate" HeaderText="<%$Resources:dgNaTISStatistics.HeaderText2 %>" DataFormatString="{0:yyyy-MM-dd HH:mm}">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="NFReturnDate" HeaderText="<%$Resources:dgNaTISStatistics.HeaderText3 %>" DataFormatString="{0:yyyy-MM-dd HH:mm}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Status" HeaderText="<%$Resources:dgNaTISStatistics.HeaderText4 %>"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid><br />
                                <asp:Panel ID="pnlSummary" runat="server" Width="125px">
                                    <asp:Label ID="lblSummary" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSummary.Text %>"
                                        Width="494px"></asp:Label><asp:DataGrid ID="dgNatisSummary" Width="495px" runat="server"
                                            BorderColor="Black" GridLines="Vertical" CellPadding="4" Font-Size="8pt" ShowFooter="True"
                                            HeaderStyle-CssClass="CartListHead" FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" AutoGenerateColumns="False" AllowPaging="True"
                                            OnPageIndexChanged="dgNatisSummary_PageIndexChanged" PageSize="16">
                                            <FooterStyle CssClass="CartListFooter" />
                                            <AlternatingItemStyle CssClass="CartListItemAlt" />
                                            <ItemStyle CssClass="CartListItem" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                            <Columns>
                                                <asp:BoundColumn DataField="AutCode" HeaderText="<%$Resources:dgNatisSummary.HeaderText %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="TNFQtyDays" HeaderText="<%$Resources:dgNatisSummary.HeaderText1 %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="TNFQtyOffences" HeaderText="<%$Resources:dgNatisSummary.HeaderText2 %>"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid><table style="width: 99px">
                                            <tr>
                                                <td style="height: 23px; width: 308px;">
                                                    &nbsp;</td>
                                                <td style="height: 23px; width: 3px;" align="right">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 308px; height: 19px">
                                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblQtyOffences.Text %>"
                                                        Width="305px"></asp:Label></td>
                                                <td style="width: 3px; height: 19px" align="right">
                                                    <asp:Label ID="lblNoOfNotFound" runat="server" CssClass="NormalBold" Width="47px"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 308px">
                                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOffencesFailed.Text %>"
                                                        Width="305px"></asp:Label></td>
                                                <td style="width: 3px" align="right">
                                                    <asp:Label ID="lblNoOfFailed" runat="server" CssClass="NormalBold" Width="47px"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                </asp:Panel>
                               
                            </td>
                        </tr>
                    </table> </ContentTemplate>
                                </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
