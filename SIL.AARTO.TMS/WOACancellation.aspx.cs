using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.WOAManagement;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;
using System.Transactions;

namespace Stalberg.TMS
{
    public partial class WOACancellation : System.Web.UI.Page
    {
        // Constants
        private const string VIEWSTATE_SELECTED_WOA = "Selected WOA";
        // Fields
        private string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        private string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        //protected string thisPage = "WOA Cancellation";
        protected string thisPageURL = "WOACancellation.aspx";
        protected string title;
        protected string description;
        protected string keywords;
        private const string DATE_FORMAT = "yyyy-MM-dd";

        private PersonaType personaType = PersonaType.Accused;
        private bool usePostalReceipt = false;
        private bool Confirm
        {
            get
            {
                if (ViewState["Confirm"] != null)
                    return (bool)ViewState["Confirm"];
                else
                    return false;
            }
            set { ViewState["Confirm"] = value; }
        }

        int SumIntNo
        {
            get
            {
                if (ViewState["SumIntNo"] != null)
                    return (int)ViewState["SumIntNo"];
                else
                    return 0;
            }
            set { ViewState["SumIntNo"] = value; }
        }

        int WoaIntNo
        {
            get
            {
                if (ViewState["WoaIntNo"] != null)
                    return (int)ViewState["WoaIntNo"];
                else
                    return 0;
            }
            set { ViewState["WoaIntNo"] = value; }
        }

        decimal SentenceAmount
        {
            get
            {
                if (ViewState["SentenceAmount"] != null)
                    return (decimal)ViewState["SentenceAmount"];
                else
                    return 0;
            }
            set { ViewState["SentenceAmount"] = value; }
        }
        decimal ContemptAmount
        {
            get
            {
                if (ViewState["ContemptAmount"] != null)
                    return (decimal)ViewState["ContemptAmount"];
                else
                    return 0;
            }
            set { ViewState["ContemptAmount"] = value; }
        }

        decimal OtherAmount
        {
            get
            {
                if (ViewState["OtherAmount"] != null)
                    return (decimal)ViewState["OtherAmount"];
                else
                    return 0;
            }
            set { ViewState["OtherAmount"] = value; }
        }

        NoticeFilmType noticeFilmType = NoticeFilmType.Others;   //added by Oscar 20101014
        NoticeFilmTypes nft = new NoticeFilmTypes();

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);


            //2013-10-23 Edge added disable WOACancellation page
            //Server.Transfer("Login.aspx?Login=invalid");

            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;
            //int 
            //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                panelDetails.Visible = false;
                //this.pnlAddress.Visible = false;
                this.dtpWOADate.Text = DateTime.Today.ToString(DATE_FORMAT);
                PopulateAuthorites(autIntNo);

                //this.ddlReceivedFrom.SelectedIndex = 3;//default to accused

                //this.ddlReceivedFrom_SelectedIndexChanged(this.ddlReceivedFrom, null);

                //this.btnConfirm.Visible = false;
                //this.btnCancel.Visible = false;
            }

        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorites(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        protected void PopulateMagistrate(int crtIntNo)
        {
            CourtMagistrateService cmservice = new CourtMagistrateService();
            TList<CourtMagistrate> magList = cmservice.GetByCrtIntNo(crtIntNo);
            this.ddlMagistrate.DataSource = magList;
            this.ddlMagistrate.DataTextField = "MagistrateName";
            this.ddlMagistrate.DataValueField = "CoMaIntNo";
            this.ddlMagistrate.DataBind();

            this.ddlMagistrate.Items.Insert(0, new ListItem("Please select a magistrate", ""));
        }

        protected void BindGrid(int autIntNo, bool showNoCountMsg)
        {
            WoaCancellationDB woaCancel = new WoaCancellationDB(connectionString);
            string woaNumb = txtWOANumb.Text;
            DataSet data = woaCancel.GetWOAList(autIntNo, woaNumb);
            dgWOA.DataSource = data;
            dgWOA.DataBind();
            //Jake 2014-03-04 comment out, pnlDetail will show after user select a WOA in GridView
            this.dgWOA.Visible = !(data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0);
            if ((data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0) && showNoCountMsg)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                //panelDetails.Visible = false;
                return;
            }

            ////Jake 2013-11-15 add a function to populate magistrate here
            //int crtIntNo = Convert.ToInt32(data.Tables[0].Rows[0]["CrtIntNo"]);

            //PopulateMagistrate(crtIntNo);

            //panelDetails.Visible = true;

        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            BindGrid(autIntNo, true);
        }

        protected void btnOptHide_Click(object sender, EventArgs e)
        {
            if (dgWOA.Visible.Equals(true))
            {
                //hide it
                dgWOA.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnShowlist.Text");
            }
            else
            {
                //show it
                dgWOA.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if ((txtWOANumb.Text.Trim().Length) > 0)
            {
                //this.txtPProsecutor.Text = string.Empty;
                //this.ddlMagistrate.SelectedIndex = 0;
                this.txtReasonCancel.Text = string.Empty;
                this.lblError.Text = string.Empty;
                this.chkWithdrawCAmount.Checked = false;
                this.dtpWOADate.Text = DateTime.Today.ToString(DATE_FORMAT);

                int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                this.BindGrid(autIntNo, true);
                this.panelDetails.Visible = false;
                //this.pnlAddress.Visible = false;

                this.btnWithdrawSummonsPermanently.Enabled = true;
                this.btnWithdrawSummonsReissue.Enabled = true;
            }
            else
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
        }


        // Jake 2013-11-12 comment out
        /*
        protected void btnCancelWarrant_Click(object sender, EventArgs e)
        {
            //Jake 2011-06-07 use sumcharge status replace 850
            //Modify the progress of WOA cancellation
            //this.CancelWarrant(741);  -- Jake 2011-06-23
            //this.CancelWarrant(850);

            if (ValidateInput())
            {
                if (chkWithdrawCAmount.Checked == false)
                {
                    this.txtContemptAmount.Text = this.ContemptAmount.ToString("N");
                }
                else
                {
                    this.txtContemptAmount.Text = "0.00";
                }
                this.txtSentenceAmount.Text = this.SentenceAmount.ToString("N");
                this.txtOtherAmount.Text = this.OtherAmount.ToString("N");
                ddlReceivedFrom_SelectedIndexChanged(sender, e);

                this.pnlAddress.Visible = true;

                this.btnWithdrawSummonsPermanently.Enabled = false;
                this.btnWithdrawSummonsReissue.Enabled = false;
            }

            this.dtpReceiptDate.Text = "";
            this.btnConfirm.Visible = false;
            this.btnCancel.Visible = false;
            this.Confirm = false;
        }
        */

        protected void btnWithdrawSummonsPermanently_Click(object sender, EventArgs e)
        {
            //Jake 2011-06-07 use sumcharge status replace 835
            //Modify the progress of WOA cancellation
            if (ValidateInput())
                this.WOAExecutionofCancelWOA();
            // this.CancelWarrant(735);
            //this.CancelWarrant(835);
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            BindGrid(autIntNo, false);
        }

        protected void btnWithdrawSummonsReissue_Click(object sender, EventArgs e)
        {
            //Jake 2011-06-07 use sumcharge status replace 836
            //Modify the progress of WOA cancellation
            if (ValidateInput())
                this.CancelWarrant(736);
            //this.CancelWarrant(836);
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            BindGrid(autIntNo, false);
        }
        /* Jake 2014-03-10 comment out . no use
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Confirm = false;
            this.lblFineAmount.Visible = false;
            this.lblRevisedFineAmount.Visible = false;
            this.lblError.Visible = false;

            if (this.chkWithdrawCAmount.Checked)
            {
                this.txtContemptAmount.Text = "0.00";
            }
            else
            {
                this.txtContemptAmount.Text = this.ContemptAmount.ToString("N");
            }
            this.txtOtherAmount.Text = this.OtherAmount.ToString("N");
            this.txtSentenceAmount.Text = this.SentenceAmount.ToString("N");
        }
        */
        protected void dgWOA_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            //Jake 2014-04-14 added, we need to disabled woa cancellation button if this woa has been cancelled
            bool woaCancelled = false;
            // dls 2011-06-21 - we cannot allow re-issue of summons for S56 (NotFilmType = M)
            if (e.Item.ItemType == ListItemType.SelectedItem || e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = (DataRowView)e.Item.DataItem;
                if (row != null)
                {
                    this.noticeFilmType = nft.GetNoticeFilmType(row["notFilmType"].ToString());
                    // Jake 2011-06-23 add SumIntNo to get address detail
                    this.SumIntNo = row["SumIntNo"] == null ? 0 : Convert.ToInt32(row["SumIntNo"].ToString());

                    this.ContemptAmount = row["WOAAddAmount"] == null ? 0 : Convert.ToDecimal(row["WOAAddAmount"].ToString());
                    this.SentenceAmount = row["WOAFineAmount"] == null ? 0 : Convert.ToDecimal(row["WOAFineAmount"].ToString());

                    //this.ContemptAmount = row["SChContemptAmount"] == null ? 0 : Convert.ToDecimal(row["SChContemptAmount"].ToString());
                    //this.SentenceAmount = row["ChgRevFineAmount"] == null ? 0 : Convert.ToDecimal(row["ChgRevFineAmount"].ToString());

                    //this.OtherAmount = row["SChOtherAmount"] == null ? 0 : Convert.ToDecimal(row["SChOtherAmount"].ToString());
                    woaCancelled = row["WOACancel"] == null ? false : Convert.ToBoolean(row["WOACancel"]);

                    LinkButton linkSelect = (LinkButton)e.Item.FindControl("lnkCmdSelect");
                    LinkButton linkReverse = (LinkButton)e.Item.FindControl("lnkCmdReverse");
                    //Label lbl = (Label)e.Item.FindControl("lblReverse");
                    if (woaCancelled)
                    {
                        if (linkSelect != null)
                        {
                            linkSelect.Visible = false;
                        }
                        if (linkReverse != null)
                        {
                            linkReverse.Visible = true;
                            //lbl.Visible = false;
                        }
                    }
                    else
                    {
                        if (linkSelect != null)
                        {
                            linkSelect.Visible = true;
                        }
                        if (linkReverse != null)
                        {
                            linkReverse.Visible = false;
                            //lbl.Visible = true;
                        }
                    }

                }

                btnWithdrawSummonsReissue.Visible = noticeFilmType == NoticeFilmType.M ? false : true;
            }

        }
        /* Jake 2014-03-10 comment out . no use
        protected void btnRecordJudgementAndCancelWarrant_Click(object sender, EventArgs e)
        {

            if (ValidateInput())
            {
                if (ValidateReceiptData())
                {
                    int returnId = 0;
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {

                        if (RecordCashReceipt(out returnId))
                        {
                            if (CancelWarrant(741))
                            {
                                scope.Complete();
                            }
                        }

                    }
                    if (returnId > 0)
                    {
                        int rctIntNo = returnId;

                        StringBuilder sb = new StringBuilder();
                        sb.Append(rctIntNo.ToString()).Append(",");

                        //check if postal receipts are used for all receipts
                        this.CheckUsePostalReceipt();

                        if (usePostalReceipt)
                            Helper_Web.BuildPopup(this, "CashReceiptTraffic_PostalReceiptViewer.aspx", "date", DateTime.Now.ToString("yyyy-MM-dd"), "CBIntNo", "0", "RCtIntNo", rctIntNo.ToString());
                        else
                            Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", sb.ToString());

                        //this.btnRecordJudgementAndCancelWarrant.Visible = false;
                        //this.btnCancel.Visible = false;
                        this.pnlAddress.Visible = false;

                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CancellationOfWOA, PunchAction.Change);

                    }
                }
            }
        }
        

        protected void ddlReceivedFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlReceivedFrom.SelectedItem.Value)
            {
                case "D":       //Driver
                    this.personaType = PersonaType.Driver;
                    ShowOrHideAddress(true);
                    break;
                case "O":       //Other
                    this.personaType = PersonaType.Owner;
                    ShowOrHideAddress(true);
                    break;
                case "P":       //Proxy
                    this.personaType = PersonaType.Proxy;
                    ShowOrHideAddress(true);
                    break;
                case "X":       //Other
                    this.personaType = PersonaType.Other;
                    ShowOrHideAddress(false);
                    break;
                case "A":
                    this.personaType = PersonaType.Accused;
                    ShowOrHideAddress(true);
                    break;
            }

            ViewState.Add("PersonaType", this.personaType);

            GetAddressDetails(false);
        }
        */
        /* Jake 2014-03-10 comment out , no use
        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            Confirm = true;
            btnRecordJudgementAndCancelWarrant_Click(sender, e);
        }
        
        protected void ShowOrHideAddress(bool show)
        {
            lblName.Visible = show;
            lblAddress.Visible = show;
            lblPostalCode.Visible = show;
            txtReceivedFrom.Visible = show;
            txtAddress1.Visible = show;
            txtAddress2.Visible = show;
            txtAddress3.Visible = show;
            txtAddress4.Visible = show;
            txtAddress5.Visible = show;
            txtPoCode.Visible = show;
        }


        private bool RecordCashReceipt(out int returnID)
        {
            returnID = 0;

            if (dgWOA.Items.Count > 0)
            {
                int schIntNo = 0;
                bool isFromCancelWarrant = true;
                string rptDate = DateTime.Now.ToShortDateString();
                string errMessage = string.Empty;

                decimal contemptAmountInput = Helper.ParseMoney(this.txtContemptAmount.Text.Trim());
                decimal sentenceAmountInput = Helper.ParseMoney(this.txtSentenceAmount.Text.Trim());
                decimal otherAmountInput = Helper.ParseMoney(this.txtOtherAmount.Text.Trim());

                //contemptAmountInput = String.IsNullOrEmpty(dgWOA.Items[0].Cells[6].Text) ? 0 : Convert.ToDecimal(dgWOA.Items[0].Cells[6].Text);
                //sentenceAmountInput = String.IsNullOrEmpty(dgWOA.Items[0].Cells[7].Text) ? 0 : Convert.ToDecimal(dgWOA.Items[0].Cells[7].Text);

                schIntNo = String.IsNullOrEmpty(dgWOA.Items[0].Cells[10].Text) ? 0 : Convert.ToInt32(dgWOA.Items[0].Cells[10].Text);


                SumChargeDB db = new SumChargeDB(this.connectionString);
                returnID = db.RecordCashReceipt(sentenceAmountInput, contemptAmountInput, otherAmountInput, 0, this.loginUser, schIntNo, rptDate,
                    this.txtAddress1.Text, this.txtAddress2.Text, this.txtAddress3.Text, this.txtAddress4.Text,
                    this.txtAddress5.Text, this.txtReceivedFrom.Text, this.txtPoCode.Text, this.txtPhone.Text,
                    isFromCancelWarrant, ref errMessage);

                string error = (string)GetLocalResourceObject("lblError.Text2");

                if (returnID < 0)
                {
                    if (errMessage.Length > 0)
                    {
                        this.lblError.Text = error + errMessage;
                        this.lblError.Visible = true;
                        return false;
                    }

                    switch (returnID)
                    {
                        case -1:
                            this.lblError.Text = error + errMessage;

                            break;
                        case -2:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text3");
                            break;
                        case -3:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text4");
                            break;
                        case -4:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text5");
                            break;
                        case -5:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text6");
                            break;
                        case -6:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text7");
                            break;
                        case -7:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text8");
                            break;
                        case -8:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text9");
                            break;
                        case -9:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text10");
                            break;
                        case -10:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text11");
                            break;
                        case -11:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text12");
                            break;
                        case -12:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text13");
                            break;
                        case -13:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text5");
                            break;
                        case -14:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text14");
                            break;
                        case -15:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text15");
                            break;
                        case -16:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text16");
                            break;
                        case -17:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text17");
                            break;
                        case -18:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text18");
                            break;
                        case -19:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text19");
                            break;
                        case -20:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text20");
                            break;
                        case -21:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text21");
                            break;
                        default:
                            this.lblError.Text = error + (string)GetLocalResourceObject("lblError.Text22");
                            break;
                    }

                    this.lblError.Visible = true;
                    return false;
                }
                else
                {
                    return true;
                }

            }


            return false;
        }
        */
        private bool CancelWarrant(int chargeStatus)
        {
            bool successed = false;
            //string prosecutor = txtPProsecutor.Text.Trim();
            string magistrate = this.ddlMagistrate.SelectedItem.Text;

            string reason = txtReasonCancel.Text.Trim();

            DateTime dt;
            DateTime.TryParse(dtpWOADate.Text, out dt);

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            string autName = ddlSelectLA.SelectedItem.Text;

            if (!GetRuleWoacancellation(autIntNo))
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text23"), "[6300]", autName);

                return false;
            }

            WoaCancellationDB db = new WoaCancellationDB(this.connectionString);
            int returnValue = db.CancelWarant(this.WoaIntNo, chargeStatus, reason, dt, magistrate, this.chkWithdrawCAmount.Checked, this.loginUser);
            if (returnValue <= 0)
            {
                switch (returnValue)
                {
                    case -1:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text24");
                        break;
                    case -2:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text25");
                        break;
                    case -3:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text26");
                        break;
                    case -4:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text27");
                        break;
                    case -5:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text28");
                        break;
                    case -6:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text29");
                        break;
                    case -8:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text30");
                        break;
                    case -9:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text31");
                        break;
                    case -10:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text32");
                        break;
                    case -11:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text33");
                        break;
                    case -12:
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text34");
                        break;

                }

                this.lblError.Text += db.Error;

            }
            else
            {
                this.panelDetails.Visible = false;
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text35");
                successed = true;

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CancellationOfWOA, PunchAction.Delete);
            }
            this.lblError.Visible = true;

            return successed;
        }

        private bool WOAExecutionofCancelWOA()
        {
            bool successed = false;
            //string prosecutor = txtPProsecutor.Text.Trim();
            string magistrate = this.ddlMagistrate.SelectedItem.Text;

            string reason = txtReasonCancel.Text.Trim();

            DateTime dt;
            DateTime.TryParse(dtpWOADate.Text, out dt);

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            string autName = ddlSelectLA.SelectedItem.Text;

            //if (!GetRuleWoacancellation(autIntNo))
            //{
            //    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text23"), "[6300]", autName);

            //    return false;
            //}

            WoaCancellationDB db = new WoaCancellationDB(this.connectionString);

            //Jake 2013-11-20 add snapshet
            SIL.AARTO.BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB wesDB = new SIL.AARTO.BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB(connectionString);

            Woa woa = new WoaService().GetByWoaIntNo(this.WoaIntNo);
            //Jake 2014-11-06 added transaction
            using (TransactionScope scope = new TransactionScope())
            {
                if (woa != null)
                {
                    if (wesDB.CheckSnapshotIsCreated(woa.WoaIntNo) == false)
                    {
                        bool flag = wesDB.SetSnapshot(woa.WoaIntNo, false, this.loginUser);

                        if (flag == false)
                        {
                            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text48");
                            return false;
                        }
                    }
                    else
                    {
                        wesDB.UnLockSnapshot(woa.WoaIntNo, this.loginUser);
                    }
                }

                int returnValue = db.WOAExecutionofCancelWOA(this.WoaIntNo, reason, dt, magistrate, this.chkWithdrawCAmount.Checked, this.loginUser);
                if (returnValue <= 0)
                {
                    switch (returnValue)
                    {
                        case -1: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                            break;
                        case -2: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text47");
                            break;
                        case -3: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text25");
                            break;
                        case -4: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text45");
                            break;
                        case -5: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text27");
                            break;
                        case -6: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text29");
                            break;
                        case -7: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text26");
                            break;
                        case -8: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text28");
                            break;
                        case -9: this.lblError.Text = (string)GetLocalResourceObject("lblError.Text46");
                            break;
                    }
                }
                else
                {
                    this.panelDetails.Visible = false;
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text35");
                    successed = true;
                    scope.Complete();
                }
            }
            this.lblError.Visible = true;

            return successed;
        }

        private bool ValidateInput()
        {

            //string prosecutor = txtPProsecutor.Text.Trim();
            if (ddlMagistrate.SelectedValue == "")
            //if (prosecutor.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text36");
                return false;
            }
            string reason = txtReasonCancel.Text.Trim();
            if (reason.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text37");
                return false;
            }
            DateTime dt;
            if (!DateTime.TryParse(dtpWOADate.Text, out dt))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text38");
                return false;
            }

            if (dt.Date.Ticks > DateTime.Today.Ticks)
            {

                this.lblError.Text =
                    (string)GetLocalResourceObject("lblError.Text39");
                return false;
            }


            return true;

        }
        /* Jake 2014-03-10 comment out
        private bool ValidateReceiptData()
        {
            DateTime dt;
            string rptDate = this.dtpReceiptDate.Text;
            if (string.IsNullOrEmpty(rptDate))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text40");
                return false;
            }
            else if (!DateTime.TryParse(rptDate, out dt))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text40");
                return false;
            }
            else if (dt.CompareTo(DateTime.Today) > 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text41");

                return false;
            }

            decimal contemptAmountInput = Helper.ParseMoney(this.txtContemptAmount.Text.Trim());
            decimal sentenceAmountInput = Helper.ParseMoney(this.txtSentenceAmount.Text.Trim());

            if ((this.SentenceAmount != sentenceAmountInput) && Confirm == false)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format((string)GetLocalResourceObject("lblError.Text42") + " </br>", Math.Round(SentenceAmount, 2), Math.Round(ContemptAmount, 2), Math.Round(sentenceAmountInput, 2)));
                sb.Append((string)GetLocalResourceObject("lblError.Text43"));

                this.btnConfirm.Visible = true;
                this.btnCancel.Visible = true;
                this.lblError.Text = sb.ToString();
                Confirm = false;

                return false;
            }

            if (String.IsNullOrEmpty(txtOtherAmount.Text.Trim()))
            {
                this.lblError.Text =
                   (string)GetLocalResourceObject("lblError.Text44");
                return false;
            }

            return true;
        }
        */
        private bool GetRuleWoacancellation(int authIntNo)
        {

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();

            arDetails.AutIntNo = authIntNo;
            arDetails.ARCode = "6300";
            arDetails.LastUser = this.loginUser;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            KeyValuePair<int, string> ruleWoaCancellation = ar.SetDefaultAuthRule();
            return ruleWoaCancellation.Value.Equals("Y");
        }

        private void CheckUsePostalReceipt()
        {
            // LMZ Added (2007-03-02): To check for Authority Rule - print all receipts to Postal Receipt
            if (this.ViewState["UsePostalReceipt"] == null)
            {
                //ard = authRules.GetAuthorityRulesDetailsByCode(this.autIntNo, "4510", "Rule to indicate using of postal receipt for all receipts", 0, "N", "Y = Yes; N= No(default)", this.login);
                int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                //20090113 SD	
                //AutIntNo, ARCode and LastUser need to be set from here
                AuthorityRulesDetails ard = new AuthorityRulesDetails();
                ard.AutIntNo = autIntNo;
                ard.ARCode = "4510";
                ard.LastUser = this.loginUser;

                DefaultAuthRules authRule = new DefaultAuthRules(ard, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

                this.usePostalReceipt = ard.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ? true : false;
                this.ViewState["UsePostalReceipt"] = this.usePostalReceipt;
            }
            else
                this.usePostalReceipt = (bool)this.ViewState["UsePostalReceipt"];

        }

        /*
        private void GetAddressDetails(bool forceChange)
        {
            NoticeForPaymentDB notice = new NoticeForPaymentDB(connectionString);
            DataSet dsAddress;

            dsAddress = notice.GetSummonsAddressDetails(SumIntNo, this.personaType);

            foreach (DataRow dr in dsAddress.Tables[0].Rows)
            {
                if (dr["Persona"].ToString().Equals(this.personaType.ToString()))
                {
                    this.txtAddress1.Text = dr["Address1"].ToString();
                    this.txtAddress2.Text = dr["Address2"].ToString();
                    this.txtAddress3.Text = dr["Address3"].ToString();
                    this.txtAddress4.Text = dr["Address4"].ToString();
                    this.txtAddress5.Text = dr["Address5"].ToString();
                    this.txtPoCode.Text = dr["PoCode"].ToString();
                    this.txtReceivedFrom.Text = dr["FullName"].ToString();
                    break;
                }
            }

            if (dsAddress.Tables[0].Rows.Count == 0)
            {
                this.txtAddress1.Text = string.Empty;
                this.txtAddress2.Text = string.Empty;
                this.txtAddress3.Text = string.Empty;
                this.txtAddress4.Text = string.Empty;
                this.txtAddress5.Text = string.Empty;
                this.txtPoCode.Text = string.Empty;
                this.txtReceivedFrom.Text = string.Empty;
            }
            //store dataset back into Session
            //Session.Add("NoticeAddressDetails", dsAddress);

            if (SumIntNo > 0)
            {
                //need to re-populate Received from list, depending on whether there is a proxy/accused or not
            }
        }
        */
        void WOACancellationReversal()
        {
            string errorMessage = string.Empty;
            string woaNumber = this.txtWOANumb.Text.Trim();
            if (String.IsNullOrEmpty(woaNumber))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            SIL.AARTO.BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB wesDB = new SIL.AARTO.BLL.WOAExecutionSnapshot.WoaExecutionSnapshotDB(connectionString);

            //TList<Woa> woas = new WoaService().GetByWoaNumber(this.txtWOANumb.Text.Trim());
            Woa woa = new WoaService().GetByWoaIntNo(this.WoaIntNo);
            if (woa != null)
            {
                Summons summons = new SummonsService().GetBySumIntNo(woa.SumIntNo);
                if (woa.WoaChargeStatus != (int)ChargeStatusList.WarrantWithdrawal && (woa.WoaCancel ?? false) != true)
                {
                    this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text51"),
                        woa.WoaChargeStatus.HasValue ? ((ChargeStatusList)woa.WoaChargeStatus).ToString() : "",
                        ((ChargeStatusList)summons.SummonsStatus).ToString());
                    return;
                }

                //UserDetails userDetails = (UserDetails)Session["userDetails"];
                // userDetails.UserLoginName;
                int userIntNo = Convert.ToInt32(Session["userIntNo"]);
                bool reversalOfS72 = false;
                TList<AartoUserRole> userRoles = new SIL.AARTO.BLL.Admin.UserRoleManager().GetUserRolesByUserIntNo(userIntNo);
                foreach(AartoUserRole  r in userRoles)
                {
                    if (r.AaUserRoleName.Equals("ReversalOfS72"))
                    {
                        reversalOfS72 = true;
                        break;
                    }
                }
 
                //Jake 2014-08-28 added a parameter to identity WOA cancellation reversal or WOA execution reversal
                //WCR -> WOA cancellation reversal
                if (wesDB.TestRollBack(woa.WoaIntNo, "WCR",reversalOfS72, out errorMessage))
                {
                    bool flag = wesDB.RollBackFromSnapshot(woa.WoaIntNo, this.loginUser);
                    if (flag)
                    {
                        this.dgWOA.Visible = true;
                        this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text49"), woaNumber);
                        return;
                    }
                    else
                    {
                        this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text50"), woaNumber);
                        return;
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        this.lblError.Text = errorMessage;
                        return;
                    }
                }
            }
            else
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            this.dgWOA.Visible = false;
        }

        protected void chkWithdrawCAmount_CheckedChanged(object sender, EventArgs e)
        {
            // Jake 2014-03-10 comment out
            //if (this.chkWithdrawCAmount.Checked)
            //{
            //    this.txtContemptAmount.Text = "0.00";
            //}
            //else
            //{
            //    this.txtContemptAmount.Text = this.ContemptAmount.ToString("N");
            //}
        }

        protected void btnWOAReversal_Click(object sender, EventArgs e)
        {
            WOACancellationReversal();

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            BindGrid(autIntNo, false);
        }

        protected void dgWOA_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            this.WoaIntNo = Convert.ToInt32(e.CommandArgument == null ? "0" : e.CommandArgument);
            if (e.CommandName == "Select")
            {
                Woa woa = new WoaService().DeepLoadByWoaIntNo(this.WoaIntNo, false, SIL.AARTO.DAL.Data.DeepLoadType.IncludeChildren, new Type[] { typeof(Summons) });
                //Woa woa = new WoaService().GetByWoaIntNo(this.WoaIntNo);
                //Summons summons = new SummonsService().GetBySumIntNo(woa.SumIntNo);
                if (woa != null && woa.SumIntNoSource != null)
                {
                    PopulateMagistrate(woa.SumIntNoSource.CrtIntNo);
                    lblCurrentMsg.Text = "Summons Number: " + woa.SumIntNoSource.SummonsNo + " WOA Number: " + woa.WoaNumber;
                }
                panelDetails.Visible = true;
            }
            else if (e.CommandName == "WOACancelReverse")
            {
                WOACancellationReversal();
            }

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, false);
        }

        protected void dgWOA_ItemDataBound(object sender, DataGridItemEventArgs e)
        {

        }


    }
}