<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.Error" Codebehind="Error.aspx.cs" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="stylesheets/Error.css" rel="stylesheet" type="text/css" />
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body>
    <form id="Form1" runat="server">
    <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>

     <div class="errorDiv">
        <div class="floatLeft">
            <img src="Images/ErrorPage.gif" />
            </div>
        <div class="floatLeft infoBg">
     
    

        <table border="0" width="100%">
            <tr>
                <td>
                   
                        <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></td>
            </tr>
            
            <tr>
                <td>
                    <%--<asp:Label ID="Label2" runat="server"  Text="Error:"></asp:Label>&nbsp;--%><asp:Label ID="lblError" runat="server" CssClass="NormalRed" Text="<%$Resources:lblError.Text %>"></asp:Label>                                </td>
              
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td valign="middle">
                    <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Text="<%$Resources:lblErrorOccurred.Text %>"></asp:Label>&nbsp;&nbsp;&nbsp; <asp:Label ID="lblErrorPage" runat="server" CssClass="Normal" Text="<%$Resources:lblErrorPage.Text %>"></asp:Label>
                    &nbsp;&nbsp;&nbsp;<asp:Label ID="lblErrorPageURL" runat="server" CssClass="Normal"
                        Text="<%$Resources:lblErrorPage.Text %>"></asp:Label> </td>
              
            </tr>
            <tr>
                <td>
                    <asp:HyperLink ID="linkToHome" runat="server" Target="_parent" Text="<%$Resources:linkToHome.Text %>"></asp:HyperLink></td>
            </tr>
            <tr>
                <td>&nbsp;                </td>
            </tr>
            <tr>
                <td>
                    <%--<input visible="false" type="button" value=" Close " class="NormalButton" onclick="javascript:window.close();"
                        id="Button1" />--%>                </td>
            </tr>
        </table>

  </div>
    </div>

    </form>
</body>
</html>
