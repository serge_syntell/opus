using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class CashierTranLog : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Cashier Transaction Log";
        protected string thisPageURL = "CashierTranLog.aspx";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                PopulateAuthorites();
                //2012-3-7 linda modified into multii-language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                PopulateCashiers();

                this.dtpStartDate.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
                this.dtpEndDate.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
            }
        }

        protected void PopulateAuthorites()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(this.autIntNo.ToString()));
        }

        public void PopulateCashiers()
        {
            Cashbox_UserDB cashboxUser = new Cashbox_UserDB(this.connectionString);

            SqlDataReader reader = cashboxUser.GetCashierList();

            this.ddlCashier.DataSource = reader;
            this.ddlCashier.DataValueField = "UserIntNo";
            this.ddlCashier.DataTextField = "Cashier";
            this.ddlCashier.DataBind();
            //2012-3-7 linda modified into multii-language
            this.ddlCashier.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCashier.Items"), "0"));
        }

        protected void btnCashierTran_Click(object sender, EventArgs e)
        {
            DateTime dt;

            if (!DateTime.TryParse(this.dtpStartDate.Text, out dt))
            {
                //2012-3-7 linda modified into multii-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                return;
            }

            if (!DateTime.TryParse(this.dtpEndDate.Text, out dt))
            {
                //2012-3-7 linda modified into multii-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }

            if (Convert.ToDateTime(dtpStartDate.Text) > Convert.ToDateTime(dtpEndDate.Text))
            {
                //2012-3-7 linda modified into multii-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            string startDate = string.Format("{0:yyyy-MM-dd}", this.dtpStartDate.Text);
            string endDate = string.Format("{0:yyyy-MM-dd}", this.dtpEndDate.Text);

            Helper_Web.BuildPopup(this, "CashierTranViewer.aspx", "StartDate", startDate, "EndDate", endDate, 
                "User", ddlCashier.SelectedValue, "Cashier", ddlCashier.SelectedItem.Text, "AutIntNo", ddlSelectLA.SelectedValue);
        }
}
}
