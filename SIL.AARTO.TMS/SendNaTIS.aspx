<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SendNaTIS" Codebehind="SendNaTIS.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%" width="100%">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label2" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    <asp:Button ID="btnOptForce" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptForce.Text %>" OnClick="btnOptForce_Click"></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" colspan="1" style="width: 100%">
                    <table border="0" width="100%" height="482">
                        <tr>
                            <td valign="top" style="height: 45px">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="614px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p align="center">
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="height: 419px">
                                <table id="Table3" height="100%" cellspacing="0" cellpadding="0" width="100%" align="left"
                                    border="0">
                                    <tr valign="top">
                                        <td nowrap>
                                            <asp:Panel ID="pnlGeneral" runat="server" Width="100%" HorizontalAlign="Left">
                                                <center>
                                                    <table id="Table5" cellspacing="1" cellpadding="1" width="300" border="0">
                                                        <tr>
                                                            <td width="150" align="left" valign="middle">
                                                                <asp:Label ID="lblSelAuthority" runat="server" Width="289px" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label><br />
                                                            </td>
                                                            <td style="width: 368px">
                                                                <asp:DropDownList ID="ddlAuthority" runat="server" OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged"
                                                                    Width="250px" AutoPostBack="True" CssClass="Normal">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="150" style="height: 42px" valign="middle">
                                                                <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Height="21px" Width="289px" Text="<%$Resources:lblFilmNum.Text %>"></asp:Label></td>
                                                            <td style="height: 42px; width: 368px;" align="center" valign="middle">
                                                                <asp:DropDownList ID="ddlFilmNo" runat="server" CssClass="Normal" Width="250px" Height="24px" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="150" style="height: 42px">
                                                                <asp:Label ID="lblGetonwithit" runat="server" CssClass="NormalBold" Height="40px"
                                                                    Width="289px"></asp:Label></td>
                                                            <td style="height: 42px; width: 368px;" align="center">
                                                                <asp:Button ID="btnSendToNatis" runat="server" CssClass="NormalButton" OnClick="btnSendToNatis_Click"
                                                                    Text="<%$Resources:btnSendToNatis.Text %>" Width="132px" />&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </center>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
