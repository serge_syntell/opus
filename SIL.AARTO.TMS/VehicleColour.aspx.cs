using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility.Cache;
using System.Collections.Generic;
using System.Threading;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Services;


namespace Stalberg.TMS 
{

	public partial class VehicleColour : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
    	protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        protected string thisPageURL = "VehicleColour.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        //protected string thisPage = "Vehicle colour maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];

			loginUser = userDetails.UserLoginName;

			//int 
            //2013-12-02 Heidi changed for add all Punch Statistics Transaction(5084)
			autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddVehicleColour.Visible = false;
				pnlUpdateVehicleColour.Visible = false;

				btnOptDelete.Visible = true;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

				PopulateVehicleColours();

			}		
		}

        protected void PopulateVehicleColours()
        {
            ddlColour.Items.Clear();//Heidi 2014-03-31 added for fixing a problem that ddlColor can Repeat filling data.(5141)
            VehicleColourDB vc = new VehicleColourDB(connectionString);
            ;
            SqlDataReader reader = vc.GetVehicleColours();
            Dictionary<int, string> lookups =
                VehicleColourLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                //2013-12-11 Heidi fixed the problem that data can not be filled in ddlColour(5084)
                //int vCCode =Int32.Parse(reader["VCCode"].ToString());
                int vCIntNo = Int32.Parse(reader["VCIntNo"].ToString());
                string vCCode = reader["VCCode"].ToString();
                if (lookups.ContainsKey(vCIntNo))
                {
                    ddlColour.Items.Add(new ListItem(lookups[vCIntNo], vCCode.ToString()));
                }
            }
            //this.ddlColour .DataSource = vc.GetVehicleColours();
            //this.ddlColour.DataValueField = "VCCode";
            //this.ddlColour.DataTextField = "VCDescr";
            //this.ddlColour.DataBind();
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			this.pnlAddVehicleColour.Visible = true;
			this.pnlUpdateVehicleColour.Visible = false;


            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
 		}

		protected void btnAddVehicleColour_Click(object sender, System.EventArgs e)
		{	
			// add the new transaction to the database table
			Stalberg.TMS.VehicleColourDB toAdd = new VehicleColourDB(connectionString);
			
			int addVTIntNo = toAdd.AddVehicleColour(txtAddCode.Text, txtAddColour.Text, loginUser);
            
            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addVTIntNo, lgEntityList, "VehicleColourLookup", "VCIntNo", "VCDescr",this.loginUser);


			if (addVTIntNo <= 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleColourMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;
            PopulateVehicleColours();
            ViewState.Remove("VCCode");
		}

		protected void btnUpdateVehicleColour_Click(object sender, System.EventArgs e)
		{
	
			// add the new transaction to the database table
			Stalberg.TMS.VehicleColourDB vtUpdate = new VehicleColourDB(connectionString);
			
			int updIntNo = vtUpdate.UpdateVehicleColour( txtUpdateCode.Text, txtUpdateColour.Text, loginUser);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(updIntNo, lgEntityList, "VehicleColourLookup", "VCIntNo", "VCDescr", this.loginUser);
           
            if (updIntNo <= 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleColourMaintenance, PunchAction.Change);  
			}

			lblError.Visible = true;
            PopulateVehicleColours();
		}

    	protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
            string sVCCode = String.Empty;
            sVCCode = ViewState["VCCode"] == null ? "-1" : ViewState["VCCode"].ToString();

            if (sVCCode == "-1" )
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                lblError.Visible = true;
                return;
            }

			VehicleColourDB vc = new Stalberg.TMS.VehicleColourDB(connectionString);

			int delIntNo = vc.DeleteVehicleColour(sVCCode);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "VehicleColourLookup", "VCIntNo");


                if (delIntNo == -1)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                }
                else
                {

                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleColourMaintenance, PunchAction.Delete);  
                }
            PopulateVehicleColours();
            txtUpdateCode.Text = string.Empty;
            txtUpdateColour.Text = string.Empty;
            ViewState.Remove("VCCode"); 
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
		}

		 

        protected void ddlColour_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pnlUpdateVehicleColour.Visible = true;
            txtUpdateCode.Text = ddlColour.SelectedValue;
            txtUpdateColour.Text = ddlColour.SelectedItem.Text; 
            this.pnlAddVehicleColour.Visible = false;
            ViewState.Add("VCCode", ddlColour.SelectedValue);

            //Heidi 2014-03-31 added for fixing a problem that can not editt(5141)
            int editVCIntNo = 0;
            VehicleColourService vcService = new VehicleColourService();
            IList<SIL.AARTO.DAL.Entities.VehicleColour> vehicleColourList= vcService.GetByVcCode(txtUpdateCode.Text);
            if (vehicleColourList != null && vehicleColourList.Count > 0)
            {
                editVCIntNo = vehicleColourList[0].VcIntNo;
            }
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editVCIntNo.ToString(), "VehicleColourLookup");
            this.ucLanguageLookupUpdate.DataBind(entityList);
        }
    }
}
