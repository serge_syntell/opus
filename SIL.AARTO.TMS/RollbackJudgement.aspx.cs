﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stalberg.TMS;
using System.Data.SqlClient;

using SIL.AARTO.BLL.JudgementSnapshot;
using System.Web.UI.HtmlControls;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

public partial class RollbackJudgement : System.Web.UI.Page
{
    private string connectionString = String.Empty;
    private string login;
    private int autIntNo;

    private decimal crDefaultContempt;

    int SumIntNo
    {
        get
        {
            if (ViewState["SumIntNo"] != null)
                return (int)ViewState["SumIntNo"];
            else
                return 0;
        }
        set { ViewState["SumIntNo"] = value; }
    }

    string SummonsNo
    {
        get
        {
            if (ViewState["SummonsNo"] != null)
                return ViewState["SummonsNo"].ToString();
            else
                return String.Empty;
        }
        set { ViewState["SummonsNo"] = value; }
    }

    //private int nDays = 0;

    protected string styleSheet;
    protected string backgroundImage;
    protected string title;
    protected DateTime cDate;
    private bool usePostalReceipt = false;
    static JudgementSnapshotDB js;

    // Populate the ddl court        
    private void GetCourts()
    {
        this.cboCourt.Items.Clear();
        ListItem item;

        CourtDB db = new CourtDB(this.connectionString);
        SqlDataReader reader = db.GetAuth_CourtListByAuth(this.autIntNo);
        while (reader.Read())
        {
            item = new ListItem();
            item.Value = reader["CrtIntNo"].ToString();
            item.Text = reader["CrtDetails"].ToString();
            this.cboCourt.Items.Add(item);
        }
        reader.Close();

        item = new ListItem();
        item.Value = "0";
        item.Text = (string)GetLocalResourceObject("cboCourt.Items");
        this.cboCourt.Items.Insert(0, item);
        this.cboCourt.SelectedIndex = 0;

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        // Retrieve the database connection string
        this.connectionString = Application["constr"].ToString();

        if (js == null)
            js = new JudgementSnapshotDB(this.connectionString);

        // Get user info from session variable
        if (Session["userDetails"] == null)
            Server.Transfer("Login.aspx?Login=invalid");
        if (Session["userIntNo"] == null)
            Server.Transfer("Login.aspx?Login=invalid");
        else
            int.Parse(Session["userIntNo"].ToString());

        // Get user details
        Stalberg.TMS.UserDB user = new UserDB(connectionString);
        Stalberg.TMS.UserDetails userDetails = new UserDetails();

        int userAccessLevel = userDetails.UserAccessLevel;
        userDetails = (UserDetails)Session["userDetails"];
        this.login = userDetails.UserLoginName;
        //int 
        this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
        //ViewState["AutIntNo"] = this.autIntNo;  //Removed by Oscar , unnecessary

        // Set domain specific variables
        General gen = new General();
        backgroundImage = gen.SetBackground(Session["drBackground"]);
        styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        title = gen.SetTitle(Session["drTitle"]);

        HtmlLink link = new HtmlLink();
        link.Href = styleSheet;
        link.Attributes.Add("rel", "stylesheet");
        link.Attributes.Add("type", "text/css");
        Page.Header.Controls.Add(link);

        if (!IsPostBack)
        {
            this.GetCourts();

            this.panelTestRollback.Visible = false;

        }
    }


    protected void cboCourt_SelectedIndexChanged(object sender, EventArgs e)
    {
        int crtIntNo = int.Parse(this.cboCourt.SelectedValue);
        if (crtIntNo < 1)
        {
            return;
        }

        //// Enable the DTP
        //this.dtpCourtDate.Enabled = true;
        //this.dtpCourtDate.Value = DateTime.Today;

        // Populate the court rooms
        this.cboCourtRoom.Items.Clear();
        ListItem item;
        CourtRoomDB db = new CourtRoomDB(this.connectionString);
        //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
        SqlDataReader reader = db.GetCourtRoomListByCrtRStatusFlag(crtIntNo);//db.GetCourtRoomList(crtIntNo);

        // jake 2013-09-11 removed translate from lookup table
        this.cboCourtRoom.DataSource = reader;
        this.cboCourtRoom.DataValueField = "CrtRIntNo";
        this.cboCourtRoom.DataTextField = "CrtRoomDetails";
        this.cboCourtRoom.DataBind();

        //Heidi 2013-09-23 removed translate from lookup table
        //Dictionary<int, string> lookups =
        //    CourtRoomLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
        //while (reader.Read())
        //{
        //    int crtRIntNo = (int)reader["CrtRIntNo"];
        //    if (lookups.ContainsKey(crtRIntNo))
        //    {
        //        cboCourtRoom.Items.Add(new ListItem(lookups[crtRIntNo], crtRIntNo.ToString()));
        //    }
        //}
        //while (reader.Read())
        //{
        //    item = new ListItem();
        //    item.Value = reader["CrtRIntNo"].ToString();
        //    item.Text = reader["CrtRoomDetails"].ToString();

        //    this.cboCourtRoom.Items.Add(item);
        //}
        reader.Close();

        // Insert the dummy item
        item = new ListItem();
        item.Value = "0";
        item.Text = (string)GetLocalResourceObject("cboCourtRoomItem.Text");
        this.cboCourtRoom.Items.Insert(0, item);
        this.cboCourtRoom.SelectedIndex = 0;

        this.cboCourtRoom.Enabled = true;

        // Obtain court details
        CourtDB court = new CourtDB(this.connectionString);
        Stalberg.TMS.CourtDetails courtDetails = court.GetCourtDetails(crtIntNo);

        this.crDefaultContempt = courtDetails.CrDefaultContempt;
        ViewState.Add("CrDefaultContempt", this.crDefaultContempt);

    }

    protected void cboCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.cboCourtRoom.SelectedIndex > 0)
        {
            //BindDataGrid();
        }
    }

    void BindDataGrid(bool isReload)
    {
        this.lblError.Visible = false;

        int crtIntNo = 0; int crtrIntNo = 0;

        Int32.TryParse(this.cboCourt.SelectedValue, out crtIntNo);
        Int32.TryParse(this.cboCourtRoom.SelectedValue, out crtrIntNo);

        if (crtIntNo == 0 || crtrIntNo == 0)
        {
            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            this.lblError.Visible = true;
            return;
        }

        if (isReload)   // 2013-04-10 add by Henry for pagination
        {
            grdCourtRollPager.RecordCount = 0;
            grdCourtRollPager.CurrentPageIndex = 1;
        }

        this.SummonsNo = this.txtSummonsNo.Text.Trim();
        int totalCount = 0;
        this.grdCourtRoll.DataSource = js.GetSummonsToRollback(crtIntNo, crtrIntNo, this.SummonsNo, grdCourtRollPager.PageSize, grdCourtRollPager.CurrentPageIndex, out totalCount);
        //this.grdCourtRoll.PageIndex = pageIndex;
        this.grdCourtRoll.DataKeyNames = new string[] { "SumIntNo", "SummonsNo" };
        this.grdCourtRoll.DataBind();

        grdCourtRollPager.RecordCount = totalCount;

        if (this.grdCourtRoll.Rows.Count == 0)
        {
            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            this.lblError.Visible = true;
            return;
        }
    }

    protected void grdCourtRoll_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "RollbackSummons")
        {
            grdCourtRoll.SelectedIndex = Convert.ToInt32(e.CommandArgument);

            this.SumIntNo = (int)this.grdCourtRoll.DataKeys[this.grdCourtRoll.SelectedIndex].Values["SumIntNo"];
            this.SummonsNo = this.grdCourtRoll.DataKeys[this.grdCourtRoll.SelectedIndex].Values["SummonsNo"].ToString();

            this.lblSummonsNo.Text = SummonsNo;
            string message = string.Empty;

            //Jake 2013-09-13 changed 
            this.pnlCourtRoll.Visible = false;
            //this.grdCourtRoll.Visible = false;
            this.panelTestRollback.Visible = true;

            if (js.TestRollBack(this.SumIntNo, out message) == false)
            {
                this.lblMessage.Text = (string)GetLocalResourceObject("lblError.Text2") + message;
                this.btnReturn.Visible = true;
                this.btnRollback.Visible = false;
            }
            else
            {
                this.lblMessage.Text = (string)GetLocalResourceObject("lblError.Text3");
                this.btnRollback.Visible = true;
                this.btnReturn.Visible = true;
            }
            //Test rollback function

            //rollback
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.panelTestRollback.Visible = false;
        //this.grdCourtRoll.Visible = true;
        this.pnlCourtRoll.Visible = true;
        BindDataGrid(true);
        //PunchStats805806 enquiry ReversalOfCourtJudgements
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        this.panelTestRollback.Visible = false;
        //this.grdCourtRoll.Visible = true;
        this.pnlCourtRoll.Visible = true;
        BindDataGrid(false);
    }
    protected void btnRollback_Click(object sender, EventArgs e)
    {
        int userIntNo = Int16.Parse(Session["userIntNo"].ToString());
        SIL.AARTO.BLL.Admin.UserRoleManager userRoleManager = new SIL.AARTO.BLL.Admin.UserRoleManager();
        SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.AartoUserRole> userRoleList = userRoleManager.GetUserRolesByUserIntNo(userIntNo);

        SIL.AARTO.DAL.Entities.AartoUserRole role = userRoleList.Where(u => u.AaUserRoleId.Equals((int)SIL.AARTO.DAL.Entities.AartoUserRoleList.Supervisor)).FirstOrDefault();
        if (role == null)
        {
            this.lblMessage.Text = (string)GetLocalResourceObject("lblError.Text4");
            return;
        }
        if (js == null) js = new JudgementSnapshotDB(this.connectionString);
        if (js.RollBackFromSnapshot(this.SumIntNo, this.login))
        {
            this.btnReturn.Visible = true;
            this.btnRollback.Visible = false;
            this.lblMessage.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), this.SummonsNo);

            //Linda 2012-9-12 add for list report19
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.login, PunchStatisticsTranTypeList.CaseResult,PunchAction.Delete);
            
        }
    }

    // 2013-04-10 add by Henry for pagination
    protected void grdCourtRollPager_PageChanged(object sender, EventArgs e)
    {
        BindDataGrid(false);
    }
}