﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.ClerkOfCourtSetup" Codebehind="ClerkOfCourtSetup.aspx.cs" %>

<%@ Register Src="RepresentationOnTheFly.ascx" TagName="RepresentationOnTheFly" TagPrefix="uc2" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <script src="Scripts/Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <style type="text/css">
        .NormalButton
        {
            height: 26px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%= background %>" topmargin="0"
    rightmargin="0">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table height="85%" cellspacing="0" width="100%" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center" style="width: 182px">
                <img style="height: 1px" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label5" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnAdd" runat="server" Text="<%$Resources:btnAdd.Text %>" Width="135px" OnClick="btnAdd_Click"
                                    CssClass="NormalButton" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                            <p style="text-align: center;">
                                <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Height="40px" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pnlErrMessage" runat="server">
                            <p>
                                <asp:Label ID="lblMessage" runat="server" CssClass="NormalRed"></asp:Label>
                                </p>
                        </asp:Panel>
                        <asp:Panel ID="pnlCofCourtList" runat="server">
                            <table cellpadding="0" cellspacing="0" width="80%">
                                <tr>
                                    <td>
                                        <!-- <asp:BoundField DataField="AutIntNo" HeaderText="AutIntNo" Visible="False" /> -->
                                        <!--<asp:BoundField DataField="AutName" HeaderText="Local Authority" /> -->
                                        <asp:GridView ID="grdCofCourtList" Width="100%" runat="server" AllowPaging="False"
                                            AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" OnSelectedIndexChanged="grdCofCourtList_SelectedIndexChanged"
                                            ShowFooter="True">
                                            <FooterStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundField DataField="CofCIntNo" HeaderText="CofCIntNo" Visible="False" />
                                                <asp:BoundField DataField="CrtIntNo" HeaderText="CrtIntNo" Visible="False" />
                                                <asp:BoundField DataField="CofCName" HeaderText="<%$Resources:grdCofCourtList.HeaderText %>" />
                                                <asp:BoundField DataField="CofCSurName" HeaderText="<%$Resources:grdCofCourtList.HeaderText1 %> " />
                                                <asp:BoundField DataField="CrtName" HeaderText="<%$Resources:grdCofCourtList.HeaderText2 %> " />
                                                <asp:BoundField DataField="Contact" HeaderText="<%$Resources:grdCofCourtList.HeaderText3 %> " />
                                                <asp:CommandField HeaderText="<%$Resources:grdCofCourtList.HeaderText4 %> " ShowSelectButton="True" SelectText="<%$Resources:grdCofCourtList.HeaderText4 %>" />
                                            </Columns>
                                            <HeaderStyle CssClass="CartListHead" />
                                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                                        </asp:GridView>
                                        <pager:AspNetPager id="grdCofCourtListPager" runat="server" 
                                            showcustominfosection="Right" width="400px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;" PageSize="15" onpagechanged="grdCofCourtListPager_PageChanged"  UpdatePanelId="upd"
                                            ></pager:AspNetPager>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlClerkDetail" runat="server">
                            <table cellpadding="0" cellspacing="0" width="80%">
                                <tr>
                                    <td colspan="5" style="height: 29px">
                                        <asp:Label ID="lblTitle" runat="server" Text="<%$Resources:lblTitle.Text %>"  CssClass="SubContentHead"></asp:Label>
                                        <br />
                                    </td>
                                </tr>
                                <tr style="width: 150px">
                                    <td>
                                        <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtName" runat="server" CssClass="Normal"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCourtList" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCourtList.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="dpCourtsList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dpCourtsList_SelectedIndexChanged" CssClass="Normal">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSurname.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSurName" runat="server" CssClass="Normal"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnOK" Text="<%$Resources:btnOK.Text %>" runat="server" Width="100px" CssClass="NormalButton"
                                            OnClick="btnOK_Click" OnClientClick="return PhoneCheck();" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" OnClick="btnUpdate_Click"
                                            Text="<%$Resources:btnUpdate.Text %>" Width="100px" OnClientClick="return PhoneCheck();" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblContactno.Text %>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtContact" runat="server" CssClass="Normal"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnDelete" Text="<%$Resources:btnDelete.Text %>" runat="server" Width="100px" CssClass="NormalButton"
                                            OnClick="btnDelete_Click" OnClientClick="return confirm($('#btnDeleteText').val());" />
                                        <input type="hidden" value="<%$Resources:btnDelete.ClickTip %>" id="btnDeleteText" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <input id="hidValidateContactMsg" value="<%$Resources:message3 %>" type="hidden" runat="server"  />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
    <script>
        function PhoneCheck() {
            var strMatch = "0123456789()-+.";

            var lenM = $("#txtContact").val().length;
            var valueM = $("#txtContact").val();
            for (var i = 0; i < lenM; i++) {
                var charM = valueM.charAt(i);
                if (charM.trim() != "" && strMatch.indexOf(charM) < 0) {
                    alert($("#hidValidateContactMsg").val());
                    return false;
                }
            }
          
            return true;
        }

    </script>
</body>
</html>
