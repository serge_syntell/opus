﻿<%@ Control Language="C#" CodeBehind="UCLanguageLookup.ascx.cs" Inherits="SIL.AARTO.TMS.DynamicData.FieldTemplates.UCLanguageLookup" %>

<asp:GridView ID="gvLookUP" runat="server" AutoGenerateColumns="False" 
     ShowHeader="False" GridLines="None" BackColor="#C0C0C0" Width="350" CssClass="table_lookup" style="margin-right: 0px">
    <Columns>
    <asp:TemplateField Visible="false">
            <ItemTemplate>
                <asp:Label ID="lblLookupId" runat="server" Text='<%# Eval("LookUpId") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="false">
            <ItemTemplate>
                <asp:Label ID="lblLsCode" runat="server" Text='<%# Eval("LsCode") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="LsDescription" ControlStyle-Width="90"/>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:TextBox ID="txtLookupValue" runat="server" 
                    Text='<%# Eval("LookupValue") %>' Height="21px" Width="260px" CssClass='<%# Eval("LsCode") %>'></asp:TextBox>
                   <span style="color:red;display:none;font-size:12px" title="Required Field">*</span>
<%--               <asp:RequiredFieldValidator ID="RFVLanguage" runat="server" ErrorMessage="*" ControlToValidate="txtLookupValue" Font-Size="Small" EnableClientScript="false"></asp:RequiredFieldValidator>--%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
 


