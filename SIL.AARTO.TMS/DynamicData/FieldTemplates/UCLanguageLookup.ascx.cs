﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Web.DynamicData;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Admin;

namespace SIL.AARTO.TMS.DynamicData.FieldTemplates
{
    public partial class UCLanguageLookup : System.Web.UI.UserControl
    { 
        List<LanguageLookupEntity> entityListReturn = new List<LanguageLookupEntity>();
        protected void Page_Load(object sender, EventArgs e)
        {            
        }
        
        public void DataBind(List<LanguageLookupEntity> entityList)
        {
            this.gvLookUP.DataSource = entityList;
            this.gvLookUP.DataBind();
        }

        public List<LanguageLookupEntity> Save()
        {
            GetLookupList();
            return entityListReturn;
        }

        public void GetLookupList() {
            for (int i = 0; i < gvLookUP.Rows.Count; i++) {
                LanguageLookupEntity entity = new LanguageLookupEntity();
                for (int m = 0; m < gvLookUP.Rows[i].Cells[0].Controls.Count; m++)
                {
                    Control c = gvLookUP.Rows[i].Cells[0].Controls[m];
                    if (c is Label)
                        entity.LookUpId = ((Label)c).Text;
                }
                for (int m = 0; m < gvLookUP.Rows[i].Cells[1].Controls.Count; m++)
                {
                    Control c = gvLookUP.Rows[i].Cells[1].Controls[m];
                    if (c is Label)
                        entity.LsCode = ((Label)c).Text;
                }    

                entity.LsDescription = gvLookUP.Rows[i].Cells[2].Text;

                for (int m = 0; m < gvLookUP.Rows[i].Cells[3].Controls.Count; m++)
                {
                    Control c = gvLookUP.Rows[i].Cells[3].Controls[m];
                    if (c is TextBox)
                        entity.LookupValue = ((TextBox)c).Text;
                }

                //entityListReturn.Add(entity);
                // 2014-04-29 changed by Heidi If one language have value can be set to ""
                //if (!string.IsNullOrEmpty(entity.LookupValue))
                //{// 2012-12-25 Added by Nancy for detect whether translation is null
                    entityListReturn.Add(entity);
                //}
            }
        }


    }
}
