using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page where a synopsis of offences can be displayed as the result of a search
    /// </summary>
    public partial class NoticeReprint_Search : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string login;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "NoticeReprint_Search.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "View notice reprint details";

        private const string DATE_FORMAT = "yyyy-MM-dd";
        // 20120807 Nick add for report engine
        protected bool isIBMPrinter = false;
        protected bool hasHWONotice = false;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            // 20120807 Nick added for report engine
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlResults.Visible = false;
                this.pnlReprintResults.Visible = false;
                this.TicketNumberSearch1.AutIntNo = autIntNo;
            }
            else
            {
                BindGrid(false);
            }

            //this.TicketNumberSearch1.Focus();
            txtTicketNo.Focus(); 
        }

        private void BindGrid(bool isReload)
        {
            this.pnlReprintResults.Visible = false;

            NoticeReportDB notice = new NoticeReportDB(this.connectionString);
            this.pnlResults.Visible = false;
            hasHWONotice = false;

            // Select which data set to generate
            NoticeQueryCriteria criteria = this.GetQueryCriteria();
            if (criteria == null)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            if (isReload)//2013-04-09 add by Henry for pagination
            {
                grvOffenceListPager.RecordCount = 0;
                grvOffenceListPager.CurrentPageIndex = 1;
            }

            int totalCount = 0;     //2013-04-09 add by Henry for pagination
            // Bind the data
            DataSet ds = notice.GetNoticeSearchDS(criteria.AutIntNo, criteria.ColumnName, criteria.Value, criteria.DateSince, criteria.MinStatus
                , grvOffenceListPager.PageSize, grvOffenceListPager.CurrentPageIndex, out totalCount);//2013-04-09 add by Henry for pagination
            grvOffenceList.DataSource = ds.Tables[1];
            //grvOffenceList.DataKeyNames = new string[] { "NotIntNo", "Representation" };
            grvOffenceList.DataKeyNames = new string[] { "NotIntNo" };
            grvOffenceList.DataBind();
            grvOffenceListPager.RecordCount = totalCount;

            // Check if there are any results
            if (grvOffenceList.Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else if (isIBMPrinter && hasHWONotice)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("errorMsg_Print");
                this.pnlResults.Visible = true;
            }
            else
            {
                lblError.Visible = false;
                this.pnlResults.Visible = true;
            }
        }

        private void BindGrid_Reprints(int notIntNo, bool isReload)
        {
            NoticeReprintDB NoticeReprint = new NoticeReprintDB(this.connectionString);
            this.pnlReprintResults.Visible = false;

            if (isReload)//2013-04-09 add by Henry for pagination
            {
                grvNoticeReprintPager.RecordCount = 0;
                grvNoticeReprintPager.CurrentPageIndex = 1;
            }

            int totalCount = 0; //2013-04-09 add by Henry for pagination
            // Bind the data
            DataSet ds = NoticeReprint.GetNoticeReprintDetails(notIntNo, grvNoticeReprintPager.PageSize, grvNoticeReprintPager.CurrentPageIndex, out totalCount);
            grvNoticeReprint.DataSource = ds.Tables[1];
            grvNoticeReprint.DataKeyNames = new string[] { "NotIntNo", "AutIntNo", "NRIntNo" };
            grvNoticeReprint.DataBind();
            grvNoticeReprintPager.RecordCount = totalCount;

            // Check if there are any results
            if (grvNoticeReprint.Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            }
            else
                this.pnlReprintResults.Visible = true;
        }
        
        private int CheckMinimumStatusRule()
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4605";
            //rule.ARComment = "Default = 255 (posted).";
            //rule.ARDescr = "Status to start showing notices on enquiry screen";
            //rule.ARNumeric = 255;
            //rule.ARString = string.Empty;
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);
            
            // SD: 20090108
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "4605";
            rule.LastUser = this.login;
            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            return value.Key; //rule.ARNumeric;
        }

        private NoticeQueryCriteria GetQueryCriteria()
        {
            // Check the authority rule for minimum charge status
            int minStatus = this.CheckMinimumStatusRule();

            if (!txtRegNo.Text.Trim().Equals(string.Empty))
                return new NoticeQueryCriteria(this.autIntNo, "RegNo", txtRegNo.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
            else if (!txtTicketNo.Text.Trim().Equals(string.Empty))
                return new NoticeQueryCriteria(this.autIntNo, "TicketNo", txtTicketNo.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
            else if (!txtIDNo.Text.Trim().Equals(string.Empty))
                return new NoticeQueryCriteria(this.autIntNo, "IDNo", txtIDNo.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
            else if (!txtSurname.Text.Equals(string.Empty) || !txtInitials.Text.Equals(string.Empty))
                return new NoticeQueryCriteria(this.autIntNo, "Names", txtInitials.Text + "*" + txtSurname.Text, this.dtpSince.Text, minStatus);
            else
                return null;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //PunchStats805806 enquiry ReprintNotices
            this.BindGrid(true);
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            NoticeQueryCriteria criteria = this.GetQueryCriteria();
            if (criteria == null)
                return;

            Session.Add("NoticeQueryCriteria", criteria);
            Helper_Web.BuildPopup(this, "ViewOffence_Report.aspx", null);
            BindGrid(true);
        }
                
        protected void grvOffenceList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //// PDF
            //GridViewRow row = grvOffenceList.Rows[e.NewEditIndex];
            //string ticketNo = row.Cells[1].Text;
            //if (ReversibleNotice.IsReversible(ticketNo))
            //{
            //    this.lblError.Text = string.Format("{0} is a dummy notice that has been created for a Suspense Account payment and it has no details to view.", ticketNo);
            //    return;
            //}

            //int notIntNo = (int)this.grvOffenceList.DataKeys[e.NewEditIndex].Value;
            //this.Session.Add("notIntNo", notIntNo);
            //Helper.BuildPopup(this, "ViewOffenceDetail_Report.aspx", null);

        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtTicketNo.Text = TicketNumberSearch1.TicketNumber;
            //PunchStats805806 enquiry ReprintNotices
            BindGrid(true);
        }

        public void EasyPayNoticeSelected(object sender, EventArgs e)
        {
            this.txtTicketNo.Text = EasyPayNumber.EasyPayReturn;
            //PunchStats805806 enquiry ReprintNotices
            BindGrid(true);
        }

        protected void grvOffenceList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            DataRowView s = (DataRowView)e.Row.DataItem;

            if (s != null)
            {
                if (isIBMPrinter && s["PrintFile"].ToString().Length >= 7 && s["PrintFile"].ToString().Substring(0, 7) == "HWO-1ST")
                {
                    e.Row.Cells[10].Enabled = false;
                    hasHWONotice = true;
                }
            }
        }

        protected void grvOffenceList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string sValue = e.CommandName;

            grvOffenceList.SelectedIndex = Convert.ToInt32(e.CommandArgument);
            int nRow = grvOffenceList.SelectedIndex;

            //int notIntNo = (int)this.grvOffenceList.DataKeys[nRow].Value;
            int notIntNo = (int)this.grvOffenceList.DataKeys[nRow].Values["NotIntNo"];

            this.Session.Add("notIntNo", notIntNo);

            string sTicketNo = grvOffenceList.Rows[nRow].Cells[1].Text;

            BindGrid_Reprints(notIntNo, true);
        }

        protected void grvNoticeReprint_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string sValue = e.CommandName;

            grvNoticeReprint.SelectedIndex = Convert.ToInt32(e.CommandArgument);
            int nRow = grvNoticeReprint.SelectedIndex;

            //int notIntNo = (int)this.grvOffenceList.DataKeys[nRow].Value;
            int notIntNo = (int)this.grvNoticeReprint.DataKeys[nRow].Values["NotIntNo"];
            int autIntNo = (int)this.grvNoticeReprint.DataKeys[nRow].Values["AutIntNo"];
            int NRIntNo = (int)this.grvNoticeReprint.DataKeys[nRow].Values["NRIntNo"];

            this.Session.Add("notIntNo", notIntNo);
            this.Session.Add("NRIntNo", NRIntNo);
            this.Session.Add("printAutIntNo",autIntNo);

            //string sTicketNo = grvNoticeReprint.Rows[nRow].Cells[1].Text;
            string sTicketNo = grvNoticeReprint.Rows[nRow].Cells[3].Text;

            if (sValue.Equals("Select"))
            {
                //PunchStats805806 enquiry ReprintNotices
                // PDF
                //Need to pass thtough the NRIntoNo for notice reprints
                 Helper_Web.BuildPopup(this, "FirstNoticeViewer.aspx", "printfile", "-1");
            }
        }

        //2013-04-09 add by Henry for pagination
        protected void grvOffenceListPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid(false);
        }

        //2013-04-09 add by Henry for pagination
        protected void grvNoticeReprintPager_PageChanged(object sender, EventArgs e)
        {
            int notIntNo = (int)this.grvOffenceList.DataKeys[grvOffenceList.SelectedIndex].Values["NotIntNo"];
            BindGrid_Reprints(notIntNo, false);
        }

         
}
}
