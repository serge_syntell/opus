﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="WOAPrintDetail" Codebehind="WOAPrintDetail.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script src="Scripts/Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>

</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="form1" runat="server">
    <div>
        <p align="center">
            <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
        <p align="center">
            <asp:Label ID="Label1" runat="server" Text="<%$Resources:lblPfileName.Text %>"></asp:Label><asp:Label ID="lblPrintFileName" runat="server"></asp:Label></p>
        <table width="95%" align="center">
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DataGrid ID="dgWoaPrintDetail" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                        AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                        FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                        Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" CssClass="Normal">
                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="WOAIntNo" HeaderText="WOAIntNo" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="WOANumber" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText %>"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText1 %>">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# String.Format("{0:yyyy-MM-dd}", DataBinder.Eval(Container, "DataItem.WOAIssueDate")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SumRegNo" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText2 %>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AccName" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText3 %>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AccIDNumber" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText4 %>"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText5 %>">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# String.Format("{0:yyyy-MM-dd}", DataBinder.Eval(Container, "DataItem.SumCourtDate")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DaysToCourt" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText6 %>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="WOAAuthorised" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText7 %>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="WOACancel" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText8 %>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CSName" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText9 %>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="WoaPrinted" HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText10 %>" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$Resources:dgWoaPrintDetail.HeaderText11 %>" >
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chbWoa" name="chhWoa"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                    </asp:DataGrid>

                    <pager:AspNetPager id="dgWoaPrintDetailPager" runat="server" 
                                    showcustominfosection="Right" width="500px" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                    FirstPageText="|&amp;lt;" 
                                    LastPageText="&amp;gt;|" 
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                    CustomInfoStyle="float:right;" PageSize="10" onpagechanged="dgWoaPrintDetailPager_PageChanged"
                                    ></pager:AspNetPager>
                </td>
            </tr>
        </table>
        <p align="center">
            <asp:Button ID="btnUpdatePrintFileName" Text="<%$Resources:btnUpdatePrintFileName.Text %>" runat="server"  OnClientClick="return Confirm();" CssClass="NormalButton"
                OnClick="btnUpdatePrintFileName_Click" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnReturn"
                    Text="<%$Resources:btnReturn.Text %>" runat="server" CssClass="NormalButton" OnClick="btnReturn_Click" />
        </p>
    </div>
    </form>
    <script type="text/javascript">
        function Confirm() {
            var selectCount = 0;
            $("input[type='checkbox']").each(function () {
                if ($(this).attr("checked")) {
                    selectCount = selectCount + 1;
                }

            });
            if (selectCount > 0) {
                if (window.confirm("Are you sure you want to create a new print file for the selected WOA?") == false) {
                    return false;
                }
            }
            else {
                alert("No woa selected");
                return false;
            }
        }

        $(document).ready(function () {
            
        });
    </script>
</body>
</html>
