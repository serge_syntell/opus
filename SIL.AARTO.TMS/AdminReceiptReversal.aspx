<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.AdminReceiptReversal" CodeBehind="AdminReceiptReversal.aspx.cs" %>

<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />

    <style>
        .headLink
        {
            font-size: 12px!important;
            color: red !important;
            text-decoration: underline !important;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="6000">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table width="100%" border="0" class="Normal">
                        <tr>
                            <td valign="top" style="height: 47px">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="60%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                    &nbsp;
                                <asp:LinkButton runat="server" CssClass="headLink" Style="font-size: 12px; font-weight: bold;" ID="linkToNoTrafficChargePymt" Text="<%$Resources:linkButton.Text1 %>" OnClick="linkToNoTrafficChargePymt_Click"></asp:LinkButton>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <!-- Start Content -->
                                <asp:UpdatePanel runat="server" ID="UDP">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlDetails" runat="server">
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label><br />
                                            <br />
                                            <table style="width: 700px;">
                                                <%-- <tr>
                                                    <td class="NormalBold" style="width: 300px;">
                                                        Enter the Individual's full ID Number:</td>
                                                    <td style="width: 300px;">
                                                        <asp:TextBox ID="textId" runat="server" Width="400px" CssClass="Normal"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" style="text-align: center;">
                                                        &nbsp;-- Or --</td>
                                                    <td>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblSequenceNum.Text %>"></asp:Label></td>
                                                    <td>
                                                        <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblTicketNum.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTicketNo" runat="server" Width="400px" CssClass="Normal"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold"></td>
                                                    <td align="right">
                                                        <asp:Button ID="btnRePrint" runat="server" OnClick="btnRePrint_Click" Text=" <%$Resources:btnRePrint.Text %> " CssClass="NormalButton" Width="120px" />&nbsp; 
                                                        <asp:Button ID="btnSearch" runat="server" OnClick="buttonSearch_Click" Text="<%$Resources:btnSearch.Text %>" CssClass="NormalButton" Width="120px" />&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Button ID="btnShowNotApplied" runat="server" OnClick="btnShowNotApplied_Click" Text="<%$Resources:btnShowNotApplied.Text %>" CssClass="NormalButton" Width="272px" /></td>
                                                    <td align="right"></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlDisplaySelectedTickets" runat="server" HorizontalAlign="Left">
                                            &nbsp;
                                            <asp:GridView ID="dgTicketDetails" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                                CssClass="Normal" GridLines="Horizontal" OnRowCreated="dgTicketDetails_RowCreated" ShowFooter="True" OnRowCommand="dgTicketDetails_RowCommand">
                                                <FooterStyle CssClass="CartListHead" />
                                                <Columns>
                                                    <asp:BoundField DataField="RctIntNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText %>" />
                                                    <asp:BoundField DataField="NotTicketNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText1 %>" />
                                                    <asp:TemplateField HeaderText="<%$Resources:dgTicketDetails.HeaderText2 %>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-dd-MM HH:mm}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FullName" HeaderText="<%$Resources:dgTicketDetails.HeaderText3 %>" />
                                                    <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText4 %>" />
                                                    <asp:BoundField DataField="NotLocDescr" HeaderText="<%$Resources:dgTicketDetails.HeaderText5 %>" />
                                                    <asp:BoundField DataField="RctNumber" HeaderText="<%$Resources:dgTicketDetails.HeaderText6 %>" />
                                                    <asp:BoundField DataField="RTAmount" HeaderText="<%$Resources:dgTicketDetails.HeaderText7 %>" />
                                                    <asp:BoundField DataField="Reversed" HeaderText="<%$Resources:dgTicketDetails.HeaderText8 %>" Visible="False" />
                                                    <asp:BoundField DataField="ParentRctIntNo" HeaderText="ParentRctIntNo" Visible="False" />
                                                    <asp:CommandField SelectText="Reverse" ShowSelectButton="True">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="CBDateOpened" HeaderText="CBDateOpened" Visible="False" />
                                                    <asp:BoundField DataField="CBDateClosed" HeaderText="CBDateClosed" Visible="False" />
                                                    <asp:BoundField DataField="CaptureDate" HeaderText="CaptureDate" Visible="False" />
                                                </Columns>
                                                <SelectedRowStyle CssClass="RowHighlighted" />
                                                <HeaderStyle CssClass="CartListHead" />
                                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                                            </asp:GridView>
                                            <pager:AspNetPager ID="dgTicketDetailsPager" runat="server"
                                                ShowCustomInfoSection="Right" Width="400px"
                                                CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                                FirstPageText="|&amp;lt;"
                                                LastPageText="&amp;gt;|"
                                                CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                                Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                                CustomInfoStyle="float:right;" PageSize="10"
                                                OnPageChanged="dgTicketDetailsPager_PageChanged" UpdatePanelId="UDP">
                                            </pager:AspNetPager>

                                        </asp:Panel>
                                        <asp:Panel ID="pnlNotReApplied" runat="server" HorizontalAlign="Left">
                                            &nbsp;
                                            <asp:GridView ID="grvNotApplied" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                                CssClass="Normal" GridLines="Horizontal" ShowFooter="True" OnRowCommand="grvNotApplied_RowCommand" Width="659px">
                                                <FooterStyle CssClass="CartListHead" />
                                                <Columns>
                                                    <asp:BoundField DataField="ARRIntNo" HeaderText="ARRIntNo" Visible="False">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="RctIntNo" HeaderText="<%$Resources:grvNotApplied.HeaderText %>">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="RctNumber" HeaderText="<%$Resources:grvNotApplied.HeaderText1 %>">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="<%$Resources:grvNotApplied.HeaderText2 %>">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ARRAmount") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ARRAmount", "R {0:, 0.00}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ARRDate" HeaderText="<%$Resources:grvNotApplied.HeaderText3 %>" DataFormatString="{0:yyyy/MM/dd}">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="LastUser" HeaderText="<%$Resources:grvNotApplied.HeaderText4 %>">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:CommandField SelectText="Re-apply payment" ShowSelectButton="True">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:CommandField>
                                                </Columns>
                                                <SelectedRowStyle CssClass="RowHighlighted" />
                                                <HeaderStyle CssClass="CartListHead" HorizontalAlign="Left" />
                                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                                            </asp:GridView>
                                            <pager:AspNetPager ID="grvNotAppliedPager" runat="server"
                                                ShowCustomInfoSection="Right" Width="400px"
                                                CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                                FirstPageText="|&amp;lt;"
                                                LastPageText="&amp;gt;|"
                                                CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                                Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                                CustomInfoStyle="float:right;" PageSize="10"
                                                OnPageChanged="grvNotAppliedPager_PageChanged" UpdatePanelId="UDP">
                                            </pager:AspNetPager>
                                        </asp:Panel>
                                        <br />
                                        <asp:Panel ID="pnlReceiptDetails" runat="server" Width="100%" HorizontalAlign="Left">
                                            <asp:Label ID="lblReceiptDetails" runat="server" CssClass="SubContentHead" Text="<%$Resources:lblReceiptDetails.Text %>"> </asp:Label><br />
                                            <asp:GridView ID="grdReceipt" runat="server" AlternatingRowStyle-CssClass="CartListItemAlt"
                                                AutoGenerateColumns="False" CssClass="Normal" FooterStyle-CssClass="cartlistfooter"
                                                HeaderStyle-CssClass="CartListHead"
                                                RowStyle-CssClass="CartListItem" ShowFooter="True">
                                                <FooterStyle CssClass="CartListFooter" />
                                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                                                <RowStyle CssClass="CartListItem" />
                                                <HeaderStyle CssClass="CartListHead" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText %>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOffenceDate" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="RctNumber" HeaderText="<%$Resources:grdReceipt.HeaderText1 %>" />
                                                    <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText2 %>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReceiptDate" runat="server" Text='<%# Bind("RctDate", "{0:yyyy-MM-dd}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="RctUser" HeaderText="<%$Resources:grdReceipt.HeaderText3 %>" />
                                                    <asp:BoundField DataField="RctDetails" HeaderText="<%$Resources:grdReceipt.HeaderText4 %>" />
                                                    <asp:BoundField DataField="ReceivedFrom" HeaderText="<%$Resources:grdReceipt.HeaderText5 %>" />
                                                    <asp:BoundField DataField="ContactNumber" HeaderText="<%$Resources:grdReceipt.HeaderText6 %>" />
                                                    <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText7 %>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCashType" runat="server" Text='<%# (Eval("CashType")).ToString().Length > 0 ? ((Stalberg.TMS.CashType)(Eval("CashType")).ToString()[0]).ToString() : "" %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTotal" runat="server" Text='<%# Bind("Total") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText8 %>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTicketNo" runat="server" Text='<%# Bind("NotTicketNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText9 %>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("RTAmount", "{0:0.00}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <pager:AspNetPager ID="grdReceiptPager" runat="server"
                                                ShowCustomInfoSection="Right" Width="400px"
                                                CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                                FirstPageText="|&amp;lt;"
                                                LastPageText="&amp;gt;|"
                                                CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                                Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                                CustomInfoStyle="float:right;" PageSiz="10" OnPageChanged="grdReceiptPager_PageChanged" UpdatePanelId="UDP">
                                            </pager:AspNetPager>

                                            <br />
                                            <asp:Button ID="btnProceed" runat="server" CssClass="NormalButton" OnClick="btnProceed_Click"
                                                Text="<%$Resources:btnProceed.Text %>" />
                                        </asp:Panel>
                                        <asp:Panel ID="pnlConfirm" runat="server" CssClass="Normal" Width="100%" HorizontalAlign="Left">
                                            <table border="0" class="Normal">
                                                <tr>
                                                    <td class="NormalBold" valign="middle">&nbsp;</td>
                                                    <td valign="middle"></td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" valign="middle">
                                                        <asp:Label ID="lblConfirmMessage" runat="server" CssClass="NormalBold"></asp:Label></td>
                                                    <td align="left" valign="middle">
                                                        <asp:CheckBox ID="chkAreYouSure" runat="server" AutoPostBack="True" CssClass="NormalRed"
                                                            OnCheckedChanged="chkAreYouSure_CheckedChanged" Text="<%$Resources:chkAreYouSure.Text %>" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" valign="middle">
                                                        <asp:Label ID="lblCashAmount" runat="server" Text="<%$Resources:lblCashAmount.Text %>"></asp:Label>
                                                        &nbsp;&nbsp;
                                                        <asp:TextBox ID="txtCashAmount" runat="server" CssClass="Normal" Width="99px"></asp:TextBox><br />
                                                    </td>
                                                    <td align="left" valign="top">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" valign="middle">
                                                        <asp:Panel ID="pnlReason" runat="server" BorderColor="White" BorderStyle="None" Visible="true">
                                                            &nbsp;<fieldset>
                                                                <legend>
                                                                    <asp:Label ID="Label4" runat="server" Text="<%$Resources:pnlReason.Text %>"></asp:Label></legend>
                                                                <asp:RadioButtonList ID="rdlReason" runat="server" CssClass="Normal" RepeatColumns="2"
                                                                    RepeatLayout="Table" Width="251px" AutoPostBack="True" OnSelectedIndexChanged="rdlReason_SelectedIndexChanged">
                                                                    <asp:ListItem Selected="True" Value="0" Text="<%$Resources:rdlReasonItem.Text %>"></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="<%$Resources:rdlReasonItem.Text1 %>"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                                <br />
                                                                <asp:CheckBox ID="chkApply" runat="server" AutoPostBack="True" CssClass="NormalBold"
                                                                    OnCheckedChanged="chkAreYouSure_CheckedChanged" Text="<%$Resources:chkApply.Text %>" Width="444px" />
                                                            </fieldset>
                                                        </asp:Panel>
                                                    </td>
                                                    <td align="left" valign="top"></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="height: 26px">
                                                        <asp:Button ID="btnConfirm" runat="server" CssClass="NormalButton" OnClick="btnConfirm_Click"
                                                            Text="<%$Resources:btnConfirm.Text %>" Width="87px" /></td>
                                                    <td align="left" style="height: 26px"></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlNext" runat="server" Height="50px" Width="125px">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnReApplyPayments" runat="server" CssClass="NormalButton"
                                                            Text="<%$Resources:btnReApplyPayments.Text %>" Width="156px" OnClick="btnReApplyPayments_Click" /></td>
                                                    <td style="width: 3px">
                                                        <asp:Button ID="btnPrintReversalReceipt" runat="server" CssClass="NormalButton"
                                                            Text="<%$Resources:btnPrintReversalReceipt.Text %>" Width="156px" OnClick="btnPrintReversalReceipt_Click" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label5"
                                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label>
                                        </p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center"></td>
                <td valign="top" align="left" width="100%"></td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>

    <script type="text/javascript" language="javascript">
        function CheckMe() {
            var id = document.getElementById("textId");
            var ticket = document.getElementById("textTicket");

            if (id.length == 0 && ticket.length == 0) {
                alert("You need to supply either an ID number or a ticket number.");
            }
        }
    </script>

</body>
</html>
