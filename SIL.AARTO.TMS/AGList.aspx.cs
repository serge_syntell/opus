using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class AGList : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Spot Fine or Admission of Guilt List";
        protected string thisPageURL = "AGList.aspx";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 

            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                this.dtpStartDate.Text = DateTime.Today.ToString(DATE_FORMAT);
                this.dtpEndDate.Text = DateTime.Today.AddDays(1).ToString(DATE_FORMAT);

                //this.PopulateCourts();
                PopulateAuthorites( autIntNo);

                //Barry Dickson - populate sort order as SF radio button is selected by default
                PopulateSortOrder(this.rblSForAG.SelectedValue);

                //BD - need to hide the court room selection as there is no court room for SF
                this.cboCourtRoom.Visible = false;
                this.lblCourtRoom.Visible = false;

                //Barry Dickson
                this.PopulateCourts();

                ShowCourtRoom();
            }

            if(ddlSelectLA.SelectedIndex > -1)
            {
                autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                Session["printAutIntNo"] = autIntNo;
            }
            
        }

        private void PopulateCourts()
        {
            this.cboCourt.Items.Clear();

            CourtDB db = new CourtDB(this.connectionString);
            SqlDataReader reader = db.GetAuth_CourtListByAuth(this.autIntNo);

            ListItem item;
            while (reader.Read())
            {
                //Jerry 2014-11-14 exclude the Unknown court
                if (reader["CrtNo"].ToString() == "000000")
                    continue;

                item = new ListItem();
                item.Value = reader["CrtIntNo"].ToString();
                item.Text = reader["CrtDetails"].ToString();

                this.cboCourt.Items.Add(item);
            }
            reader.Close();

            item = new ListItem();
            item.Text = (string)GetLocalResourceObject("cboCourt.Items");
            item.Value = "0";
            this.cboCourt.Items.Insert(0, item);

            this.cboCourt.SelectedIndex = 0;

            //BD need to create an entry for Court rooms for all courts.
            this.cboCourtRoom.Items.Clear();
            ListItem CRItem;
            CRItem = new ListItem();
            CRItem.Text = (string)GetLocalResourceObject("cboCourtRoom.Items");
            CRItem.Value = "0";
            this.cboCourtRoom.Items.Insert(0, CRItem);

            this.cboCourtRoom.SelectedIndex = 0;
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnShow_Click(object sender, EventArgs e)
        {
            DateTime dtStart;
            if (!DateTime.TryParse(this.dtpStartDate.Text,out dtStart))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            DateTime dtEnd;
            if (!DateTime.TryParse(this.dtpEndDate.Text, out dtEnd))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            if (dtEnd.Ticks < dtStart.Ticks)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            int crtIntNo = int.Parse(this.cboCourt.SelectedValue);
            int crtRoomIntNo = int.Parse(this.cboCourtRoom.SelectedValue);
            string SortOrder = this.ddlSort.SelectedValue;
            string AGorSpot = this.rblSForAG.SelectedValue;

            PageToOpen page = new PageToOpen(this, "AGListViewer.aspx");

            page.Parameters.Add(new QSParams("rAutIntNo", autIntNo));
            page.Parameters.Add(new QSParams("rReceiptStartDate", dtStart.ToString("yyyy-M-dd")));
            page.Parameters.Add(new QSParams("rReceiptEndDate", dtEnd.ToString("yyyy-M-dd")));
            //BD now need to pass through extra parameters
            page.Parameters.Add(new QSParams("AGorSpot", AGorSpot));
            page.Parameters.Add(new QSParams("CrtIntNo", crtIntNo));
            page.Parameters.Add(new QSParams("CrtRIntNo", crtRoomIntNo));
            //BD sort order can be: Notice, Receipt, Summons, AG
            page.Parameters.Add(new QSParams("SortOrder", SortOrder));

            Helper_Web.BuildPopup(page);
        }

        protected void cboCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BD need to check that we are dealing with AG and SF as SF does not have a court room and is not visible.
            if (this.rblSForAG.SelectedValue != "S")
            {
                int crtIntNo = int.Parse(this.cboCourt.SelectedValue);

                this.cboCourtRoom.Items.Clear();

                CourtRoomDB db = new CourtRoomDB(this.connectionString);
                //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
                SqlDataReader reader = db.GetCourtRoomListByCrtRStatusFlag(crtIntNo);//db.GetCourtRoomList(crtIntNo);

                this.cboCourtRoom.DataSource = reader;
                this.cboCourtRoom.DataValueField = "CrtRIntNo";
                this.cboCourtRoom.DataTextField = "CrtRoomDetails";
                this.cboCourtRoom.DataBind();
                reader.Close();

                //Heidi 2013-09-23 removed translate from lookup table

                //ListItem item;
                //Dictionary<int, string> lookups =
                //    CourtRoomLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
                //while (reader.Read())
                //{
                //    int crtRIntNo = (int)reader["CrtRIntNo"];
                //    if (lookups.ContainsKey(crtRIntNo))
                //    {
                //        cboCourtRoom.Items.Add(new ListItem(lookups[crtRIntNo], crtRIntNo.ToString()));
                //    }
                //}
                //while (reader.Read())
                //{
                //    item = new ListItem();
                //    item.Value = reader["CrtRIntNo"].ToString();
                //    item.Text = reader["CrtRoomDetails"].ToString();

                //    this.cboCourtRoom.Items.Add(item);
                //}
               // reader.Close();

                ListItem item = new ListItem();
                item.Value = "0";
                item.Text = (string)GetLocalResourceObject("cboCourtRoom.Items");
                this.cboCourtRoom.Items.Insert(0, item);

                this.cboCourtRoom.SelectedIndex = 0;
            }
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            Session["printAutIntNo"] = autIntNo;
            //Barry Dickson
            PopulateCourts();
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorites( int autIntNo)
        {

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = reader;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind( );
            //ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString( )));

            //reader.Close( );
        }

        //Barry Dickson
        private void PopulateSortOrder(string SForAF)
        {
            if (SForAF == "S")
            {
                //Receipt Number or Notice Number
                this.ddlSort.Items.Clear();

                ListItem item;
                ListItem item1;
                item = new ListItem();
                item1 = new ListItem();

                item.Value = "Receipt";
                item.Text = (string)GetLocalResourceObject("ddlSort.Items");
                this.ddlSort.Items.Insert(0, item);

                item1.Value = "Notice";
                item1.Text = (string)GetLocalResourceObject("ddlSort.Items1");
                this.ddlSort.Items.Insert(1, item1);

                //this.ddlSort.SelectedIndex = 0;
            }
            else
            {
                //Receipt Number or Summons Number or AG Number
                this.ddlSort.Items.Clear();

                ListItem item;
                ListItem item1;
                ListItem item2;
                item = new ListItem();
                item1 = new ListItem();
                item2 = new ListItem();

                item.Value = "Receipt";
                item.Text = (string)GetLocalResourceObject("ddlSort.Items");
                this.ddlSort.Items.Insert(0, item);

                item1.Value = "Summons";
                item1.Text = (string)GetLocalResourceObject("ddlSort.Items2");
                this.ddlSort.Items.Insert(1, item1);

                item2.Value = "AG";
                item2.Text = (string)GetLocalResourceObject("ddlSort.Items3");
                this.ddlSort.Items.Insert(2, item2);

                //this.ddlSort.SelectedIndex = 0;
            }
        }

        protected void rblSForAG_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateSortOrder(this.rblSForAG.SelectedValue);
            ShowCourtRoom();
        }

        private void ShowCourtRoom()
        {
            //BD need to hide or unhide the court room depending on which one is selected
            if (this.rblSForAG.SelectedValue == "S")
            {
                this.cboCourtRoom.Visible = false;
                this.lblCourtRoom.Visible = false;
            }
            else
            {
                this.cboCourtRoom.Visible = true;
                this.lblCourtRoom.Visible = true;
            }
        }

        protected void dtpStartDate_TextChanged(object sender, EventArgs e)
        {
            DateTime dtStart;
            if (!DateTime.TryParse(this.dtpStartDate.Text,out dtStart))
                return;

            DateTime dtEnd;
            if (!DateTime.TryParse(this.dtpEndDate.Text, out dtEnd))
            {
                this.dtpEndDate.Text = dtStart.AddDays(1).ToString(DATE_FORMAT);
                return;
            }

            if (dtEnd.Ticks < dtStart.Ticks)
                this.dtpEndDate.Text = dtStart.AddDays(1).ToString(DATE_FORMAT);
        }
}
}
