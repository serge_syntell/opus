<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>


<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ViewSummons_Detail" Codebehind="ViewSummons_Detail.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    
     <script>
		    function Print()
			{
				window.print();
			}
			function Close()
			{
				window.close();
			}
    </script>
    
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    onload="self.focus();" rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" colspan="2" valign="middle" style="width: 100%">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="udpFrame" runat="server">
            <ContentTemplate>
                <table border="0" width="100%">
                    <tr>
                        <td valign="top" width="100%" align="center">
                            <table width="100%">
                                <tr>
                                    <td width="20%">
                                        <asp:HyperLink ID="hlBack" runat="server" CssClass="NormalBold" NavigateUrl="javascript:Close()" Text="<%$Resources:hlBack.Text %>"></asp:HyperLink></td>
                                    <td align="center">
                                        <asp:Label ID="lblPageName" runat="server" Width="496px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></td>
                                    <td width="20%" align="right">
                                        </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top" width="100%">
                            <table width="100%">
                                <tr>
                                    <td width="100%" align="center">
                                        <table width="100%">
                                            <tr>
                                                <td align="left" valign="top" >
                                                    <table>
                                                        <tr>
                                                            <td valign="top" width="194">
                                                                <asp:Label ID="Label26" runat="server" CssClass="SubContentHead" Font-Underline="True"
                                                                    Width="194px" Text="<%$Resources:lblSummonsdetails.Text %>"></asp:Label></td>
                                                            <td valign="top" width="150">
                                                            </td>
                                                            <td valign="top" width="150">
                                                            </td>
                                                            <td valign="top" width="150">
                                                                &nbsp;</td>
                                                            <td valign="top" width="150">
                                                                &nbsp;</td>
                                                            <td valign="top" width="150">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  valign="top" width="194" style="height: 40px">
                                                                <asp:Label ID="Label17" runat="server" CssClass="NormalBold" Width="107px" Text="<%$Resources:lblSummonsNo.Text %>"></asp:Label></td>
                                                            <td  valign="top" width="150" style="height: 40px">
                                                                <asp:Label ID="lblSummonsNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td valign="top" width="150" style="height: 40px">
                                                                <asp:Label ID="Label20" runat="server" CssClass="NormalBold" Width="160px" Text="<%$Resources:lblSummonsDate.Text %>"></asp:Label></td>
                                                            <td valign="top" width="150" style="height: 40px">
                                                                <asp:Label ID="lblSummonsDate" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td valign="top" width="150" style="height: 40px">
                                                                <asp:Label ID="Label22" runat="server" CssClass="NormalBold" Width="137px" Height="19px" Text="<%$Resources:lblSummonsStatus.Text %>"></asp:Label></td>
                                                            <td valign="top" width="150" style="height: 40px">
                                                                <asp:Label ID="lblSummonsStatus" runat="server" BorderStyle="None" CssClass="Normal"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" width="194">
                                                                <asp:Label ID="Label35" runat="server" CssClass="NormalBold" Width="175px" Text="<%$Resources:lblCourtDate.Text %>"></asp:Label>
                                                            </td>
                                                            <td valign="top" width="150">
                                                                <asp:Label ID="lbSummonsCrtDate" runat="server" CssClass="Normal"></asp:Label>
                                                            </td>
                                                            <td valign="top" width="150">
                                                            </td>
                                                            <td valign="top" width="150">
                                                            </td>
                                                            <td valign="top" width="150">
                                                            </td>
                                                            <td valign="top" width="150">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" width="194">
                                                                &nbsp;</td>
                                                            <td valign="top" width="150">
                                                                &nbsp;</td>
                                                            <td valign="top" width="150">
                                                                &nbsp;</td>
                                                            <td valign="top" width="150">
                                                                &nbsp;</td>
                                                            <td valign="top" width="150">
                                                                &nbsp;</td>
                                                            <td valign="top" width="150">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td  valign="top" width="194">
                                                                <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Width="107px" Text="<%$Resources:lblTicketNo.Text %>"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="lblTicketNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="Label19" runat="server" CssClass="NormalBold" Width="160px" Text="<%$Resources:lblFilmFrame.Text %>"></asp:Label></td>
                                                            <td valign="top" width="150">
                                                                <asp:Label ID="lblFrameNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td valign="top" width="150">
                                                                <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Width="137px" Height="19px" Text="<%$Resources:lblRegNo.Text %>"></asp:Label></td>
                                                            <td valign="top" width="150">
                                                                <asp:Label ID="lblRegNo" runat="server" BorderStyle="None" CssClass="Normal"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td  valign="top" width="194">
                                                                <asp:Label ID="Label34" runat="server" CssClass="NormalBold" Width="107px" Text="<%$Resources:lblCaseNo.Text %>"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="lblCaseNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td valign="top" width="194" >
                                                                <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="173px" Height="19px" Text="<%$Resources:lblOffenceTime.Text %>"></asp:Label></td>
                                                            <td valign="top" width="150" >
                                                                <asp:Label ID="lblOffenceDate" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Width="131px" Text="<%$Resources:lblSpeed.Text %>"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="lblSpeed" runat="server" CssClass="Normal"></asp:Label>
                                                                <asp:Label ID="lblSpeed2" runat="server" CssClass="Normal"></asp:Label></td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td valign="top"  width="194" >
                                                                <asp:Label ID="Label24" runat="server" CssClass="NormalBold" Width="173px" Height="19px" Text="<%$Resources:lblFineAmount.Text %>"></asp:Label></td>
                                                            <td valign="top" width="150" >
                                                                <asp:Label ID="lblFineAmt" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="Label28" runat="server" CssClass="NormalBold" Width="131px" Text="<%$Resources:lblContemptAmount.Text %>"></asp:Label></td>
                                                            <td  valign="top"  width="150">
                                                                <asp:Label ID="lblContemptAmt" runat="server" CssClass="Normal"></asp:Label>
                                                            </td>
                                                            <td valign="top" width="150">
                                                            </td>
                                                            <td valign="top"  width="150">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table>
                                                        <tr valign=top >
                                                            <td width=194 valign=top >
                                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="194px" Height="19px" Text="<%$Resources:lblOffencedescr.Text %>"></asp:Label></td><td align="left" ><asp:Label ID="lblOffence" runat="server" CssClass="Normal"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  valign="top">
                                                <table>
                                                        <tr valign=top >
                                                            <td width = 194><asp:Label ID="Label3" runat="server" CssClass="NormalBold" Height="19px" Width="194px">Location:</asp:Label></td><td align="left"><asp:Label ID="lblLocDescr" runat="server" CssClass="Normal"></asp:Label></td></tr></table>
                                                    
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  valign="top" style="height: 21px">
                                                    &nbsp; &nbsp;&nbsp;<table id="tblWarrantDetails" runat="server">
                                                        <tr>
                                                            <td colspan="6" valign="top" width="194">
                                                                <asp:Label ID="Label21" runat="server" CssClass="SubContentHead" Width="194px" Font-Underline="True" Text="<%$Resources:lblWarrantdetails.Text %>"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" width="194">
                                                                <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Width="107px" Text="<%$Resources:lblWarrantNo.Text %>"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="lblWNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="Label23" runat="server" CssClass="NormalBold" Width="160px" Text="<%$Resources:lblFineAmount.Text %>"></asp:Label></td>
                                                            <td  valign="top" width="150">
                                                                <asp:Label ID="lblWFineAmount" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td valign="top" width="150">
                                                                <asp:Label ID="Label33" runat="server" CssClass="NormalBold" Width="137px" Height="19px" Text="<%$Resources:lblAdditionalAmount.Text %>"></asp:Label></td>
                                                            <td valign="top" width="150">
                                                                <asp:Label ID="lblWAddAmount" runat="server" BorderStyle="None" CssClass="Normal"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td  valign="top" style="height: 21px" width="194">
                                                                <asp:Label ID="Label30" runat="server" CssClass="NormalBold" Width="137px" Height="19px" Text="<%$Resources:lblGeneratedDate.Text %>"></asp:Label></td>
                                                            <td  valign="top" style="height: 21px" width="150">
                                                                <asp:Label ID="lblWGenDate" runat="server" BorderStyle="None" CssClass="Normal"></asp:Label></td>
                                                            <td  valign="top" style="height: 21px" width="150">
                                                                <asp:Label ID="Label27" runat="server" CssClass="NormalBold" Width="107px" Text="<%$Resources:lblIssueDate.Text %>"></asp:Label></td>
                                                            <td  valign="top" style="height: 21px" width="150">
                                                                <asp:Label ID="lblWIssueDate" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td valign="top" style="height: 21px" width="150">
                                                                <asp:Label ID="Label25" runat="server" CssClass="NormalBold" Height="19px" Width="137px" Text="<%$Resources:lblPaidAmount.Text %>"></asp:Label></td>
                                                            <td valign="top" style="height: 21px" width="150">
                                                                <asp:Label ID="lblWPaidAmount" runat="server" BorderStyle="None" CssClass="Normal"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 21px" valign="top" width="194">
                                                                <asp:Label ID="Label29" runat="server" CssClass="NormalBold" Width="160px" Text="<%$Resources:lblNotificationDate.Text %>"></asp:Label></td>
                                                            <td style="height: 21px" valign="top" width="150">
                                                                <asp:Label ID="lblWNotificationDate" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td style="height: 21px" valign="top" width="150">
                                                                <asp:Label ID="Label31" runat="server" CssClass="NormalBold" Height="19px" Width="137px" Text="<%$Resources:lblPrintDate.Text %>"></asp:Label></td>
                                                            <td style="height: 21px" valign="top" width="150">
                                                                <asp:Label ID="lblWPrintDate" runat="server" BorderStyle="None" CssClass="Normal"></asp:Label></td>
                                                            <td style="height: 21px" valign="top" width="150">
                                                                <asp:Label ID="Label32" runat="server" CssClass="NormalBold" Height="19px" Width="137px" Text="<%$Resources:lblDeliveredDate.Text %>"></asp:Label></td>
                                                            <td style="height: 21px" valign="top" width="150">
                                                                <asp:Label ID="lblWDelDate" runat="server" BorderStyle="None" CssClass="Normal"></asp:Label></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" >
                                                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table id="tblOwnerDetails" runat="server">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label4" runat="server" CssClass="SubContentHead" Width="194px" Font-Underline="True" Text="<%$Resources:lblOwnerdetails.Text %>"></asp:Label>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="94px" Font-Underline="True" Text="<%$Resources:lblDriverdetails.Text %>"></asp:Label>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblProxy" runat="server" CssClass="NormalBold" Width="202px" Font-Underline="True" Text="<%$Resources:lblProxy.Text %>"></asp:Label></td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="46px" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                                <asp:Label ID="lblOwnerName" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Width="31px" Text="<%$Resources:lblID.Text %>"></asp:Label>
                                                                <asp:Label ID="lblOwnerID" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Width="45px" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                                <asp:Label ID="lblDriverName" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Width="26px" Text="<%$Resources:lblID.Text %>"></asp:Label>
                                                                <asp:Label ID="lblDriverID" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblProxyName" runat="server" CssClass="NormalBold" Width="56px" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                                <asp:Label ID="lblPrxName" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="lblProxyID" runat="server" CssClass="NormalBold" Width="31px" Text="<%$Resources:lblID.Text %>"></asp:Label>
                                                                <asp:Label ID="lblPrxID" runat="server" CssClass="Normal"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="122px" Text="<%$Resources:lblPostaladdress.Text %>"> </asp:Label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOwnerPostAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Width="126px" Text="<%$Resources:lblPostaladdress.Text %>"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblDriverPostAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Width="122px" Text="<%$Resources:lblStreetaddress.Text %>"></asp:Label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOwnerStrAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                                <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Width="124px" Text="<%$Resources:lblStreetaddress.Text %>"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblDriverStrAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="width: 933px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="100%" style="height: 152px">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table cellspacing="0" cellpadding="0" border="0" height="5%" style="width: 100%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width: 100%" align="center">
                    <asp:UpdateProgress ID="udp" runat="server" AssociatedUpdatePanelID="udpFrame">
                        <ProgressTemplate>
                            <p class="NormalBold" style="text-align: center">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label36"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
