using System;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.Data.Datasets;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for FirstNotice1.
    /// </summary>
    public partial class SummonsViewer : System.Web.UI.Page
    {
        // Fields
        private string login = string.Empty;
        private string connectionString = string.Empty;
        private Int32 autIntNo = 0;
        private string sStationery = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsViewer.aspx";
        protected string thisPage = "Summons Viewer";

        #region 20120116 Oscar disabled for backup
        ///// <summary>
        ///// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        ///// </summary>
        ///// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        //protected override void OnLoad(System.EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();
        //    string reportPage = string.Empty;

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    // Get user details
        //    Stalberg.TMS.UserDB user = new UserDB(connectionString);
        //    Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
        //    this.login = userDetails.UserLoginName;

        //    if (Request.QueryString["printfile"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["printAutIntNo"] != null)
        //        Int32.TryParse(Session["printAutIntNo"].ToString(), out autIntNo);

        //    // Set domain specific variables
        //    General gen = new General();
        //    backgroundImage = gen.SetBackground(Session["drBackground"]);
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    //jerry 2011-11-16 changed
        //    //autIntNo = (int)Session["printAutIntNo"];

        //    // Generate the report details
        //    ReportDocument reportDoc = new ReportDocument();

        //    // Check the QueryString for the 'Summary' flag
        //    bool isSummary = (Request.QueryString["Summary"] != null);
        //    string printFile = Request.QueryString["printfile"].Trim();
        //    string sMode = Request.QueryString["mode"] == null ? "N" : Request.QueryString["mode"].Trim();
        //    // BD 20080526 local variable to handle stored proc name
        //    string sStoredProcName = Request.QueryString["Summary"] == null ? "NoticePrintSummons" : "NoticePrintSummonsControl";

        //    // check the print file name to check what summons to print
        //    // 20070929 was not printing summary - need to cater for old print files as well
        //    // 2011-8-3 jerry changed 
        //    string noticeFilmType = string.Empty;
        //    if (printFile.IndexOf("CPA6") >= 0)
        //        reportPage = "Summons_HWO_CPA6.rpt";
        //    else if (printFile.IndexOf("*") >= 0)
        //    {
        //        if (sMode == "N")
        //        {
        //            string noticeTicketNo = printFile.Substring(1, printFile.Length - 1);
        //            NoticeForPaymentDB noticeDB = new NoticeForPaymentDB(connectionString);
        //            noticeFilmType = noticeDB.GetFilmType(noticeTicketNo.Replace("-", string.Empty).Replace("/", string.Empty));
        //        }
        //        else
        //        {
        //            string summonsNo = printFile.Substring(1, printFile.Length - 1);
        //            SummonsDB summonsDB = new SummonsDB(connectionString);
        //            noticeFilmType = summonsDB.GetNotFilmTypeBySummonsNo(summonsNo);
        //        }
        //    }
        //    else
        //        reportPage = "Summons_CPA5.rpt";


        //    AuthReportNameDB arn = new AuthReportNameDB(this.connectionString);
        //    // 2011-8-3 jerry changed 
        //    if (printFile.IndexOf("CPA6") >= 0 || noticeFilmType == "H")
        //    {
        //        reportPage = arn.GetAuthReportName(autIntNo, "Summons_HWO");
        //    }
        //    else
        //    {
        //        reportPage = arn.GetAuthReportName(autIntNo, "Summons");
        //    }
        //    if (!isSummary)
        //    {
        //        if (reportPage.Equals(string.Empty))
        //        {
        //            reportPage = "Summons_CPA5.rpt";//Summons_Personal_DotMatrix.rpt";
        //            arn.AddAuthReportName(autIntNo, reportPage, "Summons", "System", "");
        //        }
        //    }
        //    else
        //        reportPage = "Summons_Personal_Summary.rpt";

        //    string reportPath = Server.MapPath("reports/" + reportPage);

        //    //****************************************************
        //    //SD:  20081120 - check that report actually exists
        //    string templatePath = string.Empty;
        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "Summons");
        //    if (!File.Exists(reportPath))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //****************************************************

        //    // Get the date rule for last payment date
        //    //DateRulesDB dtRule = new DateRulesDB(connectionString);
        //    //DateRulesDetails rule = new DateRulesDetails();

        //    ////rule.AutIntNo = (int)Session["AutIntNo"];
        //    //rule.AutIntNo = autIntNo;
        //    //rule.DtRDescr = "Max. no days before COURT DATE that payment can be received.";
        //    //rule.DtRNoOfDays = 7;
        //    //rule.DtRStartDate = "NotOffenceDate";
        //    //rule.DtREndDate = "CRCourtDate";
        //    //rule.LastUser = login;
        //    //rule = dtRule.GetDefaultDateRule(rule);

        //    // SD: 20090105 
        //    DateRulesDetails rule = new DateRulesDetails();

        //    rule.AutIntNo = autIntNo;
        //    rule.LastUser = login;
        //    rule.DtRStartDate = "NotOffenceDate";
        //    rule.DtREndDate = "CRCourtDate";

        //    DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
        //    int noOfDays = dateRule.SetDefaultDateRule();

        //    // jerry 2011-4-20 add
        //    AuthorityRulesDetails auRule = new AuthorityRulesDetails();
        //    auRule.AutIntNo = this.autIntNo;
        //    auRule.ARCode = "5040";
        //    auRule.LastUser = userDetails.UserLoginName;

        //    DefaultAuthRules auRuleDefault = new DefaultAuthRules(auRule, this.connectionString);
        //    auRuleDefault.SetDefaultAuthRule();

        //    bool IsCourtNumber;
        //    if (auRule.ARString == "N")
        //        IsCourtNumber = true;
        //    else
        //        IsCourtNumber = false;

        //    reportDoc.Load(reportPath);

        //    // Get the PageMargins structure and set the margins for the report.
        //    PageMargins margins = reportDoc.PrintOptions.PageMargins;
        //    margins.leftMargin = 0;
        //    margins.topMargin = 0;
        //    margins.rightMargin = 0;
        //    margins.bottomMargin = 0;

        //    // Apply the page margins.
        //    reportDoc.PrintOptions.ApplyPageMargins(margins);

        //    //jerry 2011-6-15 add
        //    if (reportPage.Equals("Summons_HWO_CPA6.rpt"))
        //    {
        //        sStoredProcName = "NoticePrintSummonsHWO";
        //    }

        //    // Retrieve the data source
        //    string tempFile = printFile;
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand com = new SqlCommand(sStoredProcName, con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = tempFile;
        //    com.Parameters.Add("@Mode", SqlDbType.VarChar, 1).Value = sMode;

        //    if (sStoredProcName != "NoticePrintSummonsHWO")
        //    {
        //        com.Parameters.Add("@NoDays", SqlDbType.Int, 4).Value = noOfDays;
        //    }

        //    com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = login;
        //    if (sStoredProcName.Equals("NoticePrintSummons"))
        //    {
        //        com.Parameters.Add("@IsCourtNumber", SqlDbType.Bit, 1).Value = IsCourtNumber;
        //    }

        //    SqlDataAdapter da = new SqlDataAdapter(com);

        //    dsPrintSummons ds = new dsPrintSummons();
        //    da.Fill(ds);
        //    da.Dispose();

        //    if (ds.Tables[1].Rows.Count == 0)
        //    {
        //        ds.Dispose();
        //        Response.Write("There are no summonses matching your search");
        //        Response.End();
        //        return;
        //    }

        //    if (sStoredProcName.Equals("NoticePrintSummons") || sStoredProcName.Equals("NoticePrintSummonsHWO"))
        //        reportDoc.PrintOptions.PaperSize = PaperSize.PaperFanfoldStdGerman; // 8.5" x 12" .PaperFanfoldStdGerman;
        //    else
        //        reportDoc.PrintOptions.PaperSize = PaperSize.DefaultPaperSize;

        //    // Bind the report to the data
        //    reportDoc.SetDataSource(ds.Tables[1]);
        //    ds.Dispose();

        //    // Perform the export
        //    MemoryStream ms = new MemoryStream();
        //    ms = (MemoryStream)reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
        //    reportDoc.Dispose();

        //    // Insert the PDF file as the only content in the Web stream
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/pdf";
        //    Response.BinaryWrite(ms.ToArray());
        //    Response.End();

        //}
        #endregion

        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            //Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            if (Request.QueryString["printfile"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["printAutIntNo"] != null)
                int.TryParse(Session["printAutIntNo"].ToString(), out this.autIntNo);

            // Set domain specific variables
            General gen = new General();
            this.backgroundImage = gen.SetBackground(Session["drBackground"]);
            this.styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            this.title = gen.SetTitle(Session["drTitle"]);

            // use module to create print pdf file, Oscar 20120117
            PrintFileProcess process = new PrintFileProcess(this.connectionString, this.login);
            process.BuildPrintFile(
                new PrintFileModuleSummons(Request.QueryString["Summary"], Request.QueryString["mode"]),
                this.autIntNo, Request.QueryString["printfile"]
            );
            if (Request.QueryString["printType"] != null && Request.QueryString["printType"] == "s")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintSummons, PunchAction.Change);  

            }
            else if (Request.QueryString["printType"] != null && Request.QueryString["printType"] == "PrintSummonsControlRegister")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintSummonsPrintSummonsControlRegister, PunchAction.Change); 
            }
            else if (Request.QueryString["printType"] != null && Request.QueryString["printType"] == "SummonsRoadBlockPrintCPA5")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsRoadBlockPrintCPA5, PunchAction.Change);
            }
            else if (Request.QueryString["printType"] != null && Request.QueryString["printType"] == "SummonsRoadBlockPrintCPA6")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SummonsRoadBlockPrintCPA6, PunchAction.Change);
            }

        }

    }
}
