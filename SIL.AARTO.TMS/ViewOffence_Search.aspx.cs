using System;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using Stalberg.TMS.Data;
using System.Data.SqlClient;
using SIL.Common;
using System.Configuration;
using SIL.AARTO.BLL.CameraManagement;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page where a synopsis of offences can be displayed as the result of a search
    /// 20090420 tf  ---modify enquiry function, add SummonsNO,  CASENO,  WOANO search,  and modify the page of the enquiry.
    /// </summary>
    public partial class ViewOffence_Search : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string login;

        protected string styleSheet;
        protected string backgroundImage;
        //protected string thisPage = "Enquiries";
        protected string thisPageURL = "ViewOffence_Search.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string strEnquiryCondition = string.Empty;

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                //2012-3-6 Linda modified into a multi-language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlResults.Visible = false;
                this.pnlBook.Visible = false;
                this.noticeNoLookup.AutIntNo = autIntNo;
                txtRegNo.Focus();
            }
        }

        // 2011-03-11 Added before redirect to dms  need to check ip or host name
        private string GetServerIp()
        {
            string serverIP = string.Empty;
            try
            {
                if (Request.ServerVariables["HTTP_VIA"] != null)
                {
                    serverIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }
                else
                {
                    if (Request.ServerVariables["REMOTE_ADDR"] != null)
                    {
                        serverIP = Request.ServerVariables["REMOTE_ADDR"];
                    }
                }
            }
            catch (Exception ex)
            {
                // string msg = ex.Message;
                throw ex;
            }
            return serverIP;
        }

        private void BindGrid()
        {
            // dls 080314 - need to clear the previous list of notices
            Session["NoticeList"] = null;

            NoticeReportDB notice = new NoticeReportDB(this.connectionString);
            this.pnlResults.Visible = false;

            // Select which data set to generate
            NoticeQueryCriteria criteria = this.GetQueryCriteria();
            if (criteria == null)
            {
                lblError.Visible = true;
                //2012-3-6 Linda modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            //remove slashes and dashes from input value.
            criteria.Value = criteria.Value.Replace("/", "").Replace("-", "");

            // Bind the data
            //dls 090601 - seearch for all notices regardless of the LA into which the user has logged
            //DataSet ds = notice.GetNoticeSearchDS(criteria.AutIntNo, criteria.ColumnName, criteria.Value, criteria.DateSince, minStatus);
            DataSet ds = notice.GetNoticeSearchDS(0, criteria.ColumnName, criteria.Value, criteria.DateSince, criteria.MinStatus);

            //jerry 2010/7/22 add fix officer errors
            List<string> userRoleNameList = new AARTOUserRoleDB(connectionString).GetAARTOUserRoleNameByUserID(
                Convert.ToInt32(Session["userIntNo"]));

            //Role == Supervisor
            string DmsWebUrl = string.Empty;
            string Ip = string.Empty;

            Ip = GetServerIp();

            string HostName = Request.ServerVariables["SERVER_NAME"];
            if (String.IsNullOrEmpty(Ip))
            {
                DmsWebUrl = ConfigurationManager.AppSettings["DMSDomainURL"];
            }
            if (ConfigurationManager.AppSettings["DMSDomainURL"].IndexOf(Ip) >= 0
                || ConfigurationManager.AppSettings["DMSDomainURL"].IndexOf(HostName) >= 0)
            {
                DmsWebUrl = ConfigurationManager.AppSettings["DMSDomainURL"];
            }
            else if (ConfigurationManager.AppSettings["DMSDomainExternalURL"].IndexOf(Ip) >= 0
               || ConfigurationManager.AppSettings["DMSDomainExternalURL"].IndexOf(HostName) >= 0)
            {
                DmsWebUrl = ConfigurationManager.AppSettings["DMSDomainExternalURL"];
            }
            else
            {
                DmsWebUrl = ConfigurationManager.AppSettings["DMSDomainURL"];
            }

            if (ds.Tables[1].Rows.Count > 0 && userRoleNameList.Contains("Supervisor"))
            {
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    //Notice.NotIsOfficerError == true && Charge.ChargeStatus < 900 
                    //&& Charge.ChgPaidDate IS NULL

                    if (Convert.ToBoolean(ds.Tables[1].Rows[i]["NotIsOfficerError"]) &&
                        Convert.ToInt32(ds.Tables[1].Rows[i]["ChargeStatus"].ToString()) < 900 &&
                        string.IsNullOrEmpty(ds.Tables[1].Rows[i]["ChgPaidDate"].ToString()))
                    {
                        string tempBatchDocID = new FrameDB(connectionString).GetFrameDmsDocumentNoByNoticeNotIntNo(
                            Convert.ToInt32(ds.Tables[1].Rows[i]["NotIntNo"]));

                        if (tempBatchDocID != "" && tempBatchDocID.Contains("_"))
                        {
                            tempBatchDocID = tempBatchDocID.Split(new Char[] { '_' })[0];
                        }

                        string baseUrl = String.Format(DmsWebUrl
                            + "/Management/EditDocInputs.aspx?BatchDocumentId={0}&UserName={1}", tempBatchDocID, login);

                        ds.Tables[1].Rows[i]["FixOfficerErrors"] = "<a href = '" + UrlVerify.GetSecretUrl(baseUrl, tempBatchDocID + login) + "' target='blank'>Fix Officer Errors</a>";
                    }
                }
            }

            grvOffenceList.DataSource = ds.Tables[1];
            grvOffenceList.DataKeyNames = new string[] { "NotIntNo", "Representation" };
            if (ds.Tables[1].Rows.Count > 0)
            {
                if (!AuthorityRulesManager.GetIsOrNotShowStatusCodeOfViewOffence_Search(login, (int)ds.Tables[1].Rows[0]["AutIntNo"]))
                {
                    grvOffenceList.Columns[7].Visible = false;
                }

                bool IsCofCT = GetIsCofCT();
                if (IsCofCT)
                {
                    grvOffenceList.Columns[15].Visible = false;
                    grvOffenceList.Columns[16].Visible = true;
                    grvOffenceList.Columns[17].Visible = true;
                }
                else
                {
                    grvOffenceList.Columns[15].Visible = true;
                    grvOffenceList.Columns[16].Visible = false;
                    grvOffenceList.Columns[17].Visible = false;
                }
               
            }
            grvOffenceList.DataBind();
            

            // Check if there are any results
            if (grvOffenceList.Rows.Count == 0)
            {
                if (BindBookGrid())
                {
                    this.pnlResults.Visible = false;
                    this.pnlBook.Visible = true;
                }
                else
                {
                    this.pnlResults.Visible = false;
                    this.pnlBook.Visible = false;
                    lblError.Visible = true;
                    //2012-3-6 Linda modified into a multi-language
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                }
            }
            else
            {
                this.pnlResults.Visible = true;
                this.pnlBook.Visible = false;
            }

            FocusControl();
        }

        public bool IsEnable(object o)
        {
            string filmtype = "";
            if (o != null)
            {
                filmtype = Convert.ToString(o);

            }
            if (filmtype.ToLower() == "m" || filmtype.ToLower() == "h")
            {
                return true;
            }

            return false;

        }

        private void FocusControl()
        {
            switch (SearchCondition.Value.Replace("mypanel1", ""))
            {
                case "RegNo":
                    txtRegNo.Focus();
                    break;
                case "IDNO":
                    txtIDNO.Focus();
                    break;
                case "NoticeNO":
                    txtNoticeNO.Focus();
                    break;
                case "SummonsNO":
                    txtSummonsNO.Focus();
                    break;
                case "CASENO":
                    txtCASENO.Focus();
                    break;
                case "WOANO":
                    txtWOANO.Focus();
                    break;
                case "Name":
                    txtName.Focus();
                    break;
            }
        }

        private bool BindBookGrid()
        {
            bool ret = false;
            NoticeDB db = new NoticeDB(this.connectionString);
            DataSet ds = null;

            if (SearchCondition.Value.Replace("mypanel1", "") == "NoticeNO")
            {
                if (txtNoticeNO.Text.IndexOf("/") > 0)
                {
                    int sequence = 0;
                    string seq = string.Empty;
                    string[] split = txtNoticeNO.Text.Trim().Split('/');
                    if (split.Length == 4 && int.TryParse(split[1],out sequence))
                    {
                        SqlDataReader read = new AuthorityDB(this.connectionString).GetAuthorityDetailsByAutNo(split[2].Trim());
                        if (read != null && read.Read())
                        {
                            AuthorityRulesDetails rule9300 = new AuthorityRulesDetails();
                            rule9300.AutIntNo = (int)read["AutIntNo"];
                            rule9300.ARCode = "9300";
                            rule9300.LastUser = login;
                            DefaultAuthRules authRule = new DefaultAuthRules(rule9300, this.connectionString);
                            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
                            //This is a problem because the Handwritten offences use 00000 numbers for Sequence Number
                            seq = split[1].Trim().PadLeft(value.Key, Convert.ToChar("0"));
                            read.Close();
                        }
                        else
                        {
                            seq = split[1].Trim();
                        }
                        ds = db.GetBMDocument(split[0].Trim(), seq);
                    }
                }
                else
                {
                    if (NoticeNumber.Value.Trim() == txtNoticeNO.Text.Trim())
                    {
                        ds = db.GetBMDocument(Prefix.Value.Trim(), Sequence.Value.Trim());
                    }
                }
            }

            if(ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ret = true;
                grvBookList.DataSource = ds.Tables[0];
                grvBookList.DataBind();
            }

            return ret;
        }

        private int CheckMinimumStatusRule()
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4605";
            //rule.ARComment = "Default = 255 (posted).";
            //rule.ARDescr = "Status to start showing notices on enquiry screen";
            //rule.ARNumeric = 255;
            //rule.ARString = string.Empty;
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
            //db.GetDefaultRule(rule);

            //return rule.ARNumeric;

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "4605";
            rule.LastUser = this.login;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
            return value.Key; //rule.ARNumeric;
        }

        /// <summary>
        /// Get query parameters for the stored procdure NoticeSearch.
        /// 20090417 tf --Add SummonsNO, CASENO, WOANO for search, and replace if else statement by switch statement
        /// 20090421 tf --Replace dojo calendar with ajaxtoolkit calendar. 
        /// </summary>
        /// <returns></returns>
        private NoticeQueryCriteria GetQueryCriteria()
        {
            // Check the authority rule for minimum charge status
            int minStatus = CheckMinimumStatusRule();

            //dls 090614 - there's no point int allowing them to do View All Details when there's only one row
            btnAllDetails.Enabled = true;

            //dls 090612 - replace all autintno with 0 so that we can search across all LA's
            switch (SearchCondition.Value.Replace("mypanel1", ""))
            {
                case "RegNo":
                    //return new NoticeQueryCriteria(this.autIntNo, "RegNo", txtRegNo.Text.Replace(" ", string.Empty), this.dtpSince.Text);
                    if (txtRegNo.Text.Trim().Length == 0)
                        return null;
                    return new NoticeQueryCriteria(0, "RegNo", txtRegNo.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "IDNO":
                    if (txtIDNO.Text.Trim().Length == 0)
                        return null;
                    return new NoticeQueryCriteria(0, "IDNo", txtIDNO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "NoticeNO":
                    btnAllDetails.Enabled = false;
                    if (txtNoticeNO.Text.Trim().Length == 0)
                        return null;
                    return new NoticeQueryCriteria(0, "TicketNo", txtNoticeNO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "SummonsNO":
                    if (txtSummonsNO.Text.Trim().Length == 0)
                        return null;
                    btnAllDetails.Enabled = false;
                    return new NoticeQueryCriteria(0, "SummonsNO", txtSummonsNO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "CASENO":
                    if (txtCASENO.Text.Trim().Length == 0)
                        return null;
                    btnAllDetails.Enabled = false;
                    return new NoticeQueryCriteria(0, "CaseNO", txtCASENO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "WOANO":
                    if (txtWOANO.Text.Trim().Length == 0)
                        return null;
                    btnAllDetails.Enabled = false;
                    return new NoticeQueryCriteria(0, "WOANO", txtWOANO.Text.Replace(" ", string.Empty), this.dtpSince.Text, minStatus);
                case "Name":
                    if (txtName.Text.Trim().Length == 0)
                        return null;
                    return new NoticeQueryCriteria(0, "Names", txtInitials.Text + "*" + txtName.Text, this.dtpSince.Text, minStatus);
            }
            return null;


            //if (!txtRegNo.Text.Trim().Equals(string.Empty))
            //    return new NoticeQueryCriteria(this.autIntNo, "RegNo", txtRegNo.Text.Replace(" ", string.Empty), this.dtpSince == null ? string.Empty : this.dtpSince.ToString(DATE_FORMAT));
            //else if (!txtTicketNo.Text.Trim().Equals(string.Empty))
            //    return new NoticeQueryCriteria(this.autIntNo, "TicketNo", txtTicketNo.Text.Replace(" ", string.Empty), this.dtpSince == null ? string.Empty : this.dtpSince.ToString(DATE_FORMAT));
            //else if (!txtIDNo.Text.Trim().Equals(string.Empty))
            //    return new NoticeQueryCriteria(this.autIntNo, "IDNo", txtIDNo.Text.Replace(" ", string.Empty), this.dtpSince == null ? string.Empty : this.dtpSince.ToString(DATE_FORMAT));
            //else if (!txtSurname.Text.Equals(string.Empty) || !txtInitials.Text.Equals(string.Empty))
            //    return new NoticeQueryCriteria(this.autIntNo, "Names", txtInitials.Text + "*" + txtSurname.Text, this.dtpSince == null ? string.Empty : this.dtpSince.ToString(DATE_FORMAT));
            //else
            //    return null;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.BindGrid();
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            NoticeQueryCriteria criteria = this.GetQueryCriteria();
            if (criteria == null)
                return;

            Session.Add("NoticeQueryCriteria", criteria);
            Helper_Web.BuildPopup(this, "ViewOffence_Report.aspx", null);
            BindGrid();
        }

        protected void btnOutstandingReport_Click(object sender, EventArgs e)
        {
            NoticeQueryCriteria criteria = this.GetQueryCriteria();
            if (criteria == null)
                return;

            Session.Add("NoticeQueryCriteria", criteria);
            //Helper_Web.BuildPopup(this, "ViewOutstandingFines_Report.aspx", null);
            Helper_Web.BuildPopup(this, "OutstandingFinesReport_ReportViewer.aspx", null);

            BindGrid();
        }

        protected void btnAllDetails_Click(object sender, EventArgs e)
        {
            //changes made by Barry Dickson started 200305 as per Rev 3803

            NoticeQueryCriteria criteria = this.GetQueryCriteria();
            if (criteria == null)
                return;

            Session.Add("NoticeQueryCriteria", criteria);

            bool f = GetIsCofCT();
            if (f)
            {
               Helper_Web.BuildPopup(this, "ViewOffenceDetailAll_CTReport.aspx", null);
               // Helper_Web.BuildPopup(this, "ViewOffenceDetailAll_Report.aspx", null);

            }
            else
            {   //int notIntNo = 5256;
                Helper_Web.BuildPopup(this, "ViewOffenceDetailAll_Report.aspx", null);
            }

            BindGrid();
        }

        //protected void grvOffenceList_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //}

        protected void grvOffenceList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            DataRowView s = (DataRowView)e.Row.DataItem;


            if (s != null)
            {
                if (s["Representation"].ToString() == "0")
                {
                    //e.Row.Cells[14].Enabled = false;
                   
                   e.Row.Cells[18].Enabled = false;//2012-12-07 update by Nancy
                  

                }
                if (s["HasComment"].ToString() == "Y")
                {//2012-12-07 add by Nancy
                    e.Row.Cells[12].Style.Add("background-color", "Yellow");
                }

                if (IsEnable(s["NotFilmType"]))
                {
                    e.Row.Cells[14].Enabled = true;//2012-12-10 add by Heidi
                    e.Row.Cells[13].Enabled = false;
                }
                else
                {
                    e.Row.Cells[13].Enabled = true;//2012-12-10 add by Heidi
                    e.Row.Cells[14].Enabled = false;
                }

            }
        }

        private bool GetIsCofCT()
        {
            bool f = false;
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            string autCode = AuthorityManager.GetAutCode(autIntNo);
            if (autCode.ToLower().Trim() == "cp")
            {
                f = true;
            }
            return f;
        }
        protected void grvOffenceList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string sValue = e.CommandName;
            
            grvOffenceList.SelectedIndex = Convert.ToInt32(e.CommandArgument);
            int nRow = grvOffenceList.SelectedIndex;

            //int notIntNo = (int)this.grvOffenceList.DataKeys[nRow].Value;
            int notIntNo = (int)this.grvOffenceList.DataKeys[nRow].Values["NotIntNo"];

            this.Session.Add("notIntNo", notIntNo);

            string sTicketNo = grvOffenceList.Rows[nRow].Cells[1].Text;

            if (sValue.Equals("Report"))
            {
                // PDF
                //GridViewRow row = grvOffenceList.Rows[nRow];
                //string ticketNo = row.Cells[1].Text;
                if (ReversibleNotice.IsReversible(sTicketNo))
                {
                    //2012-3-6 Linda modified into a multi-language
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), sTicketNo);
                    return;
                }

                Helper_Web.BuildPopup(this, "ViewOffenceDetail_Report.aspx", "NotIntNo", notIntNo.ToString());
            }
            else if (sValue.Equals("EnquiryReport"))
            {
                // PDF
                //GridViewRow row = grvOffenceList.Rows[nRow];
                //string ticketNo = row.Cells[1].Text;
                if (ReversibleNotice.IsReversible(sTicketNo))
                {
                    //2012-3-6 Linda modified into a multi-language
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), sTicketNo);
                    return;
                }

                Helper_Web.BuildPopup(this, "ViewOffenceDetail_EnquiryReport.aspx", "NotIntNo", notIntNo.ToString());
            }
            else if (sValue.Equals("HistoryReport"))
            {
                // PDF
                //GridViewRow row = grvOffenceList.Rows[nRow];
                //string ticketNo = row.Cells[1].Text;
                if (ReversibleNotice.IsReversible(sTicketNo))
                {
                    //2012-3-6 Linda modified into a multi-language
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), sTicketNo);
                    return;
                }

                Helper_Web.BuildPopup(this, "ViewOffenceDetail_HistoryReport.aspx", "NotIntNo", notIntNo.ToString());
            }
            else if (sValue.Equals("Select"))
            {
                // HTML
                if (ReversibleNotice.IsReversible(sTicketNo))
                {
                    //2012-3-6 Linda modified into a multi-language
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text2"), sTicketNo);
                    return;
                }

                //dls 080314 - they want to view all notices based on the search criteria

                NoticeQueryCriteria criteria = this.GetQueryCriteria();
                if (criteria == null)
                    return;

                Session.Add("NoticeQueryCriteria", criteria);

                Helper_Web.BuildPopup(this, "ViewOffence_Detail.aspx", null);
            }
            else if (sValue.Equals("Document"))
            {
                 //2012-12-7 Heidi add a function to see non-camera image
                this.Session.Add("_notIntNo", notIntNo);
                Helper_Web.BuildPopup(this, "NonCamera.aspx", "NotIntNo", notIntNo.ToString());



            }
            else if (sValue.Equals("Letter"))
            {
                int repIntNo = (int)this.grvOffenceList.DataKeys[nRow].Values["Representation"];
                //string letterTo = grvOffenceList.Rows[nRow].Cells[13].Text.Substring(0, 1);
                //string letterTo = grvOffenceList.Rows[nRow].Cells[13].Text.Substring(0, 1);

                //Edited by Jacob 201211220 start
                // letter send to column index is not 15
             
                //string letterTo = grvOffenceList.Rows[nRow].Cells[15].Text.Substring(0, 1);
                string letterToText = "";
                string letterTo = "";
               
                letterToText = grvOffenceList.Rows[nRow].Cells[19].Text;
               
                letterTo = letterToText.Substring(0, 1);
                
                //Edited by Jacob 201211220 end

                //Helper.BuildPopup(this, "Representation_LetterViewer.aspx", "-1", repIntNo, "LetterTo", letterTo, "TicketNo", sTicketNo);
                //Helper_Web.BuildPopup(this, "Representation_LetterViewer.aspx", "RepIntNo", repIntNo.ToString(), "LetterTo", letterTo);

                //dls 080519 - need to add the ticket no back into the search so that we can verify whether its a summons/normal rep
                Helper_Web.BuildPopup(this, "Representation_LetterViewer.aspx", "RepIntNo", repIntNo.ToString(), "LetterTo", letterTo, "TicketNo", sTicketNo);
            }
                //Oscar 20120731 added
            else if (sValue.Equals("Notes",StringComparison.OrdinalIgnoreCase))
            {
                var aartoDomain = string.Empty;
                var hostName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"] ?? string.Empty;
                var ip = string.Empty;
                if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    ip = HttpContext.Current.Request.ServerVariables["HTTP_VIA"].ToString(CultureInfo.InvariantCulture);
                else if (HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] != null)
                    ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString(CultureInfo.InvariantCulture);

                if (string.IsNullOrWhiteSpace(ip))
                    aartoDomain = ConfigurationManager.AppSettings["AARTODomainURL"];
                else if (ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(ip, StringComparison.OrdinalIgnoreCase) >= 0
                         || ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(hostName, StringComparison.OrdinalIgnoreCase) >= 0
                    )
                    aartoDomain = ConfigurationManager.AppSettings["AARTODomainURL"];
                else if (ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(ip, StringComparison.OrdinalIgnoreCase) >= 0
                         || ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(hostName, StringComparison.OrdinalIgnoreCase) >= 0
                    )
                    aartoDomain = ConfigurationManager.AppSettings["AARTODomainExternalURL"];
                Helper_Web.BuildPopup(this, aartoDomain + "/Enquiry/NoticeCommentViewer?id=" + notIntNo);
            }
            this.BindGrid();
        }

        /// <summary>
        /// Clicked the lookup button of noticeLookup control .
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void NoticeLookup(object sender, EventArgs e)
        {
            this.txtNoticeNO.Text = noticeNoLookup.Return;
            Prefix.Value = noticeNoLookup.Prefix;
            Sequence.Value = noticeNoLookup.Sequence.ToString();
            NoticeNumber.Value = noticeNoLookup.Return;
            SearchCondition.Value = "mypanel1NoticeNO";
            BindGrid();
        }

    }
}

