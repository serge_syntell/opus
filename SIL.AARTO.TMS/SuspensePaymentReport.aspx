<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.SuspensePaymentReport" Codebehind="SuspensePaymentReport.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;</p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                        <table style="border-style: none;" class="Normal">
                            <tr>
                                <td class="NormalBold">
                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblDate.Text %>"></asp:Label></td>
                                <td>
<asp:TextBox runat="server" ID="dtpDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False"                                                 />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="dtpDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqDate.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>  </td>
                            </tr>
                            <tr>
                                <td class="NormalBold">
                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblEndDate.Text %>"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="dtpEndDate" runat="server" autocomplete="off" 
                                        CssClass="Normal" Height="20px" UseSubmitBehavior="False" />
                                    <cc1:CalendarExtender ID="dtpEndDate_CalendarExtender" runat="server" 
                                        Format="yyyy-MM-dd" TargetControlID="dtpEndDate">
                                    </cc1:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                        ControlToValidate="dtpEndDate" CssClass="NormalRed" Display="Dynamic" 
                                        ErrorMessage="<%$Resources:reqDate.ErrorMessage %>" 
                                        ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
 <tr>
                            <td class="NormalBold">
                                <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblShowEasypayOnly.Text %>"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkShowEasypayOnly" runat="server" CssClass="NormalBold" Text="">
                                </asp:CheckBox>
                            </td>
                        </tr>
                            <tr>
                                <td colspan="2" style="text-align: right;">
                                    <asp:Button ID="btnShow" Text="<%$Resources:btnShow.Text %>" runat="server" CssClass="NormalButton" Width="70px"
                                        OnClick="btnShow_Click" /></td>
                            </tr>
                        </table>
                    </asp:Panel></ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
