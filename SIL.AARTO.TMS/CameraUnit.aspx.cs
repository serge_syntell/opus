using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing; 

//dls 061121 - remove MtrIntNo and CameraUnitID (becomes separate CamUnitID table) from CameraUnit table 
namespace Stalberg.TMS 
{

	public partial  class CameraUnit : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;

		protected string thisPageURL = "CameraUnit.aspx";
        //protected string thisPage = "Camera Unit Maintenance";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];

			loginUser = userDetails.UserLoginName;

			//int 
			int	autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddCameraUnit.Visible = false;
				pnlUpdateCameraUnit.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

                PopulateAuthorites(ddlSelectLA,  autIntNo);

                if (autIntNo > 0)
                {
                    BindGrid(autIntNo);
                }
                else
                {
                    //Modifed by henry 2012-1-17
                    //Mulity language resources lblError.Text1-lblError-Text14
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                    lblError.Visible = true;
                }
			}		
		}

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorites(DropDownList ddlAuthority, int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlAuthority.DataSource = data;
            //ddlAuthority.DataValueField = "AutIntNo";
            //ddlAuthority.DataTextField = "AutName";
            //ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        protected void BindGrid(int autIntNo)
		{
			CameraUnitDB camList = new CameraUnitDB(connectionString);
            
            DataSet data = camList.GetCameraUnitListDS(autIntNo);
			dgCameraUnit.DataSource = data;
			dgCameraUnit.DataKeyField = "CamUnitID";
			dgCameraUnit.DataBind();
			
			if (dgCameraUnit.Items.Count == 0)
			{
				dgCameraUnit.Visible = false;
				lblError.Visible = true;
				lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
			}
			else
			{
				dgCameraUnit.Visible = true;
			}

			data.Dispose();
			pnlAddCameraUnit.Visible = false;
			pnlUpdateCameraUnit.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddCameraUnit.Visible = true;
			pnlUpdateCameraUnit.Visible = false;

			btnOptDelete.Visible = false;
		}

		protected void btnAddCameraUnit_Click(object sender, System.EventArgs e)
		{
            if (ddlSelectLA.SelectedIndex < 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }

			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

			// add the new transaction to the database table
			CameraUnitDB toAdd = new CameraUnitDB(connectionString);
			
            int success = toAdd.AddCameraUnit(autIntNo, txtAddCamUnitID.Text, "Y", loginUser);

            if (success < 0)
			{
				lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
			}
			else
			{
				lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraUnitsMaintenance, PunchAction.Add);  
			}

			lblError.Visible = true;

            BindGrid(autIntNo);
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
            if (ddlSelectLA.SelectedIndex < 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                return;
            }

            string active = "N";

            if (chkActive.Checked) active = "Y";

			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

			// add the new transaction to the database table
			CameraUnitDB camUpdate = new CameraUnitDB(connectionString);
			
            int success = camUpdate.UpdateCameraUnit(autIntNo, txtCamUnitID.Text, active, loginUser);

            if (success < 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7"); 
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraUnitsMaintenance, PunchAction.Change);  

			}

			lblError.Visible = true;

            BindGrid(autIntNo);
        }

		protected void dgCameraUnit_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgCameraUnit.SelectedIndex = e.Item.ItemIndex;

			if (dgCameraUnit.SelectedIndex > -1) 
			{
                string editCamUnitID = dgCameraUnit.DataKeys[dgCameraUnit.SelectedIndex].ToString();

                Session["editCamUnitID"] = editCamUnitID;
				Session["prevPage"] = thisPageURL;

                ShowCameraUnitDetails(editCamUnitID);
			}
		}

        protected void ShowCameraUnitDetails(string editCamUnitID)
		{
			// Obtain and bind a list of all users
			CameraUnitDB camera = new CameraUnitDB(connectionString);

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            CameraUnitDetails camDetails = camera.GetCameraUnitDetails(autIntNo, editCamUnitID);

			txtCamUnitID.Text = camDetails.CamUnitID;

            if (camDetails.Active.Equals("Y"))
                chkActive.Checked = true;
            else
                chkActive.Checked = false;

			pnlUpdateCameraUnit.Visible = true;
			pnlAddCameraUnit.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

	
		protected void dgCameraUnit_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgCameraUnit.CurrentPageIndex = e.NewPageIndex;

			int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            BindGrid(autIntNo);
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
            string editCamUnitID = Session["editCamUnitID"].ToString();
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

			CameraUnitDB camera = new Stalberg.TMS.CameraUnitDB(connectionString);

            int success = camera.DeleteCameraUnit(autIntNo, editCamUnitID);


            switch (success)
            {
                case 0:
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.CameraUnitsMaintenance, PunchAction.Delete);  
                    break;
                case -1:
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                    break;
                case -2:
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                    break;
                case -3:
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                     break;
                case -4:
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                    break;
                default:
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                    break;
            }			

            lblError.Visible = true;

            BindGrid(autIntNo);
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgCameraUnit.Visible.Equals(true))
			{
				//hide it
				dgCameraUnit.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptShow.Text");
			}
			else
			{
				//show it
				dgCameraUnit.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            dgCameraUnit.Visible = true;
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");

            BindGrid(Convert.ToInt32(ddlSelectLA.SelectedValue));
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

	}
}
