using System;
using System.Collections;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.IO;
using Stalberg.TMS.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the main login screen for the application
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "Login.aspx";
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string keywords = string.Empty;
        protected string environment = "P";
        //protected string thisPage = "Login";

        private string GetServerIp()
        {
            string serverIP = string.Empty;
            try
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                {
                    serverIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    if (HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] != null)
                    {
                        serverIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
               // string msg = ex.Message;
                throw ex;
            }
            return serverIP;
        }


        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            string AartoWebUrl = string.Empty;
            string Ip = string.Empty;
            try
            {
                Ip = GetServerIp();
            }
            catch (Exception ex)
            {
                //Application["ErrorMessage"] = String.Format("Error Message:{0}", ex.Message);
                SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(ex, "Error", "TMS");
                //Response.Redirect("Error.aspx");
            }
            string HostName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"]; 
            if (String.IsNullOrEmpty(Ip))
            {
                AartoWebUrl = ConfigurationManager.AppSettings["AARTODomainURL"];
            }
            else if (ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(Ip) >= 0
                || ConfigurationManager.AppSettings["AARTODomainURL"].IndexOf(HostName) >= 0)
            {
                AartoWebUrl = ConfigurationManager.AppSettings["AARTODomainURL"];
            }
            else if (ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(Ip) >= 0
               || ConfigurationManager.AppSettings["AARTODomainExternalURL"].IndexOf(HostName) >= 0)
            {
                AartoWebUrl = ConfigurationManager.AppSettings["AARTODomainExternalURL"];
            }
            else
            {
                Application["ErrorMessage"] = String.Format((string)GetLocalResourceObject("ErrorMessage"));
                Response.Redirect("Error.aspx");
            }


            if (Request.QueryString["Login"].Equals("logout"))
            {
                //                //clear session variables
                Session.Abandon();
                AartoWebUrl += "/Home/Top?Login=logout";
            }
            else
            {
                AartoWebUrl += "/Account/Login";
            }

            Response.Redirect(AartoWebUrl);
            // This ensures that if the stylesheet is not loaded we reload the Home.aspx (we dont get the Login screen with email address etc)
            // LMZ 2007-08-03
            //if (Session["drStyleSheet"] == null)
            //    Response.Redirect ("Home.aspx");

            //this.connectionString = Application["constr"].ToString();

            ////set domain specific variables
            //General gen = new General();
            //backgroundImage = gen.SetBackground(Session["drBackground"]);
            //styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            //title = gen.SetTitle(Session["drTitle"]);

            //if (!Page.IsPostBack)
            //{
            //    this.GetSystemParameters();


            //    imgLogo.ImageUrl = "logos/Syntell_logo1.jpg";

            //    //pick up query string values
            //    NameValueCollection lQString = Request.QueryString;

            //    // Get names of all keys into a string array.
            //    String[] lKeyArray = lQString.AllKeys;
            //    if (lKeyArray.Length > 0)
            //    {
            //        if (!(Request.QueryString["Login"] == null))
            //        {
            //            //check whether customer has been redirected because they have logged out
            //            if (Request.QueryString["Login"].Equals("logout"))
            //            {
            //                //clear session variables
            //                Session.Abandon();

            //                // FBJ Added (2007-02-01): Now handled in Session_OnEnd in Global.asax
            //                //int userIntNo = Convert.ToInt32(Session["userIntNo"]);
            //                //Session.Contents.Clear();

            //                ////clear all entries for user in application variable
            //                //if (Application["ConfirmFilm"] != null)
            //                //    this.ClearConfirmFilmList(userIntNo);

            //                //Close Window
            //                // Updated LMZ 18-06-2007
            //                //String strRedirectScript = @"<script language =javascript>
            //                //window.close();window.opener.close();
            //                //</script> ";

            //                // Add our JavaScript to the currently rendered page
            //                //Response.Write(strRedirectScript);
            //            }
            //        }
            //    }

            //    //display login labels and textboxes depending on 
            //    //session variables from DomainRules table
            //    if (Session["drLoginReq"] != null)
            //    {
            //        if (Session["drLoginReq"].ToString().Equals("Y"))
            //        {
            //            lblLoginName.Visible = true;
            //            txtLoginName.Visible = true;
            //        }
            //        else
            //        {
            //            lblLoginName.Visible = false;
            //            txtLoginName.Visible = false;
            //        }
            //    }
            //    else
            //    {
            //        lblLoginName.Visible = false;
            //        txtLoginName.Visible = false;
            //    }

            //    if (Session["drEmailReq"] != null)
            //    {
            //        if (Session["drEmailReq"].ToString().Equals("Y"))
            //        {
            //            lblEmail.Visible = true;
            //            txtEmail.Visible = true;
            //        }
            //        else
            //        {
            //            lblEmail.Visible = false;
            //            txtEmail.Visible = false;
            //        }
            //    }
            //    else
            //    {
            //        lblLoginName.Visible = false;
            //        txtLoginName.Visible = false;
            //    }

            //    if (Session["drPasswdReq"] != null)
            //    {
            //        if (Session["drPasswdReq"].ToString().Equals("Y"))
            //        {
            //            lblPasswd.Visible = true;
            //            txtPasswd.Visible = true;
            //        }
            //        else
            //        {
            //            lblPasswd.Visible = false;
            //            txtPasswd.Visible = false;
            //        }
            //    }
            //    else
            //    {
            //        lblLoginName.Visible = false;
            //        txtLoginName.Visible = false;
            //    }

            //    if (Session["drAuthorityReq"] != null)
            //    {
            //        if (Session["drAuthorityReq"].ToString().Equals("Y"))
            //        {
            //            lblAuthority.Visible = true;
            //            ddlSelAuthority.Visible = true;

            //            PopulateAuthorityList();
            //        }
            //        else
            //        {
            //            lblAuthority.Visible = false;
            //            ddlSelAuthority.Visible = false;
            //        }
            //    }
            //    else
            //    {
            //        lblAuthority.Visible = false;
            //        ddlSelAuthority.Visible = false;
            //    }

            //    if (environment.Equals("D"))
            //    {
            //        txtLoginName.AutoCompleteType = AutoCompleteType.None;
            //        txtPasswd.AutoCompleteType = AutoCompleteType.None;
            //        txtEmail.AutoCompleteType = AutoCompleteType.None;
            //    }
            //    else
            //    {
            //        txtLoginName.AutoCompleteType = AutoCompleteType.Disabled;
            //        txtPasswd.AutoCompleteType = AutoCompleteType.Disabled;
            //        txtEmail.AutoCompleteType = AutoCompleteType.Disabled;
            //    }



            //}
        }

        private void ShowUNCTestImage()
        {
            // Get image file servers
            ImageFileServerDB imageFileServerDB = new ImageFileServerDB(connectionString);
            List<ImageFileServerDetails> fileServerList = imageFileServerDB.GetAllImageFileServers();
            if (fileServerList.Count > 0)
            {
                rptTestImages.ItemDataBound += new RepeaterItemEventHandler(rptTestImages_ItemDataBound);
                rptTestImages.DataSource = fileServerList;
                rptTestImages.DataBind();
            }
        }

        void rptTestImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ImageFileServerDetails fileServer = (ImageFileServerDetails)e.Item.DataItem;
            string strRootPath = @"\\" + fileServer.ImageMachineName + Path.DirectorySeparatorChar
                    + fileServer.ImageShareName;

            string strTestImagePath = strRootPath + Path.DirectorySeparatorChar + "test.gif";
            string strTestImagePath2 = strRootPath + Path.DirectorySeparatorChar + "test.jpg";

            WebService service = new WebService();

            //bool isConnected = service.RemoteConnect(fileServer.ImageMachineName, fileServer.ImageShareName, fileServer.RemoteUserName, fileServer.RemotePassword);
            //if (isConnected)
            //{
            System.Web.UI.WebControls.Image imgUNCTestImage = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgUNCTestImage");
            imgUNCTestImage.ToolTip = strRootPath;

            // check the test image in server
            if (File.Exists(strTestImagePath) || File.Exists(strTestImagePath2))
            {
                imgUNCTestImage.ImageUrl = "image.aspx?IFSIntNo=" + fileServer.IFSIntNo;
            }
            else
            {
                //isConnected = false;
                imgUNCTestImage.ImageUrl = "Images/NoImage.jpg";
            }
            //}

            // Set a flag about AllImageFileServerAccessible to protect image related pages.
            //if (Session["AllImageFileServerAccessible"] == null)
            //{
            //    Session["AllImageFileServerAccessible"] = isConnected;
            //}
            //else if (isConnected == false)
            //{
            //    Session["AllImageFileServerAccessible"] = false;
            //}
        }

        //private void GetSystemParameters()
        //{
        //    if (Application["sysParamList"] != null)
        //    {
        //        SysParamDB sysParam = new SysParamDB(this.connectionString);

        ////        for (int i = 0; i < sysParamList.Count; i++)
        ////        {
        ////            SysParamDetails spDetails = (SysParamDetails)sysParamList[i];

        ////            if (spDetails.SPColumnName.Equals("EnvironmentType"))
        ////            {
        ////                environment = spDetails.SPStringValue;
        ////                return;
        ////            }
        ////        }
        ////    }
        ////}

        //            if (spDetails.SPColumnName.Equals("EnvironmentType"))
        //            {
        //                _environment = spDetails.SPStringValue;
        //                return;
        //            }                    
        //        }               
        //    }
        //}

        //private void ClearConfirmFilmList(int userIntNo)
        //{
        //    //get current list of films in use from Application variables
        //    ArrayList confirmFilm = (ArrayList)Application["ConfirmFilm"];

        //    if (confirmFilm != null)
        //    {
        //        for (int i = 0; i < confirmFilm.Count; i++)
        //        {
        //            //check whether this user has any outstanding films for confirmation
        //            FilmToConfirm confirm = (FilmToConfirm)confirmFilm[i];

        //            int user = confirm.GetUser();

        //            if (user == userIntNo)
        //            {
        //                //remove current film that is linked to user
        //                confirmFilm.RemoveAt(i);
        //            }
        //        }
        //    }

        //    Application.Lock();
        //    Application["ConfirmFilm"] = confirmFilm;
        //    Application.UnLock();
        //}

        //protected void PopulateAuthorityList()
        //{
        //    AuthorityDB auth = new AuthorityDB(connectionString);

        //    SqlDataReader reader = auth.GetAuthorityList(0, "AutName");
        //    ddlSelAuthority.DataSource = reader;
        //    ddlSelAuthority.DataValueField = "AutIntNo";
        //    ddlSelAuthority.DataTextField = "AutDescr";
        //    ddlSelAuthority.DataBind();
        //    ddlSelAuthority.Items.Insert(0, "Select a local authority");

        //    reader.Close();
        //}

        protected void btnSubmit_Click(object sender, System.EventArgs e)
        {
            //    lblError.Visible = false;

            //    //check textboxes depending on session variables from DomainRules table
            //    if (Session["drLoginReq"] != null)
            //    {
            //        if (Session["drLoginReq"].ToString().Equals("Y"))
            //        {
            //            if (txtLoginName.Text.Equals(""))
            //            {
            //                lblError.Visible = true;
            //                lblError.Text = "Please enter your login name";
            //                return;
            //            }
            //        }
            //    }

            //    if (Session["drEmailReq"] != null)
            //    {
            //        if (Session["drEmailReq"].ToString().Equals("Y"))
            //        {
            //            if (txtEmail.Text.Equals(""))
            //            {
            //                lblError.Visible = true;
            //                lblError.Text = "Please enter your email address";
            //                return;
            //            }
            //        }
            //    }

            //    if (Session["drPasswdReq"] != null)
            //    {
            //        if (Session["drPasswdReq"].ToString().Equals("Y"))
            //        {
            //            if (txtPasswd.Text.Equals(""))
            //            {
            //                lblError.Visible = true;
            //                lblError.Text = "Please enter your password";
            //                return;
            //            }
            //        }
            //    }

            //    int autIntNo = 0;
            //    int mtrIntNo;

            //    if (Session["drAuthorityReq"] != null)
            //    {
            //        if (Session["drAuthorityReq"].ToString().Equals("Y"))
            //        {
            //            if (ddlSelAuthority.SelectedIndex < 1)
            //            {
            //                lblError.Visible = true;
            //                lblError.Text = "Please select your local authority";
            //                return;
            //            }

            //            autIntNo = Convert.ToInt32(ddlSelAuthority.SelectedValue);
            //        }
            //    }

            //    //set up encryption class
            //    Rc4Encrypt.clsRc4Encrypt rc4 = new clsRc4Encrypt();
            //    rc4.Password = "sacompconsult";
            //    rc4.PlainText = txtPasswd.Text;

            //    //check that login details match those in table
            //    UserDB user = new UserDB(connectionString);

            //    int userIntNo = user.Login(txtLoginName.Text, txtEmail.Text, rc4.EnDeCrypt(), autIntNo);

            //    //dls 070508 - clear session variables in case previous user didn't log out properly
            //    string url = Session["homeUrl"].ToString();

            ////need to save this value, because it is going to get cleared
            //bool isConnected= (bool)Session["AllImageFileServerAccessible"];

            //Session.RemoveAll();

            //Session["AllImageFileServerAccessible"] = isConnected;

            ////redo all settings / environment calls
            //Session["homeUrl"] = url;

            //    GetDomainSettings(url);
            //    SetDomainSettings();

            //    if (userIntNo == 0)
            //    {
            //        lblError.Visible = true;
            //        lblError.Text = "Invalid login details - login denied!";
            //        return;
            //    }

            //    // LMZ 14-03-2007 - used for officer statistics
            //    int nUSIntNo = 0;
            //    if (Session["USIntNo"] == null)
            //        nUSIntNo = user.UserShiftUpdate(userIntNo, txtLoginName.Text);

            //    // LMZ 22-03-2007 - used to get assosciated traffic officer number
            //    string sOfficerNo = user.GetOfficerNo (userIntNo);

            //    //pass userIntNo to session variables
            //    Session["userIntNo"] = userIntNo;
            //    Session["autIntNo"] = autIntNo;
            //    Session["USIntNo"] = nUSIntNo;  
            //    Session["OfficerNo"] = sOfficerNo;

            //    //dls 030730 - added this at login
            //    user.UserShiftEdit(nUSIntNo, sOfficerNo, "Officer");

            //    //20090113 SD	
            //    //AutIntNo, ARCode and LastUser need to be set from here
            //    AuthorityRulesDetails details = new AuthorityRulesDetails();
            //    details.AutIntNo = autIntNo;
            //    details.ARCode = "0400";
            //    details.LastUser = txtLoginName.Text;

            //    DefaultAuthRules authRule = new DefaultAuthRules(details, this.connectionString);
            //    KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            //    //this.Session.Add("NaTISLast", details.ARString.Equals("N") ? false : true);
            //    this.Session.Add("NaTISLast", value.Value.Equals("N") ? false : true);

            //    //dls 070120 - add rules for showing cross-hairs
            //    //details = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "3100", "Display cross-hairs for DigiCam images", 0, "Y", "Y = Yes (Default); N = No", txtLoginName.Text);

            // david lin 2010-03-10 after task 3673 remove images from database, this flag in session was useless.
            ////dls 070813 - add rule to check file system for images at Verification/Adjudication
            //details = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "2540", "Rule for saving images to File System for Adjudication/Verification",
            //            0, "N", "Y = Yes; N = No (Default)", this.txtLoginName.Text);

            //this.Session.Add("CheckFileSystemForImages", details.ARString.Equals("Y") ? true : false);


            ////determine metro parent of local authority
            //AuthorityDB authority = new AuthorityDB(connectionString);
            //AuthorityDetails authDet = authority.GetAuthorityDetails(autIntNo);

            //    //details = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "3150", "Style of DigiCam cross-hairs", 0, "", "0 = Standard yellow cross (Default); 1 = Yellow Cross with missing centre; 2 = Standard inverted cross; 3 = Inverted cross with missing centre", txtLoginName.Text);
            //    AuthorityRulesDetails details3150 = new AuthorityRulesDetails();
            //    details.AutIntNo = autIntNo;
            //    details.ARCode = "3150";
            //    details.LastUser = txtLoginName.Text;

            //    DefaultAuthRules authRule3150 = new DefaultAuthRules(details, this.connectionString);
            //    KeyValuePair<int, string> value3150 = authRule.SetDefaultAuthRule();

            //    //this.Session.Add("CrossHairStyle", details.ARNumeric);
            //    this.Session.Add("CrossHairStyle", value3150.Key);

            //    //dls 070813 - add rule to check file system for images at Verification/Adjudication
            //   // details = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "2540", "Rule for saving images to File System for Adjudication/Verification",
            //     //           0, "N", "Y = Yes; N = No (Default)", this.txtLoginName.Text);

            //    AuthorityRulesDetails details2540 = new AuthorityRulesDetails();
            //    details.AutIntNo = autIntNo;
            //    details.ARCode = "2540";
            //    details.LastUser = txtLoginName.Text;

            //    DefaultAuthRules authRule2540 = new DefaultAuthRules(details, this.connectionString);
            //    KeyValuePair<int, string> value2540 = authRule.SetDefaultAuthRule();

            //    bool checkFileSystemForImages = value2540.Value.Equals("Y") ? true : false;
            //    //this.Session.Add("CheckFileSystemForImages", value2540.Value);
            //    this.Session.Add("CheckFileSystemForImages", checkFileSystemForImages);

            //    //determine metro parent of local authority
            //    AuthorityDB authority = new AuthorityDB(connectionString);
            //    AuthorityDetails authDet = authority.GetAuthorityDetails(autIntNo);

            //    mtrIntNo = authDet.MtrIntNo;
            //    Session["mtrIntNo"] = mtrIntNo;
            //    Session["TicketProcessor"] = authDet.AutTicketProcessor;

            //    // get the user authority levels, etc
            //    Stalberg.TMS.UserDetails userDetails = user.GetUserDetails(userIntNo);

            //    Session["userDetails"] = userDetails;
            //    Session["autName"] = authDet.AutName;
            //    //dls 080108 - added
            //    Session["autNo"] = authDet.AutNo;

            //    //need to determine which menu to show the user
            //    SqlDataReader result = user.GetUserMenuName(userIntNo);

            //    while (result.Read())
            //    {
            //        Session["menuName"] = result["MenuName"].ToString();
            //        Session["menuIntNo"] = Convert.ToInt32(result["MenuIntNo"]);
            //    }

            //    result.Close();

            //    //default metro level access to No
            //    Session["metroAccess"] = "N";

            //    //load all authorities into an arraylist and store in session variable
            //    ArrayList ugaList = new ArrayList();

            //    //need to find out which authorities user has access to
            //    //			if (Session["drAuthorityReq"] != null)
            //    //			{
            //    //				if (Session["drAuthorityReq"].ToString().Equals("Y"))
            //    //				{
            //    result = user.GetAuthorityListForUser(userIntNo);

            //    if (result.HasRows)
            //    {
            //        while (result.Read())
            //        {
            //            UserGroup_AuthDetails ugaDetails = new UserGroup_AuthDetails();

            //            ugaDetails.AutIntNo = Convert.ToInt32(result["AutIntNo"]);
            //            ugaDetails.UGIntNo = Convert.ToInt32(result["UGIntNo"]);
            //            ugaDetails.UGAMetro = result["UGAMetro"].ToString();

            //            //if at least one of the rows has Metro level access = Y, set metro access
            //            if (ugaDetails.UGAMetro.Equals("Y"))
            //                Session["metroAccess"] = "Y";

            //            //store UserGroup identity key as a session variable
            //            if (ugaDetails.AutIntNo == autIntNo || autIntNo == 0)
            //                Session["ugIntNo"] = ugaDetails.UGIntNo;

            //            ugaList.Add(ugaDetails);
            //        }
            //    }
            //    else
            //    {
            //        Session["ugIntNo"] = 0;
            //    }

            //    result.Close();

            //    Session["ugaDetails"] = ugaList;
            //    //				}
            //    //			}

            //    if (Session["menuName"].ToString().Equals("WebARS"))
            //        Server.Transfer("WebARSDefault.aspx");
            //    else
            //        //re-direct user to Welcome page within domain
            //        Server.Transfer("Default.aspx");
            //}

            //protected void SetDomainSettings()
            //{

            //    //set domain specific variables
            //    General gen = new General();

            //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);

            //    //these have to get specifically set on the home page, as they are not populated earlier
            //    title = gen.SetTitle(Session["drTitle"]);
            //    description = gen.SetDescription(Session["drDescription"]);
            //    keywords = gen.SetKeywords(Session["drKeywords"]);

            //}

            //protected void GetDomainSettings(string url)
            //{
            //    //check rules and login requirements for domain
            //    DomainRuleDB domainRule = new DomainRuleDB(connectionString);
            //    SqlDataReader domainDetails = domainRule.GetDRDetailsByURL(url);

            //    while (domainDetails.Read())
            //    {
            //        Session["drLoginReq"] = domainDetails["DRLoginReq"].ToString();
            //        Session["drEmailReq"] = domainDetails["DREmailReq"].ToString();
            //        Session["drPasswdReq"] = domainDetails["DRPasswdReq"].ToString();
            //        Session["drBackground"] = domainDetails["DRBackground"].ToString();
            //        Session["drStyleSheet"] = domainDetails["DRStyleSheet"].ToString();
            //        Session["drTitle"] = domainDetails["DRTitle"].ToString();
            //        Session["drDescription"] = domainDetails["DRDescription"].ToString();
            //        Session["drKeywords"] = domainDetails["DRKeywords"].ToString();
            //        Session["drMainImage"] = domainDetails["DRMainImage"].ToString();
            //        Session["drAuthorityReq"] = domainDetails["drAuthorityReq"].ToString();
            //    }
            //    domainDetails.Close();

        }

        protected void btnClose_Click(object sender, System.EventArgs e)
        {
            ////Close window
            //String strRedirectScript;

            //// Generate the JavaScript to pop up a print-only window
            //strRedirectScript = "<script language =javascript>";
            //strRedirectScript += "window.close();";
            //strRedirectScript += "</script> ";

            //// Add our JavaScript to the currently rendered page
            //Response.Write(strRedirectScript);
        }
    }
}
