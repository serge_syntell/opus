<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.ViewOffence_Print" Codebehind="ViewOffence_Print.aspx.cs" %>

<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%--<%@ Register Assembly="Atalasoft.dotImage.WebControls" Namespace="Atalasoft.Imaging.WebControls"
    TagPrefix="cc1" %>--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">

    <script>
		    function Print()
			{
				window.print();
			}
			function Close()
			{
				window.close();
			}
    </script>

</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0" onload="self.focus();"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table border="0" width="700">
            <tr>
                <td valign="top" width="100%" align="center">
                    <table width="100%">
                        <tr>
                            <td width="33%">
                                <asp:HyperLink ID="hlPrint" runat="server" CssClass="NormalBold" NavigateUrl="javascript:Print()" Text="<%$Resources:hlPrint.Text %>"></asp:HyperLink></td>
                            <td width="33%" align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="345px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></td>
                            <td width="34%" align="right">
                                <asp:DropDownList ID="ddlScan" runat="server" Visible="False" Width="91px">
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlBack" runat="server" CssClass="NormalBold" NavigateUrl="javascript:Close()" Text="<%$Resources:hlBack.Text %>"></asp:HyperLink></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" width="100%">
                    <table width="100%">
                        <tr>
                            <td width="100%" align="center">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top">
                                                        <table width="100%">
                                                            <tr>
                                                                <td valign="top" width="40%">
                                                                    <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Width="107px" Text="<%$Resources:lblTicketNo.Text %>"></asp:Label></td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblTicketNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Width="137px" Text="<%$Resources:lblRegistrationno.Text %>"></asp:Label></td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblRegNo" runat="server" CssClass="Normal" BorderStyle="None"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="Label19" runat="server" CssClass="NormalBold" Width="244px" Text="<%$Resources:lblFilmFrameNO.Text %>"></asp:Label></td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblFrameNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="245px" Text="<%$Resources:lblOffenceDate.Text %>"></asp:Label></td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblOffenceDate" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Width="164px" Text="<%$Resources:lblSpeed.Text %>"></asp:Label></td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblSpeed" runat="server" CssClass="Normal"></asp:Label>
                                                                    <asp:Label ID="lblSpeed2" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="245px" Text="<%$Resources:lblOffencedescr.Text %>"></asp:Label></td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblOffence" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Height="19px" Width="173px" Text="<%$Resources:lblLocation.Text %>"></asp:Label></td>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblLocDescr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                        <table id="tblOwnerDetails" runat="server" width="100%">
                                                            <tr>
                                                                <td width="40%">
                                                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Font-Underline="True"
                                                                        Width="94px" Text="<%$Resources:lblOwnerdetails.Text %>"></asp:Label>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="46px" Text="<%$Resources:lblName.Text %>"> </asp:Label>
                                                                    <asp:Label ID="lblOwnerName" runat="server" CssClass="Normal"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Width="31px" Text="<%$Resources:lblID.Text %>"></asp:Label>
                                                                    <asp:Label ID="lblOwnerID" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="122px" Text="<%$Resources:lblPostaladdress.Text %>"></asp:Label>&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblOwnerPostAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Width="122px" Text="<%$Resources:lblStreetaddress.Text %>"></asp:Label>&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblOwnerStrAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Font-Underline="True"
                                                                        Width="94px" Text="<%$Resources:lblDriverdetails.Text %>"></asp:Label></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Width="45px" Text="<%$Resources:lblName.Text %>"></asp:Label><asp:Label
                                                                        ID="lblDriverName" runat="server" CssClass="Normal"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Width="26px" Text="<%$Resources:lblID.Text %>"></asp:Label><asp:Label
                                                                        ID="lblDriverID" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Width="126px" Text="<%$Resources:lblPostaladdress.Text %>"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblDriverPostAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Width="124px" Text="<%$Resources:lblStreetaddress.Text %>"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblDriverStrAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="lblProxy" runat="server" CssClass="NormalBold" Font-Underline="True"
                                                                        Width="202px" Text="<%$Resources:lblProxydetails.Text %>"></asp:Label></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 205px">
                                                                    <asp:Label ID="lblProxyName" runat="server" CssClass="NormalBold" Width="56px" Text="<%$Resources:lblName.Text %>"></asp:Label><asp:Label
                                                                        ID="lblPrxName" runat="server" CssClass="Normal"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblProxyID" runat="server" CssClass="NormalBold" Width="31px" Text="<%$Resources:lblID.Text %>"></asp:Label><asp:Label
                                                                        ID="lblPrxID" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="NormalBold" style="width: 205px">
                                                                    <asp:Label ID="Label17" runat="server" Text="<%$Resources:lblFineAmount.Text %>"></asp:Label></td>
                                                                <td>
                                                                    <asp:Label ID="lblFineAmount" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 153px">
                                                        <div id="regArea" runat="server">
                                                            <table cellpadding="0" cellspacing="0" style="font-family:Tahoma; font-size:xx-small;">
                                                                <tr>
                                                                    <td style="border: solid 1px black;">
                                                                        <asp:Label ID="regImageTitle" runat="server">Enlarged Registration</asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr >
                                                                    <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                                                         <div id="regImageDiv" style="width:146px; line-height:34px; height:34px; overflow:hidden; position:relative; text-align:center;">
                                                                            <asp:Image ID="regImage" runat="server" ImageUrl="~/Image.aspx" style="vertical-align:middle;" />
                                                                        </div>
                                                                    </td>
                                                                </tr>                                    
                                                            </table>
                                                        </div>
                                                        <%--<cc1:WebImageViewer ID="wivRegNo" runat="server" AutoZoom="BestFitShrinkOnly" BackColor="White"
                                                            BorderColor="Black" BorderWidth="1px" BrowserFormat="Gif" Centered="True" EnableViewState="False"
                                                            Font-Names="Tahoma" Font-Size="XX-Small" ForeColor="Black" Height="34px" MouseTool="None"
                                                            ScrollBarVisibility="None" Selection-EnableViewState="False" Selection-Height="0"
                                                            Selection-Rectangle="0, 0, 0, 0" Selection-Resizable="False" Selection-Visibility="Visible"
                                                            Selection-Width="0" TitleBar="&nbsp;&nbsp;Enlarged Registration" Width="146px" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" id="tblImageA" runat="server">
                                                <tr>
                                                    <td style="width: 50%;" align="center" id="tdImageA" runat="server">
                                                        <div id="imgAreaA" runat="server">
                                                            <table cellpadding="0" cellspacing="0" style="font-family:Verdana; font-size:x-small;">
                                                                <tr>
                                                                    <td style="border: solid 1px black;">
                                                                        <asp:Label ID="imageATitle" runat="server" Text="<%$Resources:imageATitle.Text %>"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr >
                                                                    <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                                                         <div id="imageADiv" style="width:385px; line-height:244px; height:244px; overflow:hidden; position:relative; text-align:center;">
                                                                            <asp:Image ID="imageA" runat="server" ImageUrl="~/Image.aspx" style="vertical-align:middle;" />
                                                                        </div>
                                                                    </td>
                                                                </tr>                                    
                                                            </table>
                                                        </div>
                                                        <%--<cc1:WebImageViewer ID="wivImageA" runat="server" AntialiasDisplay="ReductionOnly"
                                                            BackColor="White" BorderColor="Black" BorderWidth="1px" Centered="True" Font-Names="Verdana"
                                                            Font-Size="X-Small" Height="244px" TitleBar="         First Photo" Width="384px"
                                                            AutoZoom="BestFitShrinkOnly" ScrollBarVisibility="None" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" id="tblImageB" runat="server">
                                                <tr>
                                                    <td align="center" style="width: 50%;" id="tdImageB" runat="server">
                                                        <div id="imgAreaB" runat="server">
                                                            <table cellpadding="0" cellspacing="0" style="font-family:Verdana; font-size:x-small;">
                                                                <tr>
                                                                    <td style="border: solid 1px black;">
                                                                        <asp:Label ID="imageBTitle" runat="server" Text="<%$Resources:imageBTitle.Text %>"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr >
                                                                    <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                                                                         <div id="imageBDiv" style="width:385px; line-height:244px; height:244px; overflow:hidden; position:relative; text-align:center;">
                                                                            <asp:Image ID="imageB" runat="server" ImageUrl="~/Image.aspx" style="vertical-align:middle;" />
                                                                        </div>
                                                                    </td>
                                                                </tr>                                    
                                                            </table>
                                                        </div>
                                                        <%--<cc1:WebImageViewer ID="wivImageB" runat="server" AntialiasDisplay="ReductionOnly"
                                                            BackColor="White" BorderColor="Black" BorderWidth="1px" Centered="True" CssClass="Normal"
                                                            Font-Names="Verdana" Font-Size="X-Small" Height="244px" TitleBar="        Second Photo"
                                                            Width="392px" AutoZoom="BestFitShrinkOnly" ScrollBarVisibility="None" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
        </table>
    </form>
    <script type="text/javascript">
        BestFit("imageA", "<%= imageAW%>", "<%= imageAH%>");
        BestFit("imageB", "<%= imageBW%>", "<%= imageBH%>");
        BestFit("regImage", "<%= imageRW%>", "<%= imageRH%>");

        function BestFit(imageID, imageW, imageH) {
            var image = document.getElementById(imageID);
            if (!image)
                return;      

            //var XYRatio = image.width / image.height;
            var XYRatio = imageW / imageH;
            var div = image.parentNode;

            var DIV_WIDTH = div.clientWidth;
            var DIV_HEIGHT = div.clientHeight;

            if (DIV_HEIGHT * XYRatio > DIV_WIDTH) {
                image.width = DIV_WIDTH;
                image.height = DIV_WIDTH / XYRatio;
            }
            else {
                image.width = DIV_HEIGHT * XYRatio;
                image.height = DIV_HEIGHT;
            }

            image.style.left = Math.round((DIV_WIDTH - image.width) / 2);
            image.style.top = Math.round((DIV_HEIGHT - image.height) / 2);
        }
    </script>
</body>
</html>
