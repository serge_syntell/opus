<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.AuthorityRules" Codebehind="AuthorityRules.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <link href="stylesheets/Shared.css" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptCopy" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptCopy.Text %> " Height="24px" OnClick="btnOptCopy_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p align="center">
                                    <asp:Label ID="Label12" runat="server" CssClass="NormalRed" Width="781px" Text="<%$Resources:lblNote.Text %>"></asp:Label>&nbsp;</p>
                                <p>
                                    &nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneral" runat="server">
                                </asp:Panel>
                                &nbsp;&nbsp;
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                    <table id="Table5" cellspacing="1" cellpadding="1" width="300" border="0">
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelAuthority" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:DropDownList ID="ddlSelectLA" runat="server" Width="250px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>


                                         <!-- New Search -->
                                               <tr>
                                                <td>
                                                    <asp:Label ID="lblTextSearch" runat="server" Width="136px" CssClass="NormalBold"
                                                        Text="<%$Resources:lblTextSearch.Text %>"></asp:Label>
                                                </td>
                                                <td class="fullRowInput">
                                                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="2" class="floatButtonRight">
                                                    <asp:Button ID="btnSearch" runat="server" Text="<%$Resources:btnSearch.Text %>" OnClick="btnSearch_Click" />
                                                </td>
                                                <td></td>
                                            </tr>
                                            <!-- New Search -->


                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                <asp:DataGrid ID="dgAuthorityRule" Width="495px" runat="server" BorderColor="Black"
                                    GridLines="Vertical" CellPadding="4" Font-Size="8pt" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                    FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                    AutoGenerateColumns="False" OnItemCommand="dgAuthorityRule_ItemCommand" AllowPaging="False">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="WFRFAID" HeaderText="WFRFAID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="WFRFAName" HeaderText="<%$Resources:dgAuthorityRule.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="WFRFADescr" HeaderText="<%$Resources:dgAuthorityRule.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ARNumeric" HeaderText="<%$Resources:dgAuthorityRule.HeaderText3 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ARString" HeaderText="<%$Resources:dgAuthorityRule.HeaderText4 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgAuthorityRuleItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <pager:AspNetPager id="dgAuthorityRulePager" runat="server" 
                                            showcustominfosection="Right" width="495px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;" 
                                            onpagechanged="dgAuthorityRulePager_PageChanged"  UpdatePanelId="UpdatePanel1"></pager:AspNetPager>
                                <br />
                                        <asp:Button ID="btnPrintAuthorityRules" runat="server" CssClass="NormalButton" Text="<%$Resources:btnPrintAuthorityRules.Text %>" OnClick="btnPrintAuthorityRules_Click"
                                            Width="149px" /><br />
                                        <br />
                                        &nbsp;
                                <asp:Panel ID="pnlAddAuthorityRule" runat="server" Height="127px">
                                    <table id="Table1" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td height="2" valign="top">
                                                <asp:Label ID="lblAddAuthorityRule" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddAuthorityRule.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" height="25">
                                                <asp:Label ID="lblAddARCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddARCode.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <%--<asp:TextBox ID="txtAddARCode" runat="server" Width="136px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="10"></asp:TextBox>--%>
                                                    <asp:DropDownList ID="ddlSelectWFRFAName" runat="server" Width="100px" CssClass="NormalMand"
                                                    AutoPostBack="True" 
                                                    onselectedindexchanged="ddlSelectWFRFAName_SelectedIndexChanged" >
                                                </asp:DropDownList>
                                                </td>
                                            <td height="25">
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>" ControlToValidate="txtAddARCode"
                                                    Display="dynamic" ForeColor=" "></asp:RequiredFieldValidator>--%></td>
                                        </tr>
                                        <tr>
                                            <td height="25" valign="top">
                                                <asp:Label ID="lblAddARDescr" runat="server" Width="214px" CssClass="NormalBold" Text="<%$Resources:lblAddARDescr.Text %>"></asp:Label></td>
                                            <td height="25" valign="top" width="248">
                                                <asp:TextBox ID="txtAddARDescr" runat="server" CssClass="NormalMand" MaxLength="100"
                                                    TextMode="MultiLine" Width="264px" ReadOnly="true"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                        
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddARComment.Text %>"></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="txtAddARComment" runat="server" CssClass="NormalMand" MaxLength="255"
                                                    TextMode="MultiLine" Width="264px" ReadOnly="true"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAddComment" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                            <td valign="top" height="25">
                                                <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddARNumeric.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddARNumeric" runat="server" Width="62px" CssClass="Normal" MaxLength="6"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RegularExpressionValidator ID="revNumber" runat="server" CssClass="NormalRed"
                                                    ErrorMessage="<%$Resources:ReqNumeric.ErrorMessage %>" ControlToValidate="txtAddARNumeric" ForeColor=" "
                                                    ValidationExpression="^-{0,1}\d+$"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddARString.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddARString" runat="server" Height="24px"
                                                    MaxLength="50" Width="261px" CssClass="Normal"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="height: 27px">
                                            </td>
                                            <td width="248" style="height: 27px">
                                            </td>
                                            <td style="height: 27px">
                                                <asp:Button ID="btnAddAuthorityRule" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddAuthorityRule.Text %>"
                                                    OnClick="btnAddAuthorityRule_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlEditAuthorityRule" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td height="2" valign="top">
                                                <asp:Label ID="lblEditTranNo" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblEditTranNo.Text %>"></asp:Label></td>
                                            <td height="2" style="width: 248px">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" height="25">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddARCode.Text %>"></asp:Label></td>
                                            <td valign="top" height="25" style="width: 248px">
                                                <asp:TextBox ID="txtARCode" runat="server" Width="136px" CssClass="NormalMand" Height="24px"
                                                    MaxLength="10" ReadOnly="True"></asp:TextBox>
                                               </td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtARCode"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    ForeColor=" " Width="210px"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" height="25">
                                                <asp:Label ID="lblARDescr" runat="server" Width="214px" CssClass="NormalBold" Text="<%$Resources:lblAddARDescr.Text %>"></asp:Label></td>
                                            <td valign="top" height="25" style="width: 248px">
                                                <asp:TextBox ID="txtARDescr" runat="server" CssClass="NormalMand" MaxLength="100" ReadOnly="True"
                                                    TextMode="MultiLine" Width="264px"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                        
                                        <tr>
                                            <td style="height: 21px" valign="top">
                                                <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddARComment.Text %>"></asp:Label></td>
                                            <td style="height: 21px; width: 248px;">
                                                <asp:TextBox ID="txtARComment" runat="server" CssClass="NormalMand" MaxLength="255" ReadOnly="True"
                                                    TextMode="MultiLine" Width="264px"></asp:TextBox></td>
                                            <td style="height: 21px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="Label13" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucCommentLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                            <td height="25" valign="top">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddARNumeric.Text %>"></asp:Label></td>
                                            <td height="25" style="width: 248px">
                                                <asp:TextBox ID="txtARNumeric" runat="server" Width="62px" CssClass="Normal" MaxLength="6"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RegularExpressionValidator ID="revDtRumber" runat="server" CssClass="NormalRed"
                                                    ErrorMessage="<%$Resources:ReqNumeric.ErrorMessage %>" ControlToValidate="txtARNumeric" ForeColor=" "
                                                    ValidationExpression="^-{0,1}\d+$"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 6px" valign="top">
                                                <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddARString.Text %>"></asp:Label>
                                            </td>
                                            <td style="height: 6px; width: 248px;">
                                                <asp:TextBox ID="txtARString" runat="server" Width="265px" CssClass="Normal"
                                                    Height="24px" MaxLength="50"></asp:TextBox></td>
                                            <td style="height: 6px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                            </td>
                                            <td style="width: 248px">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdateTranNo" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdateTranNo.Text %>"
                                                    OnClick="btnUpdateAuthorityRule_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlCopy" runat="server">
                                    <table id="Table4" cellspacing="1" cellpadding="1" width="300" border="0">
                                        <tr>
                                            <td width="162" height="19">
                                                <asp:Label ID="Label6" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblRules.Text %>"></asp:Label></td>
                                            <td width="7" height="19">
                                            </td>
                                            <td height="19">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="Label5" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblAutFrom.Text %>"></asp:Label></td>
                                            <td valign="top" width="7">
                                                <asp:DropDownList ID="ddlAutFrom" runat="server" Width="217px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="Label7" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblAutTo.Text %>"></asp:Label></td>
                                            <td valign="top" width="7">
                                                <asp:DropDownList ID="ddlAutTo" runat="server" Width="217px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                            <td>
                                                <asp:Button ID="btnCopyAuthorityRules" runat="server" CssClass="NormalButton" Text="<%$Resources:btnCopyAuthorityRules.Text %>"
                                                    CommandName="btnCopyAuthorityRules" OnClick="btnCopyAuthorityRules_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
