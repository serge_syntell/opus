using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace Stalberg.TMS 
{

    public partial class UserMenuBuilder : System.Web.UI.Page {

         protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected string thisPageURL = "MenuBuilder.aspx";
		protected int type = 0;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Menu Builder";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

     	protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//anyone with Access level < 10 may not access the system menu
			Stalberg.TMS.UserDetails userDetails = new UserDetails();
			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			if (userAccessLevel<10)
				type = 2;
			else 
				type = 0; //if a system admin user, show all pages

			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
				lblError.Visible = false;
				pnlShowMenuItem.Visible = false;

				if (Request.QueryString["mmiIntNo"] != null)
				{
					// show main menu item
					ShowMenuItem("mmiIntNo", Convert.ToInt32(Request.QueryString["mmiIntNo"]));
				}
				else if (Request.QueryString["miIntNo"] != null)
				{
					// show menu item
					ShowMenuItem("miIntNo", Convert.ToInt32(Request.QueryString["miIntNo"]));
				}
				else if (Request.QueryString["smiIntNo"] != null)
				{
					// show sub menu item
					ShowMenuItem("smiIntNo", Convert.ToInt32(Request.QueryString["smiIntNo"]));
				}
				else if (Request.QueryString["menuIntNo"] != null)
				{
					// show main menu item
					ShowMenuItem("menuIntNo", Convert.ToInt32(Request.QueryString["menuIntNo"]));
				}
			}
		}

		protected void ShowMenuItem (string colName, int colValue)
		{
			pnlShowMenuItem.Visible = true;

			txtColName.Text = colName;
			txtColValue.Text = colValue.ToString();
			txtURL.Text = string.Empty;

			switch (colName)
			{
				case "mmiIntNo":
					//show main menu item
					MainMenuItemDB mainMenuItem = new MainMenuItemDB(connectionString);
					MainMenuItemDetails mainMenuItemDet = mainMenuItem.GetMainMenuItemDetails(colValue);

					txtName.Text = mainMenuItemDet.MMIName;
					txtURL.Text = mainMenuItemDet.MMIurl;
					break;

				case "miIntNo":
					//show menu item
					MenuItemDB menuItem = new MenuItemDB(connectionString);
					MenuItemDetails menuItemDet = menuItem.GetMenuItemDetails(colValue);

					txtName.Text = menuItemDet.MIName;
					txtURL.Text = menuItemDet.MIurl;
					break;

				case "smiIntNo":
					//show sub-menu item
					SubMenuItemDB subMenuItem = new SubMenuItemDB(connectionString);
					SubMenuItemDetails subMenuItemDet = subMenuItem.GetSubMenuItemDetails(colValue);

					txtName.Text = subMenuItemDet.SMIName;
					txtURL.Text = subMenuItemDet.SMIurl;
					break;
			}

			chkAllowAccess.Checked = true;

			if (txtURL.Text.Equals(""))
			{
				txtAPLIntNo.Text = "0";	
			}
			else
			{
				//need to get APLIntNo value from page name
				ASPPageListDB page = new ASPPageListDB(connectionString);

				int aplIntNo = page.GetASPPageByURL(txtURL.Text);
				txtAPLIntNo.Text = aplIntNo.ToString();

				//check whether user has access to this page
				int userIntNo = Convert.ToInt32(Session["editUserIntNo"]);
				int usAPIntNo = page.GetASPPageForUser(aplIntNo, userIntNo);

				if (usAPIntNo != 0)
					chkAllowAccess.Checked = false;
			}		
		}

		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			string allow = "N";

			if (chkAllowAccess.Checked)
				allow = "Y";

			int aplIntNo = Convert.ToInt32(txtAPLIntNo.Text);

			//need to get APLIntNo value from page name
			ASPPageListDB page = new ASPPageListDB(connectionString);

			int userIntNo = Convert.ToInt32(Session["editUserIntNo"]);

			int usAPIntNo = page.UpdateASPPageForUser(aplIntNo, userIntNo, allow, loginUser);

			if (usAPIntNo < 0)
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
	
			lblError.Visible = true;
		}

    }
}
