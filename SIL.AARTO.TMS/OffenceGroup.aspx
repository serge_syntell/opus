<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>


<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.OffenceGroup" CodeBehind="OffenceGroup.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlSearch" runat="server">
                                    <table id="Table1" height="30" cellspacing="1" cellpadding="1" width="542" border="0">
                                        <tr>
                                            <td width="162"></td>
                                            <td width="7"></td>
                                            <td width="7"></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="10"></asp:TextBox></td>
                                            <td width="7">
                                                <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                    OnClick="btnSearch_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgOffenceGroup" Width="495px" runat="server" BorderColor="Black"
                                    AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgOffenceGroup_ItemCommand"
                                    OnPageIndexChanged="dgOffenceGroup_PageIndexChanged" CssClass="Normal">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="OGIntNo" HeaderText="OGIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="OGCode" HeaderText="<%$Resources:dgOffenceGroup.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="OGDescr" HeaderText="<%$Resources:dgOffenceGroup.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgOffenceGroupItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>&nbsp;
                                <asp:Panel ID="pnlAddOffence" runat="server" Width="670px" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="lblAddOffence" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddOffence.Text %>"></asp:Label></td>
                                            <td width="248" height="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="2">
                                                <asp:Label ID="lblAddOGCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddOGCode.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtAddOGCode" runat="server" Width="83px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="6"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="274px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage%>"
                                                    ControlToValidate="txtAddOGCode" ForeColor=" " EnableTheming="False"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2" valign="top">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOGDescr.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtAddOGDescr" runat="server" Width="499px" CssClass="NormalMand"
                                                    Height="38px" MaxLength="100" TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                        <td height="100">
                                                            <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                        <td height="100">
                                                            <uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOffenceType.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:DropDownList ID="ddlAddOffenceType" runat="server" Width="250px"
                                                    CssClass="Normal">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="23">
                                                <asp:Label ID="Label6" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddRoadType.Text %>"></asp:Label></td>
                                            <td width="248" height="23">
                                                <asp:DropDownList ID="ddlAddRoadType" runat="server" Width="162px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="21">
                                                <asp:Label ID="Label7" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblSpeedZone.Text %>"></asp:Label></td>
                                            <td width="248" height="21">
                                                <asp:DropDownList ID="ddlAddSpeedZone" runat="server" Width="162px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label5" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddVehicleTypeGroup.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:DropDownList ID="ddlAddVehicleTypeGroup" runat="server" Width="216px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="2">
                                                <asp:Label ID="Label10" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddOGStatutoryRef.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtAddOGStatutoryRef" runat="server" Width="501px" CssClass="NormalMand"
                                                    Height="62px" MaxLength="50" TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <asp:Label ID="Label20" runat="server" CssClass="NormalBold" Width="154px" Text="<%$Resources:lblAddOGStatutoryRefShort.Text %>"></asp:Label>
                                            </td>
                                            <td width="248">
                                                <asp:TextBox ID="txtAddOGStatutoryRefShort" runat="server" Width="496px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Width="154px" Text="<%$Resources:lblOffGroupS35StatRef.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtAddOffGroupS35StatRef" runat="server" CssClass="NormalMand" Width="496px"
                                                    Height="62px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Width="154px" Text="<%$Resources:lblOffGroupS35ShortStatRef.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtAddOffGroupS35ShortStatRef" runat="server" CssClass="NormalMand" Width="496px"
                                                    Height="24px" MaxLength="255" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248">
                                                <asp:Button ID="btnAddOffence" runat="server" CssClass="NormalButton"
                                                    OnClick="btnAddOffence_Click" Text="<%$Resources:btnAddOffence.Text %>" OnClientClick="return VerifytLookupRequired()"/>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateOffence" runat="server" Width="665px" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="119" height="2">
                                                <asp:Label ID="Label19" runat="server" Width="194px" CssClass="ProductListHead" Text="<%$Resources:lblUpdate.Text %>"></asp:Label></td>
                                            <td width="248" height="2"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="119" height="2">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddOGCode.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtOGCode" runat="server" Width="83px" CssClass="NormalMand" Height="24px"
                                                    MaxLength="6"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="285px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    ControlToValidate="txtOGCode" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="119" height="25">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOGDescr.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtOGDescr" runat="server" Width="493px" CssClass="NormalMand" Height="44px"
                                                    MaxLength="100" TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                        <td height="100">
                                                            <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                        <td height="100">
                                                            <uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="119" height="20">
                                                <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOffenceType.Text %>"></asp:Label></td>
                                            <td width="248" height="20">
                                                <asp:DropDownList ID="ddlOffenceType" runat="server" Width="250px"
                                                    CssClass="Normal">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="119">
                                                <asp:Label ID="Label13" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddRoadType.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:DropDownList ID="ddlRoadType" runat="server" Width="162px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="119">
                                                <asp:Label ID="Label14" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblSpeedZone.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:DropDownList ID="ddlSpeedZone" runat="server" Width="162px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="119">
                                                <asp:Label ID="Label15" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddVehicleTypeGroup.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:DropDownList ID="ddlVehicleTypeGroup" runat="server" Width="216px" CssClass="Normal">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="119">
                                                <asp:Label ID="Label18" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddOGStatutoryRef.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtOGStatutoryRef" runat="server" Width="492px" CssClass="NormalMand"
                                                    Height="62px" MaxLength="50" TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="154px" Text="<%$Resources:lblAddOGStatutoryRefShort.Text %>"></asp:Label>
                                            </td>
                                            <td width="248">
                                                <asp:TextBox ID="txtOGStatutoryRefShort" runat="server" Width="488px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label17" runat="server" CssClass="NormalBold" Width="154px" Text="<%$Resources:lblOffGroupS35StatRef.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtOffGroupS35StatRef" runat="server" CssClass="NormalMand" Width="496px"
                                                    Height="62px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label21" runat="server" CssClass="NormalBold" Width="154px" Text="<%$Resources:lblOffGroupS35ShortStatRef.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtOffGroupS35ShortStatRef" runat="server" CssClass="NormalMand" Width="496px"
                                                    Height="24px" MaxLength="255" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="119"></td>
                                            <td width="248">
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                            <td width="248">
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
