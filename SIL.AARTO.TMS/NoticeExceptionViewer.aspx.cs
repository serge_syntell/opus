using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Handles printing the first notice
    /// </summary>
    public partial class NoticeExceptionViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;

        //protected string thisPage = "Notice Exception Report Viewer";
        protected string thisPageURL = "NoticeExceptionViewer.aspx";
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;

        private const int STATUS_ERROR = 110;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (Request.QueryString["aut"] != null)
            {
                autIntNo = Convert.ToInt32(Request.QueryString["aut"]);

                string reportPage = "NoticeExceptions_CPI.rpt";

                string reportPath = Server.MapPath("reports/" + reportPage);


                AuthReportNameDB arn = new AuthReportNameDB(this.connectionString);

                //****************************************************
                //SD:  20081120 - check that report actually exists
                string templatePath = string.Empty;
                string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "NoticeExceptions_CPI");
                if (!File.Exists(reportPath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
                else if (!sTemplate.Equals(""))
                {

                    templatePath = Server.MapPath("Templates/" + sTemplate);

                    if (!File.Exists(templatePath))
                    {
                        string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                        Response.Redirect(errorURL);
                        return;
                    }
                }

                //****************************************************


                string tempFileLoc = string.Empty;
                SqlConnection con = null;
                SqlCommand com = null;
                SqlDataAdapter da = null;
                ReportDocument _reportDoc = null;

                try
                {
                    _reportDoc = new ReportDocument();
                    _reportDoc.Load(reportPath);

                    // Fill the DataSet
                    con = new SqlConnection(this.connectionString);
                    com = new SqlCommand("NoticeExceptions_CPI", con);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
                    com.Parameters.Add("@StatusError", SqlDbType.Int, 4).Value = STATUS_ERROR;

                    //get data and populate dataset
                    dsNoticeExceptions_CPI ds = new dsNoticeExceptions_CPI();
                    da = new SqlDataAdapter(com);
                    ds.DataSetName = "dsNoticeExceptions_CPI";
                    da.Fill(ds);
                    da.Dispose();

                    //set up new report based on .rpt report class
                    _reportDoc.SetDataSource(ds.Tables[1]);
                    ds.Dispose();

                    //export the pdf file
                    MemoryStream ms = new MemoryStream();
                    ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
                    _reportDoc.Dispose();

                    // Stuff the PDF file into rendering stream first clear everything dynamically created and just send PDF file
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/pdf";
                    //Response.WriteFile(tempFileLoc); 2012-12-25 Updated by Nancy
                    Response.BinaryWrite(ms.ToArray());
                    Response.End();
                }
                finally
                {
                    con.Dispose();
                    con = null;

                    com.Dispose();
                    com = null;
                }
            }

        }
    }
}
