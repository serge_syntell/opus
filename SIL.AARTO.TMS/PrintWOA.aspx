<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.PrintWOA" CodeBehind="PrintWOA.aspx.cs" %>

<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="95%" border="0" height="5%" align="center">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%" width="95%" align="center">
            <tr>
                <%-- <td align="center" valign="top" style="width: 132px">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    &nbsp;
                </td>--%>
                <td valign="top" align="center" width="100%" colspan="1">
                    <asp:UpdatePanel ID="udpPrintSummons" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td valign="top" style="height: 49px">
                                        <p align="center">
                                            <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                        </p>
                                        <p align="center">
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                        </p>
                                    </td>
                                    <td style="height: 49px" valign="top"></td>
                                </tr>
                                <tr>
                                    <td valign="top" align="center">
                                        <asp:Panel ID="pnlGeneral" runat="server">
                                            <table>
                                                <tr>
                                                    <td style="width: 343px">
                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="309px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                                    <td style="width: 181px" valign="middle">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px">
                                                        </asp:DropDownList></td>
                                                    <td valign="middle" style="width: 408px">
                                                        <asp:CheckBox ID="chkShowAll" runat="server" AutoPostBack="True" CssClass="NormalBold"
                                                            OnCheckedChanged="chkShowAll_CheckedChanged" Text="<%$Resources:chkShowAll.Text %>" /></td>
                                                </tr>
                                                <tr>
                                                    <td> <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="309px" Text="<%$Resources:lblSelWOAType.Text %>"></asp:Label></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlWOAType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlWOAType_SelectedIndexChanged">
                                                            <asp:ListItem Value="S">Standard WOA</asp:ListItem>
                                                            <asp:ListItem Value="B">Bench WOA</asp:ListItem>
                                                            <asp:ListItem Value="D">Double WOA</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px; height: 76px;">
                                                        <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="310px" Visible="False" Text="<%$Resources:lblTicketNum.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="height: 76px; width: 255px;">
                                                        <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" Visible="false" />
                                                    </td>
                                                    <td style="width: 408px; height: 76px"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px">
                                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Width="304px" Visible="False" Text="<%$Resources:lblPrint.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 255px; height: 43px;" valign="middle">
                                                        <asp:TextBox ID="txtPrint" runat="server" Width="215px" Visible="False"></asp:TextBox></td>
                                                    <td style="height: 43px; width: 408px;" valign="left">
                                                        <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Visible="False" Text="<%$Resources:lblOr.Text %>"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px">
                                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="304px" Visible="False" Text="<%$Resources:lblSummonsNo.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 255px; height: 43px;" valign="middle">
                                                        <asp:TextBox ID="txtSummonsNo" runat="server" Width="215px" Visible="False"></asp:TextBox></td>
                                                    <td style="height: 43px; width: 408px;" valign="middle"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px; height: 43px;">&nbsp
                                                    </td>
                                                    <td style="width: 255px; height: 43px;" valign="middle">
                                                        <asp:Button ID="btnPrint" runat="server" Text="<%$Resources:btnPrint.Text %>" OnClick="btnPrint_Click"
                                                            CssClass="NormalButton" Width="260px" Visible="False" /></td>
                                                    <td style="height: 43px; width: 408px;" valign="middle">&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:GridView ID="grdErrors" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                            CssClass="Normal" ShowFooter="True" Height="186px" Width="308px">
                                            <FooterStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundField DataField="ChargeStatus" HeaderText="<%$Resources:grdErrors.HeaderText %>">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CSDescr" HeaderText="<%$Resources:grdErrors.HeaderText1 %>">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Count" HeaderText="<%$Resources:grdErrors.HeaderText2 %>">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:HyperLinkField DataNavigateUrlFields="AutIntNo,ChargeStatus" DataNavigateUrlFormatString="SummonsErrorViewer.aspx?AutIntNo={0}&amp;Status={1}"
                                                    HeaderText="<%$Resources:grdErrors.HeaderText3 %>" Text="<%$Resources:grdErrorsItem.Text %>" Target="_blank">
                                                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                                </asp:HyperLinkField>
                                            </Columns>
                                            <HeaderStyle CssClass="CartListHead" />
                                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                                        </asp:GridView>
                                        <br />
                                        <asp:DataGrid ID="dgPrintrun" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                            Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgPrintrun_ItemCommand"
                                            CssClass="Normal" OnEditCommand="dgPrintrun_EditCommand" OnSelectedIndexChanged="dgPrintrun_SelectedIndexChanged">
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn ItemStyle-Width="380px" DataField="PrintFile" HeaderText="<%$Resources:dgPrintrun.HeaderText %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoOFWarrants" HeaderText="<%$Resources:dgPrintrun.HeaderText1 %>"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText2 %>">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text='<%# String.Format("{0:yyyy-MM-dd}", DataBinder.Eval(Container, "DataItem.SumCourtDate")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText3 %>">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# String.Format("{0:yyyy-MM-dd}", DataBinder.Eval(Container, "DataItem.WOAGeneratedDate")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:ButtonColumn CommandName="PrintCourtRegister" 
                                                    Text="<%$Resources:dgPrintrunItem.Text %>"></asp:ButtonColumn>
                                               <%-- <asp:ButtonColumn CommandName="AuthorizeWOA" Text="<%$Resources:dgPrintrunItem.Text1 %>" Visible="false"></asp:ButtonColumn>--%>
                                                <asp:EditCommandColumn CancelText="<%$Resources:dgPrintrunItem.CancleText %>" EditText="<%$Resources:dgPrintrunItem.Text2 %>"
                                                    UpdateText="<%$Resources:dgPrintrunItem.UpdateText %>"></asp:EditCommandColumn>
                                                <asp:ButtonColumn CommandName="PrintControl"
                                                    Text="<%$Resources:dgPrintrunItem.Text3 %>"></asp:ButtonColumn>
                                                <asp:ButtonColumn CommandName="Select" Text="<%$Resources:dgPrintrunItem.Text4 %>"></asp:ButtonColumn>
                                                <asp:BoundColumn DataField="Printable" HeaderText="" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Authorizable" HeaderText="" Visible="false"></asp:BoundColumn>
                                                <asp:ButtonColumn Visible="false" CommandName="ViewWoa" Text="<%$Resources:dgPrintrunItem.Text5 %>"></asp:ButtonColumn>
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <table>
                                            <tr>
                                                <td>
                                                    <pager:AspNetPager ID="dgPrintrunPager" runat="server"
                                                        ShowCustomInfoSection="Right" Width="400px"
                                                        CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                                        FirstPageText="|&amp;lt;"
                                                        LastPageText="&amp;gt;|"
                                                        CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                                        Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                                        CustomInfoStyle="float:right;" PageSize="10"
                                                        OnPageChanged="dgPrintrunPager_PageChanged"  UpdatePanelId="udpPrintSummons">
                                                    </pager:AspNetPager>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlUpdate" runat="server">

                                            <asp:Label ID="lblPFile" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;&nbsp;
                                                 
                                                        <asp:Label ID="lblPrintFile" runat="server" CssClass="NormalRed"></asp:Label>

                                        </asp:Panel>
                                        <br />
                                        <asp:Label ID="lblInstruct" runat="server" Width="465px" BorderStyle="Inset" CssClass="CartListHead"
                                            Height="18px" EnableViewState="False" Text="<%$Resources:lblInstruct.Text %>"></asp:Label>
                                    </td>
                                    <td align="center" valign="top"></td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle" /><asp:Label
                                    ID="Label6" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label>
                            </p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
