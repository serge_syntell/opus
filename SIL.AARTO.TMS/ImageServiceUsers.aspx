﻿<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Page language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.ImageServiceUsers" Codebehind="ImageServiceUsers.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head>
		<TITLE>
			<%= _title %>
		</TITLE>
		<LINK href=<%= _styleSheet %> type=text/css rel=stylesheet>
			<META content=<%= _description %> name=Description>
			<META content=<%= _keywords %> name=Keywords>
	</head>
	<body bottomMargin="0" leftMargin="0" background =<%=_background %> topMargin="0"
		rightMargin="0" >
		<form id="Form2" runat="server">
			<%--<table cellSpacing="0" cellPadding="0" width="100%" border="0" height="100%">
				<tr>
				</tr>
			</table>--%><%--//2014-01-20 Heidi comment out for fixed Page blank (5103)--%>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0" height="10%">
				<tr>
					<td class="HomeHead" align="center" width="100%" colSpan="2" vAlign="middle">
						<hdr1:Header id="Header1" runat="server"></hdr1:Header></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" border="0" height="85%">
				<tr>
					<td align="center" vAlign="top"><IMG height="26" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
							<asp:panel id="pnlSubMenu" Runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
								BorderColor="#000000">
								<TABLE id="tblMenu" height="90" cellSpacing="1" cellPadding="1" border="0" runat="server">
									<TR>
										<TD align="center">
											<asp:label id="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:label></TD>
									</TR>
									<TR>
										<TD align="center">
											<asp:button id="btnOptSearch" runat="server" Width="150px" CssClass="NormalButton" Text="<%$Resources:btnOptSearch.Text %>" OnClick="btnOptSearch_Click"></asp:button></TD>
									</TR>
									<TR>
										<TD align="center">
											<asp:button id="btnOptAdd" runat="server" Width="150px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>" OnClick="btnOptAdd_Click"></asp:button></TD>
									</TR>
									<TR>
										<TD align="center">
											<asp:button id="btnOptDelete" runat="server" Width="150px" CssClass="NormalButton" Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:button></TD>
									</TR>
								</TABLE>
							</asp:panel>
					</td>
					<td vAlign="top" align="left" width="100%" colSpan="1">
						<table border="0" width="568" height="482">
							<tr>
								<td vAlign="top" height="47">
									<P align="center">
										<asp:label id="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:label></P>
									<P>
										<asp:label  id="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:label></P>
								</td>
							</tr>
							<tr>
								<td vAlign="top">
									<asp:Panel id="pnlSearch" runat="server">
										<TABLE id="Table4" height="27" cellSpacing="1" cellPadding="1" border="0">
											<TR>
												<TD vAlign="top" height="27">
													<asp:Label id="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label></TD>
												<TD height="27">
													<asp:TextBox id="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="30"></asp:TextBox>
													<asp:button id="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>" OnClick="btnSearch_Click"></asp:button></TD>
											</TR>
										</TABLE>
									</asp:Panel>
									<asp:datagrid id="dgUsers" width="495px" runat="server" BorderColor="Black" AutoGenerateColumns="False"
										AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem" FooterStyle-CssClass="cartlistfooter"
										HeaderStyle-CssClass="CartListHead" ShowFooter="True" Font-Size="8pt" cellpadding="4" GridLines="Vertical"
										AllowPaging="True" OnItemCommand="dgUsers_ItemCommand" OnPageIndexChanged="dgUsers_PageIndexChanged">
										<FooterStyle CssClass="CartListFooter"></FooterStyle>
										<AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
										<ItemStyle CssClass="CartListItem"></ItemStyle>
										<HeaderStyle CssClass="CartListHead"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="ISUIntNo" HeaderText="ISUIntNo"></asp:BoundColumn>
											<asp:BoundColumn DataField="UserName" HeaderText="<%$Resources:dgUsers.HeaderText %>"></asp:BoundColumn>
											<asp:ButtonColumn Text="<%$Resources:dgUsersItem.Text %>" CommandName="Select"></asp:ButtonColumn>
										</Columns>
										<PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
									</asp:datagrid>
									<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="300" border="0">
										<TR>
											<TD></TD>
											<TD>
												<asp:panel id="pnlUserDetails" runat="server" Height="50px">
													<TABLE id="Table2" height="61" cellSpacing="1" cellPadding="1" width="656" border="0">
														<TR class="ProductListHead">
															<TD>
																<asp:label id="lblUserDetails" runat="server" CssClass="ProductListHead"  Text="<%$Resources:lblUserDetails.Text %>"></asp:label></TD>
															<TD></TD>
															<TD></TD>
														</TR>
														<TR>
															<TD>
																<asp:Label id="lblUserName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblUserName.Text %>"></asp:Label></TD>
															<TD>
																<asp:TextBox id="txtUserName" runat="server" Width="107px" 
                                                                    CssClass="NormalMand" MaxLength="20"></asp:TextBox></TD>
															<TD>
																<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" 
                                                                    Width="210px" CssClass="NormalRed" ControlToValidate="txtUserName"
																	Display="dynamic" ErrorMessage="<%$Resources:reqUserName.ErrorMsg %>" ForeColor=" "></asp:requiredfieldvalidator></TD>
														</TR>
														<TR>
															<TD>
																<asp:Label id="lblPassWord" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPassWord.Text %>"></asp:Label></TD>
															<TD>
																<asp:TextBox id="txtPassWord" runat="server" Width="107px" 
                                                                    CssClass="NormalMand" MaxLength="20" TextMode="Password"></asp:TextBox></TD>
															<TD></TD>
														</TR>
														<TR>
															<TD></TD>
															<TD>
																<asp:button id="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>" OnClick="btnUpdate_Click"></asp:button></TD>
															<TD>
																<asp:Button ID="btnBack" runat="server" CssClass="NormalButton" 
                                                                    OnClick="btnBack_Click" Text="<%$Resources:btnBack.Text %>" />
                                                            </TD>
														</TR>
													</TABLE>
												</asp:panel>
												<asp:panel id="pnlAddUser" runat="server">
													<TABLE id="Table7" height="61" cellSpacing="1" cellPadding="1" width="656" border="0">
														<TR>
															<TD>
																<asp:label id="lblAddUser" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddUser.Text %>"></asp:label></TD>
															<TD></TD>
															<TD></TD>
														</TR>
														<TR>
															<TD vAlign="top" height="17">
																<asp:label id="lblAddUserName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblUserName.Text %>"></asp:label></TD>
															<TD vAlign="top" height="17">
																<asp:textbox id="txtAddUserName" runat="server" CssClass="NormalMand" 
                                                                    MaxLength="20" Text="<%$Resources:txtAddUserName.Text %>"></asp:textbox></TD>
															<TD height="17">
																<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" 
                                                                    Width="210px" CssClass="NormalRed" ControlToValidate="txtAddUserName"
																	Display="dynamic" ErrorMessage="<%$Resources:reqUserName.ErrorMsg %>" ForeColor=" "></asp:requiredfieldvalidator></TD>
														</TR>
														<TR>
															<TD></TD>
															<TD>
                                                                <asp:Button ID="btnAddUser" runat="server" CssClass="NormalButton" 
                                                                    OnClick="btnAddUser_Click" Text="<%$Resources:btnAddUser.Text %>" />
                                                            </TD>
															<TD></TD>
														</TR>
													</TABLE>
												</asp:panel>
											</TD>
										</TR>
									</TABLE>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			
			<table cellSpacing="0" cellPadding="0" width="100%" border="0" height="5%">
				<tr>
					<TD class="SubContentHeadSmall" vAlign="top" width="100%"></TD>
				</tr>
			</table>
		</form>
	</body>
</html>

