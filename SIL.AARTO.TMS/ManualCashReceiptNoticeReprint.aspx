<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ManualCashReceiptNoticeReprint" Codebehind="ManualCashReceiptNoticeReprint.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0" class="NormalBold">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>
                            <br />
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                        <asp:Label ID="lblPaymentConfirmationReprint" runat="server" Text="<%$Resources:lblPaymentConfirmationReprint.Text %>"
                            CssClass="NormalBold"></asp:Label><br />
                        <table border="0">
                            <tr>
                                <td style="width: 43px">
                                    <asp:TextBox ID="txtFirst" runat="server" MaxLength="2" Width="40px" CssClass="Normal" />
                                </td>
                                <td>
                                    &nbsp;/<asp:TextBox ID="txtSecond" runat="server" MaxLength="6" Width="90px" CssClass="Normal" /></td>
                                <td>
                                    &nbsp;/<asp:TextBox ID="txtThird" runat="server" MaxLength="3" Width="50px" CssClass="Normal" /></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right">
                                    <asp:Button ID="btnSearch" Text="<%$Resources:btnSearch.Text %>" runat="server" CssClass="NormalButton" OnClick="btnSearch_Click" /></td>
                            </tr>
                        </table>
                        &nbsp;&nbsp;</asp:Panel>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
