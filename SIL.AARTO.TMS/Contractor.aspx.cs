using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial  class Contractor : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo;
        //protected string thisPage = "Contractor maintenance";
		protected string thisPageURL = "Contractor.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];

			loginUser = userDetails.UserLoginName;

			//int 
		    autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddContractor.Visible = false;
				pnlUpdateContractor.Visible = false;

				btnOptDelete.Visible = false;
				btnOptAdd.Visible = true;
				btnOptHide.Visible = true;

				BindGrid();
			}		
		}

        protected string HashPassword(string plainMessage)
        {
            byte[] data = Encoding.UTF8.GetBytes(plainMessage);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                byte[] encryptedBytes = sha.TransformFinalBlock(data, 0, data.Length);
                return Convert.ToBase64String(sha.Hash);
            }
        }

		protected void BindGrid()
		{
			// Obtain and bind a list of all users

            Stalberg.TMS.ContractorDB conList = new Stalberg.TMS.ContractorDB(connectionString);

            DataSet data = conList.GetContractorListDS();
			dgContractor.DataSource = data;
			dgContractor.DataKeyField = "ConIntNo";
			dgContractor.DataBind();

			
			if (dgContractor.Items.Count == 0)
			{
				dgContractor.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
			}
			else
			{
				dgContractor.Visible = true;
			}

			data.Dispose();
			pnlAddContractor.Visible = false;
			pnlUpdateContractor.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddContractor.Visible = true;
			pnlUpdateContractor.Visible = false;

			btnOptDelete.Visible = false;

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
		}

		protected void btnAddContractor_Click(object sender, System.EventArgs e)
		{	
			// add the new transaction to the database table
			Stalberg.TMS.ContractorDB toAdd = new ContractorDB(connectionString);

            //set up encryption class
            //Rc4Encrypt.clsRc4Encrypt rc4 = new clsRc4Encrypt();
            //rc4.Password = "sacompconsult";
            //rc4.PlainText = txtAddConFTPPassword.Text;
             
			int addConIntNo = toAdd.AddContractor(txtAddConCode.Text, txtAddConName.Text, txtAddConSupportSName.Text, txtAddConSupportFName.Text,
                txtAddConSupportEmail.Text, txtAddConFTPSite.Text, txtAddConFTPFolder.Text, txtAddConFTPLogin.Text, HashPassword(txtAddConFTPPassword.Text), loginUser);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addConIntNo, lgEntityList, "ContractorLookup", "ConIntNo", "ConName", this.loginUser);


			if (addConIntNo < 0)
			{
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
            else if (addConIntNo <= 0)
            {
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            }
            else
			{
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
              
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ContractorsMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			int conIntNo = Convert.ToInt32(Session["editConIntNo"]);

            //set up encryption class
            //Rc4Encrypt.clsRc4Encrypt rc4 = new clsRc4Encrypt();
            //rc4.Password = "sacompconsult";
            //rc4.PlainText = txtConFTPPassword.Text;

			// add the new transaction to the database table
			Stalberg.TMS.ContractorDB conUpdate = new ContractorDB(connectionString);
            string ftpPassword = string.Empty;
            if (!String.IsNullOrEmpty(txtConFTPPassword.Text.Trim()))
            {
                ftpPassword = HashPassword(txtAddConFTPPassword.Text);
            }
            int updConIntNo = conUpdate.UpdateContractor(conIntNo, txtConCode.Text, txtConName.Text, txtConSupportSName.Text, txtConSupportFName.Text,
                txtConSupportEmail.Text, txtConFTPSite.Text, txtConFTPFolder.Text, txtConFTPLogin.Text, ftpPassword, loginUser);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(updConIntNo, lgEntityList, "ContractorLookup", "ConIntNo", "ConName", this.loginUser);
           
			if (updConIntNo <= 0)
			{
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
			}
			else
			{
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ContractorsMaintenance, PunchAction.Change);  
			}

			lblError.Visible = true;

			BindGrid();
		}

		protected void dgContractor_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgContractor.SelectedIndex = e.Item.ItemIndex;

			if (dgContractor.SelectedIndex > -1) 
			{			
				int editConIntNo = Convert.ToInt32(dgContractor.DataKeys[dgContractor.SelectedIndex]);

				Session["editConIntNo"] = editConIntNo;
				Session["prevPage"] = thisPageURL;

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editConIntNo.ToString(), "ContractorLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

				ShowContractorDetails(editConIntNo);
			}
		}

		protected void ShowContractorDetails(int editConIntNo)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.ContractorDB contractor = new Stalberg.TMS.ContractorDB(connectionString);
			
			Stalberg.TMS.ContractorDetails conDetails = contractor.GetContractorDetails(editConIntNo);

			txtConCode.Text = conDetails.ConCode;
			txtConName.Text = conDetails.ConName;
            txtConSupportSName.Text = conDetails.ConSupportSName;
            txtConSupportFName.Text = conDetails.ConSupportFName;
            txtConSupportEmail.Text = conDetails.ConSupportEmail;
            txtConFTPSite.Text = conDetails.ConFTPSite;
            txtConFTPFolder.Text = conDetails.ConFTPFolder;
            txtConFTPLogin.Text = conDetails.ConFTPLogin;

            //set up encryption class
            //Rc4Encrypt.clsRc4Encrypt rc4 = new clsRc4Encrypt();
            //rc4.Password = "sacompconsult";
            //rc4.PlainText = conDetails.ConFTPPassword;

            //txtConFTPPassword.Text = rc4.EnDeCrypt();
			
			pnlUpdateContractor.Visible = true;
			pnlAddContractor.Visible  = false;
			
			btnOptDelete.Visible = true;
		}

	
		protected void dgContractor_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgContractor.CurrentPageIndex = e.NewPageIndex;

			BindGrid();
				
		}

		protected void btnOptDelete_Click(object sender, System.EventArgs e)
		{
			int conIntNo = Convert.ToInt32(Session["editConIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "ContractorLookup", "ConIntNo");

			ContractorDB contractor = new Stalberg.TMS.ContractorDB(connectionString);

			string delConIntNo = contractor.DeleteContractor(conIntNo);

			if (delConIntNo.Equals("0"))
			{
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
			}
            else if (delConIntNo.Equals("-1"))
            {
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
            }
            else
            {

                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ContractorsMaintenance, PunchAction.Delete); 
            }
			lblError.Visible = true;

			BindGrid();
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgContractor.Visible.Equals(true))
			{
				//hide it
				dgContractor.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				//show it
				dgContractor.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

	}
}
