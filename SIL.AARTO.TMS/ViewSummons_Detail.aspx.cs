using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
//using Atalasoft.Imaging;
//using Atalasoft.Imaging.Codec.Jpeg2000;
//using Atalasoft.Imaging.Codec;
using System.Text;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that displays the details of an offence
    /// </summary>
    public partial class ViewSummons_Detail : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int notIntNo = 0;
        private int frameIntNo = 0;
        private string filmNo = string.Empty;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "ViewSummons_Detail.aspx";
        //protected string thisPage = "View summons and/or warrant details";

        private const string MONEY_FORMAT = "R #,##0.00";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            //int 
            this.login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Get page variables
            this.notIntNo = Convert.ToInt32(Session["notIntNo"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.SummonsDetails();
            }
        }

        private void SummonsDetails()
        {
            SummonsDB summons = new SummonsDB(this.connectionString);
            NoticeDB notice = new NoticeDB(this.connectionString);
            this.frameIntNo = notice.GetFrameForNotice(this.notIntNo);

        if (this.frameIntNo != 0)
            {
                Session["frameIntNo"] = frameIntNo;
                Session["cvPhase"] = 1;
                Session["scImIntNo"] = 0;
                Session["imgType"] = "A";

                //StringBuilder sb = new StringBuilder();
                //sb.Append("MainImageViewer.aspx?FrameIntNo=");
                //sb.Append(frameIntNo);
                //sb.Append("&ScImIntNo=0&ImgType=A");
                //this.ifImages.Attributes["src"] = sb.ToString();

                //int filmIntNo = 0;

                SqlDataReader reader = summons.SummonsAndWarrantDetails(notIntNo);
                //mrs 20080905 removed acintno from the recordset - does not appear to be used below
                while (reader.Read())
                {
                    lblSummonsNo.Text = reader["SummonsNo"].ToString();
                    lblSummonsStatus.Text = reader["SumServedStatus"].ToString();
                   // lbSummonsCrtDate.Text = reader["SumCourtDate"].ToString(); 

                    DateTime dt;
                    if (!reader["SumCourtDate"].ToString().Equals("") && DateTime.TryParse(reader["SumCourtDate"].ToString(), out dt))
                    {
                        DateTime sumCrtDate = Convert.ToDateTime(reader["SumCourtDate"]);
                        lbSummonsCrtDate.Text = sumCrtDate.Year + "-" + sumCrtDate.Month.ToString().PadLeft(2, '0') + "-" + sumCrtDate.Day.ToString().PadLeft(2, '0');
                        //+ " " + sumCrtDate.Hour.ToString().PadLeft(2, '0') + ":" + sumCrtDate.Minute.ToString().PadLeft(2, '0');  
                    }

                    if (!reader["SumServedDate"].ToString().Equals("") && DateTime.TryParse(reader["SumServedDate"].ToString(), out dt))
                    {
                        DateTime summonsDate = Convert.ToDateTime(reader["SumServedDate"]);
                        lblSummonsDate.Text = summonsDate.Year + "-" + summonsDate.Month.ToString().PadLeft(2, '0') + "-" + summonsDate.Day.ToString().PadLeft(2, '0');
                            //+ " " + summonsDate.Hour.ToString().PadLeft(2, '0') + ":" + summonsDate.Minute.ToString().PadLeft(2, '0');
                    }

                    //filmIntNo = (int)reader["FilmIntNo"];
                    filmNo = reader["NotFilmNo"].ToString();
                    lblFrameNo.Text = filmNo + " ~ " + reader["NotFrameNo"].ToString();
                    lblRegNo.Text = reader["NotRegNo"].ToString();
                    lblSpeed.Text = string.Format((string)GetLocalResourceObject("lblSpeed.Text1"), reader["NotSpeed1"].ToString());
                    lblSpeed2.Text = string.Format((string)GetLocalResourceObject("lblSpeed2.Text"), reader["NotSpeed2"].ToString());
                    lblTicketNo.Text = reader["NotTicketNo"].ToString();
                    //lblFineAmt.Text = string.Format("{0:0.00}", decimal.Parse( reader["ChgFineAmount"].ToString()));
                    
                    //update by Rachel 20140811 for  5337
                    //lblFineAmt.Text = string.Format("{0:0.00}", decimal.Parse(reader["ChgRevFineAmount"].ToString()));
                    //lblContemptAmt.Text = string.Format("{0:0.00}", decimal.Parse(reader["ChgContemptCourt"].ToString()));
                    lblFineAmt.Text = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", reader["ChgRevFineAmount"]);
                    lblContemptAmt.Text = string.Format(CultureInfo.InvariantCulture, "{0:0.00}", reader["ChgContemptCourt"]);
                    //end update by Rachel 20140811 for  5337
                    lblCaseNo.Text = reader["SumCaseNo"].ToString();

                    if (!reader["NotOffenceDate"].ToString().Equals(""))
                    {
                        DateTime offenceDate = Convert.ToDateTime(reader["NotOffenceDate"]);
                        lblOffenceDate.Text = offenceDate.Year + "-" + offenceDate.Month.ToString().PadLeft(2, '0') + "-" + offenceDate.Day.ToString().PadLeft(2, '0');
                            //+ " " + offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    }

                    lblOffence.Text = reader["ChgOffenceDescr"].ToString();
                    lblLocDescr.Text = reader["NotLocDescr"].ToString();

                    LoadOwnerDetails(notIntNo);
                    LoadDriverDetails(notIntNo);

                    if (!reader["WOANumber"].ToString().Equals(""))
                    {
                        lblWFineAmount.Text = Convert.ToDecimal(reader["WOAFineAmount"]).ToString(MONEY_FORMAT);
                        lblWPaidAmount.Text = Convert.ToDecimal(reader["WOAPaidAmount"]).ToString(MONEY_FORMAT);
                        lblWAddAmount.Text = Convert.ToDecimal(reader["WOAAddAmount"]).ToString(MONEY_FORMAT);
                        lblWNo.Text = reader["WOANumber"].ToString();

                        if (!reader["WOAIssueDate"].ToString().Equals("") && DateTime.TryParse(reader["WOAIssueDate"].ToString(), out dt))
                        {
                            DateTime WIssueDate = Convert.ToDateTime(reader["WOAIssueDate"]);
                            lblWIssueDate.Text = WIssueDate.Year + "-" + WIssueDate.Month.ToString().PadLeft(2, '0') + "-" + WIssueDate.Day.ToString().PadLeft(2, '0');
                        }
                        if (!reader["WOANotificationDate"].ToString().Equals("") && DateTime.TryParse(reader["WOANotificationDate"].ToString(), out dt))
                        {
                            DateTime WNotificationDate = Convert.ToDateTime(reader["WOANotificationDate"]);
                            lblWNotificationDate.Text = WNotificationDate.Year + "-" + WNotificationDate.Month.ToString().PadLeft(2, '0') + "-" + WNotificationDate.Day.ToString().PadLeft(2, '0');
                        }
                        if (!reader["WOAPrintDate"].ToString().Equals("") && DateTime.TryParse(reader["WOAPrintDate"].ToString(), out dt))
                        {
                            DateTime WPrintDate = Convert.ToDateTime(reader["WOAPrintDate"]);
                            lblWPrintDate.Text = WPrintDate.Year + "-" + WPrintDate.Month.ToString().PadLeft(2, '0') + "-" + WPrintDate.Day.ToString().PadLeft(2, '0');
                        }

                        if (!reader["WOAGeneratedDate"].ToString().Equals("") && DateTime.TryParse(reader["WOAGeneratedDate"].ToString(), out dt))
                        {
                            DateTime WGenDate = Convert.ToDateTime(reader["WOAGeneratedDate"]);
                            lblWGenDate.Text = WGenDate.Year + "-" + WGenDate.Month.ToString().PadLeft(2, '0') + "-" + WGenDate.Day.ToString().PadLeft(2, '0');
                        }

                        if (!reader["WOADeliveredDate"].ToString().Equals("") && DateTime.TryParse(reader["WOADeliveredDate"].ToString(), out dt))
                        {
                            DateTime wDelDate = Convert.ToDateTime(reader["WOADeliveredDate"]);
                            lblWDelDate.Text = wDelDate.Year + "-" + wDelDate.Month.ToString().PadLeft(2, '0') + "-" + wDelDate.Day.ToString().PadLeft(2, '0');
                        }

                        tblWarrantDetails.Visible = true;
                    }
                    else
                    {
                        tblWarrantDetails.Visible = false;
                    }

                    if (reader["NotProxyFlag"].ToString().Equals("Y"))
                    {
                        LoadProxyDetails(notIntNo);
                        lblProxy.Visible = true;
                        lblProxyID.Visible = true;
                        lblProxyName.Visible = true;
                        lblProxyName.Visible = true;
                        lblProxyID.Visible = true;
                    }
                    else
                    {
                        lblProxy.Visible = false;
                        lblProxyID.Visible = false;
                        lblProxyName.Visible = false;
                        lblProxyName.Visible = false;
                        lblProxyID.Visible = false;
                    }

                    tblOwnerDetails.Visible = true;

                    //this.lblFineAmount.Text = Convert.ToDecimal(reader["ChgRevFineAmount"]).ToString(MONEY_FORMAT);
                }

                reader.Close();

                // Image Viewer Control
                //this.imageViewer1.FilmNo = filmNo;
                //this.imageViewer1.FrameIntNo = frameIntNo;
                //this.imageViewer1.ScanImageIntNo = 0;
                //this.imageViewer1.ImageType = "A";
                //this.imageViewer1.FilmIntNo = filmIntNo;
                //this.imageViewer1.Phase = this.Session["cvPhase"] == null ? 1 : Convert.ToInt32(this.Session["cvPhase"]);
                //this.imageViewer1.Initialise();
            }
     else
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
        }

        private void LoadProxyDetails(int notIntNo)
        {
            ProxyDB proxy = new ProxyDB(connectionString);

            ProxyDetails proxyDet = proxy.GetProxyDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            //string postAddr = string.Empty;
            //string strAddr = string.Empty;

            try
            {
                name = proxyDet.PrxSurname.Trim();
            }
            catch { }

            try
            {
                initials = proxyDet.PrxInitials.Trim();
            }
            catch { }

            try
            {
                id = proxyDet.PrxIDNumber.Trim();
            }
            catch { }

            //try
            //{
            //    postAddr = proxyDet.PrxPOAdd1.Trim();
            //    if (!proxyDet.PrxPOAdd1.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd2.Trim();
            //    if (!proxyDet.PrxPOAdd2.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd3.Trim();
            //    if (!proxyDet.PrxPOAdd3.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd4.Trim();
            //    if (!proxyDet.PrxPOAdd4.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd5.Trim();
            //    if (!proxyDet.PrxPOAdd5.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOCode.Trim();
            //}
            //catch { }

            //try
            //{
            //    strAddr = proxyDet.PrxStAdd1.Trim();
            //    if (!proxyDet.PrxStAdd1.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd2.Trim();
            //    if (!proxyDet.PrxStAdd2.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd3.Trim();
            //    if (!proxyDet.PrxStAdd3.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd4.Trim();
            //    if (!proxyDet.PrxStAdd4.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd5.Trim();
            //    if (!proxyDet.PrxStAdd5.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxStCode.Trim();
            //}
            //catch { }

            lblPrxName.Text = proxyDet.PrxFullName;

            lblPrxID.Text = id;

            //lblProxyPostAddr.Text = postAddr;
            //lblProxyStrAddr.Text = strAddr;
        }

        private void LoadDriverDetails(int notIntNo)
        {
            DriverDB driver = new DriverDB(connectionString);

            DriverDetails driverDet = driver.GetDriverDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            string postAddr = string.Empty;
            string strAddr = string.Empty;

            try
            {
                name = driverDet.DrvSurname.Trim();
            }
            catch { }

            try
            {
                initials = driverDet.DrvInitials.Trim();
            }
            catch { }

            try
            {
                id = driverDet.DrvIDNumber.Trim();
            }
            catch { }

            try
            {
                postAddr = driverDet.DrvPOAdd1.Trim();
                if (!driverDet.DrvPOAdd1.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd2.Trim();
                if (!driverDet.DrvPOAdd2.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd3.Trim();
                if (!driverDet.DrvPOAdd3.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd4.Trim();
                if (!driverDet.DrvPOAdd4.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd5.Trim();
                if (!driverDet.DrvPOAdd5.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOCode.Trim();
            }
            catch { }

            try
            {
                strAddr = driverDet.DrvStAdd1.Trim();
                if (!driverDet.DrvStAdd1.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd2.Trim();
                if (!driverDet.DrvStAdd2.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd3.Trim();
                if (!driverDet.DrvStAdd3.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd4.Trim();
                if (!driverDet.DrvStAdd4.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStCode.Trim();
            }
            catch { }

            // LMZ Changed 12-07-2007
            lblDriverName.Text = driverDet.DrvFullName;

            lblDriverID.Text = id;

            lblDriverPostAddr.Text = postAddr;
            lblDriverStrAddr.Text = strAddr;
        }

        private void LoadOwnerDetails(int notIntNo)
        {
            OwnerDB owner = new OwnerDB(connectionString);

            OwnerDetails ownerDet = owner.GetOwnerDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            string postAddr = string.Empty;
            string strAddr = string.Empty;

            try
            {
                name = ownerDet.OwnSurname.Trim();
            }
            catch { }

            try
            {
                initials = ownerDet.OwnInitials.Trim();
            }
            catch { }

            try
            {
                id = ownerDet.OwnIDNumber.Trim();
            }
            catch { }

            try
            {
                postAddr = ownerDet.OwnPOAdd1.Trim();
                if (!ownerDet.OwnPOAdd1.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd2.Trim();
                if (!ownerDet.OwnPOAdd2.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd3.Trim();
                if (!ownerDet.OwnPOAdd3.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd4.Trim();
                if (!ownerDet.OwnPOAdd4.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd5.Trim();
                if (!ownerDet.OwnPOAdd5.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOCode.Trim();
            }
            catch { }

            try
            {
                strAddr = ownerDet.OwnStAdd1.Trim();
                if (!ownerDet.OwnStAdd1.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd2.Trim();
                if (!ownerDet.OwnStAdd2.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd3.Trim();
                if (!ownerDet.OwnStAdd3.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd4.Trim();
                if (!ownerDet.OwnStAdd4.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStCode.Trim();
            }
            catch { }

            lblOwnerName.Text = ownerDet.OwnFullName;

            lblOwnerID.Text = id;

            lblOwnerPostAddr.Text = postAddr;
            lblOwnerStrAddr.Text = strAddr;
        }

    }
}
