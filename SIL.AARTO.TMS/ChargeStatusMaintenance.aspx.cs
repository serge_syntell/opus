﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Admin;

namespace Stalberg.TMS
{
    public partial class ChargeStatusMaintenance : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        protected string thisPageURL = "ChargeStatusMaintenance.aspx";
        //protected string thisPage = "Charge Status Maintenance";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;


        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            loginUser = userDetails.UserLoginName;


            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlUpdateCS.Visible = false;
                BindGrid();
            }
        }

        private void BindGrid()
        {
            // Obtain and bind a list of all users
            ChargeStatusDB chargeStatus = new ChargeStatusDB(connectionString);

            dgCS.DataSource = chargeStatus.GetChargeStatusList();
            dgCS.DataKeyField = "CSCode";
            dgCS.DataBind();

            if (dgCS.Items.Count == 0)
            {
                dgCS.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
            {
                dgCS.Visible = true;
            }
            pnlUpdateCS.Visible = false;
        }


        protected void dgCS_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgCS.SelectedIndex = e.Item.ItemIndex;

            if (dgCS.SelectedIndex > -1)
            {
                int editCSIntNo = Convert.ToInt32(dgCS.DataKeys[dgCS.SelectedIndex]);
                string editCSRoadblockOption = Convert.ToString(dgCS.SelectedItem.Cells[3].Text);
                string editCSRoadblockIdentifier = Convert.ToString(dgCS.SelectedItem.Cells[4].Text);

                pnlUpdateCS.Visible = true;
                lblCSCodeData.Text = editCSIntNo.ToString();

                //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editCSIntNo.ToString(), "ChargeStatusLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                ddlCSRoadblockOption.Items.Clear();
                if (editCSRoadblockOption != "N")
                {
                    ddlCSRoadblockOption.Items.Add("O");
                    ddlCSRoadblockOption.Items.Add("Y");
                }
                else
                {
                    ddlCSRoadblockOption.Items.Add("N");
                }

                ddlCSRoadblockOption.Text = editCSRoadblockOption;
                ddlCSRoadblockIdentifier.SelectedItem.Text = editCSRoadblockIdentifier;
            }
        }


        protected void dgCS_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgCS.CurrentPageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            int intCSCode = Convert.ToInt32(lblCSCodeData.Text);
            ChargeStatusDB csUpdate = new ChargeStatusDB(connectionString);

            using (ConnectionScope.CreateTransaction())
            {
                int iResult = csUpdate.UpdateChargeStatusFlag(intCSCode, ddlCSRoadblockOption.Text, Convert.ToInt16(ddlCSRoadblockIdentifier.Text), loginUser);

                //Heidi 2014-03-25 added for maintaining multiple languages(5141)
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookupFkGreaterThanOrEqualZero(intCSCode, lgEntityList, "ChargeStatusLookup", "CSCode", "CSDescr", this.loginUser);

                if (iResult <= 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ChargeStatusMaintenance, PunchAction.Change);  

                }

                ConnectionScope.Complete();
            }

            lblError.Visible = true;
            BindGrid();
        }
    }
}
