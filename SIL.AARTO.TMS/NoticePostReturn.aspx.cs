using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// The Notice Post management page
    /// </summary>
    public partial class NoticePostReturn : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "NoticePost.aspx";
        private const string DATE_FORMAT = "yyyy-MM-dd";
        protected string thisPage = "Notice Post Returned Page";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                PopulatePostReturnTypes(ddlPostReturnType);
                this.TicketNumberSearch1.AutIntNo = autIntNo;
                this.TicketNumberSearch2.AutIntNo = autIntNo;
            }
        }




        protected void PopulatePostReturnTypes(DropDownList ddlPostReturnType)
        {
            PostReturnTypeDB prt = new PostReturnTypeDB(connectionString);

            SqlDataReader reader = prt.GetPostReturnTypeList();
            ddlPostReturnType.DataSource = reader;
            ddlPostReturnType.DataValueField = "PRTIntNo";
            ddlPostReturnType.DataTextField = "PRTDescr";
            ddlPostReturnType.DataBind();
            reader.Close();
        }

        protected void btnOptAdd_Click(object sender, EventArgs e)
        {
            pnlDetails.Visible = true;
            pnlSearch.Visible = false;
            trNoticeNumber.Visible = false;
            btnAction.Text = (string)GetLocalResourceObject("btnOptAdd.Text");
            lblError.Text = string.Empty;
            lblError.Visible = false;
            pnlReport.Visible = false;
            txtTicketNo.Text = string.Empty;
            txtUser.Text = this.login;
            dtpDate.Text = DateTime.Today.ToString(DATE_FORMAT);
            txtBoxNumber.Text = string.Empty;
            txtStorageLocation.Text = string.Empty;
            ddlPostReturnType.SelectedIndex = 0;
        }

        protected void btnOptUpdate_Click(object sender, EventArgs e)
        {
            pnlDetails.Visible = true;
            pnlSearch.Visible = false;
            btnAction.Text = (string)GetLocalResourceObject("btnAction.Text");
        }

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int nReturn = 0;

            if (txtTicketNo.Text == string.Empty)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                return;
            }

            pnlDetails.Visible = false;
            pnlSearch.Visible = true;

            Stalberg.TMS.NoticePostReturnDB nprDB = new NoticePostReturnDB(connectionString);
            try
            {
                nReturn = nprDB.DeletePostReturn(Convert.ToInt32(txtNPRRIntNo.Value));
                if (nReturn == 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticePostReturn, PunchAction.Delete);  

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            txtSearchTicket.Text = string.Empty;
            lblError.Visible = true;
        }

        protected void btnOptHide_Click(object sender, EventArgs e)
        {
        }

        public void doSearch()
        {
            if (txtSearchTicket.Text == string.Empty)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                lblError.Visible = true;
                return;
            }

            pnlDetails.Visible = true;
            pnlSearch.Visible = false;
            btnAction.Text = (string)GetLocalResourceObject("btnAction.Text");
            lblError.Text = string.Empty;

            // add the new transaction to the database table
            Stalberg.TMS.NoticePostReturnDB nprDB = new NoticePostReturnDB(connectionString);
            try
            {
                NoticePostReturnDetails myDetails = new NoticePostReturnDetails();
                myDetails = nprDB.GetNoticePostReturnDetails(txtSearchTicket.Text);
                if (myDetails.nNPRRIntNo == -1)
                {
                    // this ticket no does not exist
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text4") + txtSearchTicket.Text;
                    this.txtSearchTicket.Text = string.Empty;
                    pnlDetails.Visible = false;
                    pnlSearch.Visible = true;
                    trNoticeNumber.Visible = false;
                    btnAction.Text = (string)GetLocalResourceObject("btnOptAdd.Text");
                    lblError.Visible = true;
                    pnlReport.Visible = false;
                    txtUser.Text = this.login;
                    dtpDate.Text = string.Empty;
                    txtBoxNumber.Text = string.Empty;
                    txtStorageLocation.Text = string.Empty;
                    ddlPostReturnType.SelectedIndex = 0;
                    return;
                }
                else if (myDetails.nNPRRIntNo == 0)
                {
                    // ticket number found but no entry in the PostReturnRegister
                    this.txtNoticeNumber.Text = myDetails.nNotIntNo.ToString();
                    this.txtTicketNo.Text = txtSearchTicket.Text;
                    pnlDetails.Visible = true;
                    pnlSearch.Visible = false;
                    trNoticeNumber.Visible = false;
                    btnAction.Text = (string)GetLocalResourceObject("btnOptAdd.Text");
                    lblError.Visible = false;
                    pnlReport.Visible = false;
                    txtUser.Text = this.login;
                    dtpDate.Text = string.Empty;
                    txtBoxNumber.Text = string.Empty;
                    txtStorageLocation.Text = string.Empty;
                    ddlPostReturnType.SelectedIndex = 0;
                }
                else
                {
                    this.txtNoticeNumber.Text = myDetails.nNotIntNo.ToString();
                    this.txtTicketNo.Text = txtSearchTicket.Text;
                    ddlPostReturnType.SelectedValue = myDetails.nPRTIntNo.ToString();
                    txtUser.Text = myDetails.sNPRRUser;
                    txtBoxNumber.Text = myDetails.sNPRRBoxNo;
                    txtStorageLocation.Text = myDetails.sNPRRStorageLocation;
                    txtNPRRIntNo.Value = myDetails.nNPRRIntNo.ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error " + ex.Message);
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4") + txtSearchTicket.Text;
                this.txtSearchTicket.Text = string.Empty;
                pnlDetails.Visible = false;
                pnlSearch.Visible = true;
                trNoticeNumber.Visible = false;
                btnAction.Text = (string)GetLocalResourceObject("btnOptAdd.Text");
                lblError.Visible = true;
                pnlReport.Visible = false;
                txtUser.Text = this.login;
                dtpDate.Text = string.Empty;
                txtBoxNumber.Text = string.Empty;
                txtStorageLocation.Text = string.Empty;
                ddlPostReturnType.SelectedIndex = 0;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //PunchStats805806 enquiry NoticePostReturn
            doSearch();
        }

        // 2012-02-08 Nick add, update MI5 for data wash
        bool CheckDataToWash(string ticketNo)
        {
            string notSendTo = null;
            string IDNum = null;

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "9060";
            arDetails.LastUser = login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, connectionString);
            bool DWEnabled = ar.SetDefaultAuthRule().Value.ToUpper().Trim().Equals("Y");

            if (!DWEnabled) return true;

            NoticeDB notice = new NoticeDB(connectionString);
            int notIntNo = notice.GetNoticeByNumber(autIntNo, ticketNo);
            NoticeDetails notDet = notice.GetNoticeDetails(notIntNo);
            if (notDet != null) notSendTo = (notDet.NotSendTo + "").Trim();
            
            switch (notSendTo)
            {
                case "D":
                    DriverDB driverDB = new DriverDB(connectionString);
                    DriverDetails drvDet = driverDB.GetDriverDetailsByNotice(notIntNo);
                    if (drvDet != null) IDNum = (drvDet.DrvIDNumber + "").Trim();
                    break;
                case "O":
                    OwnerDB ownerDB = new OwnerDB(connectionString);
                    OwnerDetails ownDet = ownerDB.GetOwnerDetailsByNotice(notIntNo);
                    if (ownDet != null) IDNum = (ownDet.OwnIDNumber + "").Trim();
                    break;
                case "P":
                    ProxyDB proxyDB = new ProxyDB(connectionString);
                    ProxyDetails pxyDet = proxyDB.GetProxyDetailsByNotice(notIntNo);
                    if (pxyDet != null) IDNum = (pxyDet.PrxIDNumber + "").Trim();
                    break;
                default:
                    break;
            }

            if (String.IsNullOrEmpty(IDNum))
            {
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text10"), notIntNo);
                return false;
            }

            arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "9080";
            arDetails.LastUser = login;
            ar = new DefaultAuthRules(arDetails, connectionString);
            int DWRecycle = ar.SetDefaultAuthRule().Key;

            bool needDataWash = false;
            MI5DB mi5 = new MI5DB(connectionString);
            DataSet dsPersonas = mi5.GetPersonaDetailsByID(IDNum, login);
            if (dsPersonas != null && dsPersonas.Tables.Count > 0)
            {
                string strLastSentDWDate = dsPersonas.Tables[0].Rows[0]["MIPSentToDataWasherDate"] + "";
                string strLastDWDate = dsPersonas.Tables[0].Rows[0]["MIPLastWashedDate"] + "";
                DateTime? lastSentDWDate = strLastSentDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastSentDWDate);
                DateTime? lastDWDate = strLastDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastDWDate);

                if ((!lastSentDWDate.HasValue && !lastDWDate.HasValue) ||
                    (lastSentDWDate.HasValue && !lastDWDate.HasValue && Convert.ToDateTime(lastSentDWDate).AddMonths(DWRecycle) < DateTime.Now))
                {
                    needDataWash = true;
                }
                else
                {
                    lastSentDWDate = lastSentDWDate.HasValue ? lastSentDWDate : DateTime.Parse("1900-01-01");
                    if (lastSentDWDate < lastDWDate && Convert.ToDateTime(lastDWDate).AddMonths(DWRecycle) < DateTime.Now)
                    {
                        needDataWash = true;
                    }
                }
            }
            else
            {
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text11"), ticketNo);
                return false;
            }

            if (needDataWash)
            {
                mi5.UpdatePersonaLastWashedDate(IDNum, login);
                AuthorityDB autDB = new AuthorityDB(connectionString);

                SqlDataReader result = autDB.GetAuthorityDetailsByAutIntNo(autIntNo);
                string autCode = null;
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        autCode = result["AutCode"].ToString();
                        QueueItemProcessor queueProcessor = new QueueItemProcessor();
                        QueueItem pushItem = new QueueItem();
                        pushItem.Body = notIntNo;
                        pushItem.Group = autCode;
                        pushItem.ActDate = DateTime.Now;
                        pushItem.QueueType = ServiceQueueTypeList.DataWashingExtractor;
                        queueProcessor.Send(pushItem);
                    }
                }
                result.Close();
                if (string.IsNullOrEmpty(autCode))
                {
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text12"), ticketNo);
                    return false;
                }
            }
            return true;
        }

        protected void btnAction_Click(object sender, EventArgs e)
        {
            // add the new transaction to the database table
            Stalberg.TMS.NoticePostReturnDB nprDB = new NoticePostReturnDB(connectionString);
            int nReturn = 0;
            string sPass = string.Empty;
            string sFail = string.Empty;
            bool checkDW = false;

            // 20120330 Nick add for data wash push queue
            using (TransactionScope scope = new TransactionScope())
            {
                if (this.btnAction.Text == (string)GetLocalResourceObject("btnOptAdd.Text"))
                {
                    if (txtTicketNo.Text == string.Empty)
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                        lblError.Visible = true;
                        return;
                    }

                    try
                    {
                        lblError.Visible = false;
                        nReturn = nprDB.AddPostReturn(txtTicketNo.Text, 0, Convert.ToInt32(this.ddlPostReturnType.SelectedValue), DateTime.Now, //this.txtUser.Text, 
                            this.login, this.txtBoxNumber.Text, this.txtStorageLocation.Text);
                        sPass = (string)GetLocalResourceObject("lblError.Text6");
                        sFail = (string)GetLocalResourceObject("lblError.Text7");

                        if (nReturn > 0)
                        {
                            checkDW = CheckDataToWash(this.txtTicketNo.Text);
                            
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticePostReturn, PunchAction.Add);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error " + ex.Message);
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                        pnlDetails.Visible = false;
                        pnlSearch.Visible = true;
                        lblError.Visible = true;
                        return;
                    }
                }
                else if (this.btnAction.Text == (string)GetLocalResourceObject("btnAction.Text"))
                {
                    try
                    {
                        nReturn = nprDB.UpdatePostReturn(Convert.ToInt32(txtNPRRIntNo.Value), Convert.ToInt32(txtNoticeNumber.Text),
                            Convert.ToInt32(this.ddlPostReturnType.SelectedValue), DateTime.Now, //this.txtUser.Text, 
                            this.login, this.txtBoxNumber.Text, this.txtStorageLocation.Text);
                        sPass = (string)GetLocalResourceObject("lblError.Text8");
                        sFail = (string)GetLocalResourceObject("lblError.Text9");

                        if (nReturn > 0)
                        {
                            checkDW = CheckDataToWash(this.txtTicketNo.Text);
                           
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticePostReturn, PunchAction.Change);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error " + ex.Message);
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                        return;
                    }
                }

                if (nReturn > 0 && checkDW)
                {
                    scope.Complete();
                }
            }

            if (nReturn <= 0)
            {
                lblError.Text = sFail;
            }
            else
            {
                if (checkDW) lblError.Text = sPass;
            }
            lblError.Visible = true;
        }

        protected void ddlPostReturnType_SelectedIndexChanged(object sender, EventArgs e)
        {
        }


        protected void btnReportDate_Click(object sender, EventArgs e)
        {
        }

        protected void btnOptSearch_Click(object sender, EventArgs e)
        {
            pnlDetails.Visible = false;
            pnlSearch.Visible = true;
            pnlReport.Visible = false;
        }

        protected void btnOptReport_Click(object sender, EventArgs e)
        {
            pnlDetails.Visible = false;
            pnlSearch.Visible = false;
            pnlReport.Visible = true;
            lblError.Visible = false;
            lblError.Text = string.Empty;
            btnOptDelete.Visible = false;
            btnOptAdd.Visible = false;
            lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            btnOptDelete.Visible = true;
            btnOptAdd.Visible = true;
            lblError.Visible = true;
            //PunchStats805806 enquiry NoticePostReturn

            Helper_Web.BuildPopup(this, "NoticePostReturn_ReportViewer.aspx", true, "TicketNo", this.txtTicketNoReport.Text, "RegNo", this.txtRegistrationNumber.Text, "Name", this.txtName.Text, "BoxNo", txtBoxNo.Text,
                "Date", this.dtpDate.Text,
                "Location", txtLocation.Text);
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtSearchTicket.Text = this.TicketNumberSearch1.TicketNumber;
            //PunchStats805806 enquiry NoticePostReturn
            doSearch();
        }

        public void NoticeSelectedReport(object sender, EventArgs e)
        {
            this.txtTicketNoReport.Text = this.TicketNumberSearch2.TicketNumber;
        }

    }
}
