using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.BLL.Utility.Cache;
using System.Collections.Generic;
using System.Threading;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Notices Blocked by Addresses page
    /// </summary>
    public partial class NoticeBlockedAddresses : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "NoticeBlockedAddresses.aspx";
        //protected string thisPage = "Notices Blocked by Bad Addresses";

        // Constants
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);
           
            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateAuthorities(this.autIntNo);
            }
        }

         
        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities( int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");


            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            this.ddlAuthority.SelectedIndex = this.ddlAuthority.Items.IndexOf(this.ddlAuthority.Items.FindByValue(autIntNo.ToString()));
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            //2013-04-08 add by Henry for pagination
            grdBlockedAddressesPager.CurrentPageIndex = 1;
            grdBlockedAddressesPager.RecordCount = 0;
            this.BindData();
        }

        private void BindData()
        {
            DateTime dt;

            this.lblError.Text = string.Empty;
            if (!DateTime.TryParse(dtpDate.Text, out dt))
            {
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text"));
                return;
            }

            int autIntNo = int.Parse(this.ddlAuthority.SelectedValue);

            MI5DB db = new MI5DB(this.connectionString);
            int totalCount = 0;
            DataSet ds = db.GetBlockedAddressList(autIntNo, dt, grdBlockedAddressesPager.PageSize, grdBlockedAddressesPager.CurrentPageIndex, out totalCount);

            if (ds.Tables[0].Rows.Count == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                this.pnlDetails.Visible = false;
                return;
            }

            this.grdBlockedAddresses.DataSource = ds;
            this.grdBlockedAddresses.DataKeyNames = new string[] { "NotIntNo" };
            this.grdBlockedAddresses.DataBind();
            this.pnlDetails.Visible = true;
            grdBlockedAddressesPager.RecordCount = totalCount;
        }

        protected void grdBlockedAddresses_RowCreated(object sender, GridViewRowEventArgs e)
        {
            DataSet ds = (DataSet)this.grdBlockedAddresses.DataSource;

            if (ds != null && e.Row.RowIndex >= 0 && e.Row.RowIndex < ds.Tables[0].Rows.Count)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                StringBuilder sb = new StringBuilder();

                // Address
                if (row["Address1"] != DBNull.Value && row["Address1"].ToString().Length > 0)
                    sb.Append(row["Address1"]);
                if (row["Address2"] != DBNull.Value && row["Address2"].ToString().Length > 0)
                    sb.Append("<br>" + row["Address2"]);
                if (row["Address3"] != DBNull.Value && row["Address3"].ToString().Length > 0)
                    sb.Append("<br>" + row["Address3"]);
                if (row["Address4"] != DBNull.Value && row["Address4"].ToString().Length > 0)
                    sb.Append("<br>" + row["Address4"]);
                if (row["Address5"] != DBNull.Value && row["Address5"].ToString().Length > 0)
                    sb.Append("<br>" + row["Address5"]);
                if (row["PoCode"] != DBNull.Value && row["PoCode"].ToString().Length > 0)
                    sb.Append("<br>" + row["PoCode"]);
                e.Row.Cells[3].Text = sb.ToString();

                // Name
                sb.Length = 0;
                if (row["ForeNames"] != DBNull.Value && row["ForeNames"].ToString().Length > 0)
                    sb.Append(row["ForeNames"] + " ");
                else if (row["Initials"] != DBNull.Value && row["Initials"].ToString().Length > 0)
                    sb.Append(row["Initials"] + " ");
                if (row["Surname"] != DBNull.Value && row["Surname"].ToString().Length > 0)
                    sb.Append(row["Surname"]);
                e.Row.Cells[2].Text = sb.ToString();

                // Posted Date
                sb.Length = 0;
                DateTime dt;
                if (DateTime.TryParse(row["NotPosted1stNoticeDate"].ToString(), out dt))
                {
                    if (row["NotPosted1stNoticeDate"] != DBNull.Value && row["NotPosted1stNoticeDate"].ToString().Length > 0)
                        sb.Append(((DateTime)row["NotPosted1stNoticeDate"]).ToString(DATE_FORMAT));
                }
                e.Row.Cells[1].Text = sb.ToString();

            }
        }

        //2013-04-08 add by Henry for pagination
        protected void grdBlockedAddressesPager_PageChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}
