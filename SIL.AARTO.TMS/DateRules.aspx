<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup"
    TagPrefix="uc1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.DateRules"
    CodeBehind="DateRules.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
        <tr>
            <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" height="85%">
        <tr>
            <td align="center" valign="top">
                <img style="height: 1px" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                    OnClick="btnOptAdd_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptCopy" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$Resources:btnOptCopy.Text %> " Height="24px" OnClick="btnOptCopy_Click">
                                </asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" width="100%" colspan="1">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table border="0" width="568" height="482">
                            <tr>
                                <td valign="top" height="47">
                                    <p align="center">
                                        <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:Panel ID="pnlGeneral" runat="server">
                                        <table id="Table5" cellspacing="1" cellpadding="1" width="300" border="0">
                                            <tr>
                                                <td width="162">
                                                </td>
                                                <td width="7">
                                                </td>
                                                <td style="width: 31px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="162">
                                                    <asp:Label ID="lblSelAuthority" runat="server" Width="136px" CssClass="NormalBold"
                                                        Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label>
                                                </td>
                                                <td width="7">
                                                    <asp:DropDownList ID="ddlSelectLA" runat="server" Width="250px" CssClass="Normal"
                                                        AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 31px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="162">
                                                </td>
                                                <td width="7">
                                                </td>
                                                <td style="width: 31px">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:DataGrid ID="dgDateRule" Width="495px" runat="server" BorderColor="Black" GridLines="Vertical"
                                        CellPadding="4" Font-Size="8pt" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                        FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                        AutoGenerateColumns="False" OnItemCommand="dgDateRule_ItemCommand" AllowPaging="False">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="DRID" HeaderText="DRID"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="DtRDescr" HeaderText="<%$Resources:dgDateRule.HeaderText1 %>">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ADRNoOfDays" HeaderText="<%$Resources:dgDateRule.HeaderText2 %>">
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgDateRule.Command.Text %>" CommandName="Select">
                                            </asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                    </asp:DataGrid>
                                    <pager:AspNetPager id="dgDateRulePager" runat="server" 
                                            showcustominfosection="Right" width="495px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;" 
                                             onpagechanged="dgDateRulePager_PageChanged"  UpdatePanelId="UpdatePanel1"></pager:AspNetPager>

                                    <asp:Panel ID="pnlAddDateRule" runat="server" Height="127px">
                                        <table id="Table2" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td height="2">
                                                    <asp:Label ID="lblAddDateRule" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddDateRule.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2">
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" height="25">
                                                </td>
                                                <td valign="top" width="248" height="25">
                                                    <asp:DropDownList ID="ddlSelectDateRuleName" runat="server" Width="261px" CssClass="NormalMand"
                                                        AutoPostBack="True" OnSelectedIndexChanged="ddlSelectDateRuleName_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" height="25">
                                                    <asp:Label ID="lblAddDtRType" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddDtRType.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="248" height="25">
                                                    <asp:TextBox ID="txtAddDtRStartDate" runat="server" Width="261px" CssClass="NormalMand"
                                                        Height="24px" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="210px"
                                                        CssClass="NormalRed" ErrorMessage="<%$Resources:ReqStart.ErrorMessage %>" ControlToValidate="txtAddDtRStartDate"
                                                        Display="dynamic" ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" height="25">
                                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddDtREndDate.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="248" height="25">
                                                    <asp:TextBox ID="txtAddDtREndDate" runat="server" Width="261px" CssClass="NormalMand"
                                                        Height="24px" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Width="210px"
                                                        CssClass="NormalRed" ErrorMessage="<%$Resources:ReqEnd.ErrorMessage %>" ControlToValidate="txtAddDtREndDate"
                                                        Display="dynamic" ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="43">
                                                    <asp:Label ID="lblAddDtRDescr" runat="server" Width="214px" CssClass="NormalBold"
                                                        Text="<%$Resources:lblAddDtRDescr.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="43">
                                                    <asp:TextBox ID="txtAddDtRDescr" runat="server" Width="264px" CssClass="NormalMand" MaxLength="50"
                                                        TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td height="43">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                        <tr bgcolor='#FFFFFF'>
                                                            <td height="100">
                                                                <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>"
                                                                    Width="265"></asp:Label>
                                                            </td>
                                                            <td height="100">
                                                                <uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>
                                                        <asp:Label ID="lblAddDtRumber" runat="server" Width="94px" CssClass="NormalBold"
                                                            Text="<%$Resources:lblAddDtRumber.Text %>"></asp:Label></p>
                                                </td>
                                                <td width="248">
                                                    <asp:TextBox ID="txtAddDtRNoOfDays" runat="server" Width="62px" CssClass="Normal"
                                                        MaxLength="3"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revNumber" runat="server" CssClass="NormalRed"
                                                        ErrorMessage="<%$Resources:revNumber.ErrorMessage %>" ControlToValidate="txtAddDtRNoOfDays"
                                                        ForeColor=" " ValidationExpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td width="248">
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddDateRule" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddDateRule.Text %>"
                                                        OnClick="btnAddDateRule_Click"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEditDateRule" runat="server" Height="127px">
                                        <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td height="2">
                                                    <asp:Label ID="lblEditTranNo" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblEditTranNo.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2">
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" height="25">
                                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddDtRType.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="248" height="25">
                                                    <asp:TextBox ID="txtDtRStartDate" runat="server" Width="265px" Height="24px" MaxLength="50" CssClass="NormalMand"
                                                        ReadOnly="True" ></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" height="25">
                                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddDtREndDate.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="248" height="25">
                                                    <asp:TextBox ID="txtDtREndDate" runat="server" Width="265px" Height="24px" MaxLength="50"
                                                        ReadOnly="True" CssClass="NormalMand"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="25">
                                                    <asp:Label ID="lblDtRDescr" runat="server" Width="214px" CssClass="NormalBold" Text="<%$Resources:lblAddDtRDescr.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="25">
                                                    <asp:TextBox ID="txtDtRDescr" runat="server" Width="264px" CssClass="NormalMand" MaxLength="50"
                                                        TextMode="MultiLine" ReadOnly="True" ></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                        <tr bgcolor='#FFFFFF'>
                                                            <td height="100">
                                                                <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>"
                                                                    Width="265"> </asp:Label>
                                                            </td>
                                                            <td height="100">
                                                                <uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>
                                                        <asp:Label ID="lblDtRumber" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddDtRumber.Text %>"></asp:Label></p>
                                                </td>
                                                <td width="248">
                                                    <asp:TextBox ID="txtDtRNoOfDays" runat="server" Width="62px" CssClass="Normal" MaxLength="3"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revDtRumber" runat="server" CssClass="NormalRed"
                                                        ErrorMessage="<%$Resources:revNumber.ErrorMessage %>" ControlToValidate="txtDtRNoOfDays"
                                                        ForeColor=" " ValidationExpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td width="248">
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnUpdateTranNo" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdateTranNo.Text %>"
                                                        OnClick="btnUpdateDateRule_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlCopy" runat="server">
                                        <table id="Table4" cellspacing="1" cellpadding="1" width="300" border="0">
                                            <tr>
                                                <td width="162" height="19">
                                                    <asp:Label ID="Label6" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblCopy.Text %>"></asp:Label>
                                                </td>
                                                <td width="7" height="19">
                                                </td>
                                                <td height="19">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="162">
                                                    <asp:Label ID="Label5" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblAutFrom.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="7">
                                                    <asp:DropDownList ID="ddlAutFrom" runat="server" Width="217px" CssClass="Normal">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="162">
                                                    <asp:Label ID="Label7" runat="server" Width="136px" CssClass="NormalBold" Text="<%$Resources:lblAutTo.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="7">
                                                    <asp:DropDownList ID="ddlAutTo" runat="server" Width="217px" CssClass="Normal">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnCopyDateRules" runat="server" CssClass="NormalButton" Text="<%$Resources:btnCopyDateRules.Text %>"
                                                        CommandName="btnCopyDateRules" OnClick="btnCopyDateRules_Click"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
