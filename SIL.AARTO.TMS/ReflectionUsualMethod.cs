﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Admin;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace SIL.AARTO.TMS
{
    public class ReflectionUsualMethod
    {
        public List<LanguageLookupEntity> BindUCLanguageLookup()
        {
            List<LanguageLookupEntity> entityList = new List<LanguageLookupEntity>();

            SIL.AARTO.DAL.Services.LanguageSelectorService languageSelectorService = new LanguageSelectorService();
            List<LanguageSelector> languageSelectorList = languageSelectorService.GetAll().OrderByDescending(lgs=>lgs.LsCode).OrderByDescending(lgs => lgs.LsIsDefault).ToList(); 

            for (int i = 0; i < languageSelectorList.Count; i++)
            {
                LanguageLookupEntity entityAdd = new LanguageLookupEntity();
                entityAdd.LsCode = languageSelectorList[i].LsCode;
                entityAdd.LsDescription = languageSelectorList[i].LsDescription;
                entityList.Add(entityAdd);
            }
            return entityList;
        }
        
        public List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> BindUCLanguageLookupByID(string editVTIntNo, string className)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.BLL.dll"));
            System.Type type = assembly.GetType("SIL.AARTO.BLL.Admin."+className+"LangManager");
            object theObj = assembly.CreateInstance("SIL.AARTO.BLL.Admin."+className+"LangManager");

            System.Type[] paramTypes = new System.Type[1];
            paramTypes[0] = System.Type.GetType("System.String");

            System.Reflection.MethodInfo methodInfo = type.GetMethod("GetListBySourceTableID", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, paramTypes, null);

            Object[] parameters = new Object[1];
            parameters[0] = editVTIntNo;

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = (List<SIL.AARTO.BLL.Admin.LanguageLookupEntity>)methodInfo.Invoke(theObj, parameters);

            return entityList;
        }

        public void DeleteLookup(List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList, string className, string fkidName)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Services.dll"));
            System.Type serviceType = assembly.GetType("SIL.AARTO.DAL.Services." + className + "Service");
            object serviceObj = assembly.CreateInstance("SIL.AARTO.DAL.Services." + className + "Service");

            System.Reflection.Assembly assemblyLookup = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Entities.dll"));
            System.Type entityType = assemblyLookup.GetType("SIL.AARTO.DAL.Entities." + className);
            object entityObj = assemblyLookup.CreateInstance("SIL.AARTO.DAL.Entities." + className);

            for (int i = 0; i < lgEntityList.Count; i++)
            {
                System.Type[] paramTypes = new System.Type[2];
                paramTypes[0] = System.Type.GetType("System.Int32");                
                paramTypes[1] = System.Type.GetType("System.String");
                System.Reflection.MethodInfo methodInfo = serviceType.GetMethod("GetBy" + fkidName + "LsCode", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, paramTypes,null);

                Object[] parameters = new Object[2];
                parameters[0] = int.Parse(lgEntityList[i].LookUpId);
                parameters[1] = lgEntityList[i].LsCode;
                entityObj = methodInfo.Invoke(serviceObj, parameters);

                if (entityObj != null)
                {
                    Type[] typeList = new Type[] { entityType };
                    System.Reflection.MethodInfo methodInfoDel = serviceType.GetMethod("Delete", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, typeList,null);
                    methodInfoDel.Invoke(serviceObj, new object[] { entityObj });
                }
            }
        }

        #region //InsertIntoLookup
        /*
        public void InsertIntoLookup(int addVTIntNo, List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList, string className, string fkidName,string DescrName)
        {
            if (addVTIntNo > 0)
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Services.dll"));
                System.Type serviceType = assembly.GetType("SIL.AARTO.DAL.Services." + className + "Service");
                object serviceObj = assembly.CreateInstance("SIL.AARTO.DAL.Services." + className + "Service");


                for (int i = 0; i < lgEntityList.Count; i++)
                {
                    System.Reflection.Assembly assemblyEntity = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Entities.dll"));
                    System.Type entityType = assemblyEntity.GetType("SIL.AARTO.DAL.Entities." + className);
                    object entityObj = assemblyEntity.CreateInstance("SIL.AARTO.DAL.Entities." + className);

                    System.Type[] paramTypes = new System.Type[2];
                    paramTypes[0] = System.Type.GetType("System.Int32");
                    paramTypes[1] = System.Type.GetType("System.String");
                    //System.Reflection.MethodInfo methodInfo = serviceType.GetMethod("GetBy" + fkidName + "LsCode", paramTypes);
                    System.Reflection.MethodInfo methodInfo = serviceType.GetMethod("GetBy" + fkidName + "LsCode", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, paramTypes, null);


                    Object[] parameters = new Object[2];
                    parameters[0] = addVTIntNo;
                    parameters[1] = lgEntityList[i].LsCode;
                    object entityObjSelected = methodInfo.Invoke(serviceObj, parameters);
                    if (entityObjSelected == null)
                    {
                        entityType.InvokeMember(fkidName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { addVTIntNo });
                        entityType.InvokeMember("LsCode", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { lgEntityList[i].LsCode });
                        entityType.InvokeMember(DescrName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { lgEntityList[i].LookupValue });

                        Type[] typeList = new Type[] { entityType };
                        System.Reflection.MethodInfo methodInfoSave = serviceType.GetMethod("Save", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, typeList,null);
                        methodInfoSave.Invoke(serviceObj, new object[] { entityObj });
                    }
                }
            }
        }
        */
        #endregion

        public void UpdateIntoLookup(int addVTIntNo, List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList, string className, string fkidName, string DescrName,string LastUser)
        {
            //Heidi 2014-04-28 added "LastUser" for maintaining multiple languages(5141)
            if (addVTIntNo > 0)
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Services.dll"));
                System.Type serviceType = assembly.GetType("SIL.AARTO.DAL.Services." + className + "Service");
                object serviceObj = assembly.CreateInstance("SIL.AARTO.DAL.Services." + className + "Service");

                for (int i = 0; i < lgEntityList.Count; i++)
                {
                    System.Reflection.Assembly assemblyEntity = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Entities.dll"));
                    System.Type entityType = assemblyEntity.GetType("SIL.AARTO.DAL.Entities." + className);
                    object entityObj = assemblyEntity.CreateInstance("SIL.AARTO.DAL.Entities." + className);

                    System.Type[] paramTypes = new System.Type[2];
                    paramTypes[0] = System.Type.GetType("System.Int32");
                    paramTypes[1] = System.Type.GetType("System.String");
                    System.Reflection.MethodInfo methodInfo = serviceType.GetMethod("GetBy" + fkidName + "LsCode", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, paramTypes, null);

                    Object[] parameters = new Object[2];
                    parameters[0] = addVTIntNo;
                    parameters[1] = lgEntityList[i].LsCode;
                    object entityObjSelected = methodInfo.Invoke(serviceObj, parameters);

                    if (entityObjSelected == null)
                    {
                        entityType.InvokeMember(fkidName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { addVTIntNo });
                        entityType.InvokeMember("LsCode", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { lgEntityList[i].LsCode });
                        entityType.InvokeMember(DescrName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { lgEntityList[i].LookupValue });
                        entityType.InvokeMember("LastUser", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                            null, entityObj, new object[] { LastUser == null ? "" : LastUser });

                        Type[] typeList = new Type[] { entityType };
                        System.Reflection.MethodInfo methodInfoSave = serviceType.GetMethod("Save", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, typeList, null);
                        methodInfoSave.Invoke(serviceObj, new object[] { entityObj });
                    }
                    else
                    {
                        entityType.InvokeMember(fkidName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { addVTIntNo });
                        entityType.InvokeMember("LsCode", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { lgEntityList[i].LsCode });
                        entityType.InvokeMember(DescrName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { lgEntityList[i].LookupValue });
                        entityType.InvokeMember("LastUser", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                       null, entityObjSelected, new object[] { LastUser == null ? "" : LastUser });

                        Type[] typeList = new Type[] { entityType };
                        System.Reflection.MethodInfo methodInfoSave = serviceType.GetMethod("Save", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, typeList, null);
                        methodInfoSave.Invoke(serviceObj, new object[] { entityObjSelected });

                    }
                }
            }
        }

        //Heidi 2014-04-03 added for maintaining multiple languages(5141)
        public void UpdateIntoLookupFkIsVarchar(string addVTIntNo, List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList, string className, string fkidName, string DescrName, string LastUser)
        {
            if (addVTIntNo!="")
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Services.dll"));
                System.Type serviceType = assembly.GetType("SIL.AARTO.DAL.Services." + className + "Service");
                object serviceObj = assembly.CreateInstance("SIL.AARTO.DAL.Services." + className + "Service");

                for (int i = 0; i < lgEntityList.Count; i++)
                {
                    System.Reflection.Assembly assemblyEntity = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Entities.dll"));
                    System.Type entityType = assemblyEntity.GetType("SIL.AARTO.DAL.Entities." + className);
                    object entityObj = assemblyEntity.CreateInstance("SIL.AARTO.DAL.Entities." + className);

                    System.Type[] paramTypes = new System.Type[2];
                    paramTypes[0] = System.Type.GetType("System.String");
                    paramTypes[1] = System.Type.GetType("System.String");
                    System.Reflection.MethodInfo methodInfo = serviceType.GetMethod("GetBy" + fkidName + "LsCode", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, paramTypes, null);

                    Object[] parameters = new Object[2];
                    parameters[0] = addVTIntNo;
                    parameters[1] = lgEntityList[i].LsCode;
                    object entityObjSelected = methodInfo.Invoke(serviceObj, parameters);

                    if (entityObjSelected == null)
                    {
                        entityType.InvokeMember(fkidName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { addVTIntNo });
                        entityType.InvokeMember("LsCode", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { lgEntityList[i].LsCode });
                        entityType.InvokeMember(DescrName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { lgEntityList[i].LookupValue });
                        entityType.InvokeMember("LastUser", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                           null, entityObj, new object[] { LastUser == null ? "" : LastUser });

                        Type[] typeList = new Type[] { entityType };
                        System.Reflection.MethodInfo methodInfoSave = serviceType.GetMethod("Save", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, typeList, null);
                        methodInfoSave.Invoke(serviceObj, new object[] { entityObj });
                    }
                    else
                    {
                        entityType.InvokeMember(fkidName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { addVTIntNo });
                        entityType.InvokeMember("LsCode", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { lgEntityList[i].LsCode });
                        entityType.InvokeMember(DescrName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { lgEntityList[i].LookupValue });
                        entityType.InvokeMember("LastUser", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                           null, entityObjSelected, new object[] { LastUser == null ? "" : LastUser });

                        Type[] typeList = new Type[] { entityType };
                        System.Reflection.MethodInfo methodInfoSave = serviceType.GetMethod("Save", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, typeList, null);
                        methodInfoSave.Invoke(serviceObj, new object[] { entityObjSelected });

                    }
                }
            }
        }

        //Heidi 2014-04-03 added for maintaining multiple languages(5141)
        public void UpdateIntoLookupFkGreaterThanOrEqualZero(int addVTIntNo, List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList, string className, string fkidName, string DescrName, string LastUser)
        {
            if (addVTIntNo >= 0)
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Services.dll"));
                System.Type serviceType = assembly.GetType("SIL.AARTO.DAL.Services." + className + "Service");
                object serviceObj = assembly.CreateInstance("SIL.AARTO.DAL.Services." + className + "Service");

                for (int i = 0; i < lgEntityList.Count; i++)
                {
                    System.Reflection.Assembly assemblyEntity = System.Reflection.Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\SIL.AARTO.DAL.Entities.dll"));
                    System.Type entityType = assemblyEntity.GetType("SIL.AARTO.DAL.Entities." + className);
                    object entityObj = assemblyEntity.CreateInstance("SIL.AARTO.DAL.Entities." + className);

                    System.Type[] paramTypes = new System.Type[2];
                    paramTypes[0] = System.Type.GetType("System.Int32");
                    paramTypes[1] = System.Type.GetType("System.String");
                    System.Reflection.MethodInfo methodInfo = serviceType.GetMethod("GetBy" + fkidName + "LsCode", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, paramTypes, null);

                    Object[] parameters = new Object[2];
                    parameters[0] = addVTIntNo;
                    parameters[1] = lgEntityList[i].LsCode;
                    object entityObjSelected = methodInfo.Invoke(serviceObj, parameters);

                    if (entityObjSelected == null)
                    {
                        entityType.InvokeMember(fkidName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { addVTIntNo });
                        entityType.InvokeMember("LsCode", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { lgEntityList[i].LsCode });
                        entityType.InvokeMember(DescrName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObj, new object[] { lgEntityList[i].LookupValue });
                        entityType.InvokeMember("LastUser", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                           null, entityObj, new object[] { LastUser == null ? "" : LastUser });

                        Type[] typeList = new Type[] { entityType };
                        System.Reflection.MethodInfo methodInfoSave = serviceType.GetMethod("Save", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, typeList, null);
                        methodInfoSave.Invoke(serviceObj, new object[] { entityObj });
                    }
                    else
                    {
                        entityType.InvokeMember(fkidName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { addVTIntNo });
                        entityType.InvokeMember("LsCode", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { lgEntityList[i].LsCode });
                        entityType.InvokeMember(DescrName, BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                                        null, entityObjSelected, new object[] { lgEntityList[i].LookupValue });
                        entityType.InvokeMember("LastUser", BindingFlags.SetProperty | BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance,
                           null, entityObjSelected, new object[] { LastUser == null ? "" : LastUser });

                        Type[] typeList = new Type[] { entityType };
                        System.Reflection.MethodInfo methodInfoSave = serviceType.GetMethod("Save", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance, null, CallingConventions.Any, typeList, null);
                        methodInfoSave.Invoke(serviceObj, new object[] { entityObjSelected });

                    }
                }
            }
        }


    }
}