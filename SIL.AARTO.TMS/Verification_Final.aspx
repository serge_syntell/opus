<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>


<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.Verification_Final" Codebehind="Verification_Final.aspx.cs" %>

<%--<%@ Register Assembly="Atalasoft.dotImage.WebControls" Namespace="Atalasoft.Imaging.WebControls"
    TagPrefix="cc1" %>--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <%--<table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>--%>
        <table cellspacing="0" cellpadding="0" border="0" height="90%" width="100%">
            <tr>
                <td align="center" valign="top" style="height: 610px">
                    &nbsp;&nbsp;
                </td>
                <td valign="top" align="left" colspan="1" style="height: 610px">
                    <table border="0" height="482" width="100%">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead">Verification - Stage 3 (Finalise)</asp:Label></p>
                                <p align="center">
                                    &nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" width="100%">
                                <table width="100%">
                                    <tr>
                                        <td style="height: 502px;" align="center" width="50%">
                                            <asp:Panel ID="pnlAdjudicate" runat="server" Height="50px" Width="125px">
                                                <table style="width: 257px">
                                                    <tr>
                                                        <td style="height: 92px">
                                                            <table style="width: 389px">
                                                                <tr>
                                                                    <td style="width: 139px">
                                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="SubContentHead" Width="400px">Local authority:</asp:Label><br />
                                                                    </td>
                                                                    <td style="width: 3px">
                                                                        <asp:Label ID="lblAuthority" runat="server" CssClass="SubContentHead" Width="400px"></asp:Label><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 139px; height: 20px">
                                                                        <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Width="136px">Film:</asp:Label></td>
                                                                    <td style="width: 3px; height: 20px">
                                                                        <asp:Label ID="lblFilmNo" runat="server" CssClass="NormalBold" Width="136px"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 139px; height: 21px;">
                                                                    </td>
                                                                    <td style="width: 3px; height: 21px;">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="height: 26px">
                                                            <asp:Button ID="btnAccept" runat="server" OnClick="btnAccept_Click" Text="Finalise Verification"
                                                                CssClass="NormalButton" Width="296px" /></td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlContinue" runat="server" Height="50px" Width="125px">
                                                <table style="width: 397px">
                                                    <tr>
                                                        <td align="center" style="height: 21px">
                                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="height: 21px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table style="width: 391px">
                                                                <tr>
                                                                    <td align="center" style="width: 183px">
                                                                        <asp:Button ID="btnContinue" runat="server" Text="Continue" Height="24px" Width="191px"
                                                                            OnClick="btnContinue_Click" CssClass="NormalButton" /></td>
                                                                    <td align="center">
                                                                        <asp:Button ID="btnReturn" runat="server" Text="Return to menu" OnClick="btnReturn_Click"
                                                                            CssClass="NormalButton" Height="24px" Width="191px" /></td>
                                                                          
                                            
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
                        <tr>
                            <td class="SubContentHeadSmall" valign="top" width="100%">
                            </td>
                        </tr>
                    </table>
        </table>
    </form>
</body>
</html>
