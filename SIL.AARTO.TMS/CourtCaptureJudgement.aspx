﻿<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.CourtCaptureJudgement" CodeBehind="CourtCaptureJudgement.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .style1
        {
            width: 70px;
            height: 42px;
        }

        .style2
        {
            height: 42px;
        }

        #showConfirmAmount
        {
            position: absolute;
            z-index: 100001;
            color: black;
            background-color: white;
            border-color: black;
            top: 50%;
            left: 55%;
            width: 600px;
            height: auto;
            font-size: small;
            margin-left: -200px !important;
            margin-top: -50px !important;
            margin-left: 0px;
            margin-top: 0px;
            position: fixed !important;
            position: absolute;
            _top: expression(eval(document.compatMode && document.compatMode=='CSS1Compat') ? documentElement.scrollTop + (document.documentElement.clientHeight-this.offsetHeight)/2 : /*IE6*/
            document.body.scrollTop + (document.body.clientHeight - this.clientHeight)/2); /*IE5 IE5.5*/
            _left: expression(eval(document.compatMode && document.compatMode=='CSS1Compat') ? documentElement.scrollLeft + (document.documentElement.clientWidth-this.offsetWidth)/2 : /*IE6*/
            document.body.scrollLeft + (document.body.clientWidth - this.clientWidth)/2); /*IE5 IE5.5*/
        }

        .processbg
        {
            opacity: 1;
            filter: alpha(opacity=100);
            z-index: 100000;
            display: none;
            bottom: 0;
            left: 0;
            position: fixed;
            right: 0;
            top: 0;
        }
    </style>

    <script src="Scripts/Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">

        function LoadingBlocker(msg) {
            //var content = "<div style=\"text-align:center;filter:alpha(opacity=5); -moz-opacity:0.1; -khtml-opacity: 0.1; opacity: 0.1; width:150; height:1px; margin: auto\"></div>";
            $.blockUI({
                message: ""
            });

            return true;
        }

        function VerfyAmountAndAddBlock() {
            if (ValidateAmmount()) {
                LoadingBlocker('Processing');
                return true;
            }
            return false;
        }


        //Jake 2014-05-26 modified js code to use jquery function and reconsitution code
        function ValidateAmmount() {
            // Iris 2014/03/28 added for amount range verification
            //var alMsg = document.getElementById("btnAltMsg").value;
            var alMsg = $("#btnAltMsg").val();
            var str = "";
            var strField = "";
            //var sa = document.getElementById("txtSentenceAmount").value;
            //var ca = document.getElementById("txtContemptAmount").value;
            //var oa = document.getElementById("txtOtherAmount").value;
            var sa = $("#txtSentenceAmount").val() !=undefined ? $("#txtSentenceAmount").val() :"0";
            var ca = $("#txtContemptAmount").val()!=undefined ? $("#txtContemptAmount").val() :"0";
            var oa = $("#txtOtherAmount").val() !=undefined ? $("#txtOtherAmount").val() : "0";

            var tmp_sa = sa.replace(",", "");
            var str1 = "";
            if (tmp_sa != "" && (tmp_sa < 0 || tmp_sa >= 10000)) {
                str1 = "Sentence Amount:<B>" + sa + "</B><br>";
            }

            var tmp_ca = ca.replace(",", "");
            var str2 = "";
            if (tmp_ca != "" && (tmp_ca < 0 || tmp_ca >= 10000)) {
                str2 = "Contempt Amount:<B>" + ca + "</B><br>";
            }

            var tmp_oa = oa.replace(",", "");
            var str3 = "";
            if (tmp_oa != "" && (tmp_oa < 0 || tmp_oa >= 10000)) {
                str3 = "Other Amount:<B>" + oa + "</B>";
            }

            if (str1 != "" && str2 != "" && str3 != "") {
                strField = "Sentence Amount, Contempt Amount and Other Amount";
            } else if (str1 != "" && str2 != "" && str3 == "") {
                strField = "Sentence Amount and Contempt Amount";
            } else if (str1 != "" && str2 == "" && str3 == "") {
                strField = "Sentence Amount";
            } else if (str1 != "" && str2 == "" && str3 != "") {
                strField = "Sentence Amount and Other Amount";
            } else if (str1 == "" && str2 != "" && str3 != "") {
                strField = "Contempt Amount and Other Amount";
            } else if (str1 == "" && str2 != "" && str3 == "") {
                strField = "Contempt Amount";
            } else if (str1 == "" && str2 == "" && str3 != "") {
                strField = "Other Amount";
            }
            str = str1 + str2 + str3;
            if (str != "") {
                strField = alMsg.replace("field", strField) + "<br><br>" + str;
                $("#altAmountMsg").html(strField);
                //document.getElementById("dvProcessbg").style.display = 'block';
                //document.getElementById("showConfirmAmount").style.display = 'block';

                $("#dvProcessbg").show();
                $("#showConfirmAmount").show();

                return false;
            }

            return true;
        }

    </script>
</head>
<body style="margin: 0px 0px 0px 0px; background: <%=backgroundImage %>;">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="6000">
        </asp:ScriptManager>
        <table style="height: 10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" style="width: 100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table style="height: 85%; align-content:center;" cellspacing="0" cellpadding="0" border="0" align="center" >
            <tr>
                <%-- Jake comment out on 2014-08-14 ,now sub menu on this page --%>
                <%--<td valign="top" align="center">
                       
                    <img style="height: 1px" src="images/1x1.gif" alt="" width="167" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>--%>
                <td valign="top" colspan="1" style="width: 100%; text-align: center">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                        </p>
                        <p>
                            &nbsp;
                        </p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="upd" runat="server">
                        <ContentTemplate>
                            <p>
                                <asp:Label ID="lblError" ClientIDMode="Static" CssClass="NormalRed" runat="server" />
                            </p>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%" CssClass="Normal">
                                <table style="border-style: none" class="Normal" align="center" >
                                    <tr>
                                        <th class="NormalBold" align="left">
                                            <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblCourt.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:DropDownList ID="cboCourt" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboCourt_SelectedIndexChanged"
                                                CssClass="Normal" Width="203px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="left" class="NormalBold">
                                            <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblRoom.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:DropDownList ID="cboCourtRoom" ClientIDMode="Static" runat="server" CssClass="Normal" Width="203px"
                                                AutoPostBack="True" OnSelectedIndexChanged="cboCourtRoom_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="<%$Resources:cboCourtRoom.Text %>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="NormalBold" align="left">
                                            <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblDates:.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:DropDownList ID="cboCourtDates" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboCourtDates_SelectedIndexChanged"
                                                CssClass="Normal" Width="203px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="NormalBold" align="left">
                                            <asp:Label ID="Label16" runat="server" Text="<%$Resources:CaseNo %>" />
                                        </th>
                                        <td>
                                            <asp:TextBox ID="txtCaseNo" CssClass="Normal" Width="203px" runat="server" />
                                        </td>
                                    </tr>
                                    <tr style="display: <%=_ISHRK%>">
                                        <th class="NormalBold" align="left">&nbsp;
                                        </th>
                                        <td>
                                            <asp:CheckBox ID="chkPaidAtCourt" runat="server" Text="<%$Resources:chkPaidAtCourt.Text %>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="NormalBold" align="left">&nbsp;
                                        </th>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;" colspan="2" class="NormalBold">
                                            <asp:Button ID="btnPrintCourtReport" runat="server" Text="<%$Resources:btnPrintCourtReport.Text %>" CssClass="NormalButton"
                                                OnClick="btnPrintCourtReport_Click" />
                                            <asp:Button ID="btnSelectCourtRoom" runat="server" Text="<%$Resources:btnSelectCourtRoom.Text %>" CssClass="NormalButton"
                                                OnClick="btnSelectCourtRoom_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- Court roll -->
                            <asp:Panel ID="pnlCourtRoll" runat="server" Width="97%">
                                <p class="SubContentHeadSmall">
                                    <asp:Label ID="lblCourtRollTitle" runat="server" />
                                </p>
                                <asp:Label ID="lblCourtRoll" runat="server" CssClass="NormalRed" />
                                <asp:GridView ID="grdCourtRoll" runat="server" CssClass="Normal" AllowPaging="False"
                                    AutoGenerateColumns="False" ShowFooter="true"
                                    OnRowCommand="grdCourtRoll_RowCommand">
                                    <FooterStyle CssClass="CartListHead" />
                                    <RowStyle CssClass="CartListItem" />
                                    <HeaderStyle CssClass="CartListHead" />
                                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                                    <Columns>
                                        <%--Oscar 20100916 - Use new DataFields --%>
                                        <asp:BoundField DataField="Name" HeaderText="<%$Resources:grdCourtRoll.HeaderText %>" ItemStyle-Width="200" />
                                        <asp:BoundField DataField="SummonsNo" HeaderText="<%$Resources:grdCourtRoll.HeaderText1 %>" ItemStyle-Width="200" />
                                        <asp:BoundField DataField="CaseNo" HeaderText="<%$Resources:grdCourtRoll.HeaderText2 %>" ItemStyle-Width="200" />
                                        <asp:TemplateField HeaderText="  <%$Resources:btnCourtRollAccept.Text %>  ">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCmdSelect" Text="<%$Resources:btnCourtRollAccept.Text %>" CommandName="Select" CommandArgument='<%#Bind("SumIntNo") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField DataField="AccFullName" HeaderText="Accused" />
                                    <asp:BoundField DataField="SummonsNo" HeaderText="Summons No." />
                                    <asp:BoundField DataField="SumCaseNo" HeaderText="Case No." />
                                    <asp:BoundField DataField="SChStatRef" HeaderText="Charge" />--%>
                                    </Columns>
                                </asp:GridView>
                                <pager:AspNetPager ID="grdCourtRollPager" runat="server"
                                    ShowCustomInfoSection="Right" Width="400px"
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                    FirstPageText="|&amp;lt;"
                                    LastPageText="&amp;gt;|"
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                    CustomInfoStyle="float:right;" PageSize="20" OnPageChanged="grdCourtRollPager_PageChanged" UpdatePanelId="upd">
                                </pager:AspNetPager>
                                <p style="text-align: right">
                                    <asp:Button runat="server" ID="btnCourtRollAccept" Text="<%$Resources:btnCourtRollAccept.Text %>" CssClass="NormalButton"
                                        OnClick="btnCourtRollAccept_Click" />
                                </p>
                            </asp:Panel>
                            <!-- The judgement panel -->
                            <asp:Panel runat="server" ID="pnlJudgement" Width="100%">
                                <p class="SubContentHeadSmall">
                                    <asp:Label ID="lblJudgementTitle" runat="server" Text="<%$Resources:lblJudgementTitle.Text %>" />
                                </p>
                                <table style="width: 100%; border: solid 1px black; text-align: center;">
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="lblRemandedSummonsFlag" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;" class="style1"></td>
                                        <td class="style2">
                                            <asp:Label ID="lblJudgementNavigation" runat="server" CssClass="NormalBold" />
                                        </td>
                                        <td style="text-align: right;" class="style1">
                                            <asp:Panel ID="pnlChargesListNext" runat="server">
                                                <asp:LinkButton ID="lnkNext" runat="server" Text="<%$Resources:lnkNext.Text %>" OnClick="lnkNext_Click" CssClass="Normal" />
                                            </asp:Panel>
                                            <asp:Panel ID="pnlCaptureNext" runat="server">
                                                <asp:LinkButton ID="lnkCaptureNext" runat="server" Text="<%$Resources:lnkNext.Text %>"
                                                    CssClass="Normal" OnClick="lnkCaptureNext_Click" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- Charges List -->
                            <!-- Judgement type selection -->
                            <asp:Panel ID="pnlJudgementType" runat="server" Width="100%">
                                <table style="border-style: none; text-align: left;" class="Normal">
                                    <tr id="trSelectJudgementType" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top">
                                            <asp:Label ID="Label8" runat="server" Text="<%$Resources:lblJudgementType.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:DropDownList ID="cboJudgementType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboJudgementType_SelectedIndexChanged"
                                                Width="249px" CssClass="Normal" />
                                        </td>
                                    </tr>
                                    <tr id="trVerdict" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top"
                                            align="right">
                                            <asp:Label ID="Label9" runat="server" Text="<%$Resources:lblVerdict.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:TextBox ID="txtVerdict" TextMode="MultiLine" runat="server" Width="250px" Rows="4"
                                                CssClass="Normal" />
                                        </td>
                                    </tr>
                                    <tr id="trSentence" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top"
                                            align="right">
                                            <asp:Label ID="Label10" runat="server" Text="<%$Resources:lblSentence.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:TextBox ID="txtSentence" TextMode="MultiLine" runat="server" Width="250px" Rows="4" />
                                        </td>
                                    </tr>
                                    <tr id="trSentenceAmount" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top"
                                            align="right">
                                            <asp:Label ID="Label11" runat="server" Text="<%$Resources:lblSentenceAmount.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:TextBox ID="txtSentenceAmount" runat="server" ClientIDMode="Static" />
                                            <asp:Label ID="lblFineAmount" runat="server" Visible="False"></asp:Label>
                                            <asp:Label ID="lblRevisedFineAmount" runat="server" Visible="False"></asp:Label>
                                            <asp:Button ID="btnConfirm" runat="server" CssClass="NormalButton"
                                                Text="<%$Resources:btnConfirm.Text %>" OnClick="btnConfirm_Click" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="NormalButton"
                                                Text="<%$Resources:btnCancel.Text %>" OnClick="btnCancel_Click" />
                                        </td>
                                    </tr>
                                    <tr id="trContemptAmount" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top"
                                            align="right">
                                            <asp:Label ID="Label12" runat="server" Text="<%$Resources:lblContemptAmount.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:TextBox ID="txtContemptAmount" runat="server" ClientIDMode="Static" />
                                            <asp:Label ID="lblDoubleContempAmountDescr" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trOtherAmount" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top"
                                            align="right">
                                            <asp:Label ID="Label13" runat="server" Text="<%$Resources:lblOtherAmount.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:TextBox ID="txtOtherAmount" runat="server" ClientIDMode="Static" />
                                        </td>
                                    </tr>
                                    <tr id="trNewCourtDate" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top" align="right">
                                            <asp:Label ID="lblNewCourtDate" runat="server" Text="<%$Resources:lblNewCourtDate.Text %>" CssClass="NormalBold" />
                                        </th>
                                        <td>
                                            <asp:TextBox runat="server" ID="dtpNewCourtDate" CssClass="Normal" Height="20px"
                                                autocomplete="off" UseSubmitBehavior="False" />
                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="dtpNewCourtDate"
                                                Format="yyyy/MM/dd">
                                            </cc1:CalendarExtender>

                                        </td>
                                    </tr>

                                    <tr id="trReceiptDate" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top" align="right">
                                            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:lblReceiptDate.Text %>" CssClass="NormalBold" />
                                        </th>
                                        <td>
                                            <asp:TextBox runat="server" ID="dtpReceiptDate" CssClass="Normal" Height="20px" autocomplete="off"
                                                UseSubmitBehavior="False" />
                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="dtpReceiptDate"
                                                Format="yyyy/MM/dd">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <caption>
                                        <br />
                                        <asp:Panel ID="pnlAddress" runat="server" CssClass="NormalBold" Visible="False">
                                            <table>
                                                <tr>
                                                    <td class="NormalBold" style="width: 20%; height: 40px;" valign="top">
                                                        <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblReceivedFrom.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 129px; height: 40px;" valign="top">
                                                        <asp:DropDownList ID="ddlReceivedFrom" runat="server" AutoPostBack="True"
                                                            CssClass="Normal" OnSelectedIndexChanged="ddlReceivedFrom_SelectedIndexChanged"
                                                            Width="91px">
                                                            <asp:ListItem Value="D" Text="<%$Resources:ddlReceivedFromItem.Text %>"></asp:ListItem>
                                                            <asp:ListItem Value="O" Text="<%$Resources:ddlReceivedFromItem.Text1 %>"></asp:ListItem>
                                                            <asp:ListItem Value="P" Text="<%$Resources:ddlReceivedFromItem.Text2 %>"></asp:ListItem>
                                                            <asp:ListItem Value="A" Text="<%$Resources:ddlReceivedFromItem.Text3 %>"></asp:ListItem>
                                                            <asp:ListItem Value="X" Text="<%$Resources:ddlReceivedFromItem.Text4 %>"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="height: 40px" valign="top" width="20%">
                                                        <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="height: 40px" valign="top" width="40%">
                                                        <asp:TextBox ID="txtReceivedFrom" runat="server" CssClass="Normal"
                                                            Width="224px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" valign="top" width="20%"></td>
                                                    <td valign="top"></td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblAddress" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddress.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="Normal" Height="22px"
                                                            Width="224px"></asp:TextBox>
                                                        <br />
                                                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="Normal" Height="22px"
                                                            Width="224px"></asp:TextBox>
                                                        <br />
                                                        <asp:TextBox ID="txtAddress3" runat="server" CssClass="Normal" Height="22px"
                                                            Width="224px"></asp:TextBox>
                                                        <br />
                                                        <asp:TextBox ID="txtAddress4" runat="server" CssClass="Normal" Height="22px"
                                                            Width="224px"></asp:TextBox>
                                                        <br />
                                                        <asp:TextBox ID="txtAddress5" runat="server" CssClass="Normal" Height="22px"
                                                            Width="224px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" style="width: 20%; height: 26px;">
                                                        <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblPhone.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="height: 26px">
                                                        <asp:TextBox ID="txtPhone" runat="server" CssClass="Normal" Width="119px"></asp:TextBox>
                                                    </td>
                                                    <td style="height: 26px">
                                                        <asp:Label ID="lblPostalCode" runat="server" CssClass="NormalBold"
                                                            Text="<%$Resources:lblPostalCode.Text %>" Width="87px"></asp:Label>
                                                    </td>
                                                    <td style="height: 26px" width="40%">
                                                        <asp:TextBox ID="txtPoCode" runat="server" CssClass="Normal" MaxLength="5"
                                                            Width="80px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </caption>
                                    <tr id="trDeferredPayments" runat="server">
                                        <th style="vertical-align: text-top;" class="NormalBold" valign="top" align="right">
                                            <asp:Label ID="lblDeferredPayments" runat="server" Text="<%$Resources:lblDeferredPayments.Text %>" CssClass="NormalBold" />
                                            <br />
                                            <br />
                                            <asp:Label ID="lblRemainderAmount" runat="server" CssClass="NormalRed"
                                                Text="<%$Resources:lblRemainderAmount.Text %>" />
                                        </th>
                                        <td>
                                            <asp:DataGrid ID="gridDeferredPayments" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                                AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                                FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                                Font-Size="8pt" CellPadding="4" GridLines="Vertical" CssClass="Normal" UseAccessibleHeader="True"
                                                OnItemCommand="gridDeferredPayments_ItemCommand">
                                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                                <Columns>
                                                    <%--<asp:BoundColumn DataField="SCDefPayIntNo" HeaderText="SCDefPayIntNo" Visible="false">
                                                </asp:BoundColumn>--%>
                                                    <asp:BoundColumn DataField="SumIntNo" HeaderText="SumIntNo" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PaymentDate" HeaderText="Old Payment Date" Visible="false" DataFormatString="yyyy/MM/dd"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="<%$Resources:gridDeferredPayments.HeaderText %>">
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtPaymentDate" runat="server" autocomplete="off" CssClass="Normal"
                                                                Height="20px" Text='<%# String.Format("{0:yyyy/MM/dd}", DataBinder.Eval(Container, "DataItem.PaymentDate")) %>'
                                                                UseSubmitBehavior="False" />
                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="yyyy/MM/dd" TargetControlID="txtPaymentDate">
                                                            </cc1:CalendarExtender>
                                                        </FooterTemplate>
                                                        <ItemTemplate>
                                                            <%--<asp:Label ID="lblPaymentDate" runat="server" Text='<%# String.Format("{0:yyyy/MM/dd}", DataBinder.Eval(Container, "DataItem.PaymentDate")) %>'></asp:Label>--%>
                                                            <asp:TextBox runat="server" ID="txtPaymentDate" CssClass="Normal" Height="20px" autocomplete="off"
                                                                UseSubmitBehavior="False" Text='<%# String.Format("{0:yyyy/MM/dd}", DataBinder.Eval(Container, "DataItem.PaymentDate")) %>' />
                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtPaymentDate"
                                                                Format="yyyy/MM/dd">
                                                            </cc1:CalendarExtender>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <%--<asp:TextBox ID="txtEPaymentDate" runat="server" Text='<%# String.Format("{0:yyyy/MM/dd}", DataBinder.Eval(Container, "DataItem.PaymentDate")) %>' />--%>
                                                            <asp:TextBox runat="server" ID="txtPaymentDate" CssClass="Normal" Height="20px" autocomplete="off"
                                                                UseSubmitBehavior="False" Text='<%# String.Format("{0:yyyy/MM/dd}", DataBinder.Eval(Container, "DataItem.PaymentDate")) %>' />
                                                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtPaymentDate"
                                                                Format="yyyy/MM/dd">
                                                            </cc1:CalendarExtender>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="<%$Resources:gridDeferredPayments.HeaderText1 %>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPaymentAmount" runat="server" Text='<%# String.Format("{0:0.##}", DataBinder.Eval(Container, "DataItem.PaymentAmount")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtPaymentAmount" runat="server" Text='<%# String.Format("{0:0.##}", DataBinder.Eval(Container, "DataItem.PaymentAmount")) %>' />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtPaymentAmount" runat="server" />
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnUpdate" Text="<%$Resources:gridDeferredPaymentsItem.Text %>" runat="server" CommandName="<%$Resources:gridDeferredPaymentsItem.Text %>" />
                                                            <asp:Button ID="btnDelete" Text="<%$Resources:gridDeferredPaymentsItem.Text1 %>" runat="server" CommandName="<%$Resources:gridDeferredPaymentsItem.Text1 %>" />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Button ID="btnUpdate" Text="<%$Resources:gridDeferredPaymentsItem.Text2 %>" runat="server" CommandName="<%$Resources:gridDeferredPaymentsItem.Text2 %>" />
                                                            <asp:Button ID="btnCancel" Text="<%$Resources:gridDeferredPaymentsItem.Text3 %>" runat="server" CommandName="<%$Resources:gridDeferredPaymentsItem.Text3 %>" />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Button ID="btnInsert" Text="<%$Resources:gridDeferredPaymentsItem.Text4 %>" runat="server" CommandName="<%$Resources:gridDeferredPaymentsItem.Text4 %>" />
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>

                                    <%--Oscar 20101117 - added deffered payment--%>
                                    <tr id="trDeferredPaymentAble" runat="server">
                                        <th style="vertical-align: text-bottom;" class="NormalBold" valign="bottom" align="right">
                                            <asp:Label ID="Label14" runat="server" Text="<%$Resources:lblDeferredPayment.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <%--<asp:CheckBoxList ID="cblDeferredPayment" runat="server" RepeatDirection="Horizontal" Font-Size="14px" >
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:CheckBoxList>   --%>
                                            <div style="float: left">
                                                <asp:RadioButtonList ID="rblDeferredPayment" runat="server" Font-Size="14px"
                                                    RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="Y" Text="<%$Resources:rblDeferredPaymentItem.Text %>"></asp:ListItem>
                                                    <asp:ListItem Value="N" Text="<%$Resources:rblDeferredPaymentItem.Text1 %>"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div style="float: left; width: 50px"></div>
                                            <div>
                                                <asp:Button ID="btnContinueToCharge" CssClass="NormalButton" runat="server"
                                                    Text="<%$Resources:btnContinueToCharge.Text %>" OnClick="btnContinueToCharge_Click" />
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" style="text-align: right;" class="NormalBold" valign="top">
                                            <asp:Button ID="btnRecordJudgement" runat="server" Text="<%$Resources:btnRecordJudgement.Text %>" CssClass="NormalButton"
                                                OnClick="btnRecordJudgement_Click" OnClientClick="return VerfyAmountAndAddBlock();" />
                                        </td>
                                    </tr>

                                    <%--Oscar 20101117 - added continue to charges--%>
                                    <%--<tr id="trContinueToCharge" runat="server">
                                    <td colspan="2" style="text-align: right;" class="NormalBold" valign="top">
                                        <asp:LinkButton ID="lnkbtnContinueToCharges" runat="server" 
                                            onclick="lnkbtnContinueToCharges_Click">Continue to charges</asp:LinkButton>
                                    </td>
                                </tr>--%>
                                </table>
                                <input type="hidden" value="<%$Resources:lblError.Text69 %>" id="btnAltMsg" runat="server" />
                            </asp:Panel>
                            <asp:Panel ID="pnlBenchWOA" runat="server" Visible="false">
                                <table>
                                    <tr>
                                        <td class="NormalBold">
                                            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:lblBenchWoaNumber.Text %>" />:</td>
                                        <td>
                                            <asp:Label runat="server" ID="lblBenchWOANumber"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:Button runat="server" CssClass="NormalButton" ID="btnGenerateBenchWOA" Text="<%$Resources:btnGenerateBenchWOA.Text %>" OnClick="btnGenerateBenchWOA_Click" OnClientClick="LoadingBlocker('Processing');" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlDoubleWOA" runat="server" Visible="false">
                                <table style="margin: 5px; padding: 5px;">
                                    <tr>
                                        <td colspan="2" class="NormalBold">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:lblPreviousWOA.Text %>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:GridView ID="gvPreviousWOA" runat="server" Width="100%" AutoGenerateColumns="False">
                                                <HeaderStyle CssClass="CartListHead" />
                                                <RowStyle CssClass="CartListItem" />
                                                <Columns>
                                                    <asp:BoundField DataField="WOANumber" HeaderText="<%$Resources:headWOANumber.Text %>" />
                                                    <asp:BoundField DataField="WOAIssueDate" HtmlEncode="false" DataFormatString="{0:yyyy/MM/dd}" HeaderText="<%$Resources:headWOAIssueDate.Text %>" />
                                                    <asp:BoundField DataField="WOAFineAmount" HtmlEncode="false" DataFormatString="{0:N2}" HeaderText="<%$Resources:headWOAFineAmount.Text %>" />
                                                    <asp:BoundField DataField="WOAAddAmount" HtmlEncode="false" DataFormatString="{0:N2}" HeaderText="<%$Resources:headWOAAddAmount.Text %>" />
                                                    <asp:BoundField DataField="WOAGeneratedDate" HtmlEncode="false" DataFormatString="{0:yyyy/MM/dd}" HeaderText="<%$Resources:headWOAGenerateDate.Text %>" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="NormalBold">
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:lblDoubleWoaNumber.Text%>" />:</td>
                                        <td>
                                            <asp:Label runat="server" ID="lblDoubleWOANumber"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:Button runat="server" CssClass="NormalButton" ID="btnGenerateDoubleWOA" Text="<%$Resources:btnGenerateDoubleWOA.Text %>" OnClick="btnGenerateDoubleWOA_Click" OnClientClick="LoadingBlocker('Processing');" /></td>
                                    </tr>
                                </table>

                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" /><asp:Label
                                    ID="Label15" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label>
                            </p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center"></td>
               
            </tr>
        </table>
        <table style="height: 5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width: 100%"></td>
            </tr>
        </table>
        <div id="showConfirmAmount" style="display: none; border-width: 1px; border-style: solid; border-color: black">
            <br>
            <div id="altAmountMsg" style="width: auto; text-align: left; margin-left: 20px">The amount you entered in field appears to be suspect. Please confirm amount before proceeding</div>
            <br>
            <table style="height: auto; text-align: center" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <asp:Button ID="btnAccept" runat="server" CssClass="NormalButton" Text="Accept" OnClick="btnAccept_Click" OnClientClick="document.getElementById('showConfirmAmount').style.display='none'; return LoadingBlocker('Processing');" />
                    </td>
                    <td>
                        <asp:Button ID="btnReject" runat="server" CssClass="NormalButton" Text="Reject" OnClick="btnReject_Click" />
                    </td>
                </tr>
            </table>
            <br>
        </div>
        <div id="dvProcessbg" class="processbg">
        </div>
    </form>
</body>
</html>
