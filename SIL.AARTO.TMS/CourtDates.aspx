<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="true" Inherits="Stalberg.TMS.CourtDates" Codebehind="CourtDates.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat =server>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
 </head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server" type="digit.form.Form" method="post" action="">
       <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAuthority" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptAuthority.Text %>" OnClick="btnOptAuthority_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:UpdatePanel ID="upd" runat="server">
                        <ContentTemplate>
                            <table border="0" width="568" height="482">
                                <tr>
                                    <td valign="top" height="47">
                                        <p align="center">
                                            <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                        <p>
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 28px" valign="top">
                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label><asp:DropDownList
                                            ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged"
                                            Width="217px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="height: 28px" valign="top">
                                        <asp:Label ID="lblSelCourt" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblSelCourt.Text %>"></asp:Label><asp:DropDownList
                                            ID="ddlSelCourt" runat="server" AutoPostBack="True" CssClass="Normal" OnSelectedIndexChanged="ddlSelCourt_SelectedIndexChanged"
                                            Width="217px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="height: 28px" valign="top">
                                        <asp:Label ID="lblSelCourtRoom" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblSelCourtRoom.Text %>"></asp:Label><asp:DropDownList
                                            ID="ddlSelCourtRoom" runat="server" AutoPostBack="True" CssClass="Normal" OnSelectedIndexChanged="ddlSelCourtRoom_SelectedIndexChanged"
                                            Width="217px">
                                        </asp:DropDownList></td>
                                </tr>
                               <tr>
                                    <td style="height: 28px" valign="top">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:Panel ID="pnlGeneral" runat="server">
                                            <asp:DataGrid ID="dgCourtDates" Width="495px" runat="server" BorderColor="Black"
                                                AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                                FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                                Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgCourtDates_ItemCommand"
                                                >
                                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                                <Columns>
                                                    <asp:BoundColumn Visible="False" DataField="CDIntNo" HeaderText="CDIntNo"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CDate" HeaderText="<%$Resources:dgCourtDates.HeaderText %>" DataFormatString="{0:yyyy-MM-dd}">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CDNoOfCases" HeaderText="<%$Resources:dgCourtDates.HeaderText1 %>"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="OGCode" HeaderText="<%$Resources:dgCourtDates.HeaderText2 %>"></asp:BoundColumn>
                                                    <asp:ButtonColumn Text="<%$Resources:dgCourtDatesItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                                </Columns>
                                                <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                            </asp:DataGrid>
                                            <pager:AspNetPager id="dgCourtDatesPager" runat="server" 
                                            showcustominfosection="Right" width="495px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;" 
                                            onpagechanged="dgCourtDatesPager_PageChanged" UpdatePanelId="upd"></pager:AspNetPager>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlAddCourt" runat="server" Height="127px">
                                            <table id="Table2" cellspacing="1" cellpadding="1" width="654" border="0"><tr>
                                                    <td height="2" style="width: 216px">
                                                        <asp:Label ID="lblAddCourtDate" runat="server" CssClass="ProductListHead" Width="153px" Text="<%$Resources:lblAddCourtDate.Text %>"></asp:Label></td>
                                                    <td height="2" style="width: 247px">
                                                    </td>
                                                    <td height="2" style="width: 241px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="lblAddCDate" runat="server" CssClass="NormalBold" Width="152px" Text="<%$Resources:lblAddCDate.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                       <asp:TextBox runat="server" ID="dtpAddDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpAddDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td >
                                                    </td>
                                                </tr>
                                                
                                                <!-- <tr>
                                                    <td height="25" style="width: 216px">
                                                        <p>
                                                            <asp:Label ID="Label5" runat="server" Width="190px" CssClass="NormalBold" Visible="false">No of S56 allocated cases:</asp:Label></p>
                                                    </td>
                                                    <td height="25" style="width: 247px">
                                                        <asp:TextBox ID="txtS56Add" runat="server" Width="90px" CssClass="Normal" MaxLength="2" ReadOnly ="true" Visible="false"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 216px">
                                                        <p>
                                                            <asp:Label ID="Label6" runat="server" Width="245px" CssClass="NormalBold" Visible="false">No of S54 allocated cases:</asp:Label></p>
                                                    </td>
                                                    <td height="25" style="width: 247px">
                                                        <asp:TextBox ID="txtS54Add" runat="server" Width="90px" CssClass="Normal" MaxLength="2" ReadOnly ="true" Visible="false"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 216px">
                                                        <p>
                                                            <asp:Label ID="Label7" runat="server" Width="207px" CssClass="NormalBold" Visible="false">Percentage of S56 allocations:</asp:Label></p>
                                                    </td>
                                                    <td height="25" style="width: 247px">
                                                        <asp:TextBox ID="txtPercentageAdd" runat="server" Width="90px" CssClass="Normal" MaxLength="2" ReadOnly ="true" Visible="false"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 216px">
                                                        <p>
                                                            <asp:Label ID="Label8" runat="server" Width="207px" CssClass="NormalBold">Current total allocated:</asp:Label></p>
                                                    </td>
                                                    <td height="25" style="width: 247px">
                                                        <asp:TextBox ID="txtTotalAdd" runat="server" Width="90px" CssClass="Normal" MaxLength="2" ReadOnly ="true"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr> -->
                                                <tr>
                                                    <td style="width: 216px; height: 25px">
                                                        <p>
                                                            <asp:Label ID="lblAddCNoOfCases" runat="server" Width="151px" CssClass="NormalBold" Text="<%$Resources:lblAddCNoOfCases.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td style="width: 247px; height: 25px">
                                                        <asp:TextBox ID="txtAddCDNoOfCases" runat="server" Width="90px" CssClass="Normal"
                                                            MaxLength="6"></asp:TextBox></td>
                                                    <td style="width: 241px; height: 25px;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 216px; height: 25px">
                                                        <p>
                                                            <asp:Label ID="lblAddNoAOGMax" runat="server" Width="151px" CssClass="NormalBold" Text="<%$Resources:lblAddNoAOGMax.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td style="width: 247px; height: 25px">
                                                        <asp:TextBox ID="txtAddNoAOGMax" runat="server" Width="90px" CssClass="Normal"
                                                            MaxLength="6"></asp:TextBox></td>
                                                    <td style="width: 241px; height: 25px;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td style="width: 216px; height: 25px">
                                                        <p>
                                                            <asp:Label ID="Label3" runat="server" Width="151px" CssClass="NormalBold" Text="<%$Resources:lblAddNoAOGS35Max.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td style="width: 247px; height: 25px">
                                                        <asp:TextBox ID="txtAddNoAofS35Max" runat="server" Width="90px" CssClass="Normal"
                                                            MaxLength="6"></asp:TextBox></td>
                                                    <td style="width: 241px; height: 25px;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 216px; height: 25px">
                                                        <p>
                                                            <asp:Label ID="lblCDOriginGroup" runat="server" Width="151px" CssClass="NormalBold" Text="<%$Resources:lblCDOriginGroup.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td style="width: 247px; height: 25px">
                                                        <asp:DropDownList
                                            ID="ddlAddOriginGroup" runat="server" CssClass="Normal" Width="260px"></asp:DropDownList>
                                                        </td>
                                                    <td style="width: 241px; height: 25px;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 216px; height: 25px">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 247px; height: 25px">
                                                        <asp:Button ID="Button1" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddCourtDate.Text %>"
                                                            OnClick="btnAddCDate_Click"></asp:Button></td>
                                                    <td style="width: 241px; height: 25px;">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlUpdateCourt" runat="server">
                                            <table id="Table3" cellspacing="1" cellpadding="1" width="654" border="0">
                                                <tr>
                                                    <td height="2" style="width: 208px">
                                                        <asp:Label ID="Label19" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblUpdCourtDate.Text %>"></asp:Label></td>
                                                    <td width="248" height="2">
                                                    </td>
                                                    <td height="2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="height: 17px; width: 208px;">
                                                        <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCourtDate.Text %>"></asp:Label></td>
                                                    <td valign="top" width="248" style="height: 17px">
                                               <asp:TextBox runat="server" ID="dtpDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                TargetControlID="dtpDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td style="height: 17px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 208px">
                                                        <p>
                                                            <asp:Label ID="lblS56Alloc" runat="server" Width="190px" CssClass="NormalBold" Text="<%$Resources:lblS56Alloc.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td width="248" height="25">
                                                        <asp:TextBox ID="txtS56" runat="server" Width="90px" CssClass="Normal" MaxLength="2"
                                                            ReadOnly="true"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 208px">
                                                        <p>
                                                            <asp:Label ID="lblS54Alloc" runat="server" Width="190px" CssClass="NormalBold" Text="<%$Resources:lblS54Alloc.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td width="248" height="25">
                                                        <asp:TextBox ID="txtS54" runat="server" Width="90px" CssClass="Normal" MaxLength="2"
                                                            ReadOnly="true"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 208px">
                                                        <p>
                                                            <asp:Label ID="lblS56AllocPercent" runat="server" Width="245px" CssClass="NormalBold" Text="<%$Resources:lblS56AllocPercent.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td width="248" height="25">
                                                        <asp:TextBox ID="txtPercent" runat="server" Width="90px" CssClass="Normal" MaxLength="2"
                                                            ReadOnly="true"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 216px">
                                                        <p>
                                                            <asp:Label ID="lblAllocated" runat="server" Width="207px" CssClass="NormalBold" Text="<%$Resources:lblAllocated.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td height="25" style="width: 247px">
                                                        <asp:TextBox ID="txtTotal" runat="server" Width="90px" CssClass="Normal" MaxLength="2"
                                                            ReadOnly="true"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                 <tr>
                                                    <td height="25" style="width: 216px">
                                                        <p>
                                                            <asp:Label ID="lblS56Res" runat="server" Width="207px" CssClass="NormalBold" Text="<%$Resources:lblS56Res.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td height="25" style="width: 247px">
                                                        <asp:TextBox ID="txtS56Reserved" runat="server" Width="90px" CssClass="Normal" MaxLength="2"
                                                            ReadOnly="true"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                 <tr>
                                                    <td height="25" style="width: 216px">
                                                        <p>
                                                            <asp:Label ID="lblS54Res" runat="server" Width="207px" CssClass="NormalBold" Text="<%$Resources:lblS54Res.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td height="25" style="width: 247px">
                                                        <asp:TextBox ID="txtS54Reserved" runat="server" Width="90px" CssClass="Normal" MaxLength="2"
                                                            ReadOnly="true"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                 <tr>
                                                    <td height="25" style="width: 216px">
                                                        <p>
                                                            <asp:Label ID="lblUtilised" runat="server" Width="207px" CssClass="NormalBold" Text="<%$Resources:lblUtilised.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td height="25" style="width: 247px">
                                                        <asp:TextBox ID="txtUtilised" runat="server" Width="90px" CssClass="Normal" MaxLength="2"
                                                            ReadOnly="true"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                               <tr>
                                                    <td height="25" style="width: 208px">
                                                        <p>
                                                            <asp:Label ID="Label17" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblAddCNoOfCases.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td width="248" height="25">
                                                        <asp:TextBox ID="txtCDNoOfCases" runat="server" Width="90px" CssClass="Normal" MaxLength="21"></asp:TextBox></td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 216px; height: 25px">
                                                        <p>
                                                            <asp:Label ID="Label2" runat="server" Width="151px" CssClass="NormalBold" Text="<%$Resources:lblAddNoAOGMax.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td style="width: 247px; height: 25px">
                                                        <asp:TextBox ID="txtNoAOGMax" runat="server" Width="90px" CssClass="Normal"
                                                            MaxLength="6"></asp:TextBox></td>
                                                    <td style="width: 241px; height: 25px;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td style="width: 216px; height: 25px">
                                                        <p>
                                                            <asp:Label ID="Label4" runat="server" Width="151px" CssClass="NormalBold" Text="<%$Resources:lblAddNoAOGS35Max.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td style="width: 247px; height: 25px">
                                                        <asp:TextBox ID="txtNoAOGS35Max" runat="server" Width="90px" CssClass="Normal"
                                                            MaxLength="6"></asp:TextBox></td>
                                                    <td style="width: 241px; height: 25px;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 208px">
                                                        <p>
                                                            <asp:Label ID="lblEditOriginGroup" runat="server" Width="94px" CssClass="NormalBold" Text="<%$Resources:lblCDOriginGroup.Text %>"></asp:Label></p>
                                                    </td>
                                                    <td width="248" height="25">
                                                    <asp:DropDownList
                                            ID="ddlEditOriginGroup" runat="server" CssClass="Normal" Width="260px"></asp:DropDownList>
                                                        </td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25" style="width: 208px">
                                                        &nbsp;
                                                    </td>
                                                    <td width="248" height="25">
                                                        <asp:Button ID="Button2" runat="server" CssClass="NormalButton" Text="<%$Resources:benUpdCDate.Text %>"
                                                            OnClick="btnUpdateCDate_Click"></asp:Button>
                                                    </td>
                                                    <td height="25">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label2"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
   </form>
   </body>
</html>
