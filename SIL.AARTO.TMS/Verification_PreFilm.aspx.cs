using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the first step in validating file data
    /// </summary>
    public partial class Verification_PreFilm : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private int userIntNo = 0;
        private int autIntNo = 0;
        private int processType = int.MinValue;
        private string autName = string.Empty;
        private string reqOfficerNo = "Y";
        private bool isNaTISLast = false;
        private bool adjAt100 = false;
        private bool showDiscrepanciesOnly = true;

        private FilmDB films = null;
        private AuthorityRulesDB authRules = null;
        private FrameDB frame = null;

        protected string thisPage = "Verification/Adjudication";
        protected string thisPageURL = "Verification_PreFilm.aspx";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string sSource = string.Empty;

        //private bool noFrames = false;

        // Constants
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //get user details
            UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;

            userIntNo = Convert.ToInt32(Session["userIntNo"]);
            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            autName = Session["autName"].ToString();
            //int 

            // FBJ Added (2006-08-03): Support for query string parameter
            if (this.processType == int.MinValue)
            {
                if (Request.QueryString["process"] != null)
                {
                    this.processType = int.Parse(Request.QueryString["process"].ToString());
                    Session["processType"] = this.processType.ToString();
                }
            }
            if (Session["processType"] != null)
                this.processType = int.Parse(Session["processType"].ToString());

            // If there's no process type then log out
            if (this.processType == int.MinValue)
                Response.Redirect("Login.aspx?Login=invalid");

            this.isNaTISLast = (bool)this.Session["NaTISLast"];

            Helper_Web.AddPageBlocker(this.buttonBookOut);

            //dls 090618 - add this into the session variable, so that we only ahve to do it once, not for every frame!
            if (Session["adjAt100"] == null)
            {
                this.adjAt100 = GetRuleFullAdjudicated();
                Session.Add("adjAt100", this.adjAt100);
            }
            else
            {
                this.adjAt100 = (bool)Session["adjAt100"];
            }

            //dls 090618 - add this into the session variable, so that we only ahve to do it once, not for every frame!
            if (Session["showDiscrepanciesOnly"] == null)
            {
                this.showDiscrepanciesOnly = GetRuleShowDiscrepanciesOnly();
                Session.Add("showDiscrepanciesOnly", this.showDiscrepanciesOnly);
            }
            else
            {
                this.showDiscrepanciesOnly = (bool)Session["showDiscrepanciesOnly"];
            }

            this.films = new FilmDB(this.connectionString);
            this.authRules = new AuthorityRulesDB(connectionString);
            this.frame = new FrameDB(this.connectionString);

            if (!this.IsPostBack)
            {
                GetDataForQueue(true);

                Session["SaveImageSettingsForFilm"] = null;

                SqlDataReader reader = null;
                ValidationStatus status = ValidationStatus.None;
                if (this.isNaTISLast)
                    status = ValidationStatus.NaTISLast;

                switch (this.processType)
                {
                    case 1:
                    case 2:
                        status |= ValidationStatus.Verification;
                        reader = films.GetAuthorityFilmsForVerification(autIntNo, status);
                        pnlOfficerNo.Visible = false;
                        break;

                    case 3:
                        status |= ValidationStatus.Adjudication;
                        reader = films.GetAuthorityFilmsForVerification(autIntNo, status);

                        int arCode = 0;
                        //int arIntNo = authRules.GetAuthorityRulesByCode(autIntNo, "0560", "Require officer no. to be entered at adjudication", ref arCode, ref reqOfficerNo, "Y = Yes, N = No", login);
                        AuthorityRulesDetails rule = new AuthorityRulesDetails();
                        rule.AutIntNo = this.autIntNo;
                        rule.ARCode = "0560";
                        rule.LastUser = login;

                        DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                        KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
                        arCode = value.Key;
                        reqOfficerNo = value.Value;

                        //LMZ 23-03-2007 changed to allow for user to be linked to officer
                        string sOfficerNo = Session["OfficerNo"].ToString();
                        this.txtOfficerNo.Text = sOfficerNo;

                        if (reqOfficerNo.Equals("Y"))
                        {
                            this.pnlOfficerNo.Visible = true;
                            this.txtOfficerNo.Text = string.Empty;
                            if (Session["OfficerNo"] != null)
                                txtOfficerNo.Text = Session["OfficerNo"].ToString();
                            else
                                txtOfficerNo.Text = string.Empty;

                            bool proceed = this.CheckOfficerNo(txtOfficerNo.Text.Trim());

                            if (Session["adjOfficerName"] != null)
                                lblOfficerName.Text = Session["adjOfficerName"].ToString();
                            else
                                lblOfficerName.Text = string.Empty;
                        }
                        else
                            pnlOfficerNo.Visible = false;
                        break;
                }

                if (reader.HasRows)
                {
                    this.grdHeader.DataSource = reader;
                    this.grdHeader.DataBind();

                    switch (this.processType)
                    {
                        case 1:
                            this.lblAuthority.Text = (string)GetLocalResourceObject("lblAuthority.Text") + autName;
                            this.pnlOfficerNo.Visible = false;
                            break;

                        case 2:
                            this.lblAuthority.Text = (string)GetLocalResourceObject("lblAuthority.Text1") + autName;
                            this.pnlOfficerNo.Visible = false;
                            break;

                        case 3:
                            this.lblAuthority.Text = (string)GetLocalResourceObject("lblAuthority.Text2") + autName;
                            this.pnlOfficerNo.Visible = true;
                            break;
                    }
                    this.buttonBookOut.Visible = true;
                }
                else
                {
                    this.lblAuthority.Text = (string)GetLocalResourceObject("lblAuthority.Text3");
                    switch (this.processType)
                    {
                        case 1:
                            this.lblAuthority.Text += (string)GetLocalResourceObject("lblAuthority.Text4");
                            break;

                        case 2:
                            this.lblAuthority.Text += (string)GetLocalResourceObject("lblAuthority.Text5");
                            break;

                        case 3:
                            this.lblAuthority.Text += (string)GetLocalResourceObject("lblAuthority.Text6");
                            break;
                    }
                    this.lblAuthority.Text += (string)GetLocalResourceObject("lblAuthority.Text7") +autName;
                    this.buttonBookOut.Visible = false;
                    this.pnlOfficerNo.Visible = false;
                }
            }
            GetDataForQueue(false);
        }

        private void GetDataForQueue(bool firstLoad)
        {
            if (firstLoad || Session["NoOfDays"] == null)
            {
                DateRulesDetails dateRule = new DateRulesDetails();
                dateRule.AutIntNo = autIntNo;
                dateRule.DtRStartDate = "NotOffenceDate";
                dateRule.DtREndDate = "NotIssue1stNoticeDate";
                dateRule.LastUser = login;
                DefaultDateRules rule = new DefaultDateRules(dateRule, connectionString);
                Session["NoOfDays"] = rule.SetDefaultDateRule();

                AuthorityDB autDB = new AuthorityDB(connectionString);
                Session["AutCode"] = autDB.GetAuthorityDetails(autIntNo).AutCode;
            }
        }

        private bool GetRuleShowDiscrepanciesOnly()
        {
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();

            arDetails.AutIntNo = this.autIntNo;
            arDetails.ARCode = "0550";
            arDetails.LastUser = this.login;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            KeyValuePair<int, string> discrepancies = ar.SetDefaultAuthRule();
            return discrepancies.Value.Equals("Y");
        }

        private bool GetRuleFullAdjudicated()
        {
            //tf 20090611 - get AuthorityRule for 100% adjuducated
            //AuthorityRulesDB arDB = new AuthorityRulesDB(connectionString);
            //AuthorityRulesDetails arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "0600", "Rule to ensure that 100% of frames are Adjudicated",
            //    0, "N", "Y - Yes; N - No(Default)", userName);

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();

            arDetails.AutIntNo = this.autIntNo;
            arDetails.ARCode = "0600";
            arDetails.LastUser = this.login;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            KeyValuePair<int, string> adj100 = ar.SetDefaultAuthRule();
            return adj100.Value.Equals("Y");
        }

        // Book out the next film
        protected void buttonBookOut_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button) sender;
            sSource = btn.Text;
            // we need to release any films previously locked by this user
            LockFilmForUser(0, 0);

            switch (this.processType)
            {
                case 1:
                case 2:
                    this.ProcessFramesForVerification(this.processType);
                    break;

                case 3:
                    bool proceed = true;

                    if (pnlOfficerNo.Visible.Equals(true))
                        proceed = this.CheckOfficerNo(txtOfficerNo.Text.Trim());

                    if (proceed)
                    {
                        // LMZ 14-03-2007 - used for officer statistics
                        try
                        {
                            int nUSIntNo = Convert.ToInt32(Session["USIntNo"].ToString());
                            UserDB db = new UserDB(connectionString);
                            db.UserShiftEdit(nUSIntNo, txtOfficerNo.Text.Trim(), "Officer", this.login);
                        }
                        catch { }

                        this.ProcessFramesForAdjudication();
                    }
                    break;
            }
        }

        private bool CheckOfficerNo(string toNo)
        {
            bool proceed = true;
            string toName = string.Empty;

            if (toNo.Equals(""))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                proceed = false;
                return proceed;
            }

            TrafficOfficerDB officer = new TrafficOfficerDB(connectionString);
            int toIntNo = officer.ValidateTrafficOfficer(autIntNo, toNo, ref toName);

            if (toIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                proceed = false;
            }
            else
            {
                Session["OfficerNo"] = toNo;
                Session["adjOfficerName"] = toName;
            }
            return proceed;
        }

        private void ProcessFramesForVerification(int processType)
        {
            ValidationStatus status = ValidationStatus.Verification;
            if (this.isNaTISLast)
                status |= ValidationStatus.NaTISLast;
            lblError.Text = string.Empty;
            int filmIntNo = 0;
            bool bValidateDate = false;
            
            SqlDataReader filmList = films.GetNextFilmForVerificationByUser(autIntNo, status, login);

            if (!filmList.HasRows)
            {
                lblError.Visible = true;
                if (processType == 1)
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                else
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                filmList.Dispose();
            }
            else
            {
                try
                {
                    //// we need to release any films previously locked by this user
                    //LockFilmForUser(0);           -- move this to just after book out, so that films are cleared immediately
                    filmList.Read();
                    filmIntNo = Convert.ToInt32(filmList["FilmIntNo"].ToString());

                    //dls 0712102 - added check for rowversion to prevent simultaneous bookout
                    //dls 090527 - db is returning a BIGINT!
                    //int rowversion = Convert.ToInt32(filmList["rowversion"]);
                    Int64 rowversion = Convert.ToInt64(filmList["rowversion"]);

                    if (filmList["ValidateDataDateTime"] != DBNull.Value)
                    {
                        bValidateDate = true;
                    }
                   
                    string filmNo = filmList["FilmNo"].ToString();

                    filmList.Dispose();

                    bool failed = LockFilmForUser(filmIntNo, rowversion);

                    if (failed) return;

                    SetFrameList(filmIntNo, status, filmNo);

                    this.Session["filmIntNo"] = filmIntNo;

                    if (!bValidateDate)
                    {
                        this.Session["cvPhase"] = 0;
                        Response.Redirect("Verification_Film.aspx");
                    }
                    else
                    {
                        this.Session["cvPhase"] = 1;
                        Response.Redirect("Verification_Frame.aspx");
                    }
                }
                catch (Exception e)
                {
                    string msg = e.Message;
                }
            }
        }

        private void ProcessFramesForAdjudication()
        {
            int filmIntNo = 0;
            ValidationStatus status = ValidationStatus.Adjudication;
            bool isNaTISLate = (bool)this.Session["NaTISLast"];
            if (isNaTISLate)
                status |= ValidationStatus.NaTISLast;
            lblError.Text = string.Empty;
            bool bValidateDate = false;

            SqlDataReader filmList = films.GetNextFilmForAdjudicationByUser(autIntNo, status, login);

            if (!filmList.HasRows)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                filmList.Dispose();
            }
            else
            {
                try
                {
                    //// we need to release any films previously locked by this user
                    //LockFilmForUser(0);           -- move this to just after book out, so that films are cleared immediately
                    filmList.Read();
                    // LMZ - 2007-05-23 Updated to do away with application object
                    filmIntNo = Convert.ToInt32(filmList["FilmIntNo"]);

                    //dls 0712102 - added check for rowversion to prevent simultaneous bookout
                    //dls 090527 - db is returning a BIGINT!
                    //int rowversion = Convert.ToInt32(filmList["rowversion"]);
                    Int64 rowversion = Convert.ToInt64(filmList["rowversion"]);

                    if (filmList["ValidateDataDateTime"] != DBNull.Value)
                    {
                        bValidateDate = true;
                    }

                    string filmNo = filmList["FilmNo"].ToString();

                    filmList.Dispose();

                    bool failed = LockFilmForUser(filmIntNo, rowversion);

                    if (failed) return;

                    SetFrameList(filmIntNo, status, filmNo);

                    this.Session["filmIntNo"] = filmIntNo;

                    if (!bValidateDate)
                    {
                        this.Session["cvPhase"] = 0;
                        Response.Redirect("Verification_Film.aspx");
                    }
                    else
                    {
                        this.Session["cvPhase"] = 1;
                        if (sSource.IndexOf("Test") >= 0)
                            Response.Redirect("TestAdjudication_Frame.aspx");
                        else
                            Response.Redirect("Adjudication_Frame.aspx");
                    }
                }
                catch { }           //this is here because the thread gets aborted
                //catch (Exception e)
                //{
                //    string message = e.Message;
                //}

            }
        }

        private bool LockFilmForUser(int filmIntNo, Int64 rowversion)
        {
            bool failed = false;
            Int32 updFilmIntNo = films.LockUser(filmIntNo, login, rowversion);
            if (updFilmIntNo < 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                failed = true;
            }
            return failed;
        }

        private void SetFrameList(int filmIntNo, ValidationStatus status, string filmNo)
        {
            FrameList frames = null;

            if ((status & ValidationStatus.Verification) == ValidationStatus.Verification)
                frames = new FrameList(status, filmNo, frame.FrameToVerify(filmIntNo, this.autIntNo, login, status, this.showDiscrepanciesOnly));
            else
                frames = new FrameList(status, filmNo, frame.FrameToAdjudicate(filmIntNo, status));

            this.Session["FrameList"] = frames;
        }

    }
}