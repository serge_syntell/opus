using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.PostalManagement;
using System.Transactions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represent a page that displays the print files that can be printed.
    /// </summary>
    public partial class PrintFirstNotice : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private string cType = "CAM";
        private int statusLoaded = 10;
        private int statusPrinted = 250;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "PrintFirstNotice.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Select the next print file for printing of the first notice";

        //private const string RLV_DESCR = "Red Light Violation";
        //private const string SPD_DESCR = "Speed Violations";
        //private const string ASD_DESCR = "Average Speed over Distance Violations";
        //private const string NAG_DESCR = "No Admission of Guilt";
        //private const string EXP_DESCR = "Expired";
        private const int NAG_STATUS = 500;
        private const int NAG_PRINTED_STATUS = 510;
        private const int EXPIRED_STATUS = 921;

        private const string DATE_FORMAT = "yyyy-MM-dd";
        // 20120807 Nick add for report engine
        protected bool isIBMPrinter = false;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            login = userDetails.UserLoginName;
            //int 

            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            this.pnlGeneral.Visible = true;
            this.dgPrintrun.Visible = true;

            if (ddlSelectLA.SelectedIndex > -1)
            {
                this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            }
            else
            {
                this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            }

            AuthorityDB authDB = new AuthorityDB(this.connectionString);
            AuthorityDetails authDetails =  authDB.GetAuthorityDetails(autIntNo);

            //Jerry 2012-12-28 changed
            //switch (authDetails.AutTicketProcessor.ToLower())
            //{
            //    case "cipruspi":
            //        this.cType = authDetails.AutTicketProcessor;
            //        this.statusLoaded = 210;
            //        this.statusPrinted = 220;
            //        break;
            //    case "cip_cofct":
            //        this.statusLoaded = 210;
            //        this.statusPrinted = 220;
            //        break;
            //    case "cip_aarto":
            //        this.statusLoaded = NAG_STATUS;
            //        this.statusPrinted = NAG_PRINTED_STATUS;
            //        break;
            //}

            // 20120726 Nick added for report engine
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = this.autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");
            //2014-11-11 Heidi added for set dateFrom TextBox can't input(bontq1669)
            this.dateFrom.Attributes.Add("ReadOnly", "true");
            this.dateTo.Attributes.Add("ReadOnly", "true");

            if (!Page.IsPostBack)
            {
                this.lblDateFrom.Visible = false;
                this.lblDateTo.Visible = false;
                this.dateFrom.Visible = false;
                this.dateTo.Visible = false;

                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlUpdate.Visible = false;
                pnlCancel.Visible = false;

                this.chkShowAll.Checked = false;

                this.PopulateAuthorities(this.autIntNo);

                // Check all remote image file server 
                bool isAllImageFileServerAccessible = false;
                if (Session["AllImageFileServerAccessible"] != null)
                {
                    isAllImageFileServerAccessible = (bool)Session["AllImageFileServerAccessible"];
                }
                if (isAllImageFileServerAccessible == false)
                {
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorScriptIFS", "alter('Sorry, Image file server unaccessible, you can't do anything for this page. Please check the connection first!');", true);
                    //Server.Transfer("Login.aspx?Login=invalid");
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                    return;
                }
                else
                {
                    this.BindGrid(this.autIntNo, cType);

                    this.lblInstruct.Visible = false;
                    if (ddlSelectLA.SelectedIndex > -1)
                    {
                        this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
                        Session["printAutIntNo"] = autIntNo;
                        this.TicketNumberSearch1.AutIntNo = this.autIntNo;
                    }
                }
            }
            //else if (Request.Form["btnSearch"] != "")//2014-11-11 Heidi comment out for fixing when click"Refresh" button,page can't back to first page(bontq1669)
            //{
            //    BindGrid(this.autIntNo, cType);
            //}
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorities(int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        public void BindGrid(int autIntNo, string cType)
        {
            string showAll = chkShowAll.Checked ? "Y" : "N";
            //showAll = "Y";
            Stalberg.TMS.NoticeDB printList = new Stalberg.TMS.NoticeDB(connectionString);
            //2014-11-05 Heidi fixed problem that when first click"Include completed print files" button,not display have been printed document(bontq1669)
            DateTime dtFrom = lblDateFrom.Visible ? DateTime.Parse(dateFrom.Text) : chkShowAll.Checked? DateTime.Now.AddMonths(-1):DateTime.MinValue;
            DateTime dtTo = lblDateTo.Visible ? DateTime.Parse(dateTo.Text) :chkShowAll.Checked? DateTime.Now.AddDays(-1): DateTime.MaxValue;


            //DataTable dtPrintList = printList.GetPrintrunDS(autIntNo, cType, statusLoaded, showAll, dtFrom, dtTo).Tables[0];
            // 2014-08-14, Oscar changed for adding paging features
            int total;
            var dtPrintList = printList.GetPrintrunDS(autIntNo, cType, statusLoaded, showAll, dtFrom, dtTo, out total, this.dgPrintrun.PageSize, this.dgPrintrun.CurrentPageIndex + 1).Tables[0];
            dgPrintrun.VirtualItemCount = total;

            dgPrintrun.DataSource = dtPrintList;
            dgPrintrun.DataKeyField = "PrintFile";

            try
            {
                dgPrintrun.DataBind();
            }
            catch
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.DataBind();
            }

            if (dgPrintrun.Items.Count == 0)
            {
                dgPrintrun.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
            {
                dgPrintrun.Visible = true;
                lblError.Visible = false;
            }
            pnlUpdate.Visible = false;
            pnlCancel.Visible = false;
            
        }

        protected void dgPrintrun_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            dgPrintrun.SelectedIndex = e.Item.ItemIndex;

            if (dgPrintrun.SelectedIndex > -1)
            {
                string printFileName = dgPrintrun.DataKeys[dgPrintrun.SelectedIndex].ToString();

                if (e.CommandName == "Select")
                {
                    //2014-01-22 Heidi added for fixed when click "update print status","update print status" buttons's Enable attribute in the gridview are not correct.(bontq1669)
                    BindGrid(this.autIntNo, cType);

                    if (printFileName.ToLower().StartsWith("expired"))
                    {
                        pnlCancel.Visible = true;
                        lblExpFile.Text = printFileName;
                    }
                    else
                    {
                        pnlUpdate.Visible = true;
                        lblPrintFile.Text = printFileName;
                    }
                    lblError.Visible = true;
                }
                else if (e.CommandName == "PrintNotices")
                {
                    string mCommandName = e.CommandName;
                    Helper_Web.BuildPopup(this, "FirstNoticeViewer.aspx", "PrintFile", printFileName, "printType", "PrintFirstNotice");
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintFirstNotice, PunchAction.Change);  

                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            string printFileName = txtPrint.Text.Trim();
            Label lblErr_SinglePrint = FindControl("TicketNumberSearch1").FindControl("lblError") as Label;
            if (printFileName.Equals(""))
            {
                lblErr_SinglePrint.Text = (string)GetLocalResourceObject("lblError.Text2");
                lblErr_SinglePrint.Visible = true;
                //return;
            }
            else
            {
                NoticeDB notDB = new NoticeDB(this.connectionString);
                int notIntNo = notDB.GetNoticeByNumber(this.autIntNo, printFileName);
                NoticeDetails notDet = notDB.GetNoticeDetails(notIntNo);
                if (isIBMPrinter && notDet.NotFilmType == "H")
                {
                    lblErr_SinglePrint.Text = (string)GetLocalResourceObject("errorMsg_PrintSingle");
                    lblErr_SinglePrint.Visible = true;
                }
                else
                {
                    lblErr_SinglePrint.Visible = false;
                    // Add our JavaScript to the currently rendered page
                    Helper_Web.BuildPopup(this, "FirstNoticeViewer.aspx", "PrintFile", printFileName, "printType", "PrintFirstNotice");
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintFirstNotice, PunchAction.Change);  
                }
            }
            BindGrid(this.autIntNo, cType);
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            Session["printAutIntNo"] = autIntNo;
            //2014-11-11 Heidi added for fixing when click"ddlSelectLA",page can't back to first page(bontq1669)
            if (dgPrintrun != null)
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.VirtualItemCount = 0;
            }

            BindGrid(this.autIntNo, cType);
            this.TicketNumberSearch1.AutIntNo = this.autIntNo;

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "6209";
            arDetails.LastUser = this.login;
            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            isIBMPrinter = ar.SetDefaultAuthRule().Value.Equals("Y");
        }

        protected void btnUpdateStatus_Click(object sender, EventArgs e)
        {
            SetChargeStatus(this.lblPrintFile.Text);
        }

        private void SetChargeStatus(string printFileName)
        {
            string updateType = "FirstNotice";

            //we have to be able to handle ASD_NAG files via this route.
            //if (printFileName.Substring(0, 3).Equals("NAG", StringComparison.InvariantCultureIgnoreCase)
            if (printFileName.ToUpper().IndexOf("NAG", StringComparison.InvariantCultureIgnoreCase) > -1)
            {
                this.statusPrinted = NAG_PRINTED_STATUS;
                this.statusLoaded = NAG_STATUS;
                updateType = "NoAOG";
            }
            else if (printFileName.ToUpper().Substring(0, 3).Equals("EXP", StringComparison.InvariantCultureIgnoreCase))
            {
                this.statusPrinted = EXPIRED_STATUS;
                updateType = "Expired";
            }

            string errMessage = string.Empty;
            int noOfRows = 0;

            using (TransactionScope scope = new TransactionScope())
            {
                NoticeDB notice = new NoticeDB(connectionString);
                noOfRows = notice.UpdateNoticeChargeStatus(this.autIntNo, printFileName, this.statusLoaded, this.statusPrinted, updateType, login, ref errMessage);

                if (noOfRows > 0 && updateType != "Expired")
                {
                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = autIntNo;
                    arDetails.ARCode = "4200";
                    arDetails.LastUser = login;
                    DefaultAuthRules ar = new DefaultAuthRules(arDetails, connectionString);
                    bool isPostDate = ar.SetDefaultAuthRule().Value.Equals("Y");

                    if (isPostDate)
                    {
                        PostDateNotice postDate = new PostDateNotice(connectionString);
                        noOfRows = postDate.SetNoticePostedDate(printFileName, autIntNo, DateTime.Now.AddDays(arDetails.ARNumeric), login, ref errMessage);
                    }
                }

                //if (noOfRows > 0)
                // 2013-10-21, Oscar changed, we have to commit when "noOfRows = 0"
                // 2013-10-21, Oscar added -999 checking for type = 'FirstNotice' Or 'NoAOG'
                if (noOfRows >= 0 || noOfRows == -999)
                {
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintFirstNotice, PunchAction.Change); 
                    scope.Complete();
                }
            }

            this.lblError.Visible = true;

            if (noOfRows < 1)
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text3"), noOfRows.ToString());
            else
                this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text4"), noOfRows.ToString(), updateType);

            this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            this.Session["printAutIntNo"] = autIntNo;
            this.BindGrid(autIntNo, cType);

        }

        protected void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            this.lblDateFrom.Visible = chkShowAll.Checked;
            this.lblDateTo.Visible = chkShowAll.Checked;
            this.dateFrom.Visible = chkShowAll.Checked;
            this.dateTo.Visible = chkShowAll.Checked;

            if (chkShowAll.Checked)
            {
                Session["showAllNotices"] = "Y";
                this.dateFrom.Text = DateTime.Now.AddMonths(-1).ToString(DATE_FORMAT);
                this.dateTo.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
            }
            else
            {
                Session["showAllNotices"] = "N";
            }

            // 2014-08-08, Oscar removed automatic load data, we need to allow user to select date range first.
            //2014-11-11 Heidi added for fixing when click"chkShowAll",page can't back to first page(bontq1669)
            if (dgPrintrun != null)
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.VirtualItemCount = 0;
            }
            BindGrid(autIntNo, cType);
           
        }

        protected void dgPrintrun_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgPrintrun.CurrentPageIndex = e.NewPageIndex;

            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, cType);
        }

        protected void dgPrintrun_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.DataItem == null)
                return;

            //Label lbl = (Label)e.Item.FindControl("lblType");
            var lbl = (TextBox)e.Item.FindControl("lblType");
            HyperLink hlPrint = (HyperLink)e.Item.FindControl("hlPrint");
            HyperLink hlList = (HyperLink)e.Item.FindControl("hlList");
            LinkButton btnUpdate = (LinkButton)e.Item.FindControl("btnUpdate");

            DateTime printDate = DateTime.MinValue;

            DataRowView row = (DataRowView)e.Item.DataItem;

            //dls 090311 - there is a problem here when the PrintFile is NULL - how did it get to be NULL????
            //string printFileName = (string)row["PrintFile"];
            string printFileName = string.Empty;
            
            if (row["PrintFile"] != System.DBNull.Value)
                printFileName = (string)row["PrintFile"];

            //dls 081110 - this was erroring when the print file name was blank????
            //string prefix = printFileName.Substring(0, 3).ToUpper();
            string prefix = string.Empty;
            if (printFileName.Trim().Length < 3)
            {
                lbl.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), prefix);
            }
            else
            {
                prefix = printFileName.Substring(0, 3).ToUpper();

                switch (prefix)
                {
                    case "SPD":
                        lbl.Text =(string)GetLocalResourceObject("SPD_DESCR");
                        hlPrint.Enabled = true;
                        hlList.Enabled = false;
                        btnUpdate.Text = (string)GetLocalResourceObject("btnUpdate.Text");
                        break;
                    case "ASD":
                        lbl.Text = (string)GetLocalResourceObject("ASD_DESCR");
                        hlPrint.Enabled = true;
                        hlList.Enabled = false;
                        btnUpdate.Text = (string)GetLocalResourceObject("btnUpdate.Text");
                        break;
                    case "RLV":
                        lbl.Text =(string)GetLocalResourceObject("RLV_DESCR");
                        hlPrint.Enabled = true;
                        hlList.Enabled = false;
                        btnUpdate.Text = (string)GetLocalResourceObject("btnUpdate.Text");
                        break;
                    case "NAG":
                        lbl.Text = (string)GetLocalResourceObject("NAG_DESCR");
                        hlPrint.Enabled = true;
                        hlList.Enabled = false;
                        btnUpdate.Text = (string)GetLocalResourceObject("btnUpdate.Text");
                        break;
                    case "EXP":
                        lbl.Text = (string)GetLocalResourceObject("EXP_DESCR");
                        hlPrint.Enabled = false;
                        hlList.Enabled = true;
                        btnUpdate.Text = (string)GetLocalResourceObject("btnUpdate.Text1");
                        break;
                    case "BUS":
                        lbl.Text = (string)GetLocalResourceObject("BUS_DESCR");
                        hlPrint.Enabled = true;
                        hlList.Enabled = false;
                        btnUpdate.Text = (string)GetLocalResourceObject("btnUpdate.Text");
                        break;
                    default:
                        lbl.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), prefix);
                        btnUpdate.Enabled = false;
                        break;
                }
            }

            if (row["NotPrint1stNoticeDate"] != null)
            {
                //dls 0901119 - added to disable update button if the file has not been printed
                if (!DateTime.TryParse(row["NotPrint1stNoticeDate"].ToString(), out printDate))
                    btnUpdate.Enabled = false;
                else
                    btnUpdate.Enabled = true;
            }
            else
                btnUpdate.Enabled = false;

        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtPrint.Text = this.TicketNumberSearch1.TicketNumber;
            //PunchStats805806 enquiry PrintFirstNotice
            //this.BindGrid(true);
            //this.btnRepresentationReport.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            SetChargeStatus(this.lblExpFile.Text);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
           //2014-11-11 Heidi added for fixing when click"btnRefresh",page can't back to first page(bontq1669)
            if (dgPrintrun != null)
            {
                dgPrintrun.CurrentPageIndex = 0;
                dgPrintrun.VirtualItemCount = 0;
            }
            BindGrid(autIntNo, cType);
        }
}
}
