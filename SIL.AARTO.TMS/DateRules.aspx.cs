using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class DateRules : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected int userAccessLevel;
        //protected string thisPage = "Date Rules Maintenance";
        protected string thisPageURL = "DateRules.aspx";
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;

			//int 
			int autIntNo = Convert.ToInt32(Session["autIntNo"]);
			
			Session["userLoginName"] = userDetails.UserLoginName;
			userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
            if (userAccessLevel < 10)
                btnOptAdd.Visible = false;
            else
                btnOptAdd.Visible = true;

			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddDateRule.Visible = false;
				pnlEditDateRule.Visible = false;
				pnlCopy.Visible = false;
				btnOptHide.Visible = true;

				PopulateAuthorites(ddlSelectLA,  autIntNo);

				if (autIntNo>0)
				{
					BindGrid(autIntNo,txtSearch.Text);
				}
				else
				{
                    //Modefied By Linda 2012-2-28
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
					lblError.Visible = true;
				}
			}		
		}


        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
		protected void PopulateAuthorites(DropDownList ddlAuthority,  int autIntNo)
		{
            //Jerry 2013-10-16 need clear the original DDL firstly, or there has repeat items
            if (ddlAuthority.Items.Count > 0)
                ddlAuthority.Items.Clear();

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");


            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlAuthority.DataSource = reader;
            //ddlAuthority.DataSource = data;
            //ddlAuthority.DataValueField = "AutIntNo";
            //ddlAuthority.DataTextField = "AutName";
            //ddlAuthority.DataBind();
			ddlAuthority.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

			//reader.Close();
		}

		protected void BindGrid(int autIntNo,string searchString)
		{
			// Obtain and bind a list of all users
			Stalberg.TMS.DateRulesDB dateRuleList = new Stalberg.TMS.DateRulesDB(connectionString);
            int totalCount = 0;
			DataSet data = dateRuleList.GetDateRulesListDS(autIntNo, dgDateRulePager.PageSize, dgDateRulePager.CurrentPageIndex, searchString, out totalCount);
			dgDateRule.DataSource = data;
            dgDateRule.DataKeyField = "DRID";
			dgDateRule.DataBind();
            dgDateRulePager.RecordCount = totalCount;

			if (dgDateRule.Items.Count == 0)
			{
				dgDateRule.Visible = false;
				lblError.Visible = true;
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
			}
			else
			{
				dgDateRule.Visible = true;
			}

			data.Dispose();

			pnlAddDateRule.Visible = false;
			pnlEditDateRule.Visible = false;
			pnlCopy.Visible = false;
		}


		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow date rules to be added to the database table
			pnlAddDateRule.Visible = true;
			pnlEditDateRule.Visible = false;

            //Jake 2013-09-12 added
            this.pnlCopy.Visible = false;

            //Get ddlSelectWFRFAName data
            TList<DateRule> dateRuleList = new DateRuleService().GetAll();
            ddlSelectDateRuleName.DataSource = dateRuleList;
            ddlSelectDateRuleName.DataTextField = "DRName";
            ddlSelectDateRuleName.DataValueField = "DRID";
            ddlSelectDateRuleName.DataBind();

			//txtAddDtRNoOfDays.Text = "0";
            int dRID = Convert.ToInt32(ddlSelectDateRuleName.SelectedValue);
            GetAddDateRuleForAuthorityByDRID(dRID);
		}

		protected void btnAddDateRule_Click(object sender, System.EventArgs e)
        {
            #region Jerry 2012-05-24 change
            //// add the new date rule to the database table
            //Stalberg.TMS.DateRulesDB dateRuleAdd = new DateRulesDB(connectionString);
			
            //int addDtRIntNo = dateRuleAdd.AddDateRules(Convert.ToInt32(ddlSelectLA.SelectedValue), txtAddDtRStartDate.Text,
            //    txtAddDtREndDate.Text, Convert.ToInt32(txtAddDtRNoOfDays.Text), 
            //    txtAddDtRDescr.Text, loginUser);
            
            //List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            //SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            //rUMethod.UpdateIntoLookup(addDtRIntNo, lgEntityList, "DateRulesLookup", "DtRIntNo", "DtRDescr");

            //if (addDtRIntNo <= 0)
            //{
            //    //Modefied By Linda 2012-2-28
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            //}
            //else
            //{
            //    //Modefied By Linda 2012-2-28
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            //}
            #endregion

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int dRID = Convert.ToInt32(ddlSelectDateRuleName.SelectedValue);

            AuthorityDateRuleService authorityDateRuleService = new AuthorityDateRuleService();
            AuthorityDateRule authorityDateRule = authorityDateRuleService.GetByDridAutIntNo(dRID, autIntNo);
            if (authorityDateRule == null)
            {
                //add AuthorityDateRule
                authorityDateRule = new AuthorityDateRule();
                authorityDateRule.AutIntNo = autIntNo;
                authorityDateRule.Drid = dRID;
                authorityDateRule.AdrNoOfDays = txtAddDtRNoOfDays.Text.Trim() == "" ? 0 : Convert.ToInt32(txtAddDtRNoOfDays.Text.Trim());
                authorityDateRule.LastUser = this.loginUser;
                authorityDateRuleService.Save(authorityDateRule);

                //update Multilanguage
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(dRID, lgEntityList, "DateRuleLookup", "DRID", "DtRDescr", this.loginUser);
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");

                // jake 2013-09-12 comment out to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                //dgDateRulePager.CurrentPageIndex = 1;
                //dgDateRulePager.RecordCount = 0;

                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.DateRulesMaintenance, PunchAction.Add);

                BindGrid(autIntNo, txtSearch.Text);
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            }

            lblError.Visible = true;
		}

		protected void btnUpdateDateRule_Click(object sender, System.EventArgs e)
		{
            #region Jerry 2012-05-24 change
            //Stalberg.TMS.DateRulesDB dateRuleUpdate = new DateRulesDB(connectionString);
			
            //int updDateRuleIntNo = dateRuleUpdate.UpdateDateRules(Convert.ToInt32(ddlSelectLA.SelectedValue),
            //    txtDtRStartDate.Text, txtDtREndDate.Text, Convert.ToInt32(txtDtRNoOfDays.Text), 
            //    txtDtRDescr.Text, loginUser, Convert.ToInt32(Session["editDtRIntNo"]));

            //if (updDateRuleIntNo.Equals("-1"))
            //{
            //    //Modefied By Linda 2012-2-28
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            //}
            //else
            //{
            //    List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            //    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            //    rUMethod.UpdateIntoLookup(updDateRuleIntNo, lgEntityList, "DateRulesLookup", "DtRIntNo", "DtRDescr");
            //    //Modefied By Linda 2012-2-28
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
            //}
            #endregion

            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            int dRID = Convert.ToInt32(dgDateRule.DataKeys[dgDateRule.SelectedIndex]);

            //update AuthorityDateRule
            AuthorityDateRuleService authorityDateRuleService = new AuthorityDateRuleService();
            AuthorityDateRule authorityDateRule = authorityDateRuleService.GetByDridAutIntNo(dRID, autIntNo);
            authorityDateRule.AdrNoOfDays = txtDtRNoOfDays.Text.Trim() == "" ? 0 : Convert.ToInt32(txtDtRNoOfDays.Text.Trim());
            authorityDateRule.LastUser = this.loginUser;
            authorityDateRuleService.Save(authorityDateRule);

            //update Multilanguage
            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(dRID, lgEntityList, "DateRuleLookup", "DRID", "DtRDescr", this.loginUser);
            //Modefied By Linda 2012-2-28
            //Display multi - language error message is the value from the resource
            lblError.Text = (string)GetLocalResourceObject("lblError.Text5");

            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.DateRulesMaintenance, PunchAction.Change);  

            lblError.Visible = true;
            // jake 2013-09-12 comment out to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            //dgDateRulePager.CurrentPageIndex = 1;
            //dgDateRulePager.RecordCount = 0;
            BindGrid(autIntNo, txtSearch.Text);
		}

		protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			dgDateRule.Visible = true;
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            dgDateRulePager.CurrentPageIndex = 1;
            dgDateRulePager.RecordCount = 0;
            BindGrid(Convert.ToInt32(ddlSelectLA.SelectedValue), txtSearch.Text);
		}

        protected void PopulateDateRuleTextBoxes(int dRID)
		{
            //Jerry 2012-05-24 change
			// Obtain and bind a list of all users
            //Stalberg.TMS.DateRulesDB dateRule = new Stalberg.TMS.DateRulesDB(connectionString);
            //Stalberg.TMS.DateRulesDetails dateRuleDetails = dateRule.GetDateRulesDetails(dtrIntNo);
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            AuthorityDateRule authorityDateRule = new AuthorityDateRuleService().GetByDridAutIntNo(dRID, autIntNo);
            DateRule dateRule = new DateRuleService().GetByDrid(dRID);
            string[] dateRuleName = dateRule.DrName.Split('_');

            if (dRID > 0 && dateRuleName.Length == 3)
			{
                
                txtDtRStartDate.Text = dateRuleName[1];
                txtDtREndDate.Text = dateRuleName[2];
                txtDtRNoOfDays.Text = authorityDateRule.AdrNoOfDays.ToString();
                txtDtRDescr.Text = dateRule.DtRdescr;

				pnlEditDateRule.Visible = true;

				if (userAccessLevel<10)
				{
					//make text boxes read-only
					txtDtRStartDate.ReadOnly = true;
					txtDtREndDate.ReadOnly = true;
					txtDtRDescr.ReadOnly = true;
				}
				else
				{
					txtDtRStartDate.ReadOnly = false;
					txtDtREndDate.ReadOnly = false;
					txtDtRDescr.ReadOnly = false;
				}
			}
			else
			{
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
				lblError.Visible = true;
				pnlEditDateRule.Visible = false;
			}
		}

		protected void dgDateRule_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			dgDateRule.SelectedIndex = e.Item.ItemIndex;

			if (dgDateRule.SelectedIndex > -1) 
			{			
				pnlAddDateRule.Visible = false;
                //Jake 2013-09-12 added 
                this.pnlCopy.Visible = false;

                int dRID = Convert.ToInt32(dgDateRule.DataKeys[dgDateRule.SelectedIndex]);
                
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(dRID.ToString(), "DateRuleLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                PopulateDateRuleTextBoxes(dRID);
			}
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (dgDateRule.Visible.Equals(true))
			{
				//hide it
				dgDateRule.Visible = false;
                // Jake 2013-09-12 added ,if grid hiddened we need to hidden pager control too
                dgDateRulePager.Visible = false;
				btnOptHide.Text =(string)GetLocalResourceObject("btnOptHide.Text1"); 
			}
			else
			{
				//show it
				dgDateRule.Visible = true;
                dgDateRulePager.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		protected void btnOptCopy_Click(object sender, System.EventArgs e)
		{
			pnlAddDateRule.Visible = false;
			pnlEditDateRule.Visible = false;
			pnlCopy.Visible = true;

			//int 
			int autIntNo = Convert.ToInt32(Session["autIntNo"]);

			PopulateAuthorites(ddlAutFrom, autIntNo);
			PopulateAuthorites(ddlAutTo,  autIntNo);

		}

		protected void btnCopyDateRules_Click(object sender, System.EventArgs e)
		{
			int fromAutIntNo = Convert.ToInt32(ddlAutFrom.SelectedValue);
			int toAutIntNo = Convert.ToInt32(ddlAutTo.SelectedValue);

			if (fromAutIntNo == toAutIntNo)
			{
                //Modefied By Linda 2012-2-28
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
				return;
            }

            #region Jerry 2012-05-24
            //DateRulesDB dateRule = new DateRulesDB(connectionString);

            //int copyAutIntNo = dateRule.CopyDateRules(fromAutIntNo, autIntNo, loginUser);

            //if (copyAutIntNo == -1)
            //    //Modefied By Linda 2012-2-28
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            //else if (copyAutIntNo == -2)
            //    //Modefied By Linda 2012-2-28
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            //else if (copyAutIntNo == 0)
            //    //Modefied By Linda 2012-2-28
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            //else
            //    //Modefied By Linda 2012-2-28
            //    //Display multi - language error message is the value from the resource
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            #endregion

            //Jerry 2013-10-16 change the logic, allow the user to copy all date rules that do not exist for the �to?authority
            AuthorityDateRuleService authorityDateRuleService = new AuthorityDateRuleService();
            //TList<AuthorityDateRule> toAuthorityDateRuleList = authorityDateRuleService.GetByAutIntNo(toAutIntNo);
            //if (toAuthorityDateRuleList.Count > 0)
            //{
            //    lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            //    return;
            //}

            TList<AuthorityDateRule> fromAuthorityDateRuleList = authorityDateRuleService.GetByAutIntNo(fromAutIntNo);
            if (fromAuthorityDateRuleList.Count == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                return;
            }

            #region Jerry 2013-10-16 change the logic, allow the user to copy all date rules that do not exist for the �to?authority
            //Copy
            //foreach (AuthorityDateRule authorityDateRule in fromAuthorityDateRuleList)
            //{
            //    AuthorityDateRule newAuthorityDateRule = new AuthorityDateRule();
            //    newAuthorityDateRule.AutIntNo = toAutIntNo;
            //    newAuthorityDateRule.Drid = authorityDateRule.Drid;
            //    newAuthorityDateRule.AdrNoOfDays = authorityDateRule.AdrNoOfDays;
            //    newAuthorityDateRule.LastUser = this.loginUser;

            //    authorityDateRuleService.Save(newAuthorityDateRule);
            //}
            #endregion
            
            //Copy
            foreach (AuthorityDateRule fromAuthorityDateRule in fromAuthorityDateRuleList)
            {
                AuthorityDateRule toAuthorityDateRule = authorityDateRuleService.GetByDridAutIntNo(fromAuthorityDateRule.Drid, toAutIntNo);
                if(toAuthorityDateRule == null)
                {
                    toAuthorityDateRule = new AuthorityDateRule();
                    toAuthorityDateRule.AutIntNo = toAutIntNo;
                    toAuthorityDateRule.Drid = fromAuthorityDateRule.Drid;
                    toAuthorityDateRule.AdrNoOfDays = fromAuthorityDateRule.AdrNoOfDays;
                    toAuthorityDateRule.LastUser = this.loginUser;

                    authorityDateRuleService.Save(toAuthorityDateRule);
                }
            }
            lblError.Text = (string)GetLocalResourceObject("lblError.Text11");

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(toAutIntNo, this.loginUser, PunchStatisticsTranTypeList.DateRulesMaintenance, PunchAction.Add);  

            //Iris 20140310 changed for showing the selected Authority list in current page when complete 'copy'.
            //toAutIntNo = Convert.ToInt32(Session["autIntNo"]);
            toAutIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            // jake 2013-09-12 comment out to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            //dgDateRulePager.CurrentPageIndex = 1;
            //dgDateRulePager.RecordCount = 0;
			BindGrid(toAutIntNo,txtSearch.Text);
		}

        protected void ddlSelectDateRuleName_SelectedIndexChanged(object sender, EventArgs e)
        {
            int dRID = Convert.ToInt32(ddlSelectDateRuleName.SelectedValue);
            GetAddDateRuleForAuthorityByDRID(dRID);
        }

        private void GetAddDateRuleForAuthorityByDRID(int dRID)
        {
            if (dRID > 0)
            {
                DateRule dateRule = new DateRuleService().GetByDrid(dRID);
                string[] dateRuleName = dateRule.DrName.Split('_');
                if (dateRuleName.Length == 3)
                {
                    txtAddDtRStartDate.Text = dateRuleName[1];
                    txtAddDtREndDate.Text = dateRuleName[2];
                    txtAddDtRDescr.Text = dateRule.DtRdescr;
                    txtAddDtRNoOfDays.Text = "0";
                }

                //Bind Multi language
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(dRID.ToString(), "DateRuleLookup");
                this.ucLanguageLookupAdd.DataBind(entityList);

            }
        }

        protected void dgDateRulePager_PageChanged(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo,txtSearch.Text);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, txtSearch.Text);
        }

	}
}
