using System;
using System.Text;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Stalberg.TMS
{
    // dls 070330 - CashType functionality (GetCashTypeChar) of this class has been moved to CashReceiptDB - it caused a problem with the console apps to have all this stuff here!
    // dls 070330 - Persona functionality (PersonaFromDriver) of this class has been moved to MI5DB - it caused a problem with the console apps to have all this stuff here!
    /// <summary>
    /// Contains an array of Helper methods for the Web.
    /// </summary>
    public static partial class Helper_Web
    {
		/// <summary>
		/// Determines whether the supplied Web encoded string is considered to be empty.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// 	<c>true</c> if the string is considered to be empty; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsWebStringEmpty(string value)
		{
			if (value == null)
				return true;

			value = value.Trim();

			if (value.Length == 0)
				return true;

			if (value.Equals("&nbsp;", StringComparison.CurrentCultureIgnoreCase))
				return true;

			return false;
		}

        /// <summary>
        /// Builds the popup redirect string.
        /// </summary>
        /// <param name="page">The page that the script will be written to.</param>
        /// <param name="pageName">Name of the page top open in the popup.</param>
        /// <param name="qsParam">The QueryString parameter(s) for the new page (in key-value pairs).</param>
        public static void BuildPopup(Page page, string pageName, params string[] qsParam)
        {
            //string pageOpener = page.GetType().Name;
            //int pos = pageOpener.LastIndexOf('_');
            //pageOpener = string.Format("{0}.aspx", pageOpener.Substring(0, pos));

            Helper_Web.BuildPopup(string.Empty, page, pageName, false, qsParam);
        }

        /// <summary>
        /// Outputs the byte array as the page contents.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="buffer">The buffer to output.</param>
        /// <param name="type">The type of page output.</param>
        public static void OutputByteArray(Page page, byte[] buffer, BufferType type)
        {
            try
            {
                page.Response.ClearContent();
                page.Response.ClearHeaders();

                string fileName = page.ToString().Replace(' ', '_');
                switch (type)
                {
                    case BufferType.Excel:
                        fileName += ".xls";
                        page.Response.ContentType = "application/ms-excel";
                        break;
                    default:
                        fileName += ".pdf";
                        page.Response.ContentType = "application/pdf";
                        break;
                }
                page.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                page.Response.AppendHeader("Content-Length", buffer.Length.ToString());

                page.Response.BinaryWrite(buffer);
                page.Response.End();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Builds the popup redirect string.
        /// </summary>
        /// <param name="page">The page that the script will be written to.</param>
        /// <param name="pageName">Name of the page top open in the popup.</param>
        /// <param name="overrideAjax">if set to <c>true</c> the the test for Ajax is bypassed.</param>
        /// <param name="qsParam">The QueryString parameter(s) for the new page (in key-value pairs).</param>
        public static void BuildPopup(Page page, string pageName, bool overrideAjax, params string[] qsParam)
        {
            string pageOpener = page.GetType().Name;
            int pos = pageOpener.LastIndexOf('_');
            pageOpener = string.Format("{0}.aspx", pageOpener.Substring(0, pos));

            Helper_Web.BuildPopup(pageOpener, page, pageName, overrideAjax, qsParam);
        }

        /// <summary>
        /// Builds the popup redirect string.
        /// </summary>
        /// <param name="redirectName">Name of the page to redirect the current page to.</param>
        /// <param name="page">The page that the script will be written to.</param>
        /// <param name="pageName">Name of the page top open in the popup.</param>
        /// <param name="qsParam">The QueryString parameter(s) for the new page (in key-value pairs).</param>
        public static void BuildPopup(string redirectName, Page page, string pageName, params string[] qsParam)
        {
            Helper_Web.BuildPopup(redirectName, page, pageName, false, qsParam);
        }

        /// <summary>
        /// Builds the popup redirect string.
        /// </summary>
        /// <param name="redirectName">Name of the page to redirect the current page to.</param>
        /// <param name="page">The page that the script will be written to.</param>
        /// <param name="pageName">Name of the page top open in the popup.</param>
        /// <param name="overrideAjax">if set to <c>true</c> the the test for Ajax is bypassed.</param>
        /// <param name="qsParam">The QueryString parameter(s) for the new page (in key-value pairs).</param>
        public static void BuildPopup(string redirectName, Page page, string pageName, bool overrideAjax, params string[] qsParam)
        {
            if (qsParam == null)
                qsParam = new string[0];

            List<QSParams> parameters = new List<QSParams>();
            int length = qsParam.Length;

            QSParams param = null;
            for (int i = 0; i < length; i += 2)
            {
                if ((i + 1) < length)
                    param = new QSParams(qsParam[i], qsParam[i + 1]);
                else
                    param = new QSParams(qsParam[i]);
                parameters.Add(param);
            }

            Helper_Web.BuildPopup(new PageToOpen[] { new PageToOpen(page, pageName, overrideAjax, redirectName, parameters) });
        }

        /// <summary>
        /// Builds the popup redirect string.
        /// </summary>
        /// <param name="pages">The page to open.</param>
        public static void BuildPopup(PageToOpen page)
        {
            if (page == null)
                return;

            Helper_Web.BuildPopup(new PageToOpen[] { page });
        }

        /// <summary>
        /// Builds the popup redirect string.
        /// </summary>
        /// <param name="pages">The pages.</param>
        public static void BuildPopup(PageToOpen[] pages)
        {
            if (pages == null || pages.Length == 0)
                return;

            StringBuilder sb = new StringBuilder();
            Page outputPage = pages[0].ThisPage;

            // Refresh the current window
            if (pages[0].RedirectPage.Length > 0)
            {
                sb.Append("window.location = '");
                sb.Append(pages[0].RedirectPage);
                sb.Append("';");
            }

            // Create a new window for all the pages to open
            int id = 0;
            foreach (PageToOpen page in pages)
            {
                id++;

                sb.Append("var id");
                sb.Append(id);
                sb.Append(" = window.open('");
                sb.Append(page.ToString());
                sb.Append("'); ");

                sb.Append("id");
                sb.Append(id);
                sb.Append(".focus(); ");
            }

            // Register the script as a startup script on the page
            ScriptManager.RegisterStartupScript(
                outputPage, outputPage.GetType(), "OPUS_WindowOpener", sb.ToString(), true);
        }

        /// <summary>
        /// Adds the page blocker script and handler for a button.
        /// </summary>
        /// <param name="button">The button.</param>
        public static void AddPageBlocker(Button button)
        {
            ClientScriptManager csm = button.Page.ClientScript;
            Type pageType = button.Page.GetType();
            string functionName = string.Format("{0}_StalbergAddPageBlocker", button.ID);

            string eventName = "OnMouseUp";
            if (button.Attributes[eventName] == null)
                button.Attributes.Add(eventName, functionName + "();");

            if (!csm.IsClientScriptBlockRegistered(pageType, functionName))
            {
                string divName = string.Format("{0}_BlockForm", button.ID);

                StringBuilder sb = new StringBuilder();
                sb.Append("<script language='javascript' type='text/javascript'>\n\n");
                sb.Append(string.Format("\t document.write('<div id=\"{0}\" ", divName));
                sb.Append("style=\"position:absolute;background-color:white ;top:0px;left:0px;width:100%;height:100%;visibility:hidden; border:solid 0px white; float:left; filter:alpha(opacity=50); -moz-opacity:.50; opacity:.50;\"></div>');\n");
                sb.Append("\n\t function ");
                sb.Append(functionName);
                sb.Append("() {\n");
                //sb.Append(string.Format("\t\talert('Hello '+{0});\n", button.ID));
                sb.Append(string.Format("\t\t document.getElementById (\"{0}\").style.visibility = \"visible\";\n", divName));
                sb.Append(string.Format("\t\t document.getElementById (\"{0}\").style.zIndex= 1;\n", divName));
                sb.Append("\t}\n");
                sb.Append("</script>\n");

                csm.RegisterClientScriptBlock(pageType, functionName, sb.ToString(), false);
            }
        }

    }

    /// <summary>
    /// Represents a page to be opened by the BuildPopup method
    /// </summary>
    public class PageToOpen
    {
        // Fields
        private Page page;
        private bool overrideAjax;
        private string redirectPage;
        private string destinationPage;
        private List<QSParams> parameters;

        /// <summary>
        /// Initializes a new instance of the <see cref="PageToOpen"/> class.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="destinationPage">The destination page.</param>
        /// <param name="overrideAjax">if set to <c>true</c> [override ajax].</param>
        /// <param name="redirectPage">The redirect page.</param>
        /// <param name="parameters">The parameters.</param>
        public PageToOpen(Page page, string destinationPage, bool overrideAjax, string redirectPage, List<QSParams> parameters)
        {
            this.page = page;
            //if (redirectPage.Length == 0)
            //{
            //    string pageOpener = page.GetType().Name;
            //    int pos = pageOpener.LastIndexOf('_');
            //    this.redirectPage = string.Format("{0}.aspx", pageOpener.Substring(0, pos));
            //}
            //else
            this.redirectPage = redirectPage;
            this.destinationPage = destinationPage;
            this.overrideAjax = overrideAjax;
            this.parameters = parameters;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageToOpen"/> class.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="destinationPage">The destination page.</param>
        /// <param name="overrideAjax">if set to <c>true</c> [override ajax].</param>
        /// <param name="parameters">The parameters.</param>
        public PageToOpen(Page page, string destinationPage, bool overrideAjax, List<QSParams> parameters)
            : this(page, destinationPage, overrideAjax, string.Empty, parameters)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageToOpen"/> class.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="destinationPage">The destination page.</param>
        /// <param name="redirectPage">The redirect page.</param>
        /// <param name="parameters">The parameters.</param>
        public PageToOpen(Page page, string destinationPage, string redirectPage, List<QSParams> parameters)
            : this(page, destinationPage, false, redirectPage, parameters)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageToOpen"/> class.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="parameters">The parameters.</param>
        public PageToOpen(Page page, string destinationPage, List<QSParams> parameters)
            : this(page, destinationPage, false, string.Empty, parameters)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageToOpen"/> class.
        /// </summary>
        /// <param name="page">The page.</param>
        public PageToOpen(Page page, string destinationPage)
            : this(page, destinationPage, false, string.Empty, new List<QSParams>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PageToOpen"/> class.
        /// </summary>
        /// <param name="page">The page.</param>
        public PageToOpen(Page page, string destinationPage, string redirectPage)
            : this(page, destinationPage, false, redirectPage, new List<QSParams>( ))
        {
        }

        /// <summary>
        /// Gets the destination page.
        /// </summary>
        /// <value>The destination page.</value>
        public string DestinationPage
        {
            get { return this.destinationPage; }
        }

        /// <summary>
        /// Gets or sets the redirect page.
        /// </summary>
        /// <value>The redirect page.</value>
        public string RedirectPage
        {
            get { return this.redirectPage; }
            set { this.redirectPage = value; }
        }

        /// <summary>
        /// Gets the this page.
        /// </summary>
        /// <value>The this page.</value>
        public Page ThisPage
        {
            get { return this.page; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to override checking for the Ajax ScriptManager.
        /// </summary>
        /// <value><c>true</c> if [override ajax]; otherwise, <c>false</c>.</value>
        public bool OverrideAjax
        {
            get { return this.overrideAjax; }
            set { this.overrideAjax = value; }
        }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public List<QSParams> Parameters
        {
            get { return this.parameters; }
        }

		/// <summary>
		/// Adds a <see cref="QSParams"/> parameter based on the key value
		/// </summary>
		/// <param name="key">The QueryString key name.</param>
		/// <param name="value">The key's value.</param>
		/// <returns>
		/// The added <see cref="QSParams"/> object.
		/// </returns>
		public QSParams AddParameter(string key, object value)
		{
			QSParams qs = new QSParams(key, value.ToString());
			this.parameters.Add(qs);
			return qs;
		}

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.destinationPage);
            if (parameters.Count > 0)
            {
                sb.Append("?");
                foreach (QSParams parameter in parameters)
                {
                    sb.Append(parameter.ToString());
                    sb.Append('&');
                }

                sb.Length = sb.Length - 1;
            }

            return sb.ToString();
        }

    }

    /// <summary>
    /// Represents a QueryString parameter for use with a PageToSend
    /// </summary>
    public class QSParams
    {
        // Fields
        private string key;
        private string value;

        /// <summary>
        /// Initializes a new instance of the <see cref="QSParams"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        public QSParams(string key)
        {
            this.key = key;
            this.value = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QSParams"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public QSParams(string key, object value)
        {
            this.key = key;
            this.value = value.ToString();
        }

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>The key.</value>
        public string Key
        {
            get { return this.key; }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public string Value
        {
            get { return this.value; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0}={1}", this.key, this.value);
        }

    }

    /// <summary>
    /// Represents the types of binary data that can be output to a page
    /// </summary>
    public enum BufferType
    {
        /// <summary>
        /// The Adobe Portable Document Format
        /// </summary>
        PDF = 0,
        /// <summary>
        /// Microsoft Excel 2000 - 2003 format
        /// </summary>
        Excel = 1
    }

}
