﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;


namespace Stalberg.TMS
{
    public partial class ImageServiceUserDB
    {

        string mConstr = "";

        public ImageServiceUserDB(string vConstr)
        {
            mConstr = vConstr;
        }

        public int AddUser(string UserName, string userPassword, string LastUser)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ImageService_UserAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
 

            SqlParameter parameterUserSName = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
            parameterUserSName.Value = UserName;
            myCommand.Parameters.Add(parameterUserSName);

            SqlParameter parameterUserPassword = new SqlParameter("@UserPassword", SqlDbType.VarChar, 50);
            parameterUserPassword.Value = userPassword;
            myCommand.Parameters.Add(parameterUserPassword);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = LastUser;
            myCommand.Parameters.Add(parameterLastUser);

            SqlParameter parameterUserIntNo = new SqlParameter("@ISUIntNo", SqlDbType.Int, 4);
            parameterUserIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterUserIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int userId = Convert.ToInt32(parameterUserIntNo.Value);

                return userId;
            }
            catch
            {
                myConnection.Dispose();
                return 0;
            }
        }

        public DataSet SearchUserListDS(string searchText)
        {
            SqlDataAdapter sqlDAUsers = new SqlDataAdapter();
            DataSet dsUsers = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAUsers.SelectCommand = new SqlCommand();
            sqlDAUsers.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAUsers.SelectCommand.CommandText = "ImageService_SearchUserList";

            // Mark the Command as a SPROC
            sqlDAUsers.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter parameterSearchText = new SqlParameter("@SearchText", SqlDbType.VarChar, 20);
            parameterSearchText.Value = searchText;
            sqlDAUsers.SelectCommand.Parameters.Add(parameterSearchText);

            // Execute the command and close the connection
            sqlDAUsers.Fill(dsUsers);
            sqlDAUsers.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsUsers;
        }

        public int UpdateUser(int ISUIntNo, string UserName, string PassWord, string LastUser)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ImageService_UserUpdate", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterUserName = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
            parameterUserName.Value = UserName;
            myCommand.Parameters.Add(parameterUserName);

            //SqlParameter parameterPassWord = new SqlParameter("@PassWord", SqlDbType.VarChar, 50);
            //2014-01-20 Heidi fixed bug for in the code and stored procedure parameter name is not consistent (5103)
            SqlParameter parameterPassWord = new SqlParameter("@UserPassword", SqlDbType.VarChar, 50);
            parameterPassWord.Value = PassWord;
            myCommand.Parameters.Add(parameterPassWord);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = LastUser;
            myCommand.Parameters.Add(parameterLastUser);


            SqlParameter parameterUserIntNo = new SqlParameter("@ISUIntNo", SqlDbType.Int);
            parameterUserIntNo.Direction = ParameterDirection.InputOutput;
            parameterUserIntNo.Value = ISUIntNo;
            myCommand.Parameters.Add(parameterUserIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int UserIntNo = (int)myCommand.Parameters["@ISUIntNo"].Value;
                //int userId = (int)parameterUserIntNo.Value;

                return UserIntNo;
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return 0;
            }
        }

        public String DeleteUser(int userIntNo)
        {

            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("ImageService_UserDelete", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterUserIntNo = new SqlParameter("@ISUIntNo", SqlDbType.Int, 4);
            parameterUserIntNo.Value = userIntNo;
            parameterUserIntNo.Direction = ParameterDirection.InputOutput;
            myCommand.Parameters.Add(parameterUserIntNo);

            try
            {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int userId = (int)parameterUserIntNo.Value;

                return userId.ToString();
            }
            catch (Exception e)
            {
                myConnection.Dispose();
                string msg = e.Message;
                return String.Empty;
            }
        }
    }
}

