using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // MenuDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class MenuDetails 
	{
        public string MenuName;
		public string MenuComment;
		public string MenuAdminOnly;
    }

    //*******************************************************
    //
    // MenuDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Menus within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class MenuDB {

		string mConstr = string.Empty;

		public MenuDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // MenuDB.GetMenuDetails() Method <a name="GetMenuDetails"></a>
        //
        // The GetMenuDetails method returns a MenuDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public MenuDetails GetMenuDetails(int menuIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MenuDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int, 4);
            parameterMenuIntNo.Value = menuIntNo;
            myCommand.Parameters.Add(parameterMenuIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			MenuDetails myMenuDetails = new MenuDetails();

			while (result.Read())
			{
				// Populate Struct using Output Params from SPROC
				myMenuDetails.MenuName = result["MenuName"].ToString();
				myMenuDetails.MenuComment = result["MenuComment"].ToString();
				myMenuDetails.MenuAdminOnly = result["MenuAdminOnly"].ToString();
			}

			result.Close();
			myConnection.Dispose();

            return myMenuDetails;
        }

        //*******************************************************
        //
        // MenuDB.AddMenu() Method <a name="AddMenu"></a>
        //
        // The AddMenu method inserts a new menu record
        // into the menus database.  A unique "MenuId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-19 comment by Henry for useless
        //public int AddMenu (string menuName, string menuComment, string menuAdminOnly) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("MenuAdd", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterMenuName = new SqlParameter("@MenuName", SqlDbType.VarChar, 50);
        //    parameterMenuName.Value = menuName;
        //    myCommand.Parameters.Add(parameterMenuName);

        //    SqlParameter parameterMenuComment = new SqlParameter("@MenuComment", SqlDbType.VarChar, 255);
        //    parameterMenuComment.Value = menuComment;
        //    myCommand.Parameters.Add(parameterMenuComment);

        //    SqlParameter parameterMenuAdminOnly = new SqlParameter("@MenuAdminOnly", SqlDbType.Char, 1);
        //    parameterMenuAdminOnly.Value = menuAdminOnly;
        //    myCommand.Parameters.Add(parameterMenuAdminOnly);

        //    SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int, 4);
        //    parameterMenuIntNo.Direction = ParameterDirection.Output;
        //    myCommand.Parameters.Add(parameterMenuIntNo);

        //    try {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int menuIntNo = Convert.ToInt32(parameterMenuIntNo.Value);

        //        return menuIntNo;
        //    }
        //    catch {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}

		public SqlDataReader GetMenuList()
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("MenuList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetMenuListDS()
		{
			SqlDataAdapter sqlDAMenus = new SqlDataAdapter();
			DataSet dsMenus = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAMenus.SelectCommand = new SqlCommand();
			sqlDAMenus.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAMenus.SelectCommand.CommandText = "MenuList";

			// Mark the Command as a SPROC
			sqlDAMenus.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Execute the command and close the connection
			sqlDAMenus.Fill(dsMenus);
			sqlDAMenus.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsMenus;		
		}
        // 2013-07-19 comment by Henry for useless
        //public int UpdateMenu (int menuIntNo, string menuName, string menuComment, string menuAdminOnly) 
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("MenuUpdate", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC
        //    SqlParameter parameterMenuName = new SqlParameter("@MenuName", SqlDbType.VarChar, 50);
        //    parameterMenuName.Value = menuName;
        //    myCommand.Parameters.Add(parameterMenuName);

        //    SqlParameter parameterMenuComment = new SqlParameter("@MenuComment", SqlDbType.VarChar, 255);
        //    parameterMenuComment.Value = menuComment;
        //    myCommand.Parameters.Add(parameterMenuComment);

        //    SqlParameter parameterMenuAdminOnly = new SqlParameter("@MenuAdminOnly", SqlDbType.Char, 1);
        //    parameterMenuAdminOnly.Value = menuAdminOnly;
        //    myCommand.Parameters.Add(parameterMenuAdminOnly);

        //    SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int);
        //    parameterMenuIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterMenuIntNo.Value = menuIntNo;
        //    myCommand.Parameters.Add(parameterMenuIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int MenuIntNo = (int)myCommand.Parameters["@MenuIntNo"].Value;
        //        //int menuId = (int)parameterMenuIntNo.Value;

        //        return MenuIntNo;
        //    }
        //    catch 
        //    {
        //        myConnection.Dispose();
        //        return 0;
        //    }
        //}
        // 2013-07-19 comment by Henry for useless
        //public int CopyMenu (int menuIntNo)
        //{
        //    // Create Instance of Connection and Command Object
        //    SqlConnection myConnection = new SqlConnection(mConstr);
        //    SqlCommand myCommand = new SqlCommand("MenuCopy", myConnection);

        //    // Mark the Command as a SPROC
        //    myCommand.CommandType = CommandType.StoredProcedure;

        //    // Add Parameters to SPROC

        //    SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int);
        //    parameterMenuIntNo.Direction = ParameterDirection.InputOutput;
        //    parameterMenuIntNo.Value = menuIntNo;
        //    myCommand.Parameters.Add(parameterMenuIntNo);

        //    try 
        //    {
        //        myConnection.Open();
        //        myCommand.ExecuteNonQuery();
        //        myConnection.Dispose();

        //        // Calculate the CustomerID using Output Param from SPROC
        //        int MenuIntNo = (int)myCommand.Parameters["@MenuIntNo"].Value;

        //        return MenuIntNo;
        //    }
        //    catch (Exception e)
        //    {
        //        myConnection.Dispose();
        //        string msg = e.Message;
        //        return 0;
        //    }
        //}

		public String DeleteMenu (int menuIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("MenuDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int, 4);
			parameterMenuIntNo.Value = menuIntNo;
			parameterMenuIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterMenuIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int MenuIntNo = (int)parameterMenuIntNo.Value;

				return MenuIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

