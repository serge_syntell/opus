using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // MainMenuItemDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class MainMenuItemDetails 
	{
		public Int32 MenuIntNo;
        public string MMIName;
		public string MMIDescr;
		public string MMIurl;
		public int MMIOrder;
    }

    //*******************************************************
    //
    // MainMenuItemDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Menus within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class MainMenuItemDB {

		string mConstr = string.Empty;

		public MainMenuItemDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // MenuDB.GetMainMenuItemDetails() Method <a name="GetMainMenuItemDetails"></a>
        //
        // The GetMainMenuItemDetails method returns a MainMenuItemDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public MainMenuItemDetails GetMainMenuItemDetails(int mmiIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MainMenuItemDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMMIIntNo = new SqlParameter("@MMIIntNo", SqlDbType.Int, 4);
            parameterMMIIntNo.Value = mmiIntNo;
            myCommand.Parameters.Add(parameterMMIIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			MainMenuItemDetails myMainMenuItemDetails = new MainMenuItemDetails();

			while (result.Read())
				{
					// Populate Struct using Output Params from SPROC
					myMainMenuItemDetails.MenuIntNo = Convert.ToInt32(result["MenuIntNo"]);
					myMainMenuItemDetails.MMIName = result["MMIName"].ToString();
					myMainMenuItemDetails.MMIDescr = result["MMIDescr"].ToString();
					myMainMenuItemDetails.MMIurl = result["MMIurl"].ToString();
					myMainMenuItemDetails.MMIOrder = Convert.ToInt32(result["MMIOrder"]);
				}
			result.Close();
            return myMainMenuItemDetails;
        }

        //*******************************************************
        //
        // MenuDB.AddMenu() Method <a name="AddMenu"></a>
        //
        // The AddMenu method inserts a new menu record
        // into the menus database.  A unique "MenuId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-26 add parameter lastUser by Henry
        public int AddMainMenuItem (Int32 menuIntNo, string mmiName, string mmiDescr, 
			string mmiUrl, int mmiOrder, string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MainMenuItemAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int, 4);
			parameterMenuIntNo.Value = menuIntNo;
			myCommand.Parameters.Add(parameterMenuIntNo);

			SqlParameter parameterMMIName = new SqlParameter("@MMIName", SqlDbType.VarChar, 50);
			parameterMMIName.Value = mmiName;
			myCommand.Parameters.Add(parameterMMIName);

			SqlParameter parameterMMIDescr = new SqlParameter("@MMIDescr", SqlDbType.VarChar, 255);
			parameterMMIDescr.Value = mmiDescr;
			myCommand.Parameters.Add(parameterMMIDescr);

			SqlParameter parameterMMIurl = new SqlParameter("@MMIurl", SqlDbType.VarChar, 100);
			parameterMMIurl.Value = mmiUrl;
			myCommand.Parameters.Add(parameterMMIurl);
			
			SqlParameter parameterMMIOrder = new SqlParameter("@MMIOrder", SqlDbType.Int, 4);
			parameterMMIOrder.Value = mmiOrder;
			myCommand.Parameters.Add(parameterMMIOrder);

			SqlParameter parameterMMIIntNo = new SqlParameter("@MMIIntNo", SqlDbType.Int, 4);
            parameterMMIIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterMMIIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int mmiIntNo = Convert.ToInt32(parameterMMIIntNo.Value);

                return mmiIntNo;
            }
            catch {
				myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetMainMenuItemList(Int32 menuIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("MainMenuItemList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int);
			parameterMenuIntNo.Value = menuIntNo;
			myCommand.Parameters.Add(parameterMenuIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetMainMenuItemListDS(Int32 menuIntNo)
		{
			SqlDataAdapter sqlDAMenus = new SqlDataAdapter();
			DataSet dsMenus = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAMenus.SelectCommand = new SqlCommand();
			sqlDAMenus.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAMenus.SelectCommand.CommandText = "MainMenuItemList";

			// Mark the Command as a SPROC
			sqlDAMenus.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int);
			parameterMenuIntNo.Value = menuIntNo;
			sqlDAMenus.SelectCommand.Parameters.Add(parameterMenuIntNo);

			// Execute the command and close the connection
			sqlDAMenus.Fill(dsMenus);
			sqlDAMenus.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsMenus;		
		}
        // 2013-07-26 add parameter lastUser by Henry
		public int UpdateMainMenuItem (int mmiIntNo, Int32 menuIntNo, 
			string mmiName, string mmiDescr, string mmiUrl, int mmiOrder, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("MainMenuItemUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMenuIntNo = new SqlParameter("@MenuIntNo", SqlDbType.Int, 4);
			parameterMenuIntNo.Value = menuIntNo;
			myCommand.Parameters.Add(parameterMenuIntNo);

			SqlParameter parameterMMIName = new SqlParameter("@MMIName", SqlDbType.VarChar, 50);
			parameterMMIName.Value = mmiName;
			myCommand.Parameters.Add(parameterMMIName);

			SqlParameter parameterMMIDescr = new SqlParameter("@MMIDescr", SqlDbType.VarChar, 255);
			parameterMMIDescr.Value = mmiDescr;
			myCommand.Parameters.Add(parameterMMIDescr);

			SqlParameter parameterMMIurl = new SqlParameter("@MMIurl", SqlDbType.VarChar, 100);
			parameterMMIurl.Value = mmiUrl;
			myCommand.Parameters.Add(parameterMMIurl);
			
			SqlParameter parameterMMIOrder = new SqlParameter("@MMIOrder", SqlDbType.Int, 4);
			parameterMMIOrder.Value = mmiOrder;
			myCommand.Parameters.Add(parameterMMIOrder);

			SqlParameter parameterMMIIntNo = new SqlParameter("@MMIIntNo", SqlDbType.Int, 4);
			parameterMMIIntNo.Direction = ParameterDirection.InputOutput;
			parameterMMIIntNo.Value = mmiIntNo;
			myCommand.Parameters.Add(parameterMMIIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int MMIIntNo = (int)myCommand.Parameters["@MMIIntNo"].Value;
				//int menuId = (int)parameterMMIIntNo.Value;

				return MMIIntNo;
			}
			catch 
			{
				myConnection.Dispose();
				return 0;
			}
		}

		public String DeleteMainMenuItem (int mmiIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("MainMenuItemDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMMIIntNo = new SqlParameter("@MMIIntNo", SqlDbType.Int, 4);
			parameterMMIIntNo.Value = mmiIntNo;
			parameterMMIIntNo.Direction = ParameterDirection.InputOutput;
			myCommand.Parameters.Add(parameterMMIIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int MMIIntNo = (int)parameterMMIIntNo.Value;

				return MMIIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}

        public DataSet GetMenuItems(Int32 miIntNo, Int32 userIntNo)
        {
            SqlDataAdapter sqlDAMenus = new SqlDataAdapter();
            DataSet dsMenus = new DataSet();

            // Create Instance of Connection and Command Object
            sqlDAMenus.SelectCommand = new SqlCommand();
            sqlDAMenus.SelectCommand.Connection = new SqlConnection(mConstr);
            sqlDAMenus.SelectCommand.CommandText = "MenuItems";

            // Mark the Command as a SPROC
            sqlDAMenus.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int);
            parameterMMIIntNo.Value = miIntNo;
            sqlDAMenus.SelectCommand.Parameters.Add(parameterMMIIntNo);

            SqlParameter parameteruserIntNo = new SqlParameter("@UserIntNo", SqlDbType.Int);
            parameteruserIntNo.Value = userIntNo;
            sqlDAMenus.SelectCommand.Parameters.Add(parameteruserIntNo);

            // Execute the command and close the connection
            sqlDAMenus.Fill(dsMenus);
            sqlDAMenus.SelectCommand.Connection.Dispose();

            // Return the dataset result
            return dsMenus;
        }
	}
}

