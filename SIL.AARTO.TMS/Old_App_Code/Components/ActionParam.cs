﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for VerificationParam
    /// </summary>
    public class ActionParam
    {
        private string processName;
        private string rejReason;
        private string rejIntNo;
        private string registration;
        private string vehMake;
        private string vehType;
        private string userName;
        private string processType;
        private string frameNo;
        private string statusVerified;
        private string statusRejected;
        private string statusNatis;
        private string offenceDateTime;
        private string speed1;
        private string speed2;
        private string frameIntNo;
        private string preUpdatedRegNo;
        private string preUpdatedRejReason;
        private bool allowContinue;
        private string preUpdatedAllowContinue;
        private string rowVersion;

        private bool changedMake;
        private string statusProsecute;
        private string statusCancel;

        bool saveImageSettings;
        private string contrast;
        private string brightness;

        public bool SaveImageSettings
        {
            get { return saveImageSettings; }
            set { saveImageSettings = value; }
        }

        public int StatusInvestigate { get; set; }
        public int InReIntNo { get; set; }

        public string Contrast
        {
            get { return contrast; }
            set { contrast = value; }
        }

        public string Brightness
        {
            get { return brightness; }
            set { brightness = value; }
        }

        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }

        public string RejReason
        {
            get { return rejReason; }
            set { rejReason = value; }
        }

        public string RejIntNo
        {
            get { return rejIntNo; }
            set { rejIntNo = value; }
        }

        public string Registration
        {
            get { return registration; }
            set { registration = value; }
        }

        public string VehMake
        {
            get { return vehMake; }
            set { vehMake = value; }
        }

        public string VehType
        {
            get { return vehType; }
            set { vehType = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string ProcessType
        {
            get { return processType; }
            set { processType = value; }
        }

        public string FrameNo
        {
            get { return frameNo; }
            set { frameNo = value; }
        }

        public string StatusVerified
        {
            get { return statusVerified; }
            set { statusVerified = value; }
        }

        public string StatusRejected
        {
            get { return statusRejected; }
            set { statusRejected = value; }
        }

        public string StatusNatis
        {
            get { return statusNatis; }
            set { statusNatis = value; }
        }

        public string OffenceDateTime
        {
            get { return offenceDateTime; }
            set { offenceDateTime = value; }
        }

        public string Speed1
        {
            get { return speed1; }
            set { speed1 = value; }
        }

        public string Speed2
        {
            get { return speed2; }
            set { speed2 = value; }
        }

        public string FrameIntNo
        {
            get { return frameIntNo; }
            set { frameIntNo = value; }
        }

        public string PreUpdatedRegNo
        {
            get { return preUpdatedRegNo; }
            set { preUpdatedRegNo = value; }
        }

        public string PreUpdatedRejReason
        {
            get { return preUpdatedRejReason; }
            set { preUpdatedRejReason = value; }
        }

        public bool AllowContinue
        {
            get { return allowContinue; }
            set { allowContinue = value; }
        }

        public string PreUpdatedAllowContinue
        {
            get { return preUpdatedAllowContinue; }
            set { preUpdatedAllowContinue = value; }
        }

        public string RowVersion
        {
            get { return rowVersion; }
            set { rowVersion = value; }
        }

        public bool ChangedMake
        {
            get { return changedMake; }
            set { changedMake = value; }
        }

        public string StatusProsecute
        {
            get { return statusProsecute; }
            set { statusProsecute = value; }
        }

        public string StatusCancel
        {
            get { return statusCancel; }
            set { statusCancel = value; }
        }

        public ActionParam()
        {
        }
    }
}