using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // MenuItemDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class MenuItemDetails 
	{
		public Int32 MMIIntNo;
        public string MIName;
		public string MIDescr;
		public string MIurl;
		public int MIOrder;
    }

    //*******************************************************
    //
    // MenuItemDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Menus within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class MenuItemDB {

		string mConstr = string.Empty;

		public MenuItemDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // MenuDB.GetMenuItemDetails() Method <a name="GetMenuItemDetails"></a>
        //
        // The GetMenuItemDetails method returns a MenuItemDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public MenuItemDetails GetMenuItemDetails(int miIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MenuItemDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int, 4);
            parameterMIIntNo.Value = miIntNo;
            myCommand.Parameters.Add(parameterMIIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			MenuItemDetails myMenuItemDetails = new MenuItemDetails();

			while (result.Read())
				{
					// Populate Struct using Output Params from SPROC
					myMenuItemDetails.MMIIntNo = Convert.ToInt32(result["MMIIntNo"]);
					myMenuItemDetails.MIName = result["MIName"].ToString();
					myMenuItemDetails.MIDescr = result["MIDescr"].ToString();
					myMenuItemDetails.MIurl = result["MIurl"].ToString();
					myMenuItemDetails.MIOrder = Convert.ToInt32(result["MIOrder"]);
				}
			result.Close();
            //dls 060913 - added
            myConnection.Dispose();
            return myMenuItemDetails;
        }

        //*******************************************************
        //
        // MenuDB.AddMenu() Method <a name="AddMenu"></a>
        //
        // The AddMenu method inserts a new menu record
        // into the menus database.  A unique "MenuId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-26 add parameter lastUser by Henry
        public int AddMenuItem (Int32 mmiIntNo, string miName, string miDescr, 
			string miUrl, int miOrder, string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("MenuItemAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterMMIIntNo = new SqlParameter("@MMIIntNo", SqlDbType.Int, 4);
			parameterMMIIntNo.Value = mmiIntNo;
			myCommand.Parameters.Add(parameterMMIIntNo);

			SqlParameter parameterMIName = new SqlParameter("@MIName", SqlDbType.VarChar, 50);
			parameterMIName.Value = miName;
			myCommand.Parameters.Add(parameterMIName);

			SqlParameter parameterMIDescr = new SqlParameter("@MIDescr", SqlDbType.VarChar, 255);
			parameterMIDescr.Value = miDescr;
			myCommand.Parameters.Add(parameterMIDescr);

			SqlParameter parameterMIurl = new SqlParameter("@MIurl", SqlDbType.VarChar, 100);
			parameterMIurl.Value = miUrl;
			myCommand.Parameters.Add(parameterMIurl);
							
			SqlParameter parameterMIOrder = new SqlParameter("@MIOrder", SqlDbType.Int, 4);
			parameterMIOrder.Value = miOrder;
			myCommand.Parameters.Add(parameterMIOrder);

			SqlParameter parameterMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int, 4);
            parameterMIIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterMIIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int miIntNo = Convert.ToInt32(parameterMIIntNo.Value);

                return miIntNo;
            }
            catch {
				myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetMenuItemList(Int32 mmiIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("MenuItemList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMMIIntNo = new SqlParameter("@MMIIntNo", SqlDbType.Int);
			parameterMMIIntNo.Value = mmiIntNo;
			myCommand.Parameters.Add(parameterMMIIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetMenuItemListDS(Int32 mmiIntNo)
		{
			SqlDataAdapter sqlDAMenus = new SqlDataAdapter();
			DataSet dsMenus = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAMenus.SelectCommand = new SqlCommand();
			sqlDAMenus.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAMenus.SelectCommand.CommandText = "MenuItemList";

			// Mark the Command as a SPROC
			sqlDAMenus.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMMIIntNo = new SqlParameter("@MMIIntNo", SqlDbType.Int);
			parameterMMIIntNo.Value = mmiIntNo;
			sqlDAMenus.SelectCommand.Parameters.Add(parameterMMIIntNo);

			// Execute the command and close the connection
			sqlDAMenus.Fill(dsMenus);
			sqlDAMenus.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsMenus;		
		}
        // 2013-07-26 add parameter lastUser by Henry
		public int UpdateMenuItem (int miIntNo, Int32 mmiIntNo, 
			string miName, string miDescr, string miUrl, int miOrder, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("MenuItemUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMMIIntNo = new SqlParameter("@MMIIntNo", SqlDbType.Int, 4);
			parameterMMIIntNo.Value = mmiIntNo;
			myCommand.Parameters.Add(parameterMMIIntNo);

			SqlParameter parameterMIName = new SqlParameter("@MIName", SqlDbType.VarChar, 50);
			parameterMIName.Value = miName;
			myCommand.Parameters.Add(parameterMIName);

			SqlParameter parameterMIDescr = new SqlParameter("@MIDescr", SqlDbType.VarChar, 255);
			parameterMIDescr.Value = miDescr;
			myCommand.Parameters.Add(parameterMIDescr);

			SqlParameter parameterMIurl = new SqlParameter("@MIurl", SqlDbType.VarChar, 100);
			parameterMIurl.Value = miUrl;
			myCommand.Parameters.Add(parameterMIurl);
			
			SqlParameter parameterMIOrder = new SqlParameter("@MIOrder", SqlDbType.Int, 4);
			parameterMIOrder.Value = miOrder;
			myCommand.Parameters.Add(parameterMIOrder);

			SqlParameter parameterMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int, 4);
			parameterMIIntNo.Direction = ParameterDirection.InputOutput;
			parameterMIIntNo.Value = miIntNo;
			myCommand.Parameters.Add(parameterMIIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int MIIntNo = (int)myCommand.Parameters["@MIIntNo"].Value;
				//int menuId = (int)parameterMIIntNo.Value;

				return MIIntNo;
			}
			catch 
			{
				myConnection.Dispose();
				return 0;
			}
		}

		public String DeleteMenuItem (int miIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("MenuItemDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int, 4);
			parameterMIIntNo.Value = miIntNo;
			myCommand.Parameters.Add(parameterMIIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int MIIntNo = (int)parameterMIIntNo.Value;

				return MIIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

