using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;


namespace Stalberg.TMS
{

    //*******************************************************
    //
    // SubMenuItemDetails Class
    //
    // A simple data class that encapsulates details about a particular menu 
    //
    //*******************************************************

    public partial class SubMenuItemDetails 
	{
		public Int32 MIIntNo;
        public string SMIName;
		public string SMIDescr;
		public string SMIurl;
		public int SMIOrder;
    }

    //*******************************************************
    //
    // SubMenuItemDB Class
    //
    // Business/Data Logic Class that encapsulates all data
    // logic necessary to add/login/query Menus within
    // the Commerce Starter Kit Customer database.
    //
    //*******************************************************

    public partial class SubMenuItemDB {

		string mConstr = string.Empty;

		public SubMenuItemDB (string vConstr)
		{
			mConstr = vConstr;
		}

        //*******************************************************
        //
        // MenuDB.GetSubMenuItemDetails() Method <a name="GetSubMenuItemDetails"></a>
        //
        // The GetSubMenuItemDetails method returns a SubMenuItemDetails
        // struct that contains information about a specific
        // customer (name, password, etc).
        //
        //*******************************************************

        public SubMenuItemDetails GetSubMenuItemDetails(int smiIntNo) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("SubMenuItemDetail", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterSMIIntNo = new SqlParameter("@SMIIntNo", SqlDbType.Int, 4);
            parameterSMIIntNo.Value = smiIntNo;
            myCommand.Parameters.Add(parameterSMIIntNo);

            myConnection.Open();
            SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            
            // Create CustomerDetails Struct
			SubMenuItemDetails mySubMenuItemDetails = new SubMenuItemDetails();

			while (result.Read())
				{
					// Populate Struct using Output Params from SPROC
					mySubMenuItemDetails.MIIntNo = Convert.ToInt32(result["MIIntNo"]);
					mySubMenuItemDetails.SMIName = result["SMIName"].ToString();
					mySubMenuItemDetails.SMIDescr = result["SMIDescr"].ToString();
					mySubMenuItemDetails.SMIurl = result["SMIurl"].ToString();
					mySubMenuItemDetails.SMIOrder = Convert.ToInt32(result["SMIOrder"]);
				}
			result.Close();
            return mySubMenuItemDetails;
        }

        //*******************************************************
        //
        // MenuDB.AddMenu() Method <a name="AddMenu"></a>
        //
        // The AddMenu method inserts a new menu record
        // into the menus database.  A unique "MenuId"
        // key is then returned from the method.  
        //
        //*******************************************************
        // 2013-07-26 add parameter lastUser by Henry
        public int AddSubMenuItem (Int32 miIntNo, string smiName, 
			string smiDescr, string smiUrl, int smiOrder, string lastUser) 
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(mConstr);
            SqlCommand myCommand = new SqlCommand("SubMenuItemAdd", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
			SqlParameter parameterMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int, 4);
			parameterMIIntNo.Value = miIntNo;
			myCommand.Parameters.Add(parameterMIIntNo);

			SqlParameter parameterSMIName = new SqlParameter("@SMIName", SqlDbType.VarChar, 50);
			parameterSMIName.Value = smiName;
			myCommand.Parameters.Add(parameterSMIName);

			SqlParameter parameterSMIDescr = new SqlParameter("@SMIDescr", SqlDbType.VarChar, 255);
			parameterSMIDescr.Value = smiDescr;
			myCommand.Parameters.Add(parameterSMIDescr);

			SqlParameter parameterSMIurl = new SqlParameter("@SMIurl", SqlDbType.VarChar, 100);
			parameterSMIurl.Value = smiUrl;
			myCommand.Parameters.Add(parameterSMIurl);
			
			SqlParameter parameterSMIOrder = new SqlParameter("@SMIOrder", SqlDbType.Int, 4);
			parameterSMIOrder.Value = smiOrder;
			myCommand.Parameters.Add(parameterSMIOrder);

			SqlParameter parameterSMIIntNo = new SqlParameter("@SMIIntNo", SqlDbType.Int, 4);
            parameterSMIIntNo.Direction = ParameterDirection.Output;
            myCommand.Parameters.Add(parameterSMIIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

            try {
                myConnection.Open();
                myCommand.ExecuteNonQuery();
                myConnection.Dispose();

                // Calculate the CustomerID using Output Param from SPROC
                int smiIntNo = Convert.ToInt32(parameterSMIIntNo.Value);

                return smiIntNo;
            }
            catch {
				myConnection.Dispose();
                return 0;
            }
        }

		public SqlDataReader GetSubMenuItemList(Int32 miIntNo)
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("SubMenuItemList", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int);
			parameterMIIntNo.Value = miIntNo;
			myCommand.Parameters.Add(parameterMIIntNo);

			// Execute the command
			myConnection.Open();
			SqlDataReader result = myCommand.ExecuteReader(CommandBehavior.CloseConnection);

			// Return the datareader result
			return result;
		}

		public DataSet GetSubMenuItemListDS(Int32 miIntNo)
		{
			SqlDataAdapter sqlDAMenus = new SqlDataAdapter();
			DataSet dsMenus = new DataSet();

			// Create Instance of Connection and Command Object
			sqlDAMenus.SelectCommand = new SqlCommand();
			sqlDAMenus.SelectCommand.Connection = new SqlConnection(mConstr);			
			sqlDAMenus.SelectCommand.CommandText = "SubMenuItemList";

			// Mark the Command as a SPROC
			sqlDAMenus.SelectCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int);
			parameterMIIntNo.Value = miIntNo;
			sqlDAMenus.SelectCommand.Parameters.Add(parameterMIIntNo);

			// Execute the command and close the connection
			sqlDAMenus.Fill(dsMenus);
			sqlDAMenus.SelectCommand.Connection.Dispose();

			// Return the dataset result
			return dsMenus;		
		}
        // 2013-07-26 add parameter lastUser by Henry
		public int UpdateSubMenuItem (int smiIntNo, Int32 miIntNo, 
			string smiName, string smiDescr, string smiUrl, int smiOrder, string lastUser) 
		{
			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("SubMenuItemUpdate", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterMIIntNo = new SqlParameter("@MIIntNo", SqlDbType.Int, 4);
			parameterMIIntNo.Value = miIntNo;
			myCommand.Parameters.Add(parameterMIIntNo);

			SqlParameter parameterSMIName = new SqlParameter("@SMIName", SqlDbType.VarChar, 50);
			parameterSMIName.Value = smiName;
			myCommand.Parameters.Add(parameterSMIName);

			SqlParameter parameterSMIDescr = new SqlParameter("@SMIDescr", SqlDbType.VarChar, 255);
			parameterSMIDescr.Value = smiDescr;
			myCommand.Parameters.Add(parameterSMIDescr);

			SqlParameter parameterSMIurl = new SqlParameter("@SMIurl", SqlDbType.VarChar, 100);
			parameterSMIurl.Value = smiUrl;
			myCommand.Parameters.Add(parameterSMIurl);
			
			SqlParameter parameterSMIOrder = new SqlParameter("@SMIOrder", SqlDbType.Int, 4);
			parameterSMIOrder.Value = smiOrder;
			myCommand.Parameters.Add(parameterSMIOrder);

			SqlParameter parameterSMIIntNo = new SqlParameter("@SMIIntNo", SqlDbType.Int, 4);
			parameterSMIIntNo.Direction = ParameterDirection.InputOutput;
			parameterSMIIntNo.Value = smiIntNo;
			myCommand.Parameters.Add(parameterSMIIntNo);

            SqlParameter parameterLastUser = new SqlParameter("@LastUser", SqlDbType.VarChar, 50);
            parameterLastUser.Value = lastUser;
            myCommand.Parameters.Add(parameterLastUser);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int SMIIntNo = (int)myCommand.Parameters["@SMIIntNo"].Value;
				//int menuId = (int)parameterSMIIntNo.Value;

				return SMIIntNo;
			}
			catch 
			{
				myConnection.Dispose();
				return 0;
			}
		}

		public String DeleteSubMenuItem (int smiIntNo)
		{

			// Create Instance of Connection and Command Object
			SqlConnection myConnection = new SqlConnection(mConstr);
			SqlCommand myCommand = new SqlCommand("SubMenuItemDelete", myConnection);

			// Mark the Command as a SPROC
			myCommand.CommandType = CommandType.StoredProcedure;

			// Add Parameters to SPROC
			SqlParameter parameterSMIIntNo = new SqlParameter("@SMIIntNo", SqlDbType.Int, 4);
			parameterSMIIntNo.Direction = ParameterDirection.InputOutput;
			parameterSMIIntNo.Value = smiIntNo;
			myCommand.Parameters.Add(parameterSMIIntNo);

			try 
			{
				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Dispose();

				// Calculate the CustomerID using Output Param from SPROC
				int SMIIntNo = (int)parameterSMIIntNo.Value;

				return SMIIntNo.ToString();
			}
			catch 
			{
				myConnection.Dispose();
				return String.Empty;
			}
		}
	}
}

