using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Collections.Generic;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that displays the details for summonses for an LA
    /// </summary>
    public partial class PrintNoticeOfWOA : System.Web.UI.Page
    {

        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "PrintNoticeOfWOA.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Select the next print file for printing of notices for warrant of arrest";


        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {

            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //PopulateAuthorities( autIntNo);

            //GridView1.Caption = "No Of Print Files: " + GridView1.Rows.Count.ToString();

            if (!IsPostBack)
            {
                // 2013-09-13, Oscar moved, no need to load in every time.
                PopulateAuthorities(autIntNo);

                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                autIntNo = int.Parse(Session["autIntNo"].ToString());
                ddlSelectLA.SelectedValue = autIntNo.ToString();
                //2013-04-11 add by Henry for fix bug of select no change
                ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));
            }
        }

        //mrs 20081019 removed table adapter from page
        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorities( int autIntNo)
        {

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");


            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            //ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));
            //reader.Close();

            BindGrid(autIntNo, true);
        }

        //mrs 20081019 removed table adapter from page
        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            Session["printAutIntNo"] = autIntNo;
            BindGrid(autIntNo, true);
            //this.TicketNumberSearch1.AutIntNo = autIntNo;
        }

        public void BindGrid(int autIntNo, bool isReload)
        {
            //2013-04-09 add by Henry for pagination
            if (isReload)
            {
                GridView1Pager.RecordCount = 0;
                GridView1Pager.CurrentPageIndex = 1;
            }
            Stalberg.TMS.NoticeOfWoaDB printList = new Stalberg.TMS.NoticeOfWoaDB(connectionString);
            int totalCount = 0; //2013-04-09 add by Henry for pagination
            GridView1.DataSource = printList.GetNoticeOfWOAPrintrunDS(autIntNo, GridView1Pager.PageSize, GridView1Pager.CurrentPageIndex, out totalCount);
            GridView1.DataKeyNames = new string[] { "SumNoticeOfWOAPrintFile" };
            GridView1.DataBind();
            GridView1Pager.RecordCount = totalCount;

            if (GridView1.Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                GridView1.Visible = true;
                lblError.Visible = false;
            }

            //try
            //{
            //    GridView1.DataBind();
            //}
            //catch
            //{
            //    GridView1.CurrentPageIndex = 0;
            //    GridView1.DataBind();
            //}

            //if (GridView1.Items.Count == 0)
            //{
            //    GridView1.Visible = false;
            //    lblError.Visible = true;
            //    lblError.Text = "There are no print files available for printing";
            //}
            //else
            //{
            //    GridView1.Visible = true;
            //    lblError.Visible = false;
            //}
        }

        //mrs 20081019 using the default control name is lazy programming and bad documentation
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Get the grid row index of the button that was pressed.
            int index = Convert.ToInt32(e.CommandArgument);
            //Jerry 2012-04-13 add
            if (GridView1.Rows.Count <= 0)
            {
                return;
            }

            // Get the text value of the printfile name from the grid.
            string printFile = GetCellStringValueFromGridView(GridView1, index, 2);
            if (printFile == "&nbsp;" || printFile == null || printFile == string.Empty)
            {
                //can't print an empty file
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            // Get the value for the Printed and Posted check boxes and immediately
            // change it to the opposite. This value will be used later as is to
            // change the value in the grid.

            //Int16 isPrinted = Convert.ToInt16(!GetCheckBoxValueFromGridView(GridView1, index, 0));
            //Int16 isPosted = Convert.ToInt16(!GetCheckBoxValueFromGridView(GridView1, index, 1));

            //bool isPrinted = !GetCheckBoxValueFromGridView(GridView1, index, 0);
            //bool isPosted = !GetCheckBoxValueFromGridView(GridView1, index, 1);

            //bool noticePrinted = GetCheckBoxValueFromGridView(GridView1, index, 0);

            NoticeOfWoaDB noticeOfWoaDB = new NoticeOfWoaDB(this.connectionString);

            string errorMsg = string.Empty;

            switch (e.CommandName)
            {
                case "cmdPrint":
                    {
                        //mrs 20081108 if they want to reprint we are happy to let them - we don't need to change the status codes to do a reprint 
                        //if (noticePrinted)
                        //{
                        //    lblError.Text = "To reprint, please click on Update Print Status to " +
                        //        "\nreset the printed status before printing.";
                        //    break;
                        //}

                        if (IsPostBack)
                        {
                            lblError.Text = (string)GetLocalResourceObject("lblError.Text");

                            // "Print" the notice to the NoticeOfWOAViewer.aspx web page.
                            if (printFile.Equals(""))
                            {
                                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                                lblError.Visible = true;
                                return;
                            }

                            bool success = false;
                            Session.Add("success", success);
                            // Add our JavaScript to the currently rendered page
                            Helper_Web.BuildPopup(this, "NoticeOfWOAViewer.aspx", "PrintFile", printFile, "autIntNo", autIntNo.ToString(), "success");
                            lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                        }

                        break;
                    }
                case "cmdUpdatePrintStatus":
                    {
                        if (IsPostBack)
                        {
                            lblError.Text = (string)GetLocalResourceObject("lblError.Text5");

                            //20081107 added lastuser
                            int success = noticeOfWoaDB.SetNoticeOfWoaAsPrinted(printFile, true, this.login, ref errorMsg);
                            if (success < 1)
                                lblError.Text = (string)GetLocalResourceObject("lblError.Text6") + errorMsg;
                            else
                            {
                                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                                
                                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintNoticeOfWOA, PunchAction.Change);  

                            }
                        }
                        break;
                    }
                case "cmdPost":
                    {
                        if (IsPostBack)
                        {
                            lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                            //20081107 added lastuser

                            int success = noticeOfWoaDB.SetNoticeOfWoaAsPosted(printFile, true, this.login, ref errorMsg);

                            if (success < 1)
                                lblError.Text = (string)GetLocalResourceObject("lblError.Text9") + errorMsg;
                            else
                            {
                                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                                
                                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintNoticeOfWOA, PunchAction.Change); 
                            }
                        }
                        break;
                    }


            }

            lblError.Visible = true;

            GridView1.DataBind();
            GridView1.Caption = (string)GetLocalResourceObject("GridView1.Caption") + GridView1.Rows.Count.ToString();

        }

        private bool GetCheckBoxValueFromGridView(GridView pGridView, int pRowIndex, int pCollumnIndex)
        {
            CheckBox checkBox = ((CheckBox)pGridView.Rows[pRowIndex].Cells[pCollumnIndex].Controls[0]);
            bool value = checkBox.Checked;
            return value;
        }

        public void RequestEnded(object sender, EventArgs e)
        {
            lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
        }

        private string GetCellStringValueFromGridView(GridView pGridView, int pRowIndex, int pCollumnIndex)
        {
            return pGridView.Rows[pRowIndex].Cells[pCollumnIndex].Text;
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            GridView1.Caption = (string)GetLocalResourceObject("GridView1.Caption") + GridView1.Rows.Count.ToString();

            if (GridView1.Rows.Count <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
        }

        // 2013-04-09 add by Henry for pagination
        protected void GridView1Pager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo, false);
        }
    }
}
