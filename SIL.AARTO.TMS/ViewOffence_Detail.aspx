<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Register Src="ImageControl.ascx" TagName="ImageViewer" TagPrefix="iv1" %>
<%@ Page Language="c#" AutoEventWireup="false" enableEventValidation="false"
    Inherits="Stalberg.TMS.ViewOffence_Detail" Codebehind="ViewOffence_Detail.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= title %>
    </title>
    <!--<link href="<%= styleSheet %>" type="text/css" rel="stylesheet">-->
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0" onload="self.focus();"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" colspan="2" valign="middle" style="width: 100%">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
         <asp:UpdatePanel ID="udpFrame" runat="server">
                        <ContentTemplate>
       
        <table border="0" width="100%">
            <tr>
                <td valign="top" width="100%" align="center">
                <TABLE 
style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; WIDTH: 100%; BORDER-BOTTOM: black 1px solid" 
class="Normal" border=0><TBODY><TR><TD style="WIDTH: 10%; HEIGHT: 21px"><asp:LinkButton id="lnkPrevious" onclick="lnkPrevious_Click" runat="server" CssClass="MenuItem" Text="<%$Resources:lnkPrevious.Text %>"></asp:LinkButton></TD><TD 
style="WIDTH: 80%; HEIGHT: 21px; TEXT-ALIGN: center"><asp:Label id="lblNavigation" runat="server" Font-Bold="True"></asp:Label></TD><TD 
style="WIDTH: 10%; HEIGHT: 21px; TEXT-ALIGN: right"><asp:LinkButton id="lnkNext" onclick="lnkNext_Click" runat="server" CssClass="MenuItem" Text="<%$Resources:lnkNext.Text %>"></asp:LinkButton></TD></TR></TBODY></TABLE>
                    <table width="100%">
                        <tr>
                            <td width="33%">
                                &nbsp;<asp:HyperLink ID="hlPrintVersion" runat="server" CssClass="NormalButton" NavigateUrl="ViewOffence_Print.aspx"
                                    Target="_blank" ForeColor="White" Text="<%$Resources:hlPrintVersion.Text %>"></asp:HyperLink></td>
                            <td width="33%" align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="345px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></td>
                            <td width="34%" align="right">
                                <asp:HyperLink ID="hlClose" runat="server" CssClass="NormalButton" ForeColor="White"
                                    NavigateUrl="javascript:window.close();" Text="<%$Resources:hlClose.Text %>"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></td>
            </tr>
            <tr>
                <td valign="top" width="100%">
                    <table width="100%">
                        <tr>
                            <td width="100%" align="center">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top" style="width: 933px; height: 69px;">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 144px" valign="top">
                                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Width="107px" Text="<%$Resources:lblTicketNo.Text %>"></asp:Label></td>
                                                    <td style="width: 146px" valign="top">
                                                        <asp:Label ID="lblTicketNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td style="width: 180px" valign="top">
                                                        <asp:Label ID="Label19" runat="server" CssClass="NormalBold" Width="160px" Text="<%$Resources:lblFilmFrameNo.Text %>"></asp:Label></td>
                                                    <td style="width: 154px" valign="top">
                                                        <asp:Label ID="lblFrameNo" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Width="137px" Height="19px" Text="<%$Resources:lblRegistrationno.Text %>"></asp:Label></td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblRegNo" runat="server" BorderStyle="None" CssClass="Normal"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width: 144px; height: 21px;">
                                                        <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="173px" Height="19px" Text="<%$Resources:lblOffenceDate.Text %>"></asp:Label></td>
                                                    <td valign="top" style="width: 146px; height: 21px;">
                                                        <asp:Label ID="lblOffenceDate" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td style="width: 180px; height: 21px;" valign="top">
                                                        <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Width="131px" Text="<%$Resources:lblSpeed.Text %>"></asp:Label></td>
                                                    <td style="width: 250px; height: 21px;" valign="top">
                                                        <asp:Label ID="lblSpeed" runat="server" CssClass="Normal"></asp:Label>
                                                        <asp:Label ID="lblSpeed2" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td valign="top" style="height: 21px">
                                                    </td>
                                                    <td valign="top" style="height: 21px">
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="173px" Height="19px" Text="<%$Resources:lblOffencedesc.Text %>"></asp:Label>&nbsp;
                                            <asp:Label ID="lblOffence" runat="server" CssClass="Normal"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 933px" valign="top">
                                            <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Height="19px" Width="173px" Text="<%$Resources:lblLocation.Text %>"></asp:Label>&nbsp;
                                            <asp:Label ID="lblLocDescr" runat="server" CssClass="Normal"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <table id="tblOwnerDetails" runat="server" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="94px" Font-Underline="True" Text="<%$Resources:lblOwnerdetails.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="94px" Font-Underline="True" Text="<%$Resources:lblDriverdetails.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblProxy" runat="server" CssClass="NormalBold" Width="202px" Font-Underline="True" Text="<%$Resources:lblProxy.Text %>"></asp:Label></td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="46px" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                        <asp:Label ID="lblOwnerName" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Width="31px" Text="<%$Resources:lblID.Text %>"></asp:Label>
                                                        <asp:Label ID="lblOwnerID" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Width="45px" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                        <asp:Label ID="lblDriverName" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Width="26px" Text="<%$Resources:lblID.Text %>"></asp:Label>
                                                        <asp:Label ID="lblDriverID" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="lblProxyName" runat="server" CssClass="NormalBold" Width="56px" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                        <asp:Label ID="lblPrxName" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="lblProxyID" runat="server" CssClass="NormalBold" Width="31px" Text="<%$Resources:lblID.Text %>"></asp:Label>
                                                        <asp:Label ID="lblPrxID" runat="server" CssClass="Normal"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="122px" Text="<%$Resources:lblPostaladdress.Text %>"></asp:Label>&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOwnerPostAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Width="126px" Text="<%$Resources:lblPostaladdress.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDriverPostAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Width="122px" Text="<%$Resources:lblStreetaddress.Text %>"> </asp:Label>&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOwnerStrAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Width="124px" Text="<%$Resources:lblStreetaddress.Text %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDriverStrAddr" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold">
                                                        <asp:Label ID="Label17" runat="server" Text="<%$Resources:lblFineAmount.Text %>"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="lblFineAmount" runat="server" CssClass="Normal"></asp:Label></td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        <table width="100%">
            <tr>
                <td width="100%">
                        <%--<iframe id="imageViewer" src="<%=ImageViewUrl%>" width="845px" height="600px" frameborder="0" marginheight="0" marginwidth="0"></iframe>--%>
                        <iv1:ImageViewer ID="imageViewer1" runat="server" />
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="5%" style="width: 100%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width: 100%" align="center">
                    <asp:UpdateProgress ID="udp" runat="server" AssociatedUpdatePanelID="udpFrame">
                        <ProgressTemplate>
                            <p class="NormalBold" style="text-align: center">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label18"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
