using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    //mrs 20080531 removed linking to area to a new page - SummonsServerLinkToArea
	/// <summary>
	/// Represents a list with associated editor for Summons Server companies.
	/// </summary>
	public partial class SummonsServer : System.Web.UI.Page
    {
        #region cache, Oscar 2013-05-08 added
        [Serializable]
        class AuthorityModel
        {
            public int AutIntNo { get; set; }
            public string AutCode { get; set; }
            public string AutName { get; set; }
            public string AutDescr { get { return string.Format("{0} ({1})", AutName, AutCode); } }
        }
        [Serializable]
        class CourtModel
        {
            public int CrtIntNo { get; set; }
            public string CrtName { get; set; }
        }
        [Serializable]
        class SummonsServerModel
        {
            public int SSIntNo { get; set; }
            public string SSFullName { get; set; }
        }
        [Serializable]
        class PageCache
        {
            public int? ASCIntNo { get; set; }
            public int? ASCPageIndex { get; set; }
            public int? SSIntNo { get; set; }
            public List<AuthorityModel> AuthList { get; set; }
            public List<CourtModel> CourtList { get; set; }
            public List<SummonsServerModel> SumServerList { get; set; }
        }

	    ViewStateCache<PageCache> cache;
        #endregion

        // Fields
		private string connectionString = string.Empty;
		private string loginUser;
		private string thisPageURL = "SummonsServer.aspx";
		private int autIntNo = 0;
		//private DataSet dsAreas = null;

		protected string description = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string keywords = string.Empty;
		protected string title = string.Empty;
        //protected string thisPage = "Summons Server Maintenance";

	    AuthSsCourtService ascService = new AuthSsCourtService();
        public string ContinueToDelete { get { return (string)GetLocalResourceObject("ContinueToDelete"); } }

        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);
            if (cache == null)
                cache = new ViewStateCache<PageCache>(ViewState);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void OnLoad(System.EventArgs e)
		{
			this.connectionString = Application["constr"].ToString();

			// Get user info from session variable
			if (Session["userDetails"] == null)
				Server.Transfer("Login.aspx?Login=invalid");
			if (Session["userIntNo"] == null)
				Server.Transfer("Login.aspx?Login=invalid");

			// Get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();
			userDetails = (UserDetails)Session["userDetails"];
			//int 
			autIntNo = Convert.ToInt32(Session["autIntNo"]);
			Session["userLoginName"] = userDetails.UserLoginName.ToString();
			int userAccessLevel = userDetails.UserAccessLevel;
			loginUser = userDetails.UserLoginName;

			//set domain specific variables
			General gen = new General();
			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
			title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                //2012-3-1 Linda  Modified into a multi-language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				this.pnlDeleteSummonsServer.Visible = false;
				this.pnlSummonsServerDetails.Visible = false;
				//this.pnlAuthority.Visible = false;

				this.btnOptHide.Visible = true;
                
				this.BindGrid(true);

                BindDllAuthority(autIntNo);
			}
		}

		private void BindGrid(bool useEditGrid)
		{
			// Obtain and bind a list of all users
			DataSet data = (new Stalberg.TMS.SummonsServerDB(connectionString)).GetSummonsServerListDS();

			// Output to the grid or the authorities list
			if (useEditGrid)
			{
				grdSummonsServers.DataSource = data;
				grdSummonsServers.DataKeyField = "SSIntNo";

                //dls 070410 - if we're not on the first page, and we do a search, an error results
                try
                {
                    grdSummonsServers.DataBind();
                }
                catch
                {
                    grdSummonsServers.CurrentPageIndex = 0;
                    grdSummonsServers.DataBind();
                }


				if (grdSummonsServers.Items.Count == 0)
				{
					grdSummonsServers.Visible = false;
					lblError.Visible = true;
                    //2012-3-1 Linda  Modified into a multi-language
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text");
				}
				else
					grdSummonsServers.Visible = true;

				pnlSummonsServerDetails.Visible = false;
			}
			else
			{
                //this.grdSummonsServers2Link.DataSource = data;
                //this.grdSummonsServers2Link.DataKeyField = "SSIntNo";
                //this.grdSummonsServers2Link.DataBind();
			}

			data.Dispose();
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// Add a new Summon s Server
			this.pnlSummonsServerDetails.Visible = false;

			this.ShowSSDetails(0);
            this.lblSummonsServerDetails.Text = (string)GetLocalResourceObject("lblSummonsServerDetails.Text1");
            this.btnSaveSummonsServer.Text = (string)GetLocalResourceObject("btnSaveSummonsServer.Text1");
		}

		private void ShowSSDetails(int ssIntNo)
		{
			this.pnlDeleteSummonsServer.Visible = false;

			//this.ViewState.Add("SSIntNo", ssIntNo);
		    cache.Current.SSIntNo = ssIntNo;

			Stalberg.TMS.SummonsServerDB ss = new Stalberg.TMS.SummonsServerDB(connectionString);
			Stalberg.TMS.SummonsServerDetails ssDetails = ss.GetSummonsServerDetails(ssIntNo, 0);
			txtSSFullName.Text = ssDetails.SSFullName;
			txtSSAddress.Text = ssDetails.SSAddress;
			txtSSTelNo.Text = ssDetails.SSTelNo;
			txtSSEmail.Text = ssDetails.SSEmail;
            txtSSCode.Text = ssDetails.SSCode; //added column
			pnlSummonsServerDetails.Visible = true;
            //jerry 2011-12-28 add
            lblValidEmail.Text = "";

            this.lblSummonsServerDetails.Text = (string)GetLocalResourceObject("lblSummonsServerDetails.Text2");
            this.btnSaveSummonsServer.Text = (string)GetLocalResourceObject("btnSaveSummonsServer.Text");
		}

		protected void btnUpdateSS_Click(object sender, EventArgs e)
		{
			this.lblError.Visible = true;

			//int ssIntNo = Convert.ToInt32(this.ViewState["SSIntNo"]);
		    var ssIntNo = cache.Current.SSIntNo.GetValueOrDefault();
			bool isValid = true;
            //2012-3-1 Linda  Modified into a multi-language
			StringBuilder sb = new StringBuilder( (string)GetLocalResourceObject("lblError.Text1")+"\n<ul>");

			string ssName = this.txtSSFullName.Text.Trim();
			if (ssName.Length == 0)
			{
				isValid = false;
                //2012-3-1 Linda  Modified into a multi-language
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text2") + "</li>\n");
			}

            //jerry 2011-12-28 add
            string ssEmail = this.txtSSEmail.Text.Trim();
            if (ssEmail.Length > 0)
            {
                if (!Regex.IsMatch(this.txtSSEmail.Text.Trim(), @"[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"))
                {
                    isValid = false;
                    //2012-3-1 Linda  Modified into a multi-language
                    lblValidEmail.Text = (string)GetLocalResourceObject("lblValidEmail.Text");
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text3") + "</li>\n");
                }
            }

			if (!isValid)
			{
				sb.Append("</ul>\n");
				this.lblError.Text = sb.ToString();
				return;
			}

			// Update the Summons Server details
			Stalberg.TMS.SummonsServerDB ssUpdate = new SummonsServerDB(connectionString);
			ssIntNo = ssUpdate.UpdateSummonsServer(ssName, txtSSAddress.Text.Trim(), txtSSTelNo.Text.Trim(), txtSSEmail.Text.Trim(), txtSSCode.Text.Trim() ,loginUser, ssIntNo);
            //Jerry 2014-03-26 check duplicated SSFullName 
            if (ssIntNo == -1)
                lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text6"), ssName);
            else if (ssIntNo <= 0)
                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            else
            {
                //2012-3-1 Linda  Modified into a multi-language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerMaintenance, PunchAction.Change);

                //this.ViewState.Add("SSIntNo", ssIntNo);
                cache.Current.SSIntNo = ssIntNo;
                cache.Current.SumServerList = null;
                this.BindGrid(true);
            }
		}

		protected void dgSummonsServer_ItemCommand(object source, DataGridCommandEventArgs e)
		{
			// Store details of page in case of re-direct
			this.Session.Add("prevPage", thisPageURL);

			// Select what to do
			if (e.Item.ItemIndex >= 0)
			{
				grdSummonsServers.SelectedIndex = e.Item.ItemIndex;
				int ssIntNo = Convert.ToInt32(grdSummonsServers.DataKeys[grdSummonsServers.SelectedIndex]);
				//this.ViewState.Add("SSIntNo", ssIntNo);
			    cache.Current.SSIntNo = ssIntNo;

                string edit = (string)GetLocalResourceObject("grdSummonsServers.HeaderText3");
                string delete = (string)GetLocalResourceObject("grdSummonsServers.HeaderText4");
                if (((LinkButton)e.CommandSource).Text == edit) {
                    this.ShowSSDetails(ssIntNo);
                } 
                if (((LinkButton)e.CommandSource).Text == delete)
                {
                    this.CheckDeleteSummonsServer(ssIntNo, e.Item.Cells[1].Text);
                }
				
			}
		}

		protected void dgSummonsServer_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			this.grdSummonsServers.CurrentPageIndex = e.NewPageIndex;
			this.BindGrid(true);
		}

		protected void btnOptAuthority_Click(object sender, System.EventArgs e)
		{
            //this.pnlSummonsServerDetails.Visible = false;
            //this.pnlSummonsServers.Visible = false;
            //this.btnOptHide.Text = "Show List";

			//this.pnlAuthority.Visible = true;
			//this.pnlAuthorityLinks.Visible = false;
			//this.pnlAuthorityNewArea.Visible = false;
			//this.pnlAuthorityNewSummonsServer.Visible = false;

            //int 
            //this.PopulateUserGroupAuthorityList(ugIntNo);
		}

		private void PopulateUserGroupAuthorityList(int ugIntNo)
		{
			//UserGroup_AuthDB uga = new UserGroup_AuthDB(connectionString);

            //ddlAuthority.DataSource = uga.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlAuthority.DataValueField = "AutIntNo";
            //ddlAuthority.DataTextField = "AutDescr";
            //ddlAuthority.DataBind();
            //ddlAuthority.Items.Insert(0, "Please select a local authority");

            //foreach (ListItem item in this.ddlAuthority.Items)
            //{
            //    if (item.Value.Equals(this.autIntNo.ToString()))
            //    {
            //        item.Selected = true;
            //        //this.pnlAuthorityLinks.Visible = true;
            //        this.GetLinkedSummonServers();
            //        this.ViewState.Add("SelectedAuthIntNo", this.autIntNo);
            //        break;
            //    }
            //}
		}

		private void PopulateAuthorityList(int mtrIntNo)
		{
            //AuthorityDB auth = new AuthorityDB(connectionString);

            //this.ddlAuthority.DataSource = auth.GetAuthorityList(mtrIntNo, "AutDescr");
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutDescr";
            //this.ddlAuthority.DataBind();
            //this.ddlAuthority.Items.Insert(0, "Select a local authority");
		}

		protected void btnAddAuthority_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
            //if (ddlAuthority.SelectedIndex < 1)
            //{
            //    lblError.Text = "Please select a local authority to add to the summons server";
            //    lblError.Visible = true;
            //    return;
            //}

            //int autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);
            //int ssIntNo = Convert.ToInt32(Session["editSSIntNo"]);

            //SummonsServerDB court = new Stalberg.TMS.SummonsServerDB(connectionString);

            //int addASIntNo = court.AddAuth_SummonsServer(autIntNo, ssIntNo, loginUser);

            //if (addASIntNo <= 0)
            //    lblError.Text = "Unable to add local authority to this summons server - it is already linked";
            //else
            //    lblError.Text = "Summons server has been added to court";

            //lblError.Visible = true;
            ////this.PopulateAuth_SSList(ssIntNo);
		}

		private void CheckDeleteSummonsServer(int ssIntNo, string name)
		{
			this.pnlDeleteSummonsServer.Visible = true;
			//this.pnlAuthority.Visible = false;
			this.pnlSummonsServerDetails.Visible = false;

			SummonsServerDB ssDB = new SummonsServerDB(Application["constr"].ToString());
			int noLinkedAuthorities = ssDB.DeleteSummonsServer(ssIntNo, true);
			if (noLinkedAuthorities == 0)
            {//2012-3-1 Linda  Modified into a multi-language
                this.lblDeleteSummonsServer.Text = string.Format((string)GetLocalResourceObject("lblDeleteSummonsServer.Text1"), name);
				this.btnDeleteSummonsServerYes.Visible = true;
				this.btnDeleteSummonsServerNo.Visible = true;
			}
			else
            {//2012-3-1 Linda  Modified into a multi-language
                this.lblDeleteSummonsServer.Text = string.Format((string)GetLocalResourceObject("lblDeleteSummonsServer.Text1"), noLinkedAuthorities, name);
				this.btnDeleteSummonsServerYes.Visible = false;
				this.btnDeleteSummonsServerNo.Visible = true;
			}
		}

		private void DeleteSummonsServer(int ssIntNo)
		{
			SummonsServerDB ssDB = new SummonsServerDB(Application["constr"].ToString());
			int noLinkedAuthorities = ssDB.DeleteSummonsServer(ssIntNo, false);
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerMaintenance, PunchAction.Delete);  
			//this.ViewState.Add("SSIntNo", 0);
            cache.Current.SSIntNo = 0;
			this.BindGrid(true);
		}

		protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (grdSummonsServers.Visible.Equals(true))
			{
				// Hide it
				this.pnlSummonsServers.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
			}
			else
			{
				// Show it
				//this.pnlAuthority.Visible = false;
				this.pnlSummonsServers.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

		 

		protected void btnDeleteSummonsServerNo_Click(object sender, EventArgs e)
		{
			//this.pnlAuthority.Visible = false;
			this.pnlSummonsServerDetails.Visible = false;
			this.pnlDeleteSummonsServer.Visible = false;
		}

		protected void btnDeleteSummonsServerYes_Click(object sender, EventArgs e)
		{
			//this.DeleteSummonsServer((int)this.ViewState["SSIntNo"]);
            this.DeleteSummonsServer(cache.Current.SSIntNo.GetValueOrDefault());
			//this.pnlAuthority.Visible = false;
			this.pnlSummonsServerDetails.Visible = false;
			this.pnlDeleteSummonsServer.Visible = false;
		}

        //protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        //{
			// If its the select instruction clear the grids
            //if (this.ddlAuthority.SelectedIndex == 0)
            //{
            //    this.pnlAuthorityNewArea.Visible = false;
            //    this.pnlAuthorityNewSummonsServer.Visible = false;
            //    this.pnlAuthorityLinks.Visible = false;
            //    return;
            //}

            //this.ViewState.Add("SelectedAuthIntNo", int.Parse(this.ddlAuthority.SelectedValue));
            //this.pnlAuthorityLinks.Visible = true;

            //// Repopulate the list
            //this.GetLinkedSummonServers();
        //}

		private void GetLinkedSummonServers()
		{
            //int authIntNo = int.Parse(this.ddlAuthority.SelectedValue);

            //SummonsServerDB ssDB = new SummonsServerDB(Application["constr"].ToString());
            //SqlDataReader reader = ssDB.GetAuth_SummonsServerListByAuth(authIntNo);

            //this.grdLinkedSummonsServers.DataSource = reader;
            //this.grdLinkedSummonsServers.DataKeyField = "SSIntNo";
            //this.grdLinkedSummonsServers.DataBind();
		}

		protected void lnkLinkSummonsServer_Click(object sender, EventArgs e)
		{
            //this.pnlAuthorityLinks.Visible = false;
            //this.pnlAuthorityNewArea.Visible = true;
            //this.txtSearchAreaCode.Text = string.Empty;
            //this.txtSearchAreaDescription.Text = string.Empty;
            //this.PopulateAreaGrid();
		}

		private void PopulateAreaGrid()
		{
            //AreaCodeDB areaDB = new AreaCodeDB(Application["constr"].ToString());
            //this.dsAreas = areaDB.GetAllAreaCodesDS();
            //this.ViewState.Add("AreasDataSet", this.dsAreas);
            //this.grdArea.CurrentPageIndex = 0;
            //this.PopulateAreaFromDataSet();
		}
		private void PopulateAreaFromDataSet()
		{
            //DataSet ds = (DataSet)this.ViewState["AreasDataSet"];
            //this.grdArea.DataSource = ds;
            //this.grdArea.DataBind();
		}
		protected void grdLinkedSummonsServers_ItemCommand(object source, DataGridCommandEventArgs e)
		{
            //int asIntNo = int.Parse(e.Item.Cells[0].Text);
            //this.UnlinkSummonsServerFromAuthority(asIntNo);
		}

		private void UnlinkSummonsServerFromAuthority(int asIntNo)
		{
            //SummonsServerDB ssDB = new SummonsServerDB(Application["constr"].ToString());
            //ssDB.UnlinkAuthoritySummonsServerArea(asIntNo);
            //this.GetLinkedSummonServers();
		}

		protected void grdArea_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
		{
            //this.grdArea.CurrentPageIndex = e.NewPageIndex;
            //this.PopulateAreaFromDataSet();
		}

		protected void grdArea_ItemCommand(object source, DataGridCommandEventArgs e)
		{
            //if (e.CommandName == "Select")
            //{
            //    this.ViewState.Add("NewAreaCode", e.Item.Cells[0].Text);
            //    this.pnlAuthorityNewArea.Visible = false;
            //    this.pnlAuthorityNewSummonsServer.Visible = true;
            //    this.lblSelectNewSummonsServer.Text = string.Format("Select new Summons Server for area {0}", e.Item.Cells[0].Text);
            //    this.BindGrid(false);
            //}
		}

		protected void grdLinkSummonsServer_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
		{
            //this.grdLinkedSummonsServers.CurrentPageIndex = e.NewPageIndex;
            //this.BindGrid(false);
		}

		protected void grdSummonsServers2Link_ItemCommand(object source, DataGridCommandEventArgs e)
		{
            //if (e.CommandName == "Select")
            //{
            //    string areaCode = this.ViewState["NewAreaCode"].ToString();
            //    int ssIntNo = int.Parse(e.Item.Cells[0].Text);

            //    SummonsServerDB ssDB = new SummonsServerDB(Application["constr"].ToString());
            //    ssDB.LinkSummonsServer2AuthorityArea((int)this.ViewState["SelectedAuthIntNo"], ssIntNo, areaCode, loginUser);

            //    this.pnlAuthorityNewSummonsServer.Visible = false;
            //    this.pnlAuthorityLinks.Visible = true;
            //    this.GetLinkedSummonServers();
            //}
		}

		protected void btnSearch4Area_Click(object sender, EventArgs e)
		{
            //string areaCode = this.txtSearchAreaCode.Text.Trim();
            //string areaDescription = this.txtSearchAreaDescription.Text.Trim();

            //AreaCodeDB areaDB = new AreaCodeDB(Application["constr"].ToString());
            //this.dsAreas = areaDB.GetFilteredAreaCodes(areaCode, areaDescription);

            //// Nothing was found by the search
            //if (this.dsAreas.Tables[0].Rows.Count == 0)
            //{
            //    this.lblError.Text = "Nothing was returned by your search.";
            //    this.lblError.Visible = true;
            //    return;
            //}

            //// Bind the results to the grid
            //this.lblError.Visible = false;
            //this.ViewState.Add("AreasDataSet", this.dsAreas);
            //this.grdArea.CurrentPageIndex = 0;
            //this.PopulateAreaFromDataSet();
		}

		protected void btnAddAreaCode_Click(object sender, EventArgs e)
		{
            //bool valid = true;
            //this.lblError.Visible = false;
            //StringBuilder sb = new StringBuilder("You need to fill in the following before the area can be added:\n<ul>\n");

            //string areaCode = this.txtAddAreaCode.Text.Trim();
            //if (areaCode.Length == 0)
            //{
            //    valid = false;
            //    sb.Append("<li>You need to supply the area code</li>\n");
            //}

            //int areaNumber;
            //if (!int.TryParse(areaCode, out areaNumber))
            //{
            //    valid = false;
            //    sb.Append("<li>The area code is not a valid number.</li>\n");
            //}
            //else
            //{
            //    if (areaNumber > 9999)
            //    {
            //        valid = false;
            //        sb.Append("<li>The area code you supplied is too large.</li>\n");
            //    }
            //}
            //string description = this.txtAddAreaDescription.Text.Trim();

            //if (!valid)
            //{
            //    sb.Append("</ul>\n");
            //    this.lblError.Text = sb.ToString();
            //    this.lblError.Visible = true;
            //    return;
            //}

            //AreaCodeDB areaDB = new AreaCodeDB(Application["constr"].ToString());
            //if (areaDB.SaveArea(areaCode, description))
            //{
            //    this.txtAddAreaCode.Text = string.Empty;
            //    this.txtAddAreaDescription.Text = string.Empty;
            //    this.lblError.Text= "Area code successfully added.";
            //    this.PopulateAreaGrid();
            //}
            //else
            //    this.lblError.Text = "The new area could not be saved.";
            //this.lblError.Visible = true;
		}

        #region Auth_SS_Court

        DataSet GetAuthSSCourtList(int autIntNo)
        {
            var paras = new[]
	        {
	            new SqlParameter("@AutIntNo", autIntNo)
	        };

            try
            {
                using (var result = new DataSet())
                {
                    using (var conn = new SqlConnection(connectionString))
                    {
                        using (var cmd = new SqlCommand("AuthSSCourt_GetByAutIntNo", conn) { CommandType = CommandType.StoredProcedure })
                        {
                            cmd.Parameters.AddRange(paras);
                            conn.Open();

                            using (var da = new SqlDataAdapter(cmd))
                            {
                                da.Fill(result);
                            }

                            cmd.Parameters.Clear();
                            return result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

	    void BindGrdAuthSSCourt(int autIntNo, int pageIndex)
	    {
	        this.grdAuthSSCourt.DataSource = autIntNo <= 0 ? null : GetAuthSSCourtList(autIntNo);
	        this.grdAuthSSCourt.PageIndex = pageIndex;
	        this.grdAuthSSCourt.DataBind();
	        this.pnlAuthSSCourtDetail.Visible = false;
	    }

	    void BindDllForAuthSSCourt<TModel, TService, TEntity, TEntityKey>(ListControl ddl, List<TModel> cacheSource, string defaultSelection,
	        Func<TEntity, TModel> newModel,
	        Func<TModel, string> orderBy,
	        Func<TModel, ListItem> listItem,
	        Func<TModel, bool> isSelected,
	        Action<int> bindComplete = null)
	        where TEntityKey : IEntityKey, new()
	        where TEntity : IEntityId<TEntityKey>, new()
	        where TService : ServiceBaseCore<TEntity, TEntityKey>, new()
	    {
	        if (ddl == null
	            || string.IsNullOrWhiteSpace(defaultSelection)
	            || newModel == null
	            || orderBy == null
	            || listItem == null
	            || isSelected == null)
	            return;

	        ddl.Items.Clear();
	        ddl.Items.Add(new ListItem(string.Format("[{0}]", defaultSelection), "0"));

	        var selectedIndex = 0;
	        List<TModel> cacheList;

	        if (cacheSource == null || cacheSource.Count <= 0)
	        {
	            cacheList = new TService().GetAll().Select(newModel).OrderBy(orderBy).ToList();
	            cacheSource = cacheList;
	        }
	        else
	            cacheList = cacheSource;

	        for (var i = 0; i < cacheList.Count; i++)
	        {
	            var current = cacheList[i];
	            ddl.Items.Add(listItem(current));
	            if (isSelected(current))
	                selectedIndex = i + 1;
	        }
	        ddl.SelectedIndex = selectedIndex;

	        if (bindComplete != null)
	        {
	            int id;
	            int.TryParse(ddl.SelectedValue, out id);
	            bindComplete(id);
	        }
	    }

	    void BindDllAuthority(int autIntNo)
	    {
	        BindDllForAuthSSCourt<AuthorityModel, AuthorityService, SIL.AARTO.DAL.Entities.Authority, AuthorityKey>(this.ddlAuthority, cache.Current.AuthList, Convert.ToString(GetLocalResourceObject("PleaseSelectAuthority")),
	            e => new AuthorityModel
	            {
	                AutIntNo = e.AutIntNo,
	                AutCode = e.AutCode,
	                AutName = e.AutName
	            },
	            m => m.AutCode,
	            m => new ListItem(m.AutDescr, m.AutIntNo.ToString()),
	            m => autIntNo == m.AutIntNo,
	            id =>
	            {
	                cache.Current.ASCPageIndex = null;
	                this.btnCreateAuthSSCourt.Enabled = this.ddlAuthority.SelectedIndex >= 1;
	                BindGrdAuthSSCourt(id, 0);
	                this.btnUpdateAuthSSCourt.Enabled = EnableToUpdateAuthSSCourt();
	            });
	    }

        void BindDllCourtDetail(int crtIntNo)
        {
            BindDllForAuthSSCourt<CourtModel, CourtService, SIL.AARTO.DAL.Entities.Court, CourtKey>(this.ddlCourtDetail, cache.Current.CourtList, Convert.ToString(GetLocalResourceObject("PleaseSelectCourt")),
                e => new CourtModel
                {
                    CrtIntNo = e.CrtIntNo,
                    CrtName = e.CrtName
                },
                m => m.CrtName,
                m => new ListItem(m.CrtName, m.CrtIntNo.ToString()),
                m => crtIntNo == m.CrtIntNo,
                id =>
                {
                    BindDllSSDetail(0);
                    this.ddlSSDetail.Enabled = this.ddlCourtDetail.SelectedIndex >= 1;
                    this.btnUpdateAuthSSCourt.Enabled = EnableToUpdateAuthSSCourt();
                });
        }

        void BindDllSSDetail(int ssIntNo)
        {
            BindDllForAuthSSCourt<SummonsServerModel, SummonsServerService, SIL.AARTO.DAL.Entities.SummonsServer, SummonsServerKey>(this.ddlSSDetail, cache.Current.SumServerList, Convert.ToString(GetLocalResourceObject("PleaseSelectSummonsServer")),
                e => new SummonsServerModel
                {
                    SSIntNo = e.SsIntNo,
                    SSFullName = e.SsFullName
                },
                m => m.SSFullName,
                m => new ListItem(m.SSFullName, m.SSIntNo.ToString()),
                m => ssIntNo == m.SSIntNo,
                id => this.btnUpdateAuthSSCourt.Enabled = this.btnUpdateAuthSSCourt.Enabled = EnableToUpdateAuthSSCourt());
        }

	    protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
	    {
	        cache.Current.ASCPageIndex = null;
	        int autIntNo;
	        int.TryParse(this.ddlAuthority.SelectedValue, out autIntNo);
	        BindGrdAuthSSCourt(autIntNo, 0);
	    }

        protected void ddlCourtDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlSSDetail.Enabled = this.ddlCourtDetail.SelectedIndex >= 1;
            this.btnUpdateAuthSSCourt.Enabled = EnableToUpdateAuthSSCourt();
        }

        protected void ddlSSDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnUpdateAuthSSCourt.Enabled = EnableToUpdateAuthSSCourt();
        }

	    protected void grdAuthSSCourt_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("ASCEdit", StringComparison.OrdinalIgnoreCase))
            {
                var ascIntNo = Convert.ToInt32(e.CommandArgument);
                cache.Current.ASCIntNo = ascIntNo;
                GetAuthSSCourtDetail(ascIntNo);
            }
            else if (e.CommandName.Equals("ASCDelete", StringComparison.OrdinalIgnoreCase))
            {
                DeleteAuthSSCourt(Convert.ToInt32(e.CommandArgument));
            }
        }

        protected void grdAuthSSCourt_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            cache.Current.ASCPageIndex = e.NewPageIndex;
            int autIntNo;
            int.TryParse(this.ddlAuthority.SelectedValue, out autIntNo);
            BindGrdAuthSSCourt(autIntNo, e.NewPageIndex);
        }

        protected void btnCreateAuthSSCourt_Click(object sender, EventArgs e)
        {
            cache.Current.ASCIntNo = null;
            GetAuthSSCourtDetail(0);
        }

        bool EnableToUpdateAuthSSCourt()
        {
            return this.ddlAuthority.SelectedIndex >= 1
                && this.ddlCourtDetail.SelectedIndex >= 1
                && this.ddlSSDetail.SelectedIndex >= 1;
        }

        void SetASCMessage(string message)
        {
            this.lblASCError.Text = message;
            this.lblASCError.Visible = !string.IsNullOrWhiteSpace(message);
        }

	    void GetAuthSSCourtDetail(int ascIntNo)
	    {
	        SetASCMessage(null);
	        this.btnUpdateAuthSSCourt.Text = ascIntNo > 0
	            ? Convert.ToString(GetLocalResourceObject("btnSaveSummonsServer.Text"))
	            : Convert.ToString(GetLocalResourceObject("btnSaveSummonsServer.Text1"));

	        var ascEntity = ascIntNo > 0 ? ascService.GetByAscIntNo(ascIntNo) : new AuthSsCourt();
	        BindDllCourtDetail(ascEntity.CrtIntNo);
	        BindDllSSDetail(ascEntity.SsIntNo);
	        this.pnlAuthSSCourtDetail.Visible = true;
	    }

	    void UpdateAuthSSCourt(int ascIntNo)
	    {
	        var isValid = true;
	        var sb = new StringBuilder();

	        int autIntNo, crtIntNo, ssIntNo;
	        int.TryParse(this.ddlAuthority.SelectedValue, out autIntNo);
	        int.TryParse(this.ddlCourtDetail.SelectedValue, out crtIntNo);
	        int.TryParse(this.ddlSSDetail.SelectedValue, out ssIntNo);

	        if (autIntNo <= 0)
	        {
	            sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("PleaseSelectAuthority"), Environment.NewLine);
	            isValid = false;
	        }

	        if (crtIntNo <= 0)
	        {
	            sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("PleaseSelectCourt"), Environment.NewLine);
	            isValid = false;
	        }

	        if (ssIntNo <= 0)
	        {
	            sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("PleaseSelectSummonsServer"), Environment.NewLine);
	            isValid = false;
	        }

	        AuthSsCourt ascEntity;
	        if ((ascEntity = ascService.GetByAutIntNoSsIntNoCrtIntNo(autIntNo, ssIntNo, crtIntNo)) != null
	            && ascEntity.AscIntNo != ascIntNo)
	        {
	            sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("ThisAssociationAlreadyExisted"), Environment.NewLine);
	            isValid = false;
	        }

	        if (!isValid)
	        {
	            SetASCMessage(sb.ToString());
	            return;
	        }

	        ascEntity = ascIntNo > 0 ? ascService.GetByAscIntNo(ascIntNo) : new AuthSsCourt();
	        ascEntity.AutIntNo = autIntNo;
	        ascEntity.CrtIntNo = crtIntNo;
	        ascEntity.SsIntNo = ssIntNo;
	        ascEntity.EntityState = ascIntNo > 0 ? EntityState.Changed : EntityState.Added;

	        try
	        {
	            ascService.Save(ascEntity);
	            BindGrdAuthSSCourt(autIntNo, cache.Current.ASCPageIndex.GetValueOrDefault());
                if (ascIntNo > 0)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerMaintenance, PunchAction.Change);
                }
                else
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerMaintenance, PunchAction.Add);
                }
	        }
	        catch (Exception ex)
	        {
	            sb.AppendFormat("<li>{0}</li>{1}", ex.Message, Environment.NewLine);
	            SetASCMessage(sb.ToString());
	        }
	    }

	    void DeleteAuthSSCourt(int ascIntNo)
        {
            if (ascIntNo <= 0) return;
            if (ascService.Delete(ascService.GetByAscIntNo(ascIntNo)))
            {
                int autIntNo;
                int.TryParse(this.ddlAuthority.SelectedValue, out autIntNo);
                BindGrdAuthSSCourt(autIntNo, cache.Current.ASCPageIndex.GetValueOrDefault());
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerMaintenance, PunchAction.Delete);  
            }
        }

        protected void btnUpdateAuthSSCourt_Click(object sender, EventArgs e)
        {
            UpdateAuthSSCourt(cache.Current.ASCIntNo.GetValueOrDefault());
        }

        #endregion

    }
}
