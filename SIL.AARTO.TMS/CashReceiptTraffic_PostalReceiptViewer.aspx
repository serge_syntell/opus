<%@ Page Language="c#"
    AutoEventWireup="false" Inherits="Stalberg.TMS.CashReceiptTraffic_PostalReceiptViewer" Codebehind="CashReceiptTraffic_PostalReceiptViewer.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body onload="self.focus();" style="margin: 0px" background="<%=backgroundImage %>" >
    <form id="Form1" runat="server">
    </form>
</body>
</html>
