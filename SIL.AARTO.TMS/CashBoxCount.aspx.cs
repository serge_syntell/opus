using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;

namespace Stalberg.TMS
{
    /// <summary>
    /// The starting point for ticket payment receipt
    /// </summary>
    public partial class CashBoxCount : System.Web.UI.Page
    {
        // Constants
        private const string CURRENCY_FORMAT = "####";

        // Fields
        private string _connectionString = String.Empty;
        private string _login;
        private int _autIntNo;

        protected string styleSheet;
        protected string backgroundImage;
        protected double myTotal = 0;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "CashBoxCount.aspx";
        //protected string thisPage = "Cash Receipts: Confirm Cash Box Float";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this._connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            _login = userDetails.UserLoginName;
            _autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;
 
            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // FBJ Added (2006-09-26): Check to see if the current user is a valid cashier
            BankAccountDB bankAccount = new BankAccountDB(_connectionString);
            Cashier cashier = bankAccount.GetUserBankAccount(int.Parse(Session["userIntNo"].ToString()));
            if (cashier == null)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            Session.Add("BAIntNo", cashier.BAIntNo);
            Session.Add("CashBoxUserIntNo", cashier.CashBoxUserIntNo);
            this.ViewState.Add("UserIntNo", cashier.UserIntNo);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlCashBox.Visible = true;
                this.pnlConfirm.Visible = false;
                this.pnlFloat.Visible = false;

                BindGrdCashBox(true);   //2013-04-09 add by Henry for pagination
            }
        }

        //2013-04-09 add by Henry for pagination
        private void BindGrdCashBox(bool isReload)
        {
            if (isReload)
            {
                grdCashBoxPager.RecordCount = 0;
                grdCashBoxPager.CurrentPageIndex = 1;
            }

            Stalberg.TMS.CashboxDB cashbox = new Stalberg.TMS.CashboxDB(_connectionString);
            int totalCount = 0;
            DataSet ds = cashbox.GetCashboxListDS(this._autIntNo, false, grdCashBoxPager.PageSize, grdCashBoxPager.CurrentPageIndex, out totalCount);
            this.grdCashBox.DataSource = ds;
            this.grdCashBox.DataKeyNames = new string[] { "CBIntNo" };
            this.grdCashBox.DataBind();

            grdCashBoxPager.RecordCount = totalCount;

            if (ds.Tables[0].Rows.Count == 0)
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void grdCashBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pnlCashBox.Visible = false;
            this.pnlFloat.Visible = true;

            int cbIntNo = (int)this.grdCashBox.DataKeys[this.grdCashBox.SelectedIndex].Value;
            Session.Add("CBIntNo", cbIntNo);

            CashboxType cashboxType = (CashboxType)Enum.Parse(typeof(CashboxType), this.rblCashboxType.SelectedValue);
            this.Session.Add("CashboxType", cashboxType);

            Stalberg.TMS.CashboxDB cashbox = new Stalberg.TMS.CashboxDB(this._connectionString);
            Stalberg.TMS.CashboxDetails cbDetails = cashbox.GetCashboxDetails(cbIntNo);

            lblCashboxName.Text = cbDetails.CBName;
            lblBalance.Text = cbDetails.CBStartAmount.ToString("R #,##0");
            hid500.Value = cbDetails.CBRand500s.ToString(CURRENCY_FORMAT);
            hid200.Value = cbDetails.CBRand200s.ToString(CURRENCY_FORMAT);
            hid100.Value = cbDetails.CBRand100s.ToString(CURRENCY_FORMAT);
            hid50.Value = cbDetails.CBRand50s.ToString(CURRENCY_FORMAT);
            hid20.Value = cbDetails.CBRand20s.ToString(CURRENCY_FORMAT);
            hid10.Value = cbDetails.CBRand10s.ToString(CURRENCY_FORMAT);
            hid5.Value = cbDetails.CBRand5s.ToString(CURRENCY_FORMAT);
            hid2.Value = cbDetails.CBRand2s.ToString(CURRENCY_FORMAT);
            hid1.Value = cbDetails.CBRand1s.ToString(CURRENCY_FORMAT);
        }

        private bool ConfirmDenomination(TextBox txt, HiddenField hid, ref int total, StringBuilder sb, int denomination)
        {
            bool response = true;
            int temp = 0;

            // Check if there's a value in the text box
            string tempString = txt.Text.Trim();
            if (tempString.Length > 0)
            {
                if (int.TryParse(tempString, out temp))
                    total += (temp * denomination);
                else
                {
                    sb.Append(string.Format("<li>" + (string)GetLocalResourceObject("lblError.Text2") + "</li>\n", denomination, tempString));
                    response = false;
                }
            }

            // Compare the value to the 
            if (hid.Value.Length > 0)
            {
                int hiddenValue = int.Parse(hid.Value);
                if (hiddenValue != temp)
                {
                    sb.Append(string.Format("<li>" + (string)GetLocalResourceObject("lblError.Text3") + "</li>\n", denomination, temp, hiddenValue));
                    response = false;
                }
            }

            return response;
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            this.pnlConfirm.Visible = true;
            this.btnCashFloatConfirmed.Visible = false;

            StringBuilder sb = new StringBuilder("<p>" + (string)GetLocalResourceObject("lblError.Text4") + "\n<ul>");
            bool hasError = false;
            int runningTotal = 0;

            if (!this.ConfirmDenomination(this.txt500, hid500, ref runningTotal, sb, 500))
                hasError = true;

            if (!this.ConfirmDenomination(this.txt200, hid200, ref runningTotal, sb, 200))
                hasError = true;

            if (!this.ConfirmDenomination(this.txt100, hid100, ref runningTotal, sb, 100))
                hasError = true;

            if (!this.ConfirmDenomination(this.txt50, hid50, ref runningTotal, sb, 50))
                hasError = true;

            if (!this.ConfirmDenomination(this.txt20, hid20, ref runningTotal, sb, 20))
                hasError = true;

            if (!this.ConfirmDenomination(this.txt10, hid10, ref runningTotal, sb, 10))
                hasError = true;

            if (!this.ConfirmDenomination(this.txt5, hid5, ref runningTotal, sb, 5))
                hasError = true;

            if (!this.ConfirmDenomination(this.txt2, hid2, ref runningTotal, sb, 2))
                hasError = true;

            if (!this.ConfirmDenomination(this.txt1, hid1, ref runningTotal, sb, 1))
                hasError = true;

            // Check the totals agree
            int total = int.Parse(lblBalance.Text.Substring(1, lblBalance.Text.Length - 1).Trim(), NumberStyles.Any);
            if (total != runningTotal)
            {
                hasError = true;
                sb.Append(string.Format("<li>" + (string)GetLocalResourceObject("lblError.Text5") + "</li>\n", runningTotal, total));
            }

            // Halt processing and display errors if there are any
            if (hasError)
            {
                sb.Append("</ul>\n" + (string)GetLocalResourceObject("lblError.Text6") + "</p>\n");
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return;
            }

            // Proceed to final confirmation if all is OK
            this.pnlFloat.Visible = false;
            this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text7") + "<br />", (CashboxType)this.Session["CashboxType"]);

            this.btnCashFloatConfirmed.Visible = true;
        }

        protected void btnCashFloatConfirmed_Click(object sender, EventArgs e)
        {
            // Set the user's current cash box, clear the check flag, and redirect back to the payment page
            Stalberg.TMS.CashboxDB cashbox = new Stalberg.TMS.CashboxDB(this._connectionString);
            cashbox.ConfirmCashboxFloat(int.Parse(this.ViewState["UserIntNo"].ToString()),
                int.Parse(Session["CBIntNo"].ToString()),
                this._login,
                //int.Parse(Session["BAIntNo"].ToString()),
                int.Parse(Session["CashBoxUserIntNo"].ToString()),
                (CashboxType)this.Session["CashboxType"]);

            Response.Redirect("CashReceiptTraffic.aspx");
        }

        //2013-04-09 add by Henry for pagination
        protected void grdCashBoxPager_PageChanged(object sender, EventArgs e)
        {
            BindGrdCashBox(false);
        }

    }
}
