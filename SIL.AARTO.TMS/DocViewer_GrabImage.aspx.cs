using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing.Imaging;
using System.IO;

namespace Stalberg.TMS
{
	/// <summary>
	/// Summary description for GrabImage.
	/// </summary>
	public partial class GrabImage : System.Web.UI.Page
	{
        protected string connectionString = string.Empty;

        protected void Page_Load(object sender, System.EventArgs e)
		{
            connectionString = Application["constr"].ToString();

			int frame = 0;
			if (Request.QueryString["frame"] != null)
				frame = int.Parse(Request.QueryString["frame"]);
			else
                throw new ArgumentException((string)GetLocalResourceObject("strException"), "frame");
            int rdIntNo = 0;
            if (Request.QueryString["rdIntNo"] != null)
                rdIntNo = int.Parse(Request.QueryString["rdIntNo"]);
            else
                throw new ArgumentException((string)GetLocalResourceObject("strException1"), "rdIntNo");
            string tableName = string.Empty;
            if (Request.QueryString["tableName"] != null)
                tableName = Request.QueryString["tableName"];
            else
                throw new ArgumentException((string)GetLocalResourceObject("strException2"), "tableName");
            int width = 120;
            if (Request.QueryString["width"] != null)
                width = int.Parse(Request.QueryString["width"]);
            int height = 120;
            if (Request.QueryString["height"] != null)
                height = int.Parse(Request.QueryString["height"]);
            
            AccessDBMethods dbMethods = new AccessDBMethods(connectionString);

            MemoryStream memorySteam = new MemoryStream(dbMethods.DownLoadBLOB(rdIntNo, "RDIntNo", tableName, "RDImage"));
            Bitmap dbBitmap = (Bitmap)System.Drawing.Image.FromStream(memorySteam);

            System.Drawing.Image image = GetFrameImage(dbBitmap, frame);
            Bitmap bitmap = BestFit(image, width, height);
            Response.ContentType = "image/jpeg";
            bitmap.Save(Response.OutputStream, ImageFormat.Jpeg);
		}

        private System.Drawing.Image GetFrameImage(Bitmap bitmap, int frame)
        {
            bitmap.SelectActiveFrame(FrameDimension.Page, frame);
            MemoryStream byteStream = new MemoryStream();
            bitmap.Save(byteStream, ImageFormat.Jpeg);
            System.Drawing.Image image = System.Drawing.Image.FromStream(byteStream);
            return image;
        }
        private Bitmap BestFit(System.Drawing.Image image, int width, int height)
        {
            double largestRatio = Math.Max((double)image.Width / width, (double)image.Height / height);
            Bitmap bitmap = new Bitmap(Convert.ToInt32(Math.Floor(image.Width / largestRatio)), Convert.ToInt32(Math.Floor(image.Height / largestRatio)));
            Rectangle imageArea = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            Graphics g = Graphics.FromImage(bitmap);
            g.DrawImage(image, imageArea, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
            g.Dispose();

            return bitmap;
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
