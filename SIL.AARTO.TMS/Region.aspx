<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>


<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Region" Codebehind="Region.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:DataGrid ID="dgRegion" Width="495px" runat="server" BorderColor="Black" AllowPaging="True"
                                    GridLines="Vertical" CellPadding="4" Font-Size="8pt" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                    FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                    AutoGenerateColumns="False" OnItemCommand="dgRegion_ItemCommand" OnPageIndexChanged="dgRegion_PageIndexChanged">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="RegIntNo" HeaderText="RegIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RegCode" HeaderText="<%$Resources:dgRegion.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RegName" HeaderText="<%$Resources:dgRegion.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgRegionItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <asp:Panel ID="pnlAddRegion" runat="server" Height="127px">
                                    <table id="Table2" cellspacing="1" cellpadding="1" width="735" border="0">
                                        <tr>
                                            <td width="134" style="height: 1px">
                                                <asp:Label ID="lblAddRegion" runat="server" Width="120px" CssClass="ProductListHead" Text="<%$Resources:lblAddRegion.Text %>"></asp:Label></td>
                                            <td width="294" style="height: 1px">
                                            </td>
                                            <td style="height: 1px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="134" style="height: 1px">
                                                <asp:Label ID="lblAddRegionCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddRegionCode.Text %>"></asp:Label></td>
                                            <td width="294" style="height: 1px">
                                                <asp:TextBox ID="txtAddRegionCode" runat="server" Width="50px" CssClass="NormalMand"
                                                    MaxLength="3" Height="24px"></asp:TextBox></td>
                                            <td style="height: 1px">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    ControlToValidate="txtAddRegionCode"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="134" style="height: 2px">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddRegionName.Text %>"></asp:Label></td>
                                            <td width="294" style="height: 2px">
                                                <asp:TextBox ID="txtAddRegionName" runat="server" Width="242px" CssClass="NormalMand"
                                                    MaxLength="100" Height="24px"></asp:TextBox></td>
                                            <td style="height: 2px">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Width="226px"
                                                    CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqName.ErrorMessage %>"
                                                    ControlToValidate="txtAddRegionName"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td width="134" style="height: 1px">
                                                <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddRegionTel.Text %>"></asp:Label></td>
                                            <td width="294" style="height: 1px">
                                                <asp:TextBox ID="txtAddRegionTel" runat="server" Width="122px" CssClass="Normal"
                                                    MaxLength="30" Height="24px"></asp:TextBox></td>
                                            <td style="height: 1px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="134" style="height: 26px">
                                            </td>
                                            <td width="294" style="height: 26px">
                                            </td>
                                            <td style="height: 26px">
                                                <asp:Button ID="btnAddRegion" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddRegion %>"
                                                    OnClick="btnAddRegion_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateRegion" runat="server" Height="127px">
                                    <table id="Table3" cellspacing="1" cellpadding="1" width="734" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label19" runat="server" Width="158px" CssClass="ProductListHead" Text="<%$Resources:lblUpdRegion.Text %>"></asp:Label></td>
                                            <td height="2" style="width: 295px">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="2">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddRegionCode.Text %>"></asp:Label></td>
                                            <td height="2" style="width: 295px">
                                                <asp:TextBox ID="txtRegionCode" runat="server" Width="41px" CssClass="NormalMand"
                                                    MaxLength="5" Height="24px"></asp:TextBox></td>
                                            <td height="2">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    ControlToValidate="txtRegionCode"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddRegionName.Text %>"></asp:Label></td>
                                            <td valign="top" height="25" style="width: 295px">
                                                <asp:TextBox ID="txtRegionName" runat="server" Width="228px" CssClass="NormalMand"
                                                    MaxLength="100" Height="24px"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Width="210px"
                                                    CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqName.ErrorMessage %>"
                                                    ControlToValidate="txtRegionName"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td valign="top" width="157" style="height: 12px">
                                                <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddRegionTel.Text %>"></asp:Label></td>
                                            <td valign="top" style="height: 12px; width: 295px;">
                                                <asp:TextBox ID="txtRegionTel" runat="server" Width="122px" CssClass="Normal" MaxLength="30"
                                                    Height="24px"></asp:TextBox></td>
                                            <td style="height: 12px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" style="height: 8px">
                                            </td>
                                            <td style="height: 8px; width: 295px;">
                                            </td>
                                            <td style="height: 8px">
                                                <asp:Button ID="btnUpdateRegion" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdateRegion.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
        </ContentTemplate></asp:UpdatePanel>
    </form>
</body>
</html>
