<%@ Application Language="C#" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup 

        string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
        Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);
       
        // Set path for bin
        string mPath = Server.MapPath("bin");

        Application.Lock();
        Application.Add("binpath", mPath);

        Application["lastUpdated"] = SIL.AARTO.BLL.Utility.ProjectLastUpdated.TMS.ToShortDateString();

        string conStr = System.Configuration.ConfigurationManager.ConnectionStrings["TMSConnectionString"].ConnectionString; 

        Application["constr"] = conStr;

        Application["smtp"] = System.Configuration.ConfigurationManager.AppSettings["smtp"].ToString(); 

        Application.UnLock();
        
        // Add the DynamicPDF License
        ceTe.DynamicPDF.Document.AddLicense("DPS70NPDFJJGEJFZ9ikRGHBua2M3Sl0Pwtw63QxBds5agxfAQMSTfCfEtiI2O6I13jHEsL6smMhYn63QJBLCZ3B8XtMbaTRJ+C1w");

        string specificCulture = System.Configuration.ConfigurationManager.AppSettings["specificCulture"].ToString();
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(specificCulture); 

    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
        Application.Contents.Clear();

    }

    void Application_Error(object sender, EventArgs e)
    {
        Exception LastError = Server.GetLastError();
        if (LastError != null)
        {
            Application["ErrorMessage"] = LastError.GetBaseException().Message;
            SIL.AARTO.BLL.EntLib.EntLibLogger.WriteErrorLog(LastError.GetBaseException(), SIL.AARTO.BLL.EntLib.LogCategory.Exception, SIL.AARTO.DAL.Entities.AartoProjectList.TMS.ToString());
        }
    }

    void Application_BeginRequest(Object sender, EventArgs e)
    {
        //string lsCode = string.IsNullOrEmpty(Request.QueryString["LsCode"]) ? SIL.AARTO.BLL.Culture.Config.DefaultLanguage : Request.QueryString["LsCode"];
        SIL.AARTO.BLL.Culture.Config.ProcessMultiLanguage(Request);
        //System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("ms-MY");
        //System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;

        // 2015-01-15, Oscar added (bontq 1771, ref 1770)
        SIL.AARTO.BLL.Culture.Config.ResetCultureNumberFormat();
    }

    /********** Session Object ******************/

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
        Session.Contents.Clear();
    }

    void Session_End(object sender, EventArgs e)
    {
        ////dls 20120601 - mvoed to Home.cs in AARTO.WEB
        //// Code that runs when a session ends. 
        //// Note: The Session_End event is raised only when the session state mode
        //// is set to InProc in the Web.config file. If session mode is set to StateServer 
        //// or SQLServer, the event is not raised.

        //int userIntNo = Convert.ToInt32(Session["userIntNo"]);
        //int nUSIntNo = Convert.ToInt32(Session["USIntNo"]);
        //string conStr = Application["constr"].ToString();

        //// LMZ 14-03-2007 - used to logout on UserShift table
        //Stalberg.TMS.UserDB db = new Stalberg.TMS.UserDB(conStr);
        //nUSIntNo = db.UserShiftEdit(nUSIntNo, "", "Logout");
        //Stalberg.TMS.UserDetails ud = (Stalberg.TMS.UserDetails)Session["userDetails"];
        //Stalberg.TMS.FilmDB film = new Stalberg.TMS.FilmDB(conStr);
        //film.LockUser(0, ud.UserLoginName);

        ////if (Application["ConfirmFilm"] != null)
        ////    this.ClearConfirmFilmList(userIntNo);

        Session.Contents.Clear();
    }
       
</script>

