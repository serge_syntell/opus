using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for MainViewer.
    /// </summary>
    public partial class MainViewer : System.Web.UI.Page
    {

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string notIntNo = string.Empty;
                string image = string.Empty;
                int counter = 1;
                foreach (string var in Request.QueryString)
                {
                    if (counter == 1)
                    {
                        notIntNo = Request.QueryString[var];
                    }
                    else
                    {
                        image = Request.QueryString[var];
                    }
                    counter += 1;
                }
                mainImage.ImageUrl = "~/Image.aspx?NotIntNo=" + HttpUtility.UrlEncode(notIntNo)
                + "&ImageColumn=" + HttpUtility.UrlEncode(image)
                + "&Width=" + HttpUtility.UrlEncode(Convert.ToString(626))
                + "&Height=" + HttpUtility.UrlEncode(Convert.ToString(442));
     

                
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

    }
}
