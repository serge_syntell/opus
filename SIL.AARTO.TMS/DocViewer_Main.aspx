<%@ Page language="c#" Inherits="Stalberg.TMS.DocViewer" Codebehind="DocViewer_Main.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Documents</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" style="font-family:Verdana; font-size:x-small;">
                <tr>
                    <td style="border: solid 1px black;">
                        <asp:Label ID="lbTitle" runat="server" Text="<%$Resources:lbTitle.Text %>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="border-top: none none none; border-right: solid 1px black; border-bottom: solid 1px black; border-left: solid 1px black; ">
                         <div id="pre1ImageDiv" style="width:677px; line-height:771px; overflow:hidden; position:relative; text-align:center;">
                            <asp:Image ID="mainImage" runat="server" ImageUrl="~/DocViewer_GrabImage.aspx" style="vertical-align:middle;" />
                        </div>
                    </td>
                </tr>               
            </table>
			
		</form>
	</body>
</HTML>
