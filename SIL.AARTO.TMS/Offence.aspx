<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Offence" CodeBehind="Offence.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script src="~/Scripts/Jquery/jquery-1.8.3.js"></script>
    <script src="Scripts/Jquery/jquery-1.3.2-vsdoc2.js"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#chkAddS35NoAoG").change(function () {
                if ($("#chkAddS35NoAoG").attr("checked")) {
                    $("#chkAddOffNoAOG").attr("checked", true);
                }
            });

            $("#chkAddOffNoAOG").change(function () {
                if ($("#chkAddS35NoAoG").attr("checked")) {
                    $("#chkAddOffNoAOG").attr("checked", true);
                }
            });

            $("#chkS35NoAoG").change(function () {
                if ($("#chkS35NoAoG").attr("checked")) {
                    $("#chkOffNoAOG").attr("checked", true);
                }
            });

            $("#chkOffNoAOG").change(function () {
                if ($("#chkS35NoAoG").attr("checked")) {
                    $("#chkOffNoAOG").attr("checked", true);
                }
            });
        })

    </script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table id="Table1" height="30" cellspacing="1" cellpadding="1" width="542" border="0">
                                        <tr>
                                            <td width="162"></td>
                                            <td width="7"></td>
                                        </tr>
                                        <tr>
                                            <td width="162" height="15">
                                                <asp:Label ID="Label5" runat="server" Width="163px" CssClass="NormalBold" Text="<%$Resources:lblSelOffenceGroup.Text %>"></asp:Label></td>
                                            <td width="7" height="15">
                                                <asp:DropDownList ID="ddlSelOffenceGroup" runat="server" Width="562px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelOffenceGroup_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelection" runat="server" Width="142px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label></td>
                                            <td width="7" valign="top">
                                                <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="10"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td width="162"></td>
                                            <td width="7">
                                                <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                    OnClick="btnSearch_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgOffence" Width="495px" runat="server" BorderColor="Black" AllowPaging="False"
                                    GridLines="Vertical" CellPadding="4" Font-Size="8pt" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                    FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                    AutoGenerateColumns="False" OnItemCommand="dgOffence_ItemCommand">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="OffIntNo" HeaderText="OffIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="OffCode" HeaderText="<%$Resources:dgOffence.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="OffDescr" HeaderText="<%$Resources:dgOffence.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="OffRangeStart" HeaderText="<%$Resources:dgOffence.HeaderText3 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="OffRangeEnd" HeaderText="<%$Resources:dgOffence.HeaderText4 %>"></asp:BoundColumn>
                                        <%--<asp:BoundColumn DataField="OffIsByLaw" HeaderText="<%$Resources:ChkIsByLaw %>"></asp:BoundColumn>--%>
                                        <asp:ButtonColumn Text="<%$Resources:dgOffenceItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <pager:AspNetPager id="OffencePager" runat="server" 
                                    showcustominfosection="Right" width="495px" 
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                      FirstPageText="|&amp;lt;" 
                                    LastPageText="&amp;gt;|" 
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                    CustomInfoStyle="float:right;" onpagechanged="OffencePager_PageChanged"
                                     ></pager:AspNetPager>
                                    
                                <asp:Panel ID="pnlAddOffence" runat="server" Width="670px" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAddOffence" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddOffence.Text %>"></asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="lblAddOffCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddOffCode.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtAddOffCode" runat="server" Width="83px" CssClass="NormalMand"
                                                    MaxLength="6"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="266px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    ControlToValidate="txtAddOffCode" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOffDescr.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtAddOffDescr" runat="server" Width="497px"
                                                    CssClass="NormalMand" Height="66px" TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                        <td height="100">
                                                            <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                        <td height="100">
                                                            <uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 24px">
                                                <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddOffenceGroup.Text %>"></asp:Label></td>
                                            <td style="height: 24px">
                                                <asp:DropDownList ID="ddlAddOffenceGroup" runat="server" Width="500px"
                                                    CssClass="Normal" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlAddOffenceGroup_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label11" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddOffRangeStart.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtAddOffRangeStart" runat="server" Width="42px" CssClass="NormalMand"
                                                    MaxLength="3"></asp:TextBox>
                                                <asp:RangeValidator ID="Rangevalidator1" runat="server" CssClass="NormalRed" ErrorMessage="<%$Resources:RanStart.ErrorMessage %>"
                                                    ControlToValidate="txtAddOffRangeStart" ForeColor=" " MaximumValue="200" MinimumValue="0"
                                                    Type="Integer"></asp:RangeValidator>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" Width="256px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqStart.ErrorMessage %>"
                                                    ControlToValidate="txtAddOffRangeStart" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label12" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblOffRangeEnd.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtAddOffRangeEnd" runat="server" Width="43px" CssClass="NormalMand"
                                                    MaxLength="3"></asp:TextBox>
                                                <asp:RangeValidator ID="Rangevalidator2" runat="server" CssClass="NormalRed" ErrorMessage="<%$Resources:RanStart.ErrorMessage %>"
                                                    ControlToValidate="txtAddOffRangeEnd" ForeColor=" " MaximumValue="999" MinimumValue="0"
                                                    Type="Integer"></asp:RangeValidator>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Width="255px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqStart.ErrorMessage %>"
                                                    ControlToValidate="txtAddOffRangeEnd" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top"></td>
                                            <td>
                                                <asp:CheckBox ID="chkAddS35NoAoG" ClientIDMode="Static" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddOffS35NoAOG.Text%>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td ></td>
                                            <td>
                                                <asp:CheckBox ID="chkAddOffNoAOG" ClientIDMode="Static" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddOffNoAOG.Text %>" /></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CheckBox ID="chkAddOffHasMandatoryAlternate" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddOffHasMandatoryAlternate.Text %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CheckBox ID="chkIsPOD" runat="server" CssClass="NormalBold" Checked="False" Text="<%$Resources:IsPresentationOfDocument %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CheckBox ID="chkIsByLaw" runat="server" CssClass="NormalBold" Checked="False" Text="<%$Resources:ChkIsByLaw %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label7" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddOffenceCodeList.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:DropDownList ID="ddlAddOffenceCodeList" runat="server" Width="500px" CssClass="Normal">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                        <%--Oscar 20130109 added--%>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label14" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:LongDescriptionEng %>" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLongDescEng" runat="server" Width="496px" CssClass="Normal" Height="65px" TextMode="MultiLine" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label15" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:LongDescriptionAfr %>" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLongDescAfr" runat="server" Width="496px" CssClass="Normal" Height="65px" TextMode="MultiLine" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnAddOffence" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddOffence.Text %>"
                                                    OnClick="btnAddOffence_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>

                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateOffence" runat="server" Width="665px" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label19" runat="server" Width="162px" CssClass="ProductListHead" Text="<%$Resources:lblUpdate.Text %>"></asp:Label></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddOffCode.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtOffCode" runat="server" Width="83px" CssClass="NormalMand" MaxLength="6"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="271px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    ControlToValidate="txtOffCode" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOffDescr.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtOffDescr" runat="server" Width="496px"
                                                    CssClass="NormalMand" Height="65px" TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                        <td height="100">
                                                            <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                        <td height="100">
                                                            <uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label8" runat="server" Width="163px" CssClass="NormalBold" Text="<%$Resources:lblAddOffenceGroup.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:DropDownList ID="ddlOffenceGroup" runat="server" Width="500px"
                                                    CssClass="Normal" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlOffenceGroup_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label20" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddOffRangeStart.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtOffRangeStart" runat="server" Width="42px" CssClass="NormalMand"
                                                    MaxLength="3"></asp:TextBox>
                                                <asp:RangeValidator ID="Rangevalidator6" runat="server" CssClass="NormalRed" ErrorMessage="<%$Resources:RanStart.ErrorMessage %>"
                                                    ControlToValidate="txtOffRangeStart" ForeColor=" " MaximumValue="999" MinimumValue="0"
                                                    Type="Integer"></asp:RangeValidator>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" Width="260px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqStart.ErrorMessage %>"
                                                    ControlToValidate="txtOffRangeStart" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label21" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblOffRangeEnd.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtOffRangeEnd" runat="server" CssClass="NormalMand" MaxLength="3"
                                                    Width="43px"></asp:TextBox>
                                                <asp:RangeValidator ID="Rangevalidator5" runat="server" CssClass="NormalRed" ErrorMessage="<%$Resources:RanEnd.ErrorMessage %>"
                                                    ControlToValidate="txtOffRangeEnd" ForeColor=" " MaximumValue="999" MinimumValue="0"
                                                    Type="Integer"></asp:RangeValidator>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" Width="267px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqStart.ErrorMessage %>"
                                                    ControlToValidate="txtOffRangeEnd" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top"></td>
                                            <td>
                                                <asp:CheckBox ID="chkS35NoAoG" ClientIDMode="Static" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddOffS35NoAOG.Text%>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CheckBox ID="chkOffNoAOG" ClientIDMode="Static" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddOffNoAOG.Text %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CheckBox ID="chkOffHasMandatoryAlternate" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddOffHasMandatoryAlternate.Text %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CheckBox ID="chkIsPODUpdate" runat="server" CssClass="NormalBold" Checked="False" Text="<%$Resources:IsPresentationOfDocument %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CheckBox ID="chkIsByLawUpdate" runat="server" CssClass="NormalBold" Checked="False" Text="<%$Resources:ChkIsByLaw %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label6" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddOffenceCodeList.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:DropDownList ID="ddlOffenceCodeList" runat="server" Width="500px" CssClass="Normal">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                        <%--Oscar 20130109 added--%>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label10" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:LongDescriptionEng %>" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLongDescEngUpdate" runat="server" Width="496px" CssClass="Normal" Height="65px" TextMode="MultiLine" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label13" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:LongDescriptionAfr %>" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLongDescAfrUpdate" runat="server" Width="496px" CssClass="Normal" Height="65px" TextMode="MultiLine" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="height: 27px"></td>
                                            <td style="height: 27px">
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
