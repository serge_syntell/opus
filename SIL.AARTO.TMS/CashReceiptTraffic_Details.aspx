<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.CashReceiptTraffic_Details"
    CodeBehind="CashReceiptTraffic_Details.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="RepresentationOnTheFly.ascx" TagName="RepresentationOnTheFly" TagPrefix="uc1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script language="javascript" type="text/javascript">
        function SetHiddenValue() {
            document.getElementById("hidPayClick").value = "1";
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="6000">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" id="TABLE1" onclick="return TABLE1_onclick()">
        <tr>
            <td align="left" width="100%" valign="middle">
            </td>
        </tr>
    </table>
    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center" style="width: 180px;">
                <img style="height: 1px" src="images/1x1.gif" width="1" />
                &nbsp;
                <asp:Panel ID="pnlMainMenu" runat="server">
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="140px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 138px">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Button ID="btnAddNotices" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$Resources:btnAddNotices.Text %>" OnClick="btnAddNotices_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Button ID="btnRecon" runat="server" Width="135px" CssClass="NormalButton" OnClick="btnRecon_Click"
                                    Text="<%$Resources:btnRecon.Text %>" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" style="text-align: center;" class="Normal">
                <!-- Start Content -->
                <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Width="610px" Text="<%$Resources:lblPageName.Text %>"></asp:Label>&nbsp;
                <br />
                <asp:UpdatePanel ID="udpRevceipting" runat="server">
                    <ContentTemplate>
                        &nbsp;<br />
                        <asp:Panel ID="panelDetails" runat="server" HorizontalAlign="Left">
                            <asp:Label ID="lblDeferredError" runat="server" CssClass="NormalRed" Width="100%"></asp:Label><br />
                            <br />
                            &nbsp;&nbsp;<asp:Label ID="lblRepresentations" runat="server" CssClass="NormalRed"
                                Text="<%$Resources:lblRepresentations.Text %>" Visible="False" Width="1058px"></asp:Label><br />
                            <br />
                            <table border="0">
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgTicketDetails" runat="server" AlternatingItemStyle-CssClass="CartListItemAlt"
                                            AutoGenerateColumns="False" BorderColor="Black" CellPadding="4" Font-Size="8pt"
                                            FooterStyle-CssClass="cartlistfooter" GridLines="Vertical" HeaderStyle-CssClass="CartListHead"
                                            ItemStyle-CssClass="CartListItem" ShowFooter="True" CssClass="Normal" Width="100%"
                                            OnItemCreated="dgTicketDetails_ItemCreated" OnSelectedIndexChanged="dgTicketDetails_SelectedIndexChanged"
                                            OnEditCommand="dgTicketDetails_EditCommand" OnItemCommand="dgTicketDetails_ItemCommand">
                                            <FooterStyle CssClass="CartListFooter" />
                                            <AlternatingItemStyle CssClass="CartListItemAlt" />
                                            <ItemStyle CssClass="CartListItem" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundColumn DataField="NoticeIntNo" HeaderText="NotIntNo" Visible="False" ReadOnly="True">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="ChgIntNo" HeaderText="ChgIntNo" ReadOnly="True" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.HeaderText %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTicketNo" Text='<%# Bind("TicketNo") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--<asp:BoundColumn DataField="TicketNo" HeaderText="Ticket / Summons">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>--%>
                                                <asp:BoundColumn DataField="ID" HeaderText="<%$Resources:dgTicketDetails.HeaderText1 %> ">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="OffenceDate" HeaderText=" <%$Resources:dgTicketDetails.HeaderText2 %>"
                                                    DataFormatString="{0:yyyy-MM-dd}">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="RegistrationNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText3 %> ">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="OffenceDescription" HeaderText="<%$Resources:dgTicketDetails.HeaderText4 %> ">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Amount" HeaderText="<%$Resources:dgTicketDetails.HeaderText5 %> "
                                                    DataFormatString="R {0:#, ##0.00}">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoticeStatus" HeaderText="<%$Resources:dgTicketDetails.HeaderText6 %> ">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="ContemptCourtAmt" HeaderText="<%$Resources:dgTicketDetails.HeaderText7 %> "
                                                    DataFormatString="R {0:#, ##0.00}">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="CourtDate" HeaderText="<%$Resources:dgTicketDetails.HeaderText8 %> "
                                                    DataFormatString="{0:yyyy-MM-dd}">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.HeaderText9 %> ">
                                                    <ItemTemplate>
                                                        <asp:LinkButton CommandName="SummonsDetails" CommandArgument="Select" ID="lbSummonsDetails"
                                                            runat="server" Text="<%$Resources:lbSummonsDetails.Text %>" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.HeaderText10 %> ">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:ImageButton ID="igPayNow" runat="server" OnClick="ImageButton1_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="ChargeTypeIntNo" HeaderText="ChargeTypeIntNo" ReadOnly="True"
                                                    Visible="False"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.HeaderText11 %> ">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtAmountToPay" runat="server" CssClass="Normal" ReadOnly="false"
                                                            Width="95px"></asp:TextBox>
                                                        <%--                                                        <asp:Label runat="server" ID="lblAmountToPay" Text='<%# DataBinder.Eval(Container, "DataItem.AmountToPay") %>'></asp:Label>
                                                        --%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText=" <%$Resources:dgTicketDetails.HeaderText12 %>">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlSeniorOfficer" runat="server" CssClass="Normal" Width="242px">
                                                        </asp:DropDownList>
                                                        <%--                                                        <asp:Label runat="server" ID="lblSeniorOfficer" Text='<%# DataBinder.Eval(Container, "DataItem.SeniorOfficer") %>'></asp:Label>
                                                        --%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:EditCommandColumn CancelText="<%$Resources:dgTicketDetails.CancelText %>" EditText="<%$Resources:dgTicketDetails.EditText %>"
                                                    UpdateText="<%$Resources:dgTicketDetails.UpdateText %>"></asp:EditCommandColumn>
                                                <asp:BoundColumn DataField="HasJudgement" HeaderText="Has Judgement" Visible="False">
                                                </asp:BoundColumn>
                                            </Columns>
                                            <PagerStyle Visible="False" />
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgDeferredPayments" runat="server" AlternatingItemStyle-CssClass="CartListItemAlt"
                                            AutoGenerateColumns="False" BorderColor="Black" CellPadding="4" Font-Size="8pt"
                                            FooterStyle-CssClass="CartListFooter" GridLines="Vertical" HeaderStyle-CssClass="CartListHead"
                                            ItemStyle-CssClass="CartListItem" ShowFooter="True" CssClass="Normal" Width="100%"
                                            OnItemCreated="dgDeferredPayments_ItemCreated" OnSelectedIndexChanged="dgDeferredPayments_SelectedIndexChanged"
                                            OnEditCommand="dgDeferredPayments_EditCommand" OnItemCommand="dgDeferredPayments_ItemCommand">
                                            <FooterStyle CssClass="CartListFooter" />
                                            <AlternatingItemStyle CssClass="CartListItemAlt" />
                                            <ItemStyle CssClass="CartListItem" />
                                            <HeaderStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundColumn DataField="NoticeIntNo" HeaderText="NotIntNo" Visible="False" ReadOnly="True">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="ChgIntNo" HeaderText="ChgIntNo" ReadOnly="True" Visible="False">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="TicketNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="ID" HeaderText="<%$Resources:dgTicketDetails.HeaderText1 %>">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="OffenceDate" HeaderText="<%$Resources:dgTicketDetails.HeaderText2 %>"
                                                    DataFormatString="{0:yyyy-MM-dd}">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="RegistrationNo" HeaderText="<%$Resources:dgTicketDetails.HeaderText3 %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="OffenceDescription" HeaderText="<%$Resources:dgTicketDetails.HeaderText4 %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="NoticeStatus" HeaderText="<%$Resources:dgTicketDetails.HeaderText6 %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="DPDate" HeaderText="<%$Resources:dgDeferredPayments.HeaderText %>"
                                                    DataFormatString="{0:yyyy-MM-dd}">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="DPAmount" HeaderText="<%$Resources:dgDeferredPayments.HeaderText1 %>"
                                                    DataFormatString="R {0:#, ##0.00}">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="CourtDate" HeaderText="<%$Resources:dgTicketDetails.HeaderText8 %>"
                                                    DataFormatString="{0:yyyy-MM-dd}">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="SCDefPayIntNo" HeaderText="SCDefPayIntNo" Visible="False"
                                                    ReadOnly="True">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.HeaderText9 %>">
                                                    <ItemTemplate>
                                                        <asp:LinkButton CommandName="SummonsDetails" CommandArgument="Select" ID="lbSummonsDetails"
                                                            runat="server" Text="<%$Resources:lbSummonsDetails.Text %>" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgTicketDetails.HeaderText10 %>">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:ImageButton ID="igDeferredPayNow" runat="server" OnClick="ImageDeferredButton1_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="ChargeTypeIntNo" HeaderText="ChargeTypeIntNo" ReadOnly="True"
                                                    Visible="False"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="New Fine Amount" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtAmountToPay" runat="server" CssClass="Normal" ReadOnly="false"
                                                            Width="95px"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Senior Officer" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlSeniorOfficer" runat="server" CssClass="Normal" Width="242px">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:EditCommandColumn CancelText="Cancel" EditText="Modify" UpdateText="Update"
                                                    Visible="false"></asp:EditCommandColumn>
                                                <asp:BoundColumn DataField="HasJudgement" HeaderText="Has Judgement" Visible="False">
                                                </asp:BoundColumn>
                                            </Columns>
                                            <PagerStyle Visible="False" />
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="height: 21px; width: 711px;">
                                        <asp:Label ID="labelTotal" runat="server" CssClass="NormalBold"></asp:Label>
                                        &nbsp; &nbsp; &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;" align="center">
                                        <asp:Panel ID="pnlAddPayment" runat="server" Width="100%">
                                            <asp:Panel ID="pnlSuspenseNotice" runat="server" Width="100%">
                                                <table border="0" width="100%" style="text-align: left" class="Normal">
                                                    <tr>
                                                        <td class="NormalBold">
                                                            Notice Number:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNoticeNumber" runat="server" CssClass="Normal" Width="200px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="NormalBold">
                                                            Select local authority:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="ddlAuthority">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlAddPayments" runat="server" BorderStyle="Solid" BorderWidth="1px">
                                            <table>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Panel ID="pnlPaymentType" runat="server">
                                                            <p class="SubContentHead">
                                                                <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblSPaymentType.Text %>"></asp:Label></p>
                                                            <table border="0" width="100%" style="text-align: left" class="Normal">
                                                                <tr>
                                                                    <td valign="top" class="NormalBold" width="20%">
                                                                        <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblPaymentType.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="comboPaymentType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="comboPaymentType_SelectedIndexChanged"
                                                                            CssClass="Normal" Width="160px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold" valign="top">
                                                                        <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblTotal.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="txtTotal" runat="server" CssClass="Normal" AutoCompleteType="Disabled"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Panel ID="pnlCheque" runat="server" CssClass="NormalBold" Visible="False">
                                                            <p class="SubContentHead">
                                                                <asp:Label ID="Label8" runat="server" Text="<%$Resources:lblChequeDetails.Text %>"></asp:Label>
                                                            </p>
                                                            <table border="0" width="100%">
                                                                <tr>
                                                                    <td class="NormalBold" valign="top" width="20%">
                                                                        <asp:Label ID="Label9" runat="server" Text="<%$Resources:lblChequeNo.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtChequeNo" runat="server" Width="150px" CssClass="Normal" AutoCompleteType="Disabled"
                                                                            MaxLength="20"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold" valign="top">
                                                                        <asp:Label ID="Label10" runat="server" Text="<%$Resources:lblDrawer.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtChequeDrawer" runat="server" Width="250px" CssClass="Normal"
                                                                            MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold" valign="top">
                                                                        <asp:Label ID="Label11" runat="server" Text="<%$Resources:lblBankName.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtBankName" runat="server" Width="250px" CssClass="Normal" MaxLength="30"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold" valign="top">
                                                                        <asp:Label ID="Label12" runat="server" Text="<%$Resources:lblBankBranch.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtBranch" runat="server" Width="250px" CssClass="Normal" MaxLength="30"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold" valign="top">
                                                                        <asp:Label ID="Label13" runat="server" Text="<%$Resources:lblChequeDate.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="wdcChequeDate" CssClass="Normal" Height="20px" autocomplete="off"
                                                                            UseSubmitBehavior="False" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="wdcChequeDate"
                                                                            Format="yyyy-MM-dd">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="wdcChequeDate"
                                                                            CssClass="NormalRed" ErrorMessage="<%$Resources:reqValidDate.ErrorMsg %>" ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold" valign="top">
                                                                    </td>
                                                                    <td valign="top" style="width: 514px">
                                                                        <asp:CheckBox ID="checkVerified" runat="server" CssClass="NormalBold" Text="<%$Resources:checkVerified.Text %>"
                                                                            Width="328px" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlCreditCard" runat="server" CssClass="NormalBold" Visible="False">
                                                            <p class="SubContentHead">
                                                                <asp:Label ID="Label14" runat="server" Text="<%$Resources:lblCardDetails.Text %>"></asp:Label></p>
                                                            <table border="0" width="100%">
                                                                <tr>
                                                                    <td title="(MasterCard, Visa, etc.)" class="NormalBold" width="20%">
                                                                        <asp:Label ID="Label15" runat="server" Text="<%$Resources:lblCardIssuer.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCardType" runat="server" Width="100%" CssClass="Normal"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold">
                                                                        <asp:Label ID="Label16" runat="server" Text="<%$Resources:lblNameCard.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtNameOnCard" runat="server" Width="100%" CssClass="Normal"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold">
                                                                        <asp:Label ID="Label17" runat="server" Text="<%$Resources:lblExpiryDate.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td class="NormalBold" valign="top">
                                                                        <asp:Label ID="Label18" runat="server" Text="<%$Resources:lblMonth.Text %>"></asp:Label>&nbsp;
                                                                        <asp:DropDownList ID="ddlCardMonth" runat="server" CssClass="Normal">
                                                                            <asp:ListItem Value="1" Text="<%$Resources:ddlCardMonthItem.Text %>"></asp:ListItem>
                                                                            <asp:ListItem Value="2" Text="<%$Resources:ddlCardMonthItem.Text1 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="3" Text="<%$Resources:ddlCardMonthItem.Text2 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="4" Text="<%$Resources:ddlCardMonthItem.Text3 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="5" Text="<%$Resources:ddlCardMonthItem.Text4 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="6" Text="<%$Resources:ddlCardMonthItem.Text5 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="7" Text="<%$Resources:ddlCardMonthItem.Text6 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="8" Text="<%$Resources:ddlCardMonthItem.Text7 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="9" Text="<%$Resources:ddlCardMonthItem.Text8 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="10" Text="<%$Resources:ddlCardMonthItem.Text9 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="11" Text="<%$Resources:ddlCardMonthItem.Text10 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="12" Text="<%$Resources:ddlCardMonthItem.Text11 %>"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        &nbsp;
                                                                        <asp:Label ID="Label19" runat="server" Text="<%$Resources:lblYear.Text %>"></asp:Label>&nbsp;
                                                                        <asp:DropDownList ID="ddlCreditCardYear" runat="server" CssClass="Normal">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlDCardDetails" runat="server" CssClass="NormalBold" Visible="False">
                                                            <p class="SubContentHead">
                                                                <asp:Label ID="Label20" runat="server" Text="<%$Resources:lblCardDetail.Text %>"></asp:Label></p>
                                                            <table border="0" width="100%">
                                                                <tr>
                                                                    <td title="(MasterCard, Visa, etc.)" class="NormalBold" width="20%">
                                                                        <asp:Label ID="Label21" runat="server" Text="<%$Resources:lblCardIssuer.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDCardIssuer" runat="server" CssClass="Normal" Width="100%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold">
                                                                        <asp:Label ID="Label22" runat="server" Text="<%$Resources:lblNameCard.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtNameOnDCard" runat="server" CssClass="Normal" Width="100%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="NormalBold">
                                                                        <asp:Label ID="Label23" runat="server" Text="<%$Resources:lblExpiryDate.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td class="NormalBold" valign="top">
                                                                        <asp:Label ID="Label24" runat="server" Text="<%$Resources:lblMonth.Text %>"></asp:Label>&nbsp;
                                                                        <asp:DropDownList ID="ddlDCardMonth" runat="server" CssClass="Normal">
                                                                            <asp:ListItem Value="1" Text="<%$Resources:ddlCardMonthItem.Text %>"></asp:ListItem>
                                                                            <asp:ListItem Value="2" Text="<%$Resources:ddlCardMonthItem.Text1 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="3" Text="<%$Resources:ddlCardMonthItem.Text2 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="4" Text="<%$Resources:ddlCardMonthItem.Text3 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="5" Text="<%$Resources:ddlCardMonthItem.Text4 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="6" Text="<%$Resources:ddlCardMonthItem.Text5 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="7" Text="<%$Resources:ddlCardMonthItem.Text6 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="8" Text="<%$Resources:ddlCardMonthItem.Text7 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="9" Text="<%$Resources:ddlCardMonthItem.Text8 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="10" Text="<%$Resources:ddlCardMonthItem.Text9 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="11" Text="<%$Resources:ddlCardMonthItem.Text10 %>"></asp:ListItem>
                                                                            <asp:ListItem Value="12" Text="<%$Resources:ddlCardMonthItem.Text11 %>"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        &nbsp;
                                                                        <asp:Label ID="Label25" runat="server" Text="<%$Resources:lblYear.Text %>"></asp:Label>&nbsp;
                                                                        <asp:DropDownList ID="ddlDebitCardYear" runat="server" CssClass="Normal">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlPostalOrder" runat="server" CssClass="NormalBold" Visible="False">
                                                            <p class="SubContentHead">
                                                                <asp:Label ID="Label26" runat="server" Text="<%$Resources:lblPostalDetail.Text %>"></asp:Label></p>
                                                            <table border="0">
                                                                <tr>
                                                                    <td class="Normal">
                                                                        <asp:Label ID="Label27" runat="server" Text="<%$Resources:lblPostalNum.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPONo" runat="server" CssClass="Normal" AutoCompleteType="Disabled"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlBankPayment" runat="server" CssClass="NormalBold" Visible="False">
                                                            <p class="SubContentHead">
                                                                <asp:Label ID="Label28" runat="server" Text="<%$Resources:lblPaymentDetails.Text %>"></asp:Label>
                                                            </p>
                                                            <table border="0">
                                                                <tr>
                                                                    <td class="Normal">
                                                                        <asp:Label ID="Label29" runat="server" Text="<%$Resources:lblReference.Text %>"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtReference" runat="server" CssClass="Normal" MaxLength="255" Width="296px"
                                                                            AutoCompleteType="Disabled"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblError" runat="server" CssClass="NormalRed" Width="100%"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="<%$Resources:btnAdd.Text %>"
                                                            CssClass="NormalButton" Width="63px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <br />
                                        <table>
                                            <tr>
                                                <td valign="top" align="center">
                                                    <asp:Panel ID="pnlPayments" runat="server" HorizontalAlign="Center" BorderColor="Black"
                                                        BorderStyle="Solid" BorderWidth="1px">
                                                        <asp:Label ID="lblNoPayments" runat="server" Text="<%$Resources:lblNoPayments.Text %>"
                                                            CssClass="NormalRed"></asp:Label>
                                                        <asp:GridView ID="grdPayments" runat="server" AutoGenerateColumns="False" CssClass="NormalBold"
                                                            ShowFooter="True" OnRowCommand="grdPayments_RowCommand" CellPadding="5">
                                                            <Columns>
                                                                <asp:BoundField DataField="TRDIntNo" HeaderText="TRDIntNo" Visible="False" />
                                                                <asp:BoundField DataField="TRHIntNo" HeaderText="TRHIntNo" Visible="False" />
                                                                <asp:TemplateField HeaderText="<%$Resources:grdPayments.HeaderText %>">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label1" runat="server" Text='<%# (Eval("TRDCashType")).ToString().Length > 0 ? ((Stalberg.TMS.CashType)(Eval("TRDCashType")).ToString()[0]).ToString() : "" %>'
                                                                            CssClass="Normal"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="NormalBold" Font-Underline="True" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Details" HeaderText="<%$Resources:grdPayments.HeaderText1 %>">
                                                                    <HeaderStyle CssClass="NormalBold" Font-Underline="True" />
                                                                    <ItemStyle CssClass="Normal" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="<%$Resources:grdPayments.HeaderText2 %>">
                                                                    <ItemStyle CssClass="Normal" HorizontalAlign="Right" />
                                                                    <HeaderStyle CssClass="NormalBold" Font-Underline="True" HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("TRDAmount", "{0:0.00}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField SelectText="<%$Resources:DeleteText %>" ShowSelectButton="True">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <HeaderStyle BackColor="White" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="TRDCashType" HeaderText="TRDCashType" Visible="False" />
                                                            </Columns>
                                                        </asp:GridView>
                                                        <asp:Button ID="btnBackFromRepresentations" runat="server" Text="<%$Resources:btnBackFromRepresentations.Text %>"
                                                            CssClass="NormalButton" OnClick="btnBackFromRepresentations_Click" Width="170px" /></asp:Panel>
                                                </td>
                                                <td valign="top">
                                                    <asp:Panel ID="pnlTotals" runat="server" BackColor="Silver" HorizontalAlign="Center">
                                                        <table>
                                                            <tr>
                                                                <td class="NormalBold">
                                                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:lblTotalDue.Text %>" CssClass="NormalBold"
                                                                        Font-Bold="True" Font-Size="Larger"></asp:Label>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="lblTotalDue" runat="server" CssClass="NormalBold" Font-Bold="True"
                                                                        Font-Size="Larger" Height="24px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="NormalBold">
                                                                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblReceived.Text %>" CssClass="NormalBold"
                                                                        Width="163px" Font-Bold="True" Font-Size="Larger"></asp:Label>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="lblTotalPaymentReceived" runat="server" CssClass="NormalBold" Font-Bold="True"
                                                                        Font-Size="Larger" Height="24px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="NormalBold" rowspan="2" style="height: 41px">
                                                                    <asp:Label ID="lblDifference" runat="server" Text="<%$Resources:lblDifference.Text %>"
                                                                        CssClass="NormalBold" Font-Bold="True" Font-Size="Larger" ForeColor="Red"></asp:Label>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" colspan="1" rowspan="1" style="height: 41px">
                                                                    <asp:Label ID="lblOverOrUnder" runat="server" CssClass="NormalBold" BackColor="White"
                                                                        BorderStyle="None" BorderWidth="1px" Font-Bold="True" Font-Size="Larger" ForeColor="Red"
                                                                        Height="24px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <asp:CheckBox ID="chkCashierRep" runat="server" Text="<%$Resources:chkCashierRep.Text %>"
                                                            CssClass="NormalBold" Width="355px" BackColor="Silver" Font-Bold="True" Font-Size="Larger"
                                                            Height="25px" AutoPostBack="True" OnCheckedChanged="chkCashierRep_CheckedChanged" />
                                                        <br />
                                                        <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="<%$Resources:btnContinue.Text %>"
                                                            CssClass="NormalButton" Width="115px" /></asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        &nbsp; &nbsp; &nbsp;
                                        <asp:Panel ID="pnlReceiptHeader" runat="server" Width="125px">
                                            <table>
                                                <tr>
                                                    <td class="NormalBold" style="width: 20%" valign="top">
                                                    </td>
                                                    <td width="80%">
                                                        <asp:TextBox ID="txtComment" runat="server" CssClass="Normal" TextMode="MultiLine"
                                                            Width="500px" Text="<%$Resources:txtComment.Text %>"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="NormalBold" style="width: 20%; height: 21px;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 3px; height: 21px">
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <asp:Panel ID="pnlAddress" runat="server" CssClass="NormalBold" Visible="False">
                                                <table>
                                                    <tr>
                                                        <td class="NormalBold" valign="top" style="width: 20%; height: 40px;">
                                                            <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblDateReceived.Text %>"></asp:Label>
                                                        </td>
                                                        <td colspan="3" valign="top" style="width: 129px; height: 40px;">
                                                            <asp:TextBox runat="server" ID="wdcDateReceived" CssClass="Normal" Height="20px"
                                                                autocomplete="off" UseSubmitBehavior="False" />
                                                            <cc1:CalendarExtender ID="DateCalendar" runat="server" TargetControlID="wdcDateReceived"
                                                                Format="yyyy-MM-dd">
                                                            </cc1:CalendarExtender>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="wdcDateReceived"
                                                                CssClass="NormalRed" ErrorMessage="<%$Resources:reqValidDate.ErrorMsg %>" ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="NormalBold" valign="top" style="width: 20%; height: 40px;">
                                                            <asp:Label ID="Label30" runat="server" Text="<%$Resources:lblReceivedFrom.Text %>"></asp:Label>
                                                        </td>
                                                        <td valign="top" style="width: 129px; height: 40px;">
                                                            <asp:DropDownList ID="ddlReceivedFrom" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReceivedFrom_SelectedIndexChanged"
                                                                CssClass="Normal" Width="91px">
                                                                <asp:ListItem Value="D" Text="<%$Resources:ddlReceivedFromItem.Text %>"></asp:ListItem>
                                                                <asp:ListItem Value="O" Text="<%$Resources:ddlReceivedFromItem.Text1 %>"></asp:ListItem>
                                                                <asp:ListItem Value="P" Text="<%$Resources:ddlReceivedFromItem.Text2 %>"></asp:ListItem>
                                                                <asp:ListItem Value="A" Text="<%$Resources:ddlReceivedFromItem.Text3 %>"></asp:ListItem>
                                                                <asp:ListItem Value="X" Text="<%$Resources:ddlReceivedFromItem.Text4 %>"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td valign="top" width="20%" style="height: 40px">
                                                            <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                                        </td>
                                                        <td valign="top" width="40%" style="height: 40px">
                                                            <asp:TextBox ID="txtReceivedFrom" runat="server" CssClass="Normal" Width="224px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="NormalBold" valign="top" width="20%">
                                                        </td>
                                                        <td valign="top">
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Label ID="lblAddress" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddress.Text %>"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="txtAddress1" runat="server" CssClass="Normal" Width="224px" Height="22px"></asp:TextBox><br />
                                                            <asp:TextBox ID="txtAddress2" runat="server" Width="224px" CssClass="Normal" Height="22px"></asp:TextBox><br />
                                                            <asp:TextBox ID="txtAddress3" runat="server" Width="224px" CssClass="Normal" Height="22px"></asp:TextBox><br />
                                                            <asp:TextBox ID="txtAddress4" runat="server" Width="224px" CssClass="Normal" Height="22px"></asp:TextBox><br />
                                                            <asp:TextBox ID="txtAddress5" runat="server" Width="224px" CssClass="Normal" Height="22px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="NormalBold" style="width: 20%; height: 26px;">
                                                            <asp:Label ID="Label31" runat="server" Text="<%$Resources:lblPhone.Text %>"></asp:Label>
                                                        </td>
                                                        <td style="height: 26px">
                                                            <asp:TextBox ID="txtPhone" runat="server" CssClass="Normal" Width="119px"></asp:TextBox>
                                                        </td>
                                                        <td style="height: 26px">
                                                            <asp:Label ID="lblPostalCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPostalCode.Text %>"
                                                                Width="87px"></asp:Label>
                                                        </td>
                                                        <td width="40%" style="height: 26px">
                                                            <asp:TextBox ID="txtPoCode" runat="server" MaxLength="5" Width="80px" CssClass="Normal"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <br />
                                            <asp:Panel ID="pnlAllowPayment" runat="server" HorizontalAlign="Center">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="linkBack" runat="server" PostBackUrl="CashReceiptTraffic.aspx"
                                                                Width="409px" Text="<%$Resources:linkBack.Text %>"></asp:LinkButton>
                                                        </td>
                                                        <td style="width: 150px">
                                                            <asp:Button ID="btnPayNow" runat="server" Text="<%$Resources:btnPayNow.Text %>" CssClass="NormalButton"
                                                                OnClick="btnPayNow_Click" Width="90px" OnClientClick="SetHiddenValue();" />
                                                            <asp:HiddenField runat="server" ID="hidPayClick" Value="0" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        &nbsp;<!-- On-The-Fly Representations-->
                        <!-- Payment Confirmation -->
                        <asp:Panel ID="panelConfirm" runat="server" Visible="False" Width="100%">
                            <p style="text-align: left">
                                <asp:Label ID="labelConfirm" runat="server" CssClass="Normal"></asp:Label></p>
                            <p style="text-align: center">
                                <asp:Button ID="buttonBack" runat="server" Text="<%$Resources:buttonBack.Text %>"
                                    CssClass="NormalButton" OnClick="buttonBack_Click" Width="170px" />
                                <asp:Button ID="buttonVoid" runat="server" Text="<%$Resources:buttonVoid.Text %>"
                                    OnClick="buttonVoid_Click" CssClass="NormalButton" Width="170px" />&nbsp;
                                <asp:Button ID="buttonPay" runat="server" Text="<%$Resources:buttonPay.Text %>" CssClass="NormalButton"
                                    OnClick="buttonPay_Click" Width="170px" />
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pnlAnotherPayment" Width="100%" runat="server" Visible="false" HorizontalAlign="Center">
                            <br />
                            <a href="CashReceiptTraffic.aspx" class="NormalBold">
                                <asp:Label ID="Label32" runat="server" Text="<%$Resources:lblProcessPayment.Text %>"></asp:Label></a></asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="udp" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label33"
                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td style="width: 621px">
                &nbsp;
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
