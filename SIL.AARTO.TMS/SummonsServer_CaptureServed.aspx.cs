using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.QueueLibrary;
using System.Linq;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.DAL.Entities;

//using System.Windows.Input;

namespace Stalberg.TMS
{
    /// <summary>
    /// The starting point for ticket payment receipt
    /// </summary>
    public partial class SummonsServer_CaptureServed : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        //relace all sumChargeStatus with summonsStatus after 4416
        private int summonsStatus = 0;
        private DateTime sumServeByDate = DateTime.MinValue;
        private DateTime sumToSSDate = DateTime.MinValue;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsServer_CaptureServed.aspx";
        //protected string thisPage = "Capture Served Summons Details";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //this.txtSummonsNo.Focus();
            this.SetFocus("txtSummonsNo");

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlDetails.Visible = false;

                this.lblWarning.Visible = true;
                this.lblDelivery.Visible = false;
                this.lblDateServed.Visible = false;
                this.lblServedBy.Visible = false;
                this.cmbServedBy.Visible = false;
                this.txtDateServed.Visible = false;
                this.lblComment.Visible = false;
                this.txtMethodServed.Visible = false;
                this.lblMethodServed.Visible = false;
                this.cvDateServed.Visible = false;
                this.btnCapture.Visible = false;

                //this.pnlCapture.Visible = false;

                //this.cmbMethodServed.Items.Clear();
                //this.cmbMethodServed.Items.Add(new ListItem("[ Select a Delivery Method ]", "0"));
                //this.cmbMethodServed.Items.Add(new ListItem("Personal", "1"));
                //this.cmbMethodServed.Items.Add(new ListItem("Impersonal", "2"));
                //this.cmbMethodServed.Items.Add(new ListItem("Not Served/Stagnant", "3"));
            }

            //AddEnterEvent((Control)sender);
        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;
            //Jerry 2014-08-15 add
            btnCapture.Enabled = false;
            btnCapture.CssClass = "";

            string summonsNo = this.txtSummonsNo.Text.Trim();
            if (summonsNo.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                SetFocus("txtSummonsNo");
                return;
            }

            this.ShowServedSummonsResults(0);
            //this.cmbServedBy.Focus();
            //SetFocus("cmbServedBy");
        }

        private void ShowServedSummonsResults(int pageNo)
        {
            this.pnlCapture.Visible = false;

            //mrs 20080718 added serveBy date to proc so that served dates after serveBy date can be rejected
            NoticeDB db = new NoticeDB(this.connectionString);
            DataSet ds = db.SearchServedSummons(this.autIntNo, this.txtSummonsNo.Text.Trim());

            this.summonsStatus = 0;
            ViewState.Add("summonsStatus", this.summonsStatus);

            StringBuilder sb = new StringBuilder();
            sb.Append((string)GetLocalResourceObject("lblError.Text1") + "\n<ul>\n");

            if (ds.Tables[0].Rows.Count == 0)
            {
                this.lblError.Visible = true;
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                SetFocus("txtSummonsNo");
                return;
            }
            else
            {
                //need to uncomment this.........
                //BD need to handle the return of a summons that has already passed 640 but might need to change.

                //dls 090701 - check if the Court roll for the court date on this summons has already been finalised
                // First get date rules
                //DateRulesDB dateRule = new DateRulesDB(this.connectionString);
                DateRulesDetails rule = new DateRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.DtRStartDate = "CDate";
                rule.DtREndDate = "FinalCourtRoll";
                rule.LastUser = this.login;

                DefaultDateRules dr = new DefaultDateRules(rule, this.connectionString);

                int noOfDays = dr.SetDefaultDateRule();

                DateTime CourtDate;
                if (!DateTime.TryParse(ds.Tables[0].Rows[0]["SumCourtDate"].ToString(), out CourtDate))
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    SetFocus("txtSummonsNo");
                    return;
                }

                if (DateTime.Now > CourtDate.AddDays(noOfDays))
                {
                    string msg = (string)GetLocalResourceObject("msg");

                    //Jerry 2013-10-31 add time portion for EvidencePack.EPItemDate
                    //CaptureSummonsServed(Convert.ToInt32(ds.Tables[0].Rows[0]["SumIntNo"]), 0, DateTime.Today, 3, 649, msg);
                    CaptureSummonsServed(Convert.ToInt32(ds.Tables[0].Rows[0]["SumIntNo"]), 0, DateTime.Now, 3, 649, msg);

                    //SummonsDB summonsDb = new SummonsDB(this.connectionString);
                    //if (!summonsDb.UpdateSummonsStatus(Convert.ToInt32(ds.Tables[0].Rows[0]["SumIntNo"]), "Stagnated - not captured", 3, this.login))
                    //    lblError.Text += " Summons status update failed.";

                    lblWarning.Visible = false;
                    return;
                }

                this.summonsStatus = int.Parse(ds.Tables[0].Rows[0]["SummonsStatus"].ToString());
                ViewState.Add("summonsStatus", this.summonsStatus);
                try
                {
                    if (!DateTime.TryParse(ds.Tables[0].Rows[0]["SumServeByDate"].ToString(), out this.sumServeByDate))
                    {
                        this.sumServeByDate = DateTime.MinValue;
                    }
                    else
                    {
                        this.sumServeByDate = DateTime.Parse(ds.Tables[0].Rows[0]["SumServeByDate"].ToString());
                    }
                    ViewState.Add("sumServeByDate", this.sumServeByDate);
                }
                catch
                {
                    this.sumServeByDate = DateTime.MinValue;
                    ViewState.Add("sumServeByDate", this.sumServeByDate);
                }

                lblSumServeByDate.Text = this.sumServeByDate.ToString("yyyy-MM-dd");

                //mrs 20080704 need sumToSSDate
                DateTime ssDate = DateTime.MinValue;
                try
                {
                    object dtObject = ds.Tables[0].Rows[0]["SumSSToDate"];
                    if (dtObject != null)
                    {
                        if (!DateTime.TryParse(dtObject.ToString(), out ssDate))
                        {
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text4") + "</li>");
                            lblError.Text = sb.ToString();
                        }
                    }
                }
                catch
                {
                    this.sumToSSDate = DateTime.MinValue;
                }

                lblSumSSToDate.Text = this.sumToSSDate.ToString("yyyy-MM-dd");

                //if (int.Parse(ds.Tables[0].Rows[0]["SumChargeStatus"].ToString()) > 640)
                if (this.summonsStatus > 640)
                {
                    if (ds.Tables[0].Rows[0]["SumCaseNo"].ToString() == string.Empty)
                    {
                        //BD Show message, if they want to change it then carry on as normal with the update, if they made a mistake cancel the update
                        lblProcessed.Visible = true;
                        btnCancelSearch.Visible = true;
                        btnContinue.Visible = true;

                        this.lblWarning.Visible = false;
                        this.lblDelivery.Visible = false;
                        this.lblDateServed.Visible = false;
                        this.lblServedBy.Visible = false;
                        this.cmbServedBy.Visible = false;
                        this.txtDateServed.Visible = false;
                        this.lblComment.Visible = false;
                        this.txtMethodServed.Visible = false;
                        this.lblMethodServed.Visible = false;
                        this.cvDateServed.Visible = false;
                        this.btnCapture.Visible = false;

                        SetFocus("btnContinue");
                    }
                    else
                    {
                        this.lblError.Visible = true;
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                        SetFocus("txtSummonsNo");
                        return;
                    }
                }
                else
                {
                    lblProcessed.Visible = false;
                    btnCancelSearch.Visible = false;
                    btnContinue.Visible = false;

                    this.lblWarning.Visible = false;
                    this.lblDelivery.Visible = true;
                    this.lblDateServed.Visible = true;
                    this.lblServedBy.Visible = true;
                    this.cmbServedBy.Visible = true;
                    this.txtDateServed.Visible = true;
                    this.lblComment.Visible = true;
                    this.txtMethodServed.Visible = true;
                    this.lblMethodServed.Visible = true;
                    this.cvDateServed.Visible = true;
                    this.btnCapture.Visible = true;

                    this.lblError.Visible = false;
                    this.lblError.Text = string.Empty;
                    this.lblResult.Text = string.Empty;

                    this.pnlDetails.Visible = true;
                    this.dvSummons.DataSource = ds;
                    //this.dvSummons.DataKeyNames = new string[] { "NotIntNo", "ChgIntNo", "AutIntNo", "SSIntNo", "ACIntNo", "CrtIntNo", "CRIntNo" };
                    this.dvSummons.DataKeyNames = new string[] { "SumIntNo", "SSIntNo", "SumServedStatus", "SumServedDate" };
                    this.dvSummons.DataSource = ds;
                    this.dvSummons.PageIndex = pageNo;
                    this.dvSummons.DataBind();

                    //BD 20080509 moved from the select button
                    int ssIntNo = (int)dvSummons.DataKey["SSIntNo"];
                    int sumServedStatus = Convert.ToInt16(dvSummons.DataKey["SumServedStatus"]);

                    DateTime sumServedDate = DateTime.MinValue;
                    object dtObject = dvSummons.DataKey["SumServedDate"];
                    if (dtObject != null)
                        DateTime.TryParse(dtObject.ToString(), out sumServedDate);
                    // SD: 20081209 test for null value
                    if (sumServeByDate == DateTime.MinValue)
                    {
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text6") + "</li>");
                        sb.Append("</ul>");
                        lblError.Text = sb.ToString();
                    }

                    // Get Summons Server and a list of staff to populate the staff combo
                    SummonsServerDB ssDb = new SummonsServerDB(this.connectionString);
                    DataSet dsServer = ssDb.GetSummonServerStaff(ssIntNo, true, true);
                    this.cmbServedBy.DataSource = dsServer;
                    this.cmbServedBy.DataValueField = "SS_SIntNo";
                    this.cmbServedBy.DataTextField = "FullName";
                    this.cmbServedBy.DataBind();
                    this.cmbServedBy.Items.Insert(0, new ListItem((string)GetLocalResourceObject("cmbServedBy.Items"), string.Empty));

                    //BD need to link data to webcombo for infragistics - cant get it right becuase infragistics is being dumb...

                    if (!this.dvSummons.Rows[11].Cells[1].Text.Equals(""))
                        this.cmbServedBy.SelectedIndex = this.cmbServedBy.Items.IndexOf(this.cmbServedBy.Items.FindByText(this.dvSummons.Rows[11].Cells[1].Text));

                    //this.cmbMethodServed.SelectedIndex = this.cmbMethodServed.Items.IndexOf(this.cmbMethodServed.Items.FindByValue(sumServedStatus.ToString()));
                    this.txtMethodServed.Text = sumServedStatus.ToString().Equals("0") ? string.Empty : sumServedStatus.ToString();

                    DisplayMethodServed();

                    //this.lblSummonsServer.Text = this.dvSummons.Rows[10].Cells[1].Text;
                    //this.pnlCapture.Visible = true;
                    //this.lblCourtName.Text = this.dvSummons.Rows[0].Cells[1].Text;
                    //this.lblCourtNo.Text = this.dvSummons.Rows[1].Cells[1].Text;

                    //this.wdcServedDate.Value = sumServedDate; 
                    if (dvSummons.DataKey["SumServedDate"].ToString() != string.Empty)
                    {
                        this.txtDateServed.Text = Convert.ToString(sumServedDate.Year) + Convert.ToString(sumServedDate.Month).PadLeft(2, '0') + Convert.ToString(sumServedDate.Day).PadLeft(2, '0');
                    }
                    else
                    {
                        this.txtDateServed.Text = string.Empty;
                    }

                    SetFocus("cmbServedBy");
                }
            }

        }

        private void DisplayMethodServed()
        {
            switch (txtMethodServed.Text)
            {
                case "0":
                case "":
                    lblMethodServed.Text = string.Empty;
                    break;
                case "1":
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text");
                    break;
                case "2":
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text1");
                    break;
                case "3":
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text2");
                    break;
                case "4":
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text3");
                    break;
                case "5":
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text4");
                    break;
                case "6":
                    //jerry 2012-1-12 changed
                    //lblMethodServed.Text = "Summons not served - insufficient time allocated for service";
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text5");
                    break;
                case "7":
                    //jerry 2012-1-12 changed
                    //lblMethodServed.Text = "Summons not served - other";
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text6");
                    break;
                case "8":
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text7");
                    break;
                case "9":
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text8");
                    break;
                case "11"://jerry 2012-1-12 add
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text10");
                    break;
            }

        }

        protected void dvSummons_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                //BD 20080509 need to move this to the search button
                //int ssIntNo = (int)dvSummons.DataKey["SSIntNo"];
                //int sumServedStatus = Convert.ToInt16(dvSummons.DataKey["SumServedStatus"]);

                //DateTime sumServedDate;

                //DateTime.TryParse(dvSummons.DataKey["SumServedDate"].ToString(), out sumServedDate);

                //// Get Summons Server and a list of staff to populate the staff combo
                //SummonsServerDB ssDb = new SummonsServerDB(this.connectionString);
                //DataSet ds = ssDb.GetSummonServerStaff(ssIntNo, true);
                //this.cmbServedBy.DataSource = ds;
                //this.cmbServedBy.DataValueField = "SS_SIntNo";
                //this.cmbServedBy.DataTextField = "FullName";
                //this.cmbServedBy.DataBind();
                //this.cmbServedBy.Items.Insert(0, new ListItem("[ Select a Person ]", string.Empty));

                //if (!this.dvSummons.Rows[11].Cells[1].Text.Equals(""))
                //    this.cmbServedBy.SelectedIndex = this.cmbServedBy.Items.IndexOf(this.cmbServedBy.Items.FindByText(this.dvSummons.Rows[11].Cells[1].Text));

                //this.cmbMethodServed.SelectedIndex = this.cmbMethodServed.Items.IndexOf(this.cmbMethodServed.Items.FindByValue(sumServedStatus.ToString()));
                //this.lblSummonsServer.Text = this.dvSummons.Rows[10].Cells[1].Text;
                //this.pnlCapture.Visible = true;
                //this.lblCourtName.Text = this.dvSummons.Rows[0].Cells[1].Text;
                //this.lblCourtNo.Text = this.dvSummons.Rows[1].Cells[1].Text;

                //this.wdcServedDate.Value = sumServedDate; 
            }
        }

        protected void dvSummons_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
        {
            this.ShowServedSummonsResults(e.NewPageIndex);
        }

        protected void btnCapture_Click(object sender, EventArgs e)
        {
            // SD: 20081209 
            StringBuilder sb = new StringBuilder();
            sb.Append((string)GetLocalResourceObject("lblError.Text1") + "\n<ul>\n");

            int ssSIntNo = 0;
            if (this.cmbServedBy.SelectedValue.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text7"); ;
                this.lblError.Visible = true;
                return;
            }

            ssSIntNo = int.Parse(this.cmbServedBy.SelectedValue);

            DateTime dt;
            DateTime dts;

            if (txtDateServed.Text.Length < 8 || !DateTime.TryParse(txtDateServed.Text.Substring(0, 4) + "/" + txtDateServed.Text.Substring(4, 2) + "/" + txtDateServed.Text.Substring(6, 2), out dt))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                this.lblError.Visible = true;
                this.SetFocus("txtDateServed");
                return;
            }
            //SD 20081209 - test for null value
            if (txtDateServed.Text.Trim().Length == 0)
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text9") + "</li>");

            }

            if (dt < Convert.ToDateTime("2007/01/01"))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10") + " '2007/01/01'";
                this.lblError.Visible = true;
                this.SetFocus("txtDateServed");
                return;
            }

            if (dt > DateTime.Today)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                this.lblError.Visible = true;
                this.SetFocus("txtDateServed");
                return;
            }

            // mrs 20080718 add code to validate that the entered date <= the serveByDate
            if (!DateTime.TryParse(this.lblSumServeByDate.Text, out dts))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                this.lblError.Visible = true;
                this.SetFocus("txtSummonsNo");
                return;
            }

            //SD : 20081209 - test for null Sum serve by date
            if (dts == DateTime.MinValue)
            {
                sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text13") + "</li>");
                sb.Append("</ul>");
                lblError.Text = sb.ToString();
            }

            if (dts < Convert.ToDateTime("2006/01/01"))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                this.lblError.Visible = true;
                this.SetFocus("txtSummonsNo");
                return;
            }

            if (dt > dts)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text15");
                this.lblError.Visible = true;
                this.SetFocus("txtSummonsNo");
                return;
            }

            //Jerry 2014-08-15 rebuild it
            //int methodServed = Convert.ToInt16(this.txtMethodServed.Text);
            //if (methodServed == 0)
            //{
            //    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text16");
            //    this.lblError.Visible = true;
            //    this.SetFocus("txtMethodServed");
            //    return;
            //}
            int methodServed;
            if (!int.TryParse(txtMethodServed.Text.Trim(), out methodServed) || methodServed < 1)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblMethodServed.Text9");
                this.lblError.Visible = true;
                this.SetFocus("txtMethodServed");
                return;
            }

            this.lblError.Visible = false;

            this.summonsStatus = int.Parse(ViewState["summonsStatus"].ToString());

            int sumIntNo = (int)this.dvSummons.DataKey["SumIntNo"];

            //Jerry 2013-10-28 add time portion for EvidencePack.EPItemDate
            dt = new DateTime(dt.Year, dt.Month, dt.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            //dls 090703 - moved this to a separate method so that we can use it for the stagnant summons
            CaptureSummonsServed(sumIntNo, ssSIntNo, dt, methodServed, this.summonsStatus, string.Empty);

            //Linda 2012-9-12 add for list report19  
            //2013-11-7 Heidi changed for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(this.autIntNo, this.login, PunchStatisticsTranTypeList.SummonsServed,SIL.AARTO.BLL.Utility.Printing.PunchAction.Add);
        }

        private void CaptureSummonsServed(Int32 sumIntNo, Int32 ssSIntNo, DateTime dt, int methodServed, int summonsStatus, string msg)
        {
            string errMessage = string.Empty;

            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            {
                SummonsDB db = new SummonsDB(this.connectionString);

                //jerry 2012-1-16 add
                SIL.AARTO.DAL.Entities.NoticeSummons noticeSummonsEntity = new SIL.AARTO.DAL.Services.NoticeSummonsService().GetBySumIntNo(sumIntNo).FirstOrDefault();
                SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(noticeSummonsEntity.NotIntNo);

                // dls 090612 - the call to the proc is returning "false", we need more meaningful error messages
                //if (db.CaptureSummonsServed(sumIntNo, ssSIntNo, dt, methodServed, this.login, this.sumChargeStatus, ref errMessage))
                Int32 returnCode;
                if (noticeEntity.NotFilmType != "M")
                {
                    returnCode = db.CaptureSummonsServed(sumIntNo, ssSIntNo, dt, methodServed, this.login, summonsStatus, ref errMessage);

                    if (returnCode > 0)
                    {
                        this.pnlCapture.Visible = false;
                        this.pnlDetails.Visible = false;

                        this.lblWarning.Visible = true;
                        this.lblDelivery.Visible = false;
                        this.lblDateServed.Visible = false;
                        this.lblServedBy.Visible = false;
                        this.cmbServedBy.Visible = false;
                        this.txtDateServed.Visible = false;
                        this.lblComment.Visible = false;
                        this.txtMethodServed.Visible = false;
                        this.lblMethodServed.Visible = false;
                        this.cvDateServed.Visible = false;
                        this.btnCapture.Visible = false;

                        this.txtSummonsNo.Text = string.Empty;
                        this.lblError.Text = msg.Length > 0 ? msg : (string)GetLocalResourceObject("lblError.Text17");
                        this.SetFocus("txtSummonsNo");

                        // Oscar 20111229
                        QueueItemProcessor queProcessor = new QueueItemProcessor();
                        SIL.AARTO.DAL.Entities.Summons summonsEntity = new SIL.AARTO.DAL.Services.SummonsService().GetBySumIntNo(sumIntNo);

                        SIL.AARTO.DAL.Entities.Authority authority = new SIL.AARTO.DAL.Services.AuthorityService().GetByAutIntNo(summonsEntity.AutIntNo);

                        List<QueueItem> queueList = new List<QueueItem>();

                        if (summonsEntity.CrtRintNo.HasValue && summonsEntity.SumCourtDate.HasValue)
                        {
                            if (methodServed == 1 || methodServed == 2)
                            {

                                SIL.AARTO.DAL.Entities.Court courtEntity = new SIL.AARTO.DAL.Services.CourtService().GetByCrtIntNo(summonsEntity.CrtIntNo);
                                SIL.AARTO.DAL.Entities.CourtRoom courtRoomEntity = new SIL.AARTO.DAL.Services.CourtRoomService().GetByCrtRintNo(summonsEntity.CrtRintNo.Value);
                                DateRuleInfo dateRulesEntity = new DateRuleInfo().GetDateRuleInfoByDRNameAutIntNo(summonsEntity.AutIntNo, "CDate", "FinalCourtRoll");

                                string[] group = new string[4];
                                group[0] = authority.AutCode.Trim();
                                //group[1] = courtEntity.CrtNo.Trim();
                                // Oscar 201203012 changed to use CrtName
                                group[1] = courtEntity.CrtName.Trim();
                                group[2] = courtRoomEntity.CrtRoomNo.Trim();
                                group[3] = summonsEntity.SumCourtDate.Value.ToString("yyyy-MM-dd");

                                // Oscar 2013-03-11 added for passed date checking.
                                var actionDate = summonsEntity.SumCourtDate.Value.AddDays(dateRulesEntity.ADRNoOfDays);
                                if (DateTime.Now.Date <= actionDate.Date)
                                {
                                    queueList.Add(
                                        new QueueItem()
                                        {
                                            Body = sumIntNo,
                                            Group = string.Join("|", group),
                                            ActDate = actionDate,
                                            QueueType = ServiceQueueTypeList.SummonsIsServed
                                        });
                                }
                            }
                        }

                        //jerry 2012-1-16 add
                        if (methodServed == 11)
                        {
                            // Oscar 20120427 changed the Group
                            NoticeDetails notice = new NoticeDB(this.connectionString).GetNoticeDetails(noticeSummonsEntity.NotIntNo);
                            string[] group = new string[3];
                            group[0] = authority.AutCode.Trim();
                            //Jerry 2013-03-07 change
                            //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                            group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                            group[2] = Convert.ToString(notice.NotCourtName);

                            queueList.Add(
                                new QueueItem()
                                {
                                    Body = noticeSummonsEntity.NotIntNo,
                                    //Group = authority.AutCode.Trim(),
                                    Group = string.Join("|", group),
                                    //ActDate = summonsEntity.SumCourtDate.Value.AddDays(),//now withdrawn summons, so immediately generate summons
                                    ActDate = DateTime.Now,
                                    Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days,   // 2013-09-06, Oscar added
                                    //Jerry 2012-05-14 add last user
                                    LastUser = this.login,
                                    QueueType = ServiceQueueTypeList.GenerateSummons
                                }
                            );

                            //Henry 2013-03-27 add
                            AuthorityRulesDetails arDetails = new AuthorityRulesDetails
                            {
                                AutIntNo = this.autIntNo,
                                ARCode = "5050",
                                LastUser = this.login
                            };
                            KeyValuePair<int, string> aR_5050 = (new DefaultAuthRules(arDetails, this.connectionString)).SetDefaultAuthRule();
                            if (aR_5050.Value.Trim().ToUpper() == "Y")
                            {
                                queueList.Add(
                                    new QueueItem()
                                    {
                                        Body = noticeSummonsEntity.NotIntNo,
                                        Group = string.Join("|", group),
                                        ActDate = notice.NotOffenceDate.AddMonths(aR_5050.Key).AddDays(-5),
                                        Priority = notice.NotOffenceDate.Subtract(DateTime.Now).Days,   // 2013-09-06, Oscar added
                                        LastUser = this.login,
                                        QueueType = ServiceQueueTypeList.GenerateSummons
                                    }
                                );
                            }
                        }
                        queProcessor.Send(queueList);

                        scope.Complete();
                    }
                    else
                    {
                        switch (returnCode)
                        {
                            case -1:
                                errMessage = (string)GetLocalResourceObject("lblError.Text18");
                                break;
                            case -2:
                                errMessage = (string)GetLocalResourceObject("lblError.Text19");
                                break;
                            case -3:
                                errMessage = (string)GetLocalResourceObject("lblError.Text20");
                                break;
                            //jerry 2012-1-16 add
                            case -4:
                                errMessage = (string)GetLocalResourceObject("lblError.Text23");
                                break;
                            case -5:
                                errMessage = (string)GetLocalResourceObject("lblError.Text24");
                                break;
                            case -6:
                                errMessage = (string)GetLocalResourceObject("lblError.Text25");
                                break;
                            case -7:
                                errMessage = (string)GetLocalResourceObject("lblError.Text26");
                                break;
                            case -8:
                                errMessage = (string)GetLocalResourceObject("lblError.Text27");
                                break;
                            case 0:
                                //message comes from exception in try..catch
                                break;
                        }
                        this.lblError.Text = (string)GetLocalResourceObject("lblError.Text21") + errMessage;
                        this.SetFocus("txtSummonsNo");

                        this.lblWarning.Visible = false;
                        this.lblDelivery.Visible = false;
                        this.lblDateServed.Visible = false;
                        this.lblServedBy.Visible = false;
                        this.cmbServedBy.Visible = false;
                        this.txtDateServed.Visible = false;
                        this.lblComment.Visible = false;
                        this.txtMethodServed.Visible = false;
                        this.lblMethodServed.Visible = false;
                        this.cvDateServed.Visible = false;
                        this.btnCapture.Visible = false;
                    }
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text28");
                }
                
            }
            this.lblError.Visible = true;
        }

        protected void txtMethodServed_TextChanged(object sender, EventArgs e)
        {
            //BD 20080509 if they change the number in the delivery method, we need to show the correct vlaue in the label
            //"Personal", "1"
            //"Impersonal", "2"
            //"Not Served/Stagnant", "3"
            lblError.Text = "";
            lblMethodServed.Text = "";
            lblMethodServed.CssClass = "";

            btnCapture.CssClass = "";

            //Jerry 2014-08-15 rebuild
            //if (txtMethodServed.Text.Length == 0)
            //{
            //    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text9");
            //    lblMethodServed.CssClass = "NormalRed";
            //    this.summonsStatus = 0;
            //    txtMethodServed.Focus();
            //    return;
            //}

            
            int summonsServerType;
            if (!int.TryParse(txtMethodServed.Text.Trim(), out summonsServerType))
            {
                lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text9");
                lblMethodServed.CssClass = "NormalRed";
                this.summonsStatus = 0;
                txtMethodServed.Focus();
                btnCapture.Enabled = false;
                return;
            }

            //switch (int.Parse(txtMethodServed.Text))
            switch (summonsServerType)
            {
                //case 0:
                //    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text9");
                //    this.summonsStatus = 0;
                //    txtMethodServed.Focus();
                //    SetDecisionPanelVisiable(false);
                //    break;
                case 1:
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text");
                    this.summonsStatus = 641;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 2:
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text1");
                    this.summonsStatus = 642;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 3:
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text2");
                    this.summonsStatus = 643;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 4:
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text3");
                    this.summonsStatus = 644;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 5:
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text4");
                    this.summonsStatus = 645;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 6:
                    //jerry 2012-1-12 changed
                    //lblMethodServed.Text = "Summons not served - insufficient time allocated for service";
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text5");
                    this.summonsStatus = 646;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 7:
                    //jerry 2012-1-12 changed
                    //lblMethodServed.Text = "Summons not served - other";
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text6");
                    this.summonsStatus = 647;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 8:
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text7");
                    this.summonsStatus = 648;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 9:
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text8");
                    this.summonsStatus = 649;
                    SetDecisionPanelVisiable(false);
                    btnCapture.Focus();
                    break;
                case 11: //jerry 2012-1-12 add
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text10");
                    this.summonsStatus = 652;
                    SetDecisionPanelVisiable(true);
                    //btnCapture.Focus();
                    break;
                default:
                    lblMethodServed.Text = (string)GetLocalResourceObject("lblMethodServed.Text9");
                    lblMethodServed.CssClass = "NormalRed";
                    this.summonsStatus = 0;
                    txtMethodServed.Focus();
                    btnCapture.Enabled = false;
                    return;
            }

            ViewState.Add("summonsStatus", this.summonsStatus);
        }

        //jerry 2012-1-12 add
        private void SetDecisionPanelVisiable(bool isTrue)
        {
            btnCapture.Visible = !isTrue;
            pnlDecision.Visible = isTrue;
            btnCapture.Enabled = !isTrue;
            if (btnCapture.Enabled)
                btnCapture.CssClass = "NormalButton";
            else
                btnCapture.CssClass = "";
        }

        protected void cvDateServed_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length == 8)
            {
                DateTime dServed;
                if (args.Value.Length >= 8 && DateTime.TryParse(args.Value.Substring(0, 4) + "/" + args.Value.Substring(4, 2) + "/" + args.Value.Substring(6, 2), out dServed))
                {
                    if (dServed > DateTime.Now.Date || dServed < DateTime.Now.AddMonths(-3))
                    {
                        args.IsValid = false;
                        txtDateServed.Focus();

                    }
                    else if (dServed < DateTime.Parse(lblSumSSToDate.Text))
                    {
                        args.IsValid = false;
                        txtDateServed.Focus();
                    }
                    else
                    {
                        args.IsValid = true;
                        txtMethodServed.Focus();
                    }
                }
                else
                {
                    args.IsValid = false;
                    txtDateServed.Focus();

                }
            }
            else
            {
                args.IsValid = false;
                txtDateServed.Focus();
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            NoticeDB db = new NoticeDB(this.connectionString);
            DataSet ds = db.SearchServedSummons(this.autIntNo, this.txtSummonsNo.Text.Trim());

            lblProcessed.Visible = false;
            btnCancelSearch.Visible = false;
            btnContinue.Visible = false;

            this.lblWarning.Visible = true;
            this.lblDelivery.Visible = true;
            this.lblDateServed.Visible = true;
            this.lblServedBy.Visible = true;
            this.cmbServedBy.Visible = true;
            this.txtDateServed.Visible = true;
            this.lblComment.Visible = true;
            this.txtMethodServed.Visible = true;
            this.lblMethodServed.Visible = true;
            this.cvDateServed.Visible = true;
            this.btnCapture.Visible = true;

            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblResult.Text = string.Empty;

            this.pnlDetails.Visible = true;
            this.dvSummons.DataSource = ds;
            //this.dvSummons.DataKeyNames = new string[] { "NotIntNo", "ChgIntNo", "AutIntNo", "SSIntNo", "ACIntNo", "CrtIntNo", "CRIntNo" };
            this.dvSummons.DataKeyNames = new string[] { "SumIntNo", "SSIntNo", "SumServedStatus", "SumServedDate" };
            this.dvSummons.DataSource = ds;
            this.dvSummons.PageIndex = 0;
            this.dvSummons.DataBind();

            //BD 20080509 moved from the select button
            int ssIntNo = (int)dvSummons.DataKey["SSIntNo"];
            int sumServedStatus = Convert.ToInt16(dvSummons.DataKey["SumServedStatus"]);

            DateTime sumServedDate = DateTime.MinValue;
            object dtObject = dvSummons.DataKey["SumServedDate"];
            if (dtObject != null)
                DateTime.TryParse(dtObject.ToString(), out sumServedDate);

            // Get Summons Server and a list of staff to populate the staff combo
            SummonsServerDB ssDb = new SummonsServerDB(this.connectionString);
            DataSet dsServer = ssDb.GetSummonServerStaff(ssIntNo, true, true);
            this.cmbServedBy.DataSource = dsServer;
            this.cmbServedBy.DataValueField = "SS_SIntNo";
            this.cmbServedBy.DataTextField = "FullName";
            this.cmbServedBy.DataBind();
            this.cmbServedBy.Items.Insert(0, new ListItem((string)GetLocalResourceObject("cmbServedBy.Items"), string.Empty));

            if (!this.dvSummons.Rows[11].Cells[1].Text.Equals(""))
                this.cmbServedBy.SelectedIndex = this.cmbServedBy.Items.IndexOf(this.cmbServedBy.Items.FindByText(this.dvSummons.Rows[11].Cells[1].Text));

            //this.cmbMethodServed.SelectedIndex = this.cmbMethodServed.Items.IndexOf(this.cmbMethodServed.Items.FindByValue(sumServedStatus.ToString()));
            //this.txtMethodServed.Text = sumServedStatus.ToString();
            this.txtMethodServed.Text = string.Empty;

            DisplayMethodServed();

            //this.lblSummonsServer.Text = this.dvSummons.Rows[10].Cells[1].Text;
            //this.pnlCapture.Visible = true;
            //this.lblCourtName.Text = this.dvSummons.Rows[0].Cells[1].Text;
            //this.lblCourtNo.Text = this.dvSummons.Rows[1].Cells[1].Text;

            this.summonsStatus = int.Parse(ViewState["summonsStatus"].ToString());

            if (dvSummons.DataKey["SumServedDate"].ToString() != string.Empty)
            {
                this.txtDateServed.Text = Convert.ToString(sumServedDate.Year) + Convert.ToString(sumServedDate.Month).PadLeft(2, '0') + Convert.ToString(sumServedDate.Day).PadLeft(2, '0');
            }
            else
            {
                this.txtDateServed.Text = string.Empty;
            }

            SetFocus("cmbServedBy");
        }

        protected void btnCancelSearch_Click(object sender, EventArgs e)
        {
            this.lblError.Visible = true;
            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text22");
            SetFocus("txtSummonsNo");
            return;

        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            SetDecisionPanelVisiable(false);
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            this.Server.Transfer(thisPageURL);
        }
    }
}
