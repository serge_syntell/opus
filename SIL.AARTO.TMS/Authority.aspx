<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.AuthorityPage" Codebehind="Authority.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
    <style type="text/css">
        .NormalMand
        {}
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="false">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>  "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                   </asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:UpdatePanel ID="udp" runat="server">
                        <ContentTemplate>
                            <table border="0">
                                <tr>
                                    <td valign="top" height="47">
                                        <p align="center">
                                            <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                        <p>
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:Panel ID="Panel1" runat="server">
                                            <table id="Table1" height="30" cellspacing="1" cellpadding="1" border="0">
                                                <tr>
                                                    <td >
                                                    </td>
                                                    <td width="7">
                                                    </td>
                                                    <td width="7">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                        <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label></td>
                                                    <td width="7">
                                                        <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="10"></asp:TextBox></td>
                                                    <td width="7">
                                                        <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                            OnClick="btnSearch_Click"></asp:Button></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:DataGrid ID="dgAuthority" runat="server" BorderColor="Black" AllowPaging="True"
                                            GridLines="Vertical" CellPadding="4" ShowFooter="True" HeaderStyle-CssClass="CartListHead"
                                            FooterStyle-CssClass="cartlistfooter" ItemStyle-CssClass="CartListItem" AlternatingItemStyle-CssClass="CartListItemAlt"
                                            AutoGenerateColumns="False" OnItemCommand="dgAuthority_ItemCommand" OnPageIndexChanged="dgAuthority_PageIndexChanged"
                                            CssClass="Normal">
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn Visible="False" DataField="AutIntNo" HeaderText="AutIntNo"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="AutCode" HeaderText="<%$Resources:dgAuthority.HeaderText1 %>"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="AutName" HeaderText="<%$Resources:dgAuthority.HeaderText2 %>"></asp:BoundColumn>
                                                <asp:ButtonColumn Text="<%$Resources:dgAuthorityItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <asp:Panel ID="pnlUpdateAuthority" runat="server">
                                            <table id="Table3" height="418" cellspacing="1" cellpadding="1" border="0"
                                                class="NormalBold">
                                                <tr>
                                                    <td  height="2" valign="top">
                                                        <asp:Label ID="Label19" runat="server" Width="158px" CssClass="ProductListHead" Text="<%$Resources:lblUpdAuthority.Text %>"></asp:Label></td>
                                                    <td  height="2">
                                                    </td>
                                                    <td height="2" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  height="2" valign="top">
                                                        <asp:Label ID="Label6" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblMetro.Text %>"></asp:Label></td>
                                                    <td  height="2">
                                                        <asp:DropDownList ID="ddlMetro" runat="server" Width="217px" CssClass="Normal">
                                                        </asp:DropDownList></td>
                                                    <td height="2" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="2">
                                                        <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutCode.Text %>"></asp:Label></td>
                                                    <td  height="2" valign="top">
                                                        <asp:TextBox ID="txtAutCode" runat="server" Width="41px" CssClass="NormalMand" MaxLength="5"></asp:TextBox></td>
                                                    <td height="2" valign="top">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="245px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                            ControlToValidate="txtAutCode"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutNo.Text %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutNo" runat="server" Width="72px" CssClass="NormalMand" MaxLength="6"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" Width="245px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqAuthority.Text %>"
                                                            ControlToValidate="txtAutNo"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutName.Text %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutName" runat="server" Width="228px" CssClass="NormalMand" MaxLength="100"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Width="245px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqAutName.ErromMessage %>"
                                                            ControlToValidate="txtAutName"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutNameAfri.Text %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutNameAfri" runat="server" Width="228px" CssClass="NormalMand" MaxLength="100"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                        <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="245px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="Authority name may not be left blank."
                                                            ControlToValidate="txtAutName"></asp:RequiredFieldValidator>--%></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label9" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAutPostAddr.Text %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutPostAddr1" runat="server" Width="289px" CssClass="Normal"
                                                            MaxLength="100"></asp:TextBox><br />
                                                        <asp:TextBox ID="txtAutPostAddr2" runat="server" Width="286px" CssClass="Normal"
                                                            MaxLength="100"></asp:TextBox><br />
                                                        <asp:TextBox ID="txtAutPostAddr3" runat="server" Width="287px" CssClass="Normal"
                                                            MaxLength="100"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" Width="210px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqPostalAddress.ErrorMessage %>"
                                                            ControlToValidate="txtAutPostAddr1"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutPostCode.Text %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutPostCode" runat="server" Width="72px" CssClass="Normal" MaxLength="4"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" Width="210px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:ReqPostalCode.ErrorMessage %>"
                                                            ControlToValidate="txtAutPostCode"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td height="2" valign="top">
                                                        <asp:Label ID="Label39" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutPhysAddr.Text %>"></asp:Label></td>
                                                    <td height="2" >
                                                        <asp:TextBox ID="txtAutPhysAddr1" runat="server" CssClass="NormalMand"
                                                            MaxLength="100" Width="290px"></asp:TextBox>
                                                            <br /><asp:TextBox ID="txtAutPhysAddr2" runat="server"
                                                                CssClass="Normal" MaxLength="100" Width="289px"></asp:TextBox>
                                                                <br /><asp:TextBox
                                                                    ID="txtAutPhysAddr3" runat="server" CssClass="Normal" MaxLength="100"
                                                                    Width="288px"></asp:TextBox><br />
                                                        <asp:TextBox ID="txtAutPhysAddr4" runat="server" CssClass="Normal"
                                                            MaxLength="100" Width="288px"></asp:TextBox><br />
                                                        <asp:TextBox ID="txtAutPhysCode" runat="server" CssClass="NormalMand"
                                                            MaxLength="5" Width="72px"></asp:TextBox></td>
                                                    <td height="2" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutTel.Text %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutTel" runat="server" Width="122px" CssClass="Normal" MaxLength="30"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label17" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutFax.Text %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutFax" runat="server" Width="122px" CssClass="Normal" MaxLength="30"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutEmail.Text %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutEmail" runat="server" Width="47%" CssClass="NormalMand"
                                                            MaxLength="50" Height="24px"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" CssClass="NormalRed"
                                                            ForeColor=" " Display="Dynamic" ErrorMessage="<%$Resources:ReqEmailVal.ErrorMessage %>"
                                                            ControlToValidate="txtAutEmail" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"></asp:RegularExpressionValidator></td>
                                                    <td height="25" valign="top">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" Width="236px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:lblEmailBlank.ErrorMessage %>"
                                                            ControlToValidate="txtAutEmail"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td height="25" valign="top" >
                                                        <asp:Label ID="Label29" runat="server" CssClass="NormalBold" Text="<%$Resources:lblNatisCode.Text %>"></asp:Label></td>
                                                    <td height="25" valign="top" >
                                                        <asp:TextBox ID="txtAutNatisCode" runat="server" CssClass="Normal" MaxLength="4"
                                                            Width="90px"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtAutNatisCode"
                                                            CssClass="NormalRed" ErrorMessage="<%$Resources:reqNatisCode.ErrorMsg %>" ForeColor=""
                                                            ValidationExpression="\d{4}"></asp:RegularExpressionValidator></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top"  height="25">
                                                        <asp:Label ID="Label30" runat="server" CssClass="NormalBold" Text="<%$Resources:lblNatisEmail.ErrorMsg %>"></asp:Label></td>
                                                    <td valign="top"  height="25">
                                                        <asp:TextBox ID="txtAutSendNatisEmail" runat="server" Width="47%" CssClass="NormalMand"
                                                            MaxLength="50" Height="24px"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator4" runat="server" CssClass="NormalRed"
                                                            ForeColor=" " Display="Dynamic" ErrorMessage="<%$Resources:ReqEmailVal.ErrorMessage %>"
                                                            ControlToValidate="txtAutSendNatisEmail" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"></asp:RegularExpressionValidator></td>
                                                    <td height="25" valign="top">
                                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="236px"
                                                            CssClass="NormalRed" ForeColor=" " Display="dynamic" ErrorMessage="<%$Resources:lblEmailBlank.ErrorMessage %>"
                                                            ControlToValidate="txtAutSendNatisEmail"></asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr>
                                                    <td height="25" valign="top" >
                                                        <asp:Label ID="Label24" runat="server" CssClass="NormalBold" Text="<%$Resources:lblBranchCode.Text %>"></asp:Label></td>
                                                    <td height="25" valign="top" >
                                                        <asp:TextBox ID="txtBranchCode" runat="server" CssClass="Normal" MaxLength="2"
                                                            Width="45px"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtBranchCode"
                                                            CssClass="NormalRed" ErrorMessage="<%$Resources:ReqBranchCode.ErrorMessage %>" ForeColor=""
                                                            ValidationExpression="\d{2}"></asp:RegularExpressionValidator></td>
                                                </tr>
                                                <tr>
                                                    <td height="25" valign="top" >
                                                        <asp:Label ID="Label25" runat="server" CssClass="NormalBold" Text="<%$Resources:lblBranchName.Text %>"></asp:Label></td>
                                                    <td height="25" valign="top" >
                                                        <asp:TextBox ID="txtBranchName" runat="server" CssClass="Normal" MaxLength="30"
                                                            Width="184px"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="25" valign="top" >
                                                        <asp:Label ID="Label26" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutTicketProcessor.Text %>"></asp:Label></td>
                                                    <td height="25" valign="top" >
                                                        <asp:TextBox ID="txtAutTicketProcessor" runat="server" CssClass="Normal"
                                                            MaxLength="10" Width="122px"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="25" valign="top" >
                                                        <asp:Label ID="Label27" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAutPCGateway.Text %>"></asp:Label></td>
                                                    <td height="25" valign="top" >
                                                        <asp:TextBox ID="txtAutPCGateway" runat="server" CssClass="Normal"
                                                            MaxLength="20" Width="122px"></asp:TextBox></td>
                                                    <td height="25" valign="top">
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td  valign="top">
                                                        <asp:Label ID="Label28" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblPCodingSystem.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:DropDownList ID="ddl3PCodingSystem" runat="server" Width="217px" CssClass="Normal">
                                                        </asp:DropDownList></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label23" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblReceiptID.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtReceiptID" runat="server" MaxLength="4" Width="80px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label35" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblEPAccNoLength.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtEPAccNoLength" runat="server" MaxLength="4" Width="80px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label40" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblAutNoticePaymentInfo.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtAutNoticePaymentInfo" runat="server" CssClass="NormalMand" Height="75px"
                                                            TextMode="MultiLine" Width="290px"></asp:TextBox>
                                                    </td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label41" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblAutNoticeIssuedByInfo.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtAutNoticeIssuedByInfo" runat="server" CssClass="NormalMand" Height="75px"
                                                            TextMode="MultiLine" Width="290px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblJudgementPeriod.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtJudgementPeriod" runat="server" CssClass="NormalMand" Width="80px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    <asp:RegularExpressionValidator ID="revJudgementPeriod" runat="server" CssClass="NormalRed"
                                                    ErrorMessage="<%$Resources:ReqNumericVal.ErrorMessage %>" ControlToValidate="txtJudgementPeriod" ForeColor=" "
                                                    ValidationExpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblGracePeriod.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtGracePeriod" runat="server" CssClass="NormalMand" Width="80px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" CssClass="NormalRed"
                                                    ErrorMessage="<%$Resources:ReqNumericVal.ErrorMessage %>" ControlToValidate="txtGracePeriod" ForeColor=" "
                                                    ValidationExpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblGracePeriodEmail.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtGracePeriodEmail" runat="server" CssClass="NormalMand" Width="295px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblAGEmail.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtAGEmail" runat="server" CssClass="NormalMand" Width="295px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblSFEmail.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtSFEmail" runat="server" CssClass="NormalMand" Width="295px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td valign="top" >
                                                        &nbsp;</td>
                                                    <td valign="top" >
                                                        &nbsp;</td>
                                                    <td class="Normal" valign="top">
                                                        <asp:Label ID="lblLetters" runat="server" Text="<%$Resources:lblLetters.Text %>"></asp:Label></td>
                                                </tr>
                                                
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblAutDepartName.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="AutDepartName" runat="server" CssClass="NormalMand" 
                                                            Width="295px"></asp:TextBox></td>
                                                    <td valign="top" class="Normal">
                                                        <asp:Label ID="lblSafety" runat="server" Text="<%$Resources:lblSafety.Text %>"></asp:Label>
                                                        <br />
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="lblautfoot" runat="server" CssClass="NormalBold" 
                                                            Width="150px" Text="<%$Resources:lblautfoot.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="AutFooterText1" runat="server" CssClass="NormalMand" 
                                                            Width="295px"></asp:TextBox></td>
                                                    <td valign="top" class="Normal">
                                                        <asp:Label ID="Label31" runat="server" Text="<%$Resources:lblAddressDetail.Text %>"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label14" runat="server" CssClass="NormalBold" 
                                                            Width="150px" Text="<%$Resources:lblAutFooterText2.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="AutFooterText2" runat="server" CssClass="NormalMand" 
                                                            Width="295px"></asp:TextBox></td>
                                                    <td valign="top" class="Normal">
                                                        <asp:Label ID="Label32" runat="server" Text="<%$Resources:lblTelFax.Text %>"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label20" runat="server" CssClass="NormalBold" 
                                                            Width="150px" Text="<%$Resources:lblAutOfficialName.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="AutOfficialName" runat="server" CssClass="NormalMand" 
                                                            Width="295px"></asp:TextBox></td>
                                                    <td valign="top" class="Normal">
                                                        <asp:Label ID="Label33" runat="server" Text="<%$Resources:lblEgpp.Text %>"></asp:Label></td>
                                                </tr>
                                                                             <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="lbl_afri" runat="server" CssClass="NormalBold" 
                                                            Width="150px" Text="<%$Resources:lbl_afri.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="AutDocumentFromAfri" runat="server" CssClass="NormalMand" 
                                                            Width="295px" Height="75px" TextMode="MultiLine"></asp:TextBox></td>
                                                    <td valign="top" class="Normal">
                                                        <asp:Label ID="Label34" runat="server" Text="<%$Resources:lblEgName.Text %>"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label21" runat="server" CssClass="NormalBold" 
                                                            Width="150px" Text="<%$Resources:lblAutDocumentFromEnglish.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="AutDocumentFromEnglish" runat="server" CssClass="NormalMand" 
                                                            Width="295px" Height="75px" TextMode="MultiLine"></asp:TextBox></td>
                                                    <td valign="top" class="Normal">
                                                        <asp:Label ID="Label36" runat="server" Text="<%$Resources:lblOnBehalf.Text %>"></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="Label22" runat="server" CssClass="NormalBold" 
                                                            Width="150px" Text="<%$Resources:lblLogo.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:Image id="logo" runat="server" Height="100px" Width="100px"/></td>
                                                    <td valign="top" class="Normal">
                                                        <asp:Label ID="Label37" runat="server" Text="<%$Resources:lblReceipts.Text %>"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        &nbsp;</td>
                                                    <td valign="top" ><asp:FileUpload id="upload" runat="server"/></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="lblBAK" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblBAK.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtBAK" runat="server" CssClass="NormalMand" Width="295px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="lblBankName" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblBankName.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtBankName" runat="server" CssClass="NormalMand" Width="295px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="lblBAN" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblBAN.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtBAN" runat="server" CssClass="NormalMand" Width="295px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" >
                                                        <asp:Label ID="lblBBC" runat="server" CssClass="NormalBold" Width="150px" Text="<%$Resources:lblBBC.Text %>"></asp:Label></td>
                                                    <td valign="top" >
                                                        <asp:TextBox ID="txtBBC" runat="server" CssClass="NormalMand" Width="295px"></asp:TextBox></td>
                                                    <td valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  valign="top">
                                                    </td>
                                                    <td  valign="top" align="right">
                                                        <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                            OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                                    <td valign="top">
                                                        </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
