<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.OffenceFine" Codebehind="OffenceFine.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">

</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">

    </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center" style="height: 1608px">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="138px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138p
                                x">
                                    <asp:Button ID="btnOptUpdateOffenceFine" runat="server" CssClass="NormalButton" OnClick="btnOptUpdateOffenceFine_Click"
                                        Text="<%$Resources:btnOptUpdateOffenceFine.Text %>" Width="135px" /></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptShowAll" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptShowAll.Text %>" OnClick="btnOptShowAllFines_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21" style="width: 138px">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1" style="height: 1608px">
                   
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                             <table height="482" width="568" border="0">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="699px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table id="Table1" height="30" cellspacing="1" cellpadding="1" width="542" border="0">
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelAuthority" runat="server" Width="136px" CssClass="NormalBold"
                                                    Visible="False" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:DropDownList ID="ddlSelectLA" runat="server" Width="217px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged"
                                                    Visible="False">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="Label5" runat="server" Width="163px" CssClass="NormalBold" Text="<%$Resources:SelOffenceGroup.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:DropDownList ID="ddlSelOffenceGroup" runat="server" Width="563px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelOffenceGroup_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelection" runat="server" Width="149px" CssClass="NormalBold" Text="<%$Resources:lblSelection.Text %>"></asp:Label></td>
                                            <td width="7">
                                                <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="10"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                                <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>"
                                                    OnClick="btnSearch_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <table id="Table4" cellspacing="1" cellpadding="1" width="300" border="0">
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="lblOffenceList" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblOffenceList.Text %>"></asp:Label>
                                                <asp:ListBox ID="lstOffences" runat="server" Width="251px" CssClass="Normal" Height="380px"
                                                    AutoPostBack="True" OnSelectedIndexChanged="lstOffences_SelectedIndexChanged"></asp:ListBox></td>
                                            <td valign="top" align="center" style="width: 435px">
                                                <asp:DataGrid ID="dgFines" runat="server" BorderColor="Black" PageSize="5" AutoGenerateColumns="False"
                                                    AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                                    CellPadding="4" GridLines="Vertical" OnItemCommand="dgFines_ItemCommand" CssClass="Normal">
                                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundColumn Visible="False" DataField="OFIntNo" HeaderText="OFIntNo"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="OFEffectiveDate" HeaderText="<%$Resources:dgFines.HeaderText1 %>" DataFormatString="{0:yyyy-MM-dd}">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="OFFineAmount" HeaderText="<%$Resources:dgFines.HeaderText2 %>" DataFormatString="{0:f}">
                                                        </asp:BoundColumn>
                                                        <asp:ButtonColumn Text="<%$Resources:dgFinesItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                                    </Columns>
                                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                                </asp:DataGrid>
                                                <asp:Panel ID="pnlAddOffenceFine" runat="server" Width="432px" Height="127px">
                                                    <table id="Table2" height="1" cellspacing="1" cellpadding="1" border="0">
                                                        <tr>
                                                            <td width="157" height="2">
                                                                <asp:Label ID="lblAddOffenceFine" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddOffenceFine.Text %>"></asp:Label></td>
                                                            <td height="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" width="157" height="2">
                                                                <asp:Label ID="Label8" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:AddEffectiveDate.Text %>"></asp:Label></td>
                                                            <td height="2">
                                                                
                                                                 <asp:TextBox runat="server" ID="dtpAddEffectiveDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpAddEffectiveDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="dtpAddEffectiveDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:ReqAddEffectiveDate.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" width="157" height="2">
                                                                <asp:Label ID="Label9" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddOFFineAmount.Text %>"></asp:Label></td>
                                                            <td height="2">
                                                                <asp:TextBox ID="txtAddOFFineAmount" runat="server" Width="110px" CssClass="NormalMand"
                                                                    Height="24px" MaxLength="10"></asp:TextBox>
                                                                <asp:RangeValidator ID="Rangevalidator3" runat="server" CssClass="NormalRed" ErrorMessage="<%$Resources:ReqAddOFFineAmount.ErrorMessage %>"
                                                                    ControlToValidate="txtAddOFFineAmount" MaximumValue="999999" MinimumValue="0"
                                                                    Type="Double" ForeColor=" "></asp:RangeValidator></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="157">
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAddOffenceFine" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddOffenceFine.Text %>"
                                                                    OnClick="btnAddOffenceFine_Click"></asp:Button></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlUpdateOffenceFine" runat="server" Width="432px" Height="127px">
                                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" border="0">
                                                        <tr>
                                                            <td width="102" height="2">
                                                                <asp:Label ID="Label19" runat="server" Width="153px" CssClass="ProductListHead" Text="<%$Resources:lblUpdFine.Text %>"></asp:Label></td>
                                                            <td height="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" width="102">
                                                                <asp:Label ID="Label16" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:AddEffectiveDate.Text %>"></asp:Label></td>
                                                            <td>
                                                              
                                                                  <asp:TextBox runat="server" ID="dtpOFEEffectiveDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                TargetControlID="dtpOFEEffectiveDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                            ControlToValidate="dtpOFEEffectiveDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:ReqAddEffectiveDate.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator> </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" width="102">
                                                                <asp:Label ID="Label17" runat="server" Width="154px" CssClass="NormalBold" Text="<%$Resources:lblAddOFFineAmount.Text %>"></asp:Label></td>
                                                            <td>
                                                                <asp:TextBox ID="txtOFFineAmount" runat="server" Width="109px" CssClass="NormalMand"
                                                                    Height="24px" MaxLength="10"></asp:TextBox>
                                                                <asp:RangeValidator ID="Rangevalidator7" runat="server" CssClass="NormalRed" ErrorMessage="<%$Resources:ReqAddOFFineAmount.ErrorMessage %>"
                                                                    ControlToValidate="txtOFFineAmount" MaximumValue="999999" MinimumValue="0" Type="Double"
                                                                    ForeColor=" "></asp:RangeValidator></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="102">
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                                    OnClick="btnUpdate_Click"></asp:Button></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                &nbsp;
                                                <asp:TextBox ID="txtDate" runat="server" Width="109px" CssClass="NormalMand" MaxLength="3"
                                                    Visible="False"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgOffenceFine" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                    AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgFines_ItemCommand"
                                    OnPageIndexChanged="dgOffenceFine_PageIndexChanged" CssClass="Normal">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="OFIntNo" HeaderText="OFIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="OffenceName" HeaderText="<%$Resources:dgOffenceFine.HeaderText1 %>">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="OFEffectiveDate" HeaderText="<%$Resources:dgOffenceFine.HeaderText2 %>" DataFormatString="{0:yyyy-MM-dd}">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="OFFineAmount" HeaderText="<%$Resources:dgOffenceFine.HeaderText3 %>" DataFormatString="{0:f}">
                                        </asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <asp:Panel ID="pnlOffenceGroup" runat="server" Visible="false" Width="670px">
                                    <table id="Table5" border="0" cellpadding="1" cellspacing="1">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="Label20" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblUpdFineTable.Text %>"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Text="<%$Resources:lblExisting.Text %>"></asp:Label>
                                            </td>
                                            <td width="157">
                                             
                                                  <asp:TextBox runat="server" ID="dtpEffectiveDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                TargetControlID="dtpEffectiveDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                            ControlToValidate="dtpEffectiveDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:ReqAddEffectiveDate.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator> 
                                                  </td>
                                            <td height="2" width="20">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblEffectiveDate.Text %>"></asp:Label>
                                            </td>
                                            <td width="157">
                                             
                                                 <asp:TextBox runat="server" ID="dtpUpdateToDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="CalendarExtender3" runat="server" 
                                                TargetControlID="dtpUpdateToDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                            ControlToValidate="dtpUpdateToDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:ReqAddEffectiveDate.ErrorMessage %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>  </td>
                                            <td width="20">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" style="height: 27px">
                                                &nbsp;</td>
                                            <td width="157" style="height: 27px">
                                                <asp:Button ID="btnUpdateFine" runat="server" CssClass="NormalButton" OnClick="btnUpdateFine_Click"
                                                    Text="<%$Resources:btnUpdateFine.Text %>" /></td>
                                            <td width="20" style="height: 27px">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                         </ContentTemplate>
    </asp:UpdatePanel>   </td>
     </tr>
                    </table>
                </td>
                       
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
