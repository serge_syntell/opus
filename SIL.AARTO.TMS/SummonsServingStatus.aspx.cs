using System;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    public partial class SummonsServingStatus : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo;
        private string login;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsServingStatus.aspx";
        //protected string thisPage = "Summons Served Status Report";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            if (!Page.IsPostBack)
            {
                // Get user info from session variable
                if (Session["userDetails"] == null)
                    Server.Transfer("Login.aspx?Login=invalid");
                if (Session["userIntNo"] == null)
                    Server.Transfer("Login.aspx?Login=invalid");

                // Get user details
                
                this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

                UserDetails userDetails = (UserDetails)Session["userDetails"];
                this.login = userDetails.UserLoginName;

                // Set domain specific variables
                General gen = new General();
                backgroundImage = gen.SetBackground(Session["drBackground"]);
                styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
                title = gen.SetTitle(Session["drTitle"]);

                //dls 090506 - need this for Ajax Calendar extender
                HtmlLink link = new HtmlLink();
                link.Href = styleSheet;
                link.Attributes.Add("rel", "stylesheet");
                link.Attributes.Add("type", "text/css");
                Page.Header.Controls.Add(link);

                lblError.Text = string.Empty;

                if (!Page.IsPostBack)
                {
                    this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                    this.PopulateAuthorities();

                    ddlCourtRoom.Enabled = false;
                    ddlCourtRoomDates.Enabled = false;

                    CheckDateOptions();
                }
            }
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtTicketNo.Text = this.TicketNumberSearch1.TicketNumber;
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TicketNumberSearch1.AutIntNo = int.Parse(this.ddlAuthority.SelectedValue);

            PopulateCourts();
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");


            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(this.autIntNo.ToString()));
            //reader.Close();

            this.TicketNumberSearch1.AutIntNo = autIntNo;

            PopulateCourts();
        }

        private void PopulateCourts()
        {
            CourtDB court = new CourtDB(this.connectionString);
            SqlDataReader reader = court.GetAuth_CourtListByAuth(Int32.Parse(this.ddlAuthority.SelectedValue.ToString()));
            this.ddlCourt.DataSource = reader;
            this.ddlCourt.DataValueField = "CrtIntNo";
            this.ddlCourt.DataTextField = "CrtDetails";
            this.ddlCourt.DataBind();
            reader.Close();
            this.ddlCourt.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourt.Items"), string.Empty));

            ddlCourt.Enabled = true;

            CheckCourtOptions();
        }

        protected void rlOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Load StatOps in Session Variable on Load.
            string mOptionIx = rlOptions.SelectedValue;
            this.labelError.Text = string.Empty;

            txtSummonsNo.Visible = false;
            lblSummonsNo.Visible = false;
            txtSummonsNo.Visible = false;
            lblSummonsNo.Visible = false;
            txtTicketNo.Visible = false;
            lblTicketNo.Visible = false;
            TicketNumberSearch1.Visible = false;
            lblPartialTicketNo.Visible = false;
            lblPartialOr.Visible = false;
            lblOr.Visible = false;

            switch (mOptionIx)
            {
                 case "Withdrawn":
                    {
                        this.labelError.Text = (string)GetLocalResourceObject("labelError.Text");
                        break;
                    }
                case "By Summons Number or Notice Number":
                    {
                        txtSummonsNo.Visible = true;
                        lblSummonsNo.Visible = true;
                        txtTicketNo.Visible = true;
                        lblTicketNo.Visible = true;
                        TicketNumberSearch1.Visible = true;
                        lblPartialTicketNo.Visible = true;
                        lblPartialOr.Visible = true;
                        lblOr.Visible = true;
                        break;
                    }
                case "Served and Unserved":
                case "Only Served":
                case "Only Unserved":
                case "Not returned from Clerk Of Court":
                case "Not returned from Summons Server":
                default:
                    {
                        break;
                    }
            }
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
            SetMessage(null);

            // (LF:06/11/2008) Create Options Object from User selected values.
            SumServStatusOptions options = new SumServStatusOptions();
            options.OptionSelected = rlOptions.SelectedValue;

            options.AutIntNo = int.Parse(ddlAuthority.SelectedValue);
            //options.CrtIntNo = int.Parse(ddlCourt.SelectedValue);

            var isValid = true;
            var sb = new StringBuilder();

            int num;
            if (this.ddlCourt.SelectedIndex < 1
                || !int.TryParse(this.ddlCourt.SelectedValue, out num))
            {
                isValid = false;
                sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("PleaseSelectCourt"), Environment.NewLine);
            }
            else
            {
                options.CrtIntNo = num;
            }

            if (this.rblDateOption.SelectedIndex < 0)
            {
                isValid = false;
                sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("PleaseSelectCourtDateOptions"), Environment.NewLine);
            }
            else
            {
                options.DateOption = this.rblDateOption.SelectedIndex;
                if (options.DateOption == 0)
                {
                    if (!int.TryParse(ddlCourtRoom.SelectedValue, out num))
                    {
                        //lblError.Visible = true;
                        //lblError.Text = (string)GetLocalResourceObject("labelError.Text1");
                        //return;
                        isValid = false;
                        sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("labelError.Text1"), Environment.NewLine);
                    }
                    else
                    {
                        options.CrtRIntNo = int.Parse(ddlCourtRoom.SelectedValue);
                    }

                    DateTime dt;

                    if (!DateTime.TryParse(ddlCourtRoomDates.SelectedItem.Text, out dt))
                    {
                        //lblError.Visible = true;
                        //lblError.Text = (string)GetLocalResourceObject("labelError.Text2");
                        //return;
                        isValid = false;
                        sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("labelError.Text2"), Environment.NewLine);
                    }
                    else
                    {
                        options.CourtDate = dt;
                    }
                }
                else if (options.DateOption == 1)
                {
                    DateTime start, end;
                    if (string.IsNullOrWhiteSpace(this.txtCrtDateStart.Text)
                        || string.IsNullOrWhiteSpace(this.txtCrtDateEnd.Text)
                        || !DateTime.TryParse(this.txtCrtDateStart.Text, out start)
                        || !DateTime.TryParse(this.txtCrtDateEnd.Text, out end))
                    {
                        isValid = false;
                        sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("PleaseSelectStartDateAndEndDate"), Environment.NewLine);
                    }
                    else
                    {
                        if (start <= end)
                        {
                            options.CrtDateStart = start.Date;
                            options.CrtDateEnd = end.AddDays(1).Date;
                        }
                        else
                        {
                            options.CrtDateStart = end.Date;
                            options.CrtDateEnd = start.AddDays(1).Date;
                        }
                    }
                }
            }

            options.OrderBy = int.Parse(rlOrderBy.SelectedValue);

            if (rlOptions.SelectedValue == "By Summons Number or Notice Number")
            {
                if (!txtSummonsNo.Text.Trim().Equals(string.Empty))
                    options.SummonsNo = txtSummonsNo.Text;
                else if (!txtTicketNo.Text.Trim().Equals(string.Empty))
                    options.NoticeNo = txtTicketNo.Text;
                else
                {
                    //lblError.Visible = true;
                    //lblError.Text = (string)GetLocalResourceObject("labelError.Text3"); 
                    //return;
                    isValid = false;
                    sb.AppendFormat("<li>{0}</li>{1}", GetLocalResourceObject("labelError.Text3"), Environment.NewLine);
                }
            }

            if (!isValid)
            {
                SetMessage(sb.ToString());
                return;
            }

            if (Session["SumStatusOptions"] == null)
                Session.Add("SumStatusOptions", options);
            else
            {
                Session.Remove("SumStatusOptions");
                Session.Add("SumStatusOptions", options);
            }

            // (LF:06/11/2008) Build the Pop-up page.  
            PageToOpen page = new PageToOpen(this, "SummonsServingViewer.aspx");
            Helper_Web.BuildPopup(page);
            //PunchStats805806 enquiry SummonsServedStatusReport
        }

        protected void ddlCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourt.SelectedIndex > 0)
            {
                PopulateCourtRooms(Convert.ToInt32(ddlCourt.SelectedValue));
            }
            else
            {
                //Jerry 2012-07-09 change
                //ddlCourtRoom.SelectedIndex = 0;
                ddlCourtRoom.Items.Clear();
            }

            CheckCourtOptions();
        }

        private void PopulateCourtRooms(int nCrtIntNo)
        {
            //Jerry 2012-07-09 add
            ddlCourtRoom.Items.Clear();

            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            SqlDataReader reader = room.GetCourtRoomListByCrtRStatusFlag(nCrtIntNo);//room.GetCourtRoomList(nCrtIntNo);

            this.ddlCourtRoom.DataSource = reader;
            this.ddlCourtRoom.DataValueField = "CrtRIntNo";
            this.ddlCourtRoom.DataTextField = "CrtRoomDetails";
            this.ddlCourtRoom.DataBind();
            reader.Close();

            //Heidi 2013-09-23 removed translate from lookup table
            //Dictionary<int, string> lookups =
            //    CourtRoomLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            //while (reader.Read())
            //{
            //    int crtRIntNo = (int)reader["CrtRIntNo"];
            //    if (lookups.ContainsKey(crtRIntNo))
            //    {
            //        ddlCourtRoom.Items.Add(new ListItem(lookups[crtRIntNo], crtRIntNo.ToString()));
            //    }
            //}
            ////this.ddlCourtRoom.DataSource = reader;
            ////this.ddlCourtRoom.DataValueField = "CrtRIntNo";
            ////this.ddlCourtRoom.DataTextField = "CrtRoomDetails";
            ////this.ddlCourtRoom.DataBind();
            //reader.Close();

            this.ddlCourtRoom.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourtRoom.Items"), "0"));
            ddlCourtRoom.Enabled = true;
        }

        protected void ddlCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourtRoom.SelectedIndex > 0)
            {
                PopulateCourtRoomDates(Convert.ToInt32(ddlCourtRoom.SelectedValue));
            }
            else
            {
                ddlCourtRoom.SelectedIndex = 0;
            }
        }

        private void PopulateCourtRoomDates(int crtRIntNo)
        {
            CourtDatesDB db = new CourtDatesDB(this.connectionString);
            SqlDataReader reader = db.GetCourtDatesList(crtRIntNo, Convert.ToInt32(ddlAuthority.SelectedValue));
            this.ddlCourtRoomDates.DataSource = reader;
            this.ddlCourtRoomDates.DataValueField = "CDate";
            this.ddlCourtRoomDates.DataTextField = "CDate";
            this.ddlCourtRoomDates.DataTextFormatString = "{0:yyyy-MM-dd}";
            this.ddlCourtRoomDates.DataBind();
            reader.Close();
            this.ddlCourtRoomDates.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourtRoomDates.Items"), "0"));

            ddlCourtRoomDates.Enabled = true;
        }

        #region date range, Oscar 2013-05-09

        void SetMessage(string message)
        {
            this.lblError.Visible = !string.IsNullOrWhiteSpace(message);
            this.lblError.Text = message;
        }

        void CheckCourtOptions()
        {
            var selectedIndex = this.ddlCourt.SelectedIndex;
            this.rblDateOption.Enabled = selectedIndex >= 1;
            if (selectedIndex < 1)
                this.rblDateOption.ClearSelection();
            CheckDateOptions();
        }

        void CheckDateOptions()
        {
            var selectedIndex = this.rblDateOption.SelectedIndex;
            this.tbdSpecific.Visible = false;
            this.tbdRange.Visible = false;

            switch (selectedIndex)
            {
                case 0:
                    this.tbdSpecific.Visible = true;
                    break;
                case 1:
                    this.tbdRange.Visible = true;
                    break;
            }
        }

        protected void rblDateOption_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckDateOptions();
        }

        #endregion
    }
}


