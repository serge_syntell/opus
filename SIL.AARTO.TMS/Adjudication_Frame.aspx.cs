using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the main page for viewing and performing adjudication on a per frame basis.
    /// </summary>
    public partial class Adjudication_Frame : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private int filmIntNo = 0;
        private int processType = 1;          //1 = Adjudication; no longer any need for Correction
        private int statusProsecute = 600;
        private int statusCancel = 999;
        private int statusNatis = 50;           // not sure why this was 100;

        private int statusVerified = 500;
        private int statusRejected = 999;

        //private bool useJp2 = false;
        private bool isNaTISLast = true;
        private FrameList frames = null;
        private bool adjAt100 = false;

        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        protected string thisPage = "Adjudication";
        protected string thisPageURL = "Adjudication_Frame.aspx";

        protected string adjOfficerNo = string.Empty;
        protected string reqOfficerNo = "Y";
        private bool bFirstLoad = false;
        protected bool _ISASD = false;
        protected int asdInvalidRejIntNo = 0;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>

        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //this.useJp2 = (bool)Application["UseJP2Conversion"];

            // Get user details
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.loginUser = userDetails.UserLoginName;

            ddlRejection.Attributes.Add("OnChange", "doChange();");

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            HtmlMeta metaKeywords = new HtmlMeta();
            metaKeywords.Attributes.Add("name", "Keywords");
            metaKeywords.Attributes.Add("content", keywords);
            Page.Header.Controls.Add(metaKeywords);

            HtmlMeta metaDescription = new HtmlMeta();
            metaDescription.Attributes.Add("name", "Description");
            metaDescription.Attributes.Add("content", description);
            Page.Header.Controls.Add(metaDescription);

            if (Session["autIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No authority has been selected - please login correctly";
                return;
            }

            if (Session["filmIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No film has been selected - please login correctly";
                return;
            }

            //dls 070403 - changed back to Session variables again! Needed for remote invoke

            // FBJ Added (2007-03-09): Moved the frame list into the ViewState
            //if (this.ViewState["FrameList"] == null)
            //{
            //    if (this.Session["FrameList"] == null)
            //    {
            //        lblError.Visible = true;
            //        lblError.Text = "No Frame list could be found";
            //        return;
            //    }
            //    else
            //    {
            //        this.ViewState.Add("FrameList", (FrameList)this.Session["FrameList"]);
            //        this.Session.Remove("FrameList");
            //    }
            //}
            //this.frames = (FrameList)this.ViewState["FrameList"];

            //dls 090618 - add this into the session variable, so that we only ahve to do it once, not for every frame!
            if (Session["adjAt100"] == null)
            {
                this.adjAt100 = GetRuleFullAdjudicated();
                Session.Add("adjAt100", this.adjAt100);
            }
            else
            {
                this.adjAt100 = (bool)Session["adjAt100"];
            }

            if (this.Session["FrameList"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No Frame List could be found.";
                return;
            }
            this.frames = (FrameList)Session["FrameList"];

            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            this.filmIntNo = Convert.ToInt32(Session["filmIntNo"]);

            if (Session["processType"] != null)
                processType = Convert.ToInt32(Session["processType"]);

            // FBJ Added (2006-12-15): Retrieve the NaTIS last flag value
            this.isNaTISLast = (bool)this.Session["NaTISLast"];
            if (this.isNaTISLast)
            {
                this.statusVerified = 800;
                this.statusProsecute = 710;
                this.statusNatis = 710;
            }

            //dls 061120 - add check for requirement of stamping adj. officer no on each frame (CiprusPI request)
            //int arCode = 0;
            //AuthorityRulesDB authRules = new AuthorityRulesDB(this.connectionString);
            //int arIntNo = authRules.GetAuthorityRulesByCode(autIntNo, "0560", "Require officer no. to be entered at adjudication", ref arCode, ref reqOfficerNo, "Y = Yes, N = No", loginUser);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = this.autIntNo;
            rule.ARCode = "0560";
            rule.LastUser = loginUser;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> requireOfficerNoRule = authRule.SetDefaultAuthRule();
            //arCode = value.Key;
            reqOfficerNo = requireOfficerNoRule.Value;

            if (reqOfficerNo.Equals("Y"))
            {
                if (Session["OfficerNo"] == null)
                {
                    lblError.Visible = true;
                    lblError.Text = "Incorrect/missing adjudicating officer no. - unable to proceed. Please return to the main menu";
                    return;
                }
                
                adjOfficerNo = Session["OfficerNo"].ToString();
            }

            //dls 090618 - add this into the session variable, so that we only ahve to do it once, not for every frame!
            if (Session["asdInvalidRejIntNo"] == null)
            {
                this.asdInvalidRejIntNo = GetASDInvalidRejIntNo();
                Session.Add("asdInvalidRejIntNo", this.asdInvalidRejIntNo);
            }
            else
            {
                this.asdInvalidRejIntNo = (int)Session["asdInvalidRejIntNo"];
            }

            if (!Page.IsPostBack)
            {
                //dls 090618 - add this into the session variable, so that we only ahve to do it once, not for every frame!
                this.adjAt100 = GetRuleFullAdjudicated();
                Session.Add("adjAt100", this.adjAt100);

                this.lblPageName.Text = thisPage;
                // TODO: add an authority / system rule to show hide navigation
                //this.pnlNavigation.Visible = false;

                this.PopulateVehicleMakes();
                this.PopulateVehicleTypes();
                this.PopulateRejectionReasons();

                this.GetNextFrameToAdjudicate();
                bFirstLoad = true;
                this.ShowSaveImageSettingButton();
            }

            ////2010-02-08 Branch 4307
            //if (this.adjAt100)
            //    btnAccept.Attributes["onclick"] = "WarningProsecute();";
            //else
                btnAccept.Attributes["onclick"] = "Action('AdjudicateFrame'); OnRemoteInvoke();";

            btnChange.Attributes["onclick"] = "AllowChange(); ";
            btnResendNATIS.Attributes["onclick"] = "Action('ReturnToNatisAdj'); OnRemoteInvoke();";

            if (this.adjAt100 && !bFirstLoad)
                ScriptManager.RegisterStartupScript(udpFrame,
                        udpFrame.GetType(), "UpdatePanelStartUpJSHI", "rejectionReasonChange();", true);
            
        }

        private int GetASDInvalidRejIntNo()
        {
            RejectionDB rejDB = new RejectionDB(this.connectionString);
            //add ASD speed rejection reason
            string asdInvalidSetup = "ASD Camera Setup Invalid";

            return rejDB.UpdateRejection(0, asdInvalidSetup, "N", this.loginUser, 0, "Y");
        }

        private bool GetRuleFullAdjudicated()
        {
            //tf 20090611 - get AuthorityRule for 100% adjuducated
            //AuthorityRulesDB arDB = new AuthorityRulesDB(connectionString);
            //AuthorityRulesDetails arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "0600", "Rule to ensure that 100% of frames are Adjudicated",
            //    0, "N", "Y - Yes; N - No(Default)", userName);

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();

            arDetails.AutIntNo = this.autIntNo;
            arDetails.ARCode = "0600";
            arDetails.LastUser = this.loginUser;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            KeyValuePair<int, string> adj100 = ar.SetDefaultAuthRule();
            return adj100.Value.Equals("Y");
        }

        private void GetNextFrameToAdjudicate()
        {
            int frameIntNo = this.frames.Current;
            if (frameIntNo > 0)
            {
                this.ShowFrameDetails(frameIntNo);
                this.lblNavigation.Text = string.Format("Navigating - {0}", this.frames);
            }
            else
            {
                string natisLast = this.isNaTISLast.Equals(true) ? "Y" : "N";
                string errMessage = string.Empty;

                FilmDB film = new FilmDB(this.connectionString);
                film.FinaliseFilmAdjudication(this.filmIntNo, natisLast, loginUser, ref errMessage);

                if (!errMessage.Equals(""))
                {
                    lblError.Text = errMessage;
                    lblError.Visible = true;
                    return;
                }

                film.LockUser(0, loginUser);

                //Session["cvPhase"] = 2;
                Session.Remove("cvPhase");
                Session["processType"] = 3;
                this.Session.Remove("FrameList");

                Response.Redirect("Verification_PreFilm.aspx?process=3");
            }
        }

        private void ShowFrameDetails(int frameInt)
        {
            //this.btnAccept.Disabled = true;

            this.txtRegNo.ReadOnly = true;
            this.txtFirstSpeed.ReadOnly = true;
            this.txtSecondSpeed.ReadOnly = true;
            this.txtOffenceDate.ReadOnly = true;
            this.txtOffenceTime.ReadOnly = true;

            this.ddlRejection.Enabled = false;
            this.ddlVehMake.Enabled = false;
            this.ddlVehType.Enabled = false;
            this.lblFilmNo.Text = "Film no: " + this.frames.FilmNumber;

            this.chkAllowContinue.Style["display"] = "none";
            this.chkAllowContinue.Checked = true;
            this.lblError.Style["display"] = string.Empty;
            this.lblError.Text = string.Empty;

            if (frameInt > 0)
            {
                this.SetIFrame(frameInt, true);

                FrameDB frame = new FrameDB(this.connectionString);
                FrameDetails frameDet = frame.GetFrameDetails(this.frames.Current);

                this.txtFrameNo.Text = frameDet.FrameNo;
                this.txtSeq.Text = frameDet.Sequence;
                this.txtRegNo.Text = frameDet.RegNo;
                this.lblSpeedZone.Text = frameDet.SpeedLimit.ToString();
                this.lblLocationDescr.Text = frameDet.LocDescr;
                this.lblOfficerDetails.Text = frameDet.OfficerDetails;
                
                //dls 070517 - add these to hidden fields and no longer store in Session variables
                this.hidFrameIntNo.Value = frameInt.ToString();
                this.hidPreUpdatedRegNo.Value = frameDet.RegNo;
                this.hidRowVersion.Value = frameDet.RowVersion.ToString();

                this.txtSecondSpeed.Text = frameDet.SecondSpeed.ToString();
                this.txtFirstSpeed.Text = frameDet.FirstSpeed.ToString();

                //jerry 2011-08-22 add these to visual alert on high speed at adjudication
                int lowerSpeed = frameDet.FirstSpeed > frameDet.SecondSpeed ? frameDet.SecondSpeed : frameDet.FirstSpeed;
                if (lowerSpeed > frameDet.SpeedLimit + 40)
                {
                    lblSpeedInfo1.Text = lowerSpeed.ToString();
                    speedMessagePanel.Visible = true;
                    speedMessagePane2.Visible = false;
                    //System.Media.SystemSounds.Exclamation.Play();
                    //System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                    //player.SoundLocation = Server.MapPath("~/Images/Exclamation.wav");
                    //player.Load();
                    //player.Play();
                    //cmdPlay.Value = "1";
                    liMusic.Text = "<embed src='images/Exclamation.wav' autostart='true' width='0' height='0' loop='false'></embed>";
                }
                else
                {
                    lblSpeedInfo2.Text = lowerSpeed.ToString();
                    speedMessagePanel.Visible = false;
                    speedMessagePane2.Visible = true;
                    liMusic.Text = "";
                }

                string month = frameDet.OffenceDate.Month.ToString();
                string day = frameDet.OffenceDate.Day.ToString();
                string hour = frameDet.OffenceDate.Hour.ToString();
                string minute = frameDet.OffenceDate.Minute.ToString();

                this.txtOffenceDate.Text = frameDet.OffenceDate.Year + "/" + month.PadLeft(2, '0') + "/" + day.PadLeft(2, '0'); ;
                this.txtOffenceTime.Text = hour.PadLeft(2, '0') + ":" + minute.PadLeft(2, '0');

                this.ddlRejection.SelectedIndex = ddlRejection.Items.IndexOf(ddlRejection.Items.FindByValue(frameDet.RejIntNo.ToString()));
                this.ddlVehMake.SelectedIndex = ddlVehMake.Items.IndexOf(ddlVehMake.Items.FindByValue(frameDet.VMIntNo.ToString()));
                this.ddlVehType.SelectedIndex = ddlVehType.Items.IndexOf(ddlVehType.Items.FindByValue(frameDet.VTIntNo.ToString()));


                //FT 100521 ASD
                if (frameDet.ASD2ndCameraIntNo > 0)
                {
                    tableASD.Visible = true;
                    tbASDGPSDatetime1.Text = string.Format("{0:yyyy-MM-dd HH:mm}", frameDet.ASDGPSDateTime1);
                    tbASDGPSDatetime2.Text = string.Format("{0:yyyy-MM-dd HH:mm}", frameDet.ASDGPSDateTime2);
                    tbASDLane1.Text = frameDet.ASDSectionStartLane.ToString();
                    tbASDLane2.Text = frameDet.ASDSectionEndLane.ToString();

                    RejectionDB rejDB = new RejectionDB(this.connectionString);
                    //add ASD speed rejection reason
                    string asdInvalidSetup = "ASD Camera Setup Invalid";

                    int asdInvalidRejIntNo = rejDB.UpdateRejection(0, asdInvalidSetup, "N", this.loginUser, 0, "Y");

                    if (frameDet.RejIntNo == asdInvalidRejIntNo)
                    {
                        _ISASD = true;
                    }
                }
                else
                {
                    tableASD.Visible = false;
                }

                //if (GetRuleFullAdjudicated(autIntNo, loginUser) == "Y")
                if (this.adjAt100)
                {
                    pnlNatisData.Visible = true;
                    DisplayNATISData(frameDet);
                }

                //Session["RejReason"] = ddlRejection.SelectedItem.Text;
                this.hidPreUpdatedRejReason.Value = ddlRejection.SelectedItem.Text;
                //hidRejIntNo.Value = ddlRejection.SelectedValue;

                if (ddlRejection.SelectedItem.Text.Equals("None"))
                {
                    this.btnAccept.Value = "Prosecute";
                    this.ddlRejection.Style["display"] = "none";
                    this.lblRejection.Style["display"] = "none";
                }
                else
                {
                    //this.btnAccept.Value = this.adjAt100 ? "Prosecute" : "Don't prosecute";
                    this.btnAccept.Value = "Don't prosecute";
                    this.btnChange.Disabled = this.ddlRejection.SelectedItem.Text.ToLower().StartsWith("expired") ? true : false;
                    this.btnResendNATIS.Disabled = this.ddlRejection.SelectedItem.Text.ToLower().StartsWith("expired") ? true : false;
                    this.ddlRejection.Style["display"] = string.Empty;
                    this.lblRejection.Style["display"] = string.Empty;
                }

                this.hidPreUpdatedAllowContinue.Value = frameDet.AllowContinue;

                if (frameDet.AllowContinue.Equals("N"))
                {
                    lblError.Text = frameDet.ConfirmError;
                    chkAllowContinue.Checked = false;
                    this.chkAllowContinue.Style["display"] = string.Empty;
                }
            }
            else
            {
                Session["cvPhase"] = 2;
                Session["processType"] = 3;
                Response.Redirect("Verification_PreFilm.aspx?process=3");
            }

            hidStatusRejected.Value = this.statusRejected.ToString();
            hidStatusVerified.Value = this.statusVerified.ToString();

            hidStatusCancel.Value = this.statusCancel.ToString();
            hidStatusProsecute.Value = this.statusProsecute.ToString();
            hidStatusNatis.Value = this.statusNatis.ToString();
            hidUserName.Value = this.loginUser;

            Session["frameList"] = this.frames;
        }

        /// <summary>
        /// Display NATIS Data
        /// </summary>
        private void DisplayNATISData(FrameDetails frameDet)
        {
            //20090615 tf Display NATIS data
            VehicleMakeDB vm = new VehicleMakeDB(connectionString);
            VehicleMakeDetails vmDet = vm.GetVehicleMakeDetailsByCode(frameDet.FrameNatisVMCode, "N");

            if (vmDet.VMDescr != null)
                labNatisVehicleMake.Text = "Vehicle Make: " + vmDet.VMDescr;
            else
            {
                labNatisVehicleMake.Text = "Vehicle Make: " + frameDet.FrameNatisVMCode + " - No matching vehicle make in database";
            }

            VehicleTypeDB vt = new VehicleTypeDB(connectionString);
            VehicleTypeDetails vtDet = vt.GetVehicleTypeDetailsByCode(frameDet.FrameNatisVTCode, "N");
            if (vtDet.VTDescr != null)
                labNatisVehicleType.Text = "Vehicle Type: " + vtDet.VTDescr;
            else
            {
                labNatisVehicleType.Text = "Vehicle Type: " + frameDet.FrameNatisVTCode + " - No matching vehicle type in database";
            }

            if (frameDet.FrameNatisRegNo == null || frameDet.FrameNatisRegNo.Length < 1)
            {
                labRegistrationNo.Text = "No Natis registration details";
                Session["NatisData"] = false;
            }
            else
            {
                labRegistrationNo.Text = "Registration no:" + frameDet.FrameNatisRegNo;
                Session["NatisData"] = true;
            }
        }

        //private string GetRuleFullAdjudicated(int autIntNo, string userName)
        //{
        //    //tf 20090611 - get AuthorityRule for 100% adjuducated
        //    AuthorityRulesDB arDB = new AuthorityRulesDB(connectionString);
        //    AuthorityRulesDetails arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "0600", "Rule to ensure that 100% of frames are Adjudicated",
        //        0, "N", "Y - Yes; N - No(Default)", userName);

        //    return arDetails.ARString;
        //}

        private void SetIFrame(int frameInt, bool show)
        {
            if (show)
            {
                // Image Viewer Control
                this.imageViewer1.FilmNo = this.frames.FilmNumber;
                this.imageViewer1.FrameIntNo = frameInt;
                this.imageViewer1.ScanImageIntNo = 0;
                this.imageViewer1.ImageType = "A";
                this.imageViewer1.FilmIntNo = this.filmIntNo;
                this.imageViewer1.Phase = this.Session["cvPhase"] == null ? 1 : Convert.ToInt32(this.Session["cvPhase"]);
                this.imageViewer1.Initialise();

                string strURLs = imageViewer1.GetParameters();

                ScriptManager.RegisterStartupScript(udpFrame,
                        udpFrame.GetType(), "UpdatePanelChangeImageViewer", "LoadImage(" + strURLs + ");", true);
            }
        }

        protected void PopulateVehicleMakes()
        {
            VehicleMakeDB make = new VehicleMakeDB(connectionString);
            SqlDataReader reader = make.GetVehicleMakeList(string.Empty);

            ddlVehMake.DataSource = reader;
            ddlVehMake.DataValueField = "VMIntNo";
            ddlVehMake.DataTextField = "VMDescr";
            ddlVehMake.DataBind();

            reader.Close();
        }

        protected void PopulateVehicleTypes()
        {
            VehicleTypeDB type = new VehicleTypeDB(connectionString);
            SqlDataReader reader = type.GetVehicleTypeList(0, string.Empty);

            ddlVehType.DataSource = reader;
            ddlVehType.DataValueField = "VTIntNo";
            ddlVehType.DataTextField = "VTDescr";
            ddlVehType.DataBind();

            reader.Close();
        }

        protected void PopulateRejectionReasons()
        {
            RejectionDB reason = new RejectionDB(connectionString);
            SqlDataReader reader = reason.GetRejectionList();

            ddlRejection.DataSource = reader;
            ddlRejection.DataValueField = "RejIntNo";
            ddlRejection.DataTextField = "RejReason";
            ddlRejection.DataBind();

            reader.Close();
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            this.frames.GetPreviousFrame();
            this.GetNextFrameToAdjudicate();
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            this.frames.GetNextFrame(false);
            this.GetNextFrameToAdjudicate();
        }

        #region 091013 FT Handle SaveImageSetting
        protected void btnSaveImageSetting_Click(object sender, EventArgs e)
        {
            if (imageViewer1.SaveImageSetting() > 0)
            {
                lblError.Text = "Image contrast and/or brightness settings saved";
            }
            else
            {
                lblError.Text = "Image contrast and/or brightness settings failed";
            }
        }

        private void ShowSaveImageSettingButton()
        {
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = Convert.ToInt32(Session["autIntNo"]);
            arDetails.ARCode = "2560";
            arDetails.LastUser = this.loginUser;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            ar.SetDefaultAuthRule();
            if (arDetails.ARString == "Y")
            {
                chkSaveImageSettings.Visible = true;
                btnSaveSettingsForImage.Visible = true;
            }
            else
            {
                chkSaveImageSettings.Visible = false;
                btnSaveSettingsForImage.Visible = false;
            }
        }
        
        protected void chkSaveImageSettings_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSaveImageSettings.Checked)
            {
                chkSaveImageSettings.BackColor = System.Drawing.Color.Red;
                chkSaveImageSettings.ForeColor = System.Drawing.Color.White;
                if (imageViewer1.SaveImageSetting() > 0)
                {
                    imageViewer1.SaveImageSettingsToSession();
                    lblError.Text = "Image contrast and/or brightness settings saved. All following prosecuted frames will be saved with the same settings. Uncheck 'Save Image Settings for Frames on Film' to switch off.";
                }
                else
                {
                    lblError.Text = "Setting of Image contrast and/or brightness settings failed";
                }
            }
            else
            {
                imageViewer1.ClearSettings();
                chkSaveImageSettings.BackColor = System.Drawing.Color.White;
                chkSaveImageSettings.ForeColor = System.Drawing.Color.Black;
            }
        }

        #endregion

    }
}
