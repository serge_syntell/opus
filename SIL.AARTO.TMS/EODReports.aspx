<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.EODReports" Codebehind="EODReports.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" style="text-align: center; width: 500px">
                    <asp:UpdatePanel ID="upd" runat="server">
                        <ContentTemplate>
                            <table style="border-style: none;" class="Normal">
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Panel ID="pnlTitle" runat="Server">
                                            <asp:Label ID="lblPageName" runat="server" Width="500px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                        </asp:Panel>
                                        
                                    </td>
                                </tr>
                               <tr>
                                    <td align="left" colspan="1">
                                            <asp:Label ID="lblError" CssClass="NormalRed" runat="server" />                                   
                                    </td>
                                </tr> 
                                <tr>
                                    <td align="left" style="vertical-align: top; width: 200px; height: 24px;">
                                        <asp:Label ID="Label4" runat="server" Width="200px" CssClass="NormalBold" Text="<%$Resources:lblSelectLA.Text %>"></asp:Label>
                                    </td>
                                    <td style="vertical-align: top; width: 200px; height: 24px;">
                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                 Width="200px">
                                            </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="vertical-align: top; width: 200px; height: 24px;">
                                        <asp:Label ID="Label2" runat="server" Width="200px" CssClass="NormalBold" Text="<%$Resources:lblcashbox.Text %>"></asp:Label>
                                    </td>
                                    <td style="vertical-align: top; width: 200px; height: 24px;">
                                        <asp:DropDownList ID="ddlCashBox" runat="server" AutoPostBack="true" 
                                            Width="200px" CssClass="Normal">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="vertical-align: top; width: 200px">
                                        <asp:Label ID="Label3" runat="server" Width="200px" CssClass="NormalBold" Text="<%$Resources:lblDate.Text %>"></asp:Label></td>
                                    <td style="vertical-align: top; width: 300px">
                                        <asp:Calendar ID="calEODReportDate" runat="server" CellPadding="4" DayNameFormat="Shortest"
                                            Font-Names="Verdana" Font-Size="8pt" Height="180px" NextMonthText=">" PrevMonthText="<"
                                            Width="200px">
                                            <SelectedDayStyle CssClass="NormalRed" Font-Bold="True" />
                                            <TodayDayStyle CssClass="NormalButton" />
                                            <SelectorStyle CssClass="NormalButton" />
                                            <WeekendDayStyle CssClass="CartListItemAlt" />
                                            <OtherMonthDayStyle CssClass="Normal" ForeColor="Gray" />
                                            <NextPrevStyle CssClass="Normal" VerticalAlign="Bottom" />
                                            <DayHeaderStyle CssClass="CartListHead" Font-Size="7pt" />
                                            <TitleStyle CssClass="NormalButton" />
                                            <DayStyle CssClass="Normal" />
                                        </asp:Calendar>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 25px; ">
                                    </td>
                                    <td align="center" style=" height: 25px;">
                                        <asp:Button ID="btnSubmit" runat="server" Text="<%$Resources:btnSubmit.Text %>" CssClass="NormalButton" OnClick="btnSubmit_Click"
                                            Width="104px" /></td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label5"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
