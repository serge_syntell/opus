using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    // Jake 2013-09-12 move delete button to edit panel
    public partial class ReportNamesPage : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string thisPageURL = "ReportNames.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected int autIntNo = 0;
        //protected string thisPage = "Report page maintenance (system admin use only)";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;


            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //if (userAccessLevel < 10)
            //Server.Transfer(Session["prevPage"].ToString());

            //dls 080107 - allow admin usage
            //if (userAccessLevel < 9)
            //    Server.Transfer("Default.aspx");

            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddReportName.Visible = false;
                pnlCopy.Visible = false;
                pnlUpdateReportName.Visible = false;
                //btnOptDelete.Visible = false;

                PopulateAuthorites(ddlSelectLA, autIntNo);

                if (autIntNo > 0)
                {
                    BindGrid(autIntNo);
                }
                else
                {
                    //Modefied By Henry 2012-03-06
                    //Desc: Mulitiple Language use resource file lblError.Text1 - lblError.Text13
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                    lblError.Visible = true;
                }
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        protected void PopulateAuthorites(DropDownList ddlAuth, int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuth.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }

            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlAuth.DataSource = data;
            //ddlAuth.DataValueField = "AutIntNo";
            //ddlAuth.DataTextField = "AutName";
            //ddlAuth.DataBind();

            ddlAuth.SelectedIndex = ddlAuth.Items.IndexOf(ddlAuth.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();

        }

        protected void BindGrid(int autIntNo)
        {
            Stalberg.TMS.AuthReportNameDB arnList = new Stalberg.TMS.AuthReportNameDB(connectionString);

            int totalCount = 0;
            DataSet data = arnList.GetAuthReportNameListDS(autIntNo, dgReportNamesPager.PageSize, dgReportNamesPager.CurrentPageIndex, out totalCount);
            dgReportNames.DataSource = data;
            dgReportNames.DataKeyField = "ARNIntNo";
            dgReportNamesPager.RecordCount = totalCount;

            try
            {
                dgReportNames.DataBind();
            }
            catch
            {
                dgReportNames.CurrentPageIndex = 0;
                dgReportNames.DataBind();
            }

            if (dgReportNames.Items.Count == 0)
            {
                dgReportNames.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            }
            else
            {
                dgReportNames.Visible = true;
            }

            data.Dispose();
            pnlAddReportName.Visible = false;
            pnlUpdateReportName.Visible = false;
            pnlCopy.Visible = false;
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlAddReportName.Visible = true;
            pnlUpdateReportName.Visible = false;
            //btnOptDelete.Visible = false;
            pnlCopy.Visible = false;
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            dgReportNamesPager.CurrentPageIndex = 1;
            dgReportNamesPager.RecordCount = 0;
            BindGrid(autIntNo);

        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgReportNames.Visible.Equals(true))
            {
                //hide it
                dgReportNames.Visible = false;
                //Jake 2013-09-12 added to hiden pager control
                this.dgReportNamesPager.Visible = false;

                btnOptHide.Text = (string)GetLocalResourceObject("btnOptShow.Text");
            }
            else
            {
                //show it
                dgReportNames.Visible = true;
                this.dgReportNamesPager.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        protected void ShowReportNamesDetails(int editARNIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.AuthReportNameDB arn = new Stalberg.TMS.AuthReportNameDB(connectionString);

            Stalberg.TMS.AuthReportNameDetails arnDetails = arn.GetAuthReportNameDetails(editARNIntNo);

            txtARNFunction.Text = arnDetails.ARNFunction;
            txtARNReportPage.Text = arnDetails.ARNReportPage;
            ARNTemp.Text = arnDetails.ARNTemplate;
            pnlUpdateReportName.Visible = true;
            pnlAddReportName.Visible = false;

            //btnOptDelete.Visible = true;
        }

        protected void btnAddReportNames_Click(object sender, System.EventArgs e)
        {
            // add the new transaction to the database table
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            Stalberg.TMS.AuthReportNameDB arnAdd = new AuthReportNameDB(connectionString);

            int addARNIntNo = arnAdd.AddAuthReportName(autIntNo, txtAddARNReportPage.Text, txtAddARNFunction.Text, loginUser, this.ArnTemple.Text.Trim());

            if (addARNIntNo <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ReportNamesMaintenance, PunchAction.Add);  

            }

            lblError.Visible = true;
            // Jake 2013-09-12  comment out because the page index should to stay at current page index
            //dgReportNamesPager.CurrentPageIndex = 1;
            //dgReportNamesPager.RecordCount = 0;
            if (this.dgReportNamesPager.Visible == false)
            {
                this.dgReportNamesPager.Visible = true;
            }
            BindGrid(autIntNo);

        }

        protected void btnUpdateReportNames_Click(object sender, System.EventArgs e)
        {
            int arnIntNo = Convert.ToInt32(Session["editARNIntNo"]);

            // add the new transaction to the database table
            Stalberg.TMS.AuthReportNameDB arnUpdate = new AuthReportNameDB(connectionString);

            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            int updARNIntNo = arnUpdate.UpdateAuthReportName(arnIntNo, autIntNo, txtARNReportPage.Text, txtARNFunction.Text, loginUser, this.ARNTemp.Text.Trim());

            if (updARNIntNo <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ReportNamesMaintenance, PunchAction.Change); 
            }

            lblError.Visible = true;
            // Jake 2013-09-12  comment out because the page index should to stay at current page index
            //dgReportNamesPager.CurrentPageIndex = 1;
            //dgReportNamesPager.RecordCount = 0;
            if (this.dgReportNamesPager.Visible == false)
            {
                this.dgReportNamesPager.Visible = true;
            }
            BindGrid(autIntNo);

        }

        protected void dgReportNames_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgReportNames.SelectedIndex = e.Item.ItemIndex;

            if (dgReportNames.SelectedIndex > -1)
            {
                int editARNIntNo = Convert.ToInt32(dgReportNames.DataKeys[dgReportNames.SelectedIndex]);

                Session["editARNIntNo"] = editARNIntNo;
                Session["prevPage"] = thisPageURL;

                ShowReportNamesDetails(editARNIntNo);
            }

        }


        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int arnIntNo = Convert.ToInt32(Session["editARNIntNo"]);

            AuthReportNameDB arn = new Stalberg.TMS.AuthReportNameDB(connectionString);

            string delARNIntNo = arn.DeleteAuthReportName(arnIntNo);
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            if (delARNIntNo.Equals("0"))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ReportNamesMaintenance, PunchAction.Delete); 
            }

            lblError.Visible = true;
           
            // Jake 2013-09-12  comment out because the page index should to stay at current page index
            //dgReportNamesPager.CurrentPageIndex = 1;
            //dgReportNamesPager.RecordCount = 0;
            BindGrid(autIntNo);
        }
        protected void btnOptCopy_Click(object sender, EventArgs e)
        {
            pnlAddReportName.Visible = false;
            pnlUpdateReportName.Visible = false;
            pnlCopy.Visible = true;


            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            PopulateAuthorites(ddlAutFrom, autIntNo);
            PopulateAuthorites(ddlAutTo, autIntNo);
        }

        protected void btnCopyReports_Click(object sender, EventArgs e)
        {
            int fromAutIntNo = Convert.ToInt32(ddlAutFrom.SelectedValue);
            int autIntNo = Convert.ToInt32(ddlAutTo.SelectedValue);

            if (fromAutIntNo == autIntNo)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                return;
            }

            AuthReportNameDB arn = new AuthReportNameDB(connectionString);

            int copyAutIntNo = arn.CopyAutReportNames(fromAutIntNo, autIntNo, loginUser);

            if (copyAutIntNo == -1)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            else if (copyAutIntNo == -2)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            else if (copyAutIntNo == 0)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.ReportNamesMaintenance, PunchAction.Add); 
            }
            //Iris 20140310 changed for showing the selected Authority list in current page when complete 'copy'.
            //autIntNo = Convert.ToInt32(Session["autIntNo"]);
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            //dgReportNamesPager.CurrentPageIndex = 1;
            //dgReportNamesPager.RecordCount = 0;
            BindGrid(autIntNo);

        }

        protected void dgReportNamesPager_PageChanged(object sender, EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
            BindGrid(autIntNo);
        }
    }
}
