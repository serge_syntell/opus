using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.Data.Datasets;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using Stalberg.TMS;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;

namespace Stalberg.TMS
{
    /// <summary>
    /// Summary description for FirstNotice1.
    /// </summary>
    public partial class NoticeOfWOAViewer : System.Web.UI.Page
    {
        // Fields
        private string login = string.Empty;
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string thisPage = "NoticeOfWOA Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "NoticeOfWOAViewer.aspx";

        #region 2012-02-02 jerry disabled for backup
        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        //protected override void OnLoad(System.EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    // Get user details
        //    Stalberg.TMS.UserDB user = new UserDB(connectionString);
        //    Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
        //    this.login = userDetails.UserLoginName;

        //    // Set domain specific variables
        //    General gen = new General();
        //    backgroundImage = gen.SetBackground(Session["drBackground"]);
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    // SETUP
        //    string printFile = string.Empty;

        //    if (Request.QueryString["printfile"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    else
        //        printFile = Request.QueryString["printfile"];

        //    if (Request.QueryString["autIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    else
        //        autIntNo = int.Parse(Request.QueryString["autIntNo"]);

        //    // Setup the report
        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);
        //    string reportPage = arn.GetAuthReportName(autIntNo, "NoticeOfWOA");
        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "NoticeOfWOA");
        //    if (reportPage.Equals(""))
        //    {
        //        int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeOfWOA.dplx", "NoticeOfWOA", "system", "");
        //        reportPage = "NoticeOfWOA.dplx";
        //    }

        //    string path = Server.MapPath("reports/" + reportPage);

        //    string templatePath = string.Empty;
        //    if (!File.Exists(path))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        //dls 081117 - we can only check that the template path exists if there is actually a template!
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //byte[] buffer = null;

        //    DocumentLayout reportDoc = new DocumentLayout(path);
        //    Query query = (Query)reportDoc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;

        //    // Create parameters for report's stored proc.
        //    ParameterDictionary parameters = new ParameterDictionary();
        //    //parameters.Add("Return", "report");
        //    parameters.Add("AutIntNo", this.autIntNo);
        //    //mrs 20081020 must send printfile name parameter as we are only interested in the contents of the printfile
        //    parameters.Add("PrintFileName", printFile);

        //    // LAOUT OBJECT EXTRACTION:
        //    //FormattedRecordArea fraPostalAddress = (FormattedRecordArea)reportDoc.GetElementById("fraPostalAddress");
        //    //FormattedRecordArea fraPhysicalAddress = (FormattedRecordArea)reportDoc.GetElementById("fraPhysicalAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblNoticeTxt = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblTestNoticeText");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPrintDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblPrintDate");
        //    //RecordArea recordEnglishOffence = (RecordArea)reportDoc.GetElementById("recordEnglishOffence");
        //    //RecordArea recordAfrikaansOffence = (RecordArea)reportDoc.GetElementById("recordAfrikaansOffence");
        //    //FormattedRecordArea fraCrtPaymentAddr = (FormattedRecordArea)reportDoc.GetElementById("fraCrtPaymentAddr");

        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCrtName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblCrtName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSumCaseNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblSumCaseNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSumCourtDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblSumCourtDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSummonsNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblSummonsNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblJdgAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblJdgAmount");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAccFullName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblAccFullName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label fraPostalAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("fraPostalAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblTestNoticeText = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblTestNoticeText");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label recordEnglishOffence = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("recordEnglishOffence");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label fraCrtPaymentAddr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("fraCrtPaymentAddr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label fraPhysicalAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("fraPhysicalAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label recordAfrikaansOffence = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("recordAfrikaansOffence");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblmtrAfr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblmtrAfr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblMtrEng = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)reportDoc.GetElementById("lblMtrEng");


        //    //NoticeOfWOAReport noticeOfWOAReport = new NoticeOfWOAReport();

        //    //// LAYOUT EVENT METHOD REGISTRATION:
        //    //fraPostalAddress.LayingOut += new LayingOutEventHandler(noticeOfWOAReport.fraPostalAddress_LayingOut);
        //    //fraPhysicalAddress.LayingOut += new LayingOutEventHandler(noticeOfWOAReport.fraPhysicalAddress_LayingOut);
        //    //lblNoticeTxt.LayingOut += new LayingOutEventHandler(noticeOfWOAReport.lblNoticeTxt_LayingOut);
        //    //lblPrintDate.LayingOut += new LayingOutEventHandler(noticeOfWOAReport.lblPrintDate_LayingOut);
        //    //recordEnglishOffence.LayingOut += new LayingOutEventHandler(noticeOfWOAReport.recordEnglishOffence_LayingOut);
        //    //recordAfrikaansOffence.LayingOut += new LayingOutEventHandler(noticeOfWOAReport.recordAfrikaansOffence_LayingOut);
        //    //fraCrtPaymentAddr.LayingOut += new LayingOutEventHandler(noticeOfWOAReport.fraCrtPaymentAddr_LayingOut);

        //    // TRY to merge the PDF here.
        //    //Document report = new Document();

        //    //report = reportDoc.Run(parameters);

        //    SqlConnection con = null;
        //    SqlCommand com = null;
        //    SqlDataReader result = null;

        //    try
        //    {
        //        // Fill the DataSet
        //        con = new SqlConnection(this.connectionString);
        //        com = new SqlCommand("GetNoticeOfWOA", con);
        //        com.CommandType = CommandType.StoredProcedure;
        //        com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //        com.Parameters.Add("@PrintFileName", SqlDbType.VarChar, 50).Value = printFile;

        //        con.Open();
        //        result = com.ExecuteReader(CommandBehavior.CloseConnection);

        //        MergeDocument merge = new MergeDocument();
        //        byte[] buffer;
        //        Document report = null;
        //        int noOfNotices = 0;

        //        char pad0Char = Convert.ToChar("0");

        //        while (result.Read())
        //        {
        //            try
        //            {
        //                lblCrtName.Text = result["CrtName"].ToString().Trim();
        //                lblSumCaseNo.Text = result["SumCaseNo"].ToString().Trim();
        //                lblSumCourtDate.Text = string.Format("{0:d MMMM yyyy}", result["SumCourtDate"]);
        //                lblSummonsNo.Text = result["SummonsNo"].ToString().Trim();
        //                lblJdgAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(result["JdgAmount"].ToString()));
        //                lblAccFullName.Text = result["AccFullName"].ToString().Trim();
        //                lblmtrAfr.Text = result["AutDocumentFromAfri"].ToString().Trim();
        //                lblMtrEng.Text = result["AutDocumentFromEnglish"].ToString().Trim();
        //                //check to proc for which reg no is getting used here. 
        //                //Use OrigRegNo if ChangeOfRegNo=Y and LetterTo=O
        //                string regNo = result["SumRegNo"].ToString().Trim();
        //                string speedZone = result["SumSpeedLimit"].ToString().Trim();
        //                string speed = result["MinSpeed"].ToString().Trim();

        //                string description = "Dat die beskuldigde " + result["SChOcTDescr1"].ToString().Trim();
        //                // Remove the first sentence
        //                int pos = description.IndexOf('.');
        //                if (pos >= 0 && pos + 1 < description.Length)
        //                    description = description.Substring(pos + 1);

        //                recordAfrikaansOffence.Text = description.Replace("(X~1)", regNo).Replace("(X~2)", speedZone).Replace("(X~3)", speed);

        //                description = "That the accused " + result["SChOcTDescr"].ToString().Trim();
        //                // Remove the first sentence
        //                pos = description.IndexOf('.');
        //                if (pos >= 0 && pos + 1 < description.Length)
        //                    description = description.Substring(pos + 1);

        //                recordEnglishOffence.Text = description.Replace("(X~1)", regNo).Replace("(X~2)", speedZone).Replace("(X~3)", speed);

        //                string strCrtPaymentAddr1 = result["CrtPaymentAddr1"].ToString().Trim();
        //                string strCrtPaymentAddr2 = result["CrtPaymentAddr2"].ToString().Trim();
        //                string strCrtPaymentAddr3 = result["CrtPaymentAddr3"].ToString().Trim();
        //                string strCrtPaymentAddr4 = result["CrtPaymentAddr4"].ToString().Trim();

        //                string[] courtPaymentAddrStrArray = { lblCrtName.Text, strCrtPaymentAddr1, strCrtPaymentAddr2, strCrtPaymentAddr3, strCrtPaymentAddr4 };

        //                fraCrtPaymentAddr.Text = AppendAndRemoveEmptyStrs(courtPaymentAddrStrArray);

        //                lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", DateTime.Today);     //DateTime.Now.ToString("D");

        //                string newNoticeText = lblTestNoticeText.Text;
        //                newNoticeText = newNoticeText.Replace("_SumCourtDate_", lblSumCourtDate.Text);
        //                newNoticeText = newNoticeText.Replace("_SummonsNo_", lblSummonsNo.Text);
        //                newNoticeText = newNoticeText.Replace("_CrtName_", lblCrtName.Text);

        //                lblTestNoticeText.Text = newNoticeText;

        //                string strAccStAdd1 = result["AccStAdd1"].ToString().Trim();
        //                string strAccStAdd2 = result["AccStAdd2"].ToString().Trim();
        //                string strAccStAdd3 = result["AccStAdd3"].ToString().Trim();
        //                string strAccStAdd4 = result["AccStAdd4"].ToString().Trim();
        //                string strAccStAdd5 = result["AccStAdd5"].ToString().Trim();
        //                string strAccStCode = result["AccStCode"].ToString().Trim();

        //                string[] physAddresStrArray = { strAccStAdd1, strAccStAdd2, strAccStAdd3, strAccStAdd4, strAccStAdd5, strAccStCode };

        //                fraPhysicalAddress.Text = AppendAndRemoveEmptyStrs(physAddresStrArray);

        //                string strPostalAddr1 = result["AccPoAdd1"].ToString().Trim();
        //                string strPostalAddr2 = result["AccPoAdd2"].ToString().Trim();
        //                string strPostalAddr3 = result["AccPoAdd3"].ToString().Trim();
        //                string strPostalAddr4 = result["AccPoAdd4"].ToString().Trim();
        //                string strPostalAddr5 = result["AccPoAdd5"].ToString().Trim();
        //                string strAccPoCode = result["AccPoCode"].ToString().Trim();

        //                string[] postalAddresStrArray = { strPostalAddr1, strPostalAddr2, strPostalAddr3, strPostalAddr4, strPostalAddr5, strAccPoCode };

        //                fraPostalAddress.Text = AppendAndRemoveEmptyStrs(postalAddresStrArray);

        //                report = reportDoc.Run(parameters);

        //                ImportedPageArea importedPage;
        //                byte[] bufferTemplate;
        //                if (!sTemplate.Equals(""))
        //                {
        //                    if (sTemplate.ToLower().IndexOf(".dplx") > 0)
        //                    {
        //                        DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
        //                        Query queryTemplate = (Query)template.GetQueryById("Query");
        //                        queryTemplate.ConnectionString = this.connectionString;
        //                        ParameterDictionary parametersTemplate = new ParameterDictionary();
        //                        Document reportTemplate = template.Run(parametersTemplate);
        //                        bufferTemplate = reportTemplate.Draw();
        //                        PdfDocument pdf = new PdfDocument(bufferTemplate);
        //                        PdfPage page = pdf.Pages[0];
        //                        importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
        //                    }
        //                    else
        //                    {
        //                        //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                        importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                    }

        //                    ceTe.DynamicPDF.Page rptPage = report.Pages[0];
        //                    rptPage.Elements.Insert(0, importedPage);
        //                }
        //                buffer = report.Draw();
        //                merge.Append(new PdfDocument(buffer));

        //                buffer = null;
        //                bufferTemplate = null;
        //            }
        //            catch (Exception ex)
        //            {
        //                String sError = ex.Message;
        //            }

        //            noOfNotices++;
        //        }

        //        result.Close();
        //        con.Dispose();

        //        byte[] buf = merge.Draw();

        //        //fraPostalAddress.LayingOut -= new LayingOutEventHandler(noticeOfWOAReport.fraPostalAddress_LayingOut);
        //        //fraPhysicalAddress.LayingOut -= new LayingOutEventHandler(noticeOfWOAReport.fraPhysicalAddress_LayingOut);
        //        //lblNoticeTxt.LayingOut -= new LayingOutEventHandler(noticeOfWOAReport.lblNoticeTxt_LayingOut);
        //        //lblPrintDate.LayingOut -= new LayingOutEventHandler(noticeOfWOAReport.lblPrintDate_LayingOut);
        //        //recordEnglishOffence.LayingOut -= new LayingOutEventHandler(noticeOfWOAReport.recordEnglishOffence_LayingOut);
        //        //recordAfrikaansOffence.LayingOut -= new LayingOutEventHandler(noticeOfWOAReport.recordAfrikaansOffence_LayingOut);
        //        //fraCrtPaymentAddr.LayingOut -= new LayingOutEventHandler(noticeOfWOAReport.fraCrtPaymentAddr_LayingOut);

        //        Response.ClearContent();
        //        Response.ClearHeaders();
        //        Response.ContentType = "application/pdf";
        //        Response.BinaryWrite(buf);
        //        Response.End();

        //        buf = null;
        //    }
        //    catch (Exception ex)
        //    {
        //        string msg = ex.Message;
        //    }
        //    finally
        //    {
        //        GC.Collect();
        //    }

        //}
        #endregion

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // SETUP
            string printFile = string.Empty;

            if (Request.QueryString["printfile"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                printFile = Request.QueryString["printfile"];

            if (Request.QueryString["autIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                autIntNo = int.Parse(Request.QueryString["autIntNo"]);

            //jerry 2012-02-02, use module to create print pdf file
            PrintFileProcess process = new PrintFileProcess(this.connectionString, this.login);
            process.BuildPrintFile(new PrintFileModuleNoticeOfWOA(), this.autIntNo, printFile);

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.PrintNoticeOfWOA, PunchAction.Other); 
        }

        private string AppendAndRemoveEmptyStrs(params string[] pStringsToAppend)
        {
            string strComposite = "";
            if (pStringsToAppend.Length < 1)
                return "";

            for (int i = 0; i < pStringsToAppend.Length; i++)
            {
                string str = pStringsToAppend[i];

                if (str.Trim() != "" && i >= (pStringsToAppend.Length - 1))
                    strComposite += str;
                else if (str.Trim() != "")
                {
                    strComposite += str + "\n";
                }
            }
            return strComposite;
        }
    }
}
