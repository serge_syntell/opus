using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using SIL.AARTO.BLL.Utility.Cache;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that displays a printable version of an offence
    /// </summary>
    public partial class ViewOffence_Print : System.Web.UI.Page
    {
        private const int MAIN_IMAGE_MAX_WIDTH = 385;
        private const int MAIN_IMAGE_MAX_HEIGHT = 244;
        private const int REG_IMAGE_MAX_WIDTH = 146;
        private const int REG_IMAGE_MAX_HEIGHT = 34;

        private string connectionString = string.Empty;
        //private int notIntNo = 0;
        private int frameIntNo = 0;
        private int scImIntNo = 0;
        private string imgType = "A";
        //private bool useJp2 = false;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "ViewOffence_Print.aspx";
        //protected string thisPage = "Offence details";

        protected string crossHairSettings = "N";
        protected int crossHairStyle = 0;

        protected DataSet scanDS;

        private const string MONEY_FORMAT = "R #,##0.00";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();
            //this.useJp2 = (bool)Application["UseJP2Conversion"];

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            //int 
            loginUser = userDetails.UserLoginName;

            //set domain specific variables
            General gen = new General();

            this.backgroundImage = gen.SetBackground(Session["drBackground"]);
            this.styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            this.title = gen.SetTitle(Session["drTitle"]) + " ~ Offence details";

            //mrs 20080816 add getting notIntNo from querystring
            if (Request.QueryString["notice"] == null)
            {
                Response.Write((string)GetLocalResourceObject("WriteMsg"));
                Response.End();
                return;
            }
            int notIntNo = Convert.ToInt32(Request.QueryString["notice"].ToString().Trim());
            if (notIntNo > 0)
            {
                //set the viewstate
                ViewState.Add("notIntNo", notIntNo);
            }
            else
            {
                Response.Write((string)GetLocalResourceObject("WriteMsg"));
                Response.End();
                return;
            }

            if (Request.QueryString["autintno"] == null)
            {
                Response.Write((string)GetLocalResourceObject("WriteMsg1"));
                Response.End();
                return;
            }

            this.autIntNo = Convert.ToInt32(Request.QueryString["autintno"].ToString().Trim());

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.ShowFrameDetails();
            }
        }

        private string GetCrossHairSettings(int autIntNo)
        {
            //dls 100202 - move rules for showing cross-hairs out of the stored proc into the code
            AuthorityRulesDetails ar = new AuthorityRulesDetails();

            ar.AutIntNo = autIntNo;
            ar.ARCode = "3100";
            ar.LastUser = this.loginUser;

            DefaultAuthRules crossHairs = new DefaultAuthRules(ar, this.connectionString);

            KeyValuePair<int, string> crossHairRule = crossHairs.SetDefaultAuthRule();

            return crossHairRule.Value;
        }

        private int GetCrossHairStyle(int autIntNo)
        {
            //dls 100202 - move rules for showing cross-hairs out of the stored proc into the code
            AuthorityRulesDetails ar = new AuthorityRulesDetails();

            ar.AutIntNo = autIntNo;
            ar.ARCode = "3150";
            ar.LastUser = this.loginUser;

            DefaultAuthRules crossHairs = new DefaultAuthRules(ar, this.connectionString);

            KeyValuePair<int, string> crossHairRule = crossHairs.SetDefaultAuthRule();

            return crossHairRule.Key;
        }

        private void ShowFrameDetails()
        {
            NoticeDB notice = new NoticeDB(connectionString);

            //mrs 20080814 notIntNo not changing during revious and next operations
            int notIntNo = Convert.ToInt32(ViewState["notIntNo"]);

            this.crossHairSettings = GetCrossHairSettings(this.autIntNo);
            this.crossHairStyle = this.GetCrossHairStyle(this.autIntNo);

            frameIntNo = notice.GetFrameForNotice(notIntNo);

            this.crossHairSettings = GetCrossHairSettings(this.autIntNo);
            this.crossHairStyle = this.GetCrossHairStyle(this.autIntNo);

            if (frameIntNo != 0)
            {
                Session["frameIntNo"] = frameIntNo;
                Session["cvPhase"] = 1;
                Session["scImIntNo"] = 0;

                SqlDataReader reader = notice.GetNoticeViewDetails(notIntNo, this.crossHairSettings, this.crossHairStyle);

                while (reader.Read())
                {
                    this.autIntNo = int.Parse(reader["AutIntNo"].ToString());
                    //dls 070120 - add rules for showing cross-hairs
                    Session["ShowCrossHairs"] = reader["ShowCrossHairs"].ToString().Equals("Y") ? true : false;
                    Session["CrossHairStyle"] = Convert.ToInt16(reader["CrossHairStyle"]);

                    lblFrameNo.Text = reader["NotFilmNo"].ToString() + " ~ " + reader["NotFrameNo"].ToString();
                    lblRegNo.Text = reader["NotRegNo"].ToString();
                    lblSpeed.Text = reader["NotSpeed1"].ToString();
                    lblSpeed2.Text = reader["NotSpeed2"].ToString();
                    //txtVehMake.Text = reader["NotVehicleMake"].ToString();
                    //txtVehType.Text = reader["NotVehicleType"].ToString();
                    lblTicketNo.Text = reader["NotTicketNo"].ToString();

                    DateTime offenceDate = Convert.ToDateTime(reader["NotOffenceDate"]);
                    //txtOffenceDate.Text = offenceDate.Year + "/" + offenceDate.Month.ToString().PadLeft(2, '0') + "/" + offenceDate.Day.ToString().PadLeft(2, '0');
                    //txtOffenceTime.Text = offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    lblOffenceDate.Text = offenceDate.Year + "/" + offenceDate.Month.ToString().PadLeft(2, '0') + "/" + offenceDate.Day.ToString().PadLeft(2, '0')
                        + " " + offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    lblOffence.Text = reader["ChgOffenceDescr"].ToString();

                    lblLocDescr.Text = reader["NotLocDescr"].ToString();

                    LoadOwnerDetails(notIntNo);
                    LoadDriverDetails(notIntNo);

                    if (reader["NotProxyFlag"].ToString().Equals("Y"))
                    {
                        LoadProxyDetails(notIntNo);
                        lblProxy.Visible = true;
                        lblProxyID.Visible = true;
                        lblProxyName.Visible = true;
                        lblProxyName.Visible = true;
                        lblProxyID.Visible = true;
                    }
                    else
                    {
                        lblProxy.Visible = false;
                        lblProxyID.Visible = false;
                        lblProxyName.Visible = false;
                        lblProxyName.Visible = false;
                        lblProxyID.Visible = false;
                    }

                    tblOwnerDetails.Visible = true;

                    this.lblFineAmount.Text = Convert.ToDecimal(reader["ChgRevFineAmount"]).ToString(MONEY_FORMAT);
                }
                reader.Close();

                LoadFilmImages();

                if (scImIntNo == 0) FindMainImage();

                ddlScan.SelectedIndex = ddlScan.Items.IndexOf(ddlScan.Items.FindByValue(scImIntNo.ToString()));

                bool checkSettings = AllowImageSettings();

                GetImage(imageA, scImIntNo, MAIN_IMAGE_MAX_WIDTH, MAIN_IMAGE_MAX_HEIGHT, checkSettings);

                ScanImageDB scan = new ScanImageDB(this.connectionString);

                ScanImageDetails scanDetails = scan.GetScanImageDetails(scImIntNo);
                int X = scanDetails.XValue;
                int Y = scanDetails.YValue;

                DrawCrossHairs(X, Y);

                imgType = "B";
                int scImIntNoB = FindImage("B");
                if (scImIntNoB != 0)
                    //GetImage(wivImageB, scImIntNoB);
                    GetImage(imageB, scImIntNoB, MAIN_IMAGE_MAX_WIDTH, MAIN_IMAGE_MAX_HEIGHT, checkSettings);
                else
                    imgAreaB.Visible = false;
                    //wivImageB.Visibility = VisibilityStyle.Hidden;

                imgType = "R";
                int scImIntNoR = FindImage("R");
                if (scImIntNoR != 0)
                    //GetImage(wivRegNo, scImIntNoR);
                    GetImage(regImage, scImIntNoR, REG_IMAGE_MAX_WIDTH, REG_IMAGE_MAX_HEIGHT, false);
                else
                    regArea.Visible = false;
                    //wivRegNo.Visibility = VisibilityStyle.Hidden;

            }
        }
        private void SetQueryStringParameter(string paramName, string paramValue)
        {
            if (this.imageA.ImageUrl.Contains("&" + paramName + "="))
            {
                int start = this.imageA.ImageUrl.IndexOf("&" + paramName + "=");
                int end = this.imageA.ImageUrl.IndexOf("&", start + paramName.Length + 1);
                if (end < 0)
                {
                    this.imageA.ImageUrl = this.imageA.ImageUrl.Substring(0, start) + "&" + paramName + "=" + HttpUtility.UrlEncode(paramValue);
                }
                else
                {
                    this.imageA.ImageUrl = this.imageA.ImageUrl.Substring(0, start) + "&" + paramName + "=" + HttpUtility.UrlEncode(paramValue) + this.imageA.ImageUrl.Substring(end);
                }
            }
            else
            {
                this.imageA.ImageUrl += "&" + paramName + "=" + HttpUtility.UrlEncode(paramValue);
            }
        }
        private void DrawCrossHairs(int X, int Y)
        {
            if (bool.Parse(Session["ShowCrossHairs"].ToString()).Equals(true)
                && X > 0 && Y > 0)
            {
                string style = Session["CrossHairStyle"].ToString();
                SetQueryStringParameter("Type", "4");
                SetQueryStringParameter("XVal", X.ToString());
                SetQueryStringParameter("YVal", Y.ToString());
                SetQueryStringParameter("CrossStyle", style);
                //Drawing draw = new Drawing();

                //draw.CrossHairs(ref this.wivImageA, style, X, Y);
            }
        }

        private void LoadProxyDetails(int notIntNo)
        {
            ProxyDB proxy = new ProxyDB(connectionString);

            ProxyDetails proxyDet = proxy.GetProxyDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            //string postAddr = string.Empty;
            //string strAddr = string.Empty;

            try
            {
                name = proxyDet.PrxSurname.Trim();
            }
            catch { }

            try
            {
                initials = proxyDet.PrxInitials.Trim();
            }
            catch { }

            try
            {
                id = proxyDet.PrxIDNumber.Trim();
            }
            catch { }

            //try
            //{
            //    postAddr = proxyDet.PrxPOAdd1.Trim();
            //    if (!proxyDet.PrxPOAdd1.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd2.Trim();
            //    if (!proxyDet.PrxPOAdd2.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd3.Trim();
            //    if (!proxyDet.PrxPOAdd3.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd4.Trim();
            //    if (!proxyDet.PrxPOAdd4.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd5.Trim();
            //    if (!proxyDet.PrxPOAdd5.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOCode.Trim();
            //}
            //catch { }

            //try
            //{
            //    strAddr = proxyDet.PrxStAdd1.Trim();
            //    if (!proxyDet.PrxStAdd1.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd2.Trim();
            //    if (!proxyDet.PrxStAdd2.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd3.Trim();
            //    if (!proxyDet.PrxStAdd3.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd4.Trim();
            //    if (!proxyDet.PrxStAdd4.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd5.Trim();
            //    if (!proxyDet.PrxStAdd5.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxStCode.Trim();
            //}
            //catch { }

            // Updated 12-07-2007 LMZ
            lblPrxName.Text = proxyDet.PrxFullName; 

            lblPrxID.Text = id;

            //lblProxyPostAddr.Text = postAddr;
            //lblProxyStrAddr.Text = strAddr;
        }

        private void LoadDriverDetails(int notIntNo)
        {
            DriverDB driver = new DriverDB(connectionString);

            DriverDetails driverDet = driver.GetDriverDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            string postAddr = string.Empty;
            string strAddr = string.Empty;

            try
            {
                name = driverDet.DrvSurname.Trim();
            }
            catch { }

            try
            {
                initials = driverDet.DrvInitials.Trim();
            }
            catch { }

            try
            {
                id = driverDet.DrvIDNumber.Trim();
            }
            catch { }

            try
            {
                postAddr = driverDet.DrvPOAdd1.Trim();
                if (!driverDet.DrvPOAdd1.Trim().Equals("")) postAddr += ", ";

                postAddr += driverDet.DrvPOAdd2.Trim();
                if (!driverDet.DrvPOAdd2.Trim().Equals("")) postAddr += ", ";

                postAddr += driverDet.DrvPOAdd3.Trim();
                if (!driverDet.DrvPOAdd3.Trim().Equals("")) postAddr += ", ";

                postAddr += driverDet.DrvPOAdd4.Trim();
                if (!driverDet.DrvPOAdd4.Trim().Equals("")) postAddr += ", ";

                postAddr += driverDet.DrvPOAdd5.Trim();
                if (!driverDet.DrvPOAdd5.Trim().Equals("")) postAddr += ", ";

                postAddr += driverDet.DrvPOCode.Trim();
            }
            catch { }

            try
            {
                strAddr = driverDet.DrvStAdd1.Trim();
                if (!driverDet.DrvStAdd1.Trim().Equals("")) strAddr += ", ";

                strAddr += driverDet.DrvStAdd2.Trim();
                if (!driverDet.DrvStAdd2.Trim().Equals("")) strAddr += ", ";

                strAddr += driverDet.DrvStAdd3.Trim();
                if (!driverDet.DrvStAdd3.Trim().Equals("")) strAddr += ", ";

                strAddr += driverDet.DrvPOAdd4.Trim();
                if (!driverDet.DrvStAdd4.Trim().Equals("")) strAddr += ", ";

                strAddr += driverDet.DrvStCode.Trim();
            }
            catch { }

            // Updated LMZ 12-07-2007
            lblDriverName.Text = driverDet.DrvFullName;

            lblDriverID.Text = id;

            lblDriverPostAddr.Text = postAddr;
            lblDriverStrAddr.Text = strAddr;
        }

        private void LoadOwnerDetails(int notIntNo)
        {
            OwnerDB owner = new OwnerDB(connectionString);

            OwnerDetails ownerDet = owner.GetOwnerDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            string postAddr = string.Empty;
            string strAddr = string.Empty;

            try
            {
                name = ownerDet.OwnSurname.Trim();
            }
            catch { }

            try
            {
                initials = ownerDet.OwnInitials.Trim();
            }
            catch { }

            try
            {
                id = ownerDet.OwnIDNumber.Trim();
            }
            catch { }

            try
            {
                postAddr = ownerDet.OwnPOAdd1.Trim();
                if (!ownerDet.OwnPOAdd1.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd2.Trim();
                if (!ownerDet.OwnPOAdd2.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd3.Trim();
                if (!ownerDet.OwnPOAdd3.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd4.Trim();
                if (!ownerDet.OwnPOAdd4.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd5.Trim();
                if (!ownerDet.OwnPOAdd5.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOCode.Trim();
            }
            catch { }

            try
            {
                strAddr = ownerDet.OwnStAdd1.Trim();
                if (!ownerDet.OwnStAdd1.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd2.Trim();
                if (!ownerDet.OwnStAdd2.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd3.Trim();
                if (!ownerDet.OwnStAdd3.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd4.Trim();
                if (!ownerDet.OwnStAdd4.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStCode.Trim();
            }
            catch { }

            // Updated LMZ 12-07-2007
            lblOwnerName.Text = ownerDet.OwnFullName;

            lblOwnerID.Text = id;

            lblOwnerPostAddr.Text = postAddr;
            lblOwnerStrAddr.Text = strAddr;
        }

        protected int FindImage(string imgType)
        {
            int scanImage = 0;
            string scImgType = string.Empty;

            for (int i = 0; i < ddlScan.Items.Count; i++)
            {
                scImgType = ddlScan.Items[i].Text;
                switch (imgType)
                {
                    case "A":
                        if (scImgType.ToLower().Contains("main "))
                        {
                            scanImage = Convert.ToInt32(ddlScan.Items[i].Value);
                            return scanImage;
                        }
                        break;
                    case "B":
                        if (scImgType.ToLower().Contains("2nd "))
                        {
                            scanImage = Convert.ToInt32(ddlScan.Items[i].Value);
                            return scanImage;
                        }
                        break;
                    case "R":
                        if (scImgType.ToLower().Contains("registration "))
                        {
                            scanImage = Convert.ToInt32(ddlScan.Items[i].Value);
                            return scanImage;
                        }
                        break;
                }
            }
            return scanImage;
        }

        protected int imageAW = 0;
        protected int imageAH = 0;
        protected int imageBW = 0;
        protected int imageBH = 0;
        protected int imageRW = 0;
        protected int imageRH = 0;

        protected void GetImage(System.Web.UI.WebControls.Image image, int scanImage, int width, int height, bool checkSettings)
        {
            try
            {
                byte[] data = null;
                ImageProcesses img = new ImageProcesses(this.connectionString);
                //data = img.ProcessImage(scanImage, false, "", "", Server.MapPath("images/NoImage.jpg"));
                WebService service = new WebService();
                ScanImageDB imageDB = new ScanImageDB(connectionString);
                int intImageNo = scanImage;
                ScanImageDetails imageDetail = imageDB.GetImageFullPath(intImageNo);
                data = service.GetImagesFromRemoteFileServer(imageDetail);
                //if (imageDetail.FileServer != null)
                //{
                //    if (service.RemoteConnect(imageDetail.FileServer.ImageMachineName, imageDetail.FileServer.ImageShareName, imageDetail.FileServer.RemoteUserName, imageDetail.FileServer.RemotePassword))
                //    {
                //        data = img.ProcessImage(imageDetail.ImageFullPath);
                //    }
                //}

                if (data == null || data.Length == 0)
                {
                    string strNoImagePath = Server.MapPath("images/NoImage.jpg");
                    data = img.ProcessImage(strNoImagePath);                    
                }  

                System.Drawing.Image Bitimage = null;
                using (MemoryStream ms = new MemoryStream(data))
                {
                    Bitimage = System.Drawing.Image.FromStream(ms);
                }

                if (image.ID == "imageA")
                {
                    imageAW = Bitimage.Width;
                    imageAH = Bitimage.Height;
                }
                else if (image.ID == "imageB")
                {
                    imageBW = Bitimage.Width;
                    imageBH = Bitimage.Height;
                }
                else if (image.ID == "regImage")
                {
                    imageRW = Bitimage.Width;
                    imageRH = Bitimage.Height;
                }

                Bitimage.Dispose();
            }
            catch
            {
            }

            image.ImageUrl = "~/Image.aspx?ScImIntNo=" + HttpUtility.UrlEncode(Convert.ToString(scanImage))
                + "&Width=" + HttpUtility.UrlEncode(Convert.ToString(width))
                + "&Height=" + HttpUtility.UrlEncode(Convert.ToString(height));

            if (checkSettings)
            {
                foreach (DataRow dr in scanDS.Tables[0].Rows)
                {
                    if (scImIntNo == int.Parse(dr["ScImIntNo"].ToString()))
                    {
                        decimal contrast;
                        decimal brightness;

                        if (decimal.TryParse(dr["Contrast"].ToString(), out contrast))
                            image.ImageUrl += "&SavedContrast=" + contrast;

                        if (decimal.TryParse(dr["Brightness"].ToString(), out brightness))
                            image.ImageUrl += "&SavedBrightness=" + brightness*10.0m;
                    }
                }
            }
        }
     
        protected bool FindMainImage()
        {
            bool foundScan = false;
            string scImgType = string.Empty;

            for (int i = 0; i < ddlScan.Items.Count; i++)
            {
                if (foundScan == true)
                {
                    Session["scImIntNo"] = ddlScan.Items[i].Value;
                    scImIntNo = Convert.ToInt32(ddlScan.Items[i].Value);
                    break;
                }
                if (scImIntNo == Convert.ToInt32(ddlScan.Items[i].Value) || scImIntNo == 0)
                {
                    scImgType = ddlScan.Items[i].Text;
                    switch (imgType)
                    {
                        case "A":
                            if (scImgType.ToLower().Contains("main "))
                            {
                                foundScan = true;
                                if (scImIntNo == 0)
                                {
                                    Session["scImIntNo"] = ddlScan.Items[i].Value;
                                    scImIntNo = Convert.ToInt32(ddlScan.Items[i].Value);
                                    return foundScan;
                                }
                            }
                            break;
                        case "B":
                            if (scImgType.ToLower().Contains("2nd "))
                            {
                                foundScan = true;
                                if (scImIntNo == 0)
                                {
                                    Session["scImIntNo"] = ddlScan.Items[i].Value;
                                    scImIntNo = Convert.ToInt32(ddlScan.Items[i].Value);
                                    return foundScan;
                                }
                            }
                            break;
                        case "R":
                            if (scImgType.ToLower().Contains("registration "))
                            {
                                foundScan = true;
                                if (scImIntNo == 0)
                                {
                                    Session["scImIntNo"] = ddlScan.Items[i].Value;
                                    scImIntNo = Convert.ToInt32(ddlScan.Items[i].Value);
                                    return foundScan;
                                }
                            }
                            break;
                    }
                }
            }

            return foundScan;
        }

        private void LoadFilmImages()
        {
            ScanImageDB scan = new ScanImageDB(connectionString);

            //DataSet scanDS;
            //scanDS = scan.GetScanImageListDS(0, frameIntNo, "%", "");
            scanDS = scan.GetScanImageListDS(0, frameIntNo, "%");
            
            ddlScan.DataSource = scanDS;
            ddlScan.DataTextField = "ImageDetails";
            ddlScan.DataValueField = "ScImIntNo";
            ddlScan.DataBind();
        }

        private bool AllowImageSettings()
        {
            //dls 100125 - check rule for settings before showing changed image settings
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = this.autIntNo;
            arDetails.ARCode = "2560";
            arDetails.LastUser = this.loginUser;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            ar.SetDefaultAuthRule();
            return arDetails.ARString == "Y" ? true : false;
        }
    }
}