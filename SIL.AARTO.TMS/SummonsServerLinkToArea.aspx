<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SummonsServerLinkToArea" Codebehind="SummonsServerLinkToArea.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0" class="Normal">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="100%">
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center" class="Normal">
                                <asp:UpdatePanel ID="udpSummonsServers" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlSummonsServers" runat="server" Width="100%">
                                            <p>
                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>&nbsp;</p>
                                        </asp:Panel>
                                        &nbsp;&nbsp;
                                        <asp:Panel ID="pnlAuthority" runat="server" Width="100%">
                                            <br />
                                            <p style="text-align: center;">
                                                <table style="width: 473px">
                                                    <tr>
                                                        <td style="height: 21px">
                                                            <asp:Label ID="Label2" runat="server" CssClass="ProductListHead" Width="361px" Text="<%$Resources:lblAuthority.Text %>"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 24px">
                                                            <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="Label3" runat="server" CssClass="ProductListHead" Width="157px" Text="<%$Resources:lblSelectauthority.Text %>"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSummonsServer" runat="server" CssClass="Normal" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlSummonsServer_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="Label4" runat="server" CssClass="ProductListHead" Width="157px" Text="<%$Resources:lblSummonsServer.Text %>"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnAddSSForAuthority" runat="server" OnClick="btnAddSSForAuthority_Click"
                                                                Text="<%$Resources:btnAddSSForAuthority.Text %>" Width="389px" />
                                                            <br/><asp:Button ID="btnAddGroupAreaToSS" runat="server" Text="<%$Resources:AddAreaGroupToSelectedAuthorityAndSummonsServer %>" Width="389px" onclick="btnAddGroupAreaToSS_Click" /><br/>
                                                            <asp:Panel ID="pnlAreaGroup" Visible="False" Width="100%" CssClass="Normal" runat="server">
                                                                <br/>
                                                                <asp:Label ID="Label10" runat="server" Text="<%$Resources:AreaGroup %>" />:&nbsp;
                                                                <asp:DropDownList ID="ddlAreaGroup" runat="server" AutoPostBack="True" onselectedindexchanged="ddlAreaGroup_SelectedIndexChanged" />
                                                                <asp:Button ID="btnAddAreaGroup" runat="server" Text="<%$Resources:AddSelectedGroup %>" onclick="btnAddAreaGroup_Click" />
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                &nbsp;
                                            </p>
                                            &nbsp; &nbsp;
                                            <asp:Panel ID="pnlDisplayAuthSS" runat="server" Width="100%">
                                                &nbsp;<br />
                                                <asp:Label ID="lblSummonsServer" runat="server" CssClass="Normal" Width="345px" Text="<%$Resources:lblSummonsServer1.Text %>"></asp:Label><br />
                                                <br />
                                                <asp:DataGrid ID="grdLinkedSummonsServers" runat="server" AutoGenerateColumns="False"
                                                    CssClass="Normal" ShowFooter="True" GridLines="Vertical" CellPadding="4" OnItemCommand="grdLinkedSummonsServers_ItemCommand" AllowPaging="True" OnPageIndexChanged="grdLinkedSummonsServers_PageIndexChanged" PageSize="25">
                                                    <FooterStyle CssClass="CartListHead" />
                                                    <HeaderStyle CssClass="CartListHead" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="ASIntNo" HeaderText="ASIntNo" Visible="False">
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="SSIntNo" HeaderText="SSIntNo" Visible="False">
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="SSFullName" HeaderText="<%$Resources:grdLinkedSummonsServers.HeaderText %>">
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="AreaCode" HeaderText="<%$Resources:grdLinkedSummonsServers.HeaderText1 %>" DataFormatString="{0:0000}">
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="AreaDescr" HeaderText="<%$Resources:grdLinkedSummonsServers.HeaderText2 %>">
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" />
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ASSAllocateManual" HeaderText="<%$Resources:grdLinkedSummonsServers.HeaderText3 %>"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ASSPriority" HeaderText="<%$Resources:grdLinkedSummonsServers.HeaderText4 %>"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ASSPercentage" HeaderText="<%$Resources:grdLinkedSummonsServers.HeaderText5 %>"></asp:BoundColumn>
                                                        <asp:ButtonColumn CommandName="Edit" HeaderText="<%$Resources:grdLinkedSummonsServers.HeaderText6 %>" Text="<%$Resources:grdLinkedSummonsServers.HeaderText6 %>"></asp:ButtonColumn>
                                                        <asp:ButtonColumn CommandName="Delete" HeaderText="<%$Resources:grdLinkedSummonsServers.HeaderText7 %>" Text="<%$Resources:grdLinkedSummonsServersItem.Text %>">
                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" Wrap="False" HorizontalAlign="Right" />
                                                        </asp:ButtonColumn>
                                                    </Columns>
                                                    <AlternatingItemStyle CssClass="CartListItemAlt" />
                                                </asp:DataGrid>
                                                <asp:Button ID="btnAddAreaToAuthSS" runat="server" OnClick="btnAddAreaToAuthSS_Click"
                                                    Text="<%$Resources:btnAddAreaToAuthSS.Text %>" Width="417px" />
                                                <asp:Button ID="btnAddAllAreas" runat="server" OnClick="btnAddAllAreas_Click" Text="<%$Resources:btnAddAllAreas.Text %>"
                                                    Width="416px" /></asp:Panel>
                                            <asp:Panel ID="pnlUpdateAuthSS" runat="server" Width="100%" CssClass="Normal">
                                                <asp:Panel ID="pnlEditAuthSS" runat="server" Height="50px" Width="554px">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 1761px; height: 22px;">
                                                                <asp:Label ID="Label5" runat="server" Text="<%$Resources:lblManual.Text %>" Width="296px" Font-Size="Smaller"></asp:Label></td>
                                                            <td style="width: 9543px; height: 22px;">
                                                                <asp:TextBox ID="txtManual" runat="server"></asp:TextBox></td>
                                                            <td style="width: 16699px; height: 22px;">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 1761px">
                                                                <asp:Label ID="Label6" runat="server" Text="<%$Resources:lblPriority.Text %>"
                                                                    Width="295px" Font-Size="Smaller"></asp:Label></td>
                                                            <td style="width: 9543px">
                                                                <asp:TextBox ID="txtPriority" runat="server"></asp:TextBox></td>
                                                            <td style="width: 16699px">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 1761px">
                                                                <asp:Label ID="Label7" runat="server" Text="<%$Resources:lblPercentage.Text %>" Width="296px" Font-Size="Smaller"></asp:Label></td>
                                                            <td style="width: 9543px">
                                                                <asp:TextBox ID="txtPercentage" runat="server"></asp:TextBox></td>
                                                            <td style="width: 16699px">
                                                                <asp:Button ID="btnUpdateASS" runat="server" OnClick="btnUpdateASS_Click" Text="<%$Resources:btnUpdateASS.Text %>" /></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <br />
                                                <p class="NormalBold">
                                                    &nbsp;&nbsp;
                                                </p>
                                                &nbsp;
                                                &nbsp;&nbsp;&nbsp;</asp:Panel>
                                            <asp:Panel ID="pnlAddArea" runat="server" Width="100%" CssClass="Normal">
                                                <br />
                                                &nbsp;<asp:Label ID="Label8" runat="server" Text="<%$Resources:lblPartialArea.Text %>"></asp:Label>
                                                <asp:TextBox ID="txtSelectArea" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnSelectArea" runat="server" OnClick="btnSelectArea_Click" Text="<%$Resources:btnSelectArea.Text %>"
                                                    Width="108px" /><br />
                                                &nbsp;
                                                <asp:ListBox ID="lstAreaCodes" runat="server" Height="149px" SelectionMode="Multiple"
                                                    Width="403px"></asp:ListBox>
                                                <asp:Button ID="btnAddSelectedAC" runat="server" OnClick="btnAddSelectedAC_Click"
                                                    Text="<%$Resources:btnAddSelectedAC.Text %>" Width="139px" /></asp:Panel>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="udp" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label9"
                                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                    </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
