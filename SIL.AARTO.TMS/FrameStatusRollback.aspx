﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.FrameStatusRollback" Codebehind="FrameStatusRollback.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=_title%>
    </title>
    <link href="<%= _styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= _description %>" name="Description" />
    <meta content="<%= _keywords %>" name="Keywords" />
</head>
<body style="margin: 0px 0px 0px 0px; background: <%=_background %>;">
    <form id="Form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="height: 10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" style="width: 100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table style="height: 85%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center">
                <img style="height: 1px" src="images/1x1.gif" alt="" width="167" />
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
            </td>
            <td valign="top" align="left" colspan="1" style="width: 100%; text-align: center">
                <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                    <p style="text-align: center;">
                        <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                    <p>
                        &nbsp;</p>
                </asp:Panel>
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblError" CssClass="NormalRed" runat="server" />
                        <asp:Panel ID="pnlSearch" runat="server" Width="100%" CssClass="Normal">
                            <table style="width: 353px">
                             <tr>
                                <td style="width: 176px"> 
                                <asp:Label ID="lblAuthority" runat="server" CssClass="NormalBold" 
                                        Text="<%$Resources:lblAuthority.Text %>" Width="150px" />
</td>
                                <td>
                                    <asp:DropDownList ID="ddlAuthority" runat="server" CssClass="Normal" 
                                        Width="197px" onselectedindexchanged="ddlAuthority_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 3px">
                                </td>
                            </tr>
                                <tr>
                                    <td style="width: 76px">
                                        <asp:Label ID="lbFilmNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lbFilmNo.Text %>" />
                                    </td>
                                    <td style="width: 193px">
                                        <asp:TextBox runat="server" ID="textFilmNo" MaxLength="25" CssClass="Normal" 
                                            Height="16px" Width="150px"></asp:TextBox>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                <td style="width: 76px">
                                    <asp:Label ID="lbFrameNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lbFrameNo.Text %>" />
                                </td>
                                <td style="width: 193px">
                                    <asp:TextBox runat="server" ID="textFrameNo" Height="16px" MaxLength="4" 
                                        CssClass="Normal" Width="150px"></asp:TextBox>
                                </td>
                                <td><asp:Button runat="server" ID="buttonSearch" Text="<%$Resources:buttonSearch.Text %>" OnClick="buttonSearch_Click" 
                                                                    CssClass="NormalButton" /></td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlDetails" runat="server" Width="100%" CssClass="Normal">
                            <asp:DataGrid ID="gridFrame" Width="495px" runat="server" BorderColor="Black"
                                AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                Font-Size="8pt" CellPadding="4" GridLines="Vertical" OnItemCommand="gridFrame_ItemCommand">
                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="FrameIntNo" HeaderText="FrameIntNo" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NotIntNo" HeaderText="NotIntNo" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ROWVERSION" HeaderText="ROWVERSION" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FilmNo" HeaderText="<%$Resources:gridFrame.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="FrameNo" HeaderText="<%$Resources:gridFrame.HeaderText1 %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="OffenceDate" HeaderText="<%$Resources:gridFrame.HeaderText2 %>" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:ButtonColumn CommandName="Rollback" Text="<%$Resources:gridFrameItem.Text %>"></asp:ButtonColumn>
                                </Columns>
                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                            </asp:DataGrid>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="udp" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;<asp:Label
                                ID="Label1" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    <table style="height: 5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" style="width: 100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>