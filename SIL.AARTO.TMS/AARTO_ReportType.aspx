﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Stalberg.TMS.AARTO_ReportType" Codebehind="AARTO_ReportType.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0" rightmargin="0">
    <form id="Form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="height: 10%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="HomeHead" valign="middle" align="center" style="width: 100%" colspan="2">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table style="height: 85%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" align="center">
                <img style="height: 1px" src="images/1x1.gif" alt="" width="167" />
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 138px">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                <asp:Button ID="buttonAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:buttonAdd.Text %>"
                                    OnClick="buttonAdd_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                <asp:Button ID="buttonList" runat="server" Width="135px" 
                                    CssClass="NormalButton" Text="<%$Resources:buttonList.Text %>" onclick="buttonList_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 138px">
                                
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" colspan="1" style="width: 100%; text-align: center">
                <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                    <p style="text-align: center;">
                        <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                    <p>
                        &nbsp;</p>
                </asp:Panel>
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblError" CssClass="NormalRed" runat="server" />
                        <asp:Panel ID="pnlDetails" runat="server" Width="100%" CssClass="Normal">
                            <asp:DataGrid ID="gridReportTypes" Width="495px" runat="server" BorderColor="Black"
                                AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                Font-Size="8pt" CellPadding="4" GridLines="Vertical" OnItemCommand="gridReportTypes_ItemCommand">
                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Code" HeaderText="<%$Resources:gridReportTypes.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="<%$Resources:gridReportTypes.HeaderText1 %>"></asp:BoundColumn>
                                    <asp:ButtonColumn CommandName="Select" Text="<%$Resources:gridReportTypesItem.Text %>"></asp:ButtonColumn>
                                    <asp:ButtonColumn CommandName="Delete" Text="<%$Resources:gridReportTypesItem.Text1 %>"></asp:ButtonColumn>
                                </Columns>
                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                            </asp:DataGrid>
                        </asp:Panel>
                        <asp:Panel ID="panelEdit" runat="server" Width="100%" CssClass="Normal">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbRptCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lbRptCode.Text %>" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="textCode" MaxLength="10" CssClass="Normal"></asp:TextBox>
                                        <asp:HiddenField runat="server" ID="hiddenID" />
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDescription.Text %>" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="textDescription" Height="40px" MaxLength="100" CssClass="Normal"
                                        TextMode="MultiLine" Width="399px"></asp:TextBox>
                                </td>
                                </tr>
                                <tr>
                                <td></td>
                                                            <td>
                                    <asp:Button runat="server" ID="buttonSubmit" Text="<%$Resources:buttonSubmit.Text %>" OnClick="buttonSubmit_Click" 
                                                                    CssClass="NormalButton" />
                                </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="panelDelete" runat="server" Width="100%" CssClass="Normal">
                            <asp:Label runat="server" ID="labelConfirmDelete" ForeColor="Red" />
                            <asp:Button ID="buttonDelete" runat="server" Text="<%$Resources:buttonDelete.Text %>" 
                                onclick="buttonDelete_Click1" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="udp" runat="server">
                    <ProgressTemplate>
                        <p class="Normal" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;<asp:Label
                                ID="Label3" runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
            </td>
            <td valign="top" align="left" style="width: 100%">
            </td>
        </tr>
    </table>
    <table style="height: 5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" style="width: 100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
