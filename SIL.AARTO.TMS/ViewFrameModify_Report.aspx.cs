using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using System.IO;
using System.Text;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class ViewFrameModify_Report : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;

        //protected string thisPage = "View Frame Modify Report";
        protected string thisPageURL = "ViewFrameModify_Report.aspx";
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            string sAutIntNo = Request.QueryString["autintno"] ?? autIntNo.ToString();
            string sDateFrom = Request.QueryString["datefrom"] == null ? "2000-01-10" : Request.QueryString["datefrom"].ToString();
            string sDateTo = Request.QueryString["dateto"] == null ? "2020-01-10" : Request.QueryString["dateto"].ToString();
            DateTime dtFrom;
            DateTime dtTo;

            if (sDateFrom == string.Empty) 
                sDateFrom = "2000-01-01";
            if (sDateTo == string.Empty) 
                sDateTo = "2100-01-01";

            DateTime.TryParse(sDateFrom, out dtFrom);
            DateTime.TryParse(sDateTo, out dtTo);
             
            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "FrameModify");
            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "FrameModify.dplx", "FrameModify", "system", "");
                reportPage = "FrameModify.dplx";
            }

            string path = Server.MapPath("reports/" + reportPage);

            if (!File.Exists(path))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            DocumentLayout doc = new DocumentLayout(path);

            StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
            query.ConnectionString = this.connectionString;
            ParameterDictionary parameters = new ParameterDictionary();
            parameters.Add("AutIntNo", int.Parse (sAutIntNo ) );
            parameters.Add("DateFrom", dtFrom );
            parameters.Add("DateTo", dtTo );


            Document report = doc.Run(parameters);
            byte[] buffer = report.Draw();

            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(buffer);
            Response.End();
            
        }

    }
}