<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.NoticePostReturn" Codebehind="NoticePostReturn.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat=server>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">

        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptSearch" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptSearch.Text %>" OnClick="btnOptSearch_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptReport" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptReport.Text %>" OnClick="btnOptReport_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <asp:UpdatePanel ID="UDP" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                                <p style="text-align: center;">
                                    <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;
                                </p>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="Server" Width="100%">
                                <table style="width: 755px">
                                    <tr>
                                        <td style="width: 175px; height: 41px">
                                            <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="200px" Text="<%$Resources:lblSequenceNum.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 41px; width: 300px;">
                                            <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 175px; height: 27px">
                                            <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="200px" Text="<%$Resources:lblTicketNum.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 27px; width: 350px;"><asp:TextBox ID="txtSearchTicket" runat="server" Width="214px" CssClass="Normal"></asp:TextBox>
                                            <asp:Button ID="btnSearch" runat="server" CssClass="NormalButton" Text="<%$Resources:btnSearch.Text %>" OnClick="btnSearch_Click"
                                                Width="80px"></asp:Button></td>
                                    </tr>
                                    <tr>
                                        <td height="2" style="width: 175px">
                                            &nbsp;</td>
                                        <td height="2" style="width: 187px">
                                            <asp:HiddenField ID="txtNPRRIntNo" runat="server"></asp:HiddenField>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlDetails" Visible="false" runat="server" Width="100%">
                                <table>
                                    <tr>
                                        <td width="157" height="2">
                                            <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTicketNo.Text %>"></asp:Label>
                                        </td>
                                        <td width="157" height="2">
                                            <asp:TextBox ID="txtTicketNo" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="trNoticeNumber" visible="false" runat="server">
                                        <td width="157" height="2">
                                            <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblNoticeNumber.Text %>"></asp:Label>
                                        </td>
                                        <td width="157" height="2">
                                            <asp:TextBox ID="txtNoticeNumber" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="157" style="height: 2px">
                                            <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPostReturnType.Text %>"></asp:Label></td>
                                        <td width="157" style="height: 2px">
                                            <asp:DropDownList ID="ddlPostReturnType" runat="server" CssClass="Normal" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlPostReturnType_SelectedIndexChanged" Width="180px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="157" style="height: 2px">
                                            <asp:Label ID="Label17" runat="server" CssClass="NormalBold" Text="<%$Resources:lblUser.Text %>"></asp:Label>
                                        </td>
                                        <td id="tdSel" width="157" onmouseover="" style="height: 2px">
                                            <asp:TextBox ID="txtUser" runat="server" Width="180px" CssClass="Normal" Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="157" height="2">
                                            <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Text="<%$Resources:lblBoxNumber.Text %>"></asp:Label>
                                        </td>
                                        <td width="157" height="2">
                                            <asp:TextBox ID="txtBoxNumber" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="157" height="2">
                                            <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblStorageLocation.Text %>"></asp:Label>
                                        </td>
                                        <td width="157" height="2">
                                            <asp:TextBox ID="txtStorageLocation" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10" height="2">
                                            &nbsp;</td>
                                        <td width="157" height="2">
                                            <asp:Button ID="btnAction" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAction.Text %>" OnClick="btnAction_Click">
                                            </asp:Button></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                       
                    <!-- <asp:Panel visible="true" ID="lblReportHeader" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="Label66" runat="server" Width="100%" CssClass="ContentHead">Notice Post Returned Report</asp:Label>
                        </p>
                        <p>
                            <asp:Label ID="Label777" runat="server" CssClass="NormalRed" ></asp:Label>&nbsp;
                        </p>
                    </asp:Panel> -->
                    <asp:Panel Visible="false" ID="pnlReport" runat="server" Width="100%">
                        <table>
                         <tr>
                                        <td style="width: 141px; height: 50px">
                                            <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="159px" Text="<%$Resources:lblSequenceNum.Text %>"></asp:Label>
                                        </td>
                                        <td style="height: 50px; width: 400px;">
                                            <uc1:TicketNumberSearch ID="TicketNumberSearch2" runat="server" OnNoticeSelected="NoticeSelectedReport" />
                                        </td>
                                    </tr>
                            <tr>
                                <td width="157" height="2">
                                    <asp:Label ID="lblTicketNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTicketNum.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtTicketNoReport" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="157" height="2">
                                    <asp:Label ID="lblRegNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblRegNo.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtRegistrationNumber" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="157" height="2">
                                    <asp:Label ID="lblName" runat="server" CssClass="NormalBold" Text="<%$Resources:lblName.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtName" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="157" height="2">
                                    <asp:Label ID="lblBoxNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblBoxNumber.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtBoxNo" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="157" height="2">
                                    <asp:Label ID="lblLocation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblStorageLocation.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">
                                    <asp:TextBox ID="txtLocation" runat="server" Width="180px" CssClass="Normal"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="157" height="2">
                                    <asp:Label ID="lblDate" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDate.Text %>"></asp:Label>
                                </td>
                                <td width="157" height="2">

                                    <asp:TextBox runat="server" ID="dtpDate" CssClass="Normal" Height="20px" 
                                                autocomplete="off" UseSubmitBehavior="False" 
                                                />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" 
                                                TargetControlID="dtpDate" Format="yyyy-MM-dd" >
                                                        </cc1:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="dtpDate" CssClass="NormalRed" Display="Dynamic" 
                                            ErrorMessage="<%$Resources:reqDate.ErrorMsg %>" 
                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator> </td>
                            </tr>
                            <tr>
                                <td width="10" height="2">
                                    &nbsp;</td>
                                <td width="157" height="2">
                                    <asp:Button ID="btnViewReport" runat="server" CssClass="NormalButton" Text="<%$Resources:btnViewReport.Text %>"
                                        OnClick="btnViewReport_Click"></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td> </ContentTemplate>
                    </asp:UpdatePanel>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
