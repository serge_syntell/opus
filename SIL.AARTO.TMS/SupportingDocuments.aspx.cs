using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
	/// <summary>
	/// Summary description for ApplicationObjectViewer
	/// </summary>
    public partial class SupportingDocuments : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string tempFileLoc = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = string.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SupportingDocuments.aspx";
        //protected string thisPage = "Supporting Documents";

        protected int autIntNo = -1;
        private int repIntNo = 0;
        private int srIntNo = 0;
        private string login = string.Empty;
               
        protected override void OnLoad(System.EventArgs e)
        {
            
            connectionString = Application["constr"].ToString();
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            this.login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (ViewState["RepIntNo"] == null)
            {
                this.repIntNo = Convert.ToInt32(Request.QueryString["code_rep"].ToString());
                this.srIntNo = Convert.ToInt32(Request.QueryString["code_srep"].ToString());

                ViewState.Add("RepIntNo", repIntNo);
                ViewState.Add("SRIntNo", srIntNo);

                //lblTicketNo.Text = "Notice: " + Request.QueryString["ticket"].ToString();
                lblTicketNo.Text = (string)GetLocalResourceObject("lblNotice.Text") + Request.QueryString["ticket"].ToString();
            }
            else
            {
                this.repIntNo = (int)ViewState["RepIntNo"];
                this.srIntNo = (int)ViewState["SRIntNo"];
            }

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.BindRepDocGrid(repIntNo, srIntNo);
            }
        }

        protected void btnAddDoc_Click(object sender, EventArgs e)
        {
            if (FileUpload1.PostedFile == null ||
                (FileUpload1.PostedFile.FileName.ToString().Equals("") && FileUpload1.PostedFile.ContentLength == 0))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                return;
            }
            this.repIntNo = (int)this.ViewState["RepIntNo"];

            RepDocumentDB repDoc = new RepDocumentDB(connectionString);

            //string rdName = Path.GetFileNameWithoutExtension(File1.PostedFile.FileName.ToString());
            string rdName = txtDocName.Text.Trim();
            string rdType = Path.GetExtension(FileUpload1.PostedFile.FileName.ToString());

            rdType = rdType.ToUpper().Substring(1, 3);
            int size = FileUpload1.PostedFile.ContentLength;
            string errMsg = string.Empty;

            if (rdType.Equals("TIF") || rdType.Equals("JPG") || rdType.Equals("GIF") || rdType.Equals("PDF") || rdType.Equals("PNG"))
            {
                //byte[] imageData = GetDoc(FileUpload1.PostedFile.FileName, ref size, ref errMsg);
                byte[] imageData = FileUpload1.FileBytes;

                if (imageData != null)
                {
                    int rdIntNo = repDoc.AddRepDocument(repIntNo, rdName, rdType, txtAddRDComments.Text, login, srIntNo);
                    if (rdIntNo > 0)
                    {                       
                        ImageProcesses imageProc = new ImageProcesses(connectionString);

                        string tableName = string.Empty;

                        if (repIntNo > 0)
                            tableName = "RepDocument";
                        else if (srIntNo > 0)
                            tableName = "SummonsRepDocument";

                        bool success = imageProc.SaveImageToDB(rdIntNo, "RDIntNo", tableName, "RDImage", imageData, ref errMsg);

                        if (!success)
                            lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text1"), errMsg);
                        else
                        {
                            lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                           
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SupportingDocuments, PunchAction.Add);  

                        }
                    }
                    else
                    {
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text1"), errMsg);
                    }
                }
                else
                {
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text3"), errMsg);
                }
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            }
            this.lblError.Visible = true;
            this.BindRepDocGrid(repIntNo, srIntNo);
        }

        protected void grvRepDocs_SelectedIndexChanged(object sender, EventArgs e)
        {
            int rdIntNo = (int)grvRepDocs.SelectedDataKey[0];
            string rdType = grvRepDocs.SelectedDataKey[1].ToString();

            if (rdIntNo > 0)
            {
                Session["ShowRDIntNo"] = rdIntNo;
                Session["ShowRepIntNo"] = this.repIntNo;
                Session["ShowSRIntNo"] = this.srIntNo;

                // Open a new window with the representation document and print it
                if (rdType.Equals("PDF"))
                    Helper_Web.BuildPopup(this, "RepDocViewer.aspx", null);
                else
                    Helper_Web.BuildPopup(this, "DocViewer.html", null);
                //PunchStats805806 enquiry SupportingDocuments
            }
        }

        private void BindRepDocGrid(int repIntNo, int srIntNo)
        {
            RepDocumentDB repDoc = new RepDocumentDB(connectionString);
            SqlDataReader rdList = repDoc.GetRepDocumentList(repIntNo, srIntNo);
            grvRepDocs.DataSource = rdList;
            grvRepDocs.DataBind();
        }

        //private byte[] GetDoc(string filePath, ref int fileLength, ref string errMessage)
        //{
        //    try
        //    {
        //        FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
        //        BinaryReader br = new BinaryReader(fs);

        //        byte[] doc = br.ReadBytes((int)fs.Length);

        //        fileLength = Convert.ToInt32(fs.Length);

        //        br.Close();
        //        fs.Close();
        //        return doc;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        return null;
        //    }
        //}


        protected void grvRepDocs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                grvRepDocs.SelectedIndex = Convert.ToInt32(e.CommandArgument);
                int rdIntNo = (int)this.grvRepDocs.DataKeys[this.grvRepDocs.SelectedIndex].Value;
                
                RepDocumentDB repDoc = new RepDocumentDB(connectionString);

                string errMessage = string.Empty;
                int delete = repDoc.DeleteRepDocument(rdIntNo, ref errMessage, this.repIntNo, this.srIntNo);

                if (delete == -1)
                {
                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), errMessage);
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SupportingDocuments, PunchAction.Delete);  
                }
                lblError.Visible = true;

                BindRepDocGrid(repIntNo, srIntNo);

            }
        }
        protected void grvRepDocs_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
       
}   
}
