using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
//using BarcodeNETWorkShop;
using SIL.AARTO.BLL.BarCode;
using ceTe.DynamicPDF.Merger;
using Stalberg.TMS.Data.Datasets;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.PrintFile;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for Second Notices.
    /// </summary>
    public partial class SecondNotice_HWO_Viewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int _status = 260;
        private string _showAll = "N";
        private string printFile = string.Empty;
        private int autIntNo = 0;
        private string loginUser;
        private int _option = 2;              //2nd Notice

        private const int FIRST_NOTICE = 1;
        private const int SECOND_NOTICE = 2;

        protected string styleSheet;
        protected string backgroundImage;

        protected string thisPageURL = "SecondNotice_HWO_Viewer.aspx";
        protected string thisPage = "View HWO Second Notices";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;


        // Remove BarcodeNewImage instead of SIL.AARTO.BLL.BarCode  Jake 2011-02-25
        //BarcodeNETImage barcode = null;
        ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder phBarCode;
        System.Drawing.Image barCodeImage = null;

        //byte[] b = null;

        //private ConnectionInfo crConnectionInfo = new ConnectionInfo();

        #region 20120301 Oscar disabled for backup
        ///// <summary>
        ///// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        ///// </summary>
        ///// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        //protected override void OnLoad(EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    //get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");
        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    //get user details
        //    Stalberg.TMS.UserDB user = new UserDB(connectionString);
        //    Stalberg.TMS.UserDetails userDetails = new UserDetails();

        //    userDetails = (UserDetails)Session["userDetails"];
        //    loginUser = userDetails.UserLoginName;

        //    autIntNo = Convert.ToInt32(Session["printAutIntNo"]);

        //    if (Request.QueryString["printfile"].ToString() == null)
        //    {
        //        Response.Write("There was no print file supplied!");
        //        Response.End();
        //        return;
        //    }

        //    printFile = Request.QueryString["printfile"].ToString(); // Ciprus print request file name or none is the default
        //    title = printFile;

        //    //Modify by Tod Zhang on 090927
        //    string prefix = string.Empty;
        //    string longPrefix = string.Empty;
        //    string extendedPrefix = string.Empty;

        //    string sTempEng = string.Empty;
        //    string sTempAfr = string.Empty;
        //    DocumentLayout doc;

        //    int noticeStage = SECOND_NOTICE;

        //    AuthorityRulesDB authRules = new AuthorityRulesDB(connectionString);
        //    NoticeDB notice = new NoticeDB(this.connectionString);
        //    DataSet ds = null;

        //    if (printFile != "-1")
        //    {
        //        prefix = this.printFile.Substring(0, 3).ToUpper();
        //        longPrefix = printFile.Substring(0, 7).ToUpper();
        //        extendedPrefix = printFile.Substring(0, 11).ToUpper();

        //        // See if its a single notice
        //        string pattern = @"^\w{2,}/\d{2,}/\d{2,}/\d{3,}$";
        //        Regex regex = new Regex(pattern, RegexOptions.Singleline);
        //        if (regex.IsMatch(this.printFile))
        //        {
        //            if (!this.printFile[0].Equals('*'))
        //                this.printFile = "*" + this.printFile;

        //            ds = notice.GetNoticeCheckRLVDS(this.printFile.Substring(1, this.printFile.Length - 1));
        //        }
        //    }

        //    if (ds != null)
        //    {
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            DataRow dr = ds.Tables[0].Rows[0];
        //            prefix = dr["PrintFileName"].ToString();
        //            //add the check for the status of the notice here
        //            noticeStage = Convert.ToInt16(dr["NoticeStage"]);
        //        }

        //        ds.Dispose();
        //    }

        //    //20090113 SD	
        //    //AutIntNo, ARCode and LastUser need to be set from here
        //    AuthorityRulesDetails arDet1 = new AuthorityRulesDetails();
        //    arDet1.AutIntNo = autIntNo;
        //    arDet1.ARCode = "2550";
        //    arDet1.LastUser = this.loginUser;

        //    DefaultAuthRules authRule = new DefaultAuthRules(arDet1, this.connectionString);
        //    KeyValuePair<int, string> valueArDet1 = authRule.SetDefaultAuthRule();

        //    //20090113 SD	
        //    //AutIntNo, ARCode and LastUser need to be set from here
        //    AuthorityRulesDetails arDet2 = new AuthorityRulesDetails();
        //    arDet2.AutIntNo = this.autIntNo;
        //    arDet2.ARCode = "4250";
        //    arDet2.LastUser = this.loginUser;

        //    DefaultAuthRules authRule1 = new DefaultAuthRules(arDet2, this.connectionString);
        //    KeyValuePair<int, string> valueArDet2 = authRule1.SetDefaultAuthRule();

        //    //if the rule for printing 2nd notice is turned off or they want see the notice based on the selected viewer, regardless of status
        //    if (valueArDet1.Value.Equals("N") || valueArDet2.Value.Equals("N") || valueArDet2.Key == 0)
        //        noticeStage = SECOND_NOTICE;

        //    if (noticeStage == FIRST_NOTICE)
        //    //have to place this here to cater for printing 1st notices for new offenders/changed regno's
        //    {
        //        Response.Redirect("FirstNoticeViewer.aspx?printfile=" + printFile);
        //    }

        //    //BD 20090128 - removing of auth rule to control what ticket processing being used, now using AutTicketProcessor
        //    AuthorityDB authDB = new AuthorityDB(this.connectionString);
        //    AuthorityDetails authDetails = authDB.GetAuthorityDetails(autIntNo);
        //    string format = "TMS";
        //    if (authDetails.AutTicketProcessor == "CiprusPI" || authDetails.AutTicketProcessor == "Cip_CofCT")
        //        format = authDetails.AutTicketProcessor;

        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);
        //    string reportPage = string.Empty;
        //    string sTemplate = string.Empty;

        //    //****************************************************

        //    //dls 090710 - need to make all the string checks ToUpper - user doesn't necessarily use the same case
        //    // Check the print file prefix
        //    //switch (prefix)
        //    if (prefix.ToUpper() == "HWO")
        //    {
        //        reportPage = arn.GetAuthReportName(autIntNo, "SecondNoticeHWO");

        //        if (reportPage.Equals(string.Empty))
        //        {
        //            reportPage = "SecondNotice_MB_HWO.dplx";
        //            arn.AddAuthReportName(autIntNo, reportPage, "SecondNoticeHWO", "System", "SecondNoticeTemplate_MB_HWO.pdf");
        //        }

        //        sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "SecondNoticeHWO");
        //    }

        //    //****************************************************
        //    //SD:  20081120 - check that report actually exists
        //    string templatePath = string.Empty;
        //    string reportPath = Server.MapPath("Reports/" + reportPage);

        //    if (!File.Exists(reportPath))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {
        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    doc = new DocumentLayout(reportPath);
        //    Query query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;
        //    ParameterDictionary parameters = new ParameterDictionary();

        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblId = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblId");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReference = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReference");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReferenceB = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReferenceB");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFormattedNotice = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFormattedNotice");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblForAttention = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblForAttention");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblNoticeNumber = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblNoticeNumber");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPaymentInfo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPaymentInfo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblStatRef = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblStatRef");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblTime = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblTime");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblLocDescr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblLocDescr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDescrEng = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDescrEng");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDescrAfr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDescrAfr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCode = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCode");
        //    //ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCameraNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCamera");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOfficerNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOfficerNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFineAmount = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAmount");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPrintDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPrintDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOffenceTime = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOffenceTime");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAut = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblText");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblLocation = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblLocation");
        //    //ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSpeedLimit = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblSpeedLimit");
        //    //ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblSpeed = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblSpeed");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblOfficer = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblOfficer");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblRegNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblRegNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblIssuedBy = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblIssuedBy");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPaymentDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPaymentDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCourtName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCourtName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblEasyPay = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblEasyPay");
        //    //ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFilmNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFilmNo");
        //    //ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblFrameNo = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblFrameNo");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblVehicle = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblVehicle");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAuthorityAddress = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAuthorityAddress");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutTel = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAutTel");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutFax = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblAutFax");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblDisclaimer = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblDisclaimer");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReprintDate = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReprintDate");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblReprintDateText = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblReprintDateText");

        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAuthName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AuthName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutDepart = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AutDepart");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutPhysAddr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AutPhysAddr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblAutPostAddr = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AutPostAddr");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblCourt = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblCourt");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl_AuthName2 = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_AuthName2");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lbl_Court = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lbl_Court");
            
        //    phBarCode = (ceTe.DynamicPDF.ReportWriter.ReportElements.PlaceHolder)doc.GetElementById("phBarCode");
        //    phBarCode.LaidOut += new PlaceHolderLaidOutEventHandler(ph_BarCode);

        //    ceTe.DynamicPDF.ReportWriter.ReportElements.Label lblPrintFileName = (ceTe.DynamicPDF.ReportWriter.ReportElements.Label)doc.GetElementById("lblPrintFileName");
        //    ceTe.DynamicPDF.ReportWriter.ReportElements.PageNumberingLabel PageNumberingLabel1 = (ceTe.DynamicPDF.ReportWriter.ReportElements.PageNumberingLabel)doc.GetElementById("PageNumberingLabel1");


        //    if (Session["showAllNotices"] != null)
        //        if (Session["showAllNotices"].ToString().Equals("Y")) _showAll = "Y";

        //    //get additional parameters for date rules and sysparam setting
        //    SysParamDB sp = new SysParamDB(this.connectionString);

        //    string violationCutOff = "N";
        //    int spValue = 0;

        //    bool found = sp.CheckSysParam("ViolationCutOff", ref spValue, ref violationCutOff);

        //    DateRulesDetails dateRule = new DateRulesDetails();
        //    dateRule.AutIntNo = autIntNo;
        //    dateRule.DtRStartDate = "NotOffenceDate";
        //    dateRule.DtREndDate = "NotIssue1stNoticeDate";
        //    dateRule.LastUser = this.loginUser;

        //    DefaultDateRules rule = new DefaultDateRules(dateRule, this.connectionString);
        //    int noOfDaysIssue = rule.SetDefaultDateRule();

        //    if (Request.QueryString["printfile"] != null)
        //    {
        //        //autIntNo = Convert.ToInt32(Session["autIntNo"]);
        //        autIntNo = Convert.ToInt32(Session["printAutIntNo"]);

        //        string tempFileLoc = string.Empty;
        //        SqlConnection con = null;
        //        SqlCommand com = null;
        //        SqlDataReader result = null;

        //        try
        //        {
        //            // Fill the DataSet
        //            con = new SqlConnection(this.connectionString);
        //            com = new SqlCommand("NoticePrint", con);
        //            com.CommandType = CommandType.StoredProcedure;
        //            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = autIntNo;
        //            com.Parameters.Add("@PrintFile", SqlDbType.VarChar, 50).Value = printFile;
        //            com.Parameters.Add("@Status", SqlDbType.Int, 4).Value = _status;
        //            com.Parameters.Add("@ShowAll", SqlDbType.Char, 1).Value = _showAll;
        //            com.Parameters.Add("@Format", SqlDbType.VarChar, 10).Value = format;
        //            com.Parameters.Add("@Option", SqlDbType.Int, 4).Value = _option;
        //            com.Parameters.Add("@LastUser", SqlDbType.VarChar, 50).Value = loginUser;
        //            com.Parameters.Add("@ViolationCutOff", SqlDbType.Char, 1).Value = violationCutOff;
        //            com.Parameters.Add("@NoOfDaysIssue", SqlDbType.Int, 4).Value = noOfDaysIssue;

        //            con.Open();
        //            result = com.ExecuteReader(CommandBehavior.CloseConnection);

        //            MergeDocument merge = new MergeDocument();
        //            byte[] buffer;
        //            Document report = null;
        //            int noOfNotices = 0;

        //            char pad0Char = Convert.ToChar("0");

        //            while (result.Read())
        //            {
        //                try
        //                {
        //                    if (reportPage.ToUpper().IndexOf("_MB") >= 0)
        //                    {
        //                        barCodeImage = Code128Rendering.MakeBarcodeImage(result["NotTicketNo"].ToString(), 1, 25, true);

        //                        lblAuthName.Text = result["AutName"].ToString().Trim();
        //                        lbl_AuthName2.Text = result["AutName"].ToString();
        //                        lblCourt.Text = result["NotCourtName"].ToString().Trim();
        //                        lbl_Court.Text = result["NotCourtName"].ToString().Trim();
        //                        lblAutPhysAddr.Text = (result["AutPhysAddr1"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr1"].ToString().Trim() + "\n")
        //                                + (result["AutPhysAddr2"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr2"].ToString().Trim() + "\n")
        //                                + (result["AutPhysAddr3"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr3"].ToString().Trim() + "\n")
        //                                + (result["AutPhysAddr4"].ToString().Trim().Equals("") ? "" : result["AutPhysAddr4"].ToString().Trim() + "\n")
        //                                + result["AutPhysCode"].ToString();
        //                        lblAutPostAddr.Text = (result["AutPostAddr1"].ToString().Trim().Equals("") ? "" : result["AutPostAddr1"].ToString().Trim() + "\n")
        //                                + (result["AutPostAddr2"].ToString().Trim().Equals("") ? "" : result["AutPostAddr2"].ToString().Trim() + "\n")
        //                                + (result["AutPostAddr3"].ToString().Trim().Equals("") ? "" : result["AutPostAddr3"].ToString().Trim() + "\n")
        //                                + result["AutPostCode"].ToString();

        //                        if (result["NotSendTo"].ToString().Equals("P"))
        //                        {
        //                            lblForAttention.Text = result["PrxInitials"].ToString() + " " + result["PrxSurname"].ToString() + " as Representative of " + result["DrvSurname"].ToString();
        //                            lblId.Text = result["PrxIDNUmber"].ToString().Trim();
        //                        }
        //                        else
        //                        {
        //                            lblForAttention.Text = result["DrvInitials"].ToString() + " " + result["DrvSurname"].ToString();
        //                            lblId.Text = result["DrvIDNUmber"].ToString().Trim();
        //                        }

        //                        lblAddress.Text = (result["DrvPOAdd1"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd1"].ToString().Trim()) + "\n")
        //                                + (result["DrvPOAdd2"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd2"].ToString().Trim()) + "\n")
        //                                + (result["DrvPOAdd3"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd3"].ToString().Trim()) + "\n")
        //                                + (result["DrvPOAdd4"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd4"].ToString().Trim()) + "\n")
        //                                + (result["DrvPOAdd5"].ToString().Trim().Equals("") ? "" : (result["DrvPOAdd5"].ToString().Trim()) + "\n")
        //                                + result["DrvPOCode"].ToString().Trim();

        //                        lblNoticeNumber.Text = result["NotTicketNo"].ToString().Trim().Replace("/", " / ");

        //                        lblStatRef.Text = (result["ChgStatRefLine1"] == DBNull.Value ? " " : result["ChgStatRefLine1"].ToString().Trim())
        //                            + (result["ChgStatRefLine2"] == DBNull.Value ? " " : result["ChgStatRefLine2"].ToString().Trim())
        //                            + (result["ChgStatRefLine3"] == DBNull.Value ? " " : result["ChgStatRefLine3"].ToString().Trim())
        //                            + (result["ChgStatRefLine4"] == DBNull.Value ? " " : result["ChgStatRefLine4"].ToString().Trim());

        //                        lblDate.Text = string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]);
        //                        lblTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                        lblLocDescr.Text = result["NotLocDescr"].ToString();
        //                        //lblFilmNo.Text = result["NotFilmNo"].ToString().Trim();
        //                        //lblFrameNo.Text = result["NotFrameNo"].ToString().Trim();

        //                        sTempEng = result["OcTDescr"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());
        //                        sTempAfr = result["OcTDescr1"].ToString().Replace("(X~1)", result["NotRegNo"].ToString()).Replace("(X~2)", result["NotSpeedLimit"].ToString()).Replace("(X~3)", result["MinSpeed"].ToString());

        //                        if (result["NotNewOffender"].ToString().Equals("Y"))
        //                        {
        //                            lblOffenceDescrEng.Text = sTempEng.Replace("registered owner", "driver");
        //                            lblOffenceDescrAfr.Text = sTempAfr.Replace("geregistreerde eienaar", "bestuurder");
        //                        }
        //                        else
        //                        {
        //                            lblOffenceDescrEng.Text = sTempEng;
        //                            lblOffenceDescrAfr.Text = sTempAfr;
        //                        }

        //                        lblPaymentDate.Text = string.Format("{0:d MMMM yyyy}", result["Not2ndPaymentDate"]);
        //                        //lblCourtName.Text = result["NotCourtName"].ToString().ToUpper();
        //                        lblCode.Text = result["ChgOffenceCode"].ToString();
                                
        //                        //dls no need for camera if this is an HWO
        //                        //lblCameraNo.Text = result["NotCamSerialNo"].ToString();
                                
        //                        lblOfficerNo.Text = result["NotOfficerNo"].ToString();
        //                        lblFineAmount.Text = "R " + string.Format("{0:0.00}", Decimal.Parse(result["ChgRevFineAmount"].ToString()));

        //                        lblPrintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint1stNoticeDate"]);
        //                        lblReprintDate.Text = string.Format("{0:d MMMM yyyy}", result["NotPrint2ndNoticeDate"]);

        //                        lblOffenceDate.Text = string.Format("{0:d MMMM yyyy}", result["NotOffenceDate"]);
        //                        lblOffenceTime.Text = string.Format("{0:HH:mm}", result["NotOffenceDate"]);
        //                        lblLocation.Text = result["NotLocDescr"].ToString();
        //                        //lblSpeedLimit.Text = result["NotSpeedLimit"].ToString();
        //                        //lblSpeed.Text = result["NotSpeed1"].ToString();
        //                        //lblOfficer.Text = result["NotOfficerNo"].ToString();  //result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();
        //                        lblRegNo.Text = result["NotRegNo"].ToString();
        //                        lblDisclaimer.Text = result["Disclaimer"].ToString();

        //                        lblOfficer.Text = result["NotOfficerInit"].ToString() + " " + result["NotOfficerSName"].ToString();

        //                    }

        //                    report = doc.Run(parameters);

        //                    ImportedPageArea importedPage;
        //                    byte[] bufferTemplate;
        //                    if (!sTemplate.Equals(""))
        //                    {
        //                        if (sTemplate.ToLower().IndexOf(".dplx") > 0)
        //                        {
        //                            DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
        //                            Query queryTemplate = (Query)template.GetQueryById("Query");
        //                            queryTemplate.ConnectionString = this.connectionString;
        //                            ParameterDictionary parametersTemplate = new ParameterDictionary();
        //                            Document reportTemplate = template.Run(parametersTemplate);
        //                            bufferTemplate = reportTemplate.Draw();
        //                            PdfDocument pdf = new PdfDocument(bufferTemplate);
        //                            PdfPage page = pdf.Pages[0];
        //                            importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
        //                        }
        //                        else
        //                        {
        //                            //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                            importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                        }

        //                        ceTe.DynamicPDF.Page rptPage = report.Pages[0];
        //                        rptPage.Elements.Insert(0, importedPage);
        //                    }
        //                    buffer = report.Draw();
        //                    merge.Append(new PdfDocument(buffer));
        //                }
        //                catch (Exception ex)
        //                {
        //                    String sError = ex.Message;
        //                }

        //                noOfNotices++;
        //            }
        //            result.Close();
        //            con.Dispose();

        //            if (!printFile.Substring(0, 1).Equals("*") && !printFile.Equals("-1"))
        //            {
        //                //dls 071228 - add summary report page and append to end of PDF document
        //                reportPage = "NoticeSummary.dplx";
        //                string path = Server.MapPath("reports/" + reportPage);

        //                if (!File.Exists(path))
        //                {
        //                    string error = "Report " + reportPage + " does not exist";
        //                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //                    Response.Redirect(errorURL);
        //                    return;
        //                }

        //                NoticeSummary summary = new NoticeSummary(this.connectionString, autIntNo, printFile, format, _showAll, _status, _option, path, reportPage, noOfNotices);

        //                byte[] sumReport = summary.CreateSummary();

        //                merge.Append(new PdfDocument(sumReport));
        //            }

        //            byte[] buf = merge.Draw();

        //            phBarCode.LaidOut -= new PlaceHolderLaidOutEventHandler(ph_BarCode);

        //            Response.ClearContent();
        //            Response.ClearHeaders();
        //            Response.ContentType = "application/pdf";
        //            Response.BinaryWrite(buf);
        //            Response.End();
        //        }
        //        catch
        //        {
        //        }
        //    }
        //}

        //public void ph_BarCode(object sender, PlaceHolderLaidOutEventArgs e)
        //{
        //    if (barCodeImage != null)
        //    {
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            barCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //            ceTe.DynamicPDF.PageElements.Image img = new ceTe.DynamicPDF.PageElements.Image(ImageData.GetImage(ms.GetBuffer()), 0, 0);
        //            img.Height = 25.0F;
        //            e.ContentArea.Add(img);
        //        }
        //    }
        //}
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            autIntNo = Convert.ToInt32(Session["printAutIntNo"]);

            if (Request.QueryString["printfile"] != null)
                title = Request.QueryString["printfile"].ToString();

            PrintFileProcess process = new PrintFileProcess(this.connectionString, this.loginUser);
            process.BuildPrintFile(new PrintFileModule2ndNoticeHWO(), autIntNo, Request.QueryString["printfile"]);
        }

    }
}
