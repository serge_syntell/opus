<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ReceiptCashBoxSetup" Codebehind="ReceiptCashBoxSetup.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center" style="width: 182px; height: 501px;">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" colspan="1" style="width: 90%;">
                    <table width="100%" border="0">
                        <tr>
                            <td valign="top">
                                <p style="text-align: center;">
                                    <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Width="309px" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlCashboxes" runat="server" CssClass="Normal" Width="100%">
                                    <p style="text-align: center;" class="NormalBold">
                                        <asp:DataGrid ID="grdCashbox" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                                            ShowFooter="True" CellPadding="5" OnItemCommand="grdCashbox_ItemCommand">
                                            <HeaderStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundColumn DataField="CBIntNo" HeaderText="CBIntNo" Visible="False">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                               <%-- <asp:BoundColumn DataField="AutIntNo" HeaderText="AutIntNo" Visible="False">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>--%>
                                                <asp:BoundColumn DataField="CBName" HeaderText="<%$Resources:grdCashbox.HeaderText %>">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="CBStartAmount" HeaderText="<%$Resources:grdCashbox.HeaderText1 %>" DataFormatString="R {0:N}">
                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Right" Wrap="False" />
                                                </asp:BoundColumn>
                                                <asp:EditCommandColumn CancelText="<%$Resources:grdCashbox.CancleText %>" EditText="<%$Resources:grdCashbox.EditText %>" UpdateText="<%$Resources:grdCashbox.UpdateText %>"></asp:EditCommandColumn>
                                            </Columns>
                                            <FooterStyle CssClass="CartListHead" />
                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" Wrap="False" />
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <pager:AspNetPager id="grdCashBoxPager" runat="server" 
                                            showcustominfosection="Right" width="300px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"   PageSize="10" onpagechanged="grdCashBoxPager_PageChanged"
                                    ></pager:AspNetPager>
                                    </p>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="pnlEditCashbox" runat="server">
                        <table id="tblEditCashbox" height="125" cellspacing="1" cellpadding="1" border="0"
                            style="width: 418px">
                            <tr>
                                <td style="width: 248px; height: 29px">
                                </td>
                                <td style="width: 620px; height: 29px">
                                    <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Width="236px" Text="<%$Resources:lblDetails.Text %>"></asp:Label></td>
                                <td style="width: 802px; height: 29px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCbname.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBName" runat="server" Width="176px" MaxLength=30></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 28px">
                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="112px" Text="<%$Resources:lblCBStartAmount.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 28px">
                                    <asp:TextBox ID="txtCBStartAmount" runat="server" Width="101px"></asp:TextBox></td>
                                <td style="width: 802px; height: 28px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                        ControlToValidate="txtCBStartAmount" ErrorMessage="<%$Resources:reqRandsVal.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="97px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="92px" Text="<%$Resources:lblReceived.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBReceived" runat="server" Width="101px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                        ControlToValidate="txtCBReceived" ErrorMessage="<%$Resources:reqRandsVal.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="96px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="79px" Text="<%$Resources:lblCBBanked.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBBanked" runat="server" Width="101px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                        ControlToValidate="txtCBBanked" ErrorMessage="<%$Resources:reqRandsVal.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="95px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand500s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBRand500s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCBRand500s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="101px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 30px">
                                    <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand200s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 30px">
                                    <asp:TextBox ID="txtCBRand200s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 30px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCBRand200s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="102px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand100s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBRand100s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCBRand100s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="101px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand50s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBRand50s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCBRand50s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="100px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 28px">
                                    <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand20s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 28px">
                                    <asp:TextBox ID="txtCBRand20s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 28px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtCBRand20s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="100px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand10s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBRand10s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtCBRand10s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="100px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand5s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBRand5s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtCBRand5s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="99px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand2s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBRand2s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtCBRand2s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="97px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand1s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtCBRand1s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtCBRand1s"
                                        ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+" Width="97px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                </td>
                                <td style="width: 620px; height: 29px">
                                </td>
                                <td style="width: 802px; height: 29px">
                                    <asp:Button ID="btnUpdateCB" CssClass="NormalButton" runat="server" OnClick="btnUpdateCB_Click"
                                        Text="<%$Resources:btnUpdateCB.Text %>" Width="136px" /></td>
                            </tr>
                        </table>
                        &nbsp; &nbsp; &nbsp;
                    </asp:Panel>
                    <asp:Panel ID="pnlAddCashbox" runat="server">
                        <table id="Table1" height="125" cellspacing="1" cellpadding="1" border="0" style="width: 418px">
                            <tr>
                                <td style="width: 248px; height: 29px">
                                </td>
                                <td style="width: 620px; height: 29px">
                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="236px" Text="<%$Resources:lblAddNew.Text %>"></asp:Label></td>
                                <td style="width: 802px; height: 29px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label17" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCbname.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBName" runat="server" Width="176px" MaxLength=30></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 28px">
                                    <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Width="112px" Text="<%$Resources:lblCBStartAmount.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 28px">
                                    <asp:TextBox ID="txtAddCBStartAmount" runat="server" Width="101px"></asp:TextBox></td>
                                <td style="width: 802px; height: 28px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                        ControlToValidate="txtAddCBStartAmount" ErrorMessage="<%$Resources:reqRandsVal.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="97px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label19" runat="server" CssClass="NormalBold" Width="92px" Text="<%$Resources:lblReceived.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBReceived" runat="server" Width="101px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                        ControlToValidate="txtAddCBReceived" ErrorMessage="<%$Resources:reqRandsVal.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="96px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label20" runat="server" CssClass="NormalBold" Width="79px" Text="<%$Resources:lblCBBanked.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBBanked" runat="server" Width="101px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                        ControlToValidate="txtAddCBBanked" ErrorMessage="<%$Resources:reqRandsVal.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="95px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label21" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand500s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBRand500s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                        ControlToValidate="txtAddCBRand500s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="101px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 30px">
                                    <asp:Label ID="Label22" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand200s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 30px">
                                    <asp:TextBox ID="txtAddCBRand200s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 30px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                        ControlToValidate="txtAddCBRand200s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="102px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label23" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand100s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBRand100s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                        ControlToValidate="txtAddCBRand100s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="101px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label24" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand50s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBRand50s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                        ControlToValidate="txtAddCBRand50s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="100px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 28px">
                                    <asp:Label ID="Label25" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand20s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 28px">
                                    <asp:TextBox ID="txtAddCBRand20s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 28px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                        ControlToValidate="txtAddCBRand20s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="100px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label26" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand10s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBRand10s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                        ControlToValidate="txtAddCBRand10s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="100px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label27" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand5s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBRand5s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                                        ControlToValidate="txtAddCBRand5s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="99px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label28" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand2s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBRand2s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                                        ControlToValidate="txtAddCBRand2s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="97px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                    <asp:Label ID="Label29" runat="server" CssClass="NormalBold" Width="102px" Text="<%$Resources:lblCBRand1s.Text %>"></asp:Label></td>
                                <td style="width: 620px; height: 29px">
                                    <asp:TextBox ID="txtAddCBRand1s" runat="server" Width="85px"></asp:TextBox></td>
                                <td style="width: 802px; height: 29px">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server"
                                        ControlToValidate="txtAddCBRand1s" ErrorMessage="<%$Resources:reqCBRandInteger.ErrorMessage %>" ValidationExpression="^\d+"
                                        Width="97px"></asp:RegularExpressionValidator></td>
                            </tr>
                            <tr>
                                <td style="width: 248px; height: 29px">
                                </td>
                                <td style="width: 620px; height: 29px">
                                </td>
                                <td style="width: 802px; height: 29px">
                                    <asp:Button ID="btnAddCB" runat="server" CssClass="NormalButton" OnClick="btnAddCB_Click"
                                        Text="<%$Resources:btnAddCB.Text %>" Width="136px" /></td>
                            </tr>
                        </table>
                        &nbsp; &nbsp; &nbsp;
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
