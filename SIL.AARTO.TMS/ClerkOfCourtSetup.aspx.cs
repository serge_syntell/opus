﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Stalberg.TMS.Data;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class ClerkOfCourtSetup : System.Web.UI.Page
    {
        #region Fields

        // Fields
        protected string styleSheet;
        protected string background;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "ClerkOfCourtSetup.aspx";
        //protected string thisPage = "Clerk of court setup";
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo;

        #endregion

        #region User Methods

        private bool CheckRoadBlockRule()
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4522";
            //rule.ARComment = "N=No (Default); Y=Yes";
            //rule.ARDescr = "Enable processing of unauthorised Summonses & Notices at roadblocks";
            //rule.ARNumeric = 0;
            //rule.ARString = "N";
            //rule.AutIntNo = this.autIntNo;
            //rule.LastUser = this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(connectionString);
            //db.GetDefaultRule(rule);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.ARCode = "4522";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.login;

            DefaultAuthRules dr = new DefaultAuthRules(rule, this.connectionString);

            KeyValuePair<int, string> rbRule = dr.SetDefaultAuthRule();
            return rbRule.Value.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
        }

        public void PopulateCourt()
        {
            dpCourtsList.Items.Clear();
            CourtDB courtDB = new CourtDB(connectionString);

            dpCourtsList.DataSource = courtDB.GetCourtList();
            dpCourtsList.DataTextField = "CrtName";
            dpCourtsList.DataValueField = "CrtIntNo";
            dpCourtsList.DataBind();

            dpCourtsList.Items.Insert(0, new ListItem((string)GetLocalResourceObject("dpCourtsList.Items"), "0"));
        }

        private void BindData()
        {
            try
            {
                ClerkofCourtDB clertDB = new ClerkofCourtDB(connectionString);
                int totalCount = 0;
                grdCofCourtList.DataSource = clertDB.GetClerkofCourtList(grdCofCourtListPager.PageSize, grdCofCourtListPager.CurrentPageIndex, out totalCount);
                grdCofCourtList.DataKeyNames = new string[] { "CofCIntNo", "CrtIntNo" };
                grdCofCourtList.DataBind();
                grdCofCourtListPager.RecordCount = totalCount;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        private void ShowClerkOfCourtDetail(int cofcIntNo, int crtIntno)
        {
            try
            {
                ClerkofCourtDB clerkDB = new ClerkofCourtDB(connectionString);
                ClerkOfCourt clerk = clerkDB.GetClerkofCourtByNo(cofcIntNo);

                if (clerkDB != null)
                {
                    txtName.Text = clerk.CofCName;
                    txtSurName.Text = clerk.CofCSurName;
                    txtContact.Text = clerk.CofCContact;
                }

                dpCourtsList.SelectedValue = crtIntno.ToString();
                //dpLaList.SelectedValue = autIntNo.ToString();

                this.pnlClerkDetail.Visible = true;
                this.btnOK.Visible = false;
                this.btnUpdate.Visible = true;
                this.btnDelete.Visible = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int UpdateClerkOfCourt(int cofcIntNo, int crtIntNo, string cofcName, string cofcSurName, string contact)
        {
            try
            {
                ClerkofCourtDB clerkDB = new ClerkofCourtDB(connectionString);
                return clerkDB.ClerkOfCourtModify(cofcIntNo, crtIntNo, cofcName, cofcSurName, contact, this.login);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// AddClerkOfCourt
        /// Jerry 2014-03-07 add errMsg parameter
        /// </summary>
        /// <param name="crtIntNo"></param>
        /// <param name="cofcName"></param>
        /// <param name="cofcSurName"></param>
        /// <param name="contact"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        private int AddClerkOfCourt(int crtIntNo, string cofcName, string cofcSurName, string contact, ref string errMsg)
        {
            try
            {
                ClerkofCourtDB clerkDB = new ClerkofCourtDB(connectionString);
                //Jerry 2014-03-07 check ClerkOfCourt
                int count = clerkDB.CheckClertOfCourt(crtIntNo, cofcName, cofcSurName, ref errMsg);
                if (count != 0)
                {
                    errMsg = (string)GetLocalResourceObject("message4");
                    return 0;
                }

                return clerkDB.AddClertOfCourt(crtIntNo, cofcName, cofcSurName, contact, this.login, ref errMsg);
            }
            catch (Exception ex)
            {
                //throw ex;
                errMsg = ex.Message;
                return 0;
            }
        }

        private bool CheckInput(ref string message)
        {
            //if (txtName.Text.HasNoValue())
            //{
            //    message = "The Clerk of the Court's first name cannot to be blank!";
            //    txtName.Focus();
            //    return false;
            //}
            //if (txtSurName.Text.HasNoValue())
            //{
            //    message = "The Clerk of the Court's surname must not to be blank!";
            //    txtSurName.Focus();
            //    return false;
            //}

            if (txtName.Text.Trim().Length == 0)
            {
                message = (string)GetLocalResourceObject("message");
                txtName.Focus();
                return false;
            }
            if (txtSurName.Text.Trim().Length == 0)
            {
                message = (string)GetLocalResourceObject("message1");
                txtSurName.Focus();
                return false;
            }

            if (dpCourtsList.SelectedIndex <= 0)
            {
                message = (string)GetLocalResourceObject("message2");
                return false;
            }

            //Jerry 2014-03-04 add
            bool isCorrect = SIL.AARTO.BLL.Utility.Validate.CheckPhoneNumber(this.txtContact.Text.Trim());
            if (!isCorrect)
            {
                message = (string)GetLocalResourceObject("message3");
                return false;
            }

            return true;
        }

        private void Initialization()
        {
            try
            {
                this.BindData();
                this.pnlClerkDetail.Visible = false;
            }
            catch { }
        }

        #endregion

        #region System Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve the database connection string
            connectionString = Application["constr"].ToString();
            //connectionString = "Data Source=Jakezyz;Initial Catalog=TMS;Uid=sa;pwd=sa";
            //Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            ViewState.Add("AutIntNo", autIntNo);

            // Set domain specific variables
            General gen = new General();
            background = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            HtmlLink html = new HtmlLink();
            html.Href = styleSheet;

            html.Attributes.Add("rel", "stylesheet");
            html.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(html);

            if (!Page.IsPostBack)
            {
                this.Initialization();

                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                this.btnUpdate.Visible = false;
                this.btnOK.Visible = true;
                this.btnDelete.Visible = false;

                if (this.CheckRoadBlockRule())
                {
                    lblCourtList.Visible = false;
                    dpCourtsList.Visible = false;
                }
                else
                {
                    this.PopulateCourt();
                }
            }
        }

        protected void grdCofCourtList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                int crtIntNo = Convert.ToInt32(grdCofCourtList.SelectedDataKey["CrtIntNo"].ToString());
                //int autIntNo = Convert.ToInt32(grdCofCourtList.SelectedDataKey["AutIntNo"].ToString());
                int cofIntNo = Convert.ToInt32(grdCofCourtList.SelectedDataKey["CofCIntNo"].ToString());

                this.ShowClerkOfCourtDetail(cofIntNo, crtIntNo);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";

            this.pnlClerkDetail.Visible = true;
            this.btnOK.Visible = true;
            this.btnUpdate.Visible = false;
            this.btnDelete.Visible = false;

            txtContact.Text = "";
            txtName.Text = "";
            txtSurName.Text = "";
            dpCourtsList.SelectedIndex = 0;
            dpCourtsList.SelectedIndex = -1;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            string message = string.Empty;
            if (this.CheckInput(ref  message))
            {
                //Jerry 2014-03-07 add errMsg parameter
                int effort = AddClerkOfCourt(Convert.ToInt32(dpCourtsList.SelectedValue), txtName.Text, txtSurName.Text, txtContact.Text, ref message);
                if (effort <= 0)
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text");
                    if (!string.IsNullOrEmpty(message))
                        lblMessage.Text = lblMessage.Text + " " + message;
                }
                else
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text1");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ClerkOfCourt, PunchAction.Add);  

                    //Jerry 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                    //2013-04-07 add by Henry for pagination
                    //grdCofCourtListPager.RecordCount = 0;
                    //grdCofCourtListPager.CurrentPageIndex = 1;
                    Initialization();
                }
            }
            else
            {
                lblMessage.Text = message;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                int cofcIntNo = Convert.ToInt32(grdCofCourtList.SelectedDataKey["CofCIntNo"]);

                ClerkofCourtDB clerkDB = new ClerkofCourtDB(connectionString);
                //Jerry 2014-03-05 check roadblock session CofCourt table firstly
                string errMsg = string.Empty;
                int roadblockSessionCount = clerkDB.CheckClerkInRoadblockSession(cofcIntNo, ref errMsg);
                if (roadblockSessionCount == 0 && !string.IsNullOrEmpty(errMsg))
                {
                    lblMessage.Text = errMsg;
                    return;
                }
                else if (roadblockSessionCount > 0)
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text5");
                    return;
                }

                if (clerkDB.DeleteClerkOfCourtByIntNo(cofcIntNo) <= 0)
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text2");
                }
                else
                {
                  
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ClerkOfCourt, PunchAction.Delete);  
                    //Jerry 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                    //2013-04-07 add by Henry for pagination
                    //grdCofCourtListPager.CurrentPageIndex = 1;
                    //grdCofCourtListPager.RecordCount = 0;
                    if (grdCofCourtList.Rows.Count == 1)
                        grdCofCourtListPager.CurrentPageIndex--;

                    this.Initialization();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //protected void btnHideMenu_Click(object sender, EventArgs e)
        //{
        //    if (this.pnlMainMenu.Visible)
        //    {
        //        this.btnHideMenu.Text = "Show Main Menu";
        //        this.pnlMainMenu.Visible = false;
        //    }
        //    else
        //    {
        //        this.btnHideMenu.Text = "Hide Main Menu";
        //        this.pnlMainMenu.Visible = true;
        //    }
        //}

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            string message = string.Empty;
            if (this.CheckInput(ref message))
            {
                int cogcIntNo = Convert.ToInt32(grdCofCourtList.SelectedDataKey["CofCIntNo"].ToString());
                int effort = this.UpdateClerkOfCourt(cogcIntNo, Convert.ToInt32(dpCourtsList.SelectedValue), txtName.Text, txtSurName.Text, txtContact.Text);
                if (effort <= 0)
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text3"); 
                }
                else
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text4");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.ClerkOfCourt, PunchAction.Change);  
                    //Jerry 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
                    //2013-04-07 add by Henry for pagination
                    //grdCofCourtListPager.RecordCount = 0;
                    //grdCofCourtListPager.CurrentPageIndex = 1;
                    Initialization();
                }
            }
            else
            {
                lblMessage.Text = message;
            }
        }

        protected void dpCourtsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dpCourtsList.SelectedIndex > 0)
            {
            }
        }

        #endregion

        //2013-04-07 add by Henry for pagination
        protected void grdCofCourtListPager_PageChanged(object sender, EventArgs e)
        {
            Initialization();
        }
    }
}
