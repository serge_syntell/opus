using System;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the viewer page for the representations with no results report
    /// </summary>
    public partial class Representation_ReportNoResults : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private ReportDocument _reportDoc = new ReportDocument();
        //private string thisPage = "Representations with no Results";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "Representation_ReportNoResults.aspx";
        protected string loginUser = string.Empty;
        private string printFile = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //SD 10/07/2008 Get autIntNo from query string
            if (Request.QueryString["AutIntNo"] == null)
            {
                Response.Write((string)GetLocalResourceObject("strWriteMsg"));
                Response.End();
                return;
            }

            this.autIntNo = Convert.ToInt32(Request.QueryString["AutIntNo"].ToString().Trim());
            //2014-09-23 Heidi added @BeginDate and @EndDate Filter conditions(5360)
            DateTime beginDate, endDate;
            if (DateTime.TryParse(Request.QueryString["BeginDate"], out beginDate))
            {
                beginDate = beginDate.Date;
            }
            else
            {
                beginDate = DateTime.Now.AddDays(-30).Date;
            }
            if (DateTime.TryParse(Request.QueryString["EndDate"], out endDate))
            {
                endDate = endDate.AddDays(1).Date;
            }
            else
            {
                endDate = DateTime.Now.AddDays(1).Date;
            }
            RepresentationDB db = new RepresentationDB(this.connectionString);
            DataSet ds = db.RepresentationsWithNoResults(this.autIntNo,2000,beginDate.ToString(),endDate.ToString());

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "RepresentationWitNoResult");
            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "RepresentationsWithNoResults.rpt", "RepresentationWitNoResult", "system", "");
                reportPage = "RepresentationsWithNoResults.rpt";
            }

            string reportPath = Server.MapPath("reports/" + reportPage);

            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            _reportDoc.Load(reportPath);

            // Populate the report
            _reportDoc.SetDataSource(ds.Tables[1]);
            ds.Dispose();

            //export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            _reportDoc.Dispose();

            //stuff the PDF file into rendering stream
            //first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

    }
}