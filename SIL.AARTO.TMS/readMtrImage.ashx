﻿<%@ WebHandler Language="C#" Class="readMtrImage" %>

using System;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public class readMtrImage : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = ConfigurationManager.ConnectionStrings["TMSConnectionString"].ToString();
        conn.Open();
        string sql = "select MtrLogo from Metro where MtrIntNo=" + context.Request.QueryString["id"];
        SqlCommand comm = new SqlCommand(sql, conn);
        SqlDataReader dr = comm.ExecuteReader(CommandBehavior.CloseConnection);
        if (dr.Read())
        {
            if(string.IsNullOrEmpty(dr["MtrLogo"].ToString())==false)
            {context.Response.BinaryWrite((byte[])dr["MtrLogo"]);}
        }
        conn.Close();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}