using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class SuspenseAccountUndoAssignment : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SuspenseAccountUndoAssignment.aspx";
        //protected string thisPage = "Undo Suspense Account Assignment";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.autIntNo =int.Parse( this.Session["AutIntNo"].ToString());

            if (!Page.IsPostBack)
            {
                // TODO: Page setup here
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
            }
        }

        

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.pnlConfirm.Visible = false;

            string ticketNo = this.txtTicketNo.Text.Trim();
            if (ticketNo.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            this.lblError.Text = string.Empty;

            SuspenseAccount db = new SuspenseAccount(this.connectionString);
            List<ReversibleNotice> notices = db.FindAssignedNotice(ticketNo, this.autIntNo);
            this.ViewState.Add("Notices", notices);

            this.grdNotices.DataSource = notices;
            this.grdNotices.DataKeyNames = new string[] { "NotIntNo" };
            this.grdNotices.DataBind();

            if (notices.Count == 0)
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            else
            {
                this.lblError.Text = string.Empty;
            }
            //PunchStats805806 enquiry UndoSuspenseAccountAssignment
        }

        protected void grdNotices_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<ReversibleNotice> notices = (List<ReversibleNotice>)this.ViewState["Notices"];
            ReversibleNotice notice = notices[this.grdNotices.SelectedIndex];

            this.pnlConfirm.Visible = true;
            this.lblConfirm.Text = notice.ToString();
        }

        protected void grdNotices_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            List<ReversibleNotice> notices = (List<ReversibleNotice>)this.ViewState["Notices"];
            ReversibleNotice notice = notices[e.Row.DataItemIndex];

            e.Row.Cells[1].Text = notice.OffenceDate.ToString("yyyy-MM-dd");
            e.Row.Cells[2].Text = notice.ReceiptDate.ToString("yyyy-MM-dd");
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            this.ClearData();
        }

        private void ClearData()
        {
            this.pnlConfirm.Visible = false;
            this.ViewState.Remove("Notices");
            this.grdNotices.DataSource = null;
            this.grdNotices.DataBind();
            this.txtTicketNo.Text = string.Empty;
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            List<ReversibleNotice> notices = (List<ReversibleNotice>)this.ViewState["Notices"];
            ReversibleNotice notice = notices[this.grdNotices.SelectedIndex];

            SuspenseAccount db = new SuspenseAccount(this.connectionString);
            int rctIntNo = db.ReverseAssignment(notice,login);
            if (rctIntNo == -1)
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            else
            {
                Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", rctIntNo.ToString());
                this.ClearData();
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.UndoSuspenseAccountAssignment, PunchAction.Change);  

            }
        }

    }
}
