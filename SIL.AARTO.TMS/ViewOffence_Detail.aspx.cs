using System;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that displays the details of an offence
    /// </summary>
    public partial class ViewOffence_Detail : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        //private int notIntNo = 0;
        private int frameIntNo = 0;
        private int autIntNo = 0;
        private string filmNo = "";

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "ViewOffence_Detail.aspx";
        //protected string thisPage = "View offence details";
        protected NoticeViewerList noticeList;

        private const string MONEY_FORMAT = "R #,##0.00";

        protected string crossHairSettings = "N";
        protected int crossHairStyle = 0;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            //int 
            this.login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            //HtmlMeta keywords = new HtmlMeta();
            //keywords.Attributes.Add("name", "Keywords");
            //keywords.Attributes.Add("content", keywords);
            //Page.Header.Controls.Add(keywords);

            //HtmlMeta description = new HtmlMeta();
            //description.Attributes.Add("name", "Description");
            //description.Attributes.Add("content", description);
            //Page.Header.Controls.Add(description);

            if (Session["NoticeList"] == null)
                this.SetNoticeViewerList();
            else
                this.noticeList = (NoticeViewerList)Session["NoticeList"];

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                if (Session["notIntNo"] != null)
                {
                    this.noticeList.SetCurrent(Convert.ToInt32(Session["notIntNo"]));
                    //mrs 20080814 notIntNo not changing during revious and next operations
                    ViewState.Add("notIntNo", Convert.ToInt32(Session["notIntNo"]));
                    Session["notIntNo"] = null;
                }
                this.GetNextNotice();
            }
        }

        private void SetNoticeViewerList()
        {
            if (Session["NoticeQueryCriteria"] == null)
                return;
            NoticeQueryCriteria criteria = (NoticeQueryCriteria) Session["NoticeQueryCriteria"];

            NoticeDB notice = new NoticeDB(this.connectionString);
            //SqlDataReader reader = notice.GetNoticeListByCriteria(criteria);
            SqlDataReader reader = notice.GetNoticeListByCriteria_NotIntNoOnly(criteria);
            this.noticeList = new NoticeViewerList(reader);
            reader.Dispose();

            this.Session["NoticeList"] = this.noticeList;
        }

        private void GetNextNotice()
        {
            //mrs 20080814 notIntNo not changing during revious and next operations
            //this.notIntNo = this.noticeList.Current;
            int notIntNo = this.noticeList.Current;
            ViewState.Add("notIntNo", notIntNo);

            int autIntNo = ((NoticeInfo)this.noticeList[this.noticeList.CurrentIndex-1]).AutIntNo;

            if (notIntNo > 0)
            {
                //mrs 20080816 pass notintno to print function
                //hlPrintVersion.NavigateUrl = string.Format("ViewOffence_Print.aspx?notice={0}&autintno={1}", notIntNo.ToString(), autIntNo.ToString());
                this.ShowFrameDetails();
                this.lblNavigation.Text = string.Format((string)GetLocalResourceObject("lblNavigation.Text"), this.noticeList.CurrentIndex, this.noticeList.Count);
            }
        }

        private string GetCrossHairSettings(int autIntNo)
        {
            //dls 100202 - move rules for showing cross-hairs out of the stored proc into the code
            AuthorityRulesDetails ar = new AuthorityRulesDetails();

            ar.AutIntNo = autIntNo;
            ar.ARCode = "3100";
            ar.LastUser = this.login;

            DefaultAuthRules crossHairs = new DefaultAuthRules(ar, this.connectionString);

            KeyValuePair<int, string> crossHairRule = crossHairs.SetDefaultAuthRule();

            return crossHairRule.Value;
        }

        private int GetCrossHairStyle(int autIntNo)
        {
            //dls 100202 - move rules for showing cross-hairs out of the stored proc into the code
            AuthorityRulesDetails ar = new AuthorityRulesDetails();

            ar.AutIntNo = autIntNo;
            ar.ARCode = "3150";
            ar.LastUser = this.login;

            DefaultAuthRules crossHairs = new DefaultAuthRules(ar, this.connectionString);

            KeyValuePair<int, string> crossHairRule = crossHairs.SetDefaultAuthRule();

            return crossHairRule.Key;
        }

        private void ShowFrameDetails()
        {
            NoticeDB notice = new NoticeDB(this.connectionString);

            //mrs 20080814 notIntNo not changing during revious and next operations
            int notIntNo = Convert.ToInt32(ViewState["notIntNo"]);

            int autIntNo = ((NoticeInfo)this.noticeList[this.noticeList.CurrentIndex-1]).AutIntNo;

			this.hlPrintVersion.NavigateUrl = string.Format("ViewOffence_Print.aspx?notice={0}&autintno={1}", notIntNo.ToString(), autIntNo);
			
            this.crossHairSettings = GetCrossHairSettings(autIntNo);
            this.crossHairStyle = this.GetCrossHairStyle(autIntNo);

            this.frameIntNo = notice.GetFrameForNotice(notIntNo);

            if (this.frameIntNo != 0)
            {
                int filmIntNo = 0;

                SqlDataReader reader = notice.GetNoticeViewDetails(notIntNo, this.crossHairSettings, this.crossHairStyle);
                while (reader.Read())
                {
                    //dls 070120 - add rules for showing cross-hairs
                    Session["ShowCrossHairs"] = reader["ShowCrossHairs"].ToString().Equals("Y") ? true : false;
                    Session["CrossHairStyle"] = Convert.ToInt16(reader["CrossHairStyle"]);

                    filmIntNo = (int)reader["FilmIntNo"];
                    filmNo = reader["NotFilmNo"].ToString();
                    lblFrameNo.Text = filmNo + " ~ " + reader["NotFrameNo"].ToString();
                    lblRegNo.Text = reader["NotRegNo"].ToString();
                    lblSpeed.Text = "1st Speed " + reader["NotSpeed1"].ToString();
                    lblSpeed2.Text = "2nd Speed " + reader["NotSpeed2"].ToString();
                    //txtVehMake.Text = reader["NotVehicleMake"].ToString();
                    //txtVehType.Text = reader["NotVehicleType"].ToString();
                    lblTicketNo.Text = reader["NotTicketNo"].ToString();

                    DateTime offenceDate = Convert.ToDateTime(reader["NotOffenceDate"]);
                    //txtOffenceDate.Text = offenceDate.Year + "/" + offenceDate.Month.ToString().PadLeft(2, '0') + "/" + offenceDate.Day.ToString().PadLeft(2, '0');
                    //txtOffenceTime.Text = offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    lblOffenceDate.Text = offenceDate.Year + "/" + offenceDate.Month.ToString().PadLeft(2, '0') + "/" + offenceDate.Day.ToString().PadLeft(2, '0')
                        + " " + offenceDate.Hour.ToString().PadLeft(2, '0') + ":" + offenceDate.Minute.ToString().PadLeft(2, '0');
                    lblOffence.Text = reader["ChgOffenceDescr"].ToString();

                    lblLocDescr.Text = reader["NotLocDescr"].ToString();

                    LoadOwnerDetails(notIntNo);
                    LoadDriverDetails(notIntNo);

                    if (reader["NotProxyFlag"].ToString().Equals("Y"))
                    {
                        LoadProxyDetails(notIntNo);
                        lblProxy.Visible = true;
                        lblProxyID.Visible = true;
                        lblProxyName.Visible = true;
                        lblProxyName.Visible = true;
                        lblProxyID.Visible = true;
                    }
                    else
                    {
                        lblProxy.Visible = false;
                        lblProxyID.Visible = false;
                        lblProxyName.Visible = false;
                        lblProxyName.Visible = false;
                        lblProxyID.Visible = false;
                    }

                    tblOwnerDetails.Visible = true;

                    this.lblFineAmount.Text = Convert.ToDecimal(reader["ChgRevFineAmount"]).ToString(MONEY_FORMAT);
                }

                reader.Close();

                // Image Viewer Control
                // Image Viewer Control
                this.imageViewer1.FilmNo = filmNo;
                this.imageViewer1.FrameIntNo = frameIntNo;
                this.imageViewer1.ScanImageIntNo = 0;
                this.imageViewer1.ImageType = "A";
                this.imageViewer1.FilmIntNo = filmIntNo;
                this.imageViewer1.Phase = this.Session["cvPhase"] == null ? 1 : Convert.ToInt32(this.Session["cvPhase"]);
                this.imageViewer1.Initialise();

                string strURLs = imageViewer1.GetParameters();
                ScriptManager.RegisterStartupScript(udpFrame,
                        udpFrame.GetType(), "UpdatePanelChangeImageViewer", "LoadImage(" + strURLs + ");", true);
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
        }

        private void LoadProxyDetails(int notIntNo)
        {

            ProxyDB proxy = new ProxyDB(connectionString);

            ProxyDetails proxyDet = proxy.GetProxyDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            //string postAddr = string.Empty;
            //string strAddr = string.Empty;

            try
            {
                name = proxyDet.PrxSurname.Trim();
            }
            catch { }

            try
            {
                initials = proxyDet.PrxInitials.Trim();
            }
            catch { }

            try
            {
                id = proxyDet.PrxIDNumber.Trim();
            }
            catch { }

            //try
            //{
            //    postAddr = proxyDet.PrxPOAdd1.Trim();
            //    if (!proxyDet.PrxPOAdd1.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd2.Trim();
            //    if (!proxyDet.PrxPOAdd2.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd3.Trim();
            //    if (!proxyDet.PrxPOAdd3.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd4.Trim();
            //    if (!proxyDet.PrxPOAdd4.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOAdd5.Trim();
            //    if (!proxyDet.PrxPOAdd5.Trim().Equals("")) postAddr += ", ";

            //    postAddr += proxyDet.PrxPOCode.Trim();
            //}
            //catch { }

            //try
            //{
            //    strAddr = proxyDet.PrxStAdd1.Trim();
            //    if (!proxyDet.PrxStAdd1.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd2.Trim();
            //    if (!proxyDet.PrxStAdd2.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd3.Trim();
            //    if (!proxyDet.PrxStAdd3.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd4.Trim();
            //    if (!proxyDet.PrxStAdd4.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxPOAdd5.Trim();
            //    if (!proxyDet.PrxStAdd5.Trim().Equals("")) strAddr += ", ";

            //    strAddr += proxyDet.PrxStCode.Trim();
            //}
            //catch { }

            lblPrxName.Text = proxyDet.PrxFullName;

            lblPrxID.Text = id;

            //lblProxyPostAddr.Text = postAddr;
            //lblProxyStrAddr.Text = strAddr;
        }

        private void LoadDriverDetails(int notIntNo)
        {

            DriverDB driver = new DriverDB(connectionString);

            DriverDetails driverDet = driver.GetDriverDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            string postAddr = string.Empty;
            string strAddr = string.Empty;

            try
            {
                name = driverDet.DrvSurname.Trim();
            }
            catch { }

            try
            {
                initials = driverDet.DrvInitials.Trim();
            }
            catch { }

            try
            {
                id = driverDet.DrvIDNumber.Trim();
            }
            catch { }

            try
            {
                postAddr = driverDet.DrvPOAdd1.Trim();
                if (!driverDet.DrvPOAdd1.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd2.Trim();
                if (!driverDet.DrvPOAdd2.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd3.Trim();
                if (!driverDet.DrvPOAdd3.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd4.Trim();
                if (!driverDet.DrvPOAdd4.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOAdd5.Trim();
                if (!driverDet.DrvPOAdd5.Trim().Equals(""))
                    postAddr += ", ";

                postAddr += driverDet.DrvPOCode.Trim();
            }
            catch { }

            try
            {
                strAddr = driverDet.DrvStAdd1.Trim();
                if (!driverDet.DrvStAdd1.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd2.Trim();
                if (!driverDet.DrvStAdd2.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd3.Trim();
                if (!driverDet.DrvStAdd3.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStAdd4.Trim();
                if (!driverDet.DrvStAdd4.Trim().Equals(""))
                    strAddr += ", ";

                strAddr += driverDet.DrvStCode.Trim();
            }
            catch { }

            // LMZ Changed 12-07-2007
            lblDriverName.Text = driverDet.DrvFullName;

            lblDriverID.Text = id;

            lblDriverPostAddr.Text = postAddr;
            lblDriverStrAddr.Text = strAddr;
        }

        private void LoadOwnerDetails(int notIntNo)
        {

            OwnerDB owner = new OwnerDB(connectionString);

            OwnerDetails ownerDet = owner.GetOwnerDetailsByNotice(notIntNo);

            string name = string.Empty;
            string initials = string.Empty;
            string id = string.Empty;
            string postAddr = string.Empty;
            string strAddr = string.Empty;

            try
            {
                name = ownerDet.OwnSurname.Trim();
            }
            catch { }

            try
            {
                initials = ownerDet.OwnInitials.Trim();
            }
            catch { }

            try
            {
                id = ownerDet.OwnIDNumber.Trim();
            }
            catch { }

            try
            {
                postAddr = ownerDet.OwnPOAdd1.Trim();
                if (!ownerDet.OwnPOAdd1.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd2.Trim();
                if (!ownerDet.OwnPOAdd2.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd3.Trim();
                if (!ownerDet.OwnPOAdd3.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd4.Trim();
                if (!ownerDet.OwnPOAdd4.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOAdd5.Trim();
                if (!ownerDet.OwnPOAdd5.Trim().Equals("")) postAddr += ", ";

                postAddr += ownerDet.OwnPOCode.Trim();
            }
            catch { }

            try
            {
                strAddr = ownerDet.OwnStAdd1.Trim();
                if (!ownerDet.OwnStAdd1.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd2.Trim();
                if (!ownerDet.OwnStAdd2.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd3.Trim();
                if (!ownerDet.OwnStAdd3.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStAdd4.Trim();
                if (!ownerDet.OwnStAdd4.Trim().Equals("")) strAddr += ", ";

                strAddr += ownerDet.OwnStCode.Trim();
            }
            catch { }

            lblOwnerName.Text = ownerDet.OwnFullName;

            lblOwnerID.Text = id;

            lblOwnerPostAddr.Text = postAddr;
            lblOwnerStrAddr.Text = strAddr;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewOffence_Search.aspx");
        }

        //protected void btnPrint_Click(object sender, EventArgs e)
        //{
        //    //mrs 20080814 notIntNo not changing during revious and next operations
        //    int notIntNo = Convert.ToInt32(ViewState["notIntNo"]);

        //    Helper_Web.BuildPopup(this, "ViewOffence_Print.aspx", null);
        //}

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            this.noticeList.GetPreviousNotice();
            this.GetNextNotice();
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            this.noticeList.GetNextNotice();
            this.GetNextNotice();
        }
    }
}
