using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
	/// <summary>
	/// Represents a list with associated editor for Summons Server companies.
	/// </summary>
	public partial class SummonsServerLinkToArea : System.Web.UI.Page
	{
		// Fields
		private string connectionString = string.Empty;
		private string loginUser;
		private string thisPageURL = "SummonsServerLinkToArea.aspx";
		private int autIntNo = 0;
        private Regex regexManual = new Regex("[Y|Ny|n]", RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.Singleline);
        private Regex regexPriority = new Regex("[0|1]", RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.Singleline);
		protected string description = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string keywords = string.Empty;
		protected string title = string.Empty;
        //protected string thisPage = "Summons Server link to Area";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected override void OnLoad(System.EventArgs e)
		{
			this.connectionString = Application["constr"].ToString();

			// Get user info from session variable
			if (Session["userDetails"] == null)
				Server.Transfer("Login.aspx?Login=invalid");
			if (Session["userIntNo"] == null)
				Server.Transfer("Login.aspx?Login=invalid");

			// Get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();
			userDetails = (UserDetails)Session["userDetails"];
			//int 
			autIntNo = Convert.ToInt32(Session["autIntNo"]);
			Session["userLoginName"] = userDetails.UserLoginName.ToString();
			int userAccessLevel = userDetails.UserAccessLevel;
			loginUser = userDetails.UserLoginName;

			//set domain specific variables
			General gen = new General();
			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
			title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                //this.pnlAddArea.Visible = false;
                this.pnlUpdateAuthSS.Visible = false;
				this.pnlAuthority.Visible = true;
                this.ddlSummonsServer.Visible = false;
				this.btnOptHide.Visible = true;
                this.pnlAddArea.Visible = false;
                //populate authority ddl to select, default to current selected autintno
                PopulateUserGroupAuthorityList();
			}
		}

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateUserGroupAuthorityList()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB uga = new UserGroup_AuthDB(connectionString);
            //ddlAuthority.DataSource = data;// uga.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlAuthority.DataValueField = "AutIntNo";
            //ddlAuthority.DataTextField = "AutDescr";
            //ddlAuthority.DataBind();
            ddlAuthority.Items.Insert(0, (string)GetLocalResourceObject("ddlAuthority.Items"));

            foreach (ListItem item in this.ddlAuthority.Items)
            {
                if (item.Value.Equals(this.autIntNo.ToString()))
                {
                    item.Selected = true;
                    this.ddlSummonsServer.Visible = true;
                    PopulateSummonsServerList();
                    this.ViewState.Add("SelectedAuthIntNo", this.autIntNo);
                    break;
                }
            }
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pnlAreaGroup.Visible = false;

            // If its the select instruction clear the grids
            if (this.ddlAuthority.SelectedIndex == 0)
            {
                return;
            }
            this.ViewState.Add("SelectedAuthIntNo", int.Parse(this.ddlAuthority.SelectedValue));
            this.pnlAddArea.Visible = false;
            //populate the summons server ddl - this is done sequentially to force the user to select a summons server as well
            PopulateSummonsServerList();
        }

        private void PopulateSummonsServerList()
        {
            SummonsServerDB sum = new SummonsServerDB(connectionString);
            ddlSummonsServer.DataSource = sum.GetSummonsServerList();
            ddlSummonsServer.DataValueField = "SSIntNo";
            ddlSummonsServer.DataTextField = "SSFullName";
            ddlSummonsServer.DataBind();
            ddlSummonsServer.Items.Insert(0, (string)GetLocalResourceObject("ddlSummonsServer.Items"));
            this.ddlSummonsServer.Visible = true;
        }

        protected void ddlSummonsServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.pnlAreaGroup.Visible = false;

            // If its the select instruction clear the grids
            if (this.ddlSummonsServer.SelectedIndex == 0)
            {
                return;
            }
            this.ViewState.Add("SelectedSSIntNo", int.Parse(this.ddlSummonsServer.SelectedValue));
            this.pnlDisplayAuthSS.Visible = false;
            this.pnlAddArea.Visible = false;
            this.btnAddSSForAuthority.Visible = true;
            this.btnAddAllAreas.Visible = true;
            //bind the auth_ss grid
            BindGrid(true);
        }

        protected void btnAddAllAreas_Click(object sender, EventArgs e)
        {
            //add all the missing areas to the defined auth and ss
            int autIntNo = Convert.ToInt32(this.ViewState["SelectedAuthIntNo"]);
            int ssIntNo = Convert.ToInt32(this.ViewState["SelectedSSIntNo"]);

            SummonsServerDB ass = new Stalberg.TMS.SummonsServerDB(connectionString);
            int addASsIntNo = ass.AddAllAuth_SummonsServer(autIntNo, ssIntNo, loginUser);

            if (addASsIntNo <= 0)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerToAreaLink, PunchAction.Add);  

            }

            BindGrid(true);
        }

        protected void btnAddSSForAuthority_Click(object sender, EventArgs e)
        {
            //add a new summons server to the authority with a default area code of 0000
            //use the selected ss in the drop down list
            int autIntNo = Convert.ToInt32(this.ViewState["SelectedAuthIntNo"]);
            int ssIntNo = Convert.ToInt32(this.ViewState["SelectedSSIntNo"]);
            string areaCode = "0000";
            SummonsServerDB ass = new Stalberg.TMS.SummonsServerDB(connectionString);
            int addASsIntNo = ass.AddAuth_SummonsServer(autIntNo, ssIntNo, areaCode, loginUser);
            if (addASsIntNo <= 0)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text") + " " + areaCode;
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1") + " " + areaCode;
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerToAreaLink, PunchAction.Add);  
            }

            BindGrid(true);
        }

		private void BindGrid(bool useEditGrid)
		{
            int autIntNo = Convert.ToInt32(this.ViewState["SelectedAuthIntNo"]);
            int ssIntNo = Convert.ToInt32(this.ViewState["SelectedSSIntNo"]);

            DataSet data = (new Stalberg.TMS.SummonsServerDB(connectionString)).GetSummonsServerListForAuthAndSSDS(autIntNo, ssIntNo);
			// Output to the grid or the authorities list
            if (useEditGrid)
            {
                grdLinkedSummonsServers.DataSource = data;
                grdLinkedSummonsServers.DataKeyField = "ASIntNo";// 2012-02-20 change it from SSIntNo to ASIntNo

                //dls 070410 - if we're not on the first page, and we do a search, an error results
                try
                {
                    grdLinkedSummonsServers.DataBind();
                }
                catch
                {
                    grdLinkedSummonsServers.CurrentPageIndex = 0;
                    grdLinkedSummonsServers.DataBind();
                }
                //grdLinkedSummonsServers.DataSource = data;
                //grdLinkedSummonsServers.DataKeyField = "ASIntNo";
                grdLinkedSummonsServers.DataBind();
                if (grdLinkedSummonsServers.Items.Count == 0)
                {
                    grdLinkedSummonsServers.Visible = false;
                    lblError.Visible = true;
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                }
                else
                {
                    grdLinkedSummonsServers.Visible = true;
                    this.pnlDisplayAuthSS.Visible = true;
                    //this.pnlEditAuthSS.Visible = false;
                    this.pnlUpdateAuthSS.Visible = false;
                }
                data.Dispose();
            }
            else
            {
                //this.grdSummonsServers2Link.DataSource = data;
                //this.grdSummonsServers2Link.DataKeyField = "SSIntNo";
                //this.grdSummonsServers2Link.DataBind();
            }
            data.Dispose();
        }

        protected void  grdLinkedSummonsServers_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            this.Session.Add("prevPage", thisPageURL);

            if (e.Item.ItemIndex >= 0)
            {
                grdLinkedSummonsServers.SelectedIndex = e.Item.ItemIndex;
                int assIntNo = Convert.ToInt32(grdLinkedSummonsServers.DataKeys[grdLinkedSummonsServers.SelectedIndex]);
                this.ViewState.Add("ASSIntNo", assIntNo);
                switch (((LinkButton)e.CommandSource).Text)
                {
                    case "Edit":
                        this.ShowAuthSSDetails(assIntNo);
                        break;

                    case "Delete":
                        this.DeleteAuthSS(assIntNo);
                        break;
                }
            }
        }

		private void ShowAuthSSDetails(int assIntNo)
		{
			this.ViewState.Add("ASSIntNo", assIntNo);

			Stalberg.TMS.SummonsServerDB ass = new Stalberg.TMS.SummonsServerDB(connectionString);
			Stalberg.TMS.AuthSSDetails asDetails = ass.GetASSDetails(assIntNo);
            txtManual.Text = asDetails.ASSAllocateManual;
            txtPriority.Text = asDetails.ASSPriority;
            txtPercentage.Text = asDetails.ASSPercentage.ToString();
            //pnlEditAuthSS.Visible = true;
            this.pnlUpdateAuthSS.Visible = true;
		}

        protected void btnUpdateASS_Click(object sender, EventArgs e)
        {
            this.lblError.Visible = true;

            int assIntNo = Convert.ToInt32(this.ViewState["ASSIntNo"]);
            if (assIntNo <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }
            bool isValid = true;
            StringBuilder sb = new StringBuilder((string)GetLocalResourceObject("lblError.Text4")+"\n<ul>");

            //check the manual input
            string asManual = this.txtManual.Text.Trim();
            if (asManual.Length == 0 || asManual.Length > 1)
            {
                isValid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text5")+"</li>\n");
            }
            else if (!this.regexManual.IsMatch(asManual))
            {
                isValid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text6")+"</li>\n");
            } 

            //check the priority
            string asPriority = this.txtPriority.Text.Trim();
            if (asPriority.Length == 0 || asPriority.Length >1)
            {
                isValid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text7")+"</li>\n");
            }
            else if (!this.regexPriority.IsMatch(asPriority))
            {
                isValid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text8")+"</li>\n");
            }

            // Oscar 2013-05-03 added
            int topAsIntNo;
            string errorMsg;
            if ((topAsIntNo = GetAuthSSTopPriority(assIntNo, out errorMsg)) > 0
                && topAsIntNo != assIntNo
                && asPriority == "1")
            {
                isValid = false;
                sb.AppendFormat("<li>{0}</li>\n", GetLocalResourceObject("CannotAllocateMoreThanOnePriority"));
            }
            else if (!string.IsNullOrWhiteSpace(errorMsg))
            {
                isValid = false;
                sb.AppendFormat("<li>{0}</li>\n", string.Format((string)GetLocalResourceObject("AnErrorInCheckingPriority"), errorMsg));
            }

            //check the percentage
            int asPercentage;
            if (!int.TryParse(txtPercentage.Text.Trim(), out asPercentage))
            {
                isValid = false;
                sb.Append("<li>"+(string)GetLocalResourceObject("lblError.Text9")+"</li>\n");
            }
            else
            {
                if (asPercentage > 100 || asPercentage < 0)
                {
                    isValid = false;
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text10") + "</li>\n");
                }
            }

            // Oscar 2013-05-03 added
            if (asPercentage > 0
                && (topAsIntNo > 0 || asPriority == "1"))
            {
                isValid = false;
                sb.AppendFormat("<li>{0}</li>\n", GetLocalResourceObject("CannnotAllocatePercentageWithPriority"));
            }

            //finally check if we need to thow our toys at the user
            if (!isValid)
            {
                sb.Append("</ul>\n");
                this.lblError.Text = sb.ToString();
                this.lblError.Visible = true;
                return;
            }

            Stalberg.TMS.SummonsServerDB asUpdate = new SummonsServerDB(connectionString);
            assIntNo = asUpdate.UpdateAuthSS(asManual, asPriority, asPercentage, loginUser, assIntNo);
            if (assIntNo <= 0)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerToAreaLink, PunchAction.Change); 
            }

            this.ViewState.Add("ASSIntNo", assIntNo);
            this.BindGrid(true);
        }

        private void DeleteAuthSS(int assIntNo)
        {
            Stalberg.TMS.SummonsServerDB ass = new SummonsServerDB(connectionString);
            int delIntNo = ass.DeleteAuth_SummonsServer(assIntNo);
            if (delIntNo <= 0)
                lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerToAreaLink, PunchAction.Delete); 
            }

            this.ViewState.Add("ASSIntNo", 0);
            this.BindGrid(true);
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // Add a new Summon s Server
        }

		protected void btnUpdateSS_Click(object sender, EventArgs e)
		{
 		}

        protected void grdLinkedSummonsServers_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.grdLinkedSummonsServers.CurrentPageIndex = e.NewPageIndex;
            this.BindGrid(true);
        }

		protected void dgSummonsServer_ItemCommand(object source, DataGridCommandEventArgs e)
		{
		}

		protected void dgSummonsServer_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			this.grdLinkedSummonsServers.CurrentPageIndex = e.NewPageIndex;
			this.BindGrid(true);
		}

		protected void btnOptAuthority_Click(object sender, System.EventArgs e)
		{
			//this.pnlSummonsServerDetails.Visible = false;
			//this.pnlSummonsServers.Visible = false;
            //this.btnOptHide.Text = "Show List";
            //this.pnlAuthority.Visible = true;
			//this.pnlAuthorityLinks.Visible = false;
			//this.pnlAuthorityNewArea.Visible = false;
			//this.pnlAuthorityNewSummonsServer.Visible = false;

			//int 
			this.PopulateUserGroupAuthorityList();
		}

		protected void btnAddAuthority_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
		}

        protected void btnAddAreaToAuthSS_Click(object sender, EventArgs e)
        {
            // add area(s) to the selected summons server and authority
            // populate area list based on text in txtAreaCode
            this.lstAreaCodes.Visible = false;
            this.pnlAddArea.Visible = true;
        }

        protected void btnSelectArea_Click(object sender, EventArgs e)
        {
            //user selected an areacode
            string search = txtSelectArea.Text.Trim();
            PopulateArea(search);
        }

        protected void PopulateArea(string search)
        {
            AreaCodeDB area = new AreaCodeDB(Application["constr"].ToString());

            lstAreaCodes.DataSource = area.GetAllAreaCodesDS(search);
            lstAreaCodes.DataValueField = "AreaCode";
            lstAreaCodes.DataTextField = "FullDescr";
            lstAreaCodes.DataBind();
            lstAreaCodes.Items.Insert(0, (string)GetLocalResourceObject("lstAreaCodes.Items"));
            this.lstAreaCodes.Visible = true;
      
        }

         protected void btnAddSelectedAC_Click(object sender, EventArgs e)
        {
            int autIntNo = Convert.ToInt32(this.ViewState["SelectedAuthIntNo"]);
            int ssIntNo = Convert.ToInt32(this.ViewState["SelectedSSIntNo"]);
            int addCount = 0; //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
             //process all the selected items in the listbox - ASP does not have the collection so we have to do it the long way round
            foreach (ListItem item in lstAreaCodes.Items)
            {
                if (item.Selected)
                {
                    string areaCode = item.Value.ToString();
                    //add it to the auth_ss table, check not already in the table
                    SummonsServerDB ass = new Stalberg.TMS.SummonsServerDB(connectionString);
                    int addASsIntNo = ass.AddAuth_SummonsServer(autIntNo, ssIntNo, areaCode, loginUser);
                    if (addASsIntNo <= 0)
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text15") + areaCode;
                    else
                    {
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text16");
                        addCount++;
                    }

                    lblError.Visible = true;
                }
            }
            if (addCount > 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerToAreaLink, PunchAction.Add);
            }
            //bind auth_SS grid
            BindGrid(true);
        }
		
        protected void btnOptHide_Click(object sender, System.EventArgs e)
		{
			if (grdLinkedSummonsServers.Visible.Equals(true))
			{
				// Hide it
				this.pnlSummonsServers.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptShow.Text");
			}
			else
			{
				// Show it
				this.pnlAuthority.Visible = false;
				this.pnlSummonsServers.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
			}
		}

	 

		protected void btnDeleteSummonsServerNo_Click(object sender, EventArgs e)
		{
			//this.pnlAuthority.Visible = false;
            //this.pnlSummonsServerDetails.Visible = false;
            //this.pnlDeleteSummonsServer.Visible = false;
		}

		protected void btnDeleteSummonsServerYes_Click(object sender, EventArgs e)
		{
		}

		private void GetLinkedSummonServers()
		{
			int authIntNo = int.Parse(this.ddlAuthority.SelectedValue);

			SummonsServerDB ssDB = new SummonsServerDB(Application["constr"].ToString());
			SqlDataReader reader = ssDB.GetAuth_SummonsServerListByAuth(authIntNo);

			this.grdLinkedSummonsServers.DataSource = reader;
			this.grdLinkedSummonsServers.DataKeyField = "SSIntNo";
			this.grdLinkedSummonsServers.DataBind();
		}

		protected void lnkLinkSummonsServer_Click(object sender, EventArgs e)
		{
		}

		private void PopulateAreaGrid()
		{
		}

		private void PopulateAreaFromDataSet()
		{
		}

		private void UnlinkSummonsServerFromAuthority(int asIntNo)
		{
            //SummonsServerDB ssDB = new SummonsServerDB(Application["constr"].ToString());
            //ssDB.UnlinkAuthoritySummonsServerArea(asIntNo);
            //this.GetLinkedSummonServers();
		}

		protected void grdArea_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
		{
            //this.grdArea.CurrentPageIndex = e.NewPageIndex;
            //this.PopulateAreaFromDataSet();
		}

		protected void grdLinkSummonsServer_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
		{
			this.grdLinkedSummonsServers.CurrentPageIndex = e.NewPageIndex;
			this.BindGrid(false);
		}

        // Oscar 2013-05-03 added
        int GetAuthSSTopPriority(int asIntNo, out string errorMsg)
        {
            errorMsg = null;
            var paras = new[]
            {
                new SqlParameter("@ASIntNo", asIntNo)
            };
            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand("AuthSS_GetTopPriority", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    })
                    {
                        cmd.Parameters.AddRange(paras);
                        conn.Open();

                        var obj = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();

                        int asIntNo2;
                        return int.TryParse(Convert.ToString(obj), out asIntNo2)
                            ? asIntNo2 : 0;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return -1;
            }
        }

        #region Add Area Group, Oscar 2013-05-13 added

	    int AuthSS_InsertByAreaGroup(int autIntNo, int ssIntNo, string groupName, string lastUser, char allocateManual = 'N', char priority = '1', byte percentage = 0)
	    {
	        var paras = new[]
	        {
	            new SqlParameter("@AutIntNo", autIntNo),
	            new SqlParameter("@SSIntNo", ssIntNo),
	            new SqlParameter("@AreaGroup", groupName),
	            new SqlParameter("@LastUser", lastUser),
	            new SqlParameter("@ASSAllocateManual", allocateManual),
	            new SqlParameter("@ASSPriority", priority),
	            new SqlParameter("@ASSPercentage", percentage),
	        };
	        using (var conn = new SqlConnection(connectionString))
	        {
	            using (var cmd = new SqlCommand("AuthSS_InsertByAreaGroup", conn) { CommandType = CommandType.StoredProcedure })
	            {
	                cmd.Parameters.AddRange(paras);
	                conn.Open();
	                int result;
	                return int.TryParse(Convert.ToString(cmd.ExecuteScalar()), out result) ? result : -1;
	            }
	        }
	    }

	    void BindDllAreaGroup()
	    {
	        this.ddlAreaGroup.Items.Clear();
	        this.ddlAreaGroup.Items.Add(new ListItem((string)GetLocalResourceObject("PleaseSelectAreaGroup"), ""));

	        var groupList = new AreaCodeGroupService().GetAll()
	            .OrderBy(g => g.GroupName).ThenBy(g => g.CodeStart).ThenBy(g => g.CodeEnd);

	        foreach (var group in groupList)
	        {
	            this.ddlAreaGroup.Items.Add(new ListItem(string.Format("{0} ({1} - {2})", group.GroupName, group.CodeStart, group.CodeEnd), group.GroupName));
	        }

	        this.ddlAreaGroup.SelectedIndex = 0;
	    }

	    void ProcessMessage(string message, bool allInOne = true)
	    {
	        this.lblError.Visible = !string.IsNullOrWhiteSpace(message);
	        if (allInOne && !string.IsNullOrWhiteSpace(message))
	            message = string.Format("{0}<ul>{1}</ul>", GetLocalResourceObject("lblError.Text4"), message);
	        this.lblError.Text = message;
	    }

	    protected void btnAddGroupAreaToSS_Click(object sender, EventArgs e)
	    {
	        this.pnlUpdateAuthSS.Visible = false;
	        this.pnlAddArea.Visible = false;
	        this.pnlAreaGroup.Visible = true;

            BindDllAreaGroup();
	    }

        protected void ddlAreaGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnAddAreaGroup.Enabled = this.ddlAreaGroup.SelectedIndex > 0;
        }

        protected void btnAddAreaGroup_Click(object sender, EventArgs e)
        {
            ProcessMessage(null);

            var sb = new StringBuilder();
            var isValid = true;
            int autIntNo, ssIntNo;
            int.TryParse(this.ddlAuthority.SelectedValue, out autIntNo);
            int.TryParse(this.ddlSummonsServer.SelectedValue, out ssIntNo);
            var groupName = this.ddlAreaGroup.SelectedValue;

            if (autIntNo <= 0)
            {
                isValid = false;
                sb.AppendFormat("<li>{0}</li>\n", GetLocalResourceObject("ddlAuthority.Items"));
            }

            if (ssIntNo <= 0)
            {
                isValid = false;
                sb.AppendFormat("<li>{0}</li>\n", GetLocalResourceObject("ddlSummonsServer.Items"));
            }

            if (string.IsNullOrWhiteSpace(groupName))
            {
                isValid = false;
                sb.AppendFormat("<li>{0}</li>\n", GetLocalResourceObject("PleaseSelectAreaGroup"));
            }

            if (!isValid)
            {
                ProcessMessage(sb.ToString());
                sb.Clear();
                return;
            }

            int success;
            try
            {
                success = AuthSS_InsertByAreaGroup(autIntNo, ssIntNo, groupName, this.loginUser);
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.SummonsServerToAreaLink, PunchAction.Add); 
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>\n", ex.Message);
                ProcessMessage(sb.ToString());
                sb.Clear();
                return;
            }

            if (success > 0)
            {
                BindGrid(true);
            }
        }

        #endregion

    }
}
