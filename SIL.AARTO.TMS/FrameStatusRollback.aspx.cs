﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stalberg.TMS.Data;
using System.Data.SqlClient;
using System.Data;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class FrameStatusRollback : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        // Protected
        protected string _styleSheet;
        protected string _background;
        protected string _keywords = String.Empty;
        protected string _title = "FrameStatusRollback";
        protected string _description = String.Empty;
        protected string _thisPage = "FrameStatusRollback.aspx";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            //UserDB user = new UserDB(this.connectionString);
            UserDetails userDetails = new UserDetails();

            //int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            

            if (ViewState["autIntNo"] == null)
            {
                this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
                ViewState["autIntNo"] = this.autIntNo;
            }
            else
                this.autIntNo = int.Parse(ViewState["autIntNo"].ToString());

            // Set domain specific variables
            General gen = new General();
            _background = gen.SetBackground(Session["drBackground"]);
            _styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            _title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.PopulateAuthorities();
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");
            
            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(this.autIntNo.ToString()));
            //reader.Close();
            this.CheckAuthorityRule();
        }

        private void CheckAuthorityRule()
        {
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = Convert.ToInt32(this.autIntNo);
            arDetails.ARCode = "0580";
            arDetails.LastUser = this.login;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            ar.SetDefaultAuthRule();
            if (arDetails.ARString == "Y")
            {
                buttonSearch.Enabled = true;
            }
            else
            {
                buttonSearch.Enabled = false;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
        }

        private void BindData()
        {
            FrameDB frameDB = new FrameDB(connectionString);

            int iFrameNo;
            if (!int.TryParse(textFrameNo.Text, out iFrameNo))
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            string strMsg = "";
            int minFrameStatus = 0;

            bool isNaTISLast = (bool)this.Session["NaTISLast"];
            if (isNaTISLast)
            {
                minFrameStatus = 700;
            }
            else
            {
                minFrameStatus = 500;
            }

            SqlDataReader reader = frameDB.GetFramesForRollbackToAdjudicate(iFrameNo, textFilmNo.Text, minFrameStatus, ref strMsg);

            if (reader == null)
            {
                lblError.Text = strMsg;
                return;
            }

            if (reader.HasRows)
            {
                gridFrame.DataSource = reader;
                gridFrame.DataKeyField = "FrameIntNo";
                gridFrame.DataBind();
                lblError.Text = "";
            }
            else
            {
                gridFrame.DataSource = null;
                gridFrame.DataBind();
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
            }
        }

        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void gridFrame_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Rollback"))
            {
                int iFrameIntNo = Convert.ToInt32(e.Item.Cells[0].Text);
                int iNotIntNo = 0;
                int.TryParse(e.Item.Cells[1].Text, out iNotIntNo);
                Int64 ROWVERSION = Convert.ToInt64(e.Item.Cells[2].Text);

                int iFrameStatus = 0;
                bool isNaTISLast = (bool)this.Session["NaTISLast"];
                if (isNaTISLast)
                {
                    iFrameStatus = 700;
                }
                else
                {
                    iFrameStatus = 500;
                }

                //Jerry 2014-12-10 add errMsg parameter
                string errMsg = string.Empty;
                FrameDB frameDB = new FrameDB(connectionString);
                int Success = frameDB.RollbackFramesToAdjudicate(iFrameIntNo, iNotIntNo, ROWVERSION, iFrameStatus, login, ref errMsg);

                //Jerry 2014-12-10 handle the error
                switch (Success)
                {
                    case 1:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                        e.Item.Cells[6].Enabled = false;
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.FrameStatusRollback, PunchAction.Add);  
                        break;
                    case 0:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text4"), errMsg);
                        break;
                    case -1:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text8"), iNotIntNo);
                        break;
                    case -2:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text9"), iNotIntNo);
                        break;
                    case -3:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text10"), iNotIntNo);
                        break;
                    case -4:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text11"), iNotIntNo);
                        break;
                    case -5:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text12"), iNotIntNo);
                        break;
                    case -6:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text13"), iNotIntNo);
                        break;
                    case -7:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text14"), iNotIntNo);
                        break;
                    case -8:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text15"), iNotIntNo);
                        break;
                    case -9:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text16"), iNotIntNo);
                        break;
                    case -10:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text17"), iNotIntNo);
                        break;
                    case -11:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text18"), iNotIntNo);
                        break;
                    case -12:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text19"), iNotIntNo);
                        break;
                    case -13:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text20"), iNotIntNo);
                        break;
                    case -14:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text21"), iNotIntNo);
                        break;
                    case -15:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text22"), iNotIntNo);
                        break;
                    case -16:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text23"), iNotIntNo);
                        break;
                    case -17:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text24"), iNotIntNo);
                        break;
                    case -18:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text25"), iNotIntNo);
                        break;
                    case -19:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text26"), iFrameIntNo);
                        break;
                    case -20:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text27"), iFrameIntNo);
                        break;
                    case -21:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text28"), iNotIntNo);
                        break;
                    case -22:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text29"), iNotIntNo);
                        break;
                    case -23:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text30"), iNotIntNo);
                        break;
                    case -24:
                        lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text31"), iNotIntNo);
                        break;
                }
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                //SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                //punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.FrameStatusRollback, PunchAction.Add);  


                //BindData();
            }
            
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.autIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            ViewState.Add("autIntNo", this.autIntNo);
            this.CheckAuthorityRule();
        }
}
}
