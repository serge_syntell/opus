using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;


namespace Stalberg.TMS
{
    /// <summary>
    /// The Manual Cash Receipt Notice Reprint page
    /// </summary>
    public partial class ManualCashReceiptNoticeReprint : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = string.Empty;
        //protected string thisPage = "Payment Confirmation Reprint";
        protected string description = String.Empty;
        protected string thisPageURL = "ManualCashReceiptNoticeReprint.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                // TODO: Page setup here
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
            }
        }

         

        protected void txtThird_TextChanged(object sender, EventArgs e)
        {

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            string temp = this.txtFirst.Text.Trim();
            if (temp.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            sb.Append(temp);
            sb.Append('/');
            temp = this.txtSecond.Text.Trim();
            if (temp.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            sb.Append(temp);
            sb.Append('/');
            temp = this.txtThird.Text.Trim();
            if (temp.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            sb.Append(temp);
            sb.Append("/%");

            NoticeDB db = new NoticeDB(this.connectionString);
            string ticketNo = db.GetNoticeIDFromCivitasTicketNumber(sb.ToString(), this.autIntNo);

            if (ticketNo.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            if (ticketNo.IndexOf('~') == 0)
            {
                this.lblError.Text = ticketNo.Substring(1, ticketNo.Length - 1);
                return;
            }

            this.lblError.Text = string.Empty;
            Session.Add("printAutIntNo", this.autIntNo);
            Session.Add("showAllNotices", "Y");

            //dls 090325 - the prefix of * is added in the actual viewer page
            //Helper_Web.BuildPopup(this, "FirstNoticeViewer.aspx", "printfile", "*" + ticketNo);
            Helper_Web.BuildPopup(this, "FirstNoticeViewer.aspx", "printfile", ticketNo);
        }

    }
}
