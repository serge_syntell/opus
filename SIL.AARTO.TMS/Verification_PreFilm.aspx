<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>


<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.Verification_PreFilm" Codebehind="Verification_PreFilm.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
  <%--      <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>--%>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    &nbsp;
                </td>
                <td valign="top" align="center" width="100%" colspan="1">
                    <table border="0" width="100%" height="482">
                        <tr>
                            <td valign="top" align="center" width="100%">
                                &nbsp;&nbsp;
                                <table border="0" align="center" width="100%">
                                    <tr>
                                        <td valign="top" width="100%">
                                            &nbsp;<table width="100%">
                                                <tr>
                                                    <td align="center" width="100%">
                                                        <asp:Label ID="lblAuthority" runat="server" CssClass="ContentHead" Width="100%"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:GridView ID="grdHeader" runat="server" AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" GridLines="Horizontal"  ShowFooter="True" >
                                                            <FooterStyle CssClass="CartListHead" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="FilmNo" HeaderText="<%$Resources:grdHeader.HeaderText %>" />
                                                                    <asp:BoundField DataField="ToVerify" HeaderText="<%$Resources:grdHeader.HeaderText1 %>" ><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                   <asp:BoundField DataField="NoFrames" HeaderText="<%$Resources:grdHeader.HeaderText2 %>" ><ItemStyle HorizontalAlign="Center" /></asp:BoundField> 
                                                                    <asp:TemplateField HeaderText="<%$Resources:grdHeader.HeaderText3 %>">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FirstOffence", "{0:yyyy-MM-dd HH:mm}") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FirstOffence", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="<%$Resources:grdHeader.HeaderText4 %>">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("LastOffence", "{0:yyyy-MM-dd HH:mm}") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("LastOffence", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                   <asp:BoundField DataField="SinceFirstOffence" HeaderText="<%$Resources:grdHeader.HeaderText5 %>" ><ItemStyle HorizontalAlign="Center" /></asp:BoundField> 
                                                                  <asp:BoundField DataField="FilmLockUser" HeaderText="<%$Resources:grdHeader.HeaderText6 %>" ><ItemStyle HorizontalAlign="Center" /></asp:BoundField>  
                                                                </Columns>
                                                                <HeaderStyle CssClass="CartListHead" />
                                                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                                                            </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="height: 26px">
                                                        <asp:Panel ID="pnlOfficerNo" runat="server" CssClass="Normal" Height="50px" Width="400px">
                                                            <table align="center" width="100%">
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblOfficerNo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOfficerNo.Text %>"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:TextBox Enabled="false" CssClass="Normal" ID="txtOfficerNo" MaxLength="10" runat="server" Width="113px"
                                                                            AutoCompleteType="Disabled"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Label ID="lblOfficerName" runat="server" CssClass="Normal" Width="310px"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        &nbsp;<asp:Button ID="buttonBookOut" runat="server" CssClass="NormalButton" OnClick="buttonBookOut_Click"
                                                            Text="<%$Resources:buttonBookOut.Text %>" />&nbsp;
                                                            <input id="btnClose" runat="server" class="NormalButtonRed" type="button" value="<%$Resources:btnClose.Text %>" onclick="window.close();" />&nbsp;&nbsp;
                                                            <asp:Button ID="btnTest" runat="server" CssClass="NormalButton" OnClick="buttonBookOut_Click" Text="<%$Resources:btnTest.Text %>" Visible="False" /></td>
                                                                
                                                </tr>
                                                <tr>
                                                    <td align="center" style="height: 21px">
                                                        <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label></td>
                                                </tr>
                                            </table>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
