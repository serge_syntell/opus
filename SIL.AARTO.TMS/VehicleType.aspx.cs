using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class VehicleType : Page
    {
        private string conString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected int autIntNo = 0;//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected string thisPageURL = "VehicleType.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Vehicle type maintenance";

        protected override void OnLoad(EventArgs e)
        {
            conString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            //UserDB user = new UserDB(conString);
            //UserDetails userDetails = new UserDetails();
            //userDetails = (UserDetails)Session["userDetails"];
            loginUser = ((UserDetails)Session["userDetails"]).UserLoginName;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //int 
            //int autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddVehicleType.Visible = false;
                pnlUpdateVehicleType.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptHide.Visible = true;

                PopulateVehicleTypeGroups(ddlSelVTGroup);
                ddlSelVTGroup.Items.Insert(0, (string)GetLocalResourceObject("ddlSelVTGroup.Items"));

                PopulateVehicleTypeGroups(ddlVTGroup);

                PopulateVehicleTypes();

                BindGrid();
            }
        }

        protected void PopulateVehicleTypes()
        {
            VehicleTypeDB vt = new VehicleTypeDB(conString);

            cblVTLookups.DataSource = vt.GetVehicleTypeList(0, "");
            cblVTLookups.DataValueField = "VTIntNo";
            cblVTLookups.DataTextField = "VTDescr";
            cblVTLookups.DataBind();

            ClearSelection();
        }

        protected void ClearSelection()
        {
            foreach (ListItem li in cblVTLookups.Items)
            {
                li.Selected = false;
            }
        }
        protected void PopulateVehicleTypeGroups(DropDownList ddlSelVTG)
        {
            VehicleTypeGroupDB vtg = new VehicleTypeGroupDB(conString);
            SqlDataReader reader=vtg.GetVehicleTypeGroupList("");
            
            Dictionary<int, string> lookups =
                VehicleTypeGroupLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int vTGIntNo = (int)reader["VTGIntNo"];
                if (lookups.ContainsKey(vTGIntNo))
                {
                    ddlSelVTG.Items.Add(new ListItem(lookups[vTGIntNo], vTGIntNo.ToString()));
                }
            }
            //ddlSelVTG.DataSource = reader;
            //ddlSelVTG.DataValueField = "VTGIntNo";
            //ddlSelVTG.DataTextField = "VTGDescr";
            //ddlSelVTG.DataBind();
        }

        protected void BindGrid()
        {
            int vtgIntNo = 0;

            // Obtain and bind a list of all users
            if (ddlSelVTGroup.SelectedIndex > 0) vtgIntNo = Convert.ToInt32(ddlSelVTGroup.SelectedValue);

            int totalCount = 0;
            VehicleTypeDB vtList = new VehicleTypeDB(conString);
            DataSet data = vtList.GetVehicleTypeListDS(vtgIntNo, txtSearch.Text, out totalCount, dgVehicleTypePager.PageSize, dgVehicleTypePager.CurrentPageIndex);
            dgVehicleType.DataSource = data;
            dgVehicleType.DataKeyField = "VTIntNo";
            dgVehicleTypePager.RecordCount = totalCount;

            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgVehicleType.DataBind();
            }
            catch
            {
                dgVehicleType.CurrentPageIndex = 0;
                dgVehicleType.DataBind();
            }


            if (dgVehicleType.Items.Count == 0)
            {
                dgVehicleType.Visible = false;
                lblError.Visible = true;
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgVehicleType.Visible = true;
            }

            data.Dispose();

            pnlAddVehicleType.Visible = false;
            pnlUpdateVehicleType.Visible = false;

        }


        protected void btnOptAdd_Click(object sender, EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlAddVehicleType.Visible = true;
            pnlUpdateVehicleType.Visible = false;

            btnOptDelete.Visible = false;

            PopulateVehicleTypeGroups(ddlAddVTGroup);
            //2012-3-6 linda modified into a multi-language
            ddlAddVTGroup.Items.Insert(0, (string)GetLocalResourceObject("ddlAddVTGroup.Items"));

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
        }


        protected void btnAddVehicleType_Click(object sender, EventArgs e)
        {
            if (ddlAddVTGroup.SelectedIndex < 1)
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-24
                //Display multi - language error message is the value from the resource
                lblError.Text = (String)GetLocalResourceObject("lblError.Text1");
                return;
            }

            string vtArchiveExclude = chkAddVTArchiveExclude.Checked ? "Y" : "N";

            int vtgIntNo = Convert.ToInt32(ddlAddVTGroup.SelectedValue);

            // add the new transaction to the database table
            VehicleTypeDB toAdd = new VehicleTypeDB(conString);

            int addVTIntNo = toAdd.AddVehicleType(vtgIntNo,
                txtAddVTCode.Text,
                txtAddVTDescr.Text,
                txtAddNatis.Text,
                txtAddTCS.Text,
                txtAddCivitas.Text,
                loginUser,
                vtArchiveExclude,
                this.textAddNatisDescr.Text);


            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addVTIntNo, lgEntityList, "VehicleTypeLookup", "VTIntNo", "VtDescr", this.loginUser);


            //Modefied By Linda 2012-2-24
            //Display multi - language error message is the value from the resource
            lblError.Text = addVTIntNo <= 0 ? (String)GetLocalResourceObject("lblError.Text2") : (String)GetLocalResourceObject("lblError.Text3");

            lblError.Visible = true;
            //Adam 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            //dgVehicleTypePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            //dgVehicleTypePager.CurrentPageIndex = 1;
            BindGrid();
            if (addVTIntNo> 0)
            {
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.conString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleTypesMaintenance, PunchAction.Add);  

            }
           
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int vtgIntNo = Convert.ToInt32(ddlVTGroup.SelectedValue);
            int vtIntNo = Convert.ToInt32(Session["editVTIntNo"]);

            string vtArchiveExclude = chkVTArchiveExclude.Checked ? "Y" : "N";

            // add the new transaction to the database table
            VehicleTypeDB vtUpdate = new VehicleTypeDB(conString);

            int updVTIntNo = vtUpdate.UpdateVehicleType(vtIntNo,
                vtgIntNo,
                txtVTCode.Text,
                txtVTDescr.Text,
                txtNatis.Text,
                txtTCS.Text,
                txtCivitas.Text,
                loginUser,
                vtArchiveExclude,
                this.textUpdNatisDescr.Text);

                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(vtIntNo, lgEntityList, "VehicleTypeLookup", "VtIntNo", "VtDescr", this.loginUser);
           

            //Modefied By Linda 2012-2-24
            //Display multi - language error message is the value from the resource
            lblError.Text = updVTIntNo <= 0 ? (String)GetLocalResourceObject("lblError.Text4") : (String)GetLocalResourceObject("lblError.Text5");

            lblError.Visible = true;

            //need to update vehicle type lookups
            ArrayList vtl = new ArrayList();

            foreach (ListItem li in cblVTLookups.Items)
            {
                if (li.Selected)
                    vtl.Add(li.Value);
            }

            vtUpdate.AddVehicleTypeLookups(vtIntNo, vtl, this.loginUser);
            //Adam 2013-09-12 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!

            //dgVehicleTypePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            //dgVehicleTypePager.CurrentPageIndex = 1;
            BindGrid();

            if (updVTIntNo > 0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.conString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleTypesMaintenance, PunchAction.Change); 
            }
        }

        
               

        protected void dgVehicleType_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgVehicleType.SelectedIndex = e.Item.ItemIndex;

            if (dgVehicleType.SelectedIndex > -1)
            {
                string editVTIntNo = Convert.ToString(dgVehicleType.DataKeys[dgVehicleType.SelectedIndex]);

                Session["editVTIntNo"] = editVTIntNo;
                Session["prevPage"] = thisPageURL;

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editVTIntNo, "VehicleTypeLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                ShowVehicleTypeDetails(int.Parse(editVTIntNo.ToString()));
            }
        }
        

        protected void ShowVehicleTypeDetails(int editVTIntNo)
        {
            // Obtain and bind a list of all users
            VehicleTypeDB vt = new VehicleTypeDB(conString);

            Stalberg.TMS.VehicleTypeDetails vtDetails = vt.GetVehicleTypeDetails(editVTIntNo);

            txtVTCode.Text = vtDetails.VTCode;
            txtVTDescr.Text = vtDetails.VTDescr;
            txtNatis.Text = vtDetails.Natis;
            txtTCS.Text = vtDetails.TCS;
            txtCivitas.Text = vtDetails.Civitas;
            this.textUpdNatisDescr.Text = vtDetails.VTNatisDescr;
            ddlVTGroup.SelectedIndex = ddlVTGroup.Items.IndexOf(ddlVTGroup.Items.FindByValue(vtDetails.VTGIntNo.ToString()));

            chkVTArchiveExclude.Checked = vtDetails.VTArchiveExclude.Equals("Y") ? true : false;

            pnlUpdateVehicleType.Visible = true;
            pnlAddVehicleType.Visible = false;

            btnOptDelete.Visible = true;

            ClearSelection();

            SetSelection(editVTIntNo);
        }

        private void SetSelection(int editVTIntNo)
        {
            // Obtain and bind a list of all users
            VehicleTypeDB vt = new VehicleTypeDB(conString);

            DataSet ds = vt.GetVehicleTypeLookupListDS(editVTIntNo);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                foreach (ListItem li in cblVTLookups.Items)
                {
                    if (li.Value.Equals(dr["VTIntNo"].ToString()))
                        li.Selected = true;
                }
            }
        }

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int vtIntNo = Convert.ToInt32(Session["editVTIntNo"]);

            VehicleTypeDB vt = new VehicleTypeDB(conString);

            string delVTIntNo = vt.DeleteVehicleType(vtIntNo);

            if (!delVTIntNo.Equals("0"))
            {
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.DeleteLookup(lgEntityList, "VehicleTypeLookup", "VtIntNo");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.conString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.VehicleTypesMaintenance, PunchAction.Delete); 
            }

            //Modefied By Linda 2012-2-24
            //Display multi - language error message is the value from the resource
            lblError.Text = delVTIntNo.Equals("0") ? (String)GetLocalResourceObject("lblError.Text6") : (String)GetLocalResourceObject("lblError.Text7");

            //Adam 20130912 Make code changes here to make sure the page will navigate back correctly after one row is deleted.
            if (dgVehicleType.Items.Count == 1)
                this.dgVehicleTypePager.CurrentPageIndex--;

            lblError.Visible = true;
            

            
            dgVehicleTypePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            dgVehicleTypePager.CurrentPageIndex = 1;
            BindGrid();
        }

        
        protected void btnOptHide_Click(object sender, EventArgs e)
        {
            if (dgVehicleType.Visible.Equals(true))
            {
                //hide it
                dgVehicleType.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");

                // Adam 20130912 Hide the pagination control when the user clicks the hide button.
                dgVehicleTypePager.Visible = false;
            }
            else
            {
                //show it
                dgVehicleType.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
                dgVehicleTypePager.Visible = true;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            dgVehicleTypePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            dgVehicleTypePager.CurrentPageIndex = 1;
            BindGrid();
            //PunchStats805806 enquiry VehicleTypes
        }

        protected void ddlSelVTGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgVehicleType.Visible = true;
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            dgVehicleTypePager.RecordCount = 0;// 2013-03-15 add by Henry for pagination
            dgVehicleTypePager.CurrentPageIndex = 1;
            BindGrid();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void dgVehicleType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dgVehicleTypePager_PageChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}
