﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourtRoom_Tran.aspx.cs"
    Inherits="Stalberg.TMS.CourtRoom_Tran" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <style type="text/css">
        .NormalButton
        {
            height: 26px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
        <tr>
            <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upd" runat="server">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" border="0" height="85%">
                <tr>
                    <td align="center" valign="top">
                        <img style="height: 1px" src="images/1x1.gif" width="167">
                        <asp:Panel ID="pnlMainMenu" runat="server">
                        </asp:Panel>
                        <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                            BorderColor="#000000">
                            <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead">Options</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="Add "
                                            OnClick="btnOptAdd_Click"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnOptCopy" runat="server" Width="135px" CssClass="NormalButton"
                                            Text="Copy " Height="24px" OnClick="btnOptCopy_Click"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" height="21">
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td valign="top" align="left" width="100%" colspan="1">
                        <table border="0" width="100%">
                            <tr>
                                <td valign="top" style="height: 47px">
                                    <p align="center">
                                        <asp:Label ID="lblPageName" runat="server" Width="626px" CssClass="ContentHead">Court Room Transaction Number Maintenance</asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:Panel ID="pnlGeneral" runat="server" Width="100%">
                                        <table id="Table1" cellspacing="1" cellpadding="1" border="0">
                                            <tr>
                                                <td class="NormalBold">
                                                    Select&nbsp;Court: &nbsp;
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCourt" runat="server" CssClass="Normal" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="176px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="NormalBold">
                                                    Select&nbsp;Court Room: &nbsp;
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCourtRoom" runat="server" CssClass="Normal" AutoPostBack="True"
                                                        Width="176px" OnSelectedIndexChanged="ddlCourtRoom_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlTransactionList" runat="server" Width="100%">
                                        <asp:GridView ID="gvCourtRoomTran" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                            CssClass="Normal" OnSelectedIndexChanging="gvCourtRoomTran_SelectedIndexChanging" ShowFooter="true">
                                            <FooterStyle CssClass="CartListHead" />
                                            <Columns>
                                                <asp:BoundField DataField="CRTranType" HeaderText="Transaction Type">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CRTranNumber" HeaderText="Current Number">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CRTranDescr" HeaderText="Description">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CRTranYear" HeaderText="Year">
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:CommandField ShowSelectButton="true" SelectText="Edit">
                                                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                                </asp:CommandField>
                                            </Columns>
                                            <HeaderStyle CssClass="CartListHead" />
                                            <AlternatingRowStyle CssClass="CartListAlt" />
                                        </asp:GridView>
                                        <pager:AspNetPager id="gvCourtRoomTranPager" runat="server" 
                                            showcustominfosection="Right" width="400px"
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                             PageSize="10" 
                                            onpagechanged="gvCourtRoomTranPager_PageChanged" UpdatePanelId="upd"></pager:AspNetPager>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEditTranNo" runat="server" Width="100%">
                                        <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td height="2">
                                                    <asp:Label ID="lblEditTranNo" runat="server" CssClass="ProductListHead">Edit transaction</asp:Label>
                                                </td>
                                                <td width="248" height="2">
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" height="25">
                                                    <asp:Label ID="lblTNType" runat="server" CssClass="NormalBold">Type / Code:</asp:Label>
                                                </td>
                                                <td valign="top" width="248" height="25">
                                                    <asp:TextBox ID="txtCRTranType" runat="server" MaxLength="3" Width="60px"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="25">
                                                    <asp:Label ID="lblTNDescr" runat="server" CssClass="NormalBold">Description:</asp:Label>
                                                </td>
                                                <td width="248" height="25">
                                                    <asp:TextBox ID="txtCRTranDescr" runat="server" Width="264px" CssClass="Normal" MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>
                                                        <asp:Label ID="lblTNumber" runat="server" CssClass="NormalBold">Next number:</asp:Label></p>
                                                </td>
                                                <td width="248">
                                                    <asp:TextBox ID="txtCRTranNumber" runat="server" Width="60px" CssClass="Normal" MaxLength="10"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="revTNumber" runat="server" ForeColor=" " ControlToValidate="txtCRTranNumber"
                                                        ErrorMessage="Must be numeric" ValidationExpression="^[0-9]+$" CssClass="NormalRed"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>
                                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold">Year:</asp:Label></p>
                                                </td>
                                                <td width="248">
                                                    <asp:TextBox ID="txtCRTranYear" runat="server" Width="60px" CssClass="Normal" MaxLength="10"
                                                        Enabled="false"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor=" "
                                                        ControlToValidate="txtCRTranYear" ErrorMessage="Must be numeric" ValidationExpression="^[0-9]+$"
                                                        CssClass="NormalRed"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hidCRTranIntNo" runat="server" />
                                                </td>
                                                <td width="248" style="text-align: right" valign="top">
                                                    <asp:Button ID="btnUpdateTranNo" runat="server" CssClass="NormalButton" Text="Update Transaction"
                                                        OnClick="btnUpdateTranNo_Click"></asp:Button>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="udp" runat="server">
        <ProgressTemplate>
            <p class="Normal" style="text-align: center;">
                <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
