using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;
using System.Globalization;

namespace Stalberg.TMS
{
    public partial class CaptureSpotFineBatch : System.Web.UI.Page
    {
        // Fields
        private string connectionstring = string.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private int sfbIntNo = 0;
        private string sfbCompleteDate = string.Empty;
        private int notIntNo = 0;
        private int sfbnIntNo = 0;
        //private bool processBatch = false;
        private string notTicketNo = string.Empty;
        private string notOffenceDate = string.Empty;
        private string sfbnRctNumber = string.Empty;
        private string sfbnRTAmount = string.Empty;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        //protected string thisPage = "Notice and Receipt Batch Print";
        protected string description = string.Empty;
        protected string thisPageURL = "CaptureSpotFineBatch.aspx";

        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionstring = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionstring);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (this.ViewState["sfbIntNo"] != null)
                this.sfbIntNo = (int)this.ViewState["sfbIntNo"];
            if (this.ViewState["notIntNo"] != null)
                this.notIntNo = (int)this.ViewState["notIntNo"];
            if (this.ViewState["sfbnIntNo"] != null)
                this.sfbnIntNo = (int)this.ViewState["sfbnIntNo"];
            //if (this.ViewState["processBatch"] != null)
            //	this.processBatch = Convert.ToBoolean(this.ViewState["processBatch"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                btnNewBatch.Visible = false;

                this.PopulateAuthorities();

                this.btnPrintBatch.Visible = false;
                this.btnPrintRegister.Visible = false;

                //Make the Grid invisible until user clicks View Batches Button
                this.pnlBatches.Visible = false;
                this.pnlButtons.Visible = false;
                this.pnlPrintBatch.Visible = false;

                //need to check if must allow the user to start a new batch or carry on with their current batch.
                //if the batch has not been printed then they must carry on with the current batch
                Stalberg.TMS.SpotFineBatchNoticesDB sfbn = new Stalberg.TMS.SpotFineBatchNoticesDB(connectionstring);
                sfbIntNo = sfbn.CheckOpenBatches(this.login);
                if (sfbIntNo < 0)
                    btnNewBatch.Visible = true;
                
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {

            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionstring);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionstring);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            this.ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        // Fetches the batches for the chosen authority and populates the grid
        private void PopulateSpotFineBatches()
        {
            this.pnlButtons.Visible = false;

            autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);
            string CompletedBatches = "N";
            string PrintedBatches = "N";

            if (chkCompleted.Checked)
                CompletedBatches = "Y";

            if (chkPrinted.Checked)
            {
                PrintedBatches = "Y";
                CompletedBatches = "Y"; // to ensure all completed batches are included
            }

            //check if the new buttin must be visible...
            Stalberg.TMS.SpotFineBatchNoticesDB sfbn = new Stalberg.TMS.SpotFineBatchNoticesDB(connectionstring);
            sfbIntNo = sfbn.CheckOpenBatches(this.login);
            if (sfbIntNo < 0)
            {
                btnNewBatch.Visible = true;
            }
            else
            {
                btnNewBatch.Visible = false;
            }

            Stalberg.TMS.SpotFineBatchDB sfb = new Stalberg.TMS.SpotFineBatchDB(connectionstring);
            int totalCount = 0;
            DataSet dsSFB = sfb.GetSpotFineBatchListDS(autIntNo, CompletedBatches, PrintedBatches, this.login, GridViewBatchesPager.PageSize, GridViewBatchesPager.CurrentPageIndex, out totalCount);
            this.GridViewBatches.DataSource = dsSFB;
            GridViewBatchesPager.RecordCount = totalCount;

            // Bind the data
            this.GridViewBatches.DataBind();

            // Check if there are any results; if not, the GridViewBatches will not display and an error message will
            if (GridViewBatches.Rows.Count == 0)
            {
                this.pnlBatches.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
                this.pnlBatches.Visible = true;
        }

        public bool TrueFalse(string bitValue)
        {
            if (bitValue == "1" || bitValue == "true")
                return true;

            return false;
        }

        // Fetches the notices for the chosen batch and populates the grid
        private void PopulateSpotFineBatchNotices(string sortColumn = "", string drection = "")
        {
            this.autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);

            Stalberg.TMS.SpotFineBatchNoticesDB sfbn = new Stalberg.TMS.SpotFineBatchNoticesDB(connectionstring);
            //DataSet dsSFBN = sfbn.GetSpotFineBatchNoticesListDS(this.autIntNo, this.sfbIntNo, this.chkSpotFine.Checked, this.chkSummons.Checked);
            //2013-04-02 add by Henry for pagination
            int totalCount = 0;
            DataSet dsSFBN = sfbn.GetSpotFineBatchNoticesListDS(this.autIntNo, this.sfbIntNo,
                sortColumn, drection,
                GridViewNoticesPager.PageSize, GridViewNoticesPager.CurrentPageIndex, out totalCount);
            this.GridViewNotices.DataSource = dsSFBN;
            GridViewNoticesPager.RecordCount = totalCount;

            // Bind the data
            this.GridViewNotices.DataKeyNames = new string[] { "NotIntNo" };
            this.GridViewNotices.DataKeyNames = new string[] { "SFBNIntNo" };
            this.GridViewNotices.DataBind();

            //the notice check box does not need to be enabled
            //this needs to change as the check boxes are not being used and it will be by batch
            //if (this.chkSummons.Checked)
            if (this.GridViewBatches.SelectedRow.Cells[6].Text.Trim() == "AG")
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                        cbNotice.Enabled = false;
                    }

                }
            }
            else
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                        cbNotice.Enabled = true;
                    }

                }
            }
            //this.GridViewNotices.Sort("NotTicketNo", SortDirection.Ascending); //NotTicketNo = notice number

            // Check if there are any results
            if (GridViewNotices.Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            else
            {
                this.pnlBatches.Visible = false;

                this.pnlNotices.Visible = true;
                this.GridViewNotices.Visible = true;
                this.pnlButtons.Visible = true;
            }
        }

        // Populate the panel with details of the selected notice, but keep the GridView with Notices visible so 
        // the user can select another notice at any time
        private void PopulateFixNoticePanel()
        {
            // Split the Notice Ticket No. into prefix and sequence no for displaying on the panel
            string[] splitNotTicketNoArray = this.notTicketNo.Split(new char[] { '/' });
            string splitNotTicketNoPrefix = splitNotTicketNoArray[0];
            string splitNotTicketNoSeqNo = splitNotTicketNoArray[1];

            this.pnlFixNotice.Visible = true;

            //Put the data from the selected GridViewNotices row into the fields on the Notice Panel
            // this.lblSFBNIntNo.Text = Convert.Tostring(sfbnIntNo); 
            this.ViewState.Add("sfbnIntNo", this.sfbnIntNo);
            this.lblNoticePrefix.Text = splitNotTicketNoPrefix;
            this.lblNoticeSeqNo.Text = splitNotTicketNoSeqNo;
            this.lblFullNoticeNo.Text = notTicketNo;

            DateTime dt;
            if (DateTime.TryParse(notOffenceDate, out dt))
                this.lblOffenceDate.Text = DateTime.Parse(notOffenceDate).ToString(DATE_FORMAT);
            else
                this.lblOffenceDate.Text = string.Empty;

            this.txtRctNumber.Text = sfbnRctNumber;

            //update by Rachel 20140820 for 5337
            //this.lblRTAmount.Text = decimal.Parse(sfbnRTAmount).ToString("#,##0.00");
            this.lblRTAmount.Text = decimal.Parse(sfbnRTAmount).ToString("#,##0.00",CultureInfo.InvariantCulture);
            //end update by Rachel 20140820 for 5337

            this.txtRctNumber.Focus();
        }

        private void ProcessOneNotice()
        {
            this.sfbnIntNo = (int)this.ViewState["sfbnIntNo"];
            this.UpdateSpotFineBatchNotice();

            //Jerry 2013-09-13 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            // Refresh the Batch Notices GridView
            //2013-04-02 add by Henry for pagination
            //GridViewNoticesPager.RecordCount = 0;
            //GridViewNoticesPager.CurrentPageIndex = 1;
            this.PopulateSpotFineBatchNotices();
        }

        private void ProcessBatchOfNotices()
        {
            //this.sfbnIntNo = (int)this.ViewState["sfbnIntNo"];
            //this.UpdateSpotFineBatchNotice();

            //Stalberg.TMS.SpotFineBatchNoticesDB sfbngn = new Stalberg.TMS.SpotFineBatchNoticesDB(connectionstring);
            //DataSet dsSFBNGN = sfbngn.SpotFineBatch_NoticeGetNextDS(autIntNo, sfbIntNo);

            //// if a row is returned from the Get Next sql proc, then proceed
            //if (dsSFBNGN.Tables[0].Rows.Count != 0)
            //{
            //    processBatch = true;
            //    DataRow drSFBNGN = dsSFBNGN.Tables[0].Rows[0];
            //    sfbnIntNo = Convert.ToInt32(drSFBNGN["SFBNIntNo"]);
            //    notTicketNo = drSFBNGN["NotTicketNo"].ToString();
            //    notOffenceDate = drSFBNGN["NotOffenceDate"].ToString();
            //    sfbnRctNumber = drSFBNGN["SFBNRctNumber"].ToString();
            //    sfbnRTAmount = drSFBNGN["SFBNRTAmount"].ToString();

            //    this.PopulateFixNoticePanel();
            //}
            //// There were no rows returned from the Get Next sql proc
            //else
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "There are no more notices to process in this batch, please select the next batch for processing";
            //    // Should we automate the update of the SpotFineBatch table here by setting the SFBCompleteDate to current date at this point or 
            //    // must user click on Complete Batch button to do the above?
            //    this.processBatch = false;
            //    this.pnlBatches.Visible = false;
            //    this.pnlNotices.Visible = true;
            //    this.GridViewNotices.Visible = true;

            //    //hide fix notice panel -display notice grid
            //    this.PopulateSpotFineBatchNotices();
            //}

            //this.ViewState.Add("processBatch", processBatch);
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnViewBatches_Click(object sender, EventArgs e)
        {
            this.lblError.Text = string.Empty;
            //this.processBatch = false;
            //this.ViewState.Add("processBatch", processBatch);
            this.pnlNotices.Visible = false;
            this.GridViewNotices.Visible = false;
            this.pnlFixNotice.Visible = false;
            this.pnlButtons.Visible = false;
            this.pnlPrintBatch.Visible = false;
            //Henry 2013-03-28 add for pagination
            GridViewBatchesPager.RecordCount = 0;
            GridViewBatchesPager.CurrentPageIndex = 1;
            this.PopulateSpotFineBatches();
        }

        protected void btnNewBatch_Click(object sender, EventArgs e)
        {
            // BD here we need to go and fetch the latest batch to be processed 
            // This will be based on the Authority and the rules set by the supervisor
            Stalberg.TMS.SpotFineBatchDB sfb = new Stalberg.TMS.SpotFineBatchDB(connectionstring);
            int newSFBIntNo = sfb.AllocateNextBatch(login, autIntNo);
            if (newSFBIntNo == -1)
            {
                //there was an error creating the new batch
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                //set the state of the screen back to the select batch button (as if they ghad just come into the screen)
                this.btnPrintBatch.Visible = false;
                this.btnPrintRegister.Visible = false;
                this.pnlNotices.Visible = false;
                this.GridViewNotices.Visible = false;
                this.pnlFixNotice.Visible = false;
                this.pnlBatches.Visible = false;
                this.pnlButtons.Visible = false;
                this.pnlPrintBatch.Visible = false;
            }
            else if (newSFBIntNo == -2)
            {
                //there was no more batches to allocate
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                //set the state of the screen back to the select batch button (as if they ghad just come into the screen)
                this.btnPrintBatch.Visible = false;
                this.btnPrintRegister.Visible = false;
                this.pnlNotices.Visible = false;
                this.GridViewNotices.Visible = false;
                this.pnlFixNotice.Visible = false;
                this.pnlBatches.Visible = false;
                this.pnlButtons.Visible = false;
                this.pnlPrintBatch.Visible = false;
            }
            else
            {

                // Once the new batch has been allocated then must carry on with the population of the batch grid
                this.lblError.Text = string.Empty;
                this.pnlNotices.Visible = false;
                this.GridViewNotices.Visible = false;
                this.pnlFixNotice.Visible = false;
                this.pnlButtons.Visible = false;
                this.pnlPrintBatch.Visible = false;

                //BD need to reset button labels
                Session["ReceiptSelect"] = "1";
                btnReceiptSelect.Text = (string)GetLocalResourceObject("btnReceiptSelect.Text");

                Session["NoticeSelect"] = "1";
                btnNoticeSelect.Text = (string)GetLocalResourceObject("btnNoticeSelect.Text");
                //Henry 2013-03-28 add for pagination
                GridViewBatchesPager.CurrentPageIndex = 1;
                GridViewBatchesPager.RecordCount = 0;
                this.PopulateSpotFineBatches();
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticeAndReceiptBatchPrint, PunchAction.Change);  

            }
        }

        protected void GridViewBatches_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reset necessary controls
            lblError.Text = string.Empty;

            // Check for a completed date
            this.sfbCompleteDate = this.GridViewBatches.SelectedRow.Cells[3].Text.Trim();

            //BD 3858 check that the user has selected either summons or spot fine only when it is a non completed batch.
            //BD have moved this to the notices grid as the control for what to display. Defualt to show spot fines.
            //if (chkSummons.Checked || chkSpotFine.Checked || !Helper_Web.IsWebStringEmpty(this.sfbCompleteDate))
            //{
            this.pnlButtons.Visible = false;
            //this.pnlDetails.Visible = false;
            this.pnlPrintBatch.Visible = false;
            this.pnlFixNotice.Visible = false;
            this.pnlNotices.Visible = false;

            //BD set the default view to show spot fines
            chkSpotFine.Checked = true;
            //BD need to set the sort order drop down items - need to check if we are dealing with an AG or Spot Fine
            if (this.GridViewBatches.SelectedRow.Cells[6].Text.Trim() == "SF")
            {
                CreateSortList("SpotFine");
            }
            else
            {
                CreateSortList("AG");
                btnNoticeSelect.Enabled = false;
            }
            //BD 3858 set the text of the label for what has been selected
            //if (chkSpotFine.Checked && !chkSummons.Checked)
            //    this.lblNoticeHeader.Text = "Spot Fine Listing";

            //if (!chkSpotFine.Checked && chkSummons.Checked)
            //    this.lblNoticeHeader.Text = "AG Listing";

            //BD not allowed to show both at the same time    
            //if (chkSpotFine.Checked && chkSummons.Checked)
            //    this.lblNoticeHeader.Text = "All Notices";

            this.ViewState.Remove("PrintBatch");

            // Get the selected SFBIntNo
            Label lbl = (Label)this.GridViewBatches.Rows[this.GridViewBatches.SelectedIndex].Cells[0].Controls[1];
            this.sfbIntNo = Convert.ToInt32(lbl.Text);
            this.ViewState.Add("sfbIntNo", this.sfbIntNo);



            //  Check if the batch has been completed and only populate the next Notice GridView if the batch is incomplete
            if (Helper_Web.IsWebStringEmpty(this.sfbCompleteDate))
            {
                //this.processBatch = true;

                // See if there are any more notices that need receipts
                //SpotFineBatchNoticesDB db = new SpotFineBatchNoticesDB(this.connectionstring);
                //DataSet ds = db.SpotFineBatch_NoticeGetNextDS(autIntNo, sfbIntNo);

                // There are still notices to process
                //if (ds.Tables[0].Rows.Count != 0)
                //{
                //    DataRow drSFBNGN = ds.Tables[0].Rows[0];

                //    this.sfbnIntNo = Convert.ToInt32(drSFBNGN["SFBNIntNo"]);
                //    this.notTicketNo = drSFBNGN["NotTicketNo"].ToString();
                //    this.notOffenceDate = drSFBNGN["NotOffenceDate"].ToString();
                //    this.sfbnRctNumber = drSFBNGN["SFBNRctNumber"].ToString();
                //    this.sfbnRTAmount = drSFBNGN["SFBNRTAmount"].ToString();

                //    this.PopulateFixNoticePanel();
                //    this.pnlButtons.Visible = false;
                //}
                //else // There are no more notices that require receipts
                //{
                //this.processBatch = false;
                //this.lblError.Visible = true;
                //this.lblError.Text = "There are no more notices to process in this batch, please select the next batch for processing";
                this.pnlNotices.Visible = false;
                this.GridViewNotices.Visible = false;
                this.pnlFixNotice.Visible = false;

                //The user will always be allowed to update the print batch allocation
                //bool isCompleted = Helper_Web.IsWebStringEmpty(this.GridViewBatches.SelectedRow.Cells[3].Text);
                //this.btnCompleteBatch.Visible = isCompleted;
                //this.btnUpdatePrintSchedule.Visible = isCompleted;
                this.pnlButtons.Visible = true;
                //2013-04-02 add by Henry for pagination
                GridViewNoticesPager.RecordCount = 0;
                GridViewNoticesPager.CurrentPageIndex = 1;
                this.PopulateSpotFineBatchNotices();
                //}

                //this.ViewState.Add("processBatch", processBatch);
            }
            else
            {
                //this.lblError.Visible = true;
                //this.lblError.Text = string.Format("Batch {0} has already been completed, please select another batch for processing or select the print file below for printing.", lbl.Text);

                this.pnlNotices.Visible = false;
                this.GridViewNotices.Visible = false;
                this.pnlFixNotice.Visible = false;

                //The user will always be allowed to update the print batch allocation
                //bool isCompleted = Helper_Web.IsWebStringEmpty(this.GridViewBatches.SelectedRow.Cells[3].Text);
                //this.btnCompleteBatch.Visible = isCompleted;
                //this.btnUpdatePrintSchedule.Visible = isCompleted;
                this.pnlButtons.Visible = true;
                //2013-04-02 add by Henry for pagination
                GridViewNoticesPager.RecordCount = 0;
                GridViewNoticesPager.CurrentPageIndex = 1;
                this.PopulateSpotFineBatchNotices();
                //2013-04-02 add by Henry for pagination
                grdPrintBatchPager.RecordCount = 0;
                grdPrintBatchPager.CurrentPageIndex = 1;
                this.PopulatePrintBatch(this.sfbIntNo);
            }
            //}
            //else
            //{
            //    this.lblError.Visible = true;
            //	this.lblError.Text = "Please select either Summons or Spot Fine data to view.";
            //}
        }

        public void CreateSortList(string listName)
        {
            ListItem ddlRN = new ListItem(); //Receipt Number
            ddlRN.Text = (string)GetLocalResourceObject("ddlRN.Text");
            ddlRN.Value = "SFBNRctNumber";

            ListItem ddlNN = new ListItem(); //Notice Number
            ddlNN.Text = (string)GetLocalResourceObject("ddlNN.Text");
            ddlNN.Value = "NotTicketNo";

            ListItem ddlAN = new ListItem(); //AG Number
            ddlAN.Text = (string)GetLocalResourceObject("ddlAN.Text");
            ddlAN.Value = "AG Numner";

            ListItem ddlSN = new ListItem(); //Summons Number
            ddlSN.Text = (string)GetLocalResourceObject("ddlSN.Text");
            ddlSN.Value = "Summons Numner";

            ListItem ddlCD = new ListItem(); //Court Date
            ddlCD.Text = (string)GetLocalResourceObject("ddlCD.Text");
            ddlCD.Value = "Court Date";

            ddlSortOrder.Items.Clear();

            if (listName == "SpotFine")
            {
                //must create options for spot fine sort
                ddlSortOrder.Items.Add(ddlNN);
                ddlSortOrder.Items.Add(ddlRN);

            }
            else if (listName == "AG")
            {
                //must create options for AG sort
                ddlSortOrder.Items.Add(ddlAN);
                ddlSortOrder.Items.Add(ddlRN);
                ddlSortOrder.Items.Add(ddlSN);
            }
        }

        protected void GridViewNotices_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Only execute this section if user clicked Select to fix a specific notice, otherwise he has selected another page
            if (e.CommandName.Equals("Select"))
            {
                //this.processBatch = false;
                //this.ViewState.Add("processBatch", processBatch);

                this.GridViewNotices.SelectedIndex = Convert.ToInt32(e.CommandArgument);
                int nRow = GridViewNotices.SelectedIndex;

                this.notIntNo = (int)this.GridViewNotices.DataKeys[nRow].Value;
                this.sfbnIntNo = (int)this.GridViewNotices.DataKeys[nRow].Value;

                Label lblFullNoticeNo = (Label)this.GridViewNotices.SelectedRow.Cells[1].FindControl("Label1");
                Label lblOffenceDate = (Label)this.GridViewNotices.SelectedRow.Cells[3].FindControl("Label3");
                Label lblRctNumber = (Label)this.GridViewNotices.SelectedRow.Cells[4].FindControl("Label4");
                Label lblRTAmount = (Label)this.GridViewNotices.SelectedRow.Cells[5].FindControl("Label5");

                this.notTicketNo = lblFullNoticeNo.Text;
                this.notOffenceDate = lblOffenceDate.Text;
                this.sfbnRctNumber = lblRctNumber.Text;
                this.sfbnRTAmount = lblRTAmount.Text;

                this.ViewState.Add("notIntNo", notIntNo);
                this.ViewState.Add("sfbnIntNo", sfbnIntNo);

                //this.pnlNotices.Visible = false;
                this.pnlFixNotice.Visible = true;

                this.PopulateFixNoticePanel();
            }
        }

        protected void btnRejectNotice_Click(object sender, EventArgs e)
        {
            //lblError.Visible = false;

            //this.txtRctNumber.Text = "0";

            //if (processBatch == false)
            //{
            //    this.ProcessOneNotice();
            //    this.pnlFixNotice.Visible = false;
            //}
            //else
            //{
            //    //this.pnlFixNotice.Visible = false;
            //    this.ProcessBatchOfNotices();
            //}
        }

        protected void btnCancelNotice_Click(object sender, EventArgs e)
        {
            //lblError.Visible = false;

            //this.txtRctNumber.Text = "0";

            //if (processBatch == false)
            //{
            //    this.ProcessOneNotice();
            //    this.pnlFixNotice.Visible = false;
            //}
            //else
            //{
            //    //this.pnlFixNotice.Visible = false;
            //    this.ProcessBatchOfNotices();
            //}

            this.pnlFixNotice.Visible = false;
        }


        protected void btnAcceptNotice_Click(object sender, EventArgs e)
        {
            //lblError.Visible = false;

            //int iRctNumberLength = txtRctNumber.Text.Trim().Length;
            //if (iRctNumberLength == 0)
            //{
            //    //sfbnRctNumber.Value = "0";
            //    lblError.Visible = true;
            //    lblError.Text = "Please enter a valid receipt number";
            //    return;
            //}
            //else if (txtRctNumber.Text.Trim().Equals("0"))
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "Please enter a valid receipt number for this receipt to be accepted. Use 'Reject' for receipts that are not for spot fines";
            //    return;
            //}

            //// If the user wants to update only one notice
            //if (!processBatch)
            //{
            //    this.ProcessOneNotice();
            //    this.pnlFixNotice.Visible = false;
            //}// The user is processing a batch of notices
            //else
            //{
            //    this.ProcessBatchOfNotices();
            //    //this.pnlFixNotice.Visible = false;
            //}
        }

        protected void btnUpdateNotice_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;

            int iRctNumberLength = txtRctNumber.Text.Trim().Length;
            if (iRctNumberLength == 0)
            {
                //sfbnRctNumber.Value = "0";
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                return;
            }
            else if (txtRctNumber.Text.Trim().Equals("0"))
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                return;
            }

            //BD no need to process a batch of notices they would only now and again be updating one notice.

            // If the user wants to update only one notice
            //if (!processBatch)
            //{
            this.ProcessOneNotice();
            this.pnlFixNotice.Visible = false;
            //}// The user is processing a batch of notices
            //else
            //{
            //this.ProcessBatchOfNotices();
            //this.pnlFixNotice.Visible = false;
            //}
        }

        protected void UpdateSpotFineBatchNotice()
        {
            SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);

            //int updSFBNIntNo = sfbn.UpdateSpotFineBatchNotice(this.sfbnIntNo, this.txtRctNumber.Text, this.login);
            int updSFBNIntNo = sfbn.UpdateSpotFineBatchNotice(this.sfbnIntNo, this.txtRctNumber.Text, Convert.ToDecimal(this.lblRTAmount.Text), this.login);
            if (updSFBNIntNo>0)
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticeAndReceiptBatchPrint, PunchAction.Change);  

            }
        }

        protected void btnUpdatePrintSchedule_Click(object sender, EventArgs e)
        {
            int updateCount = 0;
            //BD update SpotFineBatchNotices with which notices and receipts must be printed so the 
            //user can save and come back to create the print file
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {
                    int rIndex = gvNotices.RowIndex;
                    GridViewNotices.SelectedIndex = rIndex;
                    string sfbnIntNo = GridViewNotices.SelectedDataKey["SFBNIntNo"].ToString();

                    CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                    CheckBox cbReceipt = (CheckBox)gvNotices.FindControl("printReceipt");
                    Label lblRctNumber = (Label)gvNotices.FindControl("Label4");
                    Label lblAmount = (Label)gvNotices.FindControl("Label5");

                    SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);
                    int updSFBNIntNo = sfbn.UpdateSpotFineBatchNotice(Convert.ToInt32(sfbnIntNo), lblRctNumber.Text, Convert.ToDecimal(lblAmount.Text), cbNotice.Checked, cbReceipt.Checked, this.login);
                    if (updSFBNIntNo > 0)
                    {
                        updateCount = updateCount + 1;
                    }
                    
                }
            }
            //2014-01-22 Heidi added for add hint information(5103)
            if (updateCount > 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text12");
            }

           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticeAndReceiptBatchPrint, PunchAction.Change);  


        }

        protected void btnCompleteBatch_Click(object sender, EventArgs e)
        {
            //BD the main event here is the creation of the print file 
            //BD must create a seperate print file for both AG and Spot fine

            //BD No need to create seperate print files based on AG or spotfine as they will already have been seperated... 
            //Need to create on print file name but multple print time stamps and then split the grid based on time stamps
            //this way we have one print file when needed but can break it down into each time they changed the print allocation

            if (this.sfbIntNo == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                return;
            }

            //BD Also need to check that the user has selected at least one receipt or payment.
            int oneSelected = 0;
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                    CheckBox cbReceipt = (CheckBox)gvNotices.FindControl("printReceipt");
                    if (cbNotice.Checked || cbReceipt.Checked)
                    {
                        oneSelected = 1;
                    }
                }
            }
            if (oneSelected == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                return;
            }

            //BD Must first update SpotFineBatchNotices with which notices and receipts must be printed and 
            //then can generate print file based on those results
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {
                    
                    int rIndex = gvNotices.RowIndex;
                    GridViewNotices.SelectedIndex = rIndex;
                    string sfbnIntNo = GridViewNotices.SelectedDataKey["SFBNIntNo"].ToString();

                    CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                    CheckBox cbReceipt = (CheckBox)gvNotices.FindControl("printReceipt");
                    Label lblRctNumber = (Label)gvNotices.FindControl("Label4");
                    Label lblAmount = (Label)gvNotices.FindControl("Label5");

                    //need to only set the ones that have been selected for printing...
                    if (cbNotice.Checked || cbReceipt.Checked)
                    {
                        SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);
                        int updSFBNIntNo = sfbn.UpdateSpotFineBatchNotice(Convert.ToInt32(sfbnIntNo), lblRctNumber.Text, Convert.ToDecimal(lblAmount.Text), cbNotice.Checked, cbReceipt.Checked, this.login);
                    }
                }
            }

            // Mark the batch as completed and put the notices into print batches of 50
            // BD do we need to put them into 50? They will be broken into managable sizes on creation, also going to be showing them broken down by date.
            SpotFineBatchNoticesDB db = new SpotFineBatchNoticesDB(this.connectionstring);
            db.MarkBatchAsCompleted(this.sfbIntNo, this.login);

            //2013-04-02 add by Henry for pagination
            grdPrintBatchPager.RecordCount = 0;
            grdPrintBatchPager.CurrentPageIndex = 1;
            // Show the print batches panel and grid
            this.PopulatePrintBatch(this.sfbIntNo);

            //this.pnlButtons.Visible = false;
            this.pnlFixNotice.Visible = false;
            //this.pnlDetails.Visible = false;
            //this.pnlNotices.Visible = false;

            // Repopulate the batch grid
            //this.PopulateSpotFineBatches();
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticeAndReceiptBatchPrint, PunchAction.Change);  

        }

        private void PopulatePrintBatch(int sfbIntNo)
        {
            // Get the data & populate the grid
            SpotFineBatchNoticesDB db = new SpotFineBatchNoticesDB(this.connectionstring);
            DataSet ds;
            //if (this.ViewState["PrintBatch"] != null)
            //    ds = (DataSet)this.ViewState["PrintBatch"];
            //else
            //{
            int totalCount = 0;
                ds = db.GetPrintBatches(sfbIntNo, grdPrintBatchPager.PageSize, grdPrintBatchPager.CurrentPageIndex, out totalCount);
                this.grdPrintBatch.PageIndex = 0;
                grdPrintBatchPager.RecordCount = totalCount;
            //}

            this.grdPrintBatch.DataSource = ds;
            this.grdPrintBatch.DataKeyNames = new string[] { "SFBNPrintFile", "UserCompleteDate" };
            this.ViewState.Add("PrintBatch", ds);
            this.grdPrintBatch.DataBind();

            //no need to do this anymore as there is no deleting of a print file just updating.
            //if (!this.User.IsInRole("supervisor"))
            //{
                //grdPrintBatch.Columns[7].Visible = false;
            //}

            this.pnlPrintBatch.Visible = true;
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Henry 2013-03-28 add for pagination
            GridViewBatchesPager.RecordCount = 0;
            GridViewBatchesPager.CurrentPageIndex = 1;
            this.PopulateSpotFineBatches();
        }

        protected void btnPrintRegister_Click(object sender, EventArgs e)
        {
            // TODO: Print the register (?)
        }

        protected void btnPrintBatch_Click(object sender, EventArgs e)
        {
            if (this.sfbIntNo == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                return;
            }

            // Show the print viewer
            //2013-04-02 add by Henry for pagination
            grdPrintBatchPager.RecordCount = 0;
            grdPrintBatchPager.CurrentPageIndex = 1;
            this.PopulatePrintBatch(this.sfbIntNo);
           
        }

        private void PrintBatch(string printName)
        {
            PageToOpen page = new PageToOpen(this, "CaptureSpotFineBatchViewer.aspx");
            page.AddParameter("Batch", printName);

            Helper_Web.BuildPopup(page);
        }

        protected void grdPrintBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            string printName = this.grdPrintBatch.SelectedDataKey["SFBNPrintFile"].ToString();
            //BD need to also handle the completion date so that we can control what they print in smaller numbers as they change their mind
            
            DateTime userCompleteDate;
            if (!DateTime.TryParse(this.grdPrintBatch.SelectedDataKey["UserCompleteDate"].ToString(), out userCompleteDate))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                return;
            }

            string printDate = this.grdPrintBatch.SelectedRow.Cells[2].Text.Trim();

            // Mark the print batch as having been printed
            if (string.IsNullOrEmpty(printDate) || printDate.Equals("&nbsp;", StringComparison.CurrentCultureIgnoreCase))
                this.MarkBatchAsPrinted(printName);

            //dont need the panel with the notices showing, must enable or disable the create new batch button.
            //this.PopulateSpotFineBatches();
            btnNewBatch.Visible = true;
            this.ViewState.Remove("PrintBatch");

            //Jerry 2013-09-13 Comment out these 2 lines of code to solve the problem that the page will navigate back to page 1 automatically when one row on any page is updated!!
            //2013-04-02 add by Henry for pagination
            //grdPrintBatchPager.RecordCount = 0;
            //grdPrintBatchPager.CurrentPageIndex = 1;
            if (grdPrintBatch.Rows.Count == 1)
                grdPrintBatchPager.CurrentPageIndex--;

            this.PopulatePrintBatch(this.sfbIntNo);

            PageToOpen page = new PageToOpen(this, "CaptureSpotFineBatchViewer.aspx");
            page.AddParameter("Batch", printName);
            page.AddParameter("UserCompleteDate", userCompleteDate);
            Helper_Web.BuildPopup(page);
        }

        protected void grdPrintBatch_OnRowDeleting(object sender, EventArgs e)
        {
            //BD 3858 - allow a supervisor to delete a print file for a batch so the user can go 
            //back and recreate the print file. This is only allowed to happen if the file has not been printed yet.

            //1. Need to delete the print file name from the SpotFineBatch_notice table
            //2. Need to delete the completed date out of the SpotFineBatch table
            //3. must audit trail the process

            //string printFile = string.Empty; //grdPrintBatch.["SFBNPrintFile"].ToString();
            //SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);
            //sfbn.DeletePrintFile(printFile, this.login);

            //must check which view and data I must show after delete.
            //this.PopulateSpotFineBatches();
            //this.ViewState.Remove("PrintBatch");

            //bool isCompleted = Helper_Web.IsWebStringEmpty(this.GridViewBatches.SelectedRow.Cells[3].Text);
            //this.btnCompleteBatch.Visible = isCompleted;
            //this.btnUpdatePrintSchedule.Visible = isCompleted;
            //this.pnlButtons.Visible = true;

            //this.PopulateSpotFineBatchNotices();
        }

        protected void grdPrintBatch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "pfDelete")
            {
                string printFile = e.CommandArgument.ToString();
                SpotFineBatchNoticesDB sfbn = new SpotFineBatchNoticesDB(this.connectionstring);
                sfbn.DeletePrintFile(printFile, this.login);

                //Henry 2013-03-28 add for pagination
                GridViewBatchesPager.CurrentPageIndex = 1;
                GridViewBatchesPager.RecordCount = 0;
                //must check which view and data I must show after delete.
                this.PopulateSpotFineBatches();

                this.ViewState.Remove("PrintBatch");
                //2013-04-02 add by Henry for pagination
                //grdPrintBatchPager.RecordCount = 0;
                //grdPrintBatchPager.CurrentPageIndex = 1;
                if (grdPrintBatch.Rows.Count == 1)
                    grdPrintBatchPager.CurrentPageIndex--;
                this.PopulatePrintBatch(this.sfbIntNo);

                //bool isCompleted = Helper_Web.IsWebStringEmpty(this.GridViewBatches.SelectedRow.Cells[3].Text);
                //this.btnCompleteBatch.Visible = isCompleted;
                //this.btnUpdatePrintSchedule.Visible = isCompleted;
                //this.pnlButtons.Visible = true;

                //this.PopulateSpotFineBatchNotices();
            }

        }

        private void MarkBatchAsPrinted(string printName)
        {
            SpotFineBatchNoticesDB db = new SpotFineBatchNoticesDB(this.connectionstring);
            db.MarkBatchAsPrinted(printName, this.login);
        }

        protected void chkSpotFine_CheckedChanged(object sender, EventArgs e)
        {
            chkSummons.Checked = false;
            CreateSortList("SpotFine");
            lblNoticeHeader.Text = (string)GetLocalResourceObject("lblNoticeHeader.Text");
            btnNoticeSelect.Enabled = true;
            //2013-04-02 add by Henry for pagination
            GridViewNoticesPager.RecordCount = 0;
            GridViewNoticesPager.CurrentPageIndex = 1;
            PopulateSpotFineBatchNotices();
        }
        protected void chkSummons_CheckedChanged(object sender, EventArgs e)
        {
            chkSpotFine.Checked = false;
            CreateSortList("AG");
            lblNoticeHeader.Text = (string)GetLocalResourceObject("lblNoticeHeader.Text1");
            //dont need to be able to select the receipt select all button
            btnNoticeSelect.Enabled = false;
            //2013-04-02 add by Henry for pagination
            GridViewNoticesPager.RecordCount = 0;
            GridViewNoticesPager.CurrentPageIndex = 1;
            PopulateSpotFineBatchNotices();
        }
        protected void GridViewNotices_Sorting(object sender, GridViewSortEventArgs e)
        {
            //BD sorting of notice grid
            string sortExpression = e.SortExpression;

            //Jerry 2013-09-13 change it, ought to use GridViewNoticesPager
            //2013-04-1 add by Henry for pagination
            //GridViewBatchesPager.RecordCount = 0;
            //GridViewBatchesPager.CurrentPageIndex = 1;
            GridViewNoticesPager.RecordCount = 0;
            GridViewNoticesPager.CurrentPageIndex = 1;

            ViewState["SortColumn"] = sortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, "DESC");
                ViewState["Direction"] = "DESC";
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, "ASC");
                ViewState["Direction"] = "ASC";
            }
        }

        private void SortGridView(string sortExpression, string direction)
        {

            this.autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);

            Stalberg.TMS.SpotFineBatchNoticesDB sfbn = new Stalberg.TMS.SpotFineBatchNoticesDB(connectionstring);

            int totalCount = 0; //2013-04-1 add by Henry for pagination
            //Jerry 2013-09-13 change it, ought to use GridViewNoticesPager
            //DataSet dsSFBN = sfbn.GetSpotFineBatchNoticesListDS(this.autIntNo, this.sfbIntNo, 
            //    sortExpression, direction,
            //    GridViewBatchesPager.PageSize, GridViewBatchesPager.CurrentPageIndex, out totalCount);
            //GridViewBatchesPager.RecordCount = totalCount;

            DataSet dsSFBN = sfbn.GetSpotFineBatchNoticesListDS(this.autIntNo, this.sfbIntNo,
                sortExpression, direction,
                GridViewNoticesPager.PageSize, GridViewNoticesPager.CurrentPageIndex, out totalCount);
            GridViewNoticesPager.RecordCount = totalCount;

            GridViewNotices.DataSource = dsSFBN;
            GridViewNotices.DataBind();
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }


        protected void ddlSortOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            //the sort direction does not matter here as it is being handled in the view state.
            GridViewNotices.Sort(ddlSortOrder.SelectedValue.ToString(), SortDirection.Ascending);
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            //need to find the value entered or part there of in thegrid, muts just move to that position in the grid. 
            //the value entered is based on the sort order variable
            int iCheck = 0;
            lblError.Text = string.Empty;
            //need to reset the highlighted ones
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {
                    if (gvNotices.BackColor == System.Drawing.Color.Yellow)
                    {
                        gvNotices.BackColor = System.Drawing.Color.Empty;
                    }
                }

            }
            //need to set the first highlighted one
            foreach (GridViewRow gvNotices in GridViewNotices.Rows)
            {
                if (gvNotices.RowType == DataControlRowType.DataRow)
                {
                    Label lblRctNumber = (Label)gvNotices.FindControl("Label4");
                    Label lblNoticeNumber = (Label)gvNotices.FindControl("Label1");

                    //if (txtFind.Text == lblRctNumber.Text || txtFind.Text == lblNoticeNumber.Text)
                    if (lblRctNumber.Text.Contains(txtFind.Text) || lblNoticeNumber.Text.Contains(txtFind.Text))
                    {
                        //set the variable check that we do actually find a value
                        iCheck = 1;
                        //found the row we are looking for, must now hightlight it.
                        gvNotices.BackColor = System.Drawing.Color.Yellow;
                        break;
                    }
                    
                }
            }
            if (iCheck == 0)
            {
                //must tell the user the value was not found.
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            }
        }
        protected void btnNoticeSelect_Click(object sender, EventArgs e)
        {
            // select or deselect all notices

            //the button has not been clicked as of yet or everything has been deselected
            if (Session["NoticeSelect"] == null || Session["NoticeSelect"].ToString() == "1")
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                        cbNotice.Checked = true;
                    }

                }
                Session["NoticeSelect"] = "0";
                btnNoticeSelect.Text = (string)GetLocalResourceObject("btnNoticeSelect.Text1");
            }
            else
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printNotice");
                        cbNotice.Checked = false;
                    }

                }
                Session["NoticeSelect"] = "1";
                btnNoticeSelect.Text = (string)GetLocalResourceObject("btnNoticeSelect.Text");
            }

        }
        protected void btnReceiptSelect_Click(object sender, EventArgs e)
        {
            // select or deselect all Receipts

            //the button has not been clicked as of yet or everything has been deselected
            if (Session["ReceiptSelect"] == null || Session["ReceiptSelect"].ToString() == "1")
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printReceipt");
                        cbNotice.Checked = true;
                    }

                }
                Session["ReceiptSelect"] = "0";
                btnReceiptSelect.Text = (string)GetLocalResourceObject("btnReceiptSelect.Text1");
            }
            else
            {
                foreach (GridViewRow gvNotices in GridViewNotices.Rows)
                {
                    if (gvNotices.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox cbNotice = (CheckBox)gvNotices.FindControl("printReceipt");
                        cbNotice.Checked = false;
                    }

                }
                Session["ReceiptSelect"] = "1";
                btnReceiptSelect.Text = (string)GetLocalResourceObject("btnReceiptSelect.Text");
            }
        }

        //2013-03-28 Henry add for pagination
        protected void GridViewBatchesPager_PageChanged(object sender, EventArgs e)
        {
            this.pnlNotices.Visible = false;
            this.GridViewNotices.Visible = false;
            this.pnlFixNotice.Visible = false;
            this.pnlButtons.Visible = false;
            this.pnlPrintBatch.Visible = false;

            autIntNo = Convert.ToInt32(ddlAuthority.SelectedValue);
            this.PopulateSpotFineBatches();
        }

        //2013-04-02 add by Henry for pagination
        protected void GridViewNoticesPager_PageChanged(object sender, EventArgs e)
        {
            if (ViewState["SortColumn"] != null && ViewState["Direction"] != null)
            {
                this.PopulateSpotFineBatchNotices((string)ViewState["SortColumn"], (string)ViewState["Direction"]);
            }
            else
            {
                PopulateSpotFineBatchNotices();
            }
        }

        //2013-04-02 add by Henry for pagination
        protected void grdPrintBatchPager_PageChanged(object sender, EventArgs e)
        {
            this.PopulatePrintBatch(this.sfbIntNo);
        }
}
}