<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SummonsServer" Codebehind="SummonsServer.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <script type="text/javascript">
        function ConfirmDelete() {
            var msg = "<%=ContinueToDelete %>";
            return confirm(msg);
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0" class="Normal">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="100%">
                        <tr>
                            <td valign="top" align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center" class="Normal">
                                <asp:UpdatePanel ID="udpSummonsServers" runat="server">
                                    <ContentTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 50%; text-align: left; vertical-align: top">
                                                    <asp:Panel ID="pnlSummonsServers" runat="server" Width="100%">
                                                        <p>
                                                            <asp:LinkButton ID="lnkAddSummonsServer" runat="server" OnClick="btnOptAdd_Click" Text="<%$Resources:lnkAddSummonsServer.Text %>"></asp:LinkButton>
                                                        </p>
                                                        <asp:DataGrid ID="grdSummonsServers" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                                            CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgSummonsServer_ItemCommand"
                                                            OnPageIndexChanged="dgSummonsServer_PageIndexChanged" CssClass="Normal" PageSize="20">
                                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="15" CssClass="Normal" />
                                                            <AlternatingItemStyle CssClass="CartListItemAlt" />
                                                            <ItemStyle CssClass="CartListItem" />
                                                            <HeaderStyle CssClass="CartListHead" />
                                                            <Columns>
                                                                <asp:BoundColumn Visible="False" DataField="SSIntNo" HeaderText="SSIntNo">
                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                        Font-Underline="False" Wrap="False" />
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="SSFullName" HeaderText="<%$Resources:grdSummonsServers.HeaderText1 %>">
                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                        Font-Underline="False" Wrap="False" />
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="SSCode" HeaderText="<%$Resources:grdSummonsServers.HeaderText2 %>">
                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                        Font-Underline="False" Wrap="False" />
                                                                </asp:BoundColumn>
                                                                <asp:ButtonColumn Text="<%$Resources:grdSummonsServers.HeaderText3 %>" CommandName="Select" HeaderText="<%$Resources:grdSummonsServers.HeaderText3 %>">
                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                        Font-Underline="False" Wrap="False" />
                                                                </asp:ButtonColumn>
                                                                <asp:ButtonColumn CommandName="Delete" HeaderText="<%$Resources:grdSummonsServers.HeaderText4 %>" Text="<%$Resources:grdSummonsServers.HeaderText4 %>">
                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                        Font-Underline="False" Wrap="False" />
                                                                </asp:ButtonColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlSummonsServerDetails" runat="server" Width="100%">
                                                        <table id="Table3" cellspacing="1" cellpadding="1" border="0">
                                                            <tr>
                                                                <td align="center" colspan="2">
                                                                    <asp:Label ID="lblSummonsServerDetails" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblSummonsServerDetails.Text %>"></asp:Label></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSSFullName.Text %>"></asp:Label></td>
                                                                <td valign="top">
                                                        			<asp:TextBox ID="txtSSFullName" runat="server" Width="300px" CssClass="NormalMand"
                                                            MaxLength="30"></asp:TextBox></td>
                                                                <td>
                                                                    																						<asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" CssClass="NormalRed"
                                                            ErrorMessage="<%$Resources:ReqSSFullName.ErrorMessage %>" ControlToValidate="txtSSFullName" Display="dynamic"
                                                            ForeColor=" "></asp:RequiredFieldValidator></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblAddress" runat="server" CssClass="NormalBold" Width="94px" Text="<%$Resources:lblAddress.Text %>"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSSAddress" runat="server" CssClass="Normal" MaxLength="100" Width="300px"
                                                                        Rows="5" TextMode="MultiLine"></asp:TextBox></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="94px" Text="<%$Resources:lblSSTelNo.Text %>"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSSTelNo" runat="server" CssClass="Normal" MaxLength="21" Width="300px"></asp:TextBox></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblEmail" runat="server" CssClass="NormalBold" Width="94px" Text="<%$Resources:lblEmail.Text %>"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSSEmail" runat="server" CssClass="Normal" MaxLength="50" Width="300px"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:Label ID="lblValidEmail" runat="server" CssClass="NormalRed"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="94px" Text="<%$Resources:lblSSCode.Text %>"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSSCode" runat="server" CssClass="Normal" MaxLength="21" Width="300px"></asp:TextBox></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Button ID="btnSaveSummonsServer" runat="server" CssClass="NormalButton" Text="<%$Resources:btnSaveSummonsServer.Text %>"
                                                                        OnClick="btnUpdateSS_Click" Width="80px"></asp:Button></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlDeleteSummonsServer" runat="server" Width="100%">
                                                        <table border="0">
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Label ID="lblDeleteSummonsServer" runat="server" CssClass="Normal"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:Button ID="btnDeleteSummonsServerYes" runat="server" ForeColor="Green" OnClick="btnDeleteSummonsServerYes_Click"
                                                                        Text="<%$Resources:btnDeleteSummonsServerYes.Text %>" Width="50px" CssClass="NormalButton" /></td>
                                                                <td>
                                                                    <asp:Button ID="btnDeleteSummonsServerNo" runat="server" ForeColor="Red" OnClick="btnDeleteSummonsServerNo_Click"
                                                                        Text=" <%$Resources:btnDeleteSummonsServerNo.Text %> " Width="50px" CssClass="NormalButton" /></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    &nbsp; &nbsp;
                                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="False" />
                                                </td>
                                                <td style="width: 50%; text-align: left; vertical-align: top;">
                                                    <asp:Panel ID="pnlAuthSSCourt" Width="100%" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td class="NormalBold">
                                                                    <asp:Label ID="Label4" runat="server" Text="<%$Resources:Authority %>" />:&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlAuthority" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlAuthority_SelectedIndexChanged" CssClass="Normal" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="btnCreateAuthSSCourt" runat="server" OnClick="btnCreateAuthSSCourt_Click">New Association</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:GridView ID="grdAuthSSCourt" AutoGenerateColumns="False" AllowPaging="True"
                                                                        PageSize="15" CellPadding="4" ShowFooter="True" GridLines="Vertical"
                                                                        OnRowCommand="grdAuthSSCourt_RowCommand" 
                                                                        runat="server" ShowHeaderWhenEmpty="True" CssClass="Normal" 
                                                                        onpageindexchanging="grdAuthSSCourt_PageIndexChanging">
                                                                        <FooterStyle CssClass="CartListFooter" />
                                                                        <HeaderStyle CssClass="CartListHead" />
                                                                        <RowStyle CssClass="CartListItem" />
                                                                        <AlternatingRowStyle CssClass="CartListItemAlt" />
                                                                        <PagerStyle Font-Size="Medium" CssClass="Normal" />
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="<%$Resources:Court %>" DataField="CrtName" />
                                                                            <asp:BoundField HeaderText="<%$Resources:SummonsServer %>" DataField="SSFullName" />
                                                                            <asp:TemplateField HeaderText="<%$Resources:Action %>">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="LinkButton1" Text="<%$Resources:grdSummonsServers.HeaderText3 %>" CommandName="ASCEdit" CommandArgument='<%#Eval("ASCIntNo") %>' runat="server" />
                                                                                    |
                                                                                    <asp:LinkButton ID="LinkButton2" Text="<%$Resources:grdSummonsServers.HeaderText4 %>" CommandName="ASCDelete" CommandArgument='<%#Eval("ASCIntNo") %>' OnClientClick='if(!ConfirmDelete())return false;' runat="server" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlAuthSSCourtDetail" Visible="False" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td class="NormalBold">
                                                                    <asp:Label ID="Label6" runat="server" Text="<%$Resources:Court %>" />
                                                                    :&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlCourtDetail" runat="server" AutoPostBack="True" 
                                                                        CssClass="Normal" 
                                                                        OnSelectedIndexChanged="ddlCourtDetail_SelectedIndexChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="NormalBold">
                                                                    <asp:Label ID="Label7" runat="server" Text="<%$Resources:SummonsServer %>" />
                                                                    :&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSSDetail" runat="server" AutoPostBack="True" 
                                                                        CssClass="Normal" OnSelectedIndexChanged="ddlSSDetail_SelectedIndexChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnUpdateAuthSSCourt" runat="server" CssClass="NormalButton" Enabled="False" onclick="btnUpdateAuthSSCourt_Click" Text="Update" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Label ID="lblASCError" runat="Server" CssClass="NormalRed" EnableViewState="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="udp" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
