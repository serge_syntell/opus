﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Stalberg.TMS.Data;
using System.Data;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    public partial class LocationCameraSection : System.Web.UI.Page
    {
        private enum Display
        {
            List,
            Edit,
            Delete,
            Add
        }
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        // Protected
        protected string _styleSheet;
        protected string _background;
        protected string _keywords = String.Empty;
        protected string _title = "Location Camera Section Maintenance";
        protected string _description = String.Empty;
        protected string _thisPage = "LocationCameraSection.aspx";

        static private Display lastOperate;
        static private int LCSIntNo;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            //UserDB user = new UserDB(this.connectionString);
            UserDetails userDetails = new UserDetails();

            //int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            _background = gen.SetBackground(Session["drBackground"]);
            _styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            _title = gen.SetTitle(Session["drTitle"]);

            HtmlLink link = new HtmlLink();
            link.Href = _styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                PopulateAuthorites(autIntNo);

                this.bindData();
                bindCamera();
            }

            DateTime dt;
            if (DateTime.TryParse(txtLCSStartDate.Text, out dt))
            {
                DateCalendar1.SelectedDate = dt;
            }
            if (DateTime.TryParse(txtLCSEndDate.Text, out dt))
            {
                DateCalendar2.SelectedDate = dt;
            }
 
        }

        protected void PopulateAuthorites(int autIntNo)
        {
            AuthorityDB authority = new AuthorityDB(this.connectionString);

            SqlDataReader reader = authority.GetAuthorityList(0, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int AutIntNo = (int)reader["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //ddlSelectLA.DataSource = reader;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            reader.Close();
        }

        private void bindData()
        {
            int autIntNo = int.Parse(ddlSelectLA.SelectedValue.ToString());

            this.setPanelVisibility(Display.List);

            LocationDB locationDB = new LocationDB(this.connectionString);
            DataSet data = locationDB.GetLocationListDS(autIntNo, "", "LocCameraCode");

            this.gridLocation.DataSource = data;
            this.gridLocation.DataKeyField = "LocIntNo";
            this.gridLocation.DataBind();

            if (data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0)
            {
                //Modified by Henry 2012-3-9
                //Mulity language error message lblError.Text1 - lblError.Text12
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            else
            {
                this.lblError.Text = string.Empty;
            }

            if (hiddenID.Value.Length > 0)
                bindLcsData(Convert.ToInt32(hiddenID.Value));
        }

        private void bindCamera()
        {
            CameraDB cameraDB = new CameraDB(this.connectionString);
            DataSet ds = cameraDB.GetCameraListDS();
            ddlCamera1.DataSource = ds;
            ddlCamera2.DataSource = ds;

            ddlCamera1.DataTextField = "CamSerialNo";
            ddlCamera1.DataValueField = "CamIntNo";

            ddlCamera2.DataTextField = "CamSerialNo";
            ddlCamera2.DataValueField = "CamIntNo";

            ddlCamera1.DataBind();
            ddlCamera2.DataBind();

            ddlCamera1.Items.Insert(0, (string)GetLocalResourceObject("ddlCamera1.Items"));
            ddlCamera2.Items.Insert(0, (string)GetLocalResourceObject("ddlCamera1.Items"));
        }

        private void setPanelVisibility(Display display)
        {
            this.lblError.Text = string.Empty;

            this.panelEdit.Visible = false;
            this.panelDelete.Visible = false;
            this.pnlLocation.Visible = false;
            this.DataGridLcs.Visible = false;

            switch (display)
            {
                case Display.List:
                    this.pnlLocation.Visible = true;
                    lblLocation.Visible = false;
                    break;
                case Display.Add:
                case Display.Edit:
                    this.panelEdit.Visible = true;
                    lblLocation.Visible = true;
                    break;
                case Display.Delete:
                    this.panelDelete.Visible = true;
                    lblLocation.Visible = false;
                    break;
            }
        }

        protected void btnHideMenu_Click(object sender, EventArgs e)
        {
            if (pnlMainMenu.Visible.Equals(true))
            {
                pnlMainMenu.Visible = false;
                btnHideMenu.Text = (string)GetLocalResourceObject("btnShowMenu.Text");
            }
            else
            {
                pnlMainMenu.Visible = true;
                btnHideMenu.Text = (string)GetLocalResourceObject("btnHideMenu.Text");
            }
        }

        protected void buttonAdd_Click(object sender, EventArgs e)
        {
            if (this.hiddenID.Value.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            this.setPanelVisibility(Display.Edit);
            lastOperate = Display.Add;

            this.txtLCSSectionCode.Text = string.Empty;
            this.txtLCSSectionName.Text = string.Empty;
            this.txtLCSSectionDistance.Text = string.Empty;
            this.chkLCSActive.Text = string.Empty;
            this.chkLCSActive.Checked = true;

            this.DateCalendar1.SelectedDate = DateTime.Now;
            this.DateCalendar2.SelectedDate = DateTime.Now;

            if (ddlCamera1.Items.Count > 0)
            {
                this.ddlCamera1.SelectedIndex = 0;
                this.ddlCamera2.SelectedIndex = 0;
            }
            
            //this.hiddenID.Value = "0";
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            this.setPanelVisibility(Display.Delete);
        }

        protected void buttonSubmit_Click(object sender, EventArgs e)
        {
            int id = int.Parse(this.hiddenID.Value);
            Data.LocationCameraSection lcs = new Data.LocationCameraSection();
            lcs.LCSIntNo = LCSIntNo;
            lcs.LocIntNo = Convert.ToInt32(this.hiddenID.Value);

            lcs.LCSSectionCode = this.txtLCSSectionCode.Text.Trim();
            lcs.LCSSectionName = this.txtLCSSectionName.Text.Trim();

            if (this.txtLCSSectionDistance.Text.Trim().Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }

            lcs.LCSSectionDistance = Convert.ToInt32(this.txtLCSSectionDistance.Text.Trim());

            lcs.LCSActive = this.chkLCSActive.Checked;
            if (txtLCSStartDate.Text.Length > 0)
                lcs.LCSStartDate = DateCalendar1.SelectedDate;

            if (txtLCSEndDate.Text.Length > 0)
                lcs.LCSEndDate = DateCalendar2.SelectedDate;            

            if (string.IsNullOrEmpty(lcs.LCSSectionCode))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                return;
            }
            if (string.IsNullOrEmpty(lcs.LCSSectionName))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                return;
            }

            if (lcs.LCSEndDate.HasValue && lcs.LCSStartDate.HasValue)
            {
                if (lcs.LCSEndDate < lcs.LCSStartDate)
                {
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    return;
                }
            }

            //Added by Jacob 20121226 start
            // Convert.ToInt32 will raise exception, if the first option  'Please select...' is selected 
            // should check the selection first
            // if the first option is selected, return with error text;
            //<%$Resources:lblError.MustSelectCamera1%>
            if (this.ddlCamera1.SelectedIndex == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.MustSelectCamera1");
                return;
            }

            if (this.ddlCamera2.SelectedIndex == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.MustSelectCamera2");
                return;
            }
            //Added by Jacob 20121226 end

            lcs.CameraIntNo1 = Convert.ToInt32(this.ddlCamera1.SelectedValue);
            lcs.CameraIntNo2 = Convert.ToInt32(this.ddlCamera2.SelectedValue);

            if (lcs.CameraIntNo1 == lcs.CameraIntNo2)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                return;
            }

            this.lblError.Text = string.Empty;
            int iRe = 0;

            LocationCameraSectionDB db = new LocationCameraSectionDB(this.connectionString);

            string errMessage = string.Empty;

            if (id == 0)
            {
                //if (db.InsertLocationCameraSection(lcs, this.login, ref errMessage) == -1)
                //{
                //    this.lblError.Text = errMessage;
                //    return;
                //}

                iRe = db.InsertLocationCameraSection(lcs, this.login, ref errMessage);
                if (iRe == 0)
                {
                    this.lblError.Text = errMessage;
                    return;
                }
                else if (iRe == -1)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text13"), lcs.LCSSectionCode);
                    return;
                }
            }
            else if (lastOperate == Display.Add)
            {
                iRe = db.InsertLocationCameraSection(lcs, this.login, ref errMessage);

                if (iRe == 0)
                {
                    this.lblError.Text = errMessage;
                    return;
                }
                else if (iRe == -1)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text13"), lcs.LCSSectionCode);
                    return;
                }

            }
            else if (lastOperate == Display.Edit)
            {
                iRe = db.UpdateLocationCameraSection(lcs, this.login, ref errMessage);

                if (iRe == -1)
                {
                    this.lblError.Text = errMessage;
                    return;
                }
                else if (iRe == -2)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text13"), lcs.LCSSectionCode);
                    return;
                }
            }

            this.bindData();

            if (iRe > 0)
            {
                this.lblError.Text += (string)GetLocalResourceObject("lblError.Text8");
                if (lastOperate == Display.Add)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.LocationCameraSection, PunchAction.Add);  

                }
                if (lastOperate == Display.Edit)
                {
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.LocationCameraSection, PunchAction.Change);  

                }
            }
            else
            {
                this.lblError.Text += (string)GetLocalResourceObject("lblError.Text9");
            }
        }

        protected void gridLocation_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                int LocIntNo = Convert.ToInt32(e.Item.Cells[0].Text);
                hiddenID.Value = e.Item.Cells[0].Text;
                bindLcsData(LocIntNo);
                lblLocation.Text = e.Item.Cells[2].Text;
                lblLocation.Visible = false;
            }
        }

        private void bindLcsData(int LocIntNo)
        {
            LocationCameraSectionDB db = new LocationCameraSectionDB(this.connectionString);
            List<Data.LocationCameraSection> data = db.ListLocationCameraSections(LocIntNo);

            this.DataGridLcs.Visible = true;

            this.DataGridLcs.DataSource = data;
            this.DataGridLcs.DataKeyField = "LcsIntNo";
            this.DataGridLcs.DataBind();


            if (data.Count == 0)
            {
                this.DataGridLcs.Visible = false;
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
            }
            else
            {
                this.lblError.Text = string.Empty;
            }
        }

        protected void gridLocation_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            gridLocation.CurrentPageIndex = e.NewPageIndex;

            bindData();
        }

        protected void gridLcs_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            this.hiddenID.Value = e.Item.Cells[1].Text;

            if (e.CommandName.Equals("Select"))
            {
                this.setPanelVisibility(Display.Edit);

                LCSIntNo = Convert.ToInt32(e.Item.Cells[0].Text);
                this.txtLCSSectionCode.Text = e.Item.Cells[4].Text;
                this.txtLCSSectionName.Text = e.Item.Cells[5].Text;
                this.txtLCSSectionDistance.Text = e.Item.Cells[6].Text;
                this.chkLCSActive.Checked = Convert.ToBoolean(e.Item.Cells[7].Text);

                DateTime dt;

                if (DateTime.TryParse(e.Item.Cells[8].Text, out dt))
                    this.txtLCSStartDate.Text = Convert.ToDateTime(e.Item.Cells[8].Text).ToString("yyyy-MM-dd");
                else
                {
                    this.txtLCSStartDate.Text = string.Empty;
                    DateCalendar1.SelectedDate = null;
                }

                if (DateTime.TryParse(e.Item.Cells[9].Text, out dt))
                    this.txtLCSEndDate.Text = Convert.ToDateTime(e.Item.Cells[9].Text).ToString("yyyy-MM-dd");
                else
                {
                    this.txtLCSEndDate.Text = string.Empty;
                    DateCalendar2.SelectedDate = null;
                }

                this.ddlCamera1.SelectedIndex = ddlCamera1.Items.IndexOf(ddlCamera1.Items.FindByText(e.Item.Cells[2].Text));
                this.ddlCamera2.SelectedIndex = ddlCamera2.Items.IndexOf(ddlCamera2.Items.FindByText(e.Item.Cells[3].Text));

                lastOperate = Display.Edit;
            }
            else if (e.CommandName.Equals("Delete"))
            {
                LCSIntNo = Convert.ToInt32(e.Item.Cells[0].Text);
                this.setPanelVisibility(Display.Delete);
                this.labelConfirmDelete.Text = string.Format((string)GetLocalResourceObject("labelConfirmDelete.Text"), e.Item.Cells[1].Text);

                lastOperate = Display.Delete;
            }
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.bindData();
            bindCamera();
        }

        protected void buttonDelete_Click1(object sender, EventArgs e)
        {
            int id = LCSIntNo;
            LocationCameraSectionDB db = new LocationCameraSectionDB(this.connectionString);
            int iRe = 0;
            try
            {
                iRe = db.DeleteLocationCameraSection(id);
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.LocationCameraSection, PunchAction.Delete); 
            }
            catch (Exception ex)
            {
                this.lblError.Text = ex.Message;
            }
            this.bindData();

            if (iRe > 0)
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
            else
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text12"); 
        }

        protected void buttonList_Click(object sender, EventArgs e)
        {
            this.bindData();
        }
    }
}
