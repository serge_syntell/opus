using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Util;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Notice Post management page
    /// </summary>
    public partial class OfficerStatisticsReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "OfficerStatisticsReport.aspx";
        private const string DATE_FORMAT = "yyyy-MM-dd";
        //protected string thisPage = "Officer Statistics Report";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                //2012-3-6 linda modified into a multi-language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                PopulateAuthorities();
                PopulateOfficers();
                //2012-3-7 linda modified dropdownlist into a multi-language
                ddlReportType.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlReportType.Items1"), "PDF"));
                ddlReportType.Items.Insert(1, new ListItem((string)GetLocalResourceObject("ddlReportType.Items2"), "Excel"));
                this.dtpFrom.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString(DATE_FORMAT);
                this.dtpTo.Text = DateTime.Today.ToString(DATE_FORMAT);
            }
        }

         

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();

            //2012-3-7 linda modified dropdownlist into a multi-language
            ddlAuthority.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlAuthority.Items"), "0"));
            //reader.Close();
            ddlAuthority.SelectedIndex = 0;
        }

        private void PopulateOfficers()
        {
            TrafficOfficerDB db = new TrafficOfficerDB(this.connectionString);
            SqlDataReader reader = db.GetTrafficOfficerListByAuthority(Convert.ToInt32(ddlAuthority.SelectedValue));
            this.ddlOfficer.DataSource = reader;
            this.ddlOfficer.DataValueField = "TONo";
            this.ddlOfficer.DataTextField = "TODescr";
            this.ddlOfficer.DataBind();
            //2012-3-7 linda modified dropdownlist into a multi-language
            ddlOfficer.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlAuthority.Items"), "0"));
            ddlOfficer.SelectedIndex = 0;
            reader.Close();
        }

        protected void btnOptSearch_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = true;
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            PageToOpen pto = new PageToOpen(this, "OfficerStatistics_ReportViewer.aspx");
            if (this.ddlReportType.SelectedValue.Equals("PDF"))
                pto.AddParameter("action", "PDF");
            else
            {
                pto.AddParameter("action", "XLS");
            }

            pto.AddParameter("AutIntNo", ddlAuthority.SelectedValue);
            pto.AddParameter("OfficerNumber", ddlOfficer.SelectedValue);
            pto.AddParameter("DateFrom", this.dtpFrom.Text);
            pto.AddParameter("DateTo", this.dtpTo.Text);
            Helper_Web.BuildPopup(pto);
            lblError.Visible = true;
            //PunchStats805806 enquiry OfficerStatisticsReport
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.PopulateOfficers();
        }

    }
}
