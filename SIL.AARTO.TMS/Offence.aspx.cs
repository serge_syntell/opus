using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using Stalberg.TMS.Data;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{

    public partial class Offence : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        protected string thisPageURL = "Offence.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Offence maintenance";
        protected string isUpdate = "0";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;

            //int 
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName.ToString();
            int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddOffence.Visible = false;
                pnlUpdateOffence.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptHide.Visible = true;

                PopulateOffenceGroups(ddlSelOffenceGroup);
                //modified into a multi-language
                ddlSelOffenceGroup.Items.Insert(0, (string)GetLocalResourceObject("ddlSelOffenceGroup.Items"));

                PopulateOffenceGroups(ddlOffenceGroup);

                txtSearch.Text = string.Empty;
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            int ogIntNo = 0;
            if (ddlSelOffenceGroup.SelectedIndex > 0)
                ogIntNo = Convert.ToInt32(ddlSelOffenceGroup.SelectedValue);

            // Obtain and bind a list of all users

            Stalberg.TMS.OffenceDB offList = new Stalberg.TMS.OffenceDB(connectionString);

            //DataSet data = offList.GetOffenceListDS(ogIntNo, txtSearch.Text);
            int total = 0;
            DataSet data = offList.GetOffenceListDS(ogIntNo, txtSearch.Text, OffencePager.PageSize, OffencePager.CurrentPageIndex - 1, out total);
            dgOffence.DataSource = data;
            dgOffence.DataKeyField = "OffIntNo";
            OffencePager.RecordCount = total;


            try
            {
                dgOffence.DataBind();
            }
            catch
            {
                dgOffence.CurrentPageIndex = 0;
                dgOffence.DataBind();
            }

            if (dgOffence.Items.Count == 0)
            {
                dgOffence.Visible = false;
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgOffence.Visible = true;
            }

            data.Dispose();
            pnlAddOffence.Visible = false;
            pnlUpdateOffence.Visible = false;

            //show it
            btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            //Seawen 2013-09-13 pager visible equal to grid
            OffencePager.Visible = dgOffence.Visible;
        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            isUpdate = "0";

            // allow transactions to be added to the database table
            pnlAddOffence.Visible = true;
            pnlUpdateOffence.Visible = false;

            btnOptDelete.Visible = false;

            PopulateOffenceGroups(ddlAddOffenceGroup);
            //2012-3-7 linda modified dropdownlist into multi-language
            ddlAddOffenceGroup.Items.Insert(0, (string)GetLocalResourceObject("ddlAddOffenceGroup.Items"));

            ddlAddOffenceCodeList.Items.Insert(0, (string)GetLocalResourceObject("ddlAddOffenceCodeList.Items"));

            txtAddOffRangeStart.Text = "0";
            txtAddOffRangeEnd.Text = "0";

            txtAddOffCode.Text = string.Empty;
            txtAddOffDescr.Text = string.Empty;
            chkOffNoAOG.Checked = false;
            chkIsByLaw.Checked = false;

            if (ddlSelOffenceGroup.SelectedIndex > 0)
                ddlAddOffenceGroup.SelectedIndex = ddlAddOffenceGroup.Items.IndexOf(ddlAddOffenceGroup.Items.FindByValue(ddlSelOffenceGroup.SelectedValue));


            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
        }

        protected void PopulateOffenceGroups(DropDownList ddlSelOffenceGroups)
        {
            OffenceGroupDB group = new OffenceGroupDB(connectionString);

            SqlDataReader reader = group.GetOffenceGroupList("");

            Dictionary<int, string> lookups =
                OffenceGroupLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int oGIntNo = (int)reader["OGIntNo"];
                string ogCode = reader["OGCode"].ToString();
                if (lookups.ContainsKey(oGIntNo))
                {
                    ddlSelOffenceGroups.Items.Add(new ListItem(ogCode + " ~ " + lookups[oGIntNo], oGIntNo.ToString()));
                }
            }
            //ddlSelOffenceGroups.DataSource = reader;
            //ddlSelOffenceGroups.DataValueField = "OGIntNo";
            //ddlSelOffenceGroups.DataTextField = "OffenceGroupName";
            //ddlSelOffenceGroups.DataBind();
            reader.Close();
        }

        protected void PopulateOffences(DropDownList ddlOffenceCode, DropDownList ddlOffenceGroup)
        {
            int ogIntNo = 0;
            //if (ddlOffenceGroup.SelectedIndex > 0)
            ogIntNo = Convert.ToInt32(ddlOffenceGroup.SelectedValue);

            if (ogIntNo > 0)
            {

                OffenceDB offence = new OffenceDB(connectionString);

                SqlDataReader reader = offence.GetOffenceList(ogIntNo, "");
                ddlOffenceCode.DataSource = reader;
                ddlOffenceCode.DataValueField = "OffCode";
                ddlOffenceCode.DataTextField = "OffenceName";
                ddlOffenceCode.DataBind();

                reader.Close();

                if (isUpdate == "1")
                {
                    ddlOffenceCode.Items.Remove(ddlOffenceCode.Items.FindByValue(txtOffCode.Text.Trim()));
                }
            }
            //2012-3-7 linda modified dropdownlist into multi-language
            ddlOffenceCode.Items.Insert(0, (string)GetLocalResourceObject("ddlAddOffenceCodeList.Items"));


        }


        protected void btnAddOffence_Click(object sender, System.EventArgs e)
        {
            isUpdate = "0";

            if (ddlAddOffenceGroup.SelectedIndex < 1)
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            int ogIntNo = Convert.ToInt32(ddlAddOffenceGroup.SelectedValue);

            // add the new transaction to the database table
            Stalberg.TMS.OffenceDB toAdd = new OffenceDB(connectionString);

            string noAOG = "N"; bool isS35NoAOG = false;
            bool offHasMandatoryAlternate = false;

            if (chkAddOffNoAOG.Checked) noAOG = "Y";
            // Jake 2013-11-27 added section35 no aog
            if (chkAddS35NoAoG.Checked) isS35NoAOG = true;
            if (chkAddOffHasMandatoryAlternate.Checked)
            {
                offHasMandatoryAlternate = true;

                if (ddlAddOffenceCodeList.SelectedIndex < 1)
                {
                    lblError.Visible = true;
                    //Modefied By Linda 2012-2-27
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    return;
                }
            }

            // 2010/9/29 jerry add new column into Offence table
            int addOffIntNo = toAdd.AddOffence(ogIntNo,
                txtAddOffCode.Text, txtAddOffDescr.Text,
                Convert.ToInt32(txtAddOffRangeStart.Text),
                Convert.ToInt32(txtAddOffRangeEnd.Text), noAOG, loginUser,
                ddlAddOffenceCodeList.SelectedValue, offHasMandatoryAlternate, chkIsByLaw.Checked, this.chkIsPOD.Checked, //2013-05-06 add chkIsByLaw.Checked by Henry for by-law
                this.txtLongDescEng.Text, this.txtLongDescAfr.Text,
                isS35NoAOG
                );

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.UpdateIntoLookup(addOffIntNo, lgEntityList, "OffenceLookup", "OffIntNo", "OffDescr", this.loginUser);

            if (addOffIntNo <= 0)
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
            }
            else
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
 				//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffencesMaintenance, PunchAction.Add);  
            }

            lblError.Visible = true;

            BindGrid();
        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            int offIntNo = Convert.ToInt32(Session["editOffIntNo"]);

            int ogIntNo = Convert.ToInt32(ddlOffenceGroup.SelectedValue);

            string noAOG = "N";
            bool isS35NoAOG = false;

            if (chkOffNoAOG.Checked) noAOG = "Y";
            // Jake 2013-11-27 added
            if (chkS35NoAoG.Checked) isS35NoAOG = true;
            //2010-9-30 jerry changed
            bool offHasMandatoryAlternate = false;
            if (chkOffHasMandatoryAlternate.Checked)
            {
                offHasMandatoryAlternate = true;

                if (ddlOffenceCodeList.SelectedIndex < 1)
                {
                    lblError.Visible = true;
                    //Modefied By Linda 2012-2-27
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                    return;
                }
            }

            // add the new transaction to the database table
            Stalberg.TMS.OffenceDB offUpdate = new OffenceDB(connectionString);

            // 2013-05-06 add chkIsByLawUpdate.Checked by Henry for by-law
            int updOffIntNo = offUpdate.UpdateOffence(offIntNo, ogIntNo, txtOffCode.Text, txtOffDescr.Text,
                Convert.ToInt32(txtOffRangeStart.Text), Convert.ToInt32(txtOffRangeEnd.Text), noAOG, loginUser,
                ddlOffenceCodeList.SelectedValue, offHasMandatoryAlternate, chkIsByLawUpdate.Checked,
                this.chkIsPODUpdate.Checked, this.txtLongDescEngUpdate.Text, this.txtLongDescAfrUpdate.Text, isS35NoAOG);

            if (updOffIntNo <= 0)
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
            }
            else
            {
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.UpdateIntoLookup(updOffIntNo, lgEntityList, "OffenceLookup", "OffIntNo", "OffDescr", this.loginUser);
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
				//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffencesMaintenance, PunchAction.Change);  
            }

            lblError.Visible = true;

            BindGrid();
        }

        protected void ShowOffenceDetails(int editOffIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.OffenceDB off = new Stalberg.TMS.OffenceDB(connectionString);

            Stalberg.TMS.OffenceDetails offDetails = off.GetOffenceDetails(editOffIntNo);

            txtOffCode.Text = offDetails.OffCode;
            txtOffDescr.Text = offDetails.OffDescr;
            txtOffRangeStart.Text = offDetails.OffRangeStart.ToString();
            txtOffRangeEnd.Text = offDetails.OffRangeEnd.ToString();
            chkIsByLawUpdate.Checked = offDetails.IsByLaw;

            if (offDetails.OffNoAOG.Equals("Y"))
                chkOffNoAOG.Checked = true;
            else
                chkOffNoAOG.Checked = false;

            chkS35NoAoG.Checked = offDetails.IsS35NoAOG;

            if (offDetails.OffHasMandatoryAlternate)
            {
                chkOffHasMandatoryAlternate.Checked = true;
            }
            else
            {
                chkOffHasMandatoryAlternate.Checked = false;
            }

            // Oscar 20120813 added for PresentationOfDocument
            this.chkIsPODUpdate.Checked = offDetails.IsPresentationOfDocument;

            ddlOffenceGroup.SelectedIndex = ddlOffenceGroup.Items.IndexOf(ddlOffenceGroup.Items.FindByValue(offDetails.OGIntNo.ToString()));

            PopulateOffences(ddlOffenceCodeList, ddlOffenceGroup);
            ddlOffenceCodeList.SelectedIndex = ddlOffenceCodeList.Items.IndexOf(ddlOffenceCodeList.Items.FindByValue(offDetails.OffCodeAlternate));

            // Oscar 20130109 added
            this.txtLongDescEngUpdate.Text = offDetails.LongDescriptionEng;
            this.txtLongDescAfrUpdate.Text = offDetails.LongDescriptionAfr;

            pnlUpdateOffence.Visible = true;
            pnlAddOffence.Visible = false;

            btnOptDelete.Visible = true;
        }


        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
            isUpdate = "0";

            int offIntNo = Convert.ToInt32(Session["editOffIntNo"]);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "OffenceLookup", "OffIntNo");

            OffenceDB off = new Stalberg.TMS.OffenceDB(connectionString);

            string delOffIntNo = off.DeleteOffence(offIntNo);

            if (delOffIntNo.Equals("0"))
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            }
            else if (string.IsNullOrEmpty(delOffIntNo) || delOffIntNo.Equals("-1"))
            {
                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
            }
            else
            {

                //Modefied By Linda 2012-2-27
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.OffencesMaintenance, PunchAction.Delete); 

                //Seawen 2013-09-13 last page last row
                if (dgOffence.Items.Count == 1)
                    OffencePager.CurrentPageIndex--;
            }
            lblError.Visible = true;

            BindGrid();
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            isUpdate = "0";

            if (dgOffence.Visible.Equals(true))
            {
                //hide it
                dgOffence.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
            }
            else
            {
                //show it
                dgOffence.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
            OffencePager.Visible = dgOffence.Visible;
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            isUpdate = "0";
            //PunchStats805806 enquiry Offences
            BindGrid();
        }


        protected void dgOffence_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            isUpdate = "1";

            //store details of page in case of re-direct
            dgOffence.SelectedIndex = e.Item.ItemIndex;

            if (dgOffence.SelectedIndex > -1)
            {
                int editOffIntNo = Convert.ToInt32(dgOffence.DataKeys[dgOffence.SelectedIndex]);

                Session["editOffIntNo"] = editOffIntNo;
                Session["prevPage"] = thisPageURL;

                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editOffIntNo.ToString(), "OffenceLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                ShowOffenceDetails(editOffIntNo);
            }
        }

        protected void ddlSelOffenceGroup_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            BindGrid();
        }



        protected void ddlAddOffenceGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlAddOffenceCodeList.Items.Clear();
            PopulateOffences(ddlAddOffenceCodeList, ddlAddOffenceGroup);
        }

        protected void ddlOffenceGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlOffenceCodeList.Items.Clear();
            ddlOffenceCodeList.Items.Insert(0, (string)GetLocalResourceObject("ddlOffenceCodeList.Items"));
            PopulateOffences(ddlOffenceCodeList, ddlOffenceGroup);
        }

        protected void OffencePager_PageChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}
