<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ReceiptReversal" Codebehind="ReceiptReversal.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0" onload="self.focus();"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="6000">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                </td>
                <td valign="top" align="center" width="100%" colspan="1">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
                    <table height="482" width="100%" border="0">
                        <tr>
                            <td valign="top" style="height: 47px">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" CssClass="ContentHead" Width="579px" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>&nbsp
                                  </p>
                                <p>
                                    &nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="panelGeneral" runat="server" Width="100%">
                                    <asp:GridView ID="grdReceipt" runat="server" AlternatingRowStyle-CssClass="CartListItemAlt"
                                        RowStyle-CssClass="CartListItem" FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead"
                                        AutoGenerateColumns="False" CssClass="Normal" ShowFooter="True" OnSelectedIndexChanged="grdReceipt_SelectedIndexChanged" OnRowCreated="grdReceipt_RowCreated">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingRowStyle CssClass="CartListItemAlt"></AlternatingRowStyle>
                                        <RowStyle CssClass="CartListItem"></RowStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOffenceDate" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText1 %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRctNumber" runat="server" Text='<%# Bind("RctNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText2 %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblReceiptDate" runat="server" Text='<%# Bind("RctDate", "{0:yyyy-MM-dd}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="RctUser" HeaderText="<%$Resources:grdReceipt.HeaderText3 %>" />
                                            <asp:BoundField DataField="RctDetails" HeaderText="<%$Resources:grdReceipt.HeaderText4 %>" />
                                            <asp:BoundField DataField="ReceivedFrom" HeaderText="<%$Resources:grdReceipt.HeaderText5 %>" />
                                            <asp:BoundField DataField="ContactNumber" HeaderText="<%$Resources:grdReceipt.HeaderText6 %>" />
                                            <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText7 %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCashType" runat="server" Text='<%# (Eval("CashType")).ToString().Length > 0 ? ((Stalberg.TMS.CashType)(Eval("CashType")).ToString()[0]).ToString() : "" %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Bind("Total") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText8 %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTicketNo" runat="server" Text='<%# Bind("NotTicketNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$Resources:grdReceipt.HeaderText9 %>">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("RTAmount", "{0:0.00}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RctIntNo" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRctIntNo" runat="server" Text='<%# Bind("RctIntNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NotIntNo" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNotIntNo" runat="server" Text='<%# Bind("NotIntNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <pager:AspNetPager id="grdReceiptPager" runat="server" 
                                            showcustominfosection="Right" width="608px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"   PageSize="10" onpagechanged="grdReceiptPager_PageChanged"  UpdatePanelId="UpdatePanel1"
                                    ></pager:AspNetPager>

                                    <asp:Button ID="btnProceed" runat="server" CssClass="NormalButton" OnClick="btnProceed_Click"
                                        Text="<%$Resources:btnProceed.Text %>" />
                                </asp:Panel>
                                <asp:Panel ID="pnlConfirm" runat="server" CssClass="Normal" AlternatingRowStyle-CssClass="CartListItemAlt"
                                    RowStyle-CssClass="CartListItem" FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead"
                                    Width="100%">
                                    &nbsp; &nbsp;<asp:GridView ID="grdCashBox" runat="server" AutoGenerateColumns="False"
                                        CssClass="Normal" ShowFooter="True" OnSelectedIndexChanged="grdCashBox_SelectedIndexChanged">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingRowStyle CssClass="CartListItemAlt"></AlternatingRowStyle>
                                        <RowStyle CssClass="CartListItem"></RowStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundField DataField="CBName" HeaderText="<%$Resources:grdCashBox.HeaderText %>" />
                                            <asp:BoundField DataField="CBStartAmount" HeaderText="<%$Resources:grdCashBox.HeaderText1 %>" />
                                            <asp:CommandField HeaderText="<%$Resources:grdCashBox.HeaderText2 %>" 
                                                ShowSelectButton="True" SelectText="<%$Resources:grdCashBox.HeaderText2 %>">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:CommandField>
                                        </Columns>
                                    </asp:GridView>
                                    <pager:AspNetPager id="grdCashBoxPager" runat="server" 
                                            showcustominfosection="Right" width="400px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"  PageSize="10" onpagechanged="grdCashBoxPager_PageChanged" UpdatePanelId="UpdatePanel1"
                                    ></pager:AspNetPager>
                                </asp:Panel>
                                <asp:Panel ID="pnlConfirm2" runat="server" Width="100%" CssClass="Normal">
                                    <asp:Panel ID="pnlConfirmCashbox" runat="server" CssClass="Normal" Width="100%">
                                        <table border="0" class="Normal">
                                            <tr>
                                                <td class="NormalBold" valign="middle">
                                                    &nbsp;</td>
                                                <td valign="middle">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="NormalBold" valign="middle">
                                    <asp:Label ID="lblSelectedCashbox" runat="server" CssClass="NormalBold"></asp:Label></td>
                                                <td valign="middle">
                                                    <asp:CheckBox ID="chkAreYouSure" runat="server" AutoPostBack="True" OnCheckedChanged="chkAreYouSure_CheckedChanged" CssClass="NormalRed" Text="<%$Resources:chkAreYouSure.Text %>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="NormalBold">
                                                    &nbsp;</td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="NormalBold">
                                                    <asp:Label ID="Label1" runat="server" Text="<%$Resources:lblAmount.Text %>"></asp:Label> &nbsp;&nbsp;
                                                    <asp:TextBox ID="txtCashAmount" runat="server" CssClass="Normal" Width="99px" ReadOnly="True"></asp:TextBox></td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="btnConfirm" runat="server" OnClick="btnConfirm_Click" Text="<%$Resources:btnConfirm.Text %>"
                                                        Width="87px" CssClass="NormalButton" /></td>
                                                <td align="left">
                                                    </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    &nbsp; &nbsp;
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
                </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label2"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
