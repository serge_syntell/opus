﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using SIL.AARTO.BLL.Utility;

namespace Stalberg.TMS
{
    /// <summary>
    /// 20090420 tf  ---add user control for enquiry function, merge user controlss of notice ticket lookup and easypay lookup.
    /// </summary>
    public partial class NoticeNoLookup : System.Web.UI.UserControl
    {
        private string connectionString = String.Empty;
        private string login;
        private string sReturn = String.Empty;
        private int nAutIntNo;
        private string ticketProcessor;// jerry 2011-1-13 add
        private int processorSign;// jerry 2011-1-13 add

        private string prefix;// Edge 2012-11-09 add
        private string sequence;// Edge 2012-11-09 add

        public event EventHandler NoticeSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            if (this.ViewState["TicketNumberSearch_AutIntNo"] != null)
            {
                this.nAutIntNo = (int)this.ViewState["TicketNumberSearch_AutIntNo"];
            }

            // jerry 2011-1-13 add            
            ticketProcessor = Session["TicketProcessor"] != null ? Session["TicketProcessor"].ToString() : "TMS";
            //if (ticketProcessor.Equals("TMS") || ticketProcessor.Equals("Cip_CofCT") || ticketProcessor.Equals("CiprusPI")) 2012-12-24 updated by Nancy
            if (ticketProcessor.Equals("TMS"))
            {
                processorSign = 1;
            }
            else
            {
                processorSign = 2;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            sReturn = string.Empty;
            this.prefix = string.Empty;
            this.sequence = string.Empty;
            SqlDataReader read = null;
            if (LookupCondition.Value.Contains("TickSequenceNo"))
            {
                read = Search_Ticket();
            }
            else
            {
                read = Search_EasyPay();
            }

            if (read == null)
            {
                this.NoticeSelected(string.Empty, EventArgs.Empty);
                return;
            }

            this.grdHeader.DataSource = read;

            this.grdHeader.DataBind();
            if (grdHeader.Rows.Count == 1)
            {
                grdHeader.SelectedIndex = 0;
                this.sReturn = grdHeader.SelectedRow.Cells[0].Text;

                string ticketNo = grdHeader.DataKeys[grdHeader.SelectedIndex].Values[0].ToString().Trim();
                if (ticketNo.IndexOf("/") > 0)
                {
                    this.prefix = ticketNo.Split('/')[0];
                    this.sequence = ticketNo.Split('/')[1];
                }

                if (this.NoticeSelected != null)
                {
                    this.NoticeSelected(this.sReturn, EventArgs.Empty);
                }
                this.pnlGrid.Visible = false;
                this.lblError.Visible = false;
            }
            else if (grdHeader.Rows.Count > 1)
            {
                this.pnlGrid.Visible = true;
                this.lblError.Visible = false;
            }
            else
            {
                this.sReturn = txtNoticePrefix.Text.Trim() + txtNumber.Text.Trim() + txtAuthCode.Text.Trim() + txtCDV.Text.Trim();
                this.prefix = txtNoticePrefix.Text.Trim();
                this.sequence = txtNumber.Text.Trim();

                this.pnlGrid.Visible = false;
                this.lblError.Visible = true;
                this.txtNumber.Text = string.Empty;

                this.NoticeSelected(this.sReturn, EventArgs.Empty);
            }
        }

        private SqlDataReader Search_Ticket()
        {
            this.lblError.Text = string.Empty;
            char cTemp = Convert.ToChar("0");

            int nCdv;
            int nSeq;
            String sNoticeNumber = string.Empty;

            NoticeDB ndb = new NoticeDB(connectionString);
            SqlDataReader reader = null;

            if (this.txtNoticePrefix.Text.Trim().Length == 0)
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                this.txtNoticePrefix.Focus();
                return null;
            }

            if (processorSign == 1)//processor is TMS
            {
                // TMS Ticket Processors
                // Order of text boxes:
                //  1.	1st text box = Notice Prefix/Document Type  blank, user must enter (might be hidden if AuthRule 4905 Rule to hide prefix in Ticket Search = ‘Y’)
                //  2.	2nd text box = Sequence no  blank, user must enter
                //  3.	3rd text box = Authority.AutNo for current Authority (pre-populate)

                if (txtNumber.Text.Trim().Length == 0)
                {
                    //2012-3-6 linda modified into a multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1") + ticketProcessor + (string)GetLocalResourceObject("lblError.Text2");
                    txtNumber.Focus();
                    return null;
                }

                if (this.txtAuthCode.Text.Trim().Length == 0)
                {
                    //2012-3-6 linda modified into a multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1") + ticketProcessor + (string)GetLocalResourceObject("lblError.Text3");
                    txtAuthCode.Focus();
                    return null;
                }

                //Jerry 2012-08-21 add
                AuthorityRulesDetails rule9300 = new AuthorityRulesDetails();
                rule9300.AutIntNo = nAutIntNo;
                rule9300.ARCode = "9300";
                rule9300.LastUser = login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule9300, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();
                //This is a problem because the Handwritten offences use 00000 numbers for Sequence Number
                txtNumber.Text = txtNumber.Text.PadLeft(value.Key, cTemp);

                if (!int.TryParse(txtNumber.Text.Trim(), out nSeq))
                {
                    //2012-3-6 linda modified into a multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                    txtNumber.Focus();
                    return null;
                }

                nCdv = SIL.AARTO.BLL.Utility.TicketNumber.GetCdvOfTicketNumber(nSeq, txtNoticePrefix.Text.Trim(), txtAuthCode.Text.Trim());
                if (nCdv == -1)
                {
                    //2012-3-6 linda modified into a multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                    txtNoticePrefix.Focus();
                    return null;
                }

                this.txtCDV.Text = nCdv.ToString();

                sNoticeNumber = txtNoticePrefix.Text.Trim() + "/" + txtNumber.Text.Trim() + "/" + txtAuthCode.Text.Trim() + "/" + this.txtCDV.Text;

                //reader = ndb.TicketNumberSearch(this.nAutIntNo, processorSign, nSeq, txtAuthCode.Text.Trim(), txtNoticePrefix.Text.Trim(), 0);
                reader = ndb.TicketNumberSearch(processorSign, nSeq, txtAuthCode.Text.Trim(), txtNoticePrefix.Text.Trim(), 0);

            }
            else// processor is JMPD
            {
                // PLEASE NOTE: for the AARTO Ticket Processor
                //	Order of text boxes is different!!!!
                //  1.	1st text box = Document type  blank, user must enter (always show)
                //  2.	2nd text box = Authority.ENatisAuthorityNumber for current Authority (pre-populate)
                //  3.	3rd text box = sequence no  blank, user must enter
                //	Instead of AutNo, we need to find ENatisAuthorityNumber for current Authority

                if (txtNoticePrefix.Text.Trim().Length == 0)
                {
                    //2012-3-6 linda modified into a multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1") + ticketProcessor + (string)GetLocalResourceObject("lblError.Text6");
                    txtNumber.Focus();
                    return null;
                }

                if (txtNumber.Text.Trim().Length == 0)
                {
                    //2012-3-6 linda modified into a multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1") + ticketProcessor + (string)GetLocalResourceObject("lblError.Text7");
                    txtNumber.Focus();
                    return null;
                }

                if (this.txtAuthCode.Text.Trim().Length == 0)
                {
                    //2012-3-6 linda modified into a multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1") + ticketProcessor + (string)GetLocalResourceObject("lblError.Text8");
                    txtAuthCode.Focus();
                    return null;
                }

                //txtAuthCode.Text = txtAuthCode.Text.PadLeft(5, cTemp);
                txtAuthCode.Text = txtAuthCode.Text.PadLeft(9, cTemp);

                if (!int.TryParse(txtAuthCode.Text.Trim(), out nSeq))
                {
                    //2012-3-6 linda modified into a multi-language
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                    txtAuthCode.Focus();
                    return null;
                }

                sNoticeNumber = txtNoticePrefix.Text.Trim() + "-" + txtNumber.Text.Trim() + "-" + txtAuthCode.Text.Trim() + "-";
                //sNoticeNumber = txtNoticePrefix.Text.Trim() + "-" + txtAuthCode.Text.Trim() + "-" + txtNumber.Text.Trim() + "-";//2012-12-24 updated by Nancy(JMPD number format = prefix-authority-sequence-CDV)
                string tmpStr = sNoticeNumber.Replace("-", "");
                nCdv = Verhoeff.CalculateCheckDigit(tmpStr);
                this.txtCDV.Text = nCdv.ToString();
                sNoticeNumber += this.txtCDV.Text;

                //reader = ndb.TicketNumberSearch(this.nAutIntNo, processorSign, nSeq, "", txtNoticePrefix.Text.Trim(), Convert.ToInt32(txtNumber.Text.Trim()));
                reader = ndb.TicketNumberSearch(processorSign, nSeq, "", txtNoticePrefix.Text.Trim(), Convert.ToInt32(txtNumber.Text.Trim()));

            }
            //2012-3-6 linda modified into a multi-language
            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9") + sNoticeNumber + (string)GetLocalResourceObject("lblError.Text10");
            return reader;
        }

        private SqlDataReader Search_EasyPay()
        {
            char cTemp = Convert.ToChar("0");

            if (txtEasyPayNo.Text.Trim().Length < 1)
                return null;

            this.lblError.Text = string.Empty;
            SqlDataReader reader = EasyPaySearch(this.nAutIntNo, txtEasyPayNo.Text);
            //2012-3-6 linda modified into a multi-language
            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11") + txtEasyPayNo.Text + (string)GetLocalResourceObject("lblError.Text10");
            return reader;
        }

        public string Return
        {
            get { return sReturn; }
        }

        public int AutIntNo
        {
            get { return nAutIntNo; }
            set
            {
                this.nAutIntNo = value;
                this.ViewState.Add("TicketNumberSearch_AutIntNo", value);
                DataBind();
            }
        }

        public string Prefix
        {
            get { return this.prefix; }
        }

        public string Sequence
        {
            get { return this.sequence; }
        }

        public override void DataBind()
        {
            this.connectionString = Application["constr"].ToString();

            // jerry 2011-1-13 add
            //2012-12-21 Removed by Nancy
            //ticketProcessor = Session["TicketProcessor"] != null ? Session["TicketProcessor"].ToString() : "TMS";
            //if (ticketProcessor.Equals("TMS") || ticketProcessor.Equals("Cip_CofCT") || ticketProcessor.Equals("CiprusPI"))
            //{
            //    processorSign = 1;
            //}
            //else
            //{
            //    processorSign = 2;
            //}
            ticketProcessor = "TMS";//2012-12-21 updated by Nancy
            processorSign = 1;



            if (!this.Page.IsPostBack)
            {
                UserDetails userDetails = (UserDetails)Session["userDetails"];
                this.login = userDetails.UserLoginName;
                
                AuthorityDB db = new AuthorityDB(this.connectionString);
                AuthorityDetails ad = db.GetAuthorityDetails(nAutIntNo);
                //this.txtAuthCode.Text = ad.AutNo.Trim();
                if (processorSign == 1)// jerry 2011-1-13 add, processor is TMS
                {
                    this.txtAuthCode.Text = ad.AutNo;
                }
                else// processor is AARTO
                {
                    this.txtNumber.Text = ad.ENatisAuthorityNumber.ToString();
                }
                
                NoticeTypeDB noticeType = new NoticeTypeDB(this.connectionString);
                Stalberg.TMS.NoticeTypeDetails ntDetails = noticeType.GetNoticeTypeDetailsByCode("6");
                NoticePrefixDB noticePrefix = new NoticePrefixDB(connectionString);
                
                //AuthorityRulesDB dbar = new AuthorityRulesDB(this.connectionString);
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4905";
                //rule.ARDescr = "Rule to hide prefix in Ticket Search";
                //rule.ARComment = "N = No (Default), Y = Yes";
                //rule.ARNumeric = 0;
                //rule.ARString = "N";
                //rule.AutIntNo = nAutIntNo;
                //rule.LastUser = login;
                //dbar.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.ARCode = "4905";
                rule.AutIntNo = nAutIntNo;
                rule.LastUser = login;

                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value1 = authRule.SetDefaultAuthRule();

                Stalberg.TMS.NoticePrefixDetails npDetails = noticePrefix.GetNoticePrefixDetailsByType(ntDetails.NTIntNo, nAutIntNo);
                if (value1.Value == "N")
                    this.txtNoticePrefix.Text = !string.IsNullOrEmpty(npDetails.NPrefix) ? npDetails.NPrefix : "";
            }

            this.txtNoticePrefix.Focus();
        }

        protected void grdHeader_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            grdHeader.SelectedIndex = e.NewSelectedIndex;
            string ticketNo = grdHeader.DataKeys[e.NewSelectedIndex].Values[0].ToString();
            this.sReturn = grdHeader.SelectedRow.Cells[0].Text;
            this.prefix = string.Empty;
            this.sequence = string.Empty;
            if (ticketNo.IndexOf("/") > 0)
            {
                this.prefix = ticketNo.Split('/')[0];
                this.sequence = ticketNo.Split('/')[1];
            }

            if (this.NoticeSelected != null)
            {
                this.NoticeSelected(this.sReturn, EventArgs.Empty);
            }
            this.pnlGrid.Visible = false;
        }

        /// <summary>
        /// Gets the new offender authorities.
        /// </summary>
        /// <returns></returns>
        public SqlDataReader EasyPaySearch(int nAutIntNo, string sNumber)
        {
            SqlConnection con = new SqlConnection(this.connectionString);
            SqlCommand com = new SqlCommand("EasyPayNumberSearch", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = nAutIntNo;
            com.Parameters.Add("@EasyPayNo", SqlDbType.VarChar, 50).Value = sNumber;

            con.Open();
            return com.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }
}