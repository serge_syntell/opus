<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.CameraUnit" Codebehind="CameraUnit.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img src="images/1x1.gif" width="167" style="height: 1px">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" Text="<%$Resources:lblOptions.Text %>" CssClass="ProductListHead"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                &nbsp;<asp:Panel ID="pnlGeneral" runat="server">
                                    <table id="Table5" border="0" cellpadding="1" cellspacing="1" width="300">
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                            <td style="width: 31px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSelAuthority.Text %>" Width="136px"></asp:Label></td>
                                            <td width="7">
                                                <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                    OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="250px">
                                                </asp:DropDownList></td>
                                            <td style="width: 31px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                            <td style="width: 31px">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:DataGrid ID="dgCameraUnit" Width="495px" runat="server" BorderColor="Black"
                                    AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgCameraUnit_ItemCommand"
                                    OnPageIndexChanged="dgCameraUnit_PageIndexChanged" PageSize="20">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="CamUnitID" HeaderText="<%$Resources:dgCameraUnit.CamUnitID.HeaderText %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="AutNo" HeaderText="<%$Resources:dgCameraUnit.AutNo.HeaderText %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ActiveText" HeaderText="<%$Resources:dgCameraUnit.ActiveText.HeaderText %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgCameraUnit.Select.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <asp:Panel ID="pnlAddCameraUnit" runat="server" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAddCameraUnit" runat="server" Text="<%$Resources:lblAddCameraUnit.Text%>" CssClass="ProductListHead"></asp:Label></td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCameraUnit.Text%>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtAddCamUnitID" runat="server" Width="204px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="18"></asp:TextBox></td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddCameraUnit" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddCameraUnit.Text%>"
                                                    OnClick="btnAddCameraUnit_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateCameraUnit" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td style="height: 1px">
                                                <asp:Label ID="Label3" Text="<%$Resources:lblUpdateCameraUnit.Text%>" runat="server" CssClass="ProductListHead" Width="139px"></asp:Label></td>
                                            <td style="height: 1px">
                                            </td>
                                            <td style="height: 1px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" style="height: 25px">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCameraUnit.Text%>"></asp:Label></td>
                                            <td valign="top" width="248" style="height: 25px">
                                                <asp:TextBox ID="txtCamUnitID" runat="server" Width="204px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="18" ReadOnly="True"></asp:TextBox></td>
                                            <td style="height: 25px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                                <asp:CheckBox ID="chkActive" runat="server" CssClass="Normal" Text="<%$Resources:dgCameraUnit.ActiveText.HeaderText %>" /></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text%>"
                                                    OnClick="btnUpdate_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
