<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.OffenceType" Codebehind="OffenceType.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" height="21">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <asp:DataGrid ID="dgOffenceType" Width="495px" runat="server" BorderColor="Black"
                                        AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                        FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                        Font-Name="Verdana" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgOffenceType_ItemCommand"
                                        OnPageIndexChanged="dgOffenceType_PageIndexChanged" CssClass="Normal">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="OcTIntNo" HeaderText="OcTIntNo"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="OcTCode" HeaderText="<%$Resources:dgOffenceType.HeaderText1 %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="OcType" HeaderText="<%$Resources:dgOffenceType.HeaderText2 %>"></asp:BoundColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgOffenceTypeItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                    </asp:DataGrid>
                                </asp:Panel>
                                <asp:Panel ID="pnlAddOffenceType" runat="server" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="lblAddOffenceType" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddOffenceType.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="lblAddOcTCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddOcTCode.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddOcTCode" runat="server" Width="36px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="2"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="258px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtAddOcTCode" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOcType.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtAddOcType" runat="server" Width="201px" CssClass="Normal" 
                                                    MaxLength="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Width="258px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtAddOcType" ErrorMessage="<%$Resources:ReqName.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblAddRdTDescr" runat="server" Width="135px" CssClass="NormalBold" Text="<%$Resources:lblAddRdTDescr.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddOcTDescr" runat="server" Width="500px" CssClass="Normal" Height="57px"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="Label3" runat="server" Width="149px" CssClass="NormalBold" Text="<%$Resources:lblAddOcTDescr1.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddOcTDescr1" runat="server" Width="500px" CssClass="Normal"
                                                    Height="57px" TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                         <tr>
                                            <td valign="top" width="232" height="25">
                                                <asp:Label ID="Label9" runat="server" Width="208px" CssClass="NormalBold" Text="<%$Resources:lblOctEngShortDescr.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddOcTEngShortDescr" runat="server" Width="489px" 
                                                    CssClass="Normal" Height="25px"
                                                    TextMode="SingleLine" MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="232" height="25">
                                                <asp:Label ID="Label10" runat="server" Width="191px" CssClass="NormalBold" Text="<%$Resources:lblAddOcTAfrShortDescr.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddOcTAfrShortDescr" runat="server" Width="489px" 
                                                    CssClass="Normal" Height="21px"
                                                    TextMode="SingleLine" MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblAddOcTDescriptionEng" runat="server" Width="135px" CssClass="NormalBold" 
Text="<%$Resources:lblAddOcTDescriptionEng.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddOcTDescriptionEng" runat="server" Width="500px" CssClass="Normal" Height="57px"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblAddOcTDescriptionAfr" runat="server" Width="135px" CssClass="NormalBold" 
Text="<%$Resources:lblAddOcTDescriptionAfr.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtAddOcTDescriptionAfr" runat="server" Width="500px" CssClass="Normal" Height="57px"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                                <asp:Button ID="btnAddOffenceType" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddOffenceType.Text %>"
                                                    OnClick="btnAddOffenceType_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateOffenceType" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="232" height="2">
                                                <asp:Label ID="Label19" runat="server" Width="185px" CssClass="ProductListHead" Text="<%$Resources:lblUpdate.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="232" height="37">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddOcTCode.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="37">
                                                <asp:TextBox ID="txtOcTCode" runat="server" Width="36px" CssClass="NormalMand" Height="24px"
                                                    MaxLength="2" ReadOnly="True"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="271px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtOcTCode" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="232" height="25">
                                                <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblOcType.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtOcType" runat="server" Width="193px" CssClass="Normal" 
                                                    MaxLength="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Width="273px"
                                                    CssClass="NormalRed" ForeColor=" " ControlToValidate="txtAddOcType" ErrorMessage="<%$Resources:ReqName.ErrorMessage %>"
                                                    Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="232" height="25">
                                                <asp:Label ID="Label5" runat="server" Width="135px" CssClass="NormalBold" Text="<%$Resources:lblAddRdTDescr.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtOcTDescr" runat="server" Width="489px" CssClass="Normal" Height="53px"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="232" height="25">
                                                <asp:Label ID="Label6" runat="server" Width="149px" CssClass="NormalBold" Text="<%$Resources:lblAddOcTDescr1.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtOcTDescr1" runat="server" Width="489px" CssClass="Normal" Height="57px"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        <tr>
                                            <td valign="top" width="232" height="25">
                                                <asp:Label ID="Label7" runat="server" Width="208px" CssClass="NormalBold" Text="<%$Resources:lblOctEngShortDescr.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtOctEngShortDescr" runat="server" Width="489px" 
                                                    CssClass="Normal" Height="25px"
                                                    TextMode="SingleLine" MaxLength="255" 
                                                    ToolTip="{0} will be replaced with the speed zone of the offence"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="232" height="25">
                                                <asp:Label ID="Label8" runat="server" Width="191px" CssClass="NormalBold" Text="<%$Resources:lblAddOcTAfrShortDescr.Text %>"></asp:Label></td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtOctAfrShortDescr" runat="server" Width="489px" 
                                                    CssClass="Normal" Height="21px"
                                                    TextMode="SingleLine" MaxLength="255" 
                                                    ToolTip="{0} will be replaced with the speed zone of the offence"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblOcTDescriptionEng" runat="server" Width="191px" CssClass="NormalBold" Text="<%
$Resources:lblAddOcTDescriptionEng.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtOcTDescriptionEng" runat="server" Width="500px" CssClass="Normal" Height="57px"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <p>
                                                    <asp:Label ID="lblOcTDescriptionAfr" runat="server" Width="191px" CssClass="NormalBold" Text="<%
$Resources:lblAddOcTDescriptionAfr.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" height="25">
                                                <asp:TextBox ID="txtOcTDescriptionAfr" runat="server" Width="500px" CssClass="Normal" Height="57px"
                                                    TextMode="MultiLine"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td width="232">
                                            </td>
                                            <td width="248">
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
