using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// The Template page
    /// </summary>
    public partial class SuspenseNoticeAssign : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private NoticeAssignment assignment = null;
        private int maxStatus = 900;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SuspenseNoticeAssign.aspx";
        //protected string thisPage = "Link a Suspense Notice";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (this.ViewState["NoticeAssignment"] != null)
                this.assignment = (NoticeAssignment)this.ViewState["NoticeAssignment"];

            // Check maximum payable status
            this.CheckMaximumStatusRule();

            if (!Page.IsPostBack)
            {
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.ViewState.Add("NoticeAssignment", new NoticeAssignment());
            }
        }

        private void CheckMaximumStatusRule()
        {
            if (this.ViewState["MaxStatus"] != null)
                this.maxStatus = (int)ViewState["MaxStatus"];
            else
            {
                //AuthorityRulesDetails rule = new AuthorityRulesDetails();
                //rule.ARCode = "4570";
                //rule.ARComment = "900 (Default); See ChargeStatus table for allowable values (status must be 255 or greater)";
                //rule.ARDescr = "Rule to set maximum charge status at which payment/representations are allowed";
                //rule.ARNumeric = 900;
                //rule.ARString = string.Empty;
                //rule.AutIntNo = this.autIntNo;
                //rule.LastUser = this.login;

                //AuthorityRulesDB db = new AuthorityRulesDB(this.connectionString);
                //db.GetDefaultRule(rule);

                AuthorityRulesDetails rule = new AuthorityRulesDetails();
                rule.AutIntNo = this.autIntNo;
                rule.ARCode = "4570";
                rule.LastUser = this.login;
                DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
                KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();


                this.maxStatus = value.Key; //rule.ARNumeric;
                this.ViewState.Add("MaxStatus", this.maxStatus);
            }
        }



        protected void btnDestination_Click(object sender, EventArgs e)
        {
            string ticketNo = this.txtDestination.Text.Trim();
            if (ticketNo.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }
            this.lblError.Text = string.Empty;

            //dls 070712 - added autIntNo to proc so that Auth-rule can be checked and only notices for single authority can be processed
            NoticeForPaymentDB db = new NoticeForPaymentDB(this.connectionString);
            DataSet ds = db.GetNoticesById(string.Empty, ticketNo, new List<int>(), false, 0, this.maxStatus, string.Empty);

            //dls 100202 - AuthRule removed from SP
            //dls 070713 - we now check the AuthRule inside the proc, so there may be two tables. We want to use the 2nd one in this case
            int tableID = 0;

            if (ds.Tables.Count > 1)
                tableID = 1;
            if (ds.Tables[tableID].Rows.Count > 0)
            {
                //found the notice
                this.ViewState.Add("DestinationNotices", ds);

                this.grdDestination.DataSource = ds.Tables[tableID];
                this.grdDestination.DataKeyNames = new string[] { "NoticeIntNo" };
                this.grdDestination.DataBind(); //DataBinding: 'System.Data.DataRowView' does not contain a property with the name 'NoticeIntNo'.

                this.DisplayConfirmation(false);

                if (ds.Tables[tableID].Rows.Count > 1)
                {
                    this.lblError.Text = "The selected notice has multiple charges. Please use the Admin Receipt Reversal process to reallocate the payment.";
                }
            }
            else
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
            }
            //PunchStats805806 enquiry AssignASuspenseNotice
        }

        protected void btnSuspenseNotice_Click(object sender, EventArgs e)
        {
            string ticketNo = this.txtSuspenseNotice.Text.Trim();
            if (ticketNo.Length == 0)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }
            this.lblError.Text = string.Empty;

            SuspenseAccount db = new SuspenseAccount(this.connectionString);
            DataSet ds = db.GetSuspenseNotices(ticketNo);
            this.ViewState.Add("SuspenseNotices", ds);

            if (ds.Tables[0].Rows.Count == 0)
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");

            this.grdSuspense.DataSource = ds;
            this.grdSuspense.DataKeyNames = new string[] { "NotIntNo" };
            this.grdSuspense.DataBind();

            this.DisplayConfirmation(false);
            //PunchStats805806 enquiry AssignASuspenseNotice
        }

        protected void grdSuspense_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            DataSet ds = (DataSet)this.ViewState["SuspenseNotices"];
            DataRow row = ds.Tables[0].Rows[e.Row.RowIndex];
            DateTime dt;
            float amnt;

            Label label = (Label)e.Row.Cells[1].FindControl("lblOffenceDate");
            if (label != null && DateTime.TryParse(row["NotOffenceDate"].ToString(), out dt))
                label.Text = ((DateTime)row["NotOffenceDate"]).ToString("yyyy-MM-dd");

            label = (Label)e.Row.Cells[2].FindControl("lblAmount");
            if (label != null && float.TryParse(row["ChgRevFineAmount"].ToString(), out amnt))
                label.Text = string.Format("R {0:0}", amnt);
        }


        protected void grdSuspense_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ticketNo = this.grdSuspense.SelectedRow.Cells[0].Text;
            int notIntNo = (int)this.grdSuspense.SelectedDataKey["NotIntNo"];
            int rctIntNo = 0;

            //mrs 20080915 there must be a receipt on the notice and it must not be reversed
            //             i think it imprudent to add this to noticeByIDNo stored proc as it is already too complex and used in multiple places
            NoticeForPaymentDB db = new NoticeForPaymentDB(this.connectionString);
            rctIntNo = db.NoticeHasValidReceipt(notIntNo);

            if (rctIntNo > 0)
            {
                Session["RctIntNo"] = rctIntNo;

                DataSet ds = (DataSet)this.ViewState["SuspenseNotices"];
                decimal amount = decimal.Parse(ds.Tables[0].Rows[this.grdSuspense.SelectedIndex]["ChgRevFineAmount"].ToString());

                this.assignment.SetSuspenseNotice(notIntNo, ticketNo, amount);
                this.pnlDestination.Visible = true;
                this.DisplayConfirmation(true);
            }
            else
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                return;
            }
        }

        protected void grdDestination_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdDestination.Rows.Count > 1)
            {
                this.lblError.Text = "The selected notice has multiple charges. Please use the Admin Receipt Reversal process to reallocate the payment.";
                return;
            }
            string ticketNo = this.grdDestination.SelectedRow.Cells[0].Text;
            int notIntNo = (int)this.grdDestination.SelectedDataKey["NoticeIntNo"];
            DataSet ds = (DataSet)this.ViewState["DestinationNotices"];
            int tableID = 0;

            //this should be on the source
            //int rctIntNo = 0;

            ////mrs 20080915 there must be a receipt on the notice and it must not be reversed
            ////             i think it imprudent to add this to noticeByIDNo stored proc as it is already too complex and used in multiple places
            //NoticeForPaymentDB db = new NoticeForPaymentDB(this.connectionString);
            //rctIntNo = db.NoticeHasValidReceipt(notIntNo);

            //if (rctIntNo > 0)
            //{
            //    Session["RctIntNo"] = rctIntNo;

            if (ds.Tables.Count > 1)
                tableID = 1;
            decimal amount = decimal.Parse(ds.Tables[tableID].Rows[this.grdDestination.SelectedIndex]["Amount"].ToString()); //problem hereabouts on update

            this.assignment.SetDestinationNotice(notIntNo, ticketNo, amount);
            this.DisplayConfirmation(true);
            //}
            //else
            //{
            //this.lblError.Text = "The notice you selected either has no receipt or the receipt has been reversed.";
            //return;
            //}
        }

        private void DisplayConfirmation(bool display)
        {
            if (display)
            {
                this.pnlConfirm.Visible = true;
                this.lblConfirmation.Text = this.assignment.ToString();
                this.lblError.Text = string.Empty;
                this.btnYes.Visible = this.assignment.IsValid;
            }
            else
                this.pnlConfirm.Visible = false;
        }

        protected void grdDestination_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            //if (e.Row.DataItem != null)
            //{
            //  System.Data.Common.DbDataRecord s = (System.Data.Common.DbDataRecord)e.Row.DataItem;

            //if (s != null)
            //{
            DataSet ds = (DataSet)this.ViewState["DestinationNotices"];
            //mrs 20080912 check which table returned
            int tableID = 0;
            if (ds.Tables.Count > 1)
                tableID = 1;

            DataRow row = ds.Tables[tableID].Rows[e.Row.RowIndex];

            Label label = (Label)e.Row.Cells[1].FindControl("lblOffenceDate");
            if (label != null)
                label.Text = ((DateTime)row["OffenceDate"]).ToString("yyyy-MM-dd");
            label = (Label)e.Row.Cells[4].FindControl("lblAmount");
            if (label != null)
                label.Text = string.Format("R {0:0}", ((float)row["Amount"]));
            //}
            //}
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            this.ResetControls();
        }

        private void ResetControls()
        {
            this.pnlConfirm.Visible = false;
            this.pnlDestination.Visible = false;
            this.pnlSuspense.Visible = true;

            this.grdDestination.DataSource = null;
            this.grdDestination.DataBind();
            this.txtDestination.Text = string.Empty;

            this.txtSuspenseNotice.Text = string.Empty;
            this.grdSuspense.DataSource = null;
            this.grdSuspense.DataBind();

            this.ViewState["NoticeAssignment"] = new NoticeAssignment();
            this.ViewState.Remove("SuspenseNotices");
            this.ViewState.Remove("DestinationNotices");

            this.lblError.Text = string.Empty;
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            NoticeAssignment assignment = (NoticeAssignment)this.ViewState["NoticeAssignment"];
            if (!assignment.IsValid)
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                return;
            }

            SuspenseAccount db = new SuspenseAccount(this.connectionString);
            //mrs 20080913 added login and switch
            //mrs 20080915 added rctIntNo as a parameter to force the system to use the receipt and not a reversal
            int rctIntNo = Convert.ToInt32(Session["RctIntNo"].ToString());
            rctIntNo = db.AssignNotice(assignment, login, rctIntNo);

            //if (rctIntNo > 0)
            switch (rctIntNo)
            {
                case -6:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    break;
                case -5:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                    break;
                case -4:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                    break;
                case -3:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                    break;
                case -2:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                    break;
                case -1:
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                    break;
                default:
                    Helper_Web.BuildPopup(this, "ReceiptNoteViewer.aspx", "receipt", rctIntNo.ToString());
                    this.ResetControls();
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text12"), assignment.DestinationTicketNo);
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.AssignASuspenseNotice, PunchAction.Add);  

                    break;
            }
        }

    }
}
