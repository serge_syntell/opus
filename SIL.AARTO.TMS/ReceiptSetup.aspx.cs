using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.BLL.Utility.Cache;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
	/// <summary>
	/// Defines a page for setting up the relationships between ledger accounts and charge types, and between users and 
	/// bank accounts so that they can act as cashiers
	/// </summary>
	public partial class ReceiptSetup : System.Web.UI.Page
	{
		// Constants
        //private const string LA_SELECT_TEXT = "Select an Authority";

        // Fields
        private string connectionString = string.Empty;

		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
		protected string thisPageURL = "ReceiptSetup.aspx";
        //protected string thisPage = "Cash Receipts: Setup";
        protected int _autIntNo = 0;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnLoad (System.EventArgs e)
		{
            this.connectionString = Application["constr"].ToString();

			//btnOptAdd.Visible = false;

			//get user info from session variable
			if (Session["userDetails"] == null)
				Server.Transfer("Login.aspx?Login=invalid");
			if (Session["userIntNo"] == null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			UserDetails userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;
			this._autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			// Set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
			title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				panelGeneral.Visible = true;
				panelCashierAccount.Visible = false;
				//btnOptDelete.Visible = false;

				//this.PopulateAuthorities(ugIntNo, autIntNo);
                this.PopulateCashierGrid();

                //if (_autIntNo > 0)
                //{
                //    // BindGrid(autIntNo);
                //    // PopulateCourt (ddlAuth_Court, autIntNo);
                //}
                //else
                //{
                //    lblError.Text = "Cash receipt setup for unknown authority not allowed. Please select authority first";
                //    lblError.Visible = true;
                //}
			}
		}

        protected void PopulateAccount(DropDownList ddlSelectAccount)
        {
            AccountDB road = new AccountDB(connectionString);
            
            SqlDataReader reader = road.GetAccountList();
            ddlSelectAccount.DataSource = reader;
            ddlSelectAccount.DataValueField = "AccIntNo";
            ddlSelectAccount.DataTextField = "LongName";
            ddlSelectAccount.DataBind();

            reader.Close();
        }

        //protected void PopulateBankAccount (int autIntNo, DropDownList ddlSelectBankAccount)
        //{
        //    BankAccountDB road = new BankAccountDB(connectionString);

        //    SqlDataReader reader = road.GetBankAccountList(autIntNo);

        //    ddlSelectBankAccount.Items.Clear();
        //    ddlSelectBankAccount.Items.Add(new ListItem("[ None ]", "0"));
        //    while (reader.Read())
        //        ddlSelectBankAccount.Items.Add(new ListItem(reader["LongName"].ToString(), reader["BAIntNo"].ToString()));

        //    //ddlSelectBankAccount.DataSource = reader;
        //    //ddlSelectBankAccount.DataValueField = "BAIntNo";
        //    //ddlSelectBankAccount.DataTextField = "LongName";
        //    //ddlSelectBankAccount.DataBind();

        //    reader.Close();
        //}

        public void PopulateCashboxes()
        {
            CashboxDB cashbox = new CashboxDB(connectionString);
            
            this.ddlCashBox.DataSource = cashbox.GetCashboxListDS();
            this.ddlCashBox.DataValueField = "CBIntNo";
            this.ddlCashBox.DataTextField = "CBName";
            this.ddlCashBox.DataBind();
        }

		protected void PopulateChargeType (DropDownList ddlChargeType)
		{
			ChargeTypeDB ct = new ChargeTypeDB(connectionString);
            
			SqlDataReader reader = ct.GetChargeTypeList("All");
			ddlChargeType.DataSource = reader;
			ddlChargeType.DataValueField = "CTIntNo";
			ddlChargeType.DataTextField = "ChargeType";
			ddlChargeType.DataBind();
			reader.Close();
		}

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        //protected void PopulateAuthorities (int ugIntNo, int autIntNo)
        //{
        //    UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

        //    SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);

        //    ddlSelectLA.Items.Add(LA_SELECT_TEXT);
        //    while (reader.Read())
        //    {
        //        ListItem item = new ListItem(reader["AutName"].ToString(), reader["AutIntNo"].ToString());
        //        if (item.Value.Equals(this.autIntNo.ToString()))
        //            item.Selected = true;
        //        this.ddlSelectLA.Items.Add(item);
        //    }

        //    reader.Close();

        //    if (this.autIntNo >= 0)
        //        this.PopulateCashierGrid();
        //}

		protected void btnAddAccountCharge_Click (object sender, EventArgs e)
		{
			StringBuilder sb = new StringBuilder();
            //if (this.ddlSelectLA.SelectedValue == LA_SELECT_TEXT)
            //    sb.Append("You need to select a local authority before you can link ledger accounts to charge types.<br/>");

			if (string.IsNullOrEmpty(this.ddlSelectAccount.SelectedValue))
                sb.Append((string)GetLocalResourceObject("lblError.Text") + "<br/>");

			if (string.IsNullOrEmpty(this.ddlSelectChargeType.SelectedValue))
				sb.Append((string)GetLocalResourceObject("lblError.Text3")+"<br/>");

			if (sb.ToString().Length > 0)
			{
				this.lblError.Text = sb.ToString();
				return;
			}

			int accIntNo = Convert.ToInt32(ddlSelectAccount.SelectedValue);
			int ctIntNo = Convert.ToInt32(ddlSelectChargeType.SelectedValue);
			Stalberg.TMS.AccountDB acCTAdd = new AccountDB(connectionString);
			int acCtIntNo = acCTAdd.AddAcc_CT(accIntNo, ctIntNo, loginUser);
            if (acCtIntNo <= 0)
            {
                if (acCtIntNo == -1)
                {
                    //2014-01-20 Heidi fixed bug for Prompt information is not correct.(5103)
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                }
                else
                {
                    //Modifide by Henry 2012-3-7
                    //Multi Language lblError.Text1-lblError.Text1
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                }
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");

                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this.loginUser, PunchStatisticsTranTypeList.CashierSetup, PunchAction.Add);

            }
		}

        //protected void btnOptDelete_Click (object sender, EventArgs e)
        //{
        //    int rctIntNo = Convert.ToInt32(Session["editRctIntNo"]);

        //    CashReceiptDB receipt = new Stalberg.TMS.CashReceiptDB(connectionString);

        //    string delRctIntNo = receipt.DeleteCashReceipt(rctIntNo);

        //    if (delRctIntNo.Equals("0"))
        //        lblError.Text = "Unable to delete this receipt";
        //    else if (delRctIntNo.Equals("-1"))
        //        lblError.Text = "Unable to delete this receipt - it has been linked to payment details";
        //    else
        //        lblError.Text = "Receipt has been deleted";

        //    lblError.Visible = true;

        //    int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
        //}

        //protected void ddlSelectLA_SelectedIndexChanged (object sender, EventArgs e)
        //{
        //    if (this.ddlSelectLA.SelectedValue == LA_SELECT_TEXT)
        //    {
        //        this.panelCashierAccount.Visible = false;
        //        this.ddlSelectAccount.Items.Clear();
        //        this.ddlSelectChargeType.Items.Clear();
        //        return;
        //    }

        //    this.PopulateCashierGrid();
        //}

		private void PopulateCashierGrid ()
		{
            //int authIntNo = int.Parse(ddlSelectLA.SelectedValue);

            //this.ViewState.Add("AutIntNo", authIntNo);
            //this.PopulateBankAccount(authIntNo, this.ddlBankAccount);
            //this.PopulateCashierGrid(authIntNo);
            //this.labelCashiers.Text = "Cashiers for " + this.ddlSelectLA.SelectedItem.Text;
            //this.panelCashierAccount.Visible = true;
            ////this.PopulateAccount(authIntNo, this.ddlSelectAccount);
            //this.PopulateChargeType(this.ddlSelectChargeType);

            PopulateCashboxes();
            PopulateUserList();
            this.BindCashierGrid();

            this.panelCashierAccount.Visible = true;

            this.PopulateAccount(this.ddlSelectAccount);
            this.PopulateChargeType(this.ddlSelectChargeType);
		}

        //private void PopulateCashierGrid (int authIntNo)
        //{
        //    BankAccountDB bankAccount = new BankAccountDB(connectionString);
        //    DataSet ds = bankAccount.GetAuthorityBankAccounts(authIntNo);
        //    this.gridCashiers.DataSource = ds;
        //    this.gridCashiers.DataBind();
        //}

        private void PopulateUserList()
        {
            UserDB userDB = new UserDB(connectionString);

            this.ddlUser.DataSource = userDB.GetUserList();
            this.ddlUser.DataValueField = "UserIntNo";
            this.ddlUser.DataTextField = "UserName";
            this.ddlUser.DataBind();

            this.ddlUser.Items.Insert(0, (string)GetLocalResourceObject("ddlUser.Items"));
            this.ddlUser.SelectedIndex = 0;
        }

        private void BindCashierGrid()
        {
            Cashbox_UserDB cashbox_UserDB = new Cashbox_UserDB(connectionString);
            DataSet ds = cashbox_UserDB.GetCashbox_UserDBList();
            this.gridCashiers.DataSource = ds;
            this.gridCashiers.DataBind();
        }


		protected void gridCashiers_ItemCommand (object source, DataGridCommandEventArgs e)
		{
			int userIntNo = int.Parse(e.Item.Cells[0].Text);

            if (e.CommandName == "Select")
            {
                int cashBox_UserIntNo = int.Parse(e.Item.Cells[1].Text);
                Cashbox_UserDB cashbox_UserDB = new Cashbox_UserDB(connectionString);

                int returnValue = cashbox_UserDB.DeleteCashBox_User(cashBox_UserIntNo);

                switch (returnValue)
                {
                    case 0:

                        lblCashBoxMsg.Text = (string)GetLocalResourceObject("lblCashBoxMsg.Text");
                        break;
                    default:
                        lblCashBoxMsg.Text = (string)GetLocalResourceObject("lblCashBoxMsg.Text1");
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this.loginUser, PunchStatisticsTranTypeList.CashierSetup, PunchAction.Delete);  
                        this.BindCashierGrid();
                        break;
                }
            }
            else
            {
                UserDB userDb = new UserDB(this.connectionString);
                userDb.ResetUserCountCashBox(userIntNo, loginUser);
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this.loginUser, PunchStatisticsTranTypeList.CashierSetup, PunchAction.Change);  
                this.PopulateCashierGrid();
            }
		}

        protected void btnAssign_Click(object sender, EventArgs e)
        {
            if (this.ddlUser.SelectedIndex < 1)
            {
                lblCashBoxMsg.Text = (string)GetLocalResourceObject("lblCashBoxMsg.Text2");
                return;
            }

            int userIntNo = int.Parse(ddlUser.SelectedValue);
            //int cashBoxInt = int.Parse(ddlCashBox.SelectedValue);

            Cashbox_UserDB cashbox_UserDB = new Cashbox_UserDB(connectionString);

            string errMessage = string.Empty;

            int returnValue = cashbox_UserDB.AddCashbox_UserDB(userIntNo, ref errMessage);

            switch (returnValue)
            {
                case 0:

                    lblCashBoxMsg.Text = (string)GetLocalResourceObject("lblCashBoxMsg.Text") + ": " + errMessage;
                    break;
                case -1:
                    lblCashBoxMsg.Text = (string)GetLocalResourceObject("lblCashBoxMsg.Text3");
                    break;
                //case -2:
                //    lblCashBoxMsg.Text = "This cash box has already been assigned";
                //    break;
                default:
                    lblCashBoxMsg.Text = (string)GetLocalResourceObject("lblCashBoxMsg.Text4");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, this.loginUser, PunchStatisticsTranTypeList.CashierSetup, PunchAction.Change);  
                    this.BindCashierGrid();
                    break;
            }
        }
}
}
