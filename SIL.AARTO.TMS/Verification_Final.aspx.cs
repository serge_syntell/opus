using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace Stalberg.TMS 
{

	public partial class Verification_Final : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        protected string thisPage = "Verification - Stage 3 (Finalise)";
        protected string thisPageURL = "Verification_Film.aspx";
        protected int autIntNo = 0;
        protected int filmIntNo = 0;
        protected int userIntNo = 0;
        protected int processType = 1;          //1 = Verification; 2 = Correction; 
        private bool isNaTISLast = false;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			Stalberg.TMS.UserDB user = new UserDB(connectionString);
			Stalberg.TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
            userIntNo = Convert.ToInt32(Session["userIntNo"]);
	
			//int 

            if (Session["autIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No authority has been selected - please login correctly";
                return;
            }
            else if (Session["filmIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No film has been selected - please login correctly";
                return;
            }

            autIntNo = Convert.ToInt32(Session["autIntNo"]);
            filmIntNo = Convert.ToInt32(Session["filmIntNo"]);

			loginUser = userDetails.UserLoginName;

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (Session["processType"] != null)
                processType = Convert.ToInt32(Session["processType"]);

            this.isNaTISLast = (bool)this.Session["NaTISLast"];

			if (!Page.IsPostBack)
			{
                if (processType == 1)
                {
                    lblPageName.Text = "Verification - Stage 3 (Finalise)";
                    btnAccept.Text = "Finalise verification for film";
                    btnAccept.Attributes.Add("onclick", "return confirm('Are you sure you want to finalise the verification for this film?');");
                }
                else
                {
                    lblPageName.Text = "Correction - Stage 2 (Finalise)";
                    btnAccept.Text = "Finalise corrections for film";
                    btnAccept.Attributes.Add("onclick", "return confirm('Are you sure you want to finalise the corrections for this film?');");
                }

                pnlAdjudicate.Visible = true;
                pnlContinue.Visible = false;

				if (autIntNo>0)
				{
                    LoadFilmData();
				}               
			}		
		}

      
        private void LoadFilmData()
        {
            FrameDB frame = new FrameDB(connectionString);

            FrameDetails frameDet = frame.GetFrameDetailsForFilm(filmIntNo);

            FilmDB film = new FilmDB(connectionString);

            FilmDetails filmDet = film.GetFilmDetails(filmIntNo);

            lblFilmNo.Text = filmDet.FilmNo;

            AuthorityDB auth = new AuthorityDB(connectionString);

            AuthorityDetails authDet = auth.GetAuthorityDetails(autIntNo);

            lblAuthority.Text = authDet.AutName;

        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            //user is happy with settings - update all frames according to current values
            FilmDB film = new FilmDB(connectionString);

            string errMessage = string.Empty;
            string natisLast = this.isNaTISLast ? "Y" : "N";

            int success = film.FinaliseFilmVerification(filmIntNo, 2, loginUser, ref errMessage, natisLast);      //cvPhase is 2 for Finalise

            if (success == 0)
            {
                lblError.Visible = true;
                if (processType == 1)
                    lblError.Text = "Verification successful! Click 'Process another film' to verify another film?";
                else
                    lblError.Text = "Correction successful! Click 'Process another film' to correct another film?";
                btnContinue.Text = "Process another film";
                //ClearConfirmFilmList();
            }
            else if (success == -1)
            {
                lblError.Visible = true;
                if (processType == 1)
                    lblError.Text = "Verification failed! " + errMessage;
                else
                    lblError.Text = "Correction failed! " + errMessage;
                btnContinue.Visible = false;
                btnAccept.Visible = false;
                btnReturn.Visible = false;
            }
            else
            {
                lblError.Visible = true;
                if (processType == 1)
                    lblError.Text = "Some frames are in error - verification cannot be finalised! Click 'Process incorrect frames' to correct errors?";
                else
                    lblError.Text = "Some frames are in error - correction cannot be finalised! Click 'Process incorrect frames' to correct errors?";
                btnContinue.Text = "Process incorrect frames";
            }

            pnlAdjudicate.Visible = false;
            pnlContinue.Visible = true;

        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect("Verification_PreFilm.aspx");
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            //clear this just in case they switch to a different LA
            Session["adjAt100"] = null;
            //Response.Redirect("Default.aspx");
            //Close window
            String strRedirectScript;

            // Generate the JavaScript to pop up a print-only window
            strRedirectScript = "<script language =javascript>";
            strRedirectScript += "window.close();";
            strRedirectScript += "</script> ";

            // Add our JavaScript to the currently rendered page
            Response.Write(strRedirectScript);
        }
}
}
