<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Contractor" Codebehind="Contractor.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOption.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %> "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0">
                        <tr>
                            <td valign="top">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:DataGrid ID="dgContractor" Width="495px" runat="server" BorderColor="Black"
                                    AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgContractor_ItemCommand"
                                    OnPageIndexChanged="dgContractor_PageIndexChanged">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="ConIntNo" HeaderText="ConIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ConCode" HeaderText="<%$Resources:dgContractor.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ConName" HeaderText="<%$Resources:dgContractor.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgContractorItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
                                <asp:Panel ID="pnlAddContractor" runat="server">
                                    <table id="Table2" cellspacing="1" cellpadding="1" border="0">
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="lblAddContractor" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddContractor.Text %>"></asp:Label></td>
                                            <td valign="top">
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="lblAddConCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddConCode.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConCode" runat="server" Width="83px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="5"></asp:TextBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    ControlToValidate="txtAddConCode" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblConName.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConName" runat="server" Width="204px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="18"></asp:TextBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtAddConName"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqName.ErrorMessage %>"
                                                    ForeColor=" " Width="210px"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="176px" Text="<%$Resources:lblAddConSupportSName.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConSupportSName" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="18" Width="204px"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="176px" Text="<%$Resources:lblAddConSupportFName.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConSupportFName" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="18" Width="204px"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="187px" Text="<%$Resources:lblConSupportEmail.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConSupportEmail" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="204px"></asp:TextBox></td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="emailValid" runat="server" ControlToValidate="txtAddConSupportEmail"
                                                    CssClass="NormalRed" Display="Dynamic" ErrorMessage="<%$Resources:emailValid.ErrorMessage %>"
                                                    ForeColor=" " ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddConFTPSite.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConFTPSite" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="204px"></asp:TextBox></td>
                                            <td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Width="229px" Text="<%$Resources:lblConFTPFolder.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConFTPFolder" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="204px"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddConFTPLogin.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConFTPLogin" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="18" Width="204px"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" w>
                                                <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddConFTPPassword.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtAddConFTPPassword" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="204px" TextMode="Password"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                            </td>
                                            <td valign="top">
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddContractor" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddContractor.Text %>"
                                                    OnClick="btnAddContractor_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateContractor" runat="server">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label19" runat="server" Width="194px" CssClass="ProductListHead" Text="<%$Resources:lblUpdSelected.Text %>"></asp:Label></td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddConCode.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtConCode" runat="server" Width="83px" CssClass="NormalMand" Height="24px"
                                                    MaxLength="5"></asp:TextBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                    ControlToValidate="txtConCode" ForeColor=" "></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblConName.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtConName" runat="server" Width="204px" CssClass="NormalMand" Height="24px"
                                                    MaxLength="18"></asp:TextBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtConName"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqName.ErrorMessage %>"
                                                    ForeColor=" " Width="210px"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Width="176px" Text="<%$Resources:lblAddConSupportSName.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtConSupportSName" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="18" Width="204px"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Width="176px" Text="<%$Resources:lblAddConSupportFName.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtConSupportFName" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="18" Width="204px"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Width="187px" Text="<%$Resources:lblConSupportEmail.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtConSupportEmail" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="204px"></asp:TextBox></td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ControlToValidate="txtConSupportEmail"
                                                    CssClass="NormalRed" Display="Dynamic" ErrorMessage="<%$Resources:emailValid.ErrorMessage %>"
                                                    ForeColor=" " ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddConFTPSite.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtConFTPSite" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="204px"></asp:TextBox></td>
                                            <td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label16" runat="server" CssClass="NormalBold" Width="229px" Text="<%$Resources:lblConFTPFolder.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtConFTPFolder" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="204px"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Label ID="Label17" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddConFTPLogin.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtConFTPLogin" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="18" Width="204px"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" w>
                                                <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddConFTPPassword.Text %>"></asp:Label></td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtConFTPPassword" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="204px" TextMode="Password"></asp:TextBox></td>
                                            <td>
                                                <asp:Label ID="Label20" runat="server" CssClass="NormalRed" EnableViewState="False" Text="<%$Resources:lblTip.Text %>"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
