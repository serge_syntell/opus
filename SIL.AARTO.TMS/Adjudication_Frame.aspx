<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="ImageControl.ascx" TagName="ImageViewer" TagPrefix="iv1" %>

<%@ Page Language="c#" Inherits="Stalberg.TMS.Adjudication_Frame"
    EnableEventValidation="false" Codebehind="Adjudication_Frame.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html>
<head runat="server">
    <title>
        <%=title%>
    </title>
    <style type="text/css">
        .style1
        {
            height: 47px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0" language="javascript" onload="return window_onload()">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="WebService.asmx" />
        </Services>
        <Scripts>
            <asp:ScriptReference Path="~/Scripts/WebService.js" />
        </Scripts>
    </asp:ScriptManager>
    <%--<table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" colspan="2" valign="middle" style="width: 100%">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>--%>
    <asp:UpdatePanel ID="udpFrame" runat="server">
        <ContentTemplate>
            <table border="0" width="100%">
                <tr>
                    <td valign="top" width="100%" align="center">
                        <asp:Panel ID="pnlNavigation" Width="100%" runat="server">
                            <table border="0" style="width: 100%; border: solid 1px black;" class="Normal">
                                <tr>
                                    <td style="width: 10%;">
                                        <asp:LinkButton ID="lnkPrevious" runat="server" CssClass="MenuItem" OnClick="lnkPrevious_Click"><< Previous</asp:LinkButton>
                                    </td>
                                    <td style="width: 80%; text-align: center;">
                                        <asp:Label ID="lblNavigation" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                    <td style="width: 10%; text-align: right">
                                        <asp:LinkButton ID="lnkNext" runat="server" CssClass="MenuItem" OnClick="lnkNext_Click">Next >></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <table width="100%">
                            <tr>
                                <td>
                                    &nbsp;<asp:Label ID="lblFilmNo" runat="server" CssClass="NormalBold"></asp:Label>&nbsp;
                                    <asp:Label ID="Label19" runat="server" CssClass="NormalBold">Frame no / Seq:</asp:Label>&nbsp;
                                    <asp:TextBox ID="txtFrameNo" runat="server" CssClass="Normal" MaxLength="10" ReadOnly="True"
                                        Width="71px"></asp:TextBox>&nbsp;
                                    <asp:TextBox ID="txtSeq" runat="server" CssClass="Normal" MaxLength="10" ReadOnly="True"
                                        Width="29px"></asp:TextBox>
                                </td>
                                <td align="center">
                                    <asp:CheckBox ID="chkSaveImageSettings" runat="server" CssClass="NormalBold" Checked="False"
                                        Text="Save Image Settings for Frames on Film" OnCheckedChanged="chkSaveImageSettings_CheckedChanged"
                                        AutoPostBack="true" />
                                    <asp:Label ID="lblPageName" runat="server" Width="345px" CssClass="ContentHead">Adjudication</asp:Label>
                                </td>
                                <td align="right" width="33%">
                                    <span id="lblMessage" runat="server" class="NormalRed"></span>&nbsp;<asp:Label ID="lblError"
                                        runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                    <asp:CheckBox ID="chkAllowContinue" runat="server" CssClass="NormalBold" Text="Registration Confirmed?" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlNatisData" Visible="false" Width="100%" runat="server" BorderColor="Blue"
                            BorderWidth="3" BorderStyle="Solid">
                            <table>
                                <tr>
                                    <td style="width: 100%;">
                                        <table border="0" style="width: 100%;" class="Normal">
                                            <tr>
                                                <td style="width: 10%; text-align: center;">
                                                    <asp:Label ID="labNatis" runat="server" Font-Underline="True">NATIS</asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:Label ID="labNatisVehicleMake" runat="server" Width="250px">Vehicle Make:</asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:Label ID="labNatisVehicleType" runat="server" Width="250px">Vehicle Type:</asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:Label ID="labRegistrationNo" runat="server" Width="250px">Registration no:</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: center;">
                                                    <asp:Label ID="labRejectionReason" runat="server" CssClass="NormalBold" Font-Size="Medium"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <input id="btnResendNATIS" runat="server" class="NormalButton" style="width: 120px;
                                            height: 28px;" type="button" value="Resend NATIS" />&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="100%">
                        <table width="100%">
                            <tr>
                                <td width="100%" align="center">
                                    <table width="100%">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table width="100%" class="NormalBold">
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:Label ID="Label13" runat="server" CssClass="NormalBold" Width="94px">Speed:</asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Width="161px">Offence time and date:</asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="137px" Height="19px">Vehicle make:</asp:Label>
                                                        </td>
                                                        <td style="height: 13px" valign="top">
                                                            <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="138px">Vehicle type:</asp:Label>
                                                        </td>
                                                        <td style="height: 13px" valign="top">
                                                            <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Width="137px" Height="19px">Registration no:</asp:Label>
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <asp:Label ID="lblRejection" runat="server" CssClass="NormalBold" Width="137px">Reason for rejection:</asp:Label>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <input id="btnAccept" class="NormalButton" style="width: 150px; background-color: #FF0000;"
                                                                type="button" value="Prosecute" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:TextBox ID="txtFirstSpeed" runat="server" CssClass="NormalMand" MaxLength="10"
                                                                Width="36px"></asp:TextBox>
                                                            &nbsp;
                                                            <asp:TextBox ID="txtSecondSpeed" runat="server" CssClass="NormalMand" MaxLength="10"
                                                                Width="36px"></asp:TextBox>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="txtOffenceTime" runat="server" CssClass="NormalMand" MaxLength="10"
                                                                Width="59px"></asp:TextBox>&nbsp;
                                                            <asp:TextBox ID="txtOffenceDate" runat="server" CssClass="NormalMand" MaxLength="10"
                                                                Width="80px"></asp:TextBox>
                                                        </td>
                                                        <td style="height: 13px" valign="top">
                                                            <asp:DropDownList ID="ddlVehMake" runat="server" Width="169px" CssClass="Normal"
                                                                Height="15px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="height: 13px" valign="top">
                                                            <asp:DropDownList ID="ddlVehType" runat="server" Width="169px" CssClass="Normal"
                                                                Height="15px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="height: 13px" valign="top">
                                                            <asp:TextBox ID="txtRegNo" runat="server" AutoCompleteType="Disabled" CssClass="NormalMand"
                                                                MaxLength="10" Width="134px"></asp:TextBox>
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <asp:DropDownList ID="ddlRejection" runat="server" Width="247px" CssClass="Normal"
                                                                Height="15px" AutoPostBack="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <input id="btnChange" runat="server" class="NormalButton" style="width: 150px; background-color: #0000FF;"
                                                                type="button" value="Change" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" style="height: 13px">
                                                            Zone:
                                                            <asp:Label ID="lblSpeedZone" runat="server" Width="67px"></asp:Label>
                                                        </td>
                                                        <td valign="top" colspan="2" style="height: 13px">
                                                            Location:
                                                            <asp:Label ID="lblLocationDescr" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="height: 13px" valign="top" colspan="2">
                                                            Officer: &nbsp;
                                                            <asp:Label ID="lblOfficerDetails" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="left" valign="top" style="height: 13px">
                                                            <input id="hidUserName" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidStatusProsecute" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidStatusCancel" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidStatusNatis" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidUpdateMainImage" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidFrameIntNo" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidPreUpdatedRegNo" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidPreUpdatedRejReason" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidPreUpdatedAllowContinue" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidRowVersion" style="width: 24px" type="hidden" runat="server" />
                                                            <input id="hidStatusVerified" runat="server" style="width: 24px" type="hidden" />
                                                            <input id="hidStatusRejected" runat="server" style="width: 24px" type="hidden" />
                                                        </td>
                                                        <td align="left" valign="top" style="height: 13px">
                                                            <asp:Button ID="btnSaveSettingsForImage" runat="server" class="NormalButton" Style="width: 150px;
                                                                background-color: #00FFFF; color: #000000;" Text="Save Image Setting" OnClick="btnSaveImageSetting_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" runat="server" id="tableASD" visible="false">
                                        <tr>
                                            <td colspan="6" align="center">
                                                <label style="color: White; background-color: Blue; font-size: large">
                                                    AVERAGE SPEED OVER DISTANCE</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 20%; text-align: right" class="SubContentHead">
                                                Section Start:
                                            </td>
                                            <td style="width: 10%; text-align: right" class="NormalBold">
                                                GPS Date & Time:
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox runat="server" ID="tbASDGPSDatetime1" Enabled="true" ReadOnly="false"
                                                    class="NormalBold"></asp:TextBox>
                                            </td>
                                            <td style="font-weight: bold; width: 20%; text-align: right" class="SubContentHead">
                                                Section End:
                                            </td>
                                            <td style="width: 10%; text-align: right" class="NormalBold">
                                                GPS Date & Time:
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox runat="server" ID="tbASDGPSDatetime2" Enabled="true" ReadOnly="false"
                                                    class="NormalBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td style="width: 10%; text-align: right" class="NormalBold">
                                                Lane Number:
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbASDLane1" Enabled="true" ReadOnly="false" class="NormalBold"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td style="width: 10%; text-align: right" class="NormalBold">
                                                Lane Number:
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbASDLane2" Enabled="true" ReadOnly="false" class="NormalBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <ajaxToolkit:ModalPopupExtender ID="modalPopupExtender" runat="server" TargetControlID="btnAccept"
                PopupControlID="addressPanel" BehaviorID="ModalPopupBehavior" BackgroundCssClass="modalBackground"
                PopupDragHandleControlID="dragablePanel" CancelControlID="btCancel" OkControlID="btProsecute"
                DropShadow="false" X="400" Y="300" />
            <asp:Panel ID="addressPanel" runat="server" Style="display: none" CssClass="modalPopup"
                Width="500px">
                <asp:Panel ID="dragablePanel" runat="server" Style="cursor: move; background-color: #DDDDDD;
                    border: solid 1px Gray; color: Black;">
                </asp:Panel>
                <table width="500px">
                    <tr>
                        <td align="center" style="height: 40px">
                            <asp:Label ID="labWarning" runat="server" CssClass="NormalRed" Font-Bold="True">*** This frame was rejected. ***</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="height: 40px">
                            <asp:Label ID="labRejectionReasonWarning" runat="server" Font-Bold="True">Duplicate Offence</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="height: 40px">
                            <input id="btProsecute" class="NormalButton" style="width: 118px;" type="button"
                                value="Accept/Prosecute" onclick="Action('AdjudicateFrame'); OnRemoteInvoke();"
                                runat="server" />&nbsp;&nbsp;&nbsp;
                            <input id="btCancel" class="NormalButton" style="width: 118px;" type="button" value="Don't Prosecute"
                                onclick="Action('RejectFrame'); OnRemoteInvoke(); " runat="server" />&nbsp;&nbsp;&nbsp;
                            <%--  <input id="btCancel" class="NormalButton" style="width: 118px;" type="button" value="Cancel" runat="server"/>--%>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table width="100%">
        <tr>
            <td width="70%">
                <iv1:ImageViewer ID="imageViewer1" runat="server" />
            </td>
            <td valign="top" align="center">
                <table>
                    <tr>
                        <td style="height: 40">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel ID="updateSpeedMsg" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="speedMessagePanel" runat="server" BorderColor="Black" BorderWidth="3"
                            BorderStyle="Solid" Width="80%" Visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td align="center" style="color: Red; font-size: large; height: 60; vertical-align: bottom;">
                                        Speed
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" align="center" style="color: Red; font-size: large; height: 60;
                                        vertical-align: top;">
                                        <asp:Label ID="lblSpeedInfo1" runat="server" />
                                        km/h
                                        <%--<input id="cmdPlay" runat="server" runat="server" type="hidden" />--%><asp:Literal ID="liMusic" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="speedMessagePane2" runat="server" BorderColor="Black" BorderWidth="3"
                            BorderStyle="Solid" Width="50%" Visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td align="center" style="color: Blue; font-size: medium; height: 40; vertical-align: bottom">
                                        Speed
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="color: Blue; font-size: medium; height: 40; vertical-align: top">
                                        <asp:Label ID="lblSpeedInfo2" runat="server" />
                                        km/h
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" style="width: 100%" align="center">
                &nbsp;<asp:UpdateProgress ID="udp" runat="server" AssociatedUpdatePanelID="udpFrame">
                    <ProgressTemplate>
                        <p class="NormalBold" style="text-align: center;">
                            <img alt="Loading..." src="images/ig_progressIndicator.gif" />Loading...</p>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td align="right">
                <input id="btnClose" class="NormalButtonRed" type="button" value="CLOSE" onclick="window.close();" />&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    </form>
    <%--<input type='button' id="ansuccess" onclick="playMusic('mu')" value='success' />
    <embed src="images/Exclamation.wav" id="mu" autostart="false" width="0" height="0"
        loop="false"></embed>
    <script type="text/javascript">
        function playMusic(p_id) {
            var node = document.getElementById(p_id);
            if (node != null) { node.play(); }
        }
    </script>--%>
    <%--<bgsound src= "images/Exclamation.wav" id= "bgsTest" loop= "1">--%>
    <script language="javascript" type="text/javascript">
    <!--
        setTimeout(enableButtons, 3000);
        rejectionReasonChange();

        function rejectionReasonChange() {

            var labRejectionReason = document.getElementById("labRejectionReason");

            if (labRejectionReason != null) {
                document.getElementById("labRejectionReason").innerHTML = document.Form1.ddlRejection.options[document.Form1.ddlRejection.selectedIndex].text;

                document.getElementById("labRejectionReasonWarning").innerHTML = document.Form1.ddlRejection.options[document.Form1.ddlRejection.selectedIndex].text;

                if (document.getElementById("labRejectionReason").innerHTML != "None") {
                    document.getElementById("pnlNatisData").style.backgroundColor = "#FF0000";
                    document.getElementById("labRejectionReason").style.display = "";
                }
                else {
                    document.getElementById("pnlNatisData").style.backgroundColor = "#FFFFFF";
                    document.getElementById("labRejectionReason").style.display = "none";
                }

                //        if (document.getElementById("labRejectionReason").innerHTML == "Expired. The frame offence date is too old to continue") {
                //            document.Form1.btnAccept.disabled = true;
                //        }
                //        else {
                //            document.Form1.btnAccept.disabled = false;
                //        }

                if (document.getElementById("labRejectionReason").innerHTML == "Expired. The frame offence date is too old to continue") {
                    document.getElementById("btnAccept").value = "Don't Prosecute";
                    document.Form1.btnChange.disabled = true;
                    document.Form1.btnResendNATIS.disabled = true;
                    document.Form1.ddlRejection.disabled = true;
                }
                else {
                    if (document.Form1.btnChange)
                        document.Form1.btnChange.disabled = false;
                    document.Form1.btnResendNATIS.disabled = false;
                }
            }
        }

        function enableButtons() {
            // updated by LMZ 03-01-2007
            if (document.getElementById("btnAccept") != null) {
                document.getElementById("btnAccept").disabled = false;
            }

            if (document.getElementById("btnChange") != null && '<%= _ISASD%>' != 'True') {
                document.getElementById("btnChange").disabled = false;
            }
        }

        function AllowChange() {
            document.getElementById("txtRegNo").readOnly = false;
            document.getElementById("ddlRejection").disabled = false;
            document.getElementById("ddlRejection").style.display = "";
            document.getElementById("lblRejection").style.display = "";
            document.getElementById("ddlVehMake").disabled = false;
            document.getElementById("ddlVehType").disabled = false;
        }

        function window_onload() {
            //document.getElementById("mp").play();
            document.getElementById("lblMessage").style.display = "none";
        }

        //20090615 tf add Warning Prosecute
        function WarningProsecute() {
            document.getElementById("lblError").innerHTML = "";
            if (document.getElementById("pnlNatisData") == null) {
                Action('AdjudicateFrame');
                OnRemoteInvoke();
                return;
            }

            if (document.getElementById("labRejectionReason").innerHTML == "None") {
                Action('AdjudicateFrame');
                OnRemoteInvoke();
                return;
            }
            else {
                //dls 090618 - don't display the warning if they are legitimately rejecting the frame
                if (document.getElementById("btnAccept").value != "Prosecute") {
                    Action('AdjudicateFrame');
                    OnRemoteInvoke();
                    return;
                }
            }

            var name = navigator.appName;
            if (name == "Microsoft Internet Explorer") {
                var modalPopupBehavior = $find('ModalPopupBehavior');
                modalPopupBehavior.show();
                var bgElement = document.getElementById('ModalPopupBehavior_backgroundElement');
                var addressPanel = document.getElementById("<%= this.addressPanel.ClientID %>");
                addressPanel.style.position = 'absolute';
                addressPanel.style.top = (document.body.offsetHeight - addressPanel.offsetHeight) / 2;
                addressPanel.style.left = (document.body.offsetWidth - addressPanel.offsetWidth) / 2;
                bgElement.style.position = 'absolute';
                bgElement.style.top = 0;
                bgElement.style.height = document.body.offsetHeight;
            }

        }

        /// LMZ 04-04-2007
        /// <summary>
        /// doChange - updates the Accept - Reject action 
        /// passing a JavaScript reference to the field that we want to set.
        /// </summary>
        /// <param name="strField">String. The JavaScript reference to the field that we want to set, in the format: FormName.FieldName
        /// Please note that JavaScript is case-sensitive.</param>
        function doChange() {
            var sValue = document.Form1.ddlRejection.options[document.Form1.ddlRejection.selectedIndex].text;
            var nRegNo = 0;

            document.Form1.btnAccept.value = "Prosecute";

            if (sValue == "None") {
                nRegNo = parseInt(document.Form1.txtRegNo.value);
                if (nRegNo == 0) {

                    if (document.getElementById("lblMessage").firstChild == null) {
                        document.getElementById("lblMessage").appendChild(document.createTextNode("Registration may not be zeroes if it has been accepted"));
                    }
                    else {
                        document.getElementById("lblMessage").firstChild.nodeValue = "Registration may not be zeroes if it has been accepted";
                    }
                    return;
                }
            }
            else
                document.Form1.btnAccept.value = "Don't Prosecute";

            if (document.Form1.txtRegNo.value == "") {
                document.getElementById("lblMessage").appendChild(document.createTextNode("Registration may not be blank"));
                return;
            }
            rejectionReasonChange();
        }
-->  
    </script>
</body>
</html>
