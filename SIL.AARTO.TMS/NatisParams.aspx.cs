using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Notice Post management page
    /// </summary>
    public partial class NatisParams : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "NatisParams.aspx";
        private const string DATE_FORMAT = "yyyy-MM-dd";
        //protected string thisPage = "Natis Parameters Setup Form";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);
           
            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                PopulateAuthorities();
                GetData(this.autIntNo );
            }
        }
       
         

        private void GetData(int nAuth)
        {
            try
            {
                NatisParamsDB db = new NatisParamsDB(this.connectionString);
                DataSet ds = db.GetNatisParams (nAuth);
                this.grdHeader.DataSource = ds;
                this.grdHeader.DataBind();
                this.grdHeader.Visible = true;
            }
            catch (Exception e)
            {
                Console.WriteLine((string)GetLocalResourceObject("lblError.Text15") + e.Message);
            }
        }
    

        protected void btnOptAdd_Click(object sender, EventArgs e)
        {
            this.pnlDetails.Visible = true;
            this.pnlGrid.Visible = false;
            this.lblError.Text = string.Empty;
            btnAction.Text = (string)GetLocalResourceObject("btnOptAdd.Text");
            lblAction.Text = (string)GetLocalResourceObject("lblAction.Text1");
            this.chkBox.Checked = false;
            this.txtNatisTerminal.Text = string.Empty;
            this.txtSendNatisFolder.Text = string.Empty;
            this.txtSentNatisEmail.Text = string.Empty;
            this.txtGetNatisFolder.Text = string.Empty;
            this.txtFirstRun.Text = string.Empty;
            this.txtInterval.Text = string.Empty;
            this.txtNumberRuns.Text = string.Empty;
        }

        protected void btnOptUpdate_Click(object sender, EventArgs e)
        {
            
        }

        protected void btnOptHide_Click(object sender, EventArgs e)
        {

            if (grdHeader.Visible.Equals(true))
            {
                //hide it
                grdHeader.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
            }
            else
            {
                //show it
                grdHeader.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        protected void btnAction_Click(object sender, EventArgs e)
        {
            int nReturn = 0;
            lblError.Visible = true;

            if (this.btnAction.Text == (string)GetLocalResourceObject("btnUpdate.Text"))
            {
                int nStartTime;
                if (!int.TryParse(this.txtFirstRun .Text,out nStartTime))
                {
                    //Modified by Henry 2012-3-8
                    //Mulity language error message from resource file lblError.Text1-lblError.Text14
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                    return;
                }

                // SD: 20081208 : Check for null value for first run time
                StringBuilder sb = new StringBuilder();
                sb.Append((string)GetLocalResourceObject("lblError.Text16") + "\n<ul>\n");

                if (txtFirstRun.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text17") + "</li>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }

                // ***
               

                int nInterval;
                if (!int.TryParse(this.txtInterval.Text,out nInterval))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2"); 
                    return;
                }

                // SD: 20081208 : Check for null value for Interval

                if (txtInterval.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text18") + "</li>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }

                int nNoRuns;
                if (!int.TryParse(this.txtNumberRuns.Text,out nNoRuns ))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    return;
                }

                // SD: 20081208 : Check for null value for number of runs
                if (txtNumberRuns.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text19") + "</li>");
                    sb.Append("</ul>");
                    lblError.Visible = true;
                                    
                }
                if (nNoRuns * nInterval > 23)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                    return;
                }

                string sCurrent = chkBox .Checked ? "Y" : "N";
                NatisParamsDB db = new NatisParamsDB(connectionString);
                nReturn = db.UpdateNatisParams(Convert.ToInt32(txtNPIntNo.Value ), txtNatisTerminal.Text, txtSendNatisFolder.Text, txtSentNatisEmail.Text,
                    txtGetNatisFolder.Text,Convert.ToInt32( txtFirstRun.Text), Convert.ToInt32(txtInterval.Text), Convert.ToInt32(txtNumberRuns.Text), sCurrent, login);
                              
                if (nReturn == Convert.ToInt32(txtNPIntNo.Value))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NatisParameters, PunchAction.Change);  

                    GetData(autIntNo);
                    pnlGrid.Visible = true;
                    return;
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6"); 
                    return;
                }
            }

            if (this.btnAction.Text == (string)GetLocalResourceObject("btnOptAdd.Text"))
            {
                 int nStartTime;
                if (!int.TryParse(this.txtFirstRun .Text,out nStartTime))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text7"); 
                    return;
                }

                // SD: 20081208 : Check for null value for first run time
                StringBuilder sb = new StringBuilder();
                sb.Append((string)GetLocalResourceObject("lblError.Text16") + "\n<ul>\n");

                if (txtFirstRun.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text17") + "</li>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }

                // ***



                int nInterval;
                if (!int.TryParse(this.txtInterval.Text,out nInterval))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                    return;
                }

                // SD: 20081208 : Check for null value for Interval

                if (txtInterval.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text18") + "</li>");
                    lblError.Text = sb.ToString();
                    lblError.Visible = true;
                }



                int nNoRuns;
                if (!int.TryParse(this.txtNumberRuns.Text, out nNoRuns))
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                    return;
                }


                // SD: 20081208 : Check for null value for number of runs
                if (txtNumberRuns.Text.Trim().Length < 0)
                {
                    sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text19") + "</li>");
                    sb.Append("</ul>");
                    lblError.Visible = true;

                }

                if (nNoRuns * nInterval > 23)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text10");
                    return;
                }

                string sCurrent = chkBox .Checked ? "Y" : "N";
                NatisParamsDB db = new NatisParamsDB(connectionString);
                nReturn = db.AddNatisParams(autIntNo , txtNatisTerminal.Text, txtSendNatisFolder.Text, txtSentNatisEmail.Text,
                    txtGetNatisFolder.Text, Convert.ToInt32(txtFirstRun.Text), Convert.ToInt32(txtInterval.Text), Convert.ToInt32(txtNumberRuns.Text), sCurrent , login);
                              
                if (nReturn != 0)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text11");
                    
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NatisParameters, PunchAction.Add); 
                    GetData(autIntNo);
                    pnlGrid.Visible = true;
                    return;
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text12"); 
                    return;
                }
            }
        }

        protected void grdHeader_RowCreated(object sender, GridViewRowEventArgs e)
        {
            DataRowView s = (DataRowView)e.Row.DataItem;
            
            if (s != null)
            {
                if (s["NPChecked"] != DBNull.Value)
                {
                    if (s["NPChecked"].ToString () == "true")
                        e.Row.Cells[10].Enabled = false;
                }
            }
        }

        protected void grdHeader_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grdHeader_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        protected void grdHeader_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.lblError.Text = string.Empty;
            grdHeader.SelectedIndex = e.RowIndex;
            GridViewRow row = grdHeader.Rows[e.RowIndex];
            this.txtNPIntNo.Value = row.Cells[0].Text;
            System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)row.Cells[5].Controls[1];

            NatisParamsDB db = new NatisParamsDB(connectionString);
            int nRes = db.DeleteNatisParams(Convert.ToInt32(txtNPIntNo.Value), autIntNo, lbl.Text == "true" ? "Y" : "N", this.login);
            if (nRes != 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text13");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NatisParameters, PunchAction.Delete); 
                GetData(autIntNo);
                pnlGrid.Visible = true;
                return;
            }
            else
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text14");
                return;
            }
        }

        //protected void grdHeader_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    bool bRes = false;
        //    this.pnlDetails.Visible = true;
        //    this.btnAction.Text = "Update";
        //    this.lblError.Text = string.Empty;
        //    grdHeader.SelectedIndex = e.NewEditIndex;
        //    GridViewRow row = grdHeader.Rows[e.NewEditIndex];
        //    this.txtNPIntNo.Value = row.Cells[0].Text;
        //    System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)row.Cells[5].Controls[1];
        //    bool.TryParse(lbl.Text, out bRes);
        //    this.chkBox.Checked = bRes;
        //    this.txtNatisTerminal.Text = row.Cells[1].Text;
        //    this.txtSendNatisFolder.Text = row.Cells[2].Text;
        //    this.txtSentNatisEmail.Text = row.Cells[3].Text;
        //    this.txtGetNatisFolder.Text = row.Cells[4].Text;
        //    System.Web.UI.WebControls.Label lblDate = (System.Web.UI.WebControls.Label)row.Cells[6].Controls[1];
        //    this.txtFirstRun.Text = lblDate.Text;
        //    this.txtInterval.Text = row.Cells[7].Text;
        //    this.txtNumberRuns .Text = row.Cells[8].Text;

        //    lblAction.Text = "Edit record : " + row.Cells[0].Text + " Terminal ID : " + row.Cells[1].Text;
        //}

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlDetails.Visible = false;
            pnlGrid.Visible = true;
            GetData(Convert.ToInt32(ddlAuthority .SelectedValue) );
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDetails.Visible = false;
            pnlGrid.Visible = true;
            GetData(Convert.ToInt32(ddlAuthority.SelectedValue));
        }
        
        protected void grdHeader_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            bool bRes = false;
            this.pnlDetails.Visible = true;
            this.btnAction.Text = (string)GetLocalResourceObject("btnUpdate.Text");
            this.lblError.Text = string.Empty;
            grdHeader.SelectedIndex = e.NewSelectedIndex;
            GridViewRow row = grdHeader.Rows[e.NewSelectedIndex];
            this.txtNPIntNo.Value = row.Cells[0].Text;
            System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)row.Cells[5].Controls[1];
            bool.TryParse(lbl.Text, out bRes);
            this.chkBox.Checked = bRes;
            this.txtNatisTerminal.Text = row.Cells[1].Text;
            this.txtSendNatisFolder.Text = row.Cells[2].Text;
            this.txtSentNatisEmail.Text = row.Cells[3].Text;
            this.txtGetNatisFolder.Text = row.Cells[4].Text;
            System.Web.UI.WebControls.Label lblDate = (System.Web.UI.WebControls.Label)row.Cells[6].Controls[1];
            this.txtFirstRun.Text = lblDate.Text;
            this.txtInterval.Text = row.Cells[7].Text;
            this.txtNumberRuns.Text = row.Cells[8].Text;

            lblAction.Text = (string)GetLocalResourceObject("lblAction.Text2") + row.Cells[0].Text + (string)GetLocalResourceObject("lblAction.Text3") + row.Cells[1].Text;
        }
}   
}
