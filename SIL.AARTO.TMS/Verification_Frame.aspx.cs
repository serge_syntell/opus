using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a page that allows someone to validate the details of an individual frame
    /// </summary>
    public partial class Verification_Frame : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private int filmIntNo = 0;
        private int processType = 1;          //1 = Verification; 2 = Correction

        private int statusVerified = 500;
        private int statusReturnToNatis = 50;
        private int statusRejected = 999;
        
        //private int statusRejected = 955;
        private bool isNaTISLast = false;
        private FrameList frames = null;
        private bool adjAt100 = false;

        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string thisPageURL = "Verification_Frame.aspx";
        protected string thisPage = "Verification - Stage 2 (Frame)";
        protected bool _ISASD = false;
        protected int asdInvalidRejIntNo = 0;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            ddlRejection.Attributes.Add("OnChange", "doChange();");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            loginUser = userDetails.UserLoginName;
            //int 
            int userAccessLevel = userDetails.UserAccessLevel;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);

            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            title = gen.SetTitle(Session["drTitle"]);

            HtmlMeta metaKeywords = new HtmlMeta();
            metaKeywords.Attributes.Add("name", "Keywords");
            metaKeywords.Attributes.Add("content", keywords);
            Page.Header.Controls.Add(metaKeywords);

            HtmlMeta metaDescription = new HtmlMeta();
            metaDescription.Attributes.Add("name", "Description");
            metaDescription.Attributes.Add("content", description);
            Page.Header.Controls.Add(metaDescription);

            // Make sure there's an Authority selected
            if (Session["autIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No authority has been selected - please login correctly";
                return;
            }
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Make sure there's a film selected
            if (Session["filmIntNo"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No film has been selected - please login correctly";
                return;
            }
            this.filmIntNo = Convert.ToInt32(Session["filmIntNo"]);

            if (this.Session["FrameList"] == null)
            {
                lblError.Visible = true;
                lblError.Text = "No Frame list could be found";
                return;
            }
           
            this.frames = (FrameList)Session["FrameList"];

            if (this.frames.NoFrames == true)
            {
                // LMZ - 27-03-2007 release user lock
                Stalberg.TMS.FilmDB film = new Stalberg.TMS.FilmDB(connectionString);
                film.LockUser(0, loginUser);
                //Session["cvPhase"] = 2;
                this.Session.Remove("cvPhase");
                this.Session.Remove("FrameList");
                Response.Redirect("Verification_Final.aspx");
            }

            // What process are we running
            if (Session["processType"] != null)
                processType = Convert.ToInt32(Session["processType"]);

            //dls 090618 - add this into the session variable, so that we only ahve to do it once, not for every frame!
            if (Session["adjAt100"] == null)
            {
                this.adjAt100 = GetRuleFullAdjudicated();
                Session.Add("adjAt100", this.adjAt100);
            }
            else
            {
                this.adjAt100 = (bool)Session["adjAt100"];
            }

            //dls 090618 - add this into the session variable, so that we only ahve to do it once, not for every frame!
            if (Session["asdInvalidRejIntNo"] == null)
            {
                this.asdInvalidRejIntNo = GetASDInvalidRejIntNo();
                Session.Add("asdInvalidRejIntNo", this.asdInvalidRejIntNo);
            }
            else
            {
                this.asdInvalidRejIntNo = (int)Session["asdInvalidRejIntNo"];
            }

            this.isNaTISLast = (bool)Session["NaTISLast"];

            if (this.isNaTISLast)
            {
                this.statusVerified = 800;
                //this.statusReturnToNatis = 710;
                this.statusReturnToNatis = 740;   
            }

            // Hookup the page blocker JavaScript
            //Helper_Web.AddPageBlocker(this.btnAccept);

            if (!Page.IsPostBack)
            {
                this.adjAt100 = GetRuleFullAdjudicated();
                Session.Add("adjAt100", this.adjAt100);
              
                //btnAll.Visible = false;
                this.lblPageName.Text = thisPage;
                if (processType == 1)
                {
                    this.lblPageName.Text = "Verification - Stage 2 (Frame)";
                    //btnAll.Text = "Verify all violations";
                    //btnAll.Attributes.Add("onclick", "return confirm('Are you sure you want to confirm verification for all violations?');");
                }
                else
                {
                    this.lblPageName.Text = "Correction - Stage 1 (Frame)";
                    //btnAll.Visible = false;
                }

                this.PopulateVehicleMakes();
                this.PopulateVehicleTypes();
                this.PopulateRejectionReasons();
                //this.GetDuplicateRegNos();

                this.GetNextFrameToVerify();
            }

            //dls 090601 - need to add call to the js to re-enable the link buttons and display the message
            btnAccept.Attributes["onclick"] = "Action('VerifyFrame'); OnRemoteInvoke();";
            btnChange.Attributes["onclick"] = "AllowChange(); ";
            btnNatis.Attributes["onclick"] = "Action('ReturnToNatis'); OnRemoteInvoke();";
            LabelMessageInfo.Text = string.Empty;

        }

        private int GetASDInvalidRejIntNo()
        {
            RejectionDB rejDB = new RejectionDB(this.connectionString);
            //add ASD speed rejection reason
            string asdInvalidSetup = "ASD Camera Setup Invalid";

            return rejDB.UpdateRejection(0, asdInvalidSetup, "N", this.loginUser, 0, "Y");
        }

        private void GetNextFrameToVerify()
        {
            int frameIntNo = this.frames.Current;
            if (frameIntNo > 0)
            {
                this.ShowFrameDetails(frameIntNo);
                this.lblNavigation.Text = string.Format("Navigating - {0}", this.frames);
            }
            else
            {
                // LMZ - 27-03-2007 release user lock
                Stalberg.TMS.FilmDB film = new Stalberg.TMS.FilmDB(connectionString);
                film.LockUser(0, loginUser); 
                //Session["cvPhase"] = 2;
                this.Session.Remove("cvPhase");
                this.Session.Remove("FrameList");
                Response.Redirect("Verification_Final.aspx");
            }
        }

        private void ShowFrameDetails(int frameInt)
        {
            //btnAccept.Enabled = true;

            txtRegNo.ReadOnly = true;
            txtFirstSpeed.ReadOnly = true;
            txtSecondSpeed.ReadOnly = true;
            txtOffenceDate.ReadOnly = true;
            txtOffenceTime.ReadOnly = true;

            ddlRejection.Enabled = false;
            ddlVehMake.Enabled = false;
            ddlVehType.Enabled = false;

            lblFilmNo.Text = "Film no: " + this.frames.FilmNumber;

            if (frameInt != 0)
            {
                //btnNatis.Style["display"] = "none";
                btnNatis.Style["display"] = string.Empty;

                //StringBuilder sb = new StringBuilder();
                //sb.Append("MainImageViewer.aspx?FrameIntNo=");
                //sb.Append(frameInt);
                //sb.Append("&ScImIntNo=0&ImgType=A");
                //this.ifImages.Attributes["src"] = sb.ToString();

                // Image Viewer Control
                this.imageViewer1.FilmNo = this.frames.FilmNumber;
                this.imageViewer1.FrameIntNo = frameInt;
                this.imageViewer1.ScanImageIntNo = 0;
                this.imageViewer1.ImageType = "A";
                this.imageViewer1.FilmIntNo = this.filmIntNo;
                this.imageViewer1.Phase = this.Session["cvPhase"] == null ? 1 : Convert.ToInt32(this.Session["cvPhase"]);
                this.imageViewer1.Initialise();

                string strURLs = imageViewer1.GetParameters();

                ScriptManager.RegisterStartupScript(udpFrame,
                        udpFrame.GetType(), "UpdatePanelChangeImageViewer", "LoadImage(" + strURLs + ");", true);

                FrameDB frame = new FrameDB(connectionString);
                FrameDetails frameDet = frame.GetFrameDetails(frameInt);

                txtFrameNo.Text = frameDet.FrameNo;
                txtSeq.Text = frameDet.Sequence;
                txtRegNo.Text = frameDet.RegNo;

                //this.txtNatisreturncode.Text = frameDet.NRCDescr;
                this.txtNatisreturncode.Text = frameDet.NBReason;

                txtSecondSpeed.Text = frameDet.SecondSpeed.ToString();
                txtFirstSpeed.Text = frameDet.FirstSpeed.ToString();

                this.chkAllowContinue.Style["display"] = "none";
                this.chkAllowContinue.Checked = true;

                string month = frameDet.OffenceDate.Month.ToString();
                string day = frameDet.OffenceDate.Day.ToString();
                string hour = frameDet.OffenceDate.Hour.ToString();
                string minute = frameDet.OffenceDate.Minute.ToString();

                txtOffenceDate.Text = frameDet.OffenceDate.Year + "/" + month.PadLeft(2, '0') + "/" + day.PadLeft(2, '0'); 
                txtOffenceTime.Text = hour.PadLeft(2, '0') + ":" + minute.PadLeft(2, '0');

                ddlRejection.SelectedIndex = ddlRejection.Items.IndexOf(ddlRejection.Items.FindByValue(frameDet.RejIntNo.ToString()));
                ddlVehMake.SelectedIndex = ddlVehMake.Items.IndexOf(ddlVehMake.Items.FindByValue(frameDet.VMIntNo.ToString()));
                ddlVehType.SelectedIndex = ddlVehType.Items.IndexOf(ddlVehType.Items.FindByValue(frameDet.VTIntNo.ToString()));
             
                // LMZ - 2007-05-24 Rowversion check
                hidRowVersion.Value = frameDet.RowVersion.ToString ();
                this.hidPreUpdatedRejReason.Value = ddlRejection.SelectedItem.Text;
                this.hidPreUpdatedRegNo.Value = frameDet.RegNo;
                this.hidFrameIntNo.Value = frameInt.ToString();
                this.hidPreUpdatedAllowContinue.Value = frameDet.AllowContinue;

                //FT 100521 ASD
                if (frameDet.ASD2ndCameraIntNo > 0)
                {
                    tableASD.Visible = true;
                    tbASDGPSDatetime1.Text = string.Format("{0:yyyy-MM-dd HH:mm}", frameDet.ASDGPSDateTime1);
                    tbASDGPSDatetime2.Text = string.Format("{0:yyyy-MM-dd HH:mm}", frameDet.ASDGPSDateTime2);
                    tbASDLane1.Text = frameDet.ASDSectionStartLane.ToString();
                    tbASDLane2.Text = frameDet.ASDSectionEndLane.ToString();
                    lblError.Text = frameDet.ConfirmError;
                    chkAllowContinue.Checked = false;
                    this.chkAllowContinue.Style["display"] = string.Empty;

                    if (frameDet.RejIntNo == this.asdInvalidRejIntNo)
                    {
                        _ISASD = true;
                    }
                }
                else
                {
                    tableASD.Visible = false;
                }

                if (ddlRejection.SelectedItem.Text.Equals("None"))
                {
                    btnAccept.Value = "Accept";
                    ddlRejection.Style["display"] = "none";
                    lblRejection.Style["display"] = "none";
                    btnShowOwner.Visible = true;
                }
                else
                {
                    btnAccept.Value = "Reject";
                    ddlRejection.Style["display"] = string.Empty;
                    lblRejection.Style["display"] = string.Empty;
                    btnShowOwner.Visible = false;
                }

                if (frameDet.AllowContinue.Equals("N"))
                {
                    lblError.Text = frameDet.ConfirmError;
                    chkAllowContinue.Checked = false;
                    this.chkAllowContinue.Style["display"] = "";
                }

                LoadOwnerDetails(this.frames.Current);
                LoadDriverDetails(this.frames.Current);

                if (frameDet.FrameProxyFlag.Equals("Y"))
                {
                    this.addressPanel.Width = 800;
                        this.dragablePanel.Width =800;
                    
                    LoadProxyDetails(this.frames.Current);
                    lblProxy.Visible = true;
                    txtProxyID.Visible = true;
                    txtProxyName.Visible = true;
                    lblProxyName.Visible = true;
                    lblProxyIntNo .Visible = true;
                    lblProxyID.Visible = true;
                    lblProxyPostAddr.Visible = true;
                    lblProxyPostAddr2.Visible = true;
                    lblProxyPostAddr3.Visible = true;
                    lblProxyPostAddr4.Visible = true;
                    //lblProxyPostAddr5.Visible = true;
                    lblProxyStrAddr.Visible = true;
                    lblProxyStrAddr2.Visible = true;
                    lblProxyStrAddr3.Visible = true;
                    lblProxyStrAddr4.Visible = true;
                    lblProxtinitials.Visible = true;
                    lblProxypostalcode.Visible = true;
                    lblDriverIntNo.Visible =false;
                   
                    txtProxyPostAddr.Visible = true;
                    txtProxyPostAddr2.Visible = true;
                    txtProxyPostAddr3.Visible = true;
                    txtProxyPostAddr4.Visible = true;
                    //txtProxyPostAddr5.Visible = true;
                    txtProxyStrAddr.Visible = true;
                    txtProxyStrAddr2.Visible = true;
                    txtProxyStrAddr3.Visible = true;
                    txtProxyStrAddr4.Visible = true;
                    txtProxtinitials.Visible = true;
                    txtProxypostalcode.Visible = true;
                    //txtProxyIntNo.Visible = true;
               
                }
                else
                {
                    this.addressPanel.Width = 548;
                    this.dragablePanel.Width = 548;
                    lblProxy.Visible = false;
                    txtProxyID.Visible = false;
                    txtProxyName.Visible = false;
                    lblProxyName.Visible = false;
                    lblProxyIntNo .Visible = false;
                    lblProxyID.Visible = false;
                    lblProxyPostAddr.Visible = false;
                    lblProxyPostAddr2.Visible = false;
                    lblProxyPostAddr3.Visible = false;
                    lblProxyPostAddr4.Visible = false;
                    //lblProxyPostAddr5.Visible = false;
                    lblProxyStrAddr.Visible = false;
                    lblProxyStrAddr2.Visible = false;
                    lblProxyStrAddr3.Visible = false;
                    lblProxyStrAddr4.Visible = false;
                    lblProxtinitials.Visible =false;
                    lblProxypostalcode.Visible = false;
                    lblDriverIntNo.Visible = false;

                    txtProxyPostAddr.Visible = false;
                    txtProxyPostAddr2.Visible = false;
                    txtProxyPostAddr3.Visible = false;
                    txtProxyPostAddr4.Visible = false;
                    //txtProxyPostAddr5.Visible = false;
                    txtProxyStrAddr.Visible = false;
                    txtProxyStrAddr2.Visible = false;
                    txtProxyStrAddr3.Visible = false;
                    txtProxyStrAddr4.Visible = false;
                    txtProxtinitials.Visible = false;
                    txtProxypostalcode.Visible = false;

                    txtProxyIntNo.Visible = false;
                }

                txtNatisRegNo.Visible = false;
                txtNatisVehicleType.Visible = false;
                txtNatisVehicleMake.Visible = false;
                txtNatisRejreason.Visible = false;

                lblNatis.Visible = false;

                this.dragablePanel.Width = addressPanel.Width;

                //tblOwnerDetails.Visible = false;

                ////Check the rejection reason is displayed on 
                ////the page,if not ,display the rejection reason in textbox control
                //if (ddlRejection.SelectedIndex == -1 
                //    || ddlRejection.SelectedValue.Trim() == string.Empty)
                //{
                //    txtNatisRejreason.Visible = true;

                //    txtNatisRejreason.Text = frame.GetRejReasion(frameDet.RejIntNo);
                //}

                this.txtNatisreturncode.Visible = false;
                this.lblNatisReturnCode.Visible = false;

                switch (frameDet.FrameStatus)
                {
                    //case "150":
                    //    lblError.Text = "Error code received from NATIS. Peace Officer to action";
                    //    txtNoContinue.Visible = true;
                    //    txtNoContinue.Height = 42;
                    //    txtNoContinue.Width = 600;
                    //    lblNatis.Visible = false;

                        //dls 060605 - decode description of error
                        //NatisReturnCodeDetails nrc = frame.GetNRCDetails(frameDet.FrameNatisIndicator);

                        //string field = frame.NatisFieldDescr(frameDet.FrameNaTISErrorFieldList);

                        //txtNoContinue.Text = nrc.NRCDescr;

                       // if (frameDet.FrameNatisIndicator.Equals("V"))
                            //txtNoContinue.Text += " (" + field + ")";

                        //ddlRejection.Visible = true;
                        //btnAccept.Value = "Reject";
                        //txtNatisRegNo.Visible = false;
                        //txtNatisVehicleType.Visible = false;
                        //txtNatisVehicleMake.Visible = false;
                        //lblRejection.Visible = true;

                        //break;
                    case "751":
                    case "151":
                        txtNatisVehicleMake.Visible = true;
                        txtNatisVehicleType.Visible = false;
                        GetVehicleMake(frameDet.FrameNatisVMCode);
                        lblNatis.Visible = true;
                        txtNatisRegNo.Visible = false;
                        break;
                    case "752":
                    case "152":
                        txtNatisVehicleType.Visible = true;
                        GetVehicleType(frameDet.FrameNatisVTCode);
                        lblNatis.Visible = true;
                        txtNatisRegNo.Visible = false;
                        txtNatisVehicleMake.Visible = false;
                        break;
                    case "753":
                    case "153":
                        txtNatisVehicleType.Visible = true;
                        GetVehicleMake(frameDet.FrameNatisVMCode);
                        GetVehicleType(frameDet.FrameNatisVTCode);
                        txtNatisVehicleMake.Visible = true;
                        lblNatis.Visible = true;
                        txtNatisRegNo.Visible = false;
                        break;
                    case "754":
                    case "154":
                        txtNatisRegNo.Visible = true;
                        txtNatisRegNo.Text = frameDet.FrameNatisRegNo;
                        lblNatis.Visible = true;
                        //txtNoContinue.Visible = false;
                        break;
                    case "200":
                        lblNatis.Visible = false;
                        txtNatisRegNo.Visible = false;
                        txtNatisVehicleType.Visible = false;
                        txtNatisVehicleMake.Visible = false;
                        //txtNoContinue.Visible = false;
                        break;
                    case "155":
                    case "755":
                        this.txtNatisreturncode.Visible = true;
                        this.lblNatisReturnCode.Visible = true;
                        break;
                }

                //dls 090529 - if the frame is being verified before it has actually gone to Natis, it cannot be accepted and sent onto Adjudication
                //  - it must be sent back to Natis with the changed data.
                //  - there are no owner details yet, so hide the button to display them

                if (frameDet.FrameNatisRegNo.Equals(""))
                {
                    this.statusVerified = this.adjAt100 == true ? this.statusVerified : this.statusReturnToNatis;
                    btnShowOwner.Visible = false;
                }
                else
                {
                    btnShowOwner.Text = "Show Owner Details";
                }
                //only allow the user update the fields that are in error - there may be more than one that is incorrect
                if (frameDet.FrameNatisIndicator.Equals("!"))
                {
                    btnUpdateOwner.Enabled = true;
                    btnUpdateOwner.Text = "Update Owner/Driver Details";
                    btnShowOwner.CssClass = "NormalButtonRed";
                    btnShowOwner.Text = "Update Owner/Driver Details";

                    if (frameDet.FrameNaTISErrorFieldList.ToLower().Contains("proxy id no is blank"))
                    {
                        txtProxyID.ReadOnly = false;
                        txtProxyID.CssClass = "NormalBoldRed";

                        btnUpdateOwner.Text = "Update Proxy Details";
                        btnShowOwner.Text = "Update Proxy Details";
                    }
                    else if (frameDet.FrameNaTISErrorFieldList.ToLower().Contains("id no is blank"))
                    {
                        txtOwnerID.ReadOnly = false;                 
                        txtDriverID.ReadOnly = false;

                        txtOwnerID.CssClass = "NormalBoldRed";
                        txtDriverID.CssClass = "NormalBoldRed";
                    }
                    
                    if (frameDet.FrameNaTISErrorFieldList.ToLower().Contains("surname is blank"))
                    {
                        if (frameDet.FrameProxyFlag.Equals("Y"))
                        {
                            txtProxyName.ReadOnly = false;
                            txtProxyName.CssClass = "NormalBoldRed";
                            btnUpdateOwner.Text = "Update Proxy Details";
                            btnShowOwner.Text = "Update Proxy Details";
                        }
                        else
                        {
                            txtDriverName.ReadOnly = false;
                            txtOwnerName.ReadOnly = false;

                            txtDriverName.CssClass = "NormalBoldRed";
                            txtOwnerName.CssClass = "NormalBoldRed";
                        }
                    }
                    
                    if (frameDet.FrameNaTISErrorFieldList.ToLower().Contains("po addr1 is blank"))
                    {
                        if (frameDet.FrameProxyFlag.Equals("Y"))
                        {
                            txtProxyPostAddr.ReadOnly = false;
                            txtProxyPostAddr2.ReadOnly = false;
                            txtProxyPostAddr3.ReadOnly = false;
                            txtProxyPostAddr4.ReadOnly = false;

                            txtProxyPostAddr.CssClass = "NormalBoldRed";
                            btnUpdateOwner.Text = "Update Proxy Details";
                            btnShowOwner.Text = "Update Proxy Details";
                        }
                        else
                        {
                            txtDriverPostAddr.ReadOnly = false;
                            txtDriverPostAddr2.ReadOnly = false;
                            txtDriverPostAddr3.ReadOnly = false;
                            txtDriverPostAddr4.ReadOnly = false;

                            txtOwnerPostAddr.ReadOnly = false;
                            txtOwnerPostAddr2.ReadOnly = false;
                            txtOwnerPostAddr3.ReadOnly = false;
                            txtOwnerPostAddr4.ReadOnly = false;

                            txtDriverPostAddr.CssClass = "NormalBoldRed";
                            txtOwnerPostAddr.CssClass = "NormalBoldRed";
                        }
                    }
                    
                    if (frameDet.FrameNaTISErrorFieldList.ToLower().Contains("po code is blank"))
                    {
                        if (frameDet.FrameProxyFlag.Equals("Y"))
                        {
                            txtProxypostalcode.ReadOnly = false;
                            txtProxypostalcode.CssClass = "NormalBoldRed";
                            btnUpdateOwner.Text = "Update Proxy Details";
                            btnShowOwner.Text = "Update Proxy Details";
                        }
                        else
                        {
                            txtDriverpostalcode.ReadOnly = false;
                            txtOwnerPostalcode.ReadOnly = false;

                            txtDriverpostalcode.CssClass = "NormalBoldRed";
                            txtOwnerPostalcode.CssClass = "NormalBoldRed";
                        }
                    }
                }
                else
                {
                    btnUpdateOwner.Enabled = false;
                }

                hidStatusRejected.Value = this.statusRejected.ToString();
                hidStatusVerified.Value = this.statusVerified.ToString();
                hidStatusNatis.Value = this.statusReturnToNatis.ToString();
                hidUserName.Value = this.loginUser;

                Session["frameList"] = this.frames;
                //this.ModalPopupExtender1.X =( (Convert.ToUInt32(System .Windows.Forms.SystemInformation.PrimaryMonitorMaximizedWindowSize.Width)) - addressPanel.Width) / 2;
                //this.ModalPopupExtender1.Y = (System.Windows.Forms.SystemInformation.PrimaryMonitorMaximizedWindowSize.Height - addressPanel.Height ) / 2;

               
            }
            else
            {
                // LMZ - 27-03-2007 release user lock
                Stalberg.TMS.FilmDB film = new Stalberg.TMS.FilmDB(connectionString);
                film.LockUser(0, loginUser);
                Session["cvPhase"] = 2;
                Response.Redirect("Verification_Final.aspx");
            }
        }

        private bool GetRuleFullAdjudicated()
        {
            //tf 20090611 - get AuthorityRule for 100% adjuducated
            //AuthorityRulesDB arDB = new AuthorityRulesDB(connectionString);
            //AuthorityRulesDetails arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "0600", "Rule to ensure that 100% of frames are Adjudicated",
            //    0, "N", "Y - Yes; N - No(Default)", userName);

            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();

            arDetails.AutIntNo = this.autIntNo;
            arDetails.ARCode = "0600";
            arDetails.LastUser = loginUser;

            DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
            KeyValuePair<int, string> adj100 = ar.SetDefaultAuthRule();
            return adj100.Value.Equals("Y");
        }

        private void GetVehicleMake(string vmCode)
        {
            VehicleMakeDB vm = new VehicleMakeDB(connectionString);
            VehicleMakeDetails vmDet = vm.GetVehicleMakeDetailsByCode(vmCode, "N");

            txtNatisVehicleMake.Text = "Vehicle make code = " + vmCode;
            if (vmDet.VMDescr != null)
                txtNatisVehicleMake.Text = vmDet.VMDescr;
            else
            {
                //txtNoContinue.Height = 42;
                //txtNoContinue.Width = 600;
                //txtNoContinue.Text += "No matching vehicle make in database; ";
                txtNatisVehicleMake.Text = vmCode + " - No matching vehicle make in database";
            }
        }

        private void GetVehicleType(string vtCode)
        {
            VehicleTypeDB vt = new VehicleTypeDB(connectionString);
            VehicleTypeDetails vtDet = vt.GetVehicleTypeDetailsByCode(vtCode, "N");

            txtNatisVehicleType.Text = "Vehicle type code = " + vtCode;
            if (vtDet.VTDescr != null)
                txtNatisVehicleType.Text = vtDet.VTDescr;
            else
            {
                //txtNoContinue.Height = 42;
                //txtNoContinue.Width = 600;
                //txtNoContinue.Text += "No matching vehicle type in database; ";
                txtNatisVehicleType.Text = vtCode + " - No matching vehicle type in database";
            }
        }


        private void LoadProxyDetails(int frameIntNo)
        {
            FrameProxyDB proxy = new FrameProxyDB(connectionString);
            FrameProxyDetails proxyDet = proxy.GetFrameProxyDetailsByFrame(frameIntNo);

            string name = string.Empty;
           
            string id = string.Empty;
            //string postAddr = string.Empty;
            //string strAddr = string.Empty;

            try
            {
                name = proxyDet.FrPrxSurname.Trim();
            }
            catch { }

            try
            {
                id = proxyDet.FrPrxIDNumber.Trim();
            }
            catch { }

            try
            {
                txtProxyID.Text = id;
                txtProxyName.Text = name;
                txtProxyIntNo.Text = proxyDet.FrPrxIntNo.ToString();
                txtProxtinitials.Text = proxyDet.FrPrxInitials.Trim();
                txtProxyPostAddr.Text = proxyDet.FrPrxPOAdd1.Trim();
                txtProxyPostAddr2.Text = proxyDet.FrPrxPOAdd2.Trim();
                txtProxyPostAddr3.Text = proxyDet.FrPrxPOAdd3.Trim();
                txtProxyPostAddr4.Text = proxyDet.FrPrxPOAdd4.Trim();

                txtProxyStrAddr.Text = proxyDet.FrPrxStAdd1.Trim();
                txtProxyStrAddr2.Text = proxyDet.FrPrxStAdd2.Trim();
                txtProxyStrAddr3.Text = proxyDet.FrPrxStAdd3.Trim();
                txtProxyStrAddr4.Text = proxyDet.FrPrxStAdd4.Trim();

                txtProxypostalcode.Text = proxyDet.FrPrxStCode.Trim();

                this.hidProxyID.Value = proxyDet.FrPrxIntNo.ToString();
                //txtProxyIntNo.Enabled = false;

                txtProxyID.ReadOnly = true;
                txtProxyName.ReadOnly = true;
                txtProxyIntNo.ReadOnly = true;
                txtProxtinitials.ReadOnly = true;
                txtProxyPostAddr.ReadOnly = true;
                txtProxyPostAddr2.ReadOnly = true;
                txtProxyPostAddr3.ReadOnly = true;
                txtProxyPostAddr4.ReadOnly = true;
                txtProxyStrAddr.ReadOnly = true;
                txtProxyStrAddr2.ReadOnly = true;
                txtProxyStrAddr3.ReadOnly = true;
                txtProxyStrAddr4.ReadOnly = true;
                txtProxypostalcode.ReadOnly = true;

                txtProxyID.CssClass = "NormalBold";
                txtProxyName.CssClass = "NormalBold";
                txtProxyPostAddr.CssClass = "NormalBold";
                txtProxypostalcode.CssClass = "NormalBold";
            }
            catch { }

        }

        private void LoadDriverDetails(int frameIntNo)
        {
            FrameDriverDB driver = new FrameDriverDB(connectionString);

            FrameDriverDetails driverDet = driver.GetFrameDriverDetailsByFrame(frameIntNo);

            string name = string.Empty;
           
            string id = string.Empty;
            //string postAddr = string.Empty;
            //string strAddr = string.Empty;

            try
            {
                name = driverDet.FrDrvSurname.Trim();
            }
            catch { }           
            try
            {
                id = driverDet.FrDrvIDNumber.Trim();
            }
            catch { }

            try
            {
                txtDriverID.Text = id;
                txtDriverName.Text = name;
                txtDriverIntNo.Text = driverDet.FrDrvIntNo.ToString();
                txtDriverinitials.Text = driverDet.FrDrvInitials.Trim();
                txtDriverPostAddr.Text = driverDet.FrDrvPOAdd1.Trim();
                txtDriverPostAddr2.Text = driverDet.FrDrvPOAdd2.Trim();
                txtDriverPostAddr3.Text = driverDet.FrDrvPOAdd3.Trim();
                txtDriverPostAddr4.Text = driverDet.FrDrvPOAdd4.Trim();
                txtDriverPostAddr5.Text = driverDet.FrDrvPOAdd5.Trim();

                txtDriverStrAddr.Text = driverDet.FrDrvStAdd1.Trim();
                txtDriverStrAddr2.Text = driverDet.FrDrvStAdd2.Trim();
                txtDriverStrAddr3.Text = driverDet.FrDrvStAdd3.Trim();
                txtDriverStrAddr4.Text = driverDet.FrDrvStAdd4.Trim();

                txtDriverpostalcode.Text = driverDet.FrDrvStCode.Trim();

                this.hidDriverID.Value = driverDet.FrDrvIntNo.ToString();

                //txtDriverIntNo.Enabled = false;

                txtDriverID.ReadOnly = true;
                txtDriverName.ReadOnly = true;
                txtDriverIntNo.ReadOnly = true;
                txtDriverinitials.ReadOnly = true;
                txtDriverPostAddr.ReadOnly = true;
                txtDriverPostAddr2.ReadOnly = true;
                txtDriverPostAddr3.ReadOnly = true;
                txtDriverPostAddr4.ReadOnly = true;
                txtDriverPostAddr5.ReadOnly = true;
                txtDriverStrAddr.ReadOnly = true;
                txtDriverStrAddr2.ReadOnly = true;
                txtDriverStrAddr3.ReadOnly = true;
                txtDriverStrAddr4.ReadOnly = true;
                txtDriverpostalcode.ReadOnly = true;

                txtDriverID.CssClass = "NormalBold";
                txtDriverName.CssClass = "NormalBold";
                txtDriverPostAddr.CssClass = "NormalBold";
                txtDriverpostalcode.CssClass = "NormalBold";
            }
            catch { }
        }

        private void LoadOwnerDetails(int frameIntNo)
        {
            FrameOwnerDB owner = new FrameOwnerDB(connectionString);

            FrameOwnerDetails ownerDet = owner.GetFrameOwnerDetailsByFrame(frameIntNo);

            string name = string.Empty;
            
            string id = string.Empty;
            //string postAddr = string.Empty;
            //string strAddr = string.Empty;

            try
            {
                name = ownerDet.FrOwnSurname.Trim();
            }
            catch { }         

            try
            {
                id = ownerDet.FrOwnIDNumber.Trim();
            }
            catch { }

            try
            {
                txtOwnerID.Text = id;
                txtOwnerName.Text = name;
                txtOwnerinitials.Text = ownerDet.FrOwnInitials.Trim();

                txtOwnerIntNo.Text = ownerDet.FrOwnIntNo.ToString();
                
                txtOwnerPostAddr.Text = ownerDet.FrOwnPOAdd1.Trim();
                txtOwnerPostAddr2.Text = ownerDet.FrOwnPOAdd2.Trim();
                txtOwnerPostAddr3.Text = ownerDet.FrOwnPOAdd3.Trim();
                txtOwnerPostAddr4.Text = ownerDet.FrOwnPOAdd4.Trim();
                txtOwnerPostAddr5.Text = ownerDet.FrOwnPOAdd5.Trim();

                txtOwnerStrAddr.Text = ownerDet.FrOwnStAdd1.Trim();
                txtOwnerStrAddr2.Text = ownerDet.FrOwnStAdd2.Trim();
                txtOwnerStrAddr3.Text = ownerDet.FrOwnStAdd3.Trim();
                txtOwnerStrAddr4.Text = ownerDet.FrOwnStAdd4.Trim();
                
                txtOwnerPostalcode.Text = ownerDet.FrOwnPOCode.Trim();

                this.hidOwnerID.Value = ownerDet.FrOwnIntNo.ToString();

                //txtOwnerIntNo.Enabled = false;

                txtOwnerID.ReadOnly = true;
                txtOwnerName.ReadOnly = true;
                txtOwnerinitials.ReadOnly = true;
                txtOwnerIntNo.ReadOnly = true;
                txtOwnerPostAddr.ReadOnly = true;
                txtOwnerPostAddr2.ReadOnly = true;
                txtOwnerPostAddr3.ReadOnly = true;
                txtOwnerPostAddr4.ReadOnly = true;
                txtOwnerPostAddr5.ReadOnly = true;
                txtOwnerStrAddr.ReadOnly = true;
                txtOwnerStrAddr2.ReadOnly = true;
                txtOwnerStrAddr3.ReadOnly = true;
                txtOwnerStrAddr4.ReadOnly = true;
                txtOwnerPostalcode.ReadOnly = true;

                txtOwnerID.CssClass = "NormalBold";
                txtOwnerName.CssClass = "NormalBold";
                txtOwnerPostAddr.CssClass = "NormalBold";
                txtOwnerPostalcode.CssClass = "NormalBold";
            }
            catch { }
 
        }

        protected void PopulateVehicleMakes()
        {
            VehicleMakeDB make = new VehicleMakeDB(connectionString);

            SqlDataReader reader = make.GetVehicleMakeList("");
            ddlVehMake.DataSource = reader;
            ddlVehMake.DataValueField = "VMIntNo";
            ddlVehMake.DataTextField = "VMDescr";
            ddlVehMake.DataBind();

            reader.Close();
        }

        protected void PopulateVehicleTypes()
        {
            VehicleTypeDB type = new VehicleTypeDB(connectionString);

            SqlDataReader reader = type.GetVehicleTypeList(0, "");
            ddlVehType.DataSource = reader;
            ddlVehType.DataValueField = "VTIntNo";
            ddlVehType.DataTextField = "VTDescr";
            ddlVehType.DataBind();

            reader.Close();
        }

        protected void PopulateRejectionReasons()
        {
            RejectionDB reason = new RejectionDB(connectionString);

            SqlDataReader reader = reason.GetRejectionList();
            
            Dictionary<int, string> lookups =
                RejectionLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            while (reader.Read())
            {
                int rejIntNo = (int)reader["RejIntNo"];
                if (lookups.ContainsKey(rejIntNo))
                {
                    ddlRejection.Items.Add(new ListItem(lookups[rejIntNo], rejIntNo.ToString()));
                }
            }
            //ddlRejection.DataSource = reader;
            //ddlRejection.DataValueField = "RejIntNo";
            //ddlRejection.DataTextField = "RejReason";
            //ddlRejection.DataBind();

            reader.Close();
        }

        protected void btnShowOwner_Click(object sender, EventArgs e)
        {
            //if (btnShowOwner.Text.Equals("Show owner details"))
            //{
            //    tblOwnerDetails.Visible = true;
            //    //btnShowOwner.Text = "Hide owner details";
            //}
            //else
            //{
            //    btnShowOwner.Text = "Show owner details";
            //tblOwnerDetails.Visible = false;
            //}
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            this.frames.GetPreviousFrame();
            this.GetNextFrameToVerify();
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            this.frames.GetNextFrame(false);
            this.GetNextFrameToVerify();
        }

        protected void btnUpdateOwner_Click(object sender, EventArgs e)
        {
            try
            {
                Regex regex = new Regex("^[0-9]{4}$");
                if (!regex.IsMatch(txtOwnerPostalcode.Text.Trim() ))
                {
                    this.LabelMessageInfo.Text = "Incorrect owner postal code, update failed!";
                    
                    //txtOwnerPostalcode.ForeColor = ConsoleColor.Red;
                    txtOwnerPostalcode.Focus();
                    return;
                }

                if (!regex.IsMatch(txtDriverpostalcode.Text.Trim()))
                {
                    this.LabelMessageInfo.Text = "Incorrect driver postal code, update failed!";

                    //txtDriverpostalcode.ForeColor = ConsoleColor.Red;
                    txtDriverpostalcode.Focus();
                    return;
                }
                
                if (hidProxyID.Value != string.Empty)
                {
                    if (!regex.IsMatch(txtProxypostalcode.Text.Trim()))
                    {
                        this.LabelMessageInfo.Text = "Incorrect proxy postal code, update failed!";

                        //txtProxypostalcode.ForeColor = ConsoleColor.Red;
                        txtProxypostalcode.Focus();
                        return;
                    }

                    if (txtProxyID.Text.Trim().Length == 0)
                    {
                        this.LabelMessageInfo.Text = "Incorrect proxy ID, update failed!";
                        return;
                    }

                    if (txtProxyName.Text.Trim().Length == 0)
                    {
                        this.LabelMessageInfo.Text = "Incorrect proxy surnam, update failed!";
                        return;
                    }
                }

                if (txtOwnerID.Text.Trim().Length == 0)
                {
                    this.LabelMessageInfo.Text = "Incorrect owner ID, update failed!";
                    return;
                }

                if (txtOwnerName.Text.Trim().Length == 0)
                {
                    this.LabelMessageInfo.Text = "Incorrect owner name, update failed!";
                    return;
                }

                if (txtDriverID.Text.Trim().Length == 0)
                {
                    this.LabelMessageInfo.Text = "Incorrect driver ID, update failed!";
                    return;
                }

                if (txtDriverName.Text.Trim().Length == 0)
                {
                    this.LabelMessageInfo.Text = "Incorrect driver name, update failed!";
                    return;
                }

                FrameDriverDB driverDB = new FrameDriverDB(connectionString);
                FrameProxyDB proxyDB = new FrameProxyDB(connectionString);
                FrameOwnerDB ownerDB = new FrameOwnerDB(connectionString);

                int effectNo = 0;
                int frameIntNo = this.frames.Current;

                int intNo = Convert.ToInt32(hidOwnerID.Value);

                effectNo = ownerDB.UpdateFrameOwner(intNo, frameIntNo, txtOwnerName.Text.ToUpper(), txtOwnerinitials.Text.ToUpper(),
                    txtOwnerID.Text, txtOwnerPostAddr.Text,
                    txtOwnerPostAddr2.Text, txtOwnerPostAddr3.Text,
                    txtOwnerPostAddr4.Text, txtOwnerPostAddr5.Text,
                    txtOwnerPostalcode.Text, txtOwnerStrAddr.Text,
                    txtOwnerStrAddr2.Text, txtOwnerStrAddr3.Text,
                    txtOwnerStrAddr4.Text, loginUser);

                if (effectNo == -1)
                {
                    LabelMessageInfo.Text = "Owner Details Update Failed!";
                    return;
                }

                if (hidProxyID.Value != string.Empty)
                {
                    intNo = Convert.ToInt32(hidProxyID.Value);
                    effectNo = proxyDB.UpdateFrameProxy(intNo,frameIntNo, txtProxyName.Text.ToUpper(), txtProxtinitials.Text.ToUpper(),
                        txtProxyID.Text, txtProxyPostAddr.Text, txtProxyPostAddr2.Text, txtProxyPostAddr3.Text,
                        txtProxyPostAddr4.Text, txtProxyPostAddr4.Text,
                        txtProxypostalcode.Text, txtProxyStrAddr.Text,
                        txtProxyStrAddr2.Text, txtProxyStrAddr3.Text,
                        txtProxyStrAddr4.Text, loginUser);

                    if (effectNo == -1)
                    {
                        LabelMessageInfo.Text = "Proxy Details Update Failed!";
                        return;
                    }
                }

                intNo = Convert.ToInt32(hidDriverID.Value);
                effectNo = driverDB.UpdateFrameDriver(intNo,frameIntNo, txtDriverName.Text.ToUpper(), txtDriverinitials.Text.ToUpper(),
                    txtDriverID.Text, txtDriverPostAddr.Text, txtDriverPostAddr2.Text,
                    txtDriverPostAddr3.Text, txtDriverPostAddr4.Text, txtDriverPostAddr5.Text,
                    txtDriverpostalcode.Text, txtDriverStrAddr.Text, txtDriverStrAddr2.Text,
                    txtDriverStrAddr3.Text, txtDriverStrAddr4.Text, loginUser);

                if (effectNo == -1)
                {
                    LabelMessageInfo.Text = "Driver Details Update Failed!";
                    return;
                }

                if (hidProxyID.Value != string.Empty)
                    LabelMessageInfo.Text = "Proxy Details updated successfully!";
                else 
                    LabelMessageInfo.Text = "Owner Details updated successfully!";
                
            }
            catch (Exception ex)
            {
                LabelMessageInfo.Text = "Proxy Details Update Failed - " + ex.Message;
                return;
            }
        }
        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
           
        }
}
}
