using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Util;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;

namespace Stalberg.TMS
{
    public partial class AuditTrailReport : Page
    {
        // Fields
        private string connectionString = String.Empty;
        private int autIntNo;
        private string login;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Audit Trail Report";
        protected string thisPageURL = "AuditTrailReport.aspx";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                //2012-3-6 linda modified into a multi-language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                this.PopulateAuthorities();
                this.PopulateAuditReports();

                this.dtpAfter.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
                this.dtpBefore.Text = DateTime.Now.AddDays(-1).ToString(DATE_FORMAT);
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));
            //reader.Close();
            this.TicketNumberSearch1.AutIntNo = autIntNo;
        }

        private void PopulateAuditReports()
        {
            //2012-3-7 linda modified dropdownlist items into multi-language
            this.ddlAuditReport.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlAuditReport.Items1"), string.Empty));
            this.ddlAuditReport.Items.Insert(1, new ListItem((string)GetLocalResourceObject("ddlAuditReport.Items2"), "Notice"));
            this.ddlAuditReport.Items.Insert(2, new ListItem((string)GetLocalResourceObject("ddlAuditReport.Items3"), "Charge"));
            this.ddlAuditReport.Items.Insert(3, new ListItem((string)GetLocalResourceObject("ddlAuditReport.Items4"), "Driver"));
            this.ddlAuditReport.Items.Insert(4, new ListItem((string)GetLocalResourceObject("ddlAuditReport.Items5"), "Frame"));
            this.ddlAuditReport.SelectedIndex = 0;
            return;
        }

        //protected void btnHideMenu_Click(object sender, EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (this.ddlAuthority.SelectedValue.Equals(string.Empty) || this.ddlAuthority.SelectedIndex < 0)
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            if (this.ddlAuditReport.SelectedValue.Equals(string.Empty) || this.ddlAuditReport.SelectedIndex < 1)
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            if (this.dtpAfter.Text.Trim().Length == 0 && this.dtpBefore.Text.Trim().Length == 0 &&
                this.txtRegNo.Text.Equals("") && this.txtTicketNo.Text.Equals(""))
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                return;
            }

            if (Convert.ToDateTime(dtpAfter.Text) > Convert.ToDateTime(dtpBefore.Text))
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                return;
            }

            this.lblError.Text = string.Empty;
            int selectedAutIntNo = int.Parse(this.ddlAuthority.SelectedValue);

            // FBJ 2009-02-25 - New Java based reporting engine
            //ReportingUtil util = new ReportingUtil(this.connectionString, this.login);
            //string url = util.GetReportUrl(Request.Url, "AuditTrail");

            PageToOpen pto = new PageToOpen(this.Page, "AuditTrailReportViewer.aspx");
            pto.AddParameter("TicketNo", this.txtTicketNo.Text);
            pto.AddParameter("RegNo", this.txtRegNo.Text);
            pto.AddParameter("DBAudit", this.ddlAuditReport.Text);
            pto.AddParameter("AutIntNo", selectedAutIntNo);
            pto.AddParameter("Before", this.dtpBefore.Text.Trim().Length == 0 ? "1900-01-01" : (DateTime.Parse(this.dtpBefore.Text.Trim()).ToString("yyyy-MM-dd")));
            pto.AddParameter("After", this.dtpAfter.Text.Trim().Length == 0 ? "1900-01-01" : (DateTime.Parse(this.dtpAfter.Text.Trim()).ToString("yyyy-MM-dd")));
            Helper_Web.BuildPopup(pto);
        }

        protected void ddlAuditReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlAuditReport.Text.IndexOf("Frame") >= 0)
            {
                this.lblTicketNo.Text = "Film No:";
                this.lblRegistration.Text = "Frame No:";
                this.lblPartialTicketNo.Visible = false;
                this.lblPartialOr.Visible = false;
                this.TicketNumberSearch1.Visible = false;
            }
            else
            {
                this.lblTicketNo.Text = "Ticket No:";
                this.lblRegistration.Text = "Registration No:";
                this.lblPartialTicketNo.Visible = true;
                this.lblPartialOr.Visible = true;
                this.TicketNumberSearch1.Visible = true;
            }

            this.txtTicketNo.Enabled = true;
            this.txtTicketNo.BackColor = Color.White;
            this.txtRegNo.Enabled = true;
            this.txtRegNo.BackColor = Color.White;
        }

        public void NoticeSelected(object sender, EventArgs e)
        {
            this.txtTicketNo.Text = this.TicketNumberSearch1.TicketNumber;
        }

        protected void ddlAuthority_SelectedIndexChanged(object sender, EventArgs e)
        {
                this.TicketNumberSearch1.AutIntNo = int.Parse(this.ddlAuthority.SelectedValue);
            
        }
    }
}


