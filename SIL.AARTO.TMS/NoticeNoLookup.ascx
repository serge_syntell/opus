﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Stalberg.TMS.NoticeNoLookup" Codebehind="NoticeNoLookup.ascx.cs" %>
    <script type="text/javascript">
        function AutoQueue() {
            var NPrefix = document.getElementById("<%=txtNoticePrefix.ClientID%>").value;
            if (NPrefix.toString().length >= 2) {
                document.getElementById("<%=txtNumber.ClientID%>").focus();
            }
        }
    </script>
<table width="100%">
    <tr>
        <td align="center" valign="middle" class="NormalBold">
            <asp:Label ID="lbLookUp" runat="server" Text="<%$Resources:lbLookUp.Text %>"></asp:Label>
        </td>
        <td align="right">
            <asp:Button ID="btnSearch" runat="server" Text="<%$Resources:btnSearch.Text %>" Width="80px" 
                OnClick="btnSearch_Click" CssClass="NormalButton" 
                UseSubmitBehavior="False"/>
        </td>
    </tr>
    <tr align="center">
        <td colspan="2">
            <table class="Normal">
                <tr style="height:22">
                    <td width="250px" align="left">
                        <asp:Label ID="lbTickSequenceNo" runat="server" Text="<%$Resources:lbTickSequenceNo.Text %>" CssClass="NormalBold"></asp:Label>
                    </td>
                    <td align="left" width="541px">
                        <asp:Panel ID="mypanel2TickSequenceNo" Enabled="true" runat="server" Height="16px">
                            <asp:TextBox ID="txtNoticePrefix" runat="server" Width="50px" onfocus="SetControlsStatus(this);" onkeyup="AutoQueue()"></asp:TextBox>
                            <asp:TextBox ID="txtNumber" runat="server" Width="60px" MaxLength="6" onfocus="SetControlsStatus(this);"></asp:TextBox>
                            <asp:TextBox ID="txtAuthCode" runat="server" Width="84px" 
                                onfocus="SetControlsStatus(this);" MaxLength="9"></asp:TextBox>
                            <asp:TextBox ID="txtCDV" runat="server" Width="45px" Enabled="false" onfocus="SetControlsStatus(this);"></asp:TextBox>
                            </asp:Panel>
                    </td>
                </tr>
                <tr align="center" style="height:22">
                    <td align="left">
                        <asp:Label ID="lbEasyPayNo" runat="server" Text="<%$Resources:lbEasyPayNo.Text %>" CssClass="NormalBold"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Panel ID="mypanel2EasyPayNo" Enabled="true" runat="server" Width="212px" Height="16px">
                            <asp:TextBox ID="txtEasyPayNo" runat="server" Width="215px" CssClass="Normal" onfocus="SetControlsStatus(this);"></asp:TextBox>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:Panel ID="pnlGrid" runat="server" Width="100%" Visible="true">
                            <asp:GridView ID="grdHeader" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="Normal" GridLines="Horizontal" ShowFooter="True" 
                                OnSelectedIndexChanging="grdHeader_SelectedIndexChanging" 
                                DataKeyNames="NotTicketNo">
                                <FooterStyle CssClass="CartListHead" />
                                <Columns>
                                    <asp:BoundField DataField="NotTicketNo_Enquiry" HeaderText="<%$Resources:grdHeader.HeaderText1 %>" />
                                    <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:grdHeader.HeaderText2 %>" />
                                  <asp:BoundField DataField="Name" HeaderText="<%$Resources:grdHeader.HeaderText3 %>" />
                                    <asp:CommandField HeaderText="<%$Resources:grdHeader.HeaderText4 %>" ShowSelectButton="true">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CommandField>
                                </Columns>
                                <HeaderStyle CssClass="CartListHead" />
                                <AlternatingRowStyle CssClass="CartListItemAlt" />
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>
                        &nbsp;</td>
                </tr>
            </table>
            <input id="LookupCondition" type="hidden"  value="noticeNoLookup_mypanel2TickSequenceNo" runat="server"/>
        </td>
    </tr>
</table>
