﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.Imaging;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using System.IO;
using Stalberg.TMS;

namespace SIL.AARTO.TMS
{
    public partial class ViewOffenceDetail_HistoryReport : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Notice Detail Enquiry";
        protected string thisPageURL = "ViewOffenceDetail_HistoryReport.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticeEnquiry_History");
            if (reportPage.Equals(string.Empty))
            {
                //int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiry.dplx", "NoticeEnquiry", "system");
                int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticeEnquiry_CoCT_History.dplx", "NoticeEnquiry_History", "system", "");
                reportPage = "NoticeEnquiry_CoCT_History.dplx";
            }

            string path = Server.MapPath("reports/" + reportPage);
            DocumentLayout doc = new DocumentLayout(path);


            if (Request.QueryString["NotIntNo"] == null)
            {
                Response.Write((string)GetLocalResourceObject("strWriteMsg"));
                Response.Flush();
                Response.End();
                return;
            }

            try
            {
               

                int nNotIntNo = Convert.ToInt32(Request.QueryString["NotIntNo"].ToString());

                StoredProcedureQuery query = (StoredProcedureQuery)doc.GetQueryById("Query");
                //Query query2 = (Query)doc.GetQueryById("Query2");
                StoredProcedureQuery query3 = (StoredProcedureQuery)doc.GetQueryById("Query3");
                query.ConnectionString = this.connectionString;
               // query2.ConnectionString = this.connectionString;
                query3.ConnectionString = this.connectionString;
                ParameterDictionary parameters = new ParameterDictionary();
                parameters.Add("NotIntNo", nNotIntNo);

                Document report = doc.Run(parameters);

                byte[] buffer = report.Draw();

                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite(buffer);
                Response.End();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Response.End();
            }
        }

      

    }
}