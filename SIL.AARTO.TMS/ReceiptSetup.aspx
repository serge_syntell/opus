<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.ReceiptSetup" Codebehind="ReceiptSetup.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                   <%-- <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead">Options</asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="Add ">
                                    </asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="Delete" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>--%>
            </td>
            <td valign="top" align="left" width="100%" colspan="1">
                <table height="482" width="100%" border="0">
                    <tr>
                        <td valign="top" style="height: 47px">
                            <p align="center">
                                <asp:Label ID="lblPageName" Text="<%$Resources:lblPageName.Text %>" runat="server" CssClass="ContentHead"></asp:Label></p>
                            <p>
                                <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="panelGeneral" runat="server" Width="100%" DefaultButton="btnAddAccountCharge">
                                <table id="Table2" height="125" cellspacing="1" cellpadding="1" border="0">
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <%--<tr>
                                            <td>
                                                <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold">Select local authority:</asp:Label></td>
                                            <td>
                                                <asp:DropDownList ID="ddlSelectLA" runat="server" Width="383px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged">
                                                </asp:DropDownList></td>
                                            <td>
                                            </td>
                                        </tr>--%>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblSelectAccount"  Text="<%$Resources:lblSelectAccount.Text %>" runat="server" CssClass="NormalBold"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSelectAccount" runat="server" Width="384px" CssClass="Normal"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label10" runat="server" Text="<%$Resources:lblSelectCharge.Text %>" CssClass="NormalBold"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSelectChargeType" runat="server" Width="383px" CssClass="Normal"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label11" runat="server" Width="382px" CssClass="Normal"  Text="<%$Resources:lblComment.Text %>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddAccountCharge" runat="server" CssClass="NormalButton" OnClick="btnAddAccountCharge_Click"
                                                Text="<%$Resources:btnAddAccountCharge.Text %>" Width="113px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="panelCashierAccount" runat="server" CssClass="Normal" Width="100%">
                                &nbsp;<br />
                                <p align="center">
                                    <asp:Label ID="labelCashiers" runat="server" Text="<%$Resources:labelCashiers.Text %>" CssClass="ContentHead"></asp:Label>&nbsp;</p>
                                <p align="center">
                                    <asp:Label ID="lblCashBoxMsg" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                                <p align="center" class="NormalBold">
                                <!-- 
                                    Cash Box to set for User:
                                    <asp:DropDownList ID="ddlCashBox" runat="server" CssClass="Normal">
                                    </asp:DropDownList>
                                    &nbsp;&nbsp;
                                    -->
                                    <asp:Label ID="lblUserMark" runat="server" Text="<%$Resources:lblUserMark.Text %>"></asp:Label>
                                    <asp:DropDownList ID="ddlUser" runat="server" CssClass="Normal">
                                    </asp:DropDownList>
                                </p>
                                <p align="center" class="NormalBold">
                                    <asp:Button ID="btnAssign" runat="server" Text="<%$Resources:btnAssign.Text %>" CssClass="NormalButton" OnClick="btnAssign_Click" />
                                </p>
                                <p align="center">
                                    <asp:DataGrid ID="gridCashiers" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                                        ShowFooter="True" CellPadding="5" OnItemCommand="gridCashiers_ItemCommand">
                                        <HeaderStyle CssClass="CartListHead" />
                                        <Columns>
                                            <asp:BoundColumn DataField="UserIntNo" HeaderText="UserIntNo" Visible="False">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" Wrap="False" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CBUIntNo" HeaderText="CBUIntNo" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Name" HeaderText="<%$Resources:gridCashiers.Name.HeaderText %>">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" Wrap="False" />
                                            </asp:BoundColumn>
                                         <%--   <asp:BoundColumn DataField="UGName" HeaderText="<%$Resources:gridCashiers.UGName.HeaderText %>">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" Wrap="False" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="USGName" HeaderText="<%$Resources:gridCashiers.USGName.HeaderText %>">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" Wrap="False" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="CashBoxName" HeaderText="<%$Resources:gridCashiers.CashBoxName.HeaderText %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="UserCountCashBox" HeaderText="<%$Resources:gridCashiers.UserCountCashBox.HeaderText %>"></asp:BoundColumn>
                                            <asp:ButtonColumn CommandName="Check" HeaderText="<%$Resources:gridCashiers.Check.HeaderText %>" Text="<%$Resources:gridCashiers.Check.Text %>">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Right" Wrap="False" />
                                            </asp:ButtonColumn>
                                            <asp:ButtonColumn CommandName="Select" HeaderText="<%$Resources:gridCashiers.Select.HeaderText %>" Text="<%$Resources:gridCashiers.Select.Text %>"></asp:ButtonColumn>
                                        </Columns>
                                        <FooterStyle CssClass="CartListHead" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" Wrap="False" />
                                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                    </asp:DataGrid>&nbsp;
                                </p>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
            </td>
            <td valign="top" align="left" width="100%">
            </td>
        </tr>
    </table>
    <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
