<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.CourtJudgementRecord" Codebehind="CourtJudgementRecord.aspx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
</head>
<body style="margin: 0px 0px 0px 0px; background: <%=backgroundImage %>;">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="6000">
        </asp:ScriptManager>
        <table style="height: 10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" style="width: 100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="upd" runat="server">
                        <ContentTemplate>
        <table style="height: 85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" alt="" width="167" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" colspan="1" style="width: 100%; text-align: center">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    
                            <asp:Label ID="lblError" CssClass="NormalRed" runat="server" />
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%" CssClass="Normal">
                                <table style="border-style: none" class="Normal">
                                    <tr>
                                        <th class="NormalBold" align="left">
                                            <asp:Label ID="lblCourt" runat="server" Text="<%$Resources:lblCourt.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:DropDownList ID="cboCourt" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboCourt_SelectedIndexChanged" CssClass="Normal" Width="203px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="left" class="NormalBold">
                                            <asp:Label ID="lblCourtRoom" runat="server" Text="<%$Resources:lblCourtRoom.Text %>"></asp:Label></th>
                                        <td>
                                            <asp:DropDownList ID="cboCourtRoom" runat="server" CssClass="Normal" Width="203px" AutoPostBack="True" OnSelectedIndexChanged="cboCourtRoom_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="<%$Resources:cboCourtRoomItem.Text %>"></asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <th class="NormalBold" align="left">
                                            <asp:Label ID="lblCourtDates" runat="server" Text="<%$Resources:lblCourtDates.Text %>"></asp:Label>
                                        </th>
                                        <td>
                                            <asp:DropDownList ID="cboCourtDates" runat="server" CssClass="Normal" Width="203px" AutoPostBack="True" OnSelectedIndexChanged="cboCourtDates_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="<%$Resources:cboCourtDatesItem.Text %>"></asp:ListItem>
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <th class="NormalBold" align="left">
                                            &nbsp;</th>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;" colspan="2" class="NormalBold">
                                            <asp:Button ID="btnPrintJudgements" runat="server" Text="<%$Resources:btnPrintJudgements.Text %>" CssClass="NormalButton"
                                                OnClick="btnPrintJudgements_Click" />
                                    </tr>
                                </table>
                            </asp:Panel>
                        
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" style="width: 100%">
                </td>
            </tr>
        </table>
        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" style="vertical-align: middle;" />&nbsp;Loading...</p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
        <table style="height: 5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width: 100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
