<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.SuspenseAccountUndoAssignment" Codebehind="SuspenseAccountUndoAssignment.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title%>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                     </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1" style="text-align: center">
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                    </asp:Panel>
                    <asp:UpdatePanel ID="udpSuspenseAccount" runat="server">
                        <ContentTemplate>
                            <p>
                                <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>&nbsp;</p>
                            <asp:Panel ID="pnlDetails" runat="server" Width="100%">
                                <fieldset>
                                    <legend>
                                        <asp:Label ID="Label3" runat="server" Text="<%$Resources:lblAssignedNotice.Text %>"></asp:Label></legend>
                                    <table border="0" class="Normal">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="<%$Resources:lblTicketNum.Text %>"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtTicketNo" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: right;">
                                                <asp:Button ID="btnSearch" Text="<%$Resources:btnSearch.Text %>" runat="server" CssClass="NormalButton" OnClick="btnSearch_Click"
                                                    Width="100px" /></td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="grdNotices" runat="server" AutoGenerateColumns="False" CssClass="Normal"
                                        ShowFooter="True" CellPadding="4" OnRowCreated="grdNotices_RowCreated" OnSelectedIndexChanged="grdNotices_SelectedIndexChanged">
                                        <FooterStyle CssClass="CartListHead" />
                                        <Columns>
                                            <asp:BoundField DataField="TicketNumber" HeaderText="<%$Resources:grdNotices.HeaderText %>" />
                                            <asp:TemplateField HeaderText="<%$Resources:grdNotices.HeaderText1 %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("OffenceDate") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$Resources:grdNotices.HeaderText2 %>">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ReceiptDate") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField HeaderText="<%$Resources:grdNotices.HeaderText3 %>" ShowSelectButton="True" SelectText="<%$Resources:grdNotices.SelectText %>">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:CommandField>
                                        </Columns>
                                        <HeaderStyle CssClass="CartListHead" />
                                    </asp:GridView>
                                </fieldset>
                            </asp:Panel>
                            <asp:Panel ID="pnlConfirm" runat="server" Width="100%" Visible="False">
                                <table style="border-style: none;">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblConfirm" runat="server" CssClass="Normal" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:Button ID="btnNo" Text="<%$Resources:btnNo.Text %>" runat="server" CssClass="NormalButton" OnClick="btnNo_Click"
                                                Width="70px" />
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Button ID="btnYes" Text="<%$Resources:btnYes.Text %>" runat="server" CssClass="NormalButton" OnClick="btnYes_Click"
                                                Width="70px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label5"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                </td>
                <td valign="top" align="left" width="100%">
                </td>
            </tr>
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
