<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.PrintSecondNotice" Codebehind="PrintSecondNotice.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>  
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />
    <style type="text/css">
        .style1
        {
            height: 91px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    &nbsp;
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table cellspacing="0" cellpadding="0" border="0" height="90%" width="100%">
                        <tr>
                            <td valign="top" style="height: 49px; width: 817px;">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="876px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="width: 817px">
                                <asp:UpdatePanel ID="upd" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlGeneral" runat="server">
                                              <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" 
                                                  EnableViewState="false"></asp:Label>
                                              <br />
                                              <asp:Label ID="lblErrorForPrint" runat="Server" CssClass="NormalRed" 
                                                            EnableViewState="False"></asp:Label>
                                            <table style="width: 877px">
                                               <tr>
                                                    <td style="width: 343px; height: 2px">
                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                                    <td style="width: 260px; height: 2px" valign="middle">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px">
                                                        </asp:DropDownList></td>
                                                    <td style="height: 2px; width: 549px;" valign="middle">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px">
                                                        <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="203px" Text="<%$Resources:lblSequenceNum.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="height: 26px; width: 255px;">
                                                        <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                                    </td>
                                                    <td style="width: 549px; height: 26px">
                                                        <asp:Button ID="btnPrint" runat="server" CssClass="NormalButton"   Enabled=""
                                                            OnClick="btnPrint_Click" Text="<%$Resources:btnPrint.Text %>" Width="130px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px">
                                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPrint.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 255px; height: 43px;" valign="middle">
                                                        <asp:TextBox ID="txtPrint" runat="server" Width="215px"></asp:TextBox></td>
                                                    <td style="height: 43px; width: 549px;" valign="middle">
                                                        &nbsp</td>
                                               </tr>
                                                  <tr>
                                                    <td rowspan="2" >
                                                        <asp:CheckBox ID="chkShowAll" runat="server" AutoPostBack="True" 
                                                            CssClass="NormalBold" OnCheckedChanged="chkShowAll_CheckedChanged" 
                                                            Text="<%$Resources:chkShowAll.Text %>" Width="202px" />
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblDateFrom" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateFrom.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:TextBox ID="dateFrom" runat="server" autocomplete="off" CssClass="Normal" 
                                                            Height="20px" UseSubmitBehavior="False" />
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" Format="yyyy-MM-dd" 
                                                            TargetControlID="dateFrom">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td rowspan="2"  valign="middle">
                                                        <asp:Button ID="btnRefresh" runat="server" CssClass="NormalButton" 
                                                            onclick="btnRefresh_Click" Text="<%$Resources:btnRefresh.Text %>" Width="130px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  valign="middle" class="style1">
                                                        <asp:Label ID="lblDateTo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateTo.Text %>"></asp:Label>
                                                    </td>
                                                    <td  valign="middle" class="style1">
                                                        <asp:TextBox ID="dateTo" runat="server" autocomplete="off" CssClass="Normal" 
                                                            Height="20px" UseSubmitBehavior="False" />
                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd" 
                                                            TargetControlID="dateTo">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr style="display:none">
                                                    <td style="width: 160px">
                                                        &nbsp; </td>
                                                    <td style="width: 255px; height: 43px;" valign="middle">
                                                        &nbsp;</td>
                                                    <td style="height: 43px; width: 549px;" valign="middle">
                                                        &nbsp; </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                       
                                        <asp:DataGrid ID="dgPrintrun" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                            CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgPrintrun_ItemCommand"
                                            CssClass="Normal" width="800px" 
                                            onitemcreated="dgPrintrun_ItemCreated" 
                                            onitemdatabound="dgPrintrun_ItemDataBound" OnPageIndexChanged="dgPrintrun_PageIndexChanged">
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="PrintFile" HeaderText="<%$Resources:dgPrintrun.HeaderText %>"></asp:BoundColumn>
                                                 <%--//2014-01-21 Heidi fixed bug for Format is not correct(5103)--%>
                                                <asp:BoundColumn DataField="NoOFNotices" HeaderText="<%$Resources:dgPrintrun.HeaderText1 %>">
                                                    <ItemStyle  HorizontalAlign="Right" />
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                    <%--    <asp:LinkButton ID="btnPrintNotices" runat="server" CausesValidation="false" 
                                                            CommandName="PrintNotices" Text="Print notices"></asp:LinkButton>--%>
                                                              <asp:HyperLink runat="server" ID="btnPrintNotices"  NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.PrintFile", "SecondNoticeViewer.aspx?printfile={0}&printType=PrintSecondNotice") %>'
                                                            Target="_blank"  Text="<%$Resources:dgPrintrunItem.Text %>" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--<asp:HyperLinkColumn Text="Print notices" Target="_blank" DataNavigateUrlField="PrintFile"
                                                    DataNavigateUrlFormatString="SecondNoticeViewer.aspx?printfile={0}"></asp:HyperLinkColumn>--%>
                                                   <%--  <asp:TemplateColumn HeaderText="Print" >
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="btnPrintNotices"  NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.PrintFile", "SecondNoticeViewer.aspx?printfile={0}") %>'
                                                            Target="_blank" Text="Print Notices" ID="hlPrint" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>--%>
<%--                                             <asp:BoundColumn DataField="PrintDate" HeaderText="<%$Resources:dgPrintrun.HeaderText2 %>"></asp:BoundColumn>--%>
                                                 <%--//2014-01-21 Heidi fixed bug for Format is not correct(5103)--%>
                                                 <asp:BoundColumn DataField="PrintDate" HeaderText="<%$Resources:dgPrintrun.HeaderText2 %>"
                                                    DataFormatString="{0:yyyy/MM/dd HH:mm}">
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnUpdate" runat="server" CausesValidation="False" 
                                                            CommandName="Select" Text="<%$ Resources:btnUpdate.Text %>" Enabled="<%# false %>"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="NotPrint2ndNoticeDate" HeaderText="Print Date" Visible="False">
                                                </asp:BoundColumn>
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>
                                        <pager:AspNetPager id="dgPrintrunPager" runat="server" 
                                            showcustominfosection="Right" width="800px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                              FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;" PageSize="10"  
                                              onpagechanged="dgPrintrunPager_PageChanged" UpdatePanelId="upd"></pager:AspNetPager>
                                        <asp:Panel ID="pnlUpdate" runat="server" Height="50px" Width="125px">
                                            <table style="width: 495px">
                                                <tr>
                                                    <td style="height: 20px">
                                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblPrintFile.Text %>"></asp:Label></td>
                                                    <td style="height: 20px">
                                                        <asp:Label ID="lblPrintFile" runat="server" CssClass="NormalBold" Width="348px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnUpdateStatus" runat="server" CssClass="NormalButton" OnClick="btnUpdateStatus_Click"
                                                            Text="<%$Resources:btnUpdateStatus.Text %>" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <br />
                                        <asp:Label ID="lblInstruct" runat="server" Width="465px" BorderStyle="Inset" CssClass="CartListHead"
                                            Height="18px" EnableViewState="false" Text="<%$Resources:lblInstruct.Text %>"></asp:Label></td>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                             <%--   <asp:UpdateProgress ID="udp" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label4"
                                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>--%>
                            </td>
                        </tr>
                        <tr>
                            <td> <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label4"
                                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
