<%@ Control Language="C#" AutoEventWireup="true"    Inherits="Stalberg.TMS.TicketNumberSearch" Codebehind="TicketNumberSearch.ascx.cs" %>
<script type="text/javascript">
    function AutoQueue() {
        var NPrefix = document.getElementById("<%=txtNoticePrefix.ClientID%>").value;
        if (NPrefix.toString().length >= 2) {
            document.getElementById("<%=txtNumber.ClientID%>").focus();
        }
    }
    </script>
<table width="100%">
    <tr>
        <td>
            <asp:TextBox ID="txtNoticePrefix" runat="server" Width="50px" CssClass="Normal" TabIndex="1" onkeyup="AutoQueue()"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="txtNumber" runat="server" Width="60px" CssClass="Normal" MaxLength="6" TabIndex="2"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="txtAuthCode" runat="server" Width="90px" CssClass="Normal" 
                TabIndex="3"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="txtCDV" runat="server" Width="50px" CssClass="Normal" Enabled="false" TabIndex="4"></asp:TextBox>
        </td>
        <td>
            <asp:Button ID="btnSearch" runat="server" Width="80px" Text="<%$Resources:UCTNbtnSearch.Text%>" CssClass="NormalButton"
                OnClick="btnSearch_Click" TabIndex="5"></asp:Button>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td colspan="3">
            <asp:Panel ID="pnlGrid" runat="server" Width="100%" Visible="true">
                <asp:GridView ID="grdHeader" runat="server" AutoGenerateColumns="False" CellPadding="3"
                    CssClass="Normal" GridLines="Horizontal" ShowFooter="True" OnSelectedIndexChanging="grdHeader_SelectedIndexChanging">
                    <FooterStyle CssClass="CartListHead" />
                    <Columns>
                        <asp:BoundField DataField="NotTicketNo" HeaderText="<%$Resources:UCTNGrdHeader.NotTicketNo.HeaderText %>" />
                        <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:UCTNGrdHeader.NotRegNo.HeaderText %>" />
                        <asp:BoundField DataField="Name" HeaderText="<%$Resources:UCTNGrdHeader.Name.HeaderText %>" />
                        <asp:CommandField HeaderText="<%$Resources:UCTNGrdHeader.Command.HeaderText %>" EditText="<%$Resources:UCTNGrdHeader.Command.HeaderText %>" SelectText="<%$Resources:UCTNGrdHeader.Command.HeaderText %>" ShowSelectButton="true">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:CommandField>
                    </Columns>
                    <HeaderStyle CssClass="CartListHead" />
                    <AlternatingRowStyle CssClass="CartListItemAlt" />
                </asp:GridView>
            </asp:Panel>
            <asp:Label ID="lblError" runat="server" CssClass="NormalRed"></asp:Label>
        </td>
    </tr>
</table>
