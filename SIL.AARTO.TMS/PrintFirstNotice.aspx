<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.PrintFirstNotice" Codebehind="PrintFirstNotice.aspx.cs" %>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %> 

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet"/>
    <meta content="<%= description %>" name="Description"/>
    <meta content="<%= keywords %>" name="Keywords"/>
</head>
<script src="Scripts/DisableBackspaceKey.js"></script>

<body style="margin:0px,0px,0px,0px; background:<%=backgroundImage %> " >
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height:5%">
            <tr>
                <td class="HomeHead" align="center" style="width:100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" style="height:85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167" alt="images/1x1.gif" />
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    &nbsp;
                </td>
                <td valign="top" align="left" style="width:100%" colspan="1">
                    <table cellspacing="0" cellpadding="0" border="0" style="height:90%" width="100%">
                        <tr>
                            <td valign="top" style="height: 49px; width: 819px;">
                                <p style="text-align:center">
                                    <asp:Label ID="lblPageName" runat="server" Width="790px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p style="text-align:center">
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="width: 819px">
                                <asp:UpdatePanel ID="upd" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lblError_Print" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                        <asp:Panel ID="pnlGeneral" runat="server" DefaultButton="btnPrint">
                                            <table style="width: 877px">
                                              <tr>
                                                    <td style="width: 160px; height: 2px">
                                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                                    <td style="width: 255px; height: 2px" valign="middle" colspan="2">
                                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="True" CssClass="Normal"
                                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="217px">
                                                        </asp:DropDownList></td>
                                                    <td style="height: 2px; width: 108px;" valign="middle">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                        <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="203px" Text="<%$Resources:lblTicketNumberSearch.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left"  colspan="2">
                                                        <uc1:TicketNumberSearch ID="TicketNumberSearch1" runat="server" OnNoticeSelected="NoticeSelected" />
                                                    </td>
                                                    <td >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Text="<%$Resources:lblPrint.Text %>"></asp:Label>
                                                    </td>
                                                    <td  valign="middle" colspan="2">
                                                        <asp:TextBox ID="txtPrint" runat="server" Width="215px"></asp:TextBox></td>
                                                    <td valign="middle">
                                                        <asp:Button ID="btnPrint" runat="server" CssClass="NormalButton" 
                                                            OnClick="btnPrint_Click" Text="<%$Resources:btnPrint.Text %>" Width="130px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2" >
                                                        <asp:CheckBox ID="chkShowAll" runat="server" AutoPostBack="True" 
                                                            CssClass="NormalBold" OnCheckedChanged="chkShowAll_CheckedChanged" 
                                                            Text="<%$Resources:chkShowAll.Text %>" Width="202px" />
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblDateFrom" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateFrom.Text %>"></asp:Label>
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:TextBox ID="dateFrom" runat="server" autocomplete="off" CssClass="Normal"  
                                                            Height="20px" UseSubmitBehavior="False" />
                                                        <cc1:calendarextender ID="DateCalendar" runat="server" Format="yyyy-MM-dd"  
                                                            TargetControlID="dateFrom" >
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td rowspan="2"  valign="middle">
                                                        <asp:Button ID="btnRefresh" runat="server" CssClass="NormalButton" 
                                                            onclick="btnRefresh_Click"  Text="<%$Resources:btnRefresh.Text %>" Width="130px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  valign="middle">
                                                        <asp:Label ID="lblDateTo" runat="server" CssClass="NormalBold" Text="<%$Resources:lblDateTo.Text %>"></asp:Label>
                                                    </td>
                                                    <td  valign="middle">
                                                        <asp:TextBox ID="dateTo" runat="server" autocomplete="off" CssClass="Normal" 
                                                            Height="20px" UseSubmitBehavior="False" />
                                                        <cc1:calendarextender ID="CalendarExtender1" runat="server" Format="yyyy-MM-dd" 
                                                            TargetControlID="dateTo" >
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 160px">
                                                        &nbsp
                                                    </td>
                                                    <td valign="middle" colspan="2">
                                                        &nbsp;</td>
                                                    <td valign="middle" align="center">&nbsp;
                                                        </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        &nbsp;
                                        <br />
                                        <asp:DataGrid ID="dgPrintrun" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                            AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                            FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                            Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgPrintrun_ItemCommand"
                                            OnPageIndexChanged="dgPrintrun_PageIndexChanged" 
                                            OnItemCreated="dgPrintrun_ItemCreated" PageSize="20" AllowCustomPaging="True" >
                                            <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                            <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                            <ItemStyle CssClass="CartListItem"></ItemStyle>
                                            <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="PrintFile" HeaderText="<%$Resources:dgPrintrun.HeaderText %>"></asp:BoundColumn>
                                                 <%--//2014-01-21 Heidi fixed bug for Format is not correct(5103)--%>
                                                <asp:BoundColumn DataField="NoOFNotices" HeaderText="<%$Resources:dgPrintrun.HeaderText1 %>" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText2 %>">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblType" runat="server"></asp:Label>--%>
                                                        <asp:TextBox ID="lblType" runat="server" BorderStyle="None" BorderWidth="0" style="background-color: transparent;width:225px"/>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                               <%-- <asp:ButtonColumn CommandName="PrintNotices" HeaderText="Print" Text="Print Notices">
                                                </asp:ButtonColumn>--%>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText3 %>" >
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server"  NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.PrintFile", "FirstNoticeViewer.aspx?printfile={0}") %>'
                                                            Target="_blank" Text="<%$Resources:hkPrintNotices.Text %>" ID="hlPrint" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="PrintDate" HeaderText="<%$Resources:dgPrintrun.HeaderText4 %>" DataFormatString="{0:yyyy/MM/dd HH:mm}"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText5 %>">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID = "btnUpdate" runat="server" CausesValidation="false" 
                                                        CommandName="Select" Text="<%$Resources:btnUpdate.Text %>"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="<%$Resources:dgPrintrun.HeaderText6 %>">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container, "DataItem.PrintFile", "ExpiredNoticeListViewer.aspx?printfile={0}") %>'
                                                            Target="_blank" Text="<%$Resources:hlList.Text %>" ID="hlList"></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="NotPrint1stNoticeDate" HeaderText="Date" Visible="False"></asp:BoundColumn>
                                                 
                                                 
                                            </Columns>
                                            <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                        </asp:DataGrid>&nbsp;
                                        <asp:Panel ID="pnlUpdate" runat="server" Height="50px" Width="125px">
                                            <table style="width: 495px">
                                                <tr>
                                                    <td style="height: 20px">
                                                        <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblPrintFile.Text %>"></asp:Label></td>
                                                    <td style="height: 20px">
                                                        <asp:Label ID="lblPrintFile" runat="server" CssClass="NormalBold" Width="348px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnUpdateStatus" runat="server" Text="<%$Resources:btnUpdateStatus.Text %>" CssClass="NormalButton"
                                                            OnClick="btnUpdateStatus_Click" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <br />
                                        <asp:Panel ID="pnlCancel" runat="server" Height="50px" Width="125px">
                                            <table style="width: 495px">
                                                <tr>
                                                    <td style="height: 20px">
                                                        <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Width="136px" Text="<%$Resources:lblPrintFile.Text %>"></asp:Label></td>
                                                    <td style="height: 20px">
                                                        <asp:Label ID="lblExpFile" runat="server" CssClass="NormalBold" Width="348px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnCancel" runat="server" Text="<%$Resources:btnCancel.Text %>" CssClass="NormalButton"
                                                            OnClick="btnCancel_Click" /></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <br />
                                        <asp:Label ID="lblInstruct" runat="server" Width="465px" BorderStyle="Inset" CssClass="CartListHead"
                                            Height="18px" EnableViewState="false" Text="<%$Resources:lblInstruct.Text %>"></asp:Label></td>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="udp" runat="server" AssociatedUpdatePanelID="upd">
                                    <ProgressTemplate>
                                        <p class="Normal" style="text-align: center;">
                                            <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label4"
                                                runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height:5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" style="width:100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
