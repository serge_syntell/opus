<%@ Page language="c#" Inherits="Stalberg.TMS.FilmLockUserViewer" Codebehind="FilmLockUserViewer.aspx.cs" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= description %>" name="Description" />
    <meta content="<%= keywords %>" name="Keywords" />  	
</head>
<body onload="self.focus();" style="margin: 0px" background="<%=backgroundImage %>" >
    <form id="Form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table height="10%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="HomeHead" valign="middle" align="center" width="100%" colspan="2">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table height="85%" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td valign="top" align="center" style="width: 505px">
                    <img height="26" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center" style="width: 138px">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 138px">
                                    <asp:Button ID="btnHideMenu" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnHideMenu.Text %>" OnClick="btnHideMenu_Click"></asp:Button></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                    <asp:Panel ID="pnlTitle" runat="Server" Width="100%">
                        <p style="text-align: center;">
                            <asp:Label ID="lblPageName" runat="server" Width="100%" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                        <p>
                            &nbsp;</p>
                    </asp:Panel>
                    <asp:Label ID="lblError" runat="server" CssClass="NormalRed" />
                            <table style="width: 322px">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSelAuthority" runat="server" CssClass="NormalBold" Width="240px" Text="<%$Resources:lblSelAuthority.Text %>"></asp:Label></td>
                                    <td style="width: 223px">
                                        <asp:DropDownList ID="ddlSelectLA" runat="server" AutoPostBack="true" CssClass="Normal"
                                            OnSelectedIndexChanged="ddlSelectLA_SelectedIndexChanged" Width="220px">
                                        </asp:DropDownList></td>
                                 
                                </tr>
                            </table>
                    <asp:Panel ID="pnlGrid" runat="server" Width="100%">
                        <br />
                        <asp:GridView ID="grdHeader" runat="server" AutoGenerateColumns="False" CellPadding="3" CssClass="Normal" GridLines="Horizontal"  ShowFooter="True" OnPageIndexChanging="grdHeader_PageIndexChanging" OnRowCreated="grdHeader_RowCreated" OnSelectedIndexChanged="grdHeader_SelectedIndexChanged" Width="391px" >
                            <FooterStyle CssClass="CartListHead" />
                            <Columns>
                                <asp:BoundField DataField="FilmIntNo" HeaderText="FilmIntNo" Visible=False />
                                <asp:BoundField DataField="FilmNo" HeaderText="<%$Resources:grdHeader.HeaderText %>" />
                                <asp:BoundField DataField="FilmLockUser" HeaderText="<%$Resources:grdHeader.HeaderText1 %>" ><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="CartListHead" />
                            <AlternatingRowStyle CssClass="CartListItemAlt" />
                        </asp:GridView>
                    <asp:Label ID="Label2" runat="server" Width="100%" CssClass="ContentHead"> </asp:Label>
                            &nbsp;
                    <asp:Button ID="btnRelease" runat="server" CssClass="NormalButton" OnClick="btnRelease_Click"  Text="<%$Resources:btnRelease.Text %>" /></td>
                            <br />
                            <br />
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="pnlLockFilm" runat="server" Height="50px" Width="125px">
                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblLockNum.Text %>"
                                    Width="334px"></asp:Label><br />
                                <br />
                                <table style="width: 316px">
                                    <tr>
                                        <td style="height: 26px">
                                            <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblFilmNo.Text %>" Width="88px"></asp:Label></td>
                                        <td style="height: 26px">
                                            <asp:TextBox ID="txtFilmNo" runat="server" Width="95px"></asp:TextBox></td>
                                        <td style="height: 26px">
                                            <asp:Button ID="btnLockFilm" runat="server" CssClass="NormalButton" OnClick="btnLockFilm_Click"  Text="<%$Resources:btnLockFilm.Text %>" Width="174px" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                </ContentTemplate>
                </asp:UpdatePanel>
            </tr>
          
        </table>
        <table height="5%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
    
    
</body>
</html>
