﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SIL.AARTO.BLL.Report;

namespace Stalberg.TMS
{
    public partial class PaymentHistoryViewer : System.Web.UI.Page
    {
        private string connectionString = String.Empty;

        protected string title = String.Empty;

        protected override void OnLoad(EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            General gen = new General();
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                int autIntNo = 0;
                if (Request["AutIntNo"] != null)
                {
                    int.TryParse(Request["AutIntNo"].ToString(), out autIntNo);
                }
                int year = 0;
                if (Request["Year"] != null)
                {
                    int.TryParse(Request["Year"].ToString(), out year);
                }

                string filename = "PaymentHistory.xls";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                Response.Clear();

                PaymentHistoryReport report = new PaymentHistoryReport(connectionString);

                MemoryStream stream = report.ExportToExcel(autIntNo, year);

                Response.BinaryWrite(stream.GetBuffer());
                Response.End();
            }
        }
    }
}