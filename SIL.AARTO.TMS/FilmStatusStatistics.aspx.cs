using System;
using System.Data.SqlClient;
using Stalberg.TMS.Data.Util;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using System.Web.UI.WebControls;

using SIL.AARTO.BLL.Report;

namespace Stalberg.TMS
{
    /// <summary>
    /// A report viewer for Film Status Statistics
    /// </summary>
    public partial class FilmStatusStatistics : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        //protected string thisPage = "Film Status Statistics";
        protected string description = String.Empty;
        protected string thisPageURL = "FilmStatusStatistics.aspx";
        protected string title = string.Empty;
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            UserDetails userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

            if (!Page.IsPostBack)
            {
                //2012-3-6 linda modified into a multi-language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateAuthorities();

                this.dtpAfter.Text = DateTime.Today.AddMonths(-3).ToString(DATE_FORMAT);
                this.dtpBefore.Text = DateTime.Today.ToString(DATE_FORMAT);
            }
        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlAuthority.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(this.connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(this.ugIntNo, 0);
            //this.ddlAuthority.DataSource = data;
            //this.ddlAuthority.DataValueField = "AutIntNo";
            //this.ddlAuthority.DataTextField = "AutName";
            //this.ddlAuthority.DataBind();
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (this.ddlAuthority.SelectedValue.Equals(string.Empty))
            {
                //2012-3-6 linda modified into a multi-language
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                return;
            }

            this.lblError.Text = string.Empty;
            int selectedAutIntNo = int.Parse(this.ddlAuthority.SelectedValue);

            //Helper_Web.BuildPopup(this, "FilmStatusStatistics_Viewer.aspx",
            //    "AutIntNo", selectedAutIntNo.ToString(),
            //    "Before", this.dtpAfter.Value == null ? string.Empty : ((DateTime)this.dtpAfter.Value).ToString(DATE_FORMAT),
            //    "After", this.dtpBefore.Value == null ? string.Empty : ((DateTime)this.dtpBefore.Value).ToString(DATE_FORMAT));

            //return;

            //ReportingUtil util = new ReportingUtil(this.connectionString, this.login);
            //PageToOpen pto = new PageToOpen(this.Page, util.GetReportUrl(Request.Url, "FilmStatusStatistics"));

            //Edge 2012-05-17 added
            PageToOpen pto = new PageToOpen(this.Page, "FilmStatusStatisticsViewer.aspx");
            pto.AddParameter("AutIntNo", selectedAutIntNo);
            pto.AddParameter("After", this.dtpAfter.Text == string.Empty ? "2000-01-01" : string.Format("{0:yyyy-MM-dd}", this.dtpAfter.Text));
            pto.AddParameter("Before", this.dtpBefore.Text == string.Empty ? "2050-01-01" : string.Format("{0:yyyy-MM-dd}", this.dtpBefore.Text));
            Helper_Web.BuildPopup(pto);
        }
    }
}
