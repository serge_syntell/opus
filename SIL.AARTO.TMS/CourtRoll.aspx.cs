using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;

namespace Stalberg.TMS
{
    public partial class CourtRoll : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private int nDays = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "CourtRoll.aspx";
        //protected string thisPage = "Court Roll Register";

        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            lblError.Text = string.Empty;
            nDays = CheckDateRule(this.autIntNo);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.PopulateCourts();
            }
        }

        private void PopulateCourts()
        {
            CourtDB court = new CourtDB(this.connectionString);
            SqlDataReader reader = court.GetAuth_CourtListByAuth(this.autIntNo);
            this.ddlCourt.DataSource = reader;
            this.ddlCourt.DataValueField = "CrtIntNo";
            this.ddlCourt.DataTextField = "CrtDetails";
            this.ddlCourt.DataBind();
            reader.Close();
            this.ddlCourt.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourt.Items"), string.Empty));
        }

        private int CheckDateRule(int nAutIntNo)
        { 
            //AutIntNo, StartDate, EndDate and LastUser must be set up before the default rule is called
            DateRulesDetails rule = new DateRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.LastUser = this.login;
            rule.DtRStartDate = "CDate";
            rule.DtREndDate = "FinalCourtRoll";

            DefaultDateRules dateRule = new DefaultDateRules(rule, this.connectionString);
            int noOfDays = dateRule.SetDefaultDateRule();

            return noOfDays;
        }

        private void PopulateCourtRooms(int nCrtIntNo)
        {
            CourtRoomDB room = new CourtRoomDB(this.connectionString);
            //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
            SqlDataReader reader = room.GetCourtRoomListByCrtRStatusFlag(nCrtIntNo);// room.GetCourtRoomList(nCrtIntNo);
            this.ddlCourtRoom.DataSource = reader;
            this.ddlCourtRoom.DataValueField = "CrtRIntNo";
            this.ddlCourtRoom.DataTextField = "CrtRoomDetails";
            this.ddlCourtRoom.DataBind();
            reader.Close();
            this.ddlCourtRoom.Items.Insert(0, new ListItem((string)GetLocalResourceObject("ddlCourtRoom.Items"), "0"));

            grdDates.Visible = false;
       }

        public void BindGrid(int crtRIntNo)
        {
            Stalberg.TMS.CourtDatesDB tranNoList = new Stalberg.TMS.CourtDatesDB(connectionString);
            int totalCount = 0;
            DataSet data = tranNoList.GetCourtDatesListDS(crtRIntNo, autIntNo, grdDatesPager.PageSize, grdDatesPager.CurrentPageIndex-1, out totalCount);
            grdDates.DataSource = data;
            grdDates.DataKeyNames = new string[] { "CDIntNo" };
            grdDatesPager.RecordCount = totalCount;

            grdDates.DataBind();

            if (grdDates.Rows.Count == 0)
            {
                grdDates.Visible = false;
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                grdDates.Visible = true;
                this.grdDatesPager.Visible = true;
            }

            data.Dispose();
        }

         

        public void View(String sSelectedDate)
        {
            string sReportType = "P";
            lblError.Visible = true;
            lblError.Text = string.Empty;
            SqlDataReader readerCrtR = null;
            PageToOpen page = null;
            int nCrtIntNo = -1;
            DateTime dt;
            List<PageToOpen> pages = new List<PageToOpen>();

            if (ddlCourt.SelectedIndex == 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }

            CourtDB court = new CourtDB(this.connectionString);
            nCrtIntNo = Convert.ToInt32(ddlCourt.SelectedValue);
            dt = court.CourtDateByCourt(nCrtIntNo, DateTime.Parse(sSelectedDate));
            if (dt.CompareTo(new DateTime(2000, 1, 1)) <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2") + ddlCourt.SelectedItem;
                return;
            }

            //jerry 2011-11-04 add court rules
            CourtRulesDetails courtRulesDetails = new CourtRulesDetails();
            courtRulesDetails.CrtIntNo = nCrtIntNo;
            courtRulesDetails.CRCode = "1010";
            courtRulesDetails.LastUser = this.login;
            DefaultCourtRules courtRule = new DefaultCourtRules(courtRulesDetails, connectionString);
            KeyValuePair<int, string> rule = courtRule.SetDefaultCourtRule();
            string courtRuleValue = rule.Value;

            // All court rooms selected
            if (ddlCourtRoom.SelectedValue == "0")
            {
                CourtRoomDB room = new CourtRoomDB(this.connectionString);

                dt = dt.AddDays((double)nDays);
                if (dt.CompareTo(DateTime.Now) <= 0)
                    sReportType = "F";
                //Heidi 2013-09-23 changed for lookup court room by CrtRStatusFlag='C' OR 'P'
                readerCrtR = room.GetCourtRoomListByCrtRStatusFlag(nCrtIntNo);//room.GetCourtRoomList(nCrtIntNo);
                if (readerCrtR.HasRows)
                {
                    while (readerCrtR.Read())
                    {
                        if (courtRuleValue == "Y")
                        {
                            page = new PageToOpen(this, "CourtRollViewer.aspx");
                            page.Parameters.Add(new QSParams("CrtIntNo", nCrtIntNo.ToString()));
                            page.Parameters.Add(new QSParams("CrtRIntNo", readerCrtR["CrtRIntNo"].ToString()));
                            page.Parameters.Add(new QSParams("ReportType", sReportType));
                            page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("CourtRollType", "0"));// 0 is All
                            pages.Add(page);
                        }
                        else
                        {
                            page = new PageToOpen(this, "CourtRollViewer.aspx");
                            page.Parameters.Add(new QSParams("CrtIntNo", nCrtIntNo.ToString()));
                            page.Parameters.Add(new QSParams("CrtRIntNo", readerCrtR["CrtRIntNo"].ToString()));
                            page.Parameters.Add(new QSParams("ReportType", sReportType));
                            page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("CourtRollType", "1"));// 1 is CAM
                            pages.Add(page);

                            page = new PageToOpen(this, "CourtRollViewer.aspx");
                            page.Parameters.Add(new QSParams("CrtIntNo", nCrtIntNo.ToString()));
                            page.Parameters.Add(new QSParams("CrtRIntNo", readerCrtR["CrtRIntNo"].ToString()));
                            page.Parameters.Add(new QSParams("ReportType", sReportType));
                            page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("CourtRollType", "2"));// 2 is HWO
                            pages.Add(page);
                        }
                    }
                    readerCrtR.Close();
                }
                else
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3") + ddlCourt.SelectedItem;
                    return;
                }

                //dls 091014 - this code was incorrectly commented out
                //dls 080621 - there's no point in showing labels if this is the prelim court roll
                if (sReportType.Equals("F"))
                {
                    if (courtRuleValue == "Y")
                    {
                        page = new PageToOpen(this, "CourtRollLabelsViewer.aspx");
                        page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                        page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                        page.Parameters.Add(new QSParams("CourtRollType", "0"));// 0 is All
                        pages.Add(page);
                    }
                    else
                    {
                        page = new PageToOpen(this, "CourtRollLabelsViewer.aspx");
                        page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                        page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                        page.Parameters.Add(new QSParams("CourtRollType", "1"));// 1 is CAM
                        pages.Add(page);

                        page = new PageToOpen(this, "CourtRollLabelsViewer.aspx");
                        page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                        page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                        page.Parameters.Add(new QSParams("CourtRollType", "2"));// 2 is HWO
                        pages.Add(page);
                    }
                    Helper_Web.BuildPopup(pages.ToArray());
                }
            }
            else
            {
                dt = dt.AddDays((double)nDays);
                if (dt.CompareTo(DateTime.Now) <= 0)
                    sReportType = "F";

                
                if (courtRuleValue == "Y")
                {
                    page = new PageToOpen(this, "CourtRollViewer.aspx");
                    page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                    page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                    page.Parameters.Add(new QSParams("ReportType", sReportType));
                    page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("CourtRollType", "0"));// 0 is All
                    pages.Add(page);
                }
                else
                {
                    page = new PageToOpen(this, "CourtRollViewer.aspx");
                    page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                    page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                    page.Parameters.Add(new QSParams("ReportType", sReportType));
                    page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("CourtRollType", "1"));// 1 is CAM
                    pages.Add(page);

                    page = new PageToOpen(this, "CourtRollViewer.aspx");
                    page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                    page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                    page.Parameters.Add(new QSParams("ReportType", sReportType));
                    page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                    page.Parameters.Add(new QSParams("CourtRollType", "2"));// 2 is HWO
                    pages.Add(page);
                }

                //dls 091014 - this code was incorrectly commented out
                //dls 091014 - this code was incorrectly commented out
                //dls 080621 - there's no point in showing labels if this is the prelim court roll
                if (sReportType.Equals("F"))
                {
                    if (courtRuleValue == "Y")
                    {
                        page = new PageToOpen(this, "CourtRollLabelsViewer.aspx");
                        page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                        page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                        page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                        page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                        page.Parameters.Add(new QSParams("CourtRollType", "0"));// 0 is All
                        pages.Add(page);
                    }
                    else
                    {
                        page = new PageToOpen(this, "CourtRollLabelsViewer.aspx");
                        page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                        page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                        page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                        page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                        page.Parameters.Add(new QSParams("CourtRollType", "1"));// 1 is CAM
                        pages.Add(page);

                        page = new PageToOpen(this, "CourtRollLabelsViewer.aspx");
                        page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                        page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                        page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                        page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                        page.Parameters.Add(new QSParams("CourtRollType", "2"));// 2 is HWO
                        pages.Add(page);
                    }
                }

                // jerry 2010-12-2
                AuthorityRulesDetails arChargeSheet = new AuthorityRulesDetails();
                arChargeSheet.AutIntNo = this.autIntNo;
                arChargeSheet.ARCode = "6205";
                arChargeSheet.LastUser = this.login;

                DefaultAuthRules ar = new DefaultAuthRules(arChargeSheet, this.connectionString);
                ar.SetDefaultAuthRule();
                // print charge sheet with court roll
                if (arChargeSheet.ARString == "Y")
                {
                    if (sReportType.Equals("P"))
                    {
                        // print charge sheet with preliminary court roll
                        AuthorityRulesDetails arChargeSheetWithPre = new AuthorityRulesDetails();
                        arChargeSheetWithPre.AutIntNo = this.autIntNo;
                        arChargeSheetWithPre.ARCode = "6208";
                        arChargeSheetWithPre.LastUser = this.login;

                        DefaultAuthRules arDefaultWithP = new DefaultAuthRules(arChargeSheetWithPre, this.connectionString);
                        arDefaultWithP.SetDefaultAuthRule();

                        if (arChargeSheetWithPre.ARString == "Y")
                        {
                            if (courtRuleValue == "Y")
                            {
                                page = new PageToOpen(this, "CourtRollChargeSheetViewer.aspx");
                                page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                                page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                                page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                                page.Parameters.Add(new QSParams("ReportType", sReportType));
                                page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                                page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                                page.Parameters.Add(new QSParams("CourtRollType", "0"));// 0 is All
                                pages.Add(page);
                            }
                            else
                            {
                                page = new PageToOpen(this, "CourtRollChargeSheetViewer.aspx");
                                page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                                page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                                page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                                page.Parameters.Add(new QSParams("ReportType", sReportType));
                                page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                                page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                                page.Parameters.Add(new QSParams("CourtRollType", "1"));// 1 is CAM
                                pages.Add(page);

                                page = new PageToOpen(this, "CourtRollChargeSheetViewer.aspx");
                                page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                                page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                                page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                                page.Parameters.Add(new QSParams("ReportType", sReportType));
                                page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                                page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                                page.Parameters.Add(new QSParams("CourtRollType", "2"));// 2 is HWO
                                pages.Add(page);
                            }
                        }
                    }
                    else
                    {
                        if (courtRuleValue == "Y")
                        {
                            page = new PageToOpen(this, "CourtRollChargeSheetViewer.aspx");
                            page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                            page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                            page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                            page.Parameters.Add(new QSParams("ReportType", sReportType));
                            page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("CourtRollType", "0"));// 0 is All
                            pages.Add(page);
                        }
                        else
                        {
                            page = new PageToOpen(this, "CourtRollChargeSheetViewer.aspx");
                            page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                            page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                            page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                            page.Parameters.Add(new QSParams("ReportType", sReportType));
                            page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("CourtRollType", "1"));// 1 is CAM
                            pages.Add(page);

                            page = new PageToOpen(this, "CourtRollChargeSheetViewer.aspx");
                            page.Parameters.Add(new QSParams("AutIntNo", this.autIntNo.ToString()));
                            page.Parameters.Add(new QSParams("CrtIntNo", ddlCourt.SelectedValue));
                            page.Parameters.Add(new QSParams("CrtRIntNo", ddlCourtRoom.SelectedValue));
                            page.Parameters.Add(new QSParams("ReportType", sReportType));
                            page.Parameters.Add(new QSParams("CourtDate", DateTime.Parse(sSelectedDate).ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("FinalPrintDate", dt.ToString("yyyy-MM-dd")));
                            page.Parameters.Add(new QSParams("CourtRollType", "2"));// 2 is HWO
                            pages.Add(page);
                        }
                    }
                    
                   
                }

                Helper_Web.BuildPopup(pages.ToArray());
            }
        }

        // Not used as yet
        protected void btnViewAll_Click(object sender, EventArgs e)
        {
        }

        protected void ddlCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourt.SelectedIndex > 0)
            {
                PopulateCourtRooms(Convert.ToInt32(ddlCourt.SelectedValue));

                //mrs 20080909 specific court room
                //BindGrid(Convert.ToInt32(ddlCourt.SelectedValue));
            }
            else
            {
                ddlCourtRoom.SelectedIndex = 0;
            }
        }

        protected void grdDates_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)grdDates.Rows[grdDates.SelectedIndex].Cells[0].Controls[1];
            View(lbl.Text);
        }

        protected void grdDates_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid(Convert.ToInt32(ddlCourtRoom.SelectedValue));
        }

        protected void ddlCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourtRoom.SelectedIndex > 0)
            {
                grdDatesPager.CurrentPageIndex = 1;
                grdDatesPager.RecordCount = 0;
                BindGrid(Convert.ToInt32(ddlCourtRoom.SelectedValue));
            }
            else
            {
                ddlCourtRoom.SelectedIndex = 0;
            }

        }

        protected void grdDatesPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid(Convert.ToInt32(ddlCourtRoom.SelectedValue));
        }
}
}


