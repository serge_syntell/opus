using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Transactions;
using System.Data.SqlClient;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Collections.Generic;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Entities;
using Stalberg.TMS.Data;

namespace Stalberg.TMS
{
    /// <summary>
    /// The Notice Post management page
    /// </summary>
    public partial class NoticePost : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = "Confirm Notice Posted";
        protected string description = String.Empty;
        protected string thisPageURL = "NoticePost.aspx";

        // Constants
        //private const int FIRST_NOTICE_STATUS = 250;
        //private const int FIRST_NOTICE_CIPRUS_STATUS = 220;
        //private const int SECOND_NOTICE_STATUS = 260;
        private const string DATE_FORMAT = "yyyy-MM-dd";

     
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //dls 090506 - need this for Ajax Calendar extender
            HtmlLink link = new HtmlLink();
            link.Href = styleSheet;
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(link);

         
         
            if (!Page.IsPostBack)
            {
                this.GetNoticesForPostage(0);
            }
        }


        private int  CheckSMSExtractOn2ndNoticeDataRule()
        {
            // 20141029 Tommi Added for PushSMSExtract2ndNotice
            DateRulesDetails dateRule = new DateRulesDetails();
            dateRule.AutIntNo = autIntNo;
            dateRule.DtRStartDate = "NotIssue2ndNoticeDate";
            dateRule.DtREndDate = "SMSExtractOn2ndNoticeDate";
            dateRule.LastUser = this.login;
            DefaultDateRules rule = new DefaultDateRules(dateRule, connectionString);
            int noOfDaysToSMSExtractOn2ndNoticeDate  = rule.SetDefaultDateRule();
            return noOfDaysToSMSExtractOn2ndNoticeDate;
        }
        private void GetNoticesForPostage(int page)
        {
            #region Jerry 2012-10-26 change
            //NoticeDB db = new NoticeDB(this.connectionString);
            //DataSet ds = db.GetNoticesForPostage(this.autIntNo);

            //if (ds == null || ds.Tables[0].Rows.Count == 0)
            //{
            //    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text9"));
            //    return;
            //}

            //this.ViewState.Add("DataSource", ds);

            //this.grdPosted.PageIndex = page;
            //this.grdPosted.DataSource = ds;
            #endregion

            try
            {
                NoticeDB db = new NoticeDB(this.connectionString);
                DataSet ds = db.GetNoticesForPostage(this.autIntNo);
                this.ViewState.Add("DataSource", ds);

                this.grdPosted.PageIndex = page;
                this.grdPosted.DataSource = ds;
                this.grdPosted.DataBind();

                if (ds == null || ds.Tables[0].Rows.Count == 0)
                {
                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text9"));
                    return;
                }
            }
            catch
            {
                this.grdPosted.PageIndex = 0;
                this.grdPosted.DataBind();
            }
        }



        protected void grdPosted_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlUpdate.Visible = true;
            dtpActualDate.Text = DateTime.Today.ToString(DATE_FORMAT);
        }

        protected void grdPosted_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.GetNoticesForPostage(e.NewPageIndex);
        }



        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string printFileName = this.grdPosted.SelectedRow.Cells[1].Text.Trim();
            string firstPostOrder = this.grdPosted.SelectedRow.Cells[0].Text.Trim();
            //bool isFirstNotice = firstPostOrder.Equals("1st");

            NoticeStatus status = NoticeStatus.First;
            //20090624 tf ---- 2nd notice,  positions 4,5,6 in the print file name
            if (printFileName.IndexOf("EPR") > 0 || (printFileName.Length > 5 && printFileName.Substring(4, 3).ToUpper() == "2ND"))
            {
                status = NoticeStatus.Second;
            }
            else
            {
                #region Jerry 2012-10-26 change that
                //switch (printFileName.Substring(0, 3).ToUpper())
                //{
                //    case "NAG":
                //        status = NoticeStatus.NoAOG;
                //        break;

                //    case "RNO":
                //        status = NoticeStatus.NewOffender;
                //        break;

                //    case "REG":
                //        status = NoticeStatus.NewRegNo;
                //        break;
                //}
                #endregion
                switch (firstPostOrder)
                {
                    case "1st":
                        status = NoticeStatus.First;
                        break;
                    case "2nd":
                        status = NoticeStatus.Second;
                        break;
                    case "No AOG":
                        status = NoticeStatus.NoAOG;
                        break;
                    case "New":
                        status = NoticeStatus.NewOffender;
                        break;
                    case "Reg":
                        status = NoticeStatus.NewRegNo;
                        break;
                }
            }
            //if (isFirstNotice)
            //{
            //    switch (printFileName.Substring(0, 3).ToUpper())
            //    {
            //        case "NAG":
            //            status = NoticeStatus.NoAOG;
            //            break;

            //        case "RNO":
            //            status = NoticeStatus.NewOffender;
            //            break;
            //    }
            //}
            //else
            //    status = NoticeStatus.Second;

            DateTime dtPrinted;
            if (!DateTime.TryParse(this.grdPosted.SelectedRow.Cells[3].Text, out dtPrinted))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                lblError.Visible = true;
                return;
            }

            DateTime dtDate;

            if (!DateTime.TryParse(this.dtpActualDate.Text, out dtDate))
            {
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                lblError.Visible = true;
                return;
            }

            if (dtDate.Ticks < dtPrinted.Ticks)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                lblError.Visible = true;
                return;
            }

            //fred add push queue
            using (TransactionScope scope = new TransactionScope())
            {
                NoticeDB db = new NoticeDB(this.connectionString);
                string errMessage = string.Empty;
                int iRe = -1;
                try
                {
                    iRe = db.SetPostedDate(printFileName, this.autIntNo, dtDate, this.login, status, ref errMessage);
                }
                catch
                { }

                switch (iRe)
                {
                    case 1:
                        this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text3"), printFileName, firstPostOrder);
                        break;
                    case 0:
                        this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text4"), printFileName, firstPostOrder, errMessage);
                        break;
                    case -1:
                        this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text5"), printFileName, firstPostOrder);
                        break;
                    case -2:
                        this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text6"), printFileName, firstPostOrder);
                        break;
                    case -3:
                        this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text7"), printFileName, firstPostOrder);
                        break;
                    case -4:
                        this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text8"), printFileName, firstPostOrder);
                        break;
                }

                // Nick 20120917 added for report Non-summons registrations
                DateRulesDetails dateRuleIssueSum = new DateRulesDetails();
                dateRuleIssueSum.AutIntNo = autIntNo;
                dateRuleIssueSum.DtRStartDate = "NotPosted1stNoticeDate";
                dateRuleIssueSum.DtREndDate = "NotIssueSummonsDate";
                dateRuleIssueSum.LastUser = this.login;
                DefaultDateRules ruleIssueSum = new DefaultDateRules(dateRuleIssueSum, connectionString);
                int noOfDaysToSummons = ruleIssueSum.SetDefaultDateRule();

                DataTable dt = db.GetPostedNotices(printFileName, autIntNo, ref errMessage).Tables[0];
                if (iRe == 1 && dt != null)
                {
                    NoticeMobileDetailService notMobDetServ = new NoticeMobileDetailService();
                    NoticeMobileDetail notMobDet;
                    int notIntNo;

                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = this.autIntNo;
                    arDetails.ARCode = "2312";
                    arDetails.LastUser = this.login;
                    DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
                    int noOfDays_ReportNonVerificate_IssueSum = ar.SetDefaultAuthRule().Key;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (!int.TryParse(dr["NotIntNo"] == null ? "0" : dr["NotIntNo"].ToString(), out notIntNo))
                            continue;
                        notMobDet = notMobDetServ.GetByNotIntNo(notIntNo).FirstOrDefault();
                        if (notMobDet != null)
                        {
                            if (notMobDet.IsNonSummonsRegNo)
                            {
                                notMobDet.ReportBeforeSummonsDate = DateTime.Now.AddDays(noOfDaysToSummons - noOfDays_ReportNonVerificate_IssueSum);
                                notMobDet.LastUser = this.login;
                                notMobDetServ.Update(notMobDet);
                            }
                        }
                    }
                }

                //we need to push the Summons for the NoAOG as well!!! and check data washing
                if ((status == NoticeStatus.First || status == NoticeStatus.NoAOG) && iRe == 1)
                {
                    AuthorityDB authDB = new AuthorityDB(this.connectionString);
                    AuthorityDetails authDetails = authDB.GetAuthorityDetails(autIntNo);
                    string autTicketProcessor = authDetails.AutTicketProcessor;

                    DateRulesDetails dateRule = new DateRulesDetails();
                    dateRule.AutIntNo = autIntNo;
                    dateRule.DtRStartDate = "NotPosted1stNoticeDate";
                    dateRule.DtREndDate = "NotIssue2ndNoticeDate";
                    dateRule.LastUser = this.login;
                    DefaultDateRules rule = new DefaultDateRules(dateRule, connectionString);
                    int noOfDaysTo2ndNotice = rule.SetDefaultDateRule();

                    int noOfDaysToSMSExtractOn2ndNoticeDate = CheckSMSExtractOn2ndNoticeDataRule();

                    /*
                    dateRule = new DateRulesDetails();
                    dateRule.AutIntNo = autIntNo;
                    dateRule.DtRStartDate = "NotPosted1stNoticeDate";
                    dateRule.DtREndDate = "NotIssueSummonsDate";
                    dateRule.LastUser = this.login;
                    rule = new DefaultDateRules(dateRule, connectionString);
                    int noOfDaysToSummons = rule.SetDefaultDateRule();
                    */

                    AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = this.autIntNo;
                    arDetails.ARCode = "4250";
                    arDetails.LastUser = this.login;
                    DefaultAuthRules ar = new DefaultAuthRules(arDetails, this.connectionString);
                    bool SecNotice = ar.SetDefaultAuthRule().Value.Equals("Y");

                    //  2012-01-21 Nick add, check the data washing is enabled.
                    arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = this.autIntNo;
                    arDetails.ARCode = "9060";
                    arDetails.LastUser = this.login;
                    ar = new DefaultAuthRules(arDetails, this.connectionString);
                    bool DWEnabled = ar.SetDefaultAuthRule().Value.Equals("Y");



                    //  Get the data washing recycle period.
                    arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = this.autIntNo;
                    arDetails.ARCode = "9080";
                    arDetails.LastUser = this.login;
                    ar = new DefaultAuthRules(arDetails, this.connectionString);
                    int DWRecycle = ar.SetDefaultAuthRule().Key;

                    // Henry 2013-03-27 add
                    arDetails = new AuthorityRulesDetails();
                    arDetails.AutIntNo = this.autIntNo;
                    arDetails.ARCode = "5050";
                    arDetails.LastUser = this.login;
                    KeyValuePair<int, string> aR_5050 = (new DefaultAuthRules(arDetails, this.connectionString)).SetDefaultAuthRule();

                    //DataSet ds = db.GetPostedNotices(printFileName, autIntNo, ref errMessage);   // Nick 20120918 moved upword.
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        //DataTable dt = ds.Tables[0];
                        QueueItemProcessor queueProcessor = new QueueItemProcessor();
                        foreach (DataRow dr in dt.Rows)
                        {
                            QueueItem pushItem = new QueueItem();

                            string NotIntNo = dr["NotIntNo"].ToString();
                            string AutCode = dr["AutCode"].ToString();
                            DateTime NotPosted1stNoticeDate = Convert.ToDateTime(dr["NotPosted1stNoticeDate"]);

                            // Get persona last data wash date
                            bool needDataWash = false;
                            string notSendTo = null;
                            string IDNum = null;

                            if (DWEnabled)
                            {
                                NoticeDB notDB = new NoticeDB(connectionString);
                                int notIntNo = Convert.ToInt32(NotIntNo);
                                NoticeDetails notDet = notDB.GetNoticeDetails(notIntNo);
                                if (notDet != null) notSendTo = (notDet.NotSendTo + "").Trim();

                                switch (notSendTo)
                                {
                                    case "D":
                                        DriverDB driverDB = new DriverDB(connectionString);
                                        DriverDetails drvDet = driverDB.GetDriverDetailsByNotice(notIntNo);
                                        if (drvDet != null) IDNum = (drvDet.DrvIDNumber + "").Trim();
                                        break;
                                    case "O":
                                        OwnerDB ownerDB = new OwnerDB(connectionString);
                                        OwnerDetails ownDet = ownerDB.GetOwnerDetailsByNotice(notIntNo);
                                        if (ownDet != null) IDNum = (ownDet.OwnIDNumber + "").Trim();
                                        break;
                                    case "P":
                                        ProxyDB proxyDB = new ProxyDB(connectionString);
                                        ProxyDetails pxyDet = proxyDB.GetProxyDetailsByNotice(notIntNo);
                                        if (pxyDet != null) IDNum = (pxyDet.PrxIDNumber + "").Trim();
                                        break;
                                    default:
                                        break;
                                }

                                //dls 2013-02-25 - need to work out what to do with the situation where the IDNumber is missing - cannot block the process of updating the print file status
                                if (String.IsNullOrEmpty(IDNum))
                                {
                                    this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text10"), notDet.NotTicketNo);
                                    lblError.Visible = true;
                                    //return;           //dls 2013-02-25 - temporary comment out
                                }
                                else
                                {
                                    MI5DB mi5 = new MI5DB(connectionString);
                                    DataSet personDet = mi5.GetPersonaDetailsByID(IDNum, login);
                                    if (personDet == null || personDet.Tables.Count == 0 || personDet.Tables[0].Rows.Count == 0)
                                    {
                                        this.lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text11"), notDet.NotTicketNo);
                                        lblError.Visible = true;
                                        //return;           //dls 2013-02-25 - temporary comment out
                                    }
                                    else
                                    {
                                        string strLastSentDWDate = personDet.Tables[0].Rows[0]["MIPSentToDataWasherDate"] + "";
                                        string strLastDWDate = personDet.Tables[0].Rows[0]["MIPLastWashedDate"] + "";
                                        DateTime? lastSentDWDate = strLastSentDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastSentDWDate);
                                        DateTime? lastDWDate = strLastDWDate == "" ? null : (DateTime?)Convert.ToDateTime(strLastDWDate);

                                        if ((!lastSentDWDate.HasValue && !lastDWDate.HasValue) ||
                                            (lastSentDWDate.HasValue && !lastDWDate.HasValue && Convert.ToDateTime(lastSentDWDate).AddMonths(DWRecycle) < DateTime.Now))
                                        {
                                            needDataWash = true;
                                        }
                                        else
                                        {
                                            lastSentDWDate = lastSentDWDate.HasValue ? lastSentDWDate : DateTime.Parse("1900-01-01");
                                            if (lastSentDWDate < lastDWDate && Convert.ToDateTime(lastDWDate).AddMonths(DWRecycle) < DateTime.Now)
                                            {
                                                needDataWash = true;
                                            }
                                        }
                                    }
                                }
                            }
                        
                

                            //no second notice for a NoAOG
                            if (status == NoticeStatus.First && SecNotice)
                            {
                                //Oscar 20120412 changed group
                                int frameIntNo = new NoticeDB(this.connectionString).GetFrameForNotice(Convert.ToInt32(NotIntNo));
                                string violationType = new FrameDB(this.connectionString).GetViolationType(frameIntNo);

                              

                                pushItem = new QueueItem();
                                pushItem.Body = NotIntNo;
                                pushItem.Group = string.Format("{0}|{1}", AutCode.Trim(), violationType);
                                pushItem.ActDate = NotPosted1stNoticeDate.AddDays(noOfDaysTo2ndNotice);
                                pushItem.QueueType = ServiceQueueTypeList.Generate2ndNotice;
                                queueProcessor.Send(pushItem);

                                #region
                                //2014.10.29 Tommi added for push SMSExtractOn2ndNotice
                                pushItem = new QueueItem();
                                pushItem.Body = NotIntNo;
                                pushItem.Group = AutCode.Trim();
                                pushItem.ActDate = NotPosted1stNoticeDate.AddDays(noOfDaysTo2ndNotice + noOfDaysToSMSExtractOn2ndNoticeDate);
                                pushItem.QueueType = ServiceQueueTypeList.SMSExtractOn2ndNotice;
                                queueProcessor.Send(pushItem);

                                #endregion

                                // 20120222 Nick add, push the data wash queue
                                if (needDataWash)
                                {
                                    dateRule = new DateRulesDetails();
                                    dateRule.AutIntNo = autIntNo;
                                    dateRule.DtRStartDate = "DataWashDate";
                                    dateRule.DtREndDate = "NotIssue2ndNoticeDate";
                                    dateRule.LastUser = this.login;
                                    rule = new DefaultDateRules(dateRule, connectionString);
                                    int daysDWTo2ndNotice = rule.SetDefaultDateRule();

                                    pushItem = new QueueItem();
                                    pushItem.Body = NotIntNo;
                                    pushItem.Group = AutCode;
                                    pushItem.ActDate = NotPosted1stNoticeDate.AddDays(noOfDaysTo2ndNotice - daysDWTo2ndNotice);
                                    pushItem.QueueType = ServiceQueueTypeList.DataWashingExtractor;
                                    queueProcessor.Send(pushItem);
                                }
                            }

                            //dls 2012-03-22 made a decision to push the Summons DW Queue here as well in case the Export from 2ndNotice does not return in time. Extractor rules will stop the ID from being sent more than once within a recycle period
                            //else
                            //{

                            //only need to datawash for summons is we are using these ticket processors1
                            //Jerry 2012-12-28 changed
                            SIL.AARTO.DAL.Entities.Notice noticeEntity = new SIL.AARTO.DAL.Services.NoticeService().GetByNotIntNo(Convert.ToInt32(NotIntNo));
                            //if (needDataWash && (autTicketProcessor == "TMS" || autTicketProcessor == "AARTO"))
                            if (needDataWash && noticeEntity.NotTicketProcessor == "TMS")
                            {
                                dateRule = new DateRulesDetails();
                                dateRule.AutIntNo = autIntNo;
                                dateRule.DtRStartDate = "DataWashDate";
                                dateRule.DtREndDate = "NotIssueSummonDate";
                                dateRule.LastUser = this.login;
                                rule = new DefaultDateRules(dateRule, connectionString);
                                int daysDWToSummon = rule.SetDefaultDateRule();

                                pushItem = new QueueItem();
                                pushItem.Body = NotIntNo;
                                pushItem.Group = AutCode;
                                pushItem.ActDate = NotPosted1stNoticeDate.AddDays(noOfDaysToSummons - daysDWToSummon);
                                pushItem.QueueType = ServiceQueueTypeList.DataWashingExtractor;
                                queueProcessor.Send(pushItem);
                            }
                            //}

                            // Oscar 20120315 changed the Group
                            NoticeDetails notice = db.GetNoticeDetails(int.Parse(NotIntNo));
                            string[] group = new string[3];
                            group[0] = AutCode.Trim();
                            //Jerry 2013-03-07 change
                            //group[1] = notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) ? "H" : "";
                            group[1] = (notice.NotFilmType.Equals("H", StringComparison.OrdinalIgnoreCase) && !notice.IsVideo) ? "H" : "";
                            group[2] = Convert.ToString(notice.NotCourtName);

                            // Oscar 2013-06-17 added
                            var noAogDB = new NoAOGDB(connectionString);
                            var isNoAog = noAogDB.IsNoAOG(Convert.ToInt32(NotIntNo), NoAOGQueryType.Notice);

                            pushItem = new QueueItem();
                            pushItem.Body = NotIntNo;
                            //pushItem.Group = AutCode;
                            // Oscar 20120315 changed the Group
                            pushItem.Group = string.Join("|", group);
                            pushItem.ActDate = isNoAog ? noAogDB.GetNoAOGGenerateSummonsActionDate(autIntNo, NotPosted1stNoticeDate) : NotPosted1stNoticeDate.AddDays(noOfDaysToSummons);
                            pushItem.QueueType = ServiceQueueTypeList.GenerateSummons;
                            //jerry 2012-03-31 add Priority
                            pushItem.Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days;
                            //Jerry 2012-05-14 add last user
                            pushItem.LastUser = this.login;
                            queueProcessor.Send(pushItem);

                            //Henry 2013-03-27 add
                            if (aR_5050.Value.Trim().ToUpper() == "Y")
                            {
                                pushItem = new QueueItem();
                                pushItem.Body = NotIntNo;
                                pushItem.Group = string.Join("|", group);
                                pushItem.ActDate = notice.NotOffenceDate.AddMonths(aR_5050.Key).AddDays(-5);
                                pushItem.QueueType = ServiceQueueTypeList.GenerateSummons;
                                pushItem.Priority = (notice.NotOffenceDate.Date - DateTime.Now.Date).Days;
                                pushItem.LastUser = this.login;
                                queueProcessor.Send(pushItem);
                            }
                        }
                        scope.Complete();
                    }
                    else
                        return;
                }
                else
                    scope.Complete();
            }

            this.GetNoticesForPostage(this.grdPosted.PageIndex);
            this.pnlUpdate.Visible = false;
        }


        protected void grdPosted_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            DataSet ds = (DataSet)this.ViewState["DataSource"];

            //dls 070905 - the same 1st 20 rows of data is data is being placed regardless of the page index
            //DataRow row = ds.Tables[0].Rows[e.Row.RowIndex];

            int rowIndex = (this.grdPosted.PageIndex * this.grdPosted.PageSize) + e.Row.RowIndex;
            DataRow row = ds.Tables[0].Rows[rowIndex];
            DateTime dt;

            if (row["PrintDate"] != DBNull.Value)
            {
                if (DateTime.TryParse(row["PrintDate"].ToString(), out dt))
                {
                    e.Row.Cells[3].Text = ((DateTime)row["PrintDate"]).ToString(DATE_FORMAT);
                }
                else
                {
                    e.Row.Cells[3].Text = string.Empty;
                }
            }

            if (row["FirstPostedDate"] != DBNull.Value)
            {
                if (DateTime.TryParse(row["FirstPostedDate"].ToString(), out dt))
                {
                    e.Row.Cells[4].Text = ((DateTime)row["FirstPostedDate"]).ToString(DATE_FORMAT);
                    //need to check the posted date and if set then disbale the posted link
                }
                else
                {
                    e.Row.Cells[4].Text = string.Empty;
                }
                string noticeType = row.ItemArray.GetValue(3).ToString();

                //allow updating for 2nd notice post date
                if (noticeType.Equals("1st"))
                    e.Row.Cells[5].Enabled = false;
                else
                    e.Row.Cells[5].Enabled = true;
            }
        }
    }
}
