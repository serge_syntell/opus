using System;
using System.Web;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.Data;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class SummonsServingViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        byte[] buffer;
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SummonsServingViewer.aspx";
        //protected string thisPage = "Summons Serving Status Viewer";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Retrieve the database connection string
                this.connectionString = Application["constr"].ToString();

                SumServStatusOptions options = (SumServStatusOptions)Session["SumStatusOptions"];
                Session.Remove("SumStatusOptions");
                if (options == null)
                    return;

                // Set domain specific variables
                General gen = new General();
                backgroundImage = gen.SetBackground(Session["drBackground"]);
                styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
                title = gen.SetTitle(Session["drTitle"]);

                string path = Server.MapPath("reports/SummonsServingStatus.dplx");  

                DocumentLayout report = new DocumentLayout(path);
                StoredProcedureQuery query = (StoredProcedureQuery)report.GetQueryById("Query");
                query.ConnectionString = this.connectionString;
                ParameterDictionary parameters = new ParameterDictionary();

                // Run per Authority:
                parameters.Add("AutIntNo", options.AutIntNo);

                // Oscar 2013-05-09 added DateOption
                parameters.Add("CrtIntNo", options.CrtIntNo);

                var defautDate = new DateTime(1900, 1, 1);
                parameters.Add("DateOption", options.DateOption);
                if (options.DateOption == 0)
                {
                    // Run on Court Date - Mandatory.
                    parameters.Add("CrtDate", options.CourtDate);

                    // Run on Court Room - Mandatory.
                    parameters.Add("CrtRIntNo", options.CrtRIntNo);

                    parameters.Add("CrtDateStart", defautDate);
                    parameters.Add("CrtDateEnd", defautDate);
                }
                else if (options.DateOption == 1)
                {
                    parameters.Add("CrtDateStart", options.CrtDateStart);
                    parameters.Add("CrtDateEnd", options.CrtDateEnd);

                    parameters.Add("CrtDate", defautDate);
                }

                parameters.Add("OrderBy", options.OrderBy);

                // (LF:06/11/2008) Handle Options.
                switch (options.OptionSelected)
                {
                    case "Served and Unserved":
                        parameters.Add("OptServedUnserved", "BOTH");
                        break;
                    case "Only Served":
                        parameters.Add("OptServedUnserved", "SERVED");
                        break;
                    case "Only Unserved":
                        parameters.Add("OptServedUnserved", "UNSERVED");
                        break;
                    case "Withdrawn":
                        parameters.Add("OptWithdrawn", 1);
                        break;
                    case "Not returned from Clerk Of Court":
                        parameters.Add("NotReturnedFromCofC", 1);
                        break;
                    case "Not returned from Summons Server":
                        parameters.Add("NotReturnedFromSS", 1);
                        break;
                    case "By Summons Number or Notice Number":
                        parameters.Add("SummonsNo", options.SummonsNo.Trim().Replace("/", ""));
                        parameters.Add("NoticeNo", options.NoticeNo.Trim().Replace("/", ""));
                        break;
                    default:
                        parameters.Add("ShowAll", 1);
                        break;
                }

                // (LF:06/11/2008) Create Document.
                Document document = report.Run(parameters);
                buffer = document.Draw();

                Response.ClearContent();
                Response.ClearHeaders();

                if (buffer.Length < 1720)
                {
                    Response.ContentType = "text/html";
                    Response.Redirect("NoRecordFound.aspx");
                }
                else
                {
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(buffer);
                }

                Response.End();
            }
        }

    }
}