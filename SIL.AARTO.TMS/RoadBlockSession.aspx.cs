﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.BLL.Utility;

namespace Stalberg.TMS
{
    public partial class RoadBlockSession : System.Web.UI.Page
    {
        #region Fields

        // Fields
        protected string styleSheet;
        protected string background;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "RoadBlockSession.aspx";
        //protected string thisPage = "Road Block Session - Session Details";
        private string connectionstring = string.Empty;
        private string login;
        private int autIntNo;

        private const int STATUS_SUMMONS_PRINTED = 620;
        //private static readonly Regex RegexCheckInput = new Regex("^[0-9]{6}$", RegexOptions.Compiled);
        private static readonly Regex RegexCheckInput = new Regex("^[-]?[0-9]*[.]{0,1}[0-9]{0,4}$", RegexOptions.Compiled);
        

        #endregion

        #region User Methods

        /// <summary>
        /// status =true ： select all records
        /// </summary>
        /// <param name="status"></param>
        private void BindData(bool status, bool isReload)
        {
            try
            {
                if (isReload) // 2013-04-10 add by Henry for pagination
                {
                    grdRBSessionListPager.RecordCount = 0;
                    grdRBSessionListPager.CurrentPageIndex = 1;
                }
                const int selectSession = 0;
                RoadBlockDB roadDB = new RoadBlockDB(connectionstring);
                DataSet ds = null;
                int totalCount = 0;
                if (!status)
                {
                    ds = roadDB.GetRoadBlockSessionDS(selectSession, grdRBSessionListPager.PageSize, grdRBSessionListPager.CurrentPageIndex, out totalCount, GetFilterString());
                }
                else
                {
                    ds = roadDB.GetRoadBlockSessionDS(selectSession, grdRBSessionListPager.PageSize, grdRBSessionListPager.CurrentPageIndex, out totalCount);
                }

                //Jake 2013-09-13 added 
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 0 && grdRBSessionListPager.CurrentPageIndex > 0)
                {
                    grdRBSessionListPager.CurrentPageIndex--;

                    if (!status)
                    {
                        ds = roadDB.GetRoadBlockSessionDS(selectSession, grdRBSessionListPager.PageSize, grdRBSessionListPager.CurrentPageIndex, out totalCount, GetFilterString());
                    }
                    else
                    {
                        ds = roadDB.GetRoadBlockSessionDS(selectSession, grdRBSessionListPager.PageSize, grdRBSessionListPager.CurrentPageIndex, out totalCount);
                    }
                }

                grdRBSessionList.DataSource = ds.Tables[0];
                grdRBSessionList.DataKeyNames = new string[] { "RBSIntNo", "RBSessionID", "IndcFlag" };
                grdRBSessionList.DataBind();
                grdRBSessionListPager.RecordCount = totalCount;
                if (ds.Tables[0].Rows.Count <= 0)
                    this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PopulateRoadBlockForUserSelect()
        {
            if (Session["UserIntNo"] != null)
            {
                cboRoadBlockSession.Items.Clear();
                RoadBlockDB roadBlock = new RoadBlockDB(connectionstring);
                cboRoadBlockSession.DataSource = roadBlock.GetRoadBlockByUserIntNo((int)Session["UserIntNo"]);
                cboRoadBlockSession.DataTextField = "LocDescr";
                cboRoadBlockSession.DataValueField = "RBSIntNo";
                cboRoadBlockSession.DataBind();

                cboRoadBlockSession.Items.Insert(0, new ListItem((string)GetLocalResourceObject("cboRoadBlockSession.Items"), "0"));
            }
        }

        private void VisiblePanel(string[] paneList)
        {
            pnlSessionIDFilter.Visible = false;
            pnlRoadBlockSessionList.Visible = false;
            pnlSessionDetail.Visible = false;
            pnlSummonsPrint.Visible = false;
            plSelectRoadBlock.Visible = false;
            pnlShowButton.Visible = false;
            pnlConfirm.Visible = false;

            foreach (string id in paneList)
            {
                Panel p = (Panel)this.Page.FindControl(id);
                p.Visible = true;
            }
        }

        /// <summary>
        /// check the user if it's marked with road block user
        /// </summary>
        /// <returns></returns>
        private bool CheckRoadBlockUserGroup()
        {
            if (Session["userIntNo"] == null)
            {
                return false;
            }
            int userIntNo = Convert.ToInt32(Session["userIntNo"]);

            RoadBlockSessionUserDB roadblockUser = new RoadBlockSessionUserDB(connectionstring);

            if (roadblockUser.RoadBlockSessionUserCheck(userIntNo).Trim().Equals("Y"))
            {
                return true;
            }

            return false;
        }

        private bool CheckRoadBlockRule(int autIntNo, string user)
        {
            //AuthorityRulesDetails rule = new AuthorityRulesDetails();
            //rule.ARCode = "4522";
            //rule.ARComment = "N=No (Default); Y=Yes";
            //rule.ARDescr = "Enable processing of unauthorised Summonses & Notices at roadblocks";
            //rule.ARNumeric = 0;
            //rule.ARString = "N";
            //rule.AutIntNo = autIntNo;
            //rule.LastUser = user;//this.login;

            //AuthorityRulesDB db = new AuthorityRulesDB(connectionstring);
            //db.GetDefaultRule(rule);

            //return rule.ARString.Equals("Y", StringComparison.InvariantCultureIgnoreCase);

            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.ARCode = "4522";
            rule.AutIntNo = this.autIntNo;
            rule.LastUser = this.login;

            DefaultAuthRules dr = new DefaultAuthRules(rule, this.connectionstring);

            KeyValuePair<int, string> rbRule = dr.SetDefaultAuthRule();
            return rbRule.Value.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
        }

        public void PopulateClerkOfCourt()
        {
            try
            {
                ClerkofCourtDB clerkDB = new ClerkofCourtDB(connectionstring);

                chkClerkofCourtList.DataSource = clerkDB.GetClerkofCourtList();
                chkClerkofCourtList.DataTextField = "CofCFullName";
                chkClerkofCourtList.DataValueField = "CofCIntNo";
                chkClerkofCourtList.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void PopulateAuthorityList()
        {
            AuthorityDB auth = new AuthorityDB(connectionstring);

            System.Data.SqlClient.SqlDataReader reader = auth.GetAuthorityList(0, "AutName");
            chkLocalAuthList.DataSource = reader;
            chkLocalAuthList.DataValueField = "AutIntNo";
            chkLocalAuthList.DataTextField = "AutDescr";
            chkLocalAuthList.DataBind();

            reader.Close();
        }

        //2013-04-10 modify by Henry for pagination
        private SqlParameter[] GetFilterString()
        {
            List<SqlParameter> filterParams = new List<SqlParameter>();
            if ((chbClose.Checked || chbOpen.Checked) && chbClose.Checked != chbOpen.Checked)
            {
                SqlParameter indcFlag = new SqlParameter("@IndcFlag", SqlDbType.Char, 1);
                indcFlag.Value = chbClose.Checked ? "N" : "Y";
                filterParams.Add(indcFlag);
            }

            if (chkLocation.Checked)
            {
                SqlParameter locDescr = new SqlParameter("@LocDescr", SqlDbType.VarChar, 200);
                locDescr.Value = txtLocation.Text;
                filterParams.Add(locDescr);
            }

            if (chkDate.Checked)
            {
                DateTime startTime;
                DateTime endTime;
                if (DateTime.TryParse(txtStartDate.Text.Trim(), out startTime))
                {
                    SqlParameter startDate = new SqlParameter("@StartDate", SqlDbType.SmallDateTime);
                    startDate.Value = startTime;
                    filterParams.Add(startDate);
                }
                if (DateTime.TryParse(txtEndDate.Text.Trim(), out endTime))
                {
                    SqlParameter endDate = new SqlParameter("@EndDate", SqlDbType.SmallDateTime);
                    endDate.Value = endTime;
                    filterParams.Add(endDate);
                }
            }

            if (chkSessionID.Checked)
            {
                SqlParameter sessionID = new SqlParameter("@RBSessionID", SqlDbType.Char, 5);
                sessionID.Value = txtSessionID.Text.Trim();
                filterParams.Add(sessionID);
            }

            return filterParams.ToArray();

            //string rowFilter = string.Empty;
            //string status = string.Empty;
            //if (chbOpen.Checked)
            //{
            //    status = "Open";
            //}
            //if (chbClose.Checked)
            //{
            //    status = "Closed";
            //}
            //rowFilter = " IndcFlag='" + status + "'";
            //if (chbClose.Checked && chbOpen.Checked)
            //{
            //    rowFilter = " 1=1 ";
            //}

            //if (chkLocation.Checked)
            //{
            //    rowFilter += " And LocDescr like '%" + txtLocation.Text + "%'";
            //}
            //if (chkDate.Checked)
            //{
            //    if (txtStartDate.Text.Trim() != "")
            //        rowFilter += " And CrtTime >= '" + txtStartDate.Text.Trim() + "' And CrtTime <= '" + txtStartDate.Text.Trim() + " 23:59:59'";
            //    if (txtEndDate.Text.Trim() != "")
            //        rowFilter += " And ClsTime >= '" + txtEndDate.Text.Trim() + "' And ClsTime <= '" + txtEndDate.Text.Trim() + " 23:59:59'";

            //}
            //if (this.chkSessionID.Checked)
            //{
            //    rowFilter += " And RBSessionID ='" + txtSessionID.Text.Trim() + "'";
            //}

            //return rowFilter;
        }

        private void ShowRoadBlockSessionDetail(int rbsIntNo, ref string errMessage)
        {
            errMessage = String.Empty;
            pnlSessionDetail.Visible = true;
            RoadBlockDB roadDB = new RoadBlockDB(connectionstring);
            RoadBlockSessions roadBlock = roadDB.GetRoadBlockSessionByNo(rbsIntNo);

            if (roadBlock != null)
            {
                txtLocationDetail.Text = roadBlock.LocDescr;
                //Jerry 2015-03-04 change 
                //txtGPSDetail.Text = roadBlock.LocGps;
                string gpsValue = roadBlock.LocGps;
                string gPSLatitude = string.Empty;
                string gPSLongitude = string.Empty;

                GPSUtility.GetLatitudeAndLongitude(gpsValue, out gPSLatitude, out gPSLongitude);

                txtGPSLatitude.Text = gPSLatitude;
                txtGPSLongitude.Text = gPSLongitude;

                txtComments.Text = roadBlock.Comments;
            }
            else
            {
                errMessage = (string)GetLocalResourceObject("errMessage") + rbsIntNo;
                return;
            }

            // Reset checkbox lists
            foreach (ListItem item in chkClerkofCourtList.Items)
                item.Selected = false;

            StringBuilder sb = new StringBuilder();

            // Set the selected CofC
            ClerkofCourtDB clerkDB = new ClerkofCourtDB(connectionstring);
            DataView dv_AuthClerk = new DataView(clerkDB.GetAutClerkofCourt().Tables[0]);
            SqlDataReader result = clerkDB.GetClerkofCourtByRBSNo(rbsIntNo);
            while (result.Read())
            {
                foreach (ListItem item in chkClerkofCourtList.Items)
                {
                    if (item.Value.Trim() == result["CofCIntNo"].ToString().Trim())
                    {
                        item.Selected = true;

                        // Add valid authorities
                        sb.Append(item.Value).Append(',');

                        //dv_AuthClerk.RowFilter = "CofCIntNo=" + item.Value;
                        //if (dv_AuthClerk.Count > 0)
                        //{
                        //    foreach (ListItem autItem in chkLocalAuthList.Items)
                        //    {
                        //        if (autItem.Value == dv_AuthClerk[0]["AutIntNo"].ToString().Trim())
                        //        {
                        //            autItem.Selected = true;
                        //        }
                        //    }
                        //}
                    }
                }
            }

            this.GetAuthorityList(sb.ToString());
        }

        private void GetAuthorityList(string cofcIntNo)
        {
            foreach (ListItem item in chkLocalAuthList.Items)
            {
                item.Selected = false;
            }

            if (cofcIntNo == string.Empty)
            {
                return;
            }

            //ClerkofCourtDB db = new ClerkofCourtDB(this.connectionstring);
            //ClerkOfCourt_Court[] courts = db.GetClerkofCourtCourtList();

            //foreach (ListItem autItem in chkLocalAuthList.Items)
            //{
            //    if
            //}

            ClerkofCourtDB clerkDB = new ClerkofCourtDB(connectionstring);
            DataSet dsClerkofCourt = clerkDB.GetAutClerkofCourt();
            DataView dv_AuthClerk = null;
            if (dsClerkofCourt.Tables.Count > 0)
            {
                dv_AuthClerk = new DataView(dsClerkofCourt.Tables[0]);
            }

            dv_AuthClerk.RowFilter = "CofCIntNo In (" + cofcIntNo + ")";

            string AuthDesc = "";
            if (dv_AuthClerk.Count > 0)
            {
                foreach (DataRowView drv in dv_AuthClerk)
                {
                    foreach (ListItem autItem in chkLocalAuthList.Items)
                    {
                        if (autItem.Value == drv["AutIntNo"].ToString().Trim())
                        {
                            autItem.Selected = true;
                            AuthDesc += autItem.Value + "-";
                        }
                    }
                }
            }
            Session["AutDesc"] = AuthDesc;
        }

        private void ClerAuthorityListSelect()
        {
            foreach (ListItem item in chkLocalAuthList.Items)
            {
                item.Selected = false;
            }
        }

        private void ClerClerkOfCourtListSelect()
        {
            foreach (ListItem item in chkClerkofCourtList.Items)
            {
                item.Selected = false;
            }
        }

        private void ShowButtonControl(bool isAdd)
        {
            if (isAdd)
            {
                btnAdd.Visible = true;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                btnAccessSession.Visible = false;
                btnCloseSession.Visible = false;
                btnPrintRegsiter.Visible = false;
            }
            else
            {
                btnAdd.Visible = false;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;
                btnAccessSession.Visible = true;
                btnCloseSession.Visible = true;
                btnPrintRegsiter.Visible = true;
            }
        }

        private void BindPrintFile(int rbsIntNo, bool isReload)
        {
            try
            {
                if (isReload) // 2013-04-10 add by Henry for pagination
                {
                    grdSumonsMultiplePrintPager.RecordCount = 0;
                    grdSumonsMultiplePrintPager.CurrentPageIndex = 1;
                }

                RoadBlockDB roadBlockDB = new RoadBlockDB(connectionstring);
                int totalCount = 0;
                this.grdSumonsMultiplePrint.DataSource = roadBlockDB.GetAllSummonsPrintFileNameBtRBSIntNo(rbsIntNo,
                    grdSumonsMultiplePrintPager.PageSize, grdSumonsMultiplePrintPager.CurrentPageIndex, out totalCount);

                //Jerry 2014-02-17 Remove Charge from SP and related Grid in web page
                //this.grdSumonsMultiplePrint.DataKeyNames = new string[] { "SumIntNo", "ChgIntNo", "PrintAutIntNo", "PrintFile" };
                this.grdSumonsMultiplePrint.DataKeyNames = new string[] { "SumIntNo", "PrintAutIntNo", "PrintFile" };
                this.grdSumonsMultiplePrint.DataBind();
                grdSumonsMultiplePrintPager.RecordCount = totalCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region System Events

        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve the database connection string
            connectionstring = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
            {
                Server.Transfer("Login.aspx?Login=invalid");
            }
            if (Session["userIntNo"] == null)
            {
                Server.Transfer("Login.aspx?Login=invalid");
            }

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionstring);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);
            ViewState.Add("AutIntNo", autIntNo);

            // Set domain specific variables
            General gen = new General();
            background = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Dynamic style sheet
            HtmlLink html = new HtmlLink();
            html.Href = styleSheet;
            html.Attributes.Add("rel", "stylesheet");
            html.Attributes.Add("type", "text/css");
            Page.Header.Controls.Add(html);

            if (!IsPostBack)
            {
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");

                if (CheckRoadBlockUserGroup())
                {
                    if (CheckRoadBlockRule(autIntNo, login))
                    {
                        lblCourtList.Visible = false;
                        chkClerkofCourtList.Visible = false;
                        chkLocalAuthList.Visible = false;
                        lblLA.Visible = false;
                    }
                    else
                    {
                        lblCourtList.Visible = true;
                        chkClerkofCourtList.Visible = true;
                        chkLocalAuthList.Visible = true;
                        lblLA.Visible = true;
                    }

                    this.BindData(false, false);
                    this.PopulateClerkOfCourt();
                    this.PopulateAuthorityList();
                    this.PopulateRoadBlockForUserSelect();

                    txtLocation.Enabled = false;
                    txtEndDate.Enabled = false;
                    txtStartDate.Enabled = false;
                    txtSessionID.Enabled = false;

                    this.VisiblePanel(new string[] { pnlSessionIDFilter.ID, pnlRoadBlockSessionList.ID, pnlShowButton.ID });
                }
                else
                {
                    VisiblePanel(new string[] { });
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text1");
                }
            }


        }



        protected void btnFilter_Click(object sender, EventArgs e)
        {
            this.lblMessage.Text = "";
            //this.pnlSessionDetail.Visible = false;
            VisiblePanel(new string[] { this.pnlRoadBlockSessionList.ID, this.pnlSessionIDFilter.ID, this.pnlShowButton.ID });
            this.BindData(false, true);
           
        }

        protected void chkLocation_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLocation.Checked)
            {
                txtLocation.Enabled = true;
            }
            else
            {
                txtLocation.Enabled = false;
            }
        }

        protected void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDate.Checked)
            {
                txtStartDate.Enabled = true;
                txtEndDate.Enabled = true;
            }
            else
            {
                txtStartDate.Enabled = false;
                txtEndDate.Enabled = false;
            }
        }

        protected void chkSessionID_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSessionID.Checked)
            {
                txtSessionID.Enabled = true;
            }
            else
            {
                txtSessionID.Enabled = false;
            }
        }

        protected void grdRBSessionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RoadBlockSessionUserDB roadBlockDB = new RoadBlockSessionUserDB(connectionstring);
            int userIntNo = (int)Session["userIntNo"];
            int rbsIntNo = Convert.ToInt32(grdRBSessionList.SelectedDataKey["RBSIntNo"].ToString());
            //roadBlock.RBSIntNo = Convert.ToInt32(hidRBSIntNo.Value);

            rbsIntNo = roadBlockDB.CheckRoadBlockSessionUser(rbsIntNo, userIntNo);

            //Jerry 2014-02-28 add rbsIntNo into Session["RBSIntNo"] 
            if (rbsIntNo > 0)
            {
                VisiblePanel(new string[] { pnlSessionIDFilter.ID, pnlShowButton.ID, pnlConfirm.ID, pnlRoadBlockSessionList.ID });
                Session["RBSIntNo"] = rbsIntNo;
            }
            else
            {
                displayRBSList();
            }
        }

        protected void displayRBSList()
        {
            this.lblMessage.Text = "";
            VisiblePanel(new string[] { pnlSessionIDFilter.ID, pnlShowButton.ID, pnlRoadBlockSessionList.ID });
            this.ShowButtonControl(false);

            this.lblSessionDetail.Text = "Session ID: " + grdRBSessionList.SelectedDataKey["RBSessionID"];
            int rbsIntNo = Convert.ToInt32(grdRBSessionList.SelectedDataKey["RBSIntNo"].ToString());
            if (grdRBSessionList.SelectedDataKey["IndcFlag"].ToString().Trim().ToUpper() == "CLOSED")
            {
                btnAdd.Visible = false;
                btnCloseSession.Visible = false;
                btnAccessSession.Visible = false;
                //Jerry 2014-03-06 the Print Regsiter button can be showed even when the session is closed.
                //btnPrintRegsiter.Visible = false;
                btnUpdate.Visible = false;
            }
            Session["RBSIntNo"] = rbsIntNo;
            string errMessage = String.Empty;
            this.ShowRoadBlockSessionDetail(rbsIntNo, ref errMessage);
            if (errMessage != String.Empty)
            {
                lblMessage.Text = errMessage;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            VisiblePanel(new string[] { pnlSessionIDFilter.ID, pnlShowButton.ID, pnlRoadBlockSessionList.ID });
            this.ShowButtonControl(false);
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            RoadBlockSessionUserDB roadBlockDB = new RoadBlockSessionUserDB(connectionstring);
            int userIntNo = (int)Session["userIntNo"];
            int rbsIntNo = Convert.ToInt32(grdRBSessionList.SelectedDataKey["RBSIntNo"].ToString());

            rbsIntNo = roadBlockDB.AddRoadBlockSessionUser(rbsIntNo, userIntNo, this.login);
           
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.RoadBlockSession, PunchAction.Add);  


            displayRBSList();
        }

        protected void chkClerkofCourtList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkClerkofCourtList.Items.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (ListItem item in chkClerkofCourtList.Items)
                {
                    if (item.Selected)
                    {
                        sb.Append(item.Value)
                            .Append(",");
                    }
                }

                if (sb.Length > 0)
                    sb.Length -= 1;

                this.GetAuthorityList(sb.ToString());
            }
        }

        protected void btnShowAddPnl0_Click(object sender, EventArgs e)
        {
            pnlConfirm.Visible = false;
            pnlSessionDetail.Visible = true;
            txtLocationDetail.Text = "";

            //Jerry 2015-03-04 change it
            //txtGPSDetail.Text = "";
            txtGPSLatitude.Text = "";
            txtGPSLongitude.Text = "";

            ClerAuthorityListSelect();
            ClerClerkOfCourtListSelect();
            //Jerry 2014-03-04 add
            lblSessionDetail.Text = (string)GetLocalResourceObject("chkSessionID.Text");

            ShowButtonControl(true);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";

            if (txtLocationDetail.Text.Trim() == "")
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text2");
                return;
            }

            //Jerry 2015-03-03 change it
            //if (txtGPSDetail.Text.Trim() != "")
            //{
                //if (!RegexCheckInput.IsMatch(txtGPSDetail.Text.Trim()))
                //{
                //    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text3");
                //    return;
                //}
            //}

            if (txtGPSLatitude.Text.Trim() != "")
            {
                if (!IsGPS(txtGPSLatitude.Text.Trim(), true))
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text12");
                    return;
                }
            }

            if (txtGPSLongitude.Text.Trim() != "")
            {
                if (!IsGPS(txtGPSLongitude.Text.Trim(), false))
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text13");
                    return;
                }
            }

            System.Text.StringBuilder strCofCIntNo = new System.Text.StringBuilder();
            foreach (ListItem item in chkClerkofCourtList.Items)
            {
                if (item.Selected)
                    strCofCIntNo.Append(item.Value + ",");
            }
            if (strCofCIntNo.Length > 0)
            {
                strCofCIntNo.Replace(",", "", strCofCIntNo.ToString().LastIndexOf(","), 1);
            }
            else
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text4");
                return;
            }

            //Test userIntNo need to change
            int userIntNo = (int)Session["userIntNo"];
            RoadBlockSessions roadBlock = new RoadBlockSessions();
            RoadBlockDB roadBlockDB = new RoadBlockDB(connectionstring);
            roadBlock.IndcFlag = "Y";
            roadBlock.LastUser = this.login;
            roadBlock.LocDescr = txtLocationDetail.Text;
            //roadBlock.LocGps = txtGPSDetail.Text;
            roadBlock.LocGps = txtGPSLatitude.Text.Trim() + "," + txtGPSLongitude.Text.Trim();
            roadBlock.Comments = txtComments.Text;
            //roadBlock.ClsTime 
            //roadBlock.RBSIntNo = Convert.ToInt32(hidRBSIntNo.Value);

            int rbsIntNo = roadBlockDB.AddRoadBlockSession(roadBlock, userIntNo, strCofCIntNo.ToString());
            if (rbsIntNo != 0)
            {
                Session["RBSIntNo"] = rbsIntNo;
                // Jake 2013-09-13 modified pagination 
                this.BindData(false, false);
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.RoadBlockSession, PunchAction.Add);
            }
            else
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text5");
            }

            this.pnlSessionDetail.Visible = false;

            //this.ShowButtonControl(false);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";


            if (txtLocationDetail.Text.Trim() == "")
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text2");
                return;
            }

            //Jerry 2015-03-04 change
            //if (txtGPSDetail.Text.Trim() != "")
            //{
            //    if (!RegexCheckInput.IsMatch(txtGPSDetail.Text.Trim()))
            //    {
            //        lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text3");
            //        return;
            //    }
            //}

            if (txtGPSLatitude.Text.Trim() != "")
            {
                if (!IsGPS(txtGPSLatitude.Text.Trim(), true))
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text12");
                    return;
                }
            }

            if (txtGPSLongitude.Text.Trim() != "")
            {
                if (!IsGPS(txtGPSLongitude.Text.Trim(), false))
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text13");
                    return;
                }
            }

            System.Text.StringBuilder strCofCIntNo = new System.Text.StringBuilder();
            foreach (ListItem item in chkClerkofCourtList.Items)
            {
                if (item.Selected)
                    strCofCIntNo.Append(item.Value + ",");
            }
            if (strCofCIntNo.Length > 0)
            {
                strCofCIntNo.Replace(",", "", strCofCIntNo.ToString().LastIndexOf(","), 1);
            }
            else
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text4");
                return;
            }

            int RBSIntNo = 0;

            //Test userIntNo need to change
            int userIntNo = (int)Session["userIntNo"];
            RoadBlockSessions roadBlock = new RoadBlockSessions();
            RoadBlockDB roadBlockDB = new RoadBlockDB(connectionstring);
            roadBlock.IndcFlag = grdRBSessionList.SelectedDataKey["IndcFlag"].ToString().Trim() == "Open" ? "Y" : "N";
            roadBlock.LastUser = this.login;
            roadBlock.LocDescr = txtLocationDetail.Text;
            //roadBlock.LocGps = txtGPSDetail.Text;
            roadBlock.LocGps = txtGPSLatitude.Text.Trim() + "," + txtGPSLongitude.Text.Trim();
            roadBlock.RBSIntNo = Convert.ToInt32(grdRBSessionList.SelectedDataKey["RBSIntNo"].ToString());
            roadBlock.Comments = txtComments.Text;
            //roadBlock.RBSIntNo = Convert.ToInt32(hidRBSIntNo.Value);

            RBSIntNo = roadBlockDB.UpdateRoadBolckSession(roadBlock, userIntNo, strCofCIntNo.ToString());
            if (RBSIntNo > 0)
            {
                //Session["RBSIntNo"] = RBSIntNo;
                // Jake 2013-09-13 modified pagination 
                BindData(true, false);
                pnlSessionDetail.Visible = false;
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.RoadBlockSession, PunchAction.Change);
            }
            else
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text6");
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            int rbsIntNo = Convert.ToInt32(grdRBSessionList.SelectedDataKey["RBSIntNo"].ToString());
            RoadBlockDB roadBlockDB = new RoadBlockDB(connectionstring);
            if (roadBlockDB.CheckRoadBlockInSummons(rbsIntNo) > 0)
            {
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text7");
            }
            else
            {
                if (roadBlockDB.DeleteRoadBlockSesion(rbsIntNo) > 0)
                {
                    lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text8");
                    pnlSessionDetail.Visible = false;

                    // Jake 2013-09-13 modified pagination 
                    this.BindData(false, false);
                   
                    //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                    SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                    punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.RoadBlockSession, PunchAction.Delete);
                }
            }
        }

        protected void btnAccessSession_Click(object sender, EventArgs e)
        {
            string rbsIntNo = grdRBSessionList.SelectedDataKey["RBSIntNo"].ToString();
            if (rbsIntNo != "")
            {
                Response.Redirect("SummonsRoadBlock.aspx");
            }
        }

        protected void btnCloseSession_Click(object sender, EventArgs e)
        {
            this.lblMessage.Text = "";
            int rbsIntNo = Convert.ToInt32(grdRBSessionList.SelectedDataKey["RBSIntNo"].ToString());
            this.BindPrintFile(rbsIntNo, true);

            pnlSessionIDFilter.Visible = false;
            pnlRoadBlockSessionList.Visible = false;
            pnlSessionDetail.Visible = false;
            pnlSummonsPrint.Visible = true;
            plSelectRoadBlock.Visible = false;
            pnlShowButton.Visible = false;

            int status = new RoadBlockDB(connectionstring).UpdateRoadBlockStatus(rbsIntNo, "N", this.login, DateTime.Now);
            if (status == 0)
                lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text9") + rbsIntNo + ")";
            else
            {
                this.BindData(false, false); // Jake 2013-09-13 modified pagination 
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.RoadBlockSession, PunchAction.Delete);
            }
        }

        protected void btnPrintRegsiter_Click(object sender, EventArgs e)
        {
            //plSelectRoadBlock.Visible = true;
            //PopulateRoadBlockForUserSelect();
            this.lblMessage.Text = "";
            VisiblePanel(new string[] { this.plSelectRoadBlock.ID });
            //Jerry 2014-02-28 for cboRoadBlockSession default selected value
            if (Session["RBSIntNo"] != null)
            {
                int rbsIntNo = Convert.ToInt32(Session["RBSIntNo"]);
                cboRoadBlockSession.SelectedValue = rbsIntNo.ToString();
            }
        }

        protected void grdSumonsMultiplePrint_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdSumonsMultiplePrint.SelectedIndex > -1)
            {
                this.lblMessage.Text = "";
                int sumIntNo = (int)this.grdSumonsMultiplePrint.SelectedDataKey["SumIntNo"];
                //Jerry 2014-02-17 Remove Charge from SP and related Grid in web page
                //int chgIntNo = (int)this.grdSumonsMultiplePrint.SelectedDataKey["ChgIntNo"];
                int printAutIntNo = (int)this.grdSumonsMultiplePrint.SelectedDataKey["PrintAutIntNo"];
                Session["PrintAutIntNo"] = printAutIntNo;

                ChargeDB db = new ChargeDB(connectionstring);

                //Jerry 2014-02-17 Remove Charge from SP and related Grid in web page
                //db.UpdateStatus(chgIntNo, STATUS_SUMMONS_PRINTED, this.login);
                SIL.AARTO.DAL.Entities.NoticeSummons noticeSummonsEntity = new SIL.AARTO.DAL.Services.NoticeSummonsService().GetBySumIntNo(sumIntNo)[0];
                if (noticeSummonsEntity != null)
                {
                    SIL.AARTO.DAL.Entities.TList<SIL.AARTO.DAL.Entities.Charge> chargeEntityList = new SIL.AARTO.DAL.Services.ChargeService().GetByNotIntNo(noticeSummonsEntity.NotIntNo);
                    foreach (SIL.AARTO.DAL.Entities.Charge chargeEntity in chargeEntityList)
                    {
                        //charge status <= 620
                        if (chargeEntity.ChargeStatus <= (int)SIL.AARTO.DAL.Entities.ChargeStatusList.SummonsPrinted)
                            db.UpdateStatus(chargeEntity.ChgIntNo, STATUS_SUMMONS_PRINTED, this.login);
                    }
                }

                // Open the viewer page
                string printFile = (string)this.grdSumonsMultiplePrint.SelectedDataKey["PrintFile"];

                QSParams param = new QSParams("printfile", printFile);

                // Jake 20090514 add new parameters roadBlock Status
                QSParams paramRoadBlock = new QSParams("roadBlock", "Closed");
                PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");

                pageToOpen.Parameters.Add(param);
                pageToOpen.Parameters.Add(paramRoadBlock);

                Helper_Web.BuildPopup(pageToOpen);
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionstring);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.RoadBlockSession, PunchAction.Change);
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            this.lblMessage.Text = string.Empty;
            this.pnlSessionIDFilter.Visible = true;
            this.plSelectRoadBlock.Visible = false;
            this.pnlRoadBlockSessionList.Visible = true;
            this.pnlSummonsPrint.Visible = false;
        }

        protected void OKButton_Click(object sender, EventArgs e)
        {
            this.lblMessage.Text = "";
            if (cboRoadBlockSession.SelectedIndex > 0)
            {
                //string printFile = (string)this.ViewState["PrintFile"];
                int rbsIntNo = Convert.ToInt32(cboRoadBlockSession.SelectedValue);
                ////dls 070906 - no longer necessary to pass thru '~' for a single summons, otherwise print dates don't get set correctly
                ////QSParams param = new QSParams("printfile", "~" + printFile);
                ////QSParams param = new QSParams("rbsIntNo", printFile);
                //QSParams param = new QSParams("rbsIntNo", rbsIntNo);
                //QSParams paramRoadBlock = new QSParams("roadBlock", "Close");
                //PageToOpen pageToOpen = new PageToOpen(this, "SummonsViewer.aspx");
                ////pageToOpen.Parameters.Add(param);
                //pageToOpen.Parameters.Add(param);
                //pageToOpen.Parameters.Add(paramRoadBlock);
                //Helper_Web.BuildPopup(pageToOpen);

                BindPrintFile(rbsIntNo, true);
                VisiblePanel(new string[] { pnlSummonsPrint.ID });
            }
            else
            {
                this.lblMessage.Text = (string)GetLocalResourceObject("lblMessage.Text10");
            }
        }

        #endregion

        // 2013-04-10 add by Henry for pagination
        protected void grdRBSessionListPager_PageChanged(object sender, EventArgs e)
        {
            BindData(false, false);
        }

        // 2013-04-10 add by Henry for pagination
        protected void grdSumonsMultiplePrintPager_PageChanged(object sender, EventArgs e)
        {
            //Jerry 2014-02-17 change it
            //int rbsIntNo = Convert.ToInt32(grdRBSessionList.SelectedDataKey["RBSIntNo"].ToString());
            if (cboRoadBlockSession.SelectedIndex > 0)
            {
                int rbsIntNo = Convert.ToInt32(cboRoadBlockSession.SelectedValue);
                BindPrintFile(rbsIntNo, false);
            }
        }

        private bool IsGPS(string txtGPSValue, bool isLatitude)
        {
            if (!RegexCheckInput.IsMatch(txtGPSValue.Trim()))
            {
                return false;
            }

            double gPSValue = Convert.ToDouble(txtGPSValue.Trim());

            if (isLatitude)
            {
                if (gPSValue < -90.0000 || gPSValue > 90.0000)
                {
                    return false;
                }
            }
            else
            {
                if (gPSValue < -180.0000 || gPSValue > 180.0000)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
