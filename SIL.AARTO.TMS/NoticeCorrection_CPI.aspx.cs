using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using SIL.AARTO.BLL.Utility.Cache;
using System.Threading;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Configuration;
using System.Transactions;
using System.IO;
using SIL.AARTO.BLL.Account;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents an editor page for correcting and resetting individual and batches of Ciprus errors.
    /// </summary>
    public partial class NoticeCorrection_CPI : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;
        private int autIntNo = 0;
        private int notIntNo = 0;
        private int userAccessLevel = 0;
        private string alphaOnly = @"^[\sa-zA-Z\(\)'-]+$";
        private string alphaNumeric = @"^[\s&,.\\0-9a-zA-Z\(\)'-]+$";
        private string numericOnly = @"^\d+$";

        protected string styleSheet;
        protected string backgroundImage;
        protected string thisPageURL = "NoticeCorrection_CPI.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        //protected string thisPage = "Correction of Notice Detail";

        // Constants
        private const int STATUS_ERROR = 110;
        private const int STATUS_ERROR_FIXED_CPI = 120;
        private const int STATUS_ERROR_FIXED_NONCPI = 5;
        private const int STATUS_CANCELLED_CPI = 960;
        private const int STATUS_CANCELLED_NONCPI = 922;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            Session["userLoginName"] = userDetails.UserLoginName;
            userAccessLevel = userDetails.UserAccessLevel;

            //set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                Session["editNotIntNo"] = null;

                //2012-12-21 updated by Nancy
                //if (Session["TicketProcessor"].ToString().Equals("CiprusPI"))
                //    lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                //else if (Session["TicketProcessor"].ToString().Equals("Cip_CofCT"))
                //    lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                //else
                lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text1");

                this.pnlEditNotice.Visible = false;
                this.btnOptReport.Visible = true;
                this.pnlNoticeList.Visible = false;
                this.pnlEditNotice.Visible = false;

                this.PopulateAuthorities(autIntNo);

                BindGrid(autIntNo, false);

                btnProcess.Attributes.Add("onclick", "return confirm('" + (string)GetLocalResourceObject("btnProcess.Attributes") + "');");
            }

            if (ddlSelectLA.SelectedIndex > -1)
                this.autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

        }

        // Modefied By Jake 2010-04-15
        // Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        private void PopulateAuthorities( int autIntNo)
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");
            
            Dictionary<int, string> lookups =
                AuthorityLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                int AutIntNo = (int)data.Tables[0].Rows[i]["AutIntNo"];
                if (lookups.ContainsKey(AutIntNo))
                {
                    ddlSelectLA.Items.Add(new ListItem(lookups[AutIntNo], AutIntNo.ToString()));
                }
            }
            //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);
            //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
            //ddlSelectLA.DataSource = data;
            //ddlSelectLA.DataValueField = "AutIntNo";
            //ddlSelectLA.DataTextField = "AutName";
            //ddlSelectLA.DataBind();
            ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

            //reader.Close();
        }

        private void BindGrid(int autIntNo, bool isRefresh)
        {
            Stalberg.TMS.NoticeDB notice = new Stalberg.TMS.NoticeDB(this.connectionString);
            //2013-09-13 Removed by Nancy
            //if (isRefresh) //2013-04-09 add by Henry for pagination
            //{
            //    grvNoticesPager.RecordCount = 0;
            //    grvNoticesPager.CurrentPageIndex = 1;
            //}

            //get all the notices that have errors
            int totalCount = 0;
            DataSet ds = notice.GetNoticeList_CPI_DS(autIntNo, STATUS_ERROR, STATUS_ERROR, grvNoticesPager.PageSize, grvNoticesPager.CurrentPageIndex, out totalCount);
            this.grvNotices.DataSource = ds;
            this.grvNotices.DataKeyNames = new string[] { "NotIntNo" };
            this.grvNotices.DataBind();
            grvNoticesPager.RecordCount = totalCount;

            lblError.Visible = false;
            lblError.Text = string.Empty;
            if (ds.Tables[0].Rows.Count == 0)
            {
                lblError.Visible = true;
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
                this.pnlNoticeList.Visible = false;
                this.btnOptReport.Visible = false;
            }
            else
            {
                this.pnlNoticeList.Visible = true;
                this.btnOptReport.Visible = true;
            }

            this.pnlEditNotice.Visible = false;
        }

        protected void ddlSelectLA_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);

            BindGrid(autIntNo, true);
        }

         

        private void BindDetails(NoticeDetails detail)
        {
            //2013-08-26 get user detail
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            int notIntNo = Convert.ToInt32(Session["editNotIntNo"]);

            bool systemError = false;
            btnProcess.Enabled = false;
            btnCancel.Enabled = true;
            btnSkip.Enabled = true;
            txtErrField.Text = string.Empty;
            txtErrField1.Visible = false;
            txtErrField2.Visible = false;
            txtErrField3.Visible = false;
            txtOtherDetails.Visible = false;
            lblAdditionalInfo.Visible = false;
            lblErrDescr.Text = detail.NotCiprusPIProcessDescr;

            imageFullPath.Visible = false;
            this.imageErrorInfo.Visible = false;
            this.myImage.Visible = false;

            lblNotFilmFrame.Text = detail.NotFilmNo + " / " + detail.NotFrameNo;
            Session["reference"] = lblNotFilmFrame.Text;

            lblRegNo.Text = detail.NotRegNo;
            lblOffenceDate.Text = detail.NotOffenceDate.ToString("yyyy-MM-dd HH:mm");

            string stAddr = string.Empty;
            string stAddr1 = string.Empty;
            string stAddr2 = string.Empty;
            string stAddr3 = string.Empty;
            string stPostCode = string.Empty;

            string poAddr = string.Empty;
            string poAddr1 = string.Empty;
            string poAddr2 = string.Empty;
            string poAddr3 = string.Empty;
            string poPostCode = string.Empty;

            ChargeDB charge = new ChargeDB(connectionString);
            DriverDB driver = new DriverDB(connectionString);
            ProxyDB proxy = new ProxyDB(connectionString);
            SqlDataReader reader = null;

            DriverDetails drvDetails = null;
            ProxyDetails prxDetails = null;

            ScanImageDB imageDB = new ScanImageDB(this.connectionString);

            ScanImageDetails imageDetails = null;

            if (detail.NotNatisProxyIndicator.Equals("P"))
            {
                prxDetails = proxy.GetProxyDetailsByNotice(notIntNo);
                stAddr = prxDetails.PrxStAdd1;
                stAddr1 = prxDetails.PrxStAdd2;
                stAddr2 = prxDetails.PrxStAdd3;
                stAddr3 = prxDetails.PrxStAdd4;
                stPostCode = prxDetails.PrxStCode;
                poAddr = prxDetails.PrxPOAdd1;
                poAddr1 = prxDetails.PrxPOAdd2;
                poAddr2 = prxDetails.PrxPOAdd3;
                poAddr3 = prxDetails.PrxPOAdd4;
                poPostCode = prxDetails.PrxPOCode;
            }
            else
            {
                drvDetails = driver.GetDriverDetailsByNotice(notIntNo);
                stAddr = drvDetails.DrvStAdd1;
                stAddr1 = drvDetails.DrvStAdd2;
                stAddr2 = drvDetails.DrvStAdd3;
                stAddr3 = drvDetails.DrvStAdd4;
                stPostCode = drvDetails.DrvStCode;
                poAddr = drvDetails.DrvPOAdd1;
                poAddr1 = drvDetails.DrvPOAdd2;
                poAddr2 = drvDetails.DrvPOAdd3;
                poAddr3 = drvDetails.DrvPOAdd4;
                poPostCode = drvDetails.DrvPOCode;
            }

            switch (detail.NotCiprusPIProcessCode)
            {
                case 1:         //invalid type of owner
                    txtErrField.Text = detail.NotStatutoryOwner;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text");
                    systemError = true;
                    break;

                case 2:         //invalid business name
                    txtErrField.Text = drvDetails.DrvSurname;
                    btnProcess.Enabled = true;
                    break;

                case 3:         //duplicate case (RegNo, date & time, offence code)
                    reader = charge.GetChargeListByNotice(notIntNo, STATUS_ERROR);
                    string offenceCode = string.Empty;
                    while (reader.Read())
                    {
                        offenceCode += reader["ChgOffenceCode"].ToString() + ";";
                    }
                    reader.Dispose();

                    txtErrField.Text = string.Format((string)GetLocalResourceObject("txtErrField.Text"), detail.NotRegNo, detail.NotOffenceDate.ToString("yyyy-MM-dd HH:mm"), offenceCode);

                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text1");
                    systemError = true;
                    break;

                case 9:         //Invalid ID number
                    string idNo = string.Empty;
                    if (detail.NotNatisProxyIndicator.Equals("P"))
                        idNo = prxDetails.PrxIDNumber;
                    else
                        idNo = drvDetails.DrvIDNumber;

                    txtErrField.Text = idNo;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text2");
                    break;

                case 10:         //Offence code not registered
                case 32:         //Invalid location code
                case 33:         //Unable to calculate amount. Offence code / speed?
                    reader = charge.GetChargeListByNotice(notIntNo, STATUS_ERROR);

                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text3");
                    txtErrField.Visible = true;
                    while (reader.Read())
                    {
                        txtErrField.Text += reader["Offence"].ToString();
                    }
                    reader.Dispose();
                    break;

                case 11:         //Invalid surname
                    string surname = string.Empty;
                    if (detail.NotNatisProxyIndicator.Equals("P"))
                        surname = prxDetails.PrxSurname;
                    else
                        surname = drvDetails.DrvSurname;

                    txtErrField.Text = surname;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text4");
                    break;

                case 13:         //Invalid initials
                    string intials = string.Empty;
                    if (detail.NotNatisProxyIndicator.Equals("P"))
                        intials = prxDetails.PrxInitials;
                    else
                        intials = drvDetails.DrvInitials;

                    txtErrField.Text = intials;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text4");
                    break;

                case 14:         //Processed ok - will never be an error
                    break;

                case 15:         //Invalid postal code in street address

                    txtErrField.Text = stPostCode;
                    txtOtherDetails.Text = (string)GetLocalResourceObject("txtOtherDetails.Text") + "\n" + stAddr + "\n" + stAddr1 + "\n" + stAddr2 + "\n"
                        + stAddr3 + "\n" + stPostCode + "\n\n";

                    txtOtherDetails.Text += (string)GetLocalResourceObject("txtOtherDetails.Text1") + "\n" + poAddr + "\n" + poAddr2 + "\n"
                        + poAddr3 + "\n" + poAddr3 + "\n";

                    txtOtherDetails.Visible = true;
                    lblAdditionalInfo.Visible = true;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text5");
                    //BD Need to handle the visibility of the postal code lookup as it should not be shown for all errors.
                    txtAddress4.Visible = true;
                    ddlPostalCodesA.Visible = true;
                    btnPostalCodesA.Visible = true;
                    lblPostalCode1.Visible = true;
                    lblPostalCode2.Visible = true;
                    lblPostalCode3.Visible = true;
                    lblPostalCode4.Visible = true;
                    break;

                case 16:         //Invalid street address
                    txtErrField1.Visible = true;
                    txtErrField2.Visible = true;
                    txtErrField3.Visible = true;

                    txtErrField.Text = stAddr;
                    txtErrField1.Text = stAddr1;
                    txtErrField2.Text = stAddr2;
                    txtErrField3.Text = stAddr3;

                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text6");
                    //BD Need to handle the visibility of the postal code lookup as it should not be shown for all errors.
                    txtAddress4.Visible = true;
                    ddlPostalCodesA.Visible = true;
                    btnPostalCodesA.Visible = true;
                    lblPostalCode1.Visible = true;
                    lblPostalCode2.Visible = true;
                    lblPostalCode3.Visible = true;
                    lblPostalCode4.Visible = true;
                    break;

                case 17:         //Invalid local authority code
                    txtErrField.Text = detail.AutName;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 18:         //Invalid court code
                    txtErrField.Text = detail.NotCourtNo;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text3");
                    systemError = true;
                    break;

                case 12:         //Notice number not in sequence
                case 19:         //Duplicate notice number
                case 26:         //Invalid notice number
                    txtErrField.Text = detail.NotTicketNo;

                    if (txtErrField.Text.Equals("")) txtErrField.Text = (string)GetLocalResourceObject("lblError.Tex12");
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 20:         //Invalid postal address
                    txtErrField1.Visible = true;
                    txtErrField2.Visible = true;
                    txtErrField3.Visible = true;

                    txtErrField.Text = poAddr;
                    txtErrField1.Text = poAddr1;
                    txtErrField2.Text = poAddr2;
                    txtErrField3.Text = poAddr3;

                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text6");
                    //BD Need to handle the visibility of the postal code lookup as it should not be shown for all errors.
                    txtAddress4.Visible = true;
                    ddlPostalCodesA.Visible = true;
                    btnPostalCodesA.Visible = true;
                    lblPostalCode1.Visible = true;
                    lblPostalCode2.Visible = true;
                    lblPostalCode3.Visible = true;
                    lblPostalCode4.Visible = true;
                    break;

                case 21:         //invalid record type
                case 22:         //Invalid interface type
                case 23:         //Invalid version control
                case 24:         //Invalid Supplier code
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 25:         //Invalid unit id
                    txtErrField.Text = detail.NotCameraID;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 27:         //Invalid officer-number
                    txtErrField.Text = detail.NotOfficerNo;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 28:         //Invalid location code
                    txtErrField.Text = detail.NotLocCode;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    break;

                case 29:         //Invalid date of offence
                    txtErrField.Text = detail.NotOffenceDate.ToString("yyyy-MM-dd");
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");;
                    systemError = true;
                    break;

                case 30:         //Invalid time of offence
                    txtErrField.Text = detail.NotOffenceDate.ToString("HH:mm");
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");;
                    systemError = true;
                    break;

                case 31:         //Invalid registration no
                    txtErrField.Text = detail.NotRegNo;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");;
                    systemError = true;
                    break;

                case 34:         //Invalid postal code in postal address                
                    txtErrField.Text = poPostCode;

                    txtOtherDetails.Text = (string)GetLocalResourceObject("txtOtherDetails.Text1") + "\n" + poAddr + "\n" + poAddr2 + "\n"
                        + poAddr3 + "\n" + poAddr3 + "\n" + poPostCode + "\n\n";

                    txtOtherDetails.Text += (string)GetLocalResourceObject("txtOtherDetails.Text") + "\n" + stAddr + "\n" + stAddr1 + "\n" + stAddr2 + "\n"
                        + stAddr3 + "\n" + stPostCode;

                    txtOtherDetails.Visible = true;
                    lblAdditionalInfo.Visible = true;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text8");
                    //BD Need to handle the visibility of the postal code lookup as it should not be shown for all errors.
                    txtAddress4.Visible = true;
                    ddlPostalCodesA.Visible = true;
                    btnPostalCodesA.Visible = true;
                    lblPostalCode1.Visible = true;
                    lblPostalCode2.Visible = true;
                    lblPostalCode3.Visible = true;
                    lblPostalCode4.Visible = true;
                    break;

                case 35:         //Invalid vehicle make
                    txtErrField.Text = detail.NotVehicleMakeCode;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text3");
                    systemError = true;
                    break;

                case 36:         //Invalid vehicle type
                    txtErrField.Text = detail.NotVehicleTypeCode;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text3");
                    systemError = true;
                    break;

                case 37:         //Invalid film number
                    txtErrField.Text = detail.NotFilmNo;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 38:         //Invalid reference number
                    txtErrField.Text = detail.NotIntNo.ToString();
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 39:         //Invalid speed reading
                    txtErrField.Text = detail.NotSpeed1.ToString();
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 40:         //File not processed - errors
                case 41:         //Invalid date captured
                case 42:         //Invalid date verified
                case 43:         //Invalid NaTIS date
                case 44:         //Invalid date adjudicated
                case 45:         //Invalid adjudicating officer
                case 51:         //Number of cases invalid
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;

                case 52:         //Direction invalid
                    txtErrField.Text = detail.NotTravelDirection;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;
                //Seawen 2013-08-23 add 72-81 error fixed
                /* NoticeGenerate_JMPD_ExportToNTIService will do not check the data
            case 72:        //IssAuthority
                txtErrField.Visible = false;
                lblInstruction.Text = (string)GetLocalResourceObject("IssAuthority.Text");
                systemError = true;
                break;
            case 74:        //RegAuthority
                txtErrField.Visible = false;
                lblInstruction.Text = (string)GetLocalResourceObject("RegAuthority.Text");
                systemError = true;
                break;*/
                case 73:        //because notice.Province = 4 so the error never happened
                    txtErrField.Visible = false;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text9");
                    systemError = true;
                    break;
                case 75:        //VehImage
                    lblInstruction.Text = (string)GetLocalResourceObject("VehImage.Text");
                    txtErrField.Visible = false;
                    imageFullPath.Visible = true;
                    imageDetails = imageDB.GetImageFullPath(detail.VehImageIntNo);
                    imageFullPath.Text = string.Format("{0}: {1}", "Path", imageDetails.ImageFullPath);
                    showImage(imageDetails.ImageFullPath);
                    systemError = true;
                    break;
                case 76:        //LicImage
                    lblInstruction.Text = (string)GetLocalResourceObject("LicImage.Text");
                    txtErrField.Visible = false;
                    imageFullPath.Visible = true;
                    imageDetails = imageDB.GetImageFullPath(detail.VehImageIntNo);
                    imageFullPath.Text = string.Format("{0}: {1}", "Path", imageDetails.ImageFullPath);
                    showImage(imageDetails.ImageFullPath);
                    systemError = true;
                    break;
                case 77:        //OtherLocInfo
                    txtErrField.Visible = false;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text9");
                    systemError = true;
                    break;
                case 78:        //StreetB
                    txtErrField.Visible = false;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text9");
                    systemError = true;
                    break;
                case 79:        //Suburb
                    txtErrField.Visible = false;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text9");
                    systemError = true;
                    break;
                case 80:        //MagisterialCD
                    txtErrField.Visible = false;
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text9");
                    systemError = true;
                    break;
                case 81:        //OffenceCode
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text8");
                    txtErrField.Text = "";
                    txtErrField.Visible = true;
                    systemError = true;
                    break;

                default:
                    lblInstruction.Text = (string)GetLocalResourceObject("lblInstruction.Text7");
                    systemError = true;
                    break;
            }

            if (systemError)
            {
                //dls 061229 - not sure why this wasn't allowing updates - should allow for admin user
                //btnProcess.Enabled = false;

                //Seawen 2013-08-26 
                //change it to check if user has UserRole = Administrator or SystemAdmin
                bool isAdmin = false;
                List<AartoUserRoleConjoin> roleConjoinList = UserLoginManager.GetUserRolesByUserIntNo((int)Session["userIntNo"]);
                AartoUserRoleQuery query = new AartoUserRoleQuery();
                query.AppendIn(AartoUserRoleColumn.AaUserRoleName, new string[] { "Administrator", "SystemAdmin" });
                AartoUserRole[] roleList = new AartoUserRoleService().Find(query as IFilterParameterCollection).ToArray();

                foreach (var roleItem in roleList)
                {
                    var adminRole = roleConjoinList.Find(item => item.AaUserRoleId == roleItem.AaUserRoleId);
                    if (adminRole != null)
                    {
                        isAdmin = true;
                        break;
                    }
                }

                if (isAdmin)
                {
                    btnProcess.Enabled = true;
                    btnCancel.Enabled = true;
                }
                else
                {
                    btnProcess.Enabled = false;
                    btnCancel.Enabled = false;
                }
            }
            else
            {
                btnProcess.Enabled = true;
            }
        }

        //protected void grvNotices_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    notIntNo = (int)this.grvNotices.DataKeys[e.NewEditIndex]["NotIntNo"];

        //    Session["editNotIntNo"] = notIntNo;

        //    NoticeDB db = new NoticeDB(this.connectionString);
        //    NoticeDetails details = db.GetNoticeDetails(notIntNo);
        //    this.BindDetails(details);

        //    this.pnlEditNotice.Visible = true;
        //}

        protected void btnOptReport_Click(object sender, EventArgs e)
        {
            string strRedirectScript = "<script language=\"javascript\">\n"
                + "window.open('NoticeExceptionViewer.aspx?aut=" + autIntNo.ToString() + "', 'exceptions')\n"
                        + "</script>\n";
            Response.Write(strRedirectScript);
        }

        protected void btnSkip_Click(object sender, EventArgs e)
        {
            BindGrid(autIntNo, true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            int notIntNo = Convert.ToInt32(Session["editNotIntNo"]);
            string reference = Session["reference"].ToString();

            int updChgIntNo = UpdateChargeStatus(notIntNo, "cancel");

            if (updChgIntNo > 0)
            {
                lblError.Text =string.Format((string)GetLocalResourceObject("lblError.Text1"),reference);
              
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticeCorrectionCPI, PunchAction.Change);  

                //2013-09-13 Added by Nancy for correct pagination start
                if (grvNotices.Rows.Count == 1)
                {
                    grvNoticesPager.CurrentPageIndex--;
                }
                //2013-09-13 Added by Nancy for correct pagination end
                BindGrid(autIntNo, true);
            }

        }

        private int UpdateChargeStatus(int notIntNo, string type)
        {
            int updChgIntNo = 0;
            string reference = Session["reference"].ToString();

            ChargeDB charge = new ChargeDB(connectionString);
            switch (type)
            {
                case "cancel":
                    int statusCancelled = STATUS_CANCELLED_NONCPI;
                    //2012-12-21 updated by Nancy
                    //if (Session["TicketProcessor"].ToString().Equals("CiprusPI") || Session["TicketProcessor"].ToString().Equals("Cip_CofCT"))
                    //    statusCancelled = STATUS_CANCELLED_CPI;

                    //updChgIntNo = charge.ChargeStatusChangeForNotice(notIntNo, STATUS_ERROR, STATUS_CANCELLED, login);
                    updChgIntNo = charge.ChargeStatusChangeForNotice(notIntNo, STATUS_ERROR, statusCancelled, login);
                    break;

                case "fix":
                    int statusFixed = STATUS_ERROR_FIXED_NONCPI;
                    //2012-12-21 updated by Nancy
                    //if (Session["TicketProcessor"].ToString().Equals("CiprusPI") || Session["TicketProcessor"].ToString().Equals("Cip_CofCT"))
                    //    statusFixed = STATUS_ERROR_FIXED_CPI;

                    //updChgIntNo = charge.ChargeStatusChangeForNotice(notIntNo, STATUS_ERROR, STATUS_ERROR_FIXED, login);
                    updChgIntNo = charge.ChargeStatusChangeForNotice(notIntNo, STATUS_ERROR, statusFixed, login);
                    break;
            }

            if (updChgIntNo < 1)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2") + reference;
                NoticeDB notice = new NoticeDB(this.connectionString);
                NoticeDetails details = notice.GetNoticeDetails(notIntNo);

                BindDetails(details);
            }
            return updChgIntNo;
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            this.lblError.Visible = true;
            int notIntNo = Convert.ToInt32(Session["editNotIntNo"]);
            string reference = Session["reference"].ToString();

            bool updateValue = true;
            bool isValid = true;
            StringBuilder sb = new StringBuilder("<p>" + (string)GetLocalResourceObject("lblError.Text5") + "\n<ul>");

            NoticeDB notice = new NoticeDB(this.connectionString);
            NoticeDetails details = notice.GetNoticeDetails(notIntNo);

            string column = string.Empty;
            string table = string.Empty;
            string strValue = string.Empty;
            int intValue = 0;
            string errMessage = string.Empty;

            if (details.NotNatisProxyIndicator.Equals("P"))
                table = "Proxy";
            else
                table = "Driver";

            switch (details.NotCiprusPIProcessCode)
            {

                case 1:         //invalid type of owner
                case 3:         //duplicate case (RegNo, date & time, offence code)
                case 10:         //Offence code not registered
                case 32:         //Invalid location code
                case 33:         //Unable to calculate amount. Offence code / speed?
                case 17:         //Invalid local authority code
                case 18:         //Invalid court code
                case 12:         //Notice number not in sequence
                case 19:         //Duplicate notice number
                case 26:         //Invalid notice number
                case 21:         //invalid record type
                case 22:         //Invalid interface type
                case 23:         //Invalid version control
                case 24:         //Invalid Supplier code
                case 25:         //Invalid unit id
                case 27:         //Invalid officer-number
                case 28:         //Invalid location code
                case 29:         //Invalid date of offence
                case 30:         //Invalid time of offence
                case 31:         //Invalid registration no
                case 35:         //Invalid vehicle make
                case 36:         //Invalid vehicle type
                case 37:         //Invalid film number
                case 38:         //Invalid reference number
                case 39:         //Invalid speed reading
                case 40:         //File not processed - errors
                case 41:         //Invalid date captured
                case 42:         //Invalid date verified
                case 43:         //Invalid NaTIS date
                case 44:         //Invalid date adjudicated
                case 45:         //Invalid adjudicating officer
                case 51:         //Number of cases invalid
                case 52:         //Direction invalid
                case 62:         //Invalid OfficerNatisNo
                //Seawen 2013-08-23 add 73,77,78,79,80 don't need update and user can cancel or resubmit
                //case 72:       //IssAuthority
                case 73:         //because notice.Province = 4 so the error never happened
                //case 74:       //RegAuthority
                case 77:        //LocDescr
                case 78:        //StreetB
                case 79:        //Suburb
                case 80:        //MagisterialCD
                    updateValue = false;
                    break;

                case 2:         //invalid business name
                    table = "Driver";
                    column = "DrvSurname";
                    strValue = txtErrField.Text;

                    //test that this is only alpha characters
                    if (!Regex.IsMatch(strValue, alphaOnly))
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text6") + ",(,),',- </li>\n");
                    }

                    break;

                case 11:         //Invalid surname
                    if (details.NotNatisProxyIndicator.Equals("P"))
                        column = "PrxSurname";
                    else
                        column = "DrvSurname";

                    strValue = txtErrField.Text;

                    //test that this is only alpha characters
                    if (!Regex.IsMatch(strValue, alphaOnly))
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text7") + ",(,),',- </li>\n");
                    }

                    break;

                case 13:         //Invalid initials
                    if (details.NotNatisProxyIndicator.Equals("P"))
                        column = "PrxInitials";
                    else
                        column = "DrvInitials";
                    strValue = txtErrField.Text;

                    //test that this is only alpha characters
                    if (!Regex.IsMatch(strValue, alphaOnly))
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text8") + ",(,),',- </li>\n");
                    }

                    break;

                case 15:         //Invalid postal code in street address
                    if (details.NotNatisProxyIndicator.Equals("P"))
                        column = "PrxStCode";
                    else
                        column = "DrvStCode";
                    strValue = txtErrField.Text;

                    //test that this is only numeric characters and not 0000
                    if (!Regex.IsMatch(strValue, numericOnly))
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text9") + "</li>\n");
                    }
                    else if (strValue.Equals("0000"))
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Tex10") + "</li>\n");
                    }
                    break;

                case 16:         //Invalid street address
                    //need to check all 4 lines
                    for (int i = 0; i < 5; i++)
                    {
                        int col = i;
                        if (i > 0)
                            col = i - 1;

                        if (details.NotNatisProxyIndicator.Equals("P"))
                        {
                            column = "PrxStAddr";
                            if (i > 0)
                                column += col.ToString();
                        }
                        else
                        {
                            column = "DrvStAddr";
                            if (i > 0)
                                column += col.ToString();
                        }
                        switch (i)
                        {
                            case 0:
                                strValue = txtErrField.Text;
                                break;
                            case 1:
                                strValue = txtErrField1.Text;
                                break;
                            case 2:
                                strValue = txtErrField2.Text;
                                break;
                            case 3:
                                strValue = txtErrField3.Text;
                                break;
                        }

                        //test that this is only alphanumeric characters 
                        if (!Regex.IsMatch(strValue, alphaNumeric))
                        {
                            isValid = false;
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Tex11") + ",(,),',-,&,.,/,, </li>\n");
                            break;
                        }

                    }
                    break;

                case 20:         //Invalid postal address
                    for (int i = 0; i < 5; i++)
                    {
                        int col = i;
                        if (i > 0)
                            col = i - 1;

                        if (details.NotNatisProxyIndicator.Equals("P"))
                        {
                            column = "PrxPOAddr";
                            if (i > 0)
                                column += col.ToString();
                        }
                        else
                        {
                            column = "DrvPOAddr";
                            if (i > 0)
                                column += col.ToString();
                        }
                        switch (i)
                        {
                            case 0:
                                strValue = txtErrField.Text;
                                break;
                            case 1:
                                strValue = txtErrField1.Text;
                                break;
                            case 2:
                                strValue = txtErrField2.Text;
                                break;
                            case 3:
                                strValue = txtErrField3.Text;
                                break;
                        }

                        //test that this is only alphanumeric characters 
                        if (!Regex.IsMatch(strValue, alphaNumeric))
                        {
                            isValid = false;
                            sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Tex11") + ",(,),',-,&,.,/,, </li>\n");
                            break;
                        }

                    }
                    break;


                case 34:         //Invalid postal code in postal address                
                    if (details.NotNatisProxyIndicator.Equals("P"))
                        column = "PrxPOCode";
                    else
                        column = "DrvPOCode";
                    strValue = txtErrField.Text;

                    //test that this is only numeric characters and not 0000
                    if (!Regex.IsMatch(strValue, numericOnly))
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Text9") + " </li>\n");
                    }
                    else if (strValue.Equals("0000"))
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("lblError.Tex10") + " </li>\n");
                    }
                    break;
                    //Seawen 2013-08-26 need show image path
                case 75:        //VehImage
                case 76:        //LicImage
                    updateValue = false;
                    string path = this.imageFullPath.Text;
                    if (!File.Exists(path.Substring(5)))
                    {
                        isValid = false;
                        this.imageErrorInfo.Visible = false;
                        this.myImage.Visible = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("findImagError.Text") + " </li>\n");
                    }
                    break;
                    //need update charge
                case 81:
                    table = "Charge";
                    column = "ChgOffenceCode";
                    strValue = txtErrField.Text;

                    if (!checkChargeCode(strValue))
                    {
                        isValid = false;
                        sb.Append("<li>" + (string)GetLocalResourceObject("OffenceCodeInputError.Text") + " </li>\n");
            }
                    break;

            }

            if (!isValid)
            {
                sb.Append("</ul>\n");
                this.lblError.Text = sb.ToString();
                BindDetails(details);
                return;
            }

            if (updateValue)
            {
                int updNotIntNo = notice.UpdateColumnForNoticeCorrection(table, column, strValue, intValue, login, ref errMessage, notIntNo);

                if (updNotIntNo < 1)
                {
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3") + reference + " - " + errMessage;
                    BindGrid(autIntNo, true);
                    return;
                }
            }

            if (details.NotSource == "JMPD")
            {
                eNatisDB db = new eNatisDB(this.connectionString);
                string error = null;
                using (TransactionScope scope = new TransactionScope())
                {
                    if (!db.SendToNTIDatabase(notIntNo, this.login, out error))
                    {
                        lblError.Text = error;
                    }
                    else
                    {
                        scope.Complete();
                    }
                }

                if (string.IsNullOrEmpty(error))
                {
                    //2013-09-13 Added by Nancy for correct pagination start
                    if (grvNotices.Rows.Count == 1)
                    {
                        grvNoticesPager.CurrentPageIndex--;
                    }
                    //2013-09-13 Added by Nancy for correct pagination end
                    BindGrid(autIntNo, true);
                }
            }
            else
            {
                //for all of these update status to FIXED
                int updChgIntNo = UpdateChargeStatus(notIntNo, "fix");

                if (updChgIntNo > 0)
                {
                    //add by linda 2013-01-16 push queue for Frame_Generate1stNotice service
                    QueueItem item = new QueueItem
                    {
                        Body = new NoticeFrameService().GetByNotIntNo(notIntNo)[0].FrameIntNo,
                        Group = new AuthorityService().GetByAutIntNo(details.AutIntNo).AutCode,
                        QueueType = ServiceQueueTypeList.Frame_Generate1stNotice
                    };
                    new QueueItemProcessor().Send(item);

                    lblError.Text = string.Format((string)GetLocalResourceObject("lblError.Text4"), reference);

                }
                //2013-09-13 Added by Nancy for correct pagination start
                if (grvNotices.Rows.Count == 1)
                {
                    grvNoticesPager.CurrentPageIndex--;
                }
                //2013-09-13 Added by Nancy for correct pagination end
                BindGrid(autIntNo, true);
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.NoticeCorrectionCPI, PunchAction.Change); 

                return;
            }
            
            
        }

        protected void btnBatchCorrection_Click(object sender, EventArgs e)
        {
            Response.Redirect("NoticeCorrectionBatch_CPI.aspx");
        }

        protected void btnPostalCodesA_Click(object sender, EventArgs e)
        {
            this.PopulateAreaCodes(this.txtAddress4.Text, "A");
        }

        private void PopulateAreaCodes(string sTown, string sType)
        {
            SuburbDB acdb = new SuburbDB(connectionString); //changed from areadb to suburbdb
            DataSet ds = acdb.GetFilteredSuburbCodes("0", sTown);
            if (sType == "A")
            {
                Dictionary<string, string> lookups =
                    SuburbLookupCache.GetCacheLookupValue(Thread.CurrentThread.CurrentCulture.Name);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string SubIntNo = Convert.ToString(ds.Tables[0].Rows[i]["SubIntNo"].ToString());
                    string areaCode = Convert.ToString(ds.Tables[0].Rows[i]["AreaCode"].ToString());
                    //2013-05-24 Heidi changed
                    if (lookups.ContainsKey(SubIntNo))
                    {
                        ddlPostalCodesA.Items.Add(new ListItem(lookups[SubIntNo], areaCode.ToString()));
                    }
                }
                //ddlPostalCodesA.DataSource = ds;
                //ddlPostalCodesA.DataValueField = "AreaCode";
                //ddlPostalCodesA.DataTextField = "Descr";
                //ddlPostalCodesA.DataBind();
                if (ds.Tables[0].Rows.Count == 1)
                {
                    if (ddlPostalCodesA.Items.Count > 0)
                    {
                        this.txtErrField.Text = ddlPostalCodesA.Items[0].Value;
                    }
                }
            }
            //else
            //{
            //    ddlPostalCodesB.DataSource = ds;
            //    ddlPostalCodesB.DataValueField = "AreaCode";
            //    ddlPostalCodesB.DataTextField = "Descr";
            //    ddlPostalCodesB.DataBind();
            //    if (ds.Tables[0].Rows.Count == 1)
            //        this.txtStreetArea.Text = ddlPostalCodesB.Items[0].Value;
            //}
            ds.Dispose();
        }
        protected void ddlPostalCodesA_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtErrField.Text = ddlPostalCodesA.SelectedValue;
        }

        protected void grvNotices_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            this.notIntNo = Convert.ToInt32(this.grvNotices.DataKeys[e.NewSelectedIndex][0]);

            Session["editNotIntNo"] = notIntNo;

            NoticeDB db = new NoticeDB(this.connectionString);
            NoticeDetails details = db.GetNoticeDetails(notIntNo);
            this.BindDetails(details);

            this.pnlEditNotice.Visible = true;
        }


        //2013-04-09 add by Henry for pagination
        protected void grvNoticesPager_PageChanged(object sender, EventArgs e)
        {
            BindGrid(autIntNo, false);
        }

        private bool checkChargeCode(string code)
        {
            bool flag = false;
            ChargeDB charge = new ChargeDB(connectionString);
            int intCode = 0;
            int.TryParse(code, out intCode);

            if (!string.IsNullOrEmpty(code) && intCode > 0)
            {
                int count = charge.SelectOffenceByCode(code);
                if (count > 0)
                    flag = true;
}

            return flag;
}

        protected void checkImage_Click(object sender, EventArgs e)
        {
            string strValue = this.txtErrField.Text;
            if (File.Exists(strValue))
            {
                this.myImage.ImageUrl = strValue;
                this.myImage.Visible = true;
                this.imageErrorInfo.Visible = false;
            }
            else
            {
                this.imageErrorInfo.Text = (string)GetLocalResourceObject("noImage.Text");
                this.imageErrorInfo.Visible = true;
                this.myImage.Visible = false;
            }
            if (Session["editNotIntNo"] != null)
            {
                int notIntNo = (int)Session["editNotIntNo"];
                NoticeDB db = new NoticeDB(this.connectionString);
                NoticeDetails details = db.GetNoticeDetails(notIntNo);
                this.BindDetails(details);
            }
            this.txtErrField.Text = strValue;
            
        }
        private void showImage(string path)
        {
            this.imageFullPath.Visible = true;
            if (File.Exists(path))
            {
                this.myImage.ImageUrl = path;
                this.myImage.Visible = true;
                this.imageErrorInfo.Visible = false;
            }
            else
            {
                this.imageErrorInfo.Text = (string)GetLocalResourceObject("noImage.Text");
                this.imageErrorInfo.Visible = true;
                this.myImage.Visible = false;
            }
        }
    }
}
