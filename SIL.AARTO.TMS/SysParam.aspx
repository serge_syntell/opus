<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.SysParam" Codebehind="SysParam.aspx.cs" %>


<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="String" value>
    <meta content="<%= keywords %>" name="Keywords">
     <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
 <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body background="<%=backgroundImage %>" style="margin-bottom:0; margin-left:0; margin-top:0; margin-right:0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img style="height: 1px" src="images/1x1.gif" width="167">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                        
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                           <%-- <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="Add "
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>--%>
                           <%-- <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="Delete" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>--%>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center" style="height:21">
                                    </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" style="width:100%" colspan="1">
                    <table border="0" width="568" style="height:482">
                        <tr>
                            <td valign="top" style="height:47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="500px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label></p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:DataGrid ID="dgSysParam" Width="495px" runat="server" BorderColor="Black" AutoGenerateColumns="False"
                                    AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                    FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                    Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="True" OnItemCommand="dgSysParam_ItemCommand"
                                    OnPageIndexChanged="dgSysParam_PageIndexChanged">
                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                    <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                    <ItemStyle CssClass="CartListItem"></ItemStyle>
                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="SPIntNo" HeaderText="SPIntNo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SPColumnName" HeaderText="<%$Resources:dgSysParam.HeaderText %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SPStringValue" HeaderText="<%$Resources:dgSysParam.HeaderText1 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SPIntegerValue" HeaderText="<%$Resources:dgSysParam.HeaderText2 %>"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SPDescr" HeaderText="<%$Resources:dgSysParam.HeaderText3 %>"></asp:BoundColumn>
                                        <asp:ButtonColumn Text="<%$Resources:dgSysParam.Text %>" CommandName="Select"></asp:ButtonColumn>
                                    </Columns>
                                    <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                </asp:DataGrid>
       <%--                         <asp:Panel ID="pnlAddSysParam" runat="server" Height="127px" DESIGNTIMEDRAGDROP="63">
                                    <table id="Table2" style="height:48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" style="height:2">
                                                <asp:Label ID="lblAddSysParam" runat="server" CssClass="ProductListHead" Width="179px">Add new system parameter:</asp:Label></td>
                                            <td width="248" style="height:2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="width:157;height:25">
                                                <asp:Label ID="lblAddSPColumnName" runat="server" CssClass="NormalBold">System parameter:</asp:Label></td>
                                            <td valign="top" style="width:248;height:25">
                                                <asp:TextBox ID="txtAddSPColumnName" runat="server" Width="224px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td style="height:25">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="210px"
                                                    CssClass="NormalRed" ErrorMessage="System parameter code may not be left blank."
                                                    ControlToValidate="txtAddSPColumnName" ForeColor=" " Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td style="height:25;width:157" valign="top">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Width="214px">Description/possible values:</asp:Label></td>
                                            <td style="height:25;width:248" valign="top" >
                                                <asp:TextBox ID="txtAddSPDescr" runat="server" CssClass="NormalMand" Height="24px"
                                                    MaxLength="100" Width="315px" TextMode="MultiLine"></asp:TextBox></td>
                                            <td height="25">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" style="height: 25px">
                                                <p>
                                                    <asp:Label ID="lblAddSPStringValue" runat="server" Width="286px" CssClass="NormalBold"
                                                        Height="4px">String value (blank if not applicable):</asp:Label></p>
                                            </td>
                                            <td width="248" style="height: 25px" valign="top">
                                                <asp:TextBox ID="txtAddSPStringValue" runat="server" Width="54px" CssClass="Normal"
                                                    MaxLength="5"></asp:TextBox></td>
                                            <td style="height: 25px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" width="157">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Width="248px">Integer value (0 if not applicable):</asp:Label></td>
                                            <td height="25" width="248">
                                                <asp:TextBox ID="txtAddSPIntegerValue" runat="server" CssClass="Normal" MaxLength="5"
                                                    Width="54px">0</asp:TextBox>
                                            </td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtAddSPStringValue"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="Integer may not be left blank."
                                                    ForeColor=" " Width="210px"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                                <asp:RangeValidator ID="Rangevalidator2" runat="server" CssClass="NormalRed" MaximumValue="9999"
                                                    MinimumValue="0" Type="Integer" ErrorMessage="RangeValidator" ControlToValidate="txtAddSPIntegerValue"
                                                    ForeColor=" " Width="263px">System parameter must be a numeric value</asp:RangeValidator></td>
                                            <td>
                                                <asp:Button ID="btnAddSysParam" runat="server" CssClass="NormalButton" Text="Add system parameter"
                                                    OnClick="btnAddSysParam_Click"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>--%>
                                <asp:Panel ID="pnlUpdateSysParam" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label19" runat="server" Width="287px" CssClass="ProductListHead" Text="<%$Resources:lblUpdSysParam.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                            </td>
                                            <td height="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" style="height: 26px">
                                                <asp:Label ID="Label18" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSystemparam.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" style="height: 26px">
                                                <asp:TextBox ID="txtSPColumnName" runat="server" Width="227px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td style="height: 26px">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                    CssClass="NormalRed" ErrorMessage="<%$Resources:reqSystemparam.ErrorMsg %>"
                                                    ControlToValidate="txtSPColumnName" ForeColor=" " Display="dynamic"></asp:RequiredFieldValidator></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 26px" valign="top" width="157">
                                                <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Width="214px" Text="<%$Resources:lblDescValue.Text %>"></asp:Label></td>
                                            <td style="height: 26px" valign="top" width="248">
                                                <asp:TextBox ID="txtSPDescr" runat="server" CssClass="NormalMand" Height="24px" MaxLength="100"
                                                    TextMode="MultiLine" Width="315px"></asp:TextBox></td>
                                            <td style="height: 26px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" style="height: 24px">
                                                <p>
                                                    <asp:Label ID="Label17" runat="server" Width="248px" CssClass="NormalBold" Text="<%$Resources:lblStrValue.Text %>"></asp:Label></p>
                                            </td>
                                            <td width="248" style="height: 24px">
                                                <asp:TextBox ID="txtSPStringValue" runat="server" Width="54px" CssClass="Normal"
                                                    MaxLength="5"></asp:TextBox></td>
                                            <td style="height: 24px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157">
                                                <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Width="248px" Text="<%$Resources:lblIntegrValue.Text %>"></asp:Label></td>
                                            <td width="248">
                                                <asp:TextBox ID="txtSPIntegerValue" runat="server" CssClass="Normal" MaxLength="5"
                                                    Width="54px">0</asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="txtSPIntegerValue"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:reqInteger.ErrorMsg %>"
                                                    ForeColor=" " Width="210px"></asp:RequiredFieldValidator></td>
                                        </tr>
                                         <tr>
                                            <td colspan="2">
                                                <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                        <td height="100">
                                                            <asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                        <td height="100">
                                                            <uc1:uclanguagelookup id="ucLanguageLookupUpdate" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>&nbsp;</td>
                                         </tr>
                                        <tr>
                                            <td width="157">
                                            </td>
                                            <td width="248">
                                                <asp:RangeValidator ID="Rangevalidator1" runat="server" CssClass="NormalRed" MaximumValue="9999"
                                                    MinimumValue="0" Type="Integer" ErrorMessage="RangeValidator" ControlToValidate="txtSPIntegerValue"
                                                    ForeColor=" " Height="9px" Width="294px" Text="<%$Resources:reqSysParam.ErrorMsg %>"></asp:RangeValidator></td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
