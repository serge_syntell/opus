using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// A page that allows a supervisor to receive CashBoxes back at the end of the day and print DepositSlips
    /// </summary>
    public partial class Supervisor_DepositSlips : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = string.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "Supervisor_DepositSlips.aspx";
        //protected string thisPage = "Supervisor - End of Day";

        private const string MONEY_FORMAT = "#,##0.00";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            this.autIntNo = int.Parse(this.Session["AutIntNo"].ToString());

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                this.pnlConfirm.Visible = false;
                this.pnlDepositSlips.Visible = false;

                this.GetCashBoxes();
            }
        }

        private void GetCashBoxes()
        {
            CashboxDB db = new CashboxDB(this.connectionString);
            DataSet ds = db.GetSupervisorCashBoxList();

            this.ViewState.Add("CashBoxes", ds);
            this.grdCashBoxes.DataSource = ds;
            this.grdCashBoxes.DataKeyNames = new string[] { "CBIntNo" };
            this.grdCashBoxes.DataBind();

            if (ds.Tables[0].Rows.Count == 0)
                this.ShowDepositSlips();
        }

        private void ShowDepositSlips()
        {
            DepositSlipDB db = new DepositSlipDB(this.connectionString);
            DataSet ds = db.GetSupervisorDepositSlipList();

            this.grdDepositSlips.DataSource = ds;
            this.grdDepositSlips.DataKeyNames = new string[] { "DSIntNo" };
            this.grdDepositSlips.DataBind();

            if (ds.Tables[0].Rows.Count > 0)
                this.pnlDepositSlips.Visible = true;
            else
                this.lblError.Text = (string)GetLocalResourceObject("lblError.Text");
        }

        

        protected void grdCashBoxes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = (DataSet)this.ViewState["CashBoxes"];
            DataRow row = ds.Tables[0].Rows[this.grdCashBoxes.SelectedIndex];

            this.lblFloat.Text = "R " + Convert.ToDecimal(row["CBStartAmount"].ToString()).ToString(MONEY_FORMAT);
            this.lblCheques.Text = "R " + Convert.ToDecimal(row["ReconChequeAmount"].ToString()).ToString(MONEY_FORMAT);
            this.lblCards.Text = "R " + Convert.ToDecimal(row["ReconCardAmount"].ToString()).ToString(MONEY_FORMAT);
            this.lblDebitCards.Text = "R " + Convert.ToDecimal(row["ReconDebitCardAmount"].ToString()).ToString(MONEY_FORMAT);
            this.lblPostalOrders.Text = "R " + Convert.ToDecimal(row["ReconPOAmount"].ToString()).ToString(MONEY_FORMAT);
            this.lblCash.Text = "R " + Convert.ToDecimal(row["CashAmount"].ToString()).ToString(MONEY_FORMAT);

            this.pnlConfirm.Visible = true;
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            int cbIntNo = int.Parse(this.grdCashBoxes.DataKeys[this.grdCashBoxes.SelectedIndex].Value.ToString());

            string errMessage = string.Empty;

            CashboxDB db = new CashboxDB(this.connectionString);
            int updCBIntNo = db.ReceiveCashbox(cbIntNo, ref errMessage);

            if (updCBIntNo < 0)
            {


                switch (updCBIntNo)
                {
                    case -1:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                        break;
                    case -2:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                        break;
                    case -3:
                        lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                        break;
                    default:
                        lblError.Text = errMessage;
                        break;
                }

                lblError.Visible = true;
                return;
            }
            else
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SupervisorEndOfDayBanking, PunchAction.Change);

            }

            this.pnlConfirm.Visible = false;

            this.GetCashBoxes();
        }

        protected void grdDepositSlips_RowEditing(object sender, GridViewEditEventArgs e)
        {
            // Print the deposit slip
            int dsIntNo = (int)this.grdDepositSlips.DataKeys[e.NewEditIndex].Value;

            Helper_Web.BuildPopup(this, "DepositSlipViewer.aspx", "deposit", dsIntNo.ToString());
        }

        protected void btnBalancingReport_Click(object sender, EventArgs e)
        {
            // Print the daily balancing report
            Helper_Web.BuildPopup(this, "ReceiptEODBalancingViewer.aspx", "Date", DateTime.Today.ToString("yyyy-MM-dd"));
        }

        protected void btnLock_Click(object sender, EventArgs e)
        {
            // Lock the deposit slip
            DepositSlipDB db = new DepositSlipDB(this.connectionString);
            db.LockDepositSlips(this.login);

            this.pnlConfirm.Visible = false;
            this.pnlDepositSlips.Visible = false;
            this.pnlDetails.Visible = false;

            this.lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
            
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SupervisorEndOfDayBanking, PunchAction.Change);  
        }

        protected void grdCashBoxes_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            DataSet ds = (DataSet)this.ViewState["CashBoxes"];
            DataRow row = ds.Tables[0].Rows[e.Row.DataItemIndex];

            Label label = (Label)e.Row.FindControl("lblCash");
            if (label != null)
                label.Text = Convert.ToDecimal(row["CashAmount"].ToString()).ToString(MONEY_FORMAT);
            label = (Label)e.Row.FindControl("lblCheques");
            if (label != null)
                label.Text = Convert.ToDecimal(row["ReconChequeAmount"].ToString()).ToString(MONEY_FORMAT);
            label = (Label)e.Row.FindControl("lblCards");
            if (label != null)
                label.Text = Convert.ToDecimal(row["ReconCardAmount"].ToString()).ToString(MONEY_FORMAT);
            label = (Label)e.Row.FindControl("lblDebitCards");
            if (label != null)
                label.Text = Convert.ToDecimal(row["ReconDebitCardAmount"].ToString()).ToString(MONEY_FORMAT);
            label = (Label)e.Row.FindControl("lblPostalOrders");
            if (label != null)
                label.Text = Convert.ToDecimal(row["ReconPOAmount"].ToString()).ToString(MONEY_FORMAT);
            label = (Label)e.Row.FindControl("lblFloat");
            if (label != null)
                label.Text = Convert.ToDecimal(row["CBStartAmount"].ToString()).ToString(MONEY_FORMAT);
        }

        protected void btnDepositSlip_Click(object sender, EventArgs e)
        {
            Response.Redirect("DepositSlipReport.aspx");
        }


}
}
