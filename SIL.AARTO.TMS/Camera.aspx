<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>

<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.Camera" CodeBehind="Camera.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
            <tr>
                <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                    <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" border="0" height="85%">
            <tr>
                <td align="center" valign="top">
                    <img src="images/1x1.gif" width="167" style="height: 1px">
                    <asp:Panel ID="pnlMainMenu" runat="server">
                    </asp:Panel>
                    <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                        BorderColor="#000000">
                        <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$Resources:lblOptions.Text %>"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$Resources:btnOptAdd.Text %>"
                                        OnClick="btnOptAdd_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptDelete.Text %>" OnClick="btnOptDelete_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                        Text="<%$Resources:btnOptHide.Text %>" OnClick="btnOptHide_Click"></asp:Button></td>
                            </tr>
                            <tr>
                                <td align="center"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td valign="top" align="left" width="100%" colspan="1">
                    <table border="0" width="568" height="482">
                        <tr>
                            <td valign="top" height="47">
                                <p align="center">
                                    <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">&nbsp;<asp:DataGrid ID="dgCamera" Width="495px" runat="server" BorderColor="Black"
                                AutoGenerateColumns="False" AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgCamera_ItemCommand">
                                <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                <ItemStyle CssClass="CartListItem"></ItemStyle>
                                <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="CamIntNo" HeaderText="CamIntNo"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CamSerialNo" HeaderText="<%$Resources:dgCamera.HeaderText %>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="MaxSupportingImages" HeaderText="<%$Resources:dgCamera.HeaderText1 %>"></asp:BoundColumn>
                                    <asp:ButtonColumn Text="<%$Resources:dgCameraItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                </Columns>
                                <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                            </asp:DataGrid>
                                <pager:AspNetPager ID="dgCameraPager" runat="server"
                                    ShowCustomInfoSection="Right" Width="495px"
                                    CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%"
                                      FirstPageText="|&amp;lt;"
                                    LastPageText="&amp;gt;|"
                                    CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False"
                                    Font-Size="12px" Height="20px" CustomInfoSectionWidth=""
                                    CustomInfoStyle="float:right;"
                                    OnPageChanged="dgCameraPager_PageChanged"
                                    PageSize="20">
                                </pager:AspNetPager>
                                <asp:Panel ID="pnlAddCamera" runat="server" Height="127px">
                                    <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblAddCamera" runat="server" CssClass="ProductListHead" Text="<%$Resources:lblAddCamera.Text %>"></asp:Label></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddCamSerialNo.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtAddCamSerialNo" runat="server" Width="204px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="18"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="230px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:reqAddCamSerialNo.ErrorMsg %>"
                                                    ControlToValidate="txtAddCamSerialNo" ForeColor=" " ValidationGroup="add"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMaxSupportingImages.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtAddMaxSupportingImages" runat="server" Width="204px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="18"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Width="230px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:reqAddMaxSupportingImages.ErrorMsg1 %>"
                                                    ControlToValidate="txtAddMaxSupportingImages" ForeColor=" " ValidationGroup="add"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="RangeValidator1" runat="server" CssClass="NormalRed" Display="dynamic"
                                                    ErrorMessage="<%$Resources:reqAddMaxSupportingImages.ErrorMsg2 %>" MaximumValue="10" MinimumValue="0"
                                                    ControlToValidate="txtAddMaxSupportingImages" ForeColor=" " ValidationGroup="add" Type="Integer"></asp:RangeValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248"></td>
                                            <td>
                                                <asp:Button ID="btnAddCamera" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddCamera.Text %>" OnClick="btnAddCamera_Click" ValidationGroup="add"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpdateCamera" runat="server" Height="127px">
                                    <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                        <tr>
                                            <td style="height: 1px">
                                                <asp:Label ID="Label3" runat="server" CssClass="ProductListHead" Width="139px" Text="<%$Resources:lblUpdCamera.Text %>"></asp:Label></td>
                                            <td style="height: 1px"></td>
                                            <td style="height: 1px"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="157" height="25">
                                                <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddCamSerialNo.Text %>"></asp:Label></td>
                                            <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtCamSerialNo" runat="server" Width="204px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="18"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Width="230px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:reqAddCamSerialNo.ErrorMsg %>"
                                                    ControlToValidate="txtCamSerialNo" ForeColor=" " ValidationGroup="update"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157" height="2">
                                                <asp:Label ID="Label6" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddMaxSupportingImages.Text %>"></asp:Label></td>
                                            <td width="248" height="2">
                                                <asp:TextBox ID="txtMaxSupportingImages" runat="server" Width="204px" CssClass="NormalMand"
                                                    Height="24px" MaxLength="18"></asp:TextBox></td>
                                            <td height="25">
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="230px"
                                                    CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:reqAddMaxSupportingImages.ErrorMsg1 %>"
                                                    ControlToValidate="txtMaxSupportingImages" ForeColor=" " ValidationGroup="update"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="RangeValidator2" runat="server" CssClass="NormalRed" Display="dynamic"
                                                    ErrorMessage="<%$Resources:reqAddMaxSupportingImages.ErrorMsg2 %>" MaximumValue="10" MinimumValue="0"
                                                    ControlToValidate="txtMaxSupportingImages" ForeColor=" " ValidationGroup="update" Type="Integer"></asp:RangeValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="157"></td>
                                            <td width="248"></td>
                                            <td>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                    OnClick="btnUpdate_Click" ValidationGroup="update"></asp:Button></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
            <tr>
                <td class="SubContentHeadSmall" valign="top" width="100%"></td>
            </tr>
        </table>
    </form>
</body>
</html>
