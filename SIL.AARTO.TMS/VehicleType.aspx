<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="~/DynamicData/FieldTemplates/UCLanguageLookup.ascx" TagName="UCLanguageLookup" TagPrefix="uc1" %>


<%@ Page Language="c#" AutoEventWireup="false" Inherits="Stalberg.TMS.VehicleType" Codebehind="VehicleType.aspx.cs" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%= title %>
    </title>
    <link href="<%= styleSheet %>" type="text/css" rel="stylesheet">
    <meta content="<%= description %>" name="Description">
    <meta content="<%= keywords %>" name="Keywords">
    <script src="Scripts/Jquery/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="Scripts/MultiLanguage.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" background="<%=backgroundImage %>" topmargin="0"
    rightmargin="0">
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="10%">
        <tr>
            <td class="HomeHead" align="center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" height="85%">
        <tr>
            <td align="center" valign="top">
                <img style="height: 1px" src="images/1x1.gif" width="167">
                <asp:Panel ID="pnlMainMenu" runat="server">
                    
                </asp:Panel>
                <asp:Panel ID="pnlSubMenu" runat="server" Width="126px" BorderWidth="1px" BorderStyle="Solid"
                    BorderColor="#000000">
                    <table id="tblMenu" cellspacing="1" cellpadding="1" border="0" runat="server">
                        <tr>
                            <td align="center" style="width: 139px">
                                <asp:Label ID="Label1" runat="server" Width="118px" CssClass="ProductListHead" Text="<%$ Resources:lblOptions.Text %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 139px">
                                <asp:Button ID="btnOptAdd" runat="server" Width="135px" CssClass="NormalButton" Text="<%$ Resources:btnOptAdd.Text %> "
                                    OnClick="btnOptAdd_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 139px">
                                <asp:Button ID="btnOptDelete" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$ Resources:btnOptDelete.Text%>" OnClick="btnOptDelete_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width: 139px">
                                <asp:Button ID="btnOptHide" runat="server" Width="135px" CssClass="NormalButton"
                                    Text="<%$ Resources:btnOptHide.Text%>" OnClick="btnOptHide_Click"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" height="21" style="width: 139px">
                                
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top" align="left" colspan="1" style="width: 100%">
                <table border="0" width="568" height="482">
                    <tr>
                        <td valign="top" height="47">
                            <p align="center">
                                <asp:Label ID="lblPageName" runat="server" Width="379px" CssClass="ContentHead" Text="<%$ Resources:lblPageName.Text %>"></asp:Label></p>
                            <p>
                                <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label></p>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table id="Table1" height="30" cellspacing="1" cellpadding="1" width="542" border="0">
                                        <tr>
                                            <td width="162">
                                            </td>
                                            <td width="7">
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelVTGroup" runat="server" Width="187px" CssClass="NormalBold" Text="<%$ Resources:lblSelVTGroup.Text %>"></asp:Label>
                                            </td>
                                            <td width="7">
                                                <asp:DropDownList ID="ddlSelVTGroup" runat="server" Width="234px" CssClass="Normal"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlSelVTGroup_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td width="7">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="162">
                                                <asp:Label ID="lblSelection" runat="server" Width="229px" CssClass="NormalBold" Text="<%$ Resources:lblSelection.Text %>"></asp:Label>
                                            </td>
                                            <td width="7">
                                                <asp:TextBox ID="txtSearch" runat="server" Width="107px" CssClass="Normal" MaxLength="10"></asp:TextBox>
                                            </td>
                                            <td width="7">
                                                <asp:Button ID="btnSearch" runat="server" Width="80px" CssClass="NormalButton" Text="<%$ Resources:btnSearch.Text %>"
                                                    OnClick="btnSearch_Click"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:DataGrid ID="dgVehicleType" Width="495px" runat="server" BorderColor="Black"
                                        AutoGenerateColumns="False" 
                                        AlternatingItemStyle-CssClass="CartListItemAlt" ItemStyle-CssClass="CartListItem"
                                        FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead" ShowFooter="True"
                                        Font-Size="8pt" CellPadding="4" GridLines="Vertical" AllowPaging="False" OnItemCommand="dgVehicleType_ItemCommand"
                                        onselectedindexchanged="dgVehicleType_SelectedIndexChanged">
                                        <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                        <AlternatingItemStyle CssClass="CartListItemAlt"></AlternatingItemStyle>
                                        <ItemStyle CssClass="CartListItem"></ItemStyle>
                                        <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="VTIntNo" HeaderText="VTIntNo"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="VTCode" HeaderText="<%$Resources:dgVehicleType.HeaderText1 %>"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="VTDescr" HeaderText="<%$Resources:dgVehicleType.HeaderText2 %>"></asp:BoundColumn>
                                            <asp:ButtonColumn Text="<%$Resources:dgVehicleTypeItem.Text %>" CommandName="Select"></asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="Medium" Mode="NumericPages" PageButtonCount="20" />
                                    </asp:DataGrid>
                                    <pager:AspNetPager id="dgVehicleTypePager" runat="server" 
                                            showcustominfosection="Right" width="495px" 
                                            CustomInfoHTML="Total Pages %PageCount%, Items %RecordCount%" 
                                            FirstPageText="|&amp;lt;" 
                                            LastPageText="&amp;gt;|" 
                                            CurrentPageButtonStyle="color:#000;" ShowDisabledButtons="False" 
                                            Font-Size="12px" Height="20px" CustomInfoSectionWidth="" 
                                            CustomInfoStyle="float:right;"
                                        onpagechanged="dgVehicleTypePager_PageChanged"  UpdatePanelId="UpdatePanel1"></pager:AspNetPager>

                                    &nbsp;&nbsp;
                                    <asp:Panel ID="pnlAddVehicleType" runat="server" Height="127px">
                                        <table id="Table2" height="48" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td width="157" style="height: 2px">
                                                    <asp:Label ID="lblAddVehicleType" runat="server" CssClass="ProductListHead" Text="<%$ Resources:lblAddVehicleType.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" style="height: 2px">
                                                </td>
                                                <td style="height: 2px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" height="2">
                                                    <asp:Label ID="Label5" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblAddVTGroup.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2">
                                                    <asp:DropDownList ID="ddlAddVTGroup" runat="server" Width="217px" CssClass="Normal">
                                                    </asp:DropDownList>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" height="2" valign="top">
                                                    <asp:Label ID="lblAddVTCode" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddVTCode.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2" valign="top">
                                                    <asp:TextBox ID="txtAddVTCode" runat="server" Width="83px" CssClass="NormalMand" 
                                                        Height="24px" MaxLength="5"></asp:TextBox>
                                                </td>
                                                <td height="2" valign="top">
                                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Width="210px"
                                                        CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                        ControlToValidate="txtAddVTCode" ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" height="2">
                                                    <asp:Label ID="Label2" runat="server" CssClass="NormalBold" Text="<%$Resources:lblVTDescr.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2">
                                                   <asp:TextBox ID="txtAddVTDescr" runat="server" Width="100%" CssClass="NormalMand"
                                                        Height="24px" MaxLength="30"></asp:TextBox>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"> <asp:Label ID="lblTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"></asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupAdd" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="2" width="157">
                                                    <asp:Label ID="Label15" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddNatisDescr.Text %>"></asp:Label>
                                                </td>
                                                <td height="2" width="248">
                                                    <asp:TextBox ID="textAddNatisDescr" runat="server" Width="100%"></asp:TextBox>
                                                </td>
                                                <td height="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="2" width="157">
                                                    <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Text="<%$Resources:lblNatis.Text %>"></asp:Label>
                                                </td>
                                                <td height="2" width="248">
                                                    <asp:TextBox ID="txtAddNatis" runat="server" CssClass="NormalMand" Height="24px"
                                                        MaxLength="3" Width="83px"></asp:TextBox>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="2" width="157">
                                                    <asp:Label ID="Label8" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddTCS.Text %>"></asp:Label>
                                                </td>
                                                <td height="2" width="248">
                                                    <asp:TextBox ID="txtAddTCS" runat="server" CssClass="NormalMand" Height="24px" MaxLength="3"
                                                        Width="83px"></asp:TextBox>
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 2px" width="157">
                                                    <asp:Label ID="Label9" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCivitas.Text %>"></asp:Label>
                                                </td>
                                                <td style="height: 2px" width="248">
                                                    <asp:TextBox ID="txtAddCivitas" runat="server" CssClass="NormalMand" Height="24px"
                                                        MaxLength="3" Width="83px"></asp:TextBox>
                                                </td>
                                                <td style="height: 2px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 2px" width="157">
                                                </td>
                                                <td style="height: 2px" width="248">
                                                    <asp:CheckBox ID="chkAddVTArchiveExclude" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddVTArchiveExclude.Text %>"
                                                        Width="263px" />
                                                </td>
                                                <td style="height: 2px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                </td>
                                                <td width="248">
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddVehicleType" runat="server" CssClass="NormalButton" Text="<%$Resources:btnAddVehicleType.Text %>"
                                                        OnClick="btnAddVehicleType_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlUpdateVehicleType" runat="server" Height="127px">
                                        <table id="Table3" height="118" cellspacing="1" cellpadding="1" width="654" border="0">
                                            <tr>
                                                <td width="157" height="2">
                                                    <asp:Label ID="Label19" runat="server" Width="194px" CssClass="ProductListHead" Text="<%$Resources:lblUpdate.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2">
                                                </td>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" style="height: 2px">
                                                    <asp:Label ID="Label6" runat="server" Width="187px" CssClass="NormalBold" Text="<%$Resources:lblVTGroup.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" style="height: 2px">
                                                    <asp:DropDownList ID="ddlVTGroup" runat="server" Width="217px" CssClass="Normal">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="height: 2px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" height="2" valign="top">
                                                    <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddVTCode.Text %>"></asp:Label>
                                                </td>
                                                <td width="248" height="2" valign="top">
                                                    <asp:TextBox ID="txtVTCode" runat="server" Width="83px" CssClass="NormalMand" Height="24px"
                                                        MaxLength="5"></asp:TextBox>
                                                </td>
                                                <td height="2" valign="top">
                                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Width="210px"
                                                        CssClass="NormalRed" Display="dynamic" ErrorMessage="<%$Resources:ReqCode.ErrorMessage %>"
                                                        ControlToValidate="txtVTCode" ForeColor=" "></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="157" height="25">
                                                    <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblVTDescr.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="248" height="25">
                                                <asp:TextBox ID="txtVTDescr" runat="server" Width="100%" CssClass="NormalMand" Height="24px"
                                                        MaxLength="30"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                            <td colspan="2">
                                                    <table cellspacing="1" cellpadding="0" border="0" width="615" align="center" bgcolor="#000000">
                                                    <tr bgcolor='#FFFFFF'>
                                                    <td height="100"><asp:Label ID="lblUpdTranslation" runat="server" CssClass="NormalBold" Text="<%$Resources:lblTranslation.Text %>" Width="265"> </asp:Label></td>
                                                    <td height="100"><uc1:UCLanguageLookup ID="ucLanguageLookupUpdate" runat="server" /></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="2" width="157">
                                                    <asp:Label ID="Label14" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddNatisDescr.Text %>"></asp:Label>
                                                </td>
                                                <td height="2" width="248">
                                                    <asp:TextBox ID="textUpdNatisDescr" runat="server" Width="100%"></asp:TextBox>
                                                </td>
                                                <td height="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="25" valign="top" width="157">
                                                    <asp:Label ID="Label10" runat="server" CssClass="NormalBold" Text="<%$Resources:lblNatis.Text %>"></asp:Label>
                                                </td>
                                                <td height="25" valign="top" width="248">
                                                    <asp:TextBox ID="txtNatis" runat="server" CssClass="NormalMand" Height="24px" MaxLength="3"
                                                        Width="83px"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="25" valign="top" width="157">
                                                    <asp:Label ID="Label11" runat="server" CssClass="NormalBold" Text="<%$Resources:lblAddTCS.Text %>"></asp:Label>
                                                </td>
                                                <td height="25" valign="top" width="248">
                                                    <asp:TextBox ID="txtTCS" runat="server" CssClass="NormalMand" Height="24px" MaxLength="3"
                                                        Width="83px"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="157" height="25">
                                                    <asp:Label ID="Label12" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCivitas.Text %>"></asp:Label>
                                                </td>
                                                <td valign="top" width="248" height="25">
                                                    <asp:TextBox ID="txtCivitas" runat="server" CssClass="NormalMand" Height="24px" MaxLength="3"
                                                        Width="83px"></asp:TextBox>
                                                </td>
                                                <td height="25">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                </td>
                                                <td width="248">
                                                    <asp:CheckBox ID="chkVTArchiveExclude" runat="server" CssClass="NormalBold" Text="<%$Resources:chkAddVTArchiveExclude.Text %>"
                                                        Width="263px" />
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157">
                                                </td>
                                                <td width="248">
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="157" style="height: 86px">
                                                    <asp:Panel ID="pnlVTLookups" runat="server" Height="50px" Width="125px">
                                                        <asp:Label ID="Label13" runat="server" CssClass="ProductListHead" Width="192px" Text="<%$Resources:lbllVTLookups.Text %>"></asp:Label>
                                                        <asp:CheckBoxList ID="cblVTLookups" runat="server" CssClass="NormalBold" Width="300px">
                                                        </asp:CheckBoxList>
                                                    </asp:Panel>
                                                </td>
                                                <td width="248" style="height: 86px">
                                                </td>
                                                <td valign="top" style="height: 86px">
                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="NormalButton" Text="<%$Resources:btnUpdate.Text %>"
                                                        OnClick="btnUpdate_Click" OnClientClick="return VerifytLookupRequired()"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="SubContentHeadSmall" valign="top" width="100%">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
