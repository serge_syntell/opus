using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// The Notice Post management page
    /// </summary>
    public partial class SystemOverViewReport : System.Web.UI.Page
    {
        // Fields
        private string connectionString = String.Empty;
        private string login;
        private int autIntNo = 0;
        private int userIntNo = 0;
        private bool bUpload = false;
        //private bool isCivitas = false;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "SystemOverViewReport.aspx";
        //protected string thisPage = "OPUS - System Overview Report";

        // Constants
        private const int FIRST_NOTICE_STATUS = 250;
        private const int FIRST_NOTICE_CIPRUS_STATUS = 220;
        private const int SECOND_NOTICE_STATUS = 260;
        private const string DATE_FORMAT = "yyyy-MM-dd";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Retrieve the database connection string
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            else
                this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.login = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            GetData();

            if (!Page.IsPostBack)
            {
                //dls 2010-07-08 need to check that DateRules exist:
                DateRulesDetails dr = new DateRulesDetails();
                dr.AutIntNo = this.autIntNo;
                dr.DtRStartDate = "NotOffenceDate";
                dr.DtREndDate = "NotPosted1stNoticeDate";
                dr.LastUser = this.login;

                DefaultDateRules defRules = new DefaultDateRules(dr, this.connectionString);

                int noOfDays = defRules.SetDefaultDateRule();

                // used to do checks and balances
                //SystemOverviewDB db = new SystemOverviewDB(this.connectionString);
                //lblError.Text = db.GetNatisFileQty();
                //lblError.Visible = true;
                
            }
        }

       
        

        protected void grdHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            int nRow = grdHeader.SelectedIndex;
            GetDrillDownData(nRow, 0);
            pnlInfo.Visible = true;
            this.Label3.Text = "";
            
        }

        protected void grdHeader_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        private void GetData()
        {
            try
            {
                SystemOverviewDB db = new SystemOverviewDB(this.connectionString);
                SqlDataReader dr = db.GetData();
                this.grdHeader.DataSource = dr;
                this.grdHeader.DataBind();

                //Jerry 2014-10-30 add
                dr.Close();
                dr.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error : " + e.Message);
            }
        }

        private void GetDrillDownData(int nRow, int nPage)
        {
            SystemOverviewDB db = new SystemOverviewDB(this.connectionString);
            int[] normalIndex=new int[]{0,1,2,3,4,5,6};
            int[] detailIndex=new int[]{7,8};
            int ARDays = 0;
            int windowSize = 30;
            DateRuleInfo SummonsDetailDateRule;
            DateRuleInfo dateRuleInfo = new DateRuleInfo();
            //NoticeDetailDateRule = dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssue2ndNoticeDate").ADRNoOfDays;
            //SummonsDetailDateRule = dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted2ndNoticeDate", "NotIssueSummonsDate");

            DataSet ds = null;
            bUpload = false;

            switch (nRow)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    ARDays = dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotPosted1stNoticeDate").ADRNoOfDays;
                    break;
                case 7:
                    ARDays = dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssue2ndNoticeDate").ADRNoOfDays;
                    break;
                case 8:
                    SummonsDetailDateRule = dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted2ndNoticeDate", "NotIssueSummonsDate");
                    ARDays = (SummonsDetailDateRule != null ? SummonsDetailDateRule.ADRNoOfDays : 0);
                    break;
            }
            if (nRow == 0)
            {
                ds = db.GetCDLabelDetails(ARDays,windowSize);
            }
            if (nRow == 1)
            {
                ds = db.GetNatisFileDetails(ARDays,windowSize);
            }
            if (nRow == 2)
            {
                ds = db.GetVerificationDetails(ARDays, windowSize);
            }
            if (nRow == 3)
            {
                ds = db.GetAdjudicationDetails(ARDays, windowSize);
            }
            if (nRow == 4)
            {
                bUpload = true;
                ds = db.GetCiprusUploadDetails(ARDays, windowSize);
                
            }
            if (nRow == 5)
            {
                ds = db.GetCorrectionDetails(ARDays, windowSize);
            }
            if (nRow == 6)
            {
                ds = db.Get1stNoticeDetails(ARDays,windowSize);
            }
            if (nRow == 7)
            {
                ds = db.Get2ndNoticeDetails(ARDays);
            }
            if (nRow == 8)
            {
               ds = db.GetSummonsDetails(ARDays);
            }
            if (bUpload)
            {
                this.grdDrillDownUpload.DataSource = ds;
                this.grdDrillDownUpload.PageIndex = nPage;
                this.grdDrillDownUpload.DataBind();
                this.grdDrillDown.Visible = false;
                this.grdDrillDownUpload.Visible = true; 
            }
            else
            {
                this.grdDrillDown.DataSource = ds;
                this.grdDrillDown.PageIndex = nPage;
                this.grdDrillDown.DataBind();
                this.grdDrillDownUpload.Visible = false;
                this.grdDrillDown.Visible = true;
            }
            lblDrillDownHeader.Text = string.Format((string)GetLocalResourceObject("lblDrillDownHeader.Text"), grdHeader.SelectedRow.Cells[0].Text);
        }
        

        protected void grdHeader_RowCreated(object sender, GridViewRowEventArgs e)
        {
            int nValue = -1;
            System.Data.Common.DbDataRecord s = (System.Data.Common.DbDataRecord)e.Row.DataItem;

            if (s != null)
            {
                if (s["NoDaysLeft"] != DBNull.Value)
                    nValue = Convert.ToInt32(s["NoDaysLeft"].ToString());
                if (nValue < 0)
                    e.Row.Cells[2].ControlStyle.BackColor = System.Drawing.Color.LightSteelBlue ;
                if (nValue < 6 && nValue >= 0)
                    e.Row.Cells[2].ControlStyle.BackColor = System.Drawing.Color.Red;
                if (nValue < 15 && nValue >= 6)
                    e.Row.Cells[2].ControlStyle.BackColor = System.Drawing.Color.Orange;
                if (nValue >= 15)
                    e.Row.Cells[2].ControlStyle.BackColor = System.Drawing.Color.Green;
                if (s["Quantity"].ToString() == "0" || s["Quantity"] == DBNull.Value  )
                {
                    e.Row.Cells[5].Enabled = false;
                    e.Row.Cells[2].ControlStyle.BackColor = System.Drawing.Color.Gray; 
                }
                if (s["OldestDate"] != DBNull.Value   && e.Row.RowIndex > -1)
                    e.Row.Cells[3].Text = ((DateTime)s["OldestDate"]).ToString(DATE_FORMAT);
            }
        }

        protected void grdDrillDown_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void grdDrillDown_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int nRow = grdHeader.SelectedIndex;
            GetDrillDownData(nRow, e.NewPageIndex);
        }

        protected void grdDrillDown_RowCreated(object sender, GridViewRowEventArgs e)
        {
            int nValue = 0;
            DataRowView s = (DataRowView)e.Row.DataItem;

            if (s != null)
            {
                if (s["NoDaysLeft"] != DBNull.Value)
                    nValue = Convert.ToInt32(s["NoDaysLeft"].ToString());
                if (nValue < 0)
                    e.Row.Cells[3].ControlStyle.BackColor = System.Drawing.Color.LightSteelBlue;
                if (nValue < 6 && nValue >= 0)
                    e.Row.Cells[3].ControlStyle.BackColor = System.Drawing.Color.Red;
                if (nValue < 15 && nValue >= 6)
                    e.Row.Cells[3].ControlStyle.BackColor = System.Drawing.Color.Orange;
                if (nValue >= 15)
                    e.Row.Cells[3].ControlStyle.BackColor = System.Drawing.Color.Green;

                if (s["FirstOffenceDate"] != DBNull.Value )
                    e.Row.Cells[4].Text = ((DateTime)s["FirstOffenceDate"]).ToString(DATE_FORMAT);
            }
           
        }

        protected void grdDrillDownUpload_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void grdDrillDownUpload_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int nRow = grdHeader.SelectedIndex;
            GetDrillDownData(nRow, e.NewPageIndex);
        }

        protected void grdDrillDownUpload_RowCreated(object sender, GridViewRowEventArgs e)
        {
            int nValue = 0;
            DataRowView s = (DataRowView)e.Row.DataItem;

            if (s != null)
            {
                if (s["NoDaysLeft"] != DBNull.Value)
                    nValue = Convert.ToInt32(s["NoDaysLeft"].ToString());
                if (nValue < 0)
                    e.Row.Cells[3].ControlStyle.BackColor = System.Drawing.Color.LightSteelBlue;
                if (nValue < 6 && nValue >= 0)
                    e.Row.Cells[3].ControlStyle.BackColor = System.Drawing.Color.Red;
                if (nValue < 15 && nValue >= 6)
                    e.Row.Cells[3].ControlStyle.BackColor = System.Drawing.Color.Orange;
                if (nValue >= 15)
                    e.Row.Cells[3].ControlStyle.BackColor = System.Drawing.Color.Green;

                if (s["FirstOffenceDate"] != DBNull.Value)
                    e.Row.Cells[4].Text = ((DateTime)s["FirstOffenceDate"]).ToString(DATE_FORMAT);

                if (s["CUBResponseDate"] != DBNull.Value)
                    e.Row.Cells[5].Text = "YES";
                else
                    e.Row.Cells[5].Text = "NO";

                if (s["CUBAllocDate"] != DBNull.Value)
                    e.Row.Cells[6].Text = "YES";
                else
                    e.Row.Cells[6].Text = "NO";
            }

        }

        protected void reclaculate_Click(object sender, EventArgs e)
        {
            try
            {
                int autIntNo = Convert.ToInt32(Session["autIntNo"]);
                int windowSize = 30;
                int ARDays, NoticeDetailDateRule,summonsDateRuleInt;
                DateRuleInfo SummonsDetailDateRule;
                DateRuleInfo dateRuleInfo = new DateRuleInfo();
                SystemOverviewDB db = new SystemOverviewDB(this.connectionString);
                ARDays = dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotOffenceDate", "NotPosted1stNoticeDate").ADRNoOfDays;
                NoticeDetailDateRule = dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted1stNoticeDate", "NotIssue2ndNoticeDate").ADRNoOfDays;
                SummonsDetailDateRule = dateRuleInfo.GetDateRuleInfoByDRNameAutIntNo(autIntNo, "NotPosted2ndNoticeDate", "NotIssueSummonsDate");
                summonsDateRuleInt = (SummonsDetailDateRule != null ? SummonsDetailDateRule.ADRNoOfDays : 0);

                db.SystemOverviewCreate(windowSize, ARDays, NoticeDetailDateRule, summonsDateRuleInt);
                //
                GetData();
                this.Label3.Text = "Success";
                
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.login, PunchStatisticsTranTypeList.SystemOverviewReport, PunchAction.Add);  

            }
            catch (Exception ex)
            {
                this.Label3.Text = ex.Message;
            }
        }
    }
}
