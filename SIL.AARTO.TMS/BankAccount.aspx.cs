using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS 
{

	public partial class BankAccount : System.Web.UI.Page 
	{
        protected string connectionString = string.Empty;
		protected string styleSheet;
		protected string backgroundImage;
		protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
		protected string keywords = string.Empty;
		protected string title = string.Empty;
		protected string description = string.Empty;
        protected string thisPageURL = "BankAccount.aspx";
        //protected string thisPage = "Bank account maintenance";

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

		protected void Page_Load(object sender, System.EventArgs e) 
		{
            connectionString = Application["constr"].ToString();

			//get user info from session variable
			if (Session["userDetails"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			if (Session["userIntNo"]==null)
				Server.Transfer("Login.aspx?Login=invalid");

			//get user details
			TMS.UserDB user = new UserDB(connectionString);
			TMS.UserDetails userDetails = new UserDetails();

			userDetails = (UserDetails)Session["userDetails"];
	
			loginUser = userDetails.UserLoginName;
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

			int userAccessLevel = userDetails.UserAccessLevel;

			//may need to check user access level here....
			//			if (userAccessLevel<7)
			//				Server.Transfer(Session["prevPage"].ToString());


			//set domain specific variables
			General gen = new General();

			backgroundImage = gen.SetBackground(Session["drBackground"]);
			styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

			if (!Page.IsPostBack)
			{
                //2012-3-2 Linda Modified into a multi- language
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
				pnlAddAccount.Visible = false;
				pnlUpdateAccount.Visible = false;
				btnOptDelete.Visible = false;

				//PopulateAuthorites(autIntNo);

				BindGrid();

			}		
		}

        //// Modefied By Jake 2010-04-15
        //// Desc:Removed UserGroup_Auth Table,All pages will display all authorites from Authoriry table
        //protected void PopulateAuthorites( int autIntNo)
        //{
        //    int mtrIntNo = 0;

        //    Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

        //    DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");

        //    //UserGroup_AuthDB authorities = new UserGroup_AuthDB(connectionString);

        //    //SqlDataReader reader = authorities.GetUserGroup_AuthListByUserGroup(ugIntNo, 0);
        //    ddlSelectLA.DataSource = data;
        //    ddlSelectLA.DataValueField = "AutIntNo";
        //    ddlSelectLA.DataTextField = "AutName";
        //    ddlSelectLA.DataBind();
        //    ddlSelectLA.SelectedIndex = ddlSelectLA.Items.IndexOf(ddlSelectLA.Items.FindByValue(autIntNo.ToString()));

        //    //reader.Close();

        //}

		protected void BindGrid()
		{

			TMS.BankAccountDB BankAccountList = new TMS.BankAccountDB(connectionString);

			DataSet data = BankAccountList.GetBankAccountListDS();
			dgAccount.DataSource = data;
			dgAccount.DataKeyField = "BAIntNo";
			dgAccount.DataBind();

			if (dgAccount.Items.Count == 0)
			{
				dgAccount.Visible = false;
				lblError.Visible = true;
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text"); 
			}
			else
			{
				dgAccount.Visible = true;
			}

			data.Dispose();
			pnlAddAccount.Visible = false;
			pnlUpdateAccount.Visible = false;
		}

		protected void btnOptAdd_Click(object sender, System.EventArgs e)
		{
			// allow transactions to be added to the database table
			pnlAddAccount.Visible = true;
			pnlUpdateAccount.Visible = false;
			btnOptDelete.Visible = false;

			//			txtAddVATStatus.Text = "Z";

		}

		protected void ShowBankAccountDetails(int editBAIntNo)
		{
			// Obtain and bind a list of all users
			TMS.BankAccountDB acc = new TMS.BankAccountDB(connectionString);
			
			TMS.BankAccountDetails accDetails = acc.GetBankAccountDetails(editBAIntNo);

			txtBAAccountNo.Text = accDetails.BAAccountNo;
			txtBAAccountName.Text = accDetails.BAAccountName;
			txtBAName.Text = accDetails.BAName;
			txtBABranchName.Text = accDetails.BABranchName;
			txtBABranchNo.Text = accDetails.BABranchNo;
					
			pnlUpdateAccount.Visible = true;
			pnlAddAccount.Visible  = false;
			
			btnOptDelete.Visible = true;
		}


		protected void btnAddAccount_Click(object sender, System.EventArgs e)
		{
			// add the new transaction to the database table
			TMS.BankAccountDB BankAccountAdd = new BankAccountDB(connectionString);
			
			int addBAIntNo = BankAccountAdd.AddBankAccount	(txtAddBAAccountNo.Text, txtAddBAAccountName.Text, txtAddBAName.Text, txtAddBABranchName.Text, txtAddBABranchNo.Text, loginUser);

			if (addBAIntNo <= 0)
			{
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1"); 
			}
			else
			{
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.BankAccountMaintenance, PunchAction.Add);  

			}

			lblError.Visible = true;

			BindGrid();

		}

		protected void btnUpdateAccount_Click(object sender, System.EventArgs e)
		{
			// add the new transaction to the database table
			TMS.BankAccountDB accUpdate = new BankAccountDB(connectionString);
			
			//int autIntNo = Convert.ToInt32(ddlSelectLA.SelectedValue);
			int baIntNo = Convert.ToInt32(Session["editBAIntNo"]);

			int updBAIntNo = accUpdate.UpdateBankAccount(baIntNo, txtBAAccountNo.Text, txtBAAccountName.Text, txtBAName.Text, txtBABranchName.Text, txtBABranchNo.Text, loginUser);

			if (updBAIntNo <= 0)
			{
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text3"); 
			}
			else
			{
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
               
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.BankAccountMaintenance, PunchAction.Change);  
			}

			lblError.Visible = true;

			BindGrid();

		}

		protected void dgAccount_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//store details of page in case of re-direct
			dgAccount.SelectedIndex = e.Item.ItemIndex;

			if (dgAccount.SelectedIndex > -1) 
			{			
				int editBAIntNo = Convert.ToInt32(dgAccount.DataKeys[dgAccount.SelectedIndex]);

				Session["editBAIntNo"] = editBAIntNo;
				Session["prevPage"] = thisPageURL;

				ShowBankAccountDetails(editBAIntNo);
			}

		}

        protected void btnOptDelete_Click(object sender, EventArgs e)
        {
            int baIntNo = Convert.ToInt32(Session["editBAIntNo"]);

            BankAccountDB bankAcc = new Stalberg.TMS.BankAccountDB(connectionString);

            string errMessage = string.Empty;

            int delBAIntNo = bankAcc.DeleteBankAccount(baIntNo, ref errMessage);

            //2012-3-2 Linda Modified into a multi- language
            lblError.Text = (string)GetLocalResourceObject("lblError.Text5"); 

            if (delBAIntNo == -1)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text += (string)GetLocalResourceObject("lblError.Text6"); 
            }
            else if (delBAIntNo == -2)
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text += (string)GetLocalResourceObject("lblError.Text7"); 
            }
            else if (delBAIntNo <= 0)
            {
                lblError.Text += errMessage;
            }
            else
            {
                //2012-3-2 Linda Modified into a multi- language
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.BankAccountMaintenance, PunchAction.Delete);  
            }

            lblError.Visible = true;

            BindGrid();
        }
}
}
