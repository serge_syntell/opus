using System;
using System.Data;
using System.Collections.Specialized;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    public partial class Representation_ReportViewer : System.Web.UI.Page
    {
        // Fields
        protected string connectionString = string.Empty;
        protected SqlDataAdapter sqlDAReceiptNote;
        protected SqlConnection cn;
        protected SqlParameter paramRctIntNo;
        protected ReportDocument _reportDoc = new ReportDocument();
        protected SqlCommand sqlSelectCommand1;
        protected dsRepresentation dsRepresentation;
        protected int autIntNo = 0;

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Representation Report Viewer";
        protected string thisPageURL = "Representation_ReportViewer.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            NameValueCollection qs = Request.QueryString;
            if (qs["ChgIntNo"] == null)
            {
                Response.Redirect("Login.aspx");
                return;
            }

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "Representation");
            if (reportPage.Equals(string.Empty))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "Representation.rpt", "Representation", "system", "");
                reportPage = "Representation.rpt";
            }

            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "Representation");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {

                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error2"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            _reportDoc.Load(reportPath);

            // Fill the DataSet
            //dls 070803 - changed this to use Charge PK, as we can have multiple reps per notice
            //int repIntNo = int.Parse(qs["NotIntNo"].ToString());
            int chgIntNo = int.Parse(qs["ChgIntNo"].ToString());
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("RepresentationReport");
            da.SelectCommand.Connection = new SqlConnection(Application["constr"].ToString());
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@ChgIntNo", SqlDbType.Int, 4).Value = chgIntNo;
            this.dsRepresentation = new dsRepresentation();
            this.dsRepresentation.DataSetName = "dsRepresentation";
            da.Fill(this.dsRepresentation);
            da.Dispose();

            // Populate the report
            _reportDoc.SetDataSource(this.dsRepresentation.Tables[1]);
            this.dsRepresentation.Dispose();

            //export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            _reportDoc.Dispose();

            // Stuff the PDF file into rendering stream first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();

        }

    }
}