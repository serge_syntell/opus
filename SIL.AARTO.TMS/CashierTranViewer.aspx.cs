using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Collections.Generic;
using Stalberg.TMS.Data.Datasets;


namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the end of day cash box balancing page
    /// </summary>
    public partial class CashierTranViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private ReportDocument _reportDoc = new ReportDocument();
        private dsCashierTran ds;
        private int autIntNo = 0;
        private const string DATE_FORMAT = "yyyy-MM-dd";

        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Cashier Transaction Report Viewer";
        protected string thisPageURL = "CashierTranViewer.aspx";

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            if (Request.QueryString["StartDate"] == null || Request.QueryString["EndDate"] == null)
            {
                string error = (string)GetLocalResourceObject("error");
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);
                Response.Redirect(errorURL);
                return;
            }

            DateTime dtStart = DateTime.Parse(Request.QueryString["StartDate"].ToString());
            DateTime dtEnd = DateTime.Parse(Request.QueryString["EndDate"].ToString());

            int nUser = (Request.QueryString["User"] == null ? 0 : Convert.ToInt32(Request.QueryString["User"].ToString()));
            string cashier = (Request.QueryString["Cashier"] == null ? "All" : Request.QueryString["Cashier"].ToString());
            int viewAutIntNo = (Request.QueryString["AutIntNo"] == null ? 0 : Convert.ToInt32(Request.QueryString["AutIntNo"].ToString()));

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "CashierTran");
            if (reportPage.Equals(""))
            {
                int arnIntNo = arn.AddAuthReportName(autIntNo, "CashierTran.rpt", "CashierTran", "system", "");
                reportPage = "CashierTran.rpt";
            }

            //dls 070816 - need an Authority Rule as to whether to include electronic payments in the EOD report
            // Check if this authority processes NaTIS data last
            //AuthorityRulesDB arDB = new AuthorityRulesDB(this.connectionString);
            //AuthorityRulesDetails details = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "4591",
            //    "Rule that indicates whether to include electronic/manual captured payments in the EOD report", 0,
            //    "N", "Y = Yes; N = No (Default)",
            //    userDetails.UserLoginName);

            //20090113 SD	
            //AutIntNo, ARCode and LastUser need to be set from here
            AuthorityRulesDetails rule = new AuthorityRulesDetails();
            rule.AutIntNo = autIntNo;
            rule.ARCode = "4591";
            rule.LastUser = userDetails.UserLoginName;

            DefaultAuthRules authRule = new DefaultAuthRules(rule, this.connectionString);
            KeyValuePair<int, string> value = authRule.SetDefaultAuthRule();

            string includeElectronicPayments = value.Value;
            string reportPath = Server.MapPath("reports/" + reportPage);

            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "CashierTran");

            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error1"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {
                //dls 081117 - we can only check that the template path exists if there is actually a template!
                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error2"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************
            
            _reportDoc.Load(reportPath);

            // Fill the DataSet
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("CashierTranReport");
            da.SelectCommand.Connection = new SqlConnection(this.connectionString);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@AutIntNo", SqlDbType.Int, 4).Value = viewAutIntNo;
            da.SelectCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = dtStart;
            da.SelectCommand.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = dtEnd;
            da.SelectCommand.Parameters.Add("@UserIntNo", SqlDbType.Int, 4).Value = nUser;
            da.SelectCommand.Parameters.Add("@IncludeElectronicPayments", SqlDbType.Char, 1).Value = includeElectronicPayments;

            this.ds = new dsCashierTran();
            this.ds.DataSetName = "dsCashierTran";
            da.Fill(this.ds);
            da.Dispose();

            // Populate the report
            _reportDoc.SetDataSource(this.ds.Tables[1]);
            ds.Dispose();

            //2015-02-05 changed for filterParameters formate is not correct.(5384)
            //set filter subreport data
            string filterParameters = string.Format("FILTER PARAMETERS:\tDate Start:  {0}\tDate End:  {1}\tCashier:  {2} \n\t\t\t\t\tProduced By:  {3} on {4}", string.Format("{0:yyyy-MM-dd}", dtStart), string.Format("{0:yyyy-MM-dd}", dtEnd),
                cashier, userDetails.UserInit + " " + userDetails.UserSName, DateTime.Today.ToString(DATE_FORMAT));

            dsFilterParameters dsFilterParameters = new dsFilterParameters();
            DataRow row = dsFilterParameters.Tables[0].NewRow();
            row["FilterParameters"] = filterParameters;
            dsFilterParameters.Tables[0].Rows.Add(row);

            _reportDoc.Subreports["FilterParameters"].SetDataSource(dsFilterParameters.Tables[0]);
            dsFilterParameters.Dispose();

            //export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)_reportDoc.ExportToStream(ExportFormatType.PortableDocFormat);
            _reportDoc.Dispose();

            //stuff the PDF file into rendering stream
            //first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();

        }

    }
}