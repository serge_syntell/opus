﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents a viewer for an offence(s)
    /// </summary>
    public partial class ViewHistoricalSummons_Report : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private ReportDocument report = new ReportDocument();
        private int autIntNo = 0;
        //protected string thisPage = "View Historical Summons Report";
        protected string thisPageURL = "ViewHistoricalSummons_Report.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Load"></see> event.
        /// </summary>
        /// <param name="e">The <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Check that there's a Query criteria in the Session
            NoticeQueryCriteria criteria = Session["NoticeQueryCriteria"] as NoticeQueryCriteria;
            if (criteria == null)
                Response.Redirect("Login.aspx");
            Session.Remove("NoticeQueryCriteria");

            // Setup the report
            string reportPage = "HistoricalSummonsSummary.rpt";
            string reportPath = Server.MapPath("reports/HistoricalSummonsSummary.rpt");

            if (!File.Exists(reportPath))
            {
                string error = string.Format(("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }

            this.report = new ReportDocument();
            report.Load(reportPath);

            SummonsDB summonWithdrawnReissue = new SummonsDB(this.connectionString);
            dsOfenceSummary ds = (dsOfenceSummary)summonWithdrawnReissue.GetSummonsWithdrawnReissueSearchDS(criteria.AutIntNo, criteria.ColumnName, criteria.Value, criteria.DateSince);

            // Populate the report
            report.SetDataSource(ds.Tables[1]);
            ds.Dispose();

            // Export the PDF file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat);
            report.Dispose();

            //stuff the PDF file into rendering stream
            //first clear everything dynamically created and just send PDF file
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();
        }

    }
}