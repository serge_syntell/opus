using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using System.Threading;
using SIL.AARTO.BLL.Admin;
using System.Collections.Generic;
using System.Transactions;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;



namespace Stalberg.TMS
{

    public partial class Metro : System.Web.UI.Page
    {
        protected string connectionString = string.Empty;
        protected string styleSheet;
        protected string backgroundImage;
        protected string loginUser;
        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
        protected int autIntNo = 0;
        //protected string thisPage = "Metro maintenance";
        protected string thisPageURL = "Metro.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;

            //may need to check user access level here....
            //			if (userAccessLevel<7)
            //				Server.Transfer(Session["prevPage"].ToString());


            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlAddMetro.Visible = false;
                pnlUpdateMetro.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptHide.Visible = true;

                PopulateRegion(ddlRegion);

                BindGrid();
            }
        }

        protected void PopulateRegion(DropDownList ddlSelRegion)
        {
            RegionDB region = new RegionDB(connectionString);

            SqlDataReader reader = region.GetRegionList("RegName", Thread.CurrentThread.CurrentCulture.ToString());
            ddlSelRegion.DataSource = reader;
            ddlSelRegion.DataValueField = "RegIntNo";
            ddlSelRegion.DataTextField = "RegDescr";
            ddlSelRegion.DataBind();

            reader.Close();
        }

        protected void BindGrid()
        {
            //int mtrIntNo = 0;

            Stalberg.TMS.MetroDB metroList = new Stalberg.TMS.MetroDB(connectionString);

            DataSet data = metroList.GetMetroListDS("MtrName");
            dgMetro.DataSource = data;
            dgMetro.DataKeyField = "MtrIntNo";

            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgMetro.DataBind();
            }
            catch
            {
                dgMetro.CurrentPageIndex = 0;
                dgMetro.DataBind();
            }

            if (dgMetro.Items.Count == 0)
            {
                dgMetro.Visible = false;
                lblError.Visible = true;
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgMetro.Visible = true;
            }

            data.Dispose();
            pnlAddMetro.Visible = false;
            pnlUpdateMetro.Visible = false;

        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // allow transactions to be added to the database table
            pnlAddMetro.Visible = true;
            pnlUpdateMetro.Visible = false;

            btnOptDelete.Visible = false;

            PopulateRegion(ddlAddRegion);
            //2012-3-6 linda modified into multi-language
            ddlAddRegion.Items.Insert(0, (string)GetLocalResourceObject("ddlAddRegion.Items"));

            //Jerry 2013-11-07 add multi-language
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupAdd.DataBind(entityList);
        }

        protected void btnAddMetro_Click(object sender, System.EventArgs e)
        {
            if (ddlAddRegion.SelectedIndex < 1)
            {
                lblError.Visible = true;
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                return;
            }
            byte[] bytes;
            if (upload.HasFile)
            {
                Byte[] imagebytes = new byte[this.upload.PostedFile.InputStream.Length];
                BinaryReader br = new BinaryReader(this.upload.PostedFile.InputStream);
                imagebytes = br.ReadBytes(Convert.ToInt32(this.upload.PostedFile.InputStream.Length));
                bytes = imagebytes;
            }
            else
            {
                bytes = new byte[4];
            }
            int regIntNo = Convert.ToInt32(ddlAddRegion.SelectedValue);
            //int mtrIntNo = Convert.ToInt32(Session["editMtrIntNo"]);
            //int mtrIntNo = Convert.ToInt32(ddlAddRegion.SelectedValue);

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupAdd.Save();
            if (lgEntityList == null || lgEntityList.Count <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                return;
            }

            // add the new transaction to the database table
            Stalberg.TMS.MetroDB toAdd = new MetroDB(connectionString);

            //Jerry 2013-11-07 add transaction scope
            using (TransactionScope scope = new TransactionScope())
            {
                int addMtrIntNo = toAdd.AddMetro(regIntNo, txtAddMetroCode.Text, txtAddMetroNo.Text, txtAddMetroName.Text,
                    txtAddMetroPostAddr1.Text, txtAddMetroPostAddr2.Text, txtAddMetroPostAddr3.Text, txtAddMetroPostCode.Text,
                    txtAddMetroTel.Text, txtAddMetroFax.Text, txtAddMetroEmail.Text, loginUser,
                    Add_MtrNoticePaymentInfo.Text,
    Add_MtrNoticeIssuedByInfo.Text,
    Add_MtrPhysAdd1.Text,
    Add_MtrPhysAdd2.Text,
    Add_MtrPhysAdd3.Text,
    Add_MtrPhysAdd4.Text,
    Add_MtrPhysCode.Text,
    Add_MtrPayPoint1Add1.Text,
    Add_MtrPayPoint1Add2.Text,
    Add_MtrPayPoint1Add3.Text,
    Add_MtrPayPoint1Add4.Text,
    Add_MtrPayPoint1Code.Text,
    Add_MtrPayPoint1Tel.Text,
    Add_MtrPayPoint1Fax.Text,
    Add_MtrPayPoint1Email.Text,
    Add_MtrPayPoint2Add1.Text,
    Add_MtrPayPoint2Add2.Text,
    Add_MtrPayPoint2Add3.Text,
    Add_MtrPayPoint2Add4.Text,
    Add_MtrPayPoint2Code.Text,
    Add_MtrPayPoint2Tel.Text,
    Add_MtrPayPoint2Fax.Text,
    Add_MtrPayPoint2Email.Text,
    Add_MtrReceiptEnglishAct.Text,
    Add_MtrReceiptAfrikaansAct.Text,
    Add_MtrDepartName.Text,
    Add_MtrDocumentFromEnglish.Text,
    Add_MtrDocumentFromAfrikaans.Text,
    Add_MtrFooter.Text, bytes, txtAddMtrAfri.Text.Trim());

                if (addMtrIntNo <= 0)
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                }
                else
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text3");
                    Session["editMtrIntNo"] = addMtrIntNo;

                    //Jerry 2013-11-07 add multi-language
                    
                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                    rUMethod.UpdateIntoLookup(addMtrIntNo, lgEntityList, "MetroLookup", "MtrIntNo", "MtrName", this.loginUser);
					
					//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                	SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                	punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.MetroMaintenance, PunchAction.Add); 

                    scope.Complete();
                }

                lblError.Visible = true;
            }

            BindGrid();
        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            int regIntNo = Convert.ToInt32(ddlRegion.SelectedValue);
            int mtrIntNo = Convert.ToInt32(Session["editMtrIntNo"]);
            Stalberg.TMS.MetroDB mtrUpdate = new MetroDB(connectionString);

            byte[] bytes;
            if (this.uploads.HasFile)
            {
                Byte[] imagebytes = new byte[this.uploads.PostedFile.InputStream.Length];
                BinaryReader br = new BinaryReader(this.uploads.PostedFile.InputStream);
                imagebytes = br.ReadBytes(Convert.ToInt32(this.uploads.PostedFile.InputStream.Length));
                bytes = imagebytes;
            }
            else
            {
                bytes = mtrUpdate.GetLogo(mtrIntNo).MtrLogo;
            }

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            if (lgEntityList == null || lgEntityList.Count <= 0)
            {
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                return;
            }

            // add the new transaction to the database table
            //Jerry 2013-11-07 add transaction scope
            using (TransactionScope scope = new TransactionScope())
            {
                int updMtrIntNo = mtrUpdate.UpdateMetro(regIntNo, txtMetroCode.Text, txtMetroNo.Text, txtMetroName.Text,
                    txtMetroPostAddr1.Text, txtMetroPostAddr2.Text, txtMetroPostAddr3.Text, txtMetroPostCode.Text,
                    txtMetroTel.Text, txtMetroFax.Text, txtMetroEmail.Text, loginUser, mtrIntNo,
    Up_MtrNoticePaymentInfo.Text,
    Up_MtrNoticeIssuedByInfo.Text,
    Up_MtrPhysAdd1.Text,
    Up_MtrPhysAdd2.Text,
    Up_MtrPhysAdd3.Text,
    Up_MtrPhysAdd4.Text,
    Up_MtrPhysCode.Text,
    Up_MtrPayPoint1Add1.Text,
    Up_MtrPayPoint1Add2.Text,
    Up_MtrPayPoint1Add3.Text,
    Up_MtrPayPoint1Add4.Text,
    Up_MtrPayPoint1Code.Text,
    Up_MtrPayPoint1Tel.Text,
    Up_MtrPayPoint1Fax.Text,
    Up_MtrPayPoint1Email.Text,
    Up_MtrPayPoint2Add1.Text,
    Up_MtrPayPoint2Add2.Text,
    Up_MtrPayPoint2Add3.Text,
    Up_MtrPayPoint2Add4.Text,
    Up_MtrPayPoint2Code.Text,
    Up_MtrPayPoint2Tel.Text,
    Up_MtrPayPoint2Fax.Text,
    Up_MtrPayPoint2Email.Text,
                    //Up_MtrDocumentFrom.Text,
                    //Up_MtrReceiptFrom.Text,
    Up_MtrReceiptEnglishAct.Text,
    Up_MtrReceiptAfrikaansAct.Text,
    Up_MtrDepartName.Text,
    Up_MtrDocumentFromEnglish.Text,
    Up_MtrDocumentFromAfrikaans.Text,
    Up_MtrFooter.Text, bytes, txtUpdateMtrNameAfri.Text);

                if (updMtrIntNo <= 0)
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text4");
                }
                else
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");

                    //Jerry 2013-11-07 add multi-language
                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                    rUMethod.UpdateIntoLookup(updMtrIntNo, lgEntityList, "MetroLookup", "MtrIntNo", "MtrName", this.loginUser);

					//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                	SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                	punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.MetroMaintenance, PunchAction.Change); 

                    scope.Complete();
                }

                lblError.Visible = true;
            }
            BindGrid();
        }

        protected void dgMetro_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgMetro.SelectedIndex = e.Item.ItemIndex;

            if (dgMetro.SelectedIndex > -1)
            {
                int editMtrIntNo = Convert.ToInt32(dgMetro.DataKeys[dgMetro.SelectedIndex]);

                Session["editMtrIntNo"] = editMtrIntNo;
                Session["prevPage"] = thisPageURL;

                //Jerry 2013-11-07 add multi-language
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editMtrIntNo.ToString(), "MetroLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                ShowMetroDetails(editMtrIntNo);
            }
        }

        protected void ShowMetroDetails(int editMtrIntNo)
        {
            // Obtain and bind a list of all users
            Stalberg.TMS.MetroDB aut = new Stalberg.TMS.MetroDB(connectionString);

            Stalberg.TMS.MetroDetails mtrDetails = aut.GetMetroDetails(editMtrIntNo);

            txtMetroCode.Text = mtrDetails.MtrCode;
            txtMetroName.Text = mtrDetails.MtrName;
            txtMetroNo.Text = mtrDetails.MtrNo;
            txtMetroPostAddr1.Text = mtrDetails.MtrPostAddr1;
            txtMetroPostAddr2.Text = mtrDetails.MtrPostAddr2;
            txtMetroPostAddr3.Text = mtrDetails.MtrPostAddr3;
            txtMetroPostCode.Text = mtrDetails.MtrPostCode;
            txtMetroTel.Text = mtrDetails.MtrTel;
            txtMetroFax.Text = mtrDetails.MtrFax;
            txtMetroEmail.Text = mtrDetails.MtrEmail;

            Up_MtrNoticePaymentInfo.Text = mtrDetails.MtrNoticePaymentInfo;
            Up_MtrNoticeIssuedByInfo.Text = mtrDetails.MtrNoticeIssuedByInfo;
            Up_MtrPhysAdd1.Text = mtrDetails.MtrPhysAdd1;
            Up_MtrPhysAdd2.Text = mtrDetails.MtrPhysAdd2;
            Up_MtrPhysAdd3.Text = mtrDetails.MtrPhysAdd3;
            Up_MtrPhysAdd4.Text = mtrDetails.MtrPhysAdd4;
            Up_MtrPhysCode.Text = mtrDetails.MtrPhysCode;
            Up_MtrPayPoint1Add1.Text = mtrDetails.MtrPayPoint1Add1;
            Up_MtrPayPoint1Add2.Text = mtrDetails.MtrPayPoint1Add2;
            Up_MtrPayPoint1Add3.Text = mtrDetails.MtrPayPoint1Add3;
            Up_MtrPayPoint1Add4.Text = mtrDetails.MtrPayPoint1Add4;
            Up_MtrPayPoint1Code.Text = mtrDetails.MtrPayPoint1Code;
            Up_MtrPayPoint1Tel.Text = mtrDetails.MtrPayPoint1Tel;
            Up_MtrPayPoint1Fax.Text = mtrDetails.MtrPayPoint1Fax;
            Up_MtrPayPoint1Email.Text = mtrDetails.MtrPayPoint1Email;
            Up_MtrPayPoint2Add1.Text = mtrDetails.MtrPayPoint2Add1;
            Up_MtrPayPoint2Add2.Text = mtrDetails.MtrPayPoint2Add2;
            Up_MtrPayPoint2Add3.Text = mtrDetails.MtrPayPoint2Add3;
            Up_MtrPayPoint2Add4.Text = mtrDetails.MtrPayPoint2Add4;
            Up_MtrPayPoint2Code.Text = mtrDetails.MtrPayPoint2Code;
            Up_MtrPayPoint2Tel.Text = mtrDetails.MtrPayPoint2Tel;
            Up_MtrPayPoint2Fax.Text = mtrDetails.MtrPayPoint2Fax;
            Up_MtrPayPoint2Email.Text = mtrDetails.MtrPayPoint2Email;
            //Up_MtrDocumentFrom.Text = mtrDetails.MtrDocumentFrom;
            //Up_MtrReceiptFrom.Text = mtrDetails.MtrReceiptFrom;
            Up_MtrReceiptEnglishAct.Text = mtrDetails.MtrReceiptEnglishAct;
            Up_MtrReceiptAfrikaansAct.Text = mtrDetails.MtrReceiptAfrikaansAct;
            Up_MtrDepartName.Text = mtrDetails.MtrDepartName;
            Up_MtrDocumentFromEnglish.Text = mtrDetails.MtrDocumentFromEnglish;
            Up_MtrDocumentFromAfrikaans.Text = mtrDetails.MtrDocumentFromAfrikaans;
            Up_MtrFooter.Text = mtrDetails.MtrFooter;
            this.mtrlogo.ImageUrl = "readMtrImage.ashx?id=" + editMtrIntNo;
            txtUpdateMtrNameAfri.Text = mtrDetails.MtrNameAfri;

            ddlRegion.SelectedIndex = ddlRegion.Items.IndexOf(ddlRegion.Items.FindByValue(mtrDetails.RegIntNo.ToString()));

            pnlUpdateMetro.Visible = true;
            pnlAddMetro.Visible = false;

            btnOptDelete.Visible = true;
        }


        protected void dgMetro_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgMetro.CurrentPageIndex = e.NewPageIndex;

            BindGrid();
        }

        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
            //Jerry 2013-11-07 here has a bug, should using mtroIntNo, not autIntNo
            //int autIntNo = Convert.ToInt32(Session["editAutIntNo"]);
            int mtrIntNo = Convert.ToInt32(Session["editMtrIntNo"]);

            MetroDB aut = new Stalberg.TMS.MetroDB(connectionString);

            using (TransactionScope scope = new TransactionScope())
            {
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                rUMethod.DeleteLookup(lgEntityList, "MetroLookup", "MtrIntNo");

                //int delAutIntNo = aut.DeleteMetro(autIntNo);
                int delMtrIntNo = aut.DeleteMetro(mtrIntNo);

                if (delMtrIntNo == 0)
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                }
                else if (delMtrIntNo == -1)
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
                }
                else
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text8");

					//2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                	SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                	punchStatistics.PunchStatisticsTransactionAdd(autIntNo, this.loginUser, PunchStatisticsTranTypeList.MetroMaintenance, PunchAction.Delete); 

                    scope.Complete();
                }
            }

            lblError.Visible = true;
            BindGrid();
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgMetro.Visible.Equals(true))
            {
                //hide it
                dgMetro.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
            }
            else
            {
                //show it
                dgMetro.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindGrid();
        }

        //protected void ddlSelMetro_SelectedIndexChanged(object sender, System.EventArgs e)
        //{
        //    dgMetro.Visible = true;
        //    btnOptHide.Text = "Hide list";

        //    BindGrid();
        //}



    }
}
