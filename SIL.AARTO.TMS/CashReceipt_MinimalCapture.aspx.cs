﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stalberg.TMS;
using SIL.AARTO.TMS.OffenceCodeService;
using System.Text.RegularExpressions;
using System.Text;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using System.Web.Script.Serialization;
using Stalberg.TMS.Data.Util;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;


namespace SIL.AARTO.TMS
{
    public partial class CashReceipt_NoNoticeMatch : System.Web.UI.Page
    {
        // Constants
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private const string CURRENCY_FORMAT = "###0.00";
        // Fields
        private string connectionString = String.Empty;
        private string loginUser;
        //private Cashier cashier;

        protected string styleSheet;
        protected string backgroundImage;
        protected double myTotal = 0;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        //protected string thisPage = "Cash Receipts: Traffic Department";
        protected string thisPageURL = "CashReceiptTraffic.aspx";
        protected int autIntNo = 0;
        protected int userIntNo = 0;
        protected string alwaysUsePostalReceiptReport = "N";

        protected static CourtRoomDB courtRoomDB = null;
        protected static CourtDB courtDB = null;
        protected static CourtDatesDB courtDateDB = null;
        protected static TrafficOfficerDB officerDB = null;
        protected static PaymentOriginatorService pymtOrignatorService = new PaymentOriginatorService();

        //static NoticeService notService = new NoticeService();
        //static ChargeService chgService = new ChargeService();
        //static DriverService drvService = new DriverService();
        //static CourtService crtService = new CourtService();
        static CourtRoomService crService = new CourtRoomService();

        protected void Page_Load(object sender, EventArgs e)
        {


            this.connectionString = Application["constr"].ToString();
            bool status = false;
            float fineAmount = 0;
            //Adam 20120206:Populate fine amount based on the offence code by means of  jquery asynchronous posting
            //if (Request.ServerVariables["Request_Method"] == "POST")
            //{


            // Context.Response.ContentType = "application/json";

            // String action = Context.Request.Params["Action"].ToString();
            // if(Context.Request.Params["Action"]

            // switch (action)
            // {
            //     case "PopulateFineAmount1":
            //         String offencecode1 = Context.Request.Params["offencecode1"].ToString();
            //         //string aa = this.ddlOffenceCode1.SelectedValue.ToString();

            //         fineAmount=PopulateFineAmount(offencecode1);

            //         //this.txtFineAmount1.Text = "150";
            //         break;
            //     case "PopulateFineAmount2":
            //         String offencecode2 = Context.Request.Params["offencecode2"].ToString();
            //         fineAmount = PopulateFineAmount(offencecode2);
            //         break;

            //     case "PopulateFineAmount3":
            //         String offencecode3 = Context.Request.Params["offencecode3"].ToString();
            //         fineAmount = PopulateFineAmount(offencecode3);
            //         break;
            // }
            //// status = true;
            // Context.Response.Write(JsonSerializer.JsonSerialize(fineAmount));
            // Context.Response.End();


            //}
            //else
            //{
            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDetails userDetails = (UserDetails)Session["userDetails"];
            userIntNo = Int32.Parse(Session["userIntNo"].ToString());

            loginUser = userDetails.UserLoginName;

            //int 
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.btnConfirm.Visible = false;
                this.btnManually.Visible = true;

                this.divMain.Visible = true;
                this.divConfirm.Visible = false;

                if (!String.IsNullOrEmpty(Request.QueryString["TicketNo"]))
                {
                    this.txtNoticeNumber.Text = Request.QueryString["TicketNo"];
                }

                InitConnection();

                PopulateCourt(autIntNo);

                PopulateTrafficOfficer(autIntNo);

                PopulatePaymentOriginator();

                PopulateOffenceCode();


            }
            RegisterScript();

            //}
        }

        protected void btnManually_Click(object sender, EventArgs e)
        {
            //this.ddlOffenceCode1.SelectedValue = Request.Params[this.ddlOffenceCode1.UniqueID];
            //this.ddlOffenceCode2.SelectedValue = Request.Params[this.ddlOffenceCode2.UniqueID];
            //this.ddlOffenceCode3.SelectedValue = Request.Params[this.ddlOffenceCode3.UniqueID];
            //this.ddlFileType.SelectedValue = Request.Params[this.ddlFileType.UniqueID];

            if (ValidateInput() == false)
            {
                return;
            }

            if (ValidateFineAmount())
            {
                return;
            }
            //ScriptManager.RegisterClientScriptBlock(this.UDP, this.UDP.GetType(), "message", "$(document).ready(function(){$.blockUI({ message: $('#divConfirm')});})", true);

            this.divConfirm.Visible = true;
            //this.divMain.Visible = false;
            this.btnManually.Visible = false;
            this.btnConfirm.Visible = false;
            this.btnCancelFineAmount.Visible = false;
        }
        float PopulateFineAmount(String offenceCodeFromRequest)
        {
            string offenceCode = offenceCodeFromRequest.Split('~')[0];
            float fineAmount = GetOffenceFineAmount(offenceCode);
            return fineAmount;
            //if (fineAmount == 0)
            //{
            //    txtFineAmount.Text = "";
            //    hidFineAmount.Value = "";
            //}
            //else
            //{
            //    txtFineAmount.Text = fineAmount.ToString(CURRENCY_FORMAT);
            //    hidFineAmount.Value = fineAmount.ToString();
            //}
        }
        protected void ddlOffenceCode1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string offenceCode1 = this.ddlOffenceCode1.SelectedValue.Split('~')[0];
            this.lblAmount1Msg.Text = "";
            if (this.ddlOffenceCode1.SelectedValue.IndexOf("~Y") >= 0)
            {
                this.lblAmount1Msg.Text = (string)GetLocalResourceObject("lblError.Text90");
            }

            float fineAmount = GetOffenceFineAmount(offenceCode1);

            if (fineAmount == 0)
            {
                this.txtFineAmount1.Text = "";
                this.hidFineAmount1.Value = "";
            }
            else
            {
                this.txtFineAmount1.Text = fineAmount.ToString(CURRENCY_FORMAT);
                this.hidFineAmount1.Value = fineAmount.ToString();
            }
        }

        protected void ddlOffenceCode2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string offenceCode2 = this.ddlOffenceCode2.SelectedValue.Split('~')[0];
            this.lblAmount2Msg.Text = "";
            if (this.ddlOffenceCode2.SelectedValue.IndexOf("~Y") >= 0)
            {
                this.lblAmount2Msg.Text = (string)GetLocalResourceObject("lblError.Text90");
            }

            float fineAmount = GetOffenceFineAmount(offenceCode2);
            if (fineAmount == 0)
            {
                this.txtFineAmount2.Text = "";
                this.hidFineAmount2.Value = "";
            }
            else
            {
                this.txtFineAmount2.Text = fineAmount.ToString(CURRENCY_FORMAT);
                this.hidFineAmount2.Value = fineAmount.ToString();
            }
        }

        protected void ddlOffenceCode3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string offenceCode3 = this.ddlOffenceCode3.SelectedValue.Split('~')[0];
            this.lblAmount3Msg.Text = "";
            if (this.ddlOffenceCode3.SelectedValue.IndexOf("~Y") >= 0)
            {
                this.lblAmount3Msg.Text = (string)GetLocalResourceObject("lblError.Text90");
            }

            float fineAmount = GetOffenceFineAmount(offenceCode3);
            if (fineAmount == 0)
            {
                this.txtFineAmount3.Text = "";
                this.hidFineAmount3.Value = "";
            }
            else
            {
                this.txtFineAmount3.Text = fineAmount.ToString(CURRENCY_FORMAT);
                this.hidFineAmount3.Value = fineAmount.ToString();
            }
        }

        protected void btnProceed_Click(object sender, EventArgs e)
        {
            //this.ddlOffenceCode1.SelectedValue = Request.Params[this.ddlOffenceCode1.UniqueID];
            //this.ddlOffenceCode2.SelectedValue = Request.Params[this.ddlOffenceCode2.UniqueID];
            //this.ddlOffenceCode3.SelectedValue = Request.Params[this.ddlOffenceCode3.UniqueID];
            //this.ddlFileType.SelectedValue = Request.Params[this.ddlFileType.UniqueID];
            string ticketNo = string.Empty;
            int pymtOriginator = Convert.ToInt32(dllOriginator.SelectedValue);
            int notIntNo = SaveHandwrittenOffenceByMinimalCapture(out ticketNo);
            if (notIntNo > 0)
            {
                Response.Redirect(String.Format("~/CashReceiptTraffic_Details.aspx?TicketNo={0}&PymtOriginator={1}", ticketNo, pymtOriginator));
            }
            else
            {
                if (this.ddlFileType.SelectedValue.StartsWith("H"))
                {
                    CaptureErrorMessageForSection341(notIntNo);
                }
                else
                {
                    CaptureErrorMessageForSection56(notIntNo);
                }
                this.divConfirm.Visible = false;
                this.divMain.Visible = true;
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            this.divConfirm.Visible = true;
            this.divMain.Visible = true;
            this.btnManually.Visible = false;
            this.btnConfirm.Visible = false;
            this.btnCancelFineAmount.Visible = false;
        }

        protected void btnCancelFineAmount_Click(object sender, EventArgs e)
        {

            this.ddlOffenceCode1.SelectedIndex = 0;
            this.ddlOffenceCode2.SelectedIndex = 0;
            this.ddlOffenceCode3.SelectedIndex = 0;
            this.txtFineAmount1.Text = this.hidFineAmount1.Value = "";
            this.txtFineAmount2.Text = this.hidFineAmount2.Value = "";
            this.txtFineAmount3.Text = this.hidFineAmount3.Value = "";
            this.btnManually.Visible = true;
            this.btnConfirm.Visible = false;
            this.lblError.Text = "";
            Response.Redirect("~/CashReceiptTraffic.aspx");
        }

        void InitConnection()
        {
            courtDB = new CourtDB(this.connectionString);
            courtRoomDB = new CourtRoomDB(this.connectionString);
            courtDateDB = new CourtDatesDB(this.connectionString);
            officerDB = new TrafficOfficerDB(this.connectionString);
        }

        bool ValidateFineAmount()
        {
            bool falied = false;
            bool _continue = false;
            Decimal fineAmount = 0; Decimal fineAmountInDB = 0;
            Decimal.TryParse(String.Format("{0:D}", this.txtFineAmount1.Text), out fineAmount);
            Decimal.TryParse(this.hidFineAmount1.Value, out fineAmountInDB);
            StringBuilder msg = new StringBuilder();
            msg.Append("<ul>");
            if (fineAmount == 0)
            {
                msg.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text")).Append("</li>");
                falied = true;
            }
            else
            {
                if (fineAmount != fineAmountInDB)
                {
                    msg.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text1") + this.hidFineAmount1.Value).Append("</li>");
                    falied = true;
                    _continue = true;
                }
            }
            fineAmount = fineAmountInDB = 0;
            if (this.ddlOffenceCode2.SelectedValue != "")
            {
                Decimal.TryParse(this.txtFineAmount2.Text, out fineAmount);
                Decimal.TryParse(this.hidFineAmount2.Value, out fineAmountInDB);
                if (fineAmount == 0)
                {
                    msg.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text2")).Append("</li>");
                    falied = true;
                }
                else
                {
                    if (fineAmount != fineAmountInDB)
                    {
                        msg.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text3") + this.hidFineAmount2.Value).Append("</li>");
                        falied = true;
                        _continue = true;
                    }
                }
            }
            fineAmount = fineAmountInDB = 0;
            if (this.ddlOffenceCode3.SelectedValue != "")
            {
                Decimal.TryParse(this.txtFineAmount3.Text, out fineAmount);
                Decimal.TryParse(this.hidFineAmount3.Value, out fineAmountInDB);
                if (fineAmount == 0)
                {
                    msg.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text4")).Append("</li>");
                    falied = true;
                }
                else
                {
                    if (fineAmount != fineAmountInDB)
                    {
                        msg.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text5") + this.hidFineAmount3.Value).Append("</li>");
                        falied = true;
                        _continue = true;
                    }
                }
            }

            if (falied)
            {
                msg.Append("</ul>");
                if (_continue)
                {
                    this.btnConfirm.Visible = true;
                    this.btnManually.Visible = false;
                }
                this.lblError.Text = msg.ToString();
            }

            return falied;
        }

        void PopulateOffenceCode()
        {
            OffenceCodeService.Service1 s = new OffenceCodeService.Service1();
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TMSConnectionString"].ConnectionString);
            Option[] options = s.GetOptions("Offence", "", "AARTO");// conn.Database);
            List<Option> listOptions = new List<Option>();
            listOptions = options.ToList();

            listOptions.Insert(0, new Option() { DTDLDLText = (string)GetLocalResourceObject("listOptions.Item"), DTDLDValue = "" });

            foreach (Option option in listOptions)
            {
                this.ddlOffenceCode1.Items.Add(new ListItem(option.DTDLDLText, option.DTDLDValue));
                this.ddlOffenceCode2.Items.Add(new ListItem(option.DTDLDLText, option.DTDLDValue));
                this.ddlOffenceCode3.Items.Add(new ListItem(option.DTDLDLText, option.DTDLDValue));
            }

        }

        void PopulateCourt(int autIntNo)
        {
            this.dllCourt.Items.Clear();
            ListItem item;

            SqlDataReader reader = courtDB.GetAuth_CourtListByAuth(this.autIntNo);
            while (reader.Read())
            {
                if (reader["CrtNo"].ToString().Trim() == "000000" || reader["CrtName"].ToString().Trim().ToLower() == "unknown")
                    continue;
                item = new ListItem();
                item.Value = reader["CrtIntNo"].ToString();
                item.Text = reader["CrtDetails"].ToString();

                this.dllCourt.Items.Add(item);
            }
            reader.Close();

            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("listOptions.Item");
            this.dllCourt.Items.Insert(0, item);
            this.dllCourt.SelectedIndex = 0;
        }

        void PopulateCourtRoom(int crtIntNo)
        {

            this.dllCourtRoom.Items.Clear();
            ListItem item;

            //SqlDataReader reader = courtRoomDB.GetCourtRoomList(crtIntNo);

            //this.dllCourtRoom.DataSource = reader;
            //this.dllCourtRoom.DataValueField = "CrtRIntNo";
            //this.dllCourtRoom.DataTextField = "CrtRoomDetails";
            //this.dllCourtRoom.DataBind();
            //reader.Close();

            TList<SIL.AARTO.DAL.Entities.CourtRoom> crtRooms = crService.GetActiveCourtRoomList(crtIntNo);
            foreach (SIL.AARTO.DAL.Entities.CourtRoom c in crtRooms)
            {
                item = new ListItem();
                //CrtRoomNo + ' (' + CrtRoomName + ')' as CrtRoomDetails
                item.Value = c.CrtRintNo.ToString();
                item.Text = String.Format("{0} ({1})", c.CrtRoomNo, c.CrtRoomName);
                this.dllCourtRoom.Items.Add(item);
            }

            // Insert the dummy item
            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("listOptions.Item");
            this.dllCourtRoom.Items.Insert(0, item);

            if (this.dllCourtRoom.Items.Count > 1)
            {
                this.dllCourtRoom.SelectedIndex = 1;
            }
            else
            {
                this.dllCourtRoom.SelectedIndex = 0;
            }
            this.dllCourtRoom.Enabled = false;
        }

        void PopulateCourtDate(int crtRIntNo, string notFilmType)
        {
            // Populate the court dates
            this.dllCourtDate.Items.Clear();
            ListItem item;

            List<SIL.AARTO.DAL.Entities.CourtDates> cDates = new CourtDatesService().GetByAutIntNoCrtRintNo(this.autIntNo, crtRIntNo).Where(c => c.Cdate > DateTime.Now.Date && c.CdateLocked == false).ToList();


            //SqlDataReader reader = courtDateDB.GetCourtDatesList(crtRIntNo, this.autIntNo);    // Oscar 20100920 - Change 
            //SqlDataReader reader = courtDateDB.GetCourtDatesAvailableList(crtRIntNo, this.autIntNo, notFilmType);
            //SqlDataReader reader = db.GetWarrantAndSummonsCountDatesList(crtRIntNo, this.autIntNo);   // Jake 2013-10-29 added new function
            //while (reader.Read())
            foreach (SIL.AARTO.DAL.Entities.CourtDates cd in cDates.OrderByDescending(c=>c.Cdate))
            {
                item = new ListItem();
                item.Value = String.Format("{0:yyyy-MM-dd}", cd.Cdate);  //reader["CDIntNo"].ToString();
                item.Text = String.Format("{0:yyyy-MM-dd}", cd.Cdate);

                this.dllCourtDate.Items.Add(item);
            }
            //reader.Close();

            // Insert the dummy item
            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("listOptions.Item");
            this.dllCourtDate.Items.Insert(0, item);
            this.dllCourtDate.SelectedIndex = 0;
        }

        void PopulatePaymentOriginator()
        {
            //PaymentOriginatorList.t
            string[] pymtOriginator = new string[] {
                ((int)PaymentOriginatorList.OPUS).ToString(),
                ((int)PaymentOriginatorList.TCS).ToString(),
                ((int)PaymentOriginatorList.SAPS).ToString(),
                ((int)PaymentOriginatorList.Provincial).ToString(),
                ((int)PaymentOriginatorList.CorrectionalServices).ToString()
            };
            PaymentOriginatorQuery query = new PaymentOriginatorQuery();
            query.AppendIn(PaymentOriginatorColumn.PoIntId, pymtOriginator);
            TList<PaymentOriginator> pymtOriginators = pymtOrignatorService.Find(query);

            this.dllOriginator.Items.Clear();
            foreach (PaymentOriginator p in pymtOriginators)
                this.dllOriginator.Items.Add(new ListItem(p.PoDescription, p.PoIntId.ToString()));

            //this.dllOriginator.Items.Insert(0, new ListItem((string)GetLocalResourceObject("listOptions.Item"), "0"));
        }

        void PopulateTrafficOfficer(int autIntNo)
        {
            this.dllOfficer.Items.Clear();
            ListItem item;

            SqlDataReader reader = officerDB.GetTrafficOfficerListByAuthority(autIntNo);
            while (reader.Read())
            {
                item = new ListItem();
                item.Value = reader["TONo"].ToString();
                item.Text = String.Format("{0}, {1} ({2}~{3})",
                    reader["TOSName"].ToString(),
                    reader["TOInit"].ToString(),
                    reader["TOGroup"].ToString(),
                    reader["TONo"].ToString());
                //trafficOfficer.TOSName + ", " + officer.ToInit + " (" + officerGroups[officer.OfGrIntNo].OfGrCode + "~" + officer.ToNo + ")";
                this.dllOfficer.Items.Add(item);
            }
            reader.Close();

            item = new ListItem();
            item.Value = "0";
            item.Text = (string)GetLocalResourceObject("listOptions.Item");
            this.dllOfficer.Items.Insert(0, item);
            this.dllOfficer.SelectedIndex = 0;
        }

        void CaptureErrorMessageForSection56(int notIntNo)
        {
            if (notIntNo == -1)
            {

                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text6"));

            }
            else if (notIntNo == -2)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text7"));

            }
            else if (notIntNo == -3)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text8"));

            }
            else if (notIntNo == -4)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text9"));

            }
            else if (notIntNo == -5)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text10"));

            }
            else if (notIntNo == -6)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text11"));

            }
            else if (notIntNo == -7)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text12"));

            }
            else if (notIntNo == -8)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text13"));

            }
            else if (notIntNo == -9)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text14"));

            }
            else if (notIntNo == -10)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text15"));

            }
            else if (notIntNo == -11)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text16"));

            }
            else if (notIntNo == -12)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text17"));

            }
            else if (notIntNo == -13)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text18"));

            }
            else if (notIntNo == -14)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text19"));

            }
            else if (notIntNo == -15)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text20"));

            }
            else if (notIntNo == -16)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text21"));

            }
            else if (notIntNo == -17)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text22"));

            }
            else if (notIntNo == -18)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text23"));

            }
            else if (notIntNo == -19)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text24"));

            }
            else if (notIntNo == -20)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text25"));

            }
            else if (notIntNo == -21)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text26"));

            }
            else if (notIntNo == -22)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text27"));

            }
            else if (notIntNo == -103 || notIntNo == -104)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text28"));

            }
            else if (notIntNo == -30)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text29"));

            }
            //Added 2010-10-13  ,update status = ImportedFromDMS in AartobmDocument
            else if (notIntNo == -36)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text30"));

            }
            else if (notIntNo == -27)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text31"));

            }
            else if (notIntNo == -100)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text32"), this.txtNoticeNumber.Text);
            }
            else if (notIntNo == -101)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text33"));

            }
            else if (notIntNo == -102)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text34"));

            }
            else if (notIntNo == -110)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text35"));

            }
            else if (notIntNo == -130)
            {
                this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text36"));

            }
            else if (notIntNo == -150)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text37"), this.txtNoticeNumber.Text);

            }
            else if (notIntNo == -151)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text38"), this.txtNoticeNumber.Text);
            }
            else
            {
                if (notIntNo < 0)
                {
                    this.lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text39") + notIntNo.ToString());

                }
            }
        }

        void CaptureErrorMessageForSection341(int notIntNo)
        {
            if (notIntNo == -1)
            {

                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text40"));

            }
            else if (notIntNo == -2)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text41"));

            }
            else if (notIntNo == -3)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text42"));

            }
            else if (notIntNo == -4)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text43"));

            }
            else if (notIntNo == -5)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text44"));

            }
            else if (notIntNo == -6)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text45"));

            }
            else if (notIntNo == -30)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text46"));

            }
            else if (notIntNo == -50)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text47"));

            }
            else if (notIntNo == -51)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text48"));

            }
            //Added 2010-10-13  ,update status = ImportedFromDMS in AartobmDocument
            else if (notIntNo == -16)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text49"));

            }
            else if (notIntNo == -100)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text50"), this.txtNoticeNumber.Text);
            }
            else if (notIntNo == -103 || notIntNo == -104)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text51"));

            }
            else if (notIntNo == -105)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text18"));
            }
            else if (notIntNo == -150)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text52"), this.txtNoticeNumber.Text);

            }
            else if (notIntNo == -151)
            {
                lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text53"), this.txtNoticeNumber.Text);
            }
            else
            {
                if (notIntNo < 0)
                {
                    lblError.Text = String.Format((string)GetLocalResourceObject("lblError.Text54") + notIntNo.ToString());

                }
            }
        }

        bool ValidateInput()
        {
            string patten = @"^[0-9]*\.?[0-9]*$";// "^[0-9]*[1-9][0-9]*$";

            bool validated = true;
            StringBuilder errorMessage = new StringBuilder();

            errorMessage.Append("<ul>");

            if (this.ddlFileType.SelectedValue == "")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text55")).Append("</li>");
                if (validated) validated = false;
            }

            if (this.textId.Text == "")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text56")).Append("</li>");
                if (validated) validated = false;
            }
            else
            {
                if (Validation.ValidateIdNumber(this.textId.Text, "RSA"))
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text57")).Append("</li>");
                    validated = false;
                }
            }
            if (this.txtSurname.Text == "")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text58")).Append("</li>");
                validated = false; ;
            }
            if (this.txtForenname.Text == "")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text59")).Append("</li>");
                if (validated) validated = false;
            }
            if (this.txtNoticeNumber.Text == "")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text60")).Append("</li>");
                validated = false; ;
            }
            else
            {
                if (Validation.GetInstance(this.connectionString).ValidateTicketNumber(this.txtNoticeNumber.Text, "cdv"))
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text61")).Append("</li>");
                    validated = false;
                }
            }

            if (this.dllCourt.SelectedValue == "0")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text73")).Append("</li>");
                validated = false;
            }
            if (this.dllOfficer.SelectedValue == "0")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text74")).Append("</li>");
                validated = false;
            }
            if (this.txtOffenceDate.Text == "")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text75")).Append("</li>");
                validated = false;
            }

            if (this.ddlFileType.SelectedValue.StartsWith("M"))
            {
                if (this.dllCourtRoom.SelectedValue == "0")
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text76")).Append("</li>");
                    validated = false;
                }
                if (this.dllCourtDate.SelectedValue == "0")
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text77")).Append("</li>");
                    validated = false;
                }
            }

            if (this.ddlOffenceCode1.SelectedValue == "")
            {
                errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text62")).Append("</li>");
                if (validated) validated = false;
            }
            else
            {
                if (this.ddlOffenceCode1.SelectedValue.IndexOf("~Y") >= 0)
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text63")).Append("</li>");
                    if (validated) validated = false;
                }
                else
                {
                    if (!Regex.IsMatch(this.txtFineAmount1.Text, patten))
                    {
                        errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text64")).Append("</li>");
                        if (validated) validated = false;
                    }

                }
            }
            if (this.ddlOffenceCode2.SelectedValue != "")
            {
                if (CompareOffenceCode(this.ddlOffenceCode1.SelectedValue, this.ddlOffenceCode2.SelectedValue))
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text65")).Append("</li>");
                    if (validated) validated = false;
                }

                if (this.ddlOffenceCode2.SelectedValue.IndexOf("~Y") >= 0)
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text66")).Append("</li>");
                    if (validated) validated = false;
                }
                else
                {
                    if (!Regex.IsMatch(this.txtFineAmount2.Text, patten))
                    {
                        errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text67")).Append("</li>");
                        if (validated) validated = false;
                    }
                }
            }

            if (this.ddlOffenceCode3.SelectedValue != "")
            {
                if (CompareOffenceCode(this.ddlOffenceCode2.SelectedValue, this.ddlOffenceCode3.SelectedValue))
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text68")).Append("</li>");
                    if (validated) validated = false;
                }

                if (CompareOffenceCode(this.ddlOffenceCode1.SelectedValue, this.ddlOffenceCode3.SelectedValue))
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text69")).Append("</li>");
                    if (validated) validated = false;
                }

                if (this.ddlOffenceCode3.SelectedValue.IndexOf("~Y") >= 0)
                {
                    errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text70")).Append("</li>");
                    if (validated) validated = false;
                }
                else
                {
                    if (!Regex.IsMatch(this.txtFineAmount3.Text, patten))
                    {
                        errorMessage.Append("<li>").Append((string)GetLocalResourceObject("lblError.Text71")).Append("</li>");
                        if (validated) validated = false;
                    }
                }
            }

            if (validated == false)
            {
                errorMessage.Append("</ul>");
                this.lblError.Text = errorMessage.ToString();
            }
            return validated;
        }

        bool CompareOffenceCode(string offenceCode1, string offenceCode2)
        {
            try
            {
                string[] ocArry1 = offenceCode1.Split('~');
                string[] ocArry2 = offenceCode2.Split('~');

                if (ocArry1.Length > 1 && ocArry2.Length > 1)
                {
                    return ocArry1[0] == ocArry2[0];
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        int SaveHandwrittenOffenceByMinimalCapture(out string ticketNo)
        {
            try
            {
                int notIntNo = 0;
                ticketNo = this.txtNoticeNumber.Text.Trim();// Heidi 2014-06-11 Remove extra Spaces for fixing Performance issues(5299)
                int authIntNo = Convert.ToInt32(Session["autIntNo"].ToString());
                string officerNo = this.dllOfficer.SelectedValue; //Session["OfficerNo"].ToString();
                string filmNo = string.Empty;

                Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(this.connectionString);
                AuthorityDetails authDetail = autList.GetAuthorityDetails(authIntNo);

                // Jake 2012-04-25 
                // User must enter number exactly as required. e.g. nn/mmmmmm/aaa/12345
                //if (ticketNo.IndexOf("/") >= 0 || ticketNo.IndexOf("-") >= 0)
                //{
                //    ticketNo = ticketNo.Replace("-", "/");
                //}
                //else
                //{
                //    if (ticketNo.Length <= 16)
                //    {
                //        ticketNo = ticketNo.Insert(2, "/").Insert(8, "/").Insert(12, "/");
                //    }
                //    else
                //    {
                //        ticketNo = ticketNo.Insert(2, "/").Insert(9, "/").Insert(13, "/");
                //    }
                //    // filmNo = "SCH" + ticketNo.Substring(ticketNo.Length - 5);
                //}
                filmNo = authDetail == null ? "" : authDetail.AutCode.Trim() + "_SCH_" + ticketNo.Substring(ticketNo.LastIndexOf("/") + 1);

                string frameNo = "0001";
                string idNumber = this.textId.Text;
                string surName = this.txtSurname.Text;
                string foreName = this.txtForenname.Text;
                string offenceCode1 = this.ddlOffenceCode1.SelectedValue.Split('~')[0];
                string offenceCode2 = this.ddlOffenceCode2.SelectedValue.Split('~')[0];
                string offenceCode3 = this.ddlOffenceCode3.SelectedValue.Split('~')[0];
                string offenceDescr2 = string.Empty;
                string offenceDescr3 = string.Empty;
                string offenceDescr1 = this.ddlOffenceCode1.Items[this.ddlOffenceCode1.SelectedIndex].Text;
                if (!String.IsNullOrEmpty(offenceCode2))
                    offenceDescr2 = this.ddlOffenceCode2.Items[this.ddlOffenceCode2.SelectedIndex].Text;
                if (!String.IsNullOrEmpty(offenceCode3))
                    offenceDescr3 = this.ddlOffenceCode3.Items[this.ddlOffenceCode3.SelectedIndex].Text;

                float fineAmount1 = 0;
                float fineAmount2 = 0;
                float fineAmount3 = 0;
                float.TryParse(this.txtFineAmount1.Text, out fineAmount1);
                float.TryParse(this.txtFineAmount2.Text, out fineAmount2);
                float.TryParse(this.txtFineAmount3.Text, out fineAmount3);

                string regNo = this.txtRegNo.Text;
                DateTime notOffenceDate = Convert.ToDateTime(this.txtOffenceDate.Text);
                int crtIntNo = Convert.ToInt32(this.dllCourt.SelectedValue);
                int crtRIntNo = Convert.ToInt32(this.dllCourtRoom.SelectedValue);

                //int toIntNo = Convert.ToInt32(this.dllOfficer.SelectedValue);
                int originator = Convert.ToInt32(this.dllOriginator.SelectedValue);
                //Jake 2014-05-26 added code to set NstCode for Moblie 341 and 56
                //value:H~P->S341 ; H~M->M341; M~P->S56; M~M ->M56
                string nstCode = "";
                CashReceiptDB cashReceiptDB = new CashReceiptDB(this.connectionString);
                if (this.ddlFileType.SelectedValue.StartsWith("H"))
                {

                    if (this.ddlFileType.SelectedValue == "H~M")
                    {
                        nstCode = "M341";
                    }
                    else
                    {
                        nstCode = "H341";
                    }
                    //Jerry 2013-02-07 add
                    DateTime notPaymentDate = DateTime.Now;
                    string aR_2500 = Get1stNoticeForS341HWO(authIntNo);
                    if (aR_2500.Equals("Y", StringComparison.OrdinalIgnoreCase))
                    {
                        notPaymentDate = notPaymentDate.AddDays(GetNotOffenceDateNotPaymentDate(authIntNo));
                    }

                    cashReceiptDB.MinimalCaptureForSection341(authIntNo,
                        filmNo, frameNo, ticketNo, idNumber, surName, foreName,
                        officerNo, offenceCode1, offenceCode2, offenceCode3,
                        offenceDescr1, offenceDescr2, offenceDescr3, fineAmount1, fineAmount2, fineAmount3, "", loginUser, out notIntNo, nstCode,
                        false, notPaymentDate, null, crtIntNo, regNo, notOffenceDate);  //jake 2014-12-18 added these 3 parameter for package 5383);
                }
                else if (this.ddlFileType.SelectedValue.StartsWith("M"))
                {
                    if (this.ddlFileType.SelectedValue == "M~M")
                    {
                        nstCode = "M56";
                    }
                    else
                    {
                        nstCode = "H56";
                    }
                    DateTime courtDate = Convert.ToDateTime(this.dllCourtDate.SelectedValue);
                    cashReceiptDB.MinimalCaptureForSection56(authIntNo,
                         filmNo, frameNo, ticketNo, idNumber, surName, foreName,
                         officerNo, offenceCode1, offenceCode2, offenceCode3,
                         offenceDescr1, offenceDescr2, offenceDescr3, fineAmount1, fineAmount2, fineAmount3, "", loginUser, out notIntNo, nstCode,
                        false, null, crtIntNo, crtRIntNo, courtDate, regNo, notOffenceDate);
                }

                return notIntNo;
            }
            catch (Exception ex)
            { throw ex; }
        }

        float GetOffenceFineAmount(string offenceCode)
        {
            OffenceQuery query = new OffenceQuery();
            query.Append(DAL.Entities.OffenceColumn.OffCode, offenceCode);

            SIL.AARTO.DAL.Entities.Offence offence = new OffenceService().Find(query as IFilterParameterCollection).FirstOrDefault();
            if (offence != null)
            {
                OffenceFineQuery ofQuery = new OffenceFineQuery();
                ofQuery.Append(OffenceFineColumn.AutIntNo, Session["autIntNo"].ToString());
                ofQuery.Append(OffenceFineColumn.OffIntNo, offence.OffIntNo.ToString());
                SIL.AARTO.DAL.Entities.OffenceFine offenceFine = new OffenceFineService().Find(ofQuery as IFilterParameterCollection).FirstOrDefault();
                if (offenceFine != null)
                {
                    return offenceFine.OfFineAmount;
                }
            }

            return 0;
        }

        /*
        //not in use and not finished
        void PopulateNoticeDetail(string notTicketNo)
        {
            Notice notice = notService.GetByNotTicketNo(notTicketNo).FirstOrDefault();
            if (notice == null)
                return;

            if (notice.NotFilmType == "H")
            {
                this.ddlFileType.SelectedValue = "H~P";
            }
            else if (notice.NotFilmType == "M")
            {
                this.ddlFileType.SelectedValue = "M~P";
            }

            Driver drv = drvService.GetByNotIntNo(notice.NotIntNo).FirstOrDefault();
            if (drv != null)
            {
                this.textId.Text = drv.DrvIdNumber;
                this.txtForenname.Text = drv.DrvForenames;
                this.txtSurname.Text = drv.DrvSurname;
            }

            this.txtRegNo.Text = notice.NotRegNo;
            SIL.AARTO.DAL.Entities.Court court = crtService.GetByCrtNo(notice.NotCourtNo);
            if (court != null)
            {
                this.dllCourt.SelectedValue = court.CrtIntNo.ToString();
                dllCourt_SelectedIndexChanged(null, null);
            }

            this.dllOfficer.SelectedValue = notice.NotOfficerNo;

        }
        */
        void RegisterScript()
        {
            //$('#txtOffenceDate').datepicker({ dateFormat: 'yy-mm-dd', dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, maxDate: new Date() });
            string js = "$(document).ready(function(){" +
                        "   $('#txtOffenceDate').datepicker({ dateFormat: 'yy-mm-dd', dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, maxDate: new Date() }); })";

            //ClientScript.RegisterClientScriptBlock(this.GetType(), "message", "$(document).ready(function(){$.blockUI({ message: $('#divNoMatchTicket')});})", true);
            ClientScript.RegisterClientScriptBlock(this.GetType(), "message", js, true);
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            this.txtFineAmount1.Text = this.hidFineAmount1.Value;
            this.txtFineAmount2.Text = this.hidFineAmount2.Value;
            this.txtFineAmount3.Text = this.hidFineAmount3.Value;
            this.btnManually.Visible = true;
            this.btnConfirm.Visible = false;
            this.divConfirm.Visible = false;
            this.divMain.Visible = true;
            this.lblError.Text = "";
            this.btnManually.Visible = true;
            this.btnCancelFineAmount.Visible = true;
        }

        protected void btnCancelCapture_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CashReceiptTraffic.aspx");
        }

        protected void ddlFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool visible = false;
            if (this.ddlFileType.SelectedValue.StartsWith("H"))
            {
                //this.ddlOffenceCode3.Visible = false;

                //this.txtFineAmount3.Visible = false;
                //this.labFineAmount3.Visible = false;
                //this.labOffenceCode3.Visible = false;
                //this.labFinAmount3_Ext.Visible = false;
                //this.labOffenceCode3_Ext.Visible = false;
                this.ddlOffenceCode3.SelectedValue = "";
                this.txtFineAmount3.Text = "";
                visible = false;
            }
            else
            {
                //this.ddlOffenceCode3.Visible = true;
                //this.txtFineAmount3.Visible = true;
                //this.labFineAmount3.Visible = true;
                //this.labOffenceCode3.Visible = true;
                //this.labFinAmount3_Ext.Visible = true;
                //this.labOffenceCode3_Ext.Visible = true;
                visible = true;
            }

            SetControlVisible(visible);
        }


        void SetControlVisible(bool visible)
        {
            this.ddlOffenceCode3.Visible = visible;
            this.txtFineAmount3.Visible = visible;
            this.labFineAmount3.Visible = visible;
            this.labOffenceCode3.Visible = visible;
            this.labFinAmount3_Ext.Visible = visible;
            this.labOffenceCode3_Ext.Visible = visible;

            lblCourtRoom.Visible = visible;
            lblCourtDate.Visible = visible;

            dllCourtRoom.Visible = visible;
            dllCourtDate.Visible = visible;
        }

        //Jerry 2013-02-07
        private string Get1stNoticeForS341HWO(int authIntNo)
        {
            AuthorityRulesDetails arDetails2500 = new AuthorityRulesDetails();
            arDetails2500.AutIntNo = authIntNo;
            arDetails2500.ARCode = "2500";
            arDetails2500.LastUser = loginUser;

            DefaultAuthRules authRule2500 = new DefaultAuthRules(arDetails2500, this.connectionString);
            KeyValuePair<int, string> value2500 = authRule2500.SetDefaultAuthRule();
            return value2500.Value;
        }

        //Jerry 2013-02-07 add
        private int GetNotOffenceDateNotPaymentDate(int authIntNo)
        {
            DateRulesDetails dateRule = new DateRulesDetails();
            dateRule.AutIntNo = authIntNo;
            dateRule.DtRStartDate = "NotOffenceDate";
            dateRule.DtREndDate = "NotPaymentDate";
            dateRule.LastUser = loginUser;
            DefaultDateRules rule = new DefaultDateRules(dateRule, this.connectionString);
            return rule.SetDefaultDateRule();
        }

        protected void dllCourt_SelectedIndexChanged(object sender, EventArgs e)
        {
            int crtIntNo = int.Parse(this.dllCourt.SelectedValue);
            PopulateCourtRoom(crtIntNo);

            string notFilmType;
            int crtRIntNo = int.Parse(this.dllCourtRoom.SelectedValue);
            notFilmType = this.ddlFileType.SelectedValue.StartsWith("H") ? "H" : "M";
            PopulateCourtDate(crtRIntNo, notFilmType);
        }

        protected void dllCourtRoom_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }




    public static class JsonSerializer
    {
        private static JavaScriptSerializer ser = new JavaScriptSerializer();
        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string JsonSerialize(object obj)
        {
            return ser.Serialize(obj);
        }
        /// <summary>
        /// Deserialize
        /// </summary>
        /// <param name="json"></param>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static object JsonDeserialize(string json, Type targetType)
        {
            return ser.DeserializeObject(json);
            //applicable to .Net Framework 4.0
            //return ser.Deserialize(json, targetType);
        }
    }




}