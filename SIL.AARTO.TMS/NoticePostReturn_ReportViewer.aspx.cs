using System;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Stalberg.TMS.Data.Datasets;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents the end of day cash box balancing page
    /// </summary>
    public partial class NoticePostReturn_ReportViewer : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private ReportDocument _reportDoc = new ReportDocument();
        private int autIntNo = 0;
        private const string DATE_FORMAT = "yyyy-MM-dd";
        private int userIntNo = 0;
        //private string thisPage = "Notice Post Return Report Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "NoticePostReturn_ReportViewer.aspx";
        protected string loginUser = string.Empty;
        private string printFile = string.Empty;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");
            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            this.userIntNo = int.Parse(Session["userIntNo"].ToString());

            // Get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            int userAccessLevel = userDetails.UserAccessLevel;
            userDetails = (UserDetails)Session["userDetails"];
            this.loginUser = userDetails.UserLoginName;
            //int 
            this.autIntNo = Convert.ToInt32(Session["autIntNo"]);

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            // Setup the report
            AuthReportNameDB arn = new AuthReportNameDB(connectionString);
            string reportPage = arn.GetAuthReportName(autIntNo, "NoticePostReturn");
            if (reportPage.Equals(""))
            {
                //int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticePostReturn.rpt", "NoticePostReturn", "system");
                int arnIntNo = arn.AddAuthReportName(autIntNo, "NoticePostReturn.rpt", "NoticePostReturn", "system", string.Empty);
                reportPage = "NoticePostReturn.rpt";
            }

            string reportPath = Server.MapPath("reports/" + reportPage);


            //****************************************************
            //SD:  20081120 - check that report actually exists
            string templatePath = string.Empty;
            string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "NoticePostReturn");
            if (!File.Exists(reportPath))
            {
                string error = string.Format((string)GetLocalResourceObject("error"), reportPage);
                string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                Response.Redirect(errorURL);
                return;
            }
            else if (!sTemplate.Equals(""))
            {

                templatePath = Server.MapPath("Templates/" + sTemplate);

                if (!File.Exists(templatePath))
                {
                    string error = string.Format((string)GetLocalResourceObject("error1"), sTemplate);
                    string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, (string)GetLocalResourceObject("thisPage"), thisPageURL);

                    Response.Redirect(errorURL);
                    return;
                }
            }

            //****************************************************

            ReportDocument report = new ReportDocument();
            report.Load(reportPath);

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = new SqlCommand("GetNoticePostReturnByValues", new SqlConnection(this.connectionString));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            DateTime dtHold;
            da.SelectCommand.Parameters.Add("@TicketNo", SqlDbType.VarChar, 50).Value = (Request.QueryString["TicketNo"] == string.Empty ? "%" : Request.QueryString["TicketNo"].ToString() + "%");
            da.SelectCommand.Parameters.Add("@RegNo", SqlDbType.VarChar, 20).Value = (Request.QueryString["RegNo"] == string.Empty ? "%" : Request.QueryString["RegNo"].ToString() + "%");
            da.SelectCommand.Parameters.Add("@Name", SqlDbType.VarChar, 20).Value = (Request.QueryString["Name"] == string.Empty ? "%" : Request.QueryString["Name"].ToString() + "%");
            da.SelectCommand.Parameters.Add("@BoxNo", SqlDbType.VarChar, 10).Value = (Request.QueryString["BoxNo"] == string.Empty ? "%" : Request.QueryString["BoxNo"].ToString() + "%");
            da.SelectCommand.Parameters.Add("@Location", SqlDbType.VarChar, 25).Value = (Request.QueryString["Location"] == string.Empty ? "%" : Request.QueryString["Location"].ToString() + "%");
            string sHold = (Request.QueryString["Date"] == string.Empty ? "1900-01-01" : Request.QueryString["Date"].ToString());
            DateTime.TryParse(sHold, out dtHold);
            da.SelectCommand.Parameters.Add("@InDate", SqlDbType.DateTime).Value = dtHold;

            dsNoticePostReturn ds = new dsNoticePostReturn();
            ds.DataSetName = "dsNoticePostReturn";
            da.Fill(ds);
            da.Dispose();

            report.SetDataSource(ds.Tables[1]);
            ds.Dispose();
            // Export the pdf file
            MemoryStream ms = new MemoryStream();
            ms = (MemoryStream)report.ExportToStream(ExportFormatType.PortableDocFormat);
            report.Dispose();

            // Flush the PDF into the response stream
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(ms.ToArray());
            Response.End();

        }
    }
}