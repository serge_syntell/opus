using System;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.ReportWriter;
using ceTe.DynamicPDF.ReportWriter.ReportElements;
using ceTe.DynamicPDF.ReportWriter.Data;
using ceTe.DynamicPDF.Merger;
using SIL.AARTO.BLL.Utility.PrintFile;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility.Printing;

namespace Stalberg.TMS
{
    /// <summary>
    /// Represents  viewer page for the RepresentationInsufficientDetails letter
    /// </summary>
    public partial class RepresentationInsufficientDetails_LetterViewer : DplxWebForm
    {
        // Fields
        private string connectionString = string.Empty;
        private int autIntNo = 0;
        private string thisPage = "Representation Insufficient Details Letter Viewer";
        protected string styleSheet;
        protected string backgroundImage;
        protected string keywords = String.Empty;
        protected string title = String.Empty;
        protected string description = String.Empty;
        protected string thisPageURL = "RepresentationInsufficientDetails_LetterViewer.aspx";
        protected string loginUser = string.Empty;

        //2011-10-18 jerry add
        private string printFileName = string.Empty;
        private bool printFlag = false;

        //2012-02-15 jerry add
        private int repIntNo = -1;
        private bool isSummons = false;

        #region 2012-02-15 jerry disabled for backup
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    this.connectionString = Application["constr"].ToString();

        //    // Get user info from session variable
        //    if (Session["userDetails"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    if (Session["userIntNo"] == null)
        //        Server.Transfer("Login.aspx?Login=invalid");

        //    // Set domain specific variables
        //    General gen = new General();
        //    backgroundImage = gen.SetBackground(Session["drBackground"]);
        //    styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
        //    title = gen.SetTitle(Session["drTitle"]);

        //    //2011-10-18 jerry add
        //    if (Request.QueryString["PrintFileName"] != null && Request.QueryString["PrintFlag"] != null)
        //    {
        //        this.printFileName = Request.QueryString["PrintFileName"].ToString();
        //        string tempPrintFlag = Request.QueryString["PrintFlag"].ToString();
        //        this.printFlag = tempPrintFlag == "N" ? false : true;
        //    }
        //    else
        //    {
        //        if (Request.QueryString["RepIntNo"] == null)
        //        {
        //            Response.Redirect("Login.aspx");
        //            return;
        //        }
        //    }

        //    //get user details
        //    Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
        //    Stalberg.TMS.UserDetails userDetails = new UserDetails();
        //    userDetails = (UserDetails)Session["userDetails"];
        //    autIntNo = Convert.ToInt32(Session["autIntNo"]);
        //    byte[] buffer = null;
        //    byte[] bufferTemplate = null;

        //    // Setup the report
        //    AuthReportNameDB arn = new AuthReportNameDB(connectionString);
        //    string reportPage = arn.GetAuthReportName(this.autIntNo, "InsufficientDetails");
        //    if (reportPage.Equals(string.Empty))
        //    {
        //        arn.AddAuthReportName(autIntNo, "RepresentationInsufficientDetails_PL.dplx", "RepresentationInsufficientDetails", "system", string.Empty);
        //        reportPage = "RepresentationInsufficientDetails_PL.dplx";
        //    }

        //    string sTemplate = arn.GetAuthReportNameTemplate(this.autIntNo, "InsufficientDetails");
        //    string path = Server.MapPath("reports/" + reportPage);

        //    //****************************************************
        //    //SD:  20081122 - check that report actually exists
        //    string templatePath = string.Empty;
        //    if (!File.Exists(path))
        //    {
        //        string error = "Report " + reportPage + " does not exist";
        //        string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //        Response.Redirect(errorURL);
        //        return;
        //    }
        //    else if (!sTemplate.Equals(""))
        //    {

        //        templatePath = Server.MapPath("Templates/" + sTemplate);

        //        if (!File.Exists(templatePath))
        //        {
        //            string error = "Report template " + sTemplate + " does not exist";
        //            string errorURL = string.Format("Error.aspx?error={0}&errorPage={1}&errorPageURL={2}", error, thisPage, thisPageURL);

        //            Response.Redirect(errorURL);
        //            return;
        //        }
        //    }

        //    //****************************************************

        //    DocumentLayout doc = new DocumentLayout(path);
        //    Query query = (Query)doc.GetQueryById("Query");
        //    query.ConnectionString = this.connectionString;

        //    //2011-10-18 jerry change
        //    if (string.IsNullOrEmpty(this.printFileName))
        //    {
        //        ParameterDictionary parameters = new ParameterDictionary();
        //        parameters.Add("RepIntNo", Request.QueryString["RepIntNo"].ToString());
        //        parameters.Add("IsSummons", Request.QueryString["IsSummons"].ToString().Equals("0") ? false : true);

        //        if (!sTemplate.Equals("") && sTemplate.ToLower().IndexOf(".pdf") == -1)
        //        {
        //            DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
        //            Query queryTemplate = (Query)template.GetQueryById("Query");
        //            queryTemplate.ConnectionString = this.connectionString;
        //            ParameterDictionary parametersTemplate = new ParameterDictionary();

        //            Document reportTemplate = template.Run(parametersTemplate);
        //            bufferTemplate = reportTemplate.Draw();
        //        }

        //        Document report = doc.Run(parameters);
        //        ImportedPageArea importedPage;

        //        if (!sTemplate.Equals(""))
        //        {
        //            if (sTemplate.ToLower().IndexOf(".dplx") > 0)
        //            {
        //                PdfDocument pdf = new PdfDocument(bufferTemplate);
        //                PdfPage page = pdf.Pages[0];
        //                importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
        //            }
        //            else
        //            {
        //                //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1 , 0.0F, 0.0F, 1.0F);
        //                importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //            }

        //            ceTe.DynamicPDF.Page rptPage = report.Pages[0];
        //            rptPage.Elements.Insert(0, importedPage);
        //        }

        //        buffer = report.Draw();
        //    }
        //    else
        //    {
        //        RepresentationDB represent = new RepresentationDB(this.connectionString);
        //        SqlDataReader reader = represent.GetRepInfoByPrintFile(this.printFileName, this.printFlag);

        //        MergeDocument merge = new MergeDocument();
        //        Document report;

        //        while (reader.Read())
        //        {
        //            ParameterDictionary parameters = new ParameterDictionary();
        //            parameters.Add("RepIntNo", Convert.ToInt32(reader["RepIntNo"]));
        //            parameters.Add("IsSummons", Convert.ToBoolean(reader["IsSummons"]));

        //            if (!sTemplate.Equals("") && sTemplate.ToLower().IndexOf(".pdf") == -1)
        //            {
        //                DocumentLayout template = new DocumentLayout(Server.MapPath("Templates/" + sTemplate));
        //                Query queryTemplate = (Query)template.GetQueryById("Query");
        //                queryTemplate.ConnectionString = this.connectionString;
        //                ParameterDictionary parametersTemplate = new ParameterDictionary();

        //                Document reportTemplate = template.Run(parametersTemplate);
        //                bufferTemplate = reportTemplate.Draw();
        //            }

        //            report = doc.Run(parameters);
        //            ImportedPageArea importedPage;

        //            if (!sTemplate.Equals(""))
        //            {
        //                if (sTemplate.ToLower().IndexOf(".dplx") > 0)
        //                {
        //                    PdfDocument pdf = new PdfDocument(bufferTemplate);
        //                    PdfPage page = pdf.Pages[0];
        //                    importedPage = new ImportedPageArea(page, 0.0F, 0.0F);
        //                }
        //                else
        //                {
        //                    //importedPage = new ImportedPageArea(Server.MapPath("reports/" + sTemplate), 1 , 0.0F, 0.0F, 1.0F);
        //                    importedPage = new ImportedPageArea(Server.MapPath("Templates/" + sTemplate), 1, 0.0F, 0.0F, 1.0F);
        //                }

        //                ceTe.DynamicPDF.Page rptPage = report.Pages[0];
        //                rptPage.Elements.Insert(0, importedPage);
        //            }
        //            buffer = report.Draw();
        //            merge.Append(new PdfDocument(buffer));

        //            buffer = null;
        //            bufferTemplate = null;
        //        }

        //        buffer = merge.Draw();
        //    }

            
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/pdf";
        //    Response.BinaryWrite(buffer);
        //    Response.End();

        //}

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            // Get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            // Set domain specific variables
            General gen = new General();
            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            //2011-10-18 jerry add
            if (Request.QueryString["PrintFileName"] != null && Request.QueryString["PrintFlag"] != null)
            {
                this.printFileName = Request.QueryString["PrintFileName"].ToString();
                string tempPrintFlag = Request.QueryString["PrintFlag"].ToString();
                this.printFlag = tempPrintFlag == "N" ? false : true;
            }
            else
            {
                if (Request.QueryString["RepIntNo"] == null)
                {
                    Response.Redirect("Login.aspx");
                    return;
                }
                this.repIntNo = int.Parse(Request.QueryString["RepIntNo"].ToString());
                this.isSummons = Request.QueryString["IsSummons"].ToString().Equals("0") ? false : true;
            }

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(this.connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();
            userDetails = (UserDetails)Session["userDetails"];
            autIntNo = Convert.ToInt32(Session["autIntNo"]);

            //jerry 2012-02-15, use module to create print pdf file
            PrintFileProcess process = new PrintFileProcess(this.connectionString, userDetails.UserLoginName);
            process.BuildPrintFile(new PrintFileModuleRepresentationInsufficientDetailsLetter(printFlag, repIntNo, this.isSummons), this.autIntNo, this.printFileName);
            if (Request.QueryString["printType"] != null && Request.QueryString["printType"] == "PrintRepresentationLetter")
            {
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                string lastuser = userDetails != null ? userDetails.UserLoginName : "";
                int _autIntNo = Session["printAutIntNo"] == null ? autIntNo : Convert.ToInt32(Session["printAutIntNo"]);
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(_autIntNo, lastuser, PunchStatisticsTranTypeList.PrintRepresentationLetter, PunchAction.Change);

            }

        }
    }
}