﻿<%@ Page Language="c#" AutoEventWireup="false"
    Inherits="Stalberg.TMS.ViewHistoricalSummons_Search" Codebehind="ViewHistoricalSummons_Search.aspx.cs" %>
<%--<%@ Register Assembly="Dojo.Net" Namespace="Dojo.Net" TagPrefix="dojo" %>--%>
<%@ Register Src="TicketNumberSearch.ascx" TagName="TicketNumberSearch" TagPrefix="uc1" %>
<%@ Register Src="EasyPayNumberSearch.ascx" TagName="EasyPayNumberSearch" TagPrefix="uc2" %>

<%@ Register Src="_Header.ascx" TagName="Header" TagPrefix="hdr1" %>
<%@ Register Src="_Tags.ascx" TagName="Tag" TagPrefix="tg1" %>
<%@ Register Src="NoticeNoLookup.ascx" TagName="NoticeNoLookup" TagPrefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>
        <%=_title %>
    </title>
    <link href="<%= _styleSheet %>" type="text/css" rel="stylesheet" />
    <meta content="<%= _description %>" name="Description" />
    <meta content="<%= _keywords %>" name="Keywords" />
    <script type="text/javascript">      
//20090417 tf  --Set TextBoxs status enable or diabled  and default button.

////enter event---------------------------------------------------------------------
function EnterHandler(evt){
    if(evt.keyCode==13){
        PressEnterKey(evt);
    }
}

function PressEnterKey(evt)
{
     evt.cancelBubble = true;
     evt.returnValue = false;  

    if(bSearch == true) 
    {
        clickControl(document.getElementById("btnSearch"));
    }
    else
        clickControl(document.getElementById("noticeNoLookup_btnSearch"));
}

if(document.addEventListener){
    document.addEventListener("keypress",EnterHandler, true);
}else
{
    document.attachEvent("onkeypress",EnterHandler);
}

////----------------------------------------------------------------------------


////Set textbox status-----------------------------------------------------------
var bSearch = true;
function SetControlsStatus(control)
{
    var groupname = "mypanel1";
    if(control.parentNode.id.indexOf("mypanel2") >= 0)
        groupname = "mypanel2";

    if(groupname == "mypanel1")
    bSearch = true;
    else if(groupname == "mypanel2")
    {
        bSearch = false;
    }
    SetSearchCondition(control, groupname);
    ResetStatus();
} 

function ActiveOneTextBox(control, groupname)
{      
    var   controls= document.getElementsByTagName("input")   
    for   (var   i=0;   i < controls.length;  i++)   
    {
        if(control != controls[i].parentNode && controls[i].parentNode.id.indexOf(groupname) >= 0)
        {
            controls[i].className = "DisableTextBox";
        }
        else if(control == controls[i].parentNode)
        {
            controls[i].className = "Normal";
        }
    }
}

//set search condition
function SetSearchCondition(control, groupname)
{
     if(groupname == "mypanel1")
          document.getElementById("SearchCondition").value = control.parentNode.id;
    else if(groupname == "mypanel2")
          document.getElementById("noticeNoLookup_LookupCondition").value = control.parentNode.id;
}

function SetfocusOnLookup()
{
      bSearch = false;
}

function clickControl(control)
{
    if(control.onclick)
    {
        control.onclick();
    }
    else if(control.click)
    {
        control.click();
    }
    else
    {
        SetControlsStatus(control);
    }
}

function ResetStatus()
{
    if(document.getElementById("noticeNoLookup_LookupCondition").value.length > 0)
    {
        ActiveOneTextBox(document.getElementById(document.getElementById("noticeNoLookup_LookupCondition").value), "mypanel2");
    }

    if(document.getElementById("SearchCondition").value.length > 0)
    {
        ActiveOneTextBox(document.getElementById(document.getElementById("SearchCondition").value), "mypanel1");
    }
}
</script>
    <style type="text/css">
        .style1
        {
            width: 4px;
        }
    </style>
</head>
<body bottommargin="0" leftmargin="0" background="<%=_background %>" topmargin="0"
    rightmargin="0" style="text-align: center"><form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="5%">
        <tr>
            <td class="HomeHead" style="text-align: center" width="100%" colspan="2" valign="middle">
                <hdr1:Header ID="Header1" runat="server"></hdr1:Header>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" border="0" height="90%" width="100%">
    <tr><td> <asp:UpdateProgress ID="udp" runat="server">
                        <ProgressTemplate>
                            <p class="Normal" style="text-align: center;">
                                <img alt="Loading..." src="images/ig_progressIndicator.gif" /><asp:Label ID="Label2"
                                    runat="server" Text="<%$Resources:lblLoading.Text %>"></asp:Label></p>
                        </ProgressTemplate>
                    </asp:UpdateProgress></td></tr>
        <tr>
    
            <td valign="top" style="height: 49px">
                <p style="text-align: center">
                    <asp:Label ID="lblPageName" runat="server" Width="552px" CssClass="ContentHead" Text="<%$Resources:lblPageName.Text %>"></asp:Label> <asp:LinkButton ID="LinkButton2" runat="server" 
                                                PostBackUrl="~/ViewOffence_Search.aspx" 
                                                CssClass="SubContentHead" Text="<%$Resources:lblSwitchEnquiries.Text %>"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input runat="server" id="btnClose" class="NormalButtonRed" type="button" value="<%$Resources:btnClose.Text %>" onclick="window.close();" /></p>
                <p style="text-align: center">
                    &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td valign="top" style="text-align: center; width: 100%">
                &nbsp;<br />
                <p style="text-align: center">
                   <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
                        <ContentTemplate>
                            <table width="100%">
                            <tr align="center"><td>
                                <table style="width: 500px;">
                                    <tr align="center">
                                        <td>
                                            <asp:Label ID="Label4" runat="server" CssClass="NormalBold" Text="<%$Resources:lblCriteria.Text %>"></asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:Button ID="btnSearch" runat="server" CssClass="NormalButton" 
                                                OnClick="btnSearch_Click"  Text="<%$Resources:btnSearch.Text %>" Width="80px" 
                                                UseSubmitBehavior="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; width: 495px;">
                                            <asp:Label ID="lblError" runat="Server" CssClass="NormalRed" EnableViewState="false"></asp:Label>
                                        </td>
                                        <td align="right">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr align="center">
                                        <td style="height: 241px" colspan="2">
                                            <table style="vertical-align: top;">
                                                <tr style="height:24">
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" CssClass="NormalBold" Text="<%$Resources:lblRegistrationno.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 541px">
                                                        <asp:Panel ID="mypanel1RegNo" Enabled="true" runat="server" Width="215px" Height="16px">
                                                            <asp:TextBox ID="txtRegNo" runat="server" CssClass="Normal" MaxLength="30" Width="215px" TabIndex="0" onfocus="SetControlsStatus(this);"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr style="height:24">
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" CssClass="NormalBold" Text="<%$Resources:lblIDNo.Text %>"></asp:Label>
                                                    </td>
                                                    <td style="width: 541px">
                                                        <asp:Panel ID="mypanel1IDNO" Enabled="true" runat="server" Width="215px" Height="16px">
                                                            <asp:TextBox ID="txtIDNO" runat="server" CssClass="Normal" MaxLength="30" Width="215px" onfocus="SetControlsStatus(this);"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr style="height:24">
                                                    <td>
                                                        <asp:Label ID="lbNoticeNO" runat="server" CssClass="NormalBold" Text="<%$Resources:lbNoticeNO.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 541px">
                                                        <asp:Panel ID="mypanel1NoticeNO" Enabled="true" runat="server" Width="215px" Height="16px">
                                                            <asp:TextBox ID="txtNoticeNO" runat="server" CssClass="Normal" MaxLength="30" Width="215px"  onfocus="SetControlsStatus(this);"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr style="height:24">
                                                    <td>
                                                        <asp:Label ID="lbSummonsNO" runat="server" CssClass="NormalBold" Text="<%$Resources:lbSummonsNO.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 541px">
                                                        <asp:Panel ID="mypanel1SummonsNO" Enabled="true" runat="server" Width="215px" Height="16px">
                                                            <asp:TextBox ID="txtSummonsNO" runat="server" CssClass="Normal" MaxLength="30" Width="215px" onfocus="SetControlsStatus(this);"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr style="height:24">
                                                    <td>
                                                        <asp:Label ID="lbCaseNO" runat="server" CssClass="NormalBold" Text="<%$Resources:lbCaseNO.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 541px">
                                                        <asp:Panel ID="mypanel1CASENO" Enabled="true" runat="server" Width="215px" Height="16px">
                                                            <asp:TextBox ID="txtCASENO" runat="server" CssClass="Normal" MaxLength="30" Width="215px" onfocus="SetControlsStatus(this);"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr style="height:24">
                                                    <td>
                                                        <asp:Label ID="lbWOANO" runat="server" CssClass="NormalBold" Text="<%$Resources:lbWOANO.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 541px">
                                                        <asp:Panel ID="mypanel1WOANO" Enabled="true" runat="server" Width="215px" Height="16px">
                                                            <asp:TextBox ID="txtWOANO" runat="server" CssClass="Normal" MaxLength="30" Width="215px" onfocus="SetControlsStatus(this);"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr style="height:24">
                                                    <td>
                                                        <asp:Label ID="Label7" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSurname.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 541px">
                                                        <asp:Panel ID="mypanel1Name" Enabled="true" runat="server" Width="215px" Height="16px">
                                                            <asp:TextBox ID="txtInitials" runat="server" CssClass="Normal" MaxLength="10" Width="43px" onfocus="SetControlsStatus(this);"></asp:TextBox>&nbsp;
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="Normal" MaxLength="30" Width="160px" onfocus="SetControlsStatus(this);"></asp:TextBox>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr style="height:40">
                                                    <td style="width: 250px">
                                                        <asp:Label ID="Label5" runat="server" CssClass="NormalBold" Text="<%$Resources:lblSince.Text %>"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 541px">

                                                        <asp:TextBox runat="server" ID="dtpSince" CssClass="Normal" Height="20px" autocomplete="off" UseSubmitBehavior="False"/>
                                                        <cc1:CalendarExtender ID="DateCalendar" runat="server" TargetControlID="dtpSince" Format="yyyy-MM-dd">
                                                        </cc1:CalendarExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                            ControlToValidate="dtpSince" CssClass="NormalRed" 
                                                            ErrorMessage="<%$Resources:lblValidDate.Text %>" 
                                                            ValidationExpression="(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"></asp:RegularExpressionValidator>
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <uc3:NoticeNoLookup ID="noticeNoLookup" OnNoticeSelected="NoticeLookup" runat="server" />
                                        </td>
                                    </tr>
                             
                                </table>
                                <table  Width= "100%">
                                    <tr align="center">
                                        <td>
                                            <asp:Panel ID="pnlResults" runat="server" Width= "100%">
                                                <asp:GridView ID="grvOffenceList" runat="server" AlternatingRowStyle-CssClass="CartListItemAlt"
                                                    RowStyle-CssClass="CartListItem" FooterStyle-CssClass="cartlistfooter" HeaderStyle-CssClass="CartListHead"
                                                    ShowFooter="True" Font-Size="8pt" CellPadding="4" GridLines="Vertical" AutoGenerateColumns="False"
                                                   >
                                                    <FooterStyle CssClass="CartListFooter"></FooterStyle>
                                                    <AlternatingRowStyle CssClass="CartListItemAlt"></AlternatingRowStyle>
                                                    <RowStyle CssClass="CartListItem"></RowStyle>
                                                    <HeaderStyle CssClass="CartListHead"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundField DataField="NotIntNo" HeaderText="NotIntNo" Visible="False">
                                                            <ItemStyle Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="NotTicketNo" HeaderText="<%$Resources:grvOffenceList.HeaderText %>">
                                                            <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="<%$Resources:grvOffenceList.HeaderText1 %>">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemStyle VerticalAlign="Top" Wrap="False" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("NotOffenceDate", "{0:yyyy-MM-dd HH:mm}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="NotRegNo" HeaderText="<%$Resources:grvOffenceList.HeaderText2 %>">
                                                            <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="AutName" HeaderText="<%$Resources:grvOffenceList.HeaderText3 %>">
                                                            <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ChgOffenceCode" HeaderText="<%$Resources:grvOffenceList.HeaderText4 %>">
                                                            <ItemStyle VerticalAlign="Top" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CSCode" HeaderText="<%$Resources:grvOffenceList.HeaderText5 %>">
                                                            <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CSDescr" HeaderText="<%$Resources:grvOffenceList.HeaderText6 %>">
                                                            <ItemStyle Wrap="False" VerticalAlign="Top" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ChgRevFineAmount" DataFormatString="{0:f2}" HeaderText="<%$Resources:grvOffenceList.HeaderText7 %>">
                                                            <ItemStyle VerticalAlign="Top" Wrap="False" HorizontalAlign="Right"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Offender" HeaderText="<%$Resources:grvOffenceList.HeaderText8 %>">
                                                            <ItemStyle VerticalAlign="Top" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="LetterTo" HeaderText="<%$Resources:grvOffenceList.HeaderText9 %>">
                                                            <ItemStyle Wrap="False" VerticalAlign="Top" HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnReport" runat="server" CssClass="NormalButton" OnClick="btnReport_Click"
                                                                Text="<%$Resources:btnReport.Text %>" Width="292px" 
                                                                UseSubmitBehavior="False" />&nbsp;
                                                        </td>
                                                        
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                                <input id="SearchCondition" type="hidden" value="mypanel1RegNo" runat="server"/>
                            </td></tr></table>
                   </ContentTemplate>
                   </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 5%;
        width: 100%;">
        <tr>
            <td class="SubContentHeadSmall" valign="top">
            </td>
        </tr>
    </table>
    </form>

    <script type="text/javascript">
    ActiveOneTextBox($get('noticeNoLookup_mypanel2TickSequenceNo'), "mypanel2");
    ActiveOneTextBox($get('mypanel1RegNo'), "mypanel1");
    </script>
</body>
</html>
