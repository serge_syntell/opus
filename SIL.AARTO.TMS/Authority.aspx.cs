using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Globalization;
using SIL.AARTO.BLL.Admin;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Transactions;
using SIL.AARTO.BLL.Utility.Printing;


namespace Stalberg.TMS
{
    /// <summary>
    /// Represents an editor page for and Authority's details
    /// </summary>
    public partial class AuthorityPage : System.Web.UI.Page
    {
        // Fields
        private string connectionString = string.Empty;
        private string login;

        protected string styleSheet;
        protected string backgroundImage;
        //protected string thisPage = "Authority Maintenance";
        protected string thisPageURL = "Authority.aspx";
        protected string keywords = string.Empty;
        protected string title = string.Empty;
        protected string description = string.Empty;

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"></see> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> that contains the event data.</param>
        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.connectionString = Application["constr"].ToString();

            //get user info from session variable
            if (Session["userDetails"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            if (Session["userIntNo"] == null)
                Server.Transfer("Login.aspx?Login=invalid");

            //get user details
            Stalberg.TMS.UserDB user = new UserDB(connectionString);
            Stalberg.TMS.UserDetails userDetails = new UserDetails();

            userDetails = (UserDetails)Session["userDetails"];

            login = userDetails.UserLoginName;

            //int 
            int autIntNo = Convert.ToInt32(Session["autIntNo"]);

            int userAccessLevel = userDetails.UserAccessLevel;

            //set domain specific variables
            General gen = new General();

            backgroundImage = gen.SetBackground(Session["drBackground"]);
            styleSheet = gen.SetStyleSheet(Session["drStyleSheet"]);
            title = gen.SetTitle(Session["drTitle"]);

            if (!Page.IsPostBack)
            {
                this.lblPageName.Text = (string)GetLocalResourceObject("lblPageName.Text");
                pnlUpdateAuthority.Visible = false;

                btnOptDelete.Visible = false;
                btnOptAdd.Visible = true;
                btnOptHide.Visible = true;

                PopulateMetro(ddlMetro);
                Populate3PCodingSystems();
                BindGrid();
            }
        }

        private void Populate3PCodingSystems()
        {
            ddl3PCodingSystem.Items.Add((string)GetLocalResourceObject("ddl3PCodingSystem.Items"));
            ddl3PCodingSystem.Items.Add("Syntell");
            ddl3PCodingSystem.Items.Add("TCS");
            ddl3PCodingSystem.Items.Add("Civitas");
        }

        protected void PopulateMetro(DropDownList ddlSelMetro)
        {
            MetroDB metro = new MetroDB(connectionString);

            SqlDataReader reader = metro.GetMetroList("MtrName");
            ddlSelMetro.DataSource = reader;
            ddlSelMetro.DataValueField = "MtrIntNo";
            ddlSelMetro.DataTextField = "MtrDescr";
            ddlSelMetro.DataBind();

            reader.Close();
        }

        protected void BindGrid()
        {
            int mtrIntNo = 0;

            Stalberg.TMS.AuthorityDB autList = new Stalberg.TMS.AuthorityDB(connectionString);

            DataSet data = autList.GetAuthorityListDS(mtrIntNo, "AutName");
            dgAuthority.DataSource = data;
            dgAuthority.DataKeyField = "AutIntNo";

            //dls 070410 - if we're not on the first page, and we do a search, an error results
            try
            {
                dgAuthority.DataBind();
            }
            catch
            {
                dgAuthority.CurrentPageIndex = 0;
                dgAuthority.DataBind();
            }

            if (dgAuthority.Items.Count == 0)
            {
                dgAuthority.Visible = false;
                lblError.Visible = true;
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text");
            }
            else
            {
                dgAuthority.Visible = true;
            }

            data.Dispose();
            pnlUpdateAuthority.Visible = false;
            btnOptDelete.Visible = false;

        }

        protected void btnOptAdd_Click(object sender, System.EventArgs e)
        {
            // allow transactions to be added to the database table
            this.ViewState.Remove("editAutIntNo");

            pnlUpdateAuthority.Visible = true;
            btnOptDelete.Visible = false;

            this.txtAutCode.Text = string.Empty;
            this.txtAutNo.Text = string.Empty;
            this.txtAutName.Text = string.Empty;
            this.txtAutPostAddr1.Text = string.Empty;
            this.txtAutPostAddr2.Text = string.Empty;
            this.txtAutPostAddr3.Text = string.Empty;
            this.txtAutPostCode.Text = string.Empty;
            this.txtAutTel.Text = string.Empty;
            this.txtAutFax.Text = string.Empty;
            this.txtAutEmail.Text = string.Empty;
            this.txtAutNatisCode.Text = string.Empty;   //Oscar 20120202 add
            this.txtAutSendNatisEmail.Text = string.Empty;  //Oscar 20120202 add
            this.txtBranchCode.Text = string.Empty;
            this.txtBranchName.Text = string.Empty;
            this.txtAutTicketProcessor.Text = string.Empty;
            this.txtAutPCGateway.Text = string.Empty;
            this.txtAutPhysAddr1.Text = string.Empty;
            this.txtAutPhysAddr2.Text = string.Empty;
            this.txtAutPhysAddr3.Text = string.Empty;
            this.txtAutPhysAddr4.Text = string.Empty;
            this.txtAutPhysCode.Text = string.Empty;
            this.txtAutNoticePaymentInfo.Text = string.Empty;
            this.txtAutNoticeIssuedByInfo.Text = string.Empty;
            this.txtReceiptID.Text = string.Empty;
            this.txtEPAccNoLength.Text = string.Empty;
            this.txtGracePeriodEmail.Text = string.Empty;
            this.txtAGEmail.Text = string.Empty;
            this.txtSFEmail.Text = string.Empty;
            this.txtAutNameAfri.Text = string.Empty;
            //2013-10-30 added by Nancy start
            this.txtBAK.Text=string.Empty;
            this.txtBankName.Text = string.Empty;
            this.txtBAN.Text = string.Empty;
            this.txtBBC.Text = string.Empty;
            //2013-10-30 added by Nancy end

            this.ddl3PCodingSystem.SelectedIndex = -1;
            this.ddlMetro.SelectedIndex = -1;

            this.PopulateMetro(ddlMetro);
            //2012-3-7 linda modified dropdownlist items into multi-language
            this.ddlMetro.Items.Insert(0, (string)GetLocalResourceObject("ddlMetro.Items"));

            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            List<LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookup();
            this.ucLanguageLookupUpdate.DataBind(entityList);
        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            lblError.Visible = true;
            bool isnew = true;//2013-11-8 Heidi added (5084)
            AuthorityDetails authority = new AuthorityDetails();
            int mtrIntNo = Convert.ToInt32(ddlMetro.SelectedValue);
            int autIntNo = 0;
            if (this.ViewState["editAutIntNo"] != null)
                autIntNo = (int)this.ViewState["editAutIntNo"];

            string codingSystem = ddl3PCodingSystem.SelectedItem.Text.Substring(0, 1);

            // add the new transaction to the database table
            Stalberg.TMS.AuthorityDB autUpdate = new AuthorityDB(connectionString);
            AuthorityDetails autDetailsLogo;
            byte[] bytes;
            if (autIntNo != 0)
            {
                autDetailsLogo = autUpdate.GetLogo(autIntNo);
                if (upload.HasFile)
                {
                    Byte[] imagebytes = new byte[this.upload.PostedFile.InputStream.Length];
                    BinaryReader br = new BinaryReader(this.upload.PostedFile.InputStream);
                    imagebytes = br.ReadBytes(Convert.ToInt32(this.upload.PostedFile.InputStream.Length));
                    authority.AutLogo = imagebytes;
                }
                else
                {
                    authority.AutLogo = autDetailsLogo.AutLogo;
                }
            }
            else
            {
                if (upload.HasFile)
                {
                    Byte[] imagebytes = new byte[this.upload.PostedFile.InputStream.Length];
                    BinaryReader br = new BinaryReader(this.upload.PostedFile.InputStream);
                    imagebytes = br.ReadBytes(Convert.ToInt32(this.upload.PostedFile.InputStream.Length));
                    authority.AutLogo = imagebytes;
                }
                else
                {
                    authority.AutLogo = new byte[4];
                }
            }


            if (autIntNo> 0)
            {
                isnew = false; //2013-11-8 Heidi added (5084)
            }
           

            authority.AutIntNo = autIntNo;
            authority.MtrIntNo = mtrIntNo;
            authority.LastUser = login;
            authority.AutCode = this.txtAutCode.Text.Trim();
            authority.AutNo = this.txtAutNo.Text.Trim();
            authority.AutName = this.txtAutName.Text.Trim();
            authority.AutPostAddr1 = this.txtAutPostAddr1.Text.Trim();
            authority.AutPostAddr2 = this.txtAutPostAddr2.Text.Trim();
            authority.AutPostAddr3 = this.txtAutPostAddr3.Text.Trim();
            authority.AutPostCode = this.txtAutPostCode.Text.Trim();
            authority.AutTel = this.txtAutTel.Text.Trim();
            authority.AutFax = this.txtAutFax.Text.Trim();
            authority.AutEmail = this.txtAutEmail.Text.Trim();
            authority.AutNatisCode = this.txtAutNatisCode.Text.Trim();  //Oscar 20120202 add
            authority.BranchCode = this.txtBranchCode.Text.Trim();
            authority.BranchName = this.txtBranchName.Text.Trim();
            authority.AutTicketProcessor = this.txtAutTicketProcessor.Text.Trim();
            authority.AutPCGateway = this.txtAutPCGateway.Text.Trim();
            authority.AutSendNatisEmail = this.txtAutSendNatisEmail.Text.Trim();  //Oscar 20120202 add
            authority.Aut3PCodingSystem = codingSystem;
            authority.AutPhysAddr1 = this.txtAutPhysAddr1.Text.Trim();
            authority.AutPhysAddr2 = this.txtAutPhysAddr2.Text.Trim();
            authority.AutPhysAddr3 = this.txtAutPhysAddr3.Text.Trim();
            authority.AutPhysAddr4 = this.txtAutPhysAddr4.Text.Trim();
            authority.AutPhysCode = this.txtAutPhysCode.Text.Trim();
            authority.AutNoticePaymentInfo = this.txtAutNoticePaymentInfo.Text.Trim();
            authority.AutNoticeIssuedByInfo = this.txtAutNoticeIssuedByInfo.Text.Trim();
            //Jerry 2012-05-25 change
            authority.GracePeriod = this.txtGracePeriod.Text.Trim() == "" ? 0 : Convert.ToInt32(this.txtGracePeriod.Text.Trim());
            authority.JudgementPeriod = this.txtJudgementPeriod.Text.Trim() == "" ? 0 : Convert.ToInt32(this.txtJudgementPeriod.Text.Trim());
            //authority.GracePeriodEmail = this.txtGracePeriodEmail.Text.Trim();
            authority.GracePeriodRptEmail = this.txtGracePeriodEmail.Text.Trim();
            authority.AutAGListEmail = this.txtAGEmail.Text.Trim();
            authority.AutSpotFineEmail = this.txtSFEmail.Text.Trim();
            // add by richard 2011-06-07
            authority.AutDepartName = this.AutDepartName.Text.Trim();
            authority.AutFooterText1 = this.AutFooterText1.Text.Trim();
            authority.AutFooterText2 = this.AutFooterText2.Text.Trim();
            authority.AutOfficialName = this.AutOfficialName.Text.Trim();
            authority.AutDocumentFromAfri = this.AutDocumentFromAfri.Text.Trim();
            authority.AutDocumentFromEnglish = this.AutDocumentFromEnglish.Text.Trim();
            //2011-9-7 jerry add
            authority.AutNameAfri = this.txtAutNameAfri.Text.Trim();
            //2013-10-30 added by Nancy start
            authority.BankAccountName = this.txtBAK.Text.Trim();
            authority.BankName = this.txtBankName.Text.Trim();
            authority.BankAccountNo = this.txtBAN.Text.Trim();
            authority.BankBranchCode = this.txtBBC.Text.Trim();
            //2013-10-30 added by Nancy end


            string epReceiptID = this.txtReceiptID.Text.Trim();
            if (epReceiptID.Length > 0)
            {
                short temp = 0;
                if (!short.TryParse(epReceiptID, out temp))
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text1");
                    return;
                }
                //authority.EasyPayReceiptIdentifier = temp;
                authority.EasyPayReceiptID = temp;

                temp = 0;
                if (!short.TryParse(this.txtEPAccNoLength.Text.Trim(), out temp))
                {
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    this.lblError.Text = (string)GetLocalResourceObject("lblError.Text2");
                    return;
                }

                //authority.EasyPayAccountNumberLength = temp;
                authority.EasyPayNumberLength = temp;
            }

            // SD: 20081205 test for null values
            else
            {
                StringBuilder sb = new StringBuilder();
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                sb.Append((string)GetLocalResourceObject("lblError.Text3"));
                sb.Append("\n<ul>\n<li>");
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                sb.Append((string)GetLocalResourceObject("lblError.Text4"));
                sb.Append(" </li>");
                sb.Append("</ul>");
                lblError.Text = sb.ToString();
                return;
            }

            //Jerry 2012-06-18 change it
            using (TransactionScope scope = new TransactionScope())
            {
                int updAutIntNo = autUpdate.UpdateAuthority(authority);
                if (updAutIntNo <= 0)
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text5");
                else
                {
                    AuthorityRuleService authorityRuleService = new AuthorityRuleService();
                    // 2013-08-28 modified by Henry for Enum table WorkFlowRuleForAuthorityList mistake Wfrfaid
                    WorkFlowRuleForAuthorityService wFRAutService = new WorkFlowRuleForAuthorityService();
                    int aR_6100_Wfrfaid = wFRAutService.GetByWfrfaName("AR_6100").Wfrfaid;
                    int aR_5600_Wfrfaid = wFRAutService.GetByWfrfaName("AR_5600").Wfrfaid;
                    //update 6100 authority rule
                    AuthorityRule rule6100 = authorityRuleService.GetByWfrfaidAutIntNo(aR_6100_Wfrfaid, updAutIntNo);
                    if (rule6100 != null)
                    {
                        rule6100.ArNumeric = authority.JudgementPeriod;
                        rule6100.LastUser = authority.LastUser;
                        authorityRuleService.Save(rule6100);
                    }
                    else
                    {
                        rule6100 = new AuthorityRule();
                        rule6100.AutIntNo = updAutIntNo;
                        rule6100.Wfrfaid = aR_6100_Wfrfaid;
                        rule6100.ArNumeric = authority.JudgementPeriod;
                        rule6100.ArString = "";
                        rule6100.LastUser = authority.LastUser;
                        authorityRuleService.Save(rule6100);
                    }

                    //update 5600 authority rule
                    AuthorityRule rule5600 = authorityRuleService.GetByWfrfaidAutIntNo(aR_5600_Wfrfaid, updAutIntNo);
                    if (rule5600 != null)
                    {
                        rule5600.ArNumeric = authority.GracePeriod;
                        rule5600.LastUser = authority.LastUser;
                        authorityRuleService.Save(rule5600);
                    }
                    else
                    {
                        rule5600 = new AuthorityRule();
                        rule5600.AutIntNo = updAutIntNo;
                        rule5600.Wfrfaid = aR_5600_Wfrfaid;
                        rule5600.ArNumeric = authority.GracePeriod;
                        rule5600.ArString = "";
                        rule5600.LastUser = authority.LastUser;
                        authorityRuleService.Save(rule5600);
                    }

                    List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
                    SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                    rUMethod.UpdateIntoLookup(updAutIntNo, lgEntityList, "AuthorityLookup", "AutIntNo", "AutName", this.login);
                    //Modefied By Linda 2012-2-29
                    //Display multi - language error message is the value from the resource
                    lblError.Text = (string)GetLocalResourceObject("lblError.Text6");
                    if (isnew)
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, login, PunchStatisticsTranTypeList.AuthorityMaintenance, PunchAction.Add); 
                    }
                    else
                    {
                        //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                        SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                        punchStatistics.PunchStatisticsTransactionAdd(autIntNo, login, PunchStatisticsTranTypeList.AuthorityMaintenance, PunchAction.Change); 
                    }
                }

                scope.Complete();
            }
            BindGrid();
        }

        protected void dgAuthority_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            //store details of page in case of re-direct
            dgAuthority.SelectedIndex = e.Item.ItemIndex;

            if (dgAuthority.SelectedIndex > -1)
            {
                int editAutIntNo = Convert.ToInt32(dgAuthority.DataKeys[dgAuthority.SelectedIndex]);

                this.ViewState.Add("editAutIntNo", editAutIntNo);
                Session["prevPage"] = thisPageURL;


                SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
                List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> entityList = rUMethod.BindUCLanguageLookupByID(editAutIntNo.ToString(), "AuthorityLookup");
                this.ucLanguageLookupUpdate.DataBind(entityList);

                this.ShowAuthorityDetails(editAutIntNo);
            }
        }


        protected void ShowAuthorityDetails(int editAutIntNo)
        {
            // Obtain and bind a list of all users
            AuthorityDB aut = new AuthorityDB(connectionString);
            AuthorityDetails autDetails = aut.GetAuthorityDetails(editAutIntNo);

            txtAutCode.Text = autDetails.AutCode;
            txtAutName.Text = autDetails.AutName;
            txtAutNo.Text = autDetails.AutNo;
            txtAutPostAddr1.Text = autDetails.AutPostAddr1;
            txtAutPostAddr2.Text = autDetails.AutPostAddr2;
            txtAutPostAddr3.Text = autDetails.AutPostAddr3;
            txtAutPostCode.Text = autDetails.AutPostCode;
            txtAutPhysAddr1.Text = autDetails.AutPhysAddr1;
            txtAutPhysAddr2.Text = autDetails.AutPhysAddr2;
            txtAutPhysAddr3.Text = autDetails.AutPhysAddr3;
            txtAutPhysAddr4.Text = autDetails.AutPhysAddr4;
            txtAutPhysCode.Text = autDetails.AutPhysCode;
            txtAutTel.Text = autDetails.AutTel;
            txtAutFax.Text = autDetails.AutFax;
            txtAutEmail.Text = autDetails.AutEmail;
            this.txtAutNatisCode.Text = autDetails.AutNatisCode;    //Oscar 20120202 add
            this.txtAutSendNatisEmail.Text = autDetails.AutSendNatisEmail;  //Oscar 20120202 add
            txtBranchCode.Text = autDetails.BranchCode;
            txtBranchName.Text = autDetails.BranchCode;
            txtAutPCGateway.Text = autDetails.AutPCGateway;
            txtAutTicketProcessor.Text = autDetails.AutTicketProcessor;
            txtAutNoticeIssuedByInfo.Text = autDetails.AutNoticeIssuedByInfo;
            txtAutNoticePaymentInfo.Text = autDetails.AutNoticePaymentInfo;
            txtGracePeriod.Text = autDetails.GracePeriod.ToString();
            txtJudgementPeriod.Text = autDetails.JudgementPeriod.ToString();
            txtGracePeriodEmail.Text = autDetails.GracePeriodRptEmail.ToString();
            txtAGEmail.Text = autDetails.AutAGListEmail.ToString();
            txtSFEmail.Text = autDetails.AutSpotFineEmail.ToString();
            //add by richard 2011-06-07
            //AutDocumentFrom.Text = autDetails.AutDocumentFrom.ToString();
            AutDepartName.Text = autDetails.AutDepartName.ToString();
            AutFooterText1.Text = autDetails.AutFooterText1.ToString();
            AutFooterText2.Text = autDetails.AutFooterText2.ToString();
            AutOfficialName.Text = autDetails.AutOfficialName.ToString();
            AutDocumentFromAfri.Text = autDetails.AutDocumentFromAfri.ToString();
            AutDocumentFromEnglish.Text = autDetails.AutDocumentFromEnglish.ToString();
            //Logo = autDetails.AutLogo;
            this.logo.ImageUrl = "readAutImage.ashx?id=" + editAutIntNo;
            //txtGracePeriodEmail.Text = autDetails.GracePeriodEmail.ToString();
            //if (autDetails.EasyPayReceiptIdentifier > 0)
            if (autDetails.EasyPayReceiptID > 0)
            {
                //this.txtEPAccNoLength.Text = autDetails.EasyPayAccountNumberLength.ToString();
                //this.txtReceiptID.Text = autDetails.EasyPayReceiptIdentifier.ToString();
                this.txtEPAccNoLength.Text = autDetails.EasyPayNumberLength.ToString();
                this.txtReceiptID.Text = autDetails.EasyPayReceiptID.ToString();
            }
            else
            {
                this.txtEPAccNoLength.Text = string.Empty;
                this.txtReceiptID.Text = string.Empty;
            }
            //2011-9-7 jerry add
            this.txtAutNameAfri.Text = autDetails.AutNameAfri;
            //2013-10-30 added by Nancy start
            this.txtBAK.Text = autDetails.BankAccountName;
            this.txtBankName.Text = autDetails.BankName;
            this.txtBAN.Text = autDetails.BankAccountNo;
            this.txtBBC.Text = autDetails.BankBranchCode;
            //2013-10-30 added by Nancy end

            switch (autDetails.Aut3PCodingSystem)
            {
                case "S":
                    ddl3PCodingSystem.SelectedIndex = ddl3PCodingSystem.Items.IndexOf(ddl3PCodingSystem.Items.FindByText("Syntell"));
                    break;
                case "T":
                    ddl3PCodingSystem.SelectedIndex = ddl3PCodingSystem.Items.IndexOf(ddl3PCodingSystem.Items.FindByText("TCS"));
                    break;
                case "C":
                    ddl3PCodingSystem.SelectedIndex = ddl3PCodingSystem.Items.IndexOf(ddl3PCodingSystem.Items.FindByText("Civitas"));
                    break;
            }
            ddlMetro.SelectedIndex = ddlMetro.Items.IndexOf(ddlMetro.Items.FindByValue(autDetails.MtrIntNo.ToString()));

            pnlUpdateAuthority.Visible = true;

            btnOptDelete.Visible = true;
        }


        protected void dgAuthority_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgAuthority.CurrentPageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        protected void btnOptDelete_Click(object sender, System.EventArgs e)
        {
            int autIntNo = (int)this.ViewState["editAutIntNo"];

            List<SIL.AARTO.BLL.Admin.LanguageLookupEntity> lgEntityList = this.ucLanguageLookupUpdate.Save();
            SIL.AARTO.TMS.ReflectionUsualMethod rUMethod = new SIL.AARTO.TMS.ReflectionUsualMethod();
            rUMethod.DeleteLookup(lgEntityList, "AuthorityLookup", "AutIntNo");

            this.ViewState.Remove("editAutIntNo");

            AuthorityDB aut = new Stalberg.TMS.AuthorityDB(connectionString);
            int delAutIntNo = aut.DeleteAuthority(autIntNo);

            if (delAutIntNo == 0)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text7");
            }
            else if (delAutIntNo == -1)
            {
                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text8");
            }
            else
            {

                //Modefied By Linda 2012-2-29
                //Display multi - language error message is the value from the resource
                lblError.Text = (string)GetLocalResourceObject("lblError.Text9");
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(this.connectionString);
                punchStatistics.PunchStatisticsTransactionAdd(autIntNo, login, PunchStatisticsTranTypeList.AuthorityMaintenance, PunchAction.Delete); 

            }
            lblError.Visible = true;

            BindGrid();
        }

        protected void btnOptHide_Click(object sender, System.EventArgs e)
        {
            if (dgAuthority.Visible.Equals(true))
            {
                //hide it
                dgAuthority.Visible = false;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text1");
            }
            else
            {
                //show it
                dgAuthority.Visible = true;
                btnOptHide.Text = (string)GetLocalResourceObject("btnOptHide.Text");
            }
        }

        protected void btnSearch_Click(object sender, System.EventArgs e)
        {
            BindGrid();
        }

        //protected void btnHideMenu_Click(object sender, System.EventArgs e)
        //{
        //    if (pnlMainMenu.Visible.Equals(true))
        //    {
        //        pnlMainMenu.Visible = false;
        //        btnHideMenu.Text = "Show main menu";
        //    }
        //    else
        //    {
        //        pnlMainMenu.Visible = true;
        //        btnHideMenu.Text = "Hide main menu";
        //    }
        //}

    }
}
