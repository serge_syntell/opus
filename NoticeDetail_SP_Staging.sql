USE [CoCT]
GO

/****** Object:  StoredProcedure [dbo].[NoticeDetail]    Script Date: 2015/06/09 14:09:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NoticeDetail]
(
	@NotIntNo int
)
AS
	-- Oscar 2010-12 added "DISTINCT"
	SELECT DISTINCT n.NotIntNo, NotOffenceType, NotTicketNo, NotLocDescr, NotLocCode, NotSpeedLimit, NotLaneNo, NotCameraID, 
	NotOfficerSName, NotOfficerInit, NotOfficerNo, NotOfficerGroup, NotNaTISIndicator, NotNaTISErrorFieldList,
	NotNaTISRegNo, NotRegNo, NotVehicleRegisterNo, NotVehicleLicenceExpiry, NotDateCOO, NotClearanceCert, 
	NotRegisterAuth, NotSpeed1, NotSpeed2, NotVehicleDescr, NotVehicleCat, NotNaTISVMCode, NotVehicleMakeCode,
	NotVehicleMake, NotNaTISVTCode, NotVehicleTypeCode, NotVehicleType, NotVehicleUsage, NotVehicleColour,
	NotVINorChassis, NotEngine, NotStatutoryOwner, NotNatureOfPerson, NotOffenderType, NotProxyFlag,
	NotFilmNo, NotFrameNo, NotRefNo, NotSeqNo, NotOffenceDate, NotPosted1stNoticeDate, NotPosted2ndNoticeDate,
	NotPostedSummonsDate, NotSource, NotRdTypeCode, NotRdTypeDescr, NotTravelDirection, NotCourtNo,
	NotCourtName, NotElapsedTime, NotPaymentDate, NotIssue1stNoticeDate, NotIssue2ndNoticeDate, NotPrint1stNoticeDate,
	NotPrint2ndNoticeDate, NotPrintSummonsDate, n.LastUser, AutIntNo, NotLoadDate, NotNatisINPName, NotNatisINPDate,
	NotNatisOUTName, NotNatisOUTDate, NotCiprusPrintReqName, NotCiprusPrintReqDate, ParkMeterNo, AutName, IssuedBy,
	CrtPaymentAddr1, CrtPaymentAddr2, CrtPaymentAddr3, CrtPaymentAddr4, NotCiprusPrintConfirmSendDate, 
	NotDocType, NotSummonsPrintFileName, NotSendTo, NotCamSerialNo,  NotEasyPayNumber, 
	ExportFlag, Not2ndNoticePrintFile, NotCiprusPIContraventionFile, NotCiprusPIContraventionDate, NotCiprusPIProcessCode,
	NotCiprusPISeverityCode, NotCiprusPIProcessDescr, NotNatisProxyIndicator, NotNewOffender, NotPosted1stNoticeUser,
	NotPosted2ndNoticeUser, NotFilmNo+'/'+NotFrameNo AS NotFilmNoFrameNo
	--, c.ChargeStatus
	-- Oscar changed for 4416
	, NoticeStatus
	, Not1stImageString = '', Not2ndImageString = '',		-- this resolves the issue with Notice correction crashing
	Not1stBase64String, Not2ndBase64String,
	NotFilmType = ISNULL(NotFilmType, ''),IsVideo
	--NotFilmType	-- Oscar 20120315 add NotFilmType
    , NotStatisticsCode,SMSFor2ndNotice
	FROM Notice n WITH (NOLOCK)
		--LEFT OUTER JOIN Charge c WITH (NOLOCK) ON n.NotIntNo = c.NotIntNo	-- Oscar 2011-03 removed for 4416
	WHERE n.NotIntNo = @NotIntNo
	
	--Seawen 2012-08-21 add VehImageIntNo,LicImageIntNo
	SELECT s1.ScImIntNo as VehImageIntNo,s2.ScImIntNo as LicImageIntNo FROM notice n WITH(NOLOCK)
	INNER JOIN Notice_Frame t WITH (NOLOCK) ON t.NotIntNo = n.NotIntNo
	LEFT OUTER JOIN ScanImage s1 WITH (NOLOCK) ON s1.FrameIntNo = t.FrameIntNo AND s1.ScImPrintVal = 1
	LEFT OUTER JOIN ScanImage s2 WITH (NOLOCK) ON s2.FrameIntNo = t.FrameIntNo AND s2.ScImType = 'R' --s2.ScImPrintVal = 2
	WHERE n.NotIntNo=@NotIntNo

GO


