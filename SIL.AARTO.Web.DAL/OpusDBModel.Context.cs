﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIL.AARTO.Web.DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class OpusDB : DbContext
    {
        public OpusDB()
            : base("name=OpusDB")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserAccountActivity> UserAccountActivities { get; set; }
        public virtual DbSet<WithdrawlReason> WithdrawlReasons { get; set; }
        public virtual DbSet<Notice> Notices { get; set; }
        public virtual DbSet<ImageFileServer> ImageFileServers { get; set; }
    
        public virtual int NetTiers_User_Update(Nullable<int> userIntNo, Nullable<int> usgIntNo, string userSname, string userFname, string userInit, string userLoginName, string userEmail, string userPassword, Nullable<int> userAccessLevel, string lastUser, byte[] rowversion, string userCountCashbox, Nullable<System.DateTime> spotFinePriorityDate, Nullable<int> userDefaultAutIntNo, string userGroupName, string defaultLanguage, Nullable<bool> userPasswordReset, Nullable<int> userPasswordGraceLoginCount, Nullable<System.DateTime> userPasswordExpiryDate, ObjectParameter returnedRowversion)
        {
            var userIntNoParameter = userIntNo.HasValue ?
                new ObjectParameter("UserIntNo", userIntNo) :
                new ObjectParameter("UserIntNo", typeof(int));
    
            var usgIntNoParameter = usgIntNo.HasValue ?
                new ObjectParameter("UsgIntNo", usgIntNo) :
                new ObjectParameter("UsgIntNo", typeof(int));
    
            var userSnameParameter = userSname != null ?
                new ObjectParameter("UserSname", userSname) :
                new ObjectParameter("UserSname", typeof(string));
    
            var userFnameParameter = userFname != null ?
                new ObjectParameter("UserFname", userFname) :
                new ObjectParameter("UserFname", typeof(string));
    
            var userInitParameter = userInit != null ?
                new ObjectParameter("UserInit", userInit) :
                new ObjectParameter("UserInit", typeof(string));
    
            var userLoginNameParameter = userLoginName != null ?
                new ObjectParameter("UserLoginName", userLoginName) :
                new ObjectParameter("UserLoginName", typeof(string));
    
            var userEmailParameter = userEmail != null ?
                new ObjectParameter("UserEmail", userEmail) :
                new ObjectParameter("UserEmail", typeof(string));
    
            var userPasswordParameter = userPassword != null ?
                new ObjectParameter("UserPassword", userPassword) :
                new ObjectParameter("UserPassword", typeof(string));
    
            var userAccessLevelParameter = userAccessLevel.HasValue ?
                new ObjectParameter("UserAccessLevel", userAccessLevel) :
                new ObjectParameter("UserAccessLevel", typeof(int));
    
            var lastUserParameter = lastUser != null ?
                new ObjectParameter("LastUser", lastUser) :
                new ObjectParameter("LastUser", typeof(string));
    
            var rowversionParameter = rowversion != null ?
                new ObjectParameter("Rowversion", rowversion) :
                new ObjectParameter("Rowversion", typeof(byte[]));
    
            var userCountCashboxParameter = userCountCashbox != null ?
                new ObjectParameter("UserCountCashbox", userCountCashbox) :
                new ObjectParameter("UserCountCashbox", typeof(string));
    
            var spotFinePriorityDateParameter = spotFinePriorityDate.HasValue ?
                new ObjectParameter("SpotFinePriorityDate", spotFinePriorityDate) :
                new ObjectParameter("SpotFinePriorityDate", typeof(System.DateTime));
    
            var userDefaultAutIntNoParameter = userDefaultAutIntNo.HasValue ?
                new ObjectParameter("UserDefaultAutIntNo", userDefaultAutIntNo) :
                new ObjectParameter("UserDefaultAutIntNo", typeof(int));
    
            var userGroupNameParameter = userGroupName != null ?
                new ObjectParameter("UserGroupName", userGroupName) :
                new ObjectParameter("UserGroupName", typeof(string));
    
            var defaultLanguageParameter = defaultLanguage != null ?
                new ObjectParameter("DefaultLanguage", defaultLanguage) :
                new ObjectParameter("DefaultLanguage", typeof(string));
    
            var userPasswordResetParameter = userPasswordReset.HasValue ?
                new ObjectParameter("UserPasswordReset", userPasswordReset) :
                new ObjectParameter("UserPasswordReset", typeof(bool));
    
            var userPasswordGraceLoginCountParameter = userPasswordGraceLoginCount.HasValue ?
                new ObjectParameter("UserPasswordGraceLoginCount", userPasswordGraceLoginCount) :
                new ObjectParameter("UserPasswordGraceLoginCount", typeof(int));
    
            var userPasswordExpiryDateParameter = userPasswordExpiryDate.HasValue ?
                new ObjectParameter("UserPasswordExpiryDate", userPasswordExpiryDate) :
                new ObjectParameter("UserPasswordExpiryDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("NetTiers_User_Update", userIntNoParameter, usgIntNoParameter, userSnameParameter, userFnameParameter, userInitParameter, userLoginNameParameter, userEmailParameter, userPasswordParameter, userAccessLevelParameter, lastUserParameter, rowversionParameter, userCountCashboxParameter, spotFinePriorityDateParameter, userDefaultAutIntNoParameter, userGroupNameParameter, defaultLanguageParameter, userPasswordResetParameter, userPasswordGraceLoginCountParameter, userPasswordExpiryDateParameter, returnedRowversion);
        }
    
        public virtual int sp_InsertUserAccountActivity(Nullable<int> userID, string resetLink, string activity, Nullable<System.DateTime> activityDateTime)
        {
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("userID", userID) :
                new ObjectParameter("userID", typeof(int));
    
            var resetLinkParameter = resetLink != null ?
                new ObjectParameter("ResetLink", resetLink) :
                new ObjectParameter("ResetLink", typeof(string));
    
            var activityParameter = activity != null ?
                new ObjectParameter("Activity", activity) :
                new ObjectParameter("Activity", typeof(string));
    
            var activityDateTimeParameter = activityDateTime.HasValue ?
                new ObjectParameter("ActivityDateTime", activityDateTime) :
                new ObjectParameter("ActivityDateTime", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_InsertUserAccountActivity", userIDParameter, resetLinkParameter, activityParameter, activityDateTimeParameter);
        }
    
        public virtual ObjectResult<string> spGetPrintingFileName(Nullable<int> notIntNo)
        {
            var notIntNoParameter = notIntNo.HasValue ?
                new ObjectParameter("NotIntNo", notIntNo) :
                new ObjectParameter("NotIntNo", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("spGetPrintingFileName", notIntNoParameter);
        }
    }
}
