﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using System.Threading;
using SIL.AARTO.BLL.Utility;
using System.IO;
using System.Text.RegularExpressions;
using SIL.AARTO.BLL.Model;
using SIL.AARTO.Util;

namespace PrintFactoryBackService
{
    partial class PrintFactoryService : ServiceBase
    {
        #region [Private Fields]
        private Thread workReceiveThread;
        private Thread workUpdateThread;

        private static string printDirectoryIn;
        private static string printEngineRoot;

        private static int receiveUpdateQueueTimeout;
        private static int timeSpan;
        
        private static string remoteSiteRegistrationsPath;
        private static string updatedFailedBatchesInfoFile;

        private static string updateBatchStatusQueue;
        private static string receivedQueue;

        #endregion

        public PrintFactoryService()
        {
            InitializeComponent();
        }

        #region [Override functions from base class]
        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            try
            {
                //Thread.Sleep(TimeSpan.FromSeconds(20));
                //WriteEventLog("Beginning initalization config file. ", EventLogEntryType.Information);
                InitializeConfig();

                //WriteEventLog("printDirectoryIn = " + printDirectoryIn, EventLogEntryType.Error);
                //WriteEventLog("printEngineRoot = " + printEngineRoot, EventLogEntryType.Error);
                //WriteEventLog("timeSpan = " + timeSpan.ToString(), EventLogEntryType.Error);
                
                //WriteEventLog("Beginning start threads. ", EventLogEntryType.Information);               
                workReceiveThread = new Thread(new ThreadStart(ReceivePebs));
                //Make this a background thread, so it will terminate when the main thread/process is de-activated
                    
                workReceiveThread.IsBackground = true;
                workReceiveThread.SetApartmentState(ApartmentState.STA);

                // Start the Work
                workReceiveThread.Start();

                //////////////////////////////////////////////////////

                //WriteEventLog("Beginning start threads. ", EventLogEntryType.Information);               
                workUpdateThread = new Thread(new ThreadStart(UpdateBatch));
                //Make this a background thread, so it will terminate when the main thread/process is de-activated

                workUpdateThread.IsBackground = true;
                workUpdateThread.SetApartmentState(ApartmentState.STA);

                // Start the Work
                workUpdateThread.Start();
                
                //WriteEventLog("All threads has alreadly started. ", EventLogEntryType.Information);

                base.OnStart(args);
            }
            catch
            {
            }
        }

        

        protected override void OnStop()
        {            
            try
            {
                workReceiveThread.Abort();
                workUpdateThread.Abort();
                // TODO: Add code here to perform any tear-down necessary to stop your service.
                base.OnStop();
                //emailTimer.Stop();
            }
            catch
            { }
        }
        #endregion


        #region [User functions]
        private static void WriteEventLog(string sEvent, EventLogEntryType eventLogType)
        {
            string sSource = "PrintFactory Back Service"; ;
            string sLog = "Application";

            //sEvent = "Sample Event";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            //EventLog.WriteEntry(sSource, sEvent);
            EventLog.WriteEntry(sSource, sEvent, eventLogType, 0);

        }

        private static string ReadConfigFile(string keyName)
        {
            return ConfigurationManager.AppSettings[keyName];
            //// Create an AppSettingReader object.
            //System.Configuration.AppSettingsReader appsreader
            //  = new AppSettingsReader();

            //String value = String.Empty;
            //// Get the value Hour from config file.
            //value = (String)appsreader.GetValue(keyName, value.GetType());
            //// Return it.
            //return value.ToString();
        }

        private static void InitializeConfig()
        {
            try
            {
                printDirectoryIn = ReadConfigFile("PrintDirectoryIn");
                printEngineRoot = ReadConfigFile("PrintEngineRoot");
                receivedQueue = ReadConfigFile("ReceivedQueue");

                receiveUpdateQueueTimeout = Convert.ToInt32(ReadConfigFile("ReceiveUpdateQueueTimeout"));
                updateBatchStatusQueue = ReadConfigFile("UpdateBatchStatusQueue");
                remoteSiteRegistrationsPath = ReadConfigFile("RemoteSiteRegistrationsPath");
                updatedFailedBatchesInfoFile = ReadConfigFile("UpdatedFailedBatchesInfoFile");

                timeSpan = Convert.ToInt32(ReadConfigFile("TimeSpan"));

            }
            catch (Exception ex)
            {
                WriteEventLog(ex.Message, EventLogEntryType.Error);
            }
        }

        public void ReceivePebs()
        {
            while (true)
            {
                try
                {
                    Thread.Sleep(TimeSpan.FromSeconds(timeSpan));
                    //this.eventLog1.WriteEntry("Run ReceivePebs at:" + DateTime.Now.ToString());            
                    //List<string> fileReceivedList;
                    using (IndabaClientUtil indabaClientUtil = new IndabaClientUtil())
                    {
                        //fileReceivedList = indabaClientUtil.GetReceivedFiles();
                        indabaClientUtil.GetReceivedFiles();
                    }
                    string[] pebsFiles = Directory.GetFiles(printDirectoryIn, "*.pebs");

                    if (!Directory.Exists(printEngineRoot))
                    {
                        Directory.CreateDirectory(printEngineRoot);
                    }

                    Regex ex = new Regex(".pebs$");
                    //Regex ex2 = new Regex(".pebsdocx$");
                    int count = pebsFiles.Length;

                    for (var i = 0; i < count; i++)
                    {
                        //Thread.Sleep(1000);
                        string pebsFile = pebsFiles[i];
                        string docxFile = ex.Replace(pebsFile, ".pdf");
                        if (File.Exists(docxFile))
                        {
                            FileInfo pebsFileInfo = new FileInfo(pebsFile);
                            FileInfo docxFileInfo = new FileInfo(docxFile);//(ex2.Replace(docxFile, ".pdf"));

                            // caculate the issued folder
                            PebsBatch pebs = ObjectSerializer.ReadXmlFileToObject<PebsBatch>(pebsFile);
                            StringBuilder sbNewFolder = new StringBuilder();
                            sbNewFolder.AppendFormat(@"{0}{1}{2}{1}Issued{1}{3}{1}",
                                printEngineRoot, Path.DirectorySeparatorChar,
                                pebs.AutCode, pebs.DocumentType);

                            DateTime dteIssued = DateTime.Parse(pebs.DateIssued);
                            sbNewFolder.AppendFormat(@"{1}{0}{2}{0}{3}{0}", Path.DirectorySeparatorChar,
                                dteIssued.Year.ToString(),
                                dteIssued.Month.ToString().PadLeft(2, '0'),
                                dteIssued.Day.ToString().PadLeft(2, '0'));

                            sbNewFolder.Append(pebs.BatchCode);

                            if (!Directory.Exists(sbNewFolder.ToString()))
                            {
                                Directory.CreateDirectory(sbNewFolder.ToString());
                            }

                            string newPebsPath = Path.Combine(sbNewFolder.ToString(), pebsFileInfo.Name);
                            string newDocxPath = Path.Combine(sbNewFolder.ToString(), docxFileInfo.Name);

                            File.Move(pebsFile, newPebsPath);
                            File.Move(docxFile, newDocxPath);

                            MessageQueueUtil.SendMsgToQueue(newPebsPath, receivedQueue);
                        }

                    }
                }
                catch (Exception)
                {

                }
            }

           
        }

        public void UpdateBatch()
        {
            while (true)
            {
                try
                {
                    Thread.Sleep(TimeSpan.FromSeconds(timeSpan));
                    List<UpdateMessageInfo> msgList = MessageQueueUtil.GetMsgListFromQueue<UpdateMessageInfo>(updateBatchStatusQueue, receiveUpdateQueueTimeout);
                    if (msgList != null && msgList.Count > 0)
                    {

                        int count = msgList.Count;
                        Dictionary<int, string> failedMsgList = new Dictionary<int, string>();
                        //DocStatusProcessor docStatusProcessor = new DocStatusProcessor();

                        RemoteSiteWebServices remoteSiteWebServices
                            = ObjectSerializer.ReadXmlFileToObject<RemoteSiteWebServices>(remoteSiteRegistrationsPath);
                        Dictionary<string, string> serviceUrlDict = new Dictionary<string, string>();
                        if (remoteSiteWebServices != null && remoteSiteWebServices.WebServices != null)
                        {
                            for (int i = 0; i < remoteSiteWebServices.WebServices.Count; i++)
                            {
                                serviceUrlDict.Add(
                                    remoteSiteWebServices.WebServices[i].AutCode
                                    , remoteSiteWebServices.WebServices[i].WebServiceUrl
                                    );
                            }
                        }
                        string serviceUrl;
                        string funName = "UpdateBatchStatus";
                        string inputParaName = "updateBatchStatusParam";
                        UpdateBatchStatusParam input = new UpdateBatchStatusParam();
                        //string result = WebServiceAccessor.GetFromWebServiceBySoap<string>(serviceUrl, funName, inputParaName, input);
                        for (var i = 0; i < count; i++)
                        {
                            UpdateMessageInfo msg = msgList[i];
                            if (!serviceUrlDict.ContainsKey(msg.LaCode))
                            {
                                string errmsg
                                    = string.Format("La ({0}) not exsit in RemoteSiteRegistrations.xml!"
                                    , msg.LaCode);
                                failedMsgList.Add(msg.BatchId, errmsg);
                                continue;
                            }
                            serviceUrl = serviceUrlDict[msg.LaCode];
                            input.batchId = msg.BatchId;
                            input.docStatus = msg.ToStatus;
                            input.userName = msg.UserName;
                            WebserviceResult res = WebServiceAccessor.GetFromWebServiceBySoap<WebserviceResult>(serviceUrl, funName, inputParaName, input);
                            //string result = WebServiceAccessor.GetFromWebServiceBySoap<String>(serviceUrl, funName, inputParaName, input);
                            if (res.Result != "OK")
                            {
                                failedMsgList.Add(msg.BatchId, res.Result);
                            }
                        }
                        // if any failed
                        if (failedMsgList.Count > 0)
                        {
                            UpdatedFailedBatchesInfo updatedFailedBatchesInfo = null;
                            if (File.Exists(updatedFailedBatchesInfoFile))
                            {
                                try
                                {
                                    updatedFailedBatchesInfo = ObjectSerializer.ReadXmlFileToObject<UpdatedFailedBatchesInfo>(updatedFailedBatchesInfoFile);
                                }
                                catch
                                {
                                    updatedFailedBatchesInfo = new UpdatedFailedBatchesInfo();
                                }
                            }
                            else
                            {
                                updatedFailedBatchesInfo = new UpdatedFailedBatchesInfo();
                            }
                            if (updatedFailedBatchesInfo.failedBatches == null)
                            {
                                updatedFailedBatchesInfo.failedBatches = new List<UpdatedFailedBatchItem>();
                            }
                            foreach (var msg in failedMsgList)
                            {
                                updatedFailedBatchesInfo.failedBatches.Add(new UpdatedFailedBatchItem(msg.Key, msg.Value));
                            }
                            ObjectSerializer.SaveObjectToXmlFile(updatedFailedBatchesInfo, updatedFailedBatchesInfoFile);
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
        }
      
        #endregion

        
    }
}
