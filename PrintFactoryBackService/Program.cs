﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.EntLib;
using System.Reflection;
using SIL.AARTO.BLL.Utility;
using System.Configuration;

namespace PrintFactoryBackService
{
    static class Program
    {
        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = AartoProjectList.AARTOPrintEnginePrintFactoryBackService.ToString();
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            //// There is no database connected on PrintFactory, so don't need to Check Last updated Version
            //string errorMessage;
            //if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOPrintEnginePrintFactoryBackService, out errorMessage))
            //{
            //    EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);                
            //    return;
            //}

            // Write the started Log
            APP_TITLE = string.Format("{0} - Version {1}\n Last Updated {2}\n Started at: {3}",
                APP_NAME,
                Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                ProjectLastUpdated.AARTOPrintEnginePrintFactoryBackService, DateTime.Now);

            //string strDatabaseInfo = EntLibLogger.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());

            EntLibLogger.WriteLog(LogCategory.General, null, string.Format("{0}", APP_TITLE));

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
                //new ReceiveService(),
                //new UpdateBatchStatusService()
                new PrintFactoryService()
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
