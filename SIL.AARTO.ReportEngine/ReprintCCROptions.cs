﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIL.AARTO.BLL.Utility.Printing;

namespace SIL.AARTO.ReportEngine
{
    public partial class ReprintCCROptions : Form
    {
        public ReprintCCROptions()
        {
            InitializeComponent();
            ReprintSetting = new CCRReprintSetting();
        }

        public CCRReprintSetting ReprintSetting { get; set; }

        public new DialogResult ShowDialog()
        {
            ReadSetting();

            return base.ShowDialog();
        }

        void ReadSetting()
        {
            this.chkReportCCR.Checked = ReprintSetting.Print1st;
            this.chkSurnameCCR.Checked = ReprintSetting.Print2nd;
            this.chkNoticeNoCCR.Checked = ReprintSetting.Print3rd;
            this.chkChargeSheetCCR.Checked = ReprintSetting.Print4th;
        }

        void SaveSetting()
        {
            ReprintSetting.Print1st = this.chkReportCCR.Checked;
            ReprintSetting.Print2nd = this.chkSurnameCCR.Checked;
            ReprintSetting.Print3rd = this.chkNoticeNoCCR.Checked;
            ReprintSetting.Print4th = this.chkChargeSheetCCR.Checked;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            SaveSetting();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            SaveSetting();
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ReprintCCROptions_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSetting();
            if (DialogResult == DialogResult.None)
                DialogResult = DialogResult.Cancel;
        }
    }
}
