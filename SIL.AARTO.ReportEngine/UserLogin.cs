﻿using System;
using System.Windows.Forms;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.ReportEngine
{
    public partial class UserLogin : Form
    {
        public UserLogin()
        {
            InitializeComponent();
            //this.Text = Program.APP_TITLE;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textUserName.Text.Trim()))
            {
                MessageBox.Show("User name is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(textPwd.Text.Trim()))
            {
                MessageBox.Show("Password is required.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            LoginResult loginResult;
            GlobalVariates<User>.CurrentUser = Login(textUserName.Text.Trim(), textPwd.Text.Trim(), out loginResult);
            switch (loginResult)
            {
                case LoginResult.ErrorUserName:
                    MessageBox.Show("Sorry, your user name does not exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.ErrorPassword:
                    MessageBox.Show("Sorry, your password is invalid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.IsLocked:
                    MessageBox.Show("Sorry, your user name is locked. Please contact administrator. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.Exception:
                    MessageBox.Show("Sorry, cannot connect to the database server.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                case LoginResult.Succeeded:
                    break;
            }
            this.Hide();
            frmMain mainForm = new frmMain();
            mainForm.Show();
        }
        public enum LoginResult
        {
            Succeeded = 0, IsLocked = 1, ErrorUserName = 2, ErrorPassword = 3, Exception = 4
        }
        public User Login(string userName, string pwd, out LoginResult loginResult)
        {
            if (ValidateUser(userName, pwd))
            {
                loginResult = LoginResult.Succeeded;
                UserService userService = new UserService();
                User user = userService.GetByUserLoginName(userName);
                return user;
            }
            loginResult = LoginResult.ErrorPassword;
            return null;
        }
        public bool ValidateUser(string userName, string pwd)
        {
            User user = new UserService().GetByUserLoginName(userName);
            if (user == null)
            {
                return false;
            }
            string storedPwd = user.UserPassword;
            string inputPwd = Encrypt.HashPassword(pwd);
            if (storedPwd == inputPwd)
                return true;
            return false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textPwd_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblUserName_Click(object sender, EventArgs e)
        {

        }
    }
}
