﻿namespace SIL.AARTO.ReportEngine
{
    partial class ReprintCCROptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkReportCCR = new System.Windows.Forms.CheckBox();
            this.chkSurnameCCR = new System.Windows.Forms.CheckBox();
            this.chkNoticeNoCCR = new System.Windows.Forms.CheckBox();
            this.chkChargeSheetCCR = new System.Windows.Forms.CheckBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // chkReportCCR
            // 
            this.chkReportCCR.AutoSize = true;
            this.chkReportCCR.Location = new System.Drawing.Point(78, 70);
            this.chkReportCCR.Name = "chkReportCCR";
            this.chkReportCCR.Size = new System.Drawing.Size(166, 17);
            this.chkReportCCR.TabIndex = 0;
            this.chkReportCCR.Text = "Report Criminal Case Register";
            this.chkReportCCR.UseVisualStyleBackColor = true;
            // 
            // chkSurnameCCR
            // 
            this.chkSurnameCCR.AutoSize = true;
            this.chkSurnameCCR.Location = new System.Drawing.Point(78, 93);
            this.chkSurnameCCR.Name = "chkSurnameCCR";
            this.chkSurnameCCR.Size = new System.Drawing.Size(220, 17);
            this.chkSurnameCCR.TabIndex = 1;
            this.chkSurnameCCR.Text = "Index Surname for Criminal Case Register";
            this.chkSurnameCCR.UseVisualStyleBackColor = true;
            // 
            // chkNoticeNoCCR
            // 
            this.chkNoticeNoCCR.AutoSize = true;
            this.chkNoticeNoCCR.Location = new System.Drawing.Point(78, 116);
            this.chkNoticeNoCCR.Name = "chkNoticeNoCCR";
            this.chkNoticeNoCCR.Size = new System.Drawing.Size(249, 17);
            this.chkNoticeNoCCR.TabIndex = 2;
            this.chkNoticeNoCCR.Text = "Index Notice Number for Criminal Case Register";
            this.chkNoticeNoCCR.UseVisualStyleBackColor = true;
            // 
            // chkChargeSheetCCR
            // 
            this.chkChargeSheetCCR.AutoSize = true;
            this.chkChargeSheetCCR.Checked = true;
            this.chkChargeSheetCCR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkChargeSheetCCR.Location = new System.Drawing.Point(78, 139);
            this.chkChargeSheetCCR.Name = "chkChargeSheetCCR";
            this.chkChargeSheetCCR.Size = new System.Drawing.Size(267, 17);
            this.chkChargeSheetCCR.TabIndex = 3;
            this.chkChargeSheetCCR.Text = "Charge Sheet Annexures for Criminal Case Register";
            this.chkChargeSheetCCR.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(78, 174);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "&Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(207, 174);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Please select the reports that need to reprint.";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(399, 29);
            this.label2.TabIndex = 7;
            this.label2.Text = "Please make sure that you have loaded the correct paper for the selected reports." +
                " All selected reports must use the same stationery.";
            // 
            // ReprintCCROptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 214);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.chkChargeSheetCCR);
            this.Controls.Add(this.chkNoticeNoCCR);
            this.Controls.Add(this.chkSurnameCCR);
            this.Controls.Add(this.chkReportCCR);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReprintCCROptions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Reprint CCR Options";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReprintCCROptions_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkReportCCR;
        private System.Windows.Forms.CheckBox chkSurnameCCR;
        private System.Windows.Forms.CheckBox chkNoticeNoCCR;
        private System.Windows.Forms.CheckBox chkChargeSheetCCR;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}