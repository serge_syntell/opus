﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net; 
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility;
using System.Management; 

namespace SIL.AARTO.ReportEngine
{
    /// <summary>
    /// Add by Brian 2013-10-18
    /// Local Pc Settings Edit Form
    /// </summary>
    public partial class LocalPcSettingsEditForm : EditForm
    {
        //Current entity of Data Service Instantiate
        private LocalPcSettingsService _LocalPcSettingsService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public LocalPcSettingsEditForm()
        {
            this.EntityType = typeof(LocalPcSettings);
            this._LocalPcSettingsService = new LocalPcSettingsService();
            InitializeComponent();
            this.LoadReportDownList();
            this.LoadPrinterDownList();
            this.LoadLPTPortDownList();
        }

        /// <summary>
        /// Load report config data list for DropDownList Control
        /// </summary>
        private void LoadReportDownList()
        {
            this.cmbReportConfig.DataSource = ReportPrintSettingsForm.ReportConfigList;
            this.cmbReportConfig.DisplayMember = "ReportName";
            this.cmbReportConfig.ValueMember = "RcIntNo";
        }

        /// <summary>
        /// Load printer settings data list for DropDownList Control
        /// </summary>
        private void LoadPrinterDownList()
        {
            PrinterSettingsService service = new PrinterSettingsService();
            List<PrinterSettings> printerList = service.GetAll().OrderBy(o => o.PsIntNo).ToList();
            printerList.Insert(0, new PrinterSettings() { PsIntNo = 0, PsName = string.Empty });
            this.cmbPsIntNo.DataSource = printerList;
            this.cmbPsIntNo.DisplayMember = "PsName";
            this.cmbPsIntNo.ValueMember = "PsIntNo";
        }

        /// <summary>
        /// Load LPT Port data list for DropDownList Control
        /// </summary>
        private void LoadLPTPortDownList()
        {
            List<string> lptPortList = new List<string>()
            {
                "",
                "LPT1",
                "LPT2",
                "LPT3",
                "LPT4",
                "LPT5",
                "LPT6",
                "LPT7",
                "LPT8",
                "LPT9"
            };
            this.cmbLPTPort.DataSource = lptPortList;
        }

        /// <summary>
        /// Override to expand get entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override IEntity ExpandGetEntity(IEntity entity)
        {
            LocalPcSettings editEntity = entity as LocalPcSettings;
            LocalPcSettings entityData = this.Entity as LocalPcSettings;
            if (entityData != null)
            {
                editEntity.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            }
            else
            {
                editEntity.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
                editEntity.CreateDate = DateTime.Now;
            }
            editEntity.AutIntNo = ReportPrintSettingsForm.AutIntNo;
            editEntity.PcmacAddress = ReportPrintSettingsForm.GetMACAddress();
            return entity;
        }

        /// <summary>
        /// Override to validate entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool ValidateData(IEntity entity)
        {
            LocalPcSettings editEntity = (LocalPcSettings)entity;
            if (editEntity.RcIntNo <= 0)
            {
                MessageBox.Show("Sorry, please select a report!");
                return false;
            }
            if (editEntity.PsIntNo <= 0)
            {
                MessageBox.Show("Sorry, please select a printer!");
                return false;
            }
            if (string.IsNullOrEmpty(editEntity.LptPort))
            {
                MessageBox.Show("Sorry, please select a LPT port!");
                return false;
            }
            if (this.Entity == null || (this.Entity as LocalPcSettings).RcIntNo != editEntity.RcIntNo)
            {
                LocalPcSettingsQuery query = new LocalPcSettingsQuery();
                query.Append(LocalPcSettingsColumn.RcIntNo, editEntity.RcIntNo.ToString());
                query.Append(LocalPcSettingsColumn.PcmacAddress, editEntity.PcmacAddress);
                LocalPcSettings ps = this._LocalPcSettingsService.Find(query).FirstOrDefault();
                if (ps != null)
                {
                    MessageBox.Show("Sorry, the report name already exists!");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Override to insert entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool InsertEntity(IEntity entity)
        {
            return this._LocalPcSettingsService.Insert((LocalPcSettings)entity);
        }

        /// <summary>
        /// Override to update entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool UpdateEntity(IEntity entity)
        {
            return this._LocalPcSettingsService.Update((LocalPcSettings)entity);
        }

        /// <summary>
        /// Click event of OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.OperationEntity())
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
