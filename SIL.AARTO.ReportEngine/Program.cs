﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SIL.AARTO.ReportEngine
{
    static class Program
    {
        public static readonly string APP_NAME = "ReportEngine";    // 2013-07-29 add by Henry
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
	    string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new UserLoginForm());
        }

        
    }
}
