﻿namespace SIL.AARTO.ReportEngine
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.dgvDetail = new System.Windows.Forms.DataGridView();
            this.rowCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bsForDetail = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.cb_OnlyHistory = new System.Windows.Forms.CheckBox();
            this.cb_AddHistory = new System.Windows.Forms.CheckBox();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.button7 = new System.Windows.Forms.Button();
            this.lblEngine = new System.Windows.Forms.Label();
            this.dgvHistory = new System.Windows.Forms.DataGridView();
            this.txtStart = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_SelectReportType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetNewData = new System.Windows.Forms.Button();
            this.dgvFiles = new System.Windows.Forms.DataGridView();
            this.Select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.bsForFiles = new System.Windows.Forms.BindingSource(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtDocumentNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.lblDocumentNumber = new System.Windows.Forms.Label();
            this.historyPaginateBar = new System.Windows.Forms.BindingNavigator(this.components);
            this.lblHistoryTotalPage = new System.Windows.Forms.ToolStripLabel();
            this.btnHistoryFirstPage = new System.Windows.Forms.ToolStripButton();
            this.btnHistoryPrePage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.lblHistoryCurrentPage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel8 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel9 = new System.Windows.Forms.ToolStripLabel();
            this.lblHistoryTotalCount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btnHistoryNextPage = new System.Windows.Forms.ToolStripButton();
            this.btnHistoryLastPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel11 = new System.Windows.Forms.ToolStripLabel();
            this.cmbHistoryPageSize = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.txtHistoryPageToGo = new System.Windows.Forms.ToolStripTextBox();
            this.btnGoHistoryPage = new System.Windows.Forms.ToolStripButton();
            this.pnlSearchBarForFiles = new System.Windows.Forms.Panel();
            this.btnSearchFileNameForFileList = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tbSearchFileNameForFileList = new System.Windows.Forms.TextBox();
            this.lblFolder = new System.Windows.Forms.Label();
            this.filesPaginateBar = new System.Windows.Forms.BindingNavigator(this.components);
            this.lblFilesTotalPage = new System.Windows.Forms.ToolStripLabel();
            this.btnFilesFirstPage = new System.Windows.Forms.ToolStripButton();
            this.btnFilesPrePage = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.filesPageToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.lblFilesCurrentPage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.lblFilesTotalCount = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnFilesNextPage = new System.Windows.Forms.ToolStripButton();
            this.btnFilesLastPage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.cmbFilesPageSize = new System.Windows.Forms.ToolStripComboBox();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.txtFilesPageToGo = new System.Windows.Forms.ToolStripTextBox();
            this.btnGoFilesPage = new System.Windows.Forms.ToolStripButton();
            this.pnlStartFrom = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.bindingNavigator2 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.label3 = new System.Windows.Forms.Label();
            this.cmb_SeleAut = new System.Windows.Forms.ComboBox();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.btnReportPrintSettings = new System.Windows.Forms.Button();
            this.btn_ViewCompleteFiles = new System.Windows.Forms.Button();
            this.btn_ViewNewFiles = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsForDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsForFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.historyPaginateBar)).BeginInit();
            this.historyPaginateBar.SuspendLayout();
            this.pnlSearchBarForFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filesPaginateBar)).BeginInit();
            this.filesPaginateBar.SuspendLayout();
            this.pnlStartFrom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).BeginInit();
            this.bindingNavigator2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvDetail
            // 
            this.dgvDetail.AllowUserToAddRows = false;
            this.dgvDetail.AutoGenerateColumns = false;
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rowCheck});
            this.dgvDetail.DataSource = this.bsForDetail;
            this.dgvDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDetail.Location = new System.Drawing.Point(0, 25);
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvDetail.Size = new System.Drawing.Size(847, 174);
            this.dgvDetail.TabIndex = 4;
            this.dgvDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetail_CellContentClick);
            // 
            // rowCheck
            // 
            this.rowCheck.FalseValue = "0";
            this.rowCheck.Frozen = true;
            this.rowCheck.HeaderText = "Select";
            this.rowCheck.Name = "rowCheck";
            this.rowCheck.TrueValue = "1";
            this.rowCheck.Width = 45;
            // 
            // bsForDetail
            // 
            this.bsForDetail.DataSourceChanged += new System.EventHandler(this.bindingSource1_DataSourceChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(624, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Print";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cb_OnlyHistory
            // 
            this.cb_OnlyHistory.AutoSize = true;
            this.cb_OnlyHistory.Checked = true;
            this.cb_OnlyHistory.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_OnlyHistory.Location = new System.Drawing.Point(415, 31);
            this.cb_OnlyHistory.Name = "cb_OnlyHistory";
            this.cb_OnlyHistory.Size = new System.Drawing.Size(254, 17);
            this.cb_OnlyHistory.TabIndex = 8;
            this.cb_OnlyHistory.Text = "Do not send to printer, just record to print history.";
            this.cb_OnlyHistory.UseVisualStyleBackColor = true;
            // 
            // cb_AddHistory
            // 
            this.cb_AddHistory.AutoSize = true;
            this.cb_AddHistory.Checked = true;
            this.cb_AddHistory.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_AddHistory.Enabled = false;
            this.cb_AddHistory.Location = new System.Drawing.Point(415, 8);
            this.cb_AddHistory.Name = "cb_AddHistory";
            this.cb_AddHistory.Size = new System.Drawing.Size(77, 17);
            this.cb_AddHistory.TabIndex = 11;
            this.cb_AddHistory.Text = "AddHistory";
            this.cb_AddHistory.UseVisualStyleBackColor = true;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.BindingSource = this.bsForDetail;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(847, 25);
            this.bindingNavigator1.TabIndex = 12;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Select All";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Deselect all";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Reverse select";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(16, 108);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(101, 39);
            this.button7.TabIndex = 14;
            this.button7.Text = "Show history";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // lblEngine
            // 
            this.lblEngine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblEngine.ForeColor = System.Drawing.Color.Red;
            this.lblEngine.Location = new System.Drawing.Point(730, 3);
            this.lblEngine.Name = "lblEngine";
            this.lblEngine.Size = new System.Drawing.Size(238, 50);
            this.lblEngine.TabIndex = 15;
            // 
            // dgvHistory
            // 
            this.dgvHistory.AllowUserToAddRows = false;
            this.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHistory.Location = new System.Drawing.Point(0, 0);
            this.dgvHistory.Name = "dgvHistory";
            this.dgvHistory.ReadOnly = true;
            this.dgvHistory.Size = new System.Drawing.Size(847, 101);
            this.dgvHistory.TabIndex = 4;
            this.dgvHistory.Click += new System.EventHandler(this.dataGridView2_Click);
            // 
            // txtStart
            // 
            this.txtStart.Location = new System.Drawing.Point(69, 3);
            this.txtStart.Name = "txtStart";
            this.txtStart.Size = new System.Drawing.Size(36, 20);
            this.txtStart.TabIndex = 19;
            this.txtStart.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Start from";
            // 
            // cmb_SelectReportType
            // 
            this.cmb_SelectReportType.DropDownWidth = 180;
            this.cmb_SelectReportType.FormattingEnabled = true;
            this.cmb_SelectReportType.ImeMode = System.Windows.Forms.ImeMode.On;
            this.cmb_SelectReportType.Location = new System.Drawing.Point(121, 29);
            this.cmb_SelectReportType.Name = "cmb_SelectReportType";
            this.cmb_SelectReportType.Size = new System.Drawing.Size(287, 21);
            this.cmb_SelectReportType.TabIndex = 21;
            this.cmb_SelectReportType.SelectedValueChanged += new System.EventHandler(this.cmb_SelectReportType_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Select Report Type";
            // 
            // btnGetNewData
            // 
            this.btnGetNewData.Location = new System.Drawing.Point(16, 62);
            this.btnGetNewData.Name = "btnGetNewData";
            this.btnGetNewData.Size = new System.Drawing.Size(101, 39);
            this.btnGetNewData.TabIndex = 23;
            this.btnGetNewData.Text = "Get new data";
            this.btnGetNewData.UseVisualStyleBackColor = true;
            this.btnGetNewData.Click += new System.EventHandler(this.button10_Click);
            // 
            // dgvFiles
            // 
            this.dgvFiles.AllowUserToAddRows = false;
            this.dgvFiles.AutoGenerateColumns = false;
            this.dgvFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Select});
            this.dgvFiles.DataSource = this.bsForFiles;
            this.dgvFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFiles.Location = new System.Drawing.Point(0, 57);
            this.dgvFiles.Name = "dgvFiles";
            this.dgvFiles.RowHeadersVisible = false;
            this.dgvFiles.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvFiles.Size = new System.Drawing.Size(847, 72);
            this.dgvFiles.TabIndex = 25;
            this.dgvFiles.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFiles_CellClick);
            this.dgvFiles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFiles_CellContentClick);
            // 
            // Select
            // 
            this.Select.FalseValue = "0";
            this.Select.Frozen = true;
            this.Select.HeaderText = "Select";
            this.Select.Name = "Select";
            this.Select.TrueValue = "1";
            this.Select.Width = 45;
            // 
            // bsForFiles
            // 
            this.bsForFiles.DataSourceChanged += new System.EventHandler(this.bindingSource2_DataSourceChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(16, 362);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 39);
            this.button2.TabIndex = 26;
            this.button2.Text = "GetDetailByFiles";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(122, 56);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1MinSize = 0;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvDetail);
            this.splitContainer1.Panel2.Controls.Add(this.bindingNavigator1);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(847, 519);
            this.splitContainer1.SplitterDistance = 316;
            this.splitContainer1.TabIndex = 27;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.panel2);
            this.splitContainer2.Panel1.Controls.Add(this.panel1);
            this.splitContainer2.Panel1.Controls.Add(this.historyPaginateBar);
            this.splitContainer2.Panel1MinSize = 0;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvFiles);
            this.splitContainer2.Panel2.Controls.Add(this.pnlSearchBarForFiles);
            this.splitContainer2.Panel2.Controls.Add(this.lblFolder);
            this.splitContainer2.Panel2.Controls.Add(this.filesPaginateBar);
            this.splitContainer2.Panel2.Controls.Add(this.pnlStartFrom);
            this.splitContainer2.Panel2.Controls.Add(this.bindingNavigator2);
            this.splitContainer2.Panel2MinSize = 0;
            this.splitContainer2.Size = new System.Drawing.Size(847, 316);
            this.splitContainer2.SplitterDistance = 158;
            this.splitContainer2.TabIndex = 29;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvHistory);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(847, 101);
            this.panel2.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.txtDocumentNumber);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtFileName);
            this.panel1.Controls.Add(this.lblDocumentNumber);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(847, 32);
            this.panel1.TabIndex = 31;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(628, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 36;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtDocumentNumber
            // 
            this.txtDocumentNumber.Location = new System.Drawing.Point(422, 6);
            this.txtDocumentNumber.Name = "txtDocumentNumber";
            this.txtDocumentNumber.Size = new System.Drawing.Size(200, 20);
            this.txtDocumentNumber.TabIndex = 35;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "File Name";
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(88, 6);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(200, 20);
            this.txtFileName.TabIndex = 33;
            // 
            // lblDocumentNumber
            // 
            this.lblDocumentNumber.AutoSize = true;
            this.lblDocumentNumber.Location = new System.Drawing.Point(307, 9);
            this.lblDocumentNumber.Name = "lblDocumentNumber";
            this.lblDocumentNumber.Size = new System.Drawing.Size(96, 13);
            this.lblDocumentNumber.TabIndex = 34;
            this.lblDocumentNumber.Text = "Document Number";
            // 
            // historyPaginateBar
            // 
            this.historyPaginateBar.AddNewItem = null;
            this.historyPaginateBar.CountItem = this.lblHistoryTotalPage;
            this.historyPaginateBar.CountItemFormat = "{0}";
            this.historyPaginateBar.DeleteItem = null;
            this.historyPaginateBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.historyPaginateBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnHistoryFirstPage,
            this.btnHistoryPrePage,
            this.toolStripSeparator5,
            this.toolStripLabel6,
            this.lblHistoryCurrentPage,
            this.toolStripLabel8,
            this.lblHistoryTotalPage,
            this.toolStripLabel9,
            this.lblHistoryTotalCount,
            this.toolStripSeparator6,
            this.btnHistoryNextPage,
            this.btnHistoryLastPage,
            this.toolStripSeparator7,
            this.toolStripLabel11,
            this.cmbHistoryPageSize,
            this.toolStripSeparator8,
            this.txtHistoryPageToGo,
            this.btnGoHistoryPage});
            this.historyPaginateBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.historyPaginateBar.Location = new System.Drawing.Point(0, 133);
            this.historyPaginateBar.MoveFirstItem = this.btnHistoryFirstPage;
            this.historyPaginateBar.MoveLastItem = this.btnHistoryLastPage;
            this.historyPaginateBar.MoveNextItem = this.btnHistoryNextPage;
            this.historyPaginateBar.MovePreviousItem = this.btnHistoryPrePage;
            this.historyPaginateBar.Name = "historyPaginateBar";
            this.historyPaginateBar.PositionItem = null;
            this.historyPaginateBar.Size = new System.Drawing.Size(847, 25);
            this.historyPaginateBar.TabIndex = 30;
            this.historyPaginateBar.Text = "historyPageNavigator";
            // 
            // lblHistoryTotalPage
            // 
            this.lblHistoryTotalPage.Name = "lblHistoryTotalPage";
            this.lblHistoryTotalPage.Size = new System.Drawing.Size(21, 22);
            this.lblHistoryTotalPage.Text = "{0}";
            this.lblHistoryTotalPage.ToolTipText = "Total number of items";
            // 
            // btnHistoryFirstPage
            // 
            this.btnHistoryFirstPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHistoryFirstPage.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoryFirstPage.Image")));
            this.btnHistoryFirstPage.Name = "btnHistoryFirstPage";
            this.btnHistoryFirstPage.RightToLeftAutoMirrorImage = true;
            this.btnHistoryFirstPage.Size = new System.Drawing.Size(23, 22);
            this.btnHistoryFirstPage.Text = "Move first";
            this.btnHistoryFirstPage.Click += new System.EventHandler(this.historyGetPage_Click);
            // 
            // btnHistoryPrePage
            // 
            this.btnHistoryPrePage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHistoryPrePage.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoryPrePage.Image")));
            this.btnHistoryPrePage.Name = "btnHistoryPrePage";
            this.btnHistoryPrePage.RightToLeftAutoMirrorImage = true;
            this.btnHistoryPrePage.Size = new System.Drawing.Size(23, 22);
            this.btnHistoryPrePage.Text = "Move previous";
            this.btnHistoryPrePage.Click += new System.EventHandler(this.historyGetPage_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(33, 22);
            this.toolStripLabel6.Text = "page";
            // 
            // lblHistoryCurrentPage
            // 
            this.lblHistoryCurrentPage.Name = "lblHistoryCurrentPage";
            this.lblHistoryCurrentPage.Size = new System.Drawing.Size(13, 22);
            this.lblHistoryCurrentPage.Text = "0";
            // 
            // toolStripLabel8
            // 
            this.toolStripLabel8.Name = "toolStripLabel8";
            this.toolStripLabel8.Size = new System.Drawing.Size(12, 22);
            this.toolStripLabel8.Text = "/";
            // 
            // toolStripLabel9
            // 
            this.toolStripLabel9.Name = "toolStripLabel9";
            this.toolStripLabel9.Size = new System.Drawing.Size(36, 22);
            this.toolStripLabel9.Text = "items";
            // 
            // lblHistoryTotalCount
            // 
            this.lblHistoryTotalCount.Name = "lblHistoryTotalCount";
            this.lblHistoryTotalCount.Size = new System.Drawing.Size(13, 22);
            this.lblHistoryTotalCount.Text = "0";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // btnHistoryNextPage
            // 
            this.btnHistoryNextPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHistoryNextPage.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoryNextPage.Image")));
            this.btnHistoryNextPage.Name = "btnHistoryNextPage";
            this.btnHistoryNextPage.RightToLeftAutoMirrorImage = true;
            this.btnHistoryNextPage.Size = new System.Drawing.Size(23, 22);
            this.btnHistoryNextPage.Tag = "";
            this.btnHistoryNextPage.Click += new System.EventHandler(this.historyGetPage_Click);
            // 
            // btnHistoryLastPage
            // 
            this.btnHistoryLastPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnHistoryLastPage.Image = ((System.Drawing.Image)(resources.GetObject("btnHistoryLastPage.Image")));
            this.btnHistoryLastPage.Name = "btnHistoryLastPage";
            this.btnHistoryLastPage.RightToLeftAutoMirrorImage = true;
            this.btnHistoryLastPage.Size = new System.Drawing.Size(23, 22);
            this.btnHistoryLastPage.Text = "Move last";
            this.btnHistoryLastPage.Click += new System.EventHandler(this.historyGetPage_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel11
            // 
            this.toolStripLabel11.Name = "toolStripLabel11";
            this.toolStripLabel11.Size = new System.Drawing.Size(55, 22);
            this.toolStripLabel11.Text = "page size";
            // 
            // cmbHistoryPageSize
            // 
            this.cmbHistoryPageSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHistoryPageSize.DropDownWidth = 75;
            this.cmbHistoryPageSize.Items.AddRange(new object[] {
            "20",
            "40",
            "60",
            "80",
            "100"});
            this.cmbHistoryPageSize.Name = "cmbHistoryPageSize";
            this.cmbHistoryPageSize.Size = new System.Drawing.Size(75, 25);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // txtHistoryPageToGo
            // 
            this.txtHistoryPageToGo.AutoSize = false;
            this.txtHistoryPageToGo.Name = "txtHistoryPageToGo";
            this.txtHistoryPageToGo.Size = new System.Drawing.Size(50, 23);
            this.txtHistoryPageToGo.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnGoHistoryPage
            // 
            this.btnGoHistoryPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnGoHistoryPage.Image = ((System.Drawing.Image)(resources.GetObject("btnGoHistoryPage.Image")));
            this.btnGoHistoryPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGoHistoryPage.Name = "btnGoHistoryPage";
            this.btnGoHistoryPage.Size = new System.Drawing.Size(26, 22);
            this.btnGoHistoryPage.Text = "Go";
            this.btnGoHistoryPage.Click += new System.EventHandler(this.historyGetPage_Click);
            // 
            // pnlSearchBarForFiles
            // 
            this.pnlSearchBarForFiles.Controls.Add(this.btnSearchFileNameForFileList);
            this.pnlSearchBarForFiles.Controls.Add(this.label5);
            this.pnlSearchBarForFiles.Controls.Add(this.tbSearchFileNameForFileList);
            this.pnlSearchBarForFiles.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearchBarForFiles.Location = new System.Drawing.Point(0, 25);
            this.pnlSearchBarForFiles.Name = "pnlSearchBarForFiles";
            this.pnlSearchBarForFiles.Size = new System.Drawing.Size(847, 32);
            this.pnlSearchBarForFiles.TabIndex = 32;
            // 
            // btnSearchFileNameForFileList
            // 
            this.btnSearchFileNameForFileList.Location = new System.Drawing.Point(295, 5);
            this.btnSearchFileNameForFileList.Name = "btnSearchFileNameForFileList";
            this.btnSearchFileNameForFileList.Size = new System.Drawing.Size(75, 23);
            this.btnSearchFileNameForFileList.TabIndex = 36;
            this.btnSearchFileNameForFileList.Text = "Search";
            this.btnSearchFileNameForFileList.UseVisualStyleBackColor = true;
            this.btnSearchFileNameForFileList.Click += new System.EventHandler(this.btnSearchFileNameForFileList_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "File Name";
            // 
            // tbSearchFileNameForFileList
            // 
            this.tbSearchFileNameForFileList.Location = new System.Drawing.Point(88, 6);
            this.tbSearchFileNameForFileList.Name = "tbSearchFileNameForFileList";
            this.tbSearchFileNameForFileList.Size = new System.Drawing.Size(200, 20);
            this.tbSearchFileNameForFileList.TabIndex = 33;
            // 
            // lblFolder
            // 
            this.lblFolder.AutoSize = true;
            this.lblFolder.Location = new System.Drawing.Point(499, 2);
            this.lblFolder.Name = "lblFolder";
            this.lblFolder.Size = new System.Drawing.Size(0, 13);
            this.lblFolder.TabIndex = 30;
            // 
            // filesPaginateBar
            // 
            this.filesPaginateBar.AddNewItem = null;
            this.filesPaginateBar.CountItem = this.lblFilesTotalPage;
            this.filesPaginateBar.CountItemFormat = "{0}";
            this.filesPaginateBar.DeleteItem = null;
            this.filesPaginateBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.filesPaginateBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnFilesFirstPage,
            this.btnFilesPrePage,
            this.bindingNavigatorSeparator3,
            this.filesPageToolStripLabel,
            this.lblFilesCurrentPage,
            this.toolStripLabel3,
            this.lblFilesTotalPage,
            this.toolStripLabel4,
            this.lblFilesTotalCount,
            this.bindingNavigatorSeparator4,
            this.btnFilesNextPage,
            this.btnFilesLastPage,
            this.toolStripSeparator4,
            this.toolStripLabel2,
            this.cmbFilesPageSize,
            this.bindingNavigatorSeparator5,
            this.txtFilesPageToGo,
            this.btnGoFilesPage});
            this.filesPaginateBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.filesPaginateBar.Location = new System.Drawing.Point(0, 129);
            this.filesPaginateBar.MoveFirstItem = this.btnFilesFirstPage;
            this.filesPaginateBar.MoveLastItem = this.btnFilesLastPage;
            this.filesPaginateBar.MoveNextItem = this.btnFilesNextPage;
            this.filesPaginateBar.MovePreviousItem = this.btnFilesPrePage;
            this.filesPaginateBar.Name = "filesPaginateBar";
            this.filesPaginateBar.PositionItem = null;
            this.filesPaginateBar.Size = new System.Drawing.Size(847, 25);
            this.filesPaginateBar.TabIndex = 29;
            this.filesPaginateBar.Text = "filesPageNavigator";
            // 
            // lblFilesTotalPage
            // 
            this.lblFilesTotalPage.Name = "lblFilesTotalPage";
            this.lblFilesTotalPage.Size = new System.Drawing.Size(21, 22);
            this.lblFilesTotalPage.Text = "{0}";
            this.lblFilesTotalPage.ToolTipText = "Total number of items";
            // 
            // btnFilesFirstPage
            // 
            this.btnFilesFirstPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFilesFirstPage.Image = ((System.Drawing.Image)(resources.GetObject("btnFilesFirstPage.Image")));
            this.btnFilesFirstPage.Name = "btnFilesFirstPage";
            this.btnFilesFirstPage.RightToLeftAutoMirrorImage = true;
            this.btnFilesFirstPage.Size = new System.Drawing.Size(23, 22);
            this.btnFilesFirstPage.Text = "Move first";
            this.btnFilesFirstPage.Click += new System.EventHandler(this.filesGetPage_Click);
            // 
            // btnFilesPrePage
            // 
            this.btnFilesPrePage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFilesPrePage.Image = ((System.Drawing.Image)(resources.GetObject("btnFilesPrePage.Image")));
            this.btnFilesPrePage.Name = "btnFilesPrePage";
            this.btnFilesPrePage.RightToLeftAutoMirrorImage = true;
            this.btnFilesPrePage.Size = new System.Drawing.Size(23, 22);
            this.btnFilesPrePage.Text = "Move previous";
            this.btnFilesPrePage.Click += new System.EventHandler(this.filesGetPage_Click);
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // filesPageToolStripLabel
            // 
            this.filesPageToolStripLabel.Name = "filesPageToolStripLabel";
            this.filesPageToolStripLabel.Size = new System.Drawing.Size(33, 22);
            this.filesPageToolStripLabel.Text = "page";
            // 
            // lblFilesCurrentPage
            // 
            this.lblFilesCurrentPage.Name = "lblFilesCurrentPage";
            this.lblFilesCurrentPage.Size = new System.Drawing.Size(13, 22);
            this.lblFilesCurrentPage.Text = "0";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(12, 22);
            this.toolStripLabel3.Text = "/";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(36, 22);
            this.toolStripLabel4.Text = "items";
            // 
            // lblFilesTotalCount
            // 
            this.lblFilesTotalCount.Name = "lblFilesTotalCount";
            this.lblFilesTotalCount.Size = new System.Drawing.Size(13, 22);
            this.lblFilesTotalCount.Text = "0";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnFilesNextPage
            // 
            this.btnFilesNextPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFilesNextPage.Image = ((System.Drawing.Image)(resources.GetObject("btnFilesNextPage.Image")));
            this.btnFilesNextPage.Name = "btnFilesNextPage";
            this.btnFilesNextPage.RightToLeftAutoMirrorImage = true;
            this.btnFilesNextPage.Size = new System.Drawing.Size(23, 22);
            this.btnFilesNextPage.Tag = "";
            this.btnFilesNextPage.Click += new System.EventHandler(this.filesGetPage_Click);
            // 
            // btnFilesLastPage
            // 
            this.btnFilesLastPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFilesLastPage.Image = ((System.Drawing.Image)(resources.GetObject("btnFilesLastPage.Image")));
            this.btnFilesLastPage.Name = "btnFilesLastPage";
            this.btnFilesLastPage.RightToLeftAutoMirrorImage = true;
            this.btnFilesLastPage.Size = new System.Drawing.Size(23, 22);
            this.btnFilesLastPage.Text = "Move last";
            this.btnFilesLastPage.Click += new System.EventHandler(this.filesGetPage_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(55, 22);
            this.toolStripLabel2.Text = "page size";
            // 
            // cmbFilesPageSize
            // 
            this.cmbFilesPageSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFilesPageSize.DropDownWidth = 75;
            this.cmbFilesPageSize.Items.AddRange(new object[] {
            "20",
            "40",
            "60",
            "80",
            "100"});
            this.cmbFilesPageSize.Name = "cmbFilesPageSize";
            this.cmbFilesPageSize.Size = new System.Drawing.Size(75, 25);
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // txtFilesPageToGo
            // 
            this.txtFilesPageToGo.AutoSize = false;
            this.txtFilesPageToGo.Name = "txtFilesPageToGo";
            this.txtFilesPageToGo.Size = new System.Drawing.Size(50, 23);
            this.txtFilesPageToGo.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnGoFilesPage
            // 
            this.btnGoFilesPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnGoFilesPage.Image = ((System.Drawing.Image)(resources.GetObject("btnGoFilesPage.Image")));
            this.btnGoFilesPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGoFilesPage.Name = "btnGoFilesPage";
            this.btnGoFilesPage.Size = new System.Drawing.Size(26, 22);
            this.btnGoFilesPage.Text = "Go";
            this.btnGoFilesPage.Click += new System.EventHandler(this.filesGetPage_Click);
            // 
            // pnlStartFrom
            // 
            this.pnlStartFrom.Controls.Add(this.button3);
            this.pnlStartFrom.Controls.Add(this.label1);
            this.pnlStartFrom.Controls.Add(this.txtStart);
            this.pnlStartFrom.Location = new System.Drawing.Point(325, -1);
            this.pnlStartFrom.Name = "pnlStartFrom";
            this.pnlStartFrom.Size = new System.Drawing.Size(167, 25);
            this.pnlStartFrom.TabIndex = 28;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(114, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(37, 23);
            this.button3.TabIndex = 21;
            this.button3.Text = "set";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // bindingNavigator2
            // 
            this.bindingNavigator2.AddNewItem = null;
            this.bindingNavigator2.BindingSource = this.bsForFiles;
            this.bindingNavigator2.CountItem = this.toolStripLabel1;
            this.bindingNavigator2.DeleteItem = null;
            this.bindingNavigator2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton6,
            this.toolStripButton7,
            this.toolStripSeparator3,
            this.toolStripButton8,
            this.toolStripButton9,
            this.toolStripButton10});
            this.bindingNavigator2.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator2.MoveFirstItem = this.toolStripButton4;
            this.bindingNavigator2.MoveLastItem = this.toolStripButton7;
            this.bindingNavigator2.MoveNextItem = this.toolStripButton6;
            this.bindingNavigator2.MovePreviousItem = this.toolStripButton5;
            this.bindingNavigator2.Name = "bindingNavigator2";
            this.bindingNavigator2.PositionItem = this.toolStripTextBox1;
            this.bindingNavigator2.Size = new System.Drawing.Size(847, 25);
            this.bindingNavigator2.TabIndex = 28;
            this.bindingNavigator2.Text = "bindingNavigator2";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel1.Text = "of {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.RightToLeftAutoMirrorImage = true;
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Move first";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Move previous";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "Move next";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.RightToLeftAutoMirrorImage = true;
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "Move last";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton8.Text = "Select All";
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "Deselect all";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton10.Text = "Reverse select";
            this.toolStripButton10.Click += new System.EventHandler(this.toolStripButton10_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Select Authority";
            // 
            // cmb_SeleAut
            // 
            this.cmb_SeleAut.FormattingEnabled = true;
            this.cmb_SeleAut.Items.AddRange(new object[] {
            "Cape Town"});
            this.cmb_SeleAut.Location = new System.Drawing.Point(121, 5);
            this.cmb_SeleAut.Name = "cmb_SeleAut";
            this.cmb_SeleAut.Size = new System.Drawing.Size(287, 21);
            this.cmb_SeleAut.TabIndex = 29;
            this.cmb_SeleAut.SelectedIndexChanged += new System.EventHandler(this.cmb_SeleAut_SelectedIndexChanged);
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "yyyy-MM-dd";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(16, 56);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(100, 20);
            this.dtpFrom.TabIndex = 30;
            this.dtpFrom.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpFrom.Visible = false;
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "yyyy-MM-dd";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(16, 82);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(100, 20);
            this.dtpTo.TabIndex = 31;
            this.dtpTo.Value = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpTo.Visible = false;
            // 
            // btnReportPrintSettings
            // 
            this.btnReportPrintSettings.Location = new System.Drawing.Point(498, 5);
            this.btnReportPrintSettings.Name = "btnReportPrintSettings";
            this.btnReportPrintSettings.Size = new System.Drawing.Size(120, 23);
            this.btnReportPrintSettings.TabIndex = 32;
            this.btnReportPrintSettings.Text = "Report Print Settings";
            this.btnReportPrintSettings.UseVisualStyleBackColor = true;
            this.btnReportPrintSettings.Click += new System.EventHandler(this.btnReportPrintSettings_Click);
            // 
            // btn_ViewCompleteFiles
            // 
            this.btn_ViewCompleteFiles.Location = new System.Drawing.Point(16, 200);
            this.btn_ViewCompleteFiles.Name = "btn_ViewCompleteFiles";
            this.btn_ViewCompleteFiles.Size = new System.Drawing.Size(101, 39);
            this.btn_ViewCompleteFiles.TabIndex = 33;
            this.btn_ViewCompleteFiles.Text = "View Complete files";
            this.btn_ViewCompleteFiles.UseVisualStyleBackColor = true;
            this.btn_ViewCompleteFiles.Click += new System.EventHandler(this.btn_ViewCompleteFiles_Click);
            // 
            // btn_ViewNewFiles
            // 
            this.btn_ViewNewFiles.Location = new System.Drawing.Point(16, 155);
            this.btn_ViewNewFiles.Name = "btn_ViewNewFiles";
            this.btn_ViewNewFiles.Size = new System.Drawing.Size(101, 39);
            this.btn_ViewNewFiles.TabIndex = 34;
            this.btn_ViewNewFiles.Text = "View New Files";
            this.btn_ViewNewFiles.UseVisualStyleBackColor = true;
            this.btn_ViewNewFiles.Click += new System.EventHandler(this.btn_ViewNewFiles_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 585);
            this.Controls.Add(this.btn_ViewNewFiles);
            this.Controls.Add(this.btn_ViewCompleteFiles);
            this.Controls.Add(this.btnReportPrintSettings);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.cmb_SeleAut);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnGetNewData);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmb_SelectReportType);
            this.Controls.Add(this.lblEngine);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.cb_AddHistory);
            this.Controls.Add(this.cb_OnlyHistory);
            this.Controls.Add(this.button1);
            this.Name = "frmMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

            //add by Rachel 20140818 for 5347
            string versionNumber = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.Text = "Report Print"+"   "+"Version Number: "+versionNumber;
            //end add by Rachel 20140818 for 5347

            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsForDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsForFiles)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.historyPaginateBar)).EndInit();
            this.historyPaginateBar.ResumeLayout(false);
            this.historyPaginateBar.PerformLayout();
            this.pnlSearchBarForFiles.ResumeLayout(false);
            this.pnlSearchBarForFiles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filesPaginateBar)).EndInit();
            this.filesPaginateBar.ResumeLayout(false);
            this.filesPaginateBar.PerformLayout();
            this.pnlStartFrom.ResumeLayout(false);
            this.pnlStartFrom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).EndInit();
            this.bindingNavigator2.ResumeLayout(false);
            this.bindingNavigator2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDetail;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox cb_OnlyHistory;
        private System.Windows.Forms.CheckBox cb_AddHistory;
        private System.Windows.Forms.BindingSource bsForDetail;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label lblEngine;
        private System.Windows.Forms.DataGridView dgvHistory;
        private System.Windows.Forms.TextBox txtStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_SelectReportType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetNewData;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.DataGridView dgvFiles;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.BindingNavigator bindingNavigator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.Panel pnlStartFrom;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.BindingSource bsForFiles;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Select;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmb_SeleAut;
        private System.Windows.Forms.DataGridViewCheckBoxColumn rowCheck;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.BindingNavigator filesPaginateBar;
        private System.Windows.Forms.ToolStripLabel lblFilesTotalPage;
        private System.Windows.Forms.ToolStripButton btnFilesFirstPage;
        private System.Windows.Forms.ToolStripButton btnFilesPrePage;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton btnFilesNextPage;
        private System.Windows.Forms.ToolStripButton btnFilesLastPage;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private System.Windows.Forms.ToolStripTextBox txtFilesPageToGo;
        private System.Windows.Forms.ToolStripButton btnGoFilesPage;
        private System.Windows.Forms.ToolStripLabel filesPageToolStripLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel lblFilesCurrentPage;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripLabel lblFilesTotalCount;
        private System.Windows.Forms.BindingNavigator historyPaginateBar;
        private System.Windows.Forms.ToolStripLabel lblHistoryTotalPage;
        private System.Windows.Forms.ToolStripButton btnHistoryFirstPage;
        private System.Windows.Forms.ToolStripButton btnHistoryPrePage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
        private System.Windows.Forms.ToolStripLabel lblHistoryCurrentPage;
        private System.Windows.Forms.ToolStripLabel toolStripLabel8;
        private System.Windows.Forms.ToolStripLabel toolStripLabel9;
        private System.Windows.Forms.ToolStripLabel lblHistoryTotalCount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton btnHistoryNextPage;
        private System.Windows.Forms.ToolStripButton btnHistoryLastPage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel toolStripLabel11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripTextBox txtHistoryPageToGo;
        private System.Windows.Forms.ToolStripButton btnGoHistoryPage;
        private System.Windows.Forms.ToolStripComboBox cmbHistoryPageSize;
        private System.Windows.Forms.ToolStripComboBox cmbFilesPageSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.TextBox txtDocumentNumber;
        private System.Windows.Forms.Label lblDocumentNumber;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnReportPrintSettings;
        private System.Windows.Forms.Button btn_ViewCompleteFiles;
        private System.Windows.Forms.Button btn_ViewNewFiles;
        private System.Windows.Forms.Panel pnlSearchBarForFiles;
        private System.Windows.Forms.Button btnSearchFileNameForFileList;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbSearchFileNameForFileList;
        private System.Windows.Forms.Label lblFolder;
    }
}

