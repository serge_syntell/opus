﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Management;

namespace SIL.AARTO.ReportEngine
{
    /// <summary>
    /// Add by Brian 2013-10-18
    /// Report Print Settings Form
    /// </summary>
    public partial class ReportPrintSettingsForm : Form
    {
        //Data List Properties
        private List<PrinterSettings> PrinterSettingsList { get; set; }
        private List<ReportSettings> ReportSettingsList { get; set; }
        private List<LocalPcSettings> LocalPcSettingsList { get; set; }

        //Static Properties
        private static string MacAddress { get; set; }
        public static List<ReportConfig> ReportConfigList { get; set; }
        public static int AutIntNo { get; set; }

        //Select Printer Setttings Data List for DropDownList Control and Data Binding
        private List<PrinterSettings> _SelectPrinterSettingsList = new List<PrinterSettings>();

        //Data Service Instantiate
        private PrinterSettingsService _PrinterSettingsService = null;
        private ReportSettingsService _ReportSettingsService = null;
        private LocalPcSettingsService _LocalPcSettingsService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public ReportPrintSettingsForm()
        {
            InitializeComponent();

            this.LoadLPS_ReportDownList();
            this.LoadRS_ReportDownList();
            
            this._PrinterSettingsService = new PrinterSettingsService();
            this.BindPrinterSettingsColumns();

            this._ReportSettingsService = new ReportSettingsService();
            this.BindReportSettingsColumns();

            MacAddress = GetMACAddress();
            this._LocalPcSettingsService = new LocalPcSettingsService();
            this.BindLocalPcSettingsColumns();

            this.LoadLPS_PrinterDownList();
            this.GetLocalPcSettingsList();
        }

        /// <summary>
        /// Execute ShowDialog Form for edit form
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Execute<T>(IEntity entity = null) where T : IEditForm
        {
            using (T frm = Activator.CreateInstance<T>())
            {
                frm.Entity = entity;
                frm.EntityLoad();
                return (DialogResult.OK == frm.ShowDialog());
            }
        }

        /// <summary>
        /// The static mehtod for ShowDialog current Form
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="autName"></param>
        public static void ShowForm(int width, int height, string autName)
        {
            using (ReportPrintSettingsForm frm = new ReportPrintSettingsForm())
            {
                frm.Size = new Size(width - 100, height - 100);
                frm.Text = string.Concat(autName, " ", frm.Text);
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// The click event method add local pc settings of Button Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddLocalPcSettings_Click(object sender, EventArgs e)
        {
            if (this.Execute<LocalPcSettingsEditForm>())
            {
                this.cmbLPS_Report.SelectedIndex = 0;
                this.cmbLPS_PsIntNo.SelectedIndex = 0;
                this.GetLocalPcSettingsList();
            }
        }

        /// <summary>
        /// The click event method add printer settings of Button Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnbtnAddPrinterSettings_Click(object sender, EventArgs e)
        {
            if (this.Execute<PrinterSettingsEditForm>())
            {
                this.txtPrinterName.Text = string.Empty;
                GetPrinterSettingsList();
            }
        }

        /// <summary>
        /// The click event method add report settings of Button Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddReportSettings_Click(object sender, EventArgs e)
        {
            if (this.Execute<ReportSettingsEditForm>())
            {
                this.cmbRS_Report.SelectedIndex = 0;
                this.cmbRS_PsIntNo.SelectedIndex = 0;
                this.GetReportSettingsList();
            }
        }

        /// <summary>
        /// The select index changed event method of TabControl Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender as TabControl).SelectedIndex == 0)
            {
                this.LoadLPS_PrinterDownList();
                this.GetLocalPcSettingsList();
            }
            else if ((sender as TabControl).SelectedIndex == 1)
            {
                this.LoadPrinterDownList();
                this.GetReportSettingsList();
            }
            else if ((sender as TabControl).SelectedIndex == 2)
            {
                this.GetPrinterSettingsList();
            }
        }

        /// <summary>
        /// The static method get current pc mac address
        /// </summary>
        /// <returns></returns>
        public static string GetMACAddress()
        {
            if (!string.IsNullOrEmpty(MacAddress))
            {
                return MacAddress;
            }
            string mac = "00:00:00:00:00:00";
            try
            {
                ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach (ManagementObject mo in moc)
                {
                    if (bool.Parse(mo["IPEnabled"].ToString()))
                    {
                        mac = mo["MacAddress"].ToString();
                        break;
                    }
                }
            }
            catch
            {
            }
            return mac;
        }

        /// <summary>
        /// Load Report config data list for local pc settings of DropDownList Control
        /// </summary>
        private void LoadLPS_ReportDownList()
        {
            this.cmbLPS_Report.DataSource = ReportPrintSettingsForm.ReportConfigList;
            this.cmbLPS_Report.DisplayMember = "ReportName";
            this.cmbLPS_Report.ValueMember = "RcIntNo";
        }

        /// <summary>
        /// Load Report config data list for report settings of DropDownList Control
        /// </summary>
        private void LoadRS_ReportDownList()
        {
            this.cmbRS_Report.DataSource = ReportPrintSettingsForm.ReportConfigList;
            this.cmbRS_Report.DisplayMember = "ReportName";
            this.cmbRS_Report.ValueMember = "RcIntNo";
        }

        /// <summary>
        /// Load printer settings data list for report settings of DropDownList Control
        /// </summary>
        private void LoadPrinterDownList()
        {
            this._SelectPrinterSettingsList = this._PrinterSettingsService.GetAll().OrderBy(o => o.PsIntNo).ToList();
            this._SelectPrinterSettingsList.Insert(0, new PrinterSettings() { PsIntNo = 0, PsName = string.Empty });
            this.cmbRS_PsIntNo.DataSource = this._SelectPrinterSettingsList;
            this.cmbRS_PsIntNo.DisplayMember = "PsName";
            this.cmbRS_PsIntNo.ValueMember = "PsIntNo";
        }

        /// <summary>
        /// Load printer settings data list for local pc settings of DropDownList Control
        /// </summary>
        private void LoadLPS_PrinterDownList()
        {
            this._SelectPrinterSettingsList = this._PrinterSettingsService.GetAll().OrderBy(o => o.PsIntNo).ToList();
            this._SelectPrinterSettingsList.Insert(0, new PrinterSettings() { PsIntNo = 0, PsName = string.Empty });
            this.cmbLPS_PsIntNo.DataSource = this._SelectPrinterSettingsList;
            this.cmbLPS_PsIntNo.DisplayMember = "PsName";
            this.cmbLPS_PsIntNo.ValueMember = "PsIntNo";
        }

        /// <summary>
        /// Get local pc settings data list for binding DataGridView Control
        /// </summary>
        private void GetLocalPcSettingsList()
        {
            LocalPcSettingsQuery query = new LocalPcSettingsQuery();
            query.Append(LocalPcSettingsColumn.AutIntNo, AutIntNo.ToString());
            query.Append(LocalPcSettingsColumn.PcmacAddress, MacAddress);
            this.LocalPcSettingsList = this._LocalPcSettingsService.Find(query).OrderByDescending(o => o.LpsIntNo).ToList();
            this.BindDataGridView(this.dgvLocalPcSettings, this.LocalPcSettingsList);
        }

        /// <summary>
        /// Get report settings data list for binding DataGridView Control
        /// </summary>
        private void GetReportSettingsList()
        {
            this.ReportSettingsList = this._ReportSettingsService.GetByAutIntNo(AutIntNo).OrderByDescending(o => o.RsIntNo).ToList();
            this.BindDataGridView(this.dgvReportSettings, this.ReportSettingsList);
        }

        /// <summary>
        ///  Get printer settings data list for binding DataGridView Control
        /// </summary>
        private void GetPrinterSettingsList()
        {
            this.PrinterSettingsList = this._PrinterSettingsService.GetByAutIntNo(AutIntNo).OrderByDescending(o => o.PsIntNo).ToList();
            this.BindDataGridView(this.dgvPrinterSettings, this.PrinterSettingsList);
        }

        /// <summary>
        /// Bind DataGridView Control of DataSource
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="dataSource"></param>
        private void BindDataGridView(DataGridView dgv, object dataSource)
        {
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = dataSource;
            dgv.RowsDefaultCellStyle.BackColor = SystemColors.MenuBar;//Color.Bisque;
            dgv.AlternatingRowsDefaultCellStyle.BackColor = SystemColors.Window;//second
        }

        /// <summary>
        /// Bind local pc settings DataGridViewColumn list
        /// </summary>
        private void BindLocalPcSettingsColumns()
        {
            List<DataGridViewColumn> columnList = new List<DataGridViewColumn>();
            columnList.Add(this.AddDataColumn("LpsIntNo", "LpsIntNo", false, 50));
            columnList.Add(this.AddDataColumn("PsIntNo", "PsIntNo", false, 50));
            columnList.Add(this.AddDataColumn("RcIntNo", "RcIntNo", false, 50));
            columnList.Add(this.AddDataColumn("", "Report", true, 250));
            columnList.Add(this.AddDataColumn("", "Printer", true, 150));
            columnList.Add(this.AddDataColumn("LptPort", "LPT Port", true, 100));
            columnList.Add(this.AddDataColumn("PcmacAddress", "PC MAC Address", true, 120));
            columnList.Add(this.AddDataColumn("Remark", "Remark", true, 200));
            columnList.Add(this.AddDataColumn("LastUser", "Last Update User", true, 120));
            columnList.Add(this.AddDataColumn("CreateDate", "Create Date", true, 100));
            columnList.Add(this.AddDataLinkColumn("Edit", "Edit", true, 60));
            columnList.Add(this.AddDataLinkColumn("Delete", "Delete", true, 60));
            this.dgvLocalPcSettings.Columns.AddRange(columnList.ToArray());
        }

        /// <summary>
        /// Bind report settings DataGridViewColumn list
        /// </summary>
        private void BindReportSettingsColumns()
        {
            List<DataGridViewColumn> columnList = new List<DataGridViewColumn>();
            columnList.Add(this.AddDataColumn("RsIntNo", "RsIntNo", false, 50));
            columnList.Add(this.AddDataColumn("PsIntNo", "PsIntNo", false, 50));
            columnList.Add(this.AddDataColumn("RcIntNo", "RcIntNo", false, 50));
            columnList.Add(this.AddDataColumn("", "Report", true, 250));
            columnList.Add(this.AddDataColumn("", "Printer", true, 150));
            columnList.Add(this.AddDataColumn("LptPort", "LPT Port", true, 100));
            columnList.Add(this.AddDataColumn("IsDefault", "Is Default Printer", true, 120));
            columnList.Add(this.AddDataColumn("Remark", "Remark", true, 200));
            columnList.Add(this.AddDataColumn("LastUser", "Last Update User", true, 120));
            columnList.Add(this.AddDataColumn("CreateDate", "Create Date", true, 100));
            columnList.Add(this.AddDataLinkColumn("Edit", "Edit", true, 60));
            columnList.Add(this.AddDataLinkColumn("Delete", "Delete", true, 60));
            this.dgvReportSettings.Columns.AddRange(columnList.ToArray());
        }

        /// <summary>
        /// Bind printer settings DataGridViewColumn list
        /// </summary>
        private void BindPrinterSettingsColumns()
        {
            List<DataGridViewColumn> columnList = new List<DataGridViewColumn>();
            columnList.Add(this.AddDataColumn("PsIntNo", "PsIntNo", false, 50));
            columnList.Add(this.AddDataColumn("PsName", "Printer Name", true, 250));
            columnList.Add(this.AddDataColumn("PsPath", "Printer Path", true, 250));
            columnList.Add(this.AddDataColumn("Remark", "Remark", true, 200));
            columnList.Add(this.AddDataColumn("LastUser", "Last Update User", true, 120));
            columnList.Add(this.AddDataColumn("CreateDate", "Create Date", true, 100));
            columnList.Add(this.AddDataLinkColumn("Edit", "Edit", true, 60));
            columnList.Add(this.AddDataLinkColumn("Delete", "Delete", true, 60));
            this.dgvPrinterSettings.Columns.AddRange(columnList.ToArray());
        }

        /// <summary>
        /// Draw row index for DataGridView Control
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="e"></param>
        private void DrawIndex(DataGridView dgv, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
                                                e.RowBounds.Location.Y,
                                                dgv.RowHeadersWidth - 4, e.RowBounds.Height);

            TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                                    dgv.RowHeadersDefaultCellStyle.Font,
                                    rectangle,
                                    dgv.RowHeadersDefaultCellStyle.ForeColor,
                                    TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
        }

        /// <summary>
        /// Add data column for DataGridViewTextBoxColumn
        /// </summary>
        /// <param name="dataPropertyName"></param>
        /// <param name="headerText"></param>
        /// <param name="visible"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        private DataGridViewTextBoxColumn AddDataColumn(string dataPropertyName, string headerText, bool visible, int width)
        {
            DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = dataPropertyName;
            column.HeaderText = headerText;
            column.HeaderCell.Style = new DataGridViewCellStyle() { Alignment = DataGridViewContentAlignment.MiddleCenter };
            column.Name = "col_" + Guid.NewGuid();
            column.FillWeight = width;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            column.ReadOnly = true;
            column.Visible = visible;
            column.Width = width;
            return column;
        }

        /// <summary>
        /// Add data column for DataGridViewLinkColumn
        /// </summary>
        /// <param name="text"></param>
        /// <param name="headerText"></param>
        /// <param name="visible"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        private DataGridViewLinkColumn AddDataLinkColumn(string text, string headerText, bool visible, int width)
        {
            DataGridViewLinkColumn column = new DataGridViewLinkColumn();
            column.UseColumnTextForLinkValue = true;
            column.Text = text;
            column.HeaderText = headerText;
            column.HeaderCell.Style = new DataGridViewCellStyle() { Alignment = DataGridViewContentAlignment.MiddleCenter };
            column.Name = "col_" + Guid.NewGuid();
            column.FillWeight = width;
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
            column.ReadOnly = true;
            column.DefaultCellStyle = new DataGridViewCellStyle() { Alignment = DataGridViewContentAlignment.MiddleCenter };
            column.Visible = visible;
            column.Width = width;
            return column;
        }

        /// <summary>
        /// Set printer name and report name of DataGridView cell
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="rowIndex"></param>
        private void SetPsNameColumnText(DataGridView dgv,int rowIndex)
        {
            try
            {
                int psIntNo = int.Parse(dgv.Rows[rowIndex].Cells[1].Value.ToString());
                PrinterSettings ps = this._SelectPrinterSettingsList.Where(where => where.PsIntNo == psIntNo).FirstOrDefault();
                if (ps != null)
                {
                    dgv.Rows[rowIndex].Cells[4].Value = ps.PsName;
                }

                int rcIntNo = int.Parse(dgv.Rows[rowIndex].Cells[2].Value.ToString());
                ReportConfig rc = ReportConfigList.Where(where => where.RcIntNo == rcIntNo).FirstOrDefault();
                if (rc != null)
                {
                    dgv.Rows[rowIndex].Cells[3].Value = rc.ReportName;
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// The method for local pc settings of DataGridView RowPostPaint event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvLocalPcSettings_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            this.SetPsNameColumnText(this.dgvLocalPcSettings, e.RowIndex);
            this.DrawIndex(this.dgvLocalPcSettings, e);
        }

        /// <summary>
        /// The method for report settings of DataGridView RowPostPaint event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvReportSettings_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            this.SetPsNameColumnText(this.dgvReportSettings, e.RowIndex);
            this.DrawIndex(this.dgvReportSettings, e);
        }

        /// <summary>
        /// The event method for printer settings of DataGridView RowPostPaint event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvPrinterSettings_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            this.DrawIndex(this.dgvReportSettings, e);
        }

        /// <summary>
        /// The click event method for local pc settings query of Button Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch1_Click(object sender, EventArgs e)
        {
            LocalPcSettingsQuery query = new LocalPcSettingsQuery();
            query.Append(LocalPcSettingsColumn.PcmacAddress, MacAddress);
            query.Append(LocalPcSettingsColumn.AutIntNo, AutIntNo.ToString());
            if (this.cmbLPS_Report.SelectedValue != null && int.Parse(this.cmbLPS_Report.SelectedValue.ToString()) > 0)
            {
                query.AppendLike(LocalPcSettingsColumn.RcIntNo, this.cmbLPS_Report.SelectedValue.ToString());
            }
            if (this.cmbLPS_PsIntNo.SelectedValue != null && int.Parse(this.cmbLPS_PsIntNo.SelectedValue.ToString()) > 0)
            {
                query.Append(LocalPcSettingsColumn.PsIntNo, this.cmbLPS_PsIntNo.SelectedValue.ToString());
            }
            this.LocalPcSettingsList = this._LocalPcSettingsService.Find(query).OrderByDescending(o => o.LpsIntNo).ToList();
            this.BindDataGridView(this.dgvLocalPcSettings, this.LocalPcSettingsList);
        }

        /// <summary>
        /// The click event method for printer settings query of Button Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch3_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtPrinterName.Text.Trim()))
            {
                PrinterSettingsQuery query = new PrinterSettingsQuery();
                query.Append(PrinterSettingsColumn.AutIntNo, AutIntNo.ToString());
                query.AppendLike(PrinterSettingsColumn.PsName, this.txtPrinterName.Text.Trim());
                this.PrinterSettingsList = this._PrinterSettingsService.Find(query).OrderByDescending(o => o.PsIntNo).ToList();
                this.BindDataGridView(this.dgvPrinterSettings, this.PrinterSettingsList);
            }
            else
            {
                this.GetPrinterSettingsList();
            }
        }

        /// <summary>
        /// The click event method for report settings query of Button Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch2_Click(object sender, EventArgs e)
        {
            ReportSettingsQuery query = new ReportSettingsQuery();
            query.Append(ReportSettingsColumn.AutIntNo, AutIntNo.ToString());
            if (this.cmbRS_Report.SelectedValue != null && int.Parse(this.cmbRS_Report.SelectedValue.ToString()) > 0)
            {
                query.AppendLike(ReportSettingsColumn.RcIntNo, this.cmbRS_Report.SelectedValue.ToString());
            }
            if (this.cmbRS_PsIntNo.SelectedValue != null && int.Parse(this.cmbRS_PsIntNo.SelectedValue.ToString()) > 0)
            {
                query.Append(ReportSettingsColumn.PsIntNo, this.cmbRS_PsIntNo.SelectedValue.ToString());
            }
            this.ReportSettingsList = this._ReportSettingsService.Find(query).OrderByDescending(o => o.RsIntNo).ToList();
            this.BindDataGridView(this.dgvReportSettings, this.ReportSettingsList);
        }

        /// <summary>
        /// The method for report setting of Report Config DropDownList Control KeyPress event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRS_Report_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.btnSearch1_Click(this.btnSearch1, e);
            }
        }

        /// <summary>
        /// The method for printer setting of Printer name TextBox Control KeyPress event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPrinterName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.btnSearch3_Click(this.btnSearch3, e);
            }
        }

        /// <summary>
        /// The method for local pc setting of Report Config DropDownList Control KeyPress event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbLPS_Report_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.btnSearch2_Click(this.btnSearch2, e);
            }
        }

        /// <summary>
        /// The method for local pc setting of Printer DropDownList Control KeyPress event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbmLPS_PsIntNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.btnSearch1_Click(this.btnSearch1, e);
            }
        }

        /// <summary>
        /// The method for report setting of Printer DropDownList Control KeyPress event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRS_PsIntNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.btnSearch2_Click(this.btnSearch2, e);
            }
        }

        /// <summary>
        /// The method set Select DataGridViewRow
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="iRowIndex"></param>
        private void SetSelectRow(DataGridView dgv, int iRowIndex)
        {
            dgv.ClearSelection();
            CurrencyManager cm = (CurrencyManager)dgv.BindingContext[dgv.DataSource, dgv.DataMember];
            cm.Position = iRowIndex;
            dgv.Rows[iRowIndex].Selected = true;
        }

        /// <summary>
        /// The method for local pc setting of DataGridView Control cell click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvLoaclPcSettings_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Func<List<LocalPcSettings>> getDataList = () =>
            {
                this.btnSearch1_Click(this.btnSearch1, e);
                return this.LocalPcSettingsList;
            };
            Func<int, LocalPcSettings> getEntity = (id) =>
            {
                return this.LocalPcSettingsList.Where(where => where.LpsIntNo == id).FirstOrDefault();
            };
            Func<LocalPcSettings, bool> deleteEntity = (entity =>
            {
                return this._LocalPcSettingsService.Delete(entity);
            });
            this.RowEditDelete<LocalPcSettings, LocalPcSettingsEditForm>(this.dgvLocalPcSettings, e, getDataList, getEntity, deleteEntity);
        }

        /// <summary>
        /// The method for report setting of DataGridView Control cell click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvReportSettings_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Func<List<ReportSettings>> getDataList = () =>
            {
                this.btnSearch2_Click(this.btnSearch2, e);
                return this.ReportSettingsList;
            };
            Func<int, ReportSettings> getEntity = (id) =>
            {
                return this.ReportSettingsList.Where(where => where.RsIntNo == id).FirstOrDefault();
            };
            Func<ReportSettings, bool> deleteEntity = (entity =>
            {
                return this._ReportSettingsService.Delete(entity);
            });
            this.RowEditDelete<ReportSettings, ReportSettingsEditForm>(this.dgvReportSettings, e, getDataList, getEntity, deleteEntity);
        }

        /// <summary>
        /// The method for printer setting of DataGridView Control cell click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvPrinterSettings_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Func<List<PrinterSettings>> getDataList = () =>
            {
                this.btnSearch3_Click(sender, e);
                return this.PrinterSettingsList;
            };
            Func<int, PrinterSettings> getEntity = (id) =>
            {
                return this.PrinterSettingsList.Where(where => where.PsIntNo == id).FirstOrDefault();
            };
            Func<PrinterSettings, bool> deleteEntity = (entity =>
            {
                return this.ValidateDeletePrinterSettings(entity) ? this._PrinterSettingsService.Delete(entity) : false;
            });
            this.RowEditDelete<PrinterSettings, PrinterSettingsEditForm>(this.dgvPrinterSettings, e, getDataList, getEntity, deleteEntity);
        }

        /// <summary>
        /// The method validate to delete printer settings
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private bool ValidateDeletePrinterSettings(PrinterSettings entity)
        {
            ReportSettings rs = this._ReportSettingsService.GetByPsIntNo(entity.PsIntNo).FirstOrDefault();
            if (rs != null)
            {
                MessageBox.Show("Sorry, can't delete. the current printer settings are referenced report settings!");
                return false;
            }
            LocalPcSettings lps = this._LocalPcSettingsService.GetByPsIntNo(entity.PsIntNo).FirstOrDefault();
            if (lps != null)
            {
                MessageBox.Show("Sorry, can't delete. the current printer settings are referenced local pc settings!");
                return false;
            }
            return true;
        }

        /// <summary>
        /// The method edit and delete of DataGridView row data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="S"></typeparam>
        /// <param name="dgv"></param>
        /// <param name="e"></param>
        /// <param name="getDataList"></param>
        /// <param name="getEntity"></param>
        /// <param name="deleteEntity"></param>
        private void RowEditDelete<T, S>(DataGridView dgv, DataGridViewCellEventArgs e, Func<List<T>> getDataList, Func<int, T> getEntity, Func<T, bool> deleteEntity)
            where T : IEntity
            where S : IEditForm
        {
            if (e.ColumnIndex <= 0)
            {
                return;
            }
            DataGridViewColumn col = dgv.Columns[e.ColumnIndex];
            if (col is DataGridViewLinkColumn)
            {
                int primaryKey = int.Parse(dgv.CurrentRow.Cells[0].Value.ToString());
                T entity = getEntity(primaryKey);
                if ((col as DataGridViewLinkColumn).Text == "Edit")
                {
                    if (this.Execute<S>(entity))
                    {
                        List<T> dataList = getDataList();
                        entity = getEntity(primaryKey);
                        if (entity != null)
                        {
                            int iRowIndex = dataList.IndexOf(entity);
                            this.SetSelectRow(dgv, iRowIndex);
                        }
                    }
                }
                else if ((col as DataGridViewLinkColumn).Text == "Delete")
                {
                    if (MessageBox.Show("Are you sure want to delete ?", "Delete Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        if (deleteEntity(entity))
                        {
                            getDataList();
                        }
                    }
                }
            }
        }
    }
}
