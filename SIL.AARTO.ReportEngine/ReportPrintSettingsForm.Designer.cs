﻿namespace SIL.AARTO.ReportEngine
{
    partial class ReportPrintSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvLocalPcSettings = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbLPS_Report = new System.Windows.Forms.ComboBox();
            this.btnAddLocalPsSettings = new System.Windows.Forms.Button();
            this.btnSearch1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbLPS_PsIntNo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgvReportSettings = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmbRS_Report = new System.Windows.Forms.ComboBox();
            this.btnAddReportSettings = new System.Windows.Forms.Button();
            this.btnSearch2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbRS_PsIntNo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dgvPrinterSettings = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnbtnAddPrinterSettings = new System.Windows.Forms.Button();
            this.btnSearch3 = new System.Windows.Forms.Button();
            this.txtPrinterName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalPcSettings)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportSettings)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrinterSettings)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(846, 485);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(838, 459);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Local PC Settings";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvLocalPcSettings);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 80);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(832, 376);
            this.panel2.TabIndex = 1;
            // 
            // dgvLoaclPcSettings
            // 
            this.dgvLocalPcSettings.AllowUserToAddRows = false;
            this.dgvLocalPcSettings.AllowUserToDeleteRows = false;
            this.dgvLocalPcSettings.AllowUserToOrderColumns = true;
            this.dgvLocalPcSettings.AllowUserToResizeRows = false;
            this.dgvLocalPcSettings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLocalPcSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocalPcSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocalPcSettings.Location = new System.Drawing.Point(0, 0);
            this.dgvLocalPcSettings.Name = "dgvLocalPcSettings";
            this.dgvLocalPcSettings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLocalPcSettings.Size = new System.Drawing.Size(832, 376);
            this.dgvLocalPcSettings.TabIndex = 0;
            this.dgvLocalPcSettings.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLoaclPcSettings_CellClick);
            this.dgvLocalPcSettings.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvLocalPcSettings_RowPostPaint);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbLPS_Report);
            this.panel1.Controls.Add(this.btnAddLocalPsSettings);
            this.panel1.Controls.Add(this.btnSearch1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cmbLPS_PsIntNo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(832, 77);
            this.panel1.TabIndex = 0;
            // 
            // cmbLPS_Report
            // 
            this.cmbLPS_Report.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLPS_Report.FormattingEnabled = true;
            this.cmbLPS_Report.Location = new System.Drawing.Point(130, 43);
            this.cmbLPS_Report.Name = "cmbLPS_Report";
            this.cmbLPS_Report.Size = new System.Drawing.Size(250, 21);
            this.cmbLPS_Report.TabIndex = 8;
            this.cmbLPS_Report.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLPS_Report_KeyPress);
            // 
            // btnAddLocalPsSettings
            // 
            this.btnAddLocalPsSettings.Location = new System.Drawing.Point(495, 42);
            this.btnAddLocalPsSettings.Name = "btnAddLocalPcSettings";
            this.btnAddLocalPsSettings.Size = new System.Drawing.Size(75, 23);
            this.btnAddLocalPsSettings.TabIndex = 7;
            this.btnAddLocalPsSettings.Text = "Add";
            this.btnAddLocalPsSettings.UseVisualStyleBackColor = true;
            this.btnAddLocalPsSettings.Click += new System.EventHandler(this.btnAddLocalPcSettings_Click);
            // 
            // btnSearch1
            // 
            this.btnSearch1.Location = new System.Drawing.Point(400, 42);
            this.btnSearch1.Name = "btnSearch1";
            this.btnSearch1.Size = new System.Drawing.Size(75, 23);
            this.btnSearch1.TabIndex = 4;
            this.btnSearch1.Text = "Search";
            this.btnSearch1.UseVisualStyleBackColor = true;
            this.btnSearch1.Click += new System.EventHandler(this.btnSearch1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Report";
            // 
            // cmbLPS_PsIntNo
            // 
            this.cmbLPS_PsIntNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLPS_PsIntNo.FormattingEnabled = true;
            this.cmbLPS_PsIntNo.Location = new System.Drawing.Point(130, 11);
            this.cmbLPS_PsIntNo.Name = "cmbLPS_PsIntNo";
            this.cmbLPS_PsIntNo.Size = new System.Drawing.Size(250, 21);
            this.cmbLPS_PsIntNo.TabIndex = 1;
            this.cmbLPS_PsIntNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbmLPS_PsIntNo_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Printer";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(838, 459);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Report Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dgvReportSettings);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 80);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(832, 376);
            this.panel4.TabIndex = 2;
            // 
            // dgvReportSettings
            // 
            this.dgvReportSettings.AllowUserToAddRows = false;
            this.dgvReportSettings.AllowUserToDeleteRows = false;
            this.dgvReportSettings.AllowUserToOrderColumns = true;
            this.dgvReportSettings.AllowUserToResizeRows = false;
            this.dgvReportSettings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReportSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReportSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReportSettings.Location = new System.Drawing.Point(0, 0);
            this.dgvReportSettings.Name = "dgvReportSettings";
            this.dgvReportSettings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReportSettings.Size = new System.Drawing.Size(832, 376);
            this.dgvReportSettings.TabIndex = 0;
            this.dgvReportSettings.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReportSettings_CellClick);
            this.dgvReportSettings.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvReportSettings_RowPostPaint);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cmbRS_Report);
            this.panel3.Controls.Add(this.btnAddReportSettings);
            this.panel3.Controls.Add(this.btnSearch2);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.cmbRS_PsIntNo);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(832, 77);
            this.panel3.TabIndex = 1;
            // 
            // cmbRS_Report
            // 
            this.cmbRS_Report.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRS_Report.FormattingEnabled = true;
            this.cmbRS_Report.Location = new System.Drawing.Point(130, 43);
            this.cmbRS_Report.Name = "cmbRS_Report";
            this.cmbRS_Report.Size = new System.Drawing.Size(250, 21);
            this.cmbRS_Report.TabIndex = 7;
            this.cmbRS_Report.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbRS_Report_KeyPress);
            // 
            // btnAddReportSettings
            // 
            this.btnAddReportSettings.Location = new System.Drawing.Point(495, 42);
            this.btnAddReportSettings.Name = "btnAddReportSettings";
            this.btnAddReportSettings.Size = new System.Drawing.Size(75, 23);
            this.btnAddReportSettings.TabIndex = 6;
            this.btnAddReportSettings.Text = "Add";
            this.btnAddReportSettings.UseVisualStyleBackColor = true;
            this.btnAddReportSettings.Click += new System.EventHandler(this.btnAddReportSettings_Click);
            // 
            // btnSearch2
            // 
            this.btnSearch2.Location = new System.Drawing.Point(400, 42);
            this.btnSearch2.Name = "btnSearch2";
            this.btnSearch2.Size = new System.Drawing.Size(75, 23);
            this.btnSearch2.TabIndex = 4;
            this.btnSearch2.Text = "Search";
            this.btnSearch2.UseVisualStyleBackColor = true;
            this.btnSearch2.Click += new System.EventHandler(this.btnSearch2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Report";
            // 
            // cmbRS_PsIntNo
            // 
            this.cmbRS_PsIntNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRS_PsIntNo.FormattingEnabled = true;
            this.cmbRS_PsIntNo.Location = new System.Drawing.Point(130, 11);
            this.cmbRS_PsIntNo.Name = "cmbRS_PsIntNo";
            this.cmbRS_PsIntNo.Size = new System.Drawing.Size(250, 21);
            this.cmbRS_PsIntNo.TabIndex = 1;
            this.cmbRS_PsIntNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbRS_PsIntNo_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Printer";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel6);
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(838, 459);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Printer Settings";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dgvPrinterSettings);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 50);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(838, 409);
            this.panel6.TabIndex = 1;
            // 
            // dgvPrinterSettings
            // 
            this.dgvPrinterSettings.AllowUserToAddRows = false;
            this.dgvPrinterSettings.AllowUserToDeleteRows = false;
            this.dgvPrinterSettings.AllowUserToOrderColumns = true;
            this.dgvPrinterSettings.AllowUserToResizeRows = false;
            this.dgvPrinterSettings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPrinterSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrinterSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPrinterSettings.Location = new System.Drawing.Point(0, 0);
            this.dgvPrinterSettings.Name = "dgvPrinterSettings";
            this.dgvPrinterSettings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPrinterSettings.Size = new System.Drawing.Size(838, 409);
            this.dgvPrinterSettings.TabIndex = 0;
            this.dgvPrinterSettings.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPrinterSettings_CellClick);
            this.dgvPrinterSettings.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvPrinterSettings_RowPostPaint);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnbtnAddPrinterSettings);
            this.panel5.Controls.Add(this.btnSearch3);
            this.panel5.Controls.Add(this.txtPrinterName);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(838, 50);
            this.panel5.TabIndex = 0;
            // 
            // btnbtnAddPrinterSettings
            // 
            this.btnbtnAddPrinterSettings.Location = new System.Drawing.Point(485, 9);
            this.btnbtnAddPrinterSettings.Name = "btnbtnAddPrinterSettings";
            this.btnbtnAddPrinterSettings.Size = new System.Drawing.Size(75, 23);
            this.btnbtnAddPrinterSettings.TabIndex = 3;
            this.btnbtnAddPrinterSettings.Text = "Add";
            this.btnbtnAddPrinterSettings.UseVisualStyleBackColor = true;
            this.btnbtnAddPrinterSettings.Click += new System.EventHandler(this.btnbtnAddPrinterSettings_Click);
            // 
            // btnSearch3
            // 
            this.btnSearch3.Location = new System.Drawing.Point(390, 9);
            this.btnSearch3.Name = "btnSearch3";
            this.btnSearch3.Size = new System.Drawing.Size(75, 23);
            this.btnSearch3.TabIndex = 2;
            this.btnSearch3.Text = "Search";
            this.btnSearch3.UseVisualStyleBackColor = true;
            this.btnSearch3.Click += new System.EventHandler(this.btnSearch3_Click);
            // 
            // txtPrinterName
            // 
            this.txtPrinterName.Location = new System.Drawing.Point(120, 11);
            this.txtPrinterName.Name = "txtPrinterName";
            this.txtPrinterName.Size = new System.Drawing.Size(250, 20);
            this.txtPrinterName.TabIndex = 1;
            this.txtPrinterName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrinterName_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Printer Name";
            // 
            // ReportPrintSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 485);
            this.Controls.Add(this.tabControl1);
            this.MinimizeBox = false;
            this.Name = "ReportPrintSettingsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report Print Settings";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalPcSettings)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportSettings)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrinterSettings)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgvLocalPcSettings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnSearch1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbLPS_PsIntNo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgvReportSettings;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSearch2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbRS_PsIntNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView dgvPrinterSettings;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnSearch3;
        private System.Windows.Forms.TextBox txtPrinterName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnbtnAddPrinterSettings;
        private System.Windows.Forms.Button btnAddReportSettings;
        private System.Windows.Forms.Button btnAddLocalPsSettings;
        private System.Windows.Forms.ComboBox cmbLPS_Report;
        private System.Windows.Forms.ComboBox cmbRS_Report;

    }
}