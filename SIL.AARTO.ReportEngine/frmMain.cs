﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Report;
using SIL.AARTO.BLL.Report.Model;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.Utility.Printing;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.ReportEngine
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// 20131217 edited by Jacob, extend to support file reports
    /// </remarks>
    /// 
    public partial class frmMain : Form
    {
        public static string artoConnectionString = string.Empty;
        private readonly string COMPLETE_MSG = "Completed.";
        private readonly string NO_DATA_FOUND_MSG = "No data found.";
        private readonly string NO_ROW_IS_SELECTED_MSG = "No data row is selected.";
        private readonly string ROW_NOT_SELECTED_VALUE = "0";
        private readonly string ROW_SELECTED_VALUE = "1";
        private readonly string START_NUMBER_INPUT_MENTION = "Please input a positive integer";
        private ListPrintEngine commonListReprint = new ListPrintEngine(); //added  by Jacob 20120830
        private readonly Dictionary<string, string> filNameList = new Dictionary<string, string>();

        //private readonly FreeStylePrintEngineFor1stNoticeHWO flpEngine1stNoticeHwo; //new FreeStylePrintEngineFor1stNoticeHWO();  2013-06-20 removed by Nancy 
        private readonly FreeStylePrintEngineFor1stNoticeVIDEO flpEngine1stNoticeVideo; //new FreeStylePrintEngineFor1stNoticeVIDEO();
        private readonly FreeStylePrintEngineFor2ndNotice flpEngine2ndNot; //new FreeStylePrintEngineFor2ndNotice();
        private readonly FreeStylePrintEngineFor2ndNoticeVIDEO flpEngine2ndNoticeVideo; //Jerry 2013-03-18 add new FreeStylePrintEngineFor2ndNoticeVIDEO();
        private readonly FreeStylePrintEngineForCpa5 flpEngineCPA5; //new FreeStylePrintEngineForCpa5();
        private readonly FreeStylePrintEngineForCpa6 flpEngineCPA6; //new FreeStylePrintEngineForCpa6();
        private readonly FreeStylePrintEngineForCpa5DirectPrinting flpEngineCPA5DirectPrinting;//2013-09-16 added by Nancy for direct printing
        private readonly FreeStylePrintEngineForCpa6DirectPrinting flpEngineCPA6DirectPrinting;//2013-09-16 added by Nancy for direct printing
        private readonly FreeStylePrintEngineFor2ndNoticeDirectPrinting flpEngine2ndNoticeDirectPrinting;//2013-12-23 Heidi added for 2nd Notice Direct Printing (5101)
        private readonly FreeStylePrintEngineForWOADirectPrinting flpEngineWOADirectPrinting;//2013-12-25 Heidi added for WOA Direct Printing (5101)
        private readonly FreeStylePrintEngineForGenerateLatters flpEngineGenerateLetters;//new FreeStylePrintEngineForGenerateLatters();
        private readonly FreeStylePrintEngineForNoticeOfWOA flpEngineNoticeofWOA; 
        private readonly FreeStylePrintEngineForWOA flpEngineWOA; //new FreeStylePrintEngineForWOA();
        private readonly FreeStylePrintEngineFor2ndNoticeOfBus flpEngine2ndNotofBUS;
        private readonly ReportHistoryMode hisMode = new ReportHistoryMode();
        private readonly ListPrintEngineForSummarySecondNotice listEngineForSecondNotice; // new ListPrintEngineForSummarySecondNotice();
        //private readonly ListPrintEngineForSummaryFirstNoticeHWO listEngineFor1stNoticeHwo;2013-06-20 removed by Nancy
        private readonly ListPrintEngineForSummaryFirstNoticeVIDEO listEngineFor1stNoticeVideo;
        private readonly ListPrintEngineForSummarySecondNoticeVIDEO listEngineFor2ndNoticeVideo;  //Jerry 2013-03-18 add                                                     
       
        private readonly ListPrintEngineForSummarySum listEngineForSum; //new ListPrintEngineForSummarySum();
        private readonly ListPrintEngineForSummaryWOA listEngineForWOA; // new ListPrintEngineForSummaryWOA();
        private FreeStylePrintEngineForPrintSummaryWOADirectPrinting listEngineForSummaryWOADirectPrinting;
        private readonly ReportNewMode newMode = new ReportNewMode();
        private readonly NewFilesMode newFilesMode = new NewFilesMode();//20131217 added by Jacob
        private readonly ListReportHistoryMode listReportHistoryModeMode = new ListReportHistoryMode();
        private readonly CompletedFilesMode completedFilesMode = new CompletedFilesMode();//20131217 added by Jacob
        private FreeStylePrintEngine currentEngine;
        private ReportFormMode currentFormMode;
        public FixedLayoutPrintEngine flpEngine;
        public TList<ReportConfig> rcList;
        public ReportModel rptModel;

        // Oscar 20121107 added for ccr reprint
        CCRReprintSetting CCRSetting { get; set; }

        //Add by Brian 2013-10-12
        public string FileName { get; set; }
        public string DocumentName { get; set; }

        public frmMain()
        {
            InitializeComponent();
           
            artoConnectionString
                = ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ConnectionString;

            try
            {
                flpEngineCPA5 = new FreeStylePrintEngineForCpa5(artoConnectionString);
                flpEngineCPA6 = new FreeStylePrintEngineForCpa6(artoConnectionString);
                flpEngineCPA5DirectPrinting = new FreeStylePrintEngineForCpa5DirectPrinting(artoConnectionString); //2013-09-16 added by Nancy for direct printing
                flpEngineCPA6DirectPrinting = new FreeStylePrintEngineForCpa6DirectPrinting(artoConnectionString); //2013-09-16 added by Nancy for direct priting
                flpEngine2ndNoticeDirectPrinting = new FreeStylePrintEngineFor2ndNoticeDirectPrinting(artoConnectionString); //2013-12-23 Heidi added for 2nd Notice Direct Printing (5101)
                flpEngineWOADirectPrinting = new FreeStylePrintEngineForWOADirectPrinting(artoConnectionString); //2013-12-25 Heidi added for WOA Direct Printing (5101)
                flpEngineWOA = new FreeStylePrintEngineForWOA(artoConnectionString);
                listEngineForSum = new ListPrintEngineForSummarySum(artoConnectionString);
                flpEngine2ndNot = new FreeStylePrintEngineFor2ndNotice(artoConnectionString);
                flpEngine2ndNotofBUS = new FreeStylePrintEngineFor2ndNoticeOfBus(artoConnectionString);
                listEngineForWOA = new ListPrintEngineForSummaryWOA(artoConnectionString);
                listEngineForSummaryWOADirectPrinting = new FreeStylePrintEngineForPrintSummaryWOADirectPrinting(artoConnectionString);
                listEngineForSecondNotice = new ListPrintEngineForSummarySecondNotice(artoConnectionString);
                //listEngineFor1stNoticeHwo = new ListPrintEngineForSummaryFirstNoticeHWO(artoConnectionString); 2013-06-20 removed by Nancy
                listEngineFor1stNoticeVideo = new ListPrintEngineForSummaryFirstNoticeVIDEO(artoConnectionString);
                //flpEngine1stNoticeHwo = new FreeStylePrintEngineFor1stNoticeHWO(artoConnectionString); 2013-06-20 removed by Nancy
                flpEngine1stNoticeVideo = new FreeStylePrintEngineFor1stNoticeVIDEO(artoConnectionString);
                flpEngineGenerateLetters = new FreeStylePrintEngineForGenerateLatters(artoConnectionString);
                flpEngineNoticeofWOA = new FreeStylePrintEngineForNoticeOfWOA(artoConnectionString);
                //Jerry 2013-03-18 add
                flpEngine2ndNoticeVideo = new FreeStylePrintEngineFor2ndNoticeVIDEO(artoConnectionString);
                listEngineFor2ndNoticeVideo = new ListPrintEngineForSummarySecondNoticeVIDEO(artoConnectionString);

                cmb_SeleAut.DataSource = GetAutDropList();
                cmb_SeleAut.DisplayMember = "AutName";
                cmb_SeleAut.ValueMember = "AutIntNo";
                rptModel = new ReportModel();
                var autServ = new AuthorityService();

                rptModel.DateFrom = Convert.ToDateTime("1900-01-01");
                rptModel.DateTo = Convert.ToDateTime("2079-06-06");
                rptModel.AutIntNo = cmb_SeleAut.SelectedValue.ToString();

                cmb_SelectReportType.ValueMember = "ReportCode";
                cmb_SelectReportType.DataSource = GetWinFormReportConfig();
                cmb_SelectReportType.DisplayMember = "ReportName";
                
               
                //2013-10-22 added by Nancy start
                //set dfault value for the date range
                if (dtpFrom.Value == Convert.ToDateTime("1900-01-01"))
                {
                    dtpFrom.Value = DateTime.Now.Date;
                }
                if (dtpTo.Value == Convert.ToDateTime("2015-01-01"))
                {
                    dtpTo.Value = DateTime.Now.Date;
                }
                //2013-10-22 added by Nancy end             
              
                //rcList = ReportManager.GetAllReportConfig();
                //flpEngine = new FixedLayoutPrintEngine();
                //flpEngine.connStr = artoConnectionString;
                //CurrentEngine = flpEngineCPA5;
                cmb_SelectReportType.SelectedIndex = 0;

                cmb_SeleAut.SelectedIndex = 0;
                CreateEngines();

                CCRSetting = new CCRReprintSetting { Print4th = true };
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, ex.Message);
            }
        }

        private FreeStylePrintEngine CurrentEngine
        {
            get { return currentEngine; }
            set
            {
                currentEngine = value;
                if (value!=null && value.CurrentReportDataService != null)
                { 
                    //edited by Jacob 20120906 start
                    if(string.IsNullOrEmpty(value.CurrentReportDataService.ReportType))
                        value.CurrentReportDataService.InitializeReportConfig(SelectedRcIntNo);
                    lblEngine.Text = value.CurrentReportDataService.ReportType;
                    //edited by Jacob 20120906 end

                    button7.Text = value.Button7_Text;//2013-11-19 added by Nancy

                    //Add by Brian 2013-10-11
                    //2014-03-10 Heidi changed for fixed search by document number not working
                    //RefreshLabelForDocumentNumber(value);

                    //Removed by Jacob  20131217, should use variables defined by Oscar
                    //Add by Brian 2013-10-11
                    //value.CurrentReportDataService.FileNameFieldName = value.FieldForFileName;
                    //value.CurrentReportDataService.DocumentNumberName = value.DocumentNumberFileName;

                }
            }
        }

        private void RefreshLabelForDocumentNumber(FreeStylePrintEngine value)
        {
            this.lblDocumentNumber.Text = value.FieldForDocumentNumber;//value.DocumentNumberFileName;
            int x = this.lblDocumentNumber.Location.X + this.lblDocumentNumber.Size.Width + 16;
            this.txtDocumentNumber.Location = new System.Drawing.Point(x, this.txtDocumentNumber.Location.Y);
            x = this.txtDocumentNumber.Location.X + this.txtDocumentNumber.Size.Width + 16;
            this.btnSearch.Location = new System.Drawing.Point(x, this.btnSearch.Location.Y);
        }

        private int SelectedRcIntNo
        {
            get { return Convert.ToInt16((cmb_SelectReportType.SelectedItem as DataRowView)["rcIntNo"]); }
        }

        private int SelectedAutIntNo
        {
            get { return Convert.ToInt16((cmb_SeleAut.SelectedItem as DataRowView)["AutIntNo"]); }
        }
        private ReportFormMode CurrentFormMode
        {
            get { return currentFormMode; }
            set
            {
                // this.dgvHistory.DataSource as  = null;
                // this.dgvFiles.DataSource = null;
                // this.dgvDetail.DataSource = null;
                 // currentEngine = null;
                bsForDetail.DataSource = null;
                currentFormMode = value;
                if (currentEngine != null) CurrentEngine.CurrentMode = value;
                if (value is ListReportHistoryMode)
                {
                    this.lblEngine.Text = "Reprint list report";
                }
                
                else
                {
                    if (currentEngine != null)
                    {
                        lblEngine.Text =
                            currentEngine.CurrentReportDataService.ReportType; // +currentFormMode.ModeDisplayName;
                        
                        //20131217 added by Jacob start
                        //extend for file reports.
                        if (value is NewFilesMode || value is CompletedFilesMode)
                        {
                            lblEngine.Text =
                              currentEngine.CurrentReportDataService.ReportType +currentFormMode.ModeDisplayName;
                            lblFolder.Text = currentEngine.CurrentReportDataService.RootFolder;
                        }
                        else
                            lblFolder.Text = "";
                        //20131217 added by Jacob end
                    }
                }
                //layout change?
                //big if
                //20131217 added by Jacob, for file reports.
                btn_ViewNewFiles.Visible = false;
                btn_ViewCompleteFiles.Visible = false;

                if (value is ReportHistoryMode)
                {
                    btnGetNewData.Visible = true;
                    int dgvHeight = splitContainer1.Height/3;

                    splitContainer1.SplitterDistance = dgvHeight*2;
                    splitContainer1.Panel2.Show();
                    splitContainer1.Panel1.Show();
                    splitContainer1.Show();
                    splitContainer2.SplitterDistance = dgvHeight;
                    splitContainer2.Panel1.Show();
                    splitContainer2.Show();
                    //2014-03-10 Heidi added for fixed search by document number not working
                    RefreshLabelForDocumentNumber(currentEngine);
                    pnlStartFrom.Show();
                }
                else if (value is ReportNewMode)
                {
                    btnGetNewData.Visible = true;
                    
                    splitContainer1.SplitterDistance = splitContainer1.Height;
                    splitContainer1.Panel1.Show();
                    splitContainer1.Panel2.Hide();
                    splitContainer1.Show();
                    splitContainer2.SplitterDistance = 0;
                    splitContainer2.Panel2.Show();
                    splitContainer2.Show();
                    pnlStartFrom.Show();
                }
                else if (value is CompletedFilesMode || value is NewFilesMode)
                { // tbSearchFileNameForFileList
                    btnGetNewData.Visible = false;
                    btn_ViewNewFiles.Visible = true;
                    btn_ViewCompleteFiles.Visible = true;
                    splitContainer1.SplitterDistance = splitContainer1.Height;
                    splitContainer1.Panel1.Show();
                    splitContainer1.Panel2.Hide();
                    splitContainer1.Show();
                    splitContainer2.SplitterDistance = 0;
                    splitContainer2.Panel2.Show();
                    splitContainer2.Show();
                    pnlStartFrom.Show();
                }
                else if (value is ListReportHistoryMode)
                {
                    btnGetNewData.Visible = false;

                    splitContainer1.SplitterDistance = 0; 
                    splitContainer1.Panel1.Hide();
                    splitContainer1.Panel2.Show();
                    splitContainer1.Show();
                    pnlStartFrom.Show();
                }
                if (value is CompletedFilesMode)
                {
                    pnlSearchBarForFiles.Show();
                    // tbSearchFileNameForFileList
                }
                else
                { pnlSearchBarForFiles.Hide(); }
                if (IsFreeStyleAndFileReport())
                {
                     btnGetNewData.Visible = false;
                    btn_ViewCompleteFiles.Visible = true;
                    btn_ViewNewFiles.Visible = true;

                }
                else
                {
                    //btnGetNewData.Visible = true;
                    btn_ViewCompleteFiles.Visible = false;
                    btn_ViewNewFiles.Visible = false;

                }
            }
        }


        public DataTable GetAutDropList()
        {
            string StrConnetion = artoConnectionString;

            var SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();
                var selCmd = new SqlCommand("GetAutDrpListForReportPrintEngine", SQLConn) { CommandType = CommandType.StoredProcedure };

                var adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);
                return SQLTable;
            }
        }


        public DataTable GetWinFormReportConfig()
        {
            string StrConnetion = artoConnectionString;
            var SQLTable = new DataTable();

            using (var SQLConn = new SqlConnection(StrConnetion))
            {
                SQLConn.Open();
                var selCmd = new SqlCommand("GetReportConfigByIsCustomAndIsFreeStyle", SQLConn) { CommandType = CommandType.StoredProcedure };
                selCmd.Parameters.Add(new SqlParameter("@IsCustom", SqlDbType.Bit)).Value = false;
                selCmd.Parameters.Add(new SqlParameter("@IsFreeStyle", SqlDbType.Bit)).Value = true;

                var adapter = new SqlDataAdapter(selCmd);
                adapter.Fill(SQLTable);
                return SQLTable;
            }
        }

        private DialogResult PrintFreeformReport(FreeStylePrintEngine freeEngin,
                                                 DataTable reportData = null, bool addHistory = true,
                                                 bool onlyHistory = true)
        {
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            ReportConfig reportConfig = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.RcIntNo == this.rptModel.RcIntNo);
            PunchAction punchAction = PunchAction.Other;
            if (onlyHistory && !(CurrentFormMode is ListReportHistoryMode))
            {
                // 2013-09-03 add by Henry for AR_5110 and AR_5120
                freeEngin.CurrentReportDataService.AutIntNo = (int)cmb_SeleAut.SelectedValue;

                // 2013-07-23 add by Henry
                freeEngin.CurrentReportDataService.LastUser = string.Format("{0}_{1}",Program.APP_NAME, GlobalVariates<User>.CurrentUser.UserLoginName);
                // 2012.11.05 Nick add the status change
                //freeEngin.CurrentReportDataService.ModifyPrintDate(reportData);
                //freeEngin.PrintReportWithData(reportData, addHistory, onlyHistory);
                ////freeEngin.CurrentReportDataService.ModifyNoticeStatus(reportData);
                //freeEngin.CurrentReportDataService.SaveHistory_ModifyPrintDateAndStatus(reportData,addHistory,onlyHistory);
                //freeEngin.PrintReportWithData(reportData, addHistory, onlyHistory);

                // 2013-10-15, Oscar added transaction for saving history and modifying print date and status
                freeEngin.CurrentReportDataService.SaveHistory_ModifyPrintDateAndStatus(reportData, addHistory, !(CurrentFormMode is ReportHistoryMode));
                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                if (!(CurrentFormMode is ReportHistoryMode))
                {
                    punchAction = PunchAction.Change;
                }
                AddPunchStatisticsTran(reportConfig, punchAction);

                return DialogResult.Yes;
            }
            DialogResult answer = DialogResult.Cancel;
            if (CurrentFormMode is ListReportHistoryMode)
                commonListReprint.CreateEngineWithTestData(rptModel.RcIntNo);
            else
                freeEngin.PrintTestPage();
            do
            {
                answer = MessageBox.Show("Please confirm the preview paper, " +
                                         "and then choose the next step." +
                                         "\n\n Yes----------Start print with preview (real data)" +
                                         "\n No-----------Redo Preview with test data" +
                                         "\n Cancel-------Abort",
                                         "Preview Confirm",
                                         MessageBoxButtons.YesNoCancel,
                                         MessageBoxIcon.Question);
                switch (answer)
                {
                    case DialogResult.Yes:
                        if (!(CurrentFormMode is ListReportHistoryMode))
                        {
                            if (reportData == null)
                            {
                                freeEngin.PrintReport();
                            }
                            else
                            {
                                //Add by Brian 2013-10-18
                                freeEngin.CurrentReportDataService.CurrentPCMACAddress = ReportPrintSettingsForm.GetMACAddress();

                                // 2013-09-03 add by Henry for AR_5110 and AR_5120
                                freeEngin.CurrentReportDataService.AutIntNo = (int)cmb_SeleAut.SelectedValue;
                                //if (freeEngin is FreeStylePrintEngineFor2ndNotice || freeEngin is FreeStylePrintEngineForCpa5 || freeEngin is FreeStylePrintEngineForCpa6 || freeEngin is FreeStylePrintEngineForWOA)
                                //{
                                freeEngin.CurrentReportDataService.LastUser = string.Format("{0}_{1}", Program.APP_NAME, GlobalVariates<User>.CurrentUser.UserLoginName); 
                                //freeEngin.CurrentReportDataService.ModifyPrintDate(reportData);
                                //}
                                //freeEngin.PrintReportWithData(reportData, addHistory, onlyHistory);

                                //if (freeEngin is FreeStylePrintEngineFor2ndNotice ||freeEngin is FreeStylePrintEngineForWOA)
                                //{
                                //freeEngin.CurrentReportDataService.ModifyNoticeStatus(reportData);
                                //}


                                // 2013-10-15, Oscar added transaction for saving history and modifying print date and status
                                freeEngin.CurrentReportDataService.SaveHistory_ModifyPrintDateAndStatus(reportData, addHistory, !(CurrentFormMode is ReportHistoryMode));
                                //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                                if (!(CurrentFormMode is ReportHistoryMode))
                                {
                                    punchAction = PunchAction.Change;
                                }
                                AddPunchStatisticsTran(reportConfig, punchAction);

                                freeEngin.PrintReportWithData(reportData, addHistory, onlyHistory);
                            }
                        }
                        else
                        {
                            if (commonListReprint is ReportDataServiceForCriminalCaseRegister)
                            {
                                using (var ccr = new ReprintCCROptions { ReprintSetting = CCRSetting })
                                {
                                    if (ccr.ShowDialog() == DialogResult.OK)
                                        (commonListReprint as ReportDataServiceForCriminalCaseRegister).ReprintSetting = ccr.ReprintSetting;
                                    else
                                        return DialogResult.Cancel;
                                }
                            }
                            //2013-04-10 added by Nancy for 2nd CCR started
                            else if (commonListReprint is ReportDataServiceForSecondCriminalCaseRegister)
                            {
                                using (var ccr = new ReprintCCROptions { ReprintSetting = CCRSetting })
                                {
                                    if (ccr.ShowDialog() == DialogResult.OK)
                                        (commonListReprint as ReportDataServiceForSecondCriminalCaseRegister).ReprintSetting = ccr.ReprintSetting;
                                    else
                                        return DialogResult.Cancel;
                                }
                            }
                            //2013-04-10 added by Nancy for 2nd CCR end

                            ReportModel printModel = commonListReprint.CreateReportModel
                    (rptModel.RcIntNo, rptModel.DateFrom, rptModel.DateTo);
                            UpdateModelValue();
                            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
                            AddPunchStatisticsTran(reportConfig, PunchAction.Other);
                            // 20121011 Nick changed from CreateMyEngine to CreatePreviewEngine
                            commonListReprint.CreatePreviewEngine(printModel, GetSelectedDetailDataTable());
                        }

                        break;
                    case DialogResult.No:
                        if (CurrentFormMode is ListReportHistoryMode)
                            commonListReprint.CreateEngineWithTestData(rptModel.RcIntNo);
                        else
                            freeEngin.PrintTestPage();
                        break;
                    case DialogResult.Cancel:
                        break;
                }
            } while (answer.Equals(DialogResult.No));
            return answer;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (!CheckLargeAmount()) return;

            if(currentFormMode is ListReportHistoryMode)
            {
                UpdateModelValue();

                //ReportModel printModel = commonListReprint.CreateReportModel
                //    (rptModel.RcIntNo, rptModel.DateFrom, rptModel.DateTo);
                //commonListReprint.CreateMyEngine(printModel, GetSelectedDetailDataTable());
                //return;
            }
           
            //Get print data by files selected.
            //new print data.
            //20131217 edited by Jacob, for file reports.
            //if (CurrentFormMode is ReportNewMode)
            if (CurrentFormMode is ReportNewMode || CurrentFormMode is CompletedFilesMode || CurrentFormMode is NewFilesMode)
            {
                GetDetailReportDataByFiles();
                SelectAll(dgvDetail);
            }
            
            PrintCurrentReport();
        }

        #region 2013-10-16 Oscar added checking for large amount

        bool CheckLargeAmount()
        {
            if (currentEngine == null || currentEngine.CurrentReportDataService == null) return true;

            var amount = currentEngine.CurrentReportDataService.GetDocumentsAmountLimit();

            string fieldName;
            if (!(currentFormMode is ReportNewMode)
                || amount == 0
                || string.IsNullOrWhiteSpace(fieldName = currentEngine.CurrentReportDataService.FieldForNoOfDocument)
                || !this.dgvFiles.Columns.Contains(fieldName))
                return true;
            //2014-11-03 Heidi changed "summons" to "documents" text for cover all cases (bontq1677)
            const string text = "{0} print files with a total of {1} documents have been selected. This large amount of data may result in poor performance. Are you sure you wish to select multiple files for printing?";
            const string caption = "Warning - Large amout of data";

            int files = 0, total = 0;
            foreach (DataGridViewRow row in this.dgvFiles.Rows)
            {
                int count;
                if (Convert.ToString(row.Cells[0].Value) == "1"
                    && int.TryParse(Convert.ToString(row.Cells[fieldName].Value), out count))
                {
                    files++;
                    total += count;
                }
            }

            if (files > 1
                && total > amount)
            {
                return MessageBox.Show(string.Format(text, files, total), caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes;
            }
            return true;
        }

        #endregion

        /// <summary>
        /// Get selected data from datagridview
        /// call PrintFreeformReport to preview and print.
        /// </summary>
        private void PrintCurrentReport()
        {
            try
            {
                DataTable newdt = GetSelectedDetailDataTable();
                if (newdt==null) return;
                if (newdt.Rows.Count <= 0)
                {
                    MessageBox.Show(NO_ROW_IS_SELECTED_MSG);
                    return;
                }
                else
                {
                    //print
                    DialogResult result = PrintFreeformReport(CurrentEngine, newdt, cb_AddHistory.Checked,
                                                              cb_OnlyHistory.Checked);
                    if (CurrentFormMode is ReportNewMode)
                    {
                        bsForDetail.DataSource = null;
                    }
                    if (result.Equals(DialogResult.Yes) && !(CurrentFormMode is ListReportHistoryMode))
                    {
                        MessageBox.Show(COMPLETE_MSG);
                        //20131217 edited by Jacob start, for file reports.
                        //GetNewPrintData(CurrentEngine);
                        if (CurrentFormMode is NewFilesMode || CurrentFormMode is CompletedFilesMode||IsFreeStyleAndFileReport())
                        {
                            GetNewPrintFiles();
                        }
                        else
                        {
                          GetNewPrintData();
                        }
                        //20131217 edited by Jacob end
                    }
                }
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, ex.Message);
				EntLibLogger.WriteLog(LogCategory.Exception, ex.Message, ex.ToString());
                //MessageBox.Show("Error happened", ex.Message + ex.ToString()); 
                MessageBox.Show(ex.Message, "Error happened");//20121227 updated by Nancy
            }
        }

        private void AddPunchStatisticsTran(ReportConfig rep, PunchAction punchAction = PunchAction.Other)
        {
            //2013-12-02 Heidi added for add all Punch Statistics Transaction(5084)
            int reportCode = 0;
            int autIntNo = 0; //Convert.ToInt32(cmb_SeleAut.SelectedValue);
            string lastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            PunchStatisticsTranTypeList punchStatisticsTranType = new PunchStatisticsTranTypeList();
            PunchAction _punchAction = punchAction;
            SIL.AARTO.BLL.Utility.Printing.PunchStatistics punchStatistics = new SIL.AARTO.BLL.Utility.Printing.PunchStatistics(artoConnectionString);
                if (rep != null)
                {
                    try
                    {
                        int.TryParse(rep.ReportCode, out reportCode); autIntNo = rep.AutIntNo;
                        switch (reportCode)
                        {
                            case (int)ReportConfigCodeList.AdmissionOfGuiltRegister://2014-08-25 Heidi remove 'report' from reportName field(5364)
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintAGListings;
                                break;
                            case (int)ReportConfigCodeList.CriminalCaseRegister:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintCriminalCaseRegister;
                                break;
                            case (int)ReportConfigCodeList.PaymentsCancelled:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintGetPaymentsCancelled;
                                break;
                            case (int)ReportConfigCodeList.RegisterOfControlDocumentsC:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintJ7854;
                                break;
                            case (int)ReportConfigCodeList.RegisterOfControlDocuments40:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintJ7856;
                                break;
                            case (int)ReportConfigCodeList.OfficerErrors://2014-08-25 Heidi remove 'report' from reportName field(5364)
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintListOfOfficerErrors;
                                break;
                            case 919000:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintListOfPaymentsCancelledDetailed;
                                break;
                            case (int)ReportConfigCodeList.NonSummonsRegistrations:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintNonsummonsRegistrations;
                                break;
                            case 902800:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintNoticeNotHandedInOrNotCaptured;
                                break;
                            case (int)ReportConfigCodeList.SuspensePayments:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintPaymentsInSuspense;
                                break;
                            case (int)ReportConfigCodeList.PaymentsReceipted:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintPaymentsReceipted;
                                break;
                            case 900102:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintPresentationOfDocument;
                                break;
                            case (int)ReportConfigCodeList.PunchStatics:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintPunchStatistics;
                                break;
                            case 900103:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintReissueChangeOfOffender;
                                break;
                            case (int)ReportConfigCodeList.RepReceivedFromOffender:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintRepReceivedFromOffender;
                                break;
                            case (int)ReportConfigCodeList.SecondAppearanceCCR:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintReportSecondAppearanceCCR;
                                break;
                            case (int)ReportConfigCodeList.SpotFineRegister://2014-08-25 Heidi remove 'report' from reportName field(5364)
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintReportSpotFineRegister;
                                break;
                            case 900104:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintReturnOfServiceS54;
                                break;
                            case (int)ReportConfigCodeList.S341List:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintSection341ControlListByCourt;
                                break;
                            case (int)ReportConfigCodeList.SummaryOfPaymentsByThirdParty:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintSummaryOfPaymentsByThirdParty;
                                break;
                            case (int)ReportConfigCodeList.SummonsBookedOutOnCasesNowFinalised://2014-10-11 Heidi changed for changing report name(5322)
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintSummonsBookedOutList;
                                break;
                            case (int)ReportConfigCodeList.SummonsHandToServersList:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintSummonsHandToServersList;
                                break;
                            case 903201:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintTracingReportS56NoticesCapturedAndOnTracing;
                                break;
                            case 900101:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintUpdateReportCaseResults;
                                break;
                            case 901600:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintListOfWOARegister;
                                break;
                            case (int)ReportConfigCodeList.FirstNoticeVIDEOSummary:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintControlSheetFor1stNoticeVIDEO;
                                _punchAction = PunchAction.Other;
                                break;
                            case (int)ReportConfigCodeList.SecondNoticeSheet:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintControlSheetFor2ndNotice;
                                _punchAction = PunchAction.Other;
                                break;
                            case (int)ReportConfigCodeList.SecondNoticeVIDEOSummary:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintControlSheetFor2ndNoticeVIDEO;
                                _punchAction = PunchAction.Other;
                                break;
                            case (int)ReportConfigCodeList.SummonsSheet:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintControlSheetForSummons;
                                break;
                            case (int)ReportConfigCodeList.WOASheet:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintControlSheetForWOA;
                                break;
                            case (int)ReportConfigCodeList.CPA5DirectPrinting:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.CPA5DirectPrinting;
                                break;
                            case (int)ReportConfigCodeList.CPA5:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintCPA5;
                                break;
                            case (int)ReportConfigCodeList.CPA6DirectPrinting:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.CPA6DirectPrinting;
                                break;
                            case (int)ReportConfigCodeList.CPA6:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintCPA6;
                                break;
                            case (int)ReportConfigCodeList.GenerateLetters:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintGeneralLetters;
                                break;
                            case (int)ReportConfigCodeList.FirstNoticeVIDEO:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintReportFirstNoticeVIDEO;
                                break;
                            case (int)ReportConfigCodeList.NoticeOfWOAFormLetter:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintReportNoticeOfWOAFormLetter;
                                break;
                            case (int)ReportConfigCodeList.SecondNoticeOfBUS:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintSecondNoticeBusLane;
                                break;
                            case (int)ReportConfigCodeList.SecondNoticeVIDEO:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintSecondNoticeVIDEO;
                                break;
                            case (int)ReportConfigCodeList.SecondNotice:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintSecondNotice;
                                break;
                            case (int)ReportConfigCodeList.WOA:
                                punchStatisticsTranType = PunchStatisticsTranTypeList.PrintWOA;
                                break;
                            default:
                                break;
                        }
                        if ((int)punchStatisticsTranType > 0)
                        {
                            punchStatistics.PunchStatisticsTransactionAdd(autIntNo, lastUser, punchStatisticsTranType, _punchAction);
                        }
                    }
                    catch(Exception ex)
                    {
                        EntLibLogger.WriteLog(LogCategory.Exception, null, ex.Message);
                    }
                }
        }

        private DataTable GetSelectedDetailDataTable()
        {
            var dt = (dgvDetail.DataSource as BindingSource).DataSource as DataTable;
            if (dt == null)
            {
                MessageBox.Show(NO_DATA_FOUND_MSG);
                return null;
            }
            DataTable newdt  = dt.Clone();
            foreach (DataGridViewRow row in dgvDetail.Rows)
            {
                if (row.Cells[0].Value != null && row.Cells[0].Value.Equals(ROW_SELECTED_VALUE))
                {
                    newdt.Rows.Add((row.DataBoundItem as DataRowView).Row.ItemArray);
                }
            }
            return newdt;
        }


        private void CreateEngines()
        {
            string errorReport = "";
            var sb = new StringBuilder();
            ReportConfig rep = ReportManager.GetAllReportConfig().
                FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.CPA5).ToString());
            if (rep != null) flpEngineCPA5.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.CPA5.ToString()
                                  , (int)ReportConfigCodeList.CPA5));
            }

            rep = ReportManager.GetAllReportConfig().
                FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.CPA6).ToString());
            if (rep != null) flpEngineCPA6.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.CPA6.ToString()
                                  , (int)ReportConfigCodeList.CPA6));
            }
            #region 2013-09-16 added by Nancy for direct printing start
            rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.CPA5DirectPrinting).ToString());
            if (rep != null) flpEngineCPA5DirectPrinting.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                                   string.Format("Can't load report config for {0} by report code:{1}"
                                                 , ReportConfigCodeList.CPA5DirectPrinting.ToString()
                                                 , (int)ReportConfigCodeList.CPA5DirectPrinting));
            }
            rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.CPA6DirectPrinting).ToString());
            if (rep != null) flpEngineCPA6DirectPrinting.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                                   string.Format("Can't load report config for {0} by report code:{1}"
                                                 , ReportConfigCodeList.CPA6DirectPrinting.ToString()
                                                 , (int)ReportConfigCodeList.CPA6DirectPrinting));
            }
            #endregion 2013-09-16 added by Nancy for direct printing end
            #region 2013-12-23 Heidi added for 2nd Notice Direct Printing
            rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.SecondNoticeDirectPrinting).ToString());
            if (rep != null) flpEngine2ndNoticeDirectPrinting.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                                   string.Format("Can't load report config for {0} by report code:{1}"
                                                 , ReportConfigCodeList.SecondNoticeDirectPrinting.ToString()
                                                 , (int)ReportConfigCodeList.SecondNoticeDirectPrinting));
            }
            #endregion
            #region 2013-12-25 Heidi added for WOA Direct Printing (5101)
            rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOADirectPrinting).ToString());
            if (rep != null) flpEngineWOADirectPrinting.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                                   string.Format("Can't load report config for {0} by report code:{1}"
                                                 , ReportConfigCodeList.WOADirectPrinting.ToString()
                                                 , (int)ReportConfigCodeList.WOADirectPrinting));
            }
            #endregion
              #region 2013-12-25 Heidi added for WOA Direct Printing (5101)
            rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOASheet).ToString());
            if (rep != null) listEngineForSummaryWOADirectPrinting.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                                   string.Format("Can't load report config for {0} by report code:{1}"
                                                 , ReportConfigCodeList.WOASheet.ToString()
                                                 , (int)ReportConfigCodeList.WOASheet));
            }
            #endregion
            rep = ReportManager.GetAllReportConfig().FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOARegisterDirectPrinting).ToString());
            if (rep != null) listEngineForSummaryWOADirectPrinting.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                                   string.Format("Can't load report config for {0} by report code:{1}"
                                                 , ReportConfigCodeList.WOARegisterDirectPrinting.ToString()
                                                 , (int)ReportConfigCodeList.WOARegisterDirectPrinting));
            }  


            rep = ReportManager.GetAllReportConfig().
                FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.WOA).ToString());
            if (rep != null) flpEngineWOA.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.WOA.ToString()
                                  , (int)ReportConfigCodeList.WOA));
            }
            rep = ReportManager.GetAllReportConfig().
                FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.SecondNotice).ToString());
            if (rep != null) flpEngine2ndNot.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.SecondNotice.ToString()
                                  , (int)ReportConfigCodeList.SecondNotice));
            }

            //rep = ReportManager.GetAllReportConfig().
            //   FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.FirstNoticeHWOSummary).ToString());
            //if (rep != null) listEngineFor1stNoticeHwo.CreateEngine(rep.RcIntNo);
            //else
            //{
            //    sb.AppendLine(
            //        string.Format("Can't load report config for {0} by report code:{1}"
            //                      , ReportConfigCodeList.FirstNoticeHWOSummary.ToString()
            //                      , (int)ReportConfigCodeList.FirstNoticeHWOSummary));
            //}

            //rep = ReportManager.GetAllReportConfig().
            //   FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.FirstNoticeVIDEOSummary).ToString());
            //if (rep != null) listEngineFor1stNoticeVideo.CreateEngine(rep.RcIntNo);
            //else
            //{
            //    sb.AppendLine(
            //        string.Format("Can't load report config for {0} by report code:{1}"
            //                      , ReportConfigCodeList.FirstNoticeVIDEOSummary.ToString()
            //                      , (int)ReportConfigCodeList.FirstNoticeVIDEOSummary));
            //}
            // 2013-09-04 comment out by Henry for useless
            //rep = ReportManager.GetAllReportConfig().
            //    FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.FirstNoticeHWO).ToString());
            //if (rep != null) flpEngine1stNoticeHwo.CreateEngine(rep.RcIntNo);
            //else
            //{
            //    sb.AppendLine(
            //        string.Format("Can't load report config for {0} by report code:{1}"
            //                      , ReportConfigCodeList.FirstNoticeHWO.ToString()
            //                      , (int)ReportConfigCodeList.FirstNoticeHWO));
            //}
            rep = ReportManager.GetAllReportConfig().
                FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.FirstNoticeVIDEO).ToString());
            if (rep != null) flpEngine1stNoticeVideo.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.FirstNoticeVIDEO.ToString()
                                  , (int)ReportConfigCodeList.FirstNoticeVIDEO));
            }
            rep = ReportManager.GetAllReportConfig().
                FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.GenerateLetters).ToString());
            if (rep != null) flpEngineGenerateLetters.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.GenerateLetters.ToString()
                                  , (int)ReportConfigCodeList.GenerateLetters));
            }
            rep = ReportManager.GetAllReportConfig().
                FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.NoticeOfWOAFormLetter).ToString());
            if (rep != null) flpEngineNoticeofWOA.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.NoticeOfWOAFormLetter.ToString()
                                  , (int)ReportConfigCodeList.NoticeOfWOAFormLetter));
            }
            rep = ReportManager.GetAllReportConfig().
               FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.SecondNoticeOfBUS).ToString());
            if (rep != null) flpEngine2ndNotofBUS.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.SecondNoticeOfBUS.ToString()
                                  , (int)ReportConfigCodeList.SecondNoticeOfBUS));
            }
            //Jerry 2013-03-18 add
            rep = ReportManager.GetAllReportConfig().
                FirstOrDefault(rc => rc.ReportCode == ((int)ReportConfigCodeList.SecondNoticeVIDEO).ToString());
            if (rep != null) flpEngine2ndNoticeVideo.CreateEngine(rep.RcIntNo);
            else
            {
                sb.AppendLine(
                    string.Format("Can't load report config for {0} by report code:{1}"
                                  , ReportConfigCodeList.SecondNoticeVIDEO.ToString()
                                  , (int)ReportConfigCodeList.SecondNoticeVIDEO));
            }

            string sbstr = sb.ToString();
            if (!string.IsNullOrEmpty(sbstr))
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, sbstr);
                MessageBox.Show(sbstr + "\r\nPlease look in the log folder for further error details", "Error loading report config.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        private void GetNewPrintData()
        {
            if (currentEngine != null)
            {
                GetNewPrintData(CurrentEngine);
            }
        }

        private void GetNewPrintData(FreeStylePrintEngine engine)
        {
            //CurrentEngine = engine;
            CurrentFormMode = newMode; 
            //DataTable dt = CurrentEngine.GetPrintData();
            //this.bindingSource1.DataSource = dt;
            UpdateModelValue();

            DataTable dtFiles = BindFilesGridView();// 2013-04-16 add by Henry for pagination

            RebindFiles(dtFiles);
        }

        // 2013-04-16 add by Henry for pagination
        private DataTable BindFilesGridView(int pageSize = 20, int pageIdex = 1)
        {
            filesPaginateBar.Visible = true;
            rptModel.PageIndex = pageIdex;
            rptModel.PageSize = pageSize;
            //20131217 added by Jacob, add filter for filename(partial matching) when getting new data.
            if (CurrentFormMode is CompletedFilesMode)
            {
                rptModel.FileNameForFilter = this.tbSearchFileNameForFileList.Text;
            }
            else
            {
                rptModel.FileNameForFilter = "";
            }
            DataTable dtFiles = CurrentEngine.CurrentReportDataService.LoadPrintFiles(rptModel);

            lblFilesCurrentPage.Text = (rptModel.TotalPage == 0 ? 0 : rptModel.PageIndex).ToString();
            lblFilesTotalPage.Text = rptModel.TotalPage.ToString();
            cmbFilesPageSize.Text = rptModel.PageSize.ToString();
            lblFilesTotalCount.Text = rptModel.TotalCount.ToString();

            btnFilesFirstPage.Enabled = btnFilesPrePage.Enabled = rptModel.TotalPage > 1 && rptModel.PageIndex != 1;
            btnFilesLastPage.Enabled = btnFilesNextPage.Enabled = rptModel.TotalPage > 1 && rptModel.PageIndex != rptModel.TotalPage;

            return dtFiles;
        }

        /// <summary>
        /// update report model 
        /// set values.
        /// </summary>
        private void UpdateModelValue()
        {
            this.rptModel.RcIntNo = SelectedRcIntNo;
            this.rptModel.DateFrom = dtpFrom.Value.Date;
            this.rptModel.DateTo = dtpTo.Value.Date.AddDays(1);//2013-11-15 Updated by Nancy '.AddSeconds(-1)'
            this.rptModel.AutIntNo = cmb_SeleAut.SelectedValue.ToString();
        }

        private void RebindFiles(DataTable dtFiles)
        {
            bsForFiles.DataSource = dtFiles;
            bsForFiles.ResetBindings(true);
            //this.dgvFiles.DataSource = dtFiles;
            //20131217 edited by Jacob start, auto resize all columns
            //if (dgvFiles.Columns.Count > 1) dgvFiles.Columns[1].Width = 300;
            if (dgvFiles.Columns.Count > 1) dgvFiles.AutoResizeColumns();
            //20131217 edited by Jacob end
            //2013-12-09 Added by Nancy-Modified date format 
            ApplyDateFormat(dgvFiles);
        }

        private void LoadHistroyData(int pageSize = 20, int pageIndex = 1, bool isSelectPage = false)
        {
            //Add by Brian 2013-10-11
            this.FileName = this.txtFileName.Text.Trim();
            this.DocumentName = this.txtDocumentNumber.Text.Trim();


            //CurrentFormMode = hisMode;////20131217 removed by Jacob
            if (CurrentEngine != null && CurrentEngine.CurrentReportDataService != null)
            {
                CurrentFormMode = hisMode; //20131217 added by Jacob
                //Add by Brian 2013-10-11         
                int totalCount = 0;// 2013-04-17 add by Henry for pagination
                DataTable dtHis = CurrentEngine.CurrentReportDataService.LoadPrintHistory(this.FileName, this.DocumentName, pageSize, pageIndex, out totalCount);
                int totalPage = (int)Math.Ceiling(totalCount / (pageSize * 1.0));

                if (totalPage < pageIndex)
                {
                    pageIndex = 1;
                    txtHistoryPageToGo.Text = isSelectPage ? "1" : string.Empty;
                    dtHis = CurrentEngine.CurrentReportDataService.LoadPrintHistory(this.FileName, this.DocumentName, pageSize, pageIndex, out totalCount);
                }

                lblHistoryCurrentPage.Text = (totalPage == 0 ? 0 : pageIndex).ToString();
                cmbHistoryPageSize.Text = pageSize.ToString();
                lblHistoryTotalPage.Text = totalPage.ToString();
                lblHistoryTotalCount.Text = totalCount.ToString();

                btnHistoryFirstPage.Enabled = btnHistoryPrePage.Enabled = totalPage > 1 && pageIndex != 1;
                btnHistoryLastPage.Enabled = btnHistoryNextPage.Enabled = totalPage > 1 && pageIndex != totalPage;

                dgvHistory.DataSource = dtHis;
                dgvHistory.Visible = true;
                ApplyDateFormat(dgvHistory);//2013-12-09 Added by Nancy(date Format)
                bsForFiles.DataSource = null;
                bsForDetail.DataSource = null;
            }
        }


        private void bindingSource1_DataSourceChanged(object sender, EventArgs e)
        {
            dgvDetail.AutoGenerateColumns = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //Add by Brian 2013-10-12
            this.txtFileName.Text = string.Empty;
            this.txtDocumentNumber.Text = string.Empty;
            this.FileName = string.Empty;
            this.DocumentName = string.Empty;

            filesPaginateBar.Visible = false;
            try
            {
                if (Convert.ToBoolean((cmb_SelectReportType.SelectedItem as DataRowView)["IsFreeStyle"]))
                    LoadHistroyData();
                else
                {
                    LoadHistoryForListReport();
                }

                SelectAll(dgvDetail);
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, ex.Message);
                MessageBox.Show(ex.Message + "\r\nPlease look in the log folder for further error details", "Error loading report config", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void LoadHistoryForListReport()
        {
            this.UpdateModelValue();
            //CurrentEngine = commonListReprint;
            commonListReprint.connStr = artoConnectionString;
            CurrentFormMode = listReportHistoryModeMode;
            ReportModel model = commonListReprint
                .CreateReportModel(rptModel.RcIntNo,
                       //edited by Jacob 20120903 start
                       //get value from winform controls, instead of constant value.
                                rptModel.DateFrom,// Convert.ToDateTime("1990/1/1"),
                                rptModel.DateTo);// Convert.ToDateTime("2070/1/1"));
                       //edited by Jacob 20120903 end
            //model.AutIntNo = SelectedAutIntNo.ToString();//added by Jacob 20120903 for test //please delete before submit
            DataTable dt = commonListReprint.CreateHistoryPrintData(model);
            //2013-11-14 Updated by Nancy start
            //add if..else clause( when select the date filter - preview/print the report - no showing of data, except CCR & Second CCR)
            if (commonListReprint.DirectPrintAfterLoadHistory)
            {
                commonListReprint.CreatePreviewEngine(model, dt);
            }
            else
            {
                this.bsForDetail.DataSource = dt;
                ApplyDateFormat(dgvDetail);//2013-12-10 aded by Nancy-Modified date format
            }
           // commonListReprint.CreateMyEngine(model, dt);
        }

        private void dataGridView2_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvHistory.CurrentRow != null &&
                    dgvHistory.CurrentRow.Index >= 0)
                {
                    //CurrentFormMode = hisMode;
                    int hisId = Convert.ToInt32(dgvHistory.CurrentRow.Cells[0].Value);
                    DataTable dtReportData =
                        currentEngine.CurrentReportDataService.LoadReportDataHistory(hisId, this.FileName, this.DocumentName);
                    bsForDetail.DataSource = dtReportData;
                    ApplyDateFormat(dgvDetail);//2013-12-10 aded by Nancy-Modified date format
                    //this.dgvHistory.Visible = false;
                    //added by jacob 20120626 start
                    //per Dawn's request
                    //set row number to row header cell,
                    //so that user can easily set the "start from" number for reprint.
                    int i = 0;
                    foreach (DataGridViewRow row in dgvDetail.Rows)
                    {
                        i++;
                        row.HeaderCell.Value = i.ToString();
                    }
                    //added by jacob 20120626 end
                    BuildHistoryFileList(dtReportData);
                }
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, ex.Message);
                MessageBox.Show(ex.Message + "\r\nPlease look in the log folder for further error details", "Error loading history", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BuildHistoryFileList(DataTable dtReportData)
        {
            var dtFiles = new DataTable();
            dtFiles.Columns.Add("filename");
            foreach (DataRow row in dtReportData.Rows)
            {
                string filename = row[CurrentEngine.FieldForFileName].ToString();
                bool existed = false;
                foreach (DataRow fRow in dtFiles.Rows)
                {
                    string fName = fRow[0].ToString();
                    if (filename == fName)
                    {
                        existed = true;
                        break;
                    }
                }

                if (!existed)
                {
                    dtFiles.Rows.Add(new object[] {filename});
                }
            }
            RebindFiles(dtFiles);
        }

        ////20131217 added by Jacob, true only when both flags are true.
        //if true, current report is new file reports.
        private bool IsFreeStyleAndFileReport()
        {
            try
            {
                return Convert.ToBoolean((cmb_SelectReportType.SelectedItem as DataRowView)["AutomaticGenerate"])
                    && Convert.ToBoolean((cmb_SelectReportType.SelectedItem as DataRowView)["IsFreeStyle"]);
            }
            catch (Exception ex)
            {
                return false;// no need to handle this exception. just for inner usage
            }
        }

        private void cmb_SelectReportType_SelectedValueChanged(object sender, EventArgs e)
        {
            if (rptModel != null)
            {
                btnGetNewData.Visible = true;//Jacob. integerate list report reprinting.
                dtpFrom.Visible = false;
                dtpTo.Visible = false; CurrentFormMode = newMode;
                this.cb_AddHistory.Visible = true;
                this.cb_OnlyHistory.Visible = true;
                
                int rptType = Convert.ToInt32((cmb_SelectReportType.SelectedValue == null || cmb_SelectReportType.SelectedValue.ToString() == "") ? "0" : cmb_SelectReportType.SelectedValue);
                    // (int)(cmb_SelectReportType.SelectedValue);
                switch (rptType)
                {
                    case (int) ReportConfigCodeList.CPA5:
                        CurrentEngine = flpEngineCPA5;

                        break;
                    case (int) ReportConfigCodeList.CPA6:
                        CurrentEngine = flpEngineCPA6;

                        break;
                    //2013-09-16 added by Nancy for direct printing start
                    case (int)ReportConfigCodeList.CPA5DirectPrinting:
                        CurrentEngine = flpEngineCPA5DirectPrinting;
                        CurrentEngine.CurrentMode = newFilesMode;//override newMode for loading data.
                        //CurrentEngine.CurrentReportDataService.InitializeReportConfig(SelectedRcIntNo);
                        break;
                    case (int)ReportConfigCodeList.CPA6DirectPrinting:
                        CurrentEngine = flpEngineCPA6DirectPrinting;
                        CurrentEngine.CurrentMode = newFilesMode;//override newMode.
                        break;
                    //2013-09-16 added by Nancy for direct printing end
                    //2013-12-23 Heidi added for 2nd Notice Direct Printing (5101)
                    case (int)ReportConfigCodeList.SecondNoticeDirectPrinting:
                        CurrentEngine = flpEngine2ndNoticeDirectPrinting;
                        CurrentEngine.CurrentMode = newFilesMode;//override newMode.
                        break;


                    //2013-12-25 Heidi added for WOA Direct Printing (5101)
                    case (int)ReportConfigCodeList.WOADirectPrinting:
                        CurrentEngine = flpEngineWOADirectPrinting;
                        CurrentEngine.CurrentMode = newFilesMode;//override newMode.
                        break;
                   
                        
                    case (int) ReportConfigCodeList.SecondNotice:
                        CurrentEngine = flpEngine2ndNot;
                        break;
                    case (int) ReportConfigCodeList.WOA:
                        CurrentEngine = flpEngineWOA;
                        break;
                    case (int) ReportConfigCodeList.SummonsSheet:
                        CurrentEngine = listEngineForSum;
                        break;
                  
                    //20140528 Jacob start
                    //case (int)ReportConfigCodeList.WOASheet:
                    //    CurrentEngine = listEngineForWOA;
                    //    break;
                    case (int)ReportConfigCodeList.WOASheet:
                        CurrentEngine = this.listEngineForWOA;
                        break;
                    case (int)ReportConfigCodeList.WOARegisterDirectPrinting:
                        CurrentEngine = this.listEngineForSummaryWOADirectPrinting;
                        CurrentEngine.CurrentMode = newFilesMode;//override newMode.
                        CurrentFormMode = newFilesMode;
                        break;
                    //20140528 Jacob end
                    case (int) ReportConfigCodeList.SecondNoticeSheet:
                        //case(int)ReportConfigCodeList.SecondNoticeSheetOfBus: 2013-10-31 removed by Nancy for removing the report 'Bus 2nd notice control sheet'
                        CurrentEngine = listEngineForSecondNotice;
                        break;


                        
                    //2013-06-20 removed by Nancy
                    //case (int)ReportConfigCodeList.FirstNoticeHWOSummary:
                    //    CurrentEngine = listEngineFor1stNoticeHwo;
                    //    break;
                    case (int)ReportConfigCodeList.FirstNoticeVIDEOSummary:
                        CurrentEngine = listEngineFor1stNoticeVideo;
                        break;
                    //2013-06-20 removed by Nancy
                    //case (int) ReportConfigCodeList.FirstNoticeHWO:
                    //    CurrentEngine = flpEngine1stNoticeHwo;
                    //    break;
                    case (int) ReportConfigCodeList.FirstNoticeVIDEO:
                        CurrentEngine = flpEngine1stNoticeVideo;
                        break;
                    case (int) ReportConfigCodeList.GenerateLetters:
                        CurrentEngine = flpEngineGenerateLetters;
                        break;
                    case (int)ReportConfigCodeList.NoticeOfWOAFormLetter:
                        CurrentEngine = flpEngineNoticeofWOA;
                        break;
                    case (int)ReportConfigCodeList.SecondNoticeOfBUS:
                        CurrentEngine = flpEngine2ndNotofBUS;
                        break;
                    case (int)ReportConfigCodeList.SecondNoticeVIDEO://Jerry 2013-03-18 add
                        CurrentEngine = flpEngine2ndNoticeVideo;
                        break;
                    case (int)ReportConfigCodeList.SecondNoticeVIDEOSummary://Jerry 2013-03-18 add
                        CurrentEngine = listEngineFor2ndNoticeVideo;
                        break;
                    default:
                        //list reports?
                        //Jacob , use common factory to get list engine.
                        commonListReprint = EngineFactory.GetByReportConfig(rptType);
                        // 2013-07-23 add by Henry
                        commonListReprint.LastUser = string.Format("{0}_{1}",Program.APP_NAME, GlobalVariates<User>.CurrentUser.UserLoginName); 
                        //if (rptType == (int)ReportConfigCodeList.CriminalCaseRegister)
                        //    commonListReprint = new ReportDataServiceForCriminalCaseRegister();
                        //else
                        //    commonListReprint = new ListPrintEngine();

                        btnGetNewData.Visible = false; //Jacob. integerate list report reprinting.
                        dtpFrom.Visible = true;
                        dtpTo.Visible = true;
                        CurrentFormMode = listReportHistoryModeMode;
                        this.cb_AddHistory.Visible = false;
                        this.cb_OnlyHistory.Visible = false;
                        button7.Text = commonListReprint.QueryButtonText;//2013-11-19 add by Nancy
                        break;
                }
                //2013-12-06 added by Nancy
                #region Initialize
                bsForFiles.DataSource = null;
                this.toolStripTextBox1.Text = "0";
                this.toolStripLabel1.Text = "0";
                this.txtStart.Text = "1";
                this.lblFilesCurrentPage.Text = "0";
                this.lblFilesTotalPage.Text = "0";
                this.lblFilesTotalCount.Text = "0";
                this.cmbFilesPageSize.Text = "";
                this.txtFilesPageToGo.Text = "";
                #endregion

               
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            GetNewPrintData();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SelectAll(dgvDetail);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            DeselectAll(dgvDetail);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            ReverseSelect(dgvDetail);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            GetDetailReportDataByFiles();
        }

        private void GetDetailReportDataByFiles()
        {
            var files = new List<string>();
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                if (row.Cells[0].Value == ROW_SELECTED_VALUE)
                {
                    files.Add(row.Cells[1].Value.ToString());
                }
            }
            if (files.Count > 0)
            {
                DataTable dt = CurrentEngine.CurrentReportDataService.LoadReportDataByPrintFiles(files,
                                                                                                 Convert.ToInt32(
                                                                                                     cmb_SeleAut.
                                                                                                         SelectedValue));
                bsForDetail.DataSource = dt;
                ApplyDateFormat(dgvDetail);//2013-12-10 aded by Nancy-Modified date format
            }
        }

        private void dgvFiles_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dgvFiles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvFiles.EndEdit();
            if (e.ColumnIndex == 0 & e.RowIndex >= 0)
            {
                if (CurrentFormMode is ReportHistoryMode)
                {
                    //GetDetailReportDataByFiles();
                    var ccell = (dgvFiles.Rows[e.RowIndex].Cells[e.ColumnIndex] as DataGridViewCheckBoxCell);
                    object val = ccell.Value;
                    string checkStatus = val == null ? ROW_NOT_SELECTED_VALUE : val.ToString();
                    string filename = dgvFiles.Rows[e.RowIndex].Cells[1].Value.ToString();
                    foreach (DataGridViewRow row in dgvDetail.Rows)
                    {
                        string detailFileName = row.Cells[currentEngine.FieldForFileName].Value.ToString();
                        if (filename == detailFileName)
                        {
                            row.Cells[0].Value = checkStatus;
                        }
                    }
                }
            }
        }

        private void dgvDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (currentFormMode is ListReportHistoryMode)
                return;
            filNameList.Clear();
            foreach (DataGridViewRow row in dgvDetail.Rows)
            {
                var checkCell = (DataGridViewCheckBoxCell) row.Cells[0];
                string cellCheck = checkCell.EditedFormattedValue.ToString();
                
                string detailFileName = row.Cells[currentEngine.FieldForFileName].Value.ToString();
                if (!filNameList.ContainsKey(detailFileName))
                {
                    filNameList.Add(detailFileName, cellCheck);
                }
                else
                {
                    if (cellCheck == "False")
                    {
                        filNameList[detailFileName] = cellCheck;
                    }
                }
            }
            foreach (DataGridViewRow row in dgvFiles.Rows)
            {
                string fileName = row.Cells[1].Value.ToString();
                if (filNameList.ContainsKey(fileName))
                {
                    row.Cells[0].Value = filNameList[fileName];
                }
            }
        }

        private void SelectAll(DataGridView dgvTarget)
        {
            foreach (DataGridViewRow row in dgvTarget.Rows)
            {
                row.Cells[0].Value = ROW_SELECTED_VALUE;
            }
        }

        private void DeselectAll(DataGridView dgvTarget)
        {
            foreach (DataGridViewRow row in dgvTarget.Rows)
            {
                row.Cells[0].Value = ROW_NOT_SELECTED_VALUE;
            }
        }

        private void ReverseSelect(DataGridView dgvTarget)
        {
            foreach (DataGridViewRow row in dgvTarget.Rows)
            {
                row.Cells[0].Value = row.Cells[0].Value ==
                                     ROW_NOT_SELECTED_VALUE
                                         ? ROW_SELECTED_VALUE
                                         : ROW_NOT_SELECTED_VALUE;
            }
        }


        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            SelectAll(dgvFiles);
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            DeselectAll(dgvFiles);
        }

        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            ReverseSelect(dgvFiles);
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            int start = 0;
            if (int.TryParse(txtStart.Text, out start))
            {
                if (Convert.ToInt32(txtStart.Text) > 0 && (Convert.ToInt32(txtStart.Text) <= dgvDetail.Rows.Count))
                {
                    for (int i = 0; i < start - 1; i++)
                    {
                        dgvDetail.Rows[i].Cells[0].Value = ROW_NOT_SELECTED_VALUE;
                    }
                    for (int i = start - 1; i < dgvDetail.Rows.Count; i++)
                    {
                        dgvDetail.Rows[i].Cells[0].Value = ROW_SELECTED_VALUE;
                    }
                }
            }
            else
            {
                MessageBox.Show(START_NUMBER_INPUT_MENTION);
            }
        }

        private void bindingSource2_DataSourceChanged(object sender, EventArgs e)
        {
            dgvFiles.AutoGenerateColumns = true;
        }

        private void cmb_SeleAut_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rptModel != null) rptModel.AutIntNo = cmb_SeleAut.SelectedValue.ToString();
        }

     
        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        // 2013-04-16 add by Henry for pagination
        private void filesGetPage_Click(object sender, EventArgs e)
        {
            string name = ((ToolStripButton)sender).Name;
            int pageSize = 0;
            int pageIndex = 0;
            int.TryParse(lblFilesCurrentPage.Text, out pageIndex);
            int.TryParse(cmbFilesPageSize.Text, out pageSize);

            int lastPage = 1;
            int.TryParse(lblFilesTotalPage.Text, out lastPage);

            pageIndex =
                name == btnFilesNextPage.Name ? pageIndex + 1 :
                name == btnFilesPrePage.Name ? pageIndex - 1 :
                name == btnFilesFirstPage.Name ? 1 :
                name == btnFilesLastPage.Name ? lastPage : pageIndex;

            if (name == btnGoFilesPage.Name)
            {
                int pageToGo;
                int.TryParse(txtFilesPageToGo.Text, out pageToGo);
                pageToGo = pageToGo == 0 ? 1 : pageToGo;
                pageIndex = pageToGo;
            }

            DataTable dtFiles = BindFilesGridView(pageSize, pageIndex);

            if (rptModel.TotalPage < pageIndex)
            {
                pageIndex = 1;
                dtFiles = BindFilesGridView(pageSize, pageIndex);
                txtFilesPageToGo.Text = pageIndex.ToString();
            }

            RebindFiles(dtFiles);
        }


        // 2013-04-17 add by Henry for pagination
        private void historyGetPage_Click(object sender, EventArgs e)
        {
            string name = ((ToolStripButton)sender).Name;
            int pageSize = 0;
            int pageIndex = 0;
            int.TryParse(lblHistoryCurrentPage.Text, out pageIndex);
            int.TryParse(cmbHistoryPageSize.Text, out pageSize);

            int lastPage = 1;
            int.TryParse(lblHistoryTotalPage.Text, out lastPage);

            pageIndex =
                name == btnHistoryNextPage.Name ? pageIndex + 1 :
                name == btnHistoryPrePage.Name ? pageIndex - 1 :
                name == btnHistoryFirstPage.Name ? 1 :
                name == btnHistoryLastPage.Name ? lastPage : pageIndex;

            if (name == btnGoHistoryPage.Name)
            {
                int pageToGo;
                int.TryParse(txtHistoryPageToGo.Text, out pageToGo);
                pageToGo = pageToGo == 0 ? 1 : pageToGo;
                pageIndex = pageToGo;
            }

            LoadHistroyData(pageSize, pageIndex, true);
        }

        /// <summary>
        /// Search History 
        /// Add by Brian 2013-10-11
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadHistroyData();
    		}
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Exception, null, ex.Message);
                MessageBox.Show(ex.Message + "\r\nPlease look in the log folder for further error details", "Error loading report config", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

 		/// <summary>
        /// Add by Brian 2013-10-17
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReportPrintSettings_Click(object sender, EventArgs e)
        {
            int autIntNo = int.Parse(this.cmb_SeleAut.SelectedValue.ToString());
            string autName = this.cmb_SeleAut.Text;
            List<ReportConfig> reportConfigList = new ReportConfigService().GetByAutIntNo(autIntNo).ToList();
            reportConfigList.Insert(0, new ReportConfig() { RcIntNo = 0, ReportName = string.Empty });
            ReportPrintSettingsForm.AutIntNo = autIntNo;
            ReportPrintSettingsForm.ReportConfigList = reportConfigList;
            ReportPrintSettingsForm.ShowForm(this.Width, this.Height, autName);
        }

        /// <summary>
        /// Modified date format
        /// Added by Nancy 2013-12-09
        /// </summary>
        /// <param name="dgvTarget"></param>
        private void ApplyDateFormat(DataGridView dgvTarget)
        {
            foreach (DataGridViewColumn col in dgvTarget.Columns)
            {
                if ((col.ValueType == typeof(System.DateTime)) && (col.Name == "FileCreateDate" || col.Name == "NotOffenceDate" || col.Name == "SumPrintSummonsDate") || col.Name == "WOAGenerateDate" || col.Name == "PrintTime")
                {
                    col.DefaultCellStyle.Format = "yyyy/MM/dd HH:mm";
                }
                else if (col.ValueType == typeof(System.DateTime))
                {
                    col.DefaultCellStyle.Format = "yyyy/MM/dd";
                }
            }
        }

        private void btn_ViewCompleteFiles_Click(object sender, EventArgs e)
        {
            //this.tbSearchFileNameForFileList.Text = "";
            CurrentFormMode = completedFilesMode;
           
            UpdateModelValue();

            DataTable dtFiles = BindFilesGridView();// 2013-04-16 add by Henry for pagination

            RebindFiles(dtFiles);
        }

        private void btn_ViewNewFiles_Click(object sender, EventArgs e)
        {
            GetNewPrintFiles();
       
        }

        private void GetNewPrintFiles()
        { 
            //CurrentEngine = engine;
            CurrentFormMode = newFilesMode;
            // CurrentEngine.CurrentMode = newMode;
            //CurrentEngine.CurrentReportDataService.InitializeReportConfig(SelectedRcIntNo);
            //DataTable dt = CurrentEngine.GetPrintData();
            //this.bindingSource1.DataSource = dt;
            UpdateModelValue();

            DataTable dtFiles = BindFilesGridView();// 2013-04-16 add by Henry for pagination

            RebindFiles(dtFiles);
        }

        private void btnSearchFileNameForFileList_Click(object sender, EventArgs e)
        {
            CurrentFormMode = completedFilesMode;
            
            UpdateModelValue();

            DataTable dtFiles = BindFilesGridView();// 2013-04-16 add by Henry for pagination

            RebindFiles(dtFiles);
        }
    }
}