﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.ReportEngine
{
    /// <summary>
    /// Add by Brian 2013-10-18
    /// Printer Settings Edit Form
    /// </summary>
    public partial class PrinterSettingsEditForm : EditForm
    {
        //Current entity of Data Service Instantiate
        private PrinterSettingsService _PrinterSettingsService = null; 

        /// <summary>
        /// Constructor
        /// </summary>
        public PrinterSettingsEditForm()
        {
            this.EntityType = typeof(PrinterSettings);
            this._PrinterSettingsService = new PrinterSettingsService();
            InitializeComponent();
        }

        /// <summary>
        /// Override to expand get entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override IEntity ExpandGetEntity(IEntity entity)
        {
            PrinterSettings editEntity = entity as PrinterSettings;
            PrinterSettings entityData = this.Entity as PrinterSettings;
            if (entityData != null)
            {
                editEntity.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            }
            else
            {
                editEntity.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
                editEntity.CreateDate = DateTime.Now;
            }
            editEntity.AutIntNo = ReportPrintSettingsForm.AutIntNo;
            return entity;
        }

        /// <summary>
        /// Override to validate entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool ValidateData(IEntity entity)
        {
            PrinterSettings editEntity = (PrinterSettings)entity;
            if (string.IsNullOrEmpty(editEntity.PsName))
            {
                MessageBox.Show("Sorry, the printer name can't be empty!");
                return false;
            }
            if (string.IsNullOrEmpty(editEntity.PsPath))
            {
                MessageBox.Show("Sorry, the printer path can't be empty!");
                return false;
            }
            if (this.Entity == null || (this.Entity as PrinterSettings).PsName != editEntity.PsName)
            {
                PrinterSettings ps = this._PrinterSettingsService.GetByPsName(editEntity.PsName).FirstOrDefault();
                if (ps != null)
                {
                    MessageBox.Show("Sorry, the printer name already exists!");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Override to insert entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool InsertEntity(IEntity entity)
        {
            return this._PrinterSettingsService.Insert((PrinterSettings)entity);
        }

        /// <summary>
        /// Override to update entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool UpdateEntity(IEntity entity)
        {
            return this._PrinterSettingsService.Update((PrinterSettings)entity);
        }

        /// <summary>
        /// Click event of OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.OperationEntity())
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
