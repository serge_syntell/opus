﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;

namespace SIL.AARTO.ReportEngine
{
    /// <summary>
    /// Add by Brian 2013-10-18
    /// Edit Form interface
    /// </summary>
    public interface IEditForm : IDisposable
    {
        //Properties
        IEntity Entity { get; set; }
        Type EntityType { get; set; }
        
        //Methods
        void EntityLoad();
        DialogResult ShowDialog();
    }

    /// <summary>
    /// Add by Brian 2013-10-18
    /// Edit Form class
    /// </summary>
    public class EditForm :Form ,IEditForm
    {
        //Properties
        public IEntity Entity { get; set; }
        public Type EntityType { get; set; }
        public List<PropertyInfo> PropertyList { get; set; }

        /// <summary>
        /// Expand Entity data load  for virtual method
        /// </summary>
        public virtual void ExpandEntityLoad()
        {
        }

        /// <summary>
        /// Entity data load
        /// </summary>
        public void EntityLoad()
        {
            if (this.Entity != null)
            {
                this.PropertyList = this.Entity.GetType().GetProperties().ToList();
                var pcs = (from a in this.Controls.Cast<Control>()
                           from b in this.PropertyList
                           where a.Tag != null && a.Tag.ToString().ToLower() == b.Name.ToLower()
                           select new { a, b });
                foreach (var p in pcs)
                {
                    this.SetControlValue(p.a, p.b.GetValue(this.Entity, null));
                }
            }
        }

        /// <summary>
        /// Set the control value
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="value"></param>
        private void SetControlValue(Control ctl, object value)
        {
            if (value == null)
            {
                return;
            }
            if (ctl is TextBox)
            {
                ctl.Text = value.ToString();
            }
            else if (ctl is ComboBox)
            {
                if (string.IsNullOrEmpty((ctl as ComboBox).ValueMember))
                {
                    (ctl as ComboBox).Text = value.ToString();
                }
                else
                {
                    (ctl as ComboBox).SelectedValue = value;
                }
            }
            else if (ctl is CheckBox)
            {
                if ((bool)value)
                {
                    (ctl as CheckBox).Checked = true;
                }
                else
                {
                    (ctl as CheckBox).Checked = false;
                }
            }
        }

        /// <summary>
        /// Get control value
        /// </summary>
        /// <param name="ctl"></param>
        /// <returns></returns>
        private object GetControlValue(Control ctl)
        {
            if (ctl is TextBox)
            {
                return ctl.Text.Trim();
            }
            else if (ctl is ComboBox)
            {
                if (string.IsNullOrEmpty((ctl as ComboBox).ValueMember))
                {
                    return (ctl as ComboBox).Text;
                }
                else
                {
                    return (ctl as ComboBox).SelectedValue;
                }
            }
            else if (ctl is CheckBox)
            {
                return (ctl as CheckBox).Checked;
            }
            return ctl.Text;
        }

        /// <summary>
        /// Expand get entity data for virtual method
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual IEntity ExpandGetEntity(IEntity entity)
        {
            return entity;
        }

        /// <summary>
        /// Get entity data
        /// </summary>
        /// <returns></returns>
        public IEntity GetEntity()
        {
            object entity = Activator.CreateInstance(this.EntityType);
            if (this.PropertyList == null)
            {
                this.PropertyList = entity.GetType().GetProperties().ToList();
            }
            if (this.Entity != null)
            {
                var ps = (from a in this.PropertyList
                          from b in this.Entity.TableColumns
                          where a.Name.ToLower() == b.ToLower()
                          select a).ToList();
                ps.ForEach(p =>
                {
                    p.SetValue(entity, p.GetValue(this.Entity, null), null);
                });
            }
            var pcs = (from a in this.Controls.Cast<Control>()
                       from b in this.PropertyList
                       where a.Tag != null && a.Tag.ToString().ToLower() == b.Name.ToLower()
                       select new { a, b });
            foreach (var p in pcs)
            {
                p.b.SetValue(entity, this.ChangeType(this.GetControlValue(p.a), p.b.PropertyType), null);
            }
            return this.ExpandGetEntity((IEntity)entity);
        }

        /// <summary>
        /// Change value type 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public object ChangeType(object value, Type type)
        {
            if (value == null)
            {
                return null;
            }
            if (type != typeof(string) && string.IsNullOrEmpty(value.ToString()))
            {
                return null;
            }
            if (type == typeof(Guid) || type == typeof(Nullable<Guid>))
            {
                if (value is Guid)
                {
                    return value;
                }
                return new Guid(value.ToString());
            }
            else if (type == typeof(int) || type == typeof(Nullable<int>))
            {
                if (value is int)
                {
                    return value;
                }
                return int.Parse(value.ToString());
            }
            else if (type == typeof(char) || type == typeof(Nullable<char>))
            {
                if (value is char)
                {
                    return value;
                }
                return char.Parse(value.ToString());
            }
            else if (type == typeof(byte) || type == typeof(Nullable<byte>))
            {
                if (value is byte)
                {
                    return value;
                }
                return byte.Parse(value.ToString());
            }
            else if (type == typeof(short) || type == typeof(Nullable<short>))
            {
                if (value is short)
                {
                    return value;
                }
                return short.Parse(value.ToString());
            }
            else if (type == typeof(long) || type == typeof(Nullable<long>))
            {
                if (value is long)
                {
                    return value;
                }
                return long.Parse(value.ToString());
            }
            else if (type == typeof(decimal) || type == typeof(Nullable<decimal>))
            {
                if (value is decimal)
                {
                    return value;
                }
                return decimal.Parse(value.ToString());
            }
            else if (type == typeof(double) || type == typeof(Nullable<double>))
            {
                if (value is double)
                {
                    return value;
                }
                return double.Parse(value.ToString());
            }
            else if (type == typeof(bool) || type == typeof(Nullable<bool>))
            {
                if (value is bool)
                {
                    return value;
                }
                return bool.Parse(value.ToString());
            }
            else if (type == typeof(float) || type == typeof(Nullable<float>))
            {
                if (value is float)
                {
                    return value;
                }
                return float.Parse(value.ToString());
            }
            else if (type == typeof(DateTime) || type == typeof(Nullable<DateTime>))
            {
                if (value is DateTime)
                {
                    return value;
                }
                return DateTime.Parse(value.ToString());
            }
            else if (type == typeof(string))
            {
                if (value is string)
                {
                    return value;
                }
                return value.ToString();
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Validate entity data for virual method
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool ValidateData(IEntity entity)
        {
            return true;
        }

        /// <summary>
        /// Insert entity data for virual method
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool InsertEntity(IEntity entity)
        {
            return false;
        }

        /// <summary>
        /// Update entity data for virual method
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool UpdateEntity(IEntity entity)
        {
            return false;
        }

        /// <summary>
        /// Operation entity data 
        /// </summary>
        /// <returns></returns>
        public bool OperationEntity()
        {
            try
            {
                IEntity entity = this.GetEntity();
                if (!this.ValidateData(entity))
                {
                    return false;
                }
                return this.Entity == null ? this.InsertEntity(entity) : this.UpdateEntity(entity);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
