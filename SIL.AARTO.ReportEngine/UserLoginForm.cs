﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms; 
using SIL.AARTO.WinformUC;

namespace SIL.AARTO.ReportEngine
{
    public partial class UserLoginForm : UserLogin
    {
        public UserLoginForm()
        {
            InitializeComponent();
        }
        public override void OpenMainForm()
        {
            frmMain mainForm = new frmMain();
            mainForm.Show();
        }
    }
}
