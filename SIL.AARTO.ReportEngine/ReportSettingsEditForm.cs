﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIL.AARTO.DAL.Data;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.DAL.Services;
using System.Transactions;
using SIL.AARTO.BLL.Utility;

namespace SIL.AARTO.ReportEngine
{
    /// <summary>
    /// Add by Brian 2013-10-18
    /// Report Settings Edit Form
    /// </summary>
    public partial class ReportSettingsEditForm : EditForm
    {
        //Current entity of Data Service Instantiate
        private ReportSettingsService _ReportSettingsService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public ReportSettingsEditForm()
        {
            this.EntityType = typeof(ReportSettings);
            this._ReportSettingsService = new ReportSettingsService();
            InitializeComponent();
            this.LoadReportDownList();
            this.LoadPrinterDownList();
            this.LoadLPTPortDownList();
        }

        /// <summary>
        /// Load report config data list for DropDownList Control
        /// </summary>
        private void LoadReportDownList()
        {
            this.cmbReportConfig.DataSource = ReportPrintSettingsForm.ReportConfigList;
            this.cmbReportConfig.DisplayMember = "ReportName";
            this.cmbReportConfig.ValueMember = "RcIntNo";
        }

        /// <summary>
        /// Load printer settings data list for DropDownList Control
        /// </summary>
        private void LoadPrinterDownList()
        {
            PrinterSettingsService service = new PrinterSettingsService();
            List<PrinterSettings> printerList = service.GetAll().OrderBy(o => o.PsIntNo).ToList();
            printerList.Insert(0, new PrinterSettings() { PsIntNo = 0, PsName = string.Empty });
            this.cmbPsIntNo.DataSource = printerList;
            this.cmbPsIntNo.DisplayMember = "PsName";
            this.cmbPsIntNo.ValueMember = "PsIntNo";
        }

        /// <summary>
        /// Load LPT Port data list for DropDownList Control
        /// </summary>
        private void LoadLPTPortDownList()
        {
            List<string> lptPortList = new List<string>()
            {
                "",
                "LPT1",
                "LPT2",
                "LPT3",
                "LPT4",
                "LPT5",
                "LPT6",
                "LPT7",
                "LPT8",
                "LPT9"
            };
            this.cmbLPTPort.DataSource = lptPortList;
        }

        /// <summary>
        /// Override to expand get entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override IEntity ExpandGetEntity(IEntity entity)
        {
            ReportSettings editEntity = entity as ReportSettings;
            ReportSettings entityData = this.Entity as ReportSettings;
            if (entityData != null)
            {
                editEntity.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
            }
            else
            {
                editEntity.LastUser = GlobalVariates<User>.CurrentUser.UserLoginName;
                editEntity.CreateDate = DateTime.Now;
            }
            editEntity.AutIntNo = ReportPrintSettingsForm.AutIntNo;
            return entity;
        }

        /// <summary>
        /// Override to validate entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool ValidateData(IEntity entity)
        {
            ReportSettings editEntity = (ReportSettings)entity;
            if (editEntity.RcIntNo <= 0)
            {
                MessageBox.Show("Sorry, please select a report!!");
                return false;
            }
            if (editEntity.PsIntNo <= 0)
            {
                MessageBox.Show("Sorry, please select a printer!");
                return false;
            }
            if (string.IsNullOrEmpty(editEntity.LptPort))
            {
                MessageBox.Show("Sorry, please select a LPT port!");
                return false;
            }
            if (this.Entity == null || (this.Entity as ReportSettings).RcIntNo != editEntity.RcIntNo || (this.Entity as ReportSettings).PsIntNo != editEntity.PsIntNo)
            {
                ReportSettingsQuery query = new ReportSettingsQuery();
                query.Append(ReportSettingsColumn.RcIntNo, editEntity.RcIntNo.ToString());
                query.Append(ReportSettingsColumn.PsIntNo, editEntity.PsIntNo.ToString());
                ReportSettings ps = this._ReportSettingsService.Find(query).FirstOrDefault();
                if (ps != null)
                {
                    MessageBox.Show("Sorry, the report selected printer already exists!");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Override to insert entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool InsertEntity(IEntity entity)
        {
            int iCount = 0;
            using (var scope = SIL.ServiceBase.ServiceUtility.CreateTransactionScope())
            {
                TList<ReportSettings> editEntityList = new TList<ReportSettings>();
                ReportSettings editEntity = (ReportSettings)entity;
                editEntity.EntityState = EntityState.Added;
                editEntityList.Add(editEntity);
                if (editEntity.IsDefault)
                {
                    ReportSettingsQuery query = new ReportSettingsQuery();
                    query.Append(ReportSettingsColumn.RcIntNo, editEntity.RcIntNo.ToString());
                    query.Append(ReportSettingsColumn.IsDefault, "true");
                    List<ReportSettings> rsList = this._ReportSettingsService.Find(query).ToList();
                    rsList.ForEach(r =>
                    {
                        r.IsDefault = false;
                        r.EntityState = EntityState.Changed;
                        editEntityList.Add(r);
                    });
                }
                iCount = this._ReportSettingsService.Save(editEntityList).Count;
                scope.Complete();
            }
            return iCount > 0 ? true : false;
        }

        /// <summary>
        /// Override to update entity data
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public override bool UpdateEntity(IEntity entity)
        {
            int iCount = 0;
            using (var scope = SIL.ServiceBase.ServiceUtility.CreateTransactionScope())
            {
                TList<ReportSettings> editEntityList = new TList<ReportSettings>();
                ReportSettings editEntity = (ReportSettings)entity;
                editEntity.EntityState = EntityState.Changed;
                editEntityList.Add(editEntity);
                if (editEntity.IsDefault)
                {
                    ReportSettingsQuery query = new ReportSettingsQuery();
                    query.Append(ReportSettingsColumn.RcIntNo, editEntity.RcIntNo.ToString());
                    query.Append(ReportSettingsColumn.IsDefault, "true");
                    query.AppendNotEquals(ReportSettingsColumn.RsIntNo, editEntity.RsIntNo.ToString());
                    List<ReportSettings> rsList = this._ReportSettingsService.Find(query).ToList();
                    rsList.ForEach(r =>
                    {
                        r.IsDefault = false;
                        r.EntityState = EntityState.Changed;
                        editEntityList.Add(r);
                    });
                }
                iCount = this._ReportSettingsService.Update(editEntityList).Count;
                scope.Complete();
            }
            return iCount > 0 ? true : false;
        }

        /// <summary>
        /// Click event of OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.OperationEntity())
            {
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
