﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksums;
using SIL.AARTO.BLL.EntLib;

namespace SIL.AARTO.DataTransfer
{
    public class ZipUtility
    {
        public static int Zip(string zipFileName, List<string> srcFiles, string password)
        {
            ZipOutputStream zipStream = null;
            FileStream streamWriter = null;
            string fileName;
            int count = 0;

            try
            {
                //Use Crc32
                Crc32 crc32 = new Crc32();

                //Create Zip File
                zipStream = new ZipOutputStream(File.Create(zipFileName));

                //Specify Level
                zipStream.SetLevel(Convert.ToInt32(9));

                //Specify Password
                if (password != null && password.Trim().Length > 0)
                {
                    zipStream.Password = password;
                }

                //Foreach File
                foreach (string file in srcFiles)
                {
                    //Read the file to stream                    
                    streamWriter = File.OpenRead(file);
                    byte[] buffer = new byte[streamWriter.Length];
                    streamWriter.Read(buffer, 0, buffer.Length);
                    streamWriter.Close();

                    //Specify ZipEntry
                    crc32.Reset();
                    crc32.Update(buffer);
                    fileName = file.Substring(file.LastIndexOf('\\') + 1);
                    ZipEntry zipEntry = new ZipEntry(fileName);
                    zipEntry.DateTime = DateTime.Now;
                    zipEntry.Size = buffer.Length;
                    zipEntry.Crc = crc32.Value;

                    //Put file info into zip stream
                    zipStream.PutNextEntry(zipEntry);

                    //Put file data into zip stream
                    zipStream.Write(buffer, 0, buffer.Length);

                    count++;
                }
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Error, null, Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                //Clear Resource
                if (streamWriter != null)
                {
                    streamWriter.Close();
                }
                if (zipStream != null)
                {
                    zipStream.Finish();
                    zipStream.Close();
                }
            }

            return count;
        }

        public static List<string> Unzip(string destFolder, string srcZipFile, string password)
        {
            List<string> fileList = new List<string>();
            ZipInputStream zipStream = null;
            ZipEntry zipEntry = null;
            FileStream streamWriter = null;
            int count = 0;
            int bufferSize = 2048;

            try
            {
                zipStream = new ZipInputStream(File.OpenRead(srcZipFile));
                zipStream.Password = password;

                while ((zipEntry = zipStream.GetNextEntry()) != null)
                {
                    string zipFileDirectory = Path.GetDirectoryName(zipEntry.Name);
                    string destFileDirectory = Path.Combine(destFolder, zipFileDirectory);
                    if (!Directory.Exists(destFileDirectory))
                    {
                        Directory.CreateDirectory(destFileDirectory);
                    }

                    string fileName = Path.GetFileName(zipEntry.Name);
                    if (fileName.Length > 0)
                    {
                        string destFilePath = Path.Combine(destFileDirectory, fileName);

                        streamWriter = File.Create(destFilePath);
                        int size = bufferSize;
                        byte[] data = new byte[bufferSize];
                        long extractCount = 0;
                        while (true)
                        {
                            size = zipStream.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                            extractCount += size;
                        }

                        streamWriter.Flush();
                        streamWriter.Close();
                        fileList.Add(fileName);
                        count++;

                    }
                }
            }
            catch (Exception ex)
            {
                EntLibLogger.WriteLog(LogCategory.Error, null, Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                if (zipStream != null)
                {
                    zipStream.Close();
                }

                if (streamWriter != null)
                {
                    streamWriter.Close();
                }
            }

            return fileList;
        }
    }
}
