﻿using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Configuration;
using SIL.AARTO.BLL.EntLib;
using SIL.AARTO.BLL.Utility;
using System.Data;
using SIL.AARTO.DAL.Entities;

namespace SIL.AARTO.IMX.XMLLoader
{
    static class Program
    {
        public static string APP_TITLE = string.Empty;
        public static string APP_NAME = string.Empty;//AppNameList.DMSConsole.ToString();

        static void Main(string[] args)
        {
            try
            {
                string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
                Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

                string MName = Process.GetCurrentProcess().MainModule.ModuleName;
                string PName = Path.GetFileNameWithoutExtension(MName);
                APP_NAME = PName;
                Process[] myProcess = Process.GetProcessesByName(PName);

                string errorMessage;
                if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOXMLLoader, out errorMessage))
                {
                    EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                    CommonFunction.ConsoleWriteLine(errorMessage);
                    Console.Read();
                }


                if (myProcess.Length > 1)
                {
                    return;
                }
                else
                {
                    // Write the started Log
                    APP_TITLE = string.Format("{0} - Version {1}\n Last Updated {2}\n Started at: {3}",
                        APP_NAME,
                        Assembly.GetExecutingAssembly().GetName().Version.ToString(2),
                        CheckVersionManager.GetLastUpdatedDatetime(AartoProjectList.AARTOXMLLoader), DateTime.Now);

                    //log database information
                    string[] connInfo = CommonFunction.GetServerAndDatabaseName(ConfigurationManager.ConnectionStrings["AARTOConnectionString"].ToString());
                    StringBuilder sbrDatabase = new StringBuilder();
                    sbrDatabase = sbrDatabase.AppendFormat(@"Database Configurations Server:{0}; Database: {1}.", connInfo[0], connInfo[1]);

                    CommonFunction.ConsoleWriteLine(string.Format("{0} \n\r {1}", APP_TITLE, sbrDatabase.ToString()));
                    Console.Title = APP_TITLE;

                    try
                    {
                        //IMXImporter imxExport = new IMXImporter();
                        ViolationLoader violationLoader = new ViolationLoader(Program.APP_NAME);
                        CommonFunction.InitIndaba();

                        CommonFunction.ConsoleWriteLine("Importing...");
                        violationLoader.LoadViolationsByIndaba();

                        CommonFunction.DisposeIndaba();
                    }
                    catch (Exception ex)
                    {
                        CommonFunction.ConsoleWriteLine(ex.Message, LogCategory.Error, ex);
                    }

                    // Write the ended Log for end of console app.
                    CommonFunction.ConsoleWriteLine(string.Format("{0} – Ended at {1}", APP_NAME, DateTime.Now));
                }
            }
            catch (Exception ex)
            {
                CommonFunction.ConsoleWriteLine(string.Format("{0} \n\r {1}", APP_TITLE, ex.Message), LogCategory.Critical, ex);
            }
        }
    }
}
