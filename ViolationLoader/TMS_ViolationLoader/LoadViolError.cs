using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportAppServer;
using System.IO;
using Stalberg.TMS.ViolationLoader.Components;
using System.Windows.Forms;
using System.Net.Mail;

namespace Stalberg.TMS.ViolationLoader
{
    public class LoadViolError
    {
        protected System.Data.SqlClient.SqlConnection cn;
        protected System.Data.SqlClient.SqlParameter paramTLVType;
        protected System.Data.SqlClient.SqlParameter paramAutIntNo;
        protected ReportDocument _reportDoc = new ReportDocument();
        protected ExportOptions _exportOpt;
        protected DiskFileDestinationOptions _diskFileOpt;
        protected System.Data.SqlClient.SqlDataAdapter sqlDATempLoadViol;
        protected Stalberg.TMS.Data.Datasets.dsTempLoadViolErrors dsTempLoadViolErrors;
        protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;

        public LoadViolError()
        {
        }

        public void LoadReport(int autIntNo, int tlvType, string connStr, ref StreamWriter writer, FTPFunctions.FtpParameters ftpParam, ContractorDetails contractor)
        {
            Initialise(connStr);

            string tempFileLoc;

            string reportPath = Application.StartupPath + "\\reports\\LoadViolErrors.rpt";
            _reportDoc.Load(reportPath);

            //set filter for batch to print
            paramAutIntNo.Value = autIntNo;
            paramTLVType.Value = tlvType;

            //get data and populate dataset
            //sqlDATempLoadViol.SelectCommand.Connection.ConnectionString = connStr;
            sqlDATempLoadViol.Fill(dsTempLoadViolErrors);

            //set up new report based on .rpt report class
            _reportDoc.SetDataSource(dsTempLoadViolErrors);

            string dateStr = DateTime.Now.ToString();

            dateStr = dateStr.Replace("/", "-");
            dateStr = dateStr.Replace(":", "-");

            //set temp location for pdf export
            tempFileLoc = Application.StartupPath + "\\logfiles\\LoadViolErrors_" + dateStr + autIntNo.ToString() + ".pdf";

            //set up export options
            _diskFileOpt = new DiskFileDestinationOptions();
            _diskFileOpt.DiskFileName = tempFileLoc;

            _exportOpt = _reportDoc.ExportOptions;
            _exportOpt.DestinationOptions = _diskFileOpt;
            _exportOpt.ExportDestinationType = ExportDestinationType.DiskFile;
            _exportOpt.ExportFormatType = ExportFormatType.PortableDocFormat;

            //export the pdf file
            _reportDoc.Export();

            MailAddress from = new MailAddress("violation_loader@tms.co.za");
            MailAddress to = new MailAddress(contractor.ConSupportEmail);
            MailMessage message = new MailMessage(from, to);
            // message.Subject = "Using the SmtpClient class.";
            message.Subject = ftpParam.ftpHostServer .ToUpper () + " Load Violation to TMS error report  - " + DateTime.Today.ToShortDateString();
            message.Body = "Loading of violations has failed";
            Attachment data = new Attachment(tempFileLoc);

            // Add the file attachment to this e-mail message.
            message.Attachments.Add(data);

            //Send the message.
            SmtpClient client = new SmtpClient(ftpParam.smtp);
            try
            {
                client.Send(message);
                data.Dispose();
                File.Delete(tempFileLoc);
            }
            catch (Exception e)
            {
                writer.WriteLine("LoadReport: Unable to send error report " + tempFileLoc + " error " + e.Message);
                writer.Flush();
            }

            dsTempLoadViolErrors.Dispose();

        }

        public void Initialise(string connStr)
        {
            this.sqlDATempLoadViol = new System.Data.SqlClient.SqlDataAdapter();
            this.cn = new System.Data.SqlClient.SqlConnection();
            this.dsTempLoadViolErrors = new Stalberg.TMS.Data.Datasets.dsTempLoadViolErrors();
            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
            ((System.ComponentModel.ISupportInitialize)(this.dsTempLoadViolErrors)).BeginInit();
            // 
            // sqlDATempLoadViol
            // 
            this.sqlDATempLoadViol.SelectCommand = this.sqlSelectCommand1;
            this.sqlDATempLoadViol.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																										new System.Data.Common.DataTableMapping("Table", "TempLoadViolErrorList", new System.Data.Common.DataColumnMapping[] {
																																																								 new System.Data.Common.DataColumnMapping("TErIntNo", "TErIntNo"),
																																																								 new System.Data.Common.DataColumnMapping("TErDescr", "TErDescr"),
																																																								 new System.Data.Common.DataColumnMapping("TErColumn", "TErColumn"),
																																																								 new System.Data.Common.DataColumnMapping("TErValue", "TErValue"),
																																																								 new System.Data.Common.DataColumnMapping("TErCorrectVal", "TErCorrectVal"),
																																																								 new System.Data.Common.DataColumnMapping("TErRefNo", "TErRefNo")})});
            // 
            // cn
            // 
            this.cn.ConnectionString = connStr;
            // 
            // dsTempLoadViolErrors
            // 
            this.dsTempLoadViolErrors.DataSetName = "dsTempLoadViolErrors";
            this.dsTempLoadViolErrors.Locale = new System.Globalization.CultureInfo("en-US");
            // 
            // sqlSelectCommand1
            // 
            this.sqlSelectCommand1.CommandText = "[TempLoadViolErrorList]";
            this.sqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure;
            this.sqlSelectCommand1.Connection = this.cn;
            this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, false, ((System.Byte)(0)), ((System.Byte)(0)), "", System.Data.DataRowVersion.Current, null));
            paramAutIntNo = this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@AutIntNo", System.Data.SqlDbType.Int, 4));
            paramTLVType = this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@TLVType", System.Data.SqlDbType.Int, 4));
            ((System.ComponentModel.ISupportInitialize)(this.dsTempLoadViolErrors)).EndInit();
        }
    }
}
