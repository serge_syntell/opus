using System;
using System.IO;
using System.Net.Mail;
using System.Windows.Forms;
using System.Xml;
using Stalberg.TMS.Data;
using Stalberg.TMS.ViolationLoader.Components;
using Stalberg.TMS_3P_Loader.Components;
using SIL.AARTO.DAL.Entities;
using SIL.AARTO.BLL.Utility;
using SIL.AARTO.BLL.EntLib;

namespace Stalberg.TMS.ViolationLoader
{
    internal class ViolationLoader
    {
        // Fields
        private static StreamWriter writer;
        private static string logPath;
        private static string fileName;
        public static readonly string APP_NAME = "TMS_ViolationLoader"; // 2013-07-25 add by Henry

        // Constants
        //private  string LAST_UPDATED = ProjectLastUpdated.AARTOViolation_Loader.ToShortDateString();

        /// <summary>
        /// ViolationLoader. Console application to load data into the TMS database
        /// 1. Read the sysparam.xml file to obtain the ftp folder settings and database settings
        /// 2. read thru ftp folder for input fils and load data from text files/ access mdb
        /// -   .ts1 = generic pre-adjuducation data and images (load into film/frame/frame-offence structure)
        /// -   .ts2 = generic post-adjudication data and images (load directly into notice/charge structure)
        /// -   .mdb = legacy extract access database from TrafficARS (load directly into notice/charge structure)
        /// 3. create log files and report with invalid notices for sending back to the contractor
        /// </summary>
        static void Main()
        {
            // Check Last updated Version            
            string errorMessage;

            try
            {
                // Setup the Log Writer
                string strDate = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
                Environment.SetEnvironmentVariable("FILENAME", strDate, EnvironmentVariableTarget.Process);

                // Create the log file
                logPath = Path.Combine(Application.StartupPath, "log");
                fileName = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".txt";

                if (!Directory.Exists(logPath))
                    Directory.CreateDirectory(logPath);

                writer = new StreamWriter(Path.Combine(logPath, fileName), false);

                // 2010/10/11 jerry add 
                if (!CheckVersionManager.CheckVersion(AartoProjectList.AARTOViolation_Loader, out errorMessage))
                {
                    Console.WriteLine(errorMessage);
                    EntLibLogger.WriteLog(LogCategory.General, null, errorMessage);
                    return;
                }

                // Run the application
                Run();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex);
                writer.WriteLine(ex.ToString());
                EntLibLogger.WriteErrorLog(ex.GetBaseException(), LogCategory.Exception, AartoProjectList.AARTOViolation_Loader.ToString());
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                    writer.Dispose();
                }
            }
        }

        private static void Run()
        {
            FTPFunctions ftp = new FTPFunctions();
            DBFunctions db = new DBFunctions();

            int nPLIntNo = 0;

            writer.WriteLine("TMS Violation loader started at: " + DateTime.Now);
            writer.WriteLine("Last updated: " + ProjectLastUpdated.AARTOViolation_Loader.ToShortDateString());
            writer.WriteLine();
            writer.Flush();

            //load xml file with ftp and database connect string parameters
            string sysParam = Application.StartupPath + "\\sysparam.xml";

            if (!System.IO.File.Exists(sysParam))
            {
                writer.WriteLine("Settings file (" + sysParam + ") not found - processing aborted");
                writer.WriteLine();
                writer.Flush();
                return;
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(sysParam);

            XmlNode root = xmlDoc.FirstChild;

            FTPFunctions.FtpParameters ftpParam = ftp.SetFtpParamters(root);

            writer.WriteLine("FTP Settings located");
            writer.WriteLine();
            writer.Flush();

            DBFunctions.DBSettings dbSettings = db.SetDBSettings(root);


            //Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=dawn;Initial Catalog=Traffic;Data Source=SERVER2
            string connectionString = "Data Source=" + dbSettings.dbServer + "; Initial Catalog=" + dbSettings.dbDatabase + ";";
            if (dbSettings.dbSSPI == 1)
                connectionString += "Integrated Security=SSPI;Persist Security Info=False;";
            else
                connectionString += "User ID=" + dbSettings.dbUser + ";Password=" + dbSettings.dbPwd;

            // David Lin 20100326 Remove images from database
            // Check and Connect to Default ImageFileServer
            bool isConnected = false;
            string strDefaultImageServerError = string.Empty;
            ImageFileServerDB ifsDB = new ImageFileServerDB(connectionString);
            ImageFileServerDetails CurrentImageFileServer = ifsDB.GetDefaultImageFileServer();
            if (CurrentImageFileServer != null)
            {
                string strTestImagePath = @"\\" + CurrentImageFileServer.ImageMachineName + Path.DirectorySeparatorChar
                    + CurrentImageFileServer.ImageShareName + Path.DirectorySeparatorChar + "test.gif";

                string strTestImagePath2 = @"\\" + CurrentImageFileServer.ImageMachineName + Path.DirectorySeparatorChar
                    + CurrentImageFileServer.ImageShareName + Path.DirectorySeparatorChar + "test.jpg";
                //if (RemoteManager.RemoteConnect(CurrentImageFileServer.ImageMachineName, 
                //        CurrentImageFileServer.ImageShareName,
                //        CurrentImageFileServer.RemoteUserName,
                //        CurrentImageFileServer.RemotePassword))
                //{
                isConnected = File.Exists(strTestImagePath) || File.Exists(strTestImagePath2);
                strDefaultImageServerError = "Error: Test.jpg/Test.gif access from default image file server error.";
                //}
                //else
                //{
                //    strDefaultImageServerError = "Error: Default image file server connect failure!";
                //}
            }
            else
            {
                strDefaultImageServerError = "Error: Default image file server not found!";
            }

            if (isConnected == false)
            {
                writer.WriteLine(strDefaultImageServerError);
                writer.WriteLine();
                writer.Flush();
                return;
            }

            //DBFunctions.AccessDBSettings accessDBSettings = db.SetAccessDBSettings(root);

            //dls 081029 - instantiate new error log
            TMSErrorLogDB tmsErrorLog = new TMSErrorLogDB(connectionString);
            TMSErrorLogDetails tmsErrorLogDetails = new TMSErrorLogDetails();

            tmsErrorLogDetails.TELAppName = "TMS_ViolationLoader";
            tmsErrorLogDetails.TELModuleName = "ViolationLoader";
            tmsErrorLogDetails.TELProcName = "Main";
            tmsErrorLogDetails.TELIntNo = 0;
            tmsErrorLogDetails.TELDateFixed = DateTime.MinValue;
            tmsErrorLogDetails.TELDateTime = DateTime.MinValue;
            tmsErrorLogDetails.TELFixedByUser = null;

            ProcessLogDB pl = new ProcessLogDB(connectionString);
            try
            {
                //nPLIntNo = pl.AddProcessLog(0, "TMS_ViolationLoader", DateTime.Now, DateTime.Now, "TMS_ViolationLoader");
                nPLIntNo = pl.AddProcessLog(0, tmsErrorLogDetails.TELAppName, DateTime.Now, DateTime.Now, tmsErrorLogDetails.TELAppName);
            }
            catch (Exception e)
            {
                //writer.WriteLine("Could not write to ProcessLog Table " + e.Message);
                tmsErrorLogDetails.TELErrMessage = "Could not write to ProcessLog Table " + e.Message;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
            }


            bool continueProcess = true;

            //check whether another instance is already running
            Singleton single = new Singleton("TMS_ViolationLoader");
            single.OtherProcessesToCheckFor(new string[] { "TMS_CD_Loader", 
                    "TMS_3P_Loader", 
                    "Stalberg.Payfine.Extract",
                    "Stalberg.ThaboExporter",
                    "PFB_Loader",
                    "TMS_TPExInt"});

            if (single.Check())
            {
                //writer.WriteLine(single.Message + " at " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = single.Message;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                continueProcess = false;
            }
            else if (ftpParam.mode.ToLower().Equals("mdb"))
            {
                //check whether an instance of CD loader is running
                if (single.Check())
                {
                    //writer.WriteLine("An instance of the CD Loader/3P Loader application is running - processing aborted " + DateTime.Now.ToString());
                    tmsErrorLogDetails.TELErrMessage = "An instance of the CD Loader/3P Loader application is running - processing aborted";
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                    continueProcess = false;
                }
            }

            if (continueProcess)
            {
                writer.WriteLine("Start processing violation data: " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();

                ProcessViolations process = new ProcessViolations();

                //loadFailed = process.LoadViolations(ftpParam, connStr, ref writer, accessDBSettings, logPath);
                bool loadFailed = process.LoadViolations(ftpParam, connectionString, ref writer, logPath, CurrentImageFileServer);

                if (loadFailed)
                {
                    //writer.WriteLine("Violation data not loaded successfully at: " + DateTime.Now.ToString());
                    tmsErrorLogDetails.TELErrMessage = "Violation data not loaded successfully";
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                }
                else
                {
                    writer.WriteLine("Loaded violation data successfully at: " + DateTime.Now.ToString());
                    writer.WriteLine();
                    writer.Flush();
                }
            }

            // Close log writer with a final message
            writer.WriteLine("Violation Loader completed at: " + DateTime.Now);
            writer.Close();

            //email log file

            //create new log file to save final messages
            StreamWriter writer1 = new StreamWriter(logPath + "\\" + Path.GetFileNameWithoutExtension(fileName) + "_final.txt", false);

            //dls 060602 - add emailing of log file
            string message = "On the " + DateTime.Now.ToString() + " the following log file was created by the " + ftpParam.ftpProcess + " process:\n\n"
                  + "File name: " + logPath + "\\" + fileName + "\n\n"
                  + "The file is attached to this email.\n\n"
                  + "Please check the file status.\n\n"
                  + "Thank you very much.\n\n"
                  + "Regards\n"
                  + "TMS System Administrator";

            try
            {
                MailAddress from = new MailAddress(ftpParam.email);
                MailAddress to = new MailAddress(ftpParam.email);

                MailMessage mail = new MailMessage(from, to);
                mail.Subject = ftpParam.ftpHostServer.ToUpper() + " " + ftpParam.ftpProcess + " log file";
                mail.Body = message;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                Attachment myAttachment = new Attachment(logPath + "\\" + fileName);
                mail.Attachments.Add(myAttachment);
                try
                {
                    SmtpClient mailClient = new SmtpClient();
                    mailClient.Host = ftpParam.smtp;
                    mailClient.UseDefaultCredentials = true;
                    mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                    //Send delivers the message to the mail server
                    mailClient.Send(mail);
                    //writer = new StreamWriter(logPath + "\\" + Path.GetFileNameWithoutExtension(fileName) + "_final.txt", false);
                    writer1.WriteLine("Main: email logfile sent to " + ftpParam.email + " at: " + DateTime.Now.ToString());
                    writer1.Flush();
                }
                catch (Exception smtpEx)
                {
                    //writer1.WriteLine("Main: Failed to send email of logfile " + smtpEx.Message);
                    tmsErrorLogDetails.TELErrMessage = "Failed to send email of logfile" + smtpEx.Message;
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                }
            }
            catch (Exception emailEx)
            {
                //writer1.WriteLine("Main: Failed to send email of logfile " + emailEx.Message);
                tmsErrorLogDetails.TELErrMessage = "Failed to send email of logfile " + emailEx.Message;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
            }

            try
            {
                pl.UpdateProcessLog(nPLIntNo, "TMS_ViolationLoader", DateTime.Now, "TMS_ViolationLoader");
            }
            catch (Exception e)
            {
                //writer.WriteLine("Could not update ProcessLog Table " + e.Message);
                tmsErrorLogDetails.TELErrMessage = "Could not update ProcessLog Table " + e.Message;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
            }

            writer1.Close();
            return;
        }
    }

    class ProcessViolations
    {
        public ProcessViolations()
        {
        }

        //public bool LoadViolations(FTPFunctions.FtpParameters ftpParam, string connStr, ref StreamWriter writer, DBFunctions.AccessDBSettings accessDBSettings, string logPath)
        public bool LoadViolations(FTPFunctions.FtpParameters ftpParam, string connStr, ref StreamWriter writer, string logPath, ImageFileServerDetails imageServer)
        {
            //dls 081029 - instantiate new error log
            TMSErrorLogDB tmsErrorLog = new TMSErrorLogDB(connStr);
            TMSErrorLogDetails tmsErrorLogDetails = new TMSErrorLogDetails();

            tmsErrorLogDetails.TELAppName = "TMS_ViolationLoader";
            tmsErrorLogDetails.TELModuleName = "ProcessViolations";
            tmsErrorLogDetails.TELProcName = "LoadViolations";
            tmsErrorLogDetails.TELIntNo = 0;
            tmsErrorLogDetails.TELDateFixed = DateTime.MinValue;
            tmsErrorLogDetails.TELDateTime = DateTime.MinValue;
            tmsErrorLogDetails.TELFixedByUser = null;

            bool failed = false;

            string localPath = ftpParam.ftpExportPath;
            string finalPath = ftpParam.ftpReceivedPath;

            if (ftpParam.mode.ToLower().Equals("centralised"))
            {
                FTPFunctions ftp = new FTPFunctions();

                //failed = ftp.ReadFtpFiles(ftpParam, ref writer, connStr, accessDBSettings);
                //failed = ftp.FtpFile(ftpParam, ref writer, connStr, accessDBSettings);
                failed = ftp.FtpFile(ftpParam, ref writer, connStr);

                if (failed) return failed;
            }

            //LoadViolations load = new LoadViolations(ftpParam, accessDBSettings, connStr, logPath);
            LoadViolations load = new LoadViolations(ftpParam, connStr, logPath, imageServer);

            //need to copy all content of local export folder to \Received
            GeneralFunctions gen = new GeneralFunctions();

            bool getFolder = gen.CheckFolder(finalPath);

            if (getFolder)
            {
                //writer.WriteLine("LoadViolations: unable to move files to Receive folder");
                tmsErrorLogDetails.TELErrMessage = "LoadViolations: unable to move files to Receive folder";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                return failed;
            }

            //Process the list of files found in the local directory.

            //string email = ftpParam.email;
            //string smtp = ftpParam.smtp;

            //dls 060804 - check for ViolationCutOff in SysParam
            bool rootFolder = true;
            failed = load.ProcessLocalFolders(localPath, ref writer, rootFolder, "", finalPath);

            Application.DoEvents();

            //gen.MoveFiles(localPath, finalPath);

            //try
            //{
            //    Directory.Delete(localPath, true);
            //}
            //catch { }

            return failed;
        }
    }
}
