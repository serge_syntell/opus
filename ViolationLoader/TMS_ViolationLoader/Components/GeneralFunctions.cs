using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using EnterpriseDT.Net.Ftp;
using EnterpriseDT.Util.Debug;

namespace Stalberg.TMS.ViolationLoader.Components
{
    public class GeneralFunctions
    {
        public GeneralFunctions()
        {
        }
        
         // function to delete files on local machines
        public bool DeleteFile(string fullPathName)
        {
            bool failed = false;
            try
            {
                File.Delete(fullPathName);
                failed = false;
            }
            catch
            {
                failed = true;
                return failed;
            }
            return failed;
        }

        // function to copy a file
        public bool CopyFile(string source, string destinationPath, string destinationFile)
        {
            bool failed = false;
            // must first check whether the destination directory is valid
            bool checkDir = CheckFolder(destinationPath);
            if (checkDir == true)
            {
                failed = true;
                return failed;
            }
            else
            {
                try
                {
                    // make a directory
                    DirectoryInfo di = new DirectoryInfo(destinationPath);
                    if (di.Exists == false)
                        di.Create();
                }
                catch (Exception e)
                {
                    string msg = e.Message;
                    failed = false;
                    return failed;
                }
            }
            // now can copy
            try
            {
                string destination = destinationPath + @"\" + destinationFile;
                if (File.Exists(destination))
                {
                    // do nothing
                    failed = false;
                }
                else
                {
                    File.Copy(source, destination);
                    failed = false;
                }
            }
            catch
            {
                failed = true;
                return failed;
            }
            return failed;
        }

        public bool CheckFolder(string destinationPath)
        {
            bool failed = false;
            try
            {
                // must first check whether the destination directory is valid
                if (Directory.Exists(destinationPath))
                {
                    // valid path to directory
                }
                else
                {
                    // make a directory
                    DirectoryInfo di = new DirectoryInfo(destinationPath);
                    if (di.Exists == false)
                        di.Create();
                }
                return failed;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                failed = true;
                return failed;
            }
        }

        public void MoveFiles(string sourceFolder, string targetFolder)
        {
         //Process the list of files found in the directory.
            string file = "";
            string[] fileEntries = Directory.GetFiles(sourceFolder);
            foreach (string fileName in fileEntries)
            {
                file = Path.GetFileName(fileName);
                if (File.Exists(targetFolder + "\\" + file))
                    File.Delete(targetFolder + "\\" + file);

                File.Move(fileName, targetFolder + "\\" + file);
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(sourceFolder);
            foreach (string subdirectory in subdirectoryEntries)
                if (subdirectory.Contains("Received"))
                {
                    //string msg = "Can't process the received folder(s)";
                }
                else
                {
                    string subFolder = targetFolder + "\\" + Path.GetFileName(subdirectory);
                    bool failed = CheckFolder(subFolder);
                    if (!failed)
                    {
                        MoveFiles(subdirectory, subFolder);
                        try
                        {
                            Directory.Delete(subdirectory, true);
                        }
                        catch { }
                    }
                }
        }
    }
}
