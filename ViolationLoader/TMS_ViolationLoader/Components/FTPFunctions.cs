using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using EnterpriseDT.Net.Ftp;

namespace Stalberg.TMS.ViolationLoader.Components
{
    public class FTPFunctions
    {
        public FTPFunctions()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public class FtpParameters
        {
            //added elements to struct
            public String ftpProcess;
            public String ftpExportServer;
            public String ftpExportPath;
            public String ftpHostServer;
            public String ftpHostIP;
            public String ftpHostUser;
            public String ftpHostPass;
            public String ftpHostPath;
            public String ftpReceivedPath;
            public String email;
            public String smtp;
            public string mode;         //centralised OR mdb
            public string imageFolder;  //new setting to store images in file system
        }

        public FtpParameters SetFtpParamters(XmlNode root)
        {
            FtpParameters ftp = new FtpParameters();
            //Assign the contents of the child nodes to application variables
            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    switch (root.ChildNodes[i].Name)
                    {
                        case "ftpProcess":
                            ftp.ftpProcess = root.ChildNodes[i].InnerText;
                            break;
                        case "ftpExportServer":
                            ftp.ftpExportServer = root.ChildNodes[i].InnerText;
                            break;
                        case "ftpExportPath":
                            ftp.ftpExportPath = root.ChildNodes[i].InnerText;
                            break;
                        case "ftpHostServer":
                            ftp.ftpHostServer = root.ChildNodes[i].InnerText;
                            break;
                        case "ftpHostIP":
                            ftp.ftpHostIP = root.ChildNodes[i].InnerText;
                            break;
                        case "ftpHostUser":
                            ftp.ftpHostUser = root.ChildNodes[i].InnerText;
                            break;
                        case "ftpHostPass":
                            ftp.ftpHostPass = root.ChildNodes[i].InnerText;
                            break;
                        case "ftpHostPath":
                            ftp.ftpHostPath = root.ChildNodes[i].InnerText;
                            break;
                        case "ftpReceivedPath":
                            ftp.ftpReceivedPath = root.ChildNodes[i].InnerText;
                            break;
                        case "email":
                            ftp.email = root.ChildNodes[i].InnerText;
                            break;
                        case "smtp":
                            ftp.smtp = root.ChildNodes[i].InnerText;
                            break;
                        case "mode":
                            ftp.mode = root.ChildNodes[i].InnerText;
                            break;
                        case "imageFolder":
                            ftp.imageFolder = root.ChildNodes[i].InnerText;
                            break;
                    }
                }
            }

            return ftp;
        }

        //function ftp file to TMS
        //public bool FtpFile(FtpParameters localFtp, ref StreamWriter writer, string connStr, DBFunctions.AccessDBSettings accessDBSettings)
        public bool FtpFile(FtpParameters localFtp, ref StreamWriter writer, string connStr)
        {
            bool failed = false;

            string host = localFtp.ftpHostIP;
            string user = localFtp.ftpHostUser;
            string password = localFtp.ftpHostPass;
            string hostPath = localFtp.ftpHostPath;
            string exportPath = localFtp.ftpExportPath; //not required as the folder is dynamically acquired

            GeneralFunctions gen = new GeneralFunctions();

            // test exportPath
            bool check = gen.CheckFolder(exportPath);
            if (check == true)
            {
                writer.WriteLine("FTPFile path invalid for exportpath: " + exportPath + " " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();
                failed = true;
                return failed;
            }

            try
            {
                // do ftpclient setup here
                FTPClient ftp = null;
                //ftp = new FTPClient(host);
                ftp = new FTPClient();
                ftp.RemoteHost = host;
                // set up client
                //ftp = new FTPClient(host);
                ftp.Connect();

                // login
                ftp.Login(user, password);

                // set up passive ASCII transfers
                ftp.ConnectMode = FTPConnectMode.PASV;
                ftp.TransferType = FTPTransferType.BINARY;

                writer.WriteLine("FTPFile host path: " + hostPath + " " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();

                // change directory to appropriate folder on host - root is ftpRoot
                ftp.ChDir(hostPath);

                writer.WriteLine("FTPFile chdir successful: " + hostPath + " " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();

                //failed = ProcessFtpDirectory(localFtp, ftp, ref writer, connStr, accessDBSettings, exportPath, "", true);
                failed = ProcessFtpDirectory(localFtp, ftp, ref writer, connStr, exportPath, "", true);

                // Shut down ftp client                
                ftp.Quit();
                writer.WriteLine("FtpFile quit successful: " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();
                return failed;
            }
            catch (Exception e)
            {
                writer.WriteLine("FtpFile quit failure: " + e.Message + " " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();
                return failed;
            }
        }


        //public bool ProcessFtpDirectory(FtpParameters localFtp, FTPClient ftp, ref StreamWriter writer, string connStr, DBFunctions.AccessDBSettings accessDBSettings, string targetDirectory, string subFolder, bool rootFolder)
        public bool ProcessFtpDirectory(FtpParameters localFtp, FTPClient ftp, ref StreamWriter writer, string connStr, string targetDirectory, string subFolder, bool rootFolder)
        {

            //Process the list of files found in the directory.
            string file = "";
            bool failed = false;

            try
            {
                //string[] fileEntries = ftp.Dir();
                //foreach (string fileName in fileEntries)
                FTPFile[] ftp_files = ftp.DirDetails("");
                foreach (FTPFile ftp_file in ftp_files)
                {
                    bool isDir = ftp_file.Dir;

                    if (isDir)
                    {
                        string subdirectory = ftp_file.Name;
                        ftp.ChDir(subdirectory);

                        string remotepath = targetDirectory + @"\" + subdirectory;

                        GeneralFunctions gen = new GeneralFunctions();

                        // test exportPath
                        bool check = gen.CheckFolder(remotepath);
                        if (check == true)
                        {
                            writer.WriteLine("FTPFile path invalid for exportpath: " + remotepath + " " + DateTime.Now.ToString());
                            writer.WriteLine();
                            writer.Flush();
                            failed = true;
                            return failed;
                        }

                        //failed = ProcessFtpDirectory(localFtp, ftp, ref writer, connStr, accessDBSettings, remotepath, subdirectory, false);
                        failed = ProcessFtpDirectory(localFtp, ftp, ref writer, connStr, remotepath, subdirectory, false);

                        if (failed) return failed;

                        ftp.ChDir("..");

                        try
                        {
                            if (!rootFolder)
                            {
                                //Thread.Sleep(1000);
                                ftp.RmDir(subdirectory);
                            }
                        }
                        catch (Exception err)
                        {
                            writer.WriteLine("FTP process delete folder failed for folder " + " " + err.Message + " " + DateTime.Now.ToString());
                            writer.WriteLine();
                            writer.Flush();
                        }
                    }
                    else
                    {
                        // need to ignore any error files that are in the root folder
                        if (!rootFolder)
                        {
                            //file = Path.GetFileName(fileName);
                            file = ftp_file.Name;

                            if (!file.ToLower().EndsWith("err") && !file.ToLower().EndsWith("ok"))
                                //failed = ProcessFtpFile(localFtp, ftp, ref writer, file, connStr, targetDirectory, accessDBSettings, subFolder);
                                failed = ProcessFtpFile(localFtp, ftp, ref writer, file, connStr, targetDirectory, subFolder);

                            if (failed) return failed;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                writer.WriteLine("FTP process folder failed: " + e.Message + " " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();
                failed = true;
                return failed;
            }



            return failed;
        }

        //public bool ProcessFtpFile(FtpParameters localFtp, FTPClient ftp, ref StreamWriter writer, string fileName, string connStr, string targetDirectory, DBFunctions.AccessDBSettings accessDBSettings, string subFolder)

        public bool ProcessFtpFile(FtpParameters localFtp, FTPClient ftp, ref StreamWriter writer, string fileName, string connStr, string targetDirectory, string subFolder)
        {
            //// multiple folders are a potential problem
            //// must create folder structure on ftp site equivalent to loacal folder structure
            //// ftproot\TMS\3rdParty\Contractor\film\etc
            bool failed = false;

            string remoteFile = "";

            string delimStr = " ";
            char[] delimiter = delimStr.ToCharArray();
            string[] split = null;
            string fileName1 = fileName.Replace("     ", "");
            split = fileName1.Split(delimiter, 10);
            foreach (string item in split)
            {
                remoteFile = item;
                break;
            }

            string path = localFtp.ftpHostPath + @"/" + subFolder + @"/" + remoteFile;
            string remoteFullPath = targetDirectory + @"\" + remoteFile;

            ftp.Get(remoteFullPath, remoteFile);

            // can't move in Ftp - must use put or similar. However we have a duplicate on the TOMS server if needed to resend
            // delete file from server
            try
            {
                ftp.Delete(remoteFile);
            }
            catch (Exception e)
            {
                writer.WriteLine("FTPFile delete unsuccessful path: " + path + ", Error: " + e.Message + " " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();
                failed = true;
            }

            return failed;
        }


        protected string HashPassword(string plainMessage)
        {
            byte[] data = Encoding.UTF8.GetBytes(plainMessage);
            using (HashAlgorithm sha = new SHA256Managed())
            {
                byte[] encryptedBytes = sha.TransformFinalBlock(data, 0, data.Length);
                return Convert.ToBase64String(sha.Hash);
            }
        }

        public bool FtpFileSend(ContractorDetails conDetails, ref StreamWriter writer, string returnFileName, string destinationPath)
        {
            GeneralFunctions general = new GeneralFunctions();

            bool failed = false;
            string host = conDetails.ConFTPSite;
            string user = conDetails.ConFTPLogin;
            string password = HashPassword(conDetails.ConFTPPassword);
            string hostPath = conDetails.ConFTPFolder;

            //set up encryption class
            //Rc4Encrypt.clsRc4Encrypt rc4 = new clsRc4Encrypt();
            //rc4.Password = "sacompconsult";
            //rc4.PlainText = password;

            FTPClient ftp = null;
            try
            {
                // set up client
                //ftp = new FTPClient(host);
                ftp = new FTPClient();
                ftp.RemoteHost = host;
                ftp.Connect();
                // login
                ftp.Login(user, password);
                //ftp.Login(user, rc4.EnDeCrypt());

                // set up passive ASCII transfers
                ftp.ConnectMode = FTPConnectMode.PASV;
                ftp.TransferType = FTPTransferType.BINARY;
                // change directory to appropriate folder on host - root is ftpRoot
                ftp.ChDir(hostPath);
                // build remote file name
                string remoteFile = "";
                string remoteFullPath = "";
                string path = "";
                //string destinationPath = "";

                remoteFile = Path.GetFileName(returnFileName);
                path = Path.GetDirectoryName(returnFileName);
                remoteFullPath = hostPath + @"/" + remoteFile;
                // send to remote location
                ftp.Put(returnFileName, remoteFullPath);
                //copy file to sent folder - check for folder - make folder if necessary

                string fileName = remoteFile.Substring(0, remoteFile.IndexOf(".")) + " " + DateTime.Now.ToString() + ".ts1.err";
                fileName = fileName.Replace("/", "-");
                fileName = fileName.Replace(":", "-");
                destinationPath += "\\" + fileName;

                bool copyFailed = general.CopyFile(returnFileName, destinationPath, remoteFile);
                if (copyFailed == true)
                {
                    failed = true;
                    writer.WriteLine("FtpFile copyFailed: " + returnFileName + " " + DateTime.Now.ToString());
                    //writer.WriteLine();
                    writer.Flush();
                    return failed;
                }
                // delete file from folder
                bool deleteFailed = general.DeleteFile(returnFileName);
                if (deleteFailed == true)
                {
                    failed = true;
                    writer.WriteLine("FtpFile deleteFailed: " + returnFileName + " " + DateTime.Now.ToString());
                    //writer.WriteLine();
                    writer.Flush();
                    return failed;
                }
                // Shut down client                
                ftp.Quit();
                writer.WriteLine("FtpFile quit successful: " + DateTime.Now.ToString());
                //writer.WriteLine();
                writer.Flush();
                return failed;
            }
            catch (Exception e)
            {
                failed = true;
                writer.WriteLine("FtpFile failed: " + e.Message + " " + DateTime.Now.ToString());
                //writer.WriteLine();
                writer.Flush();
                return failed;
            }
        }
    }
}

