using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Windows.Forms;
using Stalberg.TMS.Data;
using Stalberg.TMS_3P_Loader;
using SIL.QueueLibrary;
using SIL.ServiceQueueLibrary.DAL.Entities;
using System.Transactions;
using System.Data;

namespace Stalberg.TMS.ViolationLoader.Components
{
    public class LoadViolations
    {
        protected string _interface = "";
        protected int _filmStatus = 0;
        protected ArrayList contractorList = new ArrayList();
        protected FTPFunctions.FtpParameters _ftpParam;
        //protected DBFunctions.AccessDBSettings _accessDBSettings;
        protected string _connStr;
        protected string _logPath;
        protected ContractorDetails _currentContractor = new ContractorDetails();
        protected GeneralFunctions gen = new GeneralFunctions();
        protected int _filmIntNo = 0;
        protected bool _natisLast = true;
        protected int _noOfImagesToPrint = 1;
        protected int _whichImageTraffiPax = 1;
        protected int _whichImageDigital = 1;
        protected string _excluListActive = "N";
        protected int _excluSpeedAllowance = 30;
        protected bool _saveToFileSystem = false;
        protected string _fullAdjudicated = "N";
        protected string _procName = string.Empty;
        protected int noOfDays = 0;
        protected ImageFileServerDetails _imageServer = null;

        //protected const int STATUS_LOADED = 5;		//without ticket numbers
        //protected const int STATUS_TICKETS = 10;		//with ticket numbers
        //protected const int STATUS_NOFINES = 6;		//no fine amount for offence
        //protected const int STATUS_GETNATIS = 100;		//Data extracted and sent to NaTIS

        protected const int STATUS_NATIS_FIRST = 10;     //new status for setting frames that must go straight to adjudication
        protected const int STATUS_NATIS_LAST = 700;     //new status for setting frames that must go straight to adjudication
        protected const int STATUS_CANCELLED = 999;		//cancelled
        protected const int STATUS_EXPIRED = 921;		//offence > delay time period days old
        protected const int STATUS_AMOUNT_ADDED = 7;
        protected const int STATUS_NO_AOG = 500;
        protected const string CTYPE = "CAM";			//loaded from camera
        protected const string NT_CODE = "6";				//computer printed section 341 camera A6
        protected const string REGNO_FILE_EXT = "_RegNo.JPG";
        protected const string WET_FILM_FIRST_IMAGE = "_17.j";
        protected const string DIG_FILM_FIRST_IMAGE = "001.j";
        protected const string DIG_FILM_SPD_2ND_IMAGE = "002.j";
        protected const int STATUS_FILM_CREATED = 0;
        protected const int STATUS_FILM_TS1_ERROR = 100;
        protected const int STATUS_FILM_DATA_ERROR = 500;
        protected const int STATUS_FILM_SUCCESSFUL = 1000;
        protected const int STATUS_OFFICIAL_VEHICLE_EXCLUSION = 991;

        protected TMSErrorLogDB tmsErrorLog = null;
        protected TMSErrorLogDetails tmsErrorLogDetails = new TMSErrorLogDetails();

        protected string lowSpeedRejReason = string.Empty;
        protected string invalidSpeed = string.Empty;
        protected int lowSpeedRejIntNo = 0;
        protected string asdInvalidSetup = string.Empty;
        protected int asdInvalidRejIntNo = 0;
        protected string expiredRejReason = string.Empty;
        protected int expiredRejIntNo = 0;

        public struct HeaderRow
        {
            public string RecordType;           //'always D for data rows, I for image rows
            public int Version;
            public string ContractorCode;
            public string AuthCode;
            public string AuthName;
            public string AuthNo;
            public string CDLabel;
            public string FilmNo;
            public string FilmDescr;
            public string MultipleFrames;
            public string MultipleViolations;
            public int LastRefNo;
            public string FilmType;
            public string FirstOffenceDateYear;
            public string FirstOffenceDateMonth;
            public string FirstOffenceDateDay;
            public string FirstOffenceDateHour;
            public string FirstOffenceDateMinute;
        }

        public struct ContractorDataRow
        {
            public string RecordType;           //'always D for data rows, I for image rows
            public int Version;
            public string ContractorCode;
            public string AuthCode;
            public string CDLabel;
            public string FilmNo;
            public string FrameNo;
            public string Sequence;
            public string RegNo;
            public string ReferenceNo;
            public string OffenceDateYear;
            public string OffenceDateMonth;
            public string OffenceDateDay;
            public string OffenceDateHour;
            public string OffenceDateMinute;
            public int FirstSpeed;
            public int SecondSpeed;
            public string ElapsedTime;
            public string VehMCode;
            public string VehMDescr;
            public string VehTCode;
            public string VehTDescr;
            public string CourtNo;
            public string CourtName;
            public string OfficerNo;
            public string OfficerGroup;
            public string OfficerSName;
            public string OfficerInit;
            public string CameraID;
            public string CameraLocCode;
            public string LocDescr;
            public string OffenceCode;
            public string OffenceDescr;
            public string OffenceLetter;
            public int OffenderType;
            public double FineAmount;
            public string FineAllocation;
            public int SpeedZone;
            public string RdTypeCode;
            public string RdTypeDescr;
            public string TravelDirection;
            public string RejectReason;
            public string Violation;
            public string ConfirmViolation;
            public string ManualView;
            public string MultFrames;
            //dls 061020 - add CamSerialNo field to export
            public string CamSerialNo;

            //FT 100505 For Average speed over distance 
            //public int ASD2ndCameraIntNo;
            public DateTime ASDGPSDateTime1;
            public DateTime ASDGPSDateTime2;

            public int ASDTimeDifference;
            public int ASDSectionStartLane;
            public int ASDSectionEndLane;
            public int ASDSectionDistance;

            public string ASD1stCamUnitID;
            public string ASD2ndCamUnitID;
            //public int LCSIntNo;
            public string ASDCameraSerialNo1;
            public string ASDCameraSerialNo2;
        }

        public struct ImageDataRow
        {
            public string RecordType;           //'always I for image rows
            public int Version;
            public string ContractorCode;
            public string ImageType;            //A (A Frame), B (B Frame), R (RegNo), D (Driver), O (Other)
            public string AuthCode;
            public string FilmNo;
            public string FrameNo;
            public string ReferenceNo;
            public string JPegName;
            public int XValue;
            public int YValue;
            public Int32 ImageSize;             //dls 081023 - added but not yet loaded from TOMS CDCreate / 3P Loader
        }

        //public LoadViolations(FTPFunctions.FtpParameters ftpParam, DBFunctions.AccessDBSettings accessDBSettings, string connStr, string logPath)
        public LoadViolations(FTPFunctions.FtpParameters ftpParam, string connStr, string logPath, ImageFileServerDetails imageServer)
        {
            _ftpParam = ftpParam;
            //_accessDBSettings = accessDBSettings;
            _connStr = connStr;
            _logPath = logPath;
            _imageServer = imageServer;

            //dls 081029 - instantiate new error log
            tmsErrorLog = new TMSErrorLogDB(_connStr);

            tmsErrorLogDetails.TELAppName = "TMS_ViolationLoader";
            tmsErrorLogDetails.TELModuleName = "LoadViolations";
            tmsErrorLogDetails.TELIntNo = 0;
            tmsErrorLogDetails.TELDateFixed = DateTime.MinValue;
            tmsErrorLogDetails.TELDateTime = DateTime.MinValue;
            tmsErrorLogDetails.TELFixedByUser = null;

            //add camera set up rejection reason - this one actually uses the string RejResaon in the SP, not the integer key
            RejectionDB rejDB = new RejectionDB(this._connStr);
            this.lowSpeedRejReason = "Camera Setup - incorrect speed limit";
            this.lowSpeedRejIntNo = rejDB.UpdateRejection(0, lowSpeedRejReason, "N", this._ftpParam.ftpProcess, 0, "Y");

            //add low speed rejection reason
            this.invalidSpeed = "Zero or low speed - unable to prosecute";
            this.lowSpeedRejIntNo = rejDB.UpdateRejection(0, invalidSpeed, "N", this._ftpParam.ftpProcess, 0, "Y");

            //add low speed rejection reason
            this.asdInvalidSetup = "ASD Camera Setup Invalid";
            this.asdInvalidRejIntNo = rejDB.UpdateRejection(0, asdInvalidSetup, "N", this._ftpParam.ftpProcess, 0, "Y");

            //expired
            this.expiredRejReason = "Expired. The frame offence date is too old to continue";
            this.expiredRejIntNo = rejDB.UpdateRejection(0, expiredRejReason, "N", this._ftpParam.ftpProcess, 0, "Y");
        }

        public bool ProcessLocalFolders(string targetDirectory, ref StreamWriter writer, bool rootFolder, string contractorCode, string exportDirectory)
        {
            tmsErrorLogDetails.TELProcName = "ProcessLocalFolders";

            bool failed = false;

            string folder = Path.GetFileName(targetDirectory);

            if (!folder.ToLower().Equals("received") && !folder.ToLower().Equals("sent"))
            {
                if (!rootFolder) exportDirectory += "\\" + Path.GetFileName(targetDirectory);

                if (Directory.Exists(targetDirectory))
                {
                    string[] fileEntries = System.IO.Directory.GetFiles(targetDirectory);
                    foreach (string fileName in fileEntries)
                    {
                        //if (fileName.ToLower().EndsWith(".ts1") |
                        //       fileName.ToLower().EndsWith(".ts2") |
                        //       fileName.ToLower().EndsWith("_tms.mdb"))
                        if (fileName.ToLower().EndsWith(".ts1"))
                        {
                            writer.WriteLine("ProcessLocalFolders: processing " + fileName + " at " + DateTime.Now.ToString());
                            writer.WriteLine();
                            writer.Flush();

                            failed = ProcessViolations(fileName, ref writer, contractorCode);

                            //move all the contents of the sub-folders to the Received folder
                            //exportDirectory = Path.GetDirectoryName(exportDirectory);

                            bool getFolder = gen.CheckFolder(exportDirectory);

                            if (getFolder)
                            {
                                //writer.WriteLine("ProcessLocalFolders: unable to move files to Receive folder");
                                tmsErrorLogDetails.TELErrMessage = "Unable to move files to Receive folder";
                                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                return failed;
                            }

                            gen.MoveFiles(targetDirectory, exportDirectory);

                            try
                            {
                                if (!rootFolder) Directory.Delete(targetDirectory, true);
                            }
                            catch { }
                        }
                    }
                }
                if (Directory.Exists(targetDirectory))
                {

                    // read the names of the subdirectories of this root folder.
                    string[] subdirectoryEntries = System.IO.Directory.GetDirectories(targetDirectory);

                    // Recurse into subdirectories of this directory.
                    foreach (string subdirectory in subdirectoryEntries)
                    {
                        string subfolder = Path.GetFileName(subdirectory);

                        if (!subfolder.ToLower().Equals("received") && !subfolder.ToLower().Equals("sent"))
                        {
                            //need to get sub contractor folder names, codes and email addresses - only happens the first time we go in
                            if (rootFolder)
                            {
                                ContractorDB contractor = new ContractorDB(_connStr);

                                contractorCode = Path.GetFileName(subdirectory);
                                if (contractorCode.Length == 2)
                                {
                                    ContractorDetails conDetails = contractor.GetContractorDetailsByConCode(contractorCode);
                                    contractorList.Add(conDetails);
                                }
                            }
                            ProcessLocalFolders(subdirectory, ref writer, false, contractorCode, exportDirectory);
                        }
                    }
                }
            }
            return failed;
        }

        private bool CheckHashFile(string ts1FileName, ref StreamWriter writer, string conCode)
        {
            tmsErrorLogDetails.TELProcName = "CheckHashFile";

            bool noErrors = false;
            bool failed = false;

            string path = Path.GetDirectoryName(ts1FileName);
            string hashFile = path + "\\" + Path.GetFileNameWithoutExtension(ts1FileName) + ".hashes";

            writer.WriteLine("CheckHashFile: processing hashFile " + hashFile + " at " + DateTime.Now.ToString());
            writer.WriteLine();
            writer.Flush();

            Stalberg.TMS_3P_Loader.Hashing hasher = new Hashing();

            writer.WriteLine("CheckHashFile: hashFile " + hashFile + " initialised");
            writer.WriteLine();
            writer.Flush();

            noErrors = hasher.VerifyHashFile(hashFile);

            writer.WriteLine("CheckHashFile: hashFile " + hashFile + " verified " + noErrors.ToString());
            writer.WriteLine();
            writer.Flush();

            string conEmail = "";
            string conSName = "";
            string conFName = "";

            for (int i = 0; i < contractorList.Count; i++)
            {
                _currentContractor = (ContractorDetails)contractorList[i];
                if (_currentContractor.ConCode.Equals(conCode))
                {
                    conEmail = _currentContractor.ConSupportEmail;
                    conSName = _currentContractor.ConSupportSName;
                    conFName = _currentContractor.ConSupportFName;
                    break;
                }
            }

            //read and check hashFile file
            if (!noErrors)
            {
                failed = true;

                //create error file to return to Traffic/3rd party via ftp
                CreateErrorFile(0, 0, ts1FileName, "Error in hash file", ref writer, "header_only", false);

                //create the error file
                string errors = _logPath + "\\" + Path.GetFileName(hashFile) + ".errors";

                FileStream fsWriter = new FileStream(errors, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fsWriter, System.Text.Encoding.Default);

                sw.WriteLine(hasher.Errors);
                sw.Close();
                fsWriter.Close();

                //create the file list
                string fileList = _logPath + "\\" + Path.GetFileName(hashFile) + ".filelist";

                fsWriter = new FileStream(fileList, FileMode.OpenOrCreate, FileAccess.Write);
                sw = new StreamWriter(fsWriter, System.Text.Encoding.Default);

                sw.WriteLine(hasher.FileList);
                sw.Close();
                fsWriter.Close();

                if (conEmail.Equals(""))
                {
                    failed = true;
                    //writer.WriteLine("CheckHashFile: Contractor code " + conCode + " unable to email error details to contractor: no email address at: " + DateTime.Now.ToString());
                    tmsErrorLogDetails.TELErrMessage = "Contractor code " + conCode + " unable to email error details to contractor: no email address";
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                    return failed;
                }

                //email error details and filelist to contractor             
                string message = "Attention: " + conFName + " " + conSName + "\n\n"
                       + "On the " + DateTime.Now.ToString() + " the following error file was created by the " + _ftpParam.ftpProcess + " process:\n\n"
                       + "File name: " + hashFile + "\n\n"
                       + "The error list and file lists are attached to this email.\n\n"
                       + "Please check the reason for the errors.\n\n"
                       + "Thank you very much.\n\n"
                       + "Regards\n"
                       + "TMS System Administrator";

                try
                {
                    MailAddress from = new MailAddress(_ftpParam.email);
                    MailAddress to = new MailAddress(conEmail);

                    MailMessage mail = new MailMessage(from, to);
                    mail.Subject = _ftpParam.ftpHostServer.ToUpper() + " " + _ftpParam.ftpProcess + " hash file errors";
                    mail.Body = message;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    Attachment myAttachment = new Attachment(fileList);
                    mail.Attachments.Add(myAttachment);
                    myAttachment = new Attachment(errors);
                    mail.Attachments.Add(myAttachment);
                    try
                    {
                        SmtpClient mailClient = new SmtpClient();
                        mailClient.Host = _ftpParam.smtp;
                        mailClient.UseDefaultCredentials = true;
                        mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                        //Send delivers the message to the mail server
                        mailClient.Send(mail);
                        writer.WriteLine("CheckHashFile: Contractor code " + conCode + " email error details sent to " + conEmail + " at: " + DateTime.Now.ToString());
                        writer.Flush();
                    }
                    catch (Exception smtpEx)
                    {
                        //writer.WriteLine("CheckHashFile: Contractor code " + conCode + " Failed to send email of error details and filelist to " + conEmail + " - " + smtpEx.Message);
                        tmsErrorLogDetails.TELErrMessage = "Contractor code " + conCode + " Failed to send email of error details and filelist to " + conEmail + " - " + smtpEx.Message;
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                    }
                }
                catch (Exception emailEx)
                {
                    //writer.WriteLine("CheckHashFile: Contractor code " + conCode + "Failed to send email of error details and filelist - " + emailEx.Message);
                    tmsErrorLogDetails.TELErrMessage = "Contractor code " + conCode + "Failed to send email of error details and filelist - " + emailEx.Message;
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                }

            }
            return failed;
        }

        //private void CreateErrorFile(int autIntNo, int filmIntNo, string ts1FileName, string errMessge, ref StreamWriter writer, string type, bool allOK)
        //{
        //    CreateErrorFile(autIntNo, filmIntNo, ts1FileName, errMessge, ref writer, type, allOK, false);
        //}

        //private void CreateErrorFile(int autIntNo, int filmIntNo, string ts1FileName, string errMessge, ref StreamWriter writer, string type, bool allOK, bool reprocess)
        private void CreateErrorFile(int autIntNo, int filmIntNo, string ts1FileName, string errMessge, ref StreamWriter writer, string type, bool allOK)
        {
            tmsErrorLogDetails.TELProcName = "CreateErrorFile";
            FileStream fsReader = null;
            StreamReader sr = null;

            try
            {
                //read the .ts1 file to get the Header/Data rows
                fsReader = new FileStream(ts1FileName, FileMode.Open, FileAccess.Read);
                sr = new StreamReader(fsReader, System.Text.Encoding.UTF7);
                //create the error file
                string path = _ftpParam.ftpExportPath + "\\" + _currentContractor.ConCode + "\\";
                string errorFile = "";

                if (allOK)
                    errorFile = path + Path.GetFileName(ts1FileName) + ".ok";
                else
                    errorFile = path + Path.GetFileName(ts1FileName) + ".err";

                FileStream fsWriter = new FileStream(errorFile, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fsWriter, System.Text.Encoding.Default);

                int a = 255;
                char aChar = (char)a;
                String aString = aChar.ToString();

                string srLine = "";

                int b = 254;
                char bChar = (char)b;
                String bString = bChar.ToString();

                char[] delimiter = aString.ToCharArray();
                string[] split = null;

                //HeaderRow header = new HeaderRow();
                ContractorDataRow data = new ContractorDataRow();
                LoadViolationsDB loadviol = new LoadViolationsDB(_connStr);


                while (!sr.EndOfStream)
                {
                    //read the .ts1 file to get the Header row
                    srLine = sr.ReadLine();
                    if (type.Equals("header_only"))
                    {
                        //write the header row + error description
                        sw.WriteLine(srLine + aString + errMessge);
                        sw.Flush();
                        break;
                    }
                    else
                    {
                        split = srLine.Split(delimiter, 60);
                        //write each row + errors
                        string errors = "";
                        int i = 0;
                        data.RecordType = split[i];
                        if (data.RecordType.Equals("H"))
                        {
                            sw.WriteLine(srLine + aString + errMessge);
                        }
                        else if (data.RecordType.Equals("D"))
                        {
                            i += 1;
                            data.Version = Convert.ToInt32(split[i]);
                            i += 1;
                            data.ContractorCode = split[i];
                            i += 1;
                            data.AuthCode = split[i];
                            i += 1;
                            data.CDLabel = split[i];
                            i += 1;
                            data.FilmNo = split[i];
                            i += 1;
                            data.FrameNo = split[i];
                            i += 1;
                            //get errors from database by frame
                            SqlDataReader reader = loadviol.GetLoadViolationErrorsByFrame(autIntNo, data.FilmNo,
                                                                                          data.FrameNo, "0");
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    errors += reader["ErrorDescription"].ToString() + bString;
                                }
                                sw.WriteLine(srLine + aString + errors);
                            }
                            else
                            {
                                sw.WriteLine(srLine);
                            }

                            reader.Close();
                        }
                    }
                }

                sr.Close();
                fsReader.Close();

                sw.Flush();
                sw.Close();
                fsWriter.Close();

                System.Threading.Thread.Sleep(10);

                string destinationPath = Path.GetDirectoryName(_ftpParam.ftpReceivedPath) + @"\Sent\" +
                                         _currentContractor.ConCode;

                if (_ftpParam.mode.ToLower().Equals("centralised"))
                {
                    //put error file on ftp site
                    FTPFunctions ftp = new FTPFunctions();

                    bool failed = ftp.FtpFileSend(_currentContractor, ref writer, errorFile, destinationPath);

                }
                else
                {
                    //send error file by email to contractor email address
                    if (_currentContractor.ConSupportEmail.Equals(""))
                    {
                        //writer.WriteLine("CreateErrorFile: unable to email error details to contractor: no email address at: " + DateTime.Now.ToString());
                        tmsErrorLogDetails.TELErrMessage =
                            "Unable to email error details to contractor: no email address";
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                    }

                    //email error details and filelist to contractor             
                    string message = "Attention: " + _currentContractor.ConSupportFName + " " +
                                     _currentContractor.ConSupportSName + "\n\n"
                                     + "On the " + DateTime.Now.ToString() +
                                     " the following output file was created by the " + _ftpParam.ftpProcess +
                                     " process:\n\n"
                                     + "File name: " + errorFile + "\n\n"
                                     + "The file is attached to this email.\n\n"
                                     + "Thank you very much.\n\n"
                                     + "Regards\n"
                                     + "TMS System Administrator";

                    try
                    {
                        MailAddress from = new MailAddress(_ftpParam.email);
                        MailAddress to = new MailAddress(_currentContractor.ConSupportEmail);

                        MailMessage mail = new MailMessage(from, to);
                        mail.Subject = _ftpParam.ftpHostServer.ToUpper() + " " + _ftpParam.ftpProcess +
                                       " output file";
                        mail.Body = message;
                        mail.BodyEncoding = System.Text.Encoding.UTF8;
                        Attachment myAttachment = new Attachment(errorFile);
                        mail.Attachments.Add(myAttachment);
                        try
                        {
                            SmtpClient mailClient = new SmtpClient();
                            mailClient.Host = _ftpParam.smtp;
                            mailClient.UseDefaultCredentials = true;
                            mailClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                            //Send delivers the message to the mail server
                            mailClient.Send(mail);

                            writer.WriteLine("CreateErrorFile: output file " + errorFile + " sent to " +
                                             _currentContractor.ConSupportEmail + " at: " + DateTime.Now.ToString());
                            writer.Flush();
                        }
                        catch (Exception smtpEx)
                        {
                            //writer.WriteLine("CreateErrorFile: Failed to send output file to " + _currentContractor.ConSupportEmail + " - " + smtpEx.Message);
                            tmsErrorLogDetails.TELErrMessage = "Failed to send output file to " +
                                                               _currentContractor.ConSupportEmail + " - " +
                                                               smtpEx.Message;
                            tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                        }

                    }
                    catch (Exception emailEx)
                    {
                        //writer.WriteLine("CreateErrorFile: Failed to send output file " + errorFile + " - " + emailEx.Message);
                        tmsErrorLogDetails.TELErrMessage = "Failed to send output file " + errorFile + " - " +
                                                           emailEx.Message;
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                    }

                    bool copyFailed = gen.CopyFile(errorFile, destinationPath, Path.GetFileName(errorFile));
                    if (copyFailed == true)
                    {
                        //writer.WriteLine("CreateErrorFile copyFailed: " + errorFile + " " + DateTime.Now.ToString());
                        tmsErrorLogDetails.TELErrMessage = "Copy Failed: " + errorFile;
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                    }

                    // delete file from folder
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(10);

                    bool deleteFailed = gen.DeleteFile(errorFile);
                    if (deleteFailed == true)
                    {
                        //writer.WriteLine("CreateErrorFile deleteFailed: " + errorFile + " " + DateTime.Now.ToString());
                        tmsErrorLogDetails.TELErrMessage = "Delete Failed: " + errorFile;
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                    }
                }


                FilmDB film = new FilmDB(_connStr);
                int updFilmIntNo = 0;

                if (type.Equals("header_only"))
                {
                    if (allOK)
                    {
                         updFilmIntNo = film.UpdateFilmLoadStatus(filmIntNo, STATUS_FILM_SUCCESSFUL, errMessge, ViolationLoader.APP_NAME);
                    }
                    else
                    {
                        if (filmIntNo > 0)
                            updFilmIntNo = film.UpdateFilmLoadStatus(filmIntNo, STATUS_FILM_DATA_ERROR, errMessge, ViolationLoader.APP_NAME);
                    }
                }
                else
                {
                    updFilmIntNo = film.UpdateFilmLoadStatus(filmIntNo, STATUS_FILM_DATA_ERROR, errMessge, ViolationLoader.APP_NAME);
                }
                return;
            }
            catch (Exception a)
            {
                //writer.WriteLine("CreateErrorFile Error: " + a.Message);
                tmsErrorLogDetails.TELErrMessage = "Error: " + a.Message;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                return;
            }
            finally
            {
                fsReader.Dispose();
                sr.Dispose();
            }
        }

        public bool ProcessViolations(string fileName, ref StreamWriter writer, string conCode)
        {
            tmsErrorLogDetails.TELProcName = "ProcessViolations";

            bool failed = false;

            string savelocation = System.IO.Path.GetDirectoryName(fileName);

            writer.WriteLine("ProcessViolations: Check hash file for " + fileName);
            writer.WriteLine();
            writer.Flush();

            //check has file that all images have arrived, and .ts1 has correct data contents
            failed = CheckHashFile(fileName, ref writer, conCode);

            if (failed) return failed;

            //dls 081023 - we need to check that the correct images are present based on the Authority rules for this film type
            //           - move the checking of authority rules and details before we check the images.

            AuthorityDB authority = new AuthorityDB(_connStr);

            string file = System.IO.Path.GetFileName(fileName);
            int autIntNo = 0;

            try
            {
                string autCode = file.Substring(0, 2);
                string errMessage = "";

                SqlDataReader autDetails = authority.GetAuthorityDetailsByAutCode(autCode, ref errMessage);

                if (autDetails == null)
                {
                    //writer.WriteLine("ProcessViolations: GetAuthorityDetailsByAutCode failed - " + errMessage);
                    tmsErrorLogDetails.TELErrMessage = "GetAuthorityDetailsByAutCode failed - " + errMessage;
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                    failed = true;
                    return failed;
                }

                while (autDetails.Read())
                {
                    autIntNo = Convert.ToInt32(autDetails["AutIntNo"]);
                }

                if (autIntNo == 0)
                {
                    //writer.WriteLine("ProcessViolations: Authority " + autCode + " does not exist in database");
                    tmsErrorLogDetails.TELErrMessage = "Authority " + autCode + " does not exist in database";
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                    failed = true;
                    CreateErrorFile(autIntNo, 0, fileName, "Authority " + autCode + " does not exist in database", ref writer, "header_only", false);
                }
                //else if (file.ToLower().EndsWith(".ts1") | file.ToLower().EndsWith(".ts2"))
                else if (file.ToLower().EndsWith(".ts1"))
                {
                    //get the rule for working out whether Natis must be completed 1st/last
                    // Check if this authority processes NaTIS data last
                    //AuthorityRulesDB arDB = new AuthorityRulesDB(_connStr);
                    //AuthorityRulesDetails arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "0400",
                    //    "Rule that indicates whether to process NaTIS data last, i.e. after Adjudication.", 0,
                    //    "Y", "Y = Default, process NaTIS data after Adjudication, N = Process NaTIS data before verification and before Adjudication",
                    //    _ftpParam.ftpProcess);

                    //  20090113 SD:
                    AuthorityRulesDetails arDetails0400 = new AuthorityRulesDetails();
                    arDetails0400.AutIntNo = autIntNo;
                    arDetails0400.ARCode = "0400";
                    arDetails0400.LastUser = _ftpParam.ftpProcess;
                    DefaultAuthRules authRule0400 = new DefaultAuthRules(arDetails0400, _connStr);
                    KeyValuePair<int, string> value0400 = authRule0400.SetDefaultAuthRule();

                    string msg = "";
                    if (value0400.Value.Equals("Y"))
                    {
                        _natisLast = true;
                        msg = "Natis files will be sent after Adjudication";
                    }
                    else
                    {
                        _natisLast = false;
                        msg = "Natis files will be sent before Verification";
                    }
                    writer.WriteLine("ProcessViolations: " + msg + " for Authority " + autCode);
                    writer.WriteLine();
                    writer.Flush();

                    //need to get authority rules for the no. of images that should be printed
                    //   arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "2510", "Rules for notice printing - # images to print",
                    //     1, "", "1 = print 1 image (A); 2 = print 2 images (A+B); 3 = print 2 images (A+R)", _ftpParam.ftpProcess);

                    AuthorityRulesDetails arDetails2510 = new AuthorityRulesDetails();
                    arDetails2510.AutIntNo = autIntNo;
                    arDetails2510.ARCode = "2510";
                    arDetails2510.LastUser = _ftpParam.ftpProcess;
                    DefaultAuthRules authRule2510 = new DefaultAuthRules(arDetails2510, _connStr);
                    KeyValuePair<int, string> value2510 = authRule2510.SetDefaultAuthRule();

                    // 1: print only the one image (A)
                    // 2: print two images (A) and (B)
                    // 3: print two images (A) and (R)
                    // 4: print three images (A) and (B) - if it exists - and (R)
                    // 5: print three images (A) and (R) and (D) - speed only

                    _noOfImagesToPrint = value2510.Key; //arDetails.ARNumeric;

                    writer.WriteLine("ProcessViolations: No of Images to print for Authority " + autCode + ": " + _noOfImagesToPrint.ToString());
                    writer.WriteLine();
                    writer.Flush();

                    //dls 070608 - new auth rul for printing of digital  speed images
                    //arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "2520", "Rules for notice printing - select print image for TraffiPax (P)",
                    //    1, "", "1 = print 1st image (default); 2 = print 2nd image", _ftpParam.ftpProcess);

                    // SD:20090113
                    AuthorityRulesDetails arule2520 = new AuthorityRulesDetails();
                    arule2520.AutIntNo = autIntNo;
                    arule2520.ARCode = "2520";
                    arule2520.LastUser = _ftpParam.ftpProcess;
                    DefaultAuthRules authRule2520 = new DefaultAuthRules(arule2520, _connStr);
                    KeyValuePair<int, string> value2520 = authRule2520.SetDefaultAuthRule();

                    _whichImageTraffiPax = value2520.Key; //arDetails.ARNumeric;

                    writer.WriteLine("ProcessViolations: TrafficPax Image to print for Authority " + autCode + ": " + _whichImageTraffiPax.ToString());
                    writer.WriteLine();
                    writer.Flush();

                    //arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "2530", "Rules for notice printing - select print image for Digital (D)",
                    //    1, "", "1 = print 1st image(default); 2 = print 2nd image", _ftpParam.ftpProcess);


                    AuthorityRulesDetails arule2530 = new AuthorityRulesDetails();
                    arule2530.AutIntNo = autIntNo;
                    arule2530.ARCode = "2530";
                    arule2530.LastUser = _ftpParam.ftpProcess;
                    DefaultAuthRules authRule2530 = new DefaultAuthRules(arule2530, _connStr);
                    KeyValuePair<int, string> value2530 = authRule2530.SetDefaultAuthRule();

                    _whichImageDigital = value2530.Key; //arDetails.ARNumeric;

                    writer.WriteLine("ProcessViolations: Digital Image to print for Authority " + autCode + ": " + _whichImageDigital.ToString());
                    writer.WriteLine();
                    writer.Flush();

                    // David 20100326 Remove images from database to image file server
                    _saveToFileSystem = true;

                    //dls 070808 - get AuthorityRule for saving images to file system
                    //arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "2540", "Rule for saving images to File System for Adjudication/Verification",
                    //    0, "N", "Y = Yes; N = No (Default)", _ftpParam.ftpProcess);
                    //_saveToFileSystem = arDetails.ARString.Equals("Y") ? true : false;                    

                    ////check that the path for the images does exist. if not, try to create it
                    //if (_saveToFileSystem)
                    //{
                    //    if (_ftpParam.imageFolder.Equals(""))
                    //    {
                    //        //writer.WriteLine("ProcessViolations: No image folder element in parameters file for " + autCode);
                    //        tmsErrorLogDetails.TELErrMessage = "No image folder element in parameters file for " + autCode;
                    //        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                    //        failed = true;
                    //        return failed;
                    //    }
                    //    else if (!Directory.Exists(_ftpParam.imageFolder))
                    //    {
                    //        try
                    //        {
                    //            Directory.CreateDirectory(_ftpParam.imageFolder);
                    //            writer.WriteLine("ProcessViolations: Image folder (" + _ftpParam.imageFolder + ") created for Authority " + autCode);
                    //            writer.WriteLine();
                    //            writer.Flush();
                    //        }
                    //        catch (Exception exc)
                    //        {
                    //            //writer.WriteLine("ProcessViolations: Unable to create image folder (" + _ftpParam.imageFolder + ") for Authority " + autCode + ": " + exc.Message);
                    //            tmsErrorLogDetails.TELErrMessage = "Unable to create image folder (" + _ftpParam.imageFolder + ") for Authority " + autCode + ": " + exc.Message;
                    //            tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                    //            failed = true;
                    //            return failed;
                    //        }
                    //    }

                    //}
                    //writer.WriteLine("ProcessViolations: Saving images to file system for Authority " + autCode + ": " + _saveToFileSystem.ToString());
                    //writer.WriteLine();
                    //writer.Flush();

                    //tf 20090611 - get AuthorityRule for 100% adjuducated
                    //arDetails = arDB.GetAuthorityRulesDetailsByCode(autIntNo, "0600", "Rule to ensure that 100% of frames are Adjudicated",
                    //    0, "N", "Y - Yes; N - No(Default)", _ftpParam.ftpProcess);

                    //_fullAdjudicated = arDetails.ARString;

                    AuthorityRulesDetails adj100pc = new AuthorityRulesDetails();

                    adj100pc.AutIntNo = autIntNo;
                    adj100pc.ARCode = "0600";
                    adj100pc.LastUser = _ftpParam.ftpProcess;

                    DefaultAuthRules defAuthRule = new DefaultAuthRules(adj100pc, _connStr);
                    KeyValuePair<int, string> adj100pcRule = defAuthRule.SetDefaultAuthRule();
                    _fullAdjudicated = adj100pcRule.Value;

                    //dls 090617 - they are not allowed to adjudicate at 100% if they are using Natis Last = true
                    if (_natisLast && _fullAdjudicated.Equals("Y"))
                    {
                        tmsErrorLogDetails.TELErrMessage = "Authority " + autCode + " may not have Natis last = True and Adjudication @ 100% = true!";
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                        failed = true;
                        return failed;
                    }

                    //vehicle exclusions rule
                    AuthorityRulesDetails arDetails8000 = new AuthorityRulesDetails();
                    arDetails8000.AutIntNo = autIntNo;
                    arDetails8000.ARCode = "8000";
                    arDetails8000.LastUser = _ftpParam.ftpProcess;
                    DefaultAuthRules authRule8000 = new DefaultAuthRules(arDetails8000, _connStr);
                    KeyValuePair<int, string> value8000 = authRule8000.SetDefaultAuthRule();

                    _excluListActive = value8000.Value;

                    //max speed over speed limit rule
                    AuthorityRulesDetails arDetails8010 = new AuthorityRulesDetails();
                    arDetails8010.AutIntNo = autIntNo;
                    arDetails8010.ARCode = "8010";
                    arDetails8010.LastUser = _ftpParam.ftpProcess;
                    DefaultAuthRules authRule8010 = new DefaultAuthRules(arDetails8010, _connStr);
                    KeyValuePair<int, string> value8010 = authRule8010.SetDefaultAuthRule();

                    _excluSpeedAllowance = value8010.Key;

                    //Fred moved from  ValidateAndLoadInputData
                    //no of days from Offence Date to Issue Date
                    DateRulesDetails drDetails = new DateRulesDetails();
                    drDetails.AutIntNo = autIntNo;
                    drDetails.DtRStartDate = "NotOffenceDate";
                    drDetails.DtREndDate = "NotIssue1stNoticeDate";
                    drDetails.LastUser = this._ftpParam.ftpProcess;

                    DefaultDateRules dr = new DefaultDateRules(drDetails, this._connStr);
                    noOfDays = dr.SetDefaultDateRule();
                }
            }
            catch (Exception e)
            {
                //writer.WriteLine("ProcessViolations: " + e.Message + " " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = e.Message;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                failed = true;
                //CreateErrorFile(autIntNo, 0, fileName, "Data error: " + e.Message, ref writer, "header_only", false);
            }

            // check that all frames have at least one image
            failed = CheckAllImages(fileName, ref writer);

            if (failed) return failed;

            failed = ProcessViolationsTextFile(ref writer, fileName, autIntNo, savelocation);

            if (autIntNo > 0)
            {
                bool del = DeleteTempViolations(autIntNo, ref writer);
            }
            return failed;
        }

        private bool CheckAllImages(string fileName, ref StreamWriter writer)
        {
            tmsErrorLogDetails.TELProcName = "CheckAllImages";

            bool failed = false;

            HeaderRow header = new HeaderRow();

            try
            {
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs, System.Text.Encoding.UTF7);

                try
                {
                    int a = 255;
                    char aChar = (char)a;
                    String aString;

                    aString = aChar.ToString();

                    char[] delimiter = aString.ToCharArray();
                    string[] split = null;

                    ArrayList dataRows = new ArrayList();
                    ArrayList imgRows = new ArrayList();

                    string srLine = "";
                    while (!sr.EndOfStream)
                    {
                        srLine = sr.ReadLine();
                        try
                        {
                            split = srLine.Split(delimiter, 60);
                        }
                        catch
                        {
                            //writer.WriteLine("CheckAllImages: Incorrect number of fields in : " + fileName);
                            tmsErrorLogDetails.TELErrMessage = "Incorrect number of fields in : " + fileName;
                            tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                            failed = true;
                            sr.Close();
                            fs.Close();
                            CreateErrorFile(0, 0, fileName, " Incorrect number of fields in file", ref writer, "header_only", false);
                            return failed;
                        }

                        ContractorDataRow data = new ContractorDataRow();
                        ImageDataRow imageData = new ImageDataRow();

                        switch (split[0])
                        {
                            case "H":
                                try
                                {
                                    header = SetHeaderRowValues(split);
                                }
                                catch (Exception e)
                                {
                                    //writer.WriteLine("CheckAllImages: ReadHeaderDataRow " + e.Message);
                                    tmsErrorLogDetails.TELErrMessage = "ReadHeaderDataRow " + e.Message;
                                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                    failed = true;
                                    sr.Close();
                                    fs.Close();
                                    CreateErrorFile(0, 0, fileName, "Unable to read header from file " + e.Message, ref writer, "header_only", false);
                                    return failed;
                                }
                                break;

                            case "D":
                                try
                                {
                                    data = SetDataRowValues(split);

                                    dataRows.Add(data);
                                }
                                catch (Exception e)
                                {
                                    //writer.WriteLine("CheckAllImages: ReadContractorDataRow " + e.Message);
                                    tmsErrorLogDetails.TELErrMessage = "ReadContractorDataRow " + e.Message;
                                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                    failed = true;
                                    sr.Close();
                                    fs.Close();
                                    CreateErrorFile(0, 0, fileName, "Unable to read data from file " + e.Message, ref writer, "header_only", false);
                                    return failed;
                                }
                                break;

                            case "I":

                                try
                                {
                                    imageData = SetImageRowValues(split);

                                    //writer.WriteLine("CheckAllImages: found ImageDataRow " + imageData.FrameNo + ", " + imageData.JPegName);

                                    imgRows.Add(imageData);
                                }
                                catch (Exception e)
                                {
                                    //writer.WriteLine("CheckAllImages: ReadImageDataRow " + e.Message);
                                    tmsErrorLogDetails.TELErrMessage = "ReadImageDataRow " + e.Message;
                                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                    failed = true;
                                    sr.Close();
                                    fs.Close();
                                    CreateErrorFile(0, 0, fileName, "Unable to read data from file " + e.Message, ref writer, "header_only", false);
                                    return failed;
                                }
                                break;
                        }
                    }

                    sr.Close();
                    fs.Close();

                    string missingImages = "";

                    writer.WriteLine("CheckAllImages: dataRows.Count " + dataRows.Count.ToString());
                    writer.WriteLine("CheckAllImages: imgRows.Count " + imgRows.Count.ToString());

                    for (int i = 0; i < dataRows.Count; i++)
                    {
                        ContractorDataRow data = (ContractorDataRow)dataRows[i];
                        bool foundImage = false;

                        for (int x = 0; x < imgRows.Count; x++)
                        {
                            ImageDataRow imageRow = (ImageDataRow)imgRows[x];

                            //dls 090326 - sometime they put thru a 'dummy' film with one image - NoImage.jpg - this film must be allowed into the system
                            if (imgRows.Count == 1 && dataRows.Count == 1)
                            {
                                if (imageRow.JPegName.ToLower().Equals("noimage.jpg"))
                                {
                                    foundImage = true;
                                    break;
                                }
                            }

                            //dls 081023 - we have to check that the specific image required by the Authority rules is present
                            if (imageRow.FrameNo.Equals(data.FrameNo, StringComparison.InvariantCultureIgnoreCase)
                                    && imageRow.FilmNo.Equals(data.FilmNo, StringComparison.InvariantCultureIgnoreCase)
                                    && imageRow.AuthCode.Equals(data.AuthCode, StringComparison.InvariantCultureIgnoreCase)
                                //&& (imageRow.ImageType.Equals("A", StringComparison.InvariantCultureIgnoreCase) || imageRow.ImageType.Equals("B", StringComparison.InvariantCultureIgnoreCase)
                                    && GetPrintValue(header.FilmType, imageRow.JPegName, imageRow.ImageType) == 1)
                            {
                                foundImage = true;
                                break;
                            }
                        }

                        if (!foundImage)
                            missingImages += data.FrameNo + "; ";
                    }

                    if (!missingImages.Equals(""))
                    {
                        CreateErrorFile(0, 0, fileName, "Missing images for frames: " + missingImages, ref writer, "header_only", false);
                        failed = true;
                    }
                }
                catch (Exception e)
                {
                    //writer.WriteLine("CheckAllImages: Processing filestream " + fileName + " " + e.Message + " " + DateTime.Now.ToString());
                    tmsErrorLogDetails.TELErrMessage = "Processing filestream " + fileName + " " + e.Message;
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                    failed = true;
                    sr.Close();
                    fs.Close();
                    //CreateErrorFile(0, 0, fileName, "Procesing error " + e.Message, ref writer, "header_only", false);
                    return failed;
                }
            }
            catch (Exception ex)
            {
                writer.WriteLine("CheckAllImages: Error " + ex.Message + " " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();
                failed = true;
                return failed;
            }
            return failed;
        }

        public int CheckFilmIntNo(string filmNo, int autIntNo, ref StreamWriter writer, string ts1FileName)
        {
            tmsErrorLogDetails.TELProcName = "CheckFilmIntNo";

            FilmDB film = new FilmDB(_connStr);

            int chkFilmIntNo = film.CheckFilmExists(autIntNo, filmNo);

            //  FilmIntNo =  0:		in the process of loading
            //              -1:		no film exists 
            //              -2:		has already been successfully loaded
            //              -3:		has already gone to validation stage (pre-verification)

            if (chkFilmIntNo > -1)
            {
                //all systems go, we can continue
            }
            else if (chkFilmIntNo == -2)
            {
                //writer.WriteLine("ProcessViolations: film has already been sent to external interface " + filmNo + " " + DateTime.Now.ToString());
                //writer.WriteLine("ProcessViolations: film " + filmNo + " has already been successfully loaded " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Film " + filmNo + " has already been successfully loaded";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                //CreateErrorFile(autIntNo, 0, ts1FileName, "Film " + filmNo + " has already been successfully loaded", ref writer, "header_only", false);
            }
            else if (chkFilmIntNo == -3)
            {
                //writer.WriteLine("ProcessViolations: violations on film " + filmNo + " have already been validated " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Violations on film " + filmNo + " have already been validated";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                //CreateErrorFile(autIntNo, 0, ts1FileName, "Violations on film " + filmNo + " have already been validated", ref writer, "header_only", false);
            }

            return chkFilmIntNo;
        }

        public bool ProcessViolationsTextFile(ref StreamWriter writer, string fileName, int autIntNo, string savelocation)
        {
            tmsErrorLogDetails.TELProcName = "ProcessViolationsTextFile";

            bool failed = false;

            string filePath = System.IO.Path.GetDirectoryName(fileName);

            int noOfHeaderRecords = 0;
            int noOfDataRecords = 0;
            int noOfImageRecords = 0;
            int noOfFailedImageRecords = 0;

            ArrayList filmsLoaded = new ArrayList();
            LoadViolationsDB load = new LoadViolationsDB(_connStr);
            HeaderRow header = new HeaderRow();

            //use an arraylist to add all the data so that we don't keep 
            //round-tripping to the database!
            ArrayList violations = new ArrayList();

            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs, System.Text.Encoding.UTF7);

            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(300)))
                {
                    try
                    {
                        int a = 255;
                        char aChar = (char)a;
                        String aString;

                        aString = aChar.ToString();

                        char[] delimiter = aString.ToCharArray();
                        string[] split = null;

                        //delete all temp violations
                        failed = DeleteTempViolations(ref writer, autIntNo, 0);

                        if (failed) return false;

                        int lastPushedFrameIntNo = 0;
                        string srLine = "";
                        while (!sr.EndOfStream)
                        {
                            srLine = sr.ReadLine();
                            try
                            {
                                split = srLine.Split(delimiter, 60);
                            }
                            catch
                            {
                                //writer.WriteLine("ProcessViolationsTextFile: Incorrect number of fields in : " + fileName);
                                tmsErrorLogDetails.TELErrMessage = "Incorrect number of fields in : " + fileName;
                                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                failed = true;
                                sr.Close();
                                fs.Close();
                                CreateErrorFile(0, 0, fileName, " Incorrect number of fields in file", ref writer, "header_only", false);

                                return failed;
                            }

                            ContractorDataRow data = new ContractorDataRow();
                            ImageDataRow imageData = new ImageDataRow();

                            switch (split[0])
                            {
                                case "H":
                                    try
                                    {
                                        header = this.SetHeaderRowValues(split);
                                    }
                                    catch (Exception e)
                                    {
                                        //writer.WriteLine("ProcessViolationsTextFile: ReadHeaderRow " + e.Message);
                                        tmsErrorLogDetails.TELErrMessage = "ReadHeaderRow " + e.Message;
                                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                        CreateErrorFile(0, 0, fileName, "Unable to read data from file " + e.Message, ref writer, "header_only", false);
                                        failed = true;
                                        sr.Close();
                                        fs.Close();
                                        return failed;
                                    }

                                    failed = ProcessHeaderRow(header, ref writer, fileName, autIntNo, ref filmsLoaded);

                                    if (!failed)
                                        noOfHeaderRecords += 1;
                                    else
                                    {
                                        sr.Close();
                                        fs.Close();
                                        return failed;
                                    }

                                    break;

                                case "D":
                                    try
                                    {
                                        data = this.SetDataRowValues(split);

                                        failed = ProcessDataRow(ref violations, data, ref writer, fileName, autIntNo, savelocation);

                                        if (!failed) noOfDataRecords += 1;
                                    }
                                    catch (Exception e)
                                    {
                                        //writer.WriteLine("ProcessViolationsTextFile: ReadContractorDataRow " + e.Message);
                                        tmsErrorLogDetails.TELErrMessage = "ReadContractorDataRow " + e.Message;
                                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                        CreateErrorFile(0, 0, fileName, "Unable to read data from file " + e.Message, ref writer, "header_only", false);

                                        failed = true;
                                        sr.Close();
                                        fs.Close();
                                        return failed;
                                    }
                                    break;

                                case "I":

                                    if (violations.Count > 0)
                                    {
                                        //need to load data that preceeds these images
                                        //failed = ValidateAndLoadInputData(ref violations, autIntNo, ref writer, fileName, header.reprocess);
                                        failed = ValidateAndLoadInputData(ref violations, autIntNo, ref writer, fileName);

                                        violations.Clear();

                                        if (failed)
                                        {
                                            sr.Close();
                                            fs.Close();
                                            CreateErrorFile(autIntNo, _filmIntNo, fileName, "Data errors", ref writer, "data", false);
                                            return failed;
                                        }
                                    }

                                    try
                                    {
                                        imageData = SetImageRowValues(split);
                                    }
                                    catch (Exception e)
                                    {
                                        //writer.WriteLine("ProcessViolationsTextFile: ReadImageDataRow " + e.Message);
                                        tmsErrorLogDetails.TELErrMessage = "ReadImageDataRow " + e.Message;
                                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                        failed = true;
                                        sr.Close();
                                        fs.Close();
                                        CreateErrorFile(0, 0, fileName, "Unable to read data from file " + e.Message, ref writer, "header_only", false);
                                        return failed;
                                    }

                                    string imagePath = filePath + @"\" + imageData.FilmNo + @"\" + imageData.JPegName;

                                    if (imagePath.ToLower().EndsWith(".jpg"))
                                    {
                                        string jpegName = imagePath;

                                        //if (!File.Exists(imagePath))
                                        //    jpegName = System.IO.Path.ChangeExtension(imagePath, "jp2");

                                        try
                                        {
                                            if (File.Exists(jpegName))
                                            {
                                                failed = ProcessImageRow(imageData, autIntNo, jpegName, ref writer, fileName, header.AuthCode, ref lastPushedFrameIntNo);
                                                if (!failed)
                                                    noOfImageRecords++;
                                                else
                                                    noOfFailedImageRecords++;
                                            }
                                            else
                                            {
                                                //failed = true; - no point setting this to true - it gets over written ==> checking the no. of failer records instead
                                                noOfFailedImageRecords++;
                                                //writer.WriteLine("ProcessViolationsTextFile: image file " + jpegName + " does not exist");
                                                tmsErrorLogDetails.TELErrMessage = "Image file " + jpegName + " does not exist";
                                                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                                Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, imageData.FilmNo, " image file " + jpegName + " does not exist", "Image", "", "", imageData.FrameNo);
                                                //CreateErrorFile(autIntNo, _filmIntNo, fileName, "image file " + jpegName + " does not exist", ref writer, "data", false);
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            //failed = true; - no point setting this to true - it gets over written ==> checking the no. of failer records instead
                                            noOfFailedImageRecords++;
                                            //writer.WriteLine("ProcessViolationsTextFile: Processing image file " + jpegName + " " + e.Message + " " + DateTime.Now.ToString());
                                            tmsErrorLogDetails.TELErrMessage = "Processing image file " + jpegName + " " + e.Message;
                                            tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                                            Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, imageData.FilmNo, "Processing image file failed " + " " + e.Message, "JPeg name", jpegName, "", imageData.FrameNo);
                                            //CreateErrorFile(autIntNo, _filmIntNo, fileName, "Error processing image file " + jpegName + " - " + e.Message, ref writer, "data", false);
                                        }

                                    }
                                    break;
                            }
                        }

                        sr.Close();
                        fs.Close();
                    }
                    catch (Exception e)
                    {
                        //writer.WriteLine("ProcessViolationsTextFile: Processing filestream " + fileName + " " + e.Message + " " + DateTime.Now.ToString());
                        tmsErrorLogDetails.TELErrMessage = "Processing filestream " + fileName + " " + e.Message;
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                        sr.Close();
                        fs.Close();
                        failed = true;
                        //CreateErrorFile(autIntNo, _filmIntNo, fileName, "Procesing error "+ e.Message, ref writer, "header_only",false);
                        return failed;
                    }


                    writer.WriteLine("Header rows processed: " + noOfHeaderRecords.ToString());

                    /////////////////////***************************************** MUST REMOVE ******************************************////////////////////////////
                    //if (violations.Count > 0)
                    //{
                    //    //need to load data that preceeds these images
                    //    failed = ValidateAndLoadInputData(ref violations, autIntNo, ref writer, fileName);

                    //    violations.Clear();

                    //    if (failed)
                    //    {
                    //        sr.Close();
                    //        fs.Close();
                    //        CreateErrorFile(autIntNo, _filmIntNo, fileName, "Data errors", ref writer, "data", false);
                    //        return failed;
                    //    }
                    //}

                    /////////////////////***************************************** MUST REMOVE ******************************************////////////////////////////

                    writer.WriteLine("Data rows processed: " + noOfDataRecords.ToString());
                    writer.WriteLine("Image rows processed: " + noOfImageRecords.ToString());

                    //delete film if frames have not been inserted
                    FilmDB film = new FilmDB(_connStr);
                    FrameDB frame = new FrameDB(_connStr);


                    writer.WriteLine();
                    writer.Flush();

                    if (noOfDataRecords > 0 && noOfImageRecords == 0)
                    {
                        //writer.WriteLine("Input file contains 0 image rows - unable to process film");
                        tmsErrorLogDetails.TELErrMessage = "Input file contains 0 image rows - unable to process film";
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                        CreateErrorFile(autIntNo, _filmIntNo, fileName, "Input file contains 0 image rows - unable to process film", ref writer, "header_only", false);
                    }
                    else if (noOfFailedImageRecords > 0)
                    {
                        //writer.WriteLine("Input file contains " + noOfFailedImageRecords.ToString() + " failed image rows - unable to process film");
                        tmsErrorLogDetails.TELErrMessage = "Input file contains " + noOfFailedImageRecords.ToString() + " failed image rows - unable to process film";
                        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                        CreateErrorFile(autIntNo, _filmIntNo, fileName, "Error processing image files", ref writer, "data", false);
                        failed = true;
                    }
                    //else if (!failed) CreateErrorFile(autIntNo, _filmIntNo, fileName, "All frames processed correctly", ref writer, "header_only", true, header.reprocess);
                    else if (!failed) CreateErrorFile(autIntNo, _filmIntNo, fileName, "All frames processed correctly", ref writer, "header_only", true);

                    QueueItemProcessor queueProcessor = new QueueItemProcessor();
                    if (!failed)
                    {
                        for (int i = 0; i < filmsLoaded.Count; i++)
                        {
                            DataSet ds = frame.GetFrameListDS(Convert.ToInt32(filmsLoaded[i]));
                            if (ds.Tables.Count > 0)
                            {
                                //foreach (DataRow dr in ds.Tables[0].Rows)
                                //{
                                //    string frameIntNo = dr["frameIntNo"].ToString();

                                //    {
                                //        QueueItem item = new QueueItem();
                                //        item.Body = frameIntNo;
                                //        DateTime dtOffence = Convert.ToDateTime(dr["offenceDate"]);
                                //        item.ActDate = dtOffence.AddDays(expiryNoOfDays);
                                //        item.QueueType = ServiceQueueTypeList.CancelExpiredViolations_Frame;
                                //        item.Group = header.AuthCode;
                                //        queueProcessor.Send(item);
                                //    }

                                //    if (!_natisLast)
                                //    {
                                //        QueueItem item = new QueueItem();
                                //        item.Body = frameIntNo;
                                //        item.Group = header.AuthCode;

                                //        DateTime dtOffence = Convert.ToDateTime(dr["offenceDate"]);
                                //        TimeSpan ts = DateTime.Now - dtOffence;
                                //        item.Priority = expiryNoOfDays - ts.Days;
                                //        item.QueueType = ServiceQueueTypeList.Frame_Natis;
                                //        queueProcessor.Send(item);
                                //    }
                                //}

                                int filmintNo = film.DeleteFilm(Convert.ToInt32(filmsLoaded[i]));
                                if (filmintNo < 0)
                                {
                                    //writer.WriteLine("ProcessViolationsTextFile: Unable to remove films with no data");
                                    //writer.WriteLine();
                                    //writer.Flush();
                                }
                            }
                        }

                        scope.Complete();
                    }
                }
            }
            catch(Exception ex)
            {
                failed = true;
                CreateErrorFile(autIntNo, _filmIntNo, fileName, "Transaction execute failed!" + ex.Message, ref writer, "header_only", false);
                return failed;
            }

            return failed;
        }

        private void PushQueue(int frameIntNo, string AuthCode, DateTime dtOffence)
        {
            QueueItemProcessor queueProcessor = new QueueItemProcessor();

            {
                QueueItem item = new QueueItem();
                item.Body = frameIntNo;
                item.ActDate = dtOffence.AddDays(noOfDays);
                item.QueueType = ServiceQueueTypeList.CancelExpiredViolations_Frame;
                item.Group = AuthCode;
                queueProcessor.Send(item);
            }

            if (!_natisLast)
            {
                QueueItem item = new QueueItem();
                item.Body = frameIntNo;
                item.Group = AuthCode;

                TimeSpan ts = DateTime.Now - dtOffence;
                item.Priority = noOfDays - ts.Days;
                item.QueueType = ServiceQueueTypeList.Frame_Natis;
                queueProcessor.Send(item);
            }
        }

        private ImageDataRow SetImageRowValues(string[] split)
        {
            ImageDataRow imageData = new ImageDataRow();

            int i = 0;
            imageData.RecordType = split[i];
            i += 1;
            imageData.Version = Convert.ToInt32(split[i]);
            i += 1;
            imageData.ContractorCode = split[i];
            i += 1;
            imageData.ImageType = split[i];            //A (A Frame), B (B Frame), R (RegNo), D (Driver), O (Other)
            i += 1;
            imageData.AuthCode = split[i];
            i += 1;
            imageData.FilmNo = split[i];
            i += 1;
            imageData.FrameNo = split[i];
            i += 1;
            imageData.ReferenceNo = split[i];
            i += 1;
            imageData.JPegName = split[i];
            i += 1;
            imageData.XValue = Convert.ToInt32(split[i]);
            i += 1;
            imageData.YValue = Convert.ToInt32(split[i]);

            //dls 081023 - add ImageSize field to export - still to be included in TOMS CDCreate and 3P Loader
            if (imageData.Version > 3)
            {
                i += 1;
                imageData.ImageSize = Convert.ToInt32(split[i]);
            }
            else
            {
                imageData.ImageSize = -1;
            }

            return imageData;
        }

        private ContractorDataRow SetDataRowValues(string[] split)
        {
            ContractorDataRow data = new ContractorDataRow();
            int i = 0;

            data.RecordType = split[i];
            i += 1;
            data.Version = Convert.ToInt32(split[i]);
            i += 1;
            data.ContractorCode = split[i];
            i += 1;
            data.AuthCode = split[i];
            i += 1;
            data.CDLabel = split[i];
            i += 1;
            data.FilmNo = split[i];
            i += 1;
            data.FrameNo = split[i];
            i += 1;
            data.Sequence = split[i];
            i += 1;
            data.RegNo = split[i];
            i += 1;
            data.ReferenceNo = split[i];
            i += 1;
            data.OffenceDateYear = split[i];
            i += 1;
            data.OffenceDateMonth = split[i];
            i += 1;
            data.OffenceDateDay = split[i];
            i += 1;
            data.OffenceDateHour = split[i];
            i += 1;
            data.OffenceDateMinute = split[i];
            i += 1;
            data.FirstSpeed = Convert.ToInt32(split[i]);
            i += 1;
            data.SecondSpeed = Convert.ToInt32(split[i]);
            i += 1;
            data.ElapsedTime = split[i];
            i += 1;
            data.VehMCode = split[i];
            i += 1;
            data.VehMDescr = split[i];
            i += 1;
            data.VehTCode = split[i];
            i += 1;
            data.VehTDescr = split[i];
            i += 1;
            data.CourtNo = split[i];
            i += 1;
            data.CourtName = split[i];
            i += 1;
            data.OfficerNo = split[i];
            i += 1;
            data.OfficerGroup = split[i];
            i += 1;
            data.OfficerSName = split[i];
            i += 1;
            data.OfficerInit = split[i];
            i += 1;
            data.CameraID = split[i];
            i += 1;
            data.CameraLocCode = split[i];
            i += 1;
            data.LocDescr = split[i];
            i += 1;
            data.OffenceCode = split[i];
            i += 1;
            data.OffenceDescr = split[i];
            i += 1;
            data.OffenceLetter = split[i];
            i += 1;
            data.OffenderType = Convert.ToInt32(split[i]);
            i += 1;
            data.FineAmount = Convert.ToDouble(split[i]);
            i += 1;
            data.FineAllocation = split[i];
            i += 1;
            data.SpeedZone = Convert.ToInt32(split[i]);
            i += 1;
            data.RdTypeCode = split[i];
            i += 1;
            data.RdTypeDescr = split[i];
            i += 1;
            data.TravelDirection = split[i];
            i += 1;
            data.RejectReason = split[i];
            i += 1;
            data.Violation = split[i];
            i += 1;
            data.ConfirmViolation = split[i];
            i += 1;
            data.ManualView = split[i];
            i += 1;
            data.MultFrames = split[i];

            //dls 061020 - add CamSerialNo field to export
            if (data.Version > 1)
            {
                i += 1;
                data.CamSerialNo = split[i];

                //FT 100430 For Average speed over distance
                //i += 1;

                if (data.Version > 2)
                {
                    if (split.Length > i && split[i].Trim().Length > 0)
                    {
                        //data.ASD2ndCameraIntNo = Convert.ToInt32(split[i]);

                        i += 1;
                        if (split[i].Trim().Length > 0)
                            data.ASDGPSDateTime1 = Convert.ToDateTime(split[i]);

                        i += 1;
                        if (split[i].Trim().Length > 0)
                            data.ASDGPSDateTime2 = Convert.ToDateTime(split[i]);

                        i += 1;
                        data.ASDTimeDifference = Convert.ToInt32(split[i]);

                        i += 1;
                        data.ASDSectionStartLane = Convert.ToInt32(split[i]);

                        i += 1;
                        data.ASDSectionEndLane = Convert.ToInt32(split[i]);

                        i += 1;
                        data.ASDSectionDistance = Convert.ToInt32(split[i]);

                        i += 1;
                        data.ASD1stCamUnitID = split[i].ToString();

                        i += 1;
                        data.ASD2ndCamUnitID = split[i].ToString();

                        i += 1;
                        data.ASDCameraSerialNo1 = split[i].ToString();

                        i += 1;
                        data.ASDCameraSerialNo2 = split[i].ToString();
                    }
                }
            }
            else
            {
                data.CamSerialNo = "";
            }

            return data;
        }

        private HeaderRow SetHeaderRowValues(string[] split)
        {
            HeaderRow header = new HeaderRow();

            int i = 0;
            header.RecordType = split[i];
            i += 1;
            header.Version = Convert.ToInt32(split[i]);
            i += 1;
            header.ContractorCode = split[i];
            i += 1;
            header.AuthCode = split[i];
            i += 1;
            header.AuthName = split[i];
            i += 1;
            header.AuthNo = split[i];
            i += 1;
            header.CDLabel = split[i];
            i += 1;
            header.FilmNo = split[i];
            i += 1;
            header.FilmDescr = split[i];
            i += 1;
            header.MultipleFrames = split[i];
            i += 1;
            header.MultipleViolations = split[i];
            i += 1;
            header.LastRefNo = Convert.ToInt32(split[i]);
            i += 1;
            header.FilmType = split[i];
            i += 1;
            header.FirstOffenceDateYear = split[i];
            i += 1;
            header.FirstOffenceDateMonth = split[i];
            i += 1;
            header.FirstOffenceDateDay = split[i];
            i += 1;
            header.FirstOffenceDateHour = split[i];
            i += 1;
            header.FirstOffenceDateMinute = split[i];
            //dls 060801 - add Interface field to export
            if (header.Version > 1)
            {
                i += 1;
                _interface = split[i];
            }
            else
            {
                _interface = "";
            }

            return header;
        }

        public bool ProcessHeaderRow(HeaderRow header, ref StreamWriter writer, string fileName, int autIntNo,
             ref ArrayList filmsLoaded)
        {
            tmsErrorLogDetails.TELProcName = "ProcessHeaderRow";

            bool failed = false;

            string errorMsg = "";

            ContractorDB contractor = new ContractorDB(_connStr);

            ContractorDetails conDetails = contractor.GetContractorDetailsByConCode(header.ContractorCode);

            if (conDetails.ConIntNo == 0)
            {
                //writer.WriteLine("ProcessHeaderRow: Contractor code: " + header.ContractorCode + " does not exist" + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Contractor code: " + header.ContractorCode + " does not exist";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                failed = true;
                return failed;
            }

            int filmIntNo = CheckFilmIntNo(header.FilmNo, autIntNo, ref writer, fileName);

            if (filmIntNo < -1)
            {
                failed = true;
                return failed;
            }
            FilmDB film = new FilmDB(_connStr);

            string offenceDateStr = header.FirstOffenceDateYear + "/" + header.FirstOffenceDateMonth
                + "/" + header.FirstOffenceDateDay + " " + header.FirstOffenceDateHour + ":"
                + header.FirstOffenceDateMinute + ":00";

            DateTime offenceDate = Convert.ToDateTime(offenceDateStr);

            int addFilmIntNo = film.AddFilm(autIntNo, conDetails.ConIntNo, header.FilmNo, header.CDLabel, 0, 0,
                header.MultipleFrames, header.MultipleViolations, header.FilmDescr, header.LastRefNo, header.FilmType,
                DateTime.Now, "V", Path.GetFileName(fileName), offenceDate, _ftpParam.ftpProcess, ref errorMsg, filmIntNo);

            if (addFilmIntNo == 0)
            {
                //writer.WriteLine("ProcessHeaderRow: unable to add film: " + header.FilmNo + " " + errorMsg + " " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Unable to add film: " + header.FilmNo + " " + errorMsg;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                failed = true;
                CreateErrorFile(autIntNo, 0, fileName, "Unable to add film " + header.FilmNo + " " + errorMsg, ref writer, "header_only", false);
                return failed;
            }
            else
            {
                if (addFilmIntNo < -1)
                {
                    //writer.WriteLine("ProcessHeaderRow: unable to add film: " + header.FilmNo + " already exists and has already been adjudicated and the status has changed" + DateTime.Now.ToString());
                    tmsErrorLogDetails.TELErrMessage = "Unable to add film: " + header.FilmNo + " already exists and has already been adjudicated and the status has changed";
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                    CreateErrorFile(autIntNo, 0, fileName, "Unable to add film " + header.FilmNo + " already exists and has already been adjudicated and the status has changed", ref writer, "header_only", false);
                    return true;
                }

                filmsLoaded.Add(addFilmIntNo);
                _filmIntNo = addFilmIntNo;
                writer.WriteLine("ProcessHeaderRow: header row for Film " + header.FilmNo + " successfully added " + DateTime.Now.ToString());
                writer.WriteLine();
                writer.Flush();
            }

            return failed;

        }

        public bool ProcessDataRow(ref ArrayList violations, ContractorDataRow data, ref StreamWriter writer, string fileName, int autIntNo, string savelocation)
        {
            bool failed = false;

            //FilmDB film = new FilmDB(_connStr);

            //int filmIntNo = film.CheckFilmExists(autIntNo, data.FilmNo);

            //if (filmIntNo < 0)
            //{
            //    writer.WriteLine("ProcessDataRow: unable to add frame, film " + data.FilmNo + " does not exist" + DateTime.Now.ToString());
            //    writer.WriteLine();
            //    writer.Flush();
            //    failed = true;
            //    return failed;
            //}

            LoadInputDataFromTextFile(ref violations, data, autIntNo, ref writer, fileName, data.FilmNo);

            return failed;

        }

        //private bool ValidateAndLoadInputData(ref ArrayList violations, int autIntNo, ref StreamWriter writer, string fileName, bool bReprocess)
        private bool ValidateAndLoadInputData(ref ArrayList violations, int autIntNo, ref StreamWriter writer, string fileName)
        {
            tmsErrorLogDetails.TELProcName = "ValidateAndLoadInputData";

            bool failed = false;

            LoadViolationsDB loadViolations = new LoadViolationsDB(_connStr);

            //pass whole arraylist with all rows to class
            string errMessage = string.Empty;
            int noOfRecords = loadViolations.AddTempLoadViolations(violations, 0, ref errMessage);
            if (noOfRecords <= 0)
            {
                //writer.WriteLine("ProcessDataRow: unable to load data" + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Unable to load data. " + errMessage;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                CreateErrorFile(autIntNo, _filmIntNo, fileName, "Unable to load data for film", ref writer, "header_only", false);
                return true;
            }

            //validate input data
            errMessage = string.Empty;
            int invalidRows = loadViolations.ValidateTempLoadViolations(autIntNo, 0, ViolationLoader.APP_NAME, ref errMessage);

            if (!errMessage.Equals(string.Empty))
            {
                //writer.WriteLine("ValidateAndLoadInputData: ValidateTempLoadViolations error - " + errMessage + " " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "ValidateTempLoadViolations error - " + errMessage;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                return true;
            }

            string error = "";

            if (invalidRows > 0)
            {
                error = "There";

                if (invalidRows != 1)
                    error += " are " + invalidRows + " invalid rows";
                else
                    error += " is " + invalidRows + " invalid row";

                string file = System.IO.Path.GetFileName(fileName);

                error += " - unable to load data. Please view error report and fix the errors before reloading file " + file;

                //writer.WriteLine("ValidateAndLoadInputData: " + error + " " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = error;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                //at this point we need to attach the error pdf!!!

                LoadViolError lve = new LoadViolError();

                try
                {
                    lve.LoadReport(autIntNo, 0, _connStr, ref writer, _ftpParam, _currentContractor);
                }
                catch (Exception e)
                {
                    //writer.WriteLine("ValidateAndLoadInputData: Load report " + e.Message + " " + DateTime.Now.ToString());
                    tmsErrorLogDetails.TELErrMessage = "Load report " + e.Message;
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                }

                //CreateErrorFile(autIntNo, _filmIntNo, fileName, "Data errors", ref writer, "data", false);

                return true;
            }

            if (invalidRows == -1)
            {
                //writer.WriteLine("ValidateAndLoadInputData: " + error + " " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = error;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                return true;
            }

            string errorMsg = "";
            int statusLoaded;

            if (_natisLast)
                statusLoaded = STATUS_NATIS_LAST;
            else
                statusLoaded = STATUS_NATIS_FIRST;

            //Add Authority Coding system
            AuthorityDB authDB = new AuthorityDB(this._connStr);
            AuthorityDetails authDetail = authDB.GetAuthorityDetails(autIntNo);

            string aut3PCodingSystem = authDetail.Aut3PCodingSystem;

            if ((!aut3PCodingSystem.Equals("S") && !aut3PCodingSystem.Equals("T") && !aut3PCodingSystem.Equals("C")))
                aut3PCodingSystem = "S";

            //add camera set up rejection reason - this one actually uses the string RejResaon in the SP, not the integer key
            RejectionDB rejDB = new RejectionDB(this._connStr);
            string lowSpeedRejReason = "Camera Setup - incorrect speed limit";

            int lowSpeedRejIntNo = rejDB.UpdateRejection(0, lowSpeedRejReason, "N", this._ftpParam.ftpProcess, 0, "Y");

            if (lowSpeedRejIntNo <= 0)
            {
                tmsErrorLogDetails.TELErrMessage = "Unable to create reason for rejection: " + lowSpeedRejReason;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                failed = true;
                return failed;
            }

            //add low speed rejection reason
            string invalidSpeed = "Zero or low speed - unable to prosecute";

            lowSpeedRejIntNo = rejDB.UpdateRejection(0, invalidSpeed, "N", this._ftpParam.ftpProcess, 0, "Y");

            if (lowSpeedRejIntNo <= 0)
            {
                tmsErrorLogDetails.TELErrMessage = "Unable to create reason for rejection: " + invalidSpeed;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                failed = true;
                return failed;
            }

            //add vehicle exclusion rejection reason
            string exclRejReason = "Official vehicle (under assigned limit)";

            int exclRejIntNo = rejDB.UpdateRejection(0, exclRejReason, "N", this._ftpParam.ftpProcess, 0, "Y");

            if (exclRejIntNo <= 0)
            {
                tmsErrorLogDetails.TELErrMessage = "Unable to create reason for rejection: " + exclRejReason;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                failed = true;
                return failed;
            }

            string expiredFrame = "Expired. The frame offence date is too old to continue";

            int rejIntNoExpired = rejDB.UpdateRejection(0, expiredFrame, "N", this._ftpParam.ftpProcess, 0, "Y");

            if (rejIntNoExpired <= 0)
            {
                tmsErrorLogDetails.TELErrMessage = "Unable to create reason for rejection: " + expiredFrame;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                failed = true;
                return failed;
            }

            //violation cutoff
            SysParamDetails details = new SysParamDetails();

            details.SPColumnName = SysParamDetails.VIOLATION_CUT_OFF;
            details.SPIntegerValue = 0;
            details.SPStringValue = "Y";
            details.LastUser = this._ftpParam.ftpProcess;

            DefaultSysParam sysParam = new DefaultSysParam(details, this._connStr);

            KeyValuePair<int, string> spDetails = sysParam.SetDefaultSysParam();

            string cutOff = spDetails.Value.Equals("-") ? "Y" : spDetails.Value;

            //authority rule for bypassing verification for specific reasons for rejection
            AuthorityRulesDetails arDetails = new AuthorityRulesDetails();
            arDetails.AutIntNo = autIntNo;
            arDetails.ARCode = "0510";
            arDetails.LastUser = this._ftpParam.ftpProcess;

            DefaultAuthRules defAR = new DefaultAuthRules(arDetails, this._connStr);
            KeyValuePair<int, string> aRule = defAR.SetDefaultAuthRule();
            string skipRejReasons = aRule.Value;

            //authority rule for bypassing verification for RegNo = 00000000
            arDetails.ARCode = "0520";
            arDetails.LastUser = this._ftpParam.ftpProcess;

            defAR = new DefaultAuthRules(arDetails, this._connStr);
            aRule = defAR.SetDefaultAuthRule();
            string skipZeroes = aRule.Value;

            //Fred 2012/02/22 move to top level of the ProcessViolationFile(...)

            //no of days from Offence Date to Issue Date
            //DateRulesDetails drDetails = new DateRulesDetails();
            //drDetails.AutIntNo = autIntNo;
            //drDetails.DtRStartDate = "NotOffenceDate";
            //drDetails.DtREndDate = "NotIssue1stNoticeDate";
            //drDetails.LastUser = this._ftpParam.ftpProcess;

            //DefaultDateRules dr = new DefaultDateRules(drDetails, this._connStr);
            //int noOfDays = dr.SetDefaultDateRule();

            int success = loadViolations.AddFrameFromTempLoadViolations(autIntNo, 0, _ftpParam.ftpProcess,
                ref errorMsg, statusLoaded, aut3PCodingSystem, lowSpeedRejReason, cutOff, skipRejReasons, skipZeroes,
                lowSpeedRejIntNo, noOfDays, _fullAdjudicated, _excluListActive, _excluSpeedAllowance, exclRejIntNo,
                STATUS_OFFICIAL_VEHICLE_EXCLUSION, this.asdInvalidSetup, this.asdInvalidRejIntNo, rejIntNoExpired);

            if (success == 0)
            {
                writer.WriteLine("Data rows processed successfully");
                writer.WriteLine();
                writer.Flush();
            }
            else if (success == 1)
            {
                tmsErrorLogDetails.TELErrMessage = "No date rules for authority";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                CreateErrorFile(autIntNo, _filmIntNo, fileName, "No date rules for authority", ref writer, "header_only", false);
                failed = true;
            }
            else if (success == 3)
            {
                tmsErrorLogDetails.TELErrMessage = "Some frames have offence codes that do not exist in TMS - Frame_Offence records not added ";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
            }
            else if (success == -1)
            {
                tmsErrorLogDetails.TELErrMessage = errorMsg;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                CreateErrorFile(autIntNo, _filmIntNo, fileName, "Unable to process data rows " + errMessage, ref writer, "header_only", false);
                failed = true;
            }

            return failed;
        }

        private void LoadInputDataFromTextFile(ref ArrayList violations, ContractorDataRow data, int autIntNo, ref StreamWriter writer, string fileName, string filmNo)
        {
            LoadViolationsDetail lvDetail = new LoadViolationsDetail();

            lvDetail.CameraID = data.CameraID;
            lvDetail.OffenceType = data.OffenceLetter;
            lvDetail.FilmNo = data.FilmNo;
            lvDetail.FineAlloc = data.FineAllocation;
            lvDetail.FrameNo = data.FrameNo;
            lvDetail.LaneNo = "0"; //data.LaneNo;		//missing from file - not yet in ARS
            lvDetail.LocDescr = data.LocDescr;
            lvDetail.LocCode = data.CameraLocCode;
            lvDetail.OffenceCode = data.OffenceCode;

            string offenceDateStr = data.OffenceDateYear + "/" + data.OffenceDateMonth
               + "/" + data.OffenceDateDay + " " + data.OffenceDateHour + ":"
               + data.OffenceDateMinute + ":00";

            lvDetail.OffenceDate = Convert.ToDateTime(offenceDateStr);
            lvDetail.OffenderType = data.OffenderType.ToString();
            lvDetail.OfficerSName = data.OfficerSName;
            lvDetail.OfficerInit = data.OfficerInit;
            lvDetail.OfficerGroup = data.OfficerGroup;
            lvDetail.OfficerNo = data.OfficerNo;
            lvDetail.RefNo = data.ReferenceNo;
            lvDetail.RegNo = data.RegNo;
            lvDetail.Speed1 = data.FirstSpeed;
            lvDetail.Speed2 = data.SecondSpeed;
            lvDetail.SeqNo = data.Sequence;
            lvDetail.Source = "LV";			//data.Source;
            lvDetail.SpeedLimit = data.SpeedZone.ToString();
            lvDetail.VehicleMake = data.VehMDescr;
            lvDetail.VehicleType = data.VehTDescr;
            lvDetail.VehicleMakeCode = data.VehMCode;
            lvDetail.VehicleTypeCode = data.VehTCode;
            lvDetail.CDLabel = data.CDLabel;
            lvDetail.JPegNameA = "";
            lvDetail.JPegNameB = "";
            lvDetail.RegNoImage = "";
            lvDetail.RdTypeCode = data.RdTypeCode;
            lvDetail.RdTypeDescr = data.RdTypeDescr;
            lvDetail.ElapsedTime = data.ElapsedTime;
            lvDetail.TravelDirection = data.TravelDirection;
            lvDetail.CourtNo = data.CourtNo;
            lvDetail.FileDate = DateTime.Now;
            lvDetail.CourtName = data.CourtName;
            lvDetail.AutIntNo = autIntNo;
            lvDetail.RejReason = data.RejectReason;
            lvDetail.ManualView = data.ManualView;
            lvDetail.Violation = data.Violation;
            lvDetail.ConfirmViolation = data.ConfirmViolation;
            lvDetail.MultFrames = data.MultFrames;
            lvDetail.Interface = _interface;
            //dls 061120 - added
            lvDetail.CamSerialNo = data.CamSerialNo;

            //FT 100505 For Average speed over distance
            //lvDetail.ASD2ndCameraIntNo = data.ASD2ndCameraIntNo;

            lvDetail.ASDGPSDateTime1 = data.ASDGPSDateTime1;
            lvDetail.ASDGPSDateTime2 = data.ASDGPSDateTime2;

            lvDetail.ASDTimeDifference = data.ASDTimeDifference;
            lvDetail.ASDSectionStartLane = data.ASDSectionStartLane;
            lvDetail.ASDSectionEndLane = data.ASDSectionEndLane;
            lvDetail.ASDSectionDistance = data.ASDSectionDistance;

            lvDetail.ASD1stCamUnitID = data.ASD1stCamUnitID;
            lvDetail.ASD2ndCamUnitID = data.ASD2ndCamUnitID;
            //lvDetail.LCSIntNo = data.LCSIntNo;
            lvDetail.ASDCameraSerialNo1 = data.ASDCameraSerialNo1;
            lvDetail.ASDCameraSerialNo2 = data.ASDCameraSerialNo2;

            violations.Add(lvDetail);
        }

        //public bool ProcessImageRow(ImageDataRow imageData, int autIntNo, string jpegName, ref StreamWriter writer)
        public bool ProcessImageRow(ImageDataRow imageData, int autIntNo, string jpegName, ref StreamWriter writer, string fileName, string AutCode, ref int lastPushedFrameIntNo)
        {
            tmsErrorLogDetails.TELProcName = "ProcessImageRow";

            bool failed = false;

            string frameNo = imageData.FrameNo;
            string filmNo = imageData.FilmNo;

            FilmDB film = new FilmDB(_connStr);

            int filmIntNo = film.CheckFilmExists(autIntNo, filmNo);

            if (filmIntNo == -1 | filmIntNo == 0)
            {
                //all systems go, we can continue
            }
            else if (filmIntNo == -2)
            {
                //writer.WriteLine("ProcessImageRow: film " + filmNo + " has already been successfully loaded " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Film " + filmNo + " has already been successfully loaded";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                //CreateErrorFile(autIntNo, 0, ts1FileName, "Film " + filmNo + " has already been successfully loaded", ref writer, "header_only", false);
                return failed;

            }
            else if (filmIntNo == -3)
            {
                //writer.WriteLine("ProcessImageRow: violations on film " + filmNo + " have already been validated " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Violations on film " + filmNo + " have already been validated";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                //CreateErrorFile(autIntNo, 0, ts1FileName, "Violations on film " + filmNo + " have already been validated", ref writer, "header_only", false);
                return failed;
            }

            //need to find out what type of film this is
            FilmDetails filmDet = film.GetFilmDetails(filmIntNo);
            string filmType = filmDet.FilmType;

            FrameDB frame = new FrameDB(_connStr);
            LoadViolationsDB load = new LoadViolationsDB(_connStr);

            int frameIntNo = frame.CheckFrameExists(filmIntNo, frameNo);

            if (frameIntNo < 1)
            {
                //writer.WriteLine("ProcessImageRow: frame " + frameNo + " does not exist " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Frame " + frameNo + " does not exist";
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, filmNo, "Cannot load image as frame does not exist", "Image", "", "", frameNo);
                //CreateErrorFile(autIntNo, _filmIntNo, ts1FileName, "frame " + frameNo + " does not exist on film " + filmNo, ref writer, "data", false);
                failed = true;
                return failed;
            }

            int scImPrintVal = this.GetPrintValue(filmType, imageData.JPegName, imageData.ImageType);

            //add ScanImage row
            ScanImageDB scan = new ScanImageDB(_connStr);
            string errMessage = string.Empty;

            int scImIntNo = scan.AddScanImage(frameIntNo, imageData.ImageType, imageData.JPegName, scImPrintVal,
                imageData.XValue, imageData.YValue, _ftpParam.ftpProcess, ref errMessage);

            if (scImIntNo < 1)
            {
                //writer.WriteLine("ProcessImageRow: unable to load ScanImage for frame " + frameNo + " and jpeg " + imageData.JPegName + " " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = string.Format("Unable to load ScanImage for frame {0} and jpeg {1}: {2}", frameNo, imageData.JPegName, errMessage);
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, filmNo, tmsErrorLogDetails.TELErrMessage, "Jpeg name", imageData.JPegName, "", frameNo);
                //CreateErrorFile(autIntNo, _filmIntNo, fileName, "unable to load ScanImage for frame " + frameNo + " and jpeg " + imageData.JPegName, ref writer, "data", false);
                failed = true;
                return failed;
            }

            // David Lin 20100326 Remove images from database
            //AccessDBMethods access = new AccessDBMethods(_connStr);
            //Int32 fileLength = 0;


            //failed = access.UpdateBLOB("ScanImage", "ScanImage", "ScImIntNo", scImIntNo, jpegName, "image", ref fileLength, ref errMessage);

            //if (failed)
            //{
            //    tmsErrorLogDetails.TELErrMessage = string.Format("Unable to load scan image for frame {0} and jpeg {1}: {2}", frameNo, imageData.JPegName, errMessage);
            //    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

            //    Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, filmNo, string.Format("Unable to load scan image - {0}", errMessage), "Jpeg name", imageData.JPegName, "", frameNo);
            //    return failed;
            //}

            ////dls 081023 - need to check that the image size of the image just loaded is the same size that the actual image
            //Int32 imageSize = scan.CheckImageSize(scImIntNo, ref errMessage);

            //if (imageSize == 0                                                      // it is null or 0 length
            //    || imageSize != fileLength                                          // db image diff size to disk image
            //    || (imageData.ImageSize > -1                                        // if image size passed thru from CD Create,
            //        && imageSize != imageData.ImageSize))                           //              check that it matches DB image
            //{
            //    //writer.WriteLine("ProcessImageRow: Length of loaded image does not match size of Jpeg immge for frame " + frameNo + " and jpeg " + imageData.JPegName + " " + DateTime.Now.ToString());
            //    tmsErrorLogDetails.TELErrMessage = string.Format("Length of loaded image does not match size of Jpeg image for frame {0} and jpeg {1}: {2}", frameNo, imageData.JPegName, errMessage);
            //    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

            //    Int32 tlvIntNo = load.AddTempLoadViolationsErrors(autIntNo, filmNo, tmsErrorLogDetails.TELErrMessage, "Jpeg name", imageData.JPegName, "", frameNo);
            //    //CreateErrorFile(autIntNo, _filmIntNo, fileName, "Length of loaded image does not match size of Jpeg immge for frame " + frameNo + " and jpeg " + imageData.JPegName, ref writer, "data", false);
            //    failed = true;
            //    return failed;
            //}

            // Move images to default image server and update database status
            failed = RemoveImageToImageFileServer(scImIntNo, filmDet, jpegName, autIntNo, frameNo, ref writer, imageData.AuthCode.Trim());


            //Fred push queue
            if (!failed && lastPushedFrameIntNo != frameIntNo)
            {
                try
                {
                    FrameDetails frameDetails = frame.GetFrameDetails(frameIntNo);

                    //PushQueue(frameIntNo, "", frameDetails.OffenceDate);
                    //Oscar 20120320 add authcode
                    PushQueue(frameIntNo, imageData.AuthCode.Trim(), frameDetails.OffenceDate);
                    lastPushedFrameIntNo = frameIntNo;
                }
                catch(Exception ex)
                {
                    tmsErrorLogDetails.TELErrMessage = ex.Message;
                    tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                    CreateErrorFile(autIntNo, _filmIntNo, fileName, "Unable to push queue: FrameIntNo(" + frameIntNo.ToString() + ") " + errMessage, ref writer, "header_only", false);
                    failed = true;

                    Environment.Exit(-1);
                }
            }
            

            ////dls 070808 - save jpeg to file system
            //if (_saveToFileSystem)
            //{
            //    bool saved = SaveImageToDisk(scImIntNo, jpegName, filmDet.FilmNo, ref writer);

            //    if (!saved)
            //        failed = true;
            //}

            return failed;
        }

        private bool RemoveImageToImageFileServer(int scImIntNo, FilmDetails film, string jpegName, int autIntNo, string frameNo, ref StreamWriter writer, string authCode)
        {
            bool failed = false;

            //AuthorityDB authorityDB = new AuthorityDB(_connStr);
            //AuthorityDetails authority = authorityDB.GetAuthorityDetails(autIntNo);
            string strImagePath = string.Empty;
            try
            {
                string strFrameImagePath = authCode;            //authority.AutCode.Trim();
                strFrameImagePath += @"\" + film.FirstOffenceYear;
                strFrameImagePath += @"\" + film.FirstOffenceMonth;
                strFrameImagePath += @"\" + film.FilmNo;
                strFrameImagePath += @"\" + frameNo;

                string strFrameFolder = @"\\" + _imageServer.ImageMachineName;
                strFrameFolder += @"\" + _imageServer.ImageShareName;
                strFrameFolder += @"\" + strFrameImagePath;

                //check that remote folder exists
                if (!System.IO.Directory.Exists(strFrameFolder))
                    Directory.CreateDirectory(strFrameFolder);

                string fileName = Path.GetFileName(jpegName);

                strImagePath = strFrameFolder + @"\" + fileName;

                //DLS 2010-12-31 - we should not be deleting the images at this point - when there are duplicate frames they need the images from the previous frame
                //all the images should get copied to the Received folder when the film has loaded
                //if (!File.Exists(strImagePath))
                //    File.Move(jpegName, strImagePath);
                //else
                //    File.Delete(jpegName);

                if (!File.Exists(strImagePath))
                    File.Copy(jpegName, strImagePath);

                // update Frame table
                ScanImageDB imgDB = new ScanImageDB(this._connStr);
                string strError = string.Empty;
                imgDB.UpdateFrameInfoAfterRemoveImage(scImIntNo, _imageServer.IFSIntNo, strFrameImagePath, out strError, ViolationLoader.APP_NAME);
                if (strError != string.Empty)
                {
                    failed = true;
                }
            }
            catch (Exception ex)
            {
                tmsErrorLogDetails.TELErrMessage = "Unable RemoveImageToImageFileServer: " + strImagePath + ex.Message;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);
                failed = true;
            }

            return failed;
        }

        private int GetPrintValue(string filmType, string jpegName, string imageType)
        {
            int scImPrintVal = 0;
            string whichImage_1st = DIG_FILM_FIRST_IMAGE;
            string whichImage_2nd = DIG_FILM_SPD_2ND_IMAGE;

            switch (filmType)
            {
                case "O":
                    if (jpegName.ToLower().Contains("_001.j") && imageType.Equals("A"))
                        scImPrintVal = 1;
                    else if (jpegName.ToLower().Contains("002.j") && imageType.Equals("A"))
                        scImPrintVal = 2;
                    else if (imageType.Equals("R"))
                        scImPrintVal = 3;
                    else if (imageType.Equals("D"))
                        scImPrintVal = 4;
                    break;
                case "U":
                    if (jpegName.ToLower().Contains("_001.j") && imageType.Equals("A"))
                        scImPrintVal = 1;
                    else if (jpegName.ToLower().Contains("002.j") && imageType.Equals("A"))
                        scImPrintVal = 2;
                    else if (imageType.Equals("R"))
                        scImPrintVal = 3;
                    else if (imageType.Equals("D"))
                        scImPrintVal = 4;
                    break;

                case "N":               //normal wet film             
                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            break;

                        case 2:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            break;

                        case 3:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            break;

                        case 4:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 3;
                            break;
                    }

                    break;

                case "Y":               //truvella wet film
                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            break;

                        case 2:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            break;

                        case 3:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            break;

                        case 4:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (jpegName.ToLower().Contains(WET_FILM_FIRST_IMAGE) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 3;
                            break;

                    }
                    break;

                case "P":               //TraffiPax digital speed film
                    //DLS 070608 - need to introduce AuthRule to determine which image is the for printing

                    if (_whichImageTraffiPax != 1)
                    {
                        whichImage_1st = DIG_FILM_SPD_2ND_IMAGE;
                        whichImage_2nd = DIG_FILM_FIRST_IMAGE;
                    }

                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_2nd) && imageType.Equals("A"))
                                scImPrintVal = 2;
                            break;

                        case 2:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            break;

                        case 3:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            break;

                        case 4:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 3;
                            break;

                    }
                    break;

                case "D":               //red light digital film
                    //DLS 070608 - need to introduce AuthRule to determine which image is the for printing

                    if (_whichImageDigital != 1)
                    {
                        whichImage_1st = DIG_FILM_SPD_2ND_IMAGE;
                        whichImage_2nd = DIG_FILM_FIRST_IMAGE;
                    }

                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_2nd) && imageType.Equals("A"))
                                scImPrintVal = 2;
                            break;

                        case 2:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            break;

                        case 3:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            break;

                        case 4:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else if (imageType.Equals("D"))
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (jpegName.ToLower().Contains(whichImage_1st) && imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            break;

                    }
                    break;

                case "A":               //Anpri camera - single images for bus lane violations - dls 090410
                case "C":               //DigiCam film - single images
                case "B":               //Belstow camera - single images
                default:
                    switch (_noOfImagesToPrint)
                    {
                        case 0:
                            scImPrintVal = 0;
                            break;

                        case 1:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else
                                scImPrintVal = 0;
                            break;

                        case 2:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("B"))
                                scImPrintVal = 2;
                            else
                                scImPrintVal = 0;
                            break;

                        case 3:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else
                                scImPrintVal = 0;
                            break;

                        case 4:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("B"))
                                scImPrintVal = 2;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 3;
                            else
                                scImPrintVal = 4;
                            break;

                        case 5:
                            if (imageType.Equals("A"))
                                scImPrintVal = 1;
                            else if (imageType.Equals("R"))
                                scImPrintVal = 2;
                            else
                                scImPrintVal = 3;
                            break;

                    }
                    break;
            }

            return scImPrintVal;
        }

        //private bool SaveImageToDisk(int scImIntNo, string jpegName, string filmNo, ref StreamWriter writer)
        //{
        //    tmsErrorLogDetails.TELProcName = "SaveImageToDisk";

        //    bool saved = false;
        //    string filePath = _ftpParam.imageFolder + "\\" + filmNo;
        //    //string fileName = scImIntNo.ToString() + "_" + System.IO.Path.GetFileName(jpegName);
        //    string fileName = System.IO.Path.GetFileName(jpegName);

        //    try
        //    {
        //        //check that film folder exists
        //        if (!System.IO.Directory.Exists(filePath))
        //            Directory.CreateDirectory(filePath);

        //        //save image
        //        System.IO.File.Copy(jpegName, filePath + "\\" + fileName, true);

        //        saved = true;
        //    }
        //    catch (Exception e)
        //    {
        //        //writer.WriteLine("SaveImageToDisk: unable to save image " + filePath + "\\" + fileName + " to disk " + e.Message);
        //        tmsErrorLogDetails.TELErrMessage = "Unable to save image " + filePath + "\\" + fileName + " to disk " + e.Message;
        //        tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

        //        saved = false;
        //    }

        //    return saved;
        //}

        private bool DeleteTempViolations(ref StreamWriter writer, int autIntNo, int tlvType)
        {
            tmsErrorLogDetails.TELProcName = "DeleteTempViolations";

            bool failed = false;

            //delete current rows for user in temp table
            LoadViolationsDB loadViolations = new LoadViolationsDB(_connStr);

            try
            {
                loadViolations.DeleteTempLoadViolations(autIntNo, tlvType);
            }
            catch (Exception e)
            {
                //writer.WriteLine("LoadTempViolations: unable to delete temporary table " + e.Message + " " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = "Unable to delete temporary table " + e.Message;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                failed = true;
            }
            return failed;

        }

        private bool DeleteTempViolations(int autIntNo, ref StreamWriter writer)
        {
            tmsErrorLogDetails.TELProcName = "DeleteTempViolations";

            bool failed = false;
            string error = "";

            //clear temp file
            LoadViolationsDB load = new LoadViolationsDB(_connStr);

            try
            {
                //int invalidRows = load.DeleteTempLoadViolations(autIntNo, 1);
                load.DeleteTempLoadViolations(autIntNo, 0);
            }
            catch
            {
                error = "Problem deleting temporary loading tables!";
                //writer.WriteLine("DeleteTempViolations: " + error + " " + DateTime.Now.ToString());
                tmsErrorLogDetails.TELErrMessage = error;
                tmsErrorLog.WriteToErrorLog(tmsErrorLogDetails, ref writer);

                failed = true;
                return failed;
            }

            return true;
        }
    }
}
