using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Stalberg.TMS.ViolationLoader.Components
{
    public class DBFunctions
    {
        public DBFunctions()
		{
		}

        public class AccessDBSettings
        {
            //added elements to struct
            public String loadDBProvider;
            public String loadDBPwd;
        }

        public class DBSettings
        {
            //added elements to struct
            public String dbProvider;
            public String dbServer;
            public String dbDatabase;
            public String dbUser;
            public String dbPwd;
            public int dbSSPI;
        }

        public DBSettings SetDBSettings (XmlNode root)
        {
            DBSettings db = new DBSettings();
            //Assign the contents of the child nodes to application variables
            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    switch (root.ChildNodes[i].Name)
                    {
                        case "dbProvider":
                            db.dbProvider = root.ChildNodes[i].InnerText;
                            break;
                        case "dbServer":
                            db.dbServer = root.ChildNodes[i].InnerText;
                            break;
                        case "dbDatabase":
                            db.dbDatabase = root.ChildNodes[i].InnerText;
                            break;
                        case "dbUser":
                            db.dbUser = root.ChildNodes[i].InnerText;
                            break;
                        case "dbPwd":
                            db.dbPwd = root.ChildNodes[i].InnerText;
                            break;
                        case "dbSSPI":
                            db.dbSSPI = Convert.ToInt32(root.ChildNodes[i].InnerText);
                            break;
                    }
                }
            }

            return db;
        }

        public AccessDBSettings SetAccessDBSettings(XmlNode root)
        {
            AccessDBSettings db = new AccessDBSettings();
            //Assign the contents of the child nodes to application variables
            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    switch (root.ChildNodes[i].Name)
                    {
                        case "loadDBProvider":
                            db.loadDBProvider = root.ChildNodes[i].InnerText;
                            break;
                        case "loadDBPwd":
                            db.loadDBPwd = root.ChildNodes[i].InnerText;
                            break;
                    }
                }
            }

            return db;
        }
    }
}
